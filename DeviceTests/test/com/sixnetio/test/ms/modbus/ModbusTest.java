/*
 * ModbusTest.java
 *
 * Tests the Modbus server in Managed Switches.
 *
 * Jonathan Pearson
 * May 11, 2010
 *
 */

package com.sixnetio.test.ms.modbus;

import static org.junit.Assert.fail;

import java.io.*;
import java.net.SocketException;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.*;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sixnetio.Modbus.*;
import com.sixnetio.Station.ModbusLib;
import com.sixnetio.Station.TransportMethod;
import com.sixnetio.Switch.SwitchConfig;
import com.sixnetio.Switch.WebUI;
import com.sixnetio.fs.generic.File;
import com.sixnetio.io.INIParser;
import com.sixnetio.util.Utils;

/**
 * Tests the Modbus server in Managed Switches.
 *
 * @author Jonathan Pearson
 */
public class ModbusTest
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** TPL = TransPort Layer(s) */
	private static enum TPL
	{
		TCP("'tcp'"),
		UDP("'udp'"),
		TCP_UDP("'tcp+udp'"),
		;
		
		private final String value;
		private TPL(String value)
		{
			this.value = value;
		}
		
		@Override
		public String toString()
		{
			return value;
		}
	}
	
	private static String switchIP = "10.2.0.1";
	private static String user = "admin";
	private static String passwd = "admin";
	
	@BeforeClass
	public static void initialize()
	{
		Logger.getRootLogger().setLevel(Level.OFF);
		PropertyConfigurator.configureAndWatch("log4j.properties");
		
		// Read settings
		java.io.File f = new java.io.File("ModbusTest.conf");
		
		try {
			if ( ! f.isFile()) {
				// Create a skeleton file, but then fail out and require the
				// user to fill it in
				INIParser parser = new INIParser();
				parser.setValue("switch", "address", switchIP);
				parser.setValue("switch", "user", user);
				parser.setValue("switch", "password", passwd);
				
				OutputStream out = new FileOutputStream(f);
				try {
					parser.write(out);
				}
				finally {
					out.close();
				}
				
				fail("Configuration file did not exist. Update '" +
				     f.getAbsolutePath() + "' and re-run");
			}
			else {
				InputStream in = new FileInputStream(f);
				try {
					INIParser parser = new INIParser(in);
					switchIP = parser.getValue("switch", "address");
					user = parser.getValue("switch", "user");
					passwd = parser.getValue("switch", "password");
				}
				finally {
					in.close();
				}
				
				if (switchIP == null) {
					fail("Configuration file does not include the switch address. Update '" +
					     f.getAbsolutePath() + "' with [switch]address and re-run");
				}
				else if (user == null) {
					fail("Configuration file does not include the switch user. Update '" +
					     f.getAbsolutePath() + "' with [switch]user and re-run");
				}
				else if (passwd == null) {
					fail("Configuration file does not include the switch password. Update '" +
					     f.getAbsolutePath() + "' with [switch]password and re-run");
				}
			}
		}
		catch (IOException ioe) {
			fail("Error processing  configuration file '" +
			     f.getAbsolutePath() + "': " + ioe.getMessage());
		}
	}
	
	@Test
	public void tcpMessages()
	{
		setup(true, 1, TPL.TCP, 0, 4, 502);
		
		try {
			ModbusLink link = new ModbusLink(TransportMethod.TCP,
			                                 switchIP +
			                                 " " + ModbusLink.MODBUS_PORT);
			try {
				ModbusLib mbl = new ModbusLib(link, (byte)1);
				mbl.setTries(1); // No point in retrying over TCP
				testMessages(mbl);
			}
			finally {
				link.close();
			}
		}
		catch (IOException ioe) {
			logger.error("Unable to connect to the switch", ioe);
			fail("Unable to connect to the switch: " + ioe.getMessage());
		}
	}
	
	@Test
	public void udpMessages()
	{
		setup(true, 1, TPL.UDP, 0, 4, 502);
		
		try {
			ModbusLink link = new ModbusLink(TransportMethod.UDP,
			                                 switchIP +
			                                 " " + ModbusLink.MODBUS_PORT);
			try {
				ModbusLib mbl = new ModbusLib(link, (byte)1);
				testMessages(mbl);
			}
			finally {
				link.close();
			}
		}
		catch (IOException ioe) {
			logger.error("Unable to connect to the switch", ioe);
			fail("Unable to connect to the switch: " + ioe.getMessage());
		}
	}
	
	@Test
	public void tcpDifferentPort()
	{
		setup(true, 1, TPL.TCP, 0, 4, 5002);
		
		try {
			ModbusLink link = new ModbusLink(TransportMethod.TCP,
			                                 switchIP + " 5002");
			
			try {
				ModbusLib mbl = new ModbusLib(link, (byte)1);
				testMessages(mbl);
			}
			finally {
				link.close();
			}
		}
		catch (IOException ioe) {
			logger.error("Unable to connect to the switch", ioe);
			fail("Unable to connect to the switch: " + ioe.getMessage());
		}
	}
	
	@Test
	public void udpDifferentStation()
	{
		setup(true, 84, TPL.UDP, 0, 4, ModbusLink.MODBUS_PORT);
		
		try {
			ModbusLink link = new ModbusLink(TransportMethod.UDP,
			                                 switchIP +
			                                 " " + ModbusLink.MODBUS_PORT);
			
			try {
				ModbusLib mbl = new ModbusLib(link, (byte)84);
				testMessages(mbl);
				
				// Also, make sure it doesn't work as station 1
				mbl.setStation((byte)1);
				verifyBrokenLink(mbl);
			}
			finally {
				link.close();
			}
		}
		catch (IOException ioe) {
			logger.error("Unable to connect to the switch", ioe);
			fail("Unable to connect to the switch: " + ioe.getMessage());
		}
	}
	
	@Test
	public void tcpMaxconTimeout0()
	{
		ModbusLink link1, link2, link3;
		link1 = link2 = link3 = null;
		
		setup(true, 1, TPL.TCP, 0, 2, ModbusLink.MODBUS_PORT);
		
		try {
			link1 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl1 = new ModbusLib(link1, (byte)1);
			
			link2 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl2 = new ModbusLib(link2, (byte)1);
			
			// link1 is now less recently active than link2, so perform an
			// action on link1
			verifyLink(mbl1);
			
			// link2 is now the least recently active link
			// Verify that link2 still works
			verifyLink(mbl2);
			
			// link1 is now the least recently active link (again)
			// Create a new link and verify that the new one works and link1 got
			// disconnected
			link3 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl3 = new ModbusLib(link3, (byte)1);
			
			verifyLink(mbl3);
			verifyBrokenLink(mbl1);
		}
		catch (IOException ioe) {
			logger.error("Unable to connect to the switch", ioe);
			fail("Unable to connect to the switch: " + ioe.getMessage());
		}
		finally {
			if (link1 != null) {
				link1.close();
			}
			
			if (link2 != null) {
				link2.close();
			}
			
			if (link3 != null) {
				link3.close();
			}
		}
	}
	
	@Test
	public void tcpMaxconTimeout15()
	{
		ModbusLink link1, link2, link3;
		link1 = link2 = link3 = null;
		
		setup(true, 1, TPL.TCP, 15, 2, ModbusLink.MODBUS_PORT);
		
		try {
			link1 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl1 = new ModbusLib(link1, (byte)1);
			
			link2 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl2 = new ModbusLib(link2, (byte)1);
			
			// Make sure stuff isn't so slow that link1 might get dropped
			// instead of the new link
			long startTime = System.currentTimeMillis();
			
			// link1 is now less recently active than link2, so perform an
			// action on link1
			verifyLink(mbl1);
			
			// link2 is now the least recently active link
			// Verify that link2 still works
			verifyLink(mbl2);
			
			// link1 is now the least recently active link (again)
			// Create a new link before the timeout has expired, and verify that
			// the new one gets dropped and the others stay connected
			if (startTime + 14000 < System.currentTimeMillis()) {
				fail("Connection too slow, ran out of time");
			}
			
			link3 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl3 = new ModbusLib(link3, (byte)1);
			
			verifyBrokenLink(mbl3);
			verifyLink(mbl1);
			verifyLink(mbl2);
			
			link3.close();
			
			// Wait until the timeout has expired and try again
			Utils.sleep(16000);
			
			link3 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			mbl3 = new ModbusLib(link3, (byte)1);
			
			verifyLink(mbl3);
			verifyBrokenLink(mbl1);
			verifyLink(mbl2);
		}
		catch (IOException ioe) {
			logger.error("Unable to connect to the switch", ioe);
			fail("Unable to connect to the switch: " + ioe.getMessage());
		}
		finally {
			if (link1 != null) {
				link1.close();
			}
			
			if (link2 != null) {
				link2.close();
			}
			
			if (link3 != null) {
				link3.close();
			}
		}
	}
	
	@Test
	public void tcpMaxconNoTimeout()
	{
		ModbusLink link1, link2, link3;
		link1 = link2 = link3 = null;
		
		setup(true, 1, TPL.TCP, null, 2, ModbusLink.MODBUS_PORT);
		
		try {
			link1 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl1 = new ModbusLib(link1, (byte)1);
			
			link2 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl2 = new ModbusLib(link2, (byte)1);
			
			// link1 is now less recently active than link2, so perform an
			// action on link1
			verifyLink(mbl1);
			
			// link2 is now the least recently active link
			// Verify that link2 still works
			verifyLink(mbl2);
			
			// link1 is now the least recently active link (again)
			// Create a new link and verify that it gets dropped immediately
			link3 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl3 = new ModbusLib(link3, (byte)1);
			
			verifyBrokenLink(mbl3);
			verifyLink(mbl1);
			verifyLink(mbl2);
			
			// Sleep for 30 seconds (not that any reasonable amount of time
			// should matter, but at least it's another data point)
			Utils.sleep(30);
			
			// Try again (should have the same result)
			link3 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			mbl3 = new ModbusLib(link3, (byte)1);
			
			verifyBrokenLink(mbl3);
			verifyLink(mbl1);
			verifyLink(mbl2);
		}
		catch (IOException ioe) {
			logger.error("Unable to connect to the switch", ioe);
			fail("Unable to connect to the switch: " + ioe.getMessage());
		}
		finally {
			if (link1 != null) {
				link1.close();
			}
			
			if (link2 != null) {
				link2.close();
			}
			
			if (link3 != null) {
				link3.close();
			}
		}
	}
	
	@Test
	public void tcpUdpSameTime()
	{
		ModbusLink link1, link2;
		link1 = link2 = null;
		
		setup(true, 1, TPL.TCP_UDP, 0, 2, ModbusLink.MODBUS_PORT);
		
		try {
			link1 = new ModbusLink(TransportMethod.TCP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl1 = new ModbusLib(link1, (byte)1);
			
			link2 = new ModbusLink(TransportMethod.UDP,
			                       switchIP + " " + ModbusLink.MODBUS_PORT);
			ModbusLib mbl2 = new ModbusLib(link2, (byte)1);
			
			// Verify that we can talk over both without losing the connection
			verifyLink(mbl1);
			verifyLink(mbl2);
			verifyLink(mbl1);
		}
		catch (IOException ioe) {
			logger.error("Unable to connect to the switch", ioe);
			fail("Unable to connect to the switch: " + ioe.getMessage());
		}
		finally {
			if (link1 != null) {
				link1.close();
			}
			
			if (link2 != null) {
				link2.close();
			}
		}
	}
	
	private void verifyLink(ModbusLib mbl)
	{
		try {
			mbl.getDI((short)0, (short)1);
		}
		catch (IOException ioe) {
			logger.error("Unable to retrieve DI 10001", ioe);
			fail("Unable to retrieve DI 10001: " + ioe.getMessage());
		}
		catch (TimeoutException te) {
			fail("Timed out waiting for response");
		}
	}
	
	private void verifyBrokenLink(ModbusLib mbl)
	{
		try {
			mbl.getDI((short)10001, (short)1);
			fail("Able to retrieve DI 10001 from a (supposedly) broken link");
		}
		catch (SocketException se) {
			// This is expected (means the TCP connection is broken)
			logger.info("Unable to send message (expected error)", se);
		}
		catch (IOException ioe) {
			// This is bad
			logger.error("Received an error from a (supposedly) broken link",
			             ioe);
			fail("Supposedly broken link responded");
		}
		catch (TimeoutException te) {
			// This is expected (means the device won't respond)
			logger.info("Timed out receiving message (expected error)", te);
		}
	}
	
	private void testMessages(ModbusLib mbl)
	{
		try {
			// Test reading discrete inputs (should only succeed in the range
			// [0, 302])
			// Not interested in the returned values
			
			try {
				mbl.getDI((short)0, (short)303);
			}
			catch (IOException ioe) {
				fail("Unable to retrieve DIs in range [0, 303), should exist");
			}
			
			try {
				mbl.getDI((short)10000, (short)300);
				fail("Retrieved DIs in range [10000, 10300), should not exist");
			}
			catch (IOException ioe) {
				// This is expected
			}
			
			// Test reading discrete outputs (all should fail)
			try {
				mbl.getDO((short)100, (short)500);
				fail("Retrieved DOs in range [100, 600), should not exist");
			}
			catch (IOException ioe) {
				// This is expected
			}
			
			// Test reading analog inputs (all should fail)
			try {
				mbl.getAI((short)10, (short)100);
				fail("Retrieved AIs in range [10, 110), should not exist");
			}
			catch (IOException ioe) {
				// This is expected
			}
			
			// Test reading analog outputs (all should fail)
			try {
				mbl.getAO((short)500, (short)75);
				fail("Retrieved AOs in range [500, 575), should not exist");
			}
			catch (IOException ioe) {
				// This is expected
			}
			
			// Test writing discrete outputs (all should fail)
			try {
				mbl.putDO((short)25, true);
				fail("Wrote DO 25, should not exist");
			}
			catch (IOException ioe) {
				// This is expected
			}
			
			// Test writing analog outputs (all should fail)
			try {
				mbl.putAO((short)30, (short)500);
				fail("Wrote AO 30, should not exist");
			}
			catch (IOException ioe) {
				// This is expected
			}
			
			// Test writing multiple discrete outputs (should fail)
			try {
				mbl.putDO((short)18, new boolean[] { true, true, false, true });
				fail("Wrote DOs in range [18, 22), should not exist");
			}
			catch (IOException ioe) {
				// This is expected
			}
			
			// Test writing multiple analog outputs (should fail)
			try {
				mbl.putAO((short)84, new short[] { 0, 5, 10, 15, 20 });
				fail("Wrote AOs in range [84, 89), should not exist");
			}
			catch (IOException ioe) {
				// This is expected
			}
			
			// Try reading the exception byte (should succeed, but don't worry
			// about the value)
			try {
				byte exception = mbl.readException();
				logger.debug(String.format("Exception byte: 0x%02x", exception));
			}
			catch (IOException ioe) {
				fail(String.format("Unable to read the exception byte: " +
				                   ioe.getMessage()));
			}
			
			// Try reading diagnostics; the only one that should succeed is the
			// echo sub-function
			try {
				short[] test = new short[] { 1, 3, 5, 7, 9 };
				ModbusMessage response =
					mbl.send(new MB_Diagnostics(false,
					                            MB_Diagnostics.DIAG_ECHO,
					                            test));
				if ( ! (response instanceof MB_Diagnostics)) {
					fail("Response to MB_Diagnostics not an MB_Diagnostics: " +
					     response.getClass().getName());
				}
				
				short[] returned = ((MB_Diagnostics)response).getDataValues();
				if ( ! Arrays.equals(test, returned)) {
					fail("Response to MB_Diagnostics Echo did not match");
				}
			}
			catch (IOException ioe) {
				logger.error("MB_Diagnostics failed", ioe);
				fail("Unable to run MB_Diagnostics Echo: " + ioe.getMessage());
			}
			
			// Don't worry about the other diagnostics sub-functions, it would
			// not be a bad thing if some of them were implemented later
			
			// Don't worry about other message we probably don't support
			
			// Make up a message with an invalid function code
			try {
				ModbusMessage msg = new MB_DIRead((short)10001, (short)302).getGeneric();
				msg.setFunction((byte)0x74);
				msg.setup((short)500, mbl.getStation());
				
				ModbusMessage response = mbl.send(msg);
				
				if ((response.getFunction() & 0x80) != 0) {
					// The Modbus library is unlikely to detect this, since
					// there is no class associated with the message to report
					// that it is an error
					throw new IOException("Error response");
				}
				
				logger.error(String.format("Invalid Request:\n%s\nUnexpected Response:\n%s",
				                           msg.toString(),
				                           response.toString()));
				
				fail("Device responded successfully to made up message");
			}
			catch (IOException ioe) {
				// This is expected
			}
		}
		catch (TimeoutException te) {
			fail("Timed out while communicating with switch");
		}
	}
	
	private static int cfgCount = 0;
	
	private static synchronized void setup(boolean enabled, int station,
	                                       TPL tpl, Integer timeout, int maxcon,
	                                       int port)
	{
		cfgCount++;
		
		try {
			SwitchConfig cfg = WebUI.readSwitchConfig(switchIP, user, passwd);
			
			if (logger.isDebugEnabled()) {
				java.io.File cfgFile = new java.io.File("switchcfg" + cfgCount +
				                                        ".orig.tgz");
				OutputStream out = new FileOutputStream(cfgFile);
				try {
					out.write(cfg.getData());
					logger.debug("Switch configuration (before changes) saved to '" +
					             cfgFile.getAbsolutePath() + "'");
				}
				finally {
					out.close();
				}
			}
			
			File f = (File)cfg.getRoot().locate("etc/switch/modbus.config");
			if (f == null) {
				throw new IOException("No modbus.config found on the switch");
			}
			
			INIParser parser =
				new INIParser(new ByteArrayInputStream(f.getData()));
			
			final String sec = "modbus";
			parser.setValue(sec, "enabled", enabled ? "1" : "0");
			parser.setValue(sec, "stanum", "" + station);
			parser.setValue(sec, "transport", tpl.toString());
			parser.setValue(sec, "tcp_timeout",
			                timeout == null ? "none" : timeout.toString());
			parser.setValue(sec, "tcp_maxcon", "" + maxcon);
			parser.setValue(sec, "port", "" + port);
			
			f.setData(parser.getBytes());
			
			if (logger.isDebugEnabled()) {
				java.io.File cfgFile = new java.io.File("switchcfg" + cfgCount +
				                                        ".upd.tgz");
				OutputStream out = new FileOutputStream(cfgFile);
				try {
					out.write(cfg.getData());
					logger.debug("Switch configuration (after changes) saved to '" +
					             cfgFile.getAbsolutePath() + "'");
				}
				finally {
					out.close();
				}
			}
			
			WebUI.writeSwitchConfig(cfg, switchIP, user, passwd);
			
			// Give the switch a few seconds to reconfigure (the cycle time for
			// the server is 5 seconds, but it may be running under Valgrind,
			// which slows things down)
			Utils.sleep(10000);
		}
		catch (IOException ioe) {
			logger.error("Unable to update switch configuration", ioe);
			fail("Unable to update switch configuration: " + ioe.getMessage());
		}
	}
}

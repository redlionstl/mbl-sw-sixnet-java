/*
 * LogPlayer.java
 *
 * Plays a datalog file through UDR messages against the target address.
 *
 * Jonathan Pearson
 * April 14, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.*;
import org.jonp.xml.Tag;

import com.sixnetio.Automation.IOPoint;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.io.CSVParser;
import com.sixnetio.io.INIParser;
import com.sixnetio.util.Utils;

public class LogPlayer {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		String propsFile = Utils.getFirstExistingFile(new String[] {
			"log4j.properties",
			System.getProperty("user.home") + File.separator + ".log4j.properties"
		});
		
		if (propsFile != null) {
			PropertyConfigurator.configure(propsFile);
		} else {
			Logger.getRootLogger().setLevel(Level.OFF);
		}
	}
	
	public static class LogEntry {
		public final long timestamp;
		// Only written in the main() method, read-only beyond that
		public Map<String, String> values; // Tag --> Value
		
		public LogEntry(long timestamp) {
			this.timestamp = timestamp;
			
			this.values = new Hashtable<String, String>();
		}
	}
	
	public static final String PROG_VERSION = "0.1.0";
	
	private static void usage(PrintStream out) {
		out.println("LogPlayer v. " + PROG_VERSION + " (c) 2009 SIXNET");
		out.println("  Written by Jonathan Pearson (jonp@sixnetio.com)");
		out.println("Usage: LogPlayer {-s <serial port>|-t <IP address>|-u <IP address>}");
		out.println("                 -f <log file> [-d <time dilation>]");
		out.println("                 -p <project file> -n <station name> [-N <number>]");
		out.println("                 [-o] [-i] [-k]");
		out.println("       LogPlayer -p <project file> -n <station name> -x");
		out.println("  -s  Use serial communications through <serial port>");
		out.println("  -t  Use TCP communications through <IP address>");
		out.println("  -u  Use UDP communications through <IP address>");
		out.println("  <log file>  The datalog file to play back");
		out.println("  <time dilation>  Choose how long one second in the datalog file");
		out.println("                     should be worth in seconds during replay");
		out.println("                     Default is 1.0; smaller numbers replay faster");
		out.println("  <project file>  A .6pj file containing the tag definitions used by");
		out.println("                    the log data");
		out.println("  <station name>  The name of the station in the .6pj file to pull");
		out.println("                    configuration data out of");
		out.println("  <number>  The station number, or " + UDRMessage.STA_ANY + " for any station");
		out.println("              Defaults to the project-specified number");
		out.println("  -o  Play outputs. If neither -o nor -i is passed, only inputs");
		out.println("        will be played.");
		out.println("  -i  Play inputs. If neither -o nor -i is passed, only inputs");
		out.println("        will be played.");
		out.println("  -k  Keep replaying the log, until stopped by Ctrl+C or similar");
		out.println("  -x  Dump (to stdout) the mapping of tag names to I/O points and exit");
	}
	
	public static void main(String[] args) {
		// Required
		String address = null;
		TransportMethod addressType = null;
		File logFile = null;
		File projectFile = null;
		String stationName = null;
		
		// Optional
		Short stationNumber = null;
		double timeDilation = 1.0;
		
		boolean inputs = false;
		boolean outputs = false;
		boolean keepGoing = false;
		
		boolean dump = false;
		
		try {
    		for (int i = 0; i < args.length; i++) {
    			if (args[i].equals("-s")) {
    				if (addressType != null) throw new Exception("You may only specify one address");
    				
    				addressType = TransportMethod.Serial;
    				address = args[++i];
    			} else if (args[i].equals("-t")) {
    				if (addressType != null) throw new Exception("You may only specify one address");
    				
    				addressType = TransportMethod.TCP;
    				address = args[++i];
    			} else if (args[i].equals("-u")) {
    				if (addressType != null) throw new Exception("You may only specify one address");
    				
    				addressType = TransportMethod.UDP;
    				address = args[++i];
    			} else if (args[i].equals("-n")) {
    				if (stationName != null) throw new Exception("You may only specify one station name");
    				
    				stationName = args[++i];
    			} else if (args[i].equals("-N")) {
    				if (stationNumber != null) throw new Exception("You may only specify one station number");
    				
    				stationNumber = Short.parseShort(args[++i]);
    			} else if (args[i].equals("-f")) {
    				if (logFile != null) throw new Exception("You may only specify one log file");
    				
    				logFile = new File(args[++i]);
    				
    				if (!logFile.isFile()) throw new Exception("Specified log file is not a file");
    			} else if (args[i].equals("-p")) {
    				if (projectFile != null) throw new Exception("You may only specify one project file");
    				
    				projectFile = new File(args[++i]);
    				
    				if (!projectFile.isFile()) throw new Exception("Specified project file is not a file");
    			} else if (args[i].equals("-d")) {
    				timeDilation = Double.parseDouble(args[++i]);
    				
    				if (timeDilation <= 0) throw new Exception("Time dilation must be positive");
    			} else if (args[i].equals("-o")) {
    				outputs = true;
    			} else if (args[i].equals("-i")) {
    				inputs = true;
    			} else if (args[i].equals("-k")) {
    				keepGoing = true;
    			} else if (args[i].equals("-x")) {
    				dump = true;
    			} else if (args[i].equals("-h")) {
    				usage(System.out);
    				return;
    			} else {
    				throw new Exception("Unrecognized argument: " + args[i]);
    			}
    		}
    		
    		// Need to play something, default to inputs if not specified
    		if (!inputs && !outputs) {
    			inputs = true;
    		}
    		
    		if (dump) {
    			// Only need a project file and station name
    			if (projectFile == null) {
    				throw new Exception("No project file provided");
    			} else if (stationName == null) {
    				throw new Exception("No station name provided");
    			}
    		} else {
    			// Extra stuff to verify
        		if (address == null || addressType == null) {
        			throw new Exception("No address provided");
        		} else if (logFile == null) {
        			throw new Exception("No datalog file provided");
        		} else if (projectFile == null) {
        			throw new Exception("No project file provided");
        		} else if (stationName == null) {
        			throw new Exception("No station name provided");
        		}
    		}
		} catch (Exception e) {
			if (e instanceof ArrayIndexOutOfBoundsException) {
				System.err.println("Missing parameter to argument");
			} else {
				System.err.println(e.getMessage());
			}
			
			logger.fatal("Error parsing command-line arguments", e);
			
			usage(System.err);
			System.exit(1);
			return;
		}
		
		// Tag name --> IO point
		Map<String, IOPoint> ioPoints;
		
		// Load the project file
		try {
			LineNumberReader projectIn = new LineNumberReader(new FileReader(projectFile));
			
			try {
    			INIParser parser = new INIParser(projectIn);
        				
        		// Make sure that station exists and is an IPm
        		String section = String.format("IPm: %s", stationName);
        		if (!parser.sectionExists(section)) {
        			throw new Exception(String.format("IPm station '%s' does not exist in the project file", stationName));
        		} else {
        			// If necessary, load the station number from the project
        			if (stationNumber == null) {
        				try {
        					String value = parser.getValue(section, "StaNum");
        					if (value == null) throw new Exception(String.format("Station number for station '%s' is not specified in the project file", stationName));
        					
        					stationNumber = Short.parseShort(value);
        				} catch (NumberFormatException nfe) {
        					throw new Exception(String.format("Station number for station '%s' is not a valid value", stationName), nfe);
        				}
        			}
        		}
        		
        		// Load the tag --> IO point map from the project file
        		ioPoints = loadTagTranslation(parser, stationName);
			} finally {
				projectIn.close();
			}
		} catch (Exception e) {
			System.err.println("Unable to load project file: " + e.getMessage());
			logger.fatal("Unable to load project file", e);
			
			System.exit(1);
			return;
		}
		
		if (dump) {
			dumpIOMap(ioPoints);
			return;
		}
		
		// Connect to the station
		UDRLib udr;
		try {
	        MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();
	        UDRLink handler = new UDRLink(addressType, address);
	        dispatcher.registerHandler(handler);
	        
	        // Make sure we don't collide addresses
	        if (stationNumber == 0) {
	        	udr = new UDRLib((short)1, (byte)0);
	        } else {
	        	udr = new UDRLib((short)0, (byte)0);
	        }
        } catch (IOException ioe) {
	        System.err.println("Unable to set up connection: " + ioe.getMessage());
	        logger.fatal("Unable to set up connection to station", ioe);
	        
	        System.exit(1);
	        return;
        }
        
        // Load up the data log
        List<LogEntry> logEntries = new LinkedList<LogEntry>();
        try {
        	LineNumberReader in = new LineNumberReader(new FileReader(logFile));
        	
        	// Read and discard the first two lines
        	in.readLine();
        	in.readLine();
        	
        	// Read column headers from this line
        	CSVParser parser = new CSVParser(in);
        	parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
        	
        	// Read each line
        	while (parser.next()) {
        		parser.dumpWarnings(System.err);
        		
        		long timestamp;
        		String sTimestamp = parser.getColumn("SIXLOG Timestamp");
        		
        		if (sTimestamp != null) {
        			// Sixlog timestamp is in seconds since the epoch
        			timestamp = Long.parseLong(parser.getColumn("SIXLOG Timestamp")) * 1000; // It's in seconds
        		} else {
        			// Timestamp (from IOSimulator) is in milliseconds since the epoch
        			sTimestamp = parser.getColumn("Timestamp");
        			
        			if (sTimestamp == null) {
        				throw new IOException("There is no timestamp value for line " + parser.getLineNumber());
        			}
        			
        			timestamp = Long.parseLong(sTimestamp);
        		}
        		
        		LogEntry entry = new LogEntry(timestamp);
        		for (String colName : parser.getColumns()) {
        			entry.values.put(colName, parser.getColumn(colName));
        		}
        		
        		logEntries.add(entry);
        	}
        } catch (IOException ioe) {
        	System.err.println("Unable to load datalog: " + ioe.getMessage());
	        logger.fatal("Unable to load datalog", ioe);
	        
	        System.exit(1);
	        return;
        }
        
        do {
        	runSimulation(logEntries, ioPoints, stationNumber, udr, timeDilation, inputs, outputs);
        } while (keepGoing);
	}

	private static void dumpIOMap(Map<String, IOPoint> ioPoints) {
		Tag root = new Tag("iomap");
		
	    for (Map.Entry<String, IOPoint> entry : ioPoints.entrySet()) {
	    	root.addContent(entry.getValue().toXML());
	    }
	    
	    try {
	    	root.toStream(System.out);
	    } catch (IOException ioe) {
	    	throw new RuntimeException("Unexpected IOException writing to stdout", ioe);
	    }
    }

	private static void runSimulation(List<LogEntry> logEntries, Map<String, IOPoint> ioPoints, Short stationNumber, UDRLib udr,
            double timeDilation, boolean inputs, boolean outputs) {
	    
		long startTime = System.currentTimeMillis();
		long lastEntryTime = Long.MAX_VALUE; // Immediately send the first message
		
		for (LogEntry logEntry : logEntries) {
			// Compute time difference between the last and current log entries
			long logDiff = logEntry.timestamp - lastEntryTime;
			
			// Multiply by the time dilation value to see how long we should be waiting in the real world
			long waitFor = (long)(logDiff * timeDilation);
			
			// Wait for that long, then send the next message
			logger.info(String.format("Next log entry occurred at %s, sleeping for %dms",
			                           Utils.formatStandardDateTime(new Date(logEntry.timestamp), false),
			                           waitFor));
			Utils.sleep(waitFor);
			
			// Update lastEntryTime to be ready for the next update
			lastEntryTime = logEntry.timestamp;
			
			for (Map.Entry<String, String> entry : logEntry.values.entrySet()) {
				IOPoint ioPoint = ioPoints.get(entry.getKey());
				
				if (ioPoint == null) continue; // Unknown value; logged but not being replayed
				
				// Skipping inputs?
				if (!inputs && ioPoint.direction == IOPoint.Direction.Input) continue;
				
				// Skipping outputs?
				if (!outputs && ioPoint.direction == IOPoint.Direction.Output) continue;
				
				String s = entry.getValue().trim();
				if (ioPoint.lastKnownValue == null || !ioPoint.lastKnownValue.equals(s)) {
					logger.info(String.format("Setting %s to '%s'",
					                          ioPoint.toString(),
					                          s));
					
    				try {
        				switch (ioPoint.dataType) {
        					case Long: {
        						int val = Integer.parseInt(s);
        						ioPoint.set(val, stationNumber, udr);
        						break;
        					}
        					case Float: {
        						float val = Float.parseFloat(s);
        						ioPoint.set(val, stationNumber, udr);
        						break;
        					}
        					case Discrete: {
        						boolean val = (Integer.parseInt(s) != 0);
        						ioPoint.set(val, stationNumber, udr);
        						break;
        					}
        					case Analog: {
        						// This may be a temperature reading
        						short val;
        						if (s.indexOf("Deg") != -1) {
        							val = (short)(Float.parseFloat(s.substring(0, s.indexOf(' '))) * 10);
        						} else {
        							val = Short.parseShort(s);
        						}
        						ioPoint.set(val, stationNumber, udr);
        						break;
        					}
        				}
    				} catch (IOException ioe) {
    					System.err.println("I/O error communicating with station: " + ioe.getMessage());
    					logger.error("I/O error communicating with station", ioe);
    				} catch (TimeoutException te) {
    					System.err.println("Station did not respond to message: " + te.getMessage());
    					logger.error("Station did not respond to message", te);
    				} catch (NumberFormatException nfe) {
    					System.err.printf("Datalog value '%s' not parseable from field '%s', timestamp %d (%s)\n",
    					                  s,
    					                  ioPoint.tagName,
    					                  (logEntry.timestamp / 1000),
    					                  Utils.formatStandardDateTime(new Date((logEntry.timestamp)), false));
    					
    					logger.error("Datalog value not parseable", nfe);
    				}
    				
    				ioPoint.lastKnownValue = s;
				} else if (logger.isEnabledFor(Level.INFO)) {
					logger.debug(String.format("Leaving %s alone at '%s'",
					                          ioPoint.toString(),
					                          ioPoint.lastKnownValue));
				}
			}
		}
    }

	private static Map<String, IOPoint> loadTagTranslation(INIParser parser, String stationName) throws IOException {
		Map<String, IOPoint> ioPoints = new Hashtable<String, IOPoint>();
		
		// Grab the 'ModuleMap: <station name>' section to determine what modules to parse
		String mapSection = String.format("ModuleMap: %s", stationName);
		Set<String> moduleMap = parser.getKeys(mapSection);
		if (moduleMap == null) {
			throw new IOException(String.format("No module map exists for station '%s'", stationName));
		}
		
		// Name of the modules section, so we don't keep building it
		String modulesSection = String.format("Modules: %s", stationName);
		for (Iterator<String> itModules = moduleMap.iterator(); itModules.hasNext(); ) {
			String moduleNumber = itModules.next();
			
			// Grab the config data for this module from the Modules section
			String modCfg = parser.getValue(modulesSection, moduleNumber);
			if (modCfg == null) continue; // Guess it wasn't a module number
			
			// Check the module type
			String moduleType = parser.getValue(mapSection, moduleNumber);
			
			// Parse as CSV data
			String[] typeValues = Utils.tokenize(moduleType, ",", false);
			
			// Important values:
			//   [0] = Module type
			
			// Make sure that the module type is not virtual
			if (typeValues[0].startsWith("Virtual")) {
				// FUTURE: Support virtual modules
				continue;
			}
			
			// Parse module cfg as CSV data
			String[] modValues = Utils.tokenize(modCfg, ",", false);
			
			// Important values:
			//   [1] = First register number
			//   [3] = Number of registers
			
			int regStart = Integer.parseInt(modValues[1]);
			int regCount = Integer.parseInt(modValues[3]);
			
			// Now grab each of those registers
			for (int i = 0; i < regCount; i++) {
				String regCfg = parser.getValue(modulesSection, String.format("%s-%d", moduleNumber, i));
				if (regCfg == null) continue;
				
				// Parse as CSV data
				String[] regValues = Utils.tokenize(regCfg, ",", false);
				
				// Important values:
				//   [0] = Register type (like AI or DO)
				//   [1] = Tag name associated with this register
				
				// If no tag name, no point in continuing with this one
				if (regValues[1].length() == 0) continue;
				
				IOPoint.DataType type;
				IOPoint.Direction direction;
				
				if (regValues[0].equals("AI")) {
					type = IOPoint.DataType.Analog;
					direction = IOPoint.Direction.Input;
					
				} else if (regValues[0].equals("AO")) {
					type = IOPoint.DataType.Analog;
					direction = IOPoint.Direction.Output;
					
				} else if (regValues[0].equals("DI")) {
					type = IOPoint.DataType.Discrete;
					direction = IOPoint.Direction.Input;
					
				} else if (regValues[0].equals("DO")) {
					type = IOPoint.DataType.Discrete;
					direction = IOPoint.Direction.Output;
					
				} else {
					// FUTURE: Support all module types
					continue;
				}
				
				// Create an entry for this IOPoint
				// Make sure the register number is valid
				if (regStart + i > Short.MAX_VALUE) throw new IOException("Register number too high: " + (regStart + i));
				IOPoint ioPoint = new IOPoint(regValues[1], type, direction, (short)(regStart + i));
				
				if (ioPoints.containsKey(ioPoint.tagName)) throw new IOException("Duplicate tag name: " + ioPoint.tagName);
				ioPoints.put(ioPoint.tagName, ioPoint);
			}
		}
		
		return ioPoints;
    }
}

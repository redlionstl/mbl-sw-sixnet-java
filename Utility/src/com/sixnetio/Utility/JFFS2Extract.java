/*
 * JFFS2Extract.java
 *
 * Extracts a given file from a JFFS2 image and dumps it to stdout.
 *
 * Jonathan Pearson
 * September 5, 2008
 *
 */

package com.sixnetio.Utility;

import java.io.*;

import com.sixnetio.fs.generic.*;
import com.sixnetio.fs.generic.File;
import com.sixnetio.fs.jffs2.JFFS2;

public class JFFS2Extract {
	private static final int MAX_DEPTH = 5; // Number of links to follow before giving up
	
	public static void main(String[] args) {
		if (args.length != 2) {
			usage(System.err);
			System.exit(1);
			return;
		}
		
		InputStream in;
		
		if (args[0].equals("-")) {
			in = System.in;
		} else {
    		try {
    			in = new FileInputStream(args[0]);
    		} catch (IOException ioe) {
    			System.err.println("Unable to open the file: " + ioe.getMessage());
    			System.exit(1);
    			return;
    		}
		}
		
		JFFS2 image;
		
		try {
    		image = new JFFS2(in);
		} catch (IOException ioe) {
			System.err.println("Unable to parse the image: " + ioe.getMessage());
			System.exit(1);
			return;
		} finally {
			try {
				in.close();
			} catch (IOException ioe) { }
		}
		
		FSObject obj = image.getRoot().locate(args[1]);
		if (obj == null) {
			System.err.println("Unable to find file");
			System.exit(1);
		}
		
		// Loop so we can follow links
		for (int i = 0; i < MAX_DEPTH && obj != null; i++){
    		if (obj instanceof File) {
    			File file = (File)obj;
    			byte[] data = file.getData();
    			try {
    				System.out.write(data);
    				return;
    			} catch (IOException ioe) {
    				System.err.println("Unable to write to stdout: " + ioe.getMessage());
    				System.exit(1);
    			}
    		} else if (obj instanceof Symlink) {
    			Symlink link = (Symlink)obj;
    			System.err.println("File is a link, following to '" + link.getLinkName());
    			obj = link.getLinkTarget();
    		} else if (obj instanceof Device) {
    			System.err.println("Unable to extract file, it is a device");
    			System.exit(1);
    		} else if (obj instanceof Directory) {
    			System.err.println("Unable to extract file, it is a directory");
    			System.exit(1);
    		}
		}
		
		if (obj == null) {
			System.err.println("Broken link");
		}
		else {
			System.err.println("Link chain too deep, aborting");
		}
		
		System.exit(1);
	}
		
	private static void usage(PrintStream out) {
		out.println("JFFS2Extract - Extracts files from a JFFS2 filesystem image");
		out.println("  (c)2008 SIXNET, written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: JFFS2Extract <image> <path>");
		out.println("  <image> may be '-' for stdin");
	}
}

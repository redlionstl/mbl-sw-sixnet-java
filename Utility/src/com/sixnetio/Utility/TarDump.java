/*
 * TarDump.java
 *
 * Dumps a (possibly gzipped) tar file out to stdout.
 *
 * Jonathan Pearson
 * July 30, 2008
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;

import com.sixnetio.fs.generic.Directory;
import com.sixnetio.fs.generic.FSObject;
import com.sixnetio.fs.tar.GZippedTarball;
import com.sixnetio.fs.tar.Tarball;
import com.sixnetio.util.Utils;

public class TarDump
{
	private static boolean recurse = false;
	private static final int MAX_RECURSION = 4;
	
	private static void usage(PrintStream out)
	{
		out.println("TarDump Copyright 2010 Sixnet");
		out.println("  written by Jonathan Pearson (Jon.Pearson@sixnet.com)");
		out.println();
		out.println("Usage: TarDump [-r] [-z] <tarball> [-h]");
		out.println("  -r  Recurse into lower-level tarballs and dump them too");
		out.println("  -z  Force unzip, even if <tarball> does not end with 'gz'");
		out.println("  <tarball>  Specify the name of the tar file to dump, or '-' for stdin");
		out.println("  -h  Display this message and terminate");
		out.println();
		out.println("Note: A UID/GID of -1 indicates a directory that was referenced but not");
		out.println("  included in the tarball. The permissions and modification time will not be");
		out.println("  accurate.");
	}
	
	public static void main(String[] args) {
		String filename = null;
		boolean forceUnzip = false;
		
		try {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-r")) {
					recurse = true;
				}
				else if (args[i].equals("-z")) {
					forceUnzip = true;
				}
				else if (args[i].equals("-h")) {
					usage(System.out);
					return;
				}
				else {
					if (filename != null) {
						throw new Exception("Unrecognized option: " + args[i]);
					}
					
					filename = args[i];
				}
			}
			
			if (filename == null) {
				throw new Exception("No file specified");
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			usage(System.err);
			System.exit(1);
			return;
		}
		
		InputStream in;
		
		if (filename.equals("-")) {
			in = System.in;
		}
		else {
			try {
				in = new FileInputStream(filename);
			}
			catch (IOException ioe) {
				System.err.println("Unable to open '" + filename + "': " + ioe.getMessage());
				System.exit(1);
				return;
			}
		}
		
		Tarball tarball;
		
		try {
			if (forceUnzip || filename.toLowerCase().endsWith("gz")) {
				tarball = new GZippedTarball(in);
			}
			else {
				tarball = new Tarball(in);
			}
		}
		catch (IOException ioe) {
			System.err.println("Unable to untar '" + filename + "': " + ioe.getMessage());
			System.exit(1);
			return;
		}
		finally {
			try {
				in.close();
			}
			catch (IOException ioe) {
				// Ignore it
			}
		}
		
		dump(tarball, 0);
	}
	
	private static void dump(Tarball tarball, int recursion)
	{
		String indent = Utils.multiplyChar(' ', recursion * 2);
		
		if (recursion >= MAX_RECURSION) {
			System.out.println(String.format("%sMax recursion reached", indent));
			return;
		}
		
		// We could just use the standard FSObject.dump() method, but that would
		// not catch tarballs within this tarball
		dump(tarball.getRoot(), 0, recursion);
	}
	
	private static void dump(Directory dir, int level, int recursion)
	{
		FSObject[] files = dir.getChildren().toArray(new FSObject[dir.getChildCount()]);
		Arrays.sort(files, new Comparator<FSObject>() {
			@Override
			public int compare(FSObject o1, FSObject o2)
			{
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		for (int i = 0; i < files.length; i++) {
			FSObject obj = files[i]; // For convenience
			
			System.out.println(obj.getDumpLine(level));
			if (obj instanceof Directory) {
				dump((Directory)obj, level + 1, recursion);
			}
			
			if (recurse) {
				// Test if this file is a tarball
				if (obj instanceof com.sixnetio.fs.generic.File) {
					com.sixnetio.fs.generic.File file = (com.sixnetio.fs.generic.File)obj;
					
					Tarball subBall = null;
					String name = obj.getName().toLowerCase();
					if (name.endsWith(".tar")) {
						try {
							subBall = new Tarball(file.getData());
						}
						catch (IOException ioe) {
							System.out.println(String.format("%s  Not a valid tarball", Utils.multiplyChar(' ', level * 2)));
						}
					}
					else if (name.endsWith(".tar.gz") || name.endsWith(".tgz")) {
						try {
							subBall = new GZippedTarball(file.getData());
						}
						catch (IOException ioe) {
							System.out.println(String.format("%s  Not a valid gzipped tarball", Utils.multiplyChar(' ', level * 2)));
						}
					}
					
					if (subBall != null) {
						dump(subBall, recursion + 1);
					}
				}
			}
		}
	}
}

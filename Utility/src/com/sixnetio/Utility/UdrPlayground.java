/*
 * UdrPlayground.java
 *
 * A command-line utility for playing with raw UDR messages.
 *
 * Jonathan Pearson
 * December 5, 2008
 *
 */

package com.sixnetio.Utility;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Vector;

import com.sixnetio.Station.TransportMethod;
import com.sixnetio.Station.MessageDispatcher;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.util.Utils;

public class UdrPlayground extends Thread {
	public static final String PROG_VERSION = "0.1.0";
	
	private static class Connection {
		public final String address;
		public final TransportMethod method;
		public final short station;
		public final UDRLink handler;
		
		public Connection(String address, TransportMethod method, short station) throws IOException {
			this.address = address;
			this.method = method;
			this.station = station;
			
			handler = new UDRLink(method, address);
			MessageDispatcher.getDispatcher().registerHandler(handler);
		}
		
		public void close() throws IOException {
			MessageDispatcher.getDispatcher().unregisterHandler(handler);
			handler.close();
		}
	}
	
	private static void usage(PrintStream out) {
		out.println("Usage: UdrPlayground [{-s <serial port>|-t <IP address>|-u <IP address>}\n" +
		            "                     -n <station number>] [-h]");
		out.println("  -s  Use serial communications through <serial port>");
		out.println("  -t  Use TCP communications through <IP address>");
		out.println("  -u  Use UDP communications through <IP address>");
		out.println("  <station number> The number of the station; use 255 for any station");
		out.println("  -h  Display this message and quit");
	}
	
	public static void main(String[] args) {
		String address = null;
		TransportMethod method = null;
		short station = -1;
		
		try {
    		for (int i = 0; i < args.length; i++) {
    			if (args[i].equals("-s")) {
    				address = args[++i];
    				method = TransportMethod.Serial;
    			} else if (args[i].equals("-t")) {
    				address = args[++i];
    				method = TransportMethod.TCP;
    			} else if (args[i].equals("-u")) {
    				address = args[++i];
    				method = TransportMethod.UDP;
    			} else if (args[i].equals("-n")) {
    				station = Short.parseShort(args[++i]);
    			} else if (args[i].equals("-h")) {
    				usage(System.out);
    				return;
    			} else {
    				throw new Exception("Unknown argument: " + args[i]);
    			}
    		}
    		
    		if (address != null || method != null || station != -1) {
    			if (address == null || method == null || station == -1) {
        			throw new Exception("You must provide an address and a station number");
    			}
    		}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			usage(System.err);
			System.exit(1);
			return;
		}
		
		try {
    		UdrPlayground up = new UdrPlayground();
    		up.start();
    		
    		if (address != null) up.addConnection(address, method, station);
		} catch (IOException ioe) {
			Utils.debug(ioe);
			
			System.err.println("Unable to start the program: " + ioe.getMessage());
			System.exit(1);
		}
	}
	
	// This is a vector so we can easily close connections by their index
	private Vector<Connection> openConnections = new Vector<Connection>();
	
	public UdrPlayground() throws IOException {
		// Set up a message dispatcher, but that's about it
		MessageDispatcher.getDispatcher();
	}
	
	public void addConnection(String address, TransportMethod method, short station) throws IOException {
		openConnections.add(new Connection(address, method, station));
	}
	
	public void closeConnection(int index) throws IOException {
		Connection connection = openConnections.remove(index);
		connection.close();
	}
	
	public void closeConnection(Connection connection) throws IOException {
		openConnections.remove(connection);
		connection.close();
	}
	
	public Iterator<Connection> getConnections() {
		return Utils.readOnlyIterator(openConnections.iterator());
	}
	
	@Override
    public void run() {
		
	}
}

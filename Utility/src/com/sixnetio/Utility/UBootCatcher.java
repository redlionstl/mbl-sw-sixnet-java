/*
 * UBootCatcher.java
 *
 * Catch UBoot and exit.
 *
 * Jonathan Pearson
 * April 17, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.IOException;
import java.io.PrintStream;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.*;

import com.sixnetio.COM.ComPort;
import com.sixnetio.COM.UBootCommunicator;
import com.sixnetio.io.UnbufferedInputStream;
import com.sixnetio.util.Utils;

public class UBootCatcher {
	public static final String PROG_VERSION = "0.1.0";
	
	static {
		String log4jProperties = Utils.getFirstExistingFile("log4j.properties");
		
		if (log4jProperties != null) {
			PropertyConfigurator.configureAndWatch(log4jProperties, 5000);
		} else {
			Logger.getRootLogger().setLevel(Level.OFF);
		}
	}
	
	private static void usage(PrintStream out) {
		out.println("UBootCatcher v. " + PROG_VERSION + " (c) 2009 SIXNET");
		out.println("  Written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: UBootCatcher -s <serial> [-h]");
		
		out.println("-s <serial>  Specify the path to your serial device");
		out.println("-h           Output this message and exit");
	}
	
	public static void main(String[] args) throws IOException {
		String serialPath = null;
		
		try {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-s")) {
					serialPath = args[++i];
				} else if (args[i].equals("-h")) {
					usage(System.out);
					return;
				}
			}
			
			// Validate arguments
			if (serialPath == null) {
				throw new Exception("Path to serial device is required");
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			usage(System.err);
			System.exit(1);
			return;
		}
		
		UBootCommunicator uboot = new UBootCommunicator(serialPath, 9600, 8, ComPort.PARITY_NONE, ComPort.STOPBITS_1);
		
		UnbufferedInputStream in = new UnbufferedInputStream(System.in);
		
		System.out.println("Disconnect power from the device and press Enter");
		in.readLine();
		
		uboot.startCatch();
		
		System.out.println("Connect power to the device");
		try {
	        uboot.waitForCatch(60000);
        } catch (TimeoutException e) {
	        System.err.println("Unable to catch UBoot after 1 minute of trying");
	        System.exit(1);
	        return;
        }
        
        System.out.println("Caught UBoot on '" + serialPath + "', exiting");
        uboot.close();
	}
}

/*
 * IOSimulator.java
 *
 * Performs a logged, random simulation on a number of I/O
 * points using provided statistics.
 *
 * Jonathan Pearson
 * April 23, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.*;
import org.jonp.xml.Tag;

import com.sixnetio.Automation.IOPoint.Direction;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.Utility.DatalogStatistics.StatisticalIOPoint;
import com.sixnetio.util.Utils;

public class IOSimulator {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public static final String PROG_VERSION = "0.1.0";
	
	static {
		String propsFile = Utils.getFirstExistingFile(new String[] {
			"log4j.properties",
			System.getProperty("user.home") + File.separator + "iosimulator.log4j.properties"
		});
		
		if (propsFile != null) {
			PropertyConfigurator.configure(propsFile);
		} else {
			Logger.getRootLogger().setLevel(Level.OFF);
		}
	}
	
	private static void usage(PrintStream out) {
		out.println("IOSimulator v. " + PROG_VERSION + " (c) 2009 SIXNET");
		out.println("  Written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: IOSimulator -m <iomap> [-S <speed> [-d <deviation>]] [-c <cycle speed>]");
		out.println("       {-s <serial port>|-t <IP address>|-u <IP address}");
		out.println("       [-n <station>] [-h]");
		
		out.println("-m <iomap>        Specify the IOMap XML file (must include statistics)");
		out.println("-S <speed>        Specify speed of simulation. 1.0 will average one update");
		out.println("                    every second. Default is 0.1 (about one cycle every 100 ms)");
		out.println("-d <deviation>    Specify a deviation from the speed for each update (default");
		out.println("                    is 0.02 = 2%)");
		out.println("-c <cycle speed>  Specify the cycle speed in sine curves/second (default is");
		out.println("                    0.05, or 1 sine curve every 20 seconds)");
		out.println("-i                Simulate inputs (default if neither -o nor -i)");
		out.println("-o                Simulate outputs");
		out.println("-s <serial port>  Use serial communications through <serial port>");
		out.println("-t <IP address>   Use TCP communications through <IP address>");
		out.println("-u <IP address>   Use UDP communications through <IP address>");
		out.println("-n <station>      Specify the target station number; default is any station");
		out.println("-h                Output this message and exit");
		
		out.println("Log output goes to stdout");
	}
	
	public static void main(String[] args) {
		IOSimulator sim = new IOSimulator();
		
		try {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-m")) {
					sim.loadIOMap(args[++i]);
				} else if (args[i].equals("-S")) {
					try {
						sim.setUpdateSpeed(Double.parseDouble(args[++i]));
					} catch (NumberFormatException nfe) {
						throw new Exception("Update speed is not a number", nfe);
					}
				} else if (args[i].equals("-d")) {
					try {
						sim.setUpdateSpeedDeviation(Double.parseDouble(args[++i]));
					} catch (NumberFormatException nfe) {
						throw new Exception("Update speed deviation is not a number", nfe);
					}
				} else if (args[i].equals("-c")) {
					try {
						sim.setCycleSpeed(Double.parseDouble(args[++i]));
					} catch (NumberFormatException nfe) {
						throw new Exception("Cycle speed is not a number", nfe);
					}
				} else if (args[i].equals("-i")) {
					sim.setInputs(true);
				} else if (args[i].equals("-o")) {
					sim.setOutputs(true);
				} else if (args[i].equals("-s")) {
					sim.setMethod(TransportMethod.Serial);
					sim.setAddress(args[++i]);
				} else if (args[i].equals("-t")) {
					sim.setMethod(TransportMethod.TCP);
					sim.setAddress(args[++i]);
				} else if (args[i].equals("-u")) {
					sim.setMethod(TransportMethod.UDP);
					sim.setAddress(args[++i]);
				} else if (args[i].equals("-n")) {
					try {
						sim.setStationNumber(Short.parseShort(args[++i]));
					} catch (NumberFormatException nfe) {
						throw new Exception("Station is not a number", nfe);
					}
				} else if (args[i].equals("-h")) {
					usage(System.out);
					return;
				} else {
					throw new Exception("Unrecognized argument: " + args[i]);
				}
			}
			
			// Validation
			if (sim.getIOMap() == null) {
				throw new Exception("IOMap is necessary");
			} else if (sim.getMethod() == null || sim.getAddress() == null) {
				throw new Exception("Connection method/address is necessary");
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			usage(System.err);
			
			logger.error(e);
			
			System.exit(1);
			return;
		}
		
		// Optional params
		if (!sim.getInputs() && !sim.getOutputs()) sim.setInputs(true);
		
		try {
			sim.runSimulation();
		} catch (Exception e) {
			System.err.println("Simulation failed: " + e.getMessage());
			logger.fatal("Simulation failed", e);
			System.exit(1);
			return;
		}
	}
	
	// Helper functions for CurvingIOPoint
	private static boolean inRange(double val, double min, double max) {
    	return (min <= val && val <= max);
    }
	
	private class CurvingIOPoint extends StatisticalIOPoint {
		public int lastValue;
		
		private double theta;
		
		private boolean startup = true;
		
		public CurvingIOPoint(DataType dataType, Direction direction, short address) {
		    super(dataType, direction, address);
	    }
	    
	    public CurvingIOPoint(String tagName, DataType dataType, Direction direction, short address) {
		    super(tagName, dataType, direction, address);
	    }
	    
	    public CurvingIOPoint(Tag tag) {
		    super(tag);
		    
		    logger.debug("Tag:       " + tagName);
		    logger.debug("min:       " + min);
		    logger.debug("max:       " + max);
		    logger.debug("mean:      " + mean);
		    logger.debug("deviation: " + deviation);
	    }
	    
	    /**
	     * Compute the next expected value for this station.
	     * 
	     * @param timediff Time difference since the last computation, in milliseconds.
	     *        This is ignored on the first round.
	     * @return The new value in int format (may be a bitwise float, an analog, a
	     *         long int, or a discrete).
	     */
	    public int getNextValue(long timediff) {
	    	if (startup) {
	    		startup = false;
	    		
	    		theta = Utils.rand.nextDouble() * Math.PI * 2;
	    		
	    		logger.debug("Startup:");
	    		logger.debug("  theta: " + theta);
	    	}
	    	
	    	double seconds = timediff * 0.001;
	    	
	    	logger.debug("Time passed:   " + seconds + " seconds");
	    	
	    	double nextValue = Math.sin(theta) * deviation * 2;
	    	double g = Utils.rand.nextGaussian();
	    	
	    	// Probability that the value G was chosen
	    	// (would have been (g / standard deviation) ^ 2, but standard deviation is 1)
	    	double prob;
	    	if (Math.abs(g) <= 1) {
	    		// Function is meaningless below 1 standard deviation, so use a probability of 0
	    		prob = 0;
	    	} else {
	    		prob = 1 - 1 / (g * g);
	    	}
	    	
	    	logger.debug("  probability: " + prob);
	    	
	    	// Use that probability as a multiplier against the standard deviation for our
	    	//   system (make sure to randomize the sign)
	    	nextValue += deviation * prob * Math.signum(Utils.rand.nextDouble() - 0.5);
	    	
	    	// Recenter around the mean (currently centered at 0)
	    	nextValue += mean;
	    	
	    	theta += Math.PI * getCycleSpeed() * seconds;
	    	
	    	// Allow a skip-ahead of up to 1/4 of a cycle, but only once in a while
	    	if (Utils.rand.nextDouble() < 0.05) {
	    		double slideBy = Utils.rand.nextDouble() * Math.PI * 0.5;
	    		
	    		logger.debug("  Slide by " + slideBy);
	    		theta += slideBy;
	    	}
	    	
	    	if (theta > Math.PI * 2) theta -= Math.PI;
	    	
	    	logger.debug("  nextValue:   " + nextValue);
	    	
	    	lastValue = clamp((float)nextValue);
	    	
	    	logger.debug("  clamped:     " + getLastValue());
	    	
	    	logger.debug("");
	    	
	    	return lastValue;
	    }
	    
	    private int clamp(float value) {
	    	switch (dataType) {
				case Analog: {
					if (value < min) {
						logger.debug("  Value clamp");
						return (short)min;
					} else if (value > max) {
						logger.debug("  Value clamp");
						return (short)max;
					} else {
						return (short)value;
					}
				}
				case Discrete: {
					if (value < 0.5) {
						return 0;
					} else {
						return 1;
					}
				}
				case Float: {
					if (value < min) {
						logger.debug("  Value clamp");
						return Float.floatToRawIntBits((float)min);
					} else if (value > max) {
						logger.debug("  Value clamp");
						return Float.floatToRawIntBits((float)max);
					} else {
						return Float.floatToRawIntBits(value);
					}
				}
				case Long: {
					if (value < min) {
						logger.debug("  Value clamp");
						return (int)min;
					} else if (value > max) {
						logger.debug("  Value clamp");
						return (int)max;
					} else {
						return (int)value;
					}
				}
	    		default:
	    			throw new RuntimeException("Unhandled case: " + dataType.name());
	    	}
	    }
	    
	    public String getLastValue() {
	    	switch (dataType) {
				case Analog:
				case Discrete:
				case Long:
					return String.format("%d", lastValue);

				case Float:
					return String.format("%f", Float.intBitsToFloat(lastValue));
				
				default:
					throw new RuntimeException("Unhandled case: " + dataType.name());
	    	}
	    }
    }
	
	private List<CurvingIOPoint> iomap; // Read-only after construction
	private double updateSpeed = 0.1;
	private double updateSpeedDeviation = 0.02;
	private double cycleSpeed = 0.05;
	private boolean inputs = false;
	private boolean outputs = false;
	private TransportMethod method = null;
	private String address = null;
	private short stationNumber = UDRMessage.STA_ANY;
	
	public IOSimulator() { }
	
	public void loadIOMap(String path) throws IOException {
		Tag tag = Utils.xmlParser.parseXML(path, false, true);
		
		if (!tag.getName().equals("iomap")) throw new IOException("Not an IOMap: " + path);
		
		Tag[] iopointTags = tag.getEachTag("", "iopoint");
		iomap = new ArrayList<CurvingIOPoint>(iopointTags.length);
		
		for (Tag iopoint : iopointTags) {
			CurvingIOPoint point = new CurvingIOPoint(iopoint);
			
			iomap.add(point);
		}
	}
	
	public List<CurvingIOPoint> getIOMap() {
		return new ArrayList<CurvingIOPoint>(iomap);
	}
	
	public double getUpdateSpeed() {
    	return updateSpeed;
    }

	public void setUpdateSpeed(double speed) {
    	this.updateSpeed = speed;
    }

	public double getUpdateSpeedDeviation() {
    	return updateSpeedDeviation;
    }

	public void setUpdateSpeedDeviation(double speedDeviation) {
    	this.updateSpeedDeviation = speedDeviation;
    }

	public double getCycleSpeed() {
    	return cycleSpeed;
    }

	public void setCycleSpeed(double cycleSpeed) {
    	this.cycleSpeed = cycleSpeed;
    }

	public boolean getInputs() {
    	return inputs;
    }

	public void setInputs(boolean inputs) {
    	this.inputs = inputs;
    }

	public boolean getOutputs() {
    	return outputs;
    }

	public void setOutputs(boolean outputs) {
    	this.outputs = outputs;
    }

	public TransportMethod getMethod() {
    	return method;
    }

	public void setMethod(TransportMethod method) {
    	this.method = method;
    }

	public String getAddress() {
    	return address;
    }

	public void setAddress(String address) {
    	this.address = address;
    }

	public short getStationNumber() {
    	return stationNumber;
    }

	public void setStationNumber(short stationNumber) {
    	this.stationNumber = stationNumber;
    }
	
	public void runSimulation() throws IOException, TimeoutException {
		// Open a connection to the station
		UDRLib udr;
	    MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();
        UDRLink handler = new UDRLink(getMethod(), getAddress());
        dispatcher.registerHandler(handler);
        
        // Make sure we don't collide addresses
        if (getStationNumber() == 0) {
        	udr = new UDRLib((short)1, (byte)0);
        } else {
        	udr = new UDRLib((short)0, (byte)0);
        }
        
        // Remove IO points that we will not be dealing with this run
        for (Iterator<CurvingIOPoint> itIOMap = getIOMap().iterator(); itIOMap.hasNext(); ) {
        	CurvingIOPoint point = itIOMap.next();
        	
        	if (point.tagName == null) {
        		// No tag name? Don't play with it
        		itIOMap.remove();
        	} else if (!(getInputs() && point.direction == Direction.Input) &&
            	!(getOutputs() && point.direction == Direction.Output)) {
        		
        		// Not doing this type of point?
        		itIOMap.remove();
        	}
        }
        
        // Output a CSV header for the points
        printHeaders();
        
        // Update all points to have a proper value
        for (CurvingIOPoint point : getIOMap()) {
        	point.set(point.getNextValue(0), getStationNumber(), udr);
        }
        
        // Output the starting values
        printValues();
        
        // Start simulating changes
        long lastCycleEnded = System.currentTimeMillis();
    	while (true) {
    		// Sleep for <speed> seconds, +/- up to <speed deviation>
        	long sleepTime = (long)(getUpdateSpeed() * 1000);
        	long deviation = (long)(sleepTime * getUpdateSpeedDeviation());
        	
        	if (Utils.rand.nextBoolean()) deviation *= -1;
        	
        	sleepTime += (long)(Utils.rand.nextDouble() * deviation);
        	
        	Utils.sleep(sleepTime);
        	
        	// Tweak the I/O points
        	for (CurvingIOPoint point : getIOMap()) {
            	point.set(point.getNextValue(System.currentTimeMillis() - lastCycleEnded),
            	          getStationNumber(), udr);
        	}
        	
        	printValues();
        	
        	lastCycleEnded = System.currentTimeMillis();
        }
	}
	
	private void printHeaders() {
		// Station
		System.out.printf("\"%s\",\"%d\"\n", "Station:", getStationNumber());
		
		// Sixlog tag name ('raw', for now)
		System.out.printf("\"%s\",\"%s\"\n", "Sixlog Tag Name:", "raw");
		
		// Timestamp
		System.out.printf("Timestamp");
		
        for (CurvingIOPoint point : iomap) {
        	System.out.printf(",\"%s\"", point.tagName);
        }
        
        System.out.println();
	}
	
	private void printValues() {
		// Timestamp
		System.out.printf("\"%d\"", System.currentTimeMillis());
		
		for (CurvingIOPoint point : iomap) {
			System.out.printf(",\"%s\"", point.getLastValue());
		}
		
		System.out.println();
	}
}

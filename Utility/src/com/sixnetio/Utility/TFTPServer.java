/*
 * TFTPServer.java
 *
 * A front-end for the TFTP class. Provide a list of files, and it will share
 * them over TFTP for read-only access.
 * Example: java TFTPServer shared/*
 * Runs until killed (Ctrl+C)
 *
 * Jonathan Pearson
 * December 7, 2007
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.net.InetAddress;
import java.util.*;

import org.apache.log4j.*;

import com.sixnetio.server.TransferObserver;
import com.sixnetio.server.tftp.TFTP;
import com.sixnetio.util.Utils;

public class TFTPServer
	implements TransferObserver
{
	public static final String PROG_VERSION = "0.2.1";
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		String props = Utils.getFirstExistingFile(new String[] {
				"log4j.properties",
				System.getProperty("user.home") + File.separator + ".tftpserver.log4j.properties"
		});
		
		if (props == null) {
			Logger.getRootLogger().setLevel(Level.OFF);
		} else {
			PropertyConfigurator.configureAndWatch(props);
		}
	}
	
	private static void usage(PrintStream out) {
		out.println("TFTPServer v. " + PROG_VERSION + " (c) 2009 SIXNET");
		out.println("  Written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: TFTPServer [-p] [-h] [<file> [<file> [...]]]");
		out.println("-p      Display progress for each file being transferred");
		out.println("-h      Display this message and exit");
		out.println("<file>  Specify one or more files to share");
	}
	
	public static void main(String[] args) {
		TFTPServer server = new TFTPServer();
		
		int count = 0;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-p")) {
				server.setProgressIndicator(true);
			} else if (args[i].equals("-h")) {
				usage(System.out);
				return;
			} else {
                File f = new File(args[i]);
                if (!f.isFile()) {
                    System.err.println(args[i] + ": Is not a file, it will not be shared");
                } else if (server.fileExists(f.getName())) {
                    System.err.println(args[i] + ": Cannot share multiple files of the same name; only the first will be available");
                } else {
                    server.shareFile(f);
                    count++;
                }
			}
	    }
	    
		try {
	        server.start();
		} catch (IOException ioe) {
			System.err.println("Unable to start TFTP server: " + ioe.getMessage());
			logger.error("Unable to start TFTP server", ioe);
			System.exit(1);
			return;
        }
		
		logger.info("Sharing " + count + " files");
	    System.out.println("Sharing " + count + " files.");
	    System.out.println("Press Ctrl+C to exit.");
	    
	    // Now an infinite loop, but it can't be a busy one (waste of CPU cycles)
	    Object obj = new Object();
	    synchronized (obj) {
			while (true) {
				try {
					obj.wait();
				} catch (InterruptedException ie) { }
			}
		}
    }
	
	private TFTP tftp;
	private List<File> sharedFiles = new LinkedList<File>();
	private boolean progress;
	
	private Map<String, Map<InetAddress, Float>> activeTransfers =
		new LinkedHashMap<String, Map<InetAddress, Float>>();
	
	public TFTPServer() { }
	
	public void setProgressIndicator(boolean val) {
		this.progress = val;
	}
	
	public boolean fileExists(String name) {
		synchronized (sharedFiles) {
	        return sharedFiles.contains(name);
        }
	}
	
	public void shareFile(File f) {
		synchronized (sharedFiles) {
	        sharedFiles.add(f);
        }
		
		synchronized (activeTransfers) {
			activeTransfers.put(f.getName(),
			                    new LinkedHashMap<InetAddress, Float>());
		}
	}
	
	public void start() throws IOException {
		tftp = TFTP.getTFTP();
		
		synchronized (sharedFiles) {
    		for (File f : sharedFiles) {
    			tftp.addFile(f.getName(), f);
    			tftp.addObserver(f.getName(), this);
    		}
		}
	}

	@Override
    public boolean transferEnded(String path, InetAddress with,
                                 boolean success) {
		if (progress) {
			synchronized (activeTransfers) {
				activeTransfers.get(path).put(with, -1.0f);
				
				printProgress();
			}
		}
		
		return true;
    }

	@Override
    public void transferProgress(String path, InetAddress with,
                                 int block, int total) {
		
	    if (progress) {
	    	synchronized (activeTransfers) {
	    		activeTransfers.get(path).put(with,
	    		                              (float)block / total * 100.0f);
	    		
	    		printProgress();
	    	}
	    }
    }

	@Override
    public void transferStarted(String path, InetAddress with) {
	    if (progress) {
	    	synchronized (activeTransfers) {
	    		activeTransfers.get(path).put(with, 0.0f);
	    		
	    		printProgress();
	    	}
	    }
    }
	
	/**
	 * Prints a progress message for all active transfers. Set the value to -1
	 * to clear that transfer (otherwise it will be stuck on the screen at
	 * 100%).
	 */
	private void printProgress() {
		// Build a progress line
		StringBuilder builder = new StringBuilder();
		
		// Used to clear dead entries
		StringBuilder extraSpaces = new StringBuilder();
		
		synchronized (activeTransfers) {
			for (Map.Entry<String, Map<InetAddress, Float>> activeEntry :
			     activeTransfers.entrySet()) {
				
				for (Iterator<Map.Entry<InetAddress, Float>> itTransferEntries =
				     activeEntry.getValue().entrySet().iterator();
				     itTransferEntries.hasNext(); ) {
					
					Map.Entry<InetAddress, Float> transferEntry =
						itTransferEntries.next();
					
					if (transferEntry.getValue() == -1.0f) {
						// Getting deleted, calculate the number of spaces
						//   needed to clear the current message
						String tempValue = String
							.format("%s(%s): 100.0%%",
							        transferEntry.getKey().getHostAddress(),
							        activeEntry.getKey());
						
						// Add two for the ", "
						char[] spaces = new char[tempValue.length() + 2];
						Arrays.fill(spaces, ' ');
						
						extraSpaces.append(spaces);
						
						// Remove it so we aren't continuously calculating
						//   spaces even though it's gone
						itTransferEntries.remove();
					} else {
						// Actually make a message for this one
						if (builder.length() > 0) {
							builder.append(", ");
						}
						
    					builder.append(String
    						.format("%s(%s): %.1f%%",
    						        transferEntry.getKey().getHostAddress(),
    						        activeEntry.getKey(),
    						        transferEntry.getValue()));
					}
				}
			}
			
			System.out.printf("\r%s%s",
			                  builder.toString(),
			                  extraSpaces.toString());
		}
	}
}

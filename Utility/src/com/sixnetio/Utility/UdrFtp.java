/*
 * UdrFtp.java
 *
 * A UDR-based file transfer utility. This may be used as a command-line interpreter by
 * calling start() or using the main() function, but it can also be used as a library
 * by calling the public functions.
 *
 * Jonathan Pearson
 * November 19, 2008
 *
 */

package com.sixnetio.Utility;

import java.awt.Toolkit;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeoutException;

import com.sixnetio.GUI.ProgressListener;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.UDR.Filesys.FS_Dir;
import com.sixnetio.UDR.Filesys.FS_Dir.DirEntry;
import com.sixnetio.util.Utils;

public class UdrFtp extends Thread {
	public static final String PROG_VERSION = "0.1.1";
	
	private static void usage(PrintStream out) {
		out.println("Usage: UdrFtp [{-s <serial port>|-t <IP address>|-u <IP address>} -n <station number>]");
		out.println("  -s  Use serial communications through <serial port>");
		out.println("  -t  Use TCP communications through <IP address>");
		out.println("  -u  Use UDP communications through <IP address>");
		out.println("  <station number> The number of the station; use 255 for any station");
	}
	
	public static void main(String[] args) {
		String address = null;
		TransportMethod method = null;
		short station = -1;
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-s")) {
				address = args[++i];
				method = TransportMethod.Serial;
			} else if (args[i].equals("-t")) {
				address = args[++i];
				method = TransportMethod.TCP;
			} else if (args[i].equals("-u")) {
				address = args[++i];
				method = TransportMethod.UDP;
			} else if (args[i].equals("-n")) {
				station = Short.parseShort(args[++i]);
			} else {
				System.err.println("Unknown argument: " + args[i]);
				usage(System.err);
				System.exit(1);
			}
		}
		
		if (method != null && station == -1) {
			System.err.println("You must provide a station number with the address");
			usage(System.err);
			System.exit(1);
		}
		
		try {
    		UdrFtp ftp = new UdrFtp(address, method, station);
    		ftp.start();
		} catch (IOException ioe) {
			Utils.debug(ioe);
			
			System.err.println("Unable to connect to the specified device: " + ioe.getMessage());
			System.exit(1);
		}
	}
	
	private UDRLink activeHandler;
	private UDRLib udr;
	private short station;
	private Thread commandThread;
	
	/**
	 * Construct a new, unconnected UdrFtp object.
	 * @throws IOException Never, this is an implementation detail.
	 */
	public UdrFtp() throws IOException {
		this(null, null, (short)-1);
	}
	
	/**
	 * Construct a new UdrFtp object, connecting to the specified address.
	 * @param address The address to connect to; may be a path for a serial port, an IP address,
	 *   or a host name.
	 * @param method The method to use for connecting, see Method_* above. If this is Method_Unknown,
	 *   no connection is made and 'address' and 'station' are ignored.
	 * @param station The station number of the target device. The local machine will use the Any Station
	 *   number.
	 * @throws IOException If the connection fails for some reason.
	 */
	public UdrFtp(String address, TransportMethod method, short station) throws IOException {
		this.station = station;
		
		// Make sure the message dispatcher is running
		MessageDispatcher.getDispatcher();
		
		if (method != null) {
			open(method, address, station);
		}
	}
	
	private boolean running;
	
	@Override
    public void run() {
		// Start up a command-interpreter loop
		BufferedReader lineReader = new BufferedReader(new InputStreamReader(System.in));
		running = true;
		commandThread = Thread.currentThread();
		
		while (running) {
			// Get a command
			if (activeHandler == null) {
				System.out.print("UDR> ");
			} else {
				System.out.printf("%s %d UDR> ", activeHandler.getDescription(), station);
			}
			
			System.out.flush();
			
			String command;
			
			try {
				command = lineReader.readLine().trim();
			} catch (IOException ioe) {
				// Let the thread's exception handler deal with it
				throw new RuntimeException("Unable to read a line from System.in", ioe);
			}
			
			if (activeHandler != null && activeHandler.isClosed()) {
				close();
				System.err.println("Connection lost");
			}
			
			if (command.length() == 0) continue; // No actual command
			
			// Parse the command
			StringTokenizer tok = new StringTokenizer(command);
			
			if (tok.countTokens() == 0) continue; // No actual command
			
			String cmdName = tok.nextToken();
			
			// Which command?
			if (cmdName.equals("append")) {
				append(tok);
			} else if (cmdName.equals("ascii")) {
				ascii(tok);
			} else if (cmdName.equals("bell")) {
				bell(tok);
			} else if (cmdName.equals("binary")) {
				binary(tok);
			} else if (cmdName.equals("bye")) {
				bye(tok);
			} else if (cmdName.equals("cd")) {
				cd(tok);
			} else if (cmdName.equals("close")) {
				close(tok);
			} else if (cmdName.equals("delete")) {
				delete(tok);
			} else if (cmdName.equals("get")) {
				get(tok);
			} else if (cmdName.equals("hash")) {
				hash(tok);
			} else if (cmdName.equals("help")) {
				help(tok);
			} else if (cmdName.equals("lcd")) {
				lcd(tok);
			} else if (cmdName.equals("lls")) {
				lls(tok);
			} else if (cmdName.equals("lpwd")) {
				lpwd(tok);
			} else if (cmdName.equals("ls")) {
				ls(tok);
			} else if (cmdName.equals("mkdir")) {
				mkdir(tok);
			} else if (cmdName.equals("modtime")) {
				modtime(tok);
			} else if (cmdName.equals("nlist")) {
				nlist(tok);
			} else if (cmdName.equals("open")) {
				open(tok);
			} else if (cmdName.equals("put")) {
				put(tok);
			} else if (cmdName.equals("pwd")) {
				pwd(tok);
			} else if (cmdName.equals("reget")) {
				reget(tok);
			} else if (cmdName.equals("rename")) {
				rename(tok);
			} else if (cmdName.equals("restart")) {
				restart(tok);
			} else if (cmdName.equals("rmdir")) {
				rmdir(tok);
			} else if (cmdName.equals("site")) {
				site(tok);
			} else if (cmdName.equals("size")) {
				size(tok);
			} else if (cmdName.equals("status")) {
				status(tok);
			} else if (cmdName.equals("tick")) {
				tick(tok);
			} else if (cmdName.equals("timeout")) {
				timeout(tok);
			} else {
				System.err.println("Unknown command: " + cmdName);
			}
		}
	}
	
	// Data used for transfers
	
	// Timeout used for site commands, in milliseconds
	private long siteTimeout = 30000;
	
	// If true, continually updates percent transferred an a line
	// Mutually exclusive with hashPrinting
	private boolean tickPrinting = false;
	
	// Next transfer will start reading/writing at this offset on both sides
	// If non-zero, the file must exist in both locations for the transfer to run
	private int restartOffset = 0;
	
	// The current working directory on the local machine
	private File localCWD = new File(".");
	
	// The current working directory on the remote device
	private String remoteCWD = "/";
	
	// Binary vs. ascii mode
	// If false, any files leaving the local machine will have LF line endings even if they were CRLF locally,
	//   and any files arriving on the local machine will have CRLF line endings ever if they were LF remotely
	private boolean binaryMode = true;
	
	// If true, prints a # every 1% transferred
	// Mutually exclusive with tickPrinting
	private boolean hashPrinting = false;
	
	// If true, a bell will sound after each transfer completes
	private boolean bellOn = false;
	
	private void timeout(StringTokenizer tok) {
	    if (tok.countTokens() > 1) {
	    	System.err.println("Usage: timeout [<timeout>]");
	    } else {
	    	if (tok.countTokens() == 1) {
	    		long to = Long.parseLong(tok.nextToken());
    	    	
    	    	timeout(to);
	    	} else {
	    		long to = timeout();
	    		System.out.printf("Current timeout for 'site' commands is %,d ms\n", to);
	    	}
	    }
    }
	
	/**
	 * Set the timeout for 'site' commands.
	 * @param to The new timeout in milliseconds.
	 */
	public void timeout(long to) {
		siteTimeout = to;
	}
	
	/**
	 * Get the timeout for 'site' commands, in milliseconds.
	 */
	public long timeout() {
		return siteTimeout;
	}
	
	// Set the tickPrinting value
	private void tick(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
			System.err.println("Usage: tick");
		} else {
			boolean on = !tick();
			tick(on);
			
			System.out.printf("Tick printing is %s\n", on ? "on" : "off");
		}
	}
	
	/**
	 * Turn on/off tick printing.
	 * @param on If true, the percent transferred will be kept updated on a line. If true, hash
	 *   printing will be turned off.
	 */
	public void tick(boolean on) {
		tickPrinting = on;
		
		if (tickPrinting) hash(false);
	}
	
	/**
	 * Get the current tick setting.
	 */
	public boolean tick() {
		return tickPrinting;
	}
	
	private void status(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
			System.err.println("Usage: status");
		} else {
			System.out.println(status());
		}
	}
	
	/**
	 * Returns a line-by-line description of the current status of the program.
	 */
	public String status() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("UdrFtp version " + PROG_VERSION + ", by Jonathan Pearson");
		builder.append("\n");
		
		if (activeHandler != null) {
			builder.append("Connected via " + activeHandler.getDescription() + " to station #" + station);
		} else {
			builder.append("Disconnected");
		}
		builder.append("\n");
		
		builder.append(binaryMode ? "Binary mode" : "Ascii mode");
		builder.append("\n");
		
		builder.append(String.format("Completion bell is %s", bellOn ? "on" : "off"));
		builder.append("\n");

		builder.append(String.format("Hash printing is %s", hashPrinting ? "on" : "off"));
		builder.append("\n");
		
		builder.append(String.format("Tick printing is %s", tickPrinting ? "on" : "off"));
		builder.append("\n");
		
		builder.append("Local CWD: " + localCWD.getAbsolutePath());
		builder.append("\n");
		
		builder.append("Remote CWD: " + remoteCWD);
		builder.append("\n");
		
		builder.append("Next transfer will start at offset " + restartOffset);
		builder.append("\n");
		
		return builder.toString();
	}
	
	private void size(StringTokenizer tok) {
		if (tok.countTokens() != 1) {
			System.err.println("Usage: size <remote file>");
		} else {
			String fileName = tok.nextToken();
			
			try {
    			int fileSize = size(fileName);
    			System.out.printf("'%s' is %,d bytes long\n", fileName, fileSize);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Get the size of the given remote file.
	 * @param remotePath The name of the file.
	 * @return The size of the file, in bytes.
	 * @throws IOException If there is a problem communicating with the remote
	 *   device or the file does not exist.
	 * @throws TimeoutException If the remote device does not respond.
	 */
	public int size(String remotePath) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		remotePath = makeRemoteAbsolute(remotePath);
		
		int[] out_size = new int[1];
		udr.statFile(station, remotePath, out_size, null, null, null);
		
		return out_size[0];
	}
	
	private void site(StringTokenizer tok) {
		if (tok.countTokens() < 1) {
			System.err.println("Usage: site <remote shell command>");
		} else {
			StringBuilder remoteCommand = new StringBuilder();
			while (tok.hasMoreTokens()) {
				remoteCommand.append(tok.nextToken());
				remoteCommand.append(" "); // A space at the end won't hurt
			}
			
			try {
    			String response = site(remoteCommand.toString());
    			
    			if (response.length() > 0) {
        			System.out.println("Response *****");
        			System.out.println(response);
        			System.out.println("***** End of response");
    			} else {
    				System.out.println("Command finished with no output");
    			}
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Execute a shell command on the remote device in the remote CWD.
	 * @param command The command to execute. It will be prefixed by "cd &lt;cwd&gt;; ".
	 * @return The response from the remote device.
	 * @throws IOException If there is a problem executing the command.
	 * @throws TimeoutException If the remote device does not respond.
	 */
	public String site(String command) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		// Make sure we're in the right directory for it
		command = "cd " + remoteCWD + "; " + command;
		
		return udr.executeCommand(station, command, siteTimeout);
	}
	
	private void rmdir(StringTokenizer tok) {
		if (tok.countTokens() != 1) {
			System.err.println("Usage: rmdir <directory name>");
		} else {
			String dirName = tok.nextToken();
			
			try {
				rmdir(dirName);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Delete a remote directory.
	 * @param remotePath The name of the remote directory to delete.
	 * @throws IOException If there is a problem deleting the directory.
	 * @throws TimeoutException If the remote device does not respond.
	 */
	public void rmdir(String remotePath) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		remotePath = makeRemoteAbsolute(remotePath);
		
		// Make sure it's a directory name
		if (!remotePath.endsWith("/")) {
			remotePath += "/";
		}
		
		udr.deleteFile(station, remotePath);
	}
	
	private void restart(StringTokenizer tok) {
		if (tok.countTokens() > 1) {
			System.err.println("Usage: restart [<byte offset>]");
		} else {
			try {
    			if (tok.countTokens() == 1) {
    				int offset = Integer.parseInt(tok.nextToken());
    				restart(offset);
    			} else {
    				int offset = restart();
    				System.out.printf("The next transfer will start %,d bytes from the beginning of the file\n", offset);
    			}
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
    }
	
	/**
	 * Set the offset where the next transfer will begin.
	 * @param offset The offset within the file to start the next transfer. If this is non-zero,
	 *   the file must exist on both ends or the transfer will fail.
	 * @throws IOException If not connected.
	 */
	public void restart(int offset) throws IOException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		restartOffset = offset;
	}
	
	/**
	 * Get the current restart offset, in bytes from the beginning of the file.
	 * @throws IOException If not connected.
	 */
	public int restart() throws IOException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		return restartOffset;
	}
	
	private void rename(StringTokenizer tok) {
	    if (tok.countTokens() != 2) {
	    	System.err.println("Usage: rename <old name> <new name>");
	    } else {
	    	String oldName = tok.nextToken();
	    	String newName = tok.nextToken();
	    	
	    	try {
	    		rename(oldName, newName);
	    	} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
	    }
    }
	
	/**
	 * Rename a remote file.
	 * @param oldRemotePath The current name of the file.
	 * @param newRemotePath The new name that the file should take.
	 * @throws IOException If there is a problem communicating with the remote device.
	 * @throws TimeoutException If the remote device does not respond.
	 */
	public void rename(String oldRemotePath, String newRemotePath) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		oldRemotePath = makeRemoteAbsolute(oldRemotePath);
		newRemotePath = makeRemoteAbsolute(newRemotePath);
		
		udr.renameFile(station, oldRemotePath, newRemotePath);
	}
	
	private void reget(StringTokenizer tok) {
		if (tok.countTokens() < 1 || tok.countTokens() > 2) {
			System.err.println("Usage: reget <remote file> [<local file>]");
		} else {
			String remotePath = tok.nextToken();
			String localPath = remotePath;
			
			if (tok.countTokens() == 1) {
				localPath = tok.nextToken();
			}
			
			try {
				reget(remotePath, localPath);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
    }
	
	/**
	 * Resume transfer of a file starting at the end of what has already been retrieved.
	 * @param remotePath The name of the file on the remote side to continue transferring.
	 *   Must exist.
	 * @param localPath The name of local file to continue transferring. Must exist.
	 * @throws IOException If there is a problem communicating with the remote device.
	 * @throws TimeoutException If the remote device does not respond.
	 */
	public void reget(String remotePath, String localPath) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		File localFile;
		
		localFile = new File(makeLocalAbsolute(localPath));
		
		if (localFile.isFile()) {
			restart((int)localFile.length());
			get(remotePath, localPath);
		} else {
			throw new IOException("Cannot reget a non-existant file");
		}
	}
	
	private void pwd(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
			System.err.println("Usage: pwd");
		} else {
			try {
    			String remotePwd = pwd();
    			System.out.println("Remote working directory is '" + remotePwd + "'");
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Get the present working directory on the remote device.
	 * @throws IOException If not connected.
	 */
	public String pwd() throws IOException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		return remoteCWD;
	}
	
	private void put(StringTokenizer tok) {
		if (tok.countTokens() < 1 || tok.countTokens() > 2) {
			System.err.println("Usage: put <local file> [<remote file>]");
		} else {
			String localPath = tok.nextToken();
			String remotePath = localPath;
			
			if (tok.countTokens() == 1) {
				remotePath = tok.nextToken();
			}
			
			try {
				float rate = put(localPath, remotePath);
				System.out.printf("Transferred '%s' at %.3f KB/s\n", localPath, rate);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Transfer a file to the remote device.
	 * @param localPath The path to the file on the local machine.
	 * @param remotePath The path to the file on the remote device.
	 * @return The speed of the transfer in kilobytes per second.
	 * @throws IOException If there is a problem communicating with the device.
	 * @throws TimeoutException If the device does not respond.
	 */
	public float put(String localPath, String remotePath) throws IOException, TimeoutException {
		return put(localPath, remotePath, restartOffset, restartOffset);
	}
	
	/**
	 * Transfer a file to the remote device, starting at the specified offsets in the local and
	 *   remote files.
	 * @param localPath The path to the file on the local machine.
	 * @param remotePath The path to the file on the remote device.
	 * @param localStart The offset within the local file to start reading.
	 * @param remoteStart The offset within the remote file to start writing. If this is non-zero,
	 *   the remote file MUST exist.
	 * @return The speed of the transfer in kilobytes per second.
	 * @throws IOException If there is a problem communicating with the device.
	 * @throws TimeoutException If the device does not respond.
	 */
	public float put(String localPath, String remotePath, int localStart, int remoteStart) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		localPath = makeLocalAbsolute(localPath);
		remotePath = makeRemoteAbsolute(remotePath);
		
		final File localFile = new File(localPath);
		
		if (!localFile.isFile()) throw new IOException("Local file must exist and be a file");
		
		// Create/open the remote file
		int[] alias = new int[1];
		try {
			alias[0] = udr.getAlias(station, remotePath);
		} catch (IOException ioe) {
			Utils.debug(ioe);
			
			if (remoteStart > 0) {
				throw new IOException("Unable to open file on remote device for restarting: " + ioe.getMessage());
			}
			
			// This may take a while
			long oldTimeout = udr.getMessageTimeout();
			udr.setMessageTimeout(15000);
			
			alias[0] = udr.createFile(station, remotePath, (int)localFile.length());
			
			udr.setMessageTimeout(oldTimeout);
		}
		
		ProgressListener pl = null;
		
		if (tickPrinting) {
			pl = new ProgressListener() {
				public void updateProgress(float percent) {
					System.out.printf("\r%.1f%%", percent * 100.0f);
					System.out.flush();
                }
			};
		} else if (hashPrinting) {
			pl = new ProgressListener() {
				private float lastPercent = 0.0f;
				
				public void updateProgress(float percent) {
					int diff = (int)(percent * 100) - (int)(lastPercent * 100);
					
					if (diff > 0) {
    					for (int i = 0; i < diff; i++) {
    						System.out.print("#");
    					}
    					System.out.flush();
    					
    					// Leave any difference in there so we will be sure to hit all of the # marks
    					lastPercent = percent - (percent - lastPercent - (diff / 100.0f));
					}
                }
			};
		}
		
		long startTime = System.nanoTime();
		int bytesTransferred = 0;
		
		// Read large blocks of the local file, translate as necessary, and send to the remote device
		RandomAccessFile fileIn = new RandomAccessFile(localFile, "r");
		
		try {
			// Jump to the right part of the local file
			fileIn.seek(localStart);
			
    		byte[] block = new byte[8192];
    		int bytesRead;
    		int fileOffset = remoteStart;
    		
    		while ((bytesRead = fileIn.read(block)) != -1) {
    			if (!binaryMode) {
    				// Search out all occurrences of "\r\n" and turn them into "\n"
    				int oldOffset = 0;
    				int newOffset = 0;
    				byte[] newBlock = new byte[8192];
    				
    				while (oldOffset < block.length - 1) {
    					if (block[oldOffset] == (byte)'\r' &&
    						block[oldOffset + 1] == (byte)'\n') {
    						
    						oldOffset++; // Skip the '\r'
    					}
    					newBlock[newOffset++] = block[oldOffset++];
    				}
    				
    				bytesRead = newOffset; // Shorten to the new number of bytes
    				block = newBlock;
    			}
    			
    			// We could pass the progress listener directly into the function, but then we'd
    			//   need to do all sorts of weird math, since the function figures out percent by
    			//   the size of the block passed in, and we are handling chunking on this side
    			udr.writeFile(station, remotePath, block, 0, bytesRead, fileOffset, alias);
    			fileOffset += bytesRead;
    			bytesTransferred += bytesRead;
    			
    			if (pl != null) {
    				pl.updateProgress(bytesTransferred / ((float)localFile.length() - (float)localStart));
    			}
    		}
    		
    		if (tickPrinting) {
    			System.out.print("\r100.0%");
    		}
    		
    		if (pl != null) System.out.println();
		} finally {
			fileIn.close();
		}
		
		long stopTime = System.nanoTime();
		
		// Convert fileSize to kilobytes and elapsed time to seconds, then divide
		return ((bytesTransferred / 1024.0f) / ((stopTime - startTime) / 1000000000.0f));
	}
	
	private void open(StringTokenizer tok) {
		if (tok.countTokens() != 3) {
			System.err.println("Usage: open {s <serial port>|t <IP address>|u <IP address>} <station number>");
		} else {
			String methodName = tok.nextToken();
			String address = tok.nextToken();
			short number = Short.parseShort(tok.nextToken());
			
			TransportMethod method = null;
			
			if (methodName.equals("s")) {
				method = TransportMethod.Serial;
			} else if (methodName.equals("t")) {
				method = TransportMethod.TCP;
			} else if (methodName.equals("u")) {
				method = TransportMethod.UDP;
			} else {
				System.err.println("Usage: open {s <serial port>|t <IP address>|u <IP address>} <station number>");
				return;
			}
			
			try {
				open(method, address, number);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Open a new connection to a remote device.
	 * @param method The connection method, see Method_* above. Don't pass Method_Unknown.
	 * @param address The IP address/host name/serial port to connect through.
	 * @param station The station number to connect to.
	 * @throws IOException If there is a problem opening communications.
	 */
	public void open(TransportMethod method, String address, short station) throws IOException {
		this.station = station;
		
		// Connect to the station
		activeHandler = new UDRLink(method, address);
		
		MessageDispatcher.getDispatcher().registerHandler(activeHandler);
		
		udr = new UDRLib(UDRMessage.STA_ANY, (byte)0);
		
		try {
			udr.waitForResponse(station, 5000);
		} catch (TimeoutException te) {
			MessageDispatcher.getDispatcher().unregisterHandler(activeHandler);
			
			activeHandler.close();
			activeHandler = null;
			udr = null;
			
			throw new IOException("Station is not responding", te);
		}
		
		remoteCWD = "/";
	}
	
	private void nlist(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
			System.err.println("Usage: nlist");
		} else {
			try {
    			String[] response = nlist();
    			for (int i = 0; i < response.length; i++) {
    				System.out.println(response[i]);
    			}
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Get a directory listing of the current directory containing only the file and directory names.
	 * Directory names will end with a '/' character.
	 */
	public String[] nlist() throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		FS_Dir.DirEntry[] entries = udr.dir(station, remoteCWD, false, null);
		
		String[] names = new String[entries.length];
		for (int i = 0; i < entries.length; i++) {
			names[i] = entries[i].name;
		}
		
		return names;
	}
	
	private void modtime(StringTokenizer tok) {
		if (tok.countTokens() != 1) {
			System.err.println("Usage: modtime <file name>");
		} else {
			String remotePath = tok.nextToken();
			
			try {
    			int mt = modtime(remotePath);
    			String timestamp = makeDateString(mt);
    			System.out.printf("Modification time of '%s' is %s\n", remotePath, timestamp);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Get the last modified timestamp of a remote file.
	 * @param remotePath The remote file.
	 * @return The modification time, as the number of seconds since the epoch.
	 * @throws IOException If a problem occurs while communicating with the device.
	 * @throws TimeoutException If the device does not respond.
	 */
	public int modtime(String remotePath) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		remotePath = makeRemoteAbsolute(remotePath);
		
		int[] modTime = new int[1];
		udr.statFile(station, remotePath, null, modTime, null, null);
		
		return modTime[0];
	}
	
	private void mkdir(StringTokenizer tok) {
		if (tok.countTokens() != 1) {
			System.err.println("Usage: mkdir <dir name>");
		} else {
			String dirName = tok.nextToken();
			
			try {
				mkdir(dirName);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Create a new remote directory.
	 * @param remotePath The directory to create.
	 * @throws IOException If there is a problem communicating with the device.
	 * @throws TimeoutException If the device does not respond.
	 */
	public void mkdir(String remotePath) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		remotePath = makeRemoteAbsolute(remotePath);
		
		if (!remotePath.endsWith("/")) remotePath += "/";
		
		udr.createFile(station, remotePath, 0);
	}
	
	private void ls(StringTokenizer tok) {
	    if (tok.countTokens() != 0) {
	    	System.err.println("Usage: ls");
	    } else {
	    	try {
    	    	FS_Dir.DirEntry[] entries = ls();
    	    	for (int i = 0; i < entries.length; i++) {
    	    		StringBuilder perms = new StringBuilder();
    	    		
    	    		/*
    	    		// This is used to discover what the bits mean
    	    		for (int bit = 31; bit >= 0; bit--) {
    	    			if (((entries[i].mode >> bit) & 1) == 1) {
    	    				perms.append("1");
    	    			} else {
    	    				perms.append("0");
    	    			}
    	    		}
    	    		*/
    	    		
    	    		if (((entries[i].mode >> 13) & 1) == 1) {
    	    			if (((entries[i].mode >> 15) & 1) == 1) {
        	    			// Link
        	    			perms.append("l");
    	    			} else if (((entries[i].mode >> 14) & 1) == 1) {
    	    				// Block special
    	    				perms.append("b");
    	    			} else {
    	    				// Character special
    	    				perms.append("c");
    	    			}
    	    		} else if (((entries[i].mode >> 14) & 1) == 1) {
    	    			// Directory
    	    			perms.append("d");
    	    		} else {
    	    			// Nothing special
    	    			perms.append("-");
    	    		}
    	    		
    	    		// Owner
    	    		if (((entries[i].mode >> 8) & 1) == 1) {
	    				perms.append("r");
	    			} else {
	    				perms.append("-");
	    			}
	    			
	    			if (((entries[i].mode >> 7) & 1) == 1) {
	    				perms.append("w");
	    			} else {
	    				perms.append("-");
	    			}
	    			
	    			if (((entries[i].mode >> 11) & 1) == 1) {
	    				// Set UID
	    				if (((entries[i].mode >> 6) & 1) == 1) {
	    					// And executable
	    					perms.append("s");
	    				} else {
	    					// Not executable
	    					perms.append("S");
	    				}
	    			} else {
	    				// Not set UID
	    				if (((entries[i].mode >> 6) & 1) == 1) {
	    					// Executable
	    					perms.append("x");
	    				} else {
	    					perms.append("-");
	    				}
	    			}
	    			
	    			// Group
	    			if (((entries[i].mode >> 5) & 1) == 1) {
	    				perms.append("r");
	    			} else {
	    				perms.append("-");
	    			}
	    			
	    			if (((entries[i].mode >> 4) & 1) == 1) {
	    				perms.append("w");
	    			} else {
	    				perms.append("-");
	    			}
	    			
	    			if (((entries[i].mode >> 10) & 1) == 1) {
	    				// Set GID
	    				if (((entries[i].mode >> 3) & 1) == 1) {
	    					// And executable
	    					perms.append("s");
	    				} else {
	    					// Not executable
	    					perms.append("S");
	    				}
	    			} else {
	    				// Not set GID
	    				if (((entries[i].mode >> 3) & 1) == 1) {
	    					// Executable
	    					perms.append("x");
	    				} else {
	    					perms.append("-");
	    				}
	    			}
	    			
	    			// Other
	    			if (((entries[i].mode >> 2) & 1) == 1) {
	    				perms.append("r");
	    			} else {
	    				perms.append("-");
	    			}
	    			
	    			if (((entries[i].mode >> 1) & 1) == 1) {
	    				perms.append("w");
	    			} else {
	    				perms.append("-");
	    			}
	    			
	    			if (((entries[i].mode >> 9) & 1) == 1) {
	    				// Sticky
	    				if (((entries[i].mode >> 6) & 1) == 1) {
	    					// And executable
	    					perms.append("t");
	    				} else {
	    					// And not executable
	    					perms.append("T");
	    				}
    				} else if (((entries[i].mode >> 6) & 1) == 1) {
    					// Not sticky and executable
    					// Executable
	    				perms.append("x");
	    			} else {
	    				// Nothing special
	    				perms.append("-");
	    			}
	    			
    	    		System.out.printf("%10s  %3d  %3d  %9d  %19s  %s\n", perms.toString(), entries[i].uid, entries[i].gid, entries[i].size, makeDateString(entries[i].timestamp), entries[i].name);
    	    	}
	    	} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
	    }
    }
	
	/**
	 * Get a directory listing of the current remote directory.
	 * @throws IOException If there is a problem communicating with the device.
	 * @throws TimeoutException If the device does not respond.
	 */
	public FS_Dir.DirEntry[] ls() throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		// Sort the directory listing
		FS_Dir.DirEntry[] listing = udr.dir(station, remoteCWD, true, null);
		Arrays.sort(listing, new Comparator<FS_Dir.DirEntry>() {
            public int compare(DirEntry o1, DirEntry o2) {
            	return (o1.name.compareTo(o2.name));
            }
		});
		
		return listing;
	}
	
	private void lpwd(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
			System.err.println("Usage: lpwd");
		} else {
			String localPath = lpwd();
			System.out.println("Local working directory is '" + localPath + "'");
		}
    }
	
	/**
	 * Get the local present working directory.
	 */
	public String lpwd() {
		return localCWD.getAbsolutePath();
	}
	
	private void lls(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
			System.err.println("Usage: lls");
		} else {
			File[] entries = lls();
			
			for (int i = 0; i < entries.length; i++) {
				if (entries[i].isDirectory()) {
					System.out.print("d");
				} else {
					System.out.print("-");
				}
				
				if (entries[i].canRead()) {
					System.out.print("r");
				} else {
					System.out.print("-");
				}
				
				if (entries[i].canWrite()) {
					System.out.print("w");
				} else {
					System.out.print("-");
				}
				
				if (entries[i].canExecute()) {
					System.out.print("x");
				} else {
					System.out.print("-");
				}
				
				System.out.printf("  %9d  %19s  %s\n", entries[i].length(), makeDateString((int)(entries[i].lastModified() / 1000)), entries[i].getName());
			}
		}
	}
	
	/**
	 * Get a list of files in the current local directory.
	 */
	public File[] lls() {
		File[] entries = localCWD.listFiles(new FileFilter() {
			public boolean accept(File file) {
				if (file.isHidden()) return false;
				
				return true;
            }
		});
		
		return entries;
	}
	
	private void lcd(StringTokenizer tok) {
	    if (tok.countTokens() != 1) {
	    	System.err.println("Usage: lcd <directory name>");
	    } else {
	    	String dirName = tok.nextToken();
	    	
	    	try {
	    		lcd(dirName);
	    	} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
	    }
    }
	
	/**
	 * Change the local directory.
	 * @param localPath The new local directory.
	 * @throws IOException If the provided path does not reference a directory.
	 */
	public void lcd(String localPath) throws IOException {
		localPath = makeLocalAbsolute(localPath);
		
		File newCWD = new File(localPath);
		if (!newCWD.isDirectory()) {
			throw new IOException("Not a directory");
		}
		
		localCWD = newCWD.getCanonicalFile();
	}
	
	private void help(StringTokenizer tok) {
	    if (tok.countTokens() == 0) {
	    	System.out.println("append   ascii    bell    binary   bye");
	    	System.out.println("cd       close    delete  get      hash");
	    	System.out.println("help     lcd      lls     lpwd     ls");
	    	System.out.println("mkdir    modtime  nlist   open     put");
	    	System.out.println("pwd      reget    rename  restart  rmdir");
	    	System.out.println("site     size     status  tick     timeout");
	    } else if (tok.countTokens() == 1){
	    	String cmdName = tok.nextToken();
	    	
	    	if (cmdName.equals("append")) {
	    		System.out.println("Usage: append <local file> [<remote file>]");
	    		System.out.println("  Transfer all of <remote file> and save at the end of <local file>");
	    		System.out.println("  If <remote file> is not provided, uses <local file>");
			} else if (cmdName.equals("ascii")) {
				System.out.println("Usage: ascii");
				System.out.println("  Switch transfer mode to ASCII");
				System.out.println("  When sending, all CRLF strings are sent as LF");
				System.out.println("  When receiving, all LF characters are saved as CRLF");
			} else if (cmdName.equals("bell")) {
				System.out.println("Usage: bell");
				System.out.println("  When each transfer completes, the system bell will ding");
			} else if (cmdName.equals("binary")) {
				System.out.println("Usage: binary");
				System.out.println("  Switch transfer mode to BINARY");
				System.out.println("  Sending/receiving data is byte-by-byte, regardless of content");
			} else if (cmdName.equals("bye")) {
				System.out.println("Usage: bye");
				System.out.println("  Disconnect and kill the command interprenter");
			} else if (cmdName.equals("cd")) {
				System.out.println("Usage: cd <remote path>");
				System.out.println("  Change the current remote directory to <remote path>");
			} else if (cmdName.equals("close")) {
				System.out.println("Usage: close");
				System.out.println("  Disconnect any active connection");
			} else if (cmdName.equals("delete")) {
				System.out.println("Usage: delete <remote path>");
				System.out.println("  Deletes <remote path>");
			} else if (cmdName.equals("get")) {
				System.out.println("Usage: get <remote path> [<local path>]");
				System.out.println("  Retrieve <remote path>, saving the file as <local path>");
				System.out.println("  If <local path> is not provided, uses <remote path>");
			} else if (cmdName.equals("hash")) {
				System.out.println("Usage: hash");
				System.out.println("  Toggle hash printing");
				System.out.println("  If on, a # will be printed every 1% transferred");
				System.out.println("  This option is mutually exclusive with 'tick'");
			} else if (cmdName.equals("help")) {
				System.out.println("Usage: help [<command>]");
				System.out.println("  If <command> is provided, provides usage instructions for <command>");
				System.out.println("  If <command> is missing, lists all available commands");
			} else if (cmdName.equals("lcd")) {
				System.out.println("Usage: lcd <local path>");
				System.out.println("  Change the current local directory to <local path>");
			} else if (cmdName.equals("lls")) {
				System.out.println("Usage: lls");
				System.out.println("  List the contents of the current local directory");
			} else if (cmdName.equals("lpwd")) {
				System.out.println("Usage: lpwd");
				System.out.println("  Echo the path of the current local directory");
			} else if (cmdName.equals("ls")) {
				System.out.println("Usage: ls");
				System.out.println("  List the contents of the current remote directory");
			} else if (cmdName.equals("mkdir")) {
				System.out.println("Usage: mkdir <remote path>");
				System.out.println("  Create a new directory named <remote path>");
			} else if (cmdName.equals("modtime")) {
				System.out.println("Usage: modtime <remote path>");
				System.out.println("  Echo the modification time of <remote path> as seconds since the epoch");
			} else if (cmdName.equals("nlist")) {
				System.out.println("Usage: nlist");
				System.out.println("  List the contents of the current remote directory as names only");
			} else if (cmdName.equals("open")) {
				System.out.println("Usage: open {s <serial port>|t <IP address>|u <IP address>} <station number>");
				System.out.println("  Create a new connection using the specified method to communicate");
				System.out.println("    with the the station with number <station number>");
			} else if (cmdName.equals("put")) {
				System.out.println("Usage: put <local path> {<remote path>}");
				System.out.println("  Send <local path> to the device, saving it as <remote path>");
				System.out.println("  If <remote path> is not provided, <local path> will be used");
			} else if (cmdName.equals("pwd")) {
				System.out.println("Usage: pwd");
				System.out.println("  Echo the path of the current remote directory");
			} else if (cmdName.equals("reget")) {
				System.out.println("Usage: reget <remote path> [<local path>]");
				System.out.println("  Continue retrieving data into <local path> from <remote path>");
				System.out.println("  This will append data to <local path>, using the current size");
				System.out.println("    of <local path> as the starting offset for reading from");
				System.out.println("    <remote path>");
				System.out.println("  If <local path> is not provided, <remote path> will be used");
			} else if (cmdName.equals("rename")) {
				System.out.println("Usage: rename <old remote path> <new remote path>");
				System.out.println("  Renames the file/directory <old remote path> to <new remote path>");
			} else if (cmdName.equals("restart")) {
				System.out.println("Usage: restart <byte count>");
				System.out.println("  Set the file offset of the local/remote sides to start the next");
				System.out.println("    transfer at");
				System.out.println("  If <byte count> is not zero, both the local and remote files must");
				System.out.println("    exist for the transfer to work");
				System.out.println("  If <byte count> is zero, the target file will be created when a");
				System.out.println("    transfer begins");
			} else if (cmdName.equals("rmdir")) {
				System.out.println("Usage: rmdir <remote path>");
				System.out.println("  Delete the remote directory named <remote path>");
			} else if (cmdName.equals("site")) {
				System.out.println("Usage: site <command>");
				System.out.println("  Treats the rest of the line past 'site' as a shell command to");
				System.out.println("    execute on the remote device");
				System.out.println("  The output from the command will be echoed");
			} else if (cmdName.equals("size")) {
				System.out.println("Usage: size <remote path>");
				System.out.println("  Echo the size of <remote path> in bytes");
			} else if (cmdName.equals("status")) {
				System.out.println("Usage: status");
				System.out.println("  Echoes the current settings of the program");
			} else if (cmdName.equals("tick")) {
				System.out.println("Usage: tick");
				System.out.println("  Toggle tick printing");
				System.out.println("  If on, a single line will be kept updated with the current");
				System.out.println("    transfer percentage");
				System.out.println("  This option is mutually exclusive with 'hash'");
			} else if (cmdName.equals("timeout")) {
				System.out.println("Usage: timeout <milliseconds>");
				System.out.println("  Sets the timeout before deciding that a 'site' command has failed");
			} else {
				System.err.println("Unknown command: " + cmdName);
			}
	    }
    }
	
	private void hash(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
			System.err.println("Usage: hash");
		} else {
			boolean on = !hash();
			hash(on);
			
			System.out.printf("Hash printing is %s\n", on ? "on" : "off");
		}
	}
	
	/**
	 * Turn on/off hash printing.
	 * @param on If false, turns off hash printing. If true, a # will be printed after
	 *   each 1% is transferred. If true, tick printing will be turned off.
	 */
	public void hash(boolean on) {
		hashPrinting = on;
		
		if (hashPrinting) tick(false);
	}
	
	/**
	 * Get the current hash setting.
	 */
	public boolean hash() {
		return hashPrinting;
	}

	private void get(StringTokenizer tok) {
		if (tok.countTokens() < 1 || tok.countTokens() > 2) {
			System.err.println("Usage: get <remote file> [<local file>]");
		} else {
			String remotePath = tok.nextToken();
			String localPath = remotePath;
			
			if (tok.countTokens() == 1) {
				localPath = tok.nextToken();
			}
			
			try {
				float rate = get(remotePath, localPath);
				System.out.printf("Transferred '%s' at %.3f KB/s\n", remotePath, rate);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
    }
	
	/**
	 * Transfer a file from the remote device.
	 * @param remotePath The path to the file on the remote device.
	 * @param localPath The path to the file on the local machine.
	 * @return The speed of the transfer in kilobytes per second.
	 * @throws IOException If there is a problem communicating with the device.
	 * @throws TimeoutException If the device does not respond.
	 */
	public float get(String remotePath, String localPath) throws IOException, TimeoutException {
		return get(remotePath, localPath, restartOffset, restartOffset);
	}
	
	/**
	 * Transfer a file from remote device, starting at the provided offsets in both the remote
	 *   and local files.
	 * @param remotePath The path to the file on the remote device.
	 * @param localPath The path to the file on the local machine.
	 * @param remoteStart The offset within the remote file at which to start reading.
	 * @param localStart The offset within the local file at which to start writing. If this is
	 *   non-zero, the local file MUST exist.
	 * @return The speed of the transfer in kilobytes per second.
	 * @throws IOException If there is a problem communicating with the device.
	 * @throws TimeoutException If the device does not respond.
	 */
	public float get(String remotePath, String localPath, int remoteStart, int localStart) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		remotePath = makeRemoteAbsolute(remotePath);
		localPath = makeLocalAbsolute(localPath);
		
		final File localFile = new File(localPath);
		
		if (localFile.isFile() && localStart == 0) localFile.delete();
		
		// Open the remote file
		int[] alias = new int[1];
		alias[0] = udr.getAlias(station, remotePath);
		
		// Get the size of the remote file
		int[] out_fileSize = new int[1];
		udr.statFile(station, remotePath, out_fileSize, null, null, alias);
		
		final int fileSize = out_fileSize[0];
		
		ProgressListener pl = null;
		
		if (tickPrinting) {
			pl = new ProgressListener() {
				public void updateProgress(float percent) {
					System.out.printf("\r%.1f%%", percent * 100.0f);
					System.out.flush();
                }
			};
		} else if (hashPrinting) {
			pl = new ProgressListener() {
				private float lastPercent = 0.0f;
				
				public void updateProgress(float percent) {
					int diff = (int)(percent * 100) - (int)(lastPercent * 100);
					
					if (diff > 0) {
    					for (int i = 0; i < diff; i++) {
    						System.out.print("#");
    					}
    					System.out.flush();
    					
    					// Leave any difference in there so we will be sure to hit all of the # marks
    					lastPercent = percent - (percent - lastPercent - (diff / 100.0f));
					}
                }
			};
		}
		
		long startTime = System.nanoTime();
		int bytesTransferred = 0;
		
		// Read large blocks of the local file, translate as necessary, and send to the remote device
		RandomAccessFile fileOut = new RandomAccessFile(localFile, "rw");
		
		try {
			fileOut.seek(localStart);
			
    		byte[] block;
    		int fileOffset = remoteStart;
    		
    		while (fileOffset < fileSize) {
    			int blockSize = 8192;
    			if (fileSize - fileOffset < blockSize) blockSize = fileSize - fileOffset;
    			
    			block = udr.readFile(station, remotePath, fileOffset, blockSize, alias);
    			fileOffset += block.length;
    			bytesTransferred += block.length;
    			
    			if (pl != null) {
    				pl.updateProgress(bytesTransferred / ((float)fileSize - (float)remoteStart));
    			}
    			
    			if (!binaryMode) {
    				// Search out all occurrences of "\n" and turn them into "\r\n"
    				int oldOffset = 0;
    				int newOffset = 0;
    				byte[] newBlock = new byte[block.length * 2]; // Maximum expansion, if everything is '\n'
    				
    				while (oldOffset < block.length) {
    					if (block[oldOffset] == (byte)'\n') {
    						// Add a '\r'
    						newBlock[newOffset++] = (byte)'\r';
    					}
    					
    					newBlock[newOffset++] = block[oldOffset++];
    				}
    				
    				block = newBlock;
    				blockSize = newOffset;
    			}
    			
    			// Write the block to the local file
    			fileOut.write(block, 0, blockSize);
    		}
    		
    		if (tickPrinting) {
    			System.out.print("\r100.0%");
    		}
    		
    		if (pl != null) System.out.println();
    		if (bellOn) Toolkit.getDefaultToolkit().beep();
		} finally {
			fileOut.close();
		}
		
		long stopTime = System.nanoTime();
		
		// Convert fileSize to kilobytes and elapsed time to seconds, then divide
		return ((bytesTransferred / 1024.0f) / ((stopTime - startTime) / 1000000000.0f));
	}
	
	private void delete(StringTokenizer tok) {
		if (tok.countTokens() != 1) {
			System.err.println("Usage: delete <file name>");
		} else {
			String remotePath = tok.nextToken();
			
			try {
				delete(remotePath);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Delete a remote file.
	 * @param remotePath The file to delete.
	 * @throws IOException If there is a problem communicating with the remote device.
	 * @throws TimeoutException If the remote device does not respond.
	 */
	public void delete(String remotePath) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		remotePath = makeRemoteAbsolute(remotePath);
		udr.deleteFile(station, remotePath);
	}
	
	private void close(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
	    	System.err.println("Usage: close");
	    } else {
	    	close();
	    }
	}
	
	/**
	 * Close any active connection. Does nothing if not connected.
	 */
	public void close() {
		if (activeHandler == null) return;
		
		MessageDispatcher.getDispatcher().unregisterHandler(activeHandler);
		activeHandler = null;
		udr = null;
	}
	
	private void cd(StringTokenizer tok) {
	    if (tok.countTokens() != 1) {
	    	System.err.println("Usage: cd <directory>");
	    } else {
	    	String remotePath = tok.nextToken();
	    	
	    	try {
	    		cd(remotePath);
	    	} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
	    }
    }
	
	/**
	 * Change to a new remote directory.
	 * @param remotePath The new directory name.
	 * @throws IOException If the directory does not exist or some other communication problem occurs.
	 * @throws TimeoutException If the device does not respond.
	 */
	public void cd(String remotePath) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		remotePath = makeRemoteAbsolute(remotePath);
		
		if (!remotePath.endsWith("/")) remotePath += "/";
		
		// Make sure it exists
		udr.statFile(station, remotePath, null, null, null, null);
		
		remoteCWD = remotePath;
	}
	
	private void bye(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
			System.err.println("Usage: bye");
		} else {
			bye();
		}
    }
	
	/**
	 * Disconnect and stop the command interpreter.
	 */
	public void bye() {
		close();
		running = false;
		
		if (commandThread != null) commandThread.interrupt();
	}
	
	private void binary(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
			System.err.println("Usage: binary");
		} else {
			binary();
		}
	}
	
	/**
	 * Set transfer mode to binary (not ascii).
	 */
	public void binary() {
		binaryMode = true;
	}
	
	/**
	 * Returns true if transfers will use binary mode.
	 */
	public boolean isBinary() {
		return binaryMode;
	}
	
	private void bell(StringTokenizer tok) {
		if (tok.countTokens() != 0) {
			System.err.println("Usage: bell");
		} else {
			boolean on = !bell();
			bell(on);
			
			System.out.printf("Bell is %s\n", on ? "on" : "off");
		}
	}
	
	/**
	 * Set whether to sound a bell at the end of each transfer.
	 * @param on If true, the system bell will ding after each transfer.
	 */
	public void bell(boolean on) {
		bellOn = on;
	}
	
	/**
	 * Returns true if a bell will be sounded at the end of each transfer.
	 */
	public boolean bell() {
		return bellOn;
	}
	
	private void ascii(StringTokenizer tok) {
	    if (tok.countTokens() != 0) {
	    	System.err.println("Usage: ascii");
	    } else {
	    	ascii();
	    }
    }
	
	/**
	 * Set transfer mode to ascii (not binary).
	 */
	public void ascii() {
		binaryMode = false;
	}
	
	/**
	 * Returns true if transfers will use ascii mode.
	 */
	public boolean isAscii() {
		return !binaryMode;
	}
	
	private void append(StringTokenizer tok) {
		if (tok.countTokens() < 1 || tok.countTokens() > 2) {
			System.err.println("Usage: append <local file> [<remote file>]");
		} else {
			String localPath = tok.nextToken();
			String remotePath = localPath;
			
			if (tok.countTokens() == 1) {
				remotePath = tok.nextToken();
			}
			
			try {
				append(localPath, remotePath);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
    }
	
	/**
	 * Append data from a remote file to a local file.
	 * @param localPath The file on the local machine.
	 * @param remotePath The file on the remote device.
	 * @throws IOException If there is a problem communicating with the device.
	 * @throws TimeoutException If the device does not respond.
	 */
	public void append(String localPath, String remotePath) throws IOException, TimeoutException {
		if (activeHandler == null) throw new IOException("Not connected");
		
		File localFile;
		
		localFile = new File(makeLocalAbsolute(localPath));
		
		if (localFile.isFile()) {
			get(remotePath, localPath, 0, (int)localFile.length());
		} else {
			get(remotePath, localPath, 0, 0);
		}
	}
	
	// Make a (possibly) relative local path into an absolute one
	private String makeLocalAbsolute(String localPath) {
		File localFile = new File(localPath);
		if (!localFile.isAbsolute()) {
			localFile = new File(localCWD, localPath);
		}
		
		return localFile.getAbsolutePath();
	}
	
	// Make a (possibly) relative remote path into an absolute one
	private String makeRemoteAbsolute(String remotePath) {
		if (!remotePath.startsWith("/")) {
			remotePath = (remoteCWD + remotePath);
		}
		
		// Scan for "." and ".." entries
		StringTokenizer tok = new StringTokenizer(remotePath, "/");
		Deque<String> pathComponents = new LinkedList<String>();
		
		while (tok.hasMoreTokens()) {
			String pathComponent = tok.nextToken();
			
			if (pathComponent.equals(".")) {
				// Skip this
				continue;
			} else if (pathComponent.equals("..")) {
				// Remove the last path component from the list
				pathComponents.removeLast();
			} else {
				// Add this path component to the stack
				pathComponents.addLast(pathComponent);
			}
		}
		
		// Convert the path stack to a string
		StringBuilder path = new StringBuilder("/");
		while (!pathComponents.isEmpty()) {
			String pathComponent = pathComponents.removeFirst();
			path.append(pathComponent);
			
			if (!pathComponents.isEmpty()) path.append("/");
		}
		
		return path.toString();
	}
	
	/**
	 * Translate a 'seconds since the epoch' timestamp into a human-readable string.
	 * @param time The time in seconds since the epoch (January 1, 1970).
	 * @return A String representing the time in the form "yyyy-mm-dd hh:mm:ss".
	 */
	public static String makeDateString(int time) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis((long)time * 1000);
		
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);
		
		return String.format("%04d-%02d-%02d %02d:%02d:%02d", year, month, day, hour, min, sec);
	}
}

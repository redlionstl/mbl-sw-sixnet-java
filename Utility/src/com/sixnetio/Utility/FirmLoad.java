/*
 * FirmLoad.java
 *
 * Performs a firmware load operation from the CLI.
 *
 * Jonathan Pearson
 * April 17, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.util.*;

import org.apache.log4j.*;

import com.sixnetio.Station.*;
import com.sixnetio.Switch.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.io.UnbufferedInputStream;
import com.sixnetio.net.IfcUtil;
import com.sixnetio.util.Utils;

public class FirmLoad implements FirmwareLoaderUI {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public static final String PROG_VERSION = "0.1.1";
	
	static {
		String props = Utils.getFirstExistingFile(new String[] {
				"log4j.properties",
				System.getProperty("user.home") + File.separator + ".firmload.log4j.properties"
		});
		
		if (props == null) {
			Logger.getRootLogger().setLevel(Level.OFF);
		} else {
			PropertyConfigurator.configureAndWatch(props);
		}
	}
	
	private static void usage(PrintStream out) {
		out.println("FirmLoad v. " + PROG_VERSION + " (c) 2009 SIXNET");
		out.println("  Written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: FirmLoad -f <bundle> [-i <IP>] -a <switch IP> [-b <subnet>] [-g <gtwy>]");
		out.println("       [{-s <serial port> [-A <DO IP> -D <DO POINT> [-D <DO POINT> [...]]");
		out.println("       [-A ... -D ... [...]] | [-l <user>:<pswd>] [-r]");
		out.println("       [-F]}] [-d <dir>] [-w <timeout>] [{-p|-q}] [-V] [-6] [L] [-h]");
		
		out.println("-f <bundle>       Specify the firmware bundle to load onto the switch");
		out.println("-i <IP>           Specify the computer's IP address for TFTP; if not specified,");
		out.println("                    the external IP address of the computer will be used. If");
		out.println("                    the specified address is not available on this computer,");
		out.println("                    the program will exit with an error message. If there are");
		out.println("                    multiple addresses available on this computer, the most");
		out.println("                    specific (by subnet) will be chosen. If none of the");
		out.println("                    addresses will be able to communicate, the program will");
		out.println("                    exit with an error message");
		out.println("-a <switch IP>    Specify the IP address of the switch to load");
		out.println("-b <subnet>       Specify the subnet for the switch; default is 255.255.255.0");
		out.println("-g <gtwy>         Specify the gateway address for the switch; default is blank");
		out.println("-s <serial port>  Specify the serial port to use on the computer for a serial");
		out.println("                    load; if not specified, an ethernet-only load will be");
		out.println("                    performed");
		out.println("-A <DO IP>        If you would like to have the program automatically toggle");
		out.println("                    power during a serial load, this is the IP address of the");
		out.println("                    DO module that controls the power supply to the switch. If");
		out.println("                    not provided, you will be prompted to cycle power as");
		out.println("                    necessary. You may provide multiple DO IP addresses");
		out.println("                    followed by as many DO Points as necessary for each one;");
		out.println("                    all of the specified points will be toggled as closely as");
		out.println("                    possible to each other");
		out.println("-D <DO point>     The discrete output index (starting from 1) that controls");
		out.println("                    to the switch");
		out.println("-l <user>:<pswd>  Specify the user/password to use for an ethernet-only load;");
		out.println("                    if not specified, admin:admin will be used");
		out.println("-r                Restore the switch's configuration after an ethernet-only");
		out.println("                    load");
		out.println("-F                If the switch is set not to accept ethernet-only loads,");
		out.println("                    change the setting prior to performing the load; if not");
		out.println("                    specified, and the switch is set not to accept ethernet-");
		out.println("                    only loads, the program will exit with an error message");
		out.println("-d <dir>          If you are running your own TFTP server, you may specify the");
		out.println("                    directory that it is sharing with this option. The");
		out.println("                    directory must be writeable by the user running this");
		out.println("                    program");
		out.println("-w <timeout>      Specify the number of seconds to wait for the switch's web");
		out.println("                    server to respond after completing a load. Use 0 (or less)");
		out.println("                    to not wait at all, or 'forever' (the default) to wait");
		out.println("                    forever");
		out.println("-p                Display progress of TFTP transfers to the switch; mutually");
		out.println("                    exclusive with -q");
		out.println("-q                Quiet mode. Only error messages will be printed, and those go");
		out.println("                    to stderr. Normally, status messages are also printed, to");
		out.println("                    stdout. Mutually exclusive with -p");
		out.println("-V                Ignore any vendor mismatch between the firmware bundle and");
		out.println("                    the switch");
		out.println("-6                Enable IPv6 support");
		out.println("-L                Output a list of externally visible IP addresses for this");
		out.println("                    computer and exit");
		out.println("-h                Output this message and exit");
	}
	
	public static void main(String[] args) {
		FirmLoad loader = new FirmLoad();
		
		// Parse arguments
		
		try {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-f")) {
					File firmwareFile = new File(args[++i]);
					if (!firmwareFile.isFile()) {
						throw new Exception(String.format("File '%s' does not exist or is not a file",
						                                  firmwareFile.getAbsolutePath()));
					}
					
					InputStream in = new FileInputStream(firmwareFile);
					try {
						loader.setBundle(new FirmwareBundle(in));
					}
					finally {
						in.close();
					}
				} else if (args[i].equals("-i")) {
					loader.setMyIP(args[++i]);
				} else if (args[i].equals("-a")) {
					loader.setSwitchIP(args[++i]);
				} else if (args[i].equals("-b")) {
					loader.setSwitchSubnet(args[++i]);
				} else if (args[i].equals("-g")) {
					loader.setSwitchGateway(args[++i]);
				} else if (args[i].equals("-s")) {
					loader.setSerial(args[++i]);
				} else if (args[i].equals("-A")) {
					loader.addDO_IP(args[++i]);
				} else if (args[i].equals("-D")) {
					try {
						// User was told to input 1-based, we need 0-based
						short doPoint = (short)(Short.parseShort(args[++i]) - 1);
						if (doPoint < 0) throw new Exception("DO Point must be at least 1");
						
						loader.addDO_Point(doPoint);
					} catch (NumberFormatException nfe) {
						throw new Exception(String.format("DO point '%s' is not a valid value: %s",
						                                  args[i], nfe.getMessage()), nfe);
					}
				} else if (args[i].equals("-l")) {
					i++;
					
					if (args[i].indexOf(':') == -1) {
						throw new Exception("Both user name and password are required for -l");
					}
					
					loader.setUser(args[i].substring(0, args[i].indexOf(':')));
					loader.setPassword(args[i].substring(args[i].indexOf(':') + 1));
				} else if (args[i].equals("-r")) {
					loader.setRestoreConfig(true);
				} else if (args[i].equals("-F")) {
					loader.setForceLoad(true);
				} else if (args[i].equals("-d")) {
					loader.setExternalTFTPDir(new File(args[++i]));
					
					if (!loader.getExternalTFTPDir().isDirectory()) {
						throw new Exception(String.format("Directory '%s' does not exist or is not a directory",
						                                  loader.getExternalTFTPDir().getAbsolutePath()));
					}
				} else if (args[i].equals("-w")) {
					Long waitFor;
					
					try {
						if (args[++i].equalsIgnoreCase("forever")) {
							waitFor = null;
						} else {
							waitFor = Long.parseLong(args[i]);
						}
					} catch (NumberFormatException nfe) {
						throw new Exception("Argument to -w must be 'forever' or a number", nfe);
					}
					
					if (waitFor == null) {
						loader.setWaitFor(0);
					} else if (waitFor <= 0) {
						loader.setWaitFor(-1);
					} else {
						loader.setWaitFor(waitFor * 1000); // Convert to ms
					}
				} else if (args[i].equals("-p")) {
					loader.setProgressIndicator(true);
				} else if (args[i].equals("-q")) {
					loader.setQuiet(true);
				} else if (args[i].equals("-V")) {
					loader.setIgnoreVendor(true);
				} else if (args[i].equals("-6")) {
					loader.setIpv6(true);
				} else if (args[i].equals("-L")) {
					Map<String, InterfaceAddress> localAddresses =
						IfcUtil.discoverLocalAddresses(loader.getIpv6());
					
					System.out.println("Available addresses:");
					
					for (Map.Entry<String, InterfaceAddress> entry : localAddresses.entrySet()) {
						String addr = entry.getKey();
						InterfaceAddress ifcAddr = entry.getValue();
						
						System.out.printf("  %s/%d\n", addr, ifcAddr.getNetworkPrefixLength());
					}
					
					return;
				} else if (args[i].equals("-h")) {
					usage(System.out);
					return;
				} else {
					throw new Exception("Unrecognized argument: " + args[i]);
				}
			}
			
			// Validate arguments
			// Required: Firmware bundle and switch IP
			if (loader.getBundle() == null) {
				throw new Exception("You must specify a firmware bundle");
			}
			
			if (loader.getSwitchIP() == null) {
				throw new Exception("You must specify the switch's IP address");
			}
			
			// Choose either serial or user/password, restore config, force load
			if (loader.getSerial() != null &&
				(loader.getUser() != null ||
				 loader.getPassword() != null ||
				 loader.getRestoreConfig() ||
				 loader.getForceLoad())) {
				
				throw new Exception("The user/password, restore config, and force load options are incompatible with serial loading");
			}
			
			// Make sure serial is selected if using a DO
			if (loader.getSerial() == null &&
				loader.getDO_IPs().size() != 0) {
				
				throw new Exception("DO configuration is incompatible with an ethernet-only load");
			}
			
			if (loader.getProgressIndicator() && loader.isQuiet()) {
				throw new Exception("You may either have a progress indicator or a quiet load, not both");
			}
			
			// Grab a list of IP addresses for this computer
			Map<String, InterfaceAddress> localAddresses = IfcUtil.discoverLocalAddresses(loader.getIpv6());
			
			// If the user passed a local address, check that it is in the list
			if (loader.getMyIP() != null) {
				if (!localAddresses.containsKey(loader.getMyIP())) {
					throw new Exception(String.format("The specified IP address, '%s', is not available from this computer",
					                                  loader.getMyIP()));
				}
			}
			
			// If the user did not pass a local address, make sure there is only one entry in the list
			else {
				if (localAddresses.size() < 1) {
					throw new Exception("This computer does not have any externally visible IP addresses, firmware loading is impossible");
				}

				if (localAddresses.size() > 1) {
					// Try to find the most relevant address (most likely to work)
					String ipaddr = null;
					InetAddress chosenIP = null;
					InetAddress switchIP = InetAddress.getByName(loader.getSwitchIP());
					short networkPrefixLength = 0;
					
					for (Map.Entry<String, InterfaceAddress> entry : localAddresses.entrySet()) {
						// Only compare if they are the same type of address
						if (entry.getValue().getAddress().getClass()
								.equals(switchIP.getClass())) {
							// Is this address able to communicate with the
							//   switch?
							if (IfcUtil
									.sameSubnet(entry.getValue().getAddress(),
									            switchIP,
									            entry.getValue().getNetworkPrefixLength())) {
								
    							// Compare the network prefix length with the current choice
    							if (entry.getValue().getNetworkPrefixLength() >
    									networkPrefixLength) {
    								
    								ipaddr = entry.getKey();
    								chosenIP = entry.getValue().getAddress();
    								networkPrefixLength =
    									entry.getValue().getNetworkPrefixLength();
    							}
    						}
						}
					}
					
					if (ipaddr != null) {
						logger.warn("Multiple IP addresses available on this host, chose to use '" + ipaddr + "'");
						
						loader.setMyIP(ipaddr);
					} else {
						throw new Exception("There are multiple available IP addresses from this computer, but none appear to be able to communicate with the switch. Please specify one with the -i option.");
					}
				} else {
					loader.setMyIP(localAddresses.keySet().iterator().next());
				}
				
				if (!loader.isQuiet()) {
					System.out.println("Using local IP address '" + loader.getMyIP() + "'");
				}
			}
			
			// Default user/password
			if (loader.getUser() == null) loader.setUser("admin");
			if (loader.getPassword() == null) loader.setPassword("admin");
		} catch (Exception e) {
			System.err.println(e.getMessage());
			usage(System.err);
			System.exit(1);
			return;
		}
		
		loader.performLoad();
	}
	
	// Required:
	private FirmwareBundle bundle = null;
	private String myIP = null;
	private String switchIP = null;
	
	// Optional:
	private String switchSubnet = "255.255.255.0";
	private String switchGateway = "";
	private String serial = null;
        private int baudRate = 9600;
	private List<String> doIPs = new LinkedList<String>();
	private Map<String, List<Short>> doPoints = new HashMap<String, List<Short>>();
	private String user = null;
	private String password = null;
	private boolean restoreConfig = false;
	private boolean forceLoad = false;
	private File externalTFTPDir = null;
	private long waitFor = 0; // Forever
	private boolean progressIndicator = false;
	private boolean quiet = false;
	private boolean ignoreVendor = false;
	private boolean ipv6 = false;
	
	public FirmLoad() { }
	
	public void performLoad() {
		if (getSerial() != null) {
			if (getDO_IPs().size() == 0) {
    			System.out.println("Please disconnect power from the switch. Press Enter when you've finished");
    			
    			UnbufferedInputStream in = new UnbufferedInputStream(System.in);
    			
    			try {
    				in.readLine();
    			} catch (IOException ioe) {
    				throw new RuntimeException("Unexpected exception reading from stdin: " + ioe.getMessage(), ioe);
    			}
			} else {
				// Turn off power to the switch programmatically
				if (!isQuiet()) System.out.println("Powering off the switch");
				try {
	                setPower(false);
                } catch (Exception e) {
                	System.err.printf("Error turning off the switch: %s\n", e.getMessage());
            		logger.debug("Exception thrown while turning off the switch", e);
            		
            		System.exit(1);
                }
				
				// Make sure we don't turn it on too quickly
				Utils.sleep(500);
			}
			
			SerialLoader loader =
				new SerialLoader(this,
				                 getSwitchIP(),
				                 getSwitchSubnet(),
				                 getSwitchGateway(),
				                 getSerial(),
				                 getBaudRate(),
				                 getIgnoreVendor());
			loader.setResponseTimeout(getWaitFor());
			
			loader.loadFirmware(getBundle());
		} else {
			FirmwareLoader loader = new FirmwareLoader(this, getSwitchIP(), getSwitchSubnet(), getSwitchGateway(), getUser(), getPassword(), getIgnoreVendor());
			loader.setResponseTimeout(getWaitFor());
			
			loader.loadFirmware(getBundle(), getRestoreConfig(), getForceLoad());
		}
	}
	
	private void setPower(boolean on) throws Exception {
		MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();
		
		UDRLib udr = new UDRLib(UDRMessage.STA_ANY, (byte)0);
		
		List<String> doIPs = getDO_IPs();
		for (String doIP : doIPs) {
    		UDRLink handler = new UDRLink(TransportMethod.UDP, doIP);
    		dispatcher.registerHandler(handler);
    		
    		List<Short> doPoints = getDO_Points(doIP);
    		// Find the min and max
    		short min, max;
    		min = Short.MAX_VALUE;
    		max = Short.MIN_VALUE;
    		for (short doPoint : doPoints) {
    			if (doPoint < min) min = doPoint;
    			if (doPoint > max) max = doPoint;
    		}
    		
    		// Build a list of booleans
    		boolean[] values = new boolean[max - min + 1];
    		for (short doPoint : doPoints) {
    			values[doPoint - min] = true;
    		}
    		
    		if (on) {
    			udr.setD(UDRMessage.STA_ANY, min, (short)values.length, values);
    		} else {
    			udr.clrD(UDRMessage.STA_ANY, min, (short)values.length, values);
    		}
    		
    		// All done, close the handler
    		dispatcher.unregisterHandler(handler);
    		handler.close();
		}
	}
	
	public FirmwareBundle getBundle() {
    	return bundle;
    }

	public void setBundle(FirmwareBundle bundle) {
    	this.bundle = bundle;
    }

	public String getMyIP() {
    	return myIP;
    }

	public void setMyIP(String myIP) {
    	this.myIP = myIP;
    }

	public String getSwitchIP() {
    	return switchIP;
    }

	public void setSwitchIP(String switchIP) {
    	this.switchIP = switchIP;
    }

	public String getSwitchSubnet() {
    	return switchSubnet;
    }

	public void setSwitchSubnet(String switchSubnet) {
    	this.switchSubnet = switchSubnet;
    }

	public String getSwitchGateway() {
    	return switchGateway;
    }

	public void setSwitchGateway(String switchGateway) {
    	this.switchGateway = switchGateway;
    }

	public String getSerial() {
    	return serial;
    }

	public void setSerial(String serial) {
    	this.serial = serial;
    }

    public void setBaudRate(int baud) {
        this.baudRate = baud;
    }

    public int getBaudRate() {
        return this.baudRate;
    }

	public List<String> getDO_IPs() {
    	return new LinkedList<String>(doIPs);
    }

	public void addDO_IP(String doIP) {
    	this.doIPs.add(doIP);
    	this.doPoints.put(doIP, new LinkedList<Short>());
    }

	public List<Short> getDO_Points(String ip) {
    	return new LinkedList<Short>(doPoints.get(ip));
    }

	public void addDO_Point(Short doPoint) {
		this.doPoints.get(this.doIPs.get(this.doIPs.size() - 1)).add(doPoint);
    }

	public String getUser() {
    	return user;
    }

	public void setUser(String user) {
    	this.user = user;
    }

	public String getPassword() {
    	return password;
    }

	public void setPassword(String password) {
    	this.password = password;
    }

	public boolean getRestoreConfig() {
    	return restoreConfig;
    }

	public void setRestoreConfig(boolean restoreConfig) {
    	this.restoreConfig = restoreConfig;
    }

	public boolean getForceLoad() {
    	return forceLoad;
    }

	public void setForceLoad(boolean forceLoad) {
    	this.forceLoad = forceLoad;
    }

	public File getExternalTFTPDir() {
    	return externalTFTPDir;
    }

	public void setExternalTFTPDir(File externalTFTPDir) {
    	this.externalTFTPDir = externalTFTPDir;
    }

	public long getWaitFor() {
    	return waitFor;
    }

	public void setWaitFor(long waitFor) {
    	this.waitFor = waitFor;
    }

	public boolean getProgressIndicator() {
    	return progressIndicator;
    }

	public void setProgressIndicator(boolean progressIndicator) {
    	this.progressIndicator = progressIndicator;
    }

	public boolean isQuiet() {
    	return quiet;
    }

	public void setQuiet(boolean quiet) {
    	this.quiet = quiet;
    }

	public boolean getIgnoreVendor() {
    	return ignoreVendor;
    }

	public void setIgnoreVendor(boolean ignoreVendor) {
    	this.ignoreVendor = ignoreVendor;
    }

	public boolean getIpv6() {
    	return ipv6;
    }

	public void setIpv6(boolean ipv6) {
    	this.ipv6 = ipv6;
    }

	@Override
	public String getTFTPAddr() {
		return getMyIP();
	}
	
	@Override
	public String getTFTPDir() {
		if (getExternalTFTPDir() == null) return null;
		
		return getExternalTFTPDir().getAbsolutePath();
	}
	
	@Override
	public void handleException(Exception e, String extraMessage, boolean mt) {
		System.err.printf("%s: %s\n", e.getMessage(), extraMessage);
		logger.debug("Exception thrown by firmware load process", e);
		logger.debug("Extra message: " + extraMessage);
		
		System.exit(1);
	}
	
	@Override
	public void saveSwitchState(SwitchConfig state) {
		// Do nothing
		// FUTURE: Save the state to a file?
	}
	
	@Override
	public void updateState(int state) {
		// Special case: S_CyclePower needs to happen whether or not we're being quiet
		if (state == S_APPLY_POWER) {
			List<String> doIPs = getDO_IPs();
			
			if (doIPs.size() > 0) {
				// Cycle power programmatically
				if (!isQuiet()) System.out.println("Powering on the switch");
				
				try {
					setPower(true);
				} catch (Exception e) {
					throw new RuntimeException("Unable to cycle power to the switch: " + e.getMessage(), e);
				}
			} else {
				// Ask the user to cycle power
				System.out.println("Please connect power to the switch");
			}
		}
		
		if (isQuiet()) return; // No status messages
		
		switch (state) {
			// Ethernet-only load
			case S_VERIFY_ETH_ONLY:
				System.out.println("Verifying that ethernet-only loading is enabled");
				break;
				
			case S_SET_FORCE:
				System.out.println("Forcing the switch to accept ethernet-only loads");
				break;
				
			case S_PROBE_SWITCH:
				System.out.println("Probing the switch");
				break;
				
			case S_EXTRACT_FILES:
				// Shared with Ethernet+Serial loading
				System.out.println("Extracting firmware images from the bundle");
				break;
				
			case S_DOWNLOAD_CONFIG:
				System.out.println("Downloading the current switch configuration");
				break;
				
			case S_UPGRADE_REQUEST:
				System.out.println("Requesting that the switch reboot and update its firmware");
				break;
				
			case S_RESTORE_CONFIG:
				System.out.println("Restoring the switch's configuration");
				break;
				
			case S_COMPLETE:
				System.out.println("Firmware load complete");
				break;

			// Ethernet + Serial load
			case S_APPLY_POWER:
				// This was already handled
				break;
			
			case S_ARCH:
				System.out.println("Probing the switch");
				break;
				
            case S_SETUP:
            	System.out.println("Preparing the switch");
            	break;
            	
			case S_LOAD_IMAGE:
				System.out.println("Uploading the firmware image");
				break;
				
			case S_FLASH_IMAGE:
				System.out.println("Writing the firmware image to NAND");
				break;
				
			case S_LOAD_BOOT_IMAGE:
				System.out.println("Uploading the boot image");
				break;
				
			case S_FLASH_BOOT_IMAGE:
				System.out.println("Writing the boot image to NAND");
				break;
				
			case S_LOAD_USER_IMAGE:
				System.out.println("Uploading the user image");
				break;
				
			case S_FLASH_USER_IMAGE:
				System.out.println("Writing the user image to NAND");
				break;
				
			case S_WAIT:
				System.out.println("Waiting for the switch to finish");
				break;
		}
	}

	@Override
    public void tftpProgress(float percentage) {
	    if (getProgressIndicator()) {
	    	if (percentage == -100.0) {
	    		System.out.println();
	    		return;
	    	}
	    	
	    	System.out.printf("\r%.1f%% Transferred", percentage);
	    }
    }
}

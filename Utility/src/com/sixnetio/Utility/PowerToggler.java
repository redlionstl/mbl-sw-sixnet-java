/*
 * PowerToggler.java
 *
 * Automatically toggle power output from a DO module for as long as the program runs.
 *
 * Jonathan Pearson
 * November 26, 2008
 *
 */

package com.sixnetio.Utility;

import java.io.IOException;
import java.io.PrintStream;
import java.util.concurrent.TimeoutException;

import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.util.Utils;

public class PowerToggler extends Thread {
	public static final String PROG_VERSION = "0.1.0";
	
	private static void usage(PrintStream out) {
		out.println("Usage: PowerToggler {-s <serial port>|-t <IP address>|-u <IP address>} -n <station number>");
		out.println("                    -c <DO channel number> -on <seconds> -off <seconds> [-start {on|off}]");
		out.println("  -s  Use serial communications through <serial port>");
		out.println("  -t  Use TCP communications through <IP address>");
		out.println("  -u  Use UDP communications through <IP address> (recommended)");
		out.println("  <station number> The number of the station; use 255 for any station");
		out.println("  -c  Provide the DO channel number that you want to toggle (0-based)");
		out.println("  -on  Set how many seconds you want the output to remain ON each cycle");
		out.println("  -off  Set how many seconds you want the output to remain OFF each cycle");
		out.println("  -start  Set the starting state (default ON)");
	}
	
	public static void main(String[] args) {
		String address = null;
		TransportMethod method = null;
		short station = -1;
		short channel = -1;
		int offSecs = -1;
		int onSecs = -1;
		boolean start = true; // Start on
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-s")) {
				address = args[++i];
				method = TransportMethod.Serial;
			} else if (args[i].equals("-t")) {
				address = args[++i];
				method = TransportMethod.TCP;
			} else if (args[i].equals("-u")) {
				address = args[++i];
				method = TransportMethod.UDP;
			} else if (args[i].equals("-n")) {
				station = Short.parseShort(args[++i]);
			} else if (args[i].equals("-c")) {
				channel = Short.parseShort(args[++i]);
			} else if (args[i].equals("-on")) {
				onSecs = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-off")) {
				offSecs = Integer.parseInt(args[++i]);
			} else if (args[i].equals("-start")) {
				i++;
				
				if (args[i].equalsIgnoreCase("on")) {
					start = true;
				} else if (args[i].equalsIgnoreCase("off")) {
					start = false;
				} else {
					System.err.println("Starting state must be 'on' or 'off'");
					usage(System.err);
					System.exit(1);
				}
			} else {
				System.err.println("Unknown argument: " + args[i]);
				usage(System.err);
				System.exit(1);
			}
		}
		
		if (method == null) {
			System.err.println("You must provide a connection method");
			usage(System.err);
			System.exit(1);
		}
		
		if (station == -1) {
			System.err.println("You must provide a station number");
			usage(System.err);
			System.exit(1);
		}
		
		if (channel == -1) {
			System.err.println("You must provide a channel to toggle");
			usage(System.err);
			System.exit(1);
		}
		
		if (offSecs == -1) {
			System.err.println("You must provide a number of seconds to remain OFF");
			usage(System.err);
			System.exit(1);
		}
		
		if (onSecs == -1) {
			System.err.println("You must provide a number of seconds to remain ON");
			usage(System.err);
			System.exit(1);
		}
		
		try {
    		PowerToggler toggler = new PowerToggler(address, method, station, channel, offSecs, onSecs, start);
    		toggler.start();
		} catch (IOException ioe) {
			Utils.debug(ioe);
			
			System.err.println("Error: " + ioe.getMessage());
			System.exit(1);
		}
	}
	
	private UDRLink activeHandler;
	private UDRLib udr;
	
	private short station;
	private short channel;
	private int offSecs;
	private int onSecs;
	private boolean[] currentState;
	
	public PowerToggler(String address, TransportMethod method, short station, short channel, int offSecs, int onSecs, boolean start) throws IOException {
		this.station = station;
		this.channel = channel;
		this.offSecs = offSecs;
		this.onSecs = onSecs;
		this.currentState = new boolean[] { start };
		
		// Make sure the message dispatcher is running
		MessageDispatcher.getDispatcher();
		
		// Connect
		activeHandler = new UDRLink(method, address);
		
		MessageDispatcher.getDispatcher().registerHandler(activeHandler);
		
		udr = new UDRLib(UDRMessage.STA_ANY, (byte)0);
		
		// Verify connectivity
		try {
			udr.waitForResponse(station, 5000);
		} catch (TimeoutException te) {
			MessageDispatcher.getDispatcher().unregisterHandler(activeHandler);
			
			activeHandler.close();
			
			activeHandler = null;
			udr = null;
			
			throw new IOException("Station is not responding", te);
		}
		
		// Set the starting state
		try {
			udr.putD(station, channel, (short)1, currentState);
		} catch (TimeoutException te) {
			throw new IOException("Unable to set the starting state", te);
		}
	}
	
	@Override
    public void run() {
		while (true) {
			if (currentState[0]) {
				Utils.sleep(onSecs * 1000);
			} else {
				Utils.sleep(offSecs * 1000);
			}
			
			// Toggle power
			currentState[0] = !currentState[0];
			
			try {
				System.out.println("Toggling power " + (currentState[0] ? "on" : "off"));
				
				udr.putD(station, channel, (short)1, currentState);
			} catch (Exception e) {
				Utils.debug(e);
				
				System.err.println("Error: " + e.getMessage());
				
				currentState[0] = !currentState[0]; // Try again
			}
		}
	}
}

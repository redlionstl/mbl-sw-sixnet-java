/*
 * TarDiff.java
 *
 * Searches two tarballs for differences.
 *
 * Jonathan Pearson
 * October 27, 2008
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.util.*;

import com.sixnetio.fs.tar.*;
import com.sixnetio.io.VirtualTerminal;

public class TarDiff
{
	private static class TarballInfo
	{
		public String name;
		public Tarball tarball;
		public boolean forceUnzip;
		public InputStream input;
		
		public TarballInfo(String tarballFileName, boolean forceUnzip)
		{
			this.name = tarballFileName;
			this.forceUnzip = forceUnzip;
		}
		
		@Override
		public boolean equals(Object obj)
		{
			if (!(obj instanceof TarballInfo))
				return false;
			
			TarballInfo tbi = (TarballInfo)obj;
			return (tbi.name.equals(name));
		}
		
		@Override
		public int hashCode()
		{
			return name.hashCode();
		}
	}
	
	private static boolean useColors = false;
	
	public static void main(String[] args)
	{
		List<TarballInfo> files = new LinkedList<TarballInfo>();
		
		try {
			// XXX: The body of this loop modifies the loop invariant
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-z")) {
					files.add(new TarballInfo(args[++i], true));
				}
				else if (args[i].equals("-f")) {
					files.add(new TarballInfo(args[++i], false));
				}
				else if (args[i].equals("-c")) {
					useColors = true;
				}
				else if (args[i].equals("-h")) {
					usage(System.out);
					return;
				}
				else {
					throw new Exception("Unrecognized argument: " + args[i]);
				}
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			usage(System.err);
			System.exit(1);
		}
		
		if (files.size() < 2) {
			System.err.println("Not enough files");
			usage(System.err);
			System.exit(1);
		}
		
		int stdInCount = 0;
		for (TarballInfo tbi : files) {
			if (tbi.name.equals("-")) {
				stdInCount++;
				tbi.input = System.in;
			}
			else {
				try {
					tbi.input = new FileInputStream(tbi.name);
				}
				catch (IOException ioe) {
					System.err.println("Unable to open '" + tbi.name + "': " +
					                   ioe.getMessage());
					System.exit(1);
				}
			}
		}
		
		if (stdInCount > 1) {
			System.err.println("Cannot diff stdin with itself");
			usage(System.err);
			System.exit(1);
		}
		
		// Put them into a map for easy access and a list that we can pass to
		// diff()
		Map<Tarball, TarballInfo> infoMap = new HashMap<Tarball, TarballInfo>();
		List<Tarball> tarballs = new LinkedList<Tarball>();
		
		for (TarballInfo tbi : files) {
			try {
				if (tbi.forceUnzip || tbi.name.toLowerCase().endsWith("gz")) {
					tbi.tarball = new GZippedTarball(tbi.input);
				}
				else {
					tbi.tarball = new Tarball(tbi.input);
				}
			}
			catch (IOException ioe) {
				System.err.println("Unable to untar '" + tbi.name + "': " +
				                   ioe.getMessage());
				System.exit(1);
			}
			finally {
				try {
					tbi.input.close();
					tbi.input = null;
				}
				catch (IOException ioe) {
					// Ignore it
				}
			}
			
			infoMap.put(tbi.tarball, tbi);
			tarballs.add(tbi.tarball);
		}
		
		List<TarballDifferenceGenerator.Difference> differences =
			TarballDifferenceGenerator.diff(tarballs);
		
		for (TarballDifferenceGenerator.Difference diff : differences) {
			String name1 = infoMap.get(diff.tb1).name;
			String name2 = infoMap.get(diff.tb2).name;
			
			switch (diff.type) {
				case Added:
					if (useColors) {
						VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.FGCyan);
					}
					
					System.out.printf("%s:(Missing) %s:%s\n", name1, name2, diff.path);
					
					if (useColors) {
						VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.ResetAll);
					}
					break;
				case ContentsChanged:
					if (diff.lineDiff == null) {
						if (useColors) {
							VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.FGCyan);
						}
						
						System.out.printf("%s:%s %s:%s Binary files differ\n", name1, diff.path, name2, diff.path);
						
						if (useColors) {
							VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.ResetAll);
						}
					}
					else {
						// FUTURE: It would be nice to print some context around
						// each difference
						// FUTURE: It would be nice to group changes into chunks
						// so changes made to the same area showed up together
						printTextDiffHeader1(name1, name2, diff.path);
						
						for (int i = 0; i < diff.lineDiff.length; i++) {
							String line = diff.lineDiff[i];
							if (line.startsWith("-")) {
								if (useColors) {
									VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.FGRed);
								}
								
								System.out.printf("%s\n", line);
								
								if (useColors) {
									VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.ResetAll);
								}
							}
						}
						
						printTextDiffHeader2(name1, name2, diff.path);
						
						for (String line : diff.lineDiff) {
							if (line.startsWith("+")) {
								if (useColors) {
									VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.FGGreen);
								}
								
								System.out.printf("%s\n", line);
								
								if (useColors) {
									VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.ResetAll);
								}
							}
						}
					}
					break;
				case Deleted:
					if (useColors) {
						VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.FGCyan);
					}
					
					System.out.printf("%s:%s %s:(Missing)\n", name1, diff.path, name2);
					
					if (useColors) {
						VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.ResetAll);
					}
					break;
				case DeviceChanged:
					if (useColors) {
						VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.FGCyan);
					}
					
					System.out.printf("%s:%s %s:%s Device node changed: '%s' to '%s'\n",
					                  name1, diff.path, name2, diff.path,
					                  diff.lineDiff[0], diff.lineDiff[1]);
					
					if (useColors) {
						VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.ResetAll);
					}
					break;
				case TargetChanged:
					if (useColors) {
						VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.FGCyan);
					}
					
					System.out.printf("%s:%s %s:%s Symlink target changed: '%s' to '%s'\n",
					                  name1, diff.path, name2, diff.path,
					                  diff.lineDiff[0], diff.lineDiff[1]);
					
					if (useColors) {
						VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.ResetAll);
					}
					break;
				case TypeChanged:
					if (useColors) {
						VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.FGCyan);
					}
					
					System.out.printf("%s:%s %s:%s Object type changed: '%s' to '%s'\n",
					                  name1, diff.path, name2, diff.path,
					                  diff.lineDiff[0], diff.lineDiff[1]);
					
					if (useColors) {
						VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.ResetAll);
					}
					break;
				
			}
		}
	}
	
	private static void printTextDiffHeader1(String tb1, String tb2, String fileName)
	{
		if (useColors) {
			VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.FGCyan);
		}
		
		System.out.printf("<<<<< %s:%s\n", tb1, fileName);
		
		if (useColors) {
			VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.ResetAll);
		}
	}
	
	private static void printTextDiffHeader2(String tb1, String tb2, String fileName)
	{
		if (useColors) {
			VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.FGCyan);
		}
		
		System.out.printf(">>>>> %s:%s\n", tb2, fileName);
		
		if (useColors) {
			VirtualTerminal.setAttributes(System.out, VirtualTerminal.DisplayAttribute.ResetAll);
		}
	}
	
	private static void usage(PrintStream out)
	{
		out.println("Usage: TarDiff [-c] {-f|-z} <tarball 1> {-f|-z} <tarball 2> [...]");
		out.println("  -c   Print diffs in color");
		out.println("  -f   Auto-detect the gzipped status of the following tarball by filename");
		out.println("  -z   Force unzip of the following tarball");
		out.println("At most one <tarball> may be '-' to use stdin");
	}
}

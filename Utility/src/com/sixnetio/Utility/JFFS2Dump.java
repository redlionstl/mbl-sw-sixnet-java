/*
 * JFFS2Dump.java
 *
 * Dumps a full directory listing of a JFFS2 filesystem image.
 *
 * Jonathan Pearson
 * September 5, 2008
 *
 */

package com.sixnetio.Utility;

import java.io.*;

import com.sixnetio.fs.jffs2.JFFS2;

public class JFFS2Dump {
	public static void main(String[] args) {
		if (args.length != 1) {
			usage(System.err);
			System.exit(1);
			return;
		}
		
		InputStream in;
		
		if (args[0].equals("-")) {
			in = System.in;
		} else {
    		try {
    			in = new FileInputStream(args[0]);
    		} catch (IOException ioe) {
    			System.err.println("Unable to open the file: " + ioe.getMessage());
    			System.exit(1);
    			return;
    		}
		}
		
		JFFS2 image;
		
		try {
    		image = new JFFS2(in);
		} catch (IOException ioe) {
			System.err.println("Unable to parse the image: " + ioe.getMessage());
			System.exit(1);
			return;
		} finally {
			try {
				in.close();
			} catch (IOException ioe) { }
		}
		
		System.out.println("Listing of " + args[0]);
		image.getRoot().dump(System.out);
	}
		
	private static void usage(PrintStream out) {
		out.println("JFFS2Dump - Dumps a file listing of a JFFS2 filesystem image");
		out.println("  (c)2008 SIXNET, written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: JFFS2Dump <image>");
		out.println("  <image> may be '-' for stdin");
	}
}

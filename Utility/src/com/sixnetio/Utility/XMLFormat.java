/*
 * XMLFormat.java
 *
 * Format an XML file neatly, with tabs and newlines.
 *
 * Jonathan Pearson
 * October 17, 2008
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.util.*;

import org.apache.log4j.*;
import org.jonp.xml.*;

import com.sixnetio.util.Utils;

public class XMLFormat {
	static {
		String props = Utils.getFirstExistingFile(new String[] {
				"log4j.properties",
				System.getProperty("user.home") + File.separator + ".xmlformat.log4j.properties"
		});
		
		if (props == null) {
			Logger.getRootLogger().setLevel(Level.OFF);
		} else {
			PropertyConfigurator.configureAndWatch(props);
		}
	}
	
	// HTML output color schemes
	private static class ColorScheme {
		public String symbols, // Like <, >, =, /, and sometimes ?
		              tags, // Tag names
		              attNames, // Attribute names
		              attValues, // Attribute values
		              text, // Text between open and close tags
		              bgColor; // Page background
		
		public ColorScheme(String symbols, String tags, String attNames, String attValues, String text, String bgColor) {
			this.symbols = symbols;
            this.tags = tags;
            this.attNames = attNames;
            this.attValues = attValues;
            this.text = text;
            this.bgColor = bgColor;
		}
	}
	
	// Read-only after static initialization
	private static final Map<String, ColorScheme> colorSchemes;
	
	static {
		colorSchemes = new LinkedHashMap<String, ColorScheme>();
		
		colorSchemes.put("darkOnLight",
		                 new ColorScheme("#000077", // Symbols
		                                 "#0000FF", // Tags
		                                 "#000077", // Attribute Names
		                                 "#770077", // Attribute Values
		                                 "#000000", // Text
		                                 "#FFFFFF")); // Background
		colorSchemes.put("lightOnDark",
		                 new ColorScheme("#6666AA",
		                                 "#2222FF",
		                                 "#6666AA",
		                                 "#AA00AA",
		                                 "#CCCC99",
		                                 "#000000"));
	}
	
	private static final int INDENTSIZE = 3; // Spaces per level of indent
	
	public static void main(String[] args) {
		boolean niceFormat = true;
		boolean htmlFormat = false;
		String fileName = null;
		String chosenScheme = colorSchemes.keySet().iterator().next(); // Grab the first one as the default
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-html")) {
				htmlFormat = true;
			} else if (args[i].equals("-scheme")) {
				chosenScheme = args[++i];
			} else if (args[i].equals("-n")) {
				niceFormat = false;
			} else if (args[i].equals("-h")) {
				usage(System.out);
				return;
			} else {
				fileName = args[i];
			}
		}
		
		if (fileName == null) {
			System.err.println("Must provide a file name");
			usage(System.err);
			System.exit(1);
			return;
		}
		
		if (!colorSchemes.containsKey(chosenScheme)) {
			System.err.println("Unknown color scheme");
			usage(System.err);
			System.exit(1);
			return;
		}
		
		LineNumberReader in;
		
		if (fileName.equals("-")) {
			in = new LineNumberReader(new InputStreamReader(System.in));
		} else {
    		try {
    			in = new LineNumberReader(new FileReader(fileName));
    		} catch (IOException ioe) {
    			System.err.println("Unable to open the file: " + ioe.getMessage());
    			System.exit(1);
    			return;
    		}
		}
		
		// Parse the XML file
		XML xml = new XML();
		List<String> errMsgs = new LinkedList<String>();
		Tag root;
		try {
    		root = xml.parseXML(in, false, false, errMsgs);
		} catch (IOException ioe) {
			System.err.println("Error parsing XML data around line " + in.getLineNumber() + ": " + ioe.getMessage());
			
			for (String msg : errMsgs) {
				System.err.println(msg);
			}
			
			System.exit(1);
			return;
		} finally {
			try {
				in.close();
			} catch (IOException ioe) { }
		}
		
		for (String msg : errMsgs) {
			System.err.println(msg);
		}
		
		// Write it out all pretty-like
		if (htmlFormat) {
			writeHtmlFormat(root, colorSchemes.get(chosenScheme), System.out);
		} else {
			try {
				xml.writeXMLStream(root, System.out, niceFormat);
			} catch (IOException ioe) {
				System.err.println("Error writing XML data to stdout: " + ioe.getMessage());
				System.exit(1);
				return;
			}
		}
	}
	
	public static void usage(PrintStream out) {
		out.println("XMLFormat - Nicely formats XML data");
		out.println("  (c)2008 SIXNET, written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: XMLFormat [-h] [-html] [-n] [-scheme <scheme name>] <XML file>");
		out.println("  -h  Output this message and exit");
		out.println("  -html  Output will be formatted, syntax-colored HTML");
		out.println("  <XML file>  May be '-' for stdin");
		out.println("  -n  Rather than reformatting nicely, reformats into a single line");
		out.println("  -scheme  Color scheme for HTML output, choose from:");
		
		boolean first = true;
		for (String schemeName : colorSchemes.keySet()) {
			if (first) {
				out.println(String.format("    %s (default)", schemeName));
				
				first = false;
			} else {
				out.println(String.format("    %s", schemeName));
			}
		}
	}
	
	private static void writeHtmlFormat(Tag tag, ColorScheme scheme, PrintStream out) {
		// Format the XML data as color-coded HTML
		out.println(String.format("<html><head><title>XML Data</title></head><body bgcolor='%s'><pre>", scheme.bgColor));
		
		// Start with the standard XML header
		printSymbols("&lt;?", scheme, out);
		printTagName("xml", scheme, out);
		out.print(" ");
		printAttName("version", scheme, out);
		printSymbols("=\"", scheme, out);
		printAttValue("1.0", scheme, out);
		printSymbols("\"?&gt;", scheme, out);
		out.println();
		
		writeHtmlData(tag, scheme, out, 0);
		
		out.println("</pre></body></html>");
	}
	
	private static void writeHtmlData(Tag tag, ColorScheme scheme, PrintStream out, int indentCount) {
		// Indent the tag
		char[] indentChars = new char[indentCount * INDENTSIZE];
		Arrays.fill(indentChars, ' ');
		String indent = new String(indentChars);
		
		out.print(indent);
		
		printSymbols("&lt;", scheme, out);
		printTagName(tag.getName(), scheme, out);
		
		// Figure out where the first attribute will be on the line (it gets the same line as the tag name)
		int baseLen = indent.length() + 2 + tag.getName().length(); // 2 is "<" and space after tag name
		
		// The rest of the attributes get their own lines, aligned with the first attribute
		char[] baseIndentChars = new char[baseLen];
		Arrays.fill(baseIndentChars, ' ');
		String baseIndent = new String(baseIndentChars);
		
		// Print out the attributes
		boolean first = true; // So we only print out newlines after the first one
		for (String attName : tag.getAttNames()) {
			String attValue = tag.getStringAttribute(attName);
			
			if (!first) {
				out.println();
				out.print(baseIndent);
			} else {
				out.print(" "); // Need a space between the tag name and the attribute name
				first = false;
			}
			
			printAttName(attName, scheme, out);
			printSymbols("=\"", scheme, out);
			printAttValue(attValue, scheme, out);
			printSymbols("\"", scheme, out);
		}
		
		// Print out the tag contents
		if (tag.countContents() > 0) {
			printSymbols("&gt;", scheme, out);
			out.println();
			for (Content c : tag) {
				if (c.isTag()) {
					writeHtmlData(c.getTag(), scheme, out, indentCount + 1);
				} else {
					char[] nextIndentChars = new char[(indentCount + 1) * INDENTSIZE];
					Arrays.fill(nextIndentChars, ' ');
					String nextIndent = new String(nextIndentChars);
					
					out.print(String.format("<font color='%s'>", scheme.text));
					
					StringTokenizer lineTok = new StringTokenizer(c.getString(), "\n");
					while (lineTok.hasMoreTokens()) {
						String line = lineTok.nextToken();
						out.print(String.format("%s%s", nextIndent, line));
						
						if (lineTok.hasMoreTokens()) out.println(); // Don't output an extra newline at the end
					}
					
					out.println("</font>"); // The newline goes after the closing 'font' tag
				}
			}
			
			out.print(indent);
			printSymbols("&lt;/", scheme, out);
			printTagName(tag.getName(), scheme, out);
			printSymbols("&gt;", scheme, out);
			out.println();
		} else {
			out.print(" ");
			printSymbols("/&gt;", scheme, out);
			out.println();
		}
	}
	
	private static void printSymbols(String s, ColorScheme scheme, PrintStream out) {
		colorText(s, scheme.symbols, out);
	}
	
	private static void printTagName(String s, ColorScheme scheme, PrintStream out) {
		colorText(s, scheme.tags, out);
	}
	
	private static void printAttName(String s, ColorScheme scheme, PrintStream out) {
		colorText(s, scheme.attNames, out);
	}
	
	private static void printAttValue(String s, ColorScheme scheme, PrintStream out) {
		colorText(s, scheme.attValues, out);
	}
	
	private static void colorText(String s, String color, PrintStream out) {
		out.print(String.format("<font color='%s'>%s</font>", color, s));
	}
}

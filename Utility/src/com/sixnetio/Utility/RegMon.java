/*
 * RegMon.java
 *
 * Monitors a range of discrete registers, displaying their values graphically
 * on the screen.
 *
 * Jonathan Pearson
 * May 26, 2010
 *
 */

package com.sixnetio.Utility;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.*;

import com.sixnetio.GUI.Suicide;
import com.sixnetio.Modbus.ModbusLink;
import com.sixnetio.Modbus.ModbusMessage;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.util.Utils;

public class RegMon
	extends Frame
{
	static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private static final void usage(PrintStream out)
	{
		out.println("Usage: RegMon [-r] {-mu|-mt|-us|-uu|-ut} <address> <start> <len> [...]");
		out.println("Monitors registers on <address> in range [<start>, <start> + <len>)");
		out.println(" -r   Disconnect/reconnect for each TCP exchange (for testing)");
		out.println(" -mu  Monitor over Modbus UDP");
		out.println(" -mt  Monitor over Modbus TCP");
		out.println(" -us  Monitor over UDR Serial");
		out.println(" -uu  Monitor over UDR UDP");
		out.println(" -ut  Monitor over UDR TCP");
	}
	
	public static void main(String[] args)
	{
		// Initialize the logger
		Logger.getRootLogger().setLevel(Level.OFF);
		PropertyConfigurator.configureAndWatch("log4j.properties", 5000);
		
		// Construct a new RegMon, but don't display it yet
		RegMon regmon = new RegMon();
		
		// Parse arguments
		try {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-mu")) {
					// Modbus UDP
					ModbusLink l = new ModbusLink(TransportMethod.UDP, args[++i]);
					int start = Integer.parseInt(args[++i]);
					int len = Integer.parseInt(args[++i]);
					regmon.register(new ModbusLib(l, ModbusMessage.ADDR_BROADCAST),
					                start, len);
				}
				else if (args[i].equals("-mt")) {
					// Modbus TCP
					ModbusLink l = new ModbusLink(TransportMethod.TCP, args[++i]);
					int start = Integer.parseInt(args[++i]);
					int len = Integer.parseInt(args[++i]);
					regmon.register(new ModbusLib(l, ModbusMessage.ADDR_BROADCAST),
					                start, len);
				}
				else if (args[i].equals("-us")) {
					// UDR Serial
					UDRLink l = new UDRLink(TransportMethod.Serial, args[++i]);
					int start = Integer.parseInt(args[++i]);
					int len = Integer.parseInt(args[++i]);
					regmon.register(new SimpleUDRLib(l, UDRMessage.STA_ANY, (byte)0),
					                start, len);
				}
				else if (args[i].equals("-uu")) {
					// UDR UDP
					UDRLink l = new UDRLink(TransportMethod.UDP, args[++i]);
					int start = Integer.parseInt(args[++i]);
					int len = Integer.parseInt(args[++i]);
					regmon.register(new SimpleUDRLib(l, UDRMessage.STA_ANY, (byte)0),
					                start, len);
				}
				else if (args[i].equals("-ut")) {
					// UDR TCP
					UDRLink l = new UDRLink(TransportMethod.TCP, args[++i]);
					int start = Integer.parseInt(args[++i]);
					int len = Integer.parseInt(args[++i]);
					regmon.register(new SimpleUDRLib(l, UDRMessage.STA_ANY, (byte)0),
					                start, len);
				}
				else if (args[i].equals("-r")) {
					regmon.setReconnect(true);
				}
				else if (args[i].equals("-h")) {
					usage(System.out);
					return;
				}
				else {
					throw new Exception("Unrecognized argument: " + args[i]);
				}
			}
			
			if (regmon.getRangeCount() == 0) {
				throw new Exception("You must specify at least one address and range");
			}
		}
		catch (Exception e) {
			if (e instanceof NumberFormatException) {
				System.err.println("Error parsing a number");
			}
			else {
				System.err.println(e.getMessage());
			}
			
			logger.debug(e);
			
			usage(System.err);
			System.exit(1);
		}
		
		regmon.setVisible(true);
		regmon.joinOnRanges();
		regmon.setVisible(false);
		regmon.dispose();
		
		new Suicide(); // Just in case that didn't kill everything
	}
	
	
	private static class RangeCanvas
		extends Canvas
	{
		private static final Color C_BACKGROUND = Color.WHITE;
		private static final Color C_ON = new Color(255, 0, 0);
		private static final Color C_OFF = new Color(92, 0, 0);
		private static final Color C_EMPTY = Color.BLACK;
		private static final Color C_FONT = Color.WHITE;
		
		public final int offset;
		public boolean[] vals;
		public final Object l_vals = new Object();
		private boolean initialized;
		
		public RangeCanvas(int offset)
		{
			this.offset = offset;
		}
		
		@Override
		public void update(Graphics graphics)
		{
			// Don't clear the screen (like super.update() does), it causes
			// flickering
			paint(graphics);
		}
		
		@Override
		public void paint(Graphics graphics)
		{
			if ( ! initialized) {
				createBufferStrategy(2);
				initialized = true;
			}
			
			int width = getWidth();
			int height = getHeight();
			
			// These are only used if vals != null, so don't worry about the 0
			int boxWidth = (width - 2) / 16;
			int boxHeight = 0;
			
			synchronized (l_vals) {
				if (vals != null) {
					// Leave a 1px border on above/below
					boxHeight = (height - 2) / ((vals.length + 15) >> 4);
				}
				
				do {
					do {
						Graphics g = getBufferStrategy().getDrawGraphics();
						
						// These location properties of strings will not change
						int sh = g.getFontMetrics().getHeight();
						int sy = (boxHeight >> 1) + (sh >> 2);
						
						// Fill in all white
						g.setColor(C_BACKGROUND);
						
						g.fillRect(0, 0, getWidth(), getHeight());
						
						if (vals != null) {
							// Draw the registers in rows of 16
							for (int i = 0; i < vals.length; i++) {
								if (vals[i]) {
									g.setColor(C_ON);
								}
								else {
									g.setColor(C_OFF);
								}
								
								// Leave a 1px border on all sides
								g.fillRect(1 + (i & 0x0f) * boxWidth,
								           1 + (i >> 4) * boxHeight,
								           boxWidth - 1, boxHeight - 1);
								
								// Center the register number in the box
								g.setColor(C_FONT);
								String msg = "" + (offset + i);
								int sw = g.getFontMetrics().charsWidth(msg.toCharArray(),
								                                       0,
								                                       msg.length());
								
								int sx = (boxWidth >> 1) - (sw >> 1);
								
								g.drawString(msg,
								             1 + (i & 0x0f) * boxWidth + sx,
								             1 + (i >> 4) * boxHeight + sy);
							}
							
							// Fill in any remaining boxes in the row with
							// empties
							g.setColor(C_EMPTY);
							int remainder = (vals.length + 15) & ~0x0f;
							for (int i = vals.length; i < remainder; i++) {
								g.fillRect(1 + (i & 0x0f) * boxWidth,
								           1 + (i >> 4) * boxHeight,
								           boxWidth, boxHeight);
							}
						}
						else {
							// One big empty box with some question marks in the
							// center
							g.setColor(C_EMPTY);
							g.fillRect(1, 1, width - 2, height - 2);
							
							g.setColor(C_FONT);
							String msg = "???";
							int sw = g.getFontMetrics().charsWidth(msg.toCharArray(),
							                                       0,
							                                       msg.length());
							
							int sx = (width >> 1) - (sw >> 1);
							sy = (height >> 1) + (sh >> 1);
							
							g.drawString(msg, 1 + sx, 1 + sy);
						}
						
						g.dispose();
					} while (getBufferStrategy().contentsRestored());
					
					getBufferStrategy().show();
				} while (getBufferStrategy().contentsLost());
			}
		}
	}
	
	private class Range
		extends Thread
	{
		public final MessageLib lib;
		public final short start;
		public final short len;
		public final RangeCanvas canvas;
		
		public Range(MessageLib lib, int start, int len)
		{
			super("Range for " + lib.getLink().getDescription());
			
			this.lib = lib;
			
			if (start < Short.MIN_VALUE || start > Short.MAX_VALUE) {
				throw new IllegalArgumentException("Value out of range: " +
				                                   start);
			}
			
			if (len < Short.MIN_VALUE || len > Short.MAX_VALUE) {
				throw new IllegalArgumentException("Value out of range: " +
				                                   len);
			}
			
			this.start = (short)start;
			this.len = (short)len;
			
			this.canvas = new RangeCanvas(start);
			
			// Width is 16 columns * 17px per column (16px box + 1px border),
			// plus a 1px border before the first and after the last
			// Height is the same, except the number of rows depends on the len
			// parameter
			canvas.setPreferredSize(new Dimension(16 * 17 + 2,
			                                      (len + 15) / 16 * 17 + 2));
		}
		
		@Override
		public void run()
		{
			while (running) {
				try {
					if (reconnect) {
						lib.getLink().connect();
						lib.restartReceiver();
					}
					
					boolean[] vals = lib.getDI(start, len);
					
					if (reconnect) {
						lib.getLink().close();
					}
					
					synchronized (canvas.l_vals) {
						canvas.vals = vals;
					}
					
					canvas.repaint();
				}
				catch (IOException ioe) {
					logger.warn(String.format("Error retrieving DIs [%d, %d] from %s",
					                          start, start + len - 1,
					                          lib.getLink().getDescription()),
					            ioe);
					
					// Repaint as empty
					synchronized (canvas.l_vals) {
						canvas.vals = null;
					}
					
					canvas.repaint();
					
					// Stop trying to access the device, it's broken
					running = false;
				}
				catch (TimeoutException te) {
					logger.warn("Timed out retrieving DIs from " +
					            lib.getLink().getDescription());
				}
				
				Utils.sleep(500);
			}
			
			lib.getLink().close();
		}
	}
	
	private java.util.List<Range> ranges;
	private Panel main;
	
	boolean reconnect = false; // Reconnect for every query
	
	boolean running = true;
	
	/**
	 * Construct a new RegMon register monitor. It will not be visible until you
	 * call {@link #setVisible(boolean)}, and it will not contain anything until
	 * you add ranges with {@link #register(MessageLib, int, int)}.
	 */
	public RegMon()
	{
		super("Register Monitor");
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e)
			{
				if (running) {
					// First click - graceful shutdown
					logger.info("Setting 'running' to false");
					running = false;
				}
				else {
					// Second click - immediate shutdown
					logger.warn("'running' already false, killing JVM");
					System.exit(0);
				}
			}
		});
		
		ranges = new LinkedList<Range>();
		
		setLayout(new BorderLayout());
		
		main = new Panel();
		main.setLayout(new GridLayout(0, 1));
		
		ScrollPane sp = new ScrollPane(ScrollPane.SCROLLBARS_AS_NEEDED);
		sp.add(main);
		add(sp, BorderLayout.CENTER);
		
		setPreferredSize(new Dimension(800, 600));
	}
	
	/**
	 * Add a range of registers to be monitored.
	 * 
	 * @param lib The library that can be used to read those registers.
	 * @param start The starting register number.
	 * @param len The number of registers to monitor.
	 */
	public void register(MessageLib lib, int start, int len)
	{
		Range r = new Range(lib, start, len);
		
		synchronized (ranges) {
			ranges.add(r);
		}
		
		r.start();
		
		// Add to the display
		Panel p = new Panel();
		p.setLayout(new BorderLayout());
		
		p.add(new Label(lib.getLink().getDescription()), BorderLayout.WEST);
		p.add(r.canvas, BorderLayout.CENTER);
		
		main.add(p);
		
		pack();
		r.canvas.repaint();
	}
	
	/** Check whether to reconnect on every query. */
	public boolean getReconnect()
	{
		return reconnect;
	}
	
	/** Set whether to reconnect on every query. This is mainly for testing. */
	public void setReconnect(boolean reconnect)
	{
		this.reconnect = reconnect;
	}
	
	/** Get the number of ranges specified. */
	public int getRangeCount()
	{
		synchronized (ranges) {
			return ranges.size();
		}
	}
	
	/**
	 * Block until all ranges have stopped monitoring, or until the thread is
	 * interrupted.
	 * 
	 * @return True if all ranges have stopped, false if the thread was
	 *   interrupted before all ranges could be checked.
	 */
	public boolean joinOnRanges()
	{
		synchronized (ranges) {
			for (Range r : ranges) {
				try {
					r.join();
				}
				catch (InterruptedException ie) {
					logger.warn("Interrupted", ie);
					return false;
				}
			}
		}
		
		return true;
	}
}

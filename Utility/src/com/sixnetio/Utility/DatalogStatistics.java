/*
 * DatalogStastics.java
 *
 * Generates some basic statistics for a datalog file.
 *
 * Jonathan Pearson
 * April 23, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.util.*;

import org.apache.log4j.*;
import org.jonp.xml.Tag;

import com.sixnetio.Automation.IOPoint;
import com.sixnetio.Utility.LogPlayer.LogEntry;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class DatalogStatistics {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public static final String PROG_VERSION = "0.1.0";
	
	static {
		String propsFile = Utils.getFirstExistingFile(new String[] {
			"log4j.properties",
			System.getProperty("user.home") + File.separator + "datalogstatistics.log4j.properties"
		});
		
		if (propsFile != null) {
			PropertyConfigurator.configure(propsFile);
		} else {
			Logger.getRootLogger().setLevel(Level.OFF);
		}
	}
	
	private static void usage(PrintStream out) {
		out.println("IOSimulator v. " + PROG_VERSION + " (c) 2009 SIXNET");
		out.println("  Written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: DatalogStatistics -m <iomap> -l <log file> [-p] [-h]");
		out.println("-m <iomap>     Provide the IO map XML file to map tags onto I/O points");
		out.println("-l <log file>  Provide the log file for interpolation of I/O points");
		out.println("-p             Purge from the dataset all I/O points without datalog values");
		out.println("-h             Display this message and exit");
	}
	
	public static void main(String[] args) {
		DatalogStatistics stats = new DatalogStatistics();
		
		boolean haveMap = false;
		boolean haveLog = false;
		
		try {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-m")) {
					stats.loadIOMap(args[++i]);
					haveMap = true;
				} else if (args[i].equals("-l")) {
					stats.loadDatalog(args[++i]);
					haveLog = true;
				} else if (args[i].equals("-p")) {
					stats.setPurge(true);
				} else if (args[i].equals("-h")) {
					usage(System.out);
					return;
				} else {
					throw new Exception("Unrecognized argument: " + args[i]);
				}
			}
			
			if (!haveMap) {
				throw new Exception("IOMap is necessary");
			} else if (!haveLog) {
				throw new Exception("Datalog is necessary");
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			usage(System.err);
			
			logger.error(e);
			
			System.exit(1);
			return;
		}
		
		stats.augmentIOMap();
		stats.dumpIOMap(System.out);
	}
	
	public static class StatisticalIOPoint extends IOPoint {
		// 0 to 1 for discretes
		// full short range for analogs
		// full int range for longs
		// full float range for floats
		public double min;
		public double max;
		
		// Average over the known values
		public double mean;
		
		// Standard deviation over the known values
		public double deviation;
		
	    public StatisticalIOPoint(DataType dataType, Direction direction, short address) {
		    super(dataType, direction, address);
	    }
	    
	    public StatisticalIOPoint(String tagName, DataType dataType, Direction direction,
	            short address) {
		    super(tagName, dataType, direction, address);
	    }
	    
	    public StatisticalIOPoint(Tag tag) {
		    super(tag);
		    
	    	min = tag.getDoubleContents("min", 0);
	    	max = tag.getDoubleContents("max", 0);
	    	
		    mean = tag.getDoubleContents("mean", 0);
		    deviation = tag.getDoubleContents("deviation", 0);
	    }
	    
	    @Override
	    public Tag toXML() {
	    	Tag root = super.toXML();
	    	Tag temp;
	    	
	    	root.addContent(temp = new Tag("min"));
	    	temp.addContent("" + min);
	    	
	    	root.addContent(temp = new Tag("max"));
	    	temp.addContent("" + max);
	    	
	    	root.addContent(temp = new Tag("mean"));
	    	temp.addContent("" + mean);
	    	
	    	root.addContent(temp = new Tag("deviation"));
	    	temp.addContent("" + deviation);
	    	
	    	return root;
	    }
    }
	
	private Map<String, StatisticalIOPoint> iomap;
	private List<LogEntry> logEntries; // Read-only, except in loadDatalog, a synchronized function
	private boolean purge = false;
	
	public DatalogStatistics() { }
	
	public void loadIOMap(String path) throws IOException {
		iomap = new Hashtable<String, StatisticalIOPoint>();
		
		Tag tag = Utils.xmlParser.parseXML(path, false, true);
		
		if (!tag.getName().equals("iomap")) throw new IOException("Not an IOMap: " + path);
		
		synchronized (iomap) {
	        for (Tag iopoint : tag.getEachTag("", "iopoint")) {
		        StatisticalIOPoint point = new StatisticalIOPoint(iopoint);
		        
		        if (point.tagName != null) {
			        iomap.put(point.tagName, point);
		        } else {
			        iomap.put(point.toString(), point);
		        }
	        }
        }
	}
	
	public synchronized void loadDatalog(String path) throws IOException {
		logEntries = new LinkedList<LogEntry>();
		
		LineNumberReader in = new LineNumberReader(new FileReader(path));
		
		try {
    		// Read and discard the first two lines
    		in.readLine();
    		in.readLine();
    		
    		// Read column headers from this line
    		CSVParser parser = new CSVParser(in);
    		parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
    		
    		// Read each line
    		while (parser.next()) {
    			parser.dumpWarnings(System.err);
    			
    			long timestamp = Long.parseLong(parser.getColumn("SIXLOG Timestamp")) * 1000; // It's in seconds
    			
    			LogEntry entry = new LogEntry(timestamp);
    			for (String colName : parser.getColumns()) {
    				entry.values.put(colName, parser.getColumn(colName));
    			}
    			
    			logEntries.add(entry);
    		}
		} finally {
			in.close();
		}
	}
	
	public void setPurge(boolean purge) {
		this.purge = purge;
	}
	
	public boolean getPurge() {
		return purge;
	}
	
	public void augmentIOMap() {
		synchronized (iomap) {
    		if (getPurge()) {
        		// Remove from the IO map all points without datalog values
    	        iomap.keySet().retainAll(logEntries.get(0).values.keySet());
    		}
    		
		    // Loop through the IO points
	        for (Map.Entry<String, StatisticalIOPoint> iomapEntry : iomap.entrySet()) {
		        StatisticalIOPoint point = iomapEntry.getValue();
		        
		        if (point.tagName == null) continue; // Not going to find the point in the log
		        
		        // Loop through the log, pulling out each value for this point
		        Vector<Double> values = new Vector<Double>(logEntries.size());
		        for (LogEntry logEntry : logEntries) {
			        // Get the value for this point
			        String text = logEntry.values.get(point.tagName).trim();
			        if (text == null) continue;
			        
			        double value;
			        
			        switch (point.dataType) {
				        case Analog:
					        if (text.indexOf("Deg") != -1) {
						        value =
						                (int)(Float
						                           .parseFloat(text.substring(0, text.indexOf(' '))) * 10);
					        } else {
						        value = Integer.parseInt(text);
					        }
					        break;
				        case Discrete:
					        value = (Integer.parseInt(text) == 0 ? 0 : 1);
					        break;
				        case Float:
					        value = Float.parseFloat(text);
					        break;
				        case Long:
					        value = Integer.parseInt(text);
					        break;
				        
				        default:
					        throw new RuntimeException("Unimplemented case statement for " +
					                                   point.dataType.name());
			        }
			        
			        values.add(value);
		        }
		        
		        // Loop through the samples to compute statistics
		        // Set up some basics so we don't use bad initial data
		        double total = 0.0;
		        point.min = values.get(0);
		        point.max = values.get(0);
		        
		        for (double value : values) {
			        total += value;
			        
			        if (point.min > value) point.min = value;
			        if (point.max < value) point.max = value;
		        }
		        
		        // Compute the mean
		        point.mean = total / values.size();
		        
		        // Compute the standard deviation
		        total = 0.0;
		        for (double value : values) {
			        double temp = value - point.mean;
			        total += temp * temp;
		        }
		        
		        point.deviation = Math.sqrt(total / values.size());
	        }
        }
	}
	
	public void dumpIOMap(PrintStream out) {
		Tag tag = new Tag("iomap");
		
		synchronized (iomap) {
	        for (Map.Entry<String, StatisticalIOPoint> iomapEntry : iomap.entrySet()) {
		        tag.addContent(iomapEntry.getValue().toXML());
	        }
        }
		
		try {
			tag.toStream(out);
		} catch (IOException ioe) {
			throw new RuntimeException("Unexpected IOException writing to stdout", ioe);
		}
	}
}

/*
 * BundleExtract.java
 *
 * Extracts a given file from a firmware bundle and dumps it to stdout.
 *
 * Jonathan Pearson
 * September 5, 2008
 *
 */

package com.sixnetio.Utility;

import java.io.*;

import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.fs.vfs.VFile;

public class BundleExtract {
	public static void main(String[] args) {
		if (args.length != 3 && args.length != 4) {
			usage(System.err);
			System.exit(1);
			return;
		}
		
		int arch, file;
		String type = null;
		
		arch = FirmwareBundle.stringToArch(args[1]);
		file = FirmwareBundle.stringToType(args[2]);
		
		if (args.length == 4) {
			type = args[3];
		}
		
		InputStream in;
		
		try {
			in = new FileInputStream(args[0]);
		} catch (IOException ioe) {
			System.err.println("Unable to open the file: " + ioe.getMessage());
			System.exit(1);
			return;
		}
		
		FirmwareBundle bundle;
		
		try {
			bundle = new FirmwareBundle(in);
		} catch (IOException ioe) {
			System.err.println("Unable to parse the bundle: " + ioe.getMessage());
			System.exit(1);
			return;
		} finally {
			try {
				in.close();
			} catch (IOException ioe) { }
		}
		
		VFile node = bundle.find(arch, file, type);
		
		if (node == null) {
			System.err.println("Unable to find file");
			System.exit(1);
		}
		
		byte[] data = node.getData();
		try {
			System.out.write(data);
		} catch (IOException ioe) {
			System.err.println("Unable to write to stdout: " + ioe.getMessage());
			System.exit(1);
		}
	}
		
	private static void usage(PrintStream out) {
		out.println("BundleExtract - Extracts files from a firmware bundle");
		out.println("  (c)2008 SIXNET, written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: BundleExtract <bundle> <arch> <file> [<type>]");
		out.println("  <bundle> must be a file");
	}
}

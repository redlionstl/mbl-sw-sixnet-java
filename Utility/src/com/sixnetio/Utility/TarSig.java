/*
 * TarSig.java
 *
 * Generates a unique MD5-based signature for a tarball that is independent
 * of the timestamps contained within it. This is the signature used by OEMTest.
 *
 * Jonathan Pearson
 * September 10, 2008
 *
 */

package com.sixnetio.Utility;

import com.sixnetio.fs.tar.*;

import java.io.*;

public class TarSig {
	public static void main(String[] args) {
		if (args.length < 1 || args.length > 2) {
			usage(System.err);
			System.exit(1);
			return;
		}
		
		int argIndex = 0;
		boolean forceGzip = false;
		
		if (args[argIndex].equals("-z")) {
			forceGzip = true;
			argIndex++;
		}
		
		InputStream in;
		
		if (args[argIndex].equals("-")) {
			in = System.in;
		} else {
    		try {
    			in = new FileInputStream(args[argIndex]);
    		} catch (IOException ioe) {
    			System.err.println("Unable to open the file: " + ioe.getMessage());
    			System.exit(1);
    			return;
    		}
		}
		
		Tarball tarball;
		
		try {
			if (forceGzip || args[argIndex].toLowerCase().endsWith("gz")) {
				tarball = new GZippedTarball(in);
			} else {
				tarball = new Tarball(in);
			}
		} catch (IOException ioe) {
			System.err.println("Unable to un-tar the file: " + ioe.getMessage());
			System.exit(1);
			return;
		}
		
		String md5 = tarball.generateMD5();
		System.out.println(md5);
	}
	
	private static void usage(PrintStream out) {
		out.println("TarSig - Generates a MD5-based signature for a tarball that does not include");
		out.println("  time stamps, so rebuilding the tarball will generate the same signature");
		out.println("  (c)2008 SIXNET, written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: TarSig [-z] <tarball>");
		out.println("  -z means force unzip (useful for stdin)");
		out.println("  <tarball> may be '-' for stdin");
		out.println("  unzip is automatic for files ending with 'gz'");
	}
}

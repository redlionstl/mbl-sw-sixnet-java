/*
 * RemapSwitch.java
 *
 * Re-maps a switch to use the dual-image firmware loading method. Use the
 * Java firmware loader after this to load firmware onto the re-mapped switch.
 *
 * Jonathan Pearson
 * August 7, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.IOException;
import java.io.PrintStream;
import java.util.concurrent.TimeoutException;

import com.sixnetio.COM.ComPort;
import com.sixnetio.COM.UBootCommunicator;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;

public class RemapSwitch {
	public static final String PROG_VERSION = "0.1.0";
	
	private static void usage(PrintStream out) {
		out.println("RemapSwitch v. " + PROG_VERSION + " Copyright 2009 Sixnet");
		out.println("  Written by Jonathan Pearson (Jon.Pearson@sixnet.com)");
		
		out.println("Usage: RemapSwitch -s <serial port> [-A <addr> -D <point>]");
		out.println(" -s  Specify the path to the serial port for communicating with the switch");
		out.println(" -A  Specify the address of a DO module for power toggling");
		out.println(" -D  Specify the DO point (1-based) that controls power to the switch");
		out.println("       If no DO address/point is specified, you will be prompted to cycle power");
	}
	
	public static void main(String[] args) throws IOException, TimeoutException {
		// Parse command-line arguments
		String serial = null;
		String doAddr = null;
		Short doPoint = null;
		
		try {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-s")) {
					serial = args[++i];
				} else if (args[i].equals("-A")) {
					doAddr = args[++i];
				} else if (args[i].equals("-D")) {
					try {
						doPoint = Short.parseShort(args[++i]);
					} catch (NumberFormatException nfe) {
						throw new Exception("Unable to parse '" + args[i] + "' as a Short");
					}
				} else if (args[i].equals("-h")) {
					usage(System.out);
					return;
				} else {
					throw new Exception("Unrecognized argument: " + args[i]);
				}
			}
			
			if (serial == null) {
				throw new Exception("-s argument required");
			}
			
			if (doAddr == null ^ doPoint == null) {
				throw new Exception("-A and -D must be provided together or not at all");
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			usage(System.err);
			System.exit(1);
			return;
		}
		
		// Power off the switch
		UDRLib udr = null;
		
		if (doAddr != null) {
			MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();
			UDRLink handler = new UDRLink(TransportMethod.UDP, doAddr);
			dispatcher.registerHandler(handler);
			
			udr = new UDRLib(UDRMessage.STA_ANY, (byte)0);
			
			udr.putD(UDRMessage.STA_ANY, doPoint, (short)1, new boolean[] { false });
		} else {
			System.out.println("Please disconnect power from the switch and press Enter");
			System.console().readLine();
		}
		
		// Start the U-Boot catcher
		UBootCommunicator uboot = new UBootCommunicator(serial, 9600, 8, ComPort.PARITY_NONE, ComPort.STOPBITS_1);
		uboot.startCatch();
		
		// Power on and wait for the catch
		if (udr != null) {
			udr.putD(UDRMessage.STA_ANY, doPoint, (short)1, new boolean[] { true });
		} else {
			System.out.println("Please connect power to the switch and press Enter");
			System.console().readLine();
		}
		
		uboot.waitForCatch(60000); // Wait for up to one minute
		
		// Start the keep-alive thread while we update the environment
		uboot.startKeepAlive();
		
		// Set up the U-Boot environment
		uboot.setVariable("layoutVersion", "2");
		uboot.setVariable("mrootargs.1", "set bootargs $(bootargs) root=/dev/mtdblock5 $(mtdparts.linux) rootflags=noatime rootfstype=jffs2 ip=off");
		uboot.setVariable("mrootargs.2", "set bootargs $(bootargs) root=/dev/mtdblock7 $(mtdparts.linux) rootflags=noatime rootfstype=jffs2 ip=off");
		uboot.setVariable("mtdparts.1", "mtdparts=mem.0:2M@4M(boot);sxni9260.0:2M@1M(boot),13824K(root)");
		uboot.setVariable("mtdparts.2", "mtdparts=mem0:2M@4M(boot);sxni9260.0:2M@16896K(boot),13824K(root)");
		// New large page nand uses a different layout for U-Boot only; linux doesn't change.

		if (uboot.getNandSize() == 128) {
			uboot.setVariable("mtdparts.linux", "mtdparts=sxni9260.0:256K@0(u-boot),128K(environment1),128K(environment2),512K(config),2M(boot1),13824K(root1),2M(boot2),13824K(root2)");
		}
		else {
			uboot.setVariable("mtdparts.linux", "mtdparts=sxni9260.0:320K@0(u-boot),128K(environment),64K(reserved),512K(config),2M(boot1),13824K(root1),2M(boot2),13824K(root2)");
		}
		
		uboot.setVariable("activeInstall", "1");
		uboot.setVariable("mtdparts", "$(mtdparts.1)");
		uboot.setVariable("mrootargs", "$(mrootargs.1)");
		
		uboot.flushVariables();
		
		// Power off so the switch doesn't try to boot
		if (udr != null) {
			udr.putD(UDRMessage.STA_ANY, doPoint, (short)1, new boolean[] { false });
		} else {
			System.out.println("Please disconnect power from the switch and press Enter");
			System.console().readLine();
		}
		
		uboot.stopKeepAlive();
		uboot.close();
		
		System.out.println("U-Boot has been updated and is ready to take a firmware load");
	}
}

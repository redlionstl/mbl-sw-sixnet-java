package com.sixnetio.Utility;

import java.awt.GridLayout;
import java.awt.event.*;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.TimeoutException;

import javax.swing.*;

import org.apache.log4j.*;

import com.sixnetio.Station.SimpleUDRLib;
import com.sixnetio.Station.TransportMethod;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.util.Utils;

/**
 * A utility program that displays a window with the I/O values from a remote
 * device. Similar to the I/O Toolkit's tool, but more lightweight.
 */
public class IOMonitor
    extends JFrame
    implements Runnable
{
    static final Logger log = Logger.getLogger(Utils.thisClassName());

    public static final String PROG_VERSION = "0.1.1";

    private static void usage(PrintStream out)
    {
        out
            .println("Usage: IOMonitor {-s <serial port>|-t <IP address>|-u <IP address>}");
        out
            .println("                  -n <station number> [-ai <ai>] [-ao <ao>] [-di <di>] [-do <do>]");
        out.println("  -s  Use serial communications through <serial port>");
        out.println("  -t  Use TCP communications through <IP address>");
        out.println("  -u  Use UDP communications through <IP address>");
        out
            .println("  <station number> The number of the station; use 255 for any station");
        out.println("  <ai> NNumber of AI points (default -1 asks the device)");
        out.println("  <ao> Number of AO points (default -1 asks the device)");
        out.println("  <di> Number of DI points (default -1 asks the device)");
        out.println("  <do> Number of DO points (default -1 asks the device)");
    }

    public static void main(String[] args)
    {
        Logger.getRootLogger().setLevel(Level.OFF);
        PropertyConfigurator.configure("log4j.properties");

        String address = null;
        TransportMethod method = null;
        short station = -1;

        short ais = -1;
        short aos = -1;
        short dis = -1;
        short dos = -1;

        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-s")) {
                address = args[++i];
                method = TransportMethod.Serial;
            }
            else if (args[i].equals("-t")) {
                address = args[++i];
                method = TransportMethod.TCP;
            }
            else if (args[i].equals("-u")) {
                address = args[++i];
                method = TransportMethod.UDP;
            }
            else if (args[i].equals("-n")) {
                station = Short.parseShort(args[++i]);
            }
            else if (args[i].equals("-ai")) {
                ais = Short.parseShort(args[++i]);
            }
            else if (args[i].equals("-ao")) {
                aos = Short.parseShort(args[++i]);
            }
            else if (args[i].equals("-di")) {
                dis = Short.parseShort(args[++i]);
            }
            else if (args[i].equals("-do")) {
                dos = Short.parseShort(args[++i]);
            }
            else {
                System.err.println("Unknown argument: " + args[i]);
                usage(System.err);
                System.exit(1);
            }
        }

        if (method != null && station == -1) {
            System.err
                .println("You must provide a station number with the address");
            usage(System.err);
            System.exit(1);
        }

        try {
            IOMonitor iomon =
                new IOMonitor(address, method, station, ais, aos, dis, dos);
            Thread th = new Thread(iomon);
            th.start();
        }
        catch (IOException ioe) {
            log.error("Unable to connect", ioe);

            System.err.println("Unable to connect to the specified device: " +
                               ioe.getMessage());
            System.exit(1);
        }
    }

    final short station;
    final SimpleUDRLib udr;

    final short aicount;
    final short aocount;
    final short dicount;
    final short docount;

    final JLabel[] aidisp;
    final JLabel[] aodisp;
    final JLabel[] didisp;
    final JLabel[] dodisp;

    short[] aivals;
    short[] aovals;
    boolean[] divals;
    boolean[] dovals;

    boolean running;

    public IOMonitor(String _address, TransportMethod _method, short _station,
                     short _aicount, short _aocount, short _dicount,
                     short _docount)
        throws IOException
    {
        super("IOMonitor - " + _address);
        station = _station;

        UDRLink link = new UDRLink(_method, _address);
        udr = new SimpleUDRLib(link, _station, (byte)0);

        // Get the number of each data type
        try {
            if (_aicount != -1) {
                aicount = _aicount;
            }
            else {
                aicount = udr.nio(UDRMessage.T_D_AIN);
            }

            if (_aocount != -1) {
                aocount = _aocount;
            }
            else {
                aocount = udr.nio(UDRMessage.T_D_AOUT);
            }

            if (_dicount != -1) {
                dicount = _dicount;
            }
            else {
                dicount = udr.nio(UDRMessage.T_D_DIN);
            }

            if (_docount != -1) {
                docount = _docount;
            }
            else {
                docount = udr.nio(UDRMessage.T_D_DOUT);
            }

            aivals = udr.getAI((short)0, aicount);
            aovals = udr.getAO((short)0, aocount);
            divals = udr.getDI((short)0, dicount);
            dovals = udr.getDO((short)0, docount);

            aidisp = new JLabel[aicount];
            aodisp = new JLabel[aocount];
            didisp = new JLabel[dicount];
            dodisp = new JLabel[docount];

            populateLabels();
        }
        catch (TimeoutException te) {
            throw new IOException("Timed out communicating with station: " +
                                  te.getMessage(), te);
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e)
                    {
                        running = false;
                        setVisible(false);
                    }
                });

                setLayout(new GridLayout(2, 2));

                createPanel("Analog In", "AX", aidisp);
                createPanel("Discrete In", "X", didisp);
                createPanel("Analog Out", "AY", aodisp);
                createPanel("Discrete Out", "Y", dodisp);

                pack();
                setVisible(true);
            }
        });
    }

    public void run()
    {
        running = true;

        while (running) {
            try {
                try {
                    aivals = udr.getAI((short)0, aicount);
                    aovals = udr.getAO((short)0, aocount);
                    divals = udr.getDI((short)0, dicount);
                    dovals = udr.getDO((short)0, docount);
                }
                catch (TimeoutException te) {
                    throw new IOException("Timed out waiting for response", te);
                }
            }
            catch (IOException ioe) {
                log.error("Error reading from station", ioe);
                JOptionPane.showMessageDialog(this,
                    "Error reading from station: " + ioe.getMessage(), "Error",
                    JOptionPane.ERROR_MESSAGE);
                running = false;
                continue;
            }

            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run()
                    {
                        populateLabels();
                    }
                });
            }
            catch (InterruptedException ie) {
                // Ignore it
            }
            catch (InvocationTargetException ite) {
                log.error("Failed to update display", ite);
                JOptionPane.showMessageDialog(this,
                    "Failed to update display: " + ite.getMessage(), "Error",
                    JOptionPane.ERROR_MESSAGE);
                running = false;
                continue;
            }

            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException ie) {
                // Ignore it; only important if runnable is set to false
            }
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                setVisible(false);
                dispose();
            }
        });
    }

    void createPanel(String title, String prefix, JLabel[] disp)
    {
        JPanel p = new JPanel();
        p.setBorder(BorderFactory.createTitledBorder(title));
        p.setLayout(new GridLayout(0, 2));
        for (int i = 0; i < disp.length; i++) {
            p.add(new JLabel(String.format("%s %d", prefix, i)));
            p.add(disp[i]);
        }

        add(new JScrollPane(p));
    }

    void populateLabels()
    {
        populateAnalogs(aivals, aidisp, false);
        populateAnalogs(aovals, aodisp, true);
        populateDiscretes(divals, didisp, false);
        populateDiscretes(dovals, dodisp, true);
    }

    private void populateAnalogs(short[] vals, JLabel[] disp, boolean output)
    {
        for (int i = 0; i < vals.length; i++) {
            if (disp[i] == null) {
                disp[i] = new JLabel();

                if (output) {
                    // TODO: Construct a new AnalogUpdater
                }
            }

            disp[i].setText(String.format("%,d", vals[i]));
        }
    }

    private void populateDiscretes(boolean[] vals, JLabel[] disp, boolean output)
    {
        for (int i = 0; i < vals.length; i++) {
            if (disp[i] == null) {
                disp[i] = new JLabel();

                if (output) {
                    new DiscreteUpdater(disp[i], UDRMessage.T_D_DOUT, (short)i);
                }
            }

            disp[i].setText(vals[i] ? "On" : "Off");
        }
    }

    private class DiscreteUpdater
        implements MouseListener
    {
        private final byte type;
        private final short index;

        public DiscreteUpdater(JComponent cmp, byte _type, short _index)
        {
            type = _type;
            index = _index;

            cmp.addMouseListener(this);
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseClicked(MouseEvent e)
        {
            try {
                udr.putDO(index, new boolean[] {
                    !dovals[index]
                });
            }
            catch (IOException ioe) {
                log.warn("Unable to update DO " + index + ": " +
                         ioe.getMessage(), ioe);
            }
            catch (TimeoutException te) {
                log.warn("Timed out trying to update DO " + index + ": " +
                         te.getMessage(), te);
            }
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseEntered(MouseEvent e)
        {
            // Ignore it
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseExited(MouseEvent e)
        {
            // Ignore it
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
         */
        @Override
        public void mousePressed(MouseEvent e)
        {
            // Ignore it
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseReleased(MouseEvent e)
        {
            // Ignore it
        }
    }

    private class AnalogUpdater
        implements MouseListener
    {
        private final byte type;
        private final short index;

        public AnalogUpdater(JComponent cmp, byte _type, short _index)
        {
            type = _type;
            index = _index;

            cmp.addMouseListener(this);
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseClicked(MouseEvent e)
        {
            String result =
                JOptionPane.showInputDialog(IOMonitor.this,
                    "Enter new integer in the range [-32768, 32767]",
                    dovals[index]);
            short val;
            try {
                val = Short.parseShort(result);
            }
            catch (NumberFormatException nfe) {
                JOptionPane.showMessageDialog(IOMonitor.this,
                    "Not an integer in the range [-32768, 32768]: " + result,
                    "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            try {
                udr.putAO(index, val);
            }
            catch (IOException ioe) {
                log.warn("Unable to update AO " + index + ": " +
                         ioe.getMessage(), ioe);
            }
            catch (TimeoutException te) {
                log.warn("Timed out trying to update AO " + index + ": " +
                         te.getMessage(), te);
            }
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseEntered(MouseEvent e)
        {
            // Ignore it
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseExited(MouseEvent e)
        {
            // Ignore it
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
         */
        @Override
        public void mousePressed(MouseEvent e)
        {
            // Ignore it
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseReleased(MouseEvent e)
        {
            // Ignore it
        }
    }
}

/*
 * HTTPServer.java
 *
 * A front-end for the HTTP class. Provide a list of files, and it will share
 * them over HTTP for read-only access (via GET).
 * Example: java HTTPServer shared/*
 * Runs until killed (Ctrl+C)
 * 
 * This works identically to the TFTPServer.
 *
 * Jonathan Pearson
 * October 7, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.sixnetio.server.TransferObserver;
import com.sixnetio.server.http.HTTP;
import com.sixnetio.util.Utils;

public class HTTPServer
	implements TransferObserver
{
	public static final String PROG_VERSION = "0.1.0";
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		PropertyConfigurator.configureAndWatch("log4j.properties");
	}
	
	private static void usage(PrintStream out)
	{
		out.println("HTTPServer v. " + PROG_VERSION + " copyright 2009 SIXNET");
		out.println("  Written by Jonathan Pearson (Jon.Pearson@sixnet.com)");
		
		out.println("Usage: HTTPServer [-p] [-h] [<file> [<file> [...]]]");
		out.println("                  [-P <port>] [-B <backlog>] [-C <capacity>]");
		out.println("-p      Display progress for each file being transferred");
		out.println("-h      Display this message and exit");
		out.println("<file>  Specify one or more files to share");
		out.println("-P      Specify a port to listen on (default: 80)");
		out.println("-B      Specify a backlog size (default: 3)");
		out.println("-C      Specify an open connection capacity (default: 10)");
	}
	
	public static void main(String[] args)
	{
		HTTPServer server = new HTTPServer();
		
		int count = 0;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-p")) {
				server.setProgressIndicator(true);
			}
			else if (args[i].equals("-P")) {
				server.setPort(Integer.parseInt(args[++i]));
			}
			else if (args[i].equals("-h")) {
				usage(System.out);
				return;
			}
			else {
				File f = new File(args[i]);
				if (!f.isFile()) {
					System.err.println(args[i] +
					                   ": Is not a file, it will not be shared");
				}
				else if (server.fileExists(f.getName())) {
					System.err.println(args[i] +
					                   ": Cannot share multiple files of the same name; only the first will be available");
				}
				else {
					server.shareFile(f);
					count++;
				}
			}
		}
		
		try {
			server.start();
		}
		catch (IOException ioe) {
			System.err.println("Unable to start HTTP server: " +
			                   ioe.getMessage());
			logger.error("Unable to start HTTP server", ioe);
			System.exit(1);
			return;
		}
		
		logger.info("Sharing " + count + " files");
		System.out.println("Sharing " + count + " files.");
		System.out.println("Press Ctrl+C to exit.");
		
		// Now an infinite loop, but it can't be a busy one (waste of CPU
		// cycles)
		Object obj = new Object();
		synchronized (obj) {
			while (true) {
				try {
					obj.wait();
				}
				catch (InterruptedException ie) {
					// Ignore it
				}
			}
		}
	}
	
	private int port = 80;
	private int backlog = 3;
	private int capacity = 10;
	
	private HTTP http;
	private List<File> sharedFiles = new LinkedList<File>();
	private boolean progress;
	
	private Map<String, Map<InetAddress, Float>> activeTransfers =
		new LinkedHashMap<String, Map<InetAddress, Float>>();
	
	/** Construct a new HTTP server. You still need to call {@link #start()}. */
	public HTTPServer()
	{
		// Nothing to do
	}
	
	/** Set the listening port number. */
	public void setPort(int port)
	{
		this.port = port;
	}
	
	/** Set the backlog size. */
	public void setBacklog(int backlog)
	{
		this.backlog = backlog;
	}
	
	/** Set the thread capacity. */
	public void setCapacity(int capacity)
	{
		this.capacity = capacity;
	}
	
	/**
	 * Choose whether progress indicators will be printed.
	 * 
	 * @param val <tt>true</tt> to print indicators, <tt>false</tt> not to.
	 */
	public void setProgressIndicator(boolean val)
	{
		this.progress = val;
	}
	
	/**
	 * Check whether a file is going to be shared.
	 * 
	 * @param name The name of the file to check.
	 * @return <tt>true</tt> if it will be shared, <tt>false</tt> otherwise.
	 */
	public boolean fileExists(String name)
	{
		synchronized (sharedFiles) {
			return sharedFiles.contains(name);
		}
	}
	
	/**
	 * Add a file to be shared.
	 * 
	 * @param f The file to share.
	 */
	public void shareFile(File f)
	{
		synchronized (sharedFiles) {
			sharedFiles.add(f);
		}
		
		synchronized (activeTransfers) {
			activeTransfers.put("/" + f.getName(),
			                    new LinkedHashMap<InetAddress, Float>());
		}
	}
	
	/** Start the HTTP server. */
	public void start()
		throws IOException
	{
		InetSocketAddress localAddress = new InetSocketAddress(port);
		http = new HTTP(localAddress, backlog, capacity);
		
		synchronized (sharedFiles) {
			for (File f : sharedFiles) {
				http.addFile("/" + f.getName(), f);
				http.addObserver("/" + f.getName(), this);
			}
		}
		
		http.start();
	}
	
	@Override
	public boolean transferEnded(String path, InetAddress with,
	                             boolean success)
	{
		if (progress) {
			synchronized (activeTransfers) {
				activeTransfers.get(path).put(with, -1.0f);
				
				printProgress();
			}
		}
		
		return true;
	}
	
	@Override
	public void transferProgress(String path, InetAddress with,
	                             int block, int total)
	{
		if (progress) {
			synchronized (activeTransfers) {
				activeTransfers.get(path).put(with,
				                              (float)block / total * 100.0f);
				
				printProgress();
			}
		}
	}
	
	@Override
	public void transferStarted(String path, InetAddress with)
	{
		if (progress) {
			synchronized (activeTransfers) {
				activeTransfers.get(path).put(with, 0.0f);
				
				printProgress();
			}
		}
	}
	
	/**
	 * Prints a progress message for all active transfers. Set the value to -1
	 * to clear that transfer (otherwise it will be stuck on the screen at
	 * 100%).
	 */
	private void printProgress()
	{
		// Build a progress line
		StringBuilder builder = new StringBuilder();
		
		// Used to clear dead entries
		StringBuilder extraSpaces = new StringBuilder();
		
		synchronized (activeTransfers) {
			for (Map.Entry<String, Map<InetAddress, Float>> activeEntry :
				activeTransfers.entrySet()) {
				
				for (Iterator<Map.Entry<InetAddress, Float>> itTransferEntries =
				     activeEntry.getValue().entrySet().iterator();
				     itTransferEntries.hasNext(); ) {
					
					Map.Entry<InetAddress, Float> transferEntry =
						itTransferEntries.next();
					
					if (transferEntry.getValue() == -1.0f) {
						// Getting deleted, calculate the number of spaces
						//   needed to clear the current message
						String tempValue = String.format("%s(%s): 100.0%%",
						                                 transferEntry.getKey().getHostAddress(),
						                                 activeEntry.getKey());
						
						// Add two for the ", "
						char[] spaces = new char[tempValue.length() + 2];
						Arrays.fill(spaces, ' ');
						
						extraSpaces.append(spaces);
						
						// Remove it so we aren't continuously calculating
						//   spaces even though it's gone
						itTransferEntries.remove();
					}
					else {
						// Actually make a message for this one
						if (builder.length() > 0) {
							builder.append(", ");
						}
						
						builder.append(String.format("%s(%s): %.1f%%",
						                             transferEntry.getKey().getHostAddress(),
						                             activeEntry.getKey(),
						                             transferEntry.getValue()));
					}
				}
			}
			
			System.out.printf("\r%s%s", builder.toString(), extraSpaces.toString());
		}
	}
}

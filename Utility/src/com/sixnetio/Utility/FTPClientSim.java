/*
 * FTPClientSim.java
 *
 * Simulates a number of FTP clients performing transfers to an FTP server.
 *
 * Jonathan Pearson
 * September 2, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

import org.apache.commons.net.ftp.*;
import org.apache.log4j.*;

import com.sixnetio.util.Utils;

/**
 * Simulates a number of FTP clients transferring files to an FTP server with a
 * given file size range, time interval, and file number per transfer. This is
 * intended for load testing of an FTP server with a large number of datalog
 * devices transferring their files regularly.
 *
 * @author Jonathan Pearson
 */
public class FTPClientSim {
	/** The logger used by this class. */
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		// Turn off logging
		Logger.getRootLogger().setLevel(Level.OFF);
		
		// Watch for the log4j configuration file; it may turn logging back on
		PropertyConfigurator.configureAndWatch("log4j.properties", 10000);
	}
	
	/** The program version number. */
	public static final String PROG_VERSION = "0.0.1";
	
	/** The default FTP user name. */
	private static final String D_USER = "ftp";
	
	/** The default FTP password. */
	private static final String D_PASSWORD = "ftp";
	
	/** The default number of clients to run. */
	private static final String D_CLIENTS = "125";
	
	/** If a transfer fails, the default number of times to retry. */
	private static final String D_RETRIES = "3";
	
	/** The default minimum number of files to send. */
	private static final String D_FILES_MIN = "10";
	
	/** The default maximum number of files to send. */
	private static final String D_FILES_MAX = "30";
	
	/** The default minimum file size. */
	private static final String D_SIZE_MIN = "55K";
	
	/** The default maximum file size. */
	private static final String D_SIZE_MAX = "65K";
	
	/** The default minimum time interval between client transfers. */
	private static final String D_INTERVAL_MIN = "8m";
	
	/** The default maximum time interval between client transfers. */
	private static final String D_INTERVAL_MAX = "12m";
	
	/**
	 * Groups the values of the received command-line arguments.
	 *
	 * @author Jonathan Pearson
	 */
	private static class Arguments {
		/** The address of the FTP server. */
		public InetAddress address = null;
		
		/** The FTP user name. */
		public String user = D_USER;
		
		/** The FTP password. */
		public String password = D_PASSWORD;
		
		/** The number of clients to simulate. */
		public int clientCount = parseInt(D_CLIENTS);
		
		/** The number of times to retry a transfer session. */
		public int retries = parseInt(D_RETRIES);
		
		/** The minimum number of files to transfer per session. */
		public int filesMin = parseInt(D_FILES_MIN);
		
		/** The maximum number of files to transfer per session. */
		public int filesMax = parseInt(D_FILES_MAX);
		
		/** The minimum file size, in bytes. */
		public int sizeMin_bytes = parseSize(D_SIZE_MIN);
		
		/** The maximum file size, in bytes. */
		public int sizeMax_bytes = parseSize(D_SIZE_MAX);
		
		/** The minimum time interval between transfer sessions, in ms. */
		public long intervalMin_ms = parseTime(D_INTERVAL_MIN);
		
		/** The maximum time interval between transfer sessions, in ms. */
		public long intervalMax_ms = parseTime(D_INTERVAL_MAX);
		
		/** Whether each thread should upload to its own directory. */
		public boolean threadDirs = false;
	}
	
	/**
	 * Simulates an FTP client.
	 *
	 * @author Jonathan Pearson
	 */
	private static class SimulationThread
			extends Thread {
		
		/** Use a logger specific to this class, for easier message parsing. */
		private final Logger logger;
		
		/** The thread number, for easy identification. */
		private int threadNum;
		
		/**
		 * Construct a new SimulationThread.
		 * 
		 * @param threadNum The thread number of this object.
		 */
		public SimulationThread(int threadNum) {
			super("Simulation Thread " + threadNum);
			
			this.threadNum = threadNum;
			logger = Logger.getLogger(SimulationThread.class.getName() + "." +
			                          threadNum);
		}
		
		/**
		 * The main method of this thread. Sends files until the application is
		 * killed.
		 */
		@Override
		public void run() {
			int tryNumber = 0; // Number of failures
			int transferNumber = 0; // Number of successful transfers
			
			logger.debug(String.format("Thread %d is alive", threadNum));
			
			// Choose a standard wait time for this device
			long waitTime_ms = randomLong(arguments.intervalMin_ms,
			                              arguments.intervalMax_ms);
			
			logger.debug(String.format("Wait time set at %,dms", waitTime_ms));
			
			// Sleep for some fraction of that before starting
			{
				long immediateWaitTime_ms = randomLong(0, waitTime_ms);
				
				logger.debug(String.format("Waiting for %,dms before starting",
				                           immediateWaitTime_ms));
				Utils.sleep(immediateWaitTime_ms);
			}
			
			// This is intended to be killed with Ctrl+C
			while (true) {
				boolean success = true;
				
				// Choose how many files we are going to send
				int files = randomInt(arguments.filesMin, arguments.filesMax);
				
				// Open the FTP connection
				FTPClient client = new FTPClient();
				
				try {
					setupConnection(client);
					
					makeDirectories(client);
					
					for (int fileNumber = 0; fileNumber < files; fileNumber++) {
						// Send a random file
						int size =
							performTransfer(client, transferNumber);
						
						// Must have succeeded, it didn't throw an exception
						logger.debug(String.format("Transfer %,d/%,d" +
						                           " (%,d bytes) succeeded",
						                           fileNumber + 1, files, size));
						transferNumber++;
					}
				}
				catch (IOException ioe) {
					logger.error("Transfer error", ioe);
					
					success = false;
				}
				catch (Throwable th) {
					logger.error("Unexpected error, killing thread", th);
					return;
				}
				finally {
					try {
						if (client.isConnected()) {
							client.disconnect();
						}
					}
					catch (IOException ioe) {
						logger.warn("Unable to close FTP connection", ioe);
					}
				}
				
				if (success) {
					// If we succeed, sleep until the next scheduled transfer
					tryNumber = 0;
					
    				Utils.sleep(waitTime_ms);
				}
				else {
					// Otherwise, check the number of failed tries
					tryNumber++;
					
					// If we hit the max...
					if (tryNumber >= arguments.retries) {
						// Reset and sleep as if we had succeeded
						logger.debug("Too many failures, resetting counter");
						
						tryNumber = 0;
						Utils.sleep(waitTime_ms);
					}
				}
			} // while (true)
		}
		
		/**
		 * Connect, log in, and set the file type of the FTP client.
		 * 
		 * @param client The client.
		 * @throws IOException If there is a problem performing the setup.
		 */
		private void setupConnection(FTPClient client)
				throws IOException {
			
			client.connect(arguments.address);
			if (!FTPReply.isPositiveCompletion(client.getReplyCode())) {
				throw new IOException("Bad reply to connect: " +
				                      client.getReplyString());
			}
			
			if (!client.login(arguments.user, arguments.password)) {
				throw new IOException("Unable to login: " +
				                      client.getReplyString());
			}
			
			if (!client.setFileType(FTPClient.BINARY_FILE_TYPE)) {
				throw new IOException("Unable to set BINARY file type: " +
				                      client.getReplyString());
			}
		}
		
		/**
		 * If we are uploading to subdirectories, make sure those subdirectories
		 * exist.
		 * 
		 * @param client The FTP client object to use for communications.
		 * @throws IOException If there was a problem creating the directories.
		 */
		private void makeDirectories(FTPClient client)
			throws IOException
		{
			if (arguments.threadDirs) {
				// Make sure the directories exist
				boolean found = false;
				
				FTPFile[] files = client.listFiles();
				for (FTPFile file : files) {
					if (file.isDirectory() &&
					    file.getName().equals(getHostName())) {
						
						found = true;
						break;
					}
				}
				
				if ( ! found) {
					if ( ! client.makeDirectory(getHostName())) {
						throw new IOException("Unable to make directory '" +
						                      getHostName() + "'");
					}
				}
				
				if ( ! client.changeWorkingDirectory(getHostName())) {
					throw new IOException("Unable to change directory to '" +
					                      getHostName() + "'");
				}
				
				try {
					found = false;
					files = client.listFiles();
					for (FTPFile file : files) {
						if (file.isDirectory() &&
						    file.getName().equals("" + threadNum)) {
							
							found = true;
							break;
						}
					}
					
					if ( ! found) {
						if ( ! client.makeDirectory("" + threadNum)) {
							throw new IOException("Unable to make directory '" +
							                      threadNum + "'");
						}
					}
				}
				finally {
					// Change back to top level
					client.cdup();
				}
			}
		}
		
		/**
		 * Send a single file to the FTP server.
		 * 
		 * @param client The FTP client object through which to send the file.
		 * @param transferNumber The transfer number of this file, for unique
		 *   file naming.
		 * @return The number of bytes sent (a randomly chosen number).
		 * @throws IOException If there was a problem sending the file.
		 */
		private int performTransfer(FTPClient client, int transferNumber)
				throws IOException {
			
			// Choose how many bytes we are going to send
			int bytes = randomInt(arguments.sizeMin_bytes, arguments.sizeMax_bytes);
			
			// Choose a file name
			String fileName;
			if (arguments.threadDirs) {
				// This will look like "host/5/329"
				// The format is:
				// <host name>/<thread number>/<transfer number>
				fileName = String.format("%s/%d/%d", getHostName(), threadNum,
				                         transferNumber);
			}
			else {
				// This will look like "host-5-329"
				// The format is:
				// <host name>-<thread number>-<transfer number>
				fileName = String.format("%s-%d-%d", getHostName(), threadNum,
				                         transferNumber);
			}
			
			OutputStream out = client.storeFileStream(fileName);
			if (!FTPReply.isPositivePreliminary(client.getReplyCode())) {
				throw new IOException("Bad reply to open sending stream: " +
				                      client.getReplyString());
			}
			
			out.write(transferData, 0, bytes);
			out.close();
    		
			if (!client.completePendingCommand()) {
				throw new IOException("File transfer failed: " +
				                      client.getReplyString());
			}
			
			return bytes;
		}
	}
	
	/** Used for generating random data. */
	private static Random rand = new Random();
	
	/** The arguments passed on the command line. */
	private static Arguments arguments;
	
	/** A shared array of random bytes for transferring to the server. */
	private static byte[] transferData;
	
	/**
	 * Print usage information through the given stream.
	 * 
	 * @param out The stream to print on.
	 */
	private static void usage(PrintStream out) {
		out.println("DLogClientSim v. " + PROG_VERSION + ", Copyright 2009 Sixnet");
		out.println("Written by Jonathan Pearson (Jon.Pearson@sixnet.com)");
		
		out.println("Usage: FTPClientSim -a <address> [-d <dev count>] [-r <retries>]");
		out.println("                    [-u <ftp user>] [-p <ftp password>]");
		out.println("                    [-fm <min files>] [-fx <max files>]");
		out.println("                    [-sm <min size>] [-sx <max size>]");
		out.println("                    [-im <min interval>] [-ix <max interval>]");
		out.println("                    [-td]");
		
		out.println(" -a   Specify the address of the FTP server");
		out.println(" -d   Specify the number of devices to simulate");
		out.println("        Each device gets its own thread, so be careful with large numbers");
		out.println("        Default: " + D_CLIENTS);
		out.println(" -r   Specify the number of retries when transfers fail");
		out.println("        Default: " + D_RETRIES);
		out.println(" -u   Specify the FTP user name");
		out.println("        Default: " + D_USER);
		out.println(" -p   Specify the FTP password");
		out.println("        Default: " + D_PASSWORD);
		out.println(" -fm  Specify the minimum number of datalog files to transfer per session");
		out.println("        Default: " + D_FILES_MIN);
		out.println(" -fx  Specify the maximum number of datalog files to transfer per session");
		out.println("        Default: " + D_FILES_MAX);
		out.println(" -sm  Specify the minimum size for each file; postfix with B, K, M, or G");
		out.println("        Default: " + D_SIZE_MIN);
		out.println(" -sx  Specify the maximum size for each file; postfix with B, K, M, or G");
		out.println("        Default: " + D_SIZE_MAX);
		out.println(" -im  Specify the minimum interval between transfers; postfix with s, m, h,");
		out.println("        or d");
		out.println("        Default: " + D_INTERVAL_MIN);
		out.println(" -ix  Specify the maximum interval between transfers; postfix with s, m, h,");
		out.println("        or d");
		out.println("        Default: " + D_INTERVAL_MAX);
		out.println(" -td  Each thread will upload its files to a numbered directory, based on");
		out.println("        the thread number");
	}
	
	/**
	 * The main program.
	 * 
	 * @param args The command-line arguments.
	 */
	public static void main(String[] args) {
		arguments = parseArguments(args);
		if (arguments == null) {
			return;
		}
		
		// Generate enough data so that threads may choose a random amount less,
		// but will never run out if they choose more
		logger.debug(String.format("Generating %d bytes of data",
		                           arguments.sizeMax_bytes));
		transferData = generateData(arguments.sizeMax_bytes);
		
		logger.debug(String.format("Starting %d threads",
		                           arguments.clientCount));
		for (int threadNum = 0;
		     threadNum < arguments.clientCount;
		     threadNum++) {
			
			Thread th = new SimulationThread(threadNum);
			th.start();
		}
	}
	
	/**
	 * Parse the command-line arguments.
	 * 
	 * @param args The arguments to parse.
	 * @return An Arguments object, or <tt>null</tt> if there was a parse error.
	 *   This will print usage information, and will call
	 *   {@link System#exit(int)} as necessary.
	 */
	private static Arguments parseArguments(String[] args) {
		Arguments arguments = new Arguments();
		
		try {
			int i = 0;
			while (i < args.length) {
				int consumed;
				
				if (args[i].equals("-a")) {
					try {
						arguments.address = InetAddress.getByName(args[i + 1]);
						consumed = 2;
					}
					catch (UnknownHostException uhe) {
						throw new Exception("Unable to determine address: " +
						                    uhe.getMessage(), uhe);
					}
				}
				else if (args[i].equals("-d")) {
					arguments.clientCount = parseInt(args[i + 1]);
					consumed = 2;
				}
				else if (args[i].equals("-r")) {
					arguments.retries = parseInt(args[i + 1]);
					consumed = 2;
				}
				else if (args[i].equals("-u")) {
					arguments.user = args[i + 1];
					consumed = 2;
				}
				else if (args[i].equals("-p")) {
					arguments.password = args[i + 1];
					consumed = 2;
				}
				else if (args[i].equals("-fm")) {
					arguments.filesMin = parseInt(args[i + 1]);
					consumed = 2;
				}
				else if (args[i].equals("-fx")) {
					arguments.filesMax = parseInt(args[i + 1]);
					consumed = 2;
				}
				else if (args[i].equals("-sm")) {
					arguments.sizeMin_bytes = parseSize(args[i + 1]);
					consumed = 2;
				}
				else if (args[i].equals("-sx")) {
					arguments.sizeMax_bytes = parseSize(args[i + 1]);
					consumed = 2;
				}
				else if (args[i].equals("-im")) {
					arguments.intervalMin_ms = parseTime(args[i + 1]);
					consumed = 2;
				}
				else if (args[i].equals("-ix")) {
					arguments.intervalMax_ms = parseTime(args[i + 1]);
					consumed = 2;
				}
				else if (args[i].equals("-td")) {
					arguments.threadDirs = true;
					consumed = 1;
				}
				else if (args[i].equals("-h") || args[i].equals("-?")) {
					usage(System.out);
					return null;
				}
				else {
					throw new Exception("Unrecognized argument: " + args[i]);
				}
				
				i += consumed;
			} // End of while loop
			
			// Check for required arguments
			if (arguments.address == null) {
				throw new Exception("Address (-a) is a required argument");
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			logger.debug(e);
			
			usage(System.err);
			System.exit(1);
			return null;
		}
		
		return arguments;
	}
	
	/**
	 * Generate data for the worker threads to send.
	 * 
	 * @param size The size of the byte array to generate.
	 * @return A byte array containing random data.
	 */
	private static byte[] generateData(int size) {
		byte[] data = new byte[size];
		
		// Fill it in with random data
		rand.nextBytes(data);
		return data;
	}
	
	/**
	 * The host name of this machine, cached for quick access. Only used by
	 * {@link #getHostName()}.
	 */
	private static String hostName = null;
	
	/**
	 * Get the host name of this system. Will return "(Unknown)" if there is an
	 * error retrieving the host name.
	 */
	private static String getHostName() {
		if (hostName == null) {
			try {
				hostName = InetAddress.getLocalHost().getCanonicalHostName();
			}
			catch (UnknownHostException uhe) {
				logger.warn("Unable to determine local host name", uhe);
				
				hostName = "(Unknown)";
			}
		}
		
		return hostName;
	}
	
	/**
	 * A wrapper around {@link Integer#parseInt(String)} for blocks of code
	 * trying to maintain uniformity with calls to {@link #parseSize(String)}
	 * and {@link #parseTime(String)}. This will improve the error message if
	 * the number does not parse, though.
	 * 
	 * @param s The string to parse.
	 * @return The value of the string as an integer.
	 */
	public static int parseInt(String s) {
		if (s.length() == 0) {
			throw new NumberFormatException("Cannot parse empty string");
		}
		
		try {
			return Integer.parseInt(s);
		}
		catch (NumberFormatException nfe) {
			throw new NumberFormatException("Not a valid integer: " + s);
		}
	}
	
	/**
	 * Parse a string as a size.
	 * 
	 * @param s A string. If it ends with one of these letters, it will be
	 *   treated specially:
	 *   <ul>
	 *     <li><b>B</b> Bytes</li>
	 *     <li><b>K</b> Kilobytes (1024 bytes)</li>
	 *     <li><b>M</b> Megabytes (1024 kilobytes)</li>
	 *     <li><b>G</b> Gigabytes (1024 megabytes)</li>
	 *   </ul>
	 *   Any other character will cause an exception to be thrown. If no
	 *   character is found, it will be treated as bytes.
	 * @return The number of bytes specified by the size.
	 */
	public static int parseSize(String s) {
		if (s.length() == 0) {
			throw new NumberFormatException("Cannot parse empty string");
		}
		
		char type = s.charAt(s.length() - 1);
		int value;
		
		try {
			if (Character.isLetter(type)) {
				value = Integer.parseInt(s.substring(0, s.length() - 1));
				
				switch (Character.toUpperCase(type)) {
					case 'B':
						value *= 1;
						break;
					case 'K':
						value *= 1024;
						break;
					case 'M':
						value *= 1024 * 1024;
						break;
					case 'G':
						value *= 1024 * 1024 * 1024;
						break;
					default:
						throw new NumberFormatException();
				}
			}
			else {
				value = Integer.parseInt(s);
			}
		}
		catch (NumberFormatException nfe) {
			throw new NumberFormatException("Not a valid size: " + s);
		}
		
		return value;
	}
	
	/**
	 * Parse a string as a time interval.
	 * 
	 * @param s A string. If it ends with one of these letters, it will be
	 *   treated specially:
	 *   <ul>
	 *     <li><b>S</b> Seconds</li>
	 *     <li><b>M</b> Minutes</li>
	 *     <li><b>H</b> Hours</li>
	 *     <li><b>D</b> Days</li>
	 *   </ul>
	 *   Any other character will cause an exception to be thrown. If no
	 *   character is found, it will be treated as seconds.
	 * @return The number of milliseconds specified by the time interval.
	 */
	public static long parseTime(String s) {
		if (s.length() == 0) {
			throw new NumberFormatException("Cannot parse empty string");
		}
		
		char type = s.charAt(s.length() - 1);
		long value;
		
		try {
			if (Character.isLetter(type)) {
				value = Long.parseLong(s.substring(0, s.length() - 1));
				
				switch (Character.toUpperCase(type)) {
					case 'S':
						value *= 1000;
						break;
					case 'M':
						value *= 60 * 1000;
						break;
					case 'H':
						value *= 60 * 60 * 1000;
						break;
					case 'D':
						value *= 24 * 60 * 60 * 1000;
						break;
					default:
						throw new NumberFormatException();
				}
			}
			else {
				value = Long.parseLong(s) * 1000;
			}
		}
		catch (NumberFormatException nfe) {
			throw new NumberFormatException("Not a valid time interval: " + s);
		}
		
		
		return value;
	}
	
	/**
	 * Generate a random integer in a given range.
	 * 
	 * @param min The minimum value, inclusive.
	 * @param max The maximum value, exclusive.
	 * @return A random integer in the range [min, max).
	 */
	private static int randomInt(int min, int max) {
		return (int)(rand.nextDouble() * (max - min) + min);
	}
	
	/**
	 * Generate a random long in a given range.
	 * 
	 * @param min The minimum value, inclusive.
	 * @param max The maximum value, exclusive.
	 * @return A random long in the range [min, max).
	 */
	private static long randomLong(long min, long max) {
		return (long)(rand.nextDouble() * (max - min) + min);
	}
}

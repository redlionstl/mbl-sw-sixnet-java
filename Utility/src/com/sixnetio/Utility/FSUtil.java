/*
 * FSUtil.java
 *
 * Provides generic filesystem utility operations across the various
 * implementations of the generic filesystem.
 *
 * Jonathan Pearson
 * December 7, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.PrintStream;

import com.sixnetio.fs.jffs2.JFFS2;

public class FSUtil
{
	public static final String PROG_VERSION = "0.0.1";
	
	private static void usage(PrintStream out)
	{
		out.println("FSUtil v. " + PROG_VERSION + " copyright 2009 Sixnet");
		out.println("  Written by Jonathan Pearson (Jon.Pearson@sixnet.com)");
		
		out.println("Usage: FSUtil -i <fs type> {-d|-e <file>|-o <fs type>} <input>");
		
		out.println();
		out.println("Generic options:");
		out.println("  -i  Specify input filesystem type");
		out.println("        Choose from: fwb jffs2 raw tar tgz");
		out.println("  -d  Dump a directory listing of the filesystem to stdout");
		out.println("  -e  Specify a path to a file to extract from the filesystem and dump to stdout");
		out.println("  -o  Convert the filesystem to this format and dump to stdout");
		out.println("        Choose from: fwb jffs2 raw tar tgz");
		out.println("  <input> Specify the input image, or - to read from stdin");
		
		out.println();
		out.println("FWB output specific options:");
		out.println("... -v <fw version> [-oem <oem name>]");
		out.println("  -v  Specify the firmware version contained in the bundle");
		out.println("  -oem Specify the name of the OEM whose firmware is in the bundle");
		out.println("         (default is blank, meaning Sixnet)");
		
		out.println();
		out.println("JFFS2 output specific options:");
		out.println("... [-l] [-c] [-cs <size>] [-es <size>] [-ps <size>]");
		out.println("  -l  Make a little-endian file system (default is big-endian)");
		out.println("  -c  Include clean markers (default is to not include them)");
		out.println("  -cs Specify the clean marker size (default is " + JFFS2.D_CLEAN_MARKER_SIZE + " bytes)");
		out.println("  -es Specify the erase block size (default is " + JFFS2.D_ERASE_BLOCK_SIZE + " bytes)");
		out.println("  -ps Specify the page size (default is " + JFFS2.D_PAGE_SIZE + " bytes)");
		
		out.println();
		out.println("Tar/TGZ and Raw have no specific options");
		
		out.println();
		out.println("Note: The 'raw' filesystem type is an XML-based format useful for saving a");
		out.println("  filesystem image in a format which may be hand-edited and may be made into");
		out.println("  any of the other formats, including the original format, without losing");
		out.println("  information. For example, a FWB image could be converted to raw format,");
		out.println("  names of files could be modified, and then it could be converted back to");
		out.println("  FWB without losing information like the user types on the files. Obviously");
		out.println("  converting to something like JFFS2 will discard metadata like user types, as");
		out.println("  there is no facility in that filesystem for storing such information.");
		
	}
	public static void main(String[] args)
	{
		String inputType = null;
		boolean dump = false;
		String extractPath = null;
		String outputType = null;
		String inputFile = null;
		
		String fwbVersion = null;
		String fwbOEM = "";
		
		boolean jLittleEndian = false;
		boolean jCleanMarkers = false;
		int jCleanMarkerSize = JFFS2.D_CLEAN_MARKER_SIZE;
		int jEraseBlockSize = JFFS2.D_ERASE_BLOCK_SIZE;
		int jPageSize = JFFS2.D_PAGE_SIZE;
		
		try {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-i")) {
					inputType = args[++i];
				}
				else if (args[i].equals("-d")) {
					dump = true;
				}
				else if (args[i].equals("-e")) {
					extractPath = args[++i];
				}
				else if (args[i].equals("-o")) {
					outputType = args[++i];
				}
				
				// FWB-specific
				else if (args[i].equals("-v")) {
					fwbVersion = args[++i];
				}
				else if (args[i].equals("-oem")) {
					fwbOEM = args[++i];
				}
				
				// JFFS2-specific
				else if (args[i].equals("-l")) {
					jLittleEndian = true;
				}
				else if (args[i].equals("-c")) {
					jCleanMarkers = true;
				}
				else if (args[i].equals("-cs")) {
					try {
						jCleanMarkerSize = Integer.parseInt(args[++i]);
					}
					catch (NumberFormatException nfe) {
						throw new Exception("-cs must specify an integer: " + args[i], nfe);
					}
				}
				else if (args[i].equals("-es")) {
					try {
						jEraseBlockSize = Integer.parseInt(args[++i]);
					}
					catch (NumberFormatException nfe) {
						throw new Exception("-es must specify an integer: " + args[i], nfe);
					}
				}
				else if (args[i].equals("-ps")) {
					try {
						jPageSize = Integer.parseInt(args[++i]);
					}
					catch (NumberFormatException nfe) {
						throw new Exception("-ps must specify an integer: " + args[i], nfe);
					}
				}
				else {
					if (inputFile == null) {
						inputFile = args[i];
					}
					else {
						throw new Exception("Unrecognized option: " + args[i]);
					}
				}
			}
			
			if (inputType == null) {
				throw new Exception("No input type specified");
			}
			else if ( ! (inputType.equals("fwb") ||
			             inputType.equals("jffs2") ||
			             inputType.equals("raw") ||
			             inputType.equals("tar") ||
			             inputType.equals("tgz"))) {
				throw new Exception("Unrecognized input type: " + inputType);
			}
			else if (dump == false && extractPath == null && outputType == null) {
				throw new Exception("No operation (-d, -e, or -o) specified");
			}
			else if (inputFile == null) {
				throw new Exception("No input file specified");
			}
			else if (outputType != null) {
				if (outputType.equals("fwb")) {
					if (fwbVersion == null) {
						throw new Exception("FWB version number is required");
					}
				}
				else if (outputType.equals("jffs2")) {
					// No required options
				}
				else if (outputType.equals("raw")) {
					// No required options
				}
				else if (outputType.equals("tar") || outputType.equals("tgz")) {
					// No required options
				}
				else {
					throw new Exception("Unrecognized output type: " + outputType);
				}
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			usage(System.err);
			System.exit(1);
			return;
		}
		
		
	}
}

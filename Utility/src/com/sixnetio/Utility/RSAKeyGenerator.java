/*
 * RSAKeyGenerator.java
 *
 * Generates an RSA key pair in the XML format used by Sixnet.
 *
 * Jonathan Pearson
 * May 18, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.*;
import java.security.*;

import com.sixnetio.util.Utils;

public class RSAKeyGenerator {
	public static final String PROG_VERSION = "0.1.0";
	
	private static void usage(PrintStream out) {
		out.println("RSAKeyGenerator v. " + PROG_VERSION + " (c) 2009 SIXNET");
		out.println("  Written by Jonathan Pearson (Jon.Pearson@sixnet.com)");
		
		out.println("Usage: RSAKeyGenerator [-b <bits>] [-pub <pub file>] [-prv <prv file>] [-h]");
		out.println("  -b <bits>        Specify size of the key to generate, in bits (default: 1024)");
		out.println("  -pub <pub file>  Specify the path to the file for saving the public key");
		out.println("                     (default: 'pubkey.x509')");
		out.println("  -prv <prv file>  Specify the path to the file for saving the private key");
		out.println("                     (default: 'prvkey.pkcs8')");
		out.println("  -h                Display this message and exit");
	}
	
	public static void main(String[] args) {
		int bits = 1024;
		File pubFile = new File("pubkey.x509");
		File prvFile = new File("prvkey.pkcs8");
		
		try {
    		for (int i = 0; i < args.length; i++) {
    			if (args[i].equals("-b")) {
    				try {
    					bits = Integer.parseInt(args[++i]);
    				} catch (NumberFormatException nfe) {
    					throw new Exception("Argument to -b is not an integer: '" + args[i] + "'");
    				}
    			} else if (args[i].equals("-pub")) {
    				pubFile = new File(args[++i]);
    			} else if (args[i].equals("-prv")) {
    				prvFile = new File(args[++i]);
    			} else if (args[i].equals("-h")) {
    				usage(System.out);
    				return;
    			} else {
    				throw new Exception("Unrecognized argument: '" + args[i] + "'");
    			}
    		}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			usage(System.err);
			System.exit(1);
			return;
		}
		
		if (pubFile.exists()) {
			System.err.println("Cowardly refusing to overwrite '" + pubFile.getAbsolutePath() + "'");
			System.exit(1);
			return;
		}
		
		if (prvFile.exists()) {
			System.err.println("Cowardly refusing to overwrite '" + prvFile.getAbsolutePath() + "'");
			System.exit(1);
			return;
		}
		
		KeyPairGenerator generator;
        try {
        	generator = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException nsae) {
	        System.err.println("RSA is not available on this system: " + nsae.getMessage());
	        System.exit(1);
	        return;
        }
        
		generator.initialize(bits, Utils.srand);
		
		System.out.println("Generating key pair (" + bits + " bits, this may take a while)...");
		KeyPair pair = generator.generateKeyPair();
		
		PublicKey pub = pair.getPublic();
		PrivateKey prv = pair.getPrivate();
		
		OutputStream pubOut = null;
		try {
    		pubOut = new FileOutputStream(pubFile);
    		pubOut.write(pub.getEncoded());
    		System.out.println("Public key written to '" + pubFile.getAbsolutePath() + "' in '" + pub.getFormat() + "' format");
		} catch (IOException ioe) {
			System.err.println("Unable to write public key file: " + ioe.getMessage());
			System.exit(1);
			return;
		} finally {
			try {
				if (pubOut != null) pubOut.close();
			} catch (IOException ioe) {
				System.err.println("Warning: Unable to close public key file: " + ioe.getMessage());
			}
		}
		
		OutputStream prvOut = null;
        try {
	        prvOut = new FileOutputStream(prvFile);
	        prvOut.write(prv.getEncoded());
	        System.out.println("Private key written to '" + prvFile.getAbsolutePath() + "' in '" + prv.getFormat() + "' format");
        } catch (IOException ioe) {
        	System.err.println("Unable to write private key file: " + ioe.getMessage());
			System.exit(1);
			return;
        } finally {
        	try {
	            if (prvOut != null) prvOut.close();
            } catch (IOException ioe) {
            	System.err.println("Warning: Unable to close private key file: " + ioe.getMessage());
            }
        }
	}
}

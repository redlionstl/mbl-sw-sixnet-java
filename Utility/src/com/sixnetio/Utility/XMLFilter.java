/*
 * XMLFilter.java
 *
 * A filter for XML data, allowing you to run simple matching queries on an XML
 * file.
 *
 * Jonathan Pearson
 * July 29, 2009
 *
 */

package com.sixnetio.Utility;

import java.io.IOException;
import java.io.PrintStream;
import java.util.*;

import org.jonp.xml.Tag;

import com.sixnetio.util.Utils;

public class XMLFilter {
	private static void usage(PrintStream out) {
		out.println("Usage: XMLFilter <path> <match> <sub-path> <comparison> <value>");
		out.println("                 [<comparison> <value> [...]]");
		out.println(" <path>  Should look like 'path/to/tag', and references the root of");
		out.println("           where the sub-tags may be found");
		out.println(" <match>  Is the name of the sub-tags to search for");
		out.println(" <sub-path>  Is the path to the value beneath each of those sub-tags");
		out.println("               to test, in the format 'path/to/tag' or");
		out.println("               'path/to attribute");
		out.println(" <comparison>  Should be '==' or '!=' for string comparisons, or");
		out.println("                 '<', '>', '<=', or '>=' for double comparisons");
		out.println(" <value>  May be a string or a double, it will be parsed according");
		out.println("            to the comparison operator");
		out.println(" Multiple comparisons are treated as an OR operation");
		out.println(" You may achieve an AND operation by passing through XMLFilter");
		out.println("   multiple times");
		out.println();
		out.println("This operation is subtractive, meaning that non-matching values in");
		out.println("  the specified region will be removed, leaving everything else as-is");
	}
	
	public static void main(String[] args) throws IOException {
		if (args.length < 5) {
			usage(System.err);
			System.exit(1);
			return;
		}
		
		String path = args[0];
		String match = args[1];
		String subPath = args[2];
		
		List<String> comparisons = new LinkedList<String>();
		List<String> values = new LinkedList<String>();
		
		System.err.println("Comparing on:");
		
		if (subPath.indexOf(' ') == -1) {
			System.err.printf("  '%s' tag", subPath);
		} else {
			System.err.printf("  '%s' attribute", subPath);
		}
		
		for (int i = 3; i < args.length; i += 2) {
    		String comparison = args[i];
    		String value = args[i + 1];
    		
    		if (!(comparison.equals("==") || comparison.equals("!=") ||
			      comparison.equals(">") || comparison.equals("<") ||
			      comparison.equals(">=") || comparison.equals("<="))) {
				
    			System.err.println("Bad comparison: " + comparison);
				usage(System.err);
				System.exit(1);
				return;
			}
			
			comparisons.add(comparison);
    		values.add(value);
    		
    		System.err.printf("  %s %s\n", comparison, value);
		}
		
		// Read stdin for the XML data
		Tag xmlData = Utils.xmlParser.parseXML(System.in, false, false);
		
		// Wrap it in an extra tag so that the queries need to include the root
		Tag root = new Tag("root");
		root.addContent(xmlData);
		
		// Grab the sub-tags
		Tag[] subTags = root.getEachTag(path, match);
		System.err.printf("Found %d matches of '%s' under '%s'\n",
		                  subTags.length, match, path);
		
		// Remove each one that does not match the query
		Tag removeFrom = root.getTag(path);
		
		int counter = 0;
		for (Tag tag : subTags) {
			String data;
			
			if (subPath.indexOf(' ') == -1) {
				data = tag.getStringContents(subPath, null);
			} else {
				data = tag.getStringAttribute(subPath, null);
			}
			
			if (data == null) {
				removeFrom.removeTag(tag);
				continue;
			}
			
			// Compare
			boolean keep = false;
			for (Iterator<String> itComparisons = comparisons.iterator(),
			                      itValues = values.iterator();
			     itComparisons.hasNext() && itValues.hasNext(); ) {
				
				String comparison = itComparisons.next();
				String value = itValues.next();
				
				if (comparison.equals("==") || comparison.equals("!=")) {
    				if (data.equals(value)) {
    					keep = true;
    					break;
    				}
    			} else {
    				double a, b;
    				
    				try {
    					a = Double.parseDouble(data);
    				} catch (NumberFormatException nfe) {
    					a = Double.NaN;
    				}
    				
    				try {
    					b = Double.parseDouble(value);
    				} catch (NumberFormatException nfe) {
    					System.err.println("Value '" + value + "' is not a double");
    					usage(System.err);
    					System.exit(1);
    					return;
    				}
    				
    				if (a == b) {
    					keep = true;
    					break;
    				}
    			}
			}
			
			if (!keep) {
				removeFrom.removeTag(tag);
			} else {
				counter++;
			}
		}
		
		// Output
		Utils.xmlParser.writeXMLStream(xmlData, System.out, true);
		
		System.err.printf("Kept %d matches\n", counter);
	}
}

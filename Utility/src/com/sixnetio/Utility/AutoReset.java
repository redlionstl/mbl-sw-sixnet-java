/*
 * AutoReset.java
 *
 * Continuously resets a given IPm station until it refuses to respond.
 * This was written to try to catch an intermittent non-responding bug.
 *
 * Jonathan Pearson
 * December 4, 2008
 *
 */

package com.sixnetio.Utility;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.util.concurrent.TimeoutException;

import javax.swing.JOptionPane;

import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.util.Utils;

public class AutoReset {
	private static void usage(PrintStream out) {
		out.println("Usage: AutoReset -a <IP address> [-d <IP address>\n" +
		            "                 -n <station number> -o <output offset>]\n" +
		            "                 [-hard] [-p] [-h]");
		out.println("  -a  Provide the IP address of the station to auto-reset");
		out.println("  -d  Instead of using ResetIPm messages, provide the address to a");
		out.println("        discrete output module controlling the power to the device");
		out.println("  -n  The station number of the discrete output module");
		out.println("  -o  The output number (0-based) controlling power to the device");
		out.println("  -hard  Do hard resets (only relevant if not using a DO module");
		out.println("  -p  Use ping instead of a UDR NOP message to detect bootup");
		out.println("  -h  Display this message and exit");
	}
	
	public static void main(String[] args) {
		String address = null;
		String doModule = null;
		short doNumber = -1;
		short doOffset = -1;
		boolean hard = false;
		boolean ping = false;
		
		try {
    		for (int i = 0; i < args.length; i++) {
    			if (args[i].equals("-a")) {
    				address = args[++i];
    			} else if (args[i].equals("-d")) {
    				doModule = args[++i];
    			} else if (args[i].equals("-n")) {
    				doNumber = Short.parseShort(args[++i]);
    			} else if (args[i].equals("-o")) {
    				doOffset = Short.parseShort(args[++i]);
    			} else if (args[i].equals("-hard")) {
    				hard = true;
    			} else if (args[i].equals("-p")) {
    				ping = true;
    			} else if (args[i].equals("-h")) {
    				usage(System.out);
    				return;
    			} else {
    				throw new Exception("Unrecognized option: " + args[i]);
    			}
    		}
    		
    		if (address == null) {
    			throw new Exception("<IP address> must be provided");
    		}
    		
    		if (doModule != null || doNumber != -1 || doOffset != -1) {
				if (doModule == null || doNumber == -1 || doOffset == -1) {
					throw new Exception("All of the DO information must be provided");
				}
			}
		} catch (Exception e) {
			System.err.println(e);
			usage(System.err);
			System.exit(1);
			return;
		}
		
		// Set up a connection
		UDRLib udr;
		try {
			MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();
			dispatcher.registerHandler(new UDRLink(TransportMethod.UDP, address));
			
			if (doModule != null) dispatcher.registerHandler(new UDRLink(TransportMethod.UDP, doModule));
			
			udr = new UDRLib((short)100, (byte)0);
		} catch (IOException ioe) {
			System.err.println("Unable to set up communications: " + ioe.getMessage());
			return;
		}
		
		// Get the station and serial numbers from the device
		int[] serial = new int[1];
		short[] station = new short[1];
		
		if (doModule == null) {
    		try {
    			udr.readIPmSettings(UDRMessage.STA_ANY, station, serial, null, null);
    		} catch (Exception e) {
    			System.err.println("Unable to communicate with the station: " + e.getMessage());
    			return;
    		}
		}
		
		System.out.printf("Playing with station #%d with serial #%d at %s\n", station[0], serial[0], address);
		
		if (doModule != null) System.out.printf("  Using output #%d of DO module #%d at %s\n", doOffset, doNumber, doModule);
		
		boolean responded = true;
		int tries = 0;
		while (responded) {
			try {
				tries++;
    			System.out.println("Reset #" + tries);
    			
    			long startTime;
    			
    			if (doModule == null) {
    				udr.resetIPm(station[0], serial[0], hard);
    				
    				startTime = System.nanoTime();
    				Utils.sleep(4000); // Make sure it actually resets
    			} else {
    				boolean[] data = { false };
    				udr.putD(doNumber, doOffset, (short)1, data);
    				
    				Utils.sleep(1000);
    				
    				data[0] = true;
    				udr.putD(doNumber, doOffset, (short)1, data);
    				
    				startTime = System.nanoTime();
    			}
    			
    			if (ping) {
    				InetAddress addr = InetAddress.getByName(address);
    				long pingTime = System.currentTimeMillis();
    				boolean response = false;
    				while (pingTime + 60000 > System.currentTimeMillis() && !response) {
    					if (addr.isReachable(3000)) {
    						response = true;
        				}
    				}
    				
    				if (!response) throw new IOException("No response to pings for 60 seconds");
    			} else {
    				udr.waitForResponse(station[0]);
    			}
    			
    			long stopTime = System.nanoTime();
    			
    			System.out.printf("  Station took %,d seconds to respond\n", (stopTime - startTime) / 1000000000);
			} catch (IOException ioe) {
				System.err.println("Error communicating with station: " + ioe.getMessage());
				return;
			} catch (TimeoutException te) {
				responded = false;
			}
		}
		
		System.out.println("The station stopped responding after " + tries + " cycles");
		
		JOptionPane.showMessageDialog(null, "The station stopped responding", "Alert", JOptionPane.WARNING_MESSAGE);
	}
}

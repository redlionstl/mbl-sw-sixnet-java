/*
 * BundleDump.java
 *
 * Dumps a full directory listing of a firmware bundle.
 *
 * Jonathan Pearson
 * September 5, 2008
 *
 */

package com.sixnetio.Utility;

import java.io.*;

import com.sixnetio.fs.vfs.FirmwareBundle;

public class BundleDump {
	public static void main(String[] args) {
		if (args.length != 1) {
			usage(System.err);
			System.exit(1);
			return;
		}
		
		InputStream in;
		
		try {
			in = new FileInputStream(args[0]);
		} catch (IOException ioe) {
			System.err.println("Unable to open the file: " + ioe.getMessage());
			System.exit(1);
			return;
		}
		
		FirmwareBundle bundle;
		
		try {
			bundle = new FirmwareBundle(in);
		} catch (IOException ioe) {
			System.err.println("Unable to parse the bundle: " + ioe.getMessage());
			System.exit(1);
			return;
		} finally {
			try {
				in.close();
			} catch (IOException ioe) { }
		}
		
		System.out.println("Listing of " + args[0]);
		bundle.dump(System.out);
	}
		
	private static void usage(PrintStream out) {
		out.println("BundleDump - Dumps a file listing of a firmware bundle");
		out.println("  (c)2008 SIXNET, written by Jonathan Pearson (jonp@sixnetio.com)");
		
		out.println("Usage: BundleDump <bundle>");
		out.println("  <bundle> must be a file");
	}
}

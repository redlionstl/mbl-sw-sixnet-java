/*
 * JFFS2.java
 *
 * Allows for browsing and extracting file data from a JFFS2 image.
 * 
 * License: GPL v2.0 (assumed; this is basically a translation of
 *   GPL v2.0 licensed code)
 *
 * Jonathan Pearson
 * July 25, 2008
 *
 */

package com.sixnetio.fs.jffs2;

import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.jcraft.jzlib.ZInputStream;
import com.jcraft.jzlib.ZOutputStream;
import com.sixnetio.fs.generic.*;
import com.sixnetio.fs.generic.File;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;


public class JFFS2
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// When writing to a stream using toStream(out), these are the default
	// values used for the other parameters
	/** Default to writing a big-endian file system image. */
	public static final boolean D_LITTLE_ENDIAN = false;
	
	/** Default to including clean markers. */
	public static final boolean D_ADD_CLEAN_MARKERS = true;
	
	/** Default to a 12-byte long clean marker. */
	public static final int D_CLEAN_MARKER_SIZE = 12;
	
	/** Default to an erase block size of 64KiB (must be at least 8KiB). */
	public static final int D_ERASE_BLOCK_SIZE = 65536;
	
	/** Default page size. */
	public static final int D_PAGE_SIZE = 4096;
	
	/**
	 * This is used to pass around the options while writing a file system,
	 * rather than passing the options around individually.
	 *
	 * @author Jonathan Pearson
	 */
	private static class WriteOptions
	{
		/** The output stream to write the image to. */
		public final DataOutputStream out;
		
		/** Whether the file system is little-endian. */
		public final boolean littleEndian;
		
		/** A clean-marker (will be length 0 if clean markers are disabled). */
		public final byte[] cleanMarker;
		
		/** The erase block size. */
		public final int eraseBlockSize;
		
		/** The page size. */
		public final int pageSize;
		
		/** A useful byte array containing all 0xff bytes. */
		public final byte[] ffBuf;
		
		/** The current offset within the image. */
		public int offset = 0;
		
		/** A map of FSObject onto INode number, as they are allocated. */
		public final Map<FSObject, Integer> inodes =
			new HashMap<FSObject, Integer>();
		
		public WriteOptions(DataOutputStream out, boolean littleEndian,
		                    byte[] cleanMarker, int eraseBlockSize,
		                    int pageSize)
		{
			this.out = out;
			this.littleEndian = littleEndian;
			this.cleanMarker = cleanMarker;
			this.eraseBlockSize = eraseBlockSize;
			this.pageSize = pageSize;
			
			ffBuf = new byte[64];
			Arrays.fill(ffBuf, (byte)0xff);
		}
	}
	
	// This is translated from the mkfs.jffs2 source code
	public static final short JFFS2_SUPER_MAGIC = (short)0x72b6;
	public static final short JFFS2_OLD_MAGIC_BITMASK = (short)0x1984;
	public static final short JFFS2_MAGIC_BITMASK = (short)0x1985;
	
	// For detecting wrong-endian fs
	public static final short KSAMTIB_CIGAM_2SFFJ = (short)0x8519;
	public static final short JFFS2_EMPTY_BITMASK = (short)0xffff;
	public static final short JFFS2_DIRTY_BITMASK = (short)0x0000;
	
	public static final int JFFS2_SUM_MAGIC = 0x02851885;
	
	public static final byte JFFS2_MAX_NAME_LEN = (byte)254;
	public static final byte JFFS2_MIN_DATA_LEN = (byte)128;
	
	public static final byte JFFS2_COMPR_NONE = (byte)0;
	public static final byte JFFS2_COMPR_ZERO = (byte)1;
	public static final byte JFFS2_COMPR_RTIME = (byte)2;
	public static final byte JFFS2_COMPR_RUBINMIPS = (byte)3;
	public static final byte JFFS2_COPY = (byte)4;
	public static final byte JFFS2_COMPR_DYNRUBIN = (byte)5;
	public static final byte JFFS2_COMPR_ZLIB = (byte)6;
	
	public static final short JFFS2_COMPAT_MASK = (short)0xc000;
	public static final short JFFS2_NODE_ACCURATE = (short)0x2000;
	
	// Features
	public static final short JFFS2_FEATURE_INCOMPAT = (short)0xc000;
	public static final short JFFS2_FEATURE_ROCOMPAT = (short)0x8000;
	public static final short JFFS2_FEATURE_RWCOMPAT_COPY = (short)0x4000;
	public static final short JFFS2_FEATURE_RWCOMPAT_DELETE = (short)0x0000;
	
	// Node types
	public static final short JFFS2_NODETYPE_DIRENT =
		(short)(JFFS2_FEATURE_INCOMPAT | JFFS2_NODE_ACCURATE | 1);
	
	public static final short JFFS2_NODETYPE_INODE =
		(short)(JFFS2_FEATURE_INCOMPAT | JFFS2_NODE_ACCURATE | 2);
	
	public static final short JFFS2_NODETYPE_CLEANMARKER =
		(short)(JFFS2_FEATURE_RWCOMPAT_DELETE | JFFS2_NODE_ACCURATE | 3);
	
	public static final short JFFS2_NODETYPE_PADDING =
		(short)(JFFS2_FEATURE_RWCOMPAT_DELETE | JFFS2_NODE_ACCURATE | 4);
	
	public static final short JFFS2_NODETYPE_SUMMARY =
		(short)(JFFS2_FEATURE_RWCOMPAT_DELETE | JFFS2_NODE_ACCURATE | 6);
	
	public static final short JFFS2_NODETYPE_XATTR =
		(short)(JFFS2_FEATURE_INCOMPAT | JFFS2_NODE_ACCURATE | 8);
	
	public static final short JFFS2_NODETYPE_XREF =
		(short)(JFFS2_FEATURE_INCOMPAT | JFFS2_NODE_ACCURATE | 9);
	
	// XATTR Related
	public static final byte JFFS2_XPREFIX_USER = (byte)1;
	public static final byte JFFS2_XPREFIX_SECURITY = (byte)2;
	public static final byte JFFS2_XPREFIX_ACL_ACCESS = (byte)3;
	public static final byte JFFS2_XPREFIX_ACL_DEFAULT = (byte)4;
	public static final byte JFFS2_XPREFIX_TRUSTED = (byte)5;
	
	public static final short JFFS2_ACL_VERSION = (short)0x0001;
	
	public static final byte JFFS2_INO_FLAG_PREREAD = (byte)1;
	public static final byte JFFS2_INO_FLAG_USERCOMPR = (byte)2;
	
	private boolean endianKnown = false;
	private boolean littleEndian = false;
	
	// These classes are used to read the system into some temporary storage
	private static class BasicNode
	{
		public int fs_offset; // Offset of this node in the filesystem image
		
		// Header size = 12 bytes
		public short magic;
		public short nodetype;
		public int totlen;
		public int hdr_crc;
		
		// This throws IOException simply as a way to encourage the other
		// constructors to throw it
		// Note: offset is AFTER reading the other parameters of this method
		public BasicNode(int offset, short magic, short nodetype, int totlen,
		                 int hdr_crc)
			throws IOException
		{
			// Need to subtract the length of the other parameters, so the offset will point at the
			//   beginning of the node
			this.fs_offset = offset - 12;
			
			this.magic = magic;
			this.nodetype = nodetype;
			this.totlen = totlen;
			this.hdr_crc = hdr_crc;
		}
		
		public static BasicNode readNode(byte[] image, int offset,
		                                 boolean littleEndian)
			throws IOException
		{
			short magic = Conversion.bytesToShort(image, offset);
			offset += 2;
			
			short nodetype = Conversion.bytesToShort(image, offset);
			offset += 2;
			
			int totlen = Conversion.bytesToInt(image, offset);
			offset += 4;
			
			int hdr_crc = Conversion.bytesToInt(image, offset);
			offset += 4;
			
			if ( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) {
				switch (nodetype) {
					case JFFS2_NODETYPE_DIRENT:
						return new RawDirent(magic, nodetype, totlen, hdr_crc,
						                     image, offset, littleEndian);
						
					case JFFS2_NODETYPE_INODE:
						return new RawInode(magic, nodetype, totlen, hdr_crc,
						                    image, offset, littleEndian);
						
					case JFFS2_NODETYPE_SUMMARY:
						return new RawSummary(magic, nodetype, totlen, hdr_crc,
						                      image, offset, littleEndian);
						
					case JFFS2_NODETYPE_XATTR:
						return new RawXAttr(magic, nodetype, totlen, hdr_crc,
						                    image, offset, littleEndian);
						
					case JFFS2_NODETYPE_XREF:
						return new RawXRef(magic, nodetype, totlen, hdr_crc,
						                   image, offset, littleEndian);
						
					case JFFS2_NODETYPE_CLEANMARKER:
					case JFFS2_NODETYPE_PADDING:
						return null;
						
					default:
						throw new IOException("Not a recognized node type: " +
						                      nodetype);
				}
			}
			else if (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ) {
				// Filesystem is little-endian
				// Reverse these, but leave the magic number as-is so the
				// lower-level routine can detect it
				nodetype = Conversion.reverse(nodetype);
				totlen = Conversion.reverse(totlen);
				hdr_crc = Conversion.reverse(hdr_crc);
				
				switch (nodetype) {
					case JFFS2_NODETYPE_DIRENT:
						return new RawDirent(magic, nodetype, totlen, hdr_crc,
						                     image, offset, littleEndian);
						
					case JFFS2_NODETYPE_INODE:
						return new RawInode(magic, nodetype, totlen, hdr_crc,
						                    image, offset, littleEndian);
						
					case JFFS2_NODETYPE_SUMMARY:
						return new RawSummary(magic, nodetype, totlen, hdr_crc,
						                      image, offset, littleEndian);
						
					case JFFS2_NODETYPE_XATTR:
						return new RawXAttr(magic, nodetype, totlen, hdr_crc,
						                    image, offset, littleEndian);
						
					case JFFS2_NODETYPE_XREF:
						return new RawXRef(magic, nodetype, totlen, hdr_crc,
						                   image, offset, littleEndian);
						
					case JFFS2_NODETYPE_CLEANMARKER:
					case JFFS2_NODETYPE_PADDING:
						return null;
						
					default:
						throw new IOException(String.format("Not a recognized node type at 0x%x: 0x%x (%d)",
						                                    offset,
						                                    nodetype, nodetype));
				}
			}
			else {
				assert(( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) ||
				       (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ));
				
				return null; // Make the compiler happy
			}
		}
	}
	
	private static class RawDirent
		extends BasicNode
	{
		// nodetype = JFFS2_NODETYPE_DIRENT
		// Header size = 28 bytes
		public int pino; // Parent inode number
		public int version;
		public int ino; // zero for unlink
		public int mctime; // modification/change time
		public byte nsize; // name length
		public byte type; // see FSObject.DT_*
		public byte[] unused = new byte[2];
		public int node_crc;
		public int name_crc;
		
		public String name;
		
		public RawDirent(short magic, short nodetype, int totlen, int hdr_crc,
		                 byte[] image, int offset, boolean littleEndian)
			throws IOException
		{
			super(offset, magic, nodetype, totlen, hdr_crc);
			
			if ( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) {
				this.pino = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.version = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.ino = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.mctime = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.nsize = image[offset];
				offset += 1;
				
				this.type = image[offset];
				offset += 1;
				
				this.unused[0] = image[offset];
				offset += 1;
				
				this.unused[1] = image[offset];
				offset += 1;
				
				this.node_crc = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.name_crc = Conversion.bytesToInt(image, offset);
				offset += 4;
			}
			else if (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ) {
				this.pino = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.version = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.ino = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.mctime = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.nsize = image[offset];
				offset += 1;
				
				this.type = image[offset];
				offset += 1;
				
				this.unused[0] = image[offset];
				offset += 1;
				
				this.unused[1] = image[offset];
				offset += 1;
				
				this.node_crc = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.name_crc = Conversion.bytesToIntLE(image, offset);
				offset += 4;
			}
			else {
				assert(( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) ||
				       (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ));
			}
			
			this.name = new String(image, offset, nsize);
			offset += nsize;
		}
		
		@Override
		public String toString()
		{
			return name;
		}
	}
	
	private static class RawInode
		extends BasicNode
	{
		// nodetype = JFFS2_NODETYPE_INODE
		// Header = 56 bytes
		public int ino; // inode number
		public int version;
		public int mode; // The file's type or mode
		public short uid; // The file's owner
		public short gid; // The file's group
		public int isize; // Total resultant size of this inode, for truncations
		public int atime; // Last access time
		public int mtime; // Last modification time
		public int ctime; // Change time
		public int offset; // Where to begin to write
		public int csize; // (Compressed) data size
		public int dsize; // Size of the node's data (after decompression)
		public byte compr; // Compression algorithm used
		public byte usercompr; // Compression algorithm requested by the user
		public short flags; // See JFFS2_INO_FLAG_*
		public int data_crc; // CRC for the (compressed) data
		public int node_crc; // CRC for the raw inode (excluding data)
		
		public byte[] data;
		
		public RawInode(short magic, short nodetype, int totlen, int hdr_crc,
		                byte[] image, int offset, boolean littleEndian)
			throws IOException
		{
			super(offset, magic, nodetype, totlen, hdr_crc);
			
			if ( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) {
				this.ino = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.version = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.mode = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.uid = Conversion.bytesToShort(image, offset);
				offset += 2;
				
				this.gid = Conversion.bytesToShort(image, offset);
				offset += 2;
				
				this.isize = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.atime = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.mtime = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.ctime = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.offset = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.csize = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.dsize = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.compr = image[offset];
				offset += 1;
				
				this.usercompr = image[offset];
				offset += 1;
				
				this.flags = Conversion.bytesToShort(image, offset);
				offset += 2;
				
				this.data_crc = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.node_crc = Conversion.bytesToInt(image, offset);
				offset += 4;
			}
			else if (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ) {
				this.ino = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.version = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.mode = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.uid = Conversion.bytesToShortLE(image, offset);
				offset += 2;
				
				this.gid = Conversion.bytesToShortLE(image, offset);
				offset += 2;
				
				this.isize = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.atime = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.mtime = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.ctime = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.offset = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.csize = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.dsize = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.compr = image[offset];
				offset += 1;
				
				this.usercompr = image[offset];
				offset += 1;
				
				this.flags = Conversion.bytesToShortLE(image, offset);
				offset += 2;
				
				this.data_crc = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.node_crc = Conversion.bytesToIntLE(image, offset);
				offset += 4;
			}
			else {
				assert(( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) ||
				       (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ));
			}
			
			if (this.compr == JFFS2_COMPR_NONE) {
				this.data = new byte[this.dsize];
				System.arraycopy(image, offset, this.data, 0, this.dsize);
			}
			else if (this.compr == JFFS2_COMPR_ZLIB) {
				// NOTE: Assuming this will work, since zipped files are
				// compatible across platforms
				ZInputStream inp =
					new ZInputStream(new ByteArrayInputStream(image, offset,
					                                          this.csize));
				this.data = Utils.bytesToEOF(inp);
				offset += this.csize;
			}
			else if (this.compr == JFFS2_COMPR_RTIME) {
				this.data = RLE.decode(image, offset, this.csize);
				offset += this.csize;
			}
			else {
				throw new IOException("Unrecognized compression method: " +
				                      this.compr);
			}
		}
	}
	
	private static class RawXAttr
		extends BasicNode
	{
		// nodetype = JFFS2_NODETYPE_XATTR
		public int xid; // XATTR identifier number
		public int version;
		public byte xprefix;
		public byte name_len;
		public short value_len;
		public int data_crc;
		public int node_crc;
		
		public byte[] data;
		
		public RawXAttr(short magic, short nodetype, int totlen, int hdr_crc,
		                byte[] image, int offset, boolean littleEndian)
			throws IOException
		{
			super(offset, magic, nodetype, totlen, hdr_crc);
			
			if ( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) {
				this.xid = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.version = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.xprefix = image[offset];
				offset += 1;
				
				this.name_len = image[offset];
				offset += 1;
				
				this.value_len = Conversion.bytesToShort(image, offset);
				offset += 2;
				
				this.data_crc = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.node_crc = Conversion.bytesToInt(image, offset);
				offset += 4;
			}
			else if (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ) {
				this.xid = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.version = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.xprefix = image[offset];
				offset += 1;
				
				this.name_len = image[offset];
				offset += 1;
				
				this.value_len = Conversion.bytesToShortLE(image, offset);
				offset += 2;
				
				this.data_crc = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.node_crc = Conversion.bytesToIntLE(image, offset);
				offset += 4;
			}
			else {
				assert(( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) ||
				       (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ));
			}
			
			this.data = new byte[this.value_len];
			System.arraycopy(image, offset, this.data, 0, this.value_len);
			offset += this.value_len;
		}
	}
	
	private static class RawXRef
		extends BasicNode
	{
		// nodetype = JFFS2_NODETYPE_XREF
		public int ino; // inode number
		public int xid; // XATTR identifier number
		public int xseqno; // xref sequential number
		public int node_crc;
		
		public RawXRef(short magic, short nodetype, int totlen, int hdr_crc,
		               byte[] image, int offset, boolean littleEndian)
			throws IOException
		{
			super(offset, magic, nodetype, totlen, hdr_crc);
			
			if ( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) {
				this.ino = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.xid = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.xseqno = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.node_crc = Conversion.bytesToInt(image, offset);
				offset += 4;
			}
			else if (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ) {
				this.ino = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.xid = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.xseqno = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.node_crc = Conversion.bytesToIntLE(image, offset);
				offset += 4;
			}
			else {
				assert(( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) ||
				       (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ));
			}
		}
	}
	
	private static class RawSummary
		extends BasicNode
	{
		// nodetype = JFFS2_NODETYPE_SUMMARY
		public int sum_num; // Number of sum entries
		public int cln_mkr; // Clean marker size, 0 = no cleanmarker
		public int padded; // Sum of the size of padding nodes
		public int sum_crc; // Summary information crc
		public int node_crc; // Node crc
		
		public int[] sum; // inode summary info
		
		public RawSummary(short magic, short nodetype, int totlen, int hdr_crc,
		                  byte[] image, int offset, boolean littleEndian)
			throws IOException
		{
			super(offset, magic, nodetype, totlen, hdr_crc);
			
			if ( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) {
				this.sum_num = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.cln_mkr = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.padded = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.sum_crc = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.node_crc = Conversion.bytesToInt(image, offset);
				offset += 4;
				
				this.sum = new int[this.sum_num];
				for (int i = 0; i < this.sum_num; i++) {
					this.sum[i] = Conversion.bytesToInt(image, offset);
					offset += 4;
				}
			}
			else if (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ) {
				this.sum_num = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.cln_mkr = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.padded = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.sum_crc = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.node_crc = Conversion.bytesToIntLE(image, offset);
				offset += 4;
				
				this.sum = new int[this.sum_num];
				for (int i = 0; i < this.sum_num; i++) {
					this.sum[i] = Conversion.bytesToIntLE(image, offset);
					offset += 4;
				}
			}
			else {
				assert(( ! littleEndian && magic == JFFS2_MAGIC_BITMASK) ||
				       (littleEndian && magic == KSAMTIB_CIGAM_2SFFJ));
			}
		}
	}
	
	private Directory rootDir;
	
	public JFFS2(RandomAccessFile imageFile)
		throws IOException
	{
		readTree(imageFile);
	}
	
	public JFFS2(InputStream stream)
		throws IOException
	{
		readTree(stream);
	}
	
	public JFFS2(byte[] data)
		throws IOException
	{
		readTree(data);
	}
	
	public void toStream(OutputStream out)
		throws IOException
	{
		toStream(out,
		         D_LITTLE_ENDIAN,
		         D_ADD_CLEAN_MARKERS,
		         D_CLEAN_MARKER_SIZE,
		         D_ERASE_BLOCK_SIZE,
		         D_PAGE_SIZE);
	}
	
	public void toStream(OutputStream out, boolean littleEndian,
	                     boolean addCleanMarkers, int cleanMarkerSize,
	                     int eraseBlockSize, int pageSize)
		throws IOException
	{
		if (eraseBlockSize < 0x2000) {
			throw new IllegalArgumentException(String.format("Erase block size 0x%x too small, must be at least 0x2000",
			                                                 eraseBlockSize));
		}
		
		if (addCleanMarkers) {
			if (cleanMarkerSize < 12) {
				throw new IllegalArgumentException("Clean marker size must be at least 12");
			}
			else if (cleanMarkerSize >= eraseBlockSize) {
				throw new IllegalArgumentException("Clean marker size must be less than erase block size");
			}
		}
		
		// Only build one clean-marker
		byte[] cleanMarker;
		if (addCleanMarkers) {
			cleanMarker = new byte[cleanMarkerSize];
			Arrays.fill(cleanMarker, (byte)0xff);
			
			if (littleEndian) {
				Conversion.shortToBytes(cleanMarker, 0, KSAMTIB_CIGAM_2SFFJ);
				Conversion.shortToBytesLE(cleanMarker, 2, JFFS2_NODETYPE_CLEANMARKER);
				Conversion.intToBytesLE(cleanMarker, 4, cleanMarkerSize);
				Conversion.intToBytesLE(cleanMarker, 8, Utils.crc32(cleanMarker, 0, 8));
			}
			else {
				Conversion.shortToBytes(cleanMarker, 0, JFFS2_MAGIC_BITMASK);
				Conversion.shortToBytes(cleanMarker, 2, JFFS2_NODETYPE_CLEANMARKER);
				Conversion.intToBytes(cleanMarker, 4, cleanMarkerSize);
				Conversion.intToBytes(cleanMarker, 8, Utils.crc32(cleanMarker, 0, 8));
			}
		}
		else {
			cleanMarker = new byte[0];
		}
		
		WriteOptions opt = new WriteOptions(new DataOutputStream(out),
		                                    littleEndian,
		                                    cleanMarker,
		                                    eraseBlockSize,
		                                    pageSize);
		
		// There is no dirent or inode for the root directory, but remember that
		// it counts as having inode #1 (other objects need to know that)
		opt.inodes.put(getRoot(), 1);
		
		for (FSObject obj : getRoot()) {
			toStream(obj, opt);
		}
	}
	
	private void toStream(FSObject obj, WriteOptions opt)
		throws IOException
	{
		byte[] body = obj.getName().getBytes();
		
		byte[] header = new byte[40 + body.length];
		
		// Allocate an inode
		opt.inodes.put(obj, opt.inodes.size() + 1);
		
		int offset = 0;
		if (opt.littleEndian) {
			// short: magic
			Conversion.shortToBytes(header, offset, KSAMTIB_CIGAM_2SFFJ);
			offset += 2;
			
			// short: nodetype
			Conversion.shortToBytesLE(header, offset, JFFS2_NODETYPE_DIRENT);
			offset += 2;
			
			// int: totlen (header + name)
			Conversion.intToBytesLE(header, offset, header.length + body.length);
			offset += 4;
			
			// int: header crc
			Conversion.intToBytesLE(header, offset, Utils.crc32(header, 0, 8));
			offset += 4;
			
			// int: Parent inode
			Conversion.intToBytesLE(header, offset, opt.inodes.get(obj.getParent()));
			offset += 4;
			
			// int: Version (always 1 for a fresh filesystem)
			// Alternately, we could count up as we create each dirent, but
			// there is no real benefit
			Conversion.intToBytesLE(header, offset, 1);
			offset += 4;
			
			// int: INode number
			Conversion.intToBytesLE(header, offset, opt.inodes.get(obj));
			offset += 4;
			
			// int: Modification time
			Conversion.intToBytesLE(header, offset, obj.getModTime());
			offset += 4;
			
			// byte: Length of the name
			if (body.length > 255) {
				throw new IllegalArgumentException("Length of name of " +
				                                   obj.getPath() +
				                                   " is too long, max is 255 bytes");
			}
			header[offset] = (byte)(body.length & 0xff);
			offset += 1;
			
			// byte: Object type (see FSObject.DT_*)
			header[offset] = obj.getType();
			offset += 1;
			
			// short: Unused (0)
			Conversion.shortToBytesLE(header, offset, (short)0);
			offset += 2;
			
			// int: Node CRC
			Conversion.intToBytesLE(header, offset,
			                        Utils.crc32(header, 0, header.length - 8));
			offset += 4;
			
			// int: Name CRC
			Conversion.intToBytesLE(header, offset,
			                        Utils.crc32(body, 0, body.length));
			offset += 4;
		}
		else {
			// short: magic
			Conversion.shortToBytes(header, offset, JFFS2_MAGIC_BITMASK);
			offset += 2;
			
			// short: nodetype
			Conversion.shortToBytes(header, offset, JFFS2_NODETYPE_DIRENT);
			offset += 2;
			
			// int: totlen (header + name)
			Conversion.intToBytes(header, offset, header.length + body.length);
			offset += 4;
			
			// int: header crc
			Conversion.intToBytes(header, offset, Utils.crc32(header, 0, 8));
			offset += 4;
			
			// int: Parent inode
			Conversion.intToBytes(header, offset, opt.inodes.get(obj.getParent()));
			offset += 4;
			
			// int: Version (always 1 for a fresh filesystem)
			// Alternately, we could count up as we create each dirent, but
			// there is no real benefit
			Conversion.intToBytes(header, offset, 1);
			offset += 4;
			
			// int: INode number
			Conversion.intToBytes(header, offset, opt.inodes.get(obj));
			offset += 4;
			
			// int: Modification time
			Conversion.intToBytes(header, offset, obj.getModTime());
			offset += 4;
			
			// byte: Length of the name
			if (body.length > 255) {
				throw new IllegalArgumentException("Length of name of " +
				                                   obj.getPath() +
				                                   " is too long, max is 255 bytes");
			}
			header[offset] = (byte)(body.length & 0xff);
			offset += 1;
			
			// byte: Object type (see FSObject.DT_*)
			header[offset] = obj.getType();
			offset += 1;
			
			// short: Unused (0)
			Conversion.shortToBytes(header, offset, (short)0);
			offset += 2;
			
			// int: Node CRC
			Conversion.intToBytes(header, offset,
			                      Utils.crc32(header, 0, header.length - 8));
			offset += 4;
			
			// int: Name CRC
			Conversion.intToBytes(header, offset,
			                      Utils.crc32(body, 0, body.length));
			offset += 4;
		}
		
		padBlock(header.length + body.length, opt);
		
		opt.out.write(header);
		opt.offset += header.length;
		
		opt.out.write(body);
		opt.offset += body.length;
		
		padWord(opt);
		
		// Allow for garbage collection
		header = null;
		
		// Now write the actual object
		writeObject(obj, opt);
	}
	
	private void writeObject(FSObject obj, WriteOptions opt)
		throws IOException
	{
		byte[] header = new byte[68];
		
		byte[] body;
		
		if (obj instanceof File) {
			body = ((File)obj).getData();
		}
		else if (obj instanceof Device) {
			body = new byte[2];
			
			if (opt.littleEndian) {
				body[0] = ((Device)obj).getMinor();
				body[1] = ((Device)obj).getMajor();
			}
			else {
				body[0] = ((Device)obj).getMajor();
				body[1] = ((Device)obj).getMinor();
			}
		}
		else if (obj instanceof Symlink) {
			body = ((Symlink)obj).getLinkName().getBytes();
		}
		else {
			body = new byte[0];
		}
		
		// Fill in the basic information
		// We may need to iterate multiple times to push the entire object out,
		// updating just a couple of properties each time
		int offset = 0;
		int versionOffset, dataOffsetOffset, csizeOffset, dsizeOffset,
		    calgoOffset, calgoUserOffset, dataCRCOffset, headerCRCOffset;
		
		if (opt.littleEndian) {
			// short: magic
			Conversion.shortToBytes(header, offset, KSAMTIB_CIGAM_2SFFJ);
			offset += 2;
			
			// short: nodetype
			Conversion.shortToBytesLE(header, offset, JFFS2_NODETYPE_INODE);
			offset += 2;
			
			// int: totlen (header + data)
			Conversion.intToBytesLE(header, offset, header.length);
			offset += 4;
			
			// int: header crc
			Conversion.intToBytesLE(header, offset, Utils.crc32(header, 0, 8));
			offset += 4;
			
			// int: INode number
			Conversion.intToBytesLE(header, offset, opt.inodes.get(obj));
			offset += 4;
			
			// int: Version
			// Done later
			versionOffset = offset;
			offset += 4;
			
			// int: Mode
			Conversion.intToBytesLE(header, offset, obj.getMode());
			offset += 4;
			
			// short: UID
			Conversion.shortToBytesLE(header, offset, obj.getUID());
			offset += 2;
			
			// short: GID
			Conversion.shortToBytesLE(header, offset, obj.getGID());
			offset += 2;
			
			// int: Total size of the data portion at this version
			if (obj instanceof File) {
				Conversion.intToBytesLE(header, offset, body.length);
			}
			else if (obj instanceof Symlink) {
				Conversion.intToBytesLE(header, offset, body.length);
			}
			else {
				// Although device major/minor are stored in the data portion,
				// they are not included in the total size
				Conversion.intToBytesLE(header, offset, 0);
			}
			offset += 4;
			
			// int: Last access time
			Conversion.intToBytesLE(header, offset, obj.getModTime());
			offset += 4;
			
			// int: Last modification time
			Conversion.intToBytesLE(header, offset, obj.getModTime());
			offset += 4;
			
			// int: Last change time
			Conversion.intToBytesLE(header, offset, obj.getModTime());
			offset += 4;
			
			// int: Offset within the file where this version begins to write
			// Done later
			dataOffsetOffset = offset;
			offset += 4;
			
			// int: Compressed data size (this inode only, not total)
			// Done later
			csizeOffset = offset;
			offset += 4;
			
			// int: Decompressed data size (this inode only, not total)
			// Done later
			dsizeOffset = offset;
			offset += 4;
			
			// byte: Compression algorithm
			// Done later
			calgoOffset = offset;
			offset += 1;
			
			// byte: Compression algorithm requested by the user
			// Done later
			calgoUserOffset = offset;
			offset += 1;
			
			// short: Flags
			Conversion.shortToBytesLE(header, 54, (short)0);
			
			// int: CRC over compressed data
			// Done later
			dataCRCOffset = offset;
			offset += 4;
			
			// int: CRC over the inode (excluding data and data CRC)
			// Done later
			headerCRCOffset = offset;
			offset += 4;
		}
		else {
			// short: magic
			Conversion.shortToBytes(header, offset, JFFS2_MAGIC_BITMASK);
			offset += 2;
			
			// short: nodetype
			Conversion.shortToBytes(header, offset, JFFS2_NODETYPE_INODE);
			offset += 2;
			
			// int: totlen (header + data)
			Conversion.intToBytes(header, offset, header.length);
			offset += 4;
			
			// int: header crc
			Conversion.intToBytes(header, offset, Utils.crc32(header, 0, 8));
			offset += 4;
			
			// int: INode number
			Conversion.intToBytes(header, offset, opt.inodes.get(obj));
			offset += 4;
			
			// int: Version
			// Done later
			versionOffset = offset;
			offset += 4;
			
			// int: Mode
			Conversion.intToBytes(header, offset, obj.getMode());
			offset += 4;
			
			// short: UID
			Conversion.shortToBytes(header, offset, obj.getUID());
			offset += 2;
			
			// short: GID
			Conversion.shortToBytes(header, offset, obj.getGID());
			offset += 2;
			
			// int: Total size of the data portion at this version
			if (obj instanceof File) {
				Conversion.intToBytes(header, offset, body.length);
			}
			else if (obj instanceof Symlink) {
				Conversion.intToBytes(header, offset, body.length);
			}
			else {
				// Although device major/minor are stored in the data portion,
				// they are not included in the total size
				Conversion.intToBytes(header, offset, 0);
			}
			offset += 4;
			
			// int: Last access time
			Conversion.intToBytes(header, offset, obj.getModTime());
			offset += 4;
			
			// int: Last modification time
			Conversion.intToBytes(header, offset, obj.getModTime());
			offset += 4;
			
			// int: Last change time
			Conversion.intToBytes(header, offset, obj.getModTime());
			offset += 4;
			
			// int: Offset within the file where this version begins to write
			// Done later
			dataOffsetOffset = offset;
			offset += 4;
			
			// int: Compressed data size (this inode only, not total)
			// Done later
			csizeOffset = offset;
			offset += 4;
			
			// int: Decompressed data size (this inode only, not total)
			// Done later
			dsizeOffset = offset;
			offset += 4;
			
			// byte: Compression algorithm
			// Done later
			calgoOffset = offset;
			offset += 1;
			
			// byte: Compression algorithm requested by the user
			// Done later
			calgoUserOffset = offset;
			offset += 1;
			
			// short: Flags
			Conversion.shortToBytes(header, 54, (short)0);
			
			// int: CRC over compressed data
			// Done later
			dataCRCOffset = offset;
			offset += 4;
			
			// int: CRC over the inode (excluding data and data CRC)
			// Done later
			headerCRCOffset = offset;
			offset += 4;
		}
		
		// Split data up into pages, but we need to write at least one copy
		int dataOffset = 0;
		int version = 0;
		while (dataOffset < body.length || version == 0) {
			// Figure out how many bytes to write
			// Pad as necessary
			padBlock(header.length + JFFS2_MIN_DATA_LEN, opt);
			
			int length = body.length - dataOffset;
			
			// The most we can write at once is one page
			if (length > opt.pageSize) {
				length = opt.pageSize;
			}
			
			// But we may need to cut it down some more to fit into an erase
			// block; we'll figure that out when we try to run the compression
			int space = opt.eraseBlockSize -
			            (opt.offset % opt.eraseBlockSize) -
			            header.length;
			
			byte[] compressed;
			byte compression;
			
			// Choose a compression algorithm for this section
			if (body.length - dataOffset > 0) {
				byte[] rleCompressed = RLE.encode(body, dataOffset, length);
				
				ByteArrayOutputStream boutp = new ByteArrayOutputStream();
				ZOutputStream outp = new ZOutputStream(boutp);
				outp.write(body, dataOffset, length);
				byte[] zCompressed = boutp.toByteArray();
				
				// Which is smallest?
				if (rleCompressed.length < length &&
				    rleCompressed.length < zCompressed.length) {
					
					// RLE is smallest
					compressed = rleCompressed;
					compression = JFFS2_COMPR_RTIME;
				}
				else if (zCompressed.length < length &&
				         zCompressed.length < rleCompressed.length) {
					
					// ZLib is smallest
					compressed = zCompressed;
					compression = JFFS2_COMPR_ZLIB;
				}
				else {
					// No compression is smallest
					compressed = new byte[length];
					System.arraycopy(body, dataOffset, compressed, 0, length);
					compression = JFFS2_COMPR_NONE;
				}
				
				// Make sure the compressed data fits into the erase block space
				if (compressed.length > space) {
					// It does not
					// We could keep trying to compress smaller amounts of data
					// until it does fit, but that could take a while, so just
					// go with no compression and fit everything we can
					length = space;
					compressed = new byte[length];
					System.arraycopy(body, dataOffset, compressed, 0, length);
					compression = JFFS2_COMPR_NONE;
				}
			}
			else {
				compressed = new byte[0]; // No data
				compression = JFFS2_COMPR_NONE;
			}
			
			if (littleEndian) {
				// int: Version
				Conversion.intToBytesLE(header, versionOffset, ++version);
				
				// int: Offset within the file where this version begins to write
				Conversion.intToBytesLE(header, dataOffsetOffset, dataOffset);
				
				// int: Compressed data size (this inode only, not total)
				Conversion.intToBytesLE(header, csizeOffset, compressed.length);
				
				// int: Decompressed data size (this inode only, not total)
				Conversion.intToBytesLE(header, dsizeOffset, length);
				
				// byte: Compression algorithm
				header[calgoOffset] = compression;
				
				// byte: Compression algorithm requested by the user
				header[calgoUserOffset] = JFFS2_COMPR_NONE;
				
				// int: CRC over compressed data
				Conversion.intToBytesLE(header, dataCRCOffset,
				                        Utils.crc32(compressed, 0, compressed.length));
				
				// int: CRC over the inode (excluding data and data CRC)
				Conversion.intToBytesLE(header, headerCRCOffset,
				                        Utils.crc32(header, 0, header.length - 8));
			}
			else {
				// int: Version
				Conversion.intToBytes(header, versionOffset, ++version);
				
				// int: Offset within the file where this version begins to write
				Conversion.intToBytes(header, dataOffsetOffset, dataOffset);
				
				// int: Compressed data size (this inode only, not total)
				Conversion.intToBytes(header, csizeOffset, compressed.length);
				
				// int: Decompressed data size (this inode only, not total)
				Conversion.intToBytes(header, dsizeOffset, length);
				
				// byte: Compression algorithm
				header[calgoOffset] = compression;
				
				// byte: Compression algorithm requested by the user
				header[calgoUserOffset] = JFFS2_COMPR_NONE;
				
				// int: CRC over compressed data
				Conversion.intToBytes(header, dataCRCOffset,
				                      Utils.crc32(compressed, 0, compressed.length));
				
				// int: CRC over the inode (excluding data and data CRC)
				Conversion.intToBytes(header, headerCRCOffset,
				                      Utils.crc32(header, 0, header.length - 8));
			}
			
			// Actually write this inode
			// We've already padded to the end of the erase block as necessary
			opt.out.write(header);
			opt.out.write(compressed);
			padWord(opt);
			
			dataOffset += length;
		}
	}
	
	/**
	 * If there is not enough space left in the current erase block to write a
	 * node with the specified number of bytes, pad out to the end of the erase
	 * block so we can begin a new one.
	 * 
	 * @param bytesToWrite The number of bytes which are waiting to be written.
	 * @param opt The structure containing write options.
	 * @throws IOException If there is a problem writing to the stream.
	 */
	private void padBlock(int bytesToWrite, WriteOptions opt)
		throws IOException
	{
		if (opt.offset % opt.eraseBlockSize == 0) {
			// We are at the very beginning of a new erase block, write a
			// clean-marker
			opt.out.write(opt.cleanMarker);
			opt.offset += opt.cleanMarker.length;
			
			padWord(opt);
		}
		
		if ((opt.offset % opt.eraseBlockSize) + bytesToWrite > opt.eraseBlockSize) {
			// Trying to write too many bytes to this erase block, pad to the
			// end and begin a new one
			padBlock(opt);
		}
		
		if (opt.offset % opt.eraseBlockSize == 0) {
			// Same test as before, are we at the very beginning of an erase
			// block?
			opt.out.write(opt.cleanMarker);
			opt.offset += opt.cleanMarker.length;
			
			padWord(opt);
		}
	}
	
	/**
	 * Pad to the end of the current 32-bit word.
	 * 
	 * @param opt The write options structure.
	 * @throws IOException If there is a problem writing to the stream.
	 */
	private void padWord(WriteOptions opt)
		throws IOException
	{
		while (opt.offset % 4 != 0) {
			if (opt.ffBuf.length >= opt.offset % 4) {
				opt.out.write(opt.ffBuf, 0, opt.offset % 4);
				opt.offset += opt.offset % 4;
			}
			else {
				opt.out.write(opt.ffBuf);
				opt.offset += opt.ffBuf.length;
			}
		}
	}
	
	/**
	 * Pad to the end of the current erase block.
	 * 
	 * @param opt The write options structure.
	 * @throws IOException If there is a problem writing to the stream.
	 */
	private void padBlock(WriteOptions opt)
		throws IOException
	{
		while (opt.offset % opt.eraseBlockSize != 0) {
			if (opt.ffBuf.length >= opt.offset % opt.eraseBlockSize) {
				opt.out.write(opt.ffBuf, 0, opt.offset % opt.eraseBlockSize);
				opt.offset += opt.offset % opt.eraseBlockSize;
			}
			else {
				opt.out.write(opt.ffBuf);
				opt.offset += opt.ffBuf.length;
			}
		}
	}
	
	/** Get the root directory of this JFFS2 image. */
	public Directory getRoot()
	{
		return rootDir;
	}
	
	/** Check whether this JFFS2 image is little-endian. */
	public boolean isLittleEndian()
	{
		return littleEndian;
	}
	
	protected void readTree(RandomAccessFile imageFile)
		throws IOException
	{
		if (imageFile.length() > Integer.MAX_VALUE) {
			throw new IOException("Image file too large to handle");
		}
		
		byte[] image = new byte[(int)imageFile.length()];
		int bytesRead, totalRead = 0;
		while ((bytesRead = imageFile.read(image, totalRead,
		                                   image.length - totalRead)) > 0) {
			totalRead += bytesRead;
		}
		
		readTree(image);
	}
	
	protected void readTree(InputStream stream)
		throws IOException
	{
		byte[] image = Utils.bytesToEOF(stream);
		
		readTree(image);
	}
	
	protected void readTree(byte[] fsimage)
		throws IOException
	{
		// Inode number --> Dirent
		Map<Integer, RawDirent> dirents = new Hashtable<Integer, RawDirent>();
		
		// Inode number --> List of Inode versions in random order
		Map<Integer, List<RawInode>> inodes =
			new Hashtable<Integer, List<RawInode>>();
		
		// Scan the entire image, cataloging each node we find
		int offset = 0;
		while (offset < fsimage.length) {
			if (endianKnown) {
				while (offset < fsimage.length &&
				       (( ! littleEndian &&
				         Conversion.bytesToShort(fsimage, offset) != JFFS2_MAGIC_BITMASK) ||
				        (littleEndian &&
				         Conversion.bytesToShort(fsimage, offset) != KSAMTIB_CIGAM_2SFFJ))) {
					
					// Throw away two extra bytes, the magic number will be
					// 4-byte aligned
					offset += 4;
				}
			}
			else {
				// Don't know which endian yet
				while (offset < fsimage.length &&
				       Conversion.bytesToShort(fsimage, offset) != JFFS2_MAGIC_BITMASK &&
				       Conversion.bytesToShort(fsimage, offset) != KSAMTIB_CIGAM_2SFFJ) {
					
					// Throw away two extra bytes, the magic number will be
					// 4-byte aligned
					offset += 4;
				}
				
				if (offset < fsimage.length) {
					endianKnown = true;
					
					if (Conversion.bytesToShort(fsimage, offset) == JFFS2_MAGIC_BITMASK) {
						littleEndian = false;
					}
					else {
						littleEndian = true;
					}
				}
			}
			
			if (offset < fsimage.length) {
				// Found a node, read and catalog it
				BasicNode node = BasicNode.readNode(fsimage, offset, littleEndian);
				
				if (node != null) {
					if (node instanceof RawDirent) {
						RawDirent dirent = (RawDirent)node;
						dirents.put(dirent.ino, dirent);
					}
					else if (node instanceof RawInode) {
						RawInode inode = (RawInode)node;
						
						List<RawInode> list = inodes.get(inode.ino);
						if (list == null) {
							list = new LinkedList<RawInode>();
							inodes.put(inode.ino, list);
						}
						list.add(inode);
					}
					else {
						// Don't know how to deal with this type, just ignore it
					}
					
					offset += node.totlen; // Skip over this node and continue searching
					offset = (offset + 3) & ~0x03; // Make sure we're aligned on the next 4-byte block
				}
				else {
					// Weird... matching header, but not a node? Just keep searching...
					offset += 4;
				}
			}
		}
		
		parseTree(dirents, inodes);
	}
	
	// Examines the raw storage and builds a coherent filesystem tree out of it
	// This is destructive, to avoid using too many resources (it has a tendency
	// to gobble the heap)
	private void parseTree(Map<Integer, RawDirent> dirents, Map<Integer,
	                       List<RawInode> > inodes)
	{
		// Loop through dirents, creating the necessary structures for each as
		// we go
		GenericFSObjectFactory factory = new GenericFSObjectFactory();
		
		// This is for objects that reference other objects that don't exist yet
		// Easiest way to do this is to start out with everything in 'retry',
		// then loop until it's empty
		List<Integer> retry = new Vector<Integer>(dirents.keySet());
		
		// INode number --> Directory
		Map<Integer, Directory> directories = new Hashtable<Integer, Directory>();
		
		// This is used for resolving links (don't put links in here)
		// Path --> FSObject
		Map<String, FSObject> allObjects = new Hashtable<String, FSObject>();
		
		// Since the filesystem does not contain a dirent for "/", create it
		// It will always have an INode number of 1
		rootDir = factory.createDirectory("", (short)0, (short)0, 0775, 0, null);
		directories.put(1, rootDir);
		allObjects.put("/", rootDir);
		
		while (retry.size() > 0) {
			// If no changes are made, break out; there is an orphan somewhere
			boolean change = false;
			
			// Loop through all of the dirents (directory entries point to the
			// inodes for files, directories, block/character specials, ...)
			Iterator<Integer> itInodeNumber = retry.iterator();
			while (itInodeNumber.hasNext()) {
				// Grab the inode number that this dirent points to
				int inodeNumber = itInodeNumber.next();
				
				// Get the dirent associated with it
				RawDirent dirent = dirents.get(inodeNumber);
				
				// Get the parent directory, if we've already processed it
				Directory parent = directories.get(dirent.pino);
				if (parent == null) {
					// Parent hasn't been read yet
					continue;
				}
				
				// Get a list of the versions of this inode
				List<RawInode> inodeList = inodes.get(inodeNumber);
				
				// Has the most recent ownership/permissions
				RawInode inode = findNewest(inodeList);
				
				// Fold data together from older versions
				byte[] inodeData = foldInodes(inodeList);
				
				switch (dirent.type) {
					case FSObject.DT_DIR:
						// It's a directory
						Directory directory =
							factory.createDirectory(dirent.name, inode.uid,
							                        inode.gid, inode.mode,
							                        inode.mtime, parent);
						
						// Put it in the directories map so we can find it if
						// anything links this as its parent
						directories.put(inodeNumber, directory);
						
						// Put it into allObjects so links can find it
						allObjects.put(directory.getPath(), directory);
						
						break;
						
					case FSObject.DT_BLK:
						// It's a block special device file
						BlockDevice blkdev;
						if (dirent.magic == JFFS2_MAGIC_BITMASK) {
							// Big endian (major/minor in the right order)
							blkdev =
								factory.createBlockDevice(dirent.name,
								                          inodeData[0],
								                          inodeData[1],
								                          inode.uid, inode.gid,
								                          inode.mode,
								                          inode.mtime, parent);
						}
						else {
							// Little endian (reverse the major/minor numbers)
							blkdev = factory.createBlockDevice(dirent.name,
							                                   inodeData[1],
							                                   inodeData[0],
							                                   inode.uid,
							                                   inode.gid,
							                                   inode.mode,
							                                   inode.mtime,
							                                   parent);
						}
						
						// Put it into allObjects so links can find it
						allObjects.put(blkdev.getPath(), blkdev);
						
						break;
						
					case FSObject.DT_CHR:
						// It's a character special device file
						CharDevice chdev;
						if (dirent.magic == JFFS2_MAGIC_BITMASK) {
							// Big endian (major/minor in the right order)
							chdev = factory.createCharDevice(dirent.name,
							                                 inodeData[0],
							                                 inodeData[1],
							                                 inode.uid,
							                                 inode.gid,
							                                 inode.mode,
							                                 inode.mtime,
							                                 parent);
						}
						else {
							// Little endian (reverse the major/minor numbers)
							chdev = factory.createCharDevice(dirent.name,
							                                 inodeData[1],
							                                 inodeData[0],
							                                 inode.uid,
							                                 inode.gid,
							                                 inode.mode,
							                                 inode.mtime,
							                                 parent);
						}
						
						// Put it into allObjects so links can find it
						allObjects.put(chdev.getPath(), chdev);
						
						break;
						
					case FSObject.DT_REG:
						// It's a regular file
						File file = factory.createFile(dirent.name, inodeData,
						                               inode.uid, inode.gid,
						                               inode.mode, inode.mtime,
						                               parent);
						
						// Put it in allObjects so links can find it
						allObjects.put(file.getPath(), file);
						
						break;
						
					case FSObject.DT_LNK:
						// It's a link
						Symlink link = factory.createSymlink(dirent.name,
						                                     new String(inodeData),
						                                     inode.uid,
						                                     inode.gid,
						                                     inode.mode,
						                                     inode.mtime,
						                                     parent);
						
						break;
						
					case FSObject.DT_FIFO:
						FIFO fifo = factory.createFIFO(dirent.name, inode.uid,
						                               inode.gid, inode.mode,
						                               inode.mtime, parent);
						
						// Put it in allObjects so links can find it
						allObjects.put(fifo.getPath(), fifo);
						
						break;
						
					case FSObject.DT_SOCK:
						Socket sock = factory.createSocket(dirent.name,
						                                   inode.uid, inode.gid,
						                                   inode.mode,
						                                   inode.mtime, parent);
						
						// Put it in allObjects so links can find it
						allObjects.put(sock.getPath(), sock);
						
						break;
						
					case FSObject.DT_WHT:
						Whiteout wht = factory.createWhiteout(dirent.name,
						                                      inode.uid,
						                                      inode.gid,
						                                      inode.mode,
						                                      inode.mtime,
						                                      parent);
						
						// Put it in allObjects so links can find it
						allObjects.put(wht.getPath(), wht);
						
						break;
						
					case FSObject.DT_UNKNOWN:
						// Ignore this one, but log it
						// It will be automatically removed
						logger.debug(String.format("Object of type UNKNOWN at 0x%08x, inode %d",
						             dirent.fs_offset, dirent.ino));
						
						break;
						
					default:
						// Don't know how to deal with these, and since they
						// aren't readable, who cares?
						// They will be automatically removed
						logger.debug(String.format("Object of unknown type 0x%02x at 0x%08x, inode %d",
						                           dirent.type,
						                           dirent.fs_offset,
						                           dirent.ino));
				}
				
				// If we get here, something changed
				// We finished this entry
				itInodeNumber.remove();
				
				// We can garbage-collect these entries
				dirents.remove(inodeNumber);
				inodes.remove(inodeNumber);
				
				change = true;
			}
			
			if (!change) {
				break;
			}
		}
	}
	
	private byte[] foldInodes(List<RawInode> inodeList)
	{
		// Sort the list
		RawInode[] inodeArray = inodeList.toArray(new RawInode[0]);
		Arrays.sort(inodeArray, new Comparator<RawInode>() {
			public int compare(RawInode o1, RawInode o2)
			{
				return (o1.version - o2.version);
			}
		});
		
		// Figure out the size of the file
		int size;
		if (inodeArray.length == 1) {
			size = inodeArray[0].dsize;
		}
		else {
			size = inodeArray[inodeArray.length - 1].isize;
		}
		
		// Loop through the array, folding together the data from each
		byte[] data = new byte[size];
		for (int i = 0; i < inodeArray.length; i++) {
			// Copy the node data into the data array
			System.arraycopy(inodeArray[i].data, 0,
			                 data, inodeArray[i].offset,
			                 inodeArray[i].dsize);
		}
		
		return data;
	}
	
	private RawInode findNewest(List<RawInode> inodeList)
	{
		RawInode latest = inodeList.get(0);
		for (RawInode inode : inodeList) {
			if (inode.version > latest.version) {
				latest = inode;
			}
		}
		
		return latest;
	}
}

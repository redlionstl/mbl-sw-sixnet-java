/*
 * RLE.java
 *
 * Implements JFFS2's weird RLE scheme.
 * This implements the same algorithm as 'compr_rtime.c' in the MTD-Utils
 * package. Although that code was used to determine the algorithm, it was not
 * consulted while writing this file.
 * 
 * That implementation is licensed under the Red Hat eCos 1.1 license.
 *
 * Jonathan Pearson
 * October 20, 2009
 *
 */

package com.sixnetio.fs.jffs2;


public class RLE
{
	/**
	 * A wrapper for {@link #encode(byte[], int, int)} called as
	 * <tt>encode(input, 0, input.length)</tt>.
	 * 
	 * @param input The data to encode.
	 * @return The compressed version of the data. Note: This may be larger than
	 *   the original input.
	 */
	public static byte[] encode(byte[] input)
	{
		return encode(input, 0, input.length);
	}
	
	/**
	 * Encode a block of data into JFFS2-style RLE-encoded data.
	 * 
	 * @param input The data to encode.
	 * @param start The offset to start reading from in the input array.
	 * @param length The number of bytes from the input array to encode.
	 * @return The compressed version of the data. Note: This may be larger than
	 *   the original input.
	 */
	public static byte[] encode(byte[] input, int start, int length)
	{
		int inpos = start;
		
		int[] oldPositions = new int[256];
		
		// This will be expanded as necessary
		byte[] output = new byte[length];
		int outpos = 0;
		
		while (inpos < start + length) {
			// Read the next value to store
			byte value = input[inpos++];
			
			// Write it to the output
			output = writeByte(output, outpos, value);
			outpos++;
			
			// Where was the last time we wrote this byte?
			int lastPos = oldPositions[value & 0xff];
			
			// Update the oldPositions to reference this sequence
			oldPositions[value & 0xff] = inpos;
			
			// Start counting to see how many bytes to repeat from that sequence
			byte repeats = 0;
			while (inpos < start + length && // Haven't hit the end of the input
			       input[lastPos] == input[inpos] && // Matching byte
			       repeats != (byte)0xff) { // Won't overflow 'repeats'
				
				repeats++;
				lastPos++;
				inpos++;
			}
			
			// Write the repeats byte
			output = writeByte(output, outpos, repeats);
			outpos++;
		}
		
		// Shrink 'output' to match the number of used bytes, if necessary
		if (output.length > outpos) {
			byte[] temp = new byte[outpos];
			System.arraycopy(output, 0, temp, 0, temp.length);
			output = temp;
		}
		
		return output;
	}
	
	/**
	 * A wrapper for {@link #decode(byte[], int, int)} called as
	 * <tt>decode(input, 0, input.length)</tt>.
	 * 
	 * @param input The data to decode.
	 * @return The uncompressed version of the data.
	 * @throws ArrayIndexOutOfBoundsException If the data was not properly
	 *   formatted.
	 */
	public static byte[] decode(byte[] input)
	{
		return decode(input, 0, input.length);
	}
	
	/**
	 * Decode a block of JFFS2-style RLE-encoded data.
	 * 
	 * @param input The data to decode.
	 * @param start The offset to start reading from in the input array.
	 * @param length The number of bytes from the input array to read.
	 * @return The uncompressed version of the data.
	 * @throws ArrayIndexOutOfBoundsException If the data was not properly
	 *   formatted.
	 */
	public static byte[] decode(byte[] input, int start, int length)
	{
		int inpos = start;
		
		int[] oldPositions = new int[256];
		
		// This will be expanded as necessary
		byte[] output = new byte[length];
		int outpos = 0;
		
		while (inpos < start + length) {
			// Read the next value byte
			byte value = input[inpos++];
			
			// Write it to the output
			output = writeByte(output, outpos, value);
			outpos++;
			
			// Make sure there's at least one more byte (the repeat byte)
			if (inpos >= start + length) {
				throw new ArrayIndexOutOfBoundsException("Unexpected end of input");
			}
			
			// Read the next repeats byte
			byte repeats = input[inpos++];
			
			// Determine the last position where the value byte occurred
			int lastPos = oldPositions[value & 0xff];
			
			// Update to show that this is the most recent location where it
			// occurred
			oldPositions[value & 0xff] = outpos;
			
			// If any bytes are repeated from earlier, copy them
			if (repeats != 0) {
				// It will be copying from the same range we are writing to
				// We need to be able to copy the bytes that we are writing,
				// so we cannot use System.arraycopy()
				while (repeats != 0) {
					output = writeByte(output, outpos, output[lastPos++]);
					outpos++;
					repeats--;
				}
			}
		}
		
		// Resize output as necessary to avoid unused bytes at the end
		if (output.length > outpos) {
			byte[] temp = new byte[outpos];
			System.arraycopy(output, 0, temp, 0, temp.length);
			output = temp;
		}
		
		return output;
	}
	
	/**
	 * Write a byte into the array at the specified position, expanding the
	 * array as necessary to accommodate it.
	 * 
	 * @param array The array to write into.
	 * @param position The position to write at.
	 * @param value The value to write into the given position.
	 * @return An array containing the same data as the old array, which is at
	 *   least large enough to hold the data in it. This may be the same array
	 *   as was passed in, or it may be a new array.
	 */
	private static byte[] writeByte(byte[] array, int position, byte value)
	{
		// Need to resize?
		if (array.length <= position) {
			// Yes, make a new array at least large enough to hold the new
			// element
			byte[] temp = new byte[Math.max(array.length * 2, position + 1)];
			
			// Copy the old items into it
			System.arraycopy(array, 0, temp, 0, array.length);
			
			// Replace the old array
			array = temp;
		}
		
		array[position] = value;
		return array;
	}
}

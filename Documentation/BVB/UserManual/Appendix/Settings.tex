\chapter{Settings}

\label{apd:Settings}This section lists the settings used by BlueVue Group.

\notebox{Some numeric constants are used for clarity:\\
\begin{tabular}{l | l}
Constant & Value \\
\hline

$MAX$ & Maximum signed integer value, $2147483647$. \\

$ONE\_MINUTE$ & Seconds in one minute, $60$. \\

$ONE\_HOUR$ & Seconds in one hour, $3600$. \\

$ONE\_DAY$ & Seconds in one day, $86400$. \\

$ONE\_MONTH$ & Seconds in 28 days, $2419200$. \\
\end{tabular}}

\noindent If a specified setting is not in the legal range, or if a setting is
not present in the database, then the default value will be used silently.

\vspace{12pt}

\noindent These are the settings:

\begin{description}
\item [{BEP Create Modems}] Whether the BEP server should create new modems when
it cannot match a BEP message with an existing modem. A value of 0 means that
messages from unknown modems will be ignored. A value of 1 means that messages
from unknown modems will cause new modems to be created.\\
\textbf{Default:} $0$\\
\textbf{Range:} $[0, 1]$

\item [{BEP Create Status Job}] If the BEP server is set to create modems when
messages from unknown modems arrive (see ``BEP Create Modems''), this controls
whether an automatic status job should be created for the new modems. A value of
0 means that automatic status jobs will not be created. A value of 1 means that
automatic status jobs will be created. This setting is ignored if the ``BEP
Create Modems'' setting is set to 0. A status job will not be created if there
is no running server that has the ability to process status query jobs.\\
\textbf{Default:} $1$\\
\textbf{Range:} $[0, 1]$

\item [{BEP Cycle Time}] How long (in seconds) to wait for BEP messages before
pausing to check for settings updates or server shutdown. Lower values mean
faster settings updates and server shutdowns, but slightly lower responsiveness
to new BEP messages.\\
\textbf{Default:} $15$\\
\textbf{Range:} $[1, MAX]$

\item [{BEP Failure Timeout}] The maximum amount of time (in seconds) to wait
for a BEP response before killing a BEP connection due to inactivity.\\
\textbf{Default:} $15 \cdot ONE\_MINUTE$\\
\textbf{Range:} $[1, MAX]$

\item [{BEP Modem Wait Timeout}] How long (in seconds) a BEP thread should wait
on a busy modem before giving up and attempting to create a Delayed Update job
(discarding the data received by the message if such a job type is not supported
by a running job server). A value of 0 will give up immediately if the modem is
locked. A lower value will be more likely to create Delayed Update jobs (or
discard data if no server is able to process them), but a higher value will
cause the BEP server to be more likely to run out of available threads in its
thread pool.\\
\textbf{Default:} ``20''\\
\textbf{Range:} $[0, MAX]$

\item [{BEP New Modem Tag}] If new modems may be created by BEP messages (see
``BEP Create Modems''), this is the tag that will be applied to those new
modems. If blank, no tag will be applied. If the tag does not exist, it will be
created with a blank script and a polling interval of the ``No Tag Polling
Interval'' setting.\\
\textbf{Default:} ``BEP''\\
\textbf{Range:} Text, not NULL

\item [{BEP Port}] The port number that the BEP server(s) will listen on. All
BEP servers must be manually restarted after changing this setting.\\
\textbf{Default:} $10020$\\
\textbf{Range:} $[1, 65535]$

\item [{BEP Retry Timeout}] When sending a BEP message over UDP, how long (in
seconds) to wait for a response before re-sending that message.\\
\textbf{Default:} $30$\\
\textbf{Range:} $[1, MAX]$

\item [{Capabilities Cutoff}] How long (in seconds) before an entry in the
Capabilities table is considered ``stale.'' A stale entry is assumed to be from
a dead server. Locks owned by dead servers are cleared, and their entries are
removed from the table. A value of 0 means that entries are never considered
stale. If a server detects that it has not updated its entry in at least this
amount of time, it must assume that all of its locks have been cleared and it
should panic (shutdown forcefully).\\
\textbf{Default:} $90$\\
\textbf{Range:} $[0, MAX]$

\item [{Capabilities Update Interval}] How long (in seconds) between updates to
the Capabilities table. A value of $0$ means that the thread updating the
Capabilities table will not run. Changing the value away from $0$ requires a
server restart.\\
\textbf{Default:} $45$\\
\textbf{Range:} $[0, MAX]$

\item [{Clear Duplicate IPs}] Whether to clear duplicate IP addresses when
contacting modems. A value of $0$ means that known incorrect IP addresses will
be left alone. A value of $1$ means that, upon verifying a modem has a correct
IP, all other modems with the same IP address will have their IP address fields
cleared. Also, a value of $1$ means that, upon detecting that a modem has an
incorrect IP, that modem's IP address will be cleared.\\
\textbf{Default:} $1$\\
\textbf{Range:} $[0, 1]$

\item [{Database Version}] The version of the database format. If not equal to
the expected version, the program should not perform any database operations
(and should likely panic). This setting cannot be changed from the Web
interface.\\
\textbf{Default:} $0$\\
\textbf{Range:} $[0, MAX]$

\item [{Failure Timeout}] How long (in seconds) a modem may go without a job
completing successfully before it is marked with an error status.\\
\textbf{Default:} $ONE\_DAY$\\
\textbf{Range:} $[1, MAX]$

\item [{FTP Timeout}] How long (in seconds) to wait for an FTP operation (such
as uploading an update package to a modem) before giving up. This is not a
timeout on communications, this is actually the amount of time to give the
entire data transfer, so it should probably be set relatively high.\\
\textbf{Default:} $10 \cdot ONE\_MINUTE$\\
\textbf{Range:} $[1, MAX]$

\item [{History Cutoff}] How long (in seconds) entries in the History and Status
History tables should remain before being purged. A value of $0$ indicates that
entries should never be purged.\\
\textbf{Default:} $ONE\_MONTH$\\
\textbf{Range:} $[0, MAX]$

\item [{History Purge Interval}] How long (in seconds) between deleting old
entries from the History and Status History tables. A value of $0$ means that
the thread purging the History and Status History tables will not run. Changing
the value away from $0$ requires a server restart.\\
\textbf{Default:} $ONE\_MINUTE$\\
\textbf{Range:} $[0, MAX]$

\item [{HTTP Backlog}] The number of queued connections to allow to the BEP
server's internal HTTP server before refusing connections. A value of 0 means
that the operating system default value will be used.\\
\textbf{Default:} $3$\\
\textbf{Range:} $[0, MAX]$

\item [{HTTP Capacity}] The number of simultaneous downloads that the BEP
server's internal HTTP server will allow.\\
\textbf{Default:} $25$\\
\textbf{Range:} $[1, MAX]$

\item [{HTTP Port}] The port number on which the BEP server's internal HTTP
server will listen. The internal HTTP server is used to transfer files to a
modem without making an outgoing connection to that modem.\\
\textbf{Default:} $10030$\\
\textbf{Range:} $[1, 65535]$

\item [{IP Registration Create Modems}] Whether IP Registration servers should
create new modems when they are unable to match an IP registration message with
an existing modem. A value of $0$ means that messages from unknown modems will
be discarded. A value of $1$ means that messages from unknown modems will cause
new modems to be added to the Modems table.\\
\textbf{Default:} $0$\\
\textbf{Range:} $[0, 1]$

\item [{IP Registration Create Status Job}] If the IP registration server is set
to create modems when messages from unknown modems arrive (see the ``IP
Registration Create Modems'' setting), this controls whether an automatic status
query job should be created for the new modems. A value of $0$ means that
automatic status query jobs will not be created. A value of $1$ means that
automatic status query jobs will be created. This setting is ignored if the
``IP Registration Create Modems'' setting is set to $0$. A status query job will
not be created if there is no running server that has the ability to process
status query jobs.\\
\textbf{Default:} $1$\\
\textbf{Range:} $[0, 1]$

\item [{IP Registration Cycle Time}] How long (in seconds) to wait for IP
registration messages before pausing to check for settings updates or server
shutdown. Lower values mean faster settings updates and server shutdowns, but
slightly lower responsiveness to IP registration message handling.\\
\textbf{Default:} $15$\\
\textbf{Range:} $[1, MAX]$

\item [{IP Registration Modem Wait Timeout}] How long (in seconds) an IP
registration thread should wait on a busy modem before giving up and attempting
to create a Delayed Update job (discarding the data received by the message if
such a job type is not supported by any running job server). A value of 0 will
give up immediately if the modem is locked. A lower value will be more likely to
create Delayed Update jobs (or discard data, if no server is able to process
them), but a higher value will cause the IP registration server to be more
likely to run out of available threads in its thread pool.\\
\textbf{Default:} $20$\\
\textbf{Range:} $[0, MAX]$

\item [{IP Registration New Modem Tag}] If new modems may be created by IP
registration messages (see the ``IP Registration Create Modems'' setting), this
is the tag that will be applied to those new modems. If blank, no tag will be
applied. If the tag does not exist, it will be created with a blank script and a
polling interval of the ``No Tag Polling Interval'' setting.\\
\textbf{Default:} ``IPReg''\\
\textbf{Range:} Text, not NULL

\item [{IP Registration Port}] The port number that the IP Registration
server(s) should listen on. All IP Registration servers must be manually
restarted after changing this setting.\\
\textbf{Default:} $7777$\\
\textbf{Range:} $[1, 65535]$

\item [{Job Polling Interval}] How long (in seconds) between database queries
for new jobs to execute. This cannot be disabled, as this is the whole reason
the job server exists.\\
\textbf{Default:} $15$\\
\textbf{Range:} $[1, MAX]$

\item [{Job Polling Size}] The number of jobs to request at a time when
requesting jobs from the database.\\
\textbf{Default:} $15$\\
\textbf{Range:} $[1, MAX]$

\item [{License Key}] The license key for this installation. See
\prettyref{sec:LicenseKeyFormat} for information on how this is formatted. This
setting may not be changed from the Web interface, but the value may be
downloaded as an XML file.\\
\textbf{Default:} ``\textless bvblicense /\textgreater''\\
\textbf{Range:} Text, not NULL 

\item [{Lock Purge Interval}] How long (in seconds) between purging stale locks
(see the ``Lock Timeout'' setting for what constitutes a ``stale'' lock). A
value of $0$ disables the lock purging thread. Changing the value away from $0$
requires a server restart.\\
\textbf{Default:} $60$\\
\textbf{Range:} $[0, MAX]$

\item [{Lock Timeout}] How long (in seconds) a lock may be held by a server
before it is considered ``stale.'' A value of $0$ indicates that locks are never
considered stale.\\
\textbf{Default:} $2 \cdot ONE\_HOUR$\\
\textbf{Range:} $[0, MAX]$

\item [{Logging Cutoff}] How long (in seconds) entries in the Logs table should
remain before being purged. A value of $0$ indicates that entries should never
be purged. A value of $-1$ disables logging to the database from server
processes, but does not stop the database logging thread (allowing logging to be
re-enabled without a server restart).\\
\textbf{Default:} $3 \cdot ONE\_MONTH$\\
\textbf{Range:} $[-1, MAX]$

\item [{Log Dump Interval}] How long (in seconds) between dumping log entries to
the database. A value of $0$ means that the database logging thread should not
run. See the ``Logging Cutoff'' setting to disable database logging without
killing the thread. Changing this value away from $0$ requires a server
restart.\\
\textbf{Default:} $5$\\
\textbf{Range:} $[0, MAX]$

\item [{Maximum DB Connections}] The maximum number of open database connections
to maintain in the database connection pool.\\
\textbf{Default:} $5$\\
\textbf{Range:} $[1, MAX]$

\item [{Maximum Thread Pool Size}] The maximum number of threads in a thread
pool (Job processing, IP registration connection processing, ...).\\
\textbf{Default:} $50$\\
\textbf{Range:} $[1, MAX]$

\item [{Maximum Tries}] The maximum number of attempts for a job before
canceling it.\\
\textbf{Default:} $3$\\
\textbf{Range:} $[1, MAX]$

\item [{Minimum Thread Pool Size}] The minimum number of threads in a thread
pool (Job processing, IP registration connection processing, ...).\\
\textbf{Default:} $3$\\
\textbf{Range:} $[0, MAX]$

\item [{Modem Socket Timeout}] How long (in seconds) to wait for a response on a
socket before giving up and declaring the communication a failure.\\
\textbf{Default:} $50$\\
\textbf{Range:} $[1, MAX]$

\item [{No Tag Polling Interval}] The amount of time (in seconds) to use as the
polling interval for an automatic job running against a modem with not tags. A
value of $0$ will prevent the automatic job from rescheduling itself, as if it
had not been marked as automatic.\\
\textbf{Default:} $ONE\_HOUR$\\
\textbf{Range:} $[0, MAX]$

\item [{Retry Multiplier}] How long (in seconds) to add for each additional job
retry. The first retry will occur ``Retry Timeout'' seconds after the first
failure, but the second retry will occur ``Retry Timeout'' plus ``Retry
Multiplier'' seconds after the second failure. The third retry adds another
instance of ``Retry Multiplier,'' and so on. This allows you to set up a short
wait before the first retry, but a significantly longer wait until the next,
if desired.\\
\textbf{Default:} $ONE\_MINUTE$\\
\textbf{Range:} $[0, MAX]$

\item [{Retry Timeout}] The initial amount of time to wait before the first
retry. See the ``Retry Multiplier'' setting for more information.\\
\textbf{Default:} $ONE\_MINUTE$\\
\textbf{Range:} $[0, MAX]$

\item [{Setting Poll Interval}] How often (in seconds) settings should be polled
from the database to keep them fresh. A value of $0$ indicates that settings
should never be polled beyond the first time. This is also used by other threads
that periodically check for settings that have changed and update themselves. A
value of $0$ will prevent those updater threads from running, and will require a
server restart to change. Additionally, a value of $0$ will prevent the server
from noticing when it has been told to shut down, so it will need to be manually
stopped.\\
\textbf{Default:} $30$\\
\textbf{Range:} $[0, MAX]$

\item [{Shutdown Server}] Instructs individual servers to shut down. If set to
the name of a server, that server will shut down the next time it updates its
settings. If set to ``All'' (not case sensitive), all servers will shut down.
You must manually clear the ``All'' setting before starting any servers,
otherwise they will immediately stop again. Relies on the ``Setting Poll
Interval'' setting for reading this; if that setting is very large or is set to
$0$, the server will take a long time to see that it should shut down, or it
will never know at all.\\
\textbf{Default:} NULL\\
\textbf{Range:} Text

\item [{Thread Capacity}] The number of threads that may be queued for execution
before disallowing more threads to be created (via jobs to be pulled from the
database, connections to the IP registration server, ...). Changes to this will
not take effect until the server restarts. This is also the value used for the
backlog size on incoming connection servers.\\
\textbf{Default:} $25$\\
\textbf{Range:} $[1, MAX]$

\item [{Thread Keepalive}] The amount of time (in seconds) idle threads are
allowed to remain in the thread pool before being dropped. A value of $0$
indicates that threads beyond the minimum pool size should never remain after
completion. A value of $-1$ indicates that, once present, threads should stick
around forever.\\
\textbf{Default:} $180$\\
\textbf{Range:} $[-1, MAX]$

\item [{Updated Modem Query Delay}] How long (in seconds) after a modem update
to schedule a new status query job, if one does not already exist within that
time period. A value of $0$ means that a new status query should not be
automatically scheduled. A small value is likely to cause failed jobs, as the
modem may still be in the process of rebooting when the query happens (at least,
in the case of firmware updates).\\
\textbf{Default:} $600$\\
\textbf{Range:} $[0, MAX]$

\item [{Warning Timeout}] How long (in seconds) a modem may go without a job
completing successfully before it is marked with a warning status.\\
\textbf{Default:} $6 \cdot ONE\_HOUR$\\
\textbf{Range:} $[1, MAX]$

\item [{WebUI Title}] The title for the Web UI in a web browser.\\
\textbf{Default:} ``BlueVue Group''\\
\textbf{Range:} Text, not NULL
\end{description}


\section{License Key Format}

\label{sec:LicenseKeyFormat}A BlueVue Group license key is an XML file
containing customer details, restrictions specific to that customer's
BVG installation, and an RSA signature to prevent tampering. Here are the
standard fields of a license key:

\begin{description}
\item [{bvblicense}] Top-level tag.

\begin{description}
\item [{license}] Contains the details about this license.

\begin{description}
\item [{customer}] Contains customer details.

\begin{description}
\item [{name}] The name of the customer.

\item [{address}] The street address of the customer.

\item [{phone}] The phone number of the customer.

\item [{email}] The email address of the customer.
\end{description}

\item [{restrictions}] Contains \texttt{restriction} sub-tags which describe the
restrictions specific to this license key. The possible values are not described
here.
\end{description}

\item [{signature}] An RSA signature of the license key, sans this tag,
formatted without any extraneous spaces between tags.
\end{description}
\end{description}

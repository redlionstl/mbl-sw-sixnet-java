\chapter{Database Schema}

\label{apd:DatabaseSchema}This appendix is divided into one section for each
table in the database. Each table is then described with a tabular format, with
each column named and given a data type under each supported database server
(currently only PostgreSQL).

Additional notes about each table are provided beneath the tabular data.

Special notes about PostgreSQL: Timestamps are stored without time zones. We
assume that the time zone is always UTC.


\section{Batches}

\label{sec:DBBatches}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

BatchID & integer, not null & A unique identifier given to each batch. \\

UserID & integer & The User ID (see \prettyref{sec:DBUsers}) of the user that
created this batch, or NULL if there was no user associated with the batch. \\

BatchName & text, not null & The name of the batch. \\

Deleted & boolean, not null & Whether the batch has been marked as
``deleted''. \\
\end{tabular}
\vspace{\parskip}

The ``BatchID'' column auto-increments with each new row and is the primary key.

The ``BatchName'' field is \textbf{not} enforced as unique.

The ``Deleted'' column defaults to ``false.''


\section{Capabilities}

\label{sec:DBCapabilities}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

UpdatedAt & timestamp, not null & The timestamp of the last update, as specified
by the server making the update. \\

Server & character varying(253), not null & The name of the server. This will be
a concatenation of the host name, a hyphen (``-''), and the server type. \\

Databases & text, not null & The names of the database providers supported by
this server, separated by colons (``:''). \\

Jobs & text, not null & The names of the job types supported by this server,
separated by colons. Job types that are not meant to be user-selectable in the
web interface will be prefixed with an exclamation point (``!''). \\

Modems & text, not null & The model/firmware combinations of the modem plug-ins
supported by this server, separated by colons. Each model/firmware combination
is represented by the concatenation of the model number, an at sign (``@''), and
the firmware version number. \\

Extra & text, not null & XML data containing extra information about the server.
Currently, this is used to mark each server as either ``incoming,'' meaning that
modems connect to the server, or ``outgoing,'' meaning that the server connects
to modems. \\
\end{tabular}
\vspace{\parskip}

The ``Server'' column is the primary key.

The ``Extra'' column defaults to ``\verb!<extra />!''.


\section{History}

\label{sec:DBHistory}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PosgreSQL & Description \\
\hline

HistoryID & bigint, not null & A unique identifier given to each history
entry. \\

JobID & bigint, not null & The Job ID (see \prettyref{sec:DBJobs}) of the job
that caused this history entry to be created. \\

BatchID & integer & The Batch ID (see \prettyref{sec:DBBatches}) of the batch
that the job was a member of, or NULL if it was not a member of a batch. \\

ModemID & integer, not null & The Modem ID (see \prettyref{sec:DBModems}) of the
modem that the job ran against. \\

UserID & integer & The User ID (see \prettyref{sec:DBUsers}) of the user that
created the job, or NULL if there was no user associated with the job. \\

Timestamp & timestamp, not null & The timestamp of when the history entry was
made, as reported by the server creating the entry. \\

Result & character varying(32), not null & Either ``Success'' or ``Failure''
indicating the result of the job. \\

Type & character varying(32), not null & The type of job that ran (see
\prettyref{sec:DBJobs}). \\

Message & text & A message with extra information about how the job
terminated. \\
\end{tabular}
\vspace{\parskip}

The ``HistoryID'' column auto-increments with each new row and is the primary
key.

The ``ModemID'' column is a foreign key referencing the ``ModemID'' column of
the Modems table (see \prettyref{sec:DBModems}) which will cause history entries
for that modem to be deleted if the modem is deleted.


\section{Jobs}

\label{sec:DBJobs}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

JobID & bigint, not null & A unique identifier given to each job. \\

BatchID & integer & The Batch ID (see \prettyref{sec:DBBatches}) of the batch in
which this job is a member, or NULL if the job is not a member of a batch. \\

ModemID & integer, not null & The Modem ID (see \prettyref{sec:DBModems}) of the
modem against which this job will run. \\

UserID & integer & The User ID (see \prettyref{sec:DBUsers}) of the user that
created this job, or NULL if there is no user associated with this job. \\

Schedule & timestamp & The date/time at which this job should run. \\

Tries & integer, not null & The number of times that this job has tried
unsuccessfully. \\

LockedBy & character varying(253) & The name of the server (see the ``Server''
column of the capabilities table, \prettyref{sec:DBCapabilities}) that is
holding the lock on this job, or NULL if it is free. \\

LockedAt & timestamp & The time at which the lock on this job was obtained, or
NULL if it is free. \\

Type & character varying(32), not null & The type of job (see
\prettyref{cha:Jobs}). \\

Parameters & text, not null & XML data describing extra parameters of this job,
such as the checksum of the firmware image to use for a firmware update job. \\

Automatic & boolean, not null & Whether this job should automatically schedule a
new instance of itself when it completes. \\
\end{tabular}
\vspace{\parskip}

The ``Job ID'' column auto-increments with each new row and is the primary key.

The ``ModemID'' column is a foreign key referencing the ``ModemID'' column of
the Modems table (see \prettyref{sec:DBModems}) which will cause jobs for that
modem to be deleted if the modem is deleted.

The ``Tries'' column defaults to 0.

The ``Parameters'' column has these known values:

\begin{description}
\item [{job}] Top-level tag.

\begin{description}
\item [{firmware}] The checksum of the package from the ``Packages'' table (see
\prettyref{sec:DBPackages}) to install into the modem. This must be present in
``Firmware'' type jobs, but is meaningless for all other job types.

\item [{force}] If this tag exists (it has no associated value), then the
firmware in the modem will be updated even if the current running firmware has
the same version number as the firmware package specified by this job. This may
be applied to ``Firmware'' type jobs, but is meaningless for all other job
types.

\item [{mobileOriginated}] If present, may contain either \texttt{true} or
\texttt{false} indicating that the job is mobile-originated. If absent, defaults
to \texttt{false}. A mobile-originated job is initiated by a modem sending a BEP
message to the server, rather than by the server contacting the modem. This
may be applied to ``Status,'' ``Reconfiguration,'' and ``Firmware'' type jobs.
Additionally, the ``alwaysAvailable'' attribute may be set to \texttt{true} or
\texttt{false} (defaulting to false when absent), indicating whether the job
should always be available. See \prettyref{sec:MobileOriginated} for details on
how this works.

\item [{modem}] Used by the ``DelayedUpdate'' job type to specify the properties
being set in a modem. Meaningless for all other job types. These are the
sub-tags that are recognized:

\begin{description}
\item [{deviceID}] Contains the device ID to set for the modem.

\item [{alias}] Contains the alias to set for the modem.

\item [{model}] Contains the model to set for the modem.

\item [{fwVersion}] Contains the firmware version number to set for the modem.

\item [{cfgVersion}] Contains the configuration version number to set for the
modem.

\item [{ipAddress}] Contains the IP address to set for the modem.

\item [{port}] Contains the port number to set for the modem.

\item [{phoneNumber}] Contains the phone number to set for the modem.

\item [{property}] The ``name'' attribute contains the name of a property to set
for the modem (properties are saved in the Modem table's ``Status'' column in
XML; see \prettyref{sec:DBModems}). Specify sub-tags with slashes between tag
names, specify attributes with a space between the tag name and the attribute
name. For example, to set the value of the ``c'' attribute in tag ``a'' sub-tag
``b'' specify the ``name'' attribute as ``a/b c''. The contents of this tag is
the actual value to set for the given property.

\item [{status}] The ``name'' attribute contains the name of a status value (as
present in the ``StatusHistory'' table ``PropertyName'' column; see
\prettyref{sec:DBStatusHistory}). The contents of the tag are the value to set
in the ``PropertyValue'' column of the ``StatusHistory'' table. If any
``status'' tags exist, the ``timestamp'' tag must exist.

\item [{timestamp}] Contains the timestamp to set for all ``status'' tags, when
they are written to the ``StatusHistory'' table. The timestamp is the number of
milliseconds since the epoch (January 1, 1970, 00:00:00 GMT). If any ``status''
tags exist, this tag must be present.
\end{description}
\end{description}
\end{description}


\section{Logs}

\label{sec:DBLogs}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

LogID & bigint, not null & A unique identifier given to each log entry. \\

Timestamp & timestamp, not null & The time at which this log entry was created,
as specified by the server creating the entry. \\

UserID & integer & The User ID (see \prettyref{sec:DBUsers}) of the user that
caused this log entry to be created, or NULL if there is no associated user. \\

Type & character varying(16), not null & ``Info,'' ``Warning,'' or ``Error''
indicating the importance of the log entry. \\

Description & text, not null & The message being logged. \\
\end{tabular}
\vspace{\parskip}

The ``LogID'' column auto-increments with each new row and is the primary key.

Any time an object is referenced in the ``Description'' column, it will be
references as, for example, ``Job \#5'' or ``Modem \#31'' to make for easy
searching.


\section{Modems}

\label{sec:DBModems}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

ModemID & integer, not null & A unique identifier given to each modem. \\

DeviceID & bigint & The device ID of the modem (either ESN or IMEI) converted to
an integral format that will work for either, or NULL if it is not known. See
below for the calculation used to convert in either direction. \\

Alias & text & The modem's name (as reported by the AT+BMNAME command), or NULL
if it is not known. \\

Model & character varying(64) & The model number of the modem, or NULL if it is
not known. \\

FWVersion & character varying(64) & The version of firmware running in the
modem, or NULL if it is not known. \\

CfgVersion & character varying(32) & The configuration version (as reported by
the AT+BCFGV command), or NULL if it is not known or unsupported in the
modem. \\

IPAddress & character varying(253) & The IP address of the modem, or NULL if it
is not known. \\

Port & integer, not null & The port number used for communicating with the
modem. \\

PhoneNumber & character varying(64) & The phone number of the modem. \\

Status & text, not null & XML data describing additional properties of the
modem, such as the serial number and component version numbers. \\

LockedBy & character varying(253) & The name of the server (see the ``Server''
column of the capabilities table, \prettyref{sec:DBCapabilities}) that is
holding the lock on this modem, or NULL if it is free. \\

LockedAt & timestamp & The time at which the lock on this modem was obtained, or
NULL if it is free. \\
\end{tabular}
\vspace{\parskip}

The ``ModemID'' column auto-increments with each new row and is the primary key.

The ``DeviceID'' column is enforced as unique (NULL values are not considered to
be equal, according to the SQL standard, so multiple NULL values may be
present).

For the ``DeviceID'' column, to convert from an ESN or IMEI string to a bigint,
use this calculation ($s$ is the string containing the ESN or IMEI):

\begin{Verbatim}[frame=single]
if (lengthOf(s) == 11) {
  // It is an ESN
  long prefix = first 3 characters of s, parsed as a long integer
  long body = rest of s, parsed as a long integer
  
  if (prefix > 255) error, too big
  if (body > 16777215) error, too big
  
  deviceID = shiftLeft(prefix by 24 bits) bitwise-or body
} else if (lengthOf(s) == 15) {
  // It is an IMEI
  deviceID = s, parsed as a long integer (base 10)
} else if (lengthOf(s) == 8) {
  // It is an ESN, in hexadecimal
  deviceID = s, parsed as a long integer (base 16)
} else {
  error, not a recognized format
}
\end{Verbatim}

\noindent And to convert from a bigint ``DeviceID'' value to a string:

\begin{Verbatim}[frame=single]
if (byte 6 of deviceID == 0) {
  // It is an ESN
  // For the 3+8 style:
  esn = formatString("%03d%08d",
                     byte 4 of deviceID, shifted right 24 bits
                     bytes 1-3 of deviceID, not shifted);
  
  // For the hexadecimal style:
  esn = formatString("%08X", bottom 4 bytes of deviceID);
} else {
  // It is an IMEI
  imei = formatString("%015d", deviceID);
}
\end{Verbatim}

\noindent The function \texttt{formatString} is meant to be like the C printf
family of functions, or the Java String.format() function.

The combination of the ``IPAddress'' and ``Port'' columns is enforced as unique.

The ``Port'' column defaults to 5070.

The ``Status'' column has these known values:

\begin{description}
\item [{modem}] Top-level tag.

\begin{description}
\item [{passwordSettings}] Describes the password settings for the modem.

\begin{description}
\item [{access}] Contains one of \texttt{0}, indicating that the password is
required for WAN access only; \texttt{1}, indicating that the password is
required for both WAN and LAN access; or \texttt{2}, indicating that the
password is required for LAN access only.

\item [{enabled}] Contains either \texttt{0}, indicating that a password is not
required by the modem, or \texttt{1} indicating that a password is required.

\item [{password}] Contains the modem password.
\end{description}

\item [{privateConfig}] Contains an optional enhanced TTL script (see
\prettyref{cha:Scripting}) which is only applied to this modem during
``Reconfiguration'' jobs.

\item [{serial}] Contains the serial number of the modem.

\item [{simcard}] Contains the SIM card number, for GSM modems.

\item [{variables}] Contains tags representing each user-defined variable that
is available to an executing enhanced TTL script:

\begin{description}
\item [{variable}] The ``name'' attribute is the name of the variable, as used
by an enhanced TTL script. The contents are the variable's value.
\end{description}

\item [{versions}] Contains individual tags for each of the component version
numbers within the modem:

\begin{description}
\item [{gps}] The GPS module version number.

\item [{hw}] The hardware version number.

\item [{os}] The operating system version number.

\item [{prl}] The PRL version number.

\item [{rf}] The RF module version number.

\item [{uboot}] The U-Boot version number.
\end{description}
\end{description}
\end{description}

\section{Packages}

\label{sec:DBPackages}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

Type & character varying(16), not null & The package type, like ``ABT4K/5K''. \\

Version & character varying(16), not null & The package version. \\

Date & timestamp, not null & The date on which the package was built. \\

Info & character varying(16), not null & Extra information about the package. \\

Checksum & character(64), not null & The package checksum. \\

Data & bytea & The actual package data, including headers. \\
\end{tabular}
\vspace{\parskip}

The ``Checksum'' column is the primary key.


\section{Scheduling}

\label{sec:DBScheduling}

\notebox{Currently this table is not used. It is intended for scheduling modem
down-time, so than jobs will not be automatically rescheduled for times when the
modem is expected to be off, and so that a modem which is expected to be off
will not show up in a warning state.}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

ScheduleID & integer, not null & A unique identifier given to each schedule
entry. \\

ModemID & integer, not null & The Modem ID (see \prettyref{sec:DBModems}) to
which this schedule entry applies. \\

State & text, not null & The state of the modem during the time period
designated by this schedule entry. May be ``Offline'' or a comma-separated
string of job types that are allowed to be executed. \\

StartTime & timestamp, not null & The starting time for this schedule entry. \\

EndTime & timestamp, not null & The ending time for this schedule entry. \\

Relative & boolean, not null & If ``True,'' then the ``StartTime'' and
``EndTime'' fields are relative to midnight Sunday morning of the current week;
if ``False,'' then the ``StartTime'' and ``EndTime'' fields are absolute times
(relative to the epoch). If multiple schedule entries control the same period of
time, absolute entries take precedence over relative entries. A conflict between
multiple entries with the same value in the ``Relative'' field is an error. \\
\end{tabular}
\vspace{\parskip}

The ``ScheduleID'' column auto-increments with each new row and is the primary
key.


\section{Settings}

\label{sec:DBSettings}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

SettingName & character varying(64), not null & The name of this setting. \\

SettingValue & text & The current value of this setting. \\
\end{tabular}
\vspace{\parskip}

The ``SettingName'' column is the primary key.

See \prettyref{apd:Settings} for the actual settings that are recognized by the
system.


\section{StatusHistory}

\label{sec:DBStatusHistory}This table holds onto values within a modem for which
a log of historical values may be useful (such as RSSI, input voltage, ...).

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

ModemID & integer, not null & The Modem ID (see \prettyref{sec:DBModems}) from
which this value came. \\

Timestamp & timestamp, not null & The date/time at which this entry was made, as
reported by the server making the update. \\

PropertyName & character varying(32) & The name of the status value. \\

PropertyVal & text & The value of the named property. \\
\end{tabular}
\vspace{\parskip}

The ``ModemID'' column is a foreign key referencing the ``ModemID'' column of
the Modems table (see \prettyref{sec:DBModems}) which will cause status values
for that modem to be deleted if the modem is deleted.


\section{TagMatch}

\label{sec:DBTagMatch}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

TagID & integer, not null & The Tag ID (see \prettyref{sec:DBTags}) of the tag
being matched to a modem. \\

ModemID & integer, not null & The Modem ID (see \prettyref{sec:DBModems}) of the
modem being matched with a tag. \\
\end{tabular}
\vspace{\parskip}

The ``TagID'' column is a foreign key referencing the ``TagID'' column of the
Tags table (see \prettyref{sec:DBTags}) which will cause matches for that tag to
be deleted if the tag is deleted.

The ``ModemID'' column is a foreign key referencing the ``ModemID'' column of
the Modems table (see \prettyref{sec:DBModems}) which will cause matches for
that modem to be deleted if the modem is deleted.


\section{Tags}

\label{sec:DBTags}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

TagID & integer, not null & A unique identifier given to each tag. \\

Position & integer, not null & The position of this tag, relative to others,
when applying configuration scripts from multiple tags to a single modem. \\

TagName & character varying(64), not null & The name of this tag. \\

ConfigScript & text & The configuration script for this tag, or NULL if there is
none. \\

PollingInterval & integer, not null & The amount of time (in seconds) in the
future to schedule a new instance of an automatic job once it completes. \\
\end{tabular}
\vspace{\parskip}

The ``TagID'' column auto-increments with each new row and is the primary key.

The ``Position'' column is enforced as unique.

The ``TagName'' column is enforced as unique.

The ``PollingInterval'' column defaults to ``3600'' seconds (1 hour).


\section{Users}

\label{sec:DBUsers}

\begin{tabular}{p{1in} | p{1in} | p{2.25in}}
Column Name & PostgreSQL & Description \\
\hline

UserID & integer, not null & A unique identifier given to each user. \\

UserName & character varying(32), not null & The name of the user. \\

Password & character varying(128), not null & A SHA1 hash of the user's name
followed by a colon and then the actual password. That way, if two users have
the same password, it will not be obvious by looking at the database. \\

Type & character varying(24), not null & The type of user (``Root,''
``Enhanced,'' or ``Basic''). \\

Preferences & text, not null & XML data containing user preferences for the web
interface. \\
\end{tabular}
\vspace{\parskip}

The ``UserID'' column auto-increments with each new row and is the primary key.

The ``UserName'' column is enforced as unique.

The ``Preferences'' column defaults to ``\textless prefs /\textgreater''
(indicating that defaults should be used).

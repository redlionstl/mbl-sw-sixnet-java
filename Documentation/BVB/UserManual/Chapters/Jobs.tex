\chapter{Jobs}

\label{cha:Jobs}Jobs represent the actual tasks performed by BlueVue Group;
without jobs, the system would simply be a database with a pretty interface.
A job tells the Job Server (see \prettyref{cha:Servers}) to communicate with a
modem, updating the database when finished.

A job may be marked as ``Automatic,'' indicating that when complete (due to a
successful communication, or due to too many failures), it should create a new
job to perform the same task again at some future time.


\section{Retries}

\label{sec:JobRetries}Each job runs against a single modem at a scheduled time.
If it fails, it will increment a ``Tries'' counter. If the tries counter rises
above a threshold, the job will stop trying and will delete itself.

The maximum number of tries is specified by the ``Maximum Tries'' setting (see
\prettyref{apd:Settings}). The new schedule time is determined by a combination
of a delay and a multiplier. The delay (specified by the ``Retry Timeout''
setting) is used the first time a job fails. After the first time, the
multiplier (specified by the ``Retry Multiplier'' setting) is added for each
failure. Here is an example, using the 30 seconds for the retry timeout and 60
seconds for the retry multiplier:

\begin{tabular}{l | p{1.5in} | l}
Try Number & Calculation & Result (Delay in Seconds) \\
\hline

$1$ (no failures) & $Retry Timeout + 0 \cdot Retry Multiplier$ & $30$ \\

$2$ (one failure) & $Retry Timeout + 1 \cdot Retry Multiplier$ & $90$ \\

$3$ (two failures) & $Retry Timeout + 2 \cdot Retry Multiplier$ & $150$ \\
\end{tabular}


\section{Automatic Rescheduling}

\label{sec:AutomaticJobs}A job may be marked as ``Automatic,'' which means that
when it completes (successfully or otherwise), it will automatically create a
new copy of itself, scheduled in the future (unless it is an ``Always
Available'' mobile-originated job, in which case it will be rescheduled for the
moment that the job completes).

The amount of time between the job completion and the new scheduling date is
determined by the tags applied to the job. As this feature is mainly intended
for having repetitive status queries, the value is called the
``Polling Interval.'' If a modem has multiple tags, the smallest polling
interval which is greater than zero is used. If there are no tags with a polling
interval greater than zero applied to a modem, the value of the ``No Tag Polling
Interval'' setting (see \prettyref{apd:Settings}) is used instead.

\notebox{If the value of the ``No Tag Polling Interval'' setting is zero, the
job will be treated as if it were not automatic --- that is, it will not be
rescheduled.}


\section{Mobile-Originated}

\label{sec:MobileOriginated}A mobile-originated job executes in response to a
modem making a connection to the BEP server, rather than the Job server making a
connection to the modem. This allows for management of modems in restrictive
networks which block all connections to devices within the network (like the
connection made from the Job server to the modem).

Modems may be configured to send BEP (BlueTree Event Protocol) messages to the
BEP server based on some trigger. The trigger is usually a simple timer (such as
``Send a message once each hour,'' but other types of triggers exist; see the
modem documentation). When a BEP message is received, the BEP server will check
for any mobile-originated jobs against that modem which are past due, and will
use the connection made by the modem to communicate the necessary commands for
the job.

A mobile-originated job may be marked as ``Always Available,'' which indicates
that, rather than following the normal job rescheduling rules, jobs will instead
be rescheduled for the same moment when they have completed. This may be useful
in a situation where the trigger that causes the BEP message to be sent is an
input to the modem (such as the ignition to the car being turned on). If the job
ran and rescheduled itself for a long time in the future, and then the trigger
caused another message to be sent from the modem before that time had elapsed,
the job would not run again. Marking the job as ``Always Available'' will mean
that the job is always past due, and will run any time a new message arrives
from the modem.

\notebox{Although an Always Available job will always be made available after
running, the initial date that it is scheduled for is still enforced. Therefore,
scheduling a new Always Available job for tomorrow will not allow that job to
run until tomorrow, at which point it will always be available for running.}

\section{Default Job Types}

\label{sec:DefaultJobTypes}The BlueVue Group software suite initially supports
these job types:

\begin{description}
\item [{Status}] A status query job. Asks the modem for a number of properties,
and adds the resulting information to the database. Performs no modifications to
the modem.

\item [{Reconfiguration}] Reconfigures a modem by executing all applicable
configuration scripts on it. These scripts are defined in the tags applied to
the modem, as well as in the modem itself. Scripts are run in ``Position'' order
from the tags, and then finally the modem's own configuration script is run.
After the reconfiguration is complete, a non-automatic status query is scheduled
to query the modem, to read any new settings values.

\notebox{A mobile-originated reconfiguration job should not attempt to update
the BEP or related settings in a modem, as the modification may cause the link
to the modem to break prior to completing the reconfiguration.}

\item [{Firmware}] Applies a firmware update package to a modem. Once the
package has been uploaded to the modem and the modem has begun the update
procedure, a status query job is scheduled to query the modem, to make sure that
the firmware update succeeded.
\end{description}

\notebox{After a reconfiguration or firmware update job, if there is not already
a status query job scheduled within the next ``Updated Modem Query Delay''
setting seconds (see \prettyref{apd:Settings}), a non-automatic status query is
scheduled for ``Updated Modem Query Delay'' seconds in the future (unless the
job is mobile-originated, in which case the status query will be scheduled for
the current time and be marked as always-available).}


\section{Common Job Tasks}

\label{sec:CommonJobTasks}All jobs which communicate with modems perform a few
standard ``housekeeping'' actions before completing their main tasks. The main
housekeeping action that is performed is veriication of a modem's identity. The
serial number and model of a modem are checked against the expected values from
the database, and if they do not match, the job will fail before doing anything
else.

A modem which does not have a known serial number or model will have these
fields populated, rather than verified, during its first communication, so be
very careful when adding modems by IP address alone.

If a mismatch is detected (the modem that was contacted reports an incorrect
serial number or model), the job will clear the modem's IP address field before
failing. The purpose of this action is to prevent further job executions from
failing, and to reduce inaccuracies in the database.

If the contacted modem matches, all other modems (on a best-effort basis) with
the same IP address and port number as the contacted modem will have their IP
address fields cleared. As in the previous case, the purpose it to prevent
further failed jobs, and to reduce inaccuracies in the database.

The IP clearing action will only take place if the setting \texttt{Clear
Duplicate IPs} is set to \texttt{1} (see \prettyref{apd:Settings}).


\section{Plug-in Model}

\label{sec:JobPlugins}Jobs are provided by plug-ins (individual files in a
directory which provide the jobs to the server). Servers are aware of which jobs
they support, and publish this information in the Capabilities table of the
database (see \prettyref{apd:DatabaseSchema}). This allows the servers to
coordinate (with the help of the administrator) to efficiently provide the
necessary services.

For example, there may be so many modems that the Job Server would be better off
on its own machine, separate from the database server (see
\prettyref{cha:Servers}). But then a firmware update is released, and a large
number of modems all need the update at once. Firmware update jobs generally
take longer than status query jobs, and running behind on polling for modem
status would be unacceptable. To remedy this problem, remove the Firmware Update
jobs from the existing Job Server (by deleting ``FirmwareJob.jar'' from the
directory containing the job plug-ins), and start up a temporary second Job
Server that only has the ``FirmwareJob.jar'' file in its job plug-ins directory.
This will partition the jobs so that the status query job server does not get
bogged down by firmware updates.

If necessary, contact Sixnet for information on having a custom job type
created. New job types will likely be a simple drop-in file, and everything will
continue to work \textit{without any servers requiring a restart}.

These are the files that correspond to each job type:

\begin{tabular}{l | l | p{1.75in}}
Job Type & File Name & Description \\
\hline

Status & StatusJob.jar & Simple read-only status query. \\

Reconfiguration & ReconfigJob.jar & Run configuration scripts on a modem. \\

Firmware & FirmwareJob.jar & Update the firmware running in a modem. \\

Delayed Update & DelayedUpdateJob.jar & Internal-only job used to update the
database if new information is received while a modem is busy on another job
(generally due to a message sent to the IP Registration or BEP server, see
\prettyref{cha:Servers}). This may only run on a Job server; as this job cannot
be marked as mobile-originated, it cannot execute on the BEP server. \\
\end{tabular}

\chapter{Command-Line Access}

\label{cha:CLI}There is a command-line interface (CLI) program distributed with
BlueVue Group called BlueVueBatch, or BVBatch for short. It places emphasis on
power users trying to perform group updates on many objects at once.


\section{General Usage}

\label{sec:CLIGenUsage}The CLI uses a plug-in action model. This allows for
simple extensibility (and also simple limiting of available actions, for system
security). New actions may be added to the system simply by dropping a file into
place, and existing actions may be removed by simply removing the corresponding
file.

The CLI does not deal with user accounts, they are used only by the web
interface. Accounts are not enforced or manageable from the CLI (except for
resetting passwords using the \texttt{Change\_Password} command). Instead,
access to the CLI should be controlled using the standard authentication
mechanisms provided by the host operating system.

The CLI places emphasis on acting on large groups of data from the database, and
it performs virtually all of this through extensive use of the Comma Separated
Values (CSV) format. As such, an extra utility called CSVUtil is provided to
perform simple database-like actions on CSV data.

All examples describing usage of the CLI will use \texttt{bvb} to refer to the
BVBatch program, and \texttt{csvutil} to refer to the CSVUtil program.


\section{Upgrading the Database}

\label{sec:DBUpgrade}If a BlueVue Group update is released that requires a
database with a new format, none of the servers or other programs will be able
to run until the existing, older-format database is updated. The CLI is the only
way to perform this upgrade.

To upgrade the database, first acquire a copy of the active license key. This
may be accessed through the Web interface, from the Settings page, under the
name \texttt{License Key}. If the license key has expired, but a new one is
available, use the new one to upgrade the database, and then follow the
procedure in ``License Management'' (see \prettyref{sec:LicenseUpdate}) to
update the stored license.

Next, install the upgraded version of BlueVue Group.

Lastly, execute this CLI command (assuming the license key is saved as
\texttt{license.blf} in the current directory):

\begin{Verbatim}[frame=single]
bvb --licenseKey license.blf --dbupgrade
\end{Verbatim}

\notebox{The ability to retrieve the license key from the Web in this fashion
may not be available in older versions. If you do not have a copy available,
you may contact Sixnet or you may use the CLI action \texttt{View\_Settings} to
retrieve the setting named \texttt{License Key}, but before using the result,
the header line, \texttt{License Key} column, and quotes surrounding the value
must be removed by hand.}



\section{Actions}

To execute a CLI action, run the command

\begin{Verbatim}[frame=single]
bvb -a Action_Name argument1 argument2 ...
\end{Verbatim}

To get a list of all available action names, run the command

\begin{Verbatim}[frame=single]
bvb -l
\end{Verbatim}

And finally, to get help for a specific action, run the command

\begin{Verbatim}[frame=single]
bvb -a Action_Name -e
\end{Verbatim}

Help for the \texttt{bvb} program may be obtained by passing the \texttt{--help}
switch.

Here is a complete list of the standard actions:

\begin{description}
\item [{Apply\_Tags}] Applies tags to modems.

\item [{Change\_Password}] Changes the passwords of web interface users.

\item [{Create\_Batches}] Creates batches.

\item [{Create\_Jobs}] Creates jobs against modems, optionally placing each job
into a batch.

\item [{Create\_Modems}] Creates modems.

\item [{Create\_Packages}] Creates update packages.

\item [{Create\_Tags}] Creates tags.

\item [{Delete\_Batches}] Marks batches as Deleted.

\item [{Delete\_Jobs}] Deletes jobs.

\item [{Delete\_Modems}] Deletes modems.

\item [{Delete\_Packages}] Deletes update packages.

\item [{Delete\_Tags}] Deletes tags.

\item [{Remove\_Tags}] Removes tags from modems.

\item [{Shutdown\_Server}] Shuts down servers by name.

\item [{Test\_Script}] Given a modem script, verifies that it will compile.

\item [{Update\_Batches}] Updates batches.

\item [{Update\_Jobs}] Updates jobs.

\item [{Update\_Modems}] Updates modems.

\item [{Update\_Passwords}] Updates modem passwords.

\item [{Update\_Settings}] Updates system settings.

\item [{Update\_Tags}] Updates tags.

\item [{View\_Batches}] Filters and dumps items from the batches table.

\item [{View\_Capabilities}] Dumps the published capabilities of all running
servers.

\item [{View\_History}] Filters and dumps items from the history table.

\item [{View\_Jobs}] Filters and dumps items from the jobs table.

\item [{View\_Logs}] Filters and dumps items from the logs table.

\item [{View\_Modems}] Filters and dumps items from the modems table.

\item [{View\_Packages}] Filters and dumps items from the packages table.

\item [{View\_Settings}] View system settings.

\item [{View\_Status}] Dumps status data related to modems (such as Latitude and
Longitude, for a GPS-enabled modem).

\item [{View\_TagMatch}] View the tags applied to a modem, or the modems with a
given tag.

\item [{View\_Tags}] Filters and dumps items from the tags table.
\end{description}

If an action in necessary that is not listed here, please contact Sixnet.


\section{CSVUtil}

\label{sec:CSVUtil}The CSVUtil program acts as a simple filter utility for
performing database-like operations on CSV data. It is able to add/remove
columns, intersect/union the rows of two tables, left/right/inner join the rows
of two tables, rename columns, and filter rows of a single table.


\subsection{Usage}

\label{sub:CSVUtilUsage}

\begin{Verbatim}[frame=single]
CSVUtil --action -l leftTable [-r rightTable]
        [leftColumn] [rightColumn]
\end{Verbatim}

If no \texttt{rightColumn} is passed, but one is required, the
\texttt{leftColumn} value will be used in both places.

Where \texttt{action} may be:

\begin{description}
\item [{\texttt{intersect}}] Given two tables (left and right) and two columns
(left and right), output all rows (with columns from both tables) where the
value in that column is equal between the rows. For example:

\examplebox{left.tbl:\\
\begin{tabular}{ccc}
A & B & C \\
\hline
1 & 2 & 3 \\
4 & 5 & 6 \\
\end{tabular}

right.tbl:\\
\begin{tabular}{ccc}
A & C & D \\
\hline
7 & 8 & 9 \\
8 & 3 & 4 \\
\end{tabular}

Result of \texttt{csvutil --intersect -l left.tbl -r right.tbl C}\\
\begin{tabular}{cccccc}
A & B & C & right.A & right.C & D \\
\hline
1 & 2 & 3 & 8 & 3 & 4 \\
\end{tabular}
}

\item [{\texttt{union}}] Similar to intersection, but instead of taking only the
rows with equal values, this takes all rows (preventing duplicates). Here is an
example using the same tables as from \texttt{intersect}:

\examplebox{\texttt{csvutil --union -l left.tbl -r right.tbl C}\\
\begin{tabular}{cccccc}
A & B & C & right.A & right.C & D \\
\hline
1 & 2 & 3 & 8 & 3 & 4 \\
4 & 5 & 6 &   &   &   \\
  &   &   & 7 & 8 & 9 \\
\end{tabular}
}

\item [{\texttt{add}}] Similar to union, but works on two tables with identical
column titles. Here is an example:

\examplebox{left.tbl:\\
\begin{tabular}{ccc}
A & B & C \\
\hline
1 & 2 & 3 \\
4 & 5 & 6 \\
\end{tabular}

right.tbl:\\
\begin{tabular}{ccc}
A & B & C \\
\hline
4 & 5 & 6 \\
7 & 8 & 9 \\
\end{tabular}

\texttt{csvutil --add -l left.tbl -r right.tbl}\\
\begin{tabular}{ccc}
A & B & C \\
\hline
1 & 2 & 3 \\
4 & 5 & 6 \\
7 & 8 & 9
\end{tabular}
}

\item [{\texttt{subtract}}] Removes rows from the left table with column values
that match the right table. Here is an example:

\examplebox{left.tbl:\\
\begin{tabular}{ccc}
A & B & C \\
\hline
1 & 2 & 3 \\
4 & 5 & 6 \\
\end{tabular}

right.tbl:\\
\begin{tabular}{ccc}
C & D & E \\
\hline
3 & 7 & 8 \\
7 & 8 & 9 \\
\end{tabular}

\texttt{csvutil --subtract -l left.tbl -r right.tbl C}\\
\begin{tabular}{ccc}
A & B & C \\
\hline
4 & 5 & 6
\end{tabular}
}

\item [{\texttt{left-join}}] Similar to union, but rather than adding all of the
rows from the right table to the left, this adds the headers from the right
table to the left and then populates what values it can when the given column
matches both left and right. If multiple rows match, more rows are added for
each. Here is an example:

\examplebox{left.tbl:\\
\begin{tabular}{ccc}
A & B & C \\
\hline
1 & 2 & 3 \\
4 & 5 & 6 \\
\end{tabular}

right.tbl:\\
\begin{tabular}{ccc}
C & D & E \\
\hline
3 & 7 & 8 \\
3 & 2 & 4 \\
7 & 8 & 9 \\
\end{tabular}

\texttt{csvutil --left-join -l left.tbl -r right.tbl C}\\
\begin{tabular}{cccccc}
A & B & C & right.C & D & E \\
\hline
1 & 2 & 3 &    3    & 7 & 8 \\
1 & 2 & 3 &    3    & 2 & 4 \\
4 & 5 & 6 &         &   &   \\
\end{tabular}
}

\item [{\texttt{right-join}}] Same as a left-join, but all rows from the
\textit{right} table will be in the result, populated with values from the left
table if they are available. Here is an example, using the same data:

\examplebox{\texttt{csvutil --right-join -l left.tbl -r right.tbl C}\\
\begin{tabular}{cccccc}
A & B & C & right.C & D & E \\
\hline
1 & 2 & 3 &    3    & 7 & 8 \\
1 & 2 & 3 &    3    & 2 & 4 \\
  &   &   &    7    & 8 & 9 \\
\end{tabular}
}

\item [{\texttt{add-column}}] Adds a column with a constant value to the left
table (does not require a right table). Here is an example:

\examplebox{left.tbl:\\
\begin{tabular}{cc}
A & B \\
\hline
1 & 2 \\
5 & 4 \\
\end{tabular}

\texttt{csvutil --add-column 3 -l left.tbl C}\\
\begin{tabular}{ccc}
A & B & C \\
\hline
1 & 2 & 3 \\
5 & 4 & 3 \\
\end{tabular}
}

\item [{\texttt{remove-column}}] Removes the named column from the left table.
Here is an example:

\examplebox{left.tbl:\\
\begin{tabular}{cccc}
A & B & C & D \\
\hline
1 & 2 & 3 & 4 \\
4 & 5 & 6 & 7 \\
\end{tabular}

\texttt{csvutil --remove-column -l left.tbl D}\\
\begin{tabular}{ccc}
A & B & C \\
\hline
1 & 2 & 3 \\
4 & 5 & 6 \\
\end{tabular}
}

\item [{\texttt{select}}] Filters the table to only show rows with the given
column equal to the specified value. Here is an example:

\examplebox{left.tbl:\\
\begin{tabular}{ccc}
A & B & C \\
\hline
1 & 2 & 3 \\
4 & 5 & 6 \\
5 & 2 & 7 \\
7 & 8 & 9 \\
\end{tabular}

\texttt{csvutil --select 2 -l left.tbl B}\\
\begin{tabular}{ccc}
A & B & C \\
\hline
1 & 2 & 3 \\
5 & 2 & 7 \\
\end{tabular}
}

\item [{\texttt{rename-column}}] Renames a column. Here is an example:

\examplebox{left.tbl:\\
\begin{tabular}{ccc}
A & B & right.C \\
\hline
1 & 2 &    3    \\
\end{tabular}

\texttt{csvutil --rename-column C -l left.tbl right.C}\\
\begin{tabular}{ccc}
A & B & C \\
\hline
1 & 2 & 3 \\
\end{tabular}
}

\end{description}


\subsection{Chaining with BVBatch}

To make the most efficient use of CSVUtil, use it as a filter for BVBatch
commands. For example, to apply the tag with Tag ID 1 to all modems:

\begin{Verbatim}[frame=single]
bvb -a View_Modems |
csvutil --add-column 1 TagID |
bvb -a Apply_Tags hdr
\end{Verbatim}


\section{License Management}

\label{sec:LicenseUpdate}At some point it may become necessary to replace the
license key (perhaps a more expansive license was just purchased, or the old
license is about to expire). To do this, save the license key file in a local
directory. Open that directory in a terminal, and execute this command (assuming
that \texttt{license.blf} is the file name of the new license):

\begin{Verbatim}[frame=single]
bvb --license license.blf
\end{Verbatim}

This will even work if the old license has already expired.

\chapter{Getting Started}

\label{cha:GettingStarted}

\section{Logging Into the Web UI}

\label{sec:LogInWebUI}To access the BlueVue Group user interface, use a web
browser to navigate to the URL provided by the system administrator. This is
often the IP address or name of the machine running the application server. When
connecting from the same machine, the URL will likely look like
\texttt{http://localhost:8080/BVWeb}). Assuming everything went well during
installation, a login page should be displayed with the Sixnet logo to the
top-left. Enter the user name and password provided by the system administrator.
During installation, a user named ``Root'' was created with a password; for the
first log-in, use that account.


\section{Quick-Start}

\label{sec:WalkThrough}This section describes some basic tasks, such as adding
modems to the system and viewing status updates on those modems.


\subsection{Creating Modems}

\label{sub:TutCreateModems}Once logged in (see \prettyref{sec:LogInWebUI}), a
list of all modems in the system will be displayed --- there are none, for the
moment. So the first task will be to add some:

\begin{enumerate}
\item On the left is a bar containing some menus (click on a menu header to
expand the choices). Open the menu labeled ``Navigate,'' and then click on
``Create'' to open up the modem creation page.

\item Type in the \xglossary{name={IP Address}, description={A series of four
numeric values, each in the range 0-255, separated by periods, which is used to
locate a device over a network.}}{IP Address} of a modem. Unless that modem
requires a password to log in, leave everything else as is, but note the various
fields on the page:

\begin{description}
\item [{\xglossary{name={Device ID}, description={An ESN or IMEI that uniquely
identifies a cellular modem.}}{Device~ID}}] The ESN or IMEI of the modem, if
available.

\item [{\xglossary{name={Port}, description={A numeric value used in conjunction
with an IP Address to identify a specific listening program on a
network.}}{Port}}] The network port on which the modem is listening (this will
likely never need to be changed).

\item [{Apply~Tag}] Tags will be discussed in \prettyref{sub:TagModems}.

\item [{Create~Automatic~Status~Query~Job}] Automatically create a recurring
job that will poll the modem for status information.

\item [{Password~Enabled}] Check this box if the modem expects a password.

\item [{Applicable~Interfaces}] The interfaces on which the modem enforces
password authentication. This does not affect BVG at all, but is used to reflect
the status of the modem after it has been polled for status information.

\item [{Password}] The password used to log into the modem and execute AT
commands. If the modem requires a password, type it in here.
\end{description}

\item Click ``Save'' in the navigation bar to the left. This will display a page
containing the same information as was just entered, but without any editable
fields. This page will display everything that the system knows about a single
modem. For the moment, the only information known is what was just entered.
\end{enumerate}

If the \xglossary{name={Job Server}, description={A program that runs in the
background on a computer, periodically checking for and running jobs from the
database.}}{job server} is running (see the installation guide for more
information about that), the job that was automatically created to poll status
information from the modem should promptly start executing.

Click on either the ``MODEMS'' link in the red navigation bar across the
top of the page, or on the ``All Modems'' \xglossary{name={Breadcrumb},
description={A link to a page displaying a superset of the information being
displayed on the current page.}}{breadcrumb} just above the bold ``Property''
and ``Value'' table headers. This will redisplay table containing all of the
modems in the system.

The table will automatically refresh itself approximately every five seconds.
The refresh is generally invisible, unless the data has changed.

The new modem will be visible in the table, as it is the only modem in the
system. The row will have a red, yellow, green, or white background, which
corresponds to a value in the ``Modem~Status'' column of ``Failure,''
``Inactive,'' ``Normal,'' or ``Unknown,'' respectively. These status values each
have specific meanings. The first status value in this list whose criteria
matches the modem defines the modem's status:

\begin{description}
\item [{\xglossary{name={Unknown}, description={A modem state indicating that
no attempt has been made within the past 24 hours (the Failure Timeout setting)
to contact the modem.}}{Unknown}}] No attempt to contact the modem has been made
within the past 24 hours (see the ``Failure Timeout'' setting in
\prettyref{apd:Settings} for more information).

\item [{\xglossary{name={Failure}, description={A modem state indicating that
the modem has not successfully answered a contact within the past 24 hours (the
Failure Timeout setting).}}{Failure}}] Contact has been attempted, but no
successful contact has been established within the past 24 hours (the ``Failure
Timeout'' setting).

\item [{\xglossary{name={Inactive}, description={A modem state indicating that
the modem has not successfully answered a contact within the past six hours
(the Warning Timeout setting), but has successfully answered within the last 24
hours (the Failure Timeout setting).}}{Inactive}}] Contact has been attempted,
but the most recent successful communication was more than six hours ago (the
``Warning Timeout'' setting).

\item [{\xglossary{name={Normal}, description={A modem state indicating that
the modem has been successfully contacted within the past 6 hours (the Warning
Timeout setting).}}{Normal}}] Otherwise --- the modem has been contacted
successfully within the past six hours (the ``Warning Timeout'' setting).
\end{description}

When a job server is running and it has not yet attempted to contact a modem,
the status will be ``Unknown'' because no contact has been attempted within the
past 24 hours. If it has tried, the status will be one of ``Normal,'' indicating
that contact was made and status information was retrieved successfully;
``Failure,'' indicating that no successful contact has been made within the past
24 hours, although it has been attempted; or ``Inactive,'' indicating that the
modem responded successfully within the last 24 hours, but not within the last
six.

``Failure'' red rows do not necessarily mean that something is broken. It could
mean that the modem is currently \xglossary{name={Offline}, description={When a
modem is off or has no signal, and therefore is unable to communicate, it is
described as \texttt{offline}.}}{offline}, has a different IP address than
configured, or is not within range of a cell tower. Check for these scenarios
before deciding that something is malfunctioning.


\subsubsection{Batches of Modems}
\label{ssub:ModemBatches}Modems can be added to the system in batches from a
list of IP addresses in a file. A spreadsheet program (like Microsoft Excel or
OpenOffice Calc) can be used to generate a file in the right format. Type
``IPAddress'' (without the quotes) into the top-left cell, and then type in the
IP addresses of the modems, one per row, in the same column beneath
``IPAddress''.

Once the information is in the spreadsheet, save the file in ``Comma-Separated
Values (*.csv)'' format.

A text editor (such as Notepad or GEdit, not a word processor) may also be used.
Type in ``IPAddress'' on the first line, then each of the IP addresses of the
modems, one per line, beneath that. Again, save as a \xglossary{name={CSV
File}, description={A file representing tabular data with columns separated by
commas (commas within a column are enclosed within quotes), and rows separated
by newlines.}}{CSV file}.

\notebox{When using Windows Notepad, when in the ``Save As'' dialog, enclose
the name of the file in double-quotes to keep Notepad from appending ``.txt'' to
the file name.}

Switch back to the browser window. From the table that displays all modems,
open the ``Navigate'' menu on the left and click on ``Batch Create.'' In the
field labeled ``CSV File'' browse for the prepared CSV file.

The other fields may be left as they are, but here is a quick description of
each:
\begin{description}
\item [{Apply~Tag}] Discussed in \prettyref{sub:TagModems}.

\item [{Create~Automatic~Status~Query~Jobs}] Like before, this automatically
creates recurring jobs that will poll the status of each of these modems.

\item [{Batch~Name~for~Status~Queries}] If provided, this will group the status
queries that are created into a \xglossary{name={Batch}, description={A name
applied to a group of jobs and their corresponding history to make for easy
progress checks.}}{batch} with this name. Batches are discussed in detail in
\prettyref{sec:Batches}.
\end{description}

Click on ``Save'' to create the modems. If there was a typo in the file (such as
an invalid or duplicate address), an error message will be displayed; fix the
error and try again. Upon submitting a properly formatted file, the modem list
page will be displayed again, but now it will include all of the modems that
were in the CSV file.

If a job server is running, the page will change as BVG discovers the
automatically created status jobs and polls the status of each of the newly
added modems. The rows will change color and be populated with more information
as it becomes available.


\subsection{Viewing Modem Status}

\label{sub:TutViewStatus}Now click on the value in the ``Modem Label'' column
for one of the modems that has a modem status of ``Normal'' (the row should have
a green background). This will display the complete modem status view for that
modem.


\subsection{Tagging Modems}

\label{sub:TagModems}Modems may be put into groups for easier navigation using
\xglossary{name={Tag}, description={A name, a polling interval, a configuration
script, and an ordering value used to group modems and apply similar settings to
them.}}{Tags}. A modem may have multiple tags.

Click on the \texttt{TAGS} link in the top navigation bar. An empty table is
shown, but instead of being for modems, this is for tags.

Along the left navigation bar, expand the ``Navigate'' menu and click ``Create''
to create a new tag. Choose a name for it, but leave the rest of the fields
blank. If the modems in the system belong to a logical group, use a descriptive
name. In this example, the name ``Cruisers'' will be used.

Here is what the other columns do:

\begin{description}
\item [{Position}] Tags may have configuration scripts assigned to them, which
will be run on the modems with those tags. But since a modem may have multiple
tags applied to it, the ordering of those scripts is important. That is what the
``Position'' does; when configuration scripts are run on modems, they are run in
order of ascending position. By leaving the field blank, the system will choose
the next available position. If a value is entered, the system will reorder
existing tags as necessary to place the tag at that position.

\item [{Polling~Interval}] When ``Automatic'' jobs (jobs that will automatically
reschedule themselves after completion) run against a modem, this is the number
of minutes between executions\footnote{A modem with multiple tags will use the
smallest polling interval between its associated tags. A polling interval of
zero is ignored. If no valid (non-zero) polling interval is found, the value of
the ``No Tag Polling Interval'' setting (see \prettyref{apd:Settings}) will be
used instead.}
\end{description}

Once the tag name is entered, click ``Save'' in the navigation bar on the left.
This will display a details page similar to the modem details page, but for the
newly created tag. The ``Modem Count'' is 0 because this tag has not yet been
applied to any modems.

Switch back to the \texttt{MODEMS} view and check the boxes next to the modems
to which the new tag should be applied. To apply the tag to all (or most) of
the modems, use the ``Select All'' link at the top and deselect any of the
modems that should not be tagged.

Click on the ``Tag'' link in the navigation bar at the left, under ``Update
Selected Modems.'' Choose the tag to apply to the modems from the drop-down list
(``Cruisers'' in this example) and verify that the tag should be applied to all
of the modems listed. If any of the modems in the list should not be tagged,
uncheck their boxes. To apply the tag to the modems, click on the ``Tag'' link
in the left navigation pane.

The All Modems table is displayed again. Click on the ``Filter by Tag'' menu
header to the left, and then on the ``Cruisers'' tag. The table will update to
show only the modems with that tag (obviously, if the tag was applied to all of
the modems, the table will look the same).

Look at the breadcrumbs (the text just above the ``Select None'' and ``Select
All'' links): It should now look something like ``All~Modems~|~Cruisers~(~X~).''
This means that filters have been applied to the table. As more filters are
applied (for example, by clicking on some of the fields in the table), this list
will get longer. Any of the entries in the list may be clicked to remove all
of the filters that come after that entry, or the ``(~X~)'' next to any filter
may be clicked to remove that filter but leave the rest.


\subsection{Jobs}

Now that the system contains some tagged modems, a configuration script will be
written to run in those modems, and a job will be created to execute the script.

Switch back to the \texttt{TAGS} table and click on the ``Cruisers'' tag. This
will display the tag details page. Now click on the ``Create'' link in the
``Configuration Script'' field.

This script will not make any changes to the modem, but will demonstrate basic
scripting:

\begin{Verbatim}[frame=single]
sendln 'AT+BMNAME?'
waitln 'OK'
\end{Verbatim}

Click the ``Save'' link in the navigation bar to the left.

Go back to the \texttt{MODEMS} page. Check the boxes next to the modems to
reconfigure, remembering that the script is associated with a tag, so it will
only be run on modems with that tag. On the left, choose ``Schedule Jobs'' to
create the jobs that will run this script.

In the next page, choose ``Reconfiguration'' from the drop-down list\footnote{If
the ``Type'' drop-down list is empty, then either there is no job server
running, or the job server and web server have vastly different ideas about what
time it is. Make sure that the job server is running, and compare the system
clocks between the two, updating as necessary. It is helpful to run an NTP
(Network Time Protocol) client on each server to keep the system clock
synchronized with other computers on the Internet.}. Type in a name for this
group of jobs being created into the ``Batch'' field. ``Name Check'' will be
used for this example. Leave the other fields with their default values. This is
what they are for:

\begin{description}
\item [{Scheduled~Time}] The date and time when the job should be executed. This
defaults to the current time, so the job server will see the job is past due and
run it as soon as possible. When editing these fields, keep the formatting of
the date and time as it is shown; using a different format will confuse the
program and cause it to complain. In browsers other than Microsoft's Internet
Explorer 7, a calendar will be displayed when the date field is clicked. A date
may be chosen either by manipulating the calendar and clicking on a day, or by
typing a value into the field.

\item [{Automatic}] Whether the job should automatically reschedule itself to
run again in the future (according to the ``Polling Interval'' on the modem's
tags) after it has completed. In most cases, automatic rescheduling should only
be used with status query jobs.

\item [{Mobile-Originated}] Whether the job is run by the BEP server (instead of
the Job server) in response to receiving a BEP message --- the job is initiated
by a message from the modem, rather than by a message from the server.

\item [{Always~Available}] A mobile-originated job will act like any other job;
it will be rescheduled for some time in the future if it fails, or if it is
automatic. But it may be desirable to always execute a given mobile-originated
job, any time the modem calls in. Setting this property will cause a job
rescheduling to always choose the current time (when the job executes), instead
of some time in the future.

\item [{Update~Package}] When performing a firmware update, this is the package
to load into the modems.

\item [{Force Update}] When performing a firmware update and the chosen firmware
package is of the same version as what is running in the modem, checking this
box will cause the update to occur anyway. If unchecked, no update will be
performed because the modem is already running the appropriate firmware.
\end{description}

Finally, once the proper modems have their boxes checked, click on the ``Save''
link. This will display the All Jobs table. In the navigation bar on the left,
expand the ``Filter by Batch'' menu and click on the ``Name Update'' batch. This
will show only the jobs that are in that batch. Remember, there are also some
automatic status query jobs which were shown in the same table before filtering.

Now, just leaving the page alone, the table should automatically update itself
as the jobs execute. If a job fails for some reason, its ``Tries'' field will
increment, its background will change from green to yellow, and its scheduled
time will change to a minute in the future. If it fails again, its ``Tries''
field will increment again, and its scheduled time will change to two minutes in
the future. If it fails a third time, it will simply disappear. In the case of
an automatic job, a new and (almost) identical job will be scheduled for one
``Polling Interval'' in the future (defaulting to one hour if the modem had no
tags applied to it). A number of these parameters may be altered through
settings (see \prettyref{apd:Settings}).

Eventually, the table should become empty, as all of the jobs have either
succeeded or failed, and none of them were automatic. Take a quick look at the
\texttt{HISTORY} table to see why any failures occurred, and how long the
successful jobs actually took. The \texttt{LOGS} table may also be interesting;
it shows any messages logged by the system. From following this guide, it will
include the modem creations, automatic status query job creations, job
executions and reschedulings, tag creation, tag applications, and then the rest
of the job creations and executions.

Now go to the \texttt{BATCHES} table. This will show the ``Name Update'' batch.
Now that the jobs have all completed, check its box and use the ``Delete'' link
on the left to delete it. Confirm by clicking ``Delete'' again when the display
changes.

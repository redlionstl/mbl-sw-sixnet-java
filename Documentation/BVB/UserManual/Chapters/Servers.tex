\chapter{Servers}

\label{cha:Servers}

\notebox{All servers should use time synchronization with the same source. If
the ``current time'' on the servers is significantly different, the Capabilities
table may be interpreted to indicate that a server has crashed due to not having
updated its entry recently. This will result in that server's locks being
reclaimed, and may result in any job types supported by that server not showing
up on the Job Creation page in the web interface.}


\section{BlueVue Group Servers}

\label{sec:BVGServers}BlueVue Group comes with three servers.


\subsection{The Job Server (BVJobD)}

\label{sub:BVJobD}The job server is in charge of polling the database for
jobs that are scheduled to run now or in the past, and spinning off threads for
each of those jobs. It does proper locking of jobs and modems so that multiple
job servers will interact cooperatively, allowing for scalability of the
system simply by setting up additional job servers.

The job server performs a number of periodic threaded tasks:

\begin{description}
\item [{Plug-in Watcher}] The server scans the configured plug-in directories
(containing job, modem, and database plug-ins) periodically, so that dropping
new plug-ins into place seamlessly adds the corresponding functionality to the
server.

\item [{Setting Refresher}] The server checks the Settings table in the database
(see \prettyref{apd:Settings}) to make sure new settings take effect soon after
they are set.

\item [{Logger}] To prevent database logging from slowing down other operations,
a logger stores messages and dumps them to the database Logs table periodically.

\item [{Capabilities Updater}] The Capabilities table in the database needs to
be updated regularly to prevent other servers from thinking that this server has
crashed.

\item [{Capabilities Purger}] Watches for servers that have disappeared,
reclaiming any locks that they may have left behind.

\item [{History Purger}] Keeps the History table in the database from growing
too large with ancient items. Periodically removes the oldest items from the
table.

\item [{Lock Purger}] Just in case something prevents this server from properly
clearing its own locks when it finishes with jobs or modems, the lock purger
will clear old locks owned by this server periodically.

\item [{Job Poller}] Checks the Jobs table periodically for any jobs that have
passed their scheduled execution times, claims them, and passes them off to the
executor.

\item [{Executor}] Maintains a thread pool for running new jobs, and a queue for
holding onto jobs until there are free threads to take them.
\end{description}

In addition to those periodic tasks, each job runs in its own thread. For best
performance, use a system with multiple CPU cores, as each thread can run in its
own core.


\subsection{The IP Registration Server (BVIPRegD)}

\label{sub:BVIPRegD}The IP registration server is in charge of listening for IP
registration messages coming from modems. Use the \texttt{AT+BIPREG} command on
a modem to configure it to report in to the server when either its public IP
address changes, or after a given timeout (in minutes) occurs. This keeps the
database up-to-date, allowing the job server to have uninterrupted access to
modems.

Additionally, the IP registration server can be configured to actually create
new modems when IP registration messages arrive from devices that are not
currently in the system. This could be a useful tool for populating the system,
if modems already exist in the field and a group update script is available that
could instruct them all to report to the IP registration server.

The IP registration server runs many of the same periodic tasks as the Job
Server.


\subsection{The BEP Server (BVBepD)}

\label{sub:BVBepD}The BEP server is in charge of listening for BEP (BlueTree
Event Protocol) messages coming from modems. Use the Event Handling AT commands
to configure a modem to send BEP messages periodically. These will keep the
database up-to-date in a more complete fashion than the IP registration server
can, as BEP messages contain a much richer information payload.

Additionally, the BEP server enables MOM (Mobile Originated Management). This
technology allows the modem to call in and ask for tasks, rather than the Job
server contacting the modem and giving it tasks. The result is that a modem on
a network that blocks incoming traffic will still be able to communicate and be
given instructions.

The BEP server may also, like the IP Registration server, be configured to
create new modems if BEP messages arrive from unknown devices.

The BEP server runs many of the same periodic tasks as the Job and IP
Registration servers.


\notebox{If a modem reports a new IP address that is currently stored as the
address of another modem, the database update will fail because two modems may
not have the same IP address. For example, it is possible for two modems to
receive new IP addresses at the same time, and for one of them to receive the
other's old IP address. If the one that just received the other's old IP sends
an IP registration or BEP message first, the update will fail. For this reason,
it is a good idea to program modems to send IP registration or BEP messages
regularly, and not just in response to an IP address change.}


\section{Third Party Servers}

\label{sec:ThirdPartyServers}BlueVue Group uses two third-party servers
(software developed by an organization not affiliated with Sixnet/BlueTree).
These are the database server and the application server.


\subsection{The Database Server}

\label{sub:DatabaseServer}BlueVue Group requires the use of a database server.
Currently, only PostgreSQL (pronounced \textbf{post}-gres-que-ell) version 8.3
is supported. The architecture through which the database is accessed is
flexible, however, and new database plug-ins may be developed in the future.

The database server handles translating network queries and commands into
database accesses. BVG uses this to store persistent information about the
modems and jobs, and to communicate between the BVG servers and user interfaces.


\subsection{The Application Server}

\label{sub:ApplicationServer}The application server provides the web interface.
It runs a program called a Web Application (usually shortened to webapp),
developed by Sixnet, which interfaces with the database server to provide
information and allow modification of data.

BVG supports the Apache Tomcat application server, version 5.5, although (for
the most part) webapps are portable and may be run by any application server.
Other examples of application servers (which are \textit{not} supported) are
JBoss and GlassFish.

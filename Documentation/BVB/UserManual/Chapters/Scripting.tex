\chapter{Scripting}

\label{cha:Scripting}Later versions of BlueTree modems support a simple subset
of the TeraTerm scripting language (TTL), allowing only the wait and send
commands. Additionally, the ``wait'' command maps to the ``waitln'' command,
and the ``send'' command maps to the ``sendln'' command.


\section{Language Basics}

\label{sec:LanguageBasics}For simplicity, BlueVue Group handles all modem
configuration scripts as TTL scripts, translating to raw AT commands for modems
that need them\footnote{Actually, the current version of BVG translates all
scripts to raw AT commands, but future versions will likely send TTL scripts in
update packages to those modems that can understand them.}.

BVG also provides a few additional features to the basic TTL support. An
\textit{enhanced} TTL script is compiled into the simple TTL script format
supported by BlueTree modems before being run on a modem. The enhanced scripting
language includes variables and conditionals. Variables are a part of the
original TTL language, and are implemented in the same manner. Conditionals (in
the form of \texttt{if} statements) are a new feature.

These enhanced scripts are useful because a configuration script may be present
in a tag that is applied to a number of modems with differing models or
firmware revisions. For example, perhaps part of the script modifies the GPS
settings in the modems, but not all of the modems with that tag have GPS
functionality. This problem can be solved by the addition of variables and
conditionals.

Another example: Perhaps there is a tag named ``NYC'' which is applied to all
modems in New York City. Some of these modems (model BT-5600, which includes
GPS) are in vehicles, but some (model BT-4600, which does not include GPS) are
in stationary positions. To configure the GPS modems to report their position to
an AVL (Automatic Vehicle Location) server, but have the script run without
error on the stationary modems, something like this may be used:

\begin{Verbatim}[frame=single]
if (model == 'BT-5600') {
  sendln 'AT+BGPSDS=1,"1.2.3.4",6666,1'
  waitln 'OK'
  sendln 'AT+BGPSRD=1'
  waitln 'OK'
}
\end{Verbatim}


\section{Variables}

\label{sec:Variables}Variables may be set within a script for later use. For
example, this example will set the variable ``modemName'' to be the
concatenation of the modem model and its firmware revision number, with a dash
in between:

\begin{Verbatim}[frame=single]
modemName = model . '-' . versionFW
\end{Verbatim}

Variables are not saved, however, so any changes to script variables are lost as
soon as the script has finished being compiled.

A number of variables are pre-set when a script is compiled:

\begin{tabular}{l | p{3in}}
Variable Name & Description \\
\hline
\texttt{deviceID} & The device ID (ESN or IMEI) of the modem, or ``?'' if
unknown \\

\texttt{alias} & The modem's alias, or ``?'' if unknown \\

\texttt{model} & The modem's model number, or ``?'' if unknown \\

\texttt{cfgVersion} & The modem's configuration version (as retrieved by
``AT+BCFGV?''), or ``?'' if unknown \\

\texttt{ipAddress} & The modem's IP address, or ``?'' if unknown \\

\texttt{port} & The modem's port number for communication \\

\texttt{phoneNumber} & The modem's phone number, or ``?'' if unknown \\

\texttt{password} & The modem's password, or ``'' if blank/unknown \\

\texttt{passwordEnabled} & ``0'' = disabled, ``1'' = enabled, ``?'' = unknown \\

\texttt{passwordAccess} & What the password is protecting: ``0'' = WAN, ``1'' =
both, ``2'' = LAN, ``?'' = unknown \\

\texttt{serial} & The modem's serial number, or ``?'' if unknown \\

\texttt{versionFW} & The modem's currently running firmware version, or ``?'' if
unknown \\

\texttt{versionHW} & The modem's HW version number, or ``?'' if unknown \\

\texttt{versionRF} & The modem's RF version number, or ``?'' if unknown \\

\texttt{versionGPS} & The modem's GPS version number, or ``?'' if unknown \\

\texttt{versionUBoot} & The modem's UBoot version number, or ``?'' if unknown \\

\texttt{versionPRL} & The modem's PRL version number, or ``?'' if unknown \\
\end{tabular}

Additionally, more variables may be defined for each individual modem. If a
variable is accessed that has not been set, however, the script will fail to
compile and the modem will not be reconfigured.


\section{What Scripts are Run?}

\label{sec:ScriptOrder}Scripts may be specified within tags, and also within
each individual modem. Scripts are run in ascending ``Position'' order from the
tags that are applied to a modem, followed by the modem's own script. For
example, if modem A has three tags, ``NYC,'' ``USA,'' and ``Northeast,'' applied
to it, with ``Position'' fields of 3, 1, and 2 respectively, and additionally it
has its own private configuration script, these scripts will be run in this
order: ``USA,'' ``Northeast,'' ``NYC,'' and finally the modem's own script.


\section{Language Details}

\label{sec:LanguageDetails}The enhanced TTL language is compiled into the simple
TTL language that is supported by BlueTree modems. This is performed in two
discrete steps, compilation and execution, followed immediately by execution on
a modem. This may seem a bit confusing, but it is necessary to produce a simple
TTL script which may be sent as-is to a modem for remote execution, without
access to any variables or other enhanced features.

Compilation of an enhanced TTL script produces an in-memory executable, which
when executed outputs a simple TTL script consisting of only wait and send
statements. For example, this enhanced TTL script:

\begin{Verbatim}[frame=single]
if (var == '1') {
  sendln 'Hi'
} else {
  sendln 'Hello'
}
\end{Verbatim}

\noindent will compile properly. But when executed, the variable ``var'' will
not have a value, and will cause the script to fail. If the variable ``var'' had
a value of ``1'' then the script would run to produce this output:

\begin{Verbatim}[frame=single]
sendln 'Hi'
\end{Verbatim}

\noindent which may be run in a modem (although it would not actually do
anything).


\subsection{Comments}

\label{sub:Comments}Comments begin with a semicolon (\texttt{;}) character and
extend to the end of the line. They are dropped during compilation, but may be
useful to make notes about what the goals are while writing a TTL script.


\subsection{Variables}

\label{sub:Variables}A variable name starts with a lower- or upper-case letter,
which may be followed by any number of letters, digits, or underscores. Variable
names are case-sensitive, so ``myVariable,'' ``myvariable,'' and ``MyVariable''
may each have its own unique value without interfering with the others.

Variables have values assigned to them either immediately before execution,
pulling data out of the modem's entry in the database, or during execution using
the \texttt{=} operator. Variable values are always strings (character data).


\subsection{Strings}

\label{sub:Strings}All variable values are strings (a string is a ordered list
of characters, such as a single letter, word, or sentence). Strings may be
concatenated with the \texttt{.} operator (like in Perl). For example:

\begin{Verbatim}[frame=single]
var = 'a'
; Variable 'var' now has the value 'a' (without the quotes)

var = var . 'b'
; Variable 'var' now has the value 'ab'

var = 'b' . 'a'
; Variable 'var' now has the value 'ba'

var = var . var . var
; Variable 'var' now has the value 'bababa'
\end{Verbatim}


\subsection{Conditionals}

\label{sub:Conditionals}Groups of statements may be executed conditionally based
on comparisons. A conditional starts with the word \texttt{if} followed by an
open parenthesis (\texttt{(}). The comparison or comparisons are made within the
parenthesis, and are followed by a close parenthesis (\texttt{)}). A group of
statements then follows, surrounded by braces (\texttt{\{} and \texttt{\}}).
This is called a ``code block,'' or just a ``block.'' This group of statements
is only executed if the conditional evaluated to true (for example, $5 > 3$
evaluates to true).

Additional blocks may be added, dependent on the first conditional evaluating to
false. These are called \texttt{else} blocks. There may be any number of
\texttt{else if} blocks, which act just like the \texttt{if} block, and there
may be one final \texttt{else} block without a conditional \texttt{if}. Here is
an example:

\begin{Verbatim}[frame=single]
if (a > b) {
  ; Block 1
} else if (b > c) {
  ; Block 2
} else if (a < b) {
  ; Block 3
} else {
  ; Block 4
}
\end{Verbatim}

In that example, if $a > b$, then the statements in Block 1 will execute. If
$a \ngtr b$ but $b > c$, then the statements in Block 2 will execute.
Continuing down, if $b \ngtr c$ but $a < b$, then the statements in Block 3 will
execute. Finally, if none of those conditions were met, the statements in Block
4 will execute. In a group of conditionals like this, only \textit{one} code
block will execute, never more or less. If there was no final \texttt{else}
block, then either zero or one block would execute, never more than one.


\subsection{Comparison Operators}

Conditional statements are made up of comparisons. Comparisons between values
are performed by \textit{Comparison Operators} such as $>$ and $==$. Here is a
list of the supported comparison operators:

\begin{tabular}{l | p{3.5in}}
Operator & Description \\
\hline
$==$ & Compare for string equality ('a' $==$ 'a' is true, 'a' $==$ 'A' is
false) \\

$!=$ & Compare for string inequality ('a' $!=$ 'a' is false, 'a' $!=$ 'A' is
true) \\

$==i$ & Compare for string equality, ignoring case ('a' $==i$ 'A' is true,
'a' $==i$ 'b' is false) \\

$!=i$ & Compare for string inequality, ignoring case ('a' $!=i$ 'A' is false,
'a' $!=i$ 'b' is true) \\

$<$ & Compare for version number order ('1' $<$ '2' is true, '1.2.3' $<$ '3.2.1'
is true, '1' $<$ '1' is false, '2' $<$ '1' is false) \\

$>$ & Compare for version number order ('1' $>$ '2' is false, '1.2.3' $>$
'3.2.1' is false, '1' $>$ '1' is false, '2' $>$ '1' is true) \\

$<=$ & Compare for version number order ('1' $<=$ '2' is true, '1.2.3' $<=$
'3.2.1' is true, '1' $<=$ '1' is true, '2' $<=$ '1' is false) \\

$>=$ & Compare for version number order ('1' $>=$ '2' is false, '1.2.3' $>=$
'3.2.1' is false, '1' $>=$ '1' is true, '2' $>=$ '1' is true) \\
\end{tabular}

Additionally, multiple comparisons may be made within an if statement using the
Boolean operators \texttt{\&\&} (and) and \texttt{||} (inclusive or). These are
evaluated in left-to-right order. For example:

\begin{Verbatim}[frame=single]
if (a < b && b < c || a > c) {
  ; ...
}
\end{Verbatim}

\noindent will execute the code block only if $a < b$ and $b < c$, or if
$a > c$.


\subsection{Quotes}

\label{sub:Quotes}Either single quotes (\texttt{\'}) or double quotes
(\texttt{\"}) may be used enclose strings. Strings may even include quotes,
when the string itself enclosed in the other kind of quote.


\subsection{Commands}

\label{sub:Commands}The commands of the language are \texttt{send} (and its
equivalent \texttt{sendln}), and \texttt{wait} (and its equivalent
\texttt{waitln}). The rest of the line after one of these commands (minus any
comments) are treated as a value expression, which may consist of a single
string or variable, or a concatenation of any number of these values.

\notebox{Send and wait commands are built using the values that provided to
create a command with a single string following it. As a result, this string
must be able to be enclosed in quotes (of either sort). If the string
concatenation operator is used to build a string containing both types of
quotes, no such command can be created and the script execution will fail.}

/*
 * PowerStrip.java
 *
 * A simple set of glowing/darkened dots in a small window, sits where you want.
 * When clicked, a darkened dot will turn on a corresponding discrete output
 * on a device, and light itself if successful. A glowing dot will turn off
 * the corresponding discrete output and darken if successful.
 * right-clicking will bring up a configuration dialog, allowing you to choose
 * the station number of the remote station, the local computer, the number of
 * discrete outputs, and the orientation of the strip (horizontal/vertical).
 *
 * Jonathan Pearson
 * January 10, 2008
 *
 */

package com.sixnetio.PowerStrip;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.concurrent.TimeoutException;

import javax.swing.JColorChooser;
import javax.swing.JOptionPane;

import org.jonp.xml.Tag;
import org.jonp.xml.XML;

import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.util.Utils;

public class PowerStrip extends Frame implements Runnable {
	// Constants
	private static final String PROG_VERSION = "1.3.2";
	private static final String SETTINGS_FILE = System.getProperty("user.home") + File.separator + ".powerstriprc";
	
	// main
	public static void main(String[] args) {
		try {
			new PowerStrip();
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null, "Unable to start the application: " + ioe.getMessage());
			System.exit(1);
		}
	}
	
	// Class Light
	private class Light extends Canvas implements MouseListener, MouseMotionListener {
		private short index;
		
		public Light(short index) {
			this.index = index;
			
			addMouseListener(this);
			addMouseMotionListener(this);
			setPreferredSize(new Dimension(16, 16));
			setBackground(C_Background);
		}
		
		@Override
        public void paint(Graphics g) {
			setBackground(C_Background);
			if (state[index]) {
				g.setColor(C_On);
			} else {
				g.setColor(C_Off);
			}
			g.fillOval(0, 0, 16, 16);
		}
		
		public void mouseEntered(MouseEvent e) { }
		public void mouseExited(MouseEvent e) { }
		public void mouseMoved(MouseEvent e) { }
			
		public void mousePressed(MouseEvent e) {
			if (optionsVisible) {
				mouseX = e.getX();
				mouseY = e.getY();
			}
		}
		
		public void mouseReleased(MouseEvent e) {
			mouseX = mouseY = -1;
		}
		
		public void mouseDragged(MouseEvent e) {
			if (optionsVisible) {
				Point p = PowerStrip.this.getLocation();
				p.x += e.getX() - mouseX;
				p.y += e.getY() - mouseY;
				PowerStrip.this.setLocation(p);
			}
		}
		
		public void mouseClicked(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON3) {
				new OptionsDialog();
			} else if (e.getButton() == MouseEvent.BUTTON1) {
				if (optionsVisible) return;
				
				Thread th = new Thread(new Runnable() {
					public void run() {
        				boolean[] vals = new boolean[1];
        				vals[0] = true;
        				
        				try {
        					if (state[index]) {
        						// Turn it off
        						udr.clrD(UDRMessage.STA_ANY, index, (short)1, vals);
        					} else {
        						// Turn it on
        						udr.setD(UDRMessage.STA_ANY, index, (short)1, vals);
        					}
        					
        					timedOut = false;
        					xbutton.repaint();
        				} catch (IOException ioe) {
        					getToolkit().beep();
        				} catch (TimeoutException te) {
        					timedOut = true;
        					xbutton.repaint();
        					
        					getToolkit().beep();
        				}
					}
				});
				th.start();
			}
		}
	}
	
	// Class XButton
	private class XButton extends Canvas implements MouseListener {
		public XButton() {
			addMouseListener(this);
			setPreferredSize(new Dimension(16, 16));
			setBackground(C_Background);
		}
		
		@Override
        public void paint(Graphics g) {
			setBackground(C_Background);
			if (timedOut) {
				g.setColor(C_Off);
			} else {
				g.setColor(C_On);
			}
			
			g.drawLine(2, 2, getWidth() - 2, getHeight() - 2);
			g.drawLine(2, getHeight() - 2, getWidth() - 2, 2);
		}
		
		public void mouseEntered(MouseEvent e) { }
		public void mouseExited(MouseEvent e) { }
		public void mousePressed(MouseEvent e) { }
		public void mouseReleased(MouseEvent e) { }
		
		public void mouseClicked(MouseEvent e) {
			if (!optionsVisible) System.exit(0);
		}
	}
	
	// Private data members
	// For moving the window around
	private int mouseX = -1,
	            mouseY = -1;
	private boolean optionsVisible = false;
	
	private boolean horizontal = true;
	
	private int stateCount = 8;
	private boolean[] state;
	
	private Light[] leds;
	private XButton xbutton;
	
	private boolean timedOut = false; // If the last communication attempt timed out, this becomes true
	
	private String remoteAddress = "10.2.0.1";
	private boolean useUDP = true;
	
	private Point windowPosition = new Point(0, 0);
	
	private Color C_On = Color.RED;
	private Color C_Off = new Color(128, 0, 0);
	private Color C_Background = Color.BLACK;
	
	private UDRLib udr;
	
	// Constructor
	public PowerStrip() throws IOException {
		super("Power Strip");
		
		addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		loadOptions();
		
		try {
			if (useUDP) {
				MessageDispatcher.getDispatcher().registerHandler(new UDRLink(TransportMethod.UDP, remoteAddress));
			} else {
				MessageDispatcher.getDispatcher().registerHandler(new UDRLink(TransportMethod.TCP, remoteAddress));
			}
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null, "Unable to connect to station. Please right-click to set options and check the address, then restart the application.", "Error", JOptionPane.ERROR_MESSAGE);
		}
		
		udr = new UDRLib((short)126, (byte)126); // Pretty much random, doesn't matter what we choose
		udr.setTries(1);
		
		state = new boolean[stateCount];
		
		buildUI(false);
		
		Thread th = new Thread(this);
		th.start();
	}
	
	// run
	public void run() {
		int rounds = 0; // Do a System.gc() every 30 rounds (to avoid wasting too much CPU time)
		
		while (true) {
			if (rounds >= 30) {
				System.gc(); // We do a lot of regular communication, which discards objects like tissues, so clean up regularly
				rounds = 0;
			}
			
			rounds++;
			
			Utils.debug("Requesting status");
			boolean[] dos;
			
			try {
				dos = udr.getD(UDRMessage.STA_ANY, UDRMessage.T_D_DOUT, (short)0, (short)stateCount);
				
				timedOut = false;
				xbutton.repaint();
			} catch (IOException ioe) {
				// Can't do anything about this...
				Utils.sleep(1000);
				continue;
			} catch (TimeoutException te) {
				timedOut = true;
				xbutton.repaint();
				
				continue; // No point sleeping, since we just timed out (probably 3 seconds)
			}
			
			for (int i = 0; i < dos.length; i++) {
				if (state[i] != dos[i]) {
    				state[i] = dos[i];
    				
    				leds[i].repaint();
				}
			}
			
			System.gc(); // Keep things clean; the structures used for communication are quite large
			
			Utils.sleep(1000);
		}
	}
	
	// buildUI
	private void buildUI(boolean decorate) {
		setUndecorated(!decorate);
		setAlwaysOnTop(!decorate);
		
		if (horizontal) {
			setLayout(new GridLayout(1, state.length + 1));
		} else {
			setLayout(new GridLayout(state.length + 1, 1));
		}
		
		leds = new Light[state.length];
		for (int i = 0; i < state.length; i++) {
			leds[i] = new Light((short)i);
			add(leds[i]);
		}
		
		xbutton = new XButton();
		add(xbutton);
		
		pack();
		setLocation(windowPosition);
		setVisible(true);
	}
	
	// Load/Save options
	private void loadOptions() throws IOException {
		File file = new File(SETTINGS_FILE);
		if (!file.exists()) return;
		if (!file.isFile()) throw new IOException("Settings file " + SETTINGS_FILE + " is not a regular file");
		
		XML parser = new XML();
		Tag root = parser.parseXML(SETTINGS_FILE, false, false);

		if (!root.getName().equals("powerstrip")) throw new IOException("Settings file was not generated by this program");
		if (root.getFloatAttribute("version") != 1.0) throw new IOException("Settings file was generated by a newer version of this program");
		
		horizontal = root.getBooleanContents("horizontal");
		remoteAddress = root.getStringContents("remoteaddr");
		useUDP = root.getBooleanContents("udp");
		stateCount = root.getIntContents("outputcount");
		if (stateCount < 1 || stateCount > 16) stateCount = 8;
		
		C_On = new Color(root.getIntAttribute("oncolor r", C_On.getRed()),
		                 root.getIntAttribute("oncolor g", C_On.getGreen()),
		                 root.getIntAttribute("oncolor b", C_On.getBlue()));
		
		C_Off = new Color(root.getIntAttribute("offcolor r", C_Off.getRed()),
		                  root.getIntAttribute("offcolor g", C_Off.getGreen()),
		                  root.getIntAttribute("offcolor b", C_Off.getBlue()));
		
		C_Background = new Color(root.getIntAttribute("backcolor r", C_Background.getRed()),
		                         root.getIntAttribute("backcolor g", C_Background.getGreen()),
		                         root.getIntAttribute("backcolor b", C_Background.getBlue()));
		
		
		windowPosition = new Point(root.getIntAttribute("position x"), root.getIntAttribute("position y"));
	}
	
	private void saveOptions() throws IOException {
		Tag root = new Tag("powerstrip");
		root.setAttribute("version", 1.0f);
		
		Tag horiz = new Tag("horizontal");
		horiz.addContent("" + horizontal);
		root.addContent(horiz);
		
		Tag addr = new Tag("remoteaddr");
		addr.addContent(remoteAddress);
		root.addContent(addr);
		
		Tag protocol = new Tag("udp");
		protocol.addContent("" + useUDP);
		root.addContent(protocol);
		
		Tag count = new Tag("outputcount");
		count.addContent("" + stateCount);
		root.addContent(count);
		
		Tag oncolor = new Tag("oncolor");
		oncolor.setAttribute("r", C_On.getRed());
		oncolor.setAttribute("g", C_On.getGreen());
		oncolor.setAttribute("b", C_On.getBlue());
		root.addContent(oncolor);
		
		Tag offcolor = new Tag("offcolor");
		offcolor.setAttribute("r", C_Off.getRed());
		offcolor.setAttribute("g", C_Off.getGreen());
		offcolor.setAttribute("b", C_Off.getBlue());
		root.addContent(offcolor);
		
		Tag backcolor = new Tag("backcolor");
		backcolor.setAttribute("r", C_Background.getRed());
		backcolor.setAttribute("g", C_Background.getGreen());
		backcolor.setAttribute("b", C_Background.getBlue());
		root.addContent(backcolor);
		
		Tag position = new Tag("position");
		position.setAttribute("x", windowPosition.x);
		position.setAttribute("y", windowPosition.y);
		root.addContent(position);
		
		FileOutputStream out = new FileOutputStream(SETTINGS_FILE);
		root.toStream(out, true);
		out.close();
	}
	
	// Class ColorCanvas
	private class ColorCanvas extends Canvas implements MouseListener {
		private Color color;
		
		public ColorCanvas(Color color) {
			this.color = color;
			setBackground(color);
			setPreferredSize(new Dimension(15, 10));
			
			addMouseListener(this);
		}
		
		@Override
        public void paint(Graphics g) {
			// The update procedure will have already filled this component with 'color'
			// Just draw an outline around it
			g.setColor(Color.BLACK);
			g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
		}
		
		public Color getColor() {
			return color;
		}
		
		public void mouseClicked(MouseEvent e) {
			Color newColor = JColorChooser.showDialog(PowerStrip.this, "Choose a Color", color);
			
			if (newColor != null) {
				color = newColor;
				setBackground(color);
				repaint();
			}
        }
		
		public void mouseEntered(MouseEvent e) { }
		public void mouseExited(MouseEvent e) { }
		public void mousePressed(MouseEvent e) { }
		public void mouseReleased(MouseEvent e) { }
	}
	
	// Class OptionsDialog
	private class OptionsDialog extends Frame implements ActionListener {
		private CheckboxGroup grpProtocol;
		private Checkbox rdoTCP,
		                 rdoUDP;
		
		private CheckboxGroup grpOrientation;
		private Checkbox rdoHorizontal,
		                 rdoVertical;
		private TextField txtRemoteAddress,
		                  txtOutputCount;
		
		private ColorCanvas clrOn,
		                    clrOff,
		                    clrBackground;
		
		private Button btnOK,
		               btnCancel;
		
		public OptionsDialog() {
			super("Power Strip v" + PROG_VERSION + " Options");
			
			setLayout(new GridLayout(0, 2));
			
			Label l;
			
			grpOrientation = new CheckboxGroup();
			rdoHorizontal = new Checkbox("Horizontal", horizontal, grpOrientation);
			rdoVertical = new Checkbox("Vertical", !horizontal, grpOrientation);
			add(rdoHorizontal);
			add(rdoVertical);
			
			l = new Label("Remote Address");
			txtRemoteAddress = new TextField(remoteAddress, 15);
			add(l);
			add(txtRemoteAddress);
			
			grpProtocol = new CheckboxGroup();
			rdoTCP = new Checkbox("TCP", !useUDP, grpProtocol);
			rdoUDP = new Checkbox("UDP", useUDP, grpProtocol);
			add(rdoTCP);
			add(rdoUDP);
			
			l = new Label("Output Count");
			txtOutputCount = new TextField("" + stateCount, 4);
			add(l);
			add(txtOutputCount);
			
			l = new Label("LED On Color");
			clrOn = new ColorCanvas(C_On);
			add(l);
			add(clrOn);
			
			l = new Label("LED Off Color");
			clrOff = new ColorCanvas(C_Off);
			add(l);
			add(clrOff);
			
			l = new Label("Background Color");
			clrBackground = new ColorCanvas(C_Background);
			add(l);
			add(clrBackground);
			
			btnOK = new Button("OK");
			btnCancel = new Button("Cancel");
			
			btnOK.addActionListener(this);
			btnCancel.addActionListener(this);
			
			add(btnOK);
			add(btnCancel);
			
			pack();
			setVisible(true);
			optionsVisible = true;
		}
		
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == btnOK) {
				try {
					int count = Integer.parseInt(txtOutputCount.getText());
					
					if (count < 1 || count > 16) {
						JOptionPane.showMessageDialog(this, "Output count must be between 1 and 16, inclusive", "Error", JOptionPane.WARNING_MESSAGE);
						return;
					}
				} catch (NumberFormatException nfe) {
					JOptionPane.showMessageDialog(this, "Output count is not an integer", "Error", JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				boolean needsRestart = false;
				
				boolean horiz = rdoHorizontal.getState();
				if (horizontal != horiz) {
					horizontal = horiz;
					needsRestart = true;
				}
				
				String remote = txtRemoteAddress.getText();
				if (!remoteAddress.equals(remote)) {
					remoteAddress = remote;
					needsRestart = true;
				}
				
				boolean udp = rdoUDP.getState();
				if (useUDP != udp) {
					useUDP = udp;
					needsRestart = true;
				}
				
				int outcount = Integer.parseInt(txtOutputCount.getText());
				if (stateCount != outcount) {
					stateCount = Integer.parseInt(txtOutputCount.getText());
					needsRestart = true;
				}
				
				/*
				Color cOn = clrOn.getColor();
				if (!cOn.equals(C_On)) {
					C_On = cOn;
					needsRestart = true;
				}
				
				Color cOff = clrOff.getColor();
				if (!cOff.equals(C_Off)) {
					C_Off = cOff;
					needsRestart = true;
				}
				
				Color cBack = clrBackground.getColor();
				if (!cBack.equals(C_Background)) {
					C_Background = cBack;
					needsRestart = true;
				}
				*/
				
				C_On = clrOn.getColor();
				C_Off = clrOff.getColor();
				C_Background = clrBackground.getColor();
				
				windowPosition = PowerStrip.this.getLocation();
				
				// In case some colors changed
				PowerStrip.this.setVisible(false);
				PowerStrip.this.setVisible(true);
				
				setVisible(false);
				
				try {
					saveOptions();
					
					if (needsRestart) JOptionPane.showMessageDialog(PowerStrip.this, "You must restart the program for changes to take effect.", "Restart Required", JOptionPane.INFORMATION_MESSAGE);
				} catch (IOException ioe) {
					JOptionPane.showMessageDialog(PowerStrip.this, "Unable to save settings file: " + ioe.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
				}
				
				dispose();
			} else if (e.getSource() == btnCancel) {
				PowerStrip.this.setLocation(windowPosition);
				
				setVisible(false);
				dispose();
			} else {
				throw new IllegalArgumentException("Unexpected source of action event: " + e.getSource());
			}
			
			optionsVisible = false;
		}
	}
}

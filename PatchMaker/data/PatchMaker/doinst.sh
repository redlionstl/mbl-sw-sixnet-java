#!/bin/sh
log="/dev/null"
#log="/var/logs/update"

# Be careful editing the next few lines, they will be automatically modified by
#   the PatchMaker program without notice
# The 'OLDVERS' line will be modified whenever a new old version is added or one
#   is removed
# The 'NEWVERS' line will be modified whenever the New Version text field is
#   modified

# OLDVERS -- The line immediately after this will be set to 'oldvers="<versions>"'
oldvers=""

# NEWVERS -- The line immediately after this will be set to 'ver=<version>'
ver=

# This script is used to clean up the firmware whether or not the patch worked
cleanup()
{
  rm -fr /temp_arm /temp_ppc /temp_arm9 /var/switch/$ver
  rm -f /var/switch/delta*.tgz
}

#----------------------------------------
# This script processes /var/switch/delta$oldver-$ver.tgz, installing each
# file therein to its proper place under /.
#
# Therefore, the minimum (and, possibily, maximum) contents for a switch
# update including this script is:
#
#   var/switch/delta$oldver-$ver.tgz
#   install/do_inst.sh (this file)
#

#----------------------------------------
# Make sure we're installing on the right version
ov=`cat /etc/sxbuildinfo.txt | tr -s "\t" " " | cut -d " " -f 2 | tr -s "." "_"`
echo "ov:$ov" >>$log

goodVer=0
for oldver in $oldvers ; do
  echo "testing $oldver" >>$log
  if [ "$ov" = "$oldver" ]; then
    echo "Found" >>$log
    goodVer=1
    break
  fi
done

if [ "$goodVer" != "1" ]; then
  echo "No acceptable old version found. Quitting." >>$log
  cleanup
  exit 1
fi

#----------------------------------------
# Ignore patches, etc.
#oldver=`echo $oldver | cut -d "_" -f 1-2`

#----------------------------------------
# Make a directory to work in
workDir=/var/switch/$ver
mkdir -p $workDir
cd $workDir
echo "Working in $workDir" >>$log

#----------------------------------------
# The tar file is in the update as var/switch/delta...
deltaFile=/var/switch/delta$oldver-$ver.tgz
gunzip -c $deltaFile 2>/dev/null | tar xf - >/dev/null 2>&1

#----------------------------------------
# Promote any platform-specific files to their platform-neutral place

if [ -d temp_arm -o -d temp_ppc -o -d temp_arm9 ]; then
  # identify the CPU type and install the proper directory
  cpu=`/usr/local/bin/cpu.pl`
  case $cpu in
    ARM720T)
      (cd temp_arm; tar cf - *) | tar xf -
      ;;
    ARM926EJ-S)
      (cd temp_arm9; tar cf - *) | tar xf -
      ;;
    8xx)
      (cd temp_ppc; tar cf - *) | tar xf -
      ;;
    *)
      cleanup
      exit 4
      ;;
  esac

  # Remove the temp_* directories so they don't get installed
  rm -rf temp_arm temp_arm9 temp_ppc
fi

#----------------------------------------
# Process the files
for f in `find .`; do
  if [ -d $f ]; then
    # Create directories as necessary
    if [ ! -d /$f ] ; then
      echo "Creating directory /$f" >>$log
      mkdir -p /$f
    fi
  else
    # Copy anything else
    echo "Installing file /$f" >>$log
    rm -f /$f
    cp -p $f /$f
  fi
done

#----------------------------------------
# Clean up
cleanup

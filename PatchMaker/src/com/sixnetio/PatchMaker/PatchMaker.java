/*
 * PatchMaker.java
 *
 * Assists in the creation of firmware patches
 *
 * Jonathan Pearson
 * July 29, 2008
 *
 */

// Note: The import routines require lots of memory, so this program should be executed from a
//   shortcut or script that sets -Xmx512M or more.

package com.sixnetio.PatchMaker;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.DefaultTreeModel;

import com.sixnetio.GUI.ListChooserDialog;
import com.sixnetio.GUI.Suicide;
import com.sixnetio.fs.generic.FSObject;
import com.sixnetio.fs.tar.*;
import com.sixnetio.util.Utils;

public class PatchMaker extends JFrame {
	public static final String PROG_VERSION = "1.1.2";
	
	// These are for looking up switch user/group IDs given names
	// Read-only after static initialization
	private static final Map<String, Integer> uidMap, gidMap;
	
	// Keep a couple of these around for directory consistency
	private static JFileChooser patchChooser;
	private static JFileChooser fileChooser;
	
	public static final TarballFSObjectFactory factory = new TarballFSObjectFactory();
	
	static {
		// Set up the maps
		uidMap = new Hashtable<String, Integer>();
		gidMap = new Hashtable<String, Integer>();
		
		uidMap.put("root", 0);
		uidMap.put("bin", 1);
		uidMap.put("ftp", 0); // Yes, apparently ftp has the same UID as root
		uidMap.put("nobody", 99);
		uidMap.put("admin", 101);
		uidMap.put("cli", 102);
		uidMap.put("sertest", 99);
		uidMap.put("guest", 500);
		uidMap.put("PPPLink", 601);
		
		gidMap.put("root", 0);
		gidMap.put("bin", 1);
		gidMap.put("daemon", 2);
		gidMap.put("sys", 3);
		gidMap.put("adm", 4);
		gidMap.put("tty", 5);
		gidMap.put("disk", 6);
		gidMap.put("lp", 7);
		gidMap.put("mem", 8);
		gidMap.put("kmem", 9);
		gidMap.put("wheel", 10);
		gidMap.put("pppusers", 44);
		gidMap.put("ftp", 50);
		gidMap.put("modem", 90);
		gidMap.put("nobody", 99);
		gidMap.put("users", 100);
		gidMap.put("admin", 101);
		gidMap.put("guest", 500);
		gidMap.put("PPPLink", 601);
		
		// Set up the file choosers
		patchChooser = new JFileChooser(Utils.CUR_DIR);
		patchChooser.setAcceptAllFileFilterUsed(false);
		patchChooser.setDialogTitle("Firmware Patches");
		patchChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
	            if (f.isDirectory()) return true;
	            
	            return (f.isFile() && f.canRead() && f.getName().toLowerCase().endsWith(".tgz"));
            }
            
            @Override
            public String getDescription() {
	            return "Firmware Patches (*.tgz)";
            }
		});
		
		fileChooser = new JFileChooser(Utils.CUR_DIR);
		fileChooser.setAcceptAllFileFilterUsed(true);
		fileChooser.setDialogTitle("Files");
	}
	
	public static void main(String[] args) {
		new PatchMaker();
	}
	
	private JTabbedPane tabMain;
	private JTextField txtNewVersion;
	private JTextArea txtInstallScript;
	private VersionPanel allVersions;
	
	public PatchMaker() {
		this(null);
	}
	
	public PatchMaker(InputStream patchData) {
		super("PatchMaker v." + PROG_VERSION);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent e) {
				setVisible(false);
				dispose();
				
				new Suicide();
			}
		});
		
		// Tab interface
		// Each tab represents a previous version and how to get from there to the patched version
		// Extra controls allow you to add files to all tabs at once
		setLayout(new BorderLayout());
		
		// Top: version that the patch generates
		{
			JPanel p = new JPanel();
			p.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(p, BorderLayout.NORTH);
			
			p.add(new JLabel("New Version:"));
			
			txtNewVersion = new JTextField(12);
			txtNewVersion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					txtNewVersion.setText(txtNewVersion.getText().replaceAll("_", "."));
					setNewVersion(txtNewVersion.getText());
                }
			});
			txtNewVersion.addFocusListener(new FocusAdapter() {
				@Override
                public void focusLost(FocusEvent e) {
					txtNewVersion.setText(txtNewVersion.getText().replaceAll("_", "."));
					setNewVersion(txtNewVersion.getText());
                }
			});
			p.add(txtNewVersion);
		}
		
		// Center: The tabs, one for each old version, one representing all versions, and one for the install script
		{
			tabMain = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
			add(tabMain, BorderLayout.CENTER);
			
			// First tab is the install script
			txtInstallScript = new JTextArea(40, 100);
			txtInstallScript.setFont(new Font("Courier New", Font.PLAIN, 12));
			txtInstallScript.setTabSize(8);
			txtInstallScript.setText(getStandardScript());
			
			tabMain.insertTab("Install", null, new JScrollPane(txtInstallScript), "The script that installs the proper files onto the switch", tabMain.getTabCount());
			
			// Second tab represents all old versions (so you can add files to all of them at once)
			allVersions = new VersionPanel("All Versions");
			tabMain.insertTab("All Versions", null, allVersions, "Add files to all upgradable versions at the same time", tabMain.getTabCount());
			tabMain.setSelectedIndex(1); // Select the 'All Versions' view
		}
		
		// Menus
		{
			JMenuBar bar = new JMenuBar();
			setJMenuBar(bar);
			
			// File menu
			{
				JMenu mnuFile = new JMenu("File");
				mnuFile.setMnemonic('F');
				bar.add(mnuFile);
				
				JMenuItem mnuNew = new JMenuItem("New");
				mnuNew.setMnemonic('N');
				mnuNew.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
	                    new PatchMaker();
	                    
	                    PatchMaker.this.setVisible(false);
	                    PatchMaker.this.dispose();
                    }
				});
				mnuFile.add(mnuNew);
				
				mnuFile.addSeparator();
				
				JMenuItem mnuOpen = new JMenuItem("Open...");
				mnuOpen.setMnemonic('O');
				mnuOpen.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
	                    int result = patchChooser.showOpenDialog(PatchMaker.this);
	                    
	                    if (result == JFileChooser.APPROVE_OPTION) {
	                    	try {
	                    		new PatchMaker(new FileInputStream(patchChooser.getSelectedFile()));
	                    		
	                    		PatchMaker.this.setVisible(false);
		                    	PatchMaker.this.dispose();
	                    	} catch (IOException ioe) {
	                    		JOptionPane.showMessageDialog(PatchMaker.this, "Unable to open file: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	                    	}
	                    }
                    }
				});
				mnuFile.add(mnuOpen);
				
				JMenuItem mnuSave = new JMenuItem("Save...");
				mnuSave.setMnemonic('S');
				mnuSave.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
	                    int result = patchChooser.showSaveDialog(PatchMaker.this);
	                    
	                    if (result == JFileChooser.APPROVE_OPTION) {
	                    	exportPatch(patchChooser.getSelectedFile());
	                    }
                    }
				});
				mnuFile.add(mnuSave);
				
				mnuFile.addSeparator();
				
				JMenuItem mnuImport = new JMenuItem("Import...");
				mnuImport.setMnemonic('I');
				mnuImport.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String[] choices = {
							"From Tarballs",
							"From Firmware Images",
							"From Firmware Bundles"
						};
						
						ListChooserDialog dlg = new ListChooserDialog(Arrays.asList(choices), "Choose Import Method");
						
						int result = dlg.showDialog();
						
						if (result == ListChooserDialog.OKAY_OPTION) {
							Object choice = dlg.getChoice();
							
							PatchGeneratorGUI generator = null;
							if (choice == choices[0]) {
								generator = new PatchFromTarballGenerator();
							} else if (choice == choices[1]) {
								generator = new PatchFromImageGenerator();
							} else if (choice == choices[2]) {
								generator = new PatchFromBundleGenerator();
							} else {
								throw new RuntimeException("Unexpected import choice: " + choice.toString());
							}
							
							result = generator.showDialog();
							
							if (result == PatchGeneratorGUI.OKAY_OPTION) {
    							byte[] data = generator.getPatchData();
    							
    							if (data != null) {
        							new PatchMaker(new ByteArrayInputStream(data));
        							
        							setVisible(false);
        							dispose();
    							}
							}
						}
                    }
				});
				mnuFile.add(mnuImport);
				
				mnuFile.addSeparator();
				
				JMenuItem mnuExit = new JMenuItem("Exit");
				mnuExit.setMnemonic('x');
				mnuExit.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						PatchMaker.this.setVisible(false);
						PatchMaker.this.dispose();
						
						new Suicide();
					}
				});
				mnuFile.add(mnuExit);
			}
			
			// Patch menu
			{
				JMenu mnuPatch = new JMenu("Patch");
				mnuPatch.setMnemonic('P');
				bar.add(mnuPatch);
				
				JMenuItem mnuAddUgradableVersion = new JMenuItem("Add Upgradable Version...");
				mnuAddUgradableVersion.setMnemonic('U');
				mnuAddUgradableVersion.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
	                    String newVersion = JOptionPane.showInputDialog(PatchMaker.this, "Enter a version number", "New Upgradable Version", JOptionPane.QUESTION_MESSAGE);
	                    
	                    if (newVersion != null) {
	                    	createVersionTab(newVersion);
	                    }
                    }
				});
				mnuPatch.add(mnuAddUgradableVersion);
				
				JMenuItem mnuRemoveUpgradableVersion = new JMenuItem("Remove Upgradable Version");
				mnuRemoveUpgradableVersion.setMnemonic('R');
				mnuRemoveUpgradableVersion.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
	                    removeVersionTab((VersionPanel)tabMain.getSelectedComponent());
                    }
				});
				mnuPatch.add(mnuRemoveUpgradableVersion);
				
				mnuPatch.addSeparator();
				
				final JMenuItem mnuAddFile = new JMenuItem("Add File Here");
				mnuAddFile.setMnemonic('F');
				mnuAddFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_DOWN_MASK));
				mnuAddFile.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int result = fileChooser.showOpenDialog(PatchMaker.this);
						
						if (result == JFileChooser.APPROVE_OPTION) {
							addFile(fileChooser.getSelectedFile());
						}
                    }
				});
				mnuPatch.add(mnuAddFile);
				
				final JMenuItem mnuAddDirectory = new JMenuItem("Add Directory Here");
				mnuAddDirectory.setMnemonic('D');
				mnuAddDirectory.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK));
				mnuAddDirectory.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String name = JOptionPane.showInputDialog(PatchMaker.this, "Enter a new directory name", "New Directory", JOptionPane.QUESTION_MESSAGE);
						
						if (name == null) return;
						
						addDirectory(name);
                    }
				});
				mnuPatch.add(mnuAddDirectory);
				
				final JMenuItem mnuAddLink = new JMenuItem("Add Symlink Here");
				mnuAddLink.setMnemonic('L');
				mnuAddLink.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_DOWN_MASK));
				mnuAddLink.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String name = JOptionPane.showInputDialog(PatchMaker.this, "Enter the name of the link", "New Link -- Name", JOptionPane.QUESTION_MESSAGE);
						if (name == null) return;
						
						String target = JOptionPane.showInputDialog(PatchMaker.this, "Enter the target for the link", "New Link -- Target", JOptionPane.QUESTION_MESSAGE);
						if (target == null) return;
						
						addLink(name, target);
                    }
				});
				mnuPatch.add(mnuAddLink);
				
				mnuPatch.addSeparator();
				
				final JMenuItem mnuDeleteObject = new JMenuItem("Kill Object");
				mnuDeleteObject.setMnemonic('K');
				mnuDeleteObject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
				mnuDeleteObject.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						deleteCurrentObject();
                    }
				});
				mnuPatch.add(mnuDeleteObject);
				
				// When the tab pane is on the Script, disable the file/dir/link add items
				tabMain.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						if (tabMain.getSelectedIndex() == 0) {
							mnuAddFile.setEnabled(false);
							mnuAddDirectory.setEnabled(false);
							mnuAddLink.setEnabled(false);
							mnuDeleteObject.setEnabled(false);
						} else {
							mnuAddFile.setEnabled(true);
							mnuAddDirectory.setEnabled(true);
							mnuAddLink.setEnabled(true);
							mnuDeleteObject.setEnabled(true);
						}
                    }
				});
			}
		}
		
		if (patchData != null) {
			if (!openPatch(patchData)) {
				new PatchMaker();
				
				return;
			}
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Thread.currentThread().setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					public void uncaughtException(Thread t, Throwable e) {
						Utils.debug(e);
						
						String message = e.getClass().getName() + ": " + e.getMessage();
						if (e instanceof OutOfMemoryError) {
							message += "\nTry running java with a larger maximum heap size using the -Xmx option.";
							message += "\nFor example: 'java -Xmx1G -jar PatchMaker.jar' for 1 gigabyte of heap space.";
						}
						
						JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
                    }
				});
			}
		});
		
		pack();
		setVisible(true);
	}

	protected void createVersionTab(String oldVersion) {
		// Make sure it has the proper format
		oldVersion = oldVersion.replaceAll("_", ".");
		
		tabMain.insertTab(oldVersion, null, new VersionPanel(oldVersion), "Add files to allow upgrading of version " + oldVersion, tabMain.getTabCount());
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				refreshOldVersions();
			}
		});
    }
	
	protected void removeVersionTab(VersionPanel selectedComponent) {
	    tabMain.remove(selectedComponent);
	    
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				refreshOldVersions();
			}
		});
    }
	
	protected void exportPatch(File selectedFile) {
		// Check on the 'New Version' field
		// If it is empty, ask the user what to do
		// Choices:
		//   - Build a patch that will not change the version number
		//   - Enter a version number now
		//   - Cancel
		
		if (txtNewVersion.getText().length() == 0) {
			String[] options = {
				"Build w/o Version Change",
				"Enter Version Now",
				"Cancel"
			};
			
			int result = JOptionPane.showOptionDialog(this, "You have not entered a target version number, what would you like to do?", "No Target Version", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
			
			if (result == JOptionPane.CLOSED_OPTION || result == 2) {
				// Canceled
				return;
			} else if (result == 0) {
				// Do nothing, user decided to build without setting the version number
			} else {
				String newVersion = JOptionPane.showInputDialog(this, "What should the target version number be?", "New Version Number", JOptionPane.QUESTION_MESSAGE);
				
				if (newVersion == null) {
					// Canceled
					return;
				} else {
					txtNewVersion.setText(newVersion.replaceAll("_", "."));
					setNewVersion(newVersion);
				}
			}
		}
		
		GZippedTarball patch = new GZippedTarball();
		TDirectory root = patch.getRoot(); // Shortcut
		makeInstallFile(factory.createDirectory("install", root));
		
		// Build the sxbuildinfo.txt file that tells the switch what firmware version it has
		// Don't worry about txtNewVersion being used here; it is checked later
		// If it is empty, this file will not be included so we won't screw up the version
		StringBuilder buildInfoData = new StringBuilder();
		buildInfoData.append("ET-9MS\t");
		buildInfoData.append(txtNewVersion.getText().replaceAll("_", "."));
		buildInfoData.append("\t\t");
		
		Calendar cal = Calendar.getInstance();
		buildInfoData.append(String.format("%d-%02d-%02d ",
		                                   cal.get(Calendar.YEAR),
		                                   cal.get(Calendar.MONTH) + 1,
		                                   cal.get(Calendar.DAY_OF_MONTH)));
		
		int hour = cal.get(Calendar.HOUR);
		if (hour == 0) hour = 12;
		buildInfoData.append(String.format("%02d:%02d:%02d.%04d\t",
		                     hour,
		                     cal.get(Calendar.MINUTE),
		                     cal.get(Calendar.SECOND),
		                     cal.get(Calendar.MILLISECOND) * 10));
		
		// The MD5 sum is not used (good thing, too, since we don't have the data necessary to generate it)
		// So just use 0s
		buildInfoData.append("00000000000000000000000000000000");
		
		TDirectory var_switch =
			factory.createDirectory("switch",
			                        factory.createDirectory("var", root));
		
		// Collect warning messages to display at the end
		final StringBuilder warnings = new StringBuilder();
		
		try {
			FileOutputStream out = new FileOutputStream(selectedFile);
			
			try {
				// Loop through all old-version tabs, tarring up their files
				for (int tabIndex = 2; tabIndex < tabMain.getTabCount(); tabIndex++) {
					VersionPanel vp = (VersionPanel)tabMain.getComponentAt(tabIndex);
					GZippedTarball tb = new GZippedTarball();
					
					// Throw in the buildinfo file if it is needed
					if (txtNewVersion.getText().length() > 0) {
						factory.createFile("sxbuildinfo.txt",
						                   buildInfoData.toString().getBytes(),
						                   factory.createDirectory("etc", tb.getRoot()));
					}
					
					// NOTE: We need to put the all/all files in each delta, otherwise they will be applied
					//   even if the patch fails for some reason (version doesn't match)
					// The all/arch files can be in the main tarball because they won't make any difference
					//   if they are applied (other than taking up space), as long as the install script
					//   remembers to remove those directories in all cases
					tarUp(tb, allVersions.all, warnings);
					
					// Add all of the files/directories
					tarUp(tb, vp.all, warnings);
					tarUp(tb, vp.arm7, warnings);
					tarUp(tb, vp.arm9, warnings);
					tarUp(tb, vp.ppc, warnings);
					
					// Create a TFile for the new tarball
					factory.createFile(String.format("delta%s-%s.tgz",
					                                 vp.toString().replaceAll("\\.", "_"),
					                                 txtNewVersion.getText().replaceAll("\\.", "_")),
					                   tb.getData(),
					                   var_switch);
				}
				
				// By adding the real files AFTER the standard ones, they are
				//   able to mask the standard files, allowing anything that
				//   was changed by the user to show through
				// See the note above about why all/all is not here
				// The files from the All Versions tab
				tarUp(patch, allVersions.arm7, warnings);
				tarUp(patch, allVersions.arm9, warnings);
				tarUp(patch, allVersions.ppc, warnings);
				
				// Finally, dump this onto out
				patch.toStream(out);
			} finally {
				out.close();
			}
		} catch (IOException ioe) {
			Utils.debug(ioe);
			JOptionPane.showMessageDialog(this, "Unable to export file: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		if (warnings.length() > 0) {
			// FUTURE: Put this in a separate window so the user can look at
			//   it and correct the errors at the same time
			JOptionPane.showMessageDialog(PatchMaker.this, "Warnings:\n" + warnings.toString(), "Warnings", JOptionPane.WARNING_MESSAGE);
		}
		
		repaint(); // In case any users/groups were changed
    }
	
	protected void makeInstallFile(TDirectory parent)
	{
		TFile f = factory.createFile("doinst.sh",
		                             txtInstallScript.getText().getBytes(),
		                             parent);
		f.setMode(0775);
	}
	
	protected static void tarUp(Tarball tb, DirectoryTreeNode node, StringBuilder warnings) {
		// Make sure the UID/GID of node match the name/group
		rectifyIDs(node.directory, warnings);
		
		// Make a copy of the file system from this node
		TDirectory copy = factory.convertFilesystem(node.directory);
		
		// Move the children of that copy into the tarball (we do not want to
		// preserve the root directory)
		for (FSObject child : copy) {
			child.setParent(tb.getRoot());
		}
		
		// Remove empty directories
		removeEmptyDirs(tb.getRoot());
	}
	
	/**
	 * Remove the empty directories from a tree.
	 */
	protected static void removeEmptyDirs(TDirectory dir)
	{
		if (dir.getChildCount() == 0) {
			dir.setParent(null);
		}
		else {
			for (FSObject obj : dir) {
				if (obj instanceof TDirectory) {
					removeEmptyDirs((TDirectory)obj);
				}
			}
		}
	}
	
	protected void addFile(File file) {
		// Add the given file to the active view
		Component activeView = tabMain.getSelectedComponent();
		
		if (!(activeView instanceof VersionPanel)) {
			getToolkit().beep();
			return;
		}
		
		VersionPanel vp = (VersionPanel)activeView;
		
		try {
			vp.addFile(file);
		} catch (IOException ioe) {
			Utils.debug(ioe);
			JOptionPane.showMessageDialog(this, "Error reading new file: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	protected void addDirectory(String name) {
		while (name.startsWith("/")) name = name.substring(1);
		while (name.endsWith("/")) name = name.substring(0, name.length() - 1);
		
		// Add the given file to the active view
		Component activeView = tabMain.getSelectedComponent();
		
		if (!(activeView instanceof VersionPanel)) {
			getToolkit().beep();
			return;
		}
		
		VersionPanel vp = (VersionPanel)activeView;
		vp.addDirectory(name);
	}
	
	protected void addLink(String name, String target) {
		// Add the given link to the active view
		Component activeView = tabMain.getSelectedComponent();
		
		if (!(activeView instanceof VersionPanel)) {
			getToolkit().beep();
			return;
		}
		
		VersionPanel vp = (VersionPanel)activeView;
		
		vp.addLink(name, target);
	}
	
	protected void deleteCurrentObject() {
		// Delete the active object in the current view
		Component activeView = tabMain.getSelectedComponent();
		
		if (!(activeView instanceof VersionPanel)) {
			getToolkit().beep();
			return;
		}
		
		VersionPanel vp = (VersionPanel)activeView;
		
		vp.deleteCurrentObject();
	}
	
	protected static String getStandardScript() {
	    try {
	    	InputStream in = PatchMaker.class.getClassLoader().getResourceAsStream("data/PatchMaker/doinst.sh");
	    	byte[] data = Utils.bytesToEOF(in);
	    	in.close();
	    	
	    	return new String(data);
	    } catch (IOException ioe) {
	    	return "Unable to load standard script";
	    }
    }
	
	private void setNewVersion(String newVersion) {
		// Make sure newVersion is in the proper format
		newVersion = newVersion.replaceAll("\\.", "_");
		
		// Get the lines of the install script
		String[] scriptLines = Utils.getLines(txtInstallScript.getText());
		
		setNewVers(scriptLines, newVersion);
		
		// Write back to the text area
		txtInstallScript.setText(Utils.combineLines(scriptLines));
		txtInstallScript.setCaretPosition(0);
	}
	
	private void refreshOldVersions() {
		// Build up the string of old versions
		StringBuilder oldVersions = new StringBuilder();
		
		// Skip the install script and the 'all versions' tabs
		for (int i = 2; i < tabMain.getTabCount(); i++) {
			if (oldVersions.length() > 0) oldVersions.append(" ");
			
			oldVersions.append(tabMain.getComponentAt(i).toString().replaceAll("\\.", "_"));
		}
		
		// Get the lines of the install script
		String[] scriptLines = Utils.getLines(txtInstallScript.getText());
		
		setOldVers(scriptLines, oldVersions.toString());
		
		// Write back to the text area
		txtInstallScript.setText(Utils.combineLines(scriptLines));
		txtInstallScript.setCaretPosition(0);
	}
	
	// Returns true if user/groups needed to be changed because they do not exist in the switch
	protected static void rectifyIDs(TarObject obj, StringBuilder warnings) {
		short uid, gid;
		
		if (uidMap.containsKey(obj.getTarAttributes().getUserName())) {
			uid = uidMap.get(obj.getTarAttributes().getUserName()).shortValue();
		} else {
			warnings.append(String.format("%s user changed from '%s' to 'root'\n",
			                              ((FSObject)obj).getPath(),
			                              obj.getTarAttributes().getUserName()));
			
			uid = 0;
			obj.getTarAttributes().setUserName("root");
		}
		
		if (gidMap.containsKey(obj.getTarAttributes().getGroupName())) {
			gid = gidMap.get(obj.getTarAttributes().getGroupName()).shortValue();
		} else {
			warnings.append(String.format("%s group changed from '%s' to 'root'\n",
			                              ((FSObject)obj).getPath(),
			                              obj.getTarAttributes().getGroupName()));
			
			gid = 0;
			obj.getTarAttributes().setGroupName("root");
		}
		
		((FSObject)obj).setUID(uid);
		((FSObject)obj).setGID(gid);
	}
	
	// Returns true if the patch is opened successfully, false otherwise
	private boolean openPatch(InputStream patchData) {
		// Open the patchFile as a gzipped tarball
		Tarball patchTar = null;
		
		try {
			patchTar = new GZippedTarball(patchData);
		} catch (IOException ioe) {
			Utils.debug(ioe);
			JOptionPane.showMessageDialog(null, "Unable to open the patch file: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// Set the install script to "", since if we don't find one, we want it to be empty
		txtInstallScript.setText("");
		
		// Create the directory structure for All Versions
		createDirStructure(allVersions, patchTar);
		
		// Add the files, looking for a few special ones
		// Special files: etc/sxbuildinfo.txt, install/doinst.sh,
		//   var/switch/delta*.{tar.gz|tgz|tar}
		// Everything else is stuck wherever it fits
		Map<String, Tarball> patchTars = new Hashtable<String, Tarball>();
		
		for (FSObject f : allVersions.all.directory) {
			if (f.getPath().equals("install/doinst.sh")) {
				// Install script
				txtInstallScript.setText(new String(((TFile)f).getData()));
				
				// Parse that to determine the 'from' version
				String[] lines = Utils.getLines(txtInstallScript.getText());
				String newvers = null;
				
				for (int i = 0; i < lines.length; i++) {
					if (lines[i].matches("ver=.*")) {
						newvers = lines[i].substring(lines[i].indexOf('=') + 1);
						break;
					}
				}
				
				if (newvers == null) {
					JOptionPane.showMessageDialog(null, "Unable to find the target version for this patch; you will need to enter it manually.", "Unable to Parse", JOptionPane.WARNING_MESSAGE);
				} else {
					txtNewVersion.setText(newvers.replaceAll("_", "."));
				}
				
				// Remove this from the tree
				f.setParent(null);
			} else if (f.getPath().equals("etc/sxbuildinfo.txt")) {
				// Remove this from the tree
				f.setParent(null);
			} else if (f.getPath().startsWith("var/switch/delta") &&
			           (f.getName().endsWith(".tgz") ||
			            f.getName().endsWith(".tar.gz") ||
			            f.getName().endsWith(".tar"))) {
				
				// Almost definitely a delta tarball
				Tarball deltaBall;
				try {
					String lowerName = f.getName().toLowerCase();
					if (lowerName.endsWith(".tar.gz") || lowerName.endsWith(".tgz")) {
						deltaBall = new GZippedTarball(((TFile)f).getData());
					}
					else {
						deltaBall = new Tarball(((TFile)f).getData());
					}
					
					patchTars.put(f.getName(), deltaBall);
					
					// Remove this from the tree
					f.setParent(null);
				}
				catch (IOException ioe) {
					// But if it won't open, treat it as a normal file
					// It should already be in the tree
				}
			} else {
				// Just a random file, leave it in the tree
			}
		}
		
		// Loop through each delta ball, adding a new VersionPanel for it
		for (String tarballName : patchTars.keySet()) {
			// Extract the version number from the tarball name
			// Should be in the format "delta<oldvers>-<newvers>.tgz" or similar
			// BUT if it's not in that format... ask the user
			String oldvers;
			if (tarballName.matches(".*/delta[0-9]+_[0-9]+_[0-9]+[a-z]?-[0-9]+_[0-9]+_[0-9]+[a-z]?\\..*")) {
    			oldvers = tarballName.substring(tarballName.indexOf("delta") + "delta".length());
    			oldvers = oldvers.substring(0, oldvers.indexOf('-'));
    			oldvers = oldvers.replaceAll("_", ".");
			} else {
				oldvers = JOptionPane.showInputDialog(null, "Unable to determine version being upgraded from this file, please enter it: " + tarballName, "Unable to Parse", JOptionPane.WARNING_MESSAGE);
				if (oldvers == null) return false;
				
				oldvers.replaceAll("_", ".");
			}
			
			VersionPanel vp = new VersionPanel(oldvers);
			tabMain.insertTab(oldvers, null, vp, "Add files to allow upgrading of version " + oldvers, tabMain.getTabCount());
			
			Tarball deltaBall = patchTars.get(tarballName);
			
			// Separate into files/directories
			createDirStructure(vp, deltaBall);
		}
		
		return true;
	}
	
	/**
	 * Build the directory/file structure for the given version panel.
	 * 
	 * @param vp The version panel that will display the directory/file
	 *   structure.
	 * @param tb The tarball containing the directory/file structure.
	 */
	private void createDirStructure(VersionPanel vp, Tarball tb)
	{
		for (FSObject obj : tb.getRoot()) {
			if (obj.getName().equals("temp_arm")) {
				// arm7-specific files
				createDirStructure(vp, vp.arm7, obj, false);
			}
			else if (obj.getName().equals("temp_arm9")) {
				// arm9-specific files
				createDirStructure(vp, vp.arm9, obj, false);
			}
			else if (obj.getName().equals("temp_ppc")) {
				// ppc-specific files
				createDirStructure(vp, vp.ppc, obj, false);
			}
			else {
				// These fall under the 'all' category
				createDirStructure(vp, vp.all, obj, true);
			}
		}
	}
	
	/**
	 * Create the directory/file structure for the given version panel, starting
	 * at the specified point.
	 * 
	 * @param vp The version panel that will display the directory/file
	 *   structure.
	 * @param parent The parent node for the provide FSObject.
	 * @param obj The FSObject to insert at this point in the tree.
	 * @param includeObj <tt>true</tt> to include <tt>obj</tt> as a child of
	 *   <tt>parent</tt>, <tt>false</tt> to set the children of <tt>obj</tt> as
	 *   immediate children of <tt>parent</tt> and not include <tt>obj</tt> in
	 *   the tree.
	 */
	private void createDirStructure(VersionPanel vp, DirectoryTreeNode parent,
	                                FSObject obj, boolean includeObj)
	{
		DirectoryTreeNode node = parent;
		
		if (obj instanceof TDirectory) {
			if (includeObj) {
				node = new DirectoryTreeNode(vp.dirTree, parent, obj.getName(),
				                             (TDirectory)obj);
				DefaultTreeModel model = (DefaultTreeModel)vp.dirTree.getModel();
				model.insertNodeInto(node, parent, parent.getChildCount());
			}
			
			for (FSObject child : (TDirectory)obj) {
				createDirStructure(vp, node, child, true);
			}
		}
		else {
			if (includeObj) {
				node.addFile(obj);
			}
		}
	}
	
	public static void setOldVers(String[] scriptLines, String oldvers) {
		// Search for the proper line
		for (int i = 0; i < scriptLines.length; i++) {
			if (scriptLines[i].startsWith("# OLDVERS")) {
				// Replace the next line with the proper value
				scriptLines[i + 1] = "oldvers=\"" + oldvers + "\"";
				break;
			}
		}
	}
	
	public static void setNewVers(String[] scriptLines, String newvers) {
		// Search for the proper line
		for (int i = 0; i < scriptLines.length; i++) {
			if (scriptLines[i].startsWith("# NEWVERS")) {
				// Replace the next line with the proper value
				scriptLines[i + 1] = "ver=" + newvers;
				break;
			}
		}
	}
}

/*
 * FileTable.java
 *
 * A table used for displaying File data from TarredFile objects.
 *
 * Jonathan Pearson
 * July 30, 2008
 *
 */

package com.sixnetio.PatchMaker;

import javax.swing.*;
import javax.swing.table.*;

public class FileTable extends JTable {
	public FileTable(DirectoryTreeNode dirNode) {
		super(new FileTableModel(dirNode), new DefaultTableColumnModel());
		
		DefaultTableColumnModel columnModel = (DefaultTableColumnModel)getColumnModel();
		for (int i = 0; i < FileTableModel.COLUMN_NAMES.length; i++) {
			TableColumn column = new TableColumn(i, FileTableModel.getDefaultColumnWidth(i));
			column.setHeaderValue(FileTableModel.COLUMN_NAMES[i]);
			columnModel.addColumn(column);
		}
		
		setShowGrid(false);
		setRowSorter(new TableRowSorter<FileTableModel>((FileTableModel)getModel()));
	}
}

/*
 * DirectoryTreeNode.java
 *
 * A node in a JTree that represents a directory with File contents.
 *
 * Jonathan Pearson
 * July 30, 2008
 *
 */

package com.sixnetio.PatchMaker;

import java.util.Iterator;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import com.sixnetio.fs.generic.FSObject;
import com.sixnetio.fs.tar.TDirectory;

public class DirectoryTreeNode extends DefaultMutableTreeNode
	implements Iterable<FSObject>
{
	public final TDirectory directory;
	public final FileTable table;
	private String displayName;
	private JTree tree;
	
	/**
	 * Construct a new node.
	 * 
	 * @param tree The tree to add the node to.
	 * @param parent The parent of this node.
	 * @param name The name of this node.
	 * @param directory The directory that represents this node in the patch
	 *   tarball. If <tt>null</tt>, a new one will be created.
	 */
	public DirectoryTreeNode(JTree tree, DirectoryTreeNode parent, String name,
	                         TDirectory directory)
	{
		super();
		setUserObject(new FileTable(this));
		
		this.tree = tree;
		
		table = (FileTable)getUserObject();
		
		displayName = name;
		
		if (directory == null) {
			directory = PatchMaker.factory.createDirectory(name, parent.directory);
			directory.updateModTime();
		}
		
		this.directory = directory;
	}
	
	// Use this for root nodes
	public DirectoryTreeNode(String displayName, String dirName)
	{
		super();
		setUserObject(new FileTable(this));
		
		table = (FileTable)getUserObject();
		
		this.displayName = displayName;
		
		this.directory = PatchMaker.factory.createDirectory(dirName, null);
	}
	
	public void addFile(FSObject obj) {
		FileTableModel model = (FileTableModel)table.getModel();
		model.addFile(obj);
		
		obj.updateModTime();
	}
	
	public void deleteFile(FSObject obj) {
		FileTableModel model = (FileTableModel)table.getModel();
		model.deleteFile(obj);
		
		obj.updateModTime();
	}
	
	public Iterator<FSObject> iterator() {
		FileTableModel model = (FileTableModel)table.getModel();
		
		return model.iterator();
	}
	
	public void rename(String newname) {
		directory.move(newname);
		
		// Rename this node
		displayName = directory.getName();
		
		((DefaultTreeModel)tree.getModel()).nodeChanged(this);
	}
	
	@Override
    public String toString() {
		return displayName;
	}
}

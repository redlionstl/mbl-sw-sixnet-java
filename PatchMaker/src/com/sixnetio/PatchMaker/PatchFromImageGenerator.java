/*
 * PatchFromImageGenerator.java
 *
 * Description
 *
 * Jonathan Pearson
 * Aug 1, 2008
 *
 */

package com.sixnetio.PatchMaker;

import java.io.*;
import java.util.*;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import com.sixnetio.fs.jffs2.JFFS2;
import com.sixnetio.fs.tar.*;
import com.sixnetio.util.Utils;


class PatchFromImageGenerator extends PatchGeneratorGUI {
	private static final JFileChooser IMAGE_CHOOSER;
	
	static {
		IMAGE_CHOOSER = new JFileChooser(Utils.CUR_DIR);
		IMAGE_CHOOSER.setAcceptAllFileFilterUsed(false);
		IMAGE_CHOOSER.setDialogTitle("Firmware Images");
		IMAGE_CHOOSER.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
	            if (f.isDirectory()) return true;
	            
	            return (f.isFile() && f.canRead() && f.getName().toLowerCase().startsWith("sximage"));
            }
            
            @Override
            public String getDescription() {
	            return "Firmware Images (sximage*)";
            }
		});
	}
	
	public PatchFromImageGenerator() {
		super("Choose Images");
	}
	
	@Override
    public JFileChooser getFileChooser() {
		return IMAGE_CHOOSER;
	}

	@Override
    public byte[] getPatchData() {
		// Open the new version
		JFFS2 newImage;
		try {
			newImage = new JFFS2(new FileInputStream(getNewFileName()));
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null, "Unable to generate patch; error reading file '" + getNewFileName() + "': " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
		Tarball newTar = new Tarball();
		newTar.setRoot(PatchMaker.factory.convertFilesystem(newImage.getRoot()));
		
		// Loop through each old version, detecting what would be necessary to make it into the new version
		Map<String, DeltaDeletePair> deltas = new Hashtable<String, DeltaDeletePair>(getOldFileNames().size());
		for (String oldFile : getOldFileNames()) {
			JFFS2 oldImage;
			try {
				oldImage = new JFFS2(new FileInputStream(oldFile));
			} catch (IOException ioe) {
				JOptionPane.showMessageDialog(null, "Unable to generate patch; error reading file '" + oldFile + "': " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
			
			Tarball oldTar = new Tarball();
			oldTar.setRoot(PatchMaker.factory.convertFilesystem(oldImage.getRoot()));
			
			// This will receive the names of files that only exist in the old version
			Set<String> removedFiles = new HashSet<String>();
			GZippedTarball deltaTar = computeDifferences(oldTar, newTar, removedFiles);
			
			// Figure out the version number
			String oldvers = findVersionNumber(oldFile, ".*([0-9]+_[0-9]+_[0-9]+[^.]*).*", 1);
			
			deltas.put(oldvers, new DeltaDeletePair(deltaTar, removedFiles));
		}
		
		// Put together a patch tar
		GZippedTarball patchTar = new GZippedTarball();
		
		// Need a few standard directories
		TDirectory install = PatchMaker.factory.createDirectory("install",
		                                                        patchTar.getRoot());
		TDirectory var_switch =
			PatchMaker.factory.createDirectory("switch",
			                                   PatchMaker.factory.createDirectory("var",
			                                                                      patchTar.getRoot()));
		
		// Put together version strings for the install script
		String newVersion = findVersionNumber(getNewFileName(), ".*([0-9]+_[0-9]+_[0-9]+[^.]*).*", 1);
		StringBuilder oldVersions = new StringBuilder();
		for (String oldVersion : deltas.keySet()) {
			if (oldVersions.length() > 0) oldVersions.append(" ");
			oldVersions.append(oldVersion);
		}
		
		// Set the version numbers in the install script
		String[] scriptArray = Utils.getLines(PatchMaker.getStandardScript());
		PatchMaker.setOldVers(scriptArray, oldVersions.toString());
		PatchMaker.setNewVers(scriptArray, newVersion);
		
		List<String> scriptLines = Utils.makeLinkedList(scriptArray);
		
		// Add the delta tars and the delete lines in the script
		for (String oldVersion : deltas.keySet()) {
			DeltaDeletePair pair = deltas.get(oldVersion);
			
			try {
				PatchMaker.factory.createFile(String.format("delta%s-%s.tgz",
				                                            oldVersion,
				                                            newVersion),
				                              pair.deltaTar.getData(),
				                              var_switch);
			} catch (IOException ioe) {
				Utils.debug(ioe);
				JOptionPane.showMessageDialog(null, "Error building patch: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
			
			addDeleteLines(scriptLines, pair.deletedFiles, oldVersion, false);
		}
		
		// Create a file for the install script
		PatchMaker.factory.createFile("doinst.sh",
		                              Utils.combineLines(scriptLines.toArray(new String[scriptLines.size()])).getBytes(),
		                              install);
		
		// Don't worry about sxbuildinfo.txt, since it should already be in there
		try {
			return patchTar.getData();
		} catch (IOException ioe) {
			// Unable to tar it up?
			Utils.debug(ioe);
			JOptionPane.showMessageDialog(null, "Unable to create patch: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
    }
}


/*
 * PatchFromBundleGenerator.java
 *
 * Description
 *
 * Jonathan Pearson
 * Aug 1, 2008
 *
 */

package com.sixnetio.PatchMaker;

import java.io.*;
import java.util.*;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import com.sixnetio.fs.tar.*;
import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.util.Utils;

class PatchFromBundleGenerator extends PatchGeneratorGUI {
	private static final JFileChooser BUNDLE_CHOOSER;
	
	static {
		BUNDLE_CHOOSER = new JFileChooser(Utils.CUR_DIR);
		BUNDLE_CHOOSER.setAcceptAllFileFilterUsed(false);
		BUNDLE_CHOOSER.setDialogTitle("Firmware Bundles");
		BUNDLE_CHOOSER.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
	            if (f.isDirectory()) return true;
	            
	            return (f.isFile() && f.canRead() && f.getName().toLowerCase().endsWith(".fwb"));
            }
            
            @Override
            public String getDescription() {
	            return "Firmware Bundles (*.fwb)";
            }
		});
	}
	
	public PatchFromBundleGenerator() {
		super("Choose Bundles");
	}
	
	@Override
    public JFileChooser getFileChooser() {
		return BUNDLE_CHOOSER;
	}
	
	@Override
    public byte[] getPatchData() {
		// Open the new version
		FirmwareBundle newBundle;
		Tarball newTar;
		try {
			InputStream in = new FileInputStream(getNewFileName());
			try {
				newBundle = new FirmwareBundle(in);
			}
			finally {
				in.close();
			}
			
			// Convert from VFS to TAR
			TDirectory troot = PatchMaker.factory.convertFilesystem(newBundle.getRoot());
			
			newTar = new Tarball();
			newTar.setRoot(troot);
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null, "Unable to generate patch; error reading file '" + getNewFileName() + "': " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
		// Loop through each old version, detecting what would be necessary to make it into the new version
		Map<String, DeltaDeletePair> deltas = new Hashtable<String, DeltaDeletePair>(getOldFileNames().size());
		for (String oldFile : getOldFileNames()) {
			FirmwareBundle oldBundle;
			Tarball oldTar;
			try {
				InputStream in = new FileInputStream(oldFile);
				try {
					oldBundle = new FirmwareBundle(in);
				}
				finally {
					in.close();
				}
				
				TDirectory troot = PatchMaker.factory.convertFilesystem(oldBundle.getRoot());
				oldTar = new Tarball();
				oldTar.setRoot(troot);
			} catch (IOException ioe) {
				JOptionPane.showMessageDialog(null, "Unable to generate patch; error reading file '" + oldFile + "': " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
			
			if (!oldBundle.getFWVendor().equals(newBundle.getFWVendor())) {
				JOptionPane.showMessageDialog(null, "Cannot generate a patch; '" + oldFile + "' has a different vendor ID than '" + getNewFileName() + "'", "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
			
			// This will receive the names of files that only exist in the old version
			Set<String> removedFiles = new HashSet<String>();
			GZippedTarball deltaTar = computeDifferences(oldTar, newTar, removedFiles);
			
			// Figure out the version number
			String oldvers = oldBundle.getFWVersion();
			
			deltas.put(oldvers, new DeltaDeletePair(deltaTar, removedFiles));
		}
		
		// Put together a patch tar
		GZippedTarball patchTar = new GZippedTarball();
		
		TDirectory install = PatchMaker.factory.createDirectory("install",
		                                                        patchTar.getRoot());
		TDirectory var_switch =
			PatchMaker.factory.createDirectory("switch",
			                                   PatchMaker.factory.createDirectory("var",
			                                                                      patchTar.getRoot()));
		
		// Put together version strings for the install script
		String newVersion = newBundle.getFWVersion();
		StringBuilder oldVersions = new StringBuilder();
		for (String oldVersion : deltas.keySet()) {
			if (oldVersions.length() > 0) oldVersions.append(" ");
			oldVersions.append(oldVersion);
		}
		
		// Set the version numbers in the install script
		String[] scriptArray = Utils.getLines(PatchMaker.getStandardScript());
		PatchMaker.setOldVers(scriptArray, oldVersions.toString());
		PatchMaker.setNewVers(scriptArray, newVersion);
		
		List<String> scriptLines = Utils.makeLinkedList(scriptArray);
		
		// Add the delta tars and the delete lines in the script
		for (String oldVersion : deltas.keySet()) {
			DeltaDeletePair pair = deltas.get(oldVersion);
			
			try {
				PatchMaker.factory.createFile(String.format("delta%s-%s.tgz",
				                                            oldVersion,
				                                            newVersion),
				                              pair.deltaTar.getData(),
				                              var_switch);
			} catch (IOException ioe) {
				Utils.debug(ioe);
				JOptionPane.showMessageDialog(null, "Error building patch: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
			
			addDeleteLines(scriptLines, pair.deletedFiles, oldVersion, true);
		}
		
		// Create a file for the install script
		PatchMaker.factory.createFile("doinst.sh",
		                              Utils.combineLines(scriptLines.toArray(new String[scriptLines.size()])).getBytes(),
		                              install);
		
		// Don't worry about sxbuildinfo.txt, since it should already be in there
		try {
			return patchTar.getData();
		} catch (IOException ioe) {
			// Unable to tar it up?
			Utils.debug(ioe);
			JOptionPane.showMessageDialog(null, "Unable to create patch: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
    }
}

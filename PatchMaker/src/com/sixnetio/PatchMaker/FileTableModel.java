/*
 * FileTableModel.java
 *
 * A table model for displaying file data.
 *
 * Jonathan Pearson
 * July 30, 2008
 *
 */

package com.sixnetio.PatchMaker;

import java.util.*;

import javax.swing.table.AbstractTableModel;

import com.sixnetio.fs.generic.*;
import com.sixnetio.fs.tar.TarAttributes;
import com.sixnetio.fs.tar.TarObject;

public class FileTableModel
	extends AbstractTableModel
	implements Iterable<FSObject>
{
	public static final String[] COLUMN_NAMES = {
		"File Name",
		"Owner",
		"Group",
		"Permissions",
		"Size",
		"Link"
	};
	
	// Was using a LinkedList for this, but pretty much all accesses are
	// indexed, so Vector makes more sense
	private List<FSObject> data;
	private DirectoryTreeNode dirNode;
	
	public FileTableModel(DirectoryTreeNode dirNode) {
		this.data = new Vector<FSObject>();
		this.dirNode = dirNode;
	}
	
	public FileTableModel(Collection<FSObject> data, DirectoryTreeNode dirNode)
	{
		synchronized (data) {
			this.data = new Vector<FSObject>(data);
		}
		
		this.dirNode = dirNode;
	}
	
	@Override
    public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 5:
				return String.class;
				
			case 4:
				return Long.class;
				
			default:
				return null;
		}
	}
	
	public int getColumnCount() {
		return COLUMN_NAMES.length;
	}
	
	public int getRowCount() {
		synchronized (data) {
	        return data.size();
        }
	}
	
	public Object getValueAt(int rowIndex, int columnIndex) {
		// For those routines that need it
		TarAttributes attrs = ((TarObject)data.get(rowIndex)).getTarAttributes();
		
		synchronized (data) {
	        switch (columnIndex) {
		        case 0:
			        return data.get(rowIndex).getName();
		        case 1:
		        	return attrs.getUserName();
		        case 2:
			        return attrs.getGroupName();
		        case 3:
		        	// Make sure to remove the type char from the beginning
			        return data.get(rowIndex).getModeAsString().substring(1);
		        case 4:
			        return ((File)data.get(rowIndex)).getSize();
		        case 5:
			        return ((Symlink)data.get(rowIndex)).getLinkName();
		        default:
			        throw new ArrayIndexOutOfBoundsException(columnIndex);
	        }
        }
	}
	
	public void addFile(FSObject obj) {
		int row;
		
		synchronized (data) {
	        row = data.size();
	        data.add(obj);
        }
		
		fireTableRowsInserted(row, row);
	}
	
	public void deleteFile(FSObject obj) {
		synchronized (data) {
	        int row = data.indexOf(obj);
	        if (row != -1) {
		        data.remove(obj);
		        
		        fireTableRowsDeleted(row, row);
	        }
        }
	}
	
	public FSObject getFileAt(int rowIndex) {
		synchronized (data) {
	        return data.get(rowIndex);
        }
	}
	
	@Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
		return (columnIndex != 4);
	}
	
	@Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		// In case it's necessary
		TarAttributes attrs = ((TarObject)data.get(rowIndex)).getTarAttributes();
		
		synchronized (data) {
	        switch (columnIndex) {
		        case 0:
			        // Make sure the name is good
			        String newName = aValue.toString();
			        while (newName.startsWith("/"))
				        newName = newName.substring(1);
			        while (newName.endsWith("/"))
				        newName = newName.substring(0, newName.length() - 1);
			        
			        if (data.get(rowIndex) instanceof Directory) {
				        // Find the associated DirectoryTreeNode
				        DirectoryTreeNode child = null;
				        for (int i = 0; i < dirNode.getChildCount(); i++) {
					        DirectoryTreeNode thisChild = (DirectoryTreeNode)dirNode.getChildAt(i);
					        if (thisChild.directory == data.get(rowIndex)) {
						        child = thisChild;
						        break;
					        }
				        }
				        
				        if (child == null) throw new RuntimeException(
				                                                      "Directory contents do not include a known subdirectory");
				        
				        // Update the associated DirectoryTreeNode and its contents
				        child.rename(newName);
			        } else {
				        // Just update the file table
				        data.get(rowIndex).setName(newName);
			        }
			        
			        break;
		        case 1:
			        attrs.setUserName(aValue.toString());
			        break;
		        case 2:
			        attrs.setGroupName(aValue.toString());
			        break;
		        case 3:
			        data.get(rowIndex).setMode(aValue.toString());
			        break;
		        case 5:
			        ((Symlink)data.get(rowIndex)).setLinkName(aValue.toString());
			        break;
		        
		        case 4:
		        default:
			        throw new IllegalArgumentException("Column " + columnIndex + " is not editable");
	        }
        }
		
		fireTableCellUpdated(rowIndex, columnIndex);
	}

	public static int getDefaultColumnWidth(int columnIndex) {
	    switch (columnIndex) {
	    	case 0: // File name
	    	case 5: // Link name
		    		return 250;
	    	case 1: // Owner
	    	case 2: // Group
	    		return 75;
	    	case 3: // Permissions
	    		return 125;
	    	case 4: // Size
	    		return 75;
    		default: // Shouln't happen, but we need a default case
    			return 75;
	    }
    }
	
	public Iterator<FSObject> iterator() {
	    synchronized (data) {
	        return data.iterator();
        }
    }
}

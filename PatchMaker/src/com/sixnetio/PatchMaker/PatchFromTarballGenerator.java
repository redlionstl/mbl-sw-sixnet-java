/*
 * PatchFromTarballGenerator.java
 *
 * Description
 *
 * Jonathan Pearson
 * Aug 1, 2008
 *
 */

package com.sixnetio.PatchMaker;

import java.io.*;
import java.util.*;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import com.sixnetio.fs.tar.*;
import com.sixnetio.util.Utils;

class PatchFromTarballGenerator extends PatchGeneratorGUI {
	private static final JFileChooser TARBALL_CHOOSER;
	
	static {
		TARBALL_CHOOSER = new JFileChooser(Utils.CUR_DIR);
		TARBALL_CHOOSER.setAcceptAllFileFilterUsed(false);
		TARBALL_CHOOSER.setDialogTitle("Firmware Tarballs");
		TARBALL_CHOOSER.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
	            if (f.isDirectory()) return true;
	            
	            return (f.isFile() && f.canRead() && f.getName().toLowerCase().endsWith(".tgz"));
            }
            
            @Override
            public String getDescription() {
	            return "Firmware Tarballs (*.tgz)";
            }
		});
	}
	
	public PatchFromTarballGenerator() {
		super("Choose Tarballs");
	}
	
	@Override
    public JFileChooser getFileChooser() {
		return TARBALL_CHOOSER;
	}

	@Override
    public byte[] getPatchData() {
		// Open the new version
		Tarball newTar;
		try {
			newTar = new GZippedTarball(new FileInputStream(getNewFileName()));
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null, "Unable to generate patch; error reading file '" + getNewFileName() + "': " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
		// Loop through each old version, detecting what would be necessary to make it into the new version
		Map<String, DeltaDeletePair> deltas = new Hashtable<String, DeltaDeletePair>(getOldFileNames().size());
		for (String oldFile : getOldFileNames()) {
			Tarball oldTar;
			try {
				oldTar = new GZippedTarball(new FileInputStream(oldFile));
			} catch (IOException ioe) {
				JOptionPane.showMessageDialog(null, "Unable to generate patch; error reading file '" + oldFile + "': " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
			
			// This will receive the names of files that only exist in the old version
			Set<String> removedFiles = new HashSet<String>();
			GZippedTarball deltaTar = computeDifferences(oldTar, newTar, removedFiles);
			
			// Figure out the version number
			String oldvers = findVersionNumber(oldFile, ".*([0-9]+_[0-9]+_[0-9]+[^.]*).*", 1);
			
			deltas.put(oldvers, new DeltaDeletePair(deltaTar, removedFiles));
		}
		
		// Put together a patch tar
		GZippedTarball patchTar = new GZippedTarball();
		TDirectory root = patchTar.getRoot();
		
		// Need a few standard directories
		TDirectory install = PatchMaker.factory.createDirectory("install", root);
		TDirectory var = PatchMaker.factory.createDirectory("var", root);
		TDirectory var_switch = PatchMaker.factory.createDirectory("switch", var);
		
		// Put together version strings for the install script
		String newVersion = findVersionNumber(getNewFileName(), ".*([0-9]+_[0-9]+_[0-9]+[^.]*).*", 1);
		StringBuilder oldVersions = new StringBuilder();
		for (String oldVersion : deltas.keySet()) {
			if (oldVersions.length() > 0) oldVersions.append(" ");
			oldVersions.append(oldVersion);
		}
		
		// Set the version numbers in the install script
		String[] scriptArray = Utils.getLines(PatchMaker.getStandardScript());
		PatchMaker.setOldVers(scriptArray, oldVersions.toString());
		PatchMaker.setNewVers(scriptArray, newVersion);
		
		List<String> scriptLines = Utils.makeLinkedList(scriptArray);
		
		// Add the delta tars and the delete lines in the script
		for (String oldVersion : deltas.keySet()) {
			DeltaDeletePair pair = deltas.get(oldVersion);
			
			try {
				PatchMaker.factory.createFile("delta" + oldVersion + "-" + newVersion + ".tgz",
				                              pair.deltaTar.getData(),
				                              var_switch);
			} catch (IOException ioe) {
				Utils.debug(ioe);
				JOptionPane.showMessageDialog(null, "Error building patch: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
			
			addDeleteLines(scriptLines, pair.deletedFiles, oldVersion, false);
		}
		
		// Create a file for the install script
		PatchMaker.factory.createFile("doinst.sh",
		                              Utils.combineLines(scriptLines.toArray(new String[0])).getBytes(),
		                              install);
		
		// Don't worry about sxbuildinfo.txt, since it should already be in there
		try {
			return patchTar.getData();
		} catch (IOException ioe) {
			// Unable to tar it up?
			Utils.debug(ioe);
			JOptionPane.showMessageDialog(null, "Unable to create patch: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
    }
}

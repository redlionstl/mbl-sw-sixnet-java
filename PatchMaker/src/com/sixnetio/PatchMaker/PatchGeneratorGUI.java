/*
 * PatchGeneratorGUI.java
 *
 * Description
 *
 * Jonathan Pearson
 * Aug 1, 2008
 *
 */

package com.sixnetio.PatchMaker;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.*;

import com.sixnetio.fs.generic.*;
import com.sixnetio.fs.tar.*;

public abstract class PatchGeneratorGUI extends JDialog implements PatchGenerator {
	// These are for looking up switch user/group names given IDs
	// Note, these is the inverse of PatchMaker's maps
	// Read-only after static initialization
	private static final Map<Integer, String> uidMap, gidMap;
	
	static {
		// Set up the maps
		uidMap = new Hashtable<Integer, String>();
		gidMap = new Hashtable<Integer, String>();
		
		// Removed the duplicates, since they would overwrite the correct answer
		uidMap.put(0, "root");
		uidMap.put(1, "bin");
		uidMap.put(99, "nobody");
		uidMap.put(101, "admin");
		uidMap.put(102, "cli");
		uidMap.put(500, "guest");
		uidMap.put(601, "PPPLink");
		
		gidMap.put(0, "root");
		gidMap.put(1, "bin");
		gidMap.put(2, "daemon");
		gidMap.put(3, "sys");
		gidMap.put(4, "adm");
		gidMap.put(5, "tty");
		gidMap.put(6, "disk");
		gidMap.put(7, "lp");
		gidMap.put(8, "mem");
		gidMap.put(9, "kmem");
		gidMap.put(10, "wheel");
		gidMap.put(44, "pppusers");
		gidMap.put(50, "ftp");
		gidMap.put(90, "modem");
		gidMap.put(99, "nobody");
		gidMap.put(100, "users");
		gidMap.put(101, "admin");
		gidMap.put(500, "guest");
		gidMap.put(601, "PPPLink");
	}
	
	// This gets used a lot in subclasses
	protected static class DeltaDeletePair {
		public Tarball deltaTar;
		public Set<String> deletedFiles;
		
		public DeltaDeletePair(Tarball deltaTar, Set<String> deletedFiles) {
			this.deltaTar = deltaTar;
			this.deletedFiles = deletedFiles;
		}
	}
	
	public static final int CANCEL_OPTION = 0,
							OKAY_OPTION = 1;

	private JTextField txtNewVersion;
	private JList lstOldVersions;
	
	private JButton btnOkay, btnCancel;
	
	private String newFileName;
	private Collection<String> oldFileNames;
	private int result;
	
	public PatchGeneratorGUI(String title) {
		super((Dialog)null, title, true);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent e) {
				btnCancel.doClick();
			}
		});
		
		// Text box for a single target file, list box of old versions on the left
		// Buttons on the right for Okay and Cancel
		setLayout(new BorderLayout());
		
		// Left side
		{
			JPanel p = new JPanel();
			p.setLayout(new BorderLayout());
			add(p, BorderLayout.CENTER);
			
			// Top: Text field
			{
				JPanel top = new JPanel();
				top.setLayout(new FlowLayout());
				p.add(top, BorderLayout.NORTH);
				
				top.add(new JLabel("Target: "));
				
				txtNewVersion = new JTextField(30);
				top.add(txtNewVersion);
				txtNewVersion.addFocusListener(new FocusListener() {
					public void focusGained(FocusEvent e) { }
					public void focusLost(FocusEvent e) {
						btnOkay.setEnabled(txtNewVersion.getText().length() > 0 && lstOldVersions.getModel().getSize() > 0);
                    }
				});
				
				txtNewVersion.addKeyListener(new KeyAdapter() {
					@Override
                    public void keyReleased(KeyEvent e) {
						btnOkay.setEnabled(txtNewVersion.getText().length() > 0 && lstOldVersions.getModel().getSize() > 0);
                    }
				});
				
				// And a browse button
				JButton browse = new JButton("...");
				top.add(browse);
				browse.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int result = getFileChooser().showOpenDialog(PatchGeneratorGUI.this);
						
						if (result == JFileChooser.APPROVE_OPTION) {
							txtNewVersion.setText(getFileChooser().getSelectedFile().getAbsolutePath());
							btnOkay.setEnabled(txtNewVersion.getText().length() > 0 && lstOldVersions.getModel().getSize() > 0);
						}
                    }
				});
			}
			
			// Bottom: List of old versions
			{
				JPanel bottom = new JPanel();
				bottom.setLayout(new BorderLayout());
				p.add(bottom, BorderLayout.CENTER);
				
				bottom.add(new JLabel("Old Versions: "), BorderLayout.NORTH);
				
				lstOldVersions = new JList(new DefaultListModel());
				bottom.add(new JScrollPane(lstOldVersions), BorderLayout.CENTER);
				
				// Below: Add/Remove buttons
				{
					JPanel below = new JPanel();
					below.setLayout(new FlowLayout());
					bottom.add(below, BorderLayout.SOUTH);
					
					JButton btnAdd = new JButton("Add...");
					below.add(btnAdd);
					btnAdd.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							int result = getFileChooser().showOpenDialog(PatchGeneratorGUI.this);
							
							if (result == JFileChooser.APPROVE_OPTION) {
								DefaultListModel model = (DefaultListModel)lstOldVersions.getModel();
								model.add(model.getSize(), getFileChooser().getSelectedFile().getAbsolutePath());
							}
							
							btnOkay.setEnabled(txtNewVersion.getText().length() > 0 && lstOldVersions.getModel().getSize() > 0);
                        }
					});
					
					JButton btnRemove = new JButton("Remove");
					below.add(btnRemove);
					btnRemove.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							DefaultListModel model = (DefaultListModel)lstOldVersions.getModel();
							model.remove(lstOldVersions.getSelectedIndex());
							
							btnOkay.setEnabled(txtNewVersion.getText().length() > 0 && lstOldVersions.getModel().getSize() > 0);
                        }
					});
				}
			}
		}
		
		// Right side: Okay and Cancel buttons
		{
			JPanel right = new JPanel();
			right.setLayout(new BorderLayout());
			add(right, BorderLayout.EAST);
			
			// To get the buttons to look good, they need to be in FlowLayouts
			{
				JPanel top = new JPanel();
				top.setLayout(new FlowLayout());
				right.add(top, BorderLayout.NORTH);
				
				btnOkay = new JButton("Okay");
				top.add(btnOkay);
				btnOkay.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						newFileName = txtNewVersion.getText();
						
						DefaultListModel model = (DefaultListModel)lstOldVersions.getModel();
						oldFileNames = new Vector<String>(model.getSize());
						for (int i = 0; i < model.getSize(); i++) {
							oldFileNames.add(model.getElementAt(i).toString());
						}
						
						result = OKAY_OPTION;
						
						setVisible(false);
						dispose();
                    }
				});
				btnOkay.setEnabled(false);
			}
			
			{
				JPanel bottom = new JPanel();
				bottom.setLayout(new FlowLayout());
				right.add(bottom, BorderLayout.SOUTH);
				
				btnCancel = new JButton("Cancel");
				bottom.add(btnCancel);
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						result = CANCEL_OPTION;
						
						setVisible(false);
						dispose();
                    }
				});
			}
		}
		
		pack();
	}
	
	public int showDialog() {
		setVisible(true);
		
		return result;
	}
	
	public String getNewFileName() {
		return newFileName;
	}
	
	public Collection<String> getOldFileNames() {
		return new Vector<String>(oldFileNames);
	}
	
	public static String findVersionNumber(String s, String pattern, int group) {
		Pattern ptrn = Pattern.compile(pattern);
		Matcher matcher = ptrn.matcher(s);
		String oldvers;
		if (matcher.matches()) {
			oldvers = matcher.group(group);
		} else {
			oldvers = JOptionPane.showInputDialog(null, "Unable to determine version being upgraded from '" + s + "', please enter it", "Unable to Parse", JOptionPane.WARNING_MESSAGE);
			if (oldvers == null) return null;
			
			oldvers.replaceAll("\\.", "_");
		}
		
		return oldvers;
	}
	
	public static void addDeleteLines(java.util.List<String> scriptLines, Collection<String> fileNames, String version, boolean watchArch) {
		synchronized (scriptLines) {
	        scriptLines.add("if [ \"$oldver\" = \"" + version + "\" ] ; then");
	        if (watchArch) {
		        // Organize the file names alphabetically
		        String[] names = (new Vector<String>(fileNames)).toArray(new String[0]);
		        Arrays.sort(names);
		        
		        // Pull out the names that do not start with 'temp_'
		        List<String> otherNames = new LinkedList<String>();
		        List<String> tempNames = new LinkedList<String>();
		        
		        for (int i = 0; i < names.length; i++) {
			        if (names[i].startsWith("temp_")) {
				        tempNames.add(names[i]);
			        } else {
				        otherNames.add(names[i]);
			        }
		        }
		        
		        // Back to an array (easiest way to use it)
		        names = tempNames.toArray(new String[0]);
		        
		        // Generate an architecture-sensitive version of the script
		        scriptLines.add("  cpu=`/usr/local/bin/cpu.pl`");
		        scriptLines.add("  case $cpu in");
		        scriptLines.add("    ARM720T)");
		        
		        int index = 0;
		        while (index < names.length && names[index].startsWith("temp_arm/")) {
			        String name = names[index];
			        name = name.substring(name.indexOf('/') + 1);
			        scriptLines.add("      rm -fr /" + name);
			        
			        index++;
		        }
		        
		        scriptLines.add("      ;;");
		        scriptLines.add("    ARM926EJ-S)");
		        
		        while (index < names.length && names[index].startsWith("temp_arm9/")) {
			        String name = names[index];
			        name = name.substring(name.indexOf('/') + 1);
			        scriptLines.add("      rm -fr /" + name);
			        
			        index++;
		        }
		        
		        scriptLines.add("      ;;");
		        scriptLines.add("    8xx)");
		        
		        while (index < names.length && names[index].startsWith("temp_ppc/")) {
			        String name = names[index];
			        name = name.substring(name.indexOf('/') + 1);
			        scriptLines.add("      rm -fr /" + name);
			        
			        index++;
		        }
		        
		        scriptLines.add("      ;;");
		        scriptLines.add("    *)");
		        
		        // Now just loop through the remaining names
		        for (String name : otherNames) {
			        scriptLines.add("      rm -fr /" + name);
		        }
		        
		        // And finish up the script
		        scriptLines.add("      ;;");
		        scriptLines.add("  esac");
	        } else {
		        // Don't bother watching for architectures
		        for (String file : fileNames) {
			        scriptLines.add("  rm -fr " + file);
		        }
	        }
	        scriptLines.add("fi");
	        scriptLines.add("");
        }
	}
	
	public static GZippedTarball computeDifferences(Tarball oldTar, Tarball newTar, Collection<String> deletedFiles) {
		GZippedTarball output = new GZippedTarball();
		
		computeDifferences(oldTar.getRoot(), newTar.getRoot(), deletedFiles,
		                   output.getRoot());
		
		rectifyNames(output.getRoot());
		
		return output;
	}
	
	private static void computeDifferences(TDirectory od, TDirectory nd,
	                                       Collection<String> deletedFiles,
	                                       TDirectory out)
	{
		for (FSObject nChild : nd) {
			FSObject oChild = od.locate(nChild.getName());
			if (nChild instanceof TDirectory) {
				// Always include directories
				if (oChild == null || ! (oChild instanceof TDirectory)) {
					// If the directory did not exist in the old version, also
					// include all of its descendants (since they would not have
					// existed either)
					PatchMaker.factory.convertFilesystem((Directory)nChild).setParent(out);
				}
				else {
					// The directory did exist in the old version, so only
					// include the directory itself
					FSObject newObj = PatchMaker.factory.convertObject(nChild, out);
					
					// But also recurse through its children
					computeDifferences((TDirectory)oChild, (TDirectory)nChild,
					                   deletedFiles, (TDirectory)newObj);
				}
			}
			else if (oChild == null) {
				// Not a directory, only include it if it did not exist in in
				// the old version
				PatchMaker.factory.convertObject(nChild, out);
			}
		}
		
		// Now that the entire directory structure has been built, check for
		// files that have been deleted or changed
		for (FSObject oChild : od) {
			FSObject nChild = nd.locate(oChild.getName());
			if (nChild == null) {
				// Deleted from new tarball
				deletedFiles.add(oChild.getPath());
			}
			else if ( ! (nChild instanceof Directory)){
				// Exists in both, is it different?
				// Only check non-directories because we have already added all
				// of the new directories to the patch
				// Compare the files to see if they are different
				TarAttributes oldAttrs = ((TarObject)oChild).getTarAttributes();
				TarAttributes newAttrs = ((TarObject)nChild).getTarAttributes();
				
				boolean user = oldAttrs.getUserName().equals(newAttrs.getUserName());
				user |= oChild.getUID() == nChild.getUID();
				
				boolean group = oldAttrs.getGroupName().equals(newAttrs.getGroupName());
				group |= oChild.getGID() == nChild.getGID();
				
				boolean perms = oChild.getMode() == nChild.getMode();
				
				boolean type = oChild.getType() == nChild.getType();
				
				boolean changed = ! (user && group && perms && type);
				if ( ! changed) {
					// The object has not changed in any generic manner, so look
					// deeper
					
					// Already checked for type equality, so we only need to
					// check the type of one of the objects
					if (oChild instanceof File) {
						// Compare data
						changed = ! Arrays.equals(((File)oChild).getData(),
						                          ((File)nChild).getData());
					}
					else if (oChild instanceof Symlink) {
						// Compare targets
						changed = ! ((Symlink)oChild).getLinkName().equals(((Symlink)nChild).getLinkName());
					}
					else if (oChild instanceof Device) {
						// Compare major/minor device numbers
						changed = ! ((((Device)oChild).getMajor() == ((Device)nChild).getMajor()) &&
						             (((Device)oChild).getMinor() == ((Device)nChild).getMinor()));
					}
				}
				
				if (changed) {
					// File has changed, add it
					PatchMaker.factory.convertObject(nChild, out);
				}
			}
		}
	}
	
	/**
	 * Match the user/group names to the UID/GID on the given object and its
	 * descendants. Unknown UIDs/GIDs will be set to 0, and the corresponding
	 * names set to "root".
	 * 
	 * @param obj The object on which to start rectifying names.
	 */
	private static void rectifyNames(FSObject obj)
	{
		String user, group;
		
		if (uidMap.containsKey(obj.getUID())) {
			user = uidMap.get(obj.getUID());
		}
		else {
			user = "root";
			obj.setUID((short)0);
		}
		
		if (gidMap.containsKey(obj.getGID())) {
			group = gidMap.get(obj.getGID());
		}
		else {
			group = "root";
			obj.setGID((short)0);
		}
		
		TarAttributes attrs = ((TarObject)obj).getTarAttributes();
		attrs.setUserName(user);
		attrs.setGroupName(group);
		
		if (obj instanceof TDirectory) {
			for (FSObject child : (TDirectory)obj) {
				rectifyNames(obj);
			}
		}
	}
	
	public abstract JFileChooser getFileChooser();
	public abstract byte[] getPatchData();
}

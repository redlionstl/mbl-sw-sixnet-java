/*
 * PatchGenerator.java
 *
 * Description
 *
 * Jonathan Pearson
 * Aug 1, 2008
 *
 */

package com.sixnetio.PatchMaker;

interface PatchGenerator {
	public byte[] getPatchData();
}

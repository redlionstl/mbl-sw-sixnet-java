/*
 * VersionPanel.java
 *
 * Represents a single upgradable version.
 * Includes a tree of all of the files that will be replaced by this upgrade.
 *
 * Jonathan Pearson
 * July 30, 2008
 *
 */

package com.sixnetio.PatchMaker;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.*;

import com.sixnetio.fs.generic.*;
import com.sixnetio.util.Utils;

class VersionPanel extends JPanel {
	public final JTree dirTree;
	public final DirectoryTreeNode arm7, arm9, ppc, all;
	private String version;
	
	public VersionPanel(String version) {
		this.version = version;
		
		setLayout(new BorderLayout());
		
		final JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		add(split, BorderLayout.CENTER);
		
		// Left side: a tree view containing the file system for each architecture
		DirectoryTreeNode root = new DirectoryTreeNode("root", ".");
		
		root.add(all = new DirectoryTreeNode("all", "."));
		root.add(arm7 = new DirectoryTreeNode("arm7", "temp_arm"));
		root.add(arm9 = new DirectoryTreeNode("arm9", "temp_arm9"));
		root.add(ppc = new DirectoryTreeNode("ppc", "temp_ppc"));
		
		dirTree = new JTree(new DefaultTreeModel(root));
		dirTree.setEditable(false);
		dirTree.setRootVisible(false);
		dirTree.setShowsRootHandles(true);
		
		DefaultTreeCellRenderer rend = new DefaultTreeCellRenderer();
		rend.setLeafIcon(rend.getOpenIcon());
		dirTree.setCellRenderer(rend);
		
		DefaultTreeSelectionModel selModel = new DefaultTreeSelectionModel();
		selModel.setSelectionMode(DefaultTreeSelectionModel.SINGLE_TREE_SELECTION);
		dirTree.setSelectionModel(selModel);
		
		dirTree.setPreferredSize(new Dimension(200, 100));
		
		dirTree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				split.setRightComponent(new JScrollPane(((DirectoryTreeNode)e.getPath().getLastPathComponent()).table));
            }
		});
		
		split.setLeftComponent(dirTree);
		
		selModel.setSelectionPath(new TreePath(all.getPath()));
	}
	
	public void addFile(java.io.File file) throws IOException {
		// Locate the active directory and add a new file there
		TreePath treePath = dirTree.getSelectionPath();
		if (treePath == null) {
			getToolkit().beep();
			return;
		}
		
		DirectoryTreeNode node = (DirectoryTreeNode)treePath.getLastPathComponent();
		
		// Build the file
		byte[] data = Utils.readFile(file);
		File tf = PatchMaker.factory.createFile(file.getName(), data, node.directory);
		
		tf.chmod("a-xst"); // No execute or special permissions on files by default
		
		// With one exception: If this directory ends with "bin", add execute
		// permissions as a convenience
		if (node.directory.getName().endsWith("bin")) {
			tf.chmod("a+x");
		}
		
		node.addFile(tf);
	}
	
	// This will create all necessary parent directories if provided a path
	public void addDirectory(String name) {
		TreePath treePath = dirTree.getSelectionPath();
		if (treePath == null) {
			getToolkit().beep();
			return;
		}
		
		DirectoryTreeNode parent = (DirectoryTreeNode)treePath.getLastPathComponent();
		
		StringTokenizer tok = new StringTokenizer(name, "/");
		while (tok.hasMoreTokens()) {
			String dirName = tok.nextToken() + "/";
			
			// Check if that piece already exists
			boolean found = false;
			for (int i = 0; i < parent.getChildCount(); i++) {
				if (parent.getChildAt(i).toString().equals(dirName)) {
					parent = (DirectoryTreeNode)parent.getChildAt(i);
					found = true;
					break;
				}
			}
			
			DirectoryTreeNode node;
			if (found) {
				// So it will be made visible
				node = parent; // Since parent has been updated, this is what would have been created
			} else {
    			node = new DirectoryTreeNode(dirTree, parent, dirName, null);
    			DefaultTreeModel model = (DefaultTreeModel)dirTree.getModel();
    			model.insertNodeInto(node, parent, parent.getChildCount());
    			
    			parent.addFile(node.directory);
    			
    			parent = node;
			}
			
			dirTree.makeVisible(new TreePath(node.getPath()));
		}
	}
	
	public void addLink(String name, String target) {
		// Locate the active directory and add a new link there
		TreePath treePath = dirTree.getSelectionPath();
		if (treePath == null) {
			getToolkit().beep();
			return;
		}
		
		DirectoryTreeNode node = (DirectoryTreeNode)treePath.getLastPathComponent();
		Symlink link = PatchMaker.factory.createSymlink(name, target, node.directory);
		
		// No special permissions on links by default
		link.chmod("a-st");
		
		// Links generally have full normal permissions for everybody
		link.chmod("a+rwx");
		
		node.addFile(link);
	}
	
	public void deleteCurrentObject() {
		// Locate the active directory
		TreePath treePath = dirTree.getSelectionPath();
		if (treePath == null) {
			getToolkit().beep();
			return;
		}
		
		DirectoryTreeNode node = (DirectoryTreeNode)treePath.getLastPathComponent();
		
		// Find the active object in the table
		int row = node.table.getSelectedRow();
		
		if (row == -1) {
			// No object selected
			getToolkit().beep();
			return;
		}
		
		FileTableModel tableModel = (FileTableModel)node.table.getModel();
		
		FSObject tf = tableModel.getFileAt(row);
		tableModel.deleteFile(tf);
		
		// If that is a directory, we need to also delete an entry in the tree
		// 'node' is the parent, so search for the proper child
		if (tf instanceof Directory) {
			DefaultTreeModel treeModel = (DefaultTreeModel)dirTree.getModel();
			
			for (int i = 0; i < node.getChildCount(); i++) {
				DirectoryTreeNode child = (DirectoryTreeNode)node.getChildAt(i);
				if (child.directory.equals(tf)) {
					treeModel.removeNodeFromParent(child);
					break;
				}
			}
		}
	}
	
	@Override
    public String toString() {
		return version;
	}
}
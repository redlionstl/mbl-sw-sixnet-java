#include <jni.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <dirent.h>
#include <signal.h>
#include <string.h>

#include "COM_ComPort.h"

// basename() from libgen.h may modify its argument
// basename() from string.h with _GNU_SOURCE defined does not actually appear
// to exist
// this replicates the string.h functionality
static const char *basename(const char *path)
{
    char *ls = strrchr(path, '/');
    if (ls == NULL) {
        return path;
    }
    else {
        return (ls + 1);
    }
}

static int clearLock(const char *lockfile)
{
    return unlink(lockfile);
}

static int acquireLock(const char *lockfile)
{
    FILE *fout;
    int fd = open(lockfile, O_CREAT | O_EXCL | O_WRONLY, 0666);
    if (fd == -1 && errno == EEXIST) {
        // File exists, check if the locking process is still running
        FILE *f = fopen(lockfile, "r");
        if ( ! f) {
            // Exists but cannot open for reading? Error
            return 1;
        }

        // The lock file exists, read the PID out of it
        char buf[128];
        if ( ! fgets(buf, sizeof(buf), f)) {
            // Empty file, not a valid lock
            fclose(f);
            if (unlink(lockfile)) {
                // Failed to delete
                return 2;
            }
        }
        else {
            // Exists, contains data
            char *end;

            fclose(f);

            end = strchr(buf, '\n');
            if (end) {
                *end = '\0';
            }

            end = strchr(buf, '\r');
            if (end) {
                *end = '\0';
            }

            pid_t pid = (pid_t)strtoll(buf, &end, 10);
            if (*end != '\0') {
                // Invalid PID, not a valid lock
                fclose(f);
                if (unlink(lockfile)) {
                    // Failed to delete
                    return 2;
                }
            }

            // Check whether that process is still running (send SIGCHLD,
            // recommended by
            // http://www.go4expert.com/forums/showthread.php?p=36537
            // because it shouldn't do anything (no child actually died), and
            // kill() will fail if the process is not running
            if ( ! kill(pid, SIGCHLD)) {
                // Process is running, fail (lock is properly held)
                return 3;
            }

            // Unlink and open again
            if (unlink(lockfile)) {
                // Failed to delete
                return 2;
            }

            fd = open(lockfile, O_WRONLY | O_CREAT | O_EXCL, 0666);
            if (fd == -1) {
                // Simply fail, we've done all we can
                return 4;
            }
        }
    }
    else if (fd == -1) {
        // Some other error, no idea whether the file exists
        return 5;
    }

    // At this point, we have fd open for writing, so write our PID
    fout = fdopen(fd, "w");
    if ( ! fout) {
        close(fd);
        return 6;
    }

    fprintf(fout, "%d", getpid());
    fclose(fout);
    return 0;
}


JNIEXPORT jint JNICALL Java_com_sixnetio_COM_ComPort_openCom(JNIEnv *env, jclass jClass, jstring sCom, 
                                            jint nBaud, jint nBitsPerByte, jint nParity, 
                                            jint nStopBits) {
    int fdCom;
    char errmsg[1024];
    struct termios options = {0};
    jboolean isCopy;
    const char *fname = (*env)->GetStringUTFChars(env, sCom, &isCopy);
    char lockfile[1024];

    // Lock the COM port by creating /var/lock/LCK..<device> and placing our PID
    // into it
    // If it already exists, and the process with the PID contained in it is
    // still running, fail to open
    snprintf(lockfile, sizeof(lockfile), "/var/lock/LCK..%s", basename(fname));
    if (acquireLock(lockfile)) {
        sprintf(errmsg, "Unable to lock \"%s\": %s", fname, sys_errlist[errno]);

        fdCom = 0;
        goto finish;
    }
    
    fdCom = open(fname, O_RDWR | O_NOCTTY | O_NDELAY);
    
    if (fdCom == -1) {
        sprintf(errmsg, "Error opening \"%s\": %s", fname, sys_errlist[errno]);
        
        fdCom = 0;
        goto finish;
    }
    
    //fcntl(fdCom, F_SETFL, 0);
    
    if (tcgetattr(fdCom, &options)) {
        sprintf(errmsg, "tcgetattr error: %s", sys_errlist[errno]);
        
        close(fdCom);
        fdCom = 0;
        goto finish;
    }
    
    // Figure out BAUD rate
    switch (nBaud) {
        case 0: {
            nBaud = B0;
            break;
        }
        case 50: {
            nBaud = B50;
            break;
        }
        case 75: {
            nBaud = B75;
            break;
        }
        case 110: {
            nBaud = B110;
            break;
        }
        case 134: {
            nBaud = B134;
            break;
        }
        case 150: {
            nBaud = B150;
            break;
        }
        case 200: {
            nBaud = B200;
            break;
        }
        case 300: {
            nBaud = B300;
            break;
        }
        case 600: {
            nBaud = B600;
            break;
        }
        case 1200: {
            nBaud = B1200;
            break;
        }
        case 1800: {
            nBaud = B1800;
            break;
        }
        case 2400: {
            nBaud = B2400;
            break;
        }
        case 4800: {
            nBaud = B4800;
            break;
        }
        case 9600: {
            nBaud = B9600;
            break;
        }
        case 19200: {
            nBaud = B19200;
            break;
        }
        case 38400: {
            nBaud = B38400;
            break;
        }
        case 57600: {
            nBaud = B57600;
            break;
        }
        case 115200: {
            nBaud = B115200;
            break;
        }
        case 230400: {
            nBaud = B230400;
            break;
        }
        default: {
            // Not a supported speed
            sprintf(errmsg, "Unsupported speed: %d", nBaud);
            
            close(fdCom);
            fdCom = 0;
            goto finish;
        }
    }
    
    cfsetispeed(&options, nBaud);
    cfsetospeed(&options, nBaud);
    
    
    options.c_iflag |= IGNBRK;
    options.c_iflag &= ~IXON;
    options.c_iflag &= ~IXOFF;
    options.c_iflag &= ~INPCK;
    options.c_iflag &= ~BRKINT;
    options.c_iflag &= ~IGNPAR;
    options.c_iflag &= ~ISTRIP;
    options.c_iflag &= ~ICRNL;
    
    options.c_oflag &= ~OPOST;
    options.c_oflag &= ~ONLCR;
    
    options.c_cflag |= CREAD;
    options.c_cflag |= CLOCAL;
    options.c_cflag &= ~CRTSCTS;
    
    options.c_lflag &= ~ISIG;
    options.c_lflag &= ~ICANON;
    options.c_lflag &= ~IEXTEN;
    options.c_lflag &= ~ECHO;
    options.c_lflag &= ~ECHOE;
    options.c_lflag &= ~ECHOK;
    options.c_lflag &= ~ECHOCTL;
    options.c_lflag &= ~ECHOKE;
    
    if (nStopBits == com_sixnetio_COM_ComPort_STOPBITS_1) {
        options.c_cflag &= ~CSTOPB;
    } else if (nStopBits == com_sixnetio_COM_ComPort_STOPBITS_2) {
        options.c_cflag |= CSTOPB;
    } else {
        // Linux does not support 1.5 stop bits
        sprintf(errmsg, "Unsupported stop-bits value: %d", nStopBits);
        
        close(fdCom);
        fdCom = 0;
        goto finish;
    }
    
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSIZE;
    
    if (nParity == com_sixnetio_COM_ComPort_PARITY_NONE) {
        // Do nothing
    } else if (nParity == com_sixnetio_COM_ComPort_PARITY_ODD) {
        options.c_cflag |= PARODD;
    } else {
        // Linux does not support other parity modes
        sprintf(errmsg, "Unsupported parity mode: %d", nParity);
        
        close(fdCom);
        fdCom = 0;
        goto finish;
    }
    
    if (nBitsPerByte == 5) {
        options.c_cflag |= CS5;
    } else if (nBitsPerByte == 6) {
        options.c_cflag |= CS6;
    } else if (nBitsPerByte == 7) {
        options.c_cflag |= CS7;
    } else if (nBitsPerByte == 8) {
        options.c_cflag |= CS8;
    } else {
        // Unsupported byte size
        sprintf(errmsg, "Unsupported byte size: %d", nBitsPerByte);
        
        close(fdCom);
        fdCom = 0;
        goto finish;
    }
    
    options.c_cc[VMIN] = 1;
    options.c_cc[VTIME] = 5;
    
    if (tcsetattr(fdCom, TCSANOW, &options)) {
        sprintf(errmsg, "tcsetattr error: %s", sys_errlist[errno]);
        
        close(fdCom);
        fdCom = 0;
        goto finish;
    }
    
finish:
    (*env)->ReleaseStringUTFChars(env, sCom, fname);
    
    if (fdCom == 0) {
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        (*env)->ThrowNew(env, ioException, errmsg);
        (*env)->DeleteLocalRef(env, ioException);
    }
    
    return (jint)fdCom;
}

JNIEXPORT jint JNICALL Java_com_sixnetio_COM_ComPort_readCom(JNIEnv *env, jclass jClass, jint fdCom) {
    jbyte bRead = 0;
    jint nBytesRead;
    
    if ((nBytesRead = read(fdCom, &bRead, 1)) == -1 &&
        errno != EAGAIN) {
        
        // EAGAIN happens when there is no data available, since we are doing nonblocking reads
        
        jclass ioException;
        char errmsg[1024];
        
        ioException = (*env)->FindClass(env, "java/io/IOException");

        sprintf(errmsg, "Read failed: %s", sys_errlist[errno]);
        (*env)->ThrowNew(env, ioException, errmsg);
        
        (*env)->DeleteLocalRef(env, ioException);

        return 0;
    }
    
    if (nBytesRead == 0 || nBytesRead == -1) {
        return -1;
    } else {
        return ((jint)bRead) & 0xFF;
    }
}

JNIEXPORT jint JNICALL Java_com_sixnetio_COM_ComPort_readMultiCom(JNIEnv *env, jclass jClass, jint fdCom, jbyteArray jbBuffer) {
    jboolean isCopy = JNI_FALSE; // Usually don't care, but it matters here
    jbyte *buffer = (*env)->GetByteArrayElements(env, jbBuffer, &isCopy);
    jint nSize = (*env)->GetArrayLength(env, jbBuffer);
    jint nBytesRead;
    
    if ((nBytesRead = read(fdCom, buffer, nSize)) == -1 &&
        errno != EAGAIN) {
        
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        char errmsg[1024];
        
        sprintf(errmsg, "Read failed: %s", sys_errlist[errno]);
        
        (*env)->ReleaseByteArrayElements(env, jbBuffer, buffer, 0);
        (*env)->ThrowNew(env, ioException, errmsg);
        
        (*env)->DeleteLocalRef(env, ioException);
        
        return 0;
    }
    
    (*env)->ReleaseByteArrayElements(env, jbBuffer, buffer, 0);
    
    if (nBytesRead == -1) {
        return 0;
    } else {
        return nBytesRead;
    }
}

JNIEXPORT void JNICALL Java_com_sixnetio_COM_ComPort_writeCom(JNIEnv *env, jclass jClass, jint fdCom, jbyte bWrite) {
    jint nBytesWritten;
    
    if ((nBytesWritten = write(fdCom, &bWrite, 1)) == -1) {
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        char errmsg[1024];
        
        sprintf(errmsg, "Write failed: %s", sys_errlist[errno]);
        (*env)->ThrowNew(env, ioException, errmsg);
        
        (*env)->DeleteLocalRef(env, ioException);
        
        return;
    }
    
    if (nBytesWritten == 0) {
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        
        char buffer[1024];
        sprintf(buffer, "Wrote zero bytes.");
        (*env)->ThrowNew(env, ioException, buffer);
        
        (*env)->DeleteLocalRef(env, ioException);
        
        return;
    }
    
    printf("Wrote 0x%x (%d)\n", bWrite & 0xFF, bWrite & 0xFF);
}

JNIEXPORT jint JNICALL Java_com_sixnetio_COM_ComPort_writeMultiCom(JNIEnv *env, jclass jClass, jint fdCom, jbyteArray jbBuffer) {
    jboolean isCopy;
    jbyte *buffer = (*env)->GetByteArrayElements(env, jbBuffer, &isCopy);
    jint nSize = (*env)->GetArrayLength(env, jbBuffer);
    jint nBytesWritten;
    
    if ((nBytesWritten = write(fdCom, buffer, nSize)) == -1) {
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        
        char errmsg[1024];
        sprintf(errmsg, "WriteFile failed: %s", sys_errlist[errno]);
        
        (*env)->ReleaseByteArrayElements(env, jbBuffer, buffer, 0);
        (*env)->ThrowNew(env, ioException, errmsg);
        
        (*env)->DeleteLocalRef(env, ioException);
        
        return 0;
    }
    
    (*env)->ReleaseByteArrayElements(env, jbBuffer, buffer, 0);
    return nBytesWritten;
}

JNIEXPORT void JNICALL Java_com_sixnetio_COM_ComPort_closeCom(JNIEnv *env, jclass jClass, jint fdCom, jstring path) {
    char lockfile[1024];
    jboolean isCopy;
    const char *fname = (*env)->GetStringUTFChars(env, path, &isCopy);

    close(fdCom);

    snprintf(lockfile, sizeof(lockfile), "/var/lock/LCK..%s", basename(fname));
    (*env)->ReleaseStringUTFChars(env, path, fname);

    clearLock(lockfile);
}

JNIEXPORT void JNICALL Java_com_sixnetio_COM_ComPort_flushCom(JNIEnv *env, jclass jClass, jint fdCom) {
    if (fsync(fdCom) == -1) {
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        
        char errmsg[1024];
        sprintf(errmsg, "fsync failed: %s", sys_errlist[errno]);
        (*env)->ThrowNew(env, ioException, errmsg);
        
        (*env)->DeleteLocalRef(env, ioException);
    }
}

typedef struct node_s node_d;
struct node_s {
    node_d *next;
    char name[32];
};

JNIEXPORT jobjectArray JNICALL Java_com_sixnetio_COM_ComPort_enumSerialPorts(JNIEnv *env, jclass jClass)
{
    // List all files in /dev matching "ttyS*", "ttyUSB*", "ttyACM*", ...
    char errmsg[1024];
    DIR *dev;
    struct dirent *dirent;
    int count, i;
    int fail = 0; // Set to 1 if we fail at a point where we cannot return
    node_d *head;
    node_d *walk;
    jobjectArray result;
    jclass ioException;
    jclass stringClass;

    ioException = (*env)->FindClass(env, "java/io/IOException");
    if ( ! ioException) {
        return NULL;
    }

    stringClass = (*env)->FindClass(env, "java/lang/String");
    if ( ! stringClass) {
        (*env)->DeleteLocalRef(env, ioException);
        return NULL;
    }

    dev = opendir("/dev");
    if ( ! dev) {
        sprintf(errmsg, "Unable to open /dev: %s", sys_errlist[errno]);
        (*env)->ThrowNew(env, ioException, errmsg);
        (*env)->DeleteLocalRef(env, ioException);
        (*env)->DeleteLocalRef(env, stringClass);
        return NULL;
    }

    count = 0;
    head = walk = (node_d *)malloc(sizeof(node_d));
    while ((dirent = readdir(dev)) != NULL) {
        if (strncmp(dirent->d_name, "ttyS", 4) == 0 ||
            strncmp(dirent->d_name, "ttyUSB", 6) == 0 ||
            strncmp(dirent->d_name, "ttyACM", 6) == 0) { 

            strncpy(walk->name, dirent->d_name, sizeof(walk->name) - 1);
            walk->name[sizeof(walk->name) - 1] = 0; // Just to be safe
            walk = walk->next = (node_d *)malloc(sizeof(node_d));
            count++;
        }
    }

    closedir(dev);

    // Allocate a new String array to hold all of the entries
    result = (*env)->NewObjectArray(env, count, stringClass, NULL);

    // Whether or not that failed, this is no longer needed
    (*env)->DeleteLocalRef(env, stringClass);

    if ( ! result) {
        sprintf(errmsg, "Unable to allocate array");
        (*env)->ThrowNew(env, ioException, errmsg);
        (*env)->DeleteLocalRef(env, ioException);
        return NULL;
    }

    // Play the linked list of files out into the new array
    walk = head;
    for (i = 0; i < count; i++) {
        node_d *temp = walk;

        if ( ! fail) {
            char devName[128];
            jstring str;

            snprintf(devName, sizeof(devName), "/dev/%s", walk->name);
            str = (*env)->NewStringUTF(env, devName);
            if (str) {
                (*env)->SetObjectArrayElement(env, result, i, str);
                (*env)->DeleteLocalRef(env, str);
            }
            else {
                fail = 1;

                // Throw the exception here, but we need to finish cleaning up
                sprintf(errmsg, "Unable to allocate string");
                (*env)->ThrowNew(env, ioException, errmsg);
            }
        }

        // Clean up this entry and move on to the next
        walk = walk->next;
        free(temp);
    }

    // Clean up
    (*env)->DeleteLocalRef(env, ioException);

    return result;
}


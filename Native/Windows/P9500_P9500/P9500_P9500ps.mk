
P9500_P9500ps.dll: dlldata.obj P9500_P9500_p.obj P9500_P9500_i.obj
	link /dll /out:P9500_P9500ps.dll /def:P9500_P9500ps.def /entry:DllMain dlldata.obj P9500_P9500_p.obj P9500_P9500_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del P9500_P9500ps.dll
	@del P9500_P9500ps.lib
	@del P9500_P9500ps.exp
	@del dlldata.obj
	@del P9500_P9500_p.obj
	@del P9500_P9500_i.obj

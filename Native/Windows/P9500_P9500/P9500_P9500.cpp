// P9500_P9500.cpp : Implementation of DLL Exports.


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f P9500_P9500ps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include <initguid.h>
#include "P9500_P9500.h"

#include "P9500_P9500_i.c"

#include "p9500.h"
#include "P9500_exports.h"
#include <stdlib.h>


CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
END_OBJECT_MAP()

class CP9500_P9500App : public CWinApp
{
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CP9500_P9500App)
	public:
    virtual BOOL InitInstance();
    virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CP9500_P9500App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CP9500_P9500App, CWinApp)
	//{{AFX_MSG_MAP(CP9500_P9500App)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CP9500_P9500App theApp;

BOOL CP9500_P9500App::InitInstance()
{
    _Module.Init(ObjectMap, m_hInstance, &LIBID_P9500_P9500Lib);

	if (!AfxOleInit()) return FALSE;

	AfxEnableControlContainer();

	return CWinApp::InitInstance();
}

int CP9500_P9500App::ExitInstance()
{
    _Module.Term();
    return CWinApp::ExitInstance();
}

/////////////////////////////////////////////////////////////////////////////
// Used to determine whether the DLL can be unloaded by OLE

STDAPI DllCanUnloadNow(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    return (AfxDllCanUnloadNow()==S_OK && _Module.GetLockCount()==0) ? S_OK : S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    return _Module.RegisterServer(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
    return _Module.UnregisterServer(TRUE);
}


static CString *convertJString(JNIEnv *env, jstring str) {
	const char *nativeString = env->GetStringUTFChars(str, 0);
	return new CString(nativeString);
}

static BOOL convertJBoolean(JNIEnv *env, jboolean b) {
	return (b ? TRUE : FALSE);
}

static jboolean convertBOOL(JNIEnv *env, BOOL b) {
	return (b ? (jboolean)1 : (jboolean)0);
}

/*
 * Class:     com_sixnetio_P9500_P9500
 * Method:    print3ByteMACLabel
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_com_sixnetio_P9500_P9500_print3ByteMACLabel(JNIEnv *env, jclass clazz, jstring address) {
	HRESULT result = CoInitialize(NULL);
	if (result != S_OK && result != S_FALSE) return FALSE;

	Cp9500 printer;

	CString *strAddress = convertJString(env, address);

	BOOL response = printer.Print3ByteMACLabel(*strAddress);

	delete strAddress;

	jboolean ans = convertBOOL(env, response);

	CoUninitialize();

	return ans;
}


/*
 * Class:     com_sixnetio_P9500_P9500
 * Method:    printSerNumMACLabel
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_com_sixnetio_P9500_P9500_printSerNumMACLabel(JNIEnv *env, jclass clazz, jstring serial, jstring address) {
	HRESULT result = CoInitialize(NULL);
	if (result != S_OK && result != S_FALSE) return FALSE;

	Cp9500 printer;

	CString *strSerial = convertJString(env, serial);
	CString *strAddress = convertJString(env, address);

	BOOL response = printer.PrintSerNumMACLabel(*strSerial, *strAddress);

	delete strSerial;
	delete strAddress;

	jboolean ans = convertBOOL(env, response);

	CoUninitialize();

	return ans;
}


/*
 * Class:     com_sixnetio_P9500_P9500
 * Method:    printBarcode128SN_MACLabel
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_com_sixnetio_P9500_P9500_printBarcode128SN_1MACLabel(JNIEnv *env, jclass clazz, jstring serial, jstring address) {
	HRESULT result = CoInitialize(NULL);
	if (result != S_OK && result != S_FALSE) return FALSE;

	Cp9500 printer;

	CString *strSerial = convertJString(env, serial);
	CString *strAddress = convertJString(env, address);

	BOOL response = printer.PrintBarcode128SN_MACLabel(*strSerial, *strAddress);

	delete strSerial;
	delete strAddress;

	jboolean ans = convertBOOL(env, response);

	CoUninitialize();

	return ans;
}


/*
 * Class:     com_sixnetio_P9500_P9500
 * Method:    printForceComLabel
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_com_sixnetio_P9500_P9500_printForceComLabel(JNIEnv *env, jclass clazz, jstring partNumber, jstring address) {
	HRESULT result = CoInitialize(NULL);
	if (result != S_OK && result != S_FALSE) return FALSE;

	Cp9500 printer;

	CString *strPartNumber = convertJString(env, partNumber);
	CString *strAddress = convertJString(env, address);

	BOOL response = printer.PrintForceComLabel(*strPartNumber, *strAddress);

	delete strPartNumber;
	delete strAddress;

	jboolean ans = convertBOOL(env, response);

	CoUninitialize();

	return ans;
}


/*
 * Class:     com_sixnetio_P9500_P9500
 * Method:    printTextLabel
 * Signature: (Ljava/lang/String;Z)Z
 */
JNIEXPORT jboolean JNICALL Java_com_sixnetio_P9500_P9500_printTextLabel(JNIEnv *env, jclass clazz, jstring text, jboolean cut) {
	HRESULT result = CoInitialize(NULL);
	if (result != S_OK && result != S_FALSE) return FALSE;

	Cp9500 printer;

	CString *strText = convertJString(env, text);
	BOOL bCut = convertJBoolean(env, cut);

	BOOL response = printer.PrintTextLabel(*strText, bCut);

	delete strText;
	
	jboolean ans = convertBOOL(env, response);

	CoUninitialize();

	return ans;
}


/*
 * Class:     com_sixnetio_P9500_P9500
 * Method:    printBarcode39Label
 * Signature: (Ljava/lang/String;Z)Z
 */
JNIEXPORT jboolean JNICALL Java_com_sixnetio_P9500_P9500_printBarcode39Label(JNIEnv *env, jclass clazz, jstring text, jboolean cut) {
	HRESULT result = CoInitialize(NULL);
	if (result != S_OK && result != S_FALSE) return FALSE;

	Cp9500 printer;

	CString *strText = convertJString(env, text);
	BOOL bCut = convertJBoolean(env, cut);

	BOOL response = printer.PrintBarcode39Label(*strText, bCut);

	delete strText;
	
	jboolean ans = convertBOOL(env, response);

	CoUninitialize();

	return ans;
}


#include <jni.h>
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>

#include "COM_ComPort.h"

JNIEXPORT jint JNICALL Java_com_sixnetio_COM_ComPort_openCom(JNIEnv *env, jclass jClass, jstring sCom, 
                                            jint nBaud, jint nBitsPerByte, jint nParity, 
                                            jint nStopBits) {
    HANDLE hCom;
    DCB dcb = {0};
    COMMTIMEOUTS comTimeouts;
    jboolean isCopy;
    const char *fname = (*env)->GetStringUTFChars(env, sCom, &isCopy);
    
    hCom = CreateFile(fname, 
                      GENERIC_READ | GENERIC_WRITE, // Access = read/write
                      0, // Do not share
                      NULL, // No security attributes
                      OPEN_EXISTING, // Fail if it does not exist
                      0, // Not overlapped
                      NULL); // No template file
    
    if (hCom == INVALID_HANDLE_VALUE) {
        hCom = 0;
        goto finish;
    }
    
    
    if (!GetCommState(hCom, &dcb)) {
        CloseHandle(hCom);
        hCom = 0;
        goto finish;
    }
    
    dcb.DCBlength = sizeof(DCB);
    
    dcb.BaudRate = nBaud;
	dcb.fBinary = TRUE;
	dcb.fParity = TRUE;
	dcb.fOutxCtsFlow = FALSE;
	dcb.fOutxDsrFlow = FALSE;
	dcb.fDtrControl = DTR_CONTROL_ENABLE;
	dcb.fDsrSensitivity = FALSE;
	dcb.fTXContinueOnXoff = TRUE;
	dcb.fOutX = FALSE;
	dcb.fInX = FALSE;
	dcb.fErrorChar = FALSE;
	dcb.fNull = FALSE;
	dcb.fRtsControl = RTS_CONTROL_ENABLE;
	dcb.fAbortOnError = FALSE;
	dcb.ByteSize = nBitsPerByte;  // Number of bits/byte, 4-8 
	dcb.Parity = nParity;
	dcb.StopBits = nStopBits;
	
	if (!SetCommState(hCom, &dcb)) {
        CloseHandle(hCom);
        hCom = 0;
        goto finish;
    }
    
    // Retrieve the time-out parameters for all read/write operations on the port.
    GetCommTimeouts(hCom, &comTimeouts);

    // Change the COMMTIMEOUTS structure settings.
    comTimeouts.ReadIntervalTimeout = MAXDWORD;
    comTimeouts.ReadTotalTimeoutMultiplier = 0;
    comTimeouts.ReadTotalTimeoutConstant = 0;
    comTimeouts.WriteTotalTimeoutMultiplier = 0;
    comTimeouts.WriteTotalTimeoutConstant = 50;

    // Set the time-out parameters for all read/write operations on the port. 
    if (!SetCommTimeouts (hCom, &comTimeouts)) {
        CloseHandle(hCom);
        hCom = 0;
        goto finish;
    }
    
finish:
    (*env)->ReleaseStringUTFChars(env, sCom, fname);
    
    if (hCom == 0) {
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        (*env)->ThrowNew(env, ioException, "Unable to open COM port");
        (*env)->DeleteLocalRef(env, ioException);
    }
    
    return (jint)hCom;
}

JNIEXPORT jint JNICALL Java_com_sixnetio_COM_ComPort_readCom(JNIEnv *env, jclass jClass, jint nHCom) {
    HANDLE hCom = (HANDLE)nHCom;
    jbyteArray jbBuffer;
    DWORD nBytesRead = 0;
    jbyte bRead = 0;
    
    //fprintf(stderr, "Reading from COM port...\n");
    if (!ReadFile(hCom, (LPVOID)&bRead, (DWORD)1, (LPDWORD)&nBytesRead, NULL)) {
        jclass ioException;
        
        //fprintf(stderr, "Failed to read\n");
        
        ioException = (*env)->FindClass(env, "java/io/IOException");
        
        //fprintf(stderr, "ReadFile failed: %d\n", GetLastError());
        
        (*env)->ThrowNew(env, ioException, "ReadFile failed");
        
        (*env)->DeleteLocalRef(env, ioException);
        
        return 0;
    }
    //fprintf(stderr, "Read char %c, value %d\n", (char)bRead, bRead);
    //fprintf(stderr, "Bytes read is %d\n", nBytesRead);
    
    if (nBytesRead == 0) {
        return -1;
    } else {
        return ((jint)bRead) & 0xFF;
    }
}

JNIEXPORT jint JNICALL Java_com_sixnetio_COM_ComPort_readMultiCom(JNIEnv *env, jclass jClass, jint nHCom, jbyteArray jbBuffer) {
    HANDLE hCom = (HANDLE)nHCom;
    jboolean isCopy = JNI_FALSE; // Usually don't care, but it matters here
    jbyte *buffer = (*env)->GetByteArrayElements(env, jbBuffer, &isCopy);
    jint nSize = (*env)->GetArrayLength(env, jbBuffer);
    jint nBytesRead;
    
    if (!ReadFile(hCom, (LPVOID)buffer, (DWORD)nSize, (LPDWORD)&nBytesRead, NULL)) {
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        
        fprintf(stderr, "ReadFile failed: %d\n", GetLastError());
        
        (*env)->ReleaseByteArrayElements(env, jbBuffer, buffer, 0);
        (*env)->ThrowNew(env, ioException, "ReadFile failed");
        
        (*env)->DeleteLocalRef(env, ioException);
        
        return 0;
    }
    
    (*env)->ReleaseByteArrayElements(env, jbBuffer, buffer, 0);
    return nBytesRead;
}

JNIEXPORT void JNICALL Java_com_sixnetio_COM_ComPort_writeCom(JNIEnv *env, jclass jClass, jint nHCom, jbyte bWrite) {
    HANDLE hCom = (HANDLE)nHCom;
    jint nBytesWritten;
    
    if (!WriteFile(hCom, (LPCVOID)&bWrite, (DWORD)1, (LPDWORD)&nBytesWritten, NULL) ||
        nBytesWritten == 0) {
        
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        
        fprintf(stderr, "WriteFile failed: %d\n", GetLastError());
        
        (*env)->ThrowNew(env, ioException, "WriteFile failed");
        
        (*env)->DeleteLocalRef(env, ioException);
        
        return;
    }
}

JNIEXPORT jint JNICALL Java_com_sixnetio_COM_ComPort_writeMultiCom(JNIEnv *env, jclass jClass, jint nHCom, jbyteArray jbBuffer) {
    HANDLE hCom = (HANDLE)nHCom;
    jboolean isCopy;
    jbyte *buffer = (*env)->GetByteArrayElements(env, jbBuffer, &isCopy);
    jint nSize = (*env)->GetArrayLength(env, jbBuffer);
    jint nBytesWritten;
    
    if (!WriteFile(hCom, (LPCVOID)buffer, nSize, (LPDWORD)&nBytesWritten, NULL)) {
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        
        fprintf(stderr, "WriteFile failed: %d\n", GetLastError());
        
        (*env)->ReleaseByteArrayElements(env, jbBuffer, buffer, 0);
        (*env)->ThrowNew(env, ioException, "WriteFile failed");
        
        (*env)->DeleteLocalRef(env, ioException);
        
        return 0;
    }
    
    (*env)->ReleaseByteArrayElements(env, jbBuffer, buffer, 0);
    return nBytesWritten;
}

JNIEXPORT void JNICALL Java_com_sixnetio_COM_ComPort_closeCom(JNIEnv *env, jclass jClass, jint nHCom, jstring path) {
    HANDLE hCom = (HANDLE)nHCom;
    
    CloseHandle(hCom);
}

JNIEXPORT void JNICALL Java_com_sixnetio_COM_ComPort_flushCom(JNIEnv *env, jclass jClass, jint nHCom) {
    HANDLE hCom = (HANDLE)nHCom;
    
    if (!FlushFileBuffers(hCom)) {
        jclass ioException = (*env)->FindClass(env, "java/io/IOException");
        
        fprintf(stderr, "FlushFileBuffers failed: %d\n", GetLastError());
        
        (*env)->ThrowNew(env, ioException, "FlushFileBuffers failed");
        
        (*env)->DeleteLocalRef(env, ioException);
    }
}

JNIEXPORT jobjectArray JNICALL Java_com_sixnetio_COM_ComPort_enumSerialPorts(JNIEnv *env, jclass jClass)
{
    jobjectArray result;
    HKEY hKey;
    DWORD valueCount;
    DWORD maxValueNameLen;
    DWORD maxValueDataLen;
    char errmsg[1024];
    jclass ioException;
    jclass stringClass;
    char *tempNameBuf;
    char *tempDataBuf;
    DWORD i;
    int fail;
    LONG response; // Used to examine error codes
    char **winErrorBuf;

    ioException = (*env)->FindClass(env, "java/io/IOException");
    if ( ! ioException) {
        return NULL;
    }

    stringClass = (*env)->FindClass(env, "java/lang/String");
    if ( ! stringClass) {
        (*env)->DeleteLocalRef(env, ioException);
        return NULL;
    }
                                
    // Open the key (folder)
    response = RegOpenKeyEx(HKEY_LOCAL_MACHINE, // Parent key
                            TEXT("HARDWARE\\DEVICEMAP\\SERIALCOMM"), // Path
                            0, // Reserved
                            KEY_QUERY_VALUE, // Permissions
                            &hKey); // Out: Key

    if (response != ERROR_SUCCESS) {
        FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
                      NULL, // Source module/format string
                      response, // Message ID
                      0, // Language ID
                      (LPTSTR)winErrorBuf, // Output buffer
                      0,
                      NULL);

        sprintf(errmsg, "Unable to open HKLM\\HARDWARE\\DEVICEMAP\\SERIALCOMM: %s", *winErrorBuf);
        LocalFree(winErrorBuf);
        (*env)->ThrowNew(env, ioException, errmsg);
        (*env)->DeleteLocalRef(env, ioException);
        return NULL;
    }

    // Get the number of values within that key
    response = RegQueryInfoKey(hKey, // Key to query
                               NULL, // Key class
                               NULL, // Size of the key class buffer
                               NULL, // Reserved
                               NULL, // Number of subkeys
                               NULL, // Length of the longest subkey's name
                               NULL, // Length of the longest subkey's class
                               &valueCount, // Number of values
                               &maxValueNameLen, // Length of longest value name
                               &maxValueDataLen, // Length of longest value data
                               NULL, // Size of the key's security descriptor
                               NULL); // Last write time

    if (response != ERROR_SUCCESS) {
        FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
                      NULL,
                      response,
                      0,
                      (LPTSTR)winErrorBuf,
                      0,
                      NULL);

        sprintf(errmsg, "Unable to query HKLM\\HARDWARE\\DEVICEMAP\\SERIALCOMM: %s", *winErrorBuf);
        LocalFree(winErrorBuf);
        (*env)->ThrowNew(env, ioException, errmsg);
        (*env)->DeleteLocalRef(env, ioException);
        RegCloseKey(hKey);
        return NULL;
    }

    // Allocate the string array
    result = (*env)->NewObjectArray(env, valueCount, stringClass, NULL);

    // Whether or not that failed, this is no longer needed
    (*env)->DeleteLocalRef(env, stringClass);

    if ( ! result) {
        sprintf(errmsg, "Unable to allocate array");
        (*env)->ThrowNew(env, ioException, errmsg);
        (*env)->DeleteLocalRef(env, ioException);
        RegCloseKey(hKey);
        return NULL;
    }

    tempNameBuf = (char *)malloc((maxValueNameLen + 1) * sizeof(char));
    tempDataBuf = (char *)malloc((maxValueDataLen + 1) * sizeof(char));

    fail = 0;
    for (i = 0; i < valueCount; i++) {
        DWORD valNameSize = maxValueNameLen + 1;
        DWORD type;
        DWORD valDataSize = maxValueDataLen + 1;
        jstring str;
        LONG response;

        response = RegEnumValue(hKey, // Key containing the value
                                i, // Index of the value
                                (LPTSTR)tempNameBuf, // Name of the value
                                &valNameSize, // Size of the name
                                NULL, // Reserved
                                &type, // Value type
                                (LPBYTE)tempDataBuf, // Data
                                &valDataSize); // Data size

        if (response != ERROR_SUCCESS &&
            response != ERROR_NO_MORE_ITEMS) {

            fail = 1;
            FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
                          NULL,
                          response,
                          0,
                          (LPTSTR)winErrorBuf,
                          0,
                          NULL);

            sprintf(errmsg, "Unable to enumerate values: %s", *winErrorBuf);
            LocalFree(winErrorBuf);
            (*env)->ThrowNew(env, ioException, errmsg);
            break;
        }

        if (type != REG_SZ) {
            fail = 1;
            sprintf(errmsg, "Unexpected value type (%lu) while enumerating HKLM\\HARDWARE\\DEVICEMAP\\SERIALCOMM", type);
            (*env)->ThrowNew(env, ioException, errmsg);
            break;
        }

        str = (*env)->NewStringUTF(env, tempDataBuf);
        if (str) {
            (*env)->SetObjectArrayElement(env, result, i, str);
            (*env)->DeleteLocalRef(env, str);
        }
        else {
            fail = 1;
            sprintf(errmsg, "Unable to allocate a String object");
            (*env)->ThrowNew(env, ioException, errmsg);
            break;
        }
    }

    free(tempNameBuf);
    free(tempDataBuf);

    (*env)->DeleteLocalRef(env, ioException);
    RegCloseKey(hKey);

    if (fail) {
        (*env)->DeleteLocalRef(env, result);
        return NULL;
    }
    
    return result;
}


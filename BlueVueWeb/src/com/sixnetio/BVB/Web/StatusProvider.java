/*
 * StatusProvider.java
 *
 * Guarantees that implementors provide "getStatus(Object)".
 *
 * Jonathan Pearson
 * June 17, 2009
 *
 */

package com.sixnetio.BVB.Web;


public interface StatusProvider {
	public static final String S_NORMAL = "normal";
	public static final String S_WARNING = "warning";
	public static final String S_FAILURE = "failure";
	
	/**
	 * Returns a string (one of S_* above) describing the status of the object.
	 * If there are no outstanding status values for this type of object, just
	 * return {@link #S_NORMAL}.
	 * 
	 * @param obj The object to check.
	 * @return {@link #S_NORMAL} if everything is normal with this object;
	 *         {@link #S_WARNING} if the object is in a warning state; or
	 *         {@link #S_FAILURE} if the object is in a failure state.
	 */
	public String getStatus(Object obj) throws Exception;
}

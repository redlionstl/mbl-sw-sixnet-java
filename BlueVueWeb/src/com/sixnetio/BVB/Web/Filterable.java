/*
 * Filterable.java
 *
 * Implementors provide a function that returns filters on displayed data.
 *
 * Jonathan Pearson
 * June 18, 2009
 *
 */

package com.sixnetio.BVB.Web;

import java.util.Map;

public interface Filterable {
	/**
	 * Get the filters in active use on the currently displayed data.
	 * 
	 * @return A map of Title (I18n translated) onto [Filter parameters to get
	 *         to this filter, Filter parameters for all filters except this
	 *         one].
	 * 
	 * @throws Exception If there is a problem building the filter map.
	 */
	public Map<String, String[]> getFilters() throws Exception;
}

/*
 * BatchListAction.java
 *
 * Provides sortable, selectable, filterable, paginated lists of batches, and some actions
 * on the selected batches in those lists (deleting, etc).
 *
 * Jonathan Pearson
 * May 12, 2009
 *
 */

package com.sixnetio.BVB.Web.BatchActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Database.SortColumns.BatchColumn;
import com.sixnetio.BVB.Web.*;
import com.sixnetio.BVB.Web.Iterators.BatchIterable;
import com.sixnetio.util.Utils;

public class BatchListAction
	extends SelectionPreparer<Integer>
	implements BatchStatusProvider, Filterable, LinkTranslator, Sortable {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// Criteria for searching out batches
	public Integer qBatchID = null;
	public Integer qUserID = null;
	public String qName = null;
	public Boolean qDeleted = null;
	
	// If deleting batches, delete the remaining active jobs?
	public boolean deleteAssociatedJobs = true;
	
	// This holds on to jobs that we tried to delete but could not
	public Collection<Long> undeletedJobs;
	
	
	private long matchCount = 0;
	
	// This should only be set once, so no need to synchronize
	private List<BatchColumn> sortOn = null;
	
	
	@Override
	public Map<String, String[]> getFilters() throws Exception {
		// To start, fill this with Title --> Filter, but only individual
		// filters
		LinkedHashMap<String, String> filters = new LinkedHashMap<String, String>();
		
		if (qBatchID != null) {
			filters.put(getText("filter.batchID", Utils.makeLinkedList((Object)qBatchID)),
			            "qBatchID=" + qBatchID);
		}
		
		if (qUserID != null) {
			User user = DB.getDB().getUser(qUserID);
			
			if (user != null) {
				filters.put(getText("filter.user", Utils.makeLinkedList((Object)user.name)),
				            "qUserID=" + qUserID);
			} else {
				filters.put(getText("filter.userID", Utils.makeLinkedList((Object)qUserID)),
				            "qUserID=" + qUserID);
			}
		}
		
		if (qName != null) {
			filters.put(getText("filter.name", Utils.makeLinkedList((Object)qName)), "qName=" +
			                                                                         qName);
		}
		
		if (qDeleted != null) {
			filters.put(getText("filter.deleted", Utils.makeLinkedList((Object)qDeleted)),
			            "qDeleted=" + qDeleted);
		}
		
		return constructFilters(filters);
	}
	
	@Override
	public String getSortOn() {
		return BatchActionLib.translateSort(sortOn);
	}
	
	@Override
	public void setSortOn(String columns) {
		sortOn = BatchActionLib.translateSort(columns);
	}
	
	@Override
	public String addSortColumn(String colName) {
		return BatchActionLib.addSortColumn(colName, sortOn);
	}
	
	/**
	 * The user specified the List action. Loads a list of all batches, based on
	 * the properties that the user provided.
	 * 
	 * @return{@link GeneralAction#LIST}.
	 * @throws Exception If there is a database access error.
	 */
	public String list() throws Exception {
		long[] matchCount = new long[1];
		matchingIDs = new LinkedList<Integer>(DB.getDB()
				.getMatchingBatches(qBatchID, qUserID, qName, qDeleted,
				                    getPageStart(), getPageSize(),
				                    sortOn, matchCount));
		
		this.matchCount = matchCount[0];
		
		if (selectAll) {
			// Can't just copy 'batchIDs', since that only contains the matches
			// for *this* page
			// We need the matches across *all* pages
			selectedIDsOff.addAll(DB.getDB()
			                      .getMatchingBatches(qBatchID, qUserID, qName,
			                                          qDeleted,
			                                          0, -1, // Page start/size
			                                          sortOn, // Sort order
			                                          null)); // Match count
		}
		
		prepareSelectedIDs();
		return LIST;
	}
	
	/**
	 * Standard setup shared by Delete, Undelete, etc actions. Transfers
	 * selected IDs into matching IDs if necessary, selects the first page if
	 * there was no selection provided, populates matchCount, handles
	 * select-all, and prepares selections for display.
	 */
	private void selectionBasedActionSetup() throws Exception {
		if (matchingIDs.size() == 0) {
			if (selectedIDsOff.size() > 0) {
				// This is how IDs are passed in from the List page
				matchingIDs.addAll(selectedIDsOff);
				
				// Also, we must be going to a new view, so go to page 1
				pageStart = 0;
			} else {
    			// Select the first page
    			matchingIDs = new LinkedList<Integer>(DB.getDB()
    					.getMatchingBatches(qBatchID, qUserID, qName,
    					                    qDeleted,
    					                    0, getPageSize(), // Page start/size
    					                    sortOn, null)); // Sort/matches
    			
    			// But don't check their boxes
    			selectedIDsOff.clear();
    			selectAll = false;
			}
		}
		
		this.matchCount = matchingIDs.size();
		
		if (selectAll) {
			selectedIDsOff.addAll(matchingIDs);
		}
		
		prepareSelectedIDs();
	}
	
	/**
	 * The user specified the Delete action. Verifies that the user has batch
	 * delete permission (Enhanced user) and asks for confirmation.
	 * 
	 * @return INPUT
	 * @throws Exception If the user does not have batch delete permission.
	 */
	public String delete() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.deletePermission"));
		
		selectionBasedActionSetup();
		
		return INPUT;
	}
	
	/**
	 * The user confirmed a delete action. Delete the batches.
	 * 
	 * @return {@link GeneralAction#UPLEVEL}, or INPUT if no batch was
	 *         specified.
	 * @throws Exception If the user does not have batch delete permission.
	 */
	public String realDelete() throws Exception {
		verifyPermission(User.UserType.Enhanced,
		                 getText("error.deletePermission"));
		
		if (selectedIDsOff.size() == 0) {
			addActionError(getText("error.deleteNoBatch"));
			
			prepareSelectedIDs();
			return INPUT;
		}
		
		List<LogEntry> logEntries = new LinkedList<LogEntry>();
		
		for (int batchID : selectedIDsOff) {
			DB.getDB().deleteBatch(batchID);
			
			logEntries.add(new LogEntry(new Date(), getUser().userID,
			                            DBLogger.INFO,
			                            String.format("Deleted Batch #%d",
			                                          batchID)));
		}
		
		DB.getDB().log(logEntries);
		
		if (deleteAssociatedJobs) {
			// Pass these off to the Jobs realDelete action
			return "DeleteJobs";
		}
		
		// Redirect action does not need selections prepared
		return LIST;
	}
	
	/**
	 * The user specified the Undelete action. Verifies that the user has batch
	 * undelete permission (Enhanced user) and asks for confirmation.
	 * 
	 * @return INPUT
	 * @throws Exception If the user does not have batch undelete permission.
	 */
	public String undelete() throws Exception {
		verifyPermission(User.UserType.Enhanced,
		                 getText("error.undeletePermission"));
		
		selectionBasedActionSetup();
		
		return INPUT;
	}
	
	/**
	 * The user confirmed an undelete action. Undelete the batches.
	 * 
	 * @return {@link GeneralAction#UPLEVEL}, or INPUT if no batch was
	 *         specified.
	 * @throws Exception If the user does not have batch undelete permission.
	 */
	public String realUndelete() throws Exception {
		verifyPermission(User.UserType.Enhanced,
		                 getText("error.undeletePermission"));
		
		if (selectedIDsOff.size() == 0) {
			addActionError(getText("error.undeleteNoBatch"));
			
			prepareSelectedIDs();
			return INPUT;
		}
		
		List<LogEntry> logEntries = new LinkedList<LogEntry>();
		
		for (int id : selectedIDsOff) {
			JobBatch batch = DB.getDB().getBatch(id);
			batch.deleted = false;
			DB.getDB().updateBatch(batch);
			
			logEntries.add(new LogEntry(new Date(), getUser().userID,
			                            DBLogger.INFO,
			                            String.format("Un-deleted Batch #%d", id)));
		}
		
		DB.getDB().log(logEntries);
		
		// Redirect action does not need to have selections prepared
		return LIST;
	}
	
	@Override
	public long getMatchCount() {
		return matchCount;
	}
	
	@Override
	public String translateLink(String link, Object obj) {
		if (obj == null || !(obj instanceof JobBatch)) return "";
		
		JobBatch batch = (JobBatch)obj;
		String docroot = getDocumentRoot();
		
		if (link.equals("viewBatch")) {
			return String.format("%s/Batch/view?id=%d", docroot, batch.batchID);
			
		} else if (link.equals("viewUser")) {
			if (batch.userID == null) return "";
			
			return String.format("%s/User/view?id=%d", docroot, batch.userID);
			
		} else if (link.equals("filterJobs")) {
			return String.format("%s/Jobs?qBatchID=%d", docroot, batch.batchID);
			
		} else if (link.startsWith("filterBatches/")) {
			String filterName = link.substring(link.indexOf('/') + 1);
			
			Integer qBatchID = this.qBatchID;
			Integer qUserID = this.qUserID;
			String qName = this.qName;
			Boolean qDeleted = this.qDeleted;
			
			if (filterName.equals("batchID")) {
				qBatchID = batch.batchID;
			} else if (filterName.equals("userID")) {
				if (batch.userID == null) {
					qUserID = User.INV_USER;
				} else {
					qUserID = batch.userID;
				}
			} else if (filterName.equals("name")) {
				qName = batch.name;
			} else if (filterName.equals("deleted")) {
				qDeleted = batch.deleted;
			}
			
			StringBuilder builder = new StringBuilder(docroot + "/Batches?");
			
			// A trailing '&' won't hurt anything, so just generate a link with
			// it for simplicity
			
			if (qBatchID != null) {
				builder.append(String.format("qBatchID=%d&", qBatchID));
			}
			
			if (qUserID != null) {
				builder.append(String.format("qUserID=%d&", qUserID));
			}
			
			if (qName != null) {
				builder.append(String.format("qName=%s&", qName));
			}
			
			if (qDeleted != null) {
				builder.append(String.format("qDeleted=%b&", qDeleted));
			}
			
			return builder.toString();
		}
		
		return "";
	}
	
	@Override
    protected Collection<String> getIDParameterNames() {
		// Allow batch IDs to be passed around between pages
	    return Utils.makeArrayList("batchID");
    }
	
	@Override
    protected Collection<String> getSelectedParameterNames() {
		// Allow batch IDs to be passed in
	    return Utils.makeArrayList("selBatchID");
    }
	
	@Override
    protected Collection<Integer> parseID(String type, String id)
    	throws NumberFormatException, DatabaseException, Exception {
	    
		logger.debug("Parsing '" + id + "' as a '" + type + "'");
		
		if (type.equals("batchID") || type.equals("selBatchID")) {
			// Select the batch specified
			logger.debug("It was a Batch ID, parsing as an Integer");
			
			return Utils.makeArrayList(Integer.parseInt(id));
			
		} else {
			// Not recognized, must have added something and not updated this
			// method
			throw new IllegalArgumentException("Unrecognized type: " + type);
		}
    }
	
	@Override
    public Iterable<?> getItems() throws Exception {
		return new BatchIterable(getPageIDs());
    }

	@Override
    public User getBatchUser(JobBatch batch) throws Exception {
	    return BatchActionLib.getBatchUser(batch);
    }

	@Override
    public String getStatus(Object obj) throws Exception {
	    return BatchActionLib.getBatchStatus((JobBatch)obj);
    }

	@Override
	public long getBatchFailures(JobBatch batch) throws Exception {
		return BatchActionLib.getBatchFailures(batch);
	}

	@Override
	public long getBatchSuccesses(JobBatch batch) throws Exception {
		return BatchActionLib.getBatchSuccesses(batch);
	}
}

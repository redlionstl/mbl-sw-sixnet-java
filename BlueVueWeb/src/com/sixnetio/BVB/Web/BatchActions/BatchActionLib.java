/*
 * BatchActionLib.java
 *
 * Provides some library functions common to batch actions.
 *
 * Jonathan Pearson
 * May 12, 2009
 *
 */

package com.sixnetio.BVB.Web.BatchActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.SortColumns.BatchColumn;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.StatusProvider;
import com.sixnetio.BVB.Web.Iterators.HistoryIterable;
import com.sixnetio.util.Utils;

public class BatchActionLib {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Translate a string describing sorting columns into a list of
	 * BatchColumns.
	 * 
	 * @param columns The description of the sorting columns.
	 * @return A list that could be passed to
	 *         {@link Database#getMatchingBatches(Integer, Integer, String, Boolean, int, int, List, long[])}
	 *         .
	 */
	public static List<BatchColumn> translateSort(String columns) {
		String[] cols = Utils.tokenize(columns, ",", false);
		List<BatchColumn> sortOn = new Vector<BatchColumn>(cols.length);
		
		for (String column : cols) {
			if (column.startsWith("!")) {
				sortOn.add(BatchColumn.Reverse);
				column = column.substring(1);
			}
			
			sortOn.add(BatchColumn.valueOf(column));
		}
		
		return sortOn;
	}
	
	/**
	 * Translate a sort list into a string.
	 * 
	 * @param sortOn The sort list to translate. If synchronization is
	 *            necessary, it must be handled by the caller.
	 * @return A descriptive string.
	 */
	public static String translateSort(List<BatchColumn> sortOn) {
		if (sortOn == null) return "";
		
		StringBuilder builder = new StringBuilder();
		
		boolean skipComma = true;
		for (BatchColumn column : sortOn) {
			if (skipComma) {
				skipComma = false;
			} else {
				builder.append(",");
			}
			
			if (column == BatchColumn.Reverse) {
				builder.append("!");
				skipComma = true;
			} else {
				builder.append(column.name());
			}
		}
		
		return builder.toString();
	}
	
	/**
	 * Given an existing sort list, add a new primary column to it, shuffling
	 * the existing columns as necessary.
	 * 
	 * @param colName The new column to add.
	 * @param oldSortOn The existing sort list, or <tt>null</tt> if there wasn't
	 *            one. If synchronization is necessary, it must be handled by
	 *            the caller.
	 * @return A new sort list. If the new column was already the primary
	 *         column, reverses the sorting order. If it was already in the
	 *         list, moves it to the beginning in normal order. If it did not
	 *         exist, adds it at the beginning in normal order.
	 */
	public static String addSortColumn(String colName, List<BatchColumn> oldSortOn) {
		BatchColumn col;
		
		try {
			col = BatchColumn.valueOf(colName);
		} catch (IllegalArgumentException iae) {
			logger.debug("Not a legal column name: '" + colName + "'", iae);
			
			return translateSort(oldSortOn);
		}
		
		// Copy the sortOn object, since we don't actually want to change it
		List<BatchColumn> sortOn;
		
		if (oldSortOn == null) {
			// It's only going to receive one item
			sortOn = new ArrayList<BatchColumn>(1);
		} else {
			sortOn = new ArrayList<BatchColumn>(oldSortOn);
		}
		
		// Get the index of the column, to see if it's already part of the sort
		// order
		int index = sortOn.indexOf(col);
		
		// Whether to apply a 'Reverse' just before the column
		boolean reverse = false;
		
		// If the column was already in there...
		if (index != -1) {
			// Remove it
			sortOn.remove(index);
			
			// If there was a reverse just before it...
			if (index > 0 && sortOn.get(index - 1) == BatchColumn.Reverse) {
				sortOn.remove(index - 1);
				
				reverse = true;
				index--; // So we can more easily tell if it was the first
				// column
			}
			
			// If it was the first sort column
			if (index == 0) {
				// Swap the reverse property
				reverse = !reverse;
			}
		}
		
		// Push the column onto the front of the sort order
		sortOn.add(0, col);
		
		// If reversing, push reverse onto the front also
		if (reverse) {
			sortOn.add(0, BatchColumn.Reverse);
		}
		
		// Return the new sorting order
		return translateSort(sortOn);
	}
	
	public static User getBatchUser(JobBatch batch) throws Exception {
		if (batch.userID == null) return null;
		
		return DB.getDB().getUser(batch.userID);
	}
	
	/**
	 * Get the status of the given batch.
	 * 
	 * @param batch The batch.
	 * @return The status of the batch.
	 */
	public static String getBatchStatus(JobBatch batch) {
		// Batches are always 'normal'
		return StatusProvider.S_NORMAL;
	}
	
	/**
	 * Get the number of job-terminating failures for the given batch.
	 * 
	 * @param batch The batch.
	 * @return The number of job-terminating failures.
	 * @throws Exception If there was a problem retrieving the information.
	 */
	public static long getBatchFailures(JobBatch batch)
			throws Exception {
		
		// Need to do this manually, unless we augment the database interface
		Collection<Long> historyIDs =
			DB.getDB().getMatchingHistory(null, null, batch.batchID, null, null,
			                              null, null, null, null, 0, -1,
			                              null, null);
		
		// Keep track of the jobs that we've seen, to avoid duplicates
		Set<Long> allJobs = new HashSet<Long>();
		
		// Also, keep track of the successful jobs, to remove them at the end
		// Can't only look at failures, since a job may fail any number of times
		// before succeeding
		Set<Long> successes = new HashSet<Long>();
		
		// Get a list of unique job IDs (since each job may have multiple
		// associated history entries)
		HistoryIterable historyIterable = new HistoryIterable(historyIDs);
		for (HistoryEntry entry : historyIterable) {
			// Is it from a job that we've seen already?
			allJobs.add(entry.jobID);
			
			// Was it a success?
			if (entry.result.equals("Success")) successes.add(entry.jobID);
		}
		
		// Subtract the successes from the jobs
		allJobs.removeAll(successes);
		
		// The remaining jobs have failed one or more times but have not
		// succeeded
		// However, they may not have terminated either, so we need to check for
		// that
		Map<Long, Job> existingJobs = DB.getDB().getJobs(allJobs, true);
		
		// Whatever came from that still exists and should not count
		// Return the number of jobs that did not come back
		return (allJobs.size() - existingJobs.size());
	}
	
	/**
	 * Get the number of job successes for the given batch.
	 * 
	 * @param batch The batch.
	 * @return The number of job successes.
	 * @throws Exception If there was a problem retrieving the information.
	 */
	public static long getBatchSuccesses(JobBatch batch)
			throws Exception {
		
		long[] matchCount = new long[1];
	    DB.getDB().getMatchingHistory(null, null, batch.batchID, null, null,
	                                  null, "Success", null, null, 0, 1, null,
	                                  matchCount);
	    
	    return matchCount[0];
	}
}

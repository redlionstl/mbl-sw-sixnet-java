/*
 * BatchStatusProvider.java
 *
 * Provides information about a batch.
 *
 * Jonathan Pearson
 * May 12, 2009
 *
 */

package com.sixnetio.BVB.Web.BatchActions;

import com.sixnetio.BVB.Common.JobBatch;
import com.sixnetio.BVB.Common.User;
import com.sixnetio.BVB.Web.StatusProvider;

public interface BatchStatusProvider extends StatusProvider {
	/**
	 * Get the user for the given batch.
	 * 
	 * @param batch The batch.
	 * @return The user that created the batch, or <tt>null</tt> if there isn't
	 *         one.
	 * @throws Exception If there is a problem retrieving the user.
	 */
	public User getBatchUser(JobBatch batch) throws Exception;
	
	/**
	 * Get the number of job successes for the given batch.
	 * 
	 * @param batch The batch.
	 * @return The number of job successes.
	 * @throws Exception If there is a problem retrieving the information.
	 */
	public long getBatchSuccesses(JobBatch batch) throws Exception;
	
	/**
	 * Get the number of job-terminating failures for the given batch.
	 * 
	 * @param batch The batch.
	 * @return The number of failures for which the job no longer exists.
	 * @throws Exception If there is a problem retrieving the information.
	 */
	public long getBatchFailures(JobBatch batch) throws Exception;
}

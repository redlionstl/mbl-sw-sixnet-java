/*
 * BatchAction.java
 *
 * Loads a batch from the database and provides CRUD actions on it.
 *
 * Jonathan Pearson
 * June 2, 2009
 *
 */

package com.sixnetio.BVB.Web.BatchActions;

import java.util.Date;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class BatchAction extends GeneralAction implements BatchStatusProvider, Preparable {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// The ID of the batch that the user specified
	private int id = JobBatch.INV_BATCH;
	
	// The actual batch, created here or loaded from the database (for
	// single-batch actions)
	private JobBatch batch;
	
	/**
	 * Get the ID number of the batch that the user specified, or
	 * {@link JobBatch#INV_BATCH} if no batch was specified.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Get the batch.
	 */
	public JobBatch getBatch() {
		return batch;
	}
	
	/**
	 * The user specified the View action.
	 * 
	 * @return {@link GeneralAction#VIEW}.
	 * @throws Exception If the user is not logged in.
	 */
	public String view() throws Exception {
		verifyUserLoggedIn();
		
		return VIEW;
	}
	
	/**
	 * The user specified the Edit action. Verifies that the user has batch edit
	 * permission (Enhanced user) and asks for more input.
	 * 
	 * @return INPUT.
	 * @throws Exception If the user does not have batch edit permission.
	 */
	public String edit() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.updatePermission"));
		
		return INPUT;
	}
	
	/**
	 * The user has finished editing a batch. Verifies that the user has batch
	 * edit permission (Enhanced user), saves the batch, and redirects to the
	 * View page.
	 * 
	 * @return {@link GeneralAction#VIEW}.
	 * @throws Exception If the user does not have batch edit permission, or
	 *             there is a database exception trying to save the batch.
	 */
	public String update() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.updatePermission"));
		
		// Make sure there's a batch name
		if (prepareParam(batch.name) == null) {
			addActionError(getText("error.emptyBatchName"));
			return INPUT;
		}
		
		if (id == JobBatch.INV_BATCH) {
			addActionError(getText("error.updateNoBatch"));
			return INPUT;
		} else {
			DB.getDB().updateBatch(batch);
			
			LogEntry entry =
			        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
			                     String.format("Updated Batch #%d", id));
			DB.getDB().log(Utils.makeLinkedList(entry));
		}
		
		return VIEW;
	}
	
	@Override
	public void prepare() throws Exception {
		String sID = getRequest().getParameter("id");
		
		if (sID != null && !sID.equals("-1")) {
			try {
				id = Integer.parseInt(sID);
				
				logger.debug("User specified batch with ID " + id);
				
				batch = DB.getDB().getBatch(id);
			} catch (NumberFormatException nfe) {
				throw new Exception(getText("error.invalidBatchID", new String[] {
					sID
				}), nfe);
			}
		} else {
			logger.debug("No batch specified, cannot create a new one");
			
			addActionError(getText("error.noBatch"));
		}
	}
	
	@Override
	public User getBatchUser(JobBatch batch) throws Exception {
		return BatchActionLib.getBatchUser(batch);
	}
	
	@Override
	public String getStatus(Object obj) {
		return S_NORMAL;
	}
	
	@Override
	public long getBatchFailures(JobBatch batch) throws Exception {
	    return BatchActionLib.getBatchFailures(batch);
	}
	
	@Override
	public long getBatchSuccesses(JobBatch batch) throws Exception {
	    return BatchActionLib.getBatchSuccesses(batch);
    }
}

/*
 * PackageActionLib.java
 *
 * Provides some library functions common to package actions.
 *
 * Jonathan Pearson
 * May 28, 2009
 *
 */

package com.sixnetio.BVB.Web.PackageActions;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.BlueTreeImage;
import com.sixnetio.BVB.Web.StatusProvider;
import com.sixnetio.util.Utils;

public class PackageActionLib {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Get the status of the given package.
	 * 
	 * @param pkg The package.
	 * @return The status of the package.
	 */
	public static String getPackageStatus(BlueTreeImage pkg) {
		// Packages are always 'normal'
		return StatusProvider.S_NORMAL;
	}
}

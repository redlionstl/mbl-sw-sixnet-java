/*
 * PackageStatusProvider.java
 *
 * Provides information about a package.
 *
 * Jonathan Pearson
 * May 28, 2009
 *
 */

package com.sixnetio.BVB.Web.PackageActions;

import com.sixnetio.BVB.Web.StatusProvider;

public interface PackageStatusProvider extends StatusProvider {

}

/*
 * PackageAction.java
 *
 * Loads a package from the database and provides CRUD actions on it.
 *
 * Jonathan Pearson
 * June 4, 2009
 *
 */

package com.sixnetio.BVB.Web.PackageActions;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.util.Date;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class PackageAction extends GeneralAction implements PackageStatusProvider, Preparable {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// The hash of the package that the user specified
	private BigInteger id = BigInteger.ZERO;
	
	// The actual package, created here or loaded from the database (for
	// single-package actions)
	private BlueTreeImage pkg;
	
	// For uploading new files
	public File imageFile;
	
	// Content-Type of the image file (not used)
	public String imageFileContentType;
	
	/**
	 * Get the hash of the package that the user specified, or
	 * {@link BigInteger#ZERO} if no hash was specified.
	 */
	public BigInteger getId() {
		return id;
	}
	
	/**
	 * Get the package.
	 */
	public BlueTreeImage getPackage() {
		return pkg;
	}
	
	/**
	 * The user specified the View action.
	 * 
	 * @return {@link GeneralAction#VIEW}.
	 * @throws Exception If the user is not logged in.
	 */
	public String view() throws Exception {
		verifyUserLoggedIn();
		
		return VIEW;
	}
	
	/**
	 * The user specified the Create action.
	 * 
	 * @return INPUT
	 * @throws Exception If the user does not have package creation permission
	 *             (Enhanced user).
	 */
	public String create() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.createPermission"));
		
		return INPUT;
	}
	
	/**
	 * The user specified the Update action (used to upload packages).
	 * 
	 * @return VIEW, or INPUT if there was a problem.
	 * @throws Exception If the user does not have package creation permission
	 *             (Enhanced user).
	 */
	public String update() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.createPermission"));
		
		// Make sure the user provided a file
		if (imageFile == null) {
			logger.info("imageFile == null");
			
			addActionError(getText("error.noFile"));
			return INPUT;
		} else if (!imageFile.isFile()) {
			logger.info("imageFile.isFile() == false");
			
			addActionError(getText("error.notAFile"));
			return INPUT;
		} else if (!imageFile.canRead()) {
			logger.info("imageFile.canRead() == false");
			
			addActionError(getText("error.cannotRead"));
			return INPUT;
		}
		
		// Read the file
		FileInputStream fin = new FileInputStream(imageFile);
		
		try {
			byte[] data = Utils.bytesToEOF(fin);
			pkg = new BlueTreeImage(data);
		} finally {
			fin.close();
		}
		
		// Save the package
		DB.getDB().createPackage(pkg);
		
		LogEntry logEntry =
		        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
		                     String.format("Uploaded Package #%x", pkg.getPackageID()));
		DB.getDB().log(Utils.makeLinkedList(logEntry));
		
		// Update the ID for proper viewing
		id = pkg.getPackageID();
		
		return VIEW;
	}
	
	@Override
	public void prepare() throws Exception {
		String sID = getRequest().getParameter("id");
		
		if (sID != null && !sID.equals("0")) {
			try {
				id = new BigInteger(sID, 16);
				
				logger.debug("User specified package with ID " + id.toString(16));
				
				pkg = DB.getDB().getPackage(id.toString(16), true);
			} catch (NumberFormatException nfe) {
				throw new Exception(getText("error.invalidPackageID", new String[] {
					sID
				}), nfe);
			}
		} else {
			logger.debug("No package specified");
		}
	}
	
	@Override
	public String getStatus(Object obj) {
		return S_NORMAL;
	}
}

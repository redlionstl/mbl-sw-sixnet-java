/*
 * PackageListAction.java
 *
 * Provides sortable, selectable, filterable, paginated lists of packages, and some actions
 * on the selected packages in those lists (deleting, etc).
 *
 * Jonathan Pearson
 * May 28, 2009
 *
 */

package com.sixnetio.BVB.Web.PackageActions;

import java.math.BigInteger;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Web.*;
import com.sixnetio.BVB.Web.Iterators.PackageIterable;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class PackageListAction
	extends SelectionPreparer<BigInteger>
	implements Filterable, LinkTranslator, PackageStatusProvider {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// Criteria for searching out packages
	public String qType = null;
	public Version qLowVersion = null;
	public Version qHighVersion = null;
	public Date qLowDate = null;
	public Date qHighDate = null;
	public String qChecksum = null;
	
	private long matchCount = 0;
	
	@Override
	public Map<String, String[]> getFilters() throws Exception {
		// To start, fill this with Title --> Filter, but only individual
		// filters
		LinkedHashMap<String, String> filters = new LinkedHashMap<String, String>();
		
		if (qType != null) {
			filters.put(getText("filter.type", Utils.makeLinkedList((Object)qType)), "qType=" +
			                                                                         qType);
		}
		
		if (qLowVersion != null) {
			filters.put(getText("filter.lowVersion", Utils.makeLinkedList((Object)qLowVersion)),
			            "qLowVersion=" + qLowVersion);
		}
		
		if (qHighVersion != null) {
			filters.put(getText("filter.highVersion", Utils.makeLinkedList((Object)qHighVersion)),
			            "qHighVersion=" + qHighVersion);
		}
		
		if (qLowDate != null) {
			String formatted = formatData("format.date", qLowDate);
			filters.put(getText("filter.lowDate", Utils.makeLinkedList((Object)formatted)),
			            "qLowDate=" + formatted);
		}
		
		if (qHighDate != null) {
			String formatted = formatData("format.date", qHighDate);
			filters.put(getText("filter.highDate", Utils.makeLinkedList((Object)formatted)),
			            "qHighDate" + formatted);
		}
		
		if (qChecksum != null) {
			filters.put(getText("filter.checksum", Utils.makeLinkedList((Object)qChecksum)),
			            "qChecksum=" + qChecksum);
		}
		
		return constructFilters(filters);
	}
	
	/**
	 * The user specified the List action. Loads a list of all packages, based
	 * on the properties that the user provided.
	 * 
	 * @return {@link GeneralAction#LIST}.
	 * @throws Exception If there is a database access error.
	 */
	public String list() throws Exception {
		long[] matchCount = new long[1];
		Collection<String> packageIDs = DB.getDB()
			.getMatchingPackages(qType, qLowVersion, qHighVersion, qLowDate,
			                     qHighDate, qChecksum,
			                     getPageStart(), getPageSize(), matchCount);
		
		matchingIDs = new LinkedList<BigInteger>();
		for (String hash : packageIDs) {
			matchingIDs.add(new BigInteger(hash, 16));
		}
		
		this.matchCount = matchCount[0];
		
		if (selectAll) {
			// Can't just copy 'packageIDs', since that only contains the
			// matches for *this* page
			// We need the matches across *all* pages
			Collection<String> selectedPackageIDs = DB.getDB()
				.getMatchingPackages(qType, qLowVersion, qHighVersion, qLowDate,
				                     qHighDate, qChecksum,
				                     0, -1, // Page start/size
				                     null); // Match count
			
			for (String hash : selectedPackageIDs) {
				selectedIDsOff.add(new BigInteger(hash, 16));
			}
		}
		
		prepareSelectedIDs();
		return LIST;
	}
	
	/**
	 * The user specified the Delete action. Verifies that the user has package
	 * delete permission (Enhanced user) and asks for confirmation.
	 * 
	 * @return INPUT
	 * @throws Exception If the user does not have package delete permission.
	 */
	public String delete() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.deletePermission"));
		
		if (matchingIDs.size() == 0) {
			if (selectedIDsOff.size() > 0) {
				// This is how IDs are passed in from the List page
				matchingIDs.addAll(selectedIDsOff);
				
				// Also, we must be going to a new view, so go to page 1
				pageStart = 0;
			} else {
    			// Select the first page
				Collection<String> packageIDs = DB.getDB()
					.getMatchingPackages(qType, qLowVersion, qHighVersion,
					                     qLowDate, qHighDate, qChecksum,
					                     0, getPageSize(), null);
				
    			matchingIDs = new LinkedList<BigInteger>();
    			for (String hash : packageIDs) {
    				matchingIDs.add(new BigInteger(hash, 16));
    			}
    			
    			// But don't check their boxes
    			selectedIDsOff.clear();
    			selectAll = false;
			}
		}
		
		this.matchCount = matchingIDs.size();
		
		if (selectAll) {
			selectedIDsOff.addAll(matchingIDs);
		}
		
		prepareSelectedIDs();
		return INPUT;
	}
	
	/**
	 * The user confirmed a delete action. Delete the packages.
	 * 
	 * @return {@link GeneralAction#UPLEVEL}, or ERROR if no package was
	 *         specified.
	 * @throws Exception If the user does not have package delete permission.
	 */
	public String realDelete() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.deletePermission"));
		
		if (selectedIDsOff.size() == 0) {
			addActionError(getText("error.deleteNoPackage"));
			
			prepareSelectedIDs();
			return INPUT;
		}
		
		List<LogEntry> logEntries = new LinkedList<LogEntry>();
		
		for (BigInteger id : selectedIDsOff) {
			DB.getDB().deletePackage(id.toString(16));
			
			logEntries.add(new LogEntry(new Date(), getUser().userID,
			                            DBLogger.INFO,
			                            String.format("Deleted Package #%x",
			                                          id)));
		}
		
		DB.getDB().log(logEntries);
		
		// Redirect action, no need to prepare selections
		return LIST;
	}
	
	@Override
	public long getMatchCount() {
		return matchCount;
	}
	
	@Override
	public String translateLink(String link, Object obj) throws Exception {
		if (obj == null || !(obj instanceof BlueTreeImage)) return "";
		
		BlueTreeImage pkg = (BlueTreeImage)obj;
		String docroot = getDocumentRoot();
		
		if (link.equals("viewPackage")) {
			return String.format("%s/Package/view?id=%s", docroot, pkg.getChecksum());
			
		} else if (link.startsWith("filterPackages/")) {
			String filterName = link.substring(link.indexOf('/') + 1);
			
			String qType = this.qType;
			Version qLowVersion = this.qLowVersion;
			Version qHighVersion = this.qHighVersion;
			Date qLowDate = this.qLowDate;
			Date qHighDate = this.qHighDate;
			String qChecksum = this.qChecksum;
			
			if (filterName.equals("type")) {
				qType = pkg.getType().typeString;
				
			} else if (filterName.equals("version")) {
				qLowVersion = pkg.getVersion();
				qHighVersion = pkg.getVersion();
				
			} else if (filterName.equals("date")) {
				qLowDate = pkg.getDate();
				qHighDate = pkg.getDate();
				
			} else if (filterName.equals("checksum")) {
				qChecksum = pkg.getChecksum();
			}
			
			StringBuilder builder = new StringBuilder(docroot + "/Packages?");
			
			// A trailing '&' won't hurt anything, so just generate a link with
			// it for simplicity
			
			if (qType != null) {
				builder.append(String.format("qType=%s&", qType));
			}
			
			if (qLowVersion != null) {
				builder.append(String.format("qLowVersion=%s&",
				                             qLowVersion.toString()));
			}
			
			if (qHighVersion != null) {
				builder.append(String.format("qHighVersion=%s&",
				                             qHighVersion.toString()));
			}
			
			if (qLowDate != null) {
				builder.append(String.format("qLowDate=%s&",
				                             formatData("format.date",
				                                        qLowDate)));
			}
			
			if (qHighDate != null) {
				builder.append(String.format("qHighDate=%s&",
				                             formatData("format.date",
				                                        qHighDate)));
			}
			
			if (qChecksum != null) {
				builder.append(String.format("qChecksum=%s&", qChecksum));
			}
			
			return builder.toString();
		}
		
		return "";
	}
	
	@Override
    protected Collection<String> getIDParameterNames() {
	    // Allow package IDs to be passed around
		return Utils.makeArrayList("packageID");
    }
	
	@Override
    protected Collection<String> getSelectedParameterNames() {
	    // Allow package IDs to be passed in/selected
		return Utils.makeArrayList("selPackageID");
    }
	
	@Override
    protected Collection<BigInteger> parseID(String type, String id)
    	throws NumberFormatException, DatabaseException, Exception {
	    
		if (type.equals("packageID") || type.equals("selPackageID")) {
			// We do not control the output format of the hashes, and the
			// default is to output base-10, so we read base-10 here
			return Utils.makeArrayList(new BigInteger(id));
		} else {
			throw new IllegalArgumentException("Unrecognized type: " + type);
		}
    }
	
	@Override
    public Iterable<?> getItems() throws Exception {
	    return new PackageIterable(getPageIDs());
    }

	@Override
    public String getStatus(Object obj) throws Exception {
	    return PackageActionLib.getPackageStatus((BlueTreeImage)obj);
    }
}

/*
 * ServerListAction.java
 *
 * Lists servers.
 *
 * Jonathan Pearson
 * June 9, 2009
 *
 */

package com.sixnetio.BVB.Web.ServerActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class ServerListAction extends GeneralAction implements Preparable {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// Disassemble the capabilities so we can sort the individual pieces
	// These are all used in a single-threaded manner
	private List<String> serverNames;
	private Map<String, List<String>> jobs;
	private Map<String, List<String>> modems;
	private Map<String, List<String>> databases;
	
	// Populated with the current shutdown orders (likely null)
	public String shutdownOrders;
	
	/**
	 * Get the current capabilities.
	 */
	public List<String> getServers() {
		return serverNames;
	}
	
	public List<String> getJobs(String server) {
		return jobs.get(server);
	}
	
	public List<String> getModems(String server) {
		return modems.get(server);
	}
	
	public List<String> getDatabases(String server) {
		return databases.get(server);
	}
	
	/**
	 * List the active servers.
	 * 
	 * @return LIST.
	 * @throws Exception If the user does not have permission to view servers
	 *             (Root user).
	 */
	public String list() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.viewServersPermission"));
		
		return LIST;
	}
	
	/**
	 * Asks for permission to shutdown all servers.
	 * 
	 * @return INPUT.
	 * @throws Exception If the user does not have permission to shutdown
	 *             servers (Root user).
	 */
	public String shutdownAll() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.shutdownServerPermission"));
		
		return INPUT;
	}
	
	/**
	 * Really shutdown all servers.
	 * 
	 * @return LIST.
	 * @throws Exception If the user does not have permission to shutdown
	 *             servers (Root user) or there is a database access error.
	 */
	public String realShutdownAll() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.shutdownServerPermission"));
		
		DB.getDB().updateSetting(Settings.Setting.ServerShutdown.properName, "ALL");
		
		LogEntry logEntry =
		        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
		                     "Instructed ALL servers to shut down");
		DB.getDB().log(Utils.makeLinkedList(logEntry));
		
		return LIST;
	}
	
	/**
	 * Asks for permission to clear server shutdown orders.
	 * 
	 * @return INPUT.
	 * @throws Exception If the user does not have permission to clear server
	 *             shutdown orders (Root user).
	 */
	public String clearShutdown() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.clearShutdownPermission"));
		
		return INPUT;
	}
	
	/**
	 * Actually clear server shutdown orders.
	 * 
	 * @return LIST.
	 * @throws Exception If the user does not have permission to clear server
	 *             shutdown orders (Root user), or there is a database access
	 *             error.
	 */
	public String realClearShutdown() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.clearShutdownPermission"));
		
		DB.getDB().updateSetting(Settings.Setting.ServerShutdown.properName, null);
		
		LogEntry logEntry =
		        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
		                     "Cleared all server shutdown orders");
		DB.getDB().log(Utils.makeLinkedList(logEntry));
		
		return LIST;
	}
	
	@Override
	public void prepare() throws Exception {
		// Load all capabilities from the database
		Collection<Capabilities> caps = DB.getDB().getCurrentCapabilities();
		
		// Disassemble the capabilities
		Capabilities[] array = caps.toArray(new Capabilities[caps.size()]);
		
		// Server names
		String[] serverNames = new String[array.length];
		for (int i = 0; i < array.length; i++) {
			serverNames[i] = array[i].getServerName();
		}
		Arrays.sort(serverNames);
		
		this.serverNames = Utils.makeLinkedList(serverNames);
		
		// Everything else
		this.jobs = new HashMap<String, List<String>>();
		this.modems = new HashMap<String, List<String>>();
		this.databases = new HashMap<String, List<String>>();
		for (Capabilities cap : array) {
			String[] temp;
			
			temp = cap.getJobs().toArray(new String[0]);
			Arrays.sort(temp);
			this.jobs.put(cap.getServerName(), Utils.makeLinkedList(temp));
			
			temp = cap.getModems().toArray(new String[0]);
			Arrays.sort(temp);
			this.modems.put(cap.getServerName(), Utils.makeLinkedList(temp));
			
			temp = cap.getDatabases().toArray(new String[0]);
			Arrays.sort(temp);
			this.databases.put(cap.getServerName(), Utils.makeLinkedList(temp));
		}
		
		Settings shutdownOrders =
		        new Settings(DB.getDB(),
		                     Utils.makeLinkedList(Settings.Setting.ServerShutdown.properName));
		this.shutdownOrders = shutdownOrders.getStringValue(Settings.Setting.ServerShutdown);
	}
}

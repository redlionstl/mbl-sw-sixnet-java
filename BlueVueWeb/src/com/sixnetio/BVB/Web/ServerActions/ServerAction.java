/*
 * ServerAction.java
 *
 * Provides actions on a single server.
 *
 * Jonathan Pearson
 * June 9, 2009
 *
 */

package com.sixnetio.BVB.Web.ServerActions;

import java.util.Date;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class ServerAction extends GeneralAction {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public String serverName;
	
	/**
	 * Asks for permission to shutdown the server.
	 * 
	 * @return INPUT.
	 * @throws Exception If the user does not have server shutdown permission
	 *             (Root user).
	 */
	public String shutdown() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.shutdownServerPermission"));
		
		if (serverName == null) {
			addActionError(getText("error.shutdownNoServer"));
			return INPUT;
		}
		
		return INPUT;
	}
	
	/**
	 * Actually tells the server to shut down.
	 * 
	 * @return LIST, or INPUT if no server was specified.
	 * @throws Exception If the user does not have permission to shut down
	 *             servers (Root user), or there was a database error.
	 */
	public String realShutdown() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.shutdownServerPermission"));
		
		if (serverName == null) {
			addActionError(getText("error.shutdownNoServer"));
			return INPUT;
		}
		
		DB.getDB().updateSetting(Settings.Setting.ServerShutdown.properName, serverName);
		
		LogEntry logEntry =
		        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
		                     String.format("Initiated shutdown of Server '%s'", serverName));
		DB.getDB().log(Utils.makeLinkedList(logEntry));
		
		return LIST;
	}
}

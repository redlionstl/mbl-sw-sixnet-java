/*
 * DefaultPreferences.java
 *
 * Provides default preferences for users.
 *
 * Jonathan Pearson
 * May 5, 2009
 *
 */

package com.sixnetio.BVB.Web;

import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Web.Iterators.ArrayIterable;
import com.sixnetio.util.Utils;

public class Preferences {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private static Tag defaultPrefs;
	private static long timestamp = 0;
	
	private static void loadPrefs() {
		// Already loaded?
		if (defaultPrefs != null) return;
		
		// Load up the allprefs file
		InputStream in = Preferences.class.getResourceAsStream("allprefs.xml");
		
		if (in == null) {
			defaultPrefs = new Tag("userprefs");
			
			logger.error("Unable to open default preferences file");
			return;
		}
		
		Tag allPrefs;
		try {
			try {
				allPrefs = Utils.xmlParser.parseXML(in, false, false);
			} finally {
				in.close();
			}
		} catch (IOException ioe) {
			defaultPrefs = new Tag("userprefs");
			
			logger.error("Unable to read default preferences file", ioe);
			return;
		}
		
		// Convert from allprefs format to userprefs format
		defaultPrefs = new Tag("userprefs");
		
		// Loop through the sections
		for (Tag allSectionTag : allPrefs.getEachTag("", "section")) {
			Tag userSectionTag;
			if (allSectionTag.getStringAttribute("name").equals("[general]")) {
				// Root section doesn't get a separate tag
				userSectionTag = defaultPrefs;
			} else {
				// Others use the section name for the tag name
				userSectionTag = new Tag(allSectionTag.getStringAttribute("name"));
				defaultPrefs.addContent(userSectionTag);
			}
			
			logger.debug("Examining section " + allSectionTag.getStringAttribute("name"));
			
			// Grab preferences
			Tag allPrefsTag = allSectionTag.getTag("prefs");
			if (allPrefsTag != null) {
				for (Tag allPrefTag : allPrefsTag.getEachTag("", "pref")) {
					Tag userPrefTag = new Tag(allPrefTag.getStringContents());
					userPrefTag.addContent(allPrefTag.getStringAttribute("default"));
					
					userSectionTag.addContent(userPrefTag);
					
					if (logger.isDebugEnabled()) {
						logger.debug("Preference: " + userPrefTag.toString(true));
					}
				}
			}
			
			// Grab columns
			Tag allColumnsTag = allSectionTag.getTag("columns");
			if (allColumnsTag != null) {
				Tag userColumnsTag = new Tag("columns");
				userSectionTag.addContent(userColumnsTag);
				
				for (Tag allColumnTag : allColumnsTag.getEachTag("", "column")) {
					if (!allColumnTag.getBooleanAttribute("default")) continue;
					
					Tag userColumnTag = new Tag("column");
					userColumnTag.setAttribute("title", allColumnTag.getStringAttribute("title"));
					userColumnTag.setAttribute("id", allColumnTag.getStringAttribute("id"));
					
					if (allColumnTag.attExists("sort")) {
						userColumnTag.setAttribute("sort", allColumnTag.getStringAttribute("sort"));
					}
					
					userColumnTag.setAttribute("format", allColumnTag.getStringAttribute("format"));
					userColumnTag.setAttribute("type", allColumnTag.getStringAttribute("type"));
					
					userColumnTag.addContent(allColumnTag.getStringContents("value"));
					
					// Search out the default link
					if (!allColumnTag.getBooleanAttribute("linking nodefault")) {
						for (Tag allLinkTag : allColumnTag.getEachTag("linking", "link")) {
							if (allLinkTag.getBooleanAttribute("default")) {
								userColumnTag.setAttribute("link", allLinkTag.getStringContents());
								break;
							}
						}
					}
					
					userColumnsTag.addContent(userColumnTag);
					
					if (logger.isDebugEnabled()) {
						logger.debug("Column: " + userColumnTag.toString(true));
					}
				}
			}
		}
	}
	
	private static Tag getDefaultPrefs() {
		loadPrefs();
		
		return defaultPrefs;
	}
	
	/**
	 * Get a string preference.
	 * 
	 * @param key The name of the preference.
	 * @return The value, or "" if not found.
	 */
	public static String getString(String key) {
		return getString(key, "");
	}
	
	/**
	 * Get a string preference.
	 * 
	 * @param key The name of the preference.
	 * @param def What to return if no match is found.
	 * @return The value, or <tt>def</tt>.
	 */
	public static String getString(String key, String def) {
		String val = getDefaultPrefs().getStringContents(key);
		
		if (val == null) return def;
		
		return val;
	}
	
	/**
	 * Get a string preference.
	 * 
	 * @param key The name of the preference.
	 * @param from The tag to retrieve it from.
	 * @param def What to return if no match in the tag or the defaults was
	 *            found.
	 * @return The value (first from the tag, then from the defaults, and
	 *         finally from <tt>def</tt>).
	 */
	public static String getString(String key, Tag from, String def) {
		String val = from.getStringContents(key);
		
		if (val == null) val = getDefaultPrefs().getStringContents(key);
		if (val == null) return def;
		return val;
	}
	
	/**
	 * Get an integer preference.
	 * 
	 * @param key The name of the preference.
	 * @return The value, or 0 if not found.
	 */
	public static int getInt(String key) {
		return getInt(key, 0);
	}
	
	/**
	 * Get an integer preference.
	 * 
	 * @param key The name of the preference.
	 * @param def What to return if no match is found or the preference is not
	 *            an integer.
	 * @return The value, or <tt>def</tt>.
	 */
	public static int getInt(String key, int def) {
		String val = getDefaultPrefs().getStringContents(key);
		
		if (val == null) return def;
		
		try {
			return Integer.parseInt(val);
		} catch (NumberFormatException nfe) {
			logger.debug("Preference '" + key + "'='" + val + "' is not a number", nfe);
			
			return def;
		}
	}
	
	/**
	 * Get an integer preference.
	 * 
	 * @param key The name of the preference.
	 * @param from The tag to retrieve it from.
	 * @param def What to return if no match in the tag or the defaults was
	 *            found.
	 * @return The value (first from the tag, then from the defaults, and
	 *         finally from <tt>def</tt>).
	 */
	public static int getInt(String key, Tag from, int def) {
		String val = getString(key, from, null);
		
		if (val == null) return def;
		
		try {
			return Integer.parseInt(val);
		} catch (NumberFormatException nfe) {
			logger.debug("Preference '" + key + "'='" + val + "' is not a number", nfe);
			
			return def;
		}
	}
	
	/**
	 * Get an actual preference tag from the default set.
	 * 
	 * @param key The name of the tag.
	 * @return The tag, or <tt>null</tt> if not found.
	 */
	public static Tag getTag(String key) {
		return getDefaultPrefs().getTag(key);
	}
	
	/**
	 * Get an actual preference tag from the given tag, falling back to the
	 * defaults.
	 * 
	 * @param key The name of the tag.
	 * @param from The tag to examine first.
	 * @return The tag from <tt>from</tt>, falling back to the defaults, or
	 *         <tt>null</tt> if not found.
	 */
	public static Tag getTag(String key, Tag from) {
		Tag value = from.getTag(key);
		
		if (value == null) value = getDefaultPrefs().getTag(key);
		
		return value;
	}
	
	/**
	 * Get all matching tags from the default set.
	 * 
	 * @param key The path to the parent of the tags to get.
	 * @param tagName The name of the tags to get, or <tt>null</tt> to get all
	 *            children.
	 * @return An iterable over the tags that matched.
	 */
	public static Iterable<Tag> getTags(String key, String tagName) {
		Tag[] values = getDefaultPrefs().getEachTag(key, tagName);
		
		return new ArrayIterable<Tag>(values);
	}
	
	/**
	 * Get all matching tags from the given tag, falling back to the defaults.
	 * 
	 * @param key The path to the parent of the tags to get.
	 * @param tagName The name of the tags to get, or <tt>null</tt> to get all
	 *            children.
	 * @param from The tag to examine first.
	 * @return Matching children from <tt>from</tt>, falling back to the
	 *         defaults if none were found there.
	 */
	public static Iterable<Tag> getTags(String key, String tagName, Tag from) {
		Tag[] values = from.getEachTag(key, tagName);
		
		if (values.length == 0) values = getDefaultPrefs().getEachTag(key, tagName);
		
		return new ArrayIterable<Tag>(values);
	}
}

/*
 * UserStatusProvider.java
 *
 * Provides information about a user.
 *
 * Jonathan Pearson
 * June 1, 2009
 *
 */

package com.sixnetio.BVB.Web.UserActions;

import com.sixnetio.BVB.Web.StatusProvider;

public interface UserStatusProvider extends StatusProvider {

}

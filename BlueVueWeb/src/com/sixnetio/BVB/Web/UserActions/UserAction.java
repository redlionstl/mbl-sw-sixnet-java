/*
 * UserAction.java
 *
 * Loads a user from the database and provides CRUD actions on it.
 *
 * Jonathan Pearson
 * June 2, 2009
 *
 */

package com.sixnetio.BVB.Web.UserActions;

import java.util.Date;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class UserAction extends GeneralAction implements Preparable, UserStatusProvider {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// The ID of the user that the user specified
	private int id = User.INV_USER;
	
	// The actual user, created here or loaded from the database (for
	// single-user actions)
	private User user;
	
	// When updating the password, these get set
	// Root users may update passwords without providing this
	public String oldPassword = "";
	public String newPassword = "";
	public String confirm = "";
	
	/**
	 * Get the ID number of the user that the user specified, or
	 * {@link User#INV_USER} if no user was specified.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Get the user. Named this way to avoid overriding
	 * {@link GeneralAction#getUser()}.
	 */
	public User getTargetUser() {
		return user;
	}
	
	/**
	 * The user specified the Create action. Verifies that the user has
	 * permission to create a user (Root user) and returns
	 * {@link GeneralAction#EDIT}.
	 * 
	 * @return {@link GeneralAction#EDIT}.
	 * @throws Exception If the user does not have permission to create users.
	 */
	public String create() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.createPermission"));
		
		return INPUT;
	}
	
	/**
	 * The user specified the View action.
	 * 
	 * @return {@link GeneralAction#VIEW}.
	 * @throws Exception If the user is not logged in.
	 */
	public String view() throws Exception {
		verifyUserLoggedIn();
		
		return VIEW;
	}
	
	/**
	 * The user specified the Edit action. Verifies that the user has user edit
	 * permission (Root user) and asks for more input.
	 * 
	 * @return INPUT.
	 * @throws Exception If the user does not have user edit permission.
	 */
	public String edit() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.updatePermission"));
		
		return INPUT;
	}
	
	/**
	 * The user specified the Enable action. Verifies that the user is a root
	 * level user and asks for more input.
	 * 
	 * @return INPUT.
	 * @throws Exception If the user does not have user enable permission.
	 */
	public String enable() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.enablePermission"));
		
		if (id == User.INV_USER) addActionError(getText("error.enableNoUser"));
		
		return INPUT;
	}
	
	/**
	 * Check that the user is logged in and is either updating his own password
	 * or is a Root user.
	 * 
	 * @return INPUT.
	 * @throws Exception If the user is not logged in or is trying to update
	 *             someone else's password while not being a root user.
	 */
	public String changePassword() throws Exception {
		verifyUserLoggedIn();
		
		if (!hasPermission(User.UserType.Root)) {
			// Only root users may update other people's passwords
			if (id != getUser().userID) {
				throw new Exception(getText("error.notYourPassword"));
			}
		}
		
		return INPUT;
	}
	
	/**
	 * The user has finished editing a user. Verifies that the user has user
	 * edit permission (Root user), saves the user, and redirects to the View
	 * page.
	 * 
	 * @return {@link GeneralAction#VIEW}.
	 * @throws Exception If the user does not have user edit permission, or
	 *             there is a database exception trying to save the user.
	 */
	public String update() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.updatePermission"));
		
		// Make sure there is a user name
		if (prepareParam(user.name) == null) {
			addActionError(getText("error.emptyUserName"));
			return INPUT;
		}
		
		// Check if a new password was provided
		// If so, make sure it matches the confirmation and update the user
		// object appropriately
		if (newPassword.length() > 0 || confirm.length() > 0) {
			// Make sure that if the user is changing his own password, he
			// provided the correct old password
			if (getUser().userID == id) {
				String oldHashedPassword = User.hashPassword(user.name, oldPassword);
				
				if (!oldHashedPassword.equals(user.hashedPassword)) {
					addActionError(getText("error.oldPasswordMismatch"));
					return INPUT;
				}
			}
			
			if (!newPassword.equals(confirm)) {
				addActionError(getText("error.passwordMismatch"));
				return INPUT;
			}
			
			user.hashedPassword = User.hashPassword(user.name, newPassword);
		}
		
		LogEntry logEntry;
		
		if (id == User.INV_USER) {
			DB.getDB().createUser(user);
			id = user.userID;
			
			logEntry =
			        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
			                     String.format("Created User #%d", id));
		} else {
			DB.getDB().updateUser(user);
			
			logEntry =
			        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
			                     String.format("Updated User #%d", id));
		}
		
		DB.getDB().log(Utils.makeLinkedList(logEntry));
		
		return VIEW;
	}
	
	/**
	 * Actually update a user's password. If a user is updating his own
	 * password, he must provide his old password, even if he is a Root level
	 * user.
	 * 
	 * @return INPUT if any of these is true:
	 *         <ul>
	 *         <li>The user is updating his own password but provided an
	 *         incorrect old password</li>
	 *         <li>The new password and confirmation password do not match</li>
	 *         </ul>
	 *         On success, returns VIEW.
	 * @throws Exception If any of these happen:
	 *             <ul>
	 *             <li>The user is not logged in</li>
	 *             <li>The user is not a root user but is trying to update
	 *             someone else's password</li>
	 *             <li>The database throws an exception when updating the user</li>
	 *             </ul>
	 */
	public String realChangePassword() throws Exception {
		verifyUserLoggedIn();
		
		if (!hasPermission(User.UserType.Root)) {
			// Only root users may update other people's passwords
			if (id != getUser().userID) {
				throw new Exception(getText("error.notYourPassword"));
			}
		}
		
		// Users must provide their old password if updating their own password
		if (id == getUser().userID) {
			logger.debug("Hashing '" + user.name + ":" + oldPassword + "'");
			String hashedOldPassword = User.hashPassword(user.name, oldPassword);
			if (!hashedOldPassword.equals(user.hashedPassword)) {
				logger.debug(String.format("'%s' != '%s'", hashedOldPassword, user.hashedPassword));
				addActionError(getText("error.oldPasswordMismatch"));
				return INPUT;
			}
		}
		
		// Make sure the new password matches the confirmation
		if (!newPassword.equals(confirm)) {
			addActionError(getText("error.passwordMismatch"));
			return INPUT;
		}
		
		// Update the password
		user.hashedPassword = User.hashPassword(user.name, newPassword);
		DB.getDB().updateUser(user);
		
		LogEntry logEntry =
		        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
		                     String.format("Updated password of User #%d", id));
		DB.getDB().log(Utils.makeLinkedList(logEntry));
		
		return VIEW;
	}
	
	/**
	 * The user has finished enabling a user. Verifies that the user has user
	 * enable permission (Root user) and that the new password matches the
	 * confirmation, then saves the user and redirects to the View page.
	 * 
	 * @return {@link GeneralAction#VIEW} or INPUT if there is no user or the
	 *         passwords don't match.
	 * @throws Exception If the user does not have user enable permission, or
	 *             there is a database exception trying to save the user.
	 */
	public String realEnable() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.enablePermission"));
		
		if (id == User.INV_USER) {
			addActionError(getText("error.enableNoUser"));
			return INPUT;
		} else if (!newPassword.equals(confirm)) {
			addActionError(getText("error.passwordMismatch"));
			return INPUT;
		}
		
		user.hashedPassword = User.hashPassword(user.name, newPassword);
		DB.getDB().updateUser(user);
		
		LogEntry logEntry =
		        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
		                     String.format("Enabled User #%d", id));
		DB.getDB().log(Utils.makeLinkedList(logEntry));
		
		return VIEW;
	}
	
	@Override
	public void prepare() throws Exception {
		String sID = getRequest().getParameter("id");
		
		if (sID != null && !sID.equals("-1")) {
			try {
				id = Integer.parseInt(sID);
				
				logger.debug("User specified user with ID " + id);
				
				user = DB.getDB().getUser(id);
			} catch (NumberFormatException nfe) {
				throw new Exception(getText("error.invalidUserID", new String[] {
					sID
				}), nfe);
			}
		} else {
			logger.debug("No user specified, creating a new one");
			
			id = User.INV_USER;
			user = new User(id, "", "", User.UserType.Basic, new Tag("userprefs"));
		}
	}
	
	/** Get an array of legal user types. */
	public User.UserType[] getUserTypes() {
		return User.UserType.values();
	}
	
	@Override
	public String getStatus(Object obj) {
		return S_NORMAL;
	}
}

/*
 * UserListAction.java
 *
 * Provides sortable, selectable, filterable, paginated lists of users, and some actions
 * on the selected users in those lists (disabling, etc).
 *
 * Jonathan Pearson
 * June 1, 2009
 *
 */

package com.sixnetio.BVB.Web.UserActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Database.SortColumns.UserColumn;
import com.sixnetio.BVB.Web.*;
import com.sixnetio.BVB.Web.Iterators.UserIterable;
import com.sixnetio.util.Utils;

public class UserListAction
	extends SelectionPreparer<Integer>
	implements Filterable, LinkTranslator, Sortable, UserStatusProvider {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// Criteria for searching out batches
	public Integer qUserID = null;
	public String qName = null;
	public User.UserType qUserType = null;
	
	private long matchCount = 0;
	
	// This is only set once, so no need to synchronize
	private List<UserColumn> sortOn = null;
	
	@Override
	public Map<String, String[]> getFilters() throws Exception {
		// To start, fill this with Title --> Filter, but only individual
		// filters
		LinkedHashMap<String, String> filters = new LinkedHashMap<String, String>();
		
		if (qUserID != null) {
			filters.put(getText("filter.userID", Utils.makeLinkedList((Object)qUserID)),
			            "qUserID=" + qUserID);
		}
		
		if (qName != null) {
			filters.put(getText("filter.name", Utils.makeLinkedList((Object)qName)), "qName=" +
			                                                                         qName);
		}
		
		if (qUserType != null) {
			filters.put(getText("filter.type", Utils.makeLinkedList((Object)qUserType)),
			            "qUserType=" + qUserType);
		}
		
		return constructFilters(filters);
	}
	
	@Override
	public String getSortOn() {
		return UserActionLib.translateSort(sortOn);
	}
	
	@Override
	public void setSortOn(String columns) {
		sortOn = UserActionLib.translateSort(columns);
	}
	
	@Override
	public String addSortColumn(String colName) {
		return UserActionLib.addSortColumn(colName, sortOn);
	}
	
	/**
	 * The user specified the List action. Loads a list of all users, based on
	 * the properties that the user provided.
	 * 
	 * @return{@link GeneralAction#LIST}.
	 * @throws Exception If the user does not have Root permission or there is a
	 *             database access error.
	 */
	public String list() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.viewPermission"));
		
		long[] matchCount = new long[1];
		matchingIDs = new LinkedList<Integer>(DB.getDB()
				.getMatchingUsers(qUserID, qName, qUserType,
				                  getPageStart(), getPageSize(),
				                  sortOn, matchCount));
		
		this.matchCount = matchCount[0];
		
		if (selectAll) {
			// Can't just copy 'userIDs', since that only contains the matches
			// for *this* page
			// We need the matches across *all* pages
			selectedIDsOff.addAll(DB.getDB()
			                      .getMatchingUsers(qUserID, qName, qUserType,
			                                        0, -1, // Page start/size
			                                        sortOn, null)); // Sort/matches
		}
		
		prepareSelectedIDs();
		return LIST;
	}
	
	/**
	 * The user specified the Disable action. Verifies that the user has user
	 * disable permission (Root user) and asks for confirmation.
	 * 
	 * @return INPUT
	 * @throws Exception If the user does not have user disable permission.
	 */
	public String disable() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.disablePermission"));
		
		if (matchingIDs.size() == 0) {
			if (selectedIDsOff.size() > 0) {
				// This is how IDs are passed in from the List page
				matchingIDs.addAll(selectedIDsOff);
				
				// Also, we must be going to a new view, so go to page 1
				pageStart = 0;
			} else {
    			// Select the first page
    			matchingIDs = new LinkedList<Integer>(DB.getDB()
    					.getMatchingUsers(qUserID, qName, qUserType,
    					                  0, getPageSize(), // Page start/size
    					                  sortOn, null)); // Sort/matches
    			
    			// But don't check their boxes
    			selectedIDsOff.clear();
    			selectAll = false;
			}
		}
		
		this.matchCount = matchingIDs.size();
		
		if (selectAll) {
			selectedIDsOff.addAll(matchingIDs);
		}
		
		prepareSelectedIDs();
		return INPUT;
	}
	
	/**
	 * The user confirmed a disable action. Disable the users.
	 * 
	 * @return LIST, or ERROR if no user was specified.
	 * @throws Exception If the user does not have user disable permission.
	 */
	public String realDisable() throws Exception {
		verifyPermission(User.UserType.Root, getText("error.disablePermission"));
		
		if (selectedIDsOff.size() == 0) {
			addActionError(getText("error.disableNoUser"));
			
			prepareSelectedIDs();
			return INPUT;
		}
		
		List<LogEntry> logEntries = new LinkedList<LogEntry>();
		
		for (int id : selectedIDsOff) {
			User user = DB.getDB().getUser(id);
			user.hashedPassword = User.NOLOGON_PASSWORD;
			DB.getDB().updateUser(user);
			
			logEntries.add(new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
			                            String.format("Disabled User #%d", id)));
		}
		
		DB.getDB().log(logEntries);
		
		// Redirect action, no need to prepare selections
		return LIST;
	}
	
	@Override
	public long getMatchCount() {
		return matchCount;
	}
	
	@Override
	public String translateLink(String link, Object obj) {
		if (obj == null || !(obj instanceof User)) return "";
		
		User user = (User)obj;
		String docroot = getDocumentRoot();
		
		if (link.equals("viewUser")) {
			return String.format("%s/User/view?id=%d", docroot, user.userID);
			
		} else if (link.equals("filterJobs")) {
			return String.format("%s/Jobs?qUserID=%d", docroot, user.userID);
			
		} else if (link.equals("filterBatches")) {
			return String.format("%s/Batches?qUserID=%d", docroot, user.userID);
			
		} else if (link.startsWith("filterUsers/")) {
			String filterName = link.substring(link.indexOf('/') + 1);
			
			Integer qUserID = this.qUserID;
			String qName = this.qName;
			User.UserType qUserType = this.qUserType;
			
			if (filterName.equals("userID")) {
				qUserID = user.userID;
			} else if (filterName.equals("name")) {
				qName = user.name;
			} else if (filterName.equals("type")) {
				qUserType = user.type;
			}
			
			StringBuilder builder = new StringBuilder(docroot + "/Users?");
			
			// A trailing '&' won't hurt anything, so just generate a link with
			// it for simplicity
			
			if (qUserID != null) {
				builder.append(String.format("qUserID=%d&", qUserID));
			}
			
			if (qName != null) {
				builder.append(String.format("qName=%s&", qName));
			}
			
			if (qUserType != null) {
				builder.append(String.format("qUserType=%s&", qUserType.name()));
			}
			
			return builder.toString();
		}
		
		return "";
	}
	
	@Override
    protected Collection<String> getIDParameterNames() {
	    // Allow user IDs to be passed around
		return Utils.makeArrayList("userID");
    }
	
	@Override
    protected Collection<String> getSelectedParameterNames() {
	    // Allow user IDs to be selected/passed in
		return Utils.makeArrayList("selUserID");
    }
	
	@Override
    protected Collection<Integer> parseID(String type, String id)
    	throws NumberFormatException, DatabaseException, Exception {
	    
		if (type.equals("userID") || type.equals("selUserID")) {
			return Utils.makeArrayList(Integer.parseInt(id));
		} else {
			throw new IllegalArgumentException("Unrecognized type: " + type);
		}
    }
	
	@Override
    public Iterable<?> getItems() throws Exception {
	    return new UserIterable(getPageIDs());
    }

	@Override
    public String getStatus(Object obj) throws Exception {
	    return UserActionLib.getUserStatus((User)obj);
    }
}

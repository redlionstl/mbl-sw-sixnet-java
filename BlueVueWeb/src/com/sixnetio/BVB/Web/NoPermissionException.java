/*
 * NoPermissionException.java
 *
 * Thrown when the user tries to do something that he does not have permission to do.
 *
 * Jonathan Pearson
 * April 29, 2009
 *
 */

package com.sixnetio.BVB.Web;

public class NoPermissionException extends Exception {
	public NoPermissionException(String message) {
		super(message);
	}
	
	public NoPermissionException(String message, Throwable cause) {
		super(message, cause);
	}
}

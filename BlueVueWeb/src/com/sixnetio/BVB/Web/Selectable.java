/*
 * Selectable.java
 *
 * Provides functions for determining whether objects are selected.
 * This is compatible with Paginated, allowing for the determination of
 * whether objects on other pages are selected also.
 *
 * Jonathan Pearson
 * May 6, 2009
 *
 */

package com.sixnetio.BVB.Web;

public interface Selectable {
	/**
	 * Check whether the object with the given ID number is selected, across all
	 * pages.
	 * 
	 * @param id The ID number to check.
	 * @return True = the object is selected on some page, false = the object is
	 *         not selected anywhere.
	 */
	public boolean isSelected(Number id);
	
	/**
	 * Get an iterable over all selected object IDs from <i>other</i> pages (not
	 * this page). This is useful for creating hidden input fields for selected
	 * (but invisible) objects.
	 * 
	 * @return An iterable over invisible selected object IDs.
	 */
	public Iterable<? extends Number> getOtherSelectedIDs();
}

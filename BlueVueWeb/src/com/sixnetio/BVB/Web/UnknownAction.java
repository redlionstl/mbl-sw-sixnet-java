/*
 * UnknownAction.java
 *
 * Handles requests for unknown pages.
 *
 * Jonathan Pearson
 * April 27, 2009
 *
 */

package com.sixnetio.BVB.Web;

public class UnknownAction extends GeneralAction {
	@Override
	public String execute() throws Exception {
		// Check if the user is logged in
		if (getUser() == null) {
			// Nope, display the login page
			return "login";
		} else {
			// Yep; the user should know better, display an error page
			addActionError(getText("error.missingPage", new String[] {
				getRequest().getRequestURI()
			}));
			
			return ERROR;
		}
	}
}

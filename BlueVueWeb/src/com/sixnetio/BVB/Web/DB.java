/*
 * DB.java
 *
 * The server maintains a single instance of Database; this
 * holds onto that instance.
 *
 * Jonathan Pearson
 * April 15, 2009
 *
 */

package com.sixnetio.BVB.Web;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.Initializer;
import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.util.Utils;

public class DB {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public static final String CFGFILE;
	
	static {
		CFGFILE = Server.configureInstance("bvweb");
	}
	
	private static Database db;
	
	public static void connect() throws Exception {
		String cfgFilePath = CFGFILE; // No way to pass arguments...
		
		if (cfgFilePath == null) throw new Exception("Unable to start: No configuration file found");
		
		// Load the config file
		Tag config;
		try {
			config = Utils.xmlParser.parseXML(cfgFilePath, false, false);
		} catch (IOException ioe) {
			logger.error("Bad configuration file", ioe);
			throw new Exception("Unable to start: Bad configuration file: " + ioe.getMessage(), ioe);
		}
		
		// Start up a plugin watcher and wait for it to perform an update
		try {
			Initializer.loadPlugins(config.getChild("plugins"));
		} catch (IllegalArgumentException iae) {
			logger.error("Unable to load plugins", iae);
			throw new Exception("Unable to start: Unable to load plugins: " + iae.getMessage(), iae);
		}
		
		/*
		 * try { Initializer.loadClasses(config.getChild("autoload")); } catch
		 * (ClassNotFoundException cnfe) { throw new
		 * Exception("Unable to start: Unable to load classes: " +
		 * cnfe.getMessage(), cnfe); }
		 */

		// Load the database
		try {
			db = Database.getInstance(config.getStringContents("database/provider"));
		} catch (BadProviderException bpe) {
			logger.error("Bad database provider", bpe);
			throw new Exception("Unable to start: Bad database provider: " + bpe.getMessage(), bpe);
		} catch (DriverException de) {
			logger.error("Unable to load database driver", de);
			throw new Exception("Unable to start: Cannot load database driver: " + de.getMessage(),
			                    de);
		}
		
		// Connect to the database
		try {
			db.connect(config.getStringContents("database/server"),
			           config.getStringContents("database/name"),
			           config.getStringContents("database/user"),
			           config.getStringContents("database/password"), false, null,
			           Database.ConnectionReason.Client);
		} catch (DatabaseException de) {
			logger.error("Unable to connect to database", de);
			
			db = null;
			
			throw new Exception("Unable to start: Cannot connect to database: " + de.getMessage(),
			                    de);
		}
	}
	
	public static Database getDB() throws Exception {
		if (db == null) connect();
		
		return db;
	}
}

/*
 * LogoutAction.java
 *
 * Log out of the system.
 *
 * Jonathan Pearson
 * April 15, 2009
 *
 */

package com.sixnetio.BVB.Web;

public class LogoutAction extends GeneralAction {
	public String execute() throws Exception {
		setUser(null);
		
		return SUCCESS;
	}
}

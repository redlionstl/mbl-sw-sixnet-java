package com.sixnetio.BVB.Web.SettingsActions;

import com.sixnetio.BVB.Common.Settings;

/**
 * Adds a few additional user-interface-specific properties to a
 * {@link Settings.Setting}.
 */
public class SettingExtras
{
    /** The setting value. */
    public String value;

    /**
     * If <code>true</code>, the value cannot be modified using the web UI.
     * Default is <code>false</code>.
     */
    public boolean readonly;

    /**
     * If <code>true</code>, implies {@link #readonly} and also only allows
     * downloads of the setting (no direct viewing). Default is
     * <code>false</code>.
     */
    public boolean exportonly;

    /**
     * The MIME Content-Type for the export data.
     */
    public String contentType;

    /**
     * The default file name for the export data.
     */
    public String fileName;

    /**
     * Construct a new SettingExtras.
     * 
     * @param value The value of the setting. Other properties default to
     *            sensible defaults.
     */
    public SettingExtras(String value)
    {
        this.value = value;
    }
}

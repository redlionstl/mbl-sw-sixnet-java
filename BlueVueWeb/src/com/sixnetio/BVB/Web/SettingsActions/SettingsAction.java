/*
 * SettingsAction.java
 *
 * Allows for viewing and updating of settings.
 *
 * Jonathan Pearson
 * June 9, 2009
 *
 */

package com.sixnetio.BVB.Web.SettingsActions;

import java.io.*;
import java.util.Date;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class SettingsAction
    extends GeneralAction
{
    static final Logger logger = Logger.getLogger(Utils.thisClassName());

    public String settingName;
    public String settingValue;
    public boolean delete;
    public boolean makeNull;

    public String contentType;
    public String fileName;
    public InputStream exportStream;

    public String edit()
        throws Exception
    {
        verifyPermission(User.UserType.Root,
            getText("error.updateSettingsPermission"));

        return INPUT;
    }

    public String update()
        throws Exception
    {
        verifyPermission(User.UserType.Root,
            getText("error.updateSettingsPermission"));

        LogEntry logEntry;
        if (delete) {
            logger.debug("Deleting setting '" + settingName + "'");

            DB.getDB().deleteSetting(settingName);

            logEntry =
                new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
                    String.format("Deleted Setting '%s'", settingName));
        }
        else {
            if (makeNull) {
                settingValue = null;
            }

            logger.debug("Creating/updating setting '" + settingName +
                         "' with value '" + settingValue + "'");

            DB.getDB().updateSetting(settingName, settingValue);

            logEntry =
                new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
                    String.format("Created/Updated Setting '%s'", settingName));
        }

        DB.getDB().log(Utils.makeLinkedList(logEntry));

        // Since we just updated the settings, make sure the server knows about
        // it
        Server.refreshSettingsNow(DB.getDB());

        return LIST;
    }

    public String export()
        throws Exception
    {
        verifyPermission(User.UserType.Root,
            getText("error.exportSettingsPermission"));

        LogEntry logEntry;
        logEntry =
            new LogEntry(new Date(), getUser().userID, DBLogger.INFO, String
                .format("Exported Setting '%s'", settingName));

        final PipedInputStream in = new PipedInputStream();

        exportStream = in;

        // The value of export settings is not passed back in, as it is
        // probably
        // large or ugly
        Settings settings =
            new Settings(DB.getDB(), Utils.makeArrayList(new String[] {
                settingName
            }));
        final Tag tag =
            Utils.xmlParser.parseXML(new StringReader(settings
                .getStringValue(settingName)), false, false);

        Thread writer = new Thread("export-writer") {
            @Override
            public void run()
            {
                final PipedOutputStream out;
                try {
                    out = new PipedOutputStream(in);
                }
                catch (IOException ioe) {
                    logger.debug("Failed to open stream", ioe);

                    // Cause the transfer to end
                    try {
                        in.close();
                    }
                    catch (IOException ioe2) {
                        logger.debug("Failed to close other stream", ioe2);
                    }

                    return;
                }

                try {
                    Utils.xmlParser.writeXMLStream(tag, out, true);
                }
                catch (IOException ioe) {
                    logger.debug("Failed to write entire value", ioe);
                }
                finally {
                    try {
                        out.close();
                    }
                    catch (IOException ioe) {
                        logger.debug("Failed to close stream", ioe);

                        // Cause the transfer to end
                        try {
                            in.close();
                        }
                        catch (IOException ioe2) {
                            logger.debug("Failed to close other stream", ioe2);
                        }
                    }
                }
            }
        };

        writer.setDaemon(true);
        writer.start();

        DB.getDB().log(Utils.makeLinkedList(logEntry));

        // This is the only place this is used, so don't bother with a constant
        return "export";
    }
}

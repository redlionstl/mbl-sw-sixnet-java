/*
 * SettingsListAction.java
 *
 * Lists settings.
 *
 * Jonathan Pearson
 * June 9, 2009
 *
 */

package com.sixnetio.BVB.Web.SettingsActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.Settings;
import com.sixnetio.BVB.Common.User;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class SettingsListAction
    extends GeneralAction
    implements Preparable
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    // Used in a single-threaded manner
    private Map<String, SettingExtras> settings;

    public Map<String, SettingExtras> getSettings()
    {
        return settings;
    }

    public String list()
        throws Exception
    {
        verifyPermission(User.UserType.Root,
            getText("error.viewSettingsPermission"));

        return LIST;
    }

    @Override
    public void prepare()
        throws Exception
    {
        // Load all settings from the database
        Settings settings = new Settings(DB.getDB());

        // Get the setting names and sort them alphabetically
        String[] names = settings.getKnownSettingNames().toArray(new String[0]);
        Arrays.sort(names);

        // Populate the local map
        this.settings = new LinkedHashMap<String, SettingExtras>();
        for (String name : names) {
            this.settings.put(name, new SettingExtras(settings
                .getStringValue(name)));
        }

        // If there are any settings that need special attention, mark them here

        // License key is too long and messes up the view
        this.settings.get(Settings.Setting.LicenseKey.properName).exportonly =
            true;
        this.settings.get(Settings.Setting.LicenseKey.properName).contentType =
            "text/xml;charset=UTF-8";
        this.settings.get(Settings.Setting.LicenseKey.properName).fileName =
            "license.blf";

        // Database version should not be touched by the user, as it could cause
        // serious problems
        this.settings.get(Settings.Setting.DatabaseVersion.properName).readonly =
            true;
    }
}

/*
 * HistoryStatusProvider.java
 *
 * Provides information about a history entry.
 *
 * Jonathan Pearson
 * May 12, 2009
 *
 */

package com.sixnetio.BVB.Web.HistoryActions;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.StatusProvider;

public interface HistoryStatusProvider extends StatusProvider {
	/**
	 * Returns one of the S_* values from {@link StatusProvider} describing the
	 * status of this history entry.
	 * 
	 * @param obj The entry to check.
	 * @return {@link StatusProvider#S_NORMAL} if the event represents a
	 *         success, or {@link StatusProvider#S_FAILURE} if the event
	 *         represents a failure.
	 */
	@Override
	public String getStatus(Object obj) throws Exception;
	
	/**
	 * Get the job associated with the given history entry.
	 * 
	 * @param entry The entry.
	 * @return The job associated with the entry, or <tt>null</tt> if it is no
	 *         longer active.
	 * @throws Exception If there is a problem retrieving the data.
	 */
	public Job getHistoryJob(HistoryEntry entry) throws Exception;
	
	/**
	 * Get the modem associated with the given history entry.
	 * 
	 * @param entry The entry.
	 * @return The modem associated with the entry.
	 * @throws Exception If there is a problem retrieving the data.
	 */
	public Modem getHistoryModem(HistoryEntry entry) throws Exception;
	
	/**
	 * Get the user associated with the given history entry.
	 * 
	 * @param entry The entry.
	 * @return The user associated with the entry, or <tt>null</tt> if there is
	 *         none.
	 * @throws Exception If there is a problem retrieving the data.
	 */
	public User getHistoryUser(HistoryEntry entry) throws Exception;
	
	/**
	 * Get the batch associated with the given history entry.
	 * 
	 * @param entry The entry.
	 * @return The batch associated with the entry, or <tt>null</tt> if there is
	 *         none.
	 * @throws Exception If there is a problem retrieving the data.
	 */
	public JobBatch getHistoryBatch(HistoryEntry entry) throws Exception;
}

/*
 * HistoryAction.java
 *
 * Retrieves history results.
 *
 * Jonathan Pearson
 * April 27, 2009
 *
 */

package com.sixnetio.BVB.Web.HistoryActions;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class HistoryAction extends GeneralAction implements HistoryStatusProvider, Preparable {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private long id = HistoryEntry.INV_HISTORY;
	
	private HistoryEntry entry = null;
	
	@Override
	public void prepare() throws Exception {
		String sID = getRequest().getParameter("id");
		
		if (sID != null && !sID.equals("-1")) {
			try {
				id = Long.parseLong(sID);
				
				logger.debug("User specified history entry with ID " + id);
				
				entry = DB.getDB().getHistoryEntry(id);
			} catch (NumberFormatException nfe) {
				throw new Exception(getText("error.invalidHistoryID", new String[] {
					sID
				}), nfe);
			}
		} else {
			logger.debug("No history entry specified, cannot create a new one");
			
			addActionError(getText("error.noHistory"));
		}
	}
	
	public HistoryEntry getEntry() {
		return entry;
	}
	
	public String view() throws Exception {
		verifyUserLoggedIn();
		
		return VIEW;
	}
	
	@Override
	public String getStatus(Object obj) {
		return HistoryActionLib.getHistoryStatus((HistoryEntry)obj);
	}
	
	@Override
	public JobBatch getHistoryBatch(HistoryEntry entry) throws Exception {
		return HistoryActionLib.getHistoryBatch(entry);
	}
	
	@Override
	public Job getHistoryJob(HistoryEntry entry) throws Exception {
		return HistoryActionLib.getHistoryJob(entry);
	}
	
	@Override
	public Modem getHistoryModem(HistoryEntry entry) throws Exception {
		return HistoryActionLib.getHistoryModem(entry);
	}
	
	@Override
	public User getHistoryUser(HistoryEntry entry) throws Exception {
		return HistoryActionLib.getHistoryUser(entry);
	}
}

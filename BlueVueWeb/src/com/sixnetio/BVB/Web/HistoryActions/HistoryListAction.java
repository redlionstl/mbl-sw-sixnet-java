/*
 * HistoryListAction.java
 *
 * Provides sortable, filterable, paginated lists of history entries.
 *
 * Jonathan Pearson
 * May 12, 2009
 *
 */

package com.sixnetio.BVB.Web.HistoryActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Database.SortColumns.BatchColumn;
import com.sixnetio.BVB.Database.SortColumns.HistoryColumn;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.*;
import com.sixnetio.BVB.Web.Iterators.BatchIterable;
import com.sixnetio.BVB.Web.Iterators.HistoryIterable;
import com.sixnetio.util.Utils;

public class HistoryListAction
	extends IDPreparer<Long>
	implements Filterable, LinkTranslator, Paginated, Sortable,
		HistoryStatusProvider {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// Criteria for searching out history entries
	public Long qHistoryID = null;
	public Long qJobID = null;
	public Integer qBatchID = null;
	public Integer qModemID = null;
	public Integer qUserID = null;
	public String qJobType = null;
	public String qResult = null;
	public Date qLowDate = null;
	public Date qHighDate = null;
	
	private long matchCount = 0;
	
	// This should only be set once, so no need to synchronize
	private List<HistoryColumn> sortOn = null;
	
	@Override
	public Map<String, String[]> getFilters() throws Exception {
		// To start, fill this with Title --> Filter, but only individual
		// filters
		LinkedHashMap<String, String> filters = new LinkedHashMap<String, String>();
		
		if (qHistoryID != null) {
			filters.put(getText("filter.historyID", Utils.makeLinkedList((Object)qHistoryID)),
			            "qHistoryID=" + qHistoryID);
		}
		
		if (qJobID != null) {
			filters.put(getText("filter.jobID", Utils.makeLinkedList((Object)qJobID)), "qJobID=" +
			                                                                           qJobID);
		}
		
		if (qBatchID != null) {
			JobBatch batch = DB.getDB().getBatch(qBatchID);
			
			if (batch != null) {
				filters.put(getText("filter.batch", Utils.makeLinkedList((Object)batch.name)),
				            "qBatchID=" + qBatchID);
			} else {
				filters.put(getText("filter.batchID", Utils.makeLinkedList((Object)qBatchID)),
				            "qBatchID=" + qBatchID);
			}
		}
		
		if (qModemID != null) {
			Modem modem = DB.getDB().getModem(qModemID);
			
			if (modem != null) {
				filters
				       .put(
				            getText("filter.modem", Utils.makeLinkedList((Object)modem.getLabel())),
				            "qModemID=" + qModemID);
			} else {
				filters.put(getText("filter.modemID", Utils.makeLinkedList((Object)qModemID)),
				            "qModemID=" + qModemID);
			}
		}
		
		if (qUserID != null) {
			User user = DB.getDB().getUser(qUserID);
			
			if (user != null) {
				filters.put(getText("filter.user", Utils.makeLinkedList((Object)user.name)),
				            "qUserID=" + qUserID);
			} else {
				filters.put(getText("filter.userID", Utils.makeLinkedList((Object)qUserID)),
				            "qUserID=" + qUserID);
			}
		}
		
		if (qJobType != null) {
			filters.put(getText("filter.jobType", Utils.makeLinkedList((Object)qJobType)),
			            "qJobType=" + qJobType);
		}
		
		if (qResult != null) {
			filters.put(getText("filter.result", Utils.makeLinkedList((Object)qResult)),
			            "qResult=" + qResult);
		}
		
		if (qLowDate != null) {
			String formatted = formatData("format.datetime", qLowDate);
			filters.put(getText("filter.lowDate", Utils.makeLinkedList((Object)formatted)),
			            "qLowDate=" + formatted);
		}
		
		if (qHighDate != null) {
			String formatted = formatData("format.datetime", qHighDate);
			filters.put(getText("filter.highDate", Utils.makeLinkedList((Object)formatted)),
			            "qHighDate=" + formatted);
		}
		
		return constructFilters(filters);
	}
	
	@Override
	public String getSortOn() {
		return HistoryActionLib.translateSort(sortOn);
	}
	
	@Override
	public void setSortOn(String columns) {
		sortOn = HistoryActionLib.translateSort(columns);
	}
	
	@Override
	public String addSortColumn(String colName) {
		return HistoryActionLib.addSortColumn(colName, sortOn);
	}
	
	/**
	 * The user specified the List action. Loads a list of all history, based on
	 * the properties that the user provided.
	 * 
	 * @return{@link GeneralAction#LIST}.
	 * @throws Exception If there is a database access error.
	 */
	public String list() throws Exception {
		long[] matchCount = new long[1];
		matchingIDs = new LinkedList<Long>(DB.getDB()
				.getMatchingHistory(qHistoryID, qJobID, qBatchID, qModemID,
				                    qUserID, qJobType, qResult, qLowDate,
				                    qHighDate,
				                    getPageStart(), getPageSize(),
				                    sortOn, matchCount));
		
		this.matchCount = matchCount[0];
		
		return LIST;
	}
	
	@Override
	public long getMatchCount() {
		return matchCount;
	}
	
	@Override
	public String getStatus(Object obj) {
		return HistoryActionLib.getHistoryStatus((HistoryEntry)obj);
	}
	
	@Override
	public JobBatch getHistoryBatch(HistoryEntry entry) throws Exception {
		return HistoryActionLib.getHistoryBatch(entry);
	}
	
	@Override
	public Job getHistoryJob(HistoryEntry entry) throws Exception {
		return HistoryActionLib.getHistoryJob(entry);
	}
	
	@Override
	public Modem getHistoryModem(HistoryEntry entry) throws Exception {
		return HistoryActionLib.getHistoryModem(entry);
	}
	
	@Override
	public User getHistoryUser(HistoryEntry entry) throws Exception {
		return HistoryActionLib.getHistoryUser(entry);
	}
	
	@Override
	public String translateLink(String link, Object obj) throws Exception {
		if (obj == null || !(obj instanceof HistoryEntry)) return "";
		
		HistoryEntry entry = (HistoryEntry)obj;
		String docroot = getDocumentRoot();
		
		if (link.equals("viewHistory")) {
			return String.format("%s/HistoryEntries/view?id=%d", docroot, entry.historyID);
			
		} else if (link.equals("viewJob")) {
			return String.format("%s/Job/view?id=%d", docroot, entry.jobID);
			
		} else if (link.equals("viewBatch")) {
			if (entry.batchID == null) return "";
			return String.format("%s/Batch/view?id=%d", docroot, entry.batchID);
			
		} else if (link.equals("viewModem")) {
			return String.format("%s/Modem/view?id=%d", docroot, entry.modemID);
			
		} else if (link.equals("viewUser")) {
			if (entry.userID == null) return "";
			return String.format("%s/User/view?id=%d", docroot, entry.userID);
			
		} else if (link.startsWith("filterHistory/")) {
			String filterName = link.substring(link.indexOf('/') + 1);
			
			Long qHistoryID = this.qHistoryID;
			Long qJobID = this.qJobID;
			Integer qBatchID = this.qBatchID;
			Integer qModemID = this.qModemID;
			Integer qUserID = this.qUserID;
			String qJobType = this.qJobType;
			String qResult = this.qResult;
			Date qLowDate = this.qLowDate;
			Date qHighDate = this.qHighDate;
			
			if (filterName.equals("historyID")) {
				qHistoryID = entry.historyID;
				
			} else if (filterName.equals("jobID")) {
				qJobID = entry.jobID;
				
			} else if (filterName.equals("batchID")) {
				if (entry.batchID == null) {
					qBatchID = JobBatch.INV_BATCH;
				} else {
					qBatchID = entry.batchID;
				}
				
			} else if (filterName.equals("modemID")) {
				qModemID = entry.modemID;
				
			} else if (filterName.equals("userID")) {
				if (entry.userID == null) {
					qUserID = User.INV_USER;
				} else {
					qUserID = entry.userID;
				}
				
			} else if (filterName.equals("type")) {
				qJobType = entry.type;
				
			} else if (filterName.equals("result")) {
				qResult = entry.result;
				
			} else if (filterName.startsWith("date/")) {
				String range = filterName.substring(filterName.indexOf('/') + 1);
				int minuteRange = 10; // Default to 10 minutes
				
				try {
					minuteRange = Integer.parseInt(range);
				} catch (NumberFormatException nfe) {
					logger.warn("Not a number: " + range, nfe);
				}
				
				// Select within minuteRange minutes
				Calendar cal = Calendar.getInstance();
				
				cal.setTime(entry.timestamp);
				cal.add(Calendar.MINUTE, -minuteRange);
				qLowDate = cal.getTime();
				
				cal.setTime(entry.timestamp);
				cal.add(Calendar.MINUTE, minuteRange);
				qHighDate = cal.getTime();
			}
			
			StringBuilder builder = new StringBuilder(docroot + "/History?");
			
			// A trailing '&' won't hurt anything, so just generate a link with
			// it for simplicity
			
			if (qHistoryID != null) {
				builder.append(String.format("qHistoryID=%d&", qHistoryID));
			}
			
			if (qJobID != null) {
				builder.append(String.format("qJobID=%d&", qJobID));
			}
			
			if (qBatchID != null) {
				builder.append(String.format("qBatchID=%d&", qBatchID));
			}
			
			if (qModemID != null) {
				builder.append(String.format("qModemID=%d&", qModemID));
			}
			
			if (qUserID != null) {
				builder.append(String.format("qUserID=%d&", qUserID));
			}
			
			if (qJobType != null) {
				builder.append(String.format("qJobType=%s&", qJobType));
			}
			
			if (qResult != null) {
				builder.append(String.format("qResult=%s&", qResult));
			}
			
			if (qLowDate != null) {
				builder.append(String.format("qLowDate=%s&",
				                             formatData("format.datetime",
				                                        qLowDate)));
			}
			
			if (qHighDate != null) {
				builder.append(String.format("qHighDate=%s&",
				                             formatData("format.datetime",
				                                        qHighDate)));
			}
			
			return builder.toString();
		}
		
		return "";
	}

	@Override
    protected Collection<String> getIDParameterNames() {
		// Don't actually listen for anything
	    return new ArrayList<String>();
    }

	@Override
    public Iterable<?> getItems() throws Exception {
	    return new HistoryIterable(getPageIDs());
    }

	@Override
    protected Collection<Long> parseID(String type, String id)
    	throws NumberFormatException, DatabaseException, Exception {
	    
		// We don't actually allow anything to be passed around...
		throw new IllegalArgumentException("Unrecognized type: " + type);
    }
	
	/**
	 * Get an iterable over batches that are not marked as 'deleted'.
	 * 
	 * @throws Exception If there is a problem retrieving the batches.
	 */
	public BatchIterable getLiveBatches() throws Exception {
		List<BatchColumn> sort = Utils.makeArrayList(BatchColumn.BatchName);
		Collection<Integer> batchIDs = DB.getDB()
			.getMatchingBatches(null, null, null, false,
			                    0, -1, // Page start/size
			                    sort, null); // Sort/match count
		
		return new BatchIterable(batchIDs);
	}
}

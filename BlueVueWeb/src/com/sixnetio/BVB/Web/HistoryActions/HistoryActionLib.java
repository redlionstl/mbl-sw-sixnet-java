/*
 * HistoryActionLib.java
 *
 * Provides some library functions common to History actions.
 *
 * Jonathan Pearson
 * May 12, 2009
 *
 */

package com.sixnetio.BVB.Web.HistoryActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.SortColumns.HistoryColumn;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.util.Utils;

public class HistoryActionLib {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Examines the given history entry, returning a string describing the
	 * status of the entry.
	 * 
	 * @param entry The entry to check.
	 * @return {@link HistoryStatusProvider#S_NORMAL} if the entry represents a
	 *         success, {@link HistoryStatusProvider#S_FAILURE} if the entry
	 *         represents a failure.
	 */
	public static String getHistoryStatus(HistoryEntry entry) {
		if (entry.result.equals("Success")) {
			return HistoryStatusProvider.S_NORMAL;
		} else if (entry.result.equals("Failure")) {
			return HistoryStatusProvider.S_FAILURE;
		} else {
			logger.debug("Unrecognized result in history: " + entry.result);
			
			return HistoryStatusProvider.S_NORMAL;
		}
	}
	
	/**
	 * Get the job associated with the given history entry.
	 * 
	 * @param entry The entry.
	 * @return The job associated with the entry, or <tt>null</tt> if it is no
	 *         longer active.
	 * @throws Exception If a database error occurs.
	 */
	public static Job getHistoryJob(HistoryEntry entry) throws Exception {
		Job job = DB.getDB().getJob(entry.jobID, true);
		
		return job;
	}
	
	/**
	 * Get the batch associated with the given history entry.
	 * 
	 * @param entry The entry.
	 * @return The batch associated with the entry, or <tt>null</tt> if there is
	 *         none.
	 * @throws Exception If a database error occurs.
	 */
	public static JobBatch getHistoryBatch(HistoryEntry entry) throws Exception {
		if (entry.batchID == null) return null;
		
		JobBatch batch = DB.getDB().getBatch(entry.batchID);
		
		return batch;
	}
	
	/**
	 * Get the modem associated with the given history entry.
	 * 
	 * @param entry The entry.
	 * @return The modem associated with the entry.
	 * @throws Exception If a database error occurs.
	 */
	public static Modem getHistoryModem(HistoryEntry entry) throws Exception {
		Modem modem = DB.getDB().getModem(entry.modemID);
		
		return modem;
	}
	
	/**
	 * Get the user associated with the given history entry.
	 * 
	 * @param entry The entry.
	 * @return The user associated with the entry, or <tt>null</tt> if there is
	 *         none.
	 * @throws Exception If a database error occurs.
	 */
	public static User getHistoryUser(HistoryEntry entry) throws Exception {
		if (entry.userID == null) return null;
		
		User user = DB.getDB().getUser(entry.userID);
		
		return user;
	}
	
	/**
	 * Translate a string describing sorting columns into a list of
	 * HistoryColumns.
	 * 
	 * @param columns The description of the sorting columns.
	 * @return A list that could be passed to
	 *         {@link Database#getMatchingHistory(Long, Long, Integer, Integer, Integer, String, String, Date, Date, int, int, List, long[])}
	 *         .
	 */
	public static List<HistoryColumn> translateSort(String columns) {
		String[] cols = Utils.tokenize(columns, ",", false);
		List<HistoryColumn> sortOn = new Vector<HistoryColumn>(cols.length);
		
		for (String column : cols) {
			if (column.startsWith("!")) {
				sortOn.add(HistoryColumn.Reverse);
				column = column.substring(1);
			}
			
			sortOn.add(HistoryColumn.valueOf(column));
		}
		
		return sortOn;
	}
	
	/**
	 * Given an existing sort list, add a new primary column to it, shuffling
	 * the existing columns as necessary.
	 * 
	 * @param colName The new column to add.
	 * @param oldSortOn The existing sort list, or <tt>null</tt> if there wasn't
	 *            one. If synchronization is necessary, it must be handled by
	 *            the caller.
	 * @return A new sort list. If the new column was already the primary
	 *         column, reverses the sorting order. If it was already in the
	 *         list, moves it to the beginning in normal order. If it did not
	 *         exist, adds it at the beginning in normal order.
	 */
	public static String addSortColumn(String colName, List<HistoryColumn> oldSortOn) {
		HistoryColumn col;
		
		try {
			col = HistoryColumn.valueOf(colName);
		} catch (IllegalArgumentException iae) {
			logger.debug("Not a legal column name: '" + colName + "'", iae);
			
			return translateSort(oldSortOn);
		}
		
		// Copy the sortOn object, since we don't actually want to change it
		List<HistoryColumn> sortOn;
		
		if (oldSortOn == null) {
			// It's only going to receive one item
			sortOn = new ArrayList<HistoryColumn>(1);
		} else {
			sortOn = new ArrayList<HistoryColumn>(oldSortOn);
		}
		
		// Get the index of the column, to see if it's already part of the sort
		// order
		int index = sortOn.indexOf(col);
		
		// Whether to apply a 'Reverse' just before the column
		boolean reverse = false;
		
		// If the column was already in there...
		if (index != -1) {
			// Remove it
			sortOn.remove(index);
			
			// If there was a reverse just before it...
			if (index > 0 && sortOn.get(index - 1) == HistoryColumn.Reverse) {
				sortOn.remove(index - 1);
				
				reverse = true;
				index--; // So we can more easily tell if it was the first
				// column
			}
			
			// If it was the first sort column
			if (index == 0) {
				// Swap the reverse property
				reverse = !reverse;
			}
		}
		
		// Push the column onto the front of the sort order
		sortOn.add(0, col);
		
		// If reversing, push reverse onto the front also
		if (reverse) {
			sortOn.add(0, HistoryColumn.Reverse);
		}
		
		// Return the new sorting order
		return translateSort(sortOn);
	}
	
	/**
	 * Translate a sort list into a string.
	 * 
	 * @param sortOn The sort list to translate. If synchronization is
	 *            necessary, it must be handled by the caller.
	 * @return A descriptive string.
	 */
	public static String translateSort(List<HistoryColumn> sortOn) {
		if (sortOn == null) return "";
		
		StringBuilder builder = new StringBuilder();
		
		boolean skipComma = true;
		for (HistoryColumn column : sortOn) {
			if (skipComma) {
				skipComma = false;
			} else {
				builder.append(",");
			}
			
			if (column == HistoryColumn.Reverse) {
				builder.append("!");
				skipComma = true;
			} else {
				builder.append(column.name());
			}
		}
		
		return builder.toString();
	}
}

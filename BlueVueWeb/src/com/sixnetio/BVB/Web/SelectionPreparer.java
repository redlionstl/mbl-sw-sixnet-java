/*
 * SelectionPreparer.java
 *
 * Provides functions for preparing a list of ID numbers. Subclasses provide
 * information about how those ID numbers are labeled, and how to retrieve the
 * corresponding database objects.
 * 
 * This acts on selections of objects, rather on what objects are to be
 * displayed, which is what IDPreparer does.
 *
 * Jonathan Pearson
 * July 9, 2009
 *
 */

package com.sixnetio.BVB.Web;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.Utils;


/**
 * Prepares a pair of sets of selected IDs. One, {@link #selectedIDsOff}
 * contains the selected IDs of off-screen objects, while the other,
 * {@link #selectedIDsOn}, contains the selected IDs of on-screen objects. The
 * subclass is responsible for calling {@link #prepareSelectedIDs()} once it has
 * enough information to return a meaningful value from {@link #getPageIDs()}.
 * This will transfer the appropriate IDs from {@link #selectedIDsOff}, which
 * up until this point will actually contain all selected IDs, to
 * {@link #selectedIDsOn}, which up to this point will have been <tt>null</tt>.
 *
 * @author Jonathan Pearson
 * @param <E> The actual data type of the ID numbers.
 */
public abstract class SelectionPreparer<E extends Number>
	extends IDPreparer<E> {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * The selected ID numbers that are visible on the currently displayed page.
	 * These are not included in {@link #selectedIDsOff}, which is saved in the
	 * session, because these may be replaced by the user during the current
	 * interaction.
	 */
	protected Set<E> selectedIDsOn;
	
	/**
	 * The selected ID numbers that are not visible on the currently displayed
	 * page. These are saved in the user's session, and do not include any
	 * currently visible objects whose selections may be changed.
	 */
	protected Set<E> selectedIDsOff;
	
	/**
	 * If the user asks to select all objects, this will be set to true. We
	 * cannot simply select all when we see the message from the user because
	 * there is no way of knowing what "all" means at that point.
	 */
	protected boolean selectAll = false;
	
	/**
	 * Subclasses should return the parameter name(s) for ID numbers of selected
	 * objects, such as "selModemID" for modems or "selBatchID" for batches. If
	 * you support more than one type of parameter name, return a collection of
	 * those types that you do support (for example, "selTagID" and
	 * "selMmodemID" if you allow for a list of modems that may come from tags
	 * or individual modem selections).
	 */
	protected abstract Collection<String> getSelectedParameterNames();
	
	/**
	 * Since ID numbers come in different types (integers for modems, longs for
	 * jobs, BigIntegers for packages, ...) the subclass is responsible for
	 * parsing ID numbers into the appropriate type. In most cases, this will
	 * return a collection containing only one value (ArrayList is recommended
	 * in this case; see {@link Utils#makeArrayList(Object...)}). But, if the ID
	 * is for a group type (like Tags) and you are collecting object IDs for the
	 * elements of that group (like Modems), you may return the full collection
	 * of matches.
	 * 
	 * @param type The type of ID (guaranteed to be one of the values returned
	 *   by {@link #getIDParameterName()} or
	 *   {@link #getSelectedParameterNames()}).
	 * @param id The string representation of the ID number.
	 * @return A collection of Number subclasses that hold the ID number(s).
	 * @throws NumberFormatException If the string is not a valid ID for this
	 *   type.
	 * @throws DatabaseException If a database access was required but failed.
	 * @throws Exception If anything else unexpected happens.
	 */
	@Override
    protected abstract Collection<E> parseID(String type, String id)
		throws NumberFormatException, DatabaseException, Exception;
	
	
	/**
	 * Prepare selected IDs. This will first grab {@link #selectedIDsOff} from
	 * the session, and if that is <tt>null</tt>, it will create a new one.
	 * Then it will try to parse each of the selected ID types that the subclass
	 * asks for and add them to the set of selected IDs. This will <i>not</i>
	 * populate {@link #selectedIDsOn}, as it is expected that the action does
	 * not yet have enough information to return meaningful information from its
	 * {@link #getPageIDs()} function.
	 */
    @Override
    public void prepare() throws Exception {
		super.prepare();
		
		// This is not populated until prepareSelectedIDs() is called
		selectedIDsOn = null;
		
		selectedIDsOff = new LinkedHashSet<E>();
		
		// Check the selection mode
		String selectionMode = getRequest().getParameter("selection");
		
		// If it is null, then we have just now navigated to a new page
		// Keep the selection
		if (selectionMode == null) selectionMode = "keep";
		
		logger.debug("Selection mode is '" + selectionMode + "'");
		
		if (selectionMode.equals("keep")) {
    		// Search for passed-in object IDs
    		// Add them to selectedIDsOff
    		// The split out between off/on will happen later, when we decide
			//   what will be included on the current page
    		for (String paramName : getSelectedParameterNames()) {
        		selectedIDsOff.addAll(parseParameters(paramName));
    		}
    		
    		logger.debug("After parsing input, there are " +
    		             selectedIDsOff.size() +
    		             " object IDs in 'off'");
		} else if (selectionMode.equals("none")) {
			// User asked to select none, so just don't populate the collection
			logger.debug("Cleared all selections");
		} else if (selectionMode.equals("all")) {
			// User asked to select all, but we can't be sure that we know what
			//   'all' is at this point, so just set a flag for now
			selectAll = true;
			
			logger.debug("Signaled all objects to be selected");
		}
	}
	
	/**
	 * Actions should call this to slice {@link #selectedIDsOff} for populating
	 * {@link #selectedIDsOn} AFTER doing enough work such that
	 * {@link #getPageIDs()} will return the correct value.
	 */
	public void prepareSelectedIDs() {
		selectedIDsOn = new HashSet<E>();
		
		for (E id : getPageIDs()) {
			if (selectedIDsOff.contains(id)) {
				selectedIDsOff.remove(id);
				selectedIDsOn.add(id);
			}
		}
	}
	
	/**
	 * Is the given ID selected?
	 * 
	 * @param id The ID number to check.
	 * @return True = it is selected, false = it is not selected.
	 */
	public boolean isSelected(E id) {
		// Check selectedIDsOn first with short-circuiting, since it is more
		// likely to contain an ID that we are interested in
		return (selectedIDsOn.contains(id) ||
				selectedIDsOff.contains(id));
	}
	
	/**
	 * Get an iterable over the selected IDs that are not on the current page.
	 */
	public Iterable<E> getInvisibleSelections() {
		return selectedIDsOff;
	}
}

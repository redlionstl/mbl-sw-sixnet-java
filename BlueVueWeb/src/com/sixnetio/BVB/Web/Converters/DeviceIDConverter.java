/*
 * DeviceIDConverter.java
 *
 * Converts DeviceID objects to/from Strings.
 *
 * Jonathan Pearson
 * August 25, 2009
 *
 */

package com.sixnetio.BVB.Web.Converters;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.util.StrutsTypeConverter;

import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Utils;

public class DeviceIDConverter
		extends StrutsTypeConverter {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	@SuppressWarnings("unchecked")
	@Override
	public Object convertFromString(Map context, String[] values,
	                                Class toClass) {
		
		logger.debug("Parsing device ID '" + values[0] + "'");
		
		if (values[0] == null || values[0].length() == 0) {
			return null;
		}
		else {
			return DeviceID.parseDeviceID(values[0]);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String convertToString(Map context, Object o) {
		if (o == null) {
			return null;
		}
		else {
			return o.toString();
		}
	}
}

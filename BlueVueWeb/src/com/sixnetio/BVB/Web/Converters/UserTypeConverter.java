/*
 * UserTypeConverter.java
 *
 * Converts User.UserType objects to/from Strings.
 *
 * Jonathan Pearson
 * June 1, 2009
 *
 */

package com.sixnetio.BVB.Web.Converters;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import com.sixnetio.BVB.Common.User;

public class UserTypeConverter extends StrutsTypeConverter {
	@SuppressWarnings("unchecked")
	@Override
	public Object convertFromString(Map context, String[] values, Class toClass) {
		// This returns null if not found
		return User.UserType.find(values[0]);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String convertToString(Map context, Object o) {
		if (o == null) {
			return null;
		}
		else {
			if (o instanceof User.UserType) {
				User.UserType type = (User.UserType)o;
				return type.name();
			}
			else {
				return null;
			}
		}
	}
}

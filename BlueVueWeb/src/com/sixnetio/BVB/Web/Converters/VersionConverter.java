/*
 * VersionConverter.java
 *
 * Converts Version objects to/from Strings.
 *
 * Jonathan Pearson
 * April 30, 2009
 *
 */

package com.sixnetio.BVB.Web.Converters;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import com.sixnetio.util.Version;

public class VersionConverter extends StrutsTypeConverter {
	@SuppressWarnings("unchecked")
	@Override
	public Object convertFromString(Map context, String[] values, Class toClass) {
		if (values[0] == null || values[0].length() == 0) {
			return null;
		}
		else {
			return new Version(values[0]);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String convertToString(Map context, Object o) {
		if (o == null) {
			return null;
		}
		else {
			return o.toString();
		}
	}
}

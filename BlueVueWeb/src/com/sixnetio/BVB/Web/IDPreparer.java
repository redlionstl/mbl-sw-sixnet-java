/*
 * IDPreparer.java
 *
 * Provides functions for preparing a list of ID numbers. Subclasses provide
 * information about how those ID numbers are labeled, and how to retrieve the
 * corresponding database objects.
 *
 * Jonathan Pearson
 * July 9, 2009
 *
 */

package com.sixnetio.BVB.Web;

import java.util.*;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.Utils;

/**
 * Prepares ID numbers passed by the user with the help of the subclass.
 *
 * @author Jonathan Pearson
 * @param <E> The specific type of the ID numbers supported.
 */
public abstract class IDPreparer<E extends Number>
	extends GeneralAction
	implements Paginated, Preparable {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Starting row index of the current page. */
	public int pageStart = 0;
	
	/** Number of rows in a page, defaulted to the user's preferred page size. */
	public int pageSize;
	
	/**
	 * The list of ID numbers that have been passed in by the user, plus any
	 * automatically generated ID numbers that the action may have added through
	 * database filters. This will contain all ID numbers passed by the user,
	 * but actions should only add filtered ID numbers for the currently
	 * displayed page.
	 */
	protected List<E> matchingIDs;
	
	/**
	 * Subclasses should return the parameter name(s) for ID numbers, such as
	 * "modemID" for modems or "batchID" for batches. If you support more than
	 * one type of parameter name, return a collection of those types that you
	 * do support (for example, "tagID" and "modemID" if you allow for a list of
	 * modems that may come from tags or individual modem selections).
	 */
	protected abstract Collection<String> getIDParameterNames();
	
	/**
	 * Since ID numbers come in different types (integers for modems, longs for
	 * jobs, BigIntegers for packages, ...) the subclass is responsible for
	 * parsing ID numbers into the appropriate type. In most cases, this will
	 * return a collection containing only one value (ArrayList is recommended
	 * in this case; see {@link Utils#makeArrayList(Object...)}). But, if the ID
	 * is for a group type (like Tags) and you are collecting object IDs for the
	 * elements of that group (like Modems), you may return the full collection
	 * of matches.
	 * 
	 * @param type The type of ID (guaranteed to be one of the values returned
	 *   by {@link #getIDParameterName()}).
	 * @param id The string representation of the ID number.
	 * @return A collection of Number subclasses that hold the ID number(s).
	 * @throws NumberFormatException If the string is not a valid ID for this
	 *   type.
	 * @throws DatabaseException If a database access was required but failed.
	 * @throws Exception If anything else unexpected happens.
	 */
	protected abstract Collection<E> parseID(String type, String id)
		throws NumberFormatException, DatabaseException, Exception;
	
	/**
	 * Get an iterable over the actual objects that the ID numbers represent.
	 * 
	 * @throws Exception If something unexpected occurs while preparing the
	 *   data.
	 */
	public abstract Iterable<?> getItems() throws Exception;
	
	/**
	 * Prepare matching IDs. This will first grab <tt>matchingIDs</tt> from the
	 * session, and if that is <tt>null</tt>, it will create a new one. Then it
	 * will try to parse each of the ID types that the subclass asks for and add
	 * them to the set of matching IDs. This will verify that the user is logged
	 * in, but it will not verify any permissions.
	 */
	@Override
	public void prepare() throws Exception {
		verifyUserLoggedIn();
		
		logger.debug("URL: " + getRequest().getRequestURL() +
		             ", Class: " + getClass().getName());
		
		pageSize = Preferences.getInt("pagesize",
		                              getUser().preferences,
		                              25);
		
		matchingIDs = new LinkedList<E>();
		
		// Search for passed-in object IDs
		for (String paramName : getIDParameterNames()) {
			matchingIDs.addAll(parseParameters(paramName));
		}
		
		logger.debug("After processing input, there are " + matchingIDs.size() +
		             " object IDs in 'match'");
	}
	
	/**
	 * Provided as a convenience method for parsing groups of parameters into ID
	 * numbers.
	 * 
	 * @param paramName The name of the parameter to search for ID numbers.
	 * @return A list of ID numbers in the same order as they were passed in the
	 *   parameters.
	 * @throws DatabaseException If a database access was necessary by
	 *   {@link #parseID(String, String)}, but it failed.
	 * @throws Exception If anything else unexpected happens.
	 */
	protected List<E> parseParameters(String paramName)
		throws DatabaseException, Exception {
		
		List<E> ids = new LinkedList<E>();
		
		String[] idArray = getRequest().getParameterValues(paramName);
		if (idArray != null) {
			for (String idString : idArray) {
				String[] tokens;
				
				if (idString.startsWith("[") && idString.endsWith("]")) {
					// It's an array, parse it out
					logger.debug("Parameter '" + paramName +
					             "' passed in as array: " + idString);
					
					idString = idString.substring(1, idString.length() - 1);
					
					tokens = Utils.tokenize(idString, ",", false);
					
					logger.debug("Parsed " + tokens.length + " tokens from '" +
					             idString + "'");
				} else {
					// Single value
					logger.debug("Parameter '" + paramName +
					             "' passed in as singular value: " + idString);
					
					tokens = new String[] { idString };
				}
				
				for (String token : tokens) {
					logger.debug(String.format("Adding '%s' of type '%s'",
					                           token, paramName));
					
					try {
						Collection<E> idNumbers = parseID(paramName,
						                                  token);
						
						ids.addAll(idNumbers);
					} catch (NumberFormatException nfe) {
						logger.warn(String.format("User passed '%s', which " +
						                          "is not a valid %s",
						                          idString, token),
						            nfe);
					}
				}
			}
		}
		
		return ids;
	}
	
	/**
	 * Get the portion of matchingIDs that is not included on the current page.
	 * The default implementation subtracts {@link #getPageIDs()} from
	 * {@link #matchingIDs} and returns it as an ordered set.
	 */
	public Iterable<E> getInvisibleIDs() {
		Set<E> invisibleIDs = new LinkedHashSet<E>(matchingIDs);
		invisibleIDs.removeAll(getPageIDs());
		
		return invisibleIDs;
	}
	
	/**
	 * Get the total number of items, across all pages. Subclasses may want to
	 * override this, as the number of items in {@link #matchingIDs} (what it
	 * returns by default) may not be the total number across all pages.
	 */
	@Override
	public long getMatchCount() {
		return matchingIDs.size();
	}
	
	@Override
	public int getFirstPage() {
		return 0;
	}
	
	@Override
	public int getLastPage() {
		// Truncate to the page boundary row index that will include the last
		// value
		int last = (int)((getMatchCount() / pageSize) * pageSize);
		
		// If it matches up to before the truncation, the last value must be the
		// last row on the page, which would result in an empty page following
		// it; correct for that
		if (last == getMatchCount()) last -= pageSize;
		
		return last;
	}
	
	@Override
	public int getPreviousPage() {
		int previous = pageStart - pageSize;
		
		// If there is no previous page, return the first page
		if (previous < 0) return 0;
		
		return previous;
	}
	
	@Override
	public int getNextPage() {
		int next = pageStart + pageSize;
		
		// If there is no next page, return the current page
		if (next >= getMatchCount()) return pageStart;
		
		return next;
	}
	
	@Override
	public int getPageNumber() {
		return ((pageStart / pageSize) + 1);
	}
	
	@Override
	public int getTotalPages() {
		int total = (int)((getMatchCount() / pageSize) +
				(getMatchCount() % pageSize == 0 ? 0 : 1));
		
		// Avoid the "page 1 of 0" artifact
		if (total == 0) return 1;
		
		return total;
	}
	
	@Override
	public int getPageStart() {
		return pageStart;
	}
	
	@Override
	public int getPageSize() {
		return pageSize;
	}
	
	/**
	 * Get the slice of matchingIDs that is to be displayed on the current page.
	 * By default, this will return {@link #matchingIDs} if it is small enough
	 * to fit on a single page, and otherwise it will slice out a portion of
	 * {@link #matchingIDs} starting at {@link #getPageStart()} and running for
	 * {@link #getPageSize()} values.
	 */
	public List<E> getPageIDs() {
		if (matchingIDs.size() <= getPageSize()) {
	    	// It will fit on one page, so just return it as-is
	    	return matchingIDs;
	    } else {
	    	// It requires multiple pages, return the slice for the current page
	    	return Utils.slice(matchingIDs, getPageStart(), getPageSize());
	    }
	}
}

/*
 * LinkTranslator.java
 *
 * Implementors provide functionality to translate a link description into
 * a link to a specific action with arguments.
 *
 * Jonathan Pearson
 * May 28, 2009
 *
 */

package com.sixnetio.BVB.Web;

public interface LinkTranslator {
	/**
	 * Generates a link to the described page using the provided object for
	 * extra data.
	 * 
	 * @param link A page description such as "viewModem" or
	 *            "filterJobs/batchID".
	 * @param obj An object providing any extra data that may be necessary. For
	 *            the "viewModem" example, this should be a Modem object with a
	 *            modemID. For the "filterJobs/batchID" example, this should be
	 *            a Job object with a batchID.
	 * @return An absolute link to the specified page, or an empty string if the
	 *         necessary data to generate the link was not available from the
	 *         object (for example, the Job object had no associated batch ID)
	 *         or the page description is not recognized.
	 * @throws Exception If there is a problem translating the link.
	 */
	public String translateLink(String link, Object obj) throws Exception;
}

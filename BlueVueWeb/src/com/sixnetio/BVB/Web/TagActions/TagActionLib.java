/*
 * TagActionLib.java
 *
 * Provides some library functions common to tag actions.
 *
 * Jonathan Pearson
 * May 12, 2009
 *
 */

package com.sixnetio.BVB.Web.TagActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.ModemTag;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.SortColumns.TagColumn;
import com.sixnetio.BVB.Web.StatusProvider;
import com.sixnetio.util.Utils;

public class TagActionLib {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Translate a string describing sorting columns into a list of TagColumns.
	 * 
	 * @param columns The description of the sorting columns.
	 * @return A list that could be passed to
	 *         {@link Database#getMatchingTags(Integer, String, Integer, Integer, Integer, Integer, Integer, int, int, List, long[])}
	 *         .
	 */
	public static List<TagColumn> translateSort(String columns) {
		String[] cols = Utils.tokenize(columns, ",", false);
		List<TagColumn> sortOn = new Vector<TagColumn>(cols.length);
		
		for (String column : cols) {
			if (column.startsWith("!")) {
				sortOn.add(TagColumn.Reverse);
				column = column.substring(1);
			}
			
			sortOn.add(TagColumn.valueOf(column));
		}
		
		return sortOn;
	}
	
	/**
	 * Translate a sort list into a string.
	 * 
	 * @param sortOn The sort list to translate. If synchronization is
	 *            necessary, it must be handled by the caller.
	 * @return A descriptive string.
	 */
	public static String translateSort(List<TagColumn> sortOn) {
		if (sortOn == null) return "";
		
		StringBuilder builder = new StringBuilder();
		
		boolean skipComma = true;
		for (TagColumn column : sortOn) {
			if (skipComma) {
				skipComma = false;
			} else {
				builder.append(",");
			}
			
			if (column == TagColumn.Reverse) {
				builder.append("!");
				skipComma = true;
			} else {
				builder.append(column.name());
			}
		}
		
		return builder.toString();
	}
	
	/**
	 * Given an existing sort list, add a new primary column to it, shuffling
	 * the existing columns as necessary.
	 * 
	 * @param colName The new column to add.
	 * @param oldSortOn The existing sort list, or <tt>null</tt> if there wasn't
	 *            one. If synchronization is necessary, it must be handled by
	 *            the caller.
	 * @return A new sort list. If the new column was already the primary
	 *         column, reverses the sorting order. If it was already in the
	 *         list, moves it to the beginning in normal order. If it did not
	 *         exist, adds it at the beginning in normal order.
	 */
	public static String addSortColumn(String colName, List<TagColumn> oldSortOn) {
		TagColumn col;
		
		try {
			col = TagColumn.valueOf(colName);
		} catch (IllegalArgumentException iae) {
			logger.debug("Not a legal column name: '" + colName + "'", iae);
			
			return translateSort(oldSortOn);
		}
		
		// Copy the sortOn object, since we don't actually want to change it
		List<TagColumn> sortOn;
		
		if (oldSortOn == null) {
			// It's only going to receive one item
			sortOn = new ArrayList<TagColumn>(1);
		} else {
			sortOn = new ArrayList<TagColumn>(oldSortOn);
		}
		
		// Get the index of the column, to see if it's already part of the sort
		// order
		int index = sortOn.indexOf(col);
		
		// Whether to apply a 'Reverse' just before the column
		boolean reverse = false;
		
		// If the column was already in there...
		if (index != -1) {
			// Remove it
			sortOn.remove(index);
			
			// If there was a reverse just before it...
			if (index > 0 && sortOn.get(index - 1) == TagColumn.Reverse) {
				sortOn.remove(index - 1);
				
				reverse = true;
				index--; // So we can more easily tell if it was the first
				// column
			}
			
			// If it was the first sort column
			if (index == 0) {
				// Swap the reverse property
				reverse = !reverse;
			}
		}
		
		// Push the column onto the front of the sort order
		sortOn.add(0, col);
		
		// If reversing, push reverse onto the front also
		if (reverse) {
			sortOn.add(0, TagColumn.Reverse);
		}
		
		// Return the new sorting order
		return translateSort(sortOn);
	}
	
	/**
	 * Get the status of the given tag.
	 * 
	 * @param tag The tag.
	 * @return The status of the tag.
	 */
	public static String getTagStatus(ModemTag tag) {
		// Tags are always 'normal'
		return StatusProvider.S_NORMAL;
	}
}

/*
 * TagListAction.java
 *
 * Provides sortable, selectable, filterable, paginated lists of tags, and some actions
 * on the selected tags in those lists (deleting, etc).
 *
 * Jonathan Pearson
 * May 12, 2009
 *
 */

package com.sixnetio.BVB.Web.TagActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Database.SortColumns.TagColumn;
import com.sixnetio.BVB.Web.*;
import com.sixnetio.BVB.Web.Iterators.TagIterable;
import com.sixnetio.util.Utils;

public class TagListAction
	extends SelectionPreparer<Integer>
	implements Filterable, LinkTranslator, Sortable, TagStatusProvider {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// Criteria for searching out tags
	public Integer qTagID = null;
	public String qTagName = null;
	public Integer qLowPollingInterval = null;
	public Integer qHighPollingInterval = null;
	public Integer qLowPosition = null;
	public Integer qHighPosition = null;
	public Integer qModemID = null;
	
	private long matchCount = 0;
	
	// This is only set once, so no need to synchronize
	private List<TagColumn> sortOn = null;
	
	@Override
	public Map<String, String[]> getFilters() throws Exception {
		// To start, fill this with Title --> Filter, but only individual
		// filters
		LinkedHashMap<String, String> filters = new LinkedHashMap<String, String>();
		
		if (qTagID != null) {
			filters.put(getText("filter.tagID", Utils.makeLinkedList((Object)qTagID)), "qTagID=" +
			                                                                           qTagID);
		}
		
		if (qTagName != null) {
			filters.put(getText("filter.name", Utils.makeLinkedList((Object)qTagName)),
			            "qTagName=" + qTagName);
		}
		
		if (qLowPollingInterval != null) {
			filters.put(getText("filter.lowPollingInterval",
			                    Utils.makeLinkedList((Object)qLowPollingInterval)),
			            "qLowPollingInterval=" + qLowPollingInterval);
		}
		
		if (qHighPollingInterval != null) {
			filters.put(getText("filter.highPollingInterval",
			                    Utils.makeLinkedList((Object)qHighPollingInterval)),
			            "qHighPollingInterval=" + qHighPollingInterval);
		}
		
		if (qLowPosition != null) {
			filters.put(getText("filter.lowPosition", Utils.makeLinkedList((Object)qLowPosition)),
			            "qLowPosition=" + qLowPosition);
		}
		
		if (qHighPosition != null) {
			filters
			       .put(
			            getText("filter.highPosition", Utils.makeLinkedList((Object)qHighPosition)),
			            "qHighPosition=" + qHighPosition);
		}
		
		if (qModemID != null) {
			Modem modem = DB.getDB().getModem(qModemID);
			
			if (modem != null) {
				filters
				       .put(
				            getText("filter.modem", Utils.makeLinkedList((Object)modem.getLabel())),
				            "qModemID=" + qModemID);
			} else {
				filters.put(getText("filter.modemID", Utils.makeLinkedList((Object)qModemID)),
				            "qModemID=" + qModemID);
			}
		}
		
		return constructFilters(filters);
	}
	
	@Override
	public String getSortOn() {
		return TagActionLib.translateSort(sortOn);
	}
	
	@Override
	public void setSortOn(String columns) {
		sortOn = TagActionLib.translateSort(columns);
	}
	
	@Override
	public String addSortColumn(String colName) {
		return TagActionLib.addSortColumn(colName, sortOn);
	}
	
	/**
	 * The user specified the List action. Loads a list of all tags, based on
	 * the properties that the user provided.
	 * 
	 * @return{@link GeneralAction#LIST}.
	 * @throws Exception If there is a database access error.
	 */
	public String list() throws Exception {
		long[] matchCount = new long[1];
		matchingIDs = new LinkedList<Integer>(DB.getDB()
				.getMatchingTags(qTagID, qTagName,
				                 qLowPollingInterval, qHighPollingInterval,
				                 qLowPosition, qHighPosition, qModemID,
				                 getPageStart(), getPageSize(),
				                 sortOn, matchCount));
		
		this.matchCount = matchCount[0];
		
		if (selectAll) {
			// Can't just copy 'modemIDs', since that only contains the matches
			// for *this* page
			// We need the matches across *all* pages
			selectedIDsOff.addAll(DB.getDB()
			                      .getMatchingTags(qTagID, qTagName,
			                                       qLowPollingInterval,
			                                       qHighPollingInterval,
			                                       qLowPosition, qHighPosition,
			                                       qModemID,
			                                       0, -1, // Page start/size
			                                       sortOn, null)); // Sort/matches
		}
		
		prepareSelectedIDs();
		return LIST;
	}
	
	/**
	 * The user specified the Delete action. Verifies that the user has tag
	 * delete permission (Enhanced user) and asks for confirmation.
	 * 
	 * @return INPUT
	 * @throws Exception If the user does not have tag delete permission.
	 */
	public String delete() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.deletePermission"));
		
		if (matchingIDs.size() == 0) {
			if (selectedIDsOff.size() > 0) {
				// This is how IDs are passed in from the List page
				matchingIDs.addAll(selectedIDsOff);
				
				// Also, we must be going to a new view, so go to page 1
				pageStart = 0;
			} else {
    			// Select the first page
    			matchingIDs = new LinkedList<Integer>(DB.getDB()
    					.getMatchingTags(qTagID, qTagName,
    					                 qLowPollingInterval,
    					                 qHighPollingInterval,
    					                 qLowPosition, qHighPosition, qModemID,
    					                 0, getPageSize(),
    					                 sortOn, null));
    			
    			// But don't check their boxes
    			selectedIDsOff.clear();
    			selectAll = false;
			}
		}
		
		this.matchCount = matchingIDs.size();
		
		if (selectAll) {
			selectedIDsOff.addAll(matchingIDs);
		}
		
		prepareSelectedIDs();
		return INPUT;
	}
	
	/**
	 * The user confirmed a delete action. Delete the tags.
	 * 
	 * @return {@link GeneralAction#UPLEVEL}, or ERROR if no tag was specified.
	 * @throws Exception If the user does not have tag delete permission.
	 */
	public String realDelete() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.deletePermission"));
		
		if (selectedIDsOff.size() == 0) {
			addActionError(getText("error.deleteNoTag"));
			
			prepareSelectedIDs();
			return INPUT;
		}
		
		List<LogEntry> logEntries = new LinkedList<LogEntry>();
		
		for (int id : selectedIDsOff) {
			DB.getDB().deleteTag(id);
			
			logEntries.add(new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
			                            String.format("Deleted Tag #%d", id)));
		}
		
		DB.getDB().log(logEntries);
		
		// Redirect action, no need to prepare selections
		return LIST;
	}
	
	@Override
	public long getMatchCount() {
		return matchCount;
	}
	
	@Override
	public String translateLink(String link, Object obj) {
		if (obj == null || !(obj instanceof ModemTag)) return "";
		
		ModemTag tag = (ModemTag)obj;
		String docroot = getDocumentRoot();
		
		if (link.equals("viewTag")) {
			return String.format("%s/Tag/view?id=%d", docroot, tag.tagID);
			
		} else if (link.equals("viewConfig")) {
			return String.format("%s/Tag/viewConfig?id=%d", docroot, tag.tagID);
			
		} else if (link.equals("filterModems")) {
			return String.format("%s/Modems?qTagID=%d", docroot, tag.tagID);
			
		} else if (link.startsWith("filterTags/")) {
			String filterName = link.substring(link.indexOf('/') + 1);
			
			Integer qTagID = this.qTagID;
			String qTagName = this.qTagName;
			Integer qLowPollingInterval = this.qLowPollingInterval;
			Integer qHighPollingInterval = this.qHighPollingInterval;
			Integer qLowPosition = this.qLowPosition;
			Integer qHighPosition = this.qHighPosition;
			Integer qModemID = this.qModemID;
			
			if (filterName.equals("tagID")) {
				qTagID = tag.tagID;
				
			} else if (filterName.equals("tagName")) {
				qTagName = tag.name;
				
			} else if (filterName.startsWith("pollingInterval/")) {
				String range = filterName.substring(filterName.indexOf('/') + 1);
				int pollingRange = 0; // Default to exactly equal
				
				try {
					pollingRange = Integer.parseInt(range);
				} catch (NumberFormatException nfe) {
					logger.warn("Not a number: " + range, nfe);
				}
				
				qLowPollingInterval = tag.pollingInterval - pollingRange;
				qHighPollingInterval = tag.pollingInterval + pollingRange;
				
			} else if (filterName.startsWith("position/")) {
				String range = filterName.substring(filterName.indexOf('/') + 1);
				int positionRange = 2; // Default to +/- 2 positions
				
				try {
					positionRange = Integer.parseInt(range);
				} catch (NumberFormatException nfe) {
					logger.warn("Not a number: " + range, nfe);
				}
				
				// Select within positionRange positions
				qLowPosition = tag.position - positionRange;
				qHighPosition = tag.position + positionRange;
			} // Can't do Modem ID, since the tag could have more than one
			
			StringBuilder builder = new StringBuilder(docroot + "/Tags?");
			
			// A trailing '&' won't hurt anything, so just generate a link with
			// it for simplicity
			
			if (qTagID != null) {
				builder.append(String.format("qTagID=%d&", qTagID));
			}
			
			if (qTagName != null) {
				builder.append(String.format("qTagName=%s&", qTagName));
			}
			
			if (qLowPollingInterval != null) {
				builder.append(String.format("qLowPollingInterval=%d&",
				                             qLowPollingInterval));
			}
			
			if (qHighPollingInterval != null) {
				builder.append(String.format("qHighPollingInterval=%d&",
				                             qHighPollingInterval));
			}
			
			if (qLowPosition != null) {
				builder.append(String.format("qLowPosition=%d&", qLowPosition));
			}
			
			if (qHighPosition != null) {
				builder.append(String.format("qHighPosition=%d&",
				                             qHighPosition));
			}
			
			if (qModemID != null) {
				builder.append(String.format("qModemID=%d&", qModemID));
			}
			
			return builder.toString();
		}
		
		return "";
	}
	
	@Override
    protected Collection<String> getIDParameterNames() {
	    // Allow tag IDs to be passed around
		return Utils.makeArrayList("tagID");
    }
	
	@Override
    protected Collection<String> getSelectedParameterNames() {
	    // Allow tag IDs to be selected/passed in
		return Utils.makeArrayList("selTagID");
    }
	
	@Override
    protected Collection<Integer> parseID(String type, String id)
    	throws NumberFormatException, DatabaseException, Exception {
	    
		if (type.equals("tagID") || type.equals("selTagID")) {
			return Utils.makeArrayList(Integer.parseInt(id));
		} else {
			throw new IllegalArgumentException("Unrecognized type: " + type);
		}
    }
	
	@Override
    public Iterable<?> getItems() throws Exception {
	    return new TagIterable(getPageIDs());
    }

	@Override
    public String getStatus(Object obj) throws Exception {
	    return TagActionLib.getTagStatus((ModemTag)obj);
    }
}

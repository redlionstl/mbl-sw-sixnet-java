/*
 * TagStatusProvider.java
 *
 * Provides information about a tag.
 *
 * Jonathan Pearson
 * June 5, 2009
 *
 */

package com.sixnetio.BVB.Web.TagActions;

import com.sixnetio.BVB.Web.StatusProvider;

public interface TagStatusProvider extends StatusProvider {}

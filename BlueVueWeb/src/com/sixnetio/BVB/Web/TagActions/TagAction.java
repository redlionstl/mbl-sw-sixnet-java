/*
 * TagAction.java
 *
 * Loads a tag from the database and provides CRUD actions on it.
 *
 * Jonathan Pearson
 * June 5, 2009
 *
 */

package com.sixnetio.BVB.Web.TagActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Database.SortColumns.TagColumn;
import com.sixnetio.BVB.Modems.ScriptParser.ScriptCompiler;
import com.sixnetio.BVB.Modems.ScriptParser.ScriptFormatException;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class TagAction extends GeneralAction implements TagStatusProvider, Preparable {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// The ID of the tag that the user specified
	private int id = ModemTag.INV_TAG;
	
	// The actual tag, created here or loaded from the database (for
	// single-tag actions)
	private ModemTag tag;
	
	/**
	 * Get the ID number of the tag that the user specified, or
	 * {@link ModemTag#INV_TAG} if no tag was specified.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Get the tag.
	 */
	public ModemTag getTag() {
		return tag;
	}
	
	/**
	 * The user specified the View action.
	 * 
	 * @return {@link GeneralAction#VIEW}.
	 * @throws Exception If the user is not logged in.
	 */
	public String view() throws Exception {
		verifyUserLoggedIn();
		
		return VIEW;
	}
	
	/**
	 * The user specified the Edit action. Verifies that the user has tag edit
	 * permission (Enhanced user) and asks for more input.
	 * 
	 * @return INPUT.
	 * @throws Exception If the user does not have tag edit permission.
	 */
	public String edit() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.updatePermission"));
		
		return INPUT;
	}
	
	public String create() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.createPermission"));
		
		return INPUT;
	}
	
	/**
	 * The user has finished editing a tag. Verifies that the user has tag edit
	 * permission (Enhanced user), saves the tag, and redirects to the View
	 * page.
	 * 
	 * @return {@link GeneralAction#VIEW}.
	 * @throws Exception If the user does not have tag edit permission, or there
	 *             is a database exception trying to save the tag.
	 */
	public String update() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.updatePermission"));
		
		// Check that there is a name
		if (prepareParam(tag.name) == null) {
			addActionError(getText("error.emptyTagName"));
			return INPUT;
		}
		
		// Check if the user specified a position
		if (tag.position != -1) {
			// Yes... We need to clear a space for this
			freePosition(id, tag.position);
		}
		
		LogEntry logEntry;
		
		if (id == ModemTag.INV_TAG) {
			// User is creating a new tag
			DB.getDB().createTag(tag);
			id = tag.tagID;
			
			logEntry =
			        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
			                     String.format("Created Tag #%d", id));
		} else {
			DB.getDB().updateTag(tag);
			
			logEntry =
			        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
			                     String.format("Updated Tag #%d", id));
		}
		
		DB.getDB().log(Utils.makeLinkedList(logEntry));
		
		return VIEW;
	}
	
	/**
	 * Free the given position for a tag to be placed there.
	 * 
	 * @param tagID If the position contains this tag ID, no special work will
	 *            be done. If this tag ID is encountered while shifting
	 *            positions, it will be moved into the correct location. Pass
	 *            {@link ModemTag#INV_TAG} to just clear the given position.
	 * @param pos The position to free.
	 * @throws Exception If there is a problem modifying the database.
	 */
	private void freePosition(int tagID, int pos) throws Exception {
		List<TagColumn> sortOn = new ArrayList<TagColumn>(1);
		sortOn.add(TagColumn.Position);
		
		List<Integer> tagIDs =
		        DB.getDB().getMatchingTags(null, null, null, null, pos, null, null, 0, -1, sortOn,
		                                   null);
		// tagIDs[0] is at position pos or later
		
		// This is the list of tags that we have modified
		List<ModemTag> updatedTags = new LinkedList<ModemTag>();
		
		// Loop through the tags until we have shifted enough (maintaining the
		// current order) that there is a free space at 'pos'
		int curPos = pos;
		for (int curTagID : tagIDs) {
			ModemTag curTag = DB.getDB().getTag(curTagID);
			
			// If we hit the current tag, we don't need to keep shifting, since
			// it will move into that free space
			if (curTagID == tagID) {
				// Great, we're done! Just update that tag and stop
				curTag.position = pos;
				updatedTags.add(curTag);
				break;
			}
			
			// If the tag is in the position we expected to see next, move it
			// further along and update the position we expect to see next
			if (curTag.position == curPos) {
				curTag.position++;
				curPos++;
				
				updatedTags.add(curTag);
			} else {
				// If the tag is not in the expected position, there must be a
				// gap, which means we don't need to keep shifting
				break;
			}
		}
		
		// Now we need to update all of those positions, without violating the
		// uniqueness constraint...
		// Sort by position
		ModemTag[] tags = updatedTags.toArray(new ModemTag[updatedTags.size()]);
		Arrays.sort(tags, new Comparator<ModemTag>() {
			@Override
			public int compare(ModemTag o1, ModemTag o2) {
				return (o1.position - o2.position);
			}
		});
		
		// If the one in [0] is the tag we are making space for, then it used to
		// have the highest position
		if (tags.length > 0 && tags[0].tagID == tagID) {
			// Move its position to a temporary value so we have space to shift
			// the rest of the tags
			tags[0].position = Integer.MAX_VALUE;
			
			try {
				DB.getDB().updateTag(tags[0]);
			} catch (DatabaseException de) {
				throw new Exception(getText("error.tagWithMaxPosition"));
			}
			
			// Reset to where it will belong at the end
			tags[0].position = pos;
		}
		
		// Shift from the highest down
		List<LogEntry> logEntries = new LinkedList<LogEntry>();
		
		for (int i = tags.length - 1; i >= 0; i--) {
			DB.getDB().updateTag(tags[i]);
			
			logEntries.add(new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
			                            String.format("Shifted position of Tag #%d to %d",
			                                          tags[i].tagID, tags[i].position)));
		}
	}
	
	/**
	 * The user has asked to view a configuration script. Verifies that the user
	 * is logged in and redirects to the View page.
	 * 
	 * @return VIEW.
	 * @throws Exception If the user is not logged in.
	 */
	public String viewConfig() throws Exception {
		verifyUserLoggedIn();
		
		return VIEW;
	}
	
	/**
	 * The user wants to edit a configuration script. Verifies that the user has
	 * tag script editing permission (Enhanced user), and returns INPUT.
	 * 
	 * @return INPUT.
	 * @throws Exception If the user does not have tag script editing
	 *             permission.
	 */
	public String editConfig() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.updateScriptPermission"));
		
		return INPUT;
	}
	
	/**
	 * The user has finished editing a script. Verifies that the user has script
	 * editing permission and that the script compiles, and then redirects to
	 * the VIEW page.
	 * 
	 * @return VIEW, or INPUT if the script does not compile.
	 * @throws Exception If the user does not have tag script editing
	 *             permission.
	 */
	public String updateConfig() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.updateScriptPermission"));
		
		if (tag.configScript.trim().length() == 0) tag.configScript = null;
		
		logger.debug("Config script for tag " + tag.tagID + " set to '" + tag.configScript + "'");
		
		if (tag.configScript != null) {
			// Compile the script to make sure it's legal
			try {
				ScriptCompiler.checkScript(tag.configScript);
			} catch (ScriptFormatException sfe) {
				addActionError(getText("error.cannotCompile"));
				
				for (String msg : sfe.getMessages()) {
					addActionError(msg);
				}
				
				return INPUT;
			}
		}
		
		DB.getDB().updateTag(tag);
		
		LogEntry logEntry =
		        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
		                     String.format("Updated configuration script of Tag #%d", id));
		DB.getDB().log(Utils.makeLinkedList(logEntry));
		
		return VIEW;
	}
	
	@Override
	public void prepare() throws Exception {
		String sID = getRequest().getParameter("id");
		
		if (sID != null && !sID.equals("-1")) {
			try {
				id = Integer.parseInt(sID);
				
				logger.debug("User specified tag with ID " + id);
				
				tag = DB.getDB().getTag(id);
			} catch (NumberFormatException nfe) {
				throw new Exception(getText("error.invalidTagID", new String[] {
					sID
				}), nfe);
			}
		} else {
			logger.debug("No tag specified, creating a new one");
			
			tag = new ModemTag(ModemTag.INV_TAG, -1, "", null, 3600);
		}
	}
	
	@Override
	public String getStatus(Object obj) {
		return S_NORMAL;
	}
}

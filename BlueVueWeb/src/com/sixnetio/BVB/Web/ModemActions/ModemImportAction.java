/*
 * ModemImportAction.java
 *
 * Reads a CSV file for modem properties and creates/updates modems accordingly.
 *
 * Jonathan Pearson
 * June 18, 2009
 *
 */

package com.sixnetio.BVB.Web.ModemActions;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class ModemImportAction extends GeneralAction {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// For uploading files
	public File csvFile;
	
	// If the user wants to apply a tag to all modems
	public int tagID = ModemTag.INV_TAG;
	
	// If the user wants to create status queries for all modems
	public boolean createJobs;
	
	// The batch to put those jobs into
	public String jobBatchName;
	
	public String importModems() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.createModemsPermission"));
		
		return INPUT;
	}
	
	public String realImportModems() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.createModemsPermission"));
		
		// Make sure the user provided a file
		if (csvFile == null) {
			addActionError(getText("error.noFile"));
			return INPUT;
		} else if (!csvFile.isFile()) {
			addActionError(getText("error.notAFile"));
			return INPUT;
		} else if (!csvFile.canRead()) {
			addActionError(getText("error.cannotRead"));
			return INPUT;
		}
		
		// Fill this as we parse, but don't create until we're sure the input is
		//   good
		List<Modem> modemsToCreate = new LinkedList<Modem>();
		
		// Read the file
		LineNumberReader in = new LineNumberReader(new FileReader(csvFile));
		try {
			CSVParser parser = new CSVParser(in);
			parser.addColumns(Utils.tokenize(in.readLine(), ",", true));
			
			// Verify that the columns provided are good
			Set<String> expectedColumns = Utils
				.makeHashSet("DeviceID", "Alias", "Model", "FirmwareRev",
				             "IPAddress", "Port", "Phone");
			
			Set<String> requiredColumns = Utils.makeHashSet("IPAddress");
			
			if (!validateCSVParser(parser, expectedColumns, requiredColumns)) {
				return INPUT;
			}
			
			// Parse the rest of the file
			while (parser.safeNext()) {
				for (String warning : parser.getWarnings()) {
					addActionError(warning);
				}
				parser.getWarnings().clear();
				
				// Make sure to trim everything, since hand-edited CSV files are
				//   prone to "value, value" instead of the machine-generated
				//   "value,value"
				String sDevID = prepareParam(parser.getColumn("DeviceID"));
				String sAlias = prepareParam(parser.getColumn("Alias"));
				String sModel = prepareParam(parser.getColumn("Model"));
				String sFWVer = prepareParam(parser.getColumn("FirmwareRev"));
				String sIPAddr = prepareParam(parser.getColumn("IPAddress"));
				String sPort = prepareParam(parser.getColumn("Port"));
				String sPhone = prepareParam(parser.getColumn("Phone"));
				
				// Require the IP address
				if (sIPAddr == null) {
					addActionError(getText("error.ipAddressRequired",
					                       Utils.makeArrayList((Object)Integer
					                           .valueOf(parser.getLineNumber()))));
					
					continue;
				}
				
				// Verify that the IP address is valid
				try {
					InetAddress.getByName(sIPAddr);
				} catch (UnknownHostException uhe) {
					addActionError(getText("error.badIPAddress",
					                       Utils.makeArrayList((Object)Integer
					                           .valueOf(parser.getLineNumber()))));
					
					continue;
				}
				
				DeviceID devID = null;
				if (sDevID != null) {
					try {
						devID = DeviceID.parseDeviceID(sDevID);
					} catch (NumberFormatException nfe) {
						addActionError(getText("error.badDeviceID",
						                       Utils.makeArrayList((Object)Integer
						                           .valueOf(parser.getLineNumber()))));
						
						continue;
					}
				}
				
				Version fwVer = new Version(sFWVer);
				
				int port = Modem.PORT_STANDARD;
				if (sPort != null) {
					try {
						port = Integer.parseInt(sPort);
					} catch (NumberFormatException nfe) {
						addActionError(getText("error.badPort",
						                       Utils.makeArrayList((Object)Integer
						                           .valueOf(parser.getLineNumber()))));
						
						continue;
					}
				}
				
				Modem modem =
				        new Modem(Modem.INV_MODEM, devID, sAlias, sModel,
				                  fwVer, null, sIPAddr, port, sPhone,
				                  new Tag("modem"));
				modemsToCreate.add(modem);
			}
		} finally {
			in.close();
		}
		
		// Do we have errors? If so, stop now
		if (getActionErrors().size() > 0) {
			return INPUT;
		}
		
		List<LogEntry> logEntries = new LinkedList<LogEntry>();
		
		Integer batchID = null;
		
		jobBatchName = prepareParam(jobBatchName);
		if (createJobs && jobBatchName != null) {
			// Check if the batch exists already
			Collection<Integer> batchIDs = DB.getDB()
				.getMatchingBatches(null, null, jobBatchName, false,
				                    0, -1, // Page start/size
				                    null, null); // Sort/match count
			
			if (batchIDs.size() >= 1) {
				// Choose the first and re-use it
				batchID = batchIDs.iterator().next();
			} else {
				// No match, create a new one
				JobBatch batch = null;
				
    			batch = new JobBatch(JobBatch.INV_BATCH, getUser().userID,
    			                     jobBatchName);
    			DB.getDB().createBatch(batch);
    			
    			logEntries.add(new LogEntry(new Date(), getUser().userID,
    			                            DBLogger.INFO,
    			                            String.format("Created Batch #%d",
    			                                          batch.batchID)));
    			
    			batchID = batch.batchID;
			}
		}
		
		// Create the modems
		for (Modem modem : modemsToCreate) {
			try {
    			DB.getDB().createModem(modem);
    			
    			logEntries.add(new LogEntry(new Date(), getUser().userID,
    			                            DBLogger.INFO,
    			                            String.format("Created Modem #%d",
    			                                          modem.modemID)));
			} catch (DatabaseException de) {
				logger.debug("Error creating modem", de);
				
				addActionError(getText("error.cannotCreate",
				                       Utils.makeArrayList((Object)modem.ipAddress,
				                                           "" + modem.port)));
				
				// At this point, we cannot just stop and display the errors,
				//   since we've already started modifying the database
				// Just see it through and display the errors at the end
				
				continue;
			}
			
			// If there was a tag specified, apply it
			if (tagID != ModemTag.INV_TAG) {
				DB.getDB().applyTag(modem.modemID, tagID);
				
				logEntries.add(new LogEntry(new Date(), getUser().userID,
				                            DBLogger.INFO,
				                            String.format("Applied Tag #%d to Modem #%d",
				                                          tagID,
				                                          modem.modemID)));
			}
			
			// If we are supposed to create automatic status queries, create one
			if (createJobs) {
				Job.JobData data =
				        new Job.JobData(Job.JobData.INV_JOB, batchID,
				                        modem.modemID, getUser().userID,
				                        new Date(), 0, "Status", new Tag("job"),
				                        true);
				Job job = Job.makeGenericJob(data);
				DB.getDB().createJob(job);
				
				logEntries.add(new LogEntry(new Date(), getUser().userID,
				                            DBLogger.INFO,
				                            String.format("Created Job #%d",
				                                          data.jobID)));
			}
		}
		
		DB.getDB().log(logEntries);
		
		if (getActionErrors().size() > 0) {
			return INPUT;
		}
		
		return LIST;
	}
	
	public String importVars() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.updateVariablesPermission"));
		
		return INPUT;
	}
	
	public String realImportVars() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.updateVariablesPermission"));
		
		// Make sure the user provided a file
		if (csvFile == null) {
			addActionError(getText("error.noFile"));
			return INPUT;
		} else if (!csvFile.isFile()) {
			addActionError(getText("error.notAFile"));
			return INPUT;
		} else if (!csvFile.canRead()) {
			addActionError(getText("error.cannotRead"));
			return INPUT;
		}
		
		List<LogEntry> logEntries = new LinkedList<LogEntry>();
		
		// Read the file
		LineNumberReader in = new LineNumberReader(new FileReader(csvFile));
		try {
			CSVParser parser = new CSVParser(in);
			parser.addColumns(Utils.tokenize(in.readLine(), ",", true));
			
			// Unfortunately, we cannot use the validateCSVParser() function,
			//   since we simply have no idea what columns will be provided
			
			while (parser.safeNext()) {
				try {
					for (String warning : parser.getWarnings()) {
						addActionError(warning);
					}
					parser.getWarnings().clear();
					
					// Check for which modem properties are being used for
					// identification
					String sModemID = prepareParam(parser.getColumn("ModemID"));
					String sDevID = prepareParam(parser.getColumn("DeviceID"));
					String sIPAddr = prepareParam(parser.getColumn("IPAddress"));
					
					if (sModemID == null && sDevID == null && sIPAddr == null) {
						// Ignore the blank line
						continue;
					}
					
					Integer modemID = null;
					if (sModemID != null) modemID = Integer.parseInt(sModemID);
					
					DeviceID deviceID = null;
					if (sDevID != null) deviceID = DeviceID.parseDeviceID(sDevID);
					
					Collection<Integer> modemIDs =
					        DB.getDB().getMatchingModems(modemID, deviceID, null, null, null, null,
					                                     sIPAddr, null, null, null, 0, -1, null,
					                                     null);
					
					if (modemIDs.size() == 0) {
						addActionError(getText(
						                       "warning.cannotFindModem",
						                       Utils
						                            .makeLinkedList((Object)Integer
						                                                           .valueOf(in
						                                                                      .getLineNumber()))));
						continue;
					} else if (modemIDs.size() > 1) {
						addActionError(getText(
						                       "warning.notUnique",
						                       Utils
						                            .makeLinkedList((Object)Integer
						                                                           .valueOf(in
						                                                                      .getLineNumber()))));
						continue;
					}
					
					Modem modem = DB.getDB().getModem(modemIDs.iterator().next());
					
					// Update the specified properties
					for (String column : parser.getColumns()) {
						if (column.equals("ModemID") || column.equals("DeviceID") ||
						    column.equals("IPAddress")) continue;
						
						Tag variablesTag = modem.status.getTag("variables");
						if (variablesTag == null) {
							variablesTag = new Tag("variables");
							modem.status.addContent(variablesTag);
						}
						
						boolean found = false;
						for (Tag varTag : variablesTag.getEachTag("", "variable")) {
							if (varTag.getStringAttribute("name").equals(column)) {
								varTag.removeContents();
								varTag.addContent(parser.getColumn(column));
								
								logEntries
								          .add(new LogEntry(
								                            new Date(),
								                            getUser().userID,
								                            DBLogger.INFO,
								                            String
								                                  .format(
								                                          "Updated variable '%s' on Modem #%d",
								                                          column, modem.modemID)));
								
								found = true;
								break;
							}
						}
						
						if (!found) {
							Tag varTag = new Tag("variable");
							varTag.setAttribute("name", column);
							varTag.addContent(parser.getColumn(column));
							
							variablesTag.addContent(varTag);
							
							logEntries
							          .add(new LogEntry(
							                            new Date(),
							                            getUser().userID,
							                            DBLogger.INFO,
							                            String
							                                  .format(
							                                          "Created variable '%s' on Modem #%d",
							                                          column, modem.modemID)));
						}
					}
					
					DB.getDB().updateModem(modem);
				} catch (Exception e) {
					logger.error("Error while updating variables", e);
					
					addActionError(e.getMessage());
				}
			}
			
			return LIST;
		} finally {
			in.close();
			
			DB.getDB().log(logEntries);
		}
	}
	
	/**
	 * Get a listing of all tags in the system.
	 */
	public Iterable<ModemTag> getAllTags() throws Exception {
		return ModemActionLib.getAllTags();
	}
}

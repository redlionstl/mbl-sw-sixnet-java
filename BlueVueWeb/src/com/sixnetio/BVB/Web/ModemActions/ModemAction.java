/*
 * ModemAction.java
 *
 * Loads a modem from the database and provides CRUD actions on it.
 *
 * Jonathan Pearson
 * April 15, 2009
 *
 */

package com.sixnetio.BVB.Web.ModemActions;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Database.SortColumns.TagColumn;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Modems.ScriptParser.ScriptCompiler;
import com.sixnetio.BVB.Modems.ScriptParser.ScriptFormatException;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.BVB.Web.Iterators.TagIterable;
import com.sixnetio.util.Utils;

public class ModemAction
    extends GeneralAction
    implements Preparable, ModemStatusProvider
{

    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    // The ID of the modem that the user specified
    private int id = Modem.INV_MODEM;

    // The actual modem, created here or loaded from the database (for
    // single-modem actions)
    private Modem modem;

    // Contains the most recent status history for each modem
    // Modem ID --> (Property Name --> Value)
    // Used in a single-threaded environment
    protected Map<Integer, Map<String, String>> recentStatus =
        new HashMap<Integer, Map<String, String>>();

    // Contains the date of the most recent status history for each modem
    // Used in a single-threaded environment
    protected Map<Integer, Date> contactDates = new HashMap<Integer, Date>();

    // If setting a user-defined variable
    public String variableName;
    public String variableValue;

    public PasswordSettings passwordSettings;

    // If creating a new modem
    public int applyTag; // ID of tag to apply

    // True = schedule an automatic status query
    public boolean scheduleJob = true;

    /**
     * Get the ID number of the modem that the user specified, or
     * {@link Modem#INV_MODEM} if no modem was specified.
     */
    public int getId()
    {
        return id;
    }

    /** Get the modem. */
    public Modem getModem()
    {
        return modem;
    }

    public String getConfigScript()
    {
        return modem.getProperty("privateConfig");
    }

    public void setConfigScript(String configScript)
    {
        if (configScript.trim().length() == 0) {
            modem.deleteProperty("privateConfig");
        }
        else {
            modem.setProperty("privateConfig", configScript);
        }
    }

    public Map<String, String> getVariables()
    {
        Map<String, String> variables = new HashMap<String, String>();

        for (Tag variable : modem.status.getEachTag("variables", "variable")) {
            variables.put(variable.getStringAttribute("name"), variable
                .getStringContents());
        }

        return variables;
    }

    /**
     * The user specified the Create action. Verifies that the user has
     * permission to create a modem (Enhanced user) and returns
     * {@link GeneralAction#EDIT}.
     * 
     * @return {@link GeneralAction#EDIT}.
     * @throws Exception If the user does not have permission to create modems.
     */
    public String create()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.createPermission"));

        return INPUT;
    }

    /**
     * The user specified the View action.
     * 
     * @return {@link GeneralAction#VIEW}.
     * @throws Exception Never.
     */
    public String view()
        throws Exception
    {
        verifyUserLoggedIn();

        ModemActionLib.populateRecentStatus(recentStatus, contactDates, this,
            Utils.makeHashSet(id));

        return VIEW;
    }

    /**
     * The user specified the Edit action. Verifies that the user has modem edit
     * permission (Enhanced user) and asks for more input.
     * 
     * @return INPUT.
     * @throws Exception If the user does not have modem edit permission.
     */
    public String edit()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.updatePermission"));

        ModemActionLib.populateRecentStatus(recentStatus, contactDates, this,
            Utils.makeHashSet(id));

        return INPUT;
    }

    /**
     * The user has finished editing a modem. Verifies that the user has modem
     * edit permission (Enhanced user), saves the modem, and redirects to the
     * View page.
     * 
     * @return {@link GeneralAction#VIEW}.
     * @throws Exception If the user does not have modem edit permission, or
     *             there is a database exception trying to save the modem.
     */
    public String update()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.updatePermission"));

        modem.ipAddress = prepareParam(modem.ipAddress);

        if (modem.ipAddress == null && modem.deviceID == null) {
            addActionError(getText("error.ipAddressOrDeviceIDRequired"));
            return INPUT;
        }

        // Validate the IP address
        if (modem.ipAddress != null) {
            try {
                InetAddress.getByName(modem.ipAddress);
            }
            catch (UnknownHostException uhe) {
                addActionError(getText("error.badIPAddress"));
                return INPUT;
            }
        }

        modem.status.removeTag("passwordSettings");
        modem.status.addContent(passwordSettings.asTag());

        List<LogEntry> logEntries = new LinkedList<LogEntry>();
        if (id == Modem.INV_MODEM) {
            try {
                DB.getDB().createModem(modem);
            }
            catch (DatabaseException de) {
                logger.error("Unable to create modem", de);

                // Make this message as helpful as possible
                if (modem.ipAddress != null && modem.deviceID != null) {
                    addActionError(getText("error.cannotCreateBoth", Utils
                        .makeArrayList((Object)modem.ipAddress,
                            "" + modem.port, (Object)modem.deviceID.toString())));
                }
                else if (modem.ipAddress != null) {
                    addActionError(getText("error.cannotCreateIPAddress",
                        Utils
                            .makeArrayList((Object)modem.ipAddress, "" +
                                                                    modem.port)));
                }
                else if (modem.deviceID != null) {
                    addActionError(getText("error.cannotCreateDevID", Utils
                        .makeArrayList((Object)modem.deviceID.toString())));
                }

                // Don't worry about the 'else' case, we tested for both being
                // null above

                return INPUT;
            }

            id = modem.modemID;

            logEntries.add(new LogEntry(new Date(), getUser().userID,
                DBLogger.INFO, String.format("Created Modem #%d", id)));

            if (applyTag != -1) {
                DB.getDB().applyTag(id, applyTag);

                logEntries.add(new LogEntry(new Date(), getUser().userID,
                    DBLogger.INFO, String.format(
                        "Applied Tag #%d to Modem #%d", applyTag, id)));
            }

            if (scheduleJob) {
                Job.JobData data =
                    new Job.JobData(Job.JobData.INV_JOB, null, id,
                        getUser().userID, new Date(), 0, "Status", new Tag(
                            "job"), true, null, null);

                // If there is no IP address for the modem, make it an
                // always-available mobile-originated job
                if (modem.ipAddress == null) {
                    Tag mo = new Tag("mobileOriginated");
                    mo.setAttribute("alwaysAvailable", true);
                    mo.addContent("" + true);

                    data.parameters.addContent(mo);
                }

                Job job = Job.makeGenericJob(data);
                DB.getDB().createJob(job);

                logEntries.add(new LogEntry(new Date(), getUser().userID,
                    DBLogger.INFO, String.format(
                        "Created automatic Status Job #%d against Modem #%d",
                        job.getJobData().jobID, id)));
            }
        }
        else {
            try {
                DB.getDB().updateModem(modem);
            }
            catch (DatabaseException de) {
                logger.error("Unable to update modem", de);

                // Make this message as helpful as possible
                if (modem.ipAddress != null && modem.deviceID != null) {
                    addActionError(getText("error.cannotUpdateBoth", Utils
                        .makeArrayList((Object)modem.ipAddress,
                            "" + modem.port, (Object)modem.deviceID.toString())));
                }
                else if (modem.ipAddress != null) {
                    addActionError(getText("error.cannotUpdateIPAddress",
                        Utils
                            .makeArrayList((Object)modem.ipAddress, "" +
                                                                    modem.port)));
                }
                else if (modem.deviceID != null) {
                    addActionError(getText("error.cannotUpdateDevID", Utils
                        .makeArrayList((Object)modem.deviceID.toString())));
                }

                // Don't worry about the 'else' case, we tested for both being
                // null above

                return INPUT;
            }

            logEntries.add(new LogEntry(new Date(), getUser().userID,
                DBLogger.INFO, String.format("Updated Modem #%d", id)));
        }

        DB.getDB().log(logEntries);

        ModemActionLib.populateRecentStatus(recentStatus, contactDates, this,
            Utils.makeHashSet(id));

        return VIEW;
    }

    /**
     * The user has asked to view a configuration script. Verifies that the user
     * is logged in and redirects to the View page.
     * 
     * @return VIEW.
     * @throws Exception If the user is not logged in.
     */
    public String viewConfig()
        throws Exception
    {
        verifyUserLoggedIn();

        return VIEW;
    }

    /**
     * The user wants to edit a configuration script. Verifies that the user has
     * modem script editing permission (Enhanced user), and returns INPUT.
     * 
     * @return INPUT.
     * @throws Exception If the user does not have modem script editing
     *             permission.
     */
    public String editConfig()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.updateScriptPermission"));

        return INPUT;
    }

    /**
     * The user has finished editing a script. Verifies that the user has script
     * editing permission and that the script compiles, and then redirects to
     * the VIEW page.
     * 
     * @return VIEW, or INPUT if the script does not compile.
     * @throws Exception If the user does not have modem script editing
     *             permission.
     */
    public String updateConfig()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.updateScriptPermission"));

        logger.debug("Config script for modem " + modem.modemID + " set to '" +
                     getConfigScript() + "'");

        if (getConfigScript() != null) {
            // Compile the script to make sure it's legal
            try {
                ScriptCompiler.checkScript(getConfigScript());
            }
            catch (ScriptFormatException sfe) {
                addActionError(getText("error.cannotCompile"));

                for (String msg : sfe.getMessages()) {
                    addActionError(msg);
                }

                return INPUT;
            }
        }

        DB.getDB().updateModem(modem);

        LogEntry logEntry =
            new LogEntry(new Date(), getUser().userID, DBLogger.INFO, String
                .format("Updated configuration script for Modem #%d", id));
        DB.getDB().log(Utils.makeLinkedList(logEntry));

        return VIEW;
    }

    public String viewVariables()
        throws Exception
    {
        verifyUserLoggedIn();

        return VIEW;
    }

    public String editVariable()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.updateVariablesPermission"));

        return INPUT;
    }

    public String updateVariable()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.updateVariablesPermission"));

        // Make sure the 'variables' tag exists
        Tag variables = modem.status.getTag("variables");
        if (variables == null) {
            variables = new Tag("variables");
            modem.status.addContent(variables);
        }

        // Search to see if we're updating or creating
        Tag variableTag = null;
        for (Tag variable : variables.getEachTag("", "variable")) {
            if (variable.getStringAttribute("name").equals(variableName)) {
                variableTag = variable;

                break;
            }
        }

        LogEntry logEntry;
        if (variableValue.length() == 0) {
            logger.debug("Deleting variable '" + variableName + "'");

            variables.removeTag(variableTag);

            logEntry =
                new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
                    String.format("Removed variable '%s' from Modem #%d",
                        variableName, id));
        }
        else if (variableTag == null) {
            logger.debug("Creating variable '" + variableName +
                         "' with value '" + variableValue + "'");

            variableTag = new Tag("variable");
            variableTag.setAttribute("name", variableName);
            variableTag.addContent(variableValue);

            variables.addContent(variableTag);

            logEntry =
                new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
                    String.format("Created variable '%s' on Modem #%d",
                        variableName, id));
        }
        else {
            logger.debug("Updating variable '" + variableName +
                         "' with value '" + variableValue + "'");

            variableTag.removeContents();
            variableTag.addContent(variableValue);

            logEntry =
                new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
                    String.format("Updated variable '%s' on Modem #%d",
                        variableName, id));
        }

        DB.getDB().updateModem(modem);

        DB.getDB().log(Utils.makeLinkedList(logEntry));

        return VIEW;
    }

    @Override
    public void prepare()
        throws Exception
    {
        String sID = getRequest().getParameter("id");

        if (sID != null) {
            try {
                id = Integer.parseInt(sID);

                logger.debug("User specified modem with ID " + id);
            }
            catch (NumberFormatException nfe) {
                throw new Exception(getText("error.invalidModemID",
                    new String[] {
                        sID
                    }), nfe);
            }
        }
        else {
            id = Modem.INV_MODEM;
        }

        if (id == Modem.INV_MODEM) {
            logger.debug("No modem specified, creating a new one");

            modem =
                new Modem(Modem.INV_MODEM, null, null, null, null, null, null,
                    Modem.PORT_STANDARD, null, new Tag("modem"));

            passwordSettings = new PasswordSettings();
        }
        else {
            modem = DB.getDB().getModem(id);

            if (modem == null) {
                throw new Exception(getText("error.modemDoesNotExist",
                    new String[] {
                        sID
                    }));
            }

            passwordSettings =
                new PasswordSettings(modem.status.getTag("passwordSettings"));
        }
    }

    @Override
    public String getStatus(Object obj)
    {
        return ((Modem)obj).modemHealth.statusString;
    }

    @Override
    public Map<String, ? extends Object> getRecentStatus(Modem modem)
    {
        if (modem.modemID != this.modem.modemID) {
            return null;
        }

        return recentStatus.get(id);
    }

    @Override
    public Date getRecentContactDate(Modem modem)
    {
        if (modem.modemID != this.modem.modemID) {
            return null;
        }

        return contactDates.get(id);
    }

    @Override
    public Date getLastContactDate(Modem modem)
        throws Exception
    {
        return ModemActionLib.getLastContactDate(modem);
    }

    @Override
    public String getModemStatusI18n(Modem modem)
    {
        return getText("i18n.modem.status." + getStatus(modem));
    }

    public PasswordSettings.ApplicableInterfaces[] getApplicableInterfaces()
    {
        return PasswordSettings.ApplicableInterfaces.values();
    }

    public TagIterable getAllTags()
        throws Exception
    {
        Collection<Integer> tagIDs =
            DB.getDB().getMatchingTags(null, null, null, null, null, null,
                null, 0, -1, Utils.makeLinkedList(TagColumn.TagName), null);
        return new TagIterable(tagIDs);
    }

    public TagIterable getAppliedTags()
        throws Exception
    {
        Collection<Integer> tagIDs =
            DB.getDB().getMatchingTags(null, null, null, null, null, null, id,
                0, -1, Utils.makeLinkedList(TagColumn.TagName), null);
        return new TagIterable(tagIDs);
    }

    @Override
    public Date getLastUpdateTime(Modem modem)
        throws Exception
    {
        Date status = getRecentContactDate(modem);
        Date job = getLastContactDate(modem);

        if (status == null) {
            return job;
        }
        else if (job == null) {
            return status;
        }
        else if (status.compareTo(job) < 0) {
            return job;
        }
        else {
            return status;
        }
    }
}

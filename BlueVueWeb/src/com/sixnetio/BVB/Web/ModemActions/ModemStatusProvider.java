/*
 * ModemStatusProvider.java
 *
 * Guarantees that the implementor provides functions for examining modem status values.
 *
 * Jonathan Pearson
 * May 1, 2009
 *
 */

package com.sixnetio.BVB.Web.ModemActions;

import java.util.Date;
import java.util.Map;

import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Web.StatusProvider;

public interface ModemStatusProvider
    extends StatusProvider
{
    /**
     * Searches recent history for the given modem, returning a string
     * describing the status of the modem.
     * 
     * @param obj The modem to check.
     * @return {@link StatusProvider#S_NORMAL} if there was a successful
     *         communication within the {@link Setting#WarnTimeout} period,
     *         {@link StatusProvider#S_WARNING} if there was a successful
     *         communication within the {@link Setting#FailureTimeout} period,
     *         or {@link StatusProvider#S_FAILURE} if there were no successful
     *         communications in the recent past.
     */
    @Override
    public String getStatus(Object obj)
        throws Exception;

    /**
     * Retrieve the properties from the recent status of the given modem.
     * 
     * @param modem The modem for which to retrieve status information.
     * @return A map of recent status values for that modem, or <tt>null</tt> if
     *         no status information is available.
     */
    public Map<String, ? extends Object> getRecentStatus(Modem modem);

    /**
     * Get the timestamp when the recent status values were retrieved from the
     * modem.
     * 
     * @param modem The modem to check.
     * @return The date on which the values from {@link #getRecentStatus(Modem)}
     *         were retrieved.
     */
    public Date getRecentContactDate(Modem modem);

    /**
     * Similar to {@link #getRecentContactDate(Modem)}, but instead of the date
     * of the last status value update, this is the date of the last successful
     * job completion.
     * 
     * @param modem The modem to check.
     * @return The date on which the last job completed successfully, or
     *         <tt>null</tt> if none have completed successfully yet.
     * @throws Exception If there is a problem retrieving the last contact date.
     */
    public Date getLastContactDate(Modem modem)
        throws Exception;

    /**
     * A combination of {@link #getRecentContactDate(Modem)} and
     * {@link #getLastContactDate(Modem)} which returns the more recent of those
     * two values.
     * 
     * @param modem The modem to check.
     * @return The more recent of the most recent status update and the most
     *         recent successful job completion.
     * @throws Exception If there is a problem retrieving the data.
     */
    public Date getLastUpdateTime(Modem modem)
        throws Exception;

    /**
     * Get a local translation of one of the values from
     * {@link #getModemStatus(Modem)}.
     * 
     * @param modem The modem to check.
     * @return A local translation of one of the values from
     *         {@link ModemStatusProvider}.
     */
    public String getModemStatusI18n(Modem modem)
        throws Exception;
}

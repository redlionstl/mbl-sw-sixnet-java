/*
 * ModemListAction.java
 *
 * Provides sortable, selectable, filterable, paginated lists of modems, and some actions
 * on the selected modems in those lists (tagging, deleting, etc).
 *
 * Jonathan Pearson
 * April 28, 2009
 *
 */

package com.sixnetio.BVB.Web.ModemActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Database.ObjectInUseException;
import com.sixnetio.BVB.Database.SortColumns.ModemColumn;
import com.sixnetio.BVB.Web.*;
import com.sixnetio.BVB.Web.Iterators.ModemIterable;
import com.sixnetio.BVB.Web.Iterators.TagIterable;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class ModemListAction
    extends SelectionPreparer<Integer>
    implements Filterable, LinkTranslator, ModemStatusProvider, Sortable
{

    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    // Criteria for searching out modems
    public Integer qModemID = null;
    private DeviceID qDeviceID = null; // Needs special attention for conversion
    public String qAlias = null;
    public String qModel = null;
    public Version qFWVersion = null;
    public String qCfgVersion = null;
    public String qIPAddress = null;
    public Integer qPort = null;
    public String qPhone = null;
    public Integer qTagID = null;

    // Set by the Jobs page if we should display a message about creating jobs
    public boolean createJobs = false;

    // For the tagging actions
    public int tagID = ModemTag.INV_TAG;

    private long matchCount = 0;

    // This is only set once, so no need to synchronize
    private List<ModemColumn> sortOn = null;

    // Contains the most recent status history for each modem
    // Modem ID --> (Property Name --> Value)
    // Used in a single-threaded environment
    protected Map<Integer, Map<String, String>> recentStatus =
        new HashMap<Integer, Map<String, String>>();

    // Contains the date of the most recent status history for each modem
    // Used in a single-threaded environment
    protected Map<Integer, Date> contactDates = new HashMap<Integer, Date>();

    public String getQDeviceID()
    {
        return qDeviceID.toString();
    }

    public void setQDeviceID(String val)
    {
        qDeviceID = DeviceID.parseDeviceID(val);
    }

    @Override
    public String getSortOn()
    {
        return ModemActionLib.translateSort(sortOn);
    }

    @Override
    public void setSortOn(String columns)
    {
        sortOn = ModemActionLib.translateSort(columns);
    }

    @Override
    public String addSortColumn(String colName)
    {
        return ModemActionLib.addSortColumn(colName, sortOn);
    }

    @Override
    public Map<String, String[]> getFilters()
        throws Exception
    {
        // To start, fill this with Title --> Filter, but only individual
        // filters
        LinkedHashMap<String, String> filters =
            new LinkedHashMap<String, String>();

        if (qModemID != null) {
            filters.put(getText("filter.modemID", Utils
                .makeLinkedList((Object)qModemID)), "qModemID=" + qModemID);
        }

        if (qDeviceID != null) {
            filters.put(getText("filter.deviceID", Utils
                .makeLinkedList((Object)qDeviceID.toString())), "qDeviceID=" +
                                                                qDeviceID);
        }

        if (qAlias != null) {
            filters.put(getText("filter.alias", Utils
                .makeLinkedList((Object)qAlias)), "qAlias=" + qAlias);
        }

        if (qModel != null) {
            filters.put(getText("filter.model", Utils
                .makeLinkedList((Object)qModel)), "qModel=" + qModel);
        }

        if (qFWVersion != null) {
            filters.put(getText("filter.fwVersion", Utils
                .makeLinkedList((Object)qFWVersion)), "qFWVersion=" +
                                                      qFWVersion);
        }

        if (qCfgVersion != null) {
            filters.put(getText("filter.cfgVersion", Utils
                .makeLinkedList((Object)qCfgVersion)), "qCfgVersion=" +
                                                       qCfgVersion);
        }

        if (qIPAddress != null) {
            filters.put(getText("filter.ipAddress", Utils
                .makeLinkedList((Object)qIPAddress)), "qIPAddress=" +
                                                      qIPAddress);
        }

        if (qPort != null) {
            filters.put(getText("filter.port", Utils
                .makeLinkedList((Object)qPort)), "qPort=" + qPort);
        }

        if (qPhone != null) {
            filters.put(getText("filter.phone", Utils
                .makeLinkedList((Object)qPhone)), "qPhone=" + qPhone);
        }

        if (qTagID != null) {
            ModemTag tag = DB.getDB().getTag(qTagID);

            if (tag != null) {
                filters.put(getText("filter.tag", Utils
                    .makeLinkedList((Object)tag.name)), "qTagID=" + qTagID);
            }
            else {
                filters.put(getText("filter.tagID", Utils
                    .makeLinkedList((Object)qTagID)), "qTagID=" + qTagID);
            }
        }

        return constructFilters(filters);
    }

    /**
     * The user specified the List action. Loads a list of all modems, based on
     * the properties that the user provided.
     * 
     * @return {@link #LIST}.
     * @throws Exception If there is a database access error.
     */
    public String list()
        throws Exception
    {
        long[] matchCount = new long[1];
        matchingIDs =
            new LinkedList<Integer>(DB.getDB().getMatchingModems(qModemID,
                qDeviceID, qAlias, qModel, qFWVersion, qCfgVersion, qIPAddress,
                qPort, qPhone, qTagID, getPageStart(), getPageSize(), sortOn,
                matchCount));

        this.matchCount = matchCount[0];

        // Grab the most recent status history for each modem
        ModemActionLib.populateRecentStatus(recentStatus, contactDates, this,
            getPageIDs());

        if (selectAll) {
            // Can't just copy 'modemIDs', since that only contains the matches
            // for *this* page
            // We need the matches across *all* pages
            selectedIDsOff.addAll(DB.getDB().getMatchingModems(qModemID,
                qDeviceID, qAlias, qModel, qFWVersion, qCfgVersion, qIPAddress,
                qPort, qPhone, qTagID, 0, -1, // Start/Length
                sortOn, // Sort order
                null)); // Match count
        }

        prepareSelectedIDs();
        return LIST;
    }

    /**
     * Standard setup shared by Delete, Tag, Untag, etc actions. Transfers
     * selected IDs into matching IDs if necessary, selects the first page if
     * there was no selection provided, populates matchCount and status values,
     * handles select-all, and prepares selections for display.
     */
    private void selectionBasedActionSetup()
        throws Exception
    {
        if (matchingIDs.size() == 0) {
            if (selectedIDsOff.size() > 0) {
                // This is how IDs are passed in from the List page
                matchingIDs.addAll(selectedIDsOff);

                // Also, we must be going to a new view, so go to page 1
                pageStart = 0;
            }
            else {
                // Select the first page
                matchingIDs =
                    new LinkedList<Integer>(DB.getDB().getMatchingModems(
                        qModemID, qDeviceID, qAlias, qModel, qFWVersion,
                        qCfgVersion, qIPAddress, qPort, qPhone, qTagID, 0,
                        getPageSize(), sortOn, null));

                // But don't check their boxes
                selectedIDsOff.clear();
                selectAll = false;
            }
        }

        this.matchCount = matchingIDs.size();

        ModemActionLib.populateRecentStatus(recentStatus, contactDates, this,
            getPageIDs(), getPageStart(), getPageSize());

        if (selectAll) {
            selectedIDsOff.addAll(matchingIDs);
        }

        prepareSelectedIDs();
    }

    /**
     * The user specified the Delete action. Verifies that the user has modem
     * delete permission (Enhanced user) and asks for confirmation.
     * 
     * @return {@link #INPUT}
     * @throws Exception If the user does not have modem delete permission.
     */
    public String delete()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.deletePermission"));

        selectionBasedActionSetup();

        return INPUT;
    }

    /**
     * The user confirmed a delete action. Delete the modems.
     * 
     * @return {@link #LIST}, or {@link #INPUT} if no modem was specified.
     * @throws Exception If the user does not have modem delete permission.
     */
    public String realDelete()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.deletePermission"));

        if (selectedIDsOff.size() == 0) {
            addActionError(getText("error.deleteNoModem"));

            prepareSelectedIDs();
            return INPUT;
        }

        // Re-build matchingIDs with anything that was busy and could not be
        // deleted
        matchingIDs.clear();

        List<LogEntry> logEntries = new LinkedList<LogEntry>();

        for (int id : selectedIDsOff) {
            try {
                DB.getDB().deleteModem(id);

                logEntries.add(new LogEntry(new Date(), getUser().userID,
                    DBLogger.INFO, String.format("Deleted Modem #%d", id)));
            }
            catch (ObjectInUseException oiue) {
                matchingIDs.add(id);
            }
        }

        DB.getDB().log(logEntries);

        if (matchingIDs.size() > 0) {
            // Select the remaining modem IDs and return to the Delete page with
            // a helpful message
            addActionError(getText("hint.busyModems"));

            selectedIDsOff.retainAll(matchingIDs);

            matchCount = matchingIDs.size();

            prepareSelectedIDs();
            return INPUT;
        }

        // Redirect action doesn't need to have selections prepared
        return LIST;
    }

    /**
     * Get a listing of all tags in the system.
     */
    public Iterable<ModemTag> getAllTags()
        throws Exception
    {
        return ModemActionLib.getAllTags();
    }

    /**
     * Get a listing of all tags applied to the list of modems held by this
     * class.
     */
    public Iterable<ModemTag> getAppliedTags()
        throws Exception
    {
        Set<Integer> tagIDs = new HashSet<Integer>();

        for (int modemID : matchingIDs) {
            tagIDs.addAll(DB.getDB().getMatchingTags(null, null, null, null,
                null, null, modemID, 0, -1, null, null));
        }

        // Grab the actual Tag objects so we can sort by name
        final Map<Integer, ModemTag> tags = DB.getDB().getTags(tagIDs);

        final Integer[] sortedTagIDs = tagIDs.toArray(new Integer[0]);
        Arrays.sort(sortedTagIDs, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2)
            {
                return tags.get(o1).name.compareTo(tags.get(o2).name);
            }
        });

        return new TagIterable(Utils.makeArrayList(sortedTagIDs));
    }

    /**
     * Make sure the user has permission to tag modems.
     * 
     * @return {@link #INPUT}
     * @throws Exception If the user does not have permission.
     */
    public String tagModems()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced, getText("error.tagPermission"));

        selectionBasedActionSetup();

        return INPUT;
    }

    /**
     * Actually apply a tag to the modems.
     * 
     * @return {@link #LIST} if successful, or {@link #INPUT} if no modems or no
     *         tag ID was specified.
     * @throws Exception If the user does not have permission, or the database
     *             operation fails.
     */
    public String realTagModems()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced, getText("error.tagPermission"));

        if (selectedIDsOff.size() == 0) {
            addActionError(getText("error.tagNoModem"));

            prepareSelectedIDs();
            return INPUT;
        }

        if (tagID == ModemTag.INV_TAG) {
            addActionError(getText("error.tagNoTag"));

            prepareSelectedIDs();
            return INPUT;
        }

        List<LogEntry> logEntries = new LinkedList<LogEntry>();

        for (int id : selectedIDsOff) {
            DB.getDB().applyTag(id, tagID);

            logEntries.add(new LogEntry(new Date(), getUser().userID,
                DBLogger.INFO, String.format("Applied Tag #%d to "
                                             + "Modem #%d", tagID, id)));
        }

        DB.getDB().log(logEntries);

        // Redirect action doesn't need to have selections prepared
        return LIST;
    }

    /**
     * Make sure the user has permission to untag modems.
     * 
     * @return {@link #INPUT}
     * @throws Exception If the user does not have permission.
     */
    public String untagModems()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.untagPermission"));

        selectionBasedActionSetup();

        return INPUT;
    }

    /**
     * Actually remove a tag from the modems.
     * 
     * @return {@link #LIST} if successful, {@link #INPUT} if no modems or no
     *         tag ID was specified.
     * @throws Exception If the user does not have permission, or the database
     *             operation fails.
     */
    public String realUntagModems()
        throws Exception
    {
        verifyPermission(User.UserType.Enhanced,
            getText("error.untagPermission"));

        if (selectedIDsOff.size() == 0) {
            addActionError(getText("error.untagNoModem"));

            prepareSelectedIDs();
            return INPUT;
        }

        if (tagID == ModemTag.INV_TAG) {
            addActionError(getText("error.untagNoTag"));

            prepareSelectedIDs();
            return INPUT;
        }

        List<LogEntry> logEntries = new LinkedList<LogEntry>();

        for (int id : selectedIDsOff) {
            DB.getDB().removeTag(id, tagID);

            logEntries.add(new LogEntry(new Date(), getUser().userID,
                DBLogger.INFO, String.format("Removed Tag #%d from "
                                             + "Modem #%d", tagID, id)));
        }

        DB.getDB().log(logEntries);

        // Redirect action doesn't need selections prepared
        return LIST;
    }

    /**
     * Get a local translation of one of the values from
     * {@link #getModemStatus(Modem)}.
     * 
     * @param modem The modem to check.
     * @return A local translation of one of the values from
     *         {@link ModemStatusProvider}.
     */
    @Override
    public String getModemStatusI18n(Modem modem)
        throws Exception
    {
        return getText("i18n.modem.status." + getStatus(modem));
    }

    @Override
    public long getMatchCount()
    {
        return matchCount;
    }

    @Override
    public String translateLink(String link, Object obj)
    {
        if (obj == null || !(obj instanceof Modem)) {
            return "";
        }

        Modem modem = (Modem)obj;
        String docroot = getDocumentRoot();

        if (link.equals("viewModem")) {
            return String.format("%s/Modem/view?id=%d", docroot, modem.modemID);

        }
        else if (link.equals("filterJobs")) {
            return String.format("%s/Jobs?qModemID=%d", docroot, modem.modemID);

        }
        else if (link.equals("filterTags")) {
            return String.format("%s/Tags?qModemID=%d", docroot, modem.modemID);

        }
        else if (link.startsWith("filterModems/")) {
            String filterName = link.substring(link.indexOf('/') + 1);

            Integer qModemID = this.qModemID;
            DeviceID qDeviceID = this.qDeviceID;
            String qAlias = this.qAlias;
            String qModel = this.qModel;
            Version qFWVersion = this.qFWVersion;
            String qCfgVersion = this.qCfgVersion;
            String qIPAddress = this.qIPAddress;
            Integer qPort = this.qPort;
            String qPhone = this.qPhone;
            Integer qTagID = this.qTagID;

            if (filterName.equals("modemID")) {
                qModemID = modem.modemID;

            }
            else if (filterName.equals("deviceID")) {
                // Already -1 if not known
                qDeviceID = modem.deviceID;

            }
            else if (filterName.equals("alias")) {
                if (modem.alias == null) {
                    qAlias = "-";
                }
                else {
                    qAlias = modem.alias;
                }

            }
            else if (filterName.equals("model")) {
                if (modem.model == null) {
                    qModel = "-";
                }
                else {
                    qModel = modem.model;
                }

            }
            else if (filterName.equals("fwVersion")) {
                if (modem.fwVersion == null) {
                    qFWVersion = new Version("-");
                }
                else {
                    qFWVersion = modem.fwVersion;
                }

            }
            else if (filterName.equals("cfgVersion")) {
                if (modem.cfgVersion == null) {
                    qCfgVersion = "-";
                }
                else {
                    qCfgVersion = modem.cfgVersion;
                }

            }
            else if (filterName.equals("ipAddress")) {
                if (modem.ipAddress == null) {
                    qIPAddress = "-";
                }
                else {
                    qIPAddress = modem.ipAddress;
                }

            }
            else if (filterName.equals("port")) {
                qPort = modem.port;

            }
            else if (filterName.equals("phone")) {
                if (modem.phoneNumber == null) {
                    qPhone = "-";
                }
                else {
                    qPhone = modem.phoneNumber;
                }

            } // Can't do Tag ID, since the modem could have more than one

            StringBuilder builder = new StringBuilder(docroot + "/Modems?");

            // A trailing '&' won't hurt anything, so just generate a link with
            // it for simplicity

            if (qModemID != null) {
                builder.append(String.format("qModemID=%d&", qModemID));
            }

            if (qDeviceID != null) {
                builder.append(String.format("qDeviceID=%s&", qDeviceID));
            }

            if (qAlias != null) {
                builder.append(String.format("qAlias=%s&", qAlias));
            }

            if (qModel != null) {
                builder.append(String.format("qModel=%s&", qModel));
            }

            if (qFWVersion != null) {
                builder.append(String.format("qFWVersion=%s&", qFWVersion
                    .toString()));
            }

            if (qCfgVersion != null) {
                builder.append(String.format("qCfgVersion=%s&", qCfgVersion));
            }

            if (qIPAddress != null) {
                builder.append(String.format("qIPAddress=%s&", qIPAddress));
            }

            if (qPort != null) {
                builder.append(String.format("qPort=%d&", qPort));
            }

            if (qPhone != null) {
                builder.append(String.format("qPhone=%s&", qPhone));
            }

            if (qTagID != null) {
                builder.append(String.format("qTagID=%d&", qTagID));
            }

            return builder.toString();
        }

        return "";
    }

    @Override
    public Iterable<Modem> getItems()
        throws Exception
    {
        return new ModemIterable(getPageIDs());
    }

    @Override
    protected Collection<String> getIDParameterNames()
    {
        // Allow modem IDs to be passed around between pages
        return Utils.makeArrayList("modemID");
    }

    @Override
    protected Collection<String> getSelectedParameterNames()
    {
        // Allow modem IDs and tag IDs to be passed in
        return Utils.makeArrayList("selModemID", "selTagID");
    }

    @Override
    protected Collection<Integer> parseID(String type, String id)
        throws NumberFormatException, DatabaseException, Exception
    {

        if (type.equals("modemID") || type.equals("selModemID")) {
            // Select the modem specified
            return Utils.makeArrayList(Integer.parseInt(id));

        }
        else if (type.equals("selTagID")) {
            // Select all modems with that tag
            return DB.getDB()
                .getMatchingModems(null, null, null, null, null, null, null,
                    null, null, Integer.parseInt(id), 0, -1, null, null);
        }
        else {
            // Not recognized, must have added something and not updated this
            // method
            throw new IllegalArgumentException("Unrecognized type: " + type);
        }
    }

    @Override
    public Date getRecentContactDate(Modem modem)
    {
        return contactDates.get(modem.modemID);
    }

    @Override
    public Map<String, ? extends Object> getRecentStatus(Modem modem)
    {
        return recentStatus.get(modem.modemID);
    }

    @Override
    public Date getLastContactDate(Modem modem)
        throws Exception
    {
        return ModemActionLib.getLastContactDate(modem);
    }

    @Override
    public Date getLastUpdateTime(Modem modem)
        throws Exception
    {
        Date status = getRecentContactDate(modem);
        Date job = getLastContactDate(modem);

        if (status == null) {
            return job;
        }
        else if (job == null) {
            return status;
        }
        else if (status.compareTo(job) < 0) {
            return job;
        }
        else {
            return status;
        }
    }

    @Override
    public String getStatus(Object obj)
        throws Exception
    {
        return ((Modem)obj).modemHealth.statusString;
    }
}

/*
 * ModemActionLib.java
 *
 * Provides some library functions common to modem actions
 *
 * Jonathan Pearson
 * May 1, 2009
 *
 */

package com.sixnetio.BVB.Web.ModemActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.SortColumns.*;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.BVB.Web.Iterators.TagIterable;
import com.sixnetio.util.Utils;

public class ModemActionLib
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /**
     * Translate a string describing sorting columns into a list of
     * ModemColumns.
     * 
     * @param columns The description of the sorting columns.
     * @return A list that could be passed to
     *         {@link Database#getMatchingModems(Integer, Long, String, String, com.sixnetio.util.Version, String, String, Integer, String, Integer, int, int, List, long[])}
     *         .
     */
    public static List<ModemColumn> translateSort(String columns)
    {
        String[] cols = Utils.tokenize(columns, ",", false);
        List<ModemColumn> sortOn = new Vector<ModemColumn>(cols.length);

        for (String column : cols) {
            if (column.startsWith("!")) {
                sortOn.add(ModemColumn.Reverse);
                column = column.substring(1);
            }

            sortOn.add(ModemColumn.valueOf(column));
        }

        return sortOn;
    }

    /**
     * Translate a sort list into a string.
     * 
     * @param sortOn The sort list to translate. If synchronization is
     *            necessary, it must be handled by the caller.
     * @return A descriptive string.
     */
    public static String translateSort(List<ModemColumn> sortOn)
    {
        if (sortOn == null) {
            return "";
        }

        StringBuilder builder = new StringBuilder();

        boolean skipComma = true;
        for (ModemColumn column : sortOn) {
            if (skipComma) {
                skipComma = false;
            }
            else {
                builder.append(",");
            }

            if (column == ModemColumn.Reverse) {
                builder.append("!");
                skipComma = true;
            }
            else {
                builder.append(column.name());
            }
        }

        return builder.toString();
    }

    /**
     * Given an existing sort list, add a new primary column to it, shuffling
     * the existing columns as necessary.
     * 
     * @param colName The new column to add.
     * @param oldSortOn The existing sort list, or <tt>null</tt> if there wasn't
     *            one. If synchronization is necessary, it must be handled by
     *            the caller.
     * @return A new sort list. If the new column was already the primary
     *         column, reverses the sorting order. If it was already in the
     *         list, moves it to the beginning in normal order. If it did not
     *         exist, adds it at the beginning in normal order.
     */
    public static String addSortColumn(String colName,
                                       List<ModemColumn> oldSortOn)
    {
        ModemColumn col;

        try {
            col = ModemColumn.valueOf(colName);
        }
        catch (IllegalArgumentException iae) {
            logger.debug("Not a legal column name: '" + colName + "'", iae);

            return translateSort(oldSortOn);
        }

        // Copy the sortOn object, since we don't actually want to change it
        List<ModemColumn> sortOn;

        if (oldSortOn == null) {
            // It's only going to receive one item
            sortOn = new ArrayList<ModemColumn>(1);
        }
        else {
            sortOn = new ArrayList<ModemColumn>(oldSortOn);
        }

        // Get the index of the column, to see if it's already part of the sort
        // order
        int index = sortOn.indexOf(col);

        // Whether to apply a 'Reverse' just before the column
        boolean reverse = false;

        // If the column was already in there...
        if (index != -1) {
            // Remove it
            sortOn.remove(index);

            // If there was a reverse just before it...
            if (index > 0 && sortOn.get(index - 1) == ModemColumn.Reverse) {
                sortOn.remove(index - 1);

                reverse = true;
                index--; // So we can more easily tell if it was the first
                // column
            }

            // If it was the first sort column
            if (index == 0) {
                // Swap the reverse property
                reverse = !reverse;
            }
        }

        // Push the column onto the front of the sort order
        sortOn.add(0, col);

        // If reversing, push reverse onto the front also
        if (reverse) {
            sortOn.add(0, ModemColumn.Reverse);
        }

        // Return the new sorting order
        return translateSort(sortOn);
    }

    /**
     * Populate the recent status for the given modem IDs. Interested objects
     * call this in their action execution methods. If synchronization is
     * necessary, it must be done by the caller.
     * 
     * @param recentStatus The recent status object to populate.
     * @param contactDates The contact dates object to populate.
     * @param genAct A GeneralAction object, so we have access to standard text
     *            formatting methods.
     * @param modemIDs The list of modem IDs for which to assemble status
     *            information.
     * @throws Exception If there was a problem assembling the status
     *             information.
     */
    public static void populateRecentStatus(
                                            Map<Integer, Map<String, String>> recentStatus,
                                            Map<Integer, Date> contactDates,
                                            GeneralAction genAct,
                                            Collection<Integer> modemIDs)
        throws Exception
    {

        populateRecentStatus(recentStatus, contactDates, genAct, modemIDs, 0,
            modemIDs.size());
    }

    /**
     * Populate the recent status for a slice of the given modem IDs. Interested
     * objects call this in their action execution methods. If synchronization
     * is necessary, it must be done by the caller.
     * 
     * @param recentStatus The recent status object to populate.
     * @param contactDates The contact dates object to populate.
     * @param genAct A GeneralAction object, so we have access to standard text
     *            formatting methods.
     * @param modemIDs The list of modem IDs for which to assemble status
     *            information.
     * @param offset The starting offset in <tt>modemIDs</tt>.
     * @param length The number of modems to populate status on.
     * @throws Exception If there was a problem assembling the status
     *             information.
     */
    public static void populateRecentStatus(
                                            Map<Integer, Map<String, String>> recentStatus,
                                            Map<Integer, Date> contactDates,
                                            GeneralAction genAct,
                                            Collection<Integer> modemIDs,
                                            int offset, int length)
        throws Exception
    {

        int index = -1;
        for (int modemID : modemIDs) {
            index++;

            if (index < offset) {
                continue;
            }
            else if (index >= offset + length) {
                break;
            }

            // Get the status entries from the most recent communication with
            // the modem
            Map<String, String> history =
                DB.getDB().getModemStatus(modemID, true, null, null);

            // Sort the status data
            String[] sortedKeys =
                history.keySet().toArray(new String[history.size()]);
            Arrays.sort(sortedKeys);

            // Translate the known properties into the appropriate objects
            // Unknown properties will remain as strings
            Map<String, String> translated =
                new LinkedHashMap<String, String>();
            for (String key : sortedKeys) {
                // Parse the property name from the entry key
                String propName = key.substring(0, key.indexOf('@'));

                // Parse the property date stamp from the entry key
                String date = key.substring(key.indexOf('@') + 1);

                String value = history.get(key);

                // Translate known properties
                if (propName.equals("Uptime")) {
                    // This is saved as a long value representing milliseconds
                    Date time = new Date(Long.parseLong(value));

                    translated.put(propName, genAct.formatData(
                        "format.duration", time));

                }
                else if (propName.equals("Network Time")) {
                    // This is saved as a long value representing milliseconds
                    Date time = new Date(Long.parseLong(value));

                    translated.put(propName, genAct.formatData(
                        "format.datetime", time));

                }
                else if (propName.equals("GPSTime")) {
                    // This is saved as a long value representing milliseconds
                    Date time = new Date(Long.parseLong(value));

                    translated.put(propName, genAct.formatData("format.time",
                        time));

                }
                else if (propName.equals("GPSDate")) {
                    // This is saved as a long value representing milliseconds
                    Date time = new Date(Long.parseLong(value));

                    translated.put(propName, genAct.formatData("format.date",
                        time));

                }
                else if (propName.equals("Latitude") ||
                         propName.equals("Longitude") ||
                         propName.equals("GPSHeading")) {

                    float flt = Float.parseFloat(value);

                    // Add the degrees sign
                    translated.put(propName, genAct.formatData("format.number",
                        flt) +
                                             "&#176;");

                }
                else if (propName.equals("Altitude")) {
                    float flt = Float.parseFloat(value);

                    // Add the meters symbol
                    translated.put(propName, genAct.formatData("format.number",
                        flt) +
                                             " m");

                }
                else if (propName.equals("GPSSpeedKph")) {
                    float flt = Float.parseFloat(value);

                    // Add the kilometers per hour symbol
                    translated.put(propName, genAct.formatData("format.number",
                        flt) +
                                             " km/h");

                }
                else if (propName.equals("RSSI")) {
                    float flt = Float.parseFloat(value);

                    translated.put(propName, genAct.formatData("format.number",
                        flt) +
                                             " dBm");

                }
                else if (propName.equals("PWR")) {
                    float flt = Float.parseFloat(value);

                    translated.put(propName, genAct.formatData("format.number",
                        flt) +
                                             " V");

                }
                else if (propName.startsWith("AI")) {
                    float flt = Float.parseFloat(value);

                    translated.put(propName, genAct.formatData("format.number",
                        flt));

                }
                else if (propName.equals("IGN") || propName.startsWith("DI")) {
                    if (value.equals("0")) {
                        translated.put(propName, genAct.getText("value.off"));
                    }
                    else {
                        translated.put(propName, genAct.getText("value.on"));
                    }

                }
                else {
                    // Unknown/not special? Save as a string
                    translated.put(propName, value);
                }

                contactDates.put(modemID, Utils.parseStandardDateTime(date));
            }

            recentStatus.put(modemID, translated);
        }
    }

    public static Iterable<ModemTag> getAllTags()
        throws Exception
    {
        List<TagColumn> sortOn = new Vector<TagColumn>(1);
        sortOn.add(TagColumn.TagName);

        Collection<Integer> tagIDs =
            DB.getDB().getMatchingTags(null, null, null, null, null, null,
                null, 0, -1, sortOn, null);

        return new TagIterable(tagIDs);
    }

    public static Date getLastContactDate(Modem modem)
        throws Exception
    {
        // Get the most recent history ID for this modem marked as "Success"
        Collection<Long> historyIDs =
            DB.getDB().getMatchingHistory(
                null,
                null,
                null,
                modem.modemID,
                null,
                null,
                "Success",
                null,
                null,
                0,
                1,
                Utils.makeArrayList(HistoryColumn.Reverse,
                    HistoryColumn.Timestamp), null);

        if (historyIDs.size() == 0) {
            return null;
        }

        Map<Long, HistoryEntry> entries =
            DB.getDB().getHistoryEntries(historyIDs);
        if (entries.size() == 0) {
            // Very unlikely, but possible
            return null;
        }

        return entries.get(historyIDs.iterator().next()).timestamp;
    }
}

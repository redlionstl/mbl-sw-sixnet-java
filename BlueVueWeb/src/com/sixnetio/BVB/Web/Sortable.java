/*
 * Sortable.java
 *
 * Description
 *
 * Jonathan Pearson
 * May 11, 2009
 *
 */

package com.sixnetio.BVB.Web;

public interface Sortable {
	/**
	 * Get the sorting columns in the same format as expected by
	 * {@link #setSortOn(String)}.
	 * 
	 * @return The sorting columns, or "" if none were set.
	 */
	public String getSortOn();
	
	/**
	 * Set the sorting columns to use for database search results.
	 * 
	 * @param columns A comma-separated list of column names. Choose from the
	 *            appropriate column Enum, except for the Reverse element, which
	 *            should be represented as a '!' character immediately preceding
	 *            the column name. For example: "Alias,!DeviceID" will sort by
	 *            Alias, and when two modems have the same alias, it will sort
	 *            by reverse-order device ID.
	 */
	public void setSortOn(String columns);
	
	/**
	 * Return a string describing the current sort order after adding a new
	 * primary column. If the given column was already the primary column, this
	 * will reverse the sort order. If the given column is not recognized, this
	 * will return the current sort order.
	 * 
	 * @param colName The name of the column to make into the primary column.
	 * @return A string describing the new sort order
	 */
	public String addSortColumn(String colName);
}

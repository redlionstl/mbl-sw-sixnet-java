/*
 * Paginated.java
 *
 * Description
 *
 * Jonathan Pearson
 * May 5, 2009
 *
 */

package com.sixnetio.BVB.Web;

public interface Paginated {
	/** Get the starting row index of the first page. */
	public int getFirstPage();
	
	/** Get the starting row index of the previous page. */
	public int getPreviousPage();
	
	/** Get the starting row index of the next page. */
	public int getNextPage();
	
	/** Get the starting row index of the last page. */
	public int getLastPage();
	
	/** Get the total number of items, across all pages. */
	public long getMatchCount();
	
	/** Get the current page number (1-based). */
	public int getPageNumber();
	
	/** Get the total number of pages. */
	public int getTotalPages();
	
	/** Get the starting row number on this page. */
	public int getPageStart();
	
	/** Get the number of items to be displayed on each page. */
	public int getPageSize();
}

/*
 * JobCreateAction.java
 *
 * Provides job creation functionality.
 *
 * Jonathan Pearson
 * May 4, 2009
 *
 */

package com.sixnetio.BVB.Web.JobActions;

import java.text.*;
import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.SelectionPreparer;
import com.sixnetio.BVB.Web.Iterators.ModemIterable;
import com.sixnetio.BVB.Web.ModemActions.ModemActionLib;
import com.sixnetio.BVB.Web.ModemActions.ModemStatusProvider;
import com.sixnetio.util.Utils;

/**
 * Note: This class is for creating jobs, and therefore is grouped with the
 * other job classes, but it mainly acts like a <b>Modem</b> class; sorting is
 * done as for modems, and it listens for modem/tag IDs.
 * 
 * This could be combined with ModemListAction, but it would pollute that class
 * with a bunch of data that is only used for job creation.
 * 
 * @author Jonathan Pearson
 */
public class JobCreateAction
    extends SelectionPreparer<Integer>
    implements ModemStatusProvider
{

    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    public String jobType = "Status";
    private Date schedule = new Date();

    // When submitting, make sure we get both the date AND the time
    private int schedulePieces = 0;

    public boolean automatic = false;
    public boolean mobileOriginated = false;
    public boolean alwaysAvailable = true;
    public String packageHash = null;
    public boolean force = false;
    public String batchName = null;

    // Populate it once, then we can look at it as well as providing it to the
    // view without polling the database again
    // Give it a value here to keep Struts happy
    public List<String> availableJobTypes = new LinkedList<String>();

    // Contains the most recent status history for each modem
    // Modem ID --> (Property Name --> Value)
    // Used in a single-threaded environment
    protected Map<Integer, Map<String, String>> recentStatus =
        new HashMap<Integer, Map<String, String>>();

    // Contains the date of the most recent status history for each modem
    // Used in a single-threaded environment
    protected Map<Integer, Date> contactDates = new HashMap<Integer, Date>();

    public String getSchedule()
    {
        // Utils.formatStandardDate() formats for the current timezone, we need
        // UTC
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setCalendar(Calendar.getInstance(TimeZone.getTimeZone("UTC")));
        return df.format(schedule);
    }

    public void setSchedule(String s)
    {
        // Utils.parseStandardDate() parses for the current timezone, we need
        // UTC
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setCalendar(Calendar.getInstance(TimeZone.getTimeZone("UTC")));

        try {
            this.schedule = df.parse(s);
        }
        catch (ParseException pe) {
            logger.error("Error parsing schedule value", pe);
            this.schedule = null;
        }
    }

    /**
     * The user specified the 'create' action, ask for more input.
     * 
     * @return INPUT
     * @throws Exception If the user is not logged in.
     */
    public String create()
        throws Exception
    {
        if (matchingIDs.size() == 0) {
            if (selectedIDsOff.size() > 0) {
                // Passed in from a list view
                matchingIDs.addAll(selectedIDsOff);
            }
            else {
                matchingIDs =
                    new LinkedList<Integer>(DB.getDB().getMatchingModems(null,
                        null, null, null, null, null, null, null, null, null,
                        0, getPageSize(), // Page start/size
                        null, null)); // Sort/matches

                // Select none, since the user selected none
                selectedIDsOff.clear();
                selectAll = false;
            }
        }

        ModemActionLib.populateRecentStatus(recentStatus, contactDates, this,
            getPageIDs());

        if (selectAll) {
            selectedIDsOff.addAll(matchingIDs);
        }

        availableJobTypes = getAvailableJobTypes();

        if (availableJobTypes.size() == 0) {
            addActionError(getText("error.noJobServers"));
        }

        prepareSelectedIDs();
        return INPUT;
    }

    /**
     * The user specified the 'realCreate' action, try to create the jobs.
     * 
     * @return UPLEVEL if successful, INPUT if not.
     * @throws Exception If the user is not logged in or tries to create jobs
     *             other than status queries.
     */
    public String realCreate()
        throws Exception
    {
        verifyUserLoggedIn();

        if (selectedIDsOff.size() == 0) {
            addActionError(getText("error.createNoModem"));
        }

        jobType = prepareParam(jobType);
        if (jobType == null) {
            addActionError(getText("error.noJobType"));
        }

        if (schedule == null) {
            // This happens when we can't parse the schedule value as a date
            // Set it to right now and ask the user to try again
            schedule = new Date();

            addActionError(getText("error.badSchedule"));
        }

        if (jobType != null && jobType.equals("Firmware")) {
            // Check for extra properties
            if (packageHash == null || packageHash.length() == 0) {
                addActionError(getText("error.noPackage"));
            }
            else {
                // Make sure the package exists
                BlueTreeImage btImage =
                    DB.getDB().getPackage(packageHash, true);

                if (btImage == null) {
                    addActionError(getText("error.packageMissing"));
                }
            }
        }

        if (jobType != null && !jobType.equals("Status")) {
            // Make sure the user has permission for non-status query jobs
            verifyPermission(User.UserType.Enhanced,
                getText("error.createPermission"));
        }

        // Check that all modems have the necessary contact information
        ArrayList<Integer> failingModemIDs =
            new ArrayList<Integer>(selectedIDsOff.size());
        Map<Integer, Modem> modems = DB.getDB().getModems(selectedIDsOff);
        for (Map.Entry<Integer, Modem> entry : modems.entrySet()) {
            // This is split into two clauses because it is easier to read and
            // debug
            if ((mobileOriginated && entry.getValue().deviceID == null)) {
                logger.debug(String.format(
                    "Mobile-originated, modem %d has no device ID", entry
                        .getKey()));
                failingModemIDs.add(entry.getKey());
            }
            else if (!mobileOriginated && entry.getValue().ipAddress == null) {
                logger.debug(String
                    .format("Not mobile-originated, modem %d has no IP", entry
                        .getKey()));
                failingModemIDs.add(entry.getKey());
            }
            else {
                logger.debug(String.format("%s, ip = %s, devID = %s",
                    mobileOriginated ? "Mobile-originated"
                                    : "Not mobile-originated",
                    entry.getValue().ipAddress, entry.getValue().deviceID));
            }
        }

        if (!failingModemIDs.isEmpty()) {
            // Add an action error message that includes links to the failing
            // modems
            StringBuilder builder = new StringBuilder();
            if (mobileOriginated) {
                builder.append(getText("error.missingDeviceIDs"));
            }
            else {
                builder.append(getText("error.missingIPAddresses"));
            }

            for (Integer modemID : failingModemIDs) {
                builder
                    .append(String
                        .format(
                            " <a href=\"%s/Modem/view?id=%d\" target=\"_blank\">%s</a>",
                            getDocumentRoot(), modemID, modems.get(modemID)
                                .getLabel()));
            }

            addActionError(builder.toString());
        }

        if (!getActionErrors().isEmpty()) {
            availableJobTypes = getAvailableJobTypes();

            if (availableJobTypes.size() == 0) {
                addActionError(getText("error.noJobServers"));
            }

            prepareSelectedIDs();
            return INPUT;
        }

        // Did the user specify a batch?
        batchName = prepareParam(batchName);
        Integer batchID = null;
        if (batchName != null) {
            Collection<Integer> batchIDs =
                DB.getDB().getMatchingBatches(null, null, batchName, false, 0,
                    -1, // Page start/size
                    null, null); // Sort/matches

            if (batchIDs.size() == 0) {
                batchID = createBatch();
            }
            else {
                // Choose the first one
                batchID = batchIDs.iterator().next();
            }
        }

        Job.JobData data =
            new Job.JobData(Job.JobData.INV_JOB, batchID, Modem.INV_MODEM,
                getUser().userID, schedule, 0, jobType, new Tag("job"),
                automatic);

        // Set the mobile-originated and always available properties
        // Since alwaysAvailable is ignored when mobileOriginated is false, this
        // is safe in all cases
        Tag tag = new Tag("mobileOriginated");
        tag.addContent("" + mobileOriginated);
        tag.setAttribute("alwaysAvailable", alwaysAvailable);
        data.parameters.addContent(tag);

        // Special case: Firmware jobs need some extra parameters
        if (jobType.equals("Firmware")) {
            Tag hash = new Tag("firmware");
            hash.addContent(packageHash);

            data.parameters.addContent(hash);

            if (force) {
                data.parameters.addContent(new Tag("force"));
            }
        }

        List<LogEntry> logEntries = new LinkedList<LogEntry>();

        for (int modemID : selectedIDsOff) {
            data.modemID = modemID;

            DB.getDB().createJob(Job.makeGenericJob(data));

            logEntries.add(new LogEntry(new Date(), getUser().userID,
                DBLogger.INFO, String.format("Created Job #%d", data.jobID)));
        }

        DB.getDB().log(logEntries);

        // Redirect action does not need selections prepared
        return LIST;
    }

    // A utility function to create a new batch
    private int createBatch()
        throws Exception
    {
        JobBatch batch =
            new JobBatch(JobBatch.INV_BATCH, getUser().userID, batchName);
        DB.getDB().createBatch(batch);

        LogEntry entry =
            new LogEntry(new Date(), getUser().userID, DBLogger.INFO, String
                .format("Created Batch #%d", batch.batchID));
        DB.getDB().log(Utils.makeLinkedList(entry));

        return batch.batchID;
    }

    /**
     * Get a list of available job types based on the published capabilities of
     * all running job servers.
     * 
     * @return A list of available job type names.
     * @throws Exception If there is a problem accessing the database.
     */
    private List<String> getAvailableJobTypes()
        throws Exception
    {
        // Ask the database for the current capabilities of all job servers
        Collection<Capabilities> capabilities =
            DB.getDB().getCurrentCapabilities();

        // Concatenate the job types from those into a single set
        Set<String> jobTypes = new HashSet<String>();
        for (Capabilities caps : capabilities) {
            jobTypes.addAll(caps.getJobs());
        }

        // Remove job types beginning with "!", these are internal-only
        for (Iterator<String> itJobTypes = jobTypes.iterator(); itJobTypes
            .hasNext();) {
            String jobType = itJobTypes.next();

            if (jobType.startsWith("!")) {
                itJobTypes.remove();
            }
        }

        // Sort alphabetically and return
        String[] sortedTypes = jobTypes.toArray(new String[0]);
        Arrays.sort(sortedTypes);

        return Utils.makeVector(sortedTypes);
    }

    /**
     * Get a map of package hashes onto descriptions for all packages in the
     * system.
     * 
     * @return A map of hashes onto descriptions.
     * @throws Exception If there is a problem accessing the database.
     */
    public Map<String, String> getAvailablePackages()
        throws Exception
    {
        // Ask the database for the hashes of the available packages
        Collection<String> packageHashes =
            DB.getDB().getMatchingPackages(null, null, null, null, null, null,
                0, -1, null);

        // Populate the map with the descriptions of those packages
        Map<String, String> descriptions = new HashMap<String, String>();
        for (String hash : packageHashes) {
            BlueTreeImage image = DB.getDB().getPackage(hash, true);

            descriptions.put(hash, image.toString());
        }

        return descriptions;
    }

    @Override
    public String getModemStatusI18n(Modem modem)
        throws Exception
    {
        return getText("i18n.modem.status." + getStatus(modem));
    }

    @Override
    protected Collection<String> getIDParameterNames()
    {
        // Allow modem IDs to be passed between pages
        return Utils.makeArrayList("modemID");
    }

    @Override
    protected Collection<String> getSelectedParameterNames()
    {
        // Allow modems to be passed in
        return Utils.makeArrayList("selModemID");
    }

    @Override
    protected Collection<Integer> parseID(String type, String id)
        throws NumberFormatException, DatabaseException, Exception
    {

        if (type.equals("modemID") || type.equals("selModemID")) {
            // Select the modem specified
            return Utils.makeArrayList(Integer.parseInt(id));

        }
        else {
            // Not recognized, must have added something and not updated this
            // method
            throw new IllegalArgumentException("Unrecognized type: " + type);
        }
    }

    @Override
    public Iterable<?> getItems()
        throws Exception
    {
        return new ModemIterable(getPageIDs());
    }

    @Override
    public Date getRecentContactDate(Modem modem)
    {
        return contactDates.get(modem.modemID);
    }

    @Override
    public Map<String, ? extends Object> getRecentStatus(Modem modem)
    {
        return recentStatus.get(modem.modemID);
    }

    @Override
    public String getStatus(Object obj)
        throws Exception
    {
        return ((Modem)obj).modemHealth.statusString;
    }

    @Override
    public Date getLastContactDate(Modem modem)
        throws Exception
    {
        return ModemActionLib.getLastContactDate(modem);
    }

    @Override
    public Date getLastUpdateTime(Modem modem)
        throws Exception
    {
        Date status = getRecentContactDate(modem);
        Date job = getLastContactDate(modem);

        if (status == null) {
            return job;
        }
        else if (job == null) {
            return status;
        }
        else if (status.compareTo(job) < 0) {
            return job;
        }
        else {
            return status;
        }
    }
}

/*
 * JobListAction.java
 *
 * Provides sortable, filterable, paginated lists of jobs.
 *
 * Jonathan Pearson
 * May 1, 2009
 *
 */

package com.sixnetio.BVB.Web.JobActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Database.ObjectInUseException;
import com.sixnetio.BVB.Database.SortColumns.BatchColumn;
import com.sixnetio.BVB.Database.SortColumns.JobColumn;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.*;
import com.sixnetio.BVB.Web.Iterators.BatchIterable;
import com.sixnetio.BVB.Web.Iterators.JobIterable;
import com.sixnetio.util.Utils;

public class JobListAction
	extends SelectionPreparer<Long>
	implements Filterable, JobStatusProvider, LinkTranslator, Sortable {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// Criteria for searching out jobs
	public Long qJobID = null;
	public Integer qBatchID = null;
	public Integer qModemID = null;
	public Integer qUserID = null;
	public String qType = null;
	public Date qLowDate = null;
	public Date qHighDate = null;
	public Boolean qAutomatic = null;
	
	private long matchCount = 0;
	
	// This should only be set once, so no need to synchronize
	private List<JobColumn> sortOn = null;
	
	@Override
	public Map<String, String[]> getFilters() throws Exception {
		// To start, fill this with Title --> Filter, but only individual
		// filters
		LinkedHashMap<String, String> filters = new LinkedHashMap<String, String>();
		
		if (qJobID != null) {
			filters.put(getText("filter.jobID", Utils.makeLinkedList((Object)qJobID)), "qJobID=" +
			                                                                           qJobID);
		}
		
		if (qBatchID != null) {
			JobBatch batch = DB.getDB().getBatch(qBatchID);
			
			if (batch != null) {
				filters.put(getText("filter.batch", Utils.makeLinkedList((Object)batch.name)),
				            "qBatchID=" + qBatchID);
			} else {
				filters.put(getText("filter.batchID", Utils.makeLinkedList((Object)qBatchID)),
				            "qBatchID=" + qBatchID);
			}
		}
		
		if (qModemID != null) {
			Modem modem = DB.getDB().getModem(qModemID);
			
			if (modem != null) {
				filters
				       .put(
				            getText("filter.modem", Utils.makeLinkedList((Object)modem.getLabel())),
				            "qModemID=" + qModemID);
			} else {
				filters.put(getText("filter.modemID", Utils.makeLinkedList((Object)qModemID)),
				            "qModemID=" + qModemID);
			}
		}
		
		if (qUserID != null) {
			User user = DB.getDB().getUser(qUserID);
			
			if (user != null) {
				filters.put(getText("filter.user", Utils.makeLinkedList((Object)user.name)),
				            "qUserID=" + qUserID);
			} else {
				filters.put(getText("filter.userID", Utils.makeLinkedList((Object)qUserID)),
				            "qUserID=" + qUserID);
			}
		}
		
		if (qType != null) {
			filters.put(getText("filter.type", Utils.makeLinkedList((Object)qType)), "qType=" +
			                                                                         qType);
		}
		
		if (qLowDate != null) {
			String formatted = formatData("format.datetime", qLowDate);
			filters.put(getText("filter.lowDate", Utils.makeLinkedList((Object)formatted)),
			            "qLowDate=" + formatted);
		}
		
		if (qHighDate != null) {
			String formatted = formatData("format.datetime", qHighDate);
			filters.put(getText("filter.highDate", Utils.makeLinkedList((Object)formatted)),
			            "qHighDate=" + formatted);
		}
		
		if (qAutomatic != null) {
			filters.put(getText("filter.automatic", Utils.makeLinkedList((Object)qAutomatic)),
			            "qAutomatic=" + qAutomatic);
		}
		
		return constructFilters(filters);
	}
	
	@Override
	public String getSortOn() {
		return JobActionLib.translateSort(sortOn);
	}
	
	@Override
	public void setSortOn(String columns) {
		sortOn = JobActionLib.translateSort(columns);
	}
	
	@Override
	public String addSortColumn(String colName) {
		return JobActionLib.addSortColumn(colName, sortOn);
	}
	
	/**
	 * The user specified the List action. Loads a list of all jobs, based on
	 * the properties that the user provided.
	 * 
	 * @return{@link GeneralAction#LIST}.
	 * @throws Exception If there is a database access error.
	 */
	public String list() throws Exception {
		long[] matchCount = new long[1];
		matchingIDs = new LinkedList<Long>(DB.getDB()
				.getMatchingJobs(qJobID, qBatchID, qModemID, qUserID, qType,
				                 qLowDate, qHighDate, qAutomatic,
				                 getPageStart(), getPageSize(),
				                 sortOn, matchCount));
		
		this.matchCount = matchCount[0];
		
		if (selectAll) {
			// Can't just copy 'jobIDs', since that only contains the matches
			// for *this* page
			// We need the matches across *all* pages
			selectedIDsOff.addAll(DB.getDB()
			                      .getMatchingJobs(qJobID, qBatchID, qModemID,
			                                       qUserID, qType, qLowDate,
			                                       qHighDate, qAutomatic,
			                                       0, -1, // Page start/size
			                                       sortOn, // Sort order
			                                       null)); // Match count
		}
		
		prepareSelectedIDs();
		return LIST;
	}
	
	/**
	 * The user specified the Delete action. Verifies that the user has job
	 * delete permission (Enhanced user) asks for confirmation.
	 * 
	 * @return INPUT
	 * @throws Exception If the user does not have job delete permission.
	 */
	public String delete() throws Exception {
		verifyPermission(User.UserType.Enhanced,
		                 getText("error.deletePermission"));
		
		if (matchingIDs.size() == 0) {
			if (selectedIDsOff.size() > 0) {
				// This is how IDs are passed in from the List page
				matchingIDs.addAll(selectedIDsOff);
				
				// Also, we must be going to a new view, so go to page 1
				pageStart = 0;
			} else {
    			// Select the first page
    			matchingIDs = new LinkedList<Long>(DB.getDB()
    					.getMatchingJobs(qJobID, qBatchID, qModemID, qUserID,
    					                 qType, qLowDate, qHighDate, qAutomatic,
    					                 0, getPageSize(),
    					                 sortOn, null));
    			
    			// But don't check their boxes
    			selectedIDsOff.clear();
    			selectAll = false;
			}
		}
		
		this.matchCount = matchingIDs.size();
		
		if (selectAll) {
			selectedIDsOff.addAll(matchingIDs);
		}
		
		prepareSelectedIDs();
		return INPUT;
	}
	
	/**
	 * The user confirmed a delete action. Delete the jobs.
	 * 
	 * @return {@link GeneralAction#UPLEVEL}, or INPUT if no job was specified.
	 * @throws Exception If the user does not have job delete permission.
	 */
	public String realDelete() throws Exception {
		verifyPermission(User.UserType.Enhanced,
		                 getText("error.deletePermission"));
		
		if (selectedIDsOff.size() == 0) {
			addActionError(getText("error.deleteNoJob"));
			
			prepareSelectedIDs();
			return INPUT;
		}
		
		// Re-build matchingIDs with anything that was busy and could not be
		// deleted
		matchingIDs.clear();
		
		List<LogEntry> logEntries = new LinkedList<LogEntry>();
		
		for (long id : selectedIDsOff) {
			try {
    			DB.getDB().deleteJob(id);
    			
    			logEntries.add(new LogEntry(new Date(), getUser().userID,
    			                            DBLogger.INFO,
    			                            String.format("Deleted Job #%d",
    			                                          id)));
			} catch (ObjectInUseException oiue) {
				matchingIDs.add(id);
			}
		}
		
		DB.getDB().log(logEntries);
		
		if (matchingIDs.size() > 0) {
			// Select the remaining job IDs and return to the Delete page with
			//   a helpful message
			addActionError(getText("hint.busyJobs"));
			
			selectedIDsOff.retainAll(matchingIDs);
			
			matchCount = matchingIDs.size();
			
			prepareSelectedIDs();
			return INPUT;
		}
		
		// Redirect action, no need to prepare selections
		return LIST;
	}
	
	@Override
	public long getMatchCount() {
		return matchCount;
	}
	
	@Override
	public String translateLink(String link, Object obj) throws Exception {
		if (obj == null || !(obj instanceof Job)) return "";
		
		Job job = (Job)obj;
		Job.JobData jd = job.getJobData();
		String docroot = getDocumentRoot();
		
		if (link.equals("viewJob")) {
			return String.format("%s/Job/view?id=%d", docroot, jd.jobID);
			
		} else if (link.equals("viewBatch")) {
			if (jd.batchID == null) return "";
			return String.format("%s/Batch/view?id=%d", docroot, jd.batchID);
			
		} else if (link.equals("viewModem")) {
			return String.format("%s/Modem/view?id=%d", docroot, jd.modemID);
			
		} else if (link.equals("viewUser")) {
			if (jd.userID == null) return "";
			return String.format("%s/User/view?id=%d", docroot, jd.userID);
			
		} else if (link.equals("showHistory")) {
			return String.format("%s/History?qJobID=%d", docroot, jd.jobID);
			
		} else if (link.startsWith("filterJobs/")) {
			String filterName = link.substring(link.indexOf('/') + 1);
			
			// Criteria for searching out jobs
			Long qJobID = this.qJobID;
			Integer qBatchID = this.qBatchID;
			Integer qModemID = this.qModemID;
			Integer qUserID = this.qUserID;
			String qType = this.qType;
			Date qLowDate = this.qLowDate;
			Date qHighDate = this.qHighDate;
			Boolean qAutomatic = this.qAutomatic;
			
			if (filterName.equals("jobID")) {
				qJobID = jd.jobID;
				
			} else if (filterName.equals("batchID")) {
				if (jd.batchID == null) {
					qBatchID = JobBatch.INV_BATCH;
				} else {
					qBatchID = jd.batchID;
				}
				
			} else if (filterName.equals("modemID")) {
				qModemID = jd.modemID;
				
			} else if (filterName.equals("userID")) {
				if (jd.userID == null) {
					qUserID = User.INV_USER;
				} else {
					qUserID = jd.userID;
				}
				
			} else if (filterName.equals("type")) {
				qType = jd.type;
				
			} else if (filterName.startsWith("date/")) {
				String range = filterName.substring(filterName.indexOf('/') + 1);
				int minuteRange = 10; // Default to 10 minutes
				
				try {
					minuteRange = Integer.parseInt(range);
				} catch (NumberFormatException nfe) {
					logger.warn("Not a number: " + range, nfe);
				}
				
				// Select within minuteRange minutes
				Calendar cal = Calendar.getInstance();
				
				cal.setTime(jd.schedule);
				cal.add(Calendar.MINUTE, -minuteRange);
				qLowDate = cal.getTime();
				
				cal.setTime(jd.schedule);
				cal.add(Calendar.MINUTE, minuteRange);
				qHighDate = cal.getTime();
				
			} else if (filterName.equals("automatic")) {
				qAutomatic = jd.automatic;
			}
			
			StringBuilder builder = new StringBuilder(docroot + "/Jobs?");
			
			// A trailing '&' won't hurt anything, so just generate a link with
			// it for simplicity
			
			if (qJobID != null) {
				builder.append(String.format("qJobID=%d&", qJobID));
			}
			
			if (qBatchID != null) {
				builder.append(String.format("qBatchID=%d&", qBatchID));
			}
			
			if (qModemID != null) {
				builder.append(String.format("qModemID=%d&", qModemID));
			}
			
			if (qUserID != null) {
				builder.append(String.format("qUserID=%d&", qUserID));
			}
			
			if (qType != null) {
				builder.append(String.format("qType=%s&", qType));
			}
			
			if (qLowDate != null) {
				builder.append(String.format("qLowDate=%s&",
				                             formatData("format.datetime",
				                                        qLowDate)));
			}
			
			if (qHighDate != null) {
				builder.append(String.format("qHighDate=%s&",
				                             formatData("format.datetime",
				                                        qHighDate)));
			}
			
			if (qAutomatic != null) {
				builder.append(String.format("qAutomatic=%b&", qAutomatic));
			}
			
			return builder.toString();
		}
		
		return "";
	}
	
	@Override
    protected Collection<String> getIDParameterNames() {
	    // Job IDs may be passed between pages
		return Utils.makeArrayList("jobID");
    }
	
	@Override
    protected Collection<String> getSelectedParameterNames() {
	    // Jobs and batches may be passed in
		return Utils.makeArrayList("selJobID", "selBatchID");
    }
	
	@Override
    protected Collection<Long> parseID(String type, String id)
    	throws NumberFormatException, DatabaseException, Exception {
	    
		logger.debug("Parsing '" + id + "' as a '" + type + "'");
		
		if (type.equals("jobID") || type.equals("selJobID")) {
			// Select the job specified
			logger.debug("It was a Job ID, parsing as a Long");
			
			return Utils.makeArrayList(Long.parseLong(id));
			
		} else if (type.equals("selBatchID")) {
			// Select all jobs in that batch
			logger.debug("It was a Batch ID, reading matching jobs from the DB");
			
			return DB.getDB().getMatchingJobs(null, Integer.parseInt(id), null,
			                                  null, null, null, null, null,
			                                  0, -1, // Page start/size
			                                  null, null); // Sort/matches
		} else {
			// Not recognized, must have added something and not updated this
			// method
			throw new IllegalArgumentException("Unrecognized type: " + type);
		}
    }
	
	@Override
    public Iterable<?> getItems() throws Exception {
	    return new JobIterable(getPageIDs());
    }
	
	/**
	 * Get an iterable over batches that are not marked as 'deleted'.
	 * 
	 * @throws Exception If there is a problem retrieving the batches.
	 */
	public BatchIterable getLiveBatches() throws Exception {
		List<BatchColumn> sort = Utils.makeArrayList(BatchColumn.BatchName);
		Collection<Integer> batchIDs = DB.getDB()
			.getMatchingBatches(null, null, null, false,
			                    0, -1, // Page start/size
			                    sort, null); // Sort/match count
		
		return new BatchIterable(batchIDs);
	}
	
	@Override
	public boolean getMobileOriginated(Job job)
	{
		return JobActionLib.isMobileOriginated(job);
	}
	
	@Override
	public boolean getAlwaysAvailable(Job job)
	{
		return JobActionLib.isAlwaysAvailable(job);
	}
	
	@Override
    public JobBatch getJobBatch(Job job) throws Exception {
	    return JobActionLib.getJobBatch(job);
    }

	@Override
    public Modem getJobModem(Job job) throws Exception {
	    return JobActionLib.getJobModem(job);
    }

	@Override
    public String getJobState(Job job) {
	    return JobActionLib.getJobState(this, job);
    }

	@Override
    public User getJobUser(Job job) throws Exception {
	    return JobActionLib.getJobUser(job);
    }

	@Override
    public Date getLastTry(Job job) throws Exception {
	    return JobActionLib.getLastTry(job);
    }

	@Override
    public String getStatus(Object obj) {
	    return JobActionLib.getJobStatus((Job)obj);
    }
}

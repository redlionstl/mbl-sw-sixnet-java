/*
 * JobStatusProvider.java
 *
 * Guarantees that the implementor provides a function to
 * provide job status on demand, along with some other functions
 * to retrieve useful information about a job.
 *
 * Jonathan Pearson
 * May 1, 2009
 *
 */

package com.sixnetio.BVB.Web.JobActions;

import java.util.Date;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.StatusProvider;

public interface JobStatusProvider extends StatusProvider {
	/**
	 * Examines the given job, returning a string describing the status of the
	 * job.
	 * 
	 * @param obj The job to check.
	 * @return {@link StatusProvider#S_NORMAL} if the job has experienced no
	 *         failures, {@link StatusProvider#S_WARNING} if the job has failed
	 *         at least once.
	 */
	@Override
	public String getStatus(Object obj);
	
	/**
	 * Check whether the given job is mobile-originated.
	 * 
	 * @param job The job to check.
	 * @return <tt>true</tt> if it is mobile-originated, <tt>false</tt> if not.
	 */
	public boolean getMobileOriginated(Job job);
	
	/**
	 * Check whether the given job is always available.
	 * 
	 * @param job The job to check.
	 * @return <tt>true</tt> if it is always available, <tt>false</tt> if not.
	 */
	public boolean getAlwaysAvailable(Job job);
	
	/**
	 * Get the batch associated with the job.
	 * 
	 * @param job The job to check.
	 * @return The batch associated with the job, or <tt>null</tt> if there is
	 *         none.
	 * @throws Exception If there is a problem retrieving the batch.
	 */
	public JobBatch getJobBatch(Job job) throws Exception;
	
	/**
	 * Get the modem associated with the job.
	 * 
	 * @param job The job to check.
	 * @return The modem associated with the job.
	 * @throws Exception If there is a problem retrieving the modem.
	 */
	public Modem getJobModem(Job job) throws Exception;
	
	/**
	 * Get the user associated with the job.
	 * 
	 * @param job The job to check.
	 * @return The user associated with the job, or <tt>null</tt> if there is
	 *         none.
	 * @throws Exception If there is a problem retrieving the user.
	 */
	public User getJobUser(Job job) throws Exception;
	
	/**
	 * Get the timestamp of the last attempt on this job, or <tt>null</tt> if it
	 * has not yet been attempted.
	 * 
	 * @param job The job to check.
	 * @return The timestamp of the last attempt, or <tt>null</tt> if no
	 *         attempts have yet been made.
	 * @throws Exception If there is a problem accessing the database.
	 */
	public Date getLastTry(Job job) throws Exception;
	
	/**
	 * Get the state of the job, such as 'Scheduled' or 'Started at (date) by
	 * (machine)' (internationalized, of course).
	 * 
	 * @param job The job to check.
	 * @return The local translation of 'Scheduled' or 'Started at (date) by
	 *         (machine)'.
	 */
	public String getJobState(Job job);
}

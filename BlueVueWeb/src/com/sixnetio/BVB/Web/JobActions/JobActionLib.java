/*
 * JobActionLib.java
 *
 * Provides some library functions common to Job actions.
 *
 * Jonathan Pearson
 * May 4, 2009
 *
 */

package com.sixnetio.BVB.Web.JobActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.SortColumns.JobColumn;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class JobActionLib {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Examines the given job, returning a string describing the status of the
	 * job.
	 * 
	 * @param job The job to check.
	 * @return {@link JobStatusProvider#S_NORMAL} if the job has experienced no
	 *         failures, {@link JobStatusProvider#S_WARNING} if the job has
	 *         failed at least once.
	 */
	public static String getJobStatus(Job job) {
		if (job.getJobData().tries == 0) {
			return JobStatusProvider.S_NORMAL;
		} else {
			return JobStatusProvider.S_WARNING;
		}
	}
	
	/**
	 * Check whether the given job is mobile-originated.
	 * 
	 * @param job The job to check.
	 * @return <tt>true</tt> if the job is mobile-originated, <tt>false</tt>
	 *   otherwise.
	 */
	public static boolean isMobileOriginated(Job job)
	{
		return job.getJobData().parameters.getBooleanContents("mobileOriginated");
	}
	
	/**
	 * Check whether the given job is always available.
	 * 
	 * @param job The job to check.
	 * @return <tt>true</tt> if the job is always available, <tt>false</tt>
	 *   otherwise.
	 */
	public static boolean isAlwaysAvailable(Job job)
	{
		return job.getJobData().parameters.getBooleanAttribute("mobileOriginated alwaysAvailable");
	}
	
	/**
	 * Get the batch associated with the given job.
	 * 
	 * @param job The job.
	 * @return The batch associated with the job, or <tt>null</tt> if there is
	 *         none.
	 * @throws Exception If a database error occurs.
	 */
	public static JobBatch getJobBatch(Job job) throws Exception {
		if (job.getJobData().batchID == null) return null;
		
		JobBatch batch = DB.getDB().getBatch(job.getJobData().batchID);
		
		return batch;
	}
	
	/**
	 * Get the modem associated with the given job.
	 * 
	 * @param job The job.
	 * @return The modem associated with the job.
	 * @throws Exception If a database error occurs.
	 */
	public static Modem getJobModem(Job job) throws Exception {
		Modem modem = DB.getDB().getModem(job.getJobData().modemID);
		
		return modem;
	}
	
	/**
	 * Get the user associated with the given job.
	 * 
	 * @param job The job.
	 * @return The user associated with the job, or <tt>null</tt> if there is
	 *         none.
	 * @throws Exception If a database error occurs.
	 */
	public static User getJobUser(Job job) throws Exception {
		if (job.getJobData().userID == null) return null;
		
		User user = DB.getDB().getUser(job.getJobData().userID);
		
		return user;
	}
	
	/**
	 * Translate a string describing sorting columns into a list of JobColumns.
	 * 
	 * @param columns The description of the sorting columns.
	 * @return A list that could be passed to
	 *         {@link Database#getMatchingJobs(Long, Integer, Integer, Integer, String, Date, Date, Boolean, int, int, List, long[])}
	 *         .
	 */
	public static List<JobColumn> translateSort(String columns) {
		String[] cols = Utils.tokenize(columns, ",", false);
		List<JobColumn> sortOn = new Vector<JobColumn>(cols.length);
		
		for (String column : cols) {
			if (column.startsWith("!")) {
				sortOn.add(JobColumn.Reverse);
				column = column.substring(1);
			}
			
			sortOn.add(JobColumn.valueOf(column));
		}
		
		return sortOn;
	}
	
	/**
	 * Given an existing sort list, add a new primary column to it, shuffling
	 * the existing columns as necessary.
	 * 
	 * @param colName The new column to add.
	 * @param oldSortOn The existing sort list, or <tt>null</tt> if there wasn't
	 *            one. If synchronization is necessary, it must be handled by
	 *            the caller.
	 * @return A new sort list. If the new column was already the primary
	 *         column, reverses the sorting order. If it was already in the
	 *         list, moves it to the beginning in normal order. If it did not
	 *         exist, adds it at the beginning in normal order.
	 */
	public static String addSortColumn(String colName, List<JobColumn> oldSortOn) {
		JobColumn col;
		
		try {
			col = JobColumn.valueOf(colName);
		} catch (IllegalArgumentException iae) {
			logger.debug("Not a legal column name: '" + colName + "'", iae);
			
			return translateSort(oldSortOn);
		}
		
		// Copy the sortOn object, since we don't actually want to change it
		List<JobColumn> sortOn;
		
		if (oldSortOn == null) {
			// It's only going to receive one item
			sortOn = new ArrayList<JobColumn>(1);
		} else {
			sortOn = new ArrayList<JobColumn>(oldSortOn);
		}
		
		// Get the index of the column, to see if it's already part of the sort
		// order
		int index = sortOn.indexOf(col);
		
		// Whether to apply a 'Reverse' just before the column
		boolean reverse = false;
		
		// If the column was already in there...
		if (index != -1) {
			// Remove it
			sortOn.remove(index);
			
			// If there was a reverse just before it...
			if (index > 0 && sortOn.get(index - 1) == JobColumn.Reverse) {
				sortOn.remove(index - 1);
				
				reverse = true;
				index--; // So we can more easily tell if it was the first
				// column
			}
			
			// If it was the first sort column
			if (index == 0) {
				// Swap the reverse property
				reverse = !reverse;
			}
		}
		
		// Push the column onto the front of the sort order
		sortOn.add(0, col);
		
		// If reversing, push reverse onto the front also
		if (reverse) {
			sortOn.add(0, JobColumn.Reverse);
		}
		
		// Return the new sorting order
		return translateSort(sortOn);
	}
	
	/**
	 * Translate a sort list into a string.
	 * 
	 * @param sortOn The sort list to translate. If synchronization is
	 *            necessary, it must be handled by the caller.
	 * @return A descriptive string.
	 */
	public static String translateSort(List<JobColumn> sortOn) {
		if (sortOn == null) return "";
		
		StringBuilder builder = new StringBuilder();
		
		boolean skipComma = true;
		for (JobColumn column : sortOn) {
			if (skipComma) {
				skipComma = false;
			} else {
				builder.append(",");
			}
			
			if (column == JobColumn.Reverse) {
				builder.append("!");
				skipComma = true;
			} else {
				builder.append(column.name());
			}
		}
		
		return builder.toString();
	}
	
	public static Date getLastTry(Job job) throws Exception {
		// No tries? No last-attempted date
		if (job.getJobData().tries == 0) return null;
		
		Collection<Long> historyIDs =
		        DB.getDB().getMatchingHistory(null, job.getJobData().jobID, null, null, null, null,
		                                      null, null, null, 0, -1, null, null);
		
		// Must have been phased out of the database
		if (historyIDs.size() == 0) return null;
		
		// Since that is already sorted by decreasing timestamp, just grab the
		// timestamp of the first entry
		long firstEntryID = historyIDs.iterator().next();
		
		HistoryEntry firstEntry = DB.getDB().getHistoryEntry(firstEntryID);
		
		// Must have been phased out of the database just now
		if (firstEntry == null) return null;
		
		return firstEntry.timestamp;
	}
	
	public static String getJobState(GeneralAction genAct, Job job) {
		if (job.getJobData().lockedBy == null) {
			return genAct.getText("i18n.job.state.scheduled");
		} else {
			List<Object> args = new ArrayList<Object>(2);
			args.add(job.getJobData().lockedAt);
			args.add(job.getJobData().lockedBy);
			
			return genAct.getText("i18n.job.state.started", args);
		}
	}
}

/*
 * JobAction.java
 *
 * Loads a job from the database and provides CRUD actions on it (minus the Create).
 *
 * Jonathan Pearson
 * May 4, 2009
 *
 */

package com.sixnetio.BVB.Web.JobActions;

import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Jobs.Job.JobData;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class JobAction extends GeneralAction implements Preparable, JobStatusProvider {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// The ID of the modem that the user specified
	private long id = Job.JobData.INV_JOB;
	
	// The actual job, created here or loaded from the database (for
	// single-job actions)
	private Job job;
	
	/**
	 * Get the ID number of the job that the user specified, or
	 * {@link JobData#INV_JOB} if no job was specified.
	 */
	public long getId() {
		return id;
	}
	
	/** Get the job. */
	public Job getJob() {
		return job;
	}
	
	/**
	 * The user specified the View action.
	 * 
	 * @return {@link GeneralAction#VIEW}.
	 * @throws Exception Never.
	 */
	public String view() throws Exception {
		verifyUserLoggedIn();
		
		return VIEW;
	}
	
	/**
	 * The user specified the Edit action. Verifies that the user has job edit
	 * permission (Enhanced user) and asks for more input.
	 * 
	 * @return INPUT.
	 * @throws Exception If the user does not have job edit permission.
	 */
	public String edit() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.updatePermission"));
		
		return INPUT;
	}
	
	/**
	 * The user has finished editing a job. Verifies that the user has job edit
	 * permission (Enhanced user), saves the job, and redirects to the View
	 * page.
	 * 
	 * @return {@link GeneralAction#VIEW}, or INPUT if no job ID was specified.
	 * @throws Exception If the user does not have job edit permission, or there
	 *             is a database exception trying to save the job.
	 */
	public String update() throws Exception {
		verifyPermission(User.UserType.Enhanced, getText("error.updatePermission"));
		
		if (id == Job.JobData.INV_JOB) {
			addActionError(getText("error.noJob"));
			return INPUT;
		} else {
			DB.getDB().updateJob(job);
			
			LogEntry entry =
			        new LogEntry(new Date(), getUser().userID, DBLogger.INFO,
			                     String.format("Updated Job #%d", job.getJobData().jobID));
			DB.getDB().log(Utils.makeLinkedList(entry));
		}
		
		return VIEW;
	}
	
	@Override
	public void prepare() throws Exception {
		String sID = getRequest().getParameter("id");
		
		if (sID != null) {
			try {
				id = Integer.parseInt(sID);
				
				logger.debug("User specified job with ID " + id);
				
				job = DB.getDB().getJob(id, true);
			} catch (NumberFormatException nfe) {
				throw new Exception(getText("error.invalidJobID", new String[] {
					sID
				}), nfe);
			}
		} else {
			id = Job.JobData.INV_JOB;
			
			addActionError(getText("error.noJob"));
		}
	}
	
	public Map<String, String> getExtraProperties() {
		Tag[] props = job.getJobData().parameters.getEachTag("", null);
		
		Map<String, String> propMap = new HashMap<String, String>();
		for (Tag prop : props) {
			// Special cases go here
			if (prop.getName().equals("mobileOriginated")) {
				// Mobile-originated is treated as a first-class property, so
				// skip it
				continue;
			}
			
			propMap.put(prop.getName(), prop.getStringContents());
		}
		
		return propMap;
	}
	
	@Override
	public String getStatus(Object obj) {
		return JobActionLib.getJobStatus((Job)obj);
	}
	
	@Override
	public boolean getMobileOriginated(Job job)
	{
		return JobActionLib.isMobileOriginated(job);
	}
	
	@Override
	public boolean getAlwaysAvailable(Job job)
	{
		return JobActionLib.isAlwaysAvailable(job);
	}
	
	// To make the property get/set work properly, we also provide no-argument
	// versions of the getMobileOriginated() and getAlwaysAvailable functions
	
	/**
	 * Get the value of the 'mobileOriginated' property of this job.
	 */
	public boolean getMobileOriginated()
	{
		return JobActionLib.isMobileOriginated(job);
	}
	
	/**
	 * Get the value of the 'mobileOriginated alwaysAvailable' property of this
	 * job.
	 */
	public boolean getAlwaysAvailable()
	{
		return JobActionLib.isAlwaysAvailable(job);
	}
	
	/**
	 * Set the value of the 'mobileOriginated' property of this job.
	 * 
	 * @param value The new value.
	 */
	public void setMobileOriginated(boolean value)
	{
		Tag tag = job.getJobData().parameters.getTag("mobileOriginated");
		
		if (tag == null) {
			tag = new Tag("mobileOriginated");
			job.getJobData().parameters.addContent(tag);
		}
		else {
			tag.removeContents();
		}
		
		tag.addContent("" + value);
	}
	
	/**
	 * Set the value of the 'mobileOriginated alwaysAvailable' property of this
	 * job.
	 * 
	 * @param value The new value.
	 */
	public void setAlwaysAvailable(boolean value)
	{
		Tag tag = job.getJobData().parameters.getTag("mobileOriginated");
		
		if (tag == null) {
			tag = new Tag("mobileOriginated");
			
			// If it was not there, then it was not mobile originated
			tag.addContent("" + false);
			job.getJobData().parameters.addContent(tag);
		}
		
		tag.setAttribute("alwaysAvailable", value);
	}
	
	@Override
	public JobBatch getJobBatch(Job job) throws Exception {
		return JobActionLib.getJobBatch(job);
	}
	
	@Override
	public Modem getJobModem(Job job) throws Exception {
		return JobActionLib.getJobModem(job);
	}
	
	@Override
	public User getJobUser(Job job) throws Exception {
		return JobActionLib.getJobUser(job);
	}
	
	@Override
	public Date getLastTry(Job job) throws Exception {
		return JobActionLib.getLastTry(job);
	}
	
	@Override
	public String getJobState(Job job) {
		return JobActionLib.getJobState(this, job);
	}
}

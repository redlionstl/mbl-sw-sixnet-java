/*
 * PreferencesAction.java
 *
 * Provides a sectioned lists of preferences.
 *
 * Jonathan Pearson
 * May 7, 2009
 *
 */

package com.sixnetio.BVB.Web.PreferencesActions;

import java.io.InputStream;
import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.User;
import com.sixnetio.BVB.Web.*;
import com.sixnetio.util.Utils;

public class PreferencesListAction extends GeneralAction implements Preparable {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public class Link {
		public String i18n; // Untranslated title
		public String title; // Translated via I18n
		public boolean dflt;
		public String value;
		
		public Link(Tag linkTag) {
			this.i18n = linkTag.getStringAttribute("title");
			this.title = getText(this.i18n);
			this.dflt = linkTag.getBooleanAttribute("default");
			this.value = linkTag.getStringContents();
		}
	}
	
	public class Column {
		public String i18n; // Untranslated title
		public String title; // Translated via I18n
		public String sort;
		public String id;
		public String format;
		public String type;
		public boolean dflt;
		
		// Link value --> Link
		public Map<String, Link> links;
		public String value;
		
		public Column(Tag colTag) {
			this.i18n = colTag.getStringAttribute("title");
			this.title = getText(this.i18n);
			this.sort = colTag.getStringAttribute("sort");
			this.id = colTag.getStringAttribute("id");
			this.format = colTag.getStringAttribute("format");
			this.type = colTag.getStringAttribute("type");
			this.dflt = colTag.getBooleanAttribute("default");
			
			this.links = new LinkedHashMap<String, Link>();
			for (Tag linkTag : colTag.getEachTag("linking", "link")) {
				Link link = new Link(linkTag);
				this.links.put(link.value, link);
			}
			
			this.value = colTag.getStringContents("value");
		}
	}
	
	public class Preference {
		public String i18n; // Untranslated title
		public String title; // Translated via I18n
		public String id; // Unique identifier
		public String name; // Tag name in user preferences
		public String value; // User value, of default if not specified
		
		public Preference(Tag prefTag) {
			this.i18n = prefTag.getStringAttribute("title");
			this.title = getText(this.i18n);
			this.id = prefTag.getStringAttribute("id");
			this.name = prefTag.getStringContents();
			this.value =
			        Preferences.getString(this.name, getUser().preferences,
			                              prefTag.getStringAttribute("default"));
		}
	}
	
	// Input/Output parameters
	/**
	 * The section of preferences to display/edit. "[general]" for the top-level
	 * section.
	 */
	// Set by prepare()
	private String section = "[general]";
	
	/**
	 * The user ID to show. INV_USER for the logged-in user.
	 */
	// Set by prepare()
	private int id = User.INV_USER;
	private User user; // The user whose preferences are being shown
	
	/** Chosen column links. */
	// Column ID --> Link value
	// This is retrieved in prepare()
	public Map<String, String> chosenLinks = new HashMap<String, String>();
	
	// Output parameters
	
	/** Lookup from section name to display string. */
	// section name --> display string
	public Map<String, String> sections = new LinkedHashMap<String, String>();
	
	/** The map of preferences for the given section. */
	// ID --> Preference
	public Map<String, Preference> prefs = new LinkedHashMap<String, Preference>();
	
	/** The list of available columns for the current section. */
	// ID --> Column
	public Map<String, Column> availableColumns = new LinkedHashMap<String, Column>();
	
	/** The list of chosen columns for the current section. */
	// ID
	public List<String> chosenColumns = new LinkedList<String>();
	
	public String getSection() {
		return section;
	}
	
	public int getId() {
		return id;
	}
	
	public User getTargetUser() {
		return user;
	}
	
	public List<String> getUnchosenColumns() {
		List<String> unchosenColumns = new LinkedList<String>(availableColumns.keySet());
		unchosenColumns.removeAll(chosenColumns);
		
		if (logger.isDebugEnabled()) {
			logger.debug("There are " + unchosenColumns.size() + " unchosen columns: " +
			             unchosenColumns.toString());
		}
		
		return unchosenColumns;
	}
	
	/**
	 * The user specified the List action. Loads a list of preferences in the
	 * given section.
	 * 
	 * @return {@link GeneralAction#LIST}.
	 * @throws Exception If the user is not logged in.
	 */
	public String list() throws Exception {
		// Update with what the user actually has in his preferences
		// Preferences
		for (Map.Entry<String, Preference> entry : prefs.entrySet()) {
			Preference pref = entry.getValue();
			if (section.equals("[general]")) {
				pref.value = Preferences.getString(pref.name, user.preferences, pref.value);
			} else {
				pref.value =
				        Preferences.getString(section + "/" + pref.name, user.preferences,
				                              pref.value);
			}
		}
		
		// Chosen columns
		// Also, remove chosen columns from available columns so they can't
		// duplicate
		Tag chosenColumns;
		if (section.equals("[general]")) {
			chosenColumns = Preferences.getTag("columns", user.preferences);
		} else {
			chosenColumns = Preferences.getTag(section + "/columns", user.preferences);
		}
		
		if (chosenColumns != null) {
			for (Tag tag : chosenColumns.getEachTag("", "column")) {
				String colID = tag.getStringAttribute("id");
				this.chosenColumns.add(colID);
				this.chosenLinks.put(colID, tag.getStringAttribute("link"));
			}
		} else if (logger.isDebugEnabled()) {
			logger.debug("No columns found for section " + section);
		}
		
		return LIST;
	}
	
	public String update() throws Exception {
		// Update the user preferences with the new values from chosenColumns
		// and link_[id] fields
		Tag userPrefsTag = user.preferences;
		if (userPrefsTag == null) {
			userPrefsTag = new Tag("userprefs");
			user.preferences = userPrefsTag;
		}
		
		// Grab the current section out of userPrefs
		Tag sectionTag;
		if (section.equals("[general]")) {
			// No actual section for this
			sectionTag = userPrefsTag;
		} else {
			sectionTag = userPrefsTag.getTag(section);
			
			if (sectionTag == null) {
				sectionTag = new Tag(section);
				userPrefsTag.addContent(sectionTag);
			}
		}
		
		// Blank out the section tag and replace with the new preferences
		sectionTag.removeContents();
		
		// Preferences
		for (Map.Entry<String, Preference> entry : prefs.entrySet()) {
			Preference pref = entry.getValue();
			
			Tag prefTag = new Tag(pref.name);
			prefTag.addContent(pref.value);
			
			sectionTag.addContent(prefTag);
		}
		
		// Columns
		Tag columnsTag = new Tag("columns");
		sectionTag.addContent(columnsTag);
		for (String columnID : chosenColumns) {
			Column col = availableColumns.get(columnID);
			
			if (col == null) continue;
			
			Tag colTag = new Tag("column");
			colTag.setAttribute("title", col.i18n);
			colTag.setAttribute("sort", col.sort);
			colTag.setAttribute("format", col.format);
			colTag.setAttribute("type", col.type);
			colTag.setAttribute("id", col.id);
			
			if (chosenLinks.containsKey(col.id)) {
				colTag.setAttribute("link", chosenLinks.get(col.id));
			}
			
			colTag.addContent(col.value);
			
			columnsTag.addContent(colTag);
		}
		
		DB.getDB().updateUser(user);
		
		return INPUT;
	}
	
	@Override
	public void prepare() throws Exception {
		verifyUserLoggedIn();
		
		if (getRequest().getParameter("section") != null) {
			// Default is [general] if not provided
			section = getRequest().getParameter("section");
			
			logger.debug("User specified section '" + section + "'");
		}
		
		if (getRequest().getParameter("id") != null) {
			try {
				id = Integer.parseInt(getRequest().getParameter("id"));
				
				logger.debug("User specified user " + id);
			} catch (NumberFormatException nfe) {
				throw new Exception(getText("error.invalidUserID"), nfe);
			}
		}
		
		// If trying to view preferences for a user other than yourself, you
		// must be a root user
		if (id == User.INV_USER) {
			id = getUser().userID;
			user = getUser();
		} else if (id != getUser().userID) {
			verifyPermission(User.UserType.Root, getText("error.notYou"));
		}
		
		if (user == null) {
			user = DB.getDB().getUser(id);
			
			if (user == null) throw new Exception(getText("error.noSuchUser"));
		}
		
		// Populate the common maps
		populateData();
		
		Map<?, ?> parameterMap = getRequest().getParameterMap();
		
		// Hunt down:
		// * "link_[id]" fields for link values
		// * "pref_[id]" fields for preference values
		for (Map.Entry<?, ?> entry : parameterMap.entrySet()) {
			String key = (String)entry.getKey();
			String[] values = (String[])entry.getValue();
			
			if (values == null || values.length == 0) continue;
			
			if (key.startsWith("link_")) {
				key = key.substring(key.indexOf('_') + 1);
				chosenLinks.put(key, values[0]);
				
				logger.debug("Found link '" + key + "' --> " + values[0]);
			} else if (key.startsWith("pref_")) {
				key = key.substring(key.indexOf('_') + 1);
				prefs.get(key).value = values[0];
				
				logger.debug("Found preference '" + key + "' --> " + values[0]);
			}
		}
	}
	
	// Populate the 'sections', 'prefs', and 'availableColumns' maps
	private void populateData() throws Exception {
		InputStream in = getClass().getResourceAsStream("../allprefs.xml");
		
		if (in == null) {
			throw new Exception(getText("error.cannotRetrievePrefs"));
		}
		
		Tag allPrefs;
		try {
			allPrefs = Utils.xmlParser.parseXML(in, false, false);
		} finally {
			in.close();
		}
		
		// Find the specified section
		// At the same time, index all of the sections
		logger.debug("Searching for section '" + section + "'");
		
		Tag[] allSections = allPrefs.getEachTag("", "section");
		Tag sectionTag = null;
		for (Tag tag : allSections) {
			String access = tag.getStringAttribute("access", "Basic");
			
			if (hasPermission(user, access)) {
				sections.put(tag.getStringAttribute("name"),
				             getText(tag.getStringAttribute("title")));
				
				if (tag.getStringAttribute("name").equals(section)) {
					if (sectionTag != null) {
						throw new Exception(getText("error.duplicateSection",
						                            Utils.makeLinkedList((Object)section)));
						
					}
					
					logger.debug("Found the section");
					sectionTag = tag;
					
					// Can't break out, we still need to index the rest of the
					// sections
				}
			}
		}
		if (sectionTag == null) {
			throw new Exception(getText("error.noSuchSection",
			                            Utils.makeLinkedList((Object)section)));
		}
		
		// Load preferences from the given section
		// Individual preferences
		for (Tag tag : sectionTag.getEachTag("prefs", "pref")) {
			Preference pref = new Preference(tag);
			
			prefs.put(pref.id, pref);
		}
		
		// Available columns
		Tag availableColumns = sectionTag.getTag("columns");
		if (availableColumns != null) {
			for (Tag tag : availableColumns.getEachTag("", "column")) {
				Column column = new Column(tag);
				this.availableColumns.put(column.id, column);
			}
		}
	}
}

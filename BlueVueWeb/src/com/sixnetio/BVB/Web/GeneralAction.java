/*
 * GeneralAction.java
 *
 * Provides methods and data applicable to most actions.
 *
 * Jonathan Pearson
 * April 15, 2009
 *
 */

package com.sixnetio.BVB.Web;

import java.text.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.jonp.xml.Tag;

import com.opensymphony.xwork2.ActionSupport;
import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.Restrictions.Restriction;
import com.sixnetio.BVB.Common.User.UserType;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public abstract class GeneralAction extends ActionSupport implements ServletRequestAware,
        SessionAware {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public static final String PROG_VERSION = "@bvg.major@.@bvg.minor@.@bvg.build@";
	
	/** Actions return this to display the View view. */
	public static final String VIEW = "view";
	
	/** Actions return this to display the multi-list view. */
	public static final String LIST = "list";
	
	private HttpServletRequest request;
	private Map<String, Object> session;
	
	/**
	 * Required by ServletRequestAware, sets the servlet Request object.
	 */
	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	/**
	 * Required by SessionAware, sets the session object.
	 */
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
	
	/**
	 * Get the servlet request object set by
	 * {@link #setServletRequest(HttpServletRequest)}.
	 */
	public HttpServletRequest getRequest() {
		return request;
	}
	
	/**
	 * Get the session object set by {@link #setSession(Map)}.
	 */
	public Map<String, Object> getSession() {
		return session;
	}
	
	/**
	 * Get the document root for this instance of the Web UI. This assumes that
	 * the Web UI is running from one level below the root (for example,
	 * /BlueVueWeb).
	 */
	public String getDocumentRoot() {
		String contextPath = request.getContextPath();
		
		// The document root is the first part of the context path
		if (contextPath.indexOf('/', 1) == -1) {
			// There are no '/' chars beyond the first
			return contextPath;
		} else {
			// There are '/' chars beyond the first
			// Trim so we are only returning the first part
			return contextPath.substring(0, contextPath.indexOf('/', 1));
		}
	}
	
	/**
	 * Parse and return the URI path, splitting on '/' characters.
	 * 
	 * @return Components of the URI path split on '/' characters. For example,
	 *         'path/to/here.action' would become
	 *         <ol>
	 *         <li>path</li>
	 *         <li>to</li>
	 *         <li>here.action</li>
	 *         </ol>
	 */
	public String[] parseURI() {
		return Utils.tokenize(getRequest().getRequestURI(), "/", false);
	}
	
	/**
	 * Get the logged-in user.
	 * 
	 * @return The logged-in user (from the session), or <tt>null</tt> if no
	 *         user is logged in.
	 */
	public User getUser() {
		return (User)getRequest().getSession().getAttribute("User");
	}
	
	/**
	 * Set the logged-in user for the session.
	 * 
	 * @param user The user to set as logged-in for the session.
	 */
	public void setUser(User user) {
		getRequest().getSession().setAttribute("User", user);
	}
	
	/**
	 * Verify that there is a user logged-in for the session.
	 * 
	 * @throws Exception If there is no logged-in user.
	 */
	public void verifyUserLoggedIn() throws Exception {
		if (!isLoggedIn()) throw new NotLoggedInException(getText("error.notLoggedIn"));
	}
	
	/**
	 * Check whether there is a user is logged in.
	 * 
	 * @return True = there is a user logged in; false = no user is logged in.
	 */
	public boolean isLoggedIn() {
		return (getUser() != null);
	}
	
	/**
	 * Verify that the logged-in user's type has at least the permission level
	 * as the passed-in type.
	 * 
	 * @param perm The permission level that is required.
	 * @param noPermissionMsg The message for the exception, if the user does
	 *            not have the required permission.
	 * @throws Exception If the user is not logged in or does not have the
	 *             required permission level.
	 */
	public void verifyPermission(User.UserType perm, String noPermissionMsg) throws Exception {
		verifyUserLoggedIn();
		
		if (!hasPermission(perm)) {
			throw new NoPermissionException(noPermissionMsg);
		}
	}
	
	/**
	 * Check whether the logged-in user has at least the permission level
	 * specified.
	 * 
	 * @param perm The permission level to check for.
	 * @return True = the user has at least that permission level; False = the
	 *         user does not have that permission or is not logged in.
	 */
	public boolean hasPermission(User.UserType perm) {
		if (!isLoggedIn()) return false;
		
		return hasPermission(getUser(), perm);
	}
	
	/**
	 * Check whether the specified user has at least the permission level
	 * specified.
	 * 
	 * @param user The user to check.
	 * @param perm The permission level to check for.
	 * @return True = the user has at least that permission level; False = the
	 *         user does not have that permission.
	 */
	public boolean hasPermission(User user, User.UserType perm) {
		boolean hasPerm = (user.type.compareTo(perm) >= 0);
		
		logger.debug(String.format("User %s of type %s %s have %s", user.name,
		                           user.type.toString(), hasPerm ? "does" : "does not",
		                           perm.toString()));
		return hasPerm;
	}
	
	/**
	 * Check whether the logged-in user has at least the permission level
	 * specified.
	 * 
	 * @param perm The name of the permission level (see {@link UserType}).
	 * @return True = the user has at least that permission level; False = the
	 *         user does not have that permission or is not logged in.
	 * @throws IllegalArgumentException If there is no such permission as the
	 *             one named.
	 * @throws NullPointerException If the specified permission is <tt>null</tt>
	 *             .
	 */
	public boolean hasPermission(String perm) {
		User.UserType realPerm = User.UserType.valueOf(perm);
		
		return hasPermission(realPerm);
	}
	
	/**
	 * Check whether the specified user has at least the permission level
	 * specified.
	 * 
	 * @param user The user to check.
	 * @param perm The name of the permission level (see {@link UserType}).
	 * @return True = the user has at least that permission level; False = the
	 *         user does not have that permission.
	 * @throws IllegalArgumentException If there is no such permission as the
	 *             one named.
	 * @throws NullPointerException If the specified permission is <tt>null</tt>
	 *             .
	 */
	public boolean hasPermission(User user, String perm) {
		User.UserType realPerm = User.UserType.valueOf(perm);
		
		return hasPermission(user, realPerm);
	}
	
	/**
	 * Get an iterable over the matching user preference tags.
	 * 
	 * @param path The path to the parent tag.
	 * @param name The name of the tags to retrieve.
	 * @return An iterable to run across those matching tags.
	 */
	public Iterable<Tag> getPrefTags(String path, String name) {
		return Preferences.getTags(path, name, getUser().preferences);
	}
	
	public String formatData(String format, Object data) throws Exception {
		String str;
		
		if (format.equals("format.string")) {
			str = data.toString();
		} else if (format.equals("format.number")) {
			NumberFormat fmt = NumberFormat.getInstance(getRequest().getLocale());
			
			if (data instanceof Number) {
				// These are in order of likelihood to be passed in
				if (data instanceof Double) {
					str = fmt.format(((Double)data).doubleValue());
				} else if (data instanceof Integer) {
					str = fmt.format(((Integer)data).longValue());
				} else if (data instanceof Float) {
					str = fmt.format(((Float)data).doubleValue());
				} else if (data instanceof Long) {
					str = fmt.format(((Long)data).longValue());
				} else if (data instanceof Short) {
					str = fmt.format(((Short)data).longValue());
				} else if (data instanceof Byte) {
					str = fmt.format(((Byte)data).longValue());
				} else {
					// Fallback for unknown type
					str = data.toString();
				}
			} else {
				try {
    				if (data.toString().indexOf('.') == -1) {
    					str = fmt.format(Long.parseLong(data.toString()));
    				} else {
    					str = fmt.format(Double.parseDouble(data.toString()));
    				}
				} catch (NumberFormatException nfe) {
					logger.warn("Unable to determine type of '" +
					            data.toString() + "'");
					
					str = data.toString();
				}
			}
		} else if (format.equals("format.date") || format.equals("format.time")
		           || format.equals("format.datetime")
		           || format.equals("format.duration")) {
			
			DateFormat fmt;
			
			// NOTE: Date/time formats need to be DateFormat.SHORT for Struts to
			//   understand how to parse them and store them automatically into
			//   Date objects
			
			if (format.equals("format.date")) {
				fmt = DateFormat.getDateInstance(DateFormat.SHORT,
				                                 getRequest().getLocale());
			} else if (format.equals("format.time")) {
				fmt = DateFormat.getTimeInstance(DateFormat.SHORT,
		                                            getRequest().getLocale());
			} else if (format.equals("format.datetime")
			           || format.equals("format.duration")) {
				
				fmt = DateFormat.getDateTimeInstance(DateFormat.SHORT,
				                                     DateFormat.SHORT,
				                                     getRequest().getLocale());
			} else {
				// This shouldn't happen, we just checked for all three of these
				//   to get into this clause
				throw new RuntimeException("Unexpected format value: " + format);
			}
			
			Date dt;
			
			if (data instanceof Date) {
				// It could be something like a java.sql.Timestamp, which includes
				//   nanoseconds, so clean it up
				dt = new Date(((Date)data).getTime());
			} else {
				try {
					dt = fmt.parse(data.toString());
				} catch (ParseException pe) {
					// Try the standard format
					try {
						dt = Utils.parseStandardDateTime(data.toString());
					} catch (ParseException pe2) {
						logger.warn("Unable to parse '" + data.toString() +
						            "' as '" + format + "'");
						
						dt = null;
					}
				}
			}
			
			if (dt != null) {
				if (format.equals("format.duration")) {
					str = durationOf(dt);
				} else {
					str = fmt.format(dt);
				}
			} else {
				logger.warn("Unable to determine type of '" +
				            data.toString() + "'");
				
				str = data.toString();
			}
		} else {
			logger.warn("Unrecognized format: '" + format + "'");
			
			str = data.toString();
		}
		
		logger.debug("Formatting '" + data.toString() + "' as " + format +
		             ": '" + str + "'");
		
		return str;
	}
	
	/**
	 * Get a string representing the given date as a time duration.
	 * 
	 * @param value The date representing the time duration.
	 * @return A string value representing that duration, according to the
	 *         property 'format.duration'.
	 */
	public String durationOf(Date value) {
		long ms = value.getTime();
		
		long s = ms / 1000;
		long m = s / 60;
		long h = m / 60;
		long d = h / 24;
		
		ms -= s * 1000;
		s -= m * 60;
		m -= h * 60;
		h -= d * 24;
		
		List<Object> args = new ArrayList<Object>(5);
		args.add(d);
		args.add(h);
		args.add(m);
		args.add(s);
		args.add(ms);
		
		return getText("format.duration", args);
	}
	
	/**
	 * Given a LinkedHashMap of Title onto an individual filter, this builds a
	 * new map of title onto [filter up to this point, entire filter without
	 * this one].
	 * 
	 * @param filters The map of Title onto individual filter. It should look
	 *            something like this:
	 *            <ul>
	 *            <li>IP Address 10.3.11.1 :: qIPAddress=10.3.11.1</li>
	 *            <li>Phone Number 5188775173 :: qPhone=5188775173</li>
	 *            </ul>
	 * 
	 * @return A new map of Title onto [filter up to this point, entire filter
	 *         without this one]. It will look something like this:
	 *         <ul>
	 *         <li>IP Address 10.3.11.1 :: [qIPAddress=10.3.11.1,
	 *         qPhone=5188775173]</li>
	 *         <li>Phone Number 5188775173 ::
	 *         [qIPAddress=10.3.11.1&qPhone=5188775173, qIPAddress=10.3.11.1]</li>
	 *         </ul>
	 */
	public static LinkedHashMap<String, String[]> constructFilters(
	        LinkedHashMap<String, String> filters) {
		
		logger.debug("Building a filter map");
		
		LinkedHashMap<String, String[]> out = new LinkedHashMap<String, String[]>();
		StringBuilder urlBuilder = new StringBuilder();
		
		for (Map.Entry<String, String> entry : filters.entrySet()) {
			String title = entry.getKey();
			String curFilter = entry.getValue();
			
			if (urlBuilder.length() > 0) urlBuilder.append("&");
			urlBuilder.append(curFilter);
			
			StringBuilder otherBuilder = new StringBuilder();
			for (Map.Entry<String, String> otherEntry : filters.entrySet()) {
				// Skip the current filter
				if (otherEntry.getKey() == entry.getKey()) continue;
				
				if (otherBuilder.length() > 0) otherBuilder.append("&");
				otherBuilder.append(otherEntry.getValue());
			}
			
			out.put(title, new String[] {
			        urlBuilder.toString(), otherBuilder.toString()
			});
			
			logger.debug(String.format("'%s' --> ['%s', '%s']", title, urlBuilder.toString(),
			                           otherBuilder.toString()));
		}
		
		return out;
	}
	
	/**
	 * This is easier than using OGNL's method for getting static fields,
	 * especially because OGNL's method involves surrounding the class name with @
	 * characters, which would cause a filter match/replace by Ant if the class
	 * name matched a filter.
	 */
	public String getProgVersion() {
		// Can't just check for equality, since the stuff between the @s would
		// be replaced here as well
		if (PROG_VERSION.startsWith("@bvweb") && PROG_VERSION.endsWith("@")) {
			return "(Unbuilt development instance)";
		} else {
			return PROG_VERSION;
		}
	}
	
	/**
	 * Returns <tt>null</tt> if the given string is <tt>null</tt> or empty,
	 * otherwise returning a trimmed version of the string.
	 * 
	 * @param s The string to check.
	 * @return <tt>null</tt> if <tt>s</tt> is <tt>null</tt> or
	 *         <tt>s.length() == 0</tt>. Otherwise, returns <tt>s.trim()</tt>.
	 */
	public String prepareParam(String s) {
		if (s == null) return null;
		
		s = s.trim();
		if (s.length() == 0) return null;
		
		return s;
	}
	
	/**
	 * Verify that the given parser contains the expected and required columns.
	 * 
	 * @param parser The parser to verify.
	 * @param expectedColumns If any column from the parser is missing from this
	 *   set, fail. This should include all required columns.
	 * @param requiredColumns If any column from this set is missing from the
	 *   parser, fail.
	 * @return True = the parser contains the appropriate columns. False = the
	 *   parser is either missing columns, or contains unexpected columns.
	 *   Action errors will be added describing these issues.
	 */
	public boolean validateCSVParser(CSVParser parser,
	                                 Set<String> expectedColumns,
	                                 Set<String> requiredColumns) {
		
		boolean result = true;
		
		// Remove each column as we see it
		Set<String> requiredColumnsLocal = new HashSet<String>(requiredColumns);
		
		for (String column : parser.getColumns()) {
			if (!expectedColumns.contains(column)) {
				addActionError(getText("error.unexpectedColumn",
				                       Utils.makeArrayList((Object)column)));
				
				result = false;
			}
			
			requiredColumnsLocal.remove(column);
		}
		
		// If there's anything left, we didn't see it, so it's an error
		for (String column : requiredColumnsLocal) {
			addActionError(getText("error.missingColumn",
			                       Utils.makeArrayList((Object)column)));
			
			result = false;
		}
		
		return result;
	}
	
	// Cache the value from getLicenseKeyDaysRemaining here
	// We do this because of the database access to refresh settings
	private boolean licenseKeyDaysRemainingCached = false;
	private Integer licenseKeyDaysRemaining = null;
	
	/**
	 * Get the number of days remaining on the current license key.
	 * 
	 * @return The number of days remaining, or <tt>null</tt> if there is no
	 *   date restriction on the license key.
	 */
	public Integer getLicenseKeyDaysRemaining() throws Exception {
		if (licenseKeyDaysRemainingCached) {
			return licenseKeyDaysRemaining;
		}
		
		// Make sure the settings are up-to-date
		Server.refreshSettingsNow(DB.getDB());
		
		LicenseKey licenseKey = Server.getLicenseKey();
		if (licenseKey == null) {
			// The server is panicking, but it cannot actually shut down the Web
			//   server, so just return 0
			return 0;
		}
		
		String restriction = licenseKey.getRestrictions()
			.getStringValue(Restriction.DateLimit);
		
		Date endDate;
		if (restriction.length() > 0) {
			try {
				endDate = Utils.parseStandardDateTime(restriction);
			} catch (ParseException pe) {
				logger.error("Unable to parse license key date restriction, " +
				             "assuming there is none");
				
				endDate = null;
			}
		} else {
			endDate = null;
		}
		
		if (endDate == null) {
			return null;
		}
		
		// Get a Date that points to right now
		Date now = new Date();
		
		// Subtract 'now' from the expiration date
		long msRemaining = endDate.getTime() - now.getTime();
		
		// Convert to days
		// 1000 ms/s * 60 s/min * 60 min/hr * 24 hr/day
		licenseKeyDaysRemaining = (int)(msRemaining / (1000 * 60 * 60 * 24));
		licenseKeyDaysRemainingCached = true;
		
		return licenseKeyDaysRemaining;
	}
	
	/**
	 * Get the language and country codes from the locale, separated by hyphens,
	 * and with a leading hyphen. If either piece is missing, an empty string is
	 * returned. This is mainly intended for use in determining which datejs
	 * include file to use.
	 */
	public String getLocaleName() {
		Locale locale = getRequest().getLocale();
		
		String language = locale.getLanguage();
		String country = locale.getCountry();
		
		if (language == null || language.length() == 0) return "";
		if (country == null || country.length() == 0) return "";
		
		return String.format("-%s-%s", language, country);
	}
}

/*
 * HistoryIterable.java
 * 
 * Provides iterators on demand to get HistoryEntries from the database, given an
 * Iterable over history ID numbers.
 * 
 * Jonathan Pearson
 * May 12, 2009
 * 
 */

package com.sixnetio.BVB.Web.Iterators;

import java.util.*;

import com.sixnetio.BVB.Common.HistoryEntry;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.util.Utils;

/**
 * This exists to provide something for the Struts2 iterator tag to iterate
 * over.
 * 
 * @author Jonathan Pearson
 */
public class HistoryIterable implements Iterable<HistoryEntry> {
	private Iterable<HistoryEntry> historyEntries;
	
	/**
	 * Construct a new HistoryIterable.
	 * 
	 * @param historyIDs The collection of History IDs to allow iteration over.
	 */
	public HistoryIterable(Collection<Long> historyIDs)
		throws Exception {
		
		Map<Long, HistoryEntry> historyMap = DB.getDB()
			.getHistoryEntries(historyIDs);
		
		List<HistoryEntry> historyEntries = new LinkedList<HistoryEntry>();
		for (long id : historyIDs) {
			historyEntries.add(historyMap.get(id));
		}
		
		this.historyEntries = historyEntries;
	}
	
	/**
	 * Get a read-only iterator over the history entries represented by the
	 * history IDs that this class was constructed with.
	 */
	@Override
	public Iterator<HistoryEntry> iterator() {
		return Utils.readOnlyIterator(historyEntries.iterator());
	}
}

/*
 * LogIterable.java
 * 
 * Provides iterators on demand to get LogEntries from the database, given an
 * Iterable over log ID numbers.
 * 
 * Jonathan Pearson
 * May 13, 2009
 * 
 */

package com.sixnetio.BVB.Web.Iterators;

import java.util.*;

import com.sixnetio.BVB.Common.LogEntry;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.util.Utils;

/**
 * This exists to provide something for the Struts2 iterator tag to iterate
 * over.
 * 
 * @author Jonathan Pearson
 */
public class LogIterable implements Iterable<LogEntry> {
	private Iterable<LogEntry> logEntries;
	
	/**
	 * Construct a new LogIterable.
	 * 
	 * @param logIDs The collection of Log IDs to allow iteration over.
	 */
	public LogIterable(Collection<Long> logIDs) throws Exception {
		Map<Long, LogEntry> logMap = DB.getDB().getLogEntries(logIDs);
		
		List<LogEntry> logEntries = new LinkedList<LogEntry>();
		for (long id : logIDs) {
			logEntries.add(logMap.get(id));
		}
		
		this.logEntries = logEntries;
	}
	
	/**
	 * Get a read-only iterator over the log entries represented by the log IDs
	 * that this class was constructed with.
	 */
	@Override
	public Iterator<LogEntry> iterator() {
		return Utils.readOnlyIterator(logEntries.iterator());
	}
}

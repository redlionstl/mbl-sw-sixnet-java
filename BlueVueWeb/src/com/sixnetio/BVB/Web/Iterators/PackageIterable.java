/*
 * PackageIterable.java
 * 
 * Provides iterators on demand to get BlueTreeImages from the database, given an
 * Iterable over package hashes.
 * 
 * Jonathan Pearson
 * May 28, 2009
 * 
 */

package com.sixnetio.BVB.Web.Iterators;

import java.math.BigInteger;
import java.util.*;

import com.sixnetio.BVB.Common.BlueTreeImage;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.util.Utils;

/**
 * This exists to provide something for the Struts2 iterator tag to iterate
 * over.
 * 
 * @author Jonathan Pearson
 */
public class PackageIterable implements Iterable<BlueTreeImage> {
	private Iterable<BlueTreeImage> packages;
	
	/**
	 * Construct a new PackageIterable.
	 * 
	 * @param packageIDs The collection of Package IDs to allow iteration over.
	 * This will only iterate over the metadata of the packages, not their
	 * content.
	 */
	public PackageIterable(Collection<BigInteger> packageIDs) throws Exception {
		Collection<String> packageHashes = new LinkedList<String>();
		for (BigInteger id : packageIDs) {
			packageHashes.add(id.toString(16));
		}
		
		Map<String, BlueTreeImage> packageMap = DB.getDB()
			.getPackages(packageHashes, true);
		
		List<BlueTreeImage> packages = new LinkedList<BlueTreeImage>();
		for (String id : packageHashes) {
			packages.add(packageMap.get(id));
		}
		
		this.packages = packages;
	}
	
	/**
	 * Get a read-only iterator over the packages represented by the package
	 * hashes that this class was constructed with.
	 */
	@Override
	public Iterator<BlueTreeImage> iterator() {
		return Utils.readOnlyIterator(packages.iterator());
	}
}

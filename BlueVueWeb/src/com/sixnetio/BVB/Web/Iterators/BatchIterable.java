/*
 * BatchIterable.java
 * 
 * Provides iterators on demand to get JobBatches from the database, given an
 * Iterable over batch ID numbers.
 * 
 * Jonathan Pearson
 * May 12, 2009
 * 
 */

package com.sixnetio.BVB.Web.Iterators;

import java.util.*;

import com.sixnetio.BVB.Common.JobBatch;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.util.Utils;

/**
 * This exists to provide something for the Struts2 iterator tag to iterate
 * over.
 * 
 * @author Jonathan Pearson
 */
public class BatchIterable implements Iterable<JobBatch> {
	private Iterable<JobBatch> batches;
	
	/**
	 * Construct a new BatchIterable.
	 * 
	 * @param batchIDs The collection of Batch IDs to allow iteration over.
	 */
	public BatchIterable(Collection<Integer> batchIDs)
		throws Exception {
		
		Map<Integer, JobBatch> batchMap = DB.getDB().getBatches(batchIDs);
		
		List<JobBatch> batches = new LinkedList<JobBatch>();
		for (int id : batchIDs) {
			batches.add(batchMap.get(id));
		}
		
		this.batches = batches;
	}
	
	/**
	 * Get a read-only iterator over the batches represented by the batch IDs
	 * that this class was constructed with.
	 */
	@Override
	public Iterator<JobBatch> iterator() {
		return Utils.readOnlyIterator(batches.iterator());
	}
}

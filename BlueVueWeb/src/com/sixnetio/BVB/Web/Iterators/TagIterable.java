/*
 * TagIterable.java
 * 
 * Provides iterators on demand to get ModemTags from the database, given an
 * Iterable over tag ID numbers.
 * 
 * Jonathan Pearson
 * April 28, 2009
 * 
 */

package com.sixnetio.BVB.Web.Iterators;

import java.util.*;

import com.sixnetio.BVB.Common.ModemTag;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.util.Utils;

/**
 * This exists to provide something for the Struts2 iterator tag to iterate
 * over.
 * 
 * @author Jonathan Pearson
 */
public class TagIterable implements Iterable<ModemTag> {
	private Iterable<ModemTag> tags;
	
	/**
	 * Construct a new TagIterable.
	 * 
	 * @param tagIDs The collection of Tag IDs to allow iteration over.
	 */
	public TagIterable(Collection<Integer> tagIDs) throws Exception {
		Map<Integer, ModemTag> tagMap = DB.getDB().getTags(tagIDs);
		
		List<ModemTag> tags = new LinkedList<ModemTag>();
		for (int id : tagIDs) {
			tags.add(tagMap.get(id));
		}
		
		this.tags = tags;
	}
	
	/**
	 * Get a read-only iterator over the tags represented by the tag IDs that
	 * this class was constructed with.
	 */
	@Override
	public Iterator<ModemTag> iterator() {
		return Utils.readOnlyIterator(tags.iterator());
	}
}

/*
 * ArrayIterator.java
 *
 * A read-only iterator that runs over an array.
 *
 * Jonathan Pearson
 * May 7, 2009
 *
 */

package com.sixnetio.BVB.Web.Iterators;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayIterator<E> implements Iterator<E> {
	private E[] array;
	private int index = -1;
	
	public ArrayIterator(E[] array) {
		this.array = array;
	}
	
	@Override
	public boolean hasNext() {
		return (index < array.length - 1);
	}
	
	@Override
	public E next() {
		if (!hasNext()) throw new NoSuchElementException("No more elements left");
		
		return array[++index];
	}
	
	@Override
	public void remove() {
		throw new UnsupportedOperationException("Remove operation not supported by ArrayIterator");
	}
	
}

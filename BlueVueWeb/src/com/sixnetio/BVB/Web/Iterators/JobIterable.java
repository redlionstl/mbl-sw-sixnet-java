/*
 * JobIterable.java
 * 
 * Provides iterators on demand to get Jobs from the database, given an
 * Iterable over job ID numbers.
 * 
 * Jonathan Pearson
 * April 28, 2009
 * 
 */

package com.sixnetio.BVB.Web.Iterators;

import java.util.*;

import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.util.Utils;

/**
 * This exists to provide something for the Struts2 iterator job to iterate
 * over.
 * 
 * @author Jonathan Pearson
 */
public class JobIterable implements Iterable<Job> {
	private Iterable<Job> jobs;
	
	/**
	 * Construct a new JobIterable.
	 * 
	 * @param jobIDs The collection of Job IDs to allow iteration over. This
	 *   will return generic jobs.
	 */
	public JobIterable(Collection<Long> jobIDs) throws Exception {
		Map<Long, Job> jobMap = DB.getDB().getJobs(jobIDs, true);
		
		List<Job> jobs = new LinkedList<Job>();
		for (long id : jobIDs) {
			jobs.add(jobMap.get(id));
		}
		
		this.jobs = jobs;
	}
	
	/**
	 * Get a read-only iterator over the jobs represented by the job IDs that
	 * this class was constructed with.
	 */
	@Override
	public Iterator<Job> iterator() {
		return Utils.readOnlyIterator(jobs.iterator());
	}
}

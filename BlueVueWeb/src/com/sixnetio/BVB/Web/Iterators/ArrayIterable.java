/*
 * ArrayIterable.java
 *
 * Generates ArrayIterators that will run over the given array.
 *
 * Jonathan Pearson
 * May 7, 2009
 *
 */

package com.sixnetio.BVB.Web.Iterators;

import java.util.Iterator;

public class ArrayIterable<T> implements Iterable<T> {
	private T[] array;
	
	public ArrayIterable(T[] array) {
		this.array = array;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new ArrayIterator<T>(array);
	}
}

/*
 * UserIterable.java
 * 
 * Provides iterators on demand to get Users from the database, given an
 * Iterable over user ID numbers.
 * 
 * Jonathan Pearson
 * June 1, 2009
 * 
 */

package com.sixnetio.BVB.Web.Iterators;

import java.util.*;

import com.sixnetio.BVB.Common.User;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.util.Utils;

/**
 * This exists to provide something for the Struts2 iterator tag to iterate
 * over.
 * 
 * @author Jonathan Pearson
 */
public class UserIterable implements Iterable<User> {
	private Iterable<User> users;
	
	/**
	 * Construct a new UserIterable.
	 * 
	 * @param userIDs The collection of User IDs to allow iteration over.
	 */
	public UserIterable(Collection<Integer> userIDs) throws Exception {
		Map<Integer, User> userMap = DB.getDB().getUsers(userIDs);
		
		List<User> users = new LinkedList<User>();
		for (int id : userIDs) {
			users.add(userMap.get(id));
		}
		
		this.users = users;
	}
	
	/**
	 * Get a read-only iterator over the users represented by the user IDs that
	 * this class was constructed with.
	 */
	@Override
	public Iterator<User> iterator() {
		return Utils.readOnlyIterator(users.iterator());
	}
}

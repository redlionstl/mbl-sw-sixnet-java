/*
 * ModemIterable.java
 * 
 * Provides iterators on demand to get Modems from the database, given an
 * Iterable over modem ID numbers.
 * 
 * Jonathan Pearson
 * April 28, 2009
 * 
 */

package com.sixnetio.BVB.Web.Iterators;

import java.util.*;

import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.util.Utils;

/**
 * This exists to provide something for the Struts2 iterator tag to iterate
 * over.
 * 
 * @author Jonathan Pearson
 */
public class ModemIterable implements Iterable<Modem> {
	private Iterable<Modem> modems;
	
	/**
	 * Construct a new ModemIterable.
	 * 
	 * @param modemIDs The collection of Modem IDs to allow iteration over.
	 * @throws Exception If there is a problem retrieving the modems from the
	 *   database.
	 */
	public ModemIterable(Collection<Integer> modemIDs)
		throws Exception {
		
		Map<Integer, Modem> modemMap = DB.getDB().getModems(modemIDs);
		
		List<Modem> modems = new LinkedList<Modem>();
		for (int id : modemIDs) {
			modems.add(modemMap.get(id));
		}
		
		this.modems = modems;
	}
	
	/**
	 * Get a read-only iterator over the modems represented by the modem IDs
	 * that this class was constructed with.
	 */
	@Override
	public Iterator<Modem> iterator() {
		return Utils.readOnlyIterator(modems.iterator());
	}
}

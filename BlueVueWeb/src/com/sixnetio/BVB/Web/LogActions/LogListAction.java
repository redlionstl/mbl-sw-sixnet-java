/*
 * LogListAction.java
 *
 * Provides sortable, filterable, paginated lists of log entries.
 *
 * Jonathan Pearson
 * May 13, 2009
 *
 */

package com.sixnetio.BVB.Web.LogActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.LogEntry;
import com.sixnetio.BVB.Common.User;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Database.SortColumns.LogColumn;
import com.sixnetio.BVB.Web.*;
import com.sixnetio.BVB.Web.Iterators.LogIterable;
import com.sixnetio.util.Utils;

public class LogListAction
	extends IDPreparer<Long>
	implements Filterable, LinkTranslator, Sortable, LogStatusProvider {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// Criteria for searching out log entries
	public Long qLogID = null;
	public Date qLowDate = null;
	public Date qHighDate = null;
	public Integer qUserID = null;
	public String qLogType = null;
	
	private long matchCount = 0;
	
	// This is only set once, so no need for synchronization
	private List<LogColumn> sortOn = null;
	
	@Override
	public Map<String, String[]> getFilters() throws Exception {
		// To start, fill this with Title --> Filter, but only individual
		// filters
		LinkedHashMap<String, String> filters = new LinkedHashMap<String, String>();
		
		if (qLogID != null) {
			filters.put(getText("filter.logID", Utils.makeLinkedList((Object)qLogID)), "qLogID=" +
			                                                                           qLogID);
		}
		
		if (qLowDate != null) {
			String formatted = formatData("format.datetime", qLowDate);
			filters.put(getText("filter.lowDate", Utils.makeLinkedList((Object)formatted)),
			            "qLowDate=" + formatted);
		}
		
		if (qHighDate != null) {
			String formatted = formatData("format.datetime", qHighDate);
			filters.put(getText("filter.highDate", Utils.makeLinkedList((Object)formatted)),
			            "qHighDate=" + formatted);
		}
		
		if (qUserID != null) {
			User user = DB.getDB().getUser(qUserID);
			
			if (user != null) {
				filters.put(getText("filter.user", Utils.makeLinkedList((Object)user.name)),
				            "qUserID=" + qUserID);
			} else {
				filters.put(getText("filter.userID", Utils.makeLinkedList((Object)qUserID)),
				            "qUserID=" + qUserID);
			}
		}
		
		if (qLogType != null) {
			filters.put(getText("filter.type", Utils.makeLinkedList((Object)qLogType)),
			            "qLogType=" + qLogType);
		}
		
		return constructFilters(filters);
	}
	
	@Override
	public String getSortOn() {
		return LogActionLib.translateSort(sortOn);
	}
	
	@Override
	public void setSortOn(String columns) {
		sortOn = LogActionLib.translateSort(columns);
	}
	
	@Override
	public String addSortColumn(String colName) {
		return LogActionLib.addSortColumn(colName, sortOn);
	}
	
	/**
	 * The user specified the List action. Loads a list of all history, based on
	 * the properties that the user provided.
	 * 
	 * @return{@link GeneralAction#LIST}.
	 * @throws Exception If there is a database access error.
	 */
	public String list() throws Exception {
		long[] matchCount = new long[1];
		matchingIDs = new LinkedList<Long>(DB.getDB()
				.getMatchingLogs(qLogID, qLowDate, qHighDate, qUserID, qLogType,
				                 getPageStart(), getPageSize(),
				                 sortOn, matchCount));
		
		this.matchCount = matchCount[0];
		
		return LIST;
	}
	
	@Override
	public long getMatchCount() {
		return matchCount;
	}
	
	@Override
	public String getStatus(Object obj) {
		return LogActionLib.getLogStatus((LogEntry)obj);
	}
	
	@Override
	public User getLogUser(LogEntry entry) throws Exception {
		return LogActionLib.getLogUser(entry);
	}
	
	@Override
	public String translateLink(String link, Object obj) throws Exception {
		if (obj == null || !(obj instanceof LogEntry)) return "";
		
		LogEntry entry = (LogEntry)obj;
		String docroot = getDocumentRoot();
		
		if (link.equals("viewLog")) {
			return String.format("%s/Log/view?id=%d", docroot, entry.logID);
			
		} else if (link.equals("viewUser")) {
			if (entry.userID == null) return "";
			return String.format("%s/User/view?id=%d", docroot, entry.userID);
			
		} else if (link.startsWith("filterLogs/")) {
			String filterName = link.substring(link.indexOf('/') + 1);
			
			Long qLogID = this.qLogID;
			Date qLowDate = this.qLowDate;
			Date qHighDate = this.qHighDate;
			Integer qUserID = this.qUserID;
			String qLogType = this.qLogType;
			
			if (filterName.equals("logID")) {
				qLogID = entry.logID;
				
			} else if (filterName.startsWith("date/")) {
				String range = filterName.substring(filterName.indexOf('/') + 1);
				int minuteRange = 10; // Default to 10 minutes
				
				try {
					minuteRange = Integer.parseInt(range);
				} catch (NumberFormatException nfe) {
					logger.warn("Not a number: " + range, nfe);
				}
				
				// Select within minuteRange minutes
				Calendar cal = Calendar.getInstance();
				
				cal.setTime(entry.timestamp);
				cal.add(Calendar.MINUTE, -minuteRange);
				qLowDate = cal.getTime();
				
				cal.setTime(entry.timestamp);
				cal.add(Calendar.MINUTE, minuteRange);
				qHighDate = cal.getTime();
				
			} else if (filterName.equals("userID")) {
				if (entry.userID == null) {
					qUserID = User.INV_USER;
				} else {
					qUserID = entry.userID;
				}
				
			} else if (filterName.equals("type")) {
				qLogType = entry.type;
			}
			
			StringBuilder builder = new StringBuilder(docroot + "/Logs?");
			
			// A trailing '&' won't hurt anything, so just generate a link with
			// it for simplicity
			
			if (qLogID != null) {
				builder.append(String.format("qLogID=%d&", qLogID));
			}
			
			if (qLowDate != null) {
				builder.append(String.format("qLowDate=%s&",
				                             formatData("format.datetime",
				                                        qLowDate)));
			}
			
			if (qHighDate != null) {
				builder.append(String.format("qHighDate=%s&",
				                             formatData("format.datetime",
				                                        qHighDate)));
			}
			
			if (qUserID != null) {
				builder.append(String.format("qUserID=%d&", qUserID));
			}
			
			if (qLogType != null) {
				builder.append(String.format("qLogType=%s&", qLogType));
			}
			
			return builder.toString();
		}
		
		return "";
	}
	
	@Override
    protected Collection<String> getIDParameterNames() {
	    // Nothing may be passed in/around
		return new ArrayList<String>();
    }
	
	@Override
    public Iterable<?> getItems() throws Exception {
	    return new LogIterable(getPageIDs());
    }
	
	@Override
    protected Collection<Long> parseID(String type, String id)
    	throws NumberFormatException, DatabaseException, Exception {
	    
		throw new IllegalArgumentException("Unrecognized type: " + type);
    }
}

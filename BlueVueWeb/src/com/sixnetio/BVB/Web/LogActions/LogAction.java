/*
 * LogAction.java
 *
 * Retrieves log results.
 *
 * Jonathan Pearson
 * June 4, 2009
 *
 */

package com.sixnetio.BVB.Web.LogActions;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;
import com.sixnetio.BVB.Common.LogEntry;
import com.sixnetio.BVB.Common.User;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.BVB.Web.GeneralAction;
import com.sixnetio.util.Utils;

public class LogAction extends GeneralAction implements LogStatusProvider, Preparable {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private long id = LogEntry.INV_LOG;
	
	private LogEntry entry = null;
	
	@Override
	public void prepare() throws Exception {
		String sID = getRequest().getParameter("id");
		
		if (sID != null && !sID.equals("-1")) {
			try {
				id = Long.parseLong(sID);
				
				logger.debug("User specified log entry with ID " + id);
				
				entry = DB.getDB().getLogEntry(id);
			} catch (NumberFormatException nfe) {
				throw new Exception(getText("error.invalidLogID", new String[] {
					sID
				}), nfe);
			}
		} else {
			logger.debug("No log entry specified, cannot create a new one");
			
			addActionError(getText("error.noLog"));
		}
	}
	
	public LogEntry getEntry() {
		return entry;
	}
	
	public String view() throws Exception {
		verifyUserLoggedIn();
		
		return VIEW;
	}
	
	@Override
	public String getStatus(Object obj) {
		return LogActionLib.getLogStatus((LogEntry)obj);
	}
	
	@Override
	public User getLogUser(LogEntry entry) throws Exception {
		return LogActionLib.getLogUser(entry);
	}
}

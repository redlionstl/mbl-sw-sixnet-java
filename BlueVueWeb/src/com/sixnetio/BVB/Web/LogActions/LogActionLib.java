/*
 * LogActionLib.java
 *
 * Provides some library functions common to Log actions.
 *
 * Jonathan Pearson
 * May 13, 2009
 *
 */

package com.sixnetio.BVB.Web.LogActions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.SortColumns.LogColumn;
import com.sixnetio.BVB.Web.DB;
import com.sixnetio.util.Utils;

public class LogActionLib {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Examines the given log entry, returning a string describing the status of
	 * the entry.
	 * 
	 * @param entry The entry to check.
	 * @return {@link LogStatusProvider#S_NORMAL} if the entry represents a
	 *         success, {@link LogStatusProvider#S_FAILURE} if the entry
	 *         represents a failure, or {@link LogStatusProvider#S_WARNING} if
	 *         the entry represents a warning.
	 */
	public static String getLogStatus(LogEntry entry) {
		if (entry.type.equals(DBLogger.INFO)) {
			return LogStatusProvider.S_NORMAL;
		} else if (entry.type.equals(DBLogger.WARNING)) {
			return LogStatusProvider.S_WARNING;
		} else if (entry.type.equals(DBLogger.ERROR)) {
			return LogStatusProvider.S_FAILURE;
		} else {
			logger.debug("Unrecognized type of log: " + entry.type);
			
			return LogStatusProvider.S_NORMAL;
		}
	}
	
	/**
	 * Get the user associated with the given log entry.
	 * 
	 * @param entry The entry.
	 * @return The user associated with the entry, or <tt>null</tt> if there is
	 *         none.
	 * @throws Exception If a database error occurs.
	 */
	public static User getLogUser(LogEntry entry) throws Exception {
		if (entry.userID == null) return null;
		
		User user = DB.getDB().getUser(entry.userID);
		
		return user;
	}
	
	/**
	 * Translate a string describing sorting columns into a list of LogColumns.
	 * 
	 * @param columns The description of the sorting columns.
	 * @return A list that could be passed to
	 *         {@link Database#getMatchingLogs(Long, Date, Date, Integer, String, int, int, List, long[])}
	 *         .
	 */
	public static List<LogColumn> translateSort(String columns) {
		String[] cols = Utils.tokenize(columns, ",", false);
		List<LogColumn> sortOn = new Vector<LogColumn>(cols.length);
		
		for (String column : cols) {
			if (column.startsWith("!")) {
				sortOn.add(LogColumn.Reverse);
				column = column.substring(1);
			}
			
			sortOn.add(LogColumn.valueOf(column));
		}
		
		return sortOn;
	}
	
	/**
	 * Given an existing sort list, add a new primary column to it, shuffling
	 * the existing columns as necessary.
	 * 
	 * @param colName The new column to add.
	 * @param oldSortOn The existing sort list, or <tt>null</tt> if there wasn't
	 *            one. If synchronization is necessary, it must be handled by
	 *            the caller.
	 * @return A new sort list. If the new column was already the primary
	 *         column, reverses the sorting order. If it was already in the
	 *         list, moves it to the beginning in normal order. If it did not
	 *         exist, adds it at the beginning in normal order.
	 */
	public static String addSortColumn(String colName, List<LogColumn> oldSortOn) {
		LogColumn col;
		
		try {
			col = LogColumn.valueOf(colName);
		} catch (IllegalArgumentException iae) {
			logger.debug("Not a legal column name: '" + colName + "'", iae);
			
			return translateSort(oldSortOn);
		}
		
		// Copy the sortOn object, since we don't actually want to change it
		List<LogColumn> sortOn;
		
		if (oldSortOn == null) {
			// It's only going to receive one item
			sortOn = new ArrayList<LogColumn>(1);
		} else {
			sortOn = new ArrayList<LogColumn>(oldSortOn);
		}
		
		// Get the index of the column, to see if it's already part of the sort
		// order
		int index = sortOn.indexOf(col);
		
		// Whether to apply a 'Reverse' just before the column
		boolean reverse = false;
		
		// If the column was already in there...
		if (index != -1) {
			// Remove it
			sortOn.remove(index);
			
			// If there was a reverse just before it...
			if (index > 0 && sortOn.get(index - 1) == LogColumn.Reverse) {
				sortOn.remove(index - 1);
				
				reverse = true;
				index--; // So we can more easily tell if it was the first
				// column
			}
			
			// If it was the first sort column
			if (index == 0) {
				// Swap the reverse property
				reverse = !reverse;
			}
		}
		
		// Push the column onto the front of the sort order
		sortOn.add(0, col);
		
		// If reversing, push reverse onto the front also
		if (reverse) {
			sortOn.add(0, LogColumn.Reverse);
		}
		
		// Return the new sorting order
		return translateSort(sortOn);
	}
	
	/**
	 * Translate a sort list into a string.
	 * 
	 * @param sortOn The sort list to translate. If synchronization is
	 *            necessary, it must be handled by the caller.
	 * @return A descriptive string.
	 */
	public static String translateSort(List<LogColumn> sortOn) {
		if (sortOn == null) return "";
		
		StringBuilder builder = new StringBuilder();
		
		boolean skipComma = true;
		for (LogColumn column : sortOn) {
			if (skipComma) {
				skipComma = false;
			} else {
				builder.append(",");
			}
			
			if (column == LogColumn.Reverse) {
				builder.append("!");
				skipComma = true;
			} else {
				builder.append(column.name());
			}
		}
		
		return builder.toString();
	}
}

/*
 * LogStatusProvider.java
 *
 * Provides information about log entries.
 *
 * Jonathan Pearson
 * May 13, 2009
 *
 */

package com.sixnetio.BVB.Web.LogActions;

import com.sixnetio.BVB.Common.LogEntry;
import com.sixnetio.BVB.Common.User;
import com.sixnetio.BVB.Web.StatusProvider;

public interface LogStatusProvider extends StatusProvider {
	/**
	 * Get a string describing the status of the given log entry.
	 * 
	 * @param obj The entry to check.
	 * @return One of the S_* values from {@link StatusProvider}.
	 * @throws Exception If there is a problem deciding on the status.
	 */
	@Override
	public String getStatus(Object obj) throws Exception;
	
	/**
	 * Get the user associated with the given log entry.
	 * 
	 * @param entry The entry to check.
	 * @return The user associated with the entry, or <tt>null</tt> if there is
	 *         none.
	 * @throws Exception If there is a problem getting the user.
	 */
	public User getLogUser(LogEntry entry) throws Exception;
}

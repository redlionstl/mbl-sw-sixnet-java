/*
 * LoginAction.java
 *
 * Perform system login.
 *
 * Jonathan Pearson
 * April 15, 2009
 *
 */

package com.sixnetio.BVB.Web;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.User;
import com.sixnetio.util.Utils;

public class LoginAction extends GeneralAction {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private String userName;
	private String password;
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String execute() throws Exception {
		// Make sure that there is a database connection
		DB.getDB();
		
		// If already logged in, skip the dialog
		if (getUser() != null) return SUCCESS;
		
		if (userName == null || password == null) {
			return INPUT;
		} else {
			// Try to log in
			Collection<Integer> users =
			        DB.getDB().getMatchingUsers(null, userName, null, 0, -1, null, null);
			if (users.size() == 0) {
				// Failed, wait a few seconds to slow down password attacks
				Utils.sleep(3000);
				
				addActionError(getText("error.credentials"));
				return INPUT;
			} else if (users.size() > 1) {
				addActionError(getText("error.uniqueUsers"));
				return INPUT;
			}
			
			// There is exactly one user in the list, get it
			User user = DB.getDB().getUser(users.iterator().next());
			
			// Check the password
			if (!user.hashedPassword.trim().equalsIgnoreCase(User.hashPassword(userName, password))) {
				// Failed, wait a few seconds to slow down password attacks
				Utils.sleep(3000);
				
				logger.info(String.format("Bad login credentials: found '%s', expected '%s'",
				                          User.hashPassword(userName, password),
				                          user.hashedPassword));
				
				addActionError(getText("error.credentials"));
				return INPUT;
			}
			
			setUser(user);
			
			logger.debug("Locale is " + getRequest().getLocale().toString());
			
			return SUCCESS;
		}
	}
}

/*
 * NotLoggedInException.java
 *
 * Thrown when the user is not logged in.
 *
 * Jonathan Pearson
 * April 29, 2009
 *
 */

package com.sixnetio.BVB.Web;

public class NotLoggedInException extends Exception {
	public NotLoggedInException(String message) {
		super(message);
	}
	
	public NotLoggedInException(String message, Throwable cause) {
		super(message, cause);
	}
}

#!/usr/bin/perl -w

use strict;
use diagnostics;

use Time::HiRes qw(sleep);
use Test::WWW::Selenium;
use Test::More;
use Test::Exception;
use File::Temp qw(tempfile);
use Getopt::Long;
use WWW::Mechanize;

use Expect;

use POSIX qw(strftime);

use constant PORT_BASE => 5070; # default simulator start port

# Init our selenium test object with remote machine.

my ($HOST,$PORT,$BROWSER,$URL,$USER,$PASS,$SIMULATOR,$BASEPORT,$LOCAL_PATH,
    $REMOTE_PATH,$RUNLONG,$IP,$DO,@REAL_MODEMS);

GetOptions("host=s"    => \$HOST,
           "port=i"    => \$PORT,
           "browser=s" => \$BROWSER,
           "url=s"     => \$URL,
           "user=s"    => \$USER,
           "pass=s"    => \$PASS,
           "simulator=s" => \$SIMULATOR,
           "baseport=s"=> \$BASEPORT,
           "runlong"   => \$RUNLONG,
           "ip=s"      => \$IP,
           "do=s"      => \$DO,
           "modem=s" => \@REAL_MODEMS,
          );

if(!defined($PORT)){
    $PORT=4444; # Standard Selenium-RC server port
}

if(!defined($BASEPORT)){
    $BASEPORT = PORT_BASE;
}

BAIL_OUT("Neets a host to connect to (use -host)") if !defined($PORT);
BAIL_OUT("Neets a browser to use (use -browser)") if !defined($BROWSER);
BAIL_OUT("Neets a url to use (use -url)") if !defined($URL);
BAIL_OUT("Neets a username (use -user)") if !defined($USER);
BAIL_OUT("Neets a password (use -pass)") if !defined($PASS);
BAIL_OUT("Neets a simulator ip (use -simulator)") if !defined($SIMULATOR);
BAIL_OUT("Neets a single modem ip (use -ip)") if !defined($IP);
# FIXME - waiting on DO support
# BAIL_OUT("Neets a do channel address (use -do)") if !defined($DO);


my $msTIMEOUT = "30000"; # global timeout for selenium things (in ms)
my $sTIMEOUT = $msTIMEOUT/1000; # global timeout for selenium things (in s)

my $sLONG_TIMEOUT = "600"; # global timeout for long things (page
                          # refreshes, etc) (in s)

my $JOB_DATE_FORMAT = "%B %d, %Y";
my $JOB_TIME_FORMAT = "%I:%M %p";

sub main();
sub login($);
sub add_ip_address_to_modems_list($);
sub test_2_1_1($$$);
sub test_2_1_2($$$);
sub test_2_1_3($$$);
sub test_2_1_4($$$);
sub test_2_1_5($$$$$);
sub test_2_1_6($$);
sub test_2_2_1($$);
sub test_2_2_2($$);

sub script_test_helper($$$$$$);
sub group_test($$$;$$$$);
sub add_jobs($$;$$$$);
sub verify_jobs($$$;$$$);
sub create_tag($$);
sub create_modem($$$;$);
sub create_modem_group($$$;$@);
sub batch_create_modems($$$$;$@);
sub verify_table($$$;$@);
sub verify_jobs_table($;$@);
sub verify_history_table($$%);
sub count_elements($$$);
sub batch_create_modem_csv($$);
sub wait_for_table_value($$$$);
sub wait_for_table_ok($$$$);
sub wait_for_table($$$;$$);
sub wait_for_table_regexp($$$$);
sub set_tag_polling_interval($$$);
sub select_tagged_modems($$);
sub select_modem_by_ip($$);
sub tag_all_modems($$);
sub tag_modems($$);
sub delete_modems($;$);
sub delete_tags($;$);
sub delete_jobs($;$);
sub delete_batches($;$);
sub wait_for_jobs($$%);


main();

##
# Main - Setup, runs tests, does cleanup.
#
sub main()
{
    # these are bogus numbers. Need to actually calculate them once
    # all tests are written
    if($RUNLONG){
        plan("tests" => 200);
    }
    else{
        plan("tests" => 85);
    }


    my $sel = Test::WWW::Selenium->new( host => $HOST, 
                                        port => $PORT, 
                                        browser => "*${BROWSER}", 
                                        browser_url => $URL);

    # fudge to let us set the timeout. This is a bad request, but it
    # works.
    $sel->open("/");
    # default timeout
    $sel->set_timeout($msTIMEOUT);

    # Login, if we need to
    login($sel);

    # add IP address to our UI, if we need to
    add_ip_address_to_modems_list($sel);

    # reset to initial state
    cleanout($sel);

    # most of these tests depend on the "Small Group" and "Large
    # Group" being there - create them
    
    my $small_group_modems = 5;
    my $small_group_tag = "Small Group";
    my $large_group_modems = 300;
    my $large_group_tag = "Large Group";
    my $single_modem_tag = "Single Modem";

    create_modem_group($sel, $small_group_modems, $small_group_tag, $BASEPORT,
                       @REAL_MODEMS);

    batch_create_modems($sel,$SIMULATOR,$large_group_modems,
                        $large_group_tag, 
                        $BASEPORT + $small_group_modems - scalar(@REAL_MODEMS));
   
    # creating a tag for a Single Modem to make life easier
    create_tag($sel,$single_modem_tag);
    
    # tag the single modem
    select_modem_by_ip($sel,$IP) || 
      die("Unable to find modem with IP ${IP}. Aborting all tests.");
    tag_modems($sel,$single_modem_tag);
    
    test_2_1_1($sel,$small_group_modems,$small_group_tag);

    test_2_1_2($sel,$large_group_modems,$large_group_tag);

    test_2_1_3($sel,$small_group_modems,$small_group_tag);
    
    test_2_1_4($sel,$small_group_modems,$small_group_tag);

# FIXME - waiting on DO support
#    test_2_1_5($sel,$small_group_modems,$small_group_tag,
#               $single_modem_tag,$DO);

    test_2_1_6($sel,$single_modem_tag);

# FIXME - waiting on DHCP server
#    test_2_1_7($sel,$single_modem_tag,$server_ip);

    test_2_2_1($sel,$single_modem_tag);

    test_2_2_2($sel,$single_modem_tag);
}

## 
# login to web service, if necessary
#
# @param[in] $sel - Selenium object
#
sub login($)
{
    my ($sel) = @_;
    &diag("Checking to see if we should log in");
    $sel->open("/BVWeb/Login");
    if($sel->get_title() =~ /^LOGIN/){
        &diag("Logging in");
        $sel->type("Login_userName", $USER);
        $sel->type("Login_password", $PASS);
        $sel->click("Login_0");
        $sel->wait_for_page_to_load($msTIMEOUT);
    }
    else{
        &diag("No need to log in, skipping");
    }
}

##
# Add IP address to modems list, if necessary.
#
# This is important for navigation, since it is often difficult to
# keep track of which modem is which. IP Address helps with this.
#
# @param[in] $sel - Selenium object
#
sub add_ip_address_to_modems_list($)
{
    my ($sel) = @_;
    
    &diag("Checking to see if we need to add IP Address column added.");

    $sel->click("link=PREFERENCES");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->click("link=Navigate");
    $sel->click("link=Modems");
    $sel->wait_for_page_to_load($msTIMEOUT);
    
    my @selected = $sel->get_selected_label("id=update_chosenColumns");
    my $stop = scalar(@selected);
    my $foundit = 0;
    for(my $index = 0; $index < $stop && !$foundit; ++$index){
        &diag("Checking " . $selected[$index]);
        if($selected[$index] eq "IP Address"){
            $foundit = 1;
            &diag("Found existing IP Address column. No need to change");
        }
    }

    if(!$foundit){
        &diag("No IP Address column, adding.");

        $sel->add_selection("update_", "label=IP Address");
        $sel->click("//input[\@value='->']");
        $sel->select("update_chosenColumns", "label=IP Address");
        $sel->click("//input[\@name='link_modems_ipAddress' and \@value='viewModem']");
        $sel->click("link=Save");
        $sel->wait_for_page_to_load($msTIMEOUT);
    }

}

## 
# clean out all existing modems and jobs
#
# @param[in] $sel - Selenium object
#
sub cleanout($)
{
    my ($sel) = @_;

    &diag("Resetting to initial configuration");

    delete_jobs($sel);
    delete_modems($sel);
    delete_tags($sel);
    delete_batches($sel);
}

##
# delete modems
# 
# @param[in] $sel - Selenium object
# @param[in] $tag - tag (optional). Default deletes all modems
#
sub delete_modems($;$){
    my ($sel,$tag) = @_;

    if(defined($tag)){
        &diag("Deleting modems tagged ${tag}");
        select_tagged_modems($sel,$tag);
    }
    else{
        &diag("Deleting all modems");
        $sel->click("link=MODEMS");
        $sel->wait_for_page_to_load($msTIMEOUT);
    }

    $sel->click("link=Select All");
    $sel->wait_for_page_to_load($msTIMEOUT);

    $sel->click("link=Delete");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->click("link=Delete");
    $sel->wait_for_page_to_load($msTIMEOUT);
}

##
# delete tag
# 
# @param[in] $sel - Selenium object
# @param[in] $tag - tag to delete (optional, default deletes all)
#
sub delete_tags($;$){
    my ($sel,$tag) = @_;

    $sel->click("link=TAGS");
    $sel->wait_for_page_to_load($msTIMEOUT);
    if(defined($tag)){
        &diag("Deleting tag ${tag}");
        $sel->click("link=${tag}");
        $sel->wait_for_page_to_load($msTIMEOUT);
    }
    else{
        &diag("Deleting all tags");
        $sel->click("link=Select All");
        $sel->wait_for_page_to_load($msTIMEOUT);
    }
    $sel->click("link=Delete");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->click("link=Delete");
    $sel->wait_for_page_to_load($msTIMEOUT);
}
##
# delete jobs
# 
# @param[in] $sel - Selenium object
# @param[in] $batch - delete jobs belonging to this batch (default all)
#
sub delete_jobs($;$){
    my ($sel,$batch) = @_;

    $sel->click("link=JOBS");
    $sel->wait_for_page_to_load($msTIMEOUT);

    if(defined($batch)){
        &diag("Deleting jobs from batch ${batch}");
        $sel->click("link=Filter by Batch");
        $sel->click("link=${batch}");
        $sel->wait_for_page_to_load($msTIMEOUT);
    }
    else{
        &diag("Deleting all jobs");
    }
    $sel->click("link=Select All");
    $sel->wait_for_page_to_load($msTIMEOUT);

    $sel->click("link=Delete");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->click("link=Delete");
    $sel->wait_for_page_to_load($msTIMEOUT);
}

##
# delete batches
# 
# @param[in] $sel - Selenium object
# @param[in] $batch - delete this batch (default all)
#
sub delete_batches($;$){
    my ($sel,$batch) = @_;

    $sel->click("link=BATCHES");
    $sel->wait_for_page_to_load($msTIMEOUT);

    if(defined($batch)){
        &diag("Deleting batch ${batch}");
        $sel->click("link=${batch}");
        $sel->wait_for_page_to_load($msTIMEOUT);
    }
    else{
        &diag("Deleting all batches");
        $sel->click("link=Select All");
        $sel->wait_for_page_to_load($msTIMEOUT);
    }

    $sel->click("link=Delete");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->click("link=Delete");
    $sel->wait_for_page_to_load($msTIMEOUT);
}

## 
# Run test 2.1.1 - Retrieve the status of a small group of modems.
#
# @param[in] $sel - Selenium object
# @param[in] $num_modems - Number of modems we should expect to see
# @param[in] $tag - tag which should be applied to the modems above.
#
# @test 1. Create a new tag called "Small Group" on the Tags display
# by clicking on Navigate/Create on the left-hand side of the
# display. Click Save after entering the new tag name. [Note - this is
# done in the setup.]
#
# @test 2. Add 3-5 modems, with public static IP accounts to BlueVue
# Group using the "Create" link on the Modems display (under
# Navigate). Enter the IP address for one of the modems, ensure that
# he "Automatic" check box is NOT set (if it is available) and click
# "Save". Repeat this for each new modem. [Note - this is done in the
# setup.]
#
# @test 3. Once all the new modems have been added select the modems
# on the main display by clicking on their checkboxes and select
# Tag. Add the "Small Group" tag to them. [Note - this is done in the
# setup.]
#
# @test 4. Select these new modems on the Modems display and click on
# the "Schedule Jobs" action to create a new status request
# job. Create a status query for these modems.
#
# @test 5. Ensure that the status jobs are issued and that the modem
# display and details are updated to reflect the current status of the
# modems.
#
# @test Expected Results: The user should be able to have BlueVue
# Group retrieve the status from a small number of modems. The BVG
# display should be updated accordingly, reflecting the current status
# of the modems. The history (i.e. success/failure) of the status
# request jobs should be stored in the database and viewable through
# the GUI.
#
sub test_2_1_1($$$)
{
    my ($sel,$num_modems,$tag) = @_;

    &diag("Running test 2.1.1");

    # steps 1-3 are done in the setup.

    # steps 4-5 are the standard "group test"
    group_test($sel,$num_modems,$tag);
}

##
# Run test 2.1.2 Retrieve the status of a large group of modems.
#
# @param[in] $sel - Selenium object
# @param[in] $num_modems - Number of modems we should expect to see
# @param[in] $tag - tag which should be applied to the modems above.
#
# @test 1. Create a new tag called "Large Group" in the Tags
# display. [Note - this is done in the setup.]
#
# @test 2. Using a tool like Excel, create a CSV file with columns
# named "IPAddress", and "Port". Under "IPAddress", enter the IP
# address for each modem to add (if using the Modem Simulator, this
# will the the simulator machine address). Under "Port", list the port
# for each modem (if using the Modem Simulator, this value will be
# unique for each modem, otherwise it will be 5070). Add at least 200
# new modems to the CSV file. To save a file in Excel as CSV, select
# "Save As..." and set thefile type to CSV. [Note - this is done in
# the setup.]
#
# @test 3. Add the list of modems, with public static IP accounts, to
# BlueVue Group using the "Batch Create Modems" link on the Modems
# page. Use the CSV file created in step 2. When adding these new
# modems to BVG, associate them with the "Large Group" tag and ensure
# that the "Create Automatic Status Queries" checkbox is clear. [Note
# - this is done in the setup.]
#
# @test 4. Creating a status request for a large group of modems can
# either be done by clicking on "Select All" ont he Modesm page or
# scheduling the job for all modems with the "Large Group" tag (which
# can be accomplished on the Tags page).
#
# @test 5. Ensure that the status jobs are issued and that the modem
# display and details are updated to reflect the current status of the
# modems.
#
# @test Expected Results: The user should be able to have BlueVue
# Group retrieve the status of any number of modems (large or
# small). The BVG display should be updated accordingly, reflecting
# the current status of the modems. The history (i.e. success/failure)
# of the status request jobs should be stored in the database and
# viewable through the GUI.
# 
sub test_2_1_2($$$)
{
    my ($sel,$num_modems,$tag) = @_;

    &diag("Running test 2.1.2");

    # steps 1-3 are done in the setup.
    
    # steps 4-5 are the standard "group test"
    group_test($sel,$num_modems,$tag);
}

##
# Run test 2.1.3 - Create an auto rescheduling status request
#
# @param[in] $sel - Selenium object
# @param[in] $num_modems - Number of modems we should expect to see
# @param[in] $tag - tag which should be applied to the modems above.
#
# @test 1. Select several modems (or a group with a unique tag) on the
# Modems page (or the Tags page, if creating the job for a specific
# tag group).
#
# @test 2. Click on the "Schedule Job" to create a new status job
# against these modems. The job type of status should be selected and
# the "Automatic" checkbox should be set. It is recommended that a
# "Batch" name is specified so that the jobs can be easily tracked.
#
# @test 3. Similar to test procedures 2.1.1 and 2.1.2, ensure that a
# status request is performed for each modem that was selected.
#
# @test 4. Wait for the polling interval to expire (this value is set
# by any tags associated with the modem, which defaults to 1 hour) and
# ensure that a new status query is issued to all originally selected
# modems.
#
# @test 5. If possible, continue this test for an extended period
# (several days) and ensure that the status queries are regularly
# rescheduled.
#
# @test 6. Once done, cancel/delete the jobs. If a batch name was
# provided, this can be done on the Batches page.
#
# @test Expected Results: It should be possible to create a status
# query job which automatically rescheduled itself, allowing the user
# to monitor the health of their remote devices. When an "automatic"
# job is created, a new job will automaticlaly be scheduled once the
# prior one is completed.
#
sub test_2_1_3($$$)
{
    my ($sel,$num_modems,$tag) = @_;

    &diag("Running test 2.1.3");

    # create our jobs, make sure there are the right amount and that
    # they work.

    # default, just tests that it recurs
    my $numtests = 2;
    # unless we want to run the long tests, in which case do it for 2 days.
    if($RUNLONG){
        $numtests = 48;
    }

    my $batch = "Test Batch 2.1.3";

    # group_test is steps 1-5
    group_test($sel, $num_modems, $tag, $batch, 1, $numtests);

    # step 6 - delete the batch
    delete_jobs($sel,$batch);
}

##
# Run test 2.1.4 - Create an auto rescheduling status request with a
# frequent period
#
# @param[in] $sel - Selenium object
# @param[in] $num_modems - Number of modems we should expect to see
# @param[in] $tag - tag which should be applied to the modems above.
#
# @test 1. The default query interval for automatic jobs is once an
# hour. Edit the settings of the "Small Group" tag, created in
# procedure 2.1.1, to change the query interval to 10 minutes.
#
# @test 2. On the Tags page, create an automatic status job for all
# the modems in the "Small Group" by clicking on the number of modems
# link, selecting all modems and selecting the "Schedule Job" action
# (under Navigate). The new job should be of type Status and have the
# "Automatic" checkbox selected. A batch name should be specified, so
# that the jobs can be easily monitored and removed.
#
# @test 3. Ensure that a new status request is issued to all modems in
# the "Small Group" every 10 minutes.
#
# @test 4. After several status queries (30-40 minutes) change the
# polling interval associated with the "Small Group" tag to 240 (4
# hours).
#
# @test 5. Ensure that after the next status query, that new status
# queries are being scheduled at a 4 hour interval.
#
# @test 6. Once done, cancel/delete the automatic jobs. If a batch
# name was specified, deletion of the batch can be performed on the
# batches page.
#
# @test 7. Change the polling interval for the "Small Group" back to
# the default (60 minutes). 
#
# @test Expected Results: Setting the polling interval associated with
# a tag will adjust the interval at which "automatic" jobs for modems
# with that tag are issued. Setting this value to 10 minutes should
# result in status queries being issued every 10 minutes. Changing
# this value to a long interval, like 4 hours, will result in future
# jobs being rescheduled every 4 hours.
#
sub test_2_1_4($$$)
{
    my ($sel,$num_modems,$tag) = @_;

    &diag("Running test 2.1.4");

    my $batch = "Test Batch 2.1.4";

    # step 1 - set the polling interval to 10 mins
    set_tag_polling_interval($sel,$tag,10);
    
    # steps 2-3 - create our jobs, make sure there are the right
    # amount and that they work.

    # for starters, we want to run "several", 30-40 minutes, so call
    # it 5 iterations.
    group_test($sel, $num_modems, $tag, $batch, 1, 5, 10);

    # step 4 - set the interval to 240 (4 hours)
    set_tag_polling_interval($sel,$tag,240);

    # step 5
    # and now we want to make sure that new queries will be at a 4
    # hour interval. We don't actually need to wait for 4 hours, just
    # until the current jobs are done and we start seeing jobs which
    # are "awhile" from now. Since the verification tests that the
    # jobs were rescheduled for about $interval from now, we can just
    # test once.
    verify_jobs($sel, $tag, $num_modems, 1, 1, 240);

    # step 6 - clear jobs for that batch
    delete_jobs($sel,$batch);
    
    # step 7 - set the interval back to 60 mins
    set_tag_polling_interval($sel,$tag,60);
}

##
# Run test 2.1.5 - Auto status polling with off-line modem
#
# @param[in] $sel - Selenium object
# @param[in] $single_modem_tag - tag for the single modem we wish to
# work on
# @param[in] $do - do channel controlling the modem we're going to
# power cycle.
#
# @test 1. Change the polling interval for the "Small Group" modems to
# 10 minutes, as performed in test procedure 2.1.4
#
# @test 2. On the Modems page, create an automatic status job for all
# the modems in the "Small Group" by first filtering the display to
# modems with the "Small Group" tag, clicking "Select All" and
# selecting the "Schedule Job" action. This new jhob shoul dbe of type
# status and have the "Automatic" checkbox selected. A back name
# should be specified, so the jobs can be easily monitored and
# removed.
#
# @test 3. Select one of the active modems in the "Small Group" and
# disconnect the power.
#
# @test 4. Ensure that the next status query for this modem faisl and
# ensure that a new status query is scheduled for 10 minutes from now.
#
# @test 5. Check that the GUI properly reflects the status of this
# off-line modem. The modem should be highlighted in yellow in the
# Modem list tidplay (not: the default settings require 6 hours if
# inactivity before a modem is flagged, so for testing, this value may
# be decreased.
#
# @test 6. Ensure that the second query fails and then reconnect the modem.
#
# @test 7. After another 10 minutes, ensure that the queries do work
# now that the modme has been reconnected. Also ensure that the GUI is
# updated to reflect the state of the modem now that it is on-line (it
# should no longer be highlighted in yellow or red).
#
# @test 8. Once done, cancel/delete the automatic jobs. If a batch
# name was specified, deletion of the batch can be performed from the
# Batches page.
#
# @test 9. Change the polling interval for the "Small Group" back to
# the default (60 minutes)
#
# @test Expected Results: Setting the polling interval associated with
# a tag will adjust the interval at which "automatic" jobs for modems
# with that tag are issued. Setting this value to 10 minutes should
# result in status queries being issued every 10 minutes. Changing
# this value to a long interval, like 4 hours, will result in future
# jobs being rescheduled every 4 hours.
#
sub test_2_1_5($$$$$)
{
    my ($sel,$single_modem_tag,$do) = @_;

    &diag("Running test 2.1.5");

    my $batch = "Test Batch 2.1.5";

    # step 1 - set the polling interval to 10 mins
    set_tag_polling_interval($sel,$single_modem_tag,10);
    
    # step 2 - create our jobs, make sure there are the right
    # amount and that they work, so run the base validation once.
    group_test($sel, 1, $single_modem_tag, $batch, 1, 1, 10);

    # step 3 - cut power to the modem 

    # FIXME - we need to call the power command and/or use the SIXNET
    # supporting perl module...


    # step 4 - verify that the query for the modem fails and that a
    # new query is scheduled.

    # step 5 - check that the GUI reflects the stats of the modem
        
    # FIXME (steps 4 and 5) - we know the IP address of the modem, so
    # we can look it up by that.
    
    # step 6 - ensure that the second query fails and then reconnect
    # the modem

    # FIXME - we need to call the power command and/or use the SIXNET
    # supporting perl module...

    # step 7 - make sure the queries now work again

    # FIXME - do the above
    
    # step 8 - delete jobs
    delete_jobs($sel,$batch);

    # step 9 - set the interval back to 60 mins
    set_tag_polling_interval($sel,$single_modem_tag,60);
}

##
# Run test 2.1.6 - Poll status from a modem configured with a password
#
# @param[in] $sel - Selenium object
# @param[in] $single_modem_tag - tag for the single modem we wish to
# work on
#
# @test 1. Initially set a password in one of the modems being used
# for testing purposes. A password can be set either through BlueVue
# Device Manager (on the Conf Password tab) or by connecting to the
# telnet server in the modem and issuing the following AT commands:
# AT+BRPSWD=1,1,password
# AT&W
#
# @test 2. Configure BlueVue Group with the matching password for that
# modem. If the modem being used has not already been added to Group,
# it must be added first (see 2.1.1). To set the password for a modem,
# click on the Alias for the modem in the Modems display and the click
# "Edit" on the modem details page. Check the "Password Enabled" box
# and enter the password, in this case password, into the relevant
# text box. Finally, click "Save" to save the change.
#
# @test 3. On the modem details page, ensure that the Password Enabled
# field now indicates true.
#
# @test 4. Create a status query job for this modem by clicking on the
# "Schedule Job" link and setting the job type to Status. Create the
# job for this modem by clicking "Save".
#
# @test 5. Monitor the job status (Navigate/View Jobs) and the job
# history (Navigate/View History) to ensure that the newly create
# status job completes successfully.
#
# @test 6. If these modems will be used for future testing, the
# passwords can be left configured in both the modems and BlueVue
# Group, as Group should be able to use passwords for all job
# types. Otherwise, the passwords can be removed by issuing:
# AT+BRPSWD=0,1,password
# AT&W
# If the password is removed from the modem, it should also be
# disabled in group.
#
# @test 7. Expected Results: BlueVue Group should be able to perform
# status queries, configurations and firmare updates for modems with
# and without passwords enabled. In this case, the status query should
# succeed when the proper password is configured to match that in the
# modem.
#
sub test_2_1_6($$)
{
    my ($sel, $single_modem_tag) = @_;

    &diag("Running test 2.1.6");

    # number of tests for skip
    my $numtests = 8;

  SKIP: {
        # get the IP from our modem
        select_tagged_modems($sel,$single_modem_tag);
        # link to modem information page for first modem in list
        $sel->click("//tr[2]/td[2]/a");
        $sel->wait_for_page_to_load($msTIMEOUT);

        my $ip = get_switch_ip_address($sel);

        if(!$ip){
            fail("Failed to get IP address for modem at ${ip}.");
            skip("Skipping all tests in 2.1.6", $numtests-1);
        }
        
        # set up telnet
        my $telnet = Expect->spawn("telnet ${ip} 6070");

        $telnet->log_stdout(0); # turn this on for debugging
        
        # step 1 - set the password in the modem

        if (!eval{
            $telnet->expect($sTIMEOUT,
                            "Welcome to BlueTree Wireless BT modem")|| 
                              die("Unable to connect to modem at ${ip}\n");
            $telnet->send("AT+BRPSWD=1,1,password\n");
            $telnet->expect($sTIMEOUT,"OK") ||
              die("Unable to set password in modem at ${ip}\n");
            $telnet->send("AT&W\n");
            $telnet->expect($sTIMEOUT,"OK") || 
              die("Unable to save password setting in modem at ${ip}\n");
        }) {
            if ($@) {
                &diag("Attempt to set password in modem at ${ip} failed. " . 
                      "Message was: " . $@);
            }
            fail("Failed to set password in modem at ${ip}.");
            skip("Skipping all tests in 2.1.6", $numtests-1);
        }

        # success
        
        # step 2 - set the password in the UI
        # set the password
        select_tagged_modems($sel,$single_modem_tag);
        # link to modem information page for first modem in list
        $sel->click("//tr[2]/td[2]/a");
        $sel->wait_for_page_to_load($msTIMEOUT);
        $sel->click("link=Edit");
        $sel->wait_for_page_to_load($msTIMEOUT);
        $sel->click("update_passwordSettings_passwordEnabled");
        $sel->type("update_passwordSettings_password", "password");
        $sel->click("link=Save");
        $sel->wait_for_page_to_load($msTIMEOUT);

        # step 3 - verify that the password is now enabled.
        my $data = $sel->get_table("//div[\@id='content']/div/table.13.1");

        cmp_ok($data, "eq", "true", 
               "Password enabled for modem at ip ${ip}");
        
        # step 4 - add a job status query for this modem
        add_jobs($sel, 1, $single_modem_tag);

        # step 5 - monitor the job status / check the history (all
        # done by verify job).
        verify_jobs($sel,$single_modem_tag,1);

        # step 6 - clear the password..

        # in group
        select_tagged_modems($sel,$single_modem_tag);
        # link to modem information page for first modem in list
        $sel->click("//tr[2]/td[2]/a");
        $sel->wait_for_page_to_load($msTIMEOUT);
        $sel->click("link=Edit");
        $sel->wait_for_page_to_load($msTIMEOUT);
        $sel->click("update_passwordSettings_passwordEnabled");
        $sel->click("link=Save");
        $sel->wait_for_page_to_load($msTIMEOUT);

        # and clear it in the modem
        if (!eval{
            $telnet->send("AT+BRPSWD=0,1,password\n");
            $telnet->expect($sTIMEOUT,"OK") ||
              die("Unable to clear password in modem at ${ip}\n");
            $telnet->send("AT&W\n");
            $telnet->expect($sTIMEOUT,"OK") || 
              die("Unable to save password setting in modem at ${ip}\n");
            $telnet->soft_close();
        }) {
            if ($@) {
                &diag("Attempt to clear password in modem at ${ip} failed."
                      . "Message was: " . $@);
            }
            &diag("Failed to clear password in modem at ${ip}.");
        }
    }
    ;
}

##
# Run test 2.1.7 - IP address updates using IP notifications from the modem
#
# @param[in] $sel - Selenium object
# @param[in] $single_modem_tag - tag for the single modem we wish to
# work on
# @param[in] $server_ip - IP address of the listening jobs server
#
# @test 1. BlueVue Group, by default, listens on TCP and UDP ports
# 7777 for IP registration messages. The modems under test must be
# able to initiate a connection to the BlueVue Group server on these
# ports, so the server must either be on the public-facing internet or
# have the appropriate ports forwarded from the firewall. The ports
# can be changed by logging in as the root user and changing the
# appropriate setting.
#
# @test 2. Ensure that at least one modem is currently "registered"
# with BlueVue Group. If a modem needs to be added to Group, one can
# be added through the "Create Modems" link on the Modems page.
#
# @test 3. Set up one of the modems registered with Group to send an
# IP registration message to Group, preferably this modem will have a
# dynamic public IP account. The IP registration capability can be
# enabled either through BlueVue Device Manager (on the Conf General
# tab, by setting the Dynamic IP Registration values) or by connecting
# to the telnet server in the modem and issuing the following AT
# commands:
# AT+BIPREG=1,<server IP>,<server port>,<server port type>,0
# AT&W
# where: the server IP should be the publicly accessible IP address of
# the Group server, the server port should be the port which Group is
# configured to listen on (see step 1) and the server port type is 0
# if UDP is to be used or 1 if TCP is to be used.
#
# @test 4. Check that the modem currently has an IP address listed in
# Group and create a status query job for the modem (select the
# modem's checkbox, click Schedule Jobs, set the job type to Status
# and finally click Save)
#
# @test 5. Ensure that the status query job is able to contact the
# modem properly
#
# @test 6. Reset the modem to force it to obtain a new IP address from
# the network.
#
# @test 7. Ensure the IP address in group is updated from the new IP
# address from the modem (this should take 1-2 minutes from the reboot
# of the modem).
#
# @test 8. Create another status query job for the modem and ensure
# that it is able to successfully contact the modem under test at the
# new IP address.
#
# @test Expected Results: BlueVue Group will update the database with
# the latest IP address when an IP registration message is received
# from the modem. This allows Group to track and contact modems with
# dynamic public IP addresses.
#
# FIXME - This needs a DHCP server
#
sub test_2_1_7($$)
{
    my ($sel, $single_modem_tag, $server_ip) = @_;

    &diag("Running test 2.1.7");

    # number of tests for skip
    my $numtests = 3;

    # step 1 - this is a system setup thing - nothing to do, really

    # step 2 - we're using a modem which is already registered as part
    # of the "small group", so we don't need to register it

  SKIP: {
        # step 3 - turn on the IP registration

        # get the IP from our modem
        select_tagged_modems($sel,$single_modem_tag);
        # link to modem information page for first modem in list
        $sel->click("//tr[2]/td[2]/a");
        $sel->wait_for_page_to_load($msTIMEOUT);
        my $ip = get_switch_ip_address($sel);
        
        if(!$ip){
            fail("Failed to get IP address for modem at ${ip}.");
            skip("Skipping all remaining tests in 2.1.7", $numtests-1);
        }

        # set up telnet
        my $telnet = Expect->spawn("telnet ${ip} 6070");

        $telnet->log_stdout(0); # turn this on for debugging

        if (!eval{
            $telnet->expect($sTIMEOUT,
                            "Welcome to BlueTree Wireless BT modem")
              || die("Unable to connect to modem at ${ip}\n");
            $telnet->send("AT+BIPREG=1,${server_ip},7777,1,0\n");
            $telnet->expect($sTIMEOUT,"OK") ||
              die("Unable to set IP registration in modem at ${ip}\n");
            $telnet->send("AT+W\n");
            $telnet->expect($sTIMEOUT,"OK") || 
              die("Unable to save IP registration setting in modem at " .
                  "${ip}\n");
        }) {
            if ($@) {
                &diag("Attempt to set IP registration in modem at ${ip} " .
                      "failed. Message was: " . $@);
            }
            fail("Failed to set IP registration in modem at ${ip}.");
            skip("Skipping all tests in 2.1.7", $numtests-1);
        }

        # Step 4 - check that the modem currently has an IP address
        # hmm.. this looks familiar...
        select_tagged_modems($sel,$single_modem_tag);
        # link to modem information page for first modem in list
        $sel->click("//tr[2]/td[2]/a");
        $sel->wait_for_page_to_load($msTIMEOUT);
        $ip = get_switch_ip_address($sel);

        # FIXME - probably want to validate that the IP is actually valid..
        cmp_ok($ip, "ne", "", "Verified that modem has an IP address");

        # add a job for it
        add_jobs($sel, 1, $single_modem_tag);

        # step 5 - verify that the job worked
        verify_jobs($sel,$single_modem_tag,1);

        # FIXME - telnet in and turn on DHCP on the modem

        # step 6 - reset the modem

        # FIXME - power cycle the modem

        select_tagged_modems($sel,$single_modem_tag);
            
        sleep(120);             # sleep to give the modem time to boot

        # step 7 - verify that the IP address changed
        my $new_ip = wait_for_table($sel, 
                                    "//div[\@id='content']/div/table.6.1",
                                    $msTIMEOUT, $ip, 1);
        cmp_ok($new_ip, "ne", $ip, 
               "IP address for modem formerly at ${ip} changed.");

        # step 8 - verify that we get status at new IP

        # add a status job for it
        add_jobs($sel, 1, $single_modem_tag);

        verify_jobs($sel,$single_modem_tag,1);

        # FIXME - telnet in, turn DHCP off and set to static IP.
    }
    ;
}

##
# Run test 2.2.1 - Load a simple configuration script
#
# @param[in] $sel - Selenium object
# @param[in] $single_modem_tag - tag for the single modem we wish to
# work on
#
# @test 1. On the Modems page, click on the "Alias" for a single modem
# to display the modem's details. If there are currently no active
# modems in Group, a new one can be added by clicking on the "Create"
# link under Navigate on the Modems page.
#
# @test 2. On the modem details page, click on the link next to
# Configuration Script (will likely be iether edit or view). This will
# display the configuration editing page. You may need to click on
# "Edit Script" on the left if a script previously existed for the
# modem.
#
# @test 3. In the configuration text box, enter a short script. For example:
# sendln 'AT+BMNAME="ShortCfgTest"'
# waitln 'OK'
# sendln 'AT+BCFGV="TestCfg_221"'
# waitln 'OK'
# sendln 'AT&W'
# waitln 'OK'
#
# @test 4. Click "Save" to save the configuration script and then
# click "View Modem" to return the modem details [Note - the "View
# Modem" actually is "View Modem <Alias>" which is kind of a pain do
# to programmatically, so we're going to get back to the modem details
# page by looking it up by IP].
#
# @test 5. Click "Schedule Job" and create a "Reconfiguration" job for
# this modem.
#
# @test 6. Ensure that BlueVue Group eventually executes the job for
# this modem by checking the Job History for the modem. When complete,
# the history should display the job as completed successfully.
#
# @test 7. The configuration job will automaticlaly schedule a "status
# query" job for this modem. Once the status job completes, ensure
# that the Alias is now ShortCfgTest and the configuration version is
# reported as TestCfg_221
#
# @test Expected Results: BlueVue Group allows the user to specify a
# configuration script for one or more modems. This configuration
# script is issued by BlueVue Group, altering the modem in question's
# configuration.
#
sub test_2_2_1($$)
{
    my ($sel, $single_modem_tag) = @_;

    &diag("Running test 2.2.1");

    my $batch = "Test Batch 2.2.1";
    my $alias = "ShortCfgTest";
    my $config = "TestCfg_221";

    # Note that we're deviating from the script above and using the
    # simulator IP, as we assume it is a machine to which we can get,
    # and which we don't really care about.
    my $script = qq^
sendln 'AT+BMNAME="${alias}"'
waitln 'OK'
sendln 'AT+BCFGV="${config}"'
waitln 'OK'
sendln 'AT&W'
waitln 'OK'
^;
    script_test_helper($sel, $single_modem_tag, $script, 
                       $batch, $alias, $config);
}

##
# Run test 2.2.2 - Load a complex configuration script into a modem
#
# @param[in] $sel - Selenium object
# @param[in] $single_modem_tag - tag for the single modem we wish to
# work on
#
# @test 1. On the Modems page, click on the "Alias" for a single modem
# to display the modem's details. If there are currently no active
# modems in Group, a new one can be added by clicking on the "Create"
# link under Navigate on the Modems page.
#
# @test 2. On the modem details page, click on the link next to
# Configuration Script (will likely be iether edit or view). This will
# display the configuration editing page. You may need to click on
# "Edit Script" on the left if a script previously existed for the
# modem.
#
# @test 3. In the configuration text box, enter a long script. For example:
# sendln 'AT+BFWUPS=0'
# waitln 'OK'
# sendln 'AT&F'
# waitln 'OK'
# sendln 'AT+BMNAME="LongCfgTest"'
# waitln 'OK'
# sendln 'AT+BCMODE=1'
# waitln 'OK'
# sendln 'AT+BSFMBS=256'
# waitln 'OK'
# sendln 'AT+BGPSOE=1'
# waitln 'OK'
# sendln 'AT+BGPSPR=1,1,1;+BGPSNM="RMC"'
# waitln 'OK'
# sendln 'AT+BRPRDS=3,1,"10.150.82.13",51600,0'
# waitln 'OK'
# sendln 'AT+BRPRDS=9,1,"10.150.5.78",51600,0'
# waitln 'OK'
# sendln 'AT+BEVRPR=10,"IO1-GO1-GP1-RF1","9",0,1,500'
# waitln 'OK'
# sendln 'AT+BEVDIS=5,"IGN=0"'
# waitln 'OK'
# sendln 'AT+BEVENT=4,"DIS5!","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=5,"DIS5","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVGSS=1,"GS>2"'
# waitln 'OK'
# sendln 'AT+BEVENT=10,"GSS1","T5","S",0,0'
# waitln 'OK'
# sendln 'AT+BEVENT=25,"EVS10","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=26,"STOP","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVDIS=2,"DI2=0"'
# waitln 'OK'
# sendln 'AT+BEVENT=29,"DIS2!","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=19,"DIS2","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=11,"DST","D0100","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVGSS=2,"GS>60"'
# waitln 'OK'
# sendln 'AT+BEVENT=27,"STOP!","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=28,"GSS2","T120","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVDIS=1,"DI1=0"'
# waitln 'OK'
# sendln 'AT+BEVENT=21,"DIS1!","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=22,"DIS1","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVDIS=3,"DI3=1"'
# waitln 'OK'
# sendln 'AT+BEVENT=23,"DIS3","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=24,"DIS3!","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVDIS=4,"DI4=0"'
# waitln 'OK'
# sendln 'AT+BEVENT=14,"DIS4!","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=15,"DIS4","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=30,"DIS1!&DIS3","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=31,"DIS1&DIS3!","R","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=16,"DIS5!","T300","S",0,10'
# waitln 'OK'
# sendln 'AT+BEVENT=17,"DIS5","T600","S",0,10'
# waitln 'OK'
# sendln 'AT+BCMODE=1'
# waitln 'OK'
# sendln 'AT+BIGNIT=65'
# waitln 'OK'
# sendln 'AT&W'
# waitln 'OK'
# sendln 'AT+BCFGV="TestCfg_222"'
# waitln 'OK'
#
# @test 4. Click "Save" to save the configuration script and then
# click "View Modem" to return the modem details [Note - the "View
# Modem" actually is "View Modem <Alias>" which is kind of a pain do
# to programmatically, so we're going to get back to the modem details
# page by looking it up by IP].
#
# @test 5. Click "Schedule Job" and create a "Reconfiguration" job for
# this modem.
#
# @test 6. Ensure that BlueVue Group eventually executes the job for
# this modem by checking the Job History for the modem. When complete,
# the history should display the job as completed successfully.
#
# @test 7. The configuration job will automaticlaly schedule a "status
# query" job for this modem. Once the status job completes, ensure
# that the Alias is now LongCfgTest and the configuration version is
# reported as TestCfg_222
#
# @test Expected Results: Previously, we ensured that BlueVue Groupc
# an accept a simple configurations cript. This test ensures that a
# long confiugration script can also be utilized with BlueVue Group.
#
sub test_2_2_2($$)
{
    my ($sel, $single_modem_tag) = @_;

    &diag("Running test 2.2.2");

    my $batch = "Test Batch 2.2.2";
    my $alias = "LongCfgTest";
    my $config = "TestCfg_222";

    # Note that we're deviating from the script above and using the
    # simulator IP, as we assume it is a machine to which we can get,
    # and which we don't really care about.
    my $script = qq^
sendln 'AT+BFWUPS=0'
waitln 'OK'
sendln 'AT&F'
waitln 'OK'
sendln 'AT+BMNAME="${alias}"'
waitln 'OK'
sendln 'AT+BCMODE=1'
waitln 'OK'
sendln 'AT+BSFMBS=256'
waitln 'OK'
sendln 'AT+BGPSOE=1'
waitln 'OK'
sendln 'AT+BGPSPR=1,1,1;+BGPSNM="RMC"'
waitln 'OK'
sendln 'AT+BRPRDS=3,1,"${SIMULATOR}",51600,0'
waitln 'OK'
sendln 'AT+BRPRDS=9,1,"${SIMULATOR}",51600,0'
waitln 'OK'
sendln 'AT+BEVRPR=10,"IO1-GO1-GP1-RF1","9",0,1,500'
waitln 'OK'
sendln 'AT+BEVDIS=5,"IGN=0"'
waitln 'OK'
sendln 'AT+BEVENT=4,"DIS5!","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=5,"DIS5","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVGSS=1,"GS>2"'
waitln 'OK'
sendln 'AT+BEVENT=10,"GSS1","T5","S",0,0'
waitln 'OK'
sendln 'AT+BEVENT=25,"EVS10","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=26,"STOP","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVDIS=2,"DI2=0"'
waitln 'OK'
sendln 'AT+BEVENT=29,"DIS2!","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=19,"DIS2","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=11,"DST","D0100","S",0,10'
waitln 'OK'
sendln 'AT+BEVGSS=2,"GS>60"'
waitln 'OK'
sendln 'AT+BEVENT=27,"STOP!","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=28,"GSS2","T120","S",0,10'
waitln 'OK'
sendln 'AT+BEVDIS=1,"DI1=0"'
waitln 'OK'
sendln 'AT+BEVENT=21,"DIS1!","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=22,"DIS1","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVDIS=3,"DI3=1"'
waitln 'OK'
sendln 'AT+BEVENT=23,"DIS3","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=24,"DIS3!","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVDIS=4,"DI4=0"'
waitln 'OK'
sendln 'AT+BEVENT=14,"DIS4!","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=15,"DIS4","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=30,"DIS1!&DIS3","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=31,"DIS1&DIS3!","R","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=16,"DIS5!","T300","S",0,10'
waitln 'OK'
sendln 'AT+BEVENT=17,"DIS5","T600","S",0,10'
waitln 'OK'
sendln 'AT+BCMODE=1'
waitln 'OK'
sendln 'AT+BIGNIT=65'
waitln 'OK'
sendln 'AT&W'
waitln 'OK'
sendln 'AT+BCFGV="${config}"'
waitln 'OK'
^;

    script_test_helper($sel, $single_modem_tag, $script, 
                       $batch, $alias, $config);
}

##
# Helper for 2.2.1 and 2.2.2, since they are both basically the same
#
# @param[in] $sel - Selenium object
# @param[in] $single_modem_tag - tag for the single modem we wish to
# work on
# @param[in] $script - script to load into the modem
# @param[in] $batch - name of the batch which holds the job
# @param[in] $alias - alias to which the modem will be set
# @param[in] $config - config version to which the modem will be set
#
sub script_test_helper($$$$$$)
{
    my ($sel, $single_modem_tag, $script, $batch, $alias, $config) = @_;

    # step 1 - get the modem by IP
    select_tagged_modems($sel,$single_modem_tag);

    # link to modem information page for first modem in list
    $sel->click("//tr[2]/td[2]/a");
    $sel->wait_for_page_to_load($msTIMEOUT);
    my $ip = get_switch_ip_address($sel);
    
    # step 2 - click the Create/View link for the config script
    $sel->click("//div[\@id='content']/div/table/tbody/tr[12]/td[2]/a");
    $sel->wait_for_page_to_load($msTIMEOUT);

    clear_existing_modem_script($sel);

    # step 3 - type in the script
    # Note that we're deviating from the script above and using the
    # simulator IP, as we assume it is a machine to which we can get,
    # and which we don't really care about.
    $sel->type("updateConfig_configScript", $script);

    # step 4 - save
    $sel->click("link=Save");
    $sel->wait_for_page_to_load($msTIMEOUT);

    select_tagged_modems($sel,$single_modem_tag);
    
    # step 5 - Schedule a job for the modem
    add_jobs($sel, 1, $single_modem_tag, $batch, 0, "Reconfiguration");

    # step 6 - Verify that the job was executed
    verify_jobs($sel, $single_modem_tag, 1);

    # step 7 - check that the Alias is now ShortCfgTest and the
    # configuration version is TestCfg_221

    select_tagged_modems($sel,$single_modem_tag);
    $sel->click("//tr[2]/td[2]/a");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->table_is("//div[\@id='content']/div/table.1.1", $alias);
    $sel->table_is("//div[\@id='content']/div/table.5.1", $config);
}

## 
# This tests a group of modems to see if we can schedule a status job
# for all modems and then making sure the modems are all checked.
#
# @param[in] $sel - Selenium object
# @param[in] $num_modems - Number of modems which need to be checked
# (also the number of jobs which should have been created, the number
# of history items we have, etc.)
# @param[in] $tag - the name of the tag to use to select modems
# @param[in] $batch - the batch name to assign to the created jobs
# @param[in] $automatic - whether to create an automatic job or not 
#            (default not)
# @param[in] $numtests - Number of times to test that an automatic job
#                       was rescheduled. Has no meaning if automatic
#                       is not set.
# @param[in] $interval - polling interval which was specified in
#                        minutes. Default is 60.
#
# Note - this is basically a wrapper to calls to add_jobs and verify_jobs.
# 
sub group_test($$$;$$$$)
{
    my ($sel, $num_modems, $tag, $batch, $automatic, $numtests, 
        $interval ) = @_;

    add_jobs($sel, $num_modems, $tag, $batch, $automatic, "Status");

    verify_jobs($sel, $tag, $num_modems, $automatic, $numtests, $interval);
}

##
# Add jobs for the given modems
#
# @param[in] $sel - Selenium object
# @param[in] $num_modems - Number of modems which need to be checked
# (also the number of jobs which should have been created, the number
# of history items we have, etc.)
# @param[in] $tag - the name of the tag to use to select modems
# @param[in] $batch - batch to assign to the job (optional, default none)
# @param[in] $automatic - whether to create an automatic job or not 
#            (default not)
# @param[in] $type - type of job to create (default status)
#
sub add_jobs($$;$$$$)
{
    my ($sel, $num_modems, $tag, $batch, $automatic, $type) = @_;

    # Add job for modems

    if(defined($tag)){
        # select modems with the given tag
        select_tagged_modems($sel,$tag);
    }
    else {
        # select all modems
        $sel->click("link=MODEMS");
        $sel->wait_for_page_to_load($msTIMEOUT);
    }

    $sel->click("link=Select All");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->click("link=Schedule Jobs");
    $sel->wait_for_page_to_load($msTIMEOUT);

    # set the time for $num_modems seconds from now, so that we have
    # time to count the jobs (because it takes us about three  seconds to
    # count each modem) + add a pad factor of 1 minutes (which is also
    # useful since we drop seconds in our time conversion)
    my @now = localtime(time());
    my @dt = localtime(time() + $num_modems * 3 + 60);

    # date popup doesn't work for ieeeeee

    if($BROWSER eq "iexplore"){
        $sel->type("scheduleDate",strftime($JOB_DATE_FORMAT,@dt));
    }
    else{
        # this is the date popuppy thingy. We either need to pick today or
        # tomorrow.. anyway...
        $sel->click("scheduleDate"); # click it to get the popup
        if($now[4] != $dt[4]){
            # click the next arrow on the date picker popup
            $sel->click("//div[\@id='ui-datepicker-div']/div/a[2]/span");
        }
        # click the day on the popup, which is just the number of the day
        # of the month.
        $sel->click("link=${dt[3]}");
    }

    # and then the time is normal
    $sel->type("scheduleTime",strftime($JOB_TIME_FORMAT,@dt));

    if(!defined($type)){
        $type = "Status";
    }

    $sel->select("realCreate_jobType","label=${type}");

    if(defined($automatic) && $automatic){
        $sel->click("realCreate_automatic");
    }

    if(defined($batch) && length($batch) > 0){
        $sel->type("realCreate_batchName", $batch);
    }

    $sel->click("link=Save");
    $sel->wait_for_page_to_load($msTIMEOUT);
}

## 
# Verify that our job was created/recreated and everything looks
# right.
#
# @param[in] $sel - Selenium object
# @param[in] $tag - tag for our group of modems
# @param[in] $num_modems - Number of modems which need to be checked
# (also the number of jobs which should have been created, the number
# of history items we have, etc.)
# @param[in] $automatic - the job is automatic or not (default not)
# @param[in] $numtests - Number of times to test that an automatic job
#                       was rescheduled. Has no meaning if automatic
#                       is not set.
# @param[in] $interval - polling interval which was specified in
#                        minutes. Default is 60.
#
sub verify_jobs($$$;$$$)
{
    my ($sel, $tag, $num_modems, $automatic, $numtests, $interval) = @_;

    if(!defined($interval)){
        $interval = 60;
    }

    if(!defined($automatic)){
        $automatic = 0;
    }

    # In general, we want to run this only once
    if(!$automatic){
        $numtests = 1;
    }

    my $callback;
    my $elements;
    my %job_ids;

    # Make sure automatic is set to true if we wanted automatic.
    if($automatic){
        $callback = sub ($$;$)
        {
            my ($sel,$row) = @_;
            my $success = 1;
            
            if(!verify_table_helper($sel,
                                    "//form[\@id='Jobs']/table.${row}.8",
                                    "true",
                                    $msTIMEOUT)){
                $success = 0;
            }
            
            return $success;
        };
    }
    # else we just need an array of JobIDs
    %job_ids = verify_jobs_table($sel,$callback);
    
    # make sure there are exactly $num_modems jobs
    cmp_ok(scalar(keys(%job_ids)),"==",$num_modems, 
           "Created ${num_modems} jobs");

    while($numtests > 0)
    {
        # Okay, let's try to explain this
        #
        # In a one-off job case (the else case here), we want to wait
        # until there are no outstanding jobs, and we'll then check
        # everything, including the modem status. If it's not normal,
        # we complain.
        #
        # However, this doesn't work for the automatic case, because
        # the outstanding jobs will NEVER be 0, they will always be
        # 1. So, we need to wait for all the jobs we saw last time to
        # be gone, and then we'll check the table.

        # FIXME - use "Last contacted" instead? Is this any better?

        $callback = sub ($$)
        {
            my ($sel,$row,$num_jobs) = @_;
            my $success = 1;
            
            if(!verify_table_helper($sel,
                                    "//form[\@id='Modems']/table.${row}.4",
                                    "${num_jobs}",
                                    $sLONG_TIMEOUT,
                                    1)){
                $success = 0;
            }
            
            
            if(!verify_table_helper($sel,
                                    "//form[\@id='Modems']/table.${row}.3",
                                    "Normal",
                                    $sLONG_TIMEOUT)){
                $success = 0;
            }
            
            return $success;
            
        };

        my $num_jobs = "0";
        if($automatic){
            # Wait for the jobs to be done.
            # No verification is needed here, as if the jobs fail,
            # we'll catch that in the history (either missing jobs, or
            # failed jobs).
            wait_for_jobs($sel, $sLONG_TIMEOUT, %job_ids);
            $num_jobs = "1";
        }
        
        # Check results
        select_tagged_modems($sel,$tag);
        verify_table($sel,"Modems","Modem Label", $callback, $num_jobs);

        # no need to check that the number of modems is right - we checked
        # this when we created them.
        
        $callback = 
          sub($$)
          {
              my ($sel,$row) = @_;
              my $success = 1;

              if(!verify_table_helper($sel,
                                      "//form[\@id='History']/table.${row}.6",
                                      "Success",
                                      $msTIMEOUT)){
                  $success = 0;
              }
              
              my $location = "//form[\@id='History']/table.${row}.8";
              my $data = wait_for_table($sel,
                                        $location,
                                        $msTIMEOUT);
              my $regexp = qr/Job succeeded/;

              if($data !~ $regexp){
                  &diag("FAIL: Expected match of ${regexp} at ${location}, ". 
                        "got ${data}");
                  $success = 0;
              }
              
              return $success;
          };

        verify_history_table($sel, $callback, %job_ids);
        
        # if automatic, now that the jobs have run, make sure they
        # rescheduled somewhere near the proscribed interval
        $callback = 
          sub($$)
          {
              my ($sel,$row,$interval) = @_;
              my $success = 1;
              my $data = wait_for_table($sel, 
                                        "//form[\@id='Jobs']/table.${row}.5",
                                       $msTIMEOUT);

              # interval is in minutes, scale to seconds.
              my @dt = localtime(time() + ($interval * 60));

              my $timestring = strftime($JOB_TIME_FORMAT,@dt);

              # FIXME - our formatting string has leading 0's. Jon's
              # doesn't. There's no nice way using strftime to just
              # print months, days and hours as ints without a leading
              # 0, so we need to munge it. Of course, if we take the
              # data we get back and convert it to something we can
              # assert_delta with, that's even better.

              # FIXME - this is likely wrong. Basically, it goes that
              # far off the LAST successful query, not off now.. so,
              # we actually need to look in the history for that one,
              # add interval to it, and then check it.
              if($data eq $timestring){
                  &diag("Job Scheduling - Expected ${timestring}, got ${data}");
                  $success = 0;
              }
                 return $success;
          };

        %job_ids = verify_jobs_table($sel, $callback, $interval);

        --$numtests;
        if($numtests > 0){
            &diag("Sleeping for ${interval} minutes to test automatic
            job rescheduling");
            sleep($interval * 60);
        }
    }
}

##
# Create a tag
#
# @param[in] $sel - Selenium object
# @param[in] $name - Name of tag to create
#
sub create_tag($$)
{
    my ($sel,$name) = @_;

    $sel->click("link=TAGS");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->click("link=Navigate");
    $sel->click("link=Create");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->type("update_tag_name", $name);
    $sel->click("link=Save");
    $sel->wait_for_page_to_load($msTIMEOUT);

    cmp_ok($sel->get_table("//div[\@id='content']/table.1.1"),"eq",
           $name,"Created tag ${name}");
}

##
# create a modem via the GUI interface alone (as opposed to CSV import)
#
# @param[in] $sel - Selenium object
# @param[in] $ip - IP address where the modem lives
# @param[in] $port - port number on which the BlueTree modem interface
#                    listens
# @param[in] $tag - tag to associate with the modem (optional, default none)
#

sub create_modem($$$;$)
{
    my ($sel,$ip,$port,$tag) = @_;

    $sel->click("link=MODEMS");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->click("link=Navigate");
    $sel->click("link=Create");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->type("update_modem_ipAddress", $ip);
    $sel->type("update_modem_port", $port);
    if(defined($tag)){
        $sel->select("update_applyTag", "label=${tag}");
    }
    $sel->click("update_scheduleJob");
    $sel->click("link=Save");
    $sel->wait_for_page_to_load($msTIMEOUT);

    my $message = "Created modem ${ip}:${port}";
    
    if(defined($tag)){
        $message .= " with tag ${tag}";
    }
    
    # verify that the modem was added
    if($sel->get_table("//div[\@id='content']/div/table.6.1") eq $ip && 
       $sel->get_table("//div[\@id='content']/div/table.7.1") eq $port){
        pass($message);
    }
    else{
        fail($message);
    }
}

##
# Create a bunch of modems associated with the given tag, using the
# optional list of real modems first
#
# This basically implements the "add a bunch of modems" procedure
# described in the initial steps of 2.1.1
#
# @param[in] $sel - Selenium object
# @param[in] $num_modems - total number of modems to create
# @param[in] $tag - tag to create and associate modems with
# @param[in] $simulator_port - initial port number to use on simulator
#                    (optional, default 5070)
# @param[in] \@modems (optional) - list of physical modems to
# create. These are used first and then the list is padded out to
# $num_modems length
#
sub create_modem_group($$$;$@)
{
    my ($sel, $num_modems, $tag, $simulator_port, @modems) = @_;

    if(!defined($simulator_port)){
        $simulator_port = PORT_BASE;
    }

    # Add our modems, real modems first
    my $count = 0;
    for my $modem (@REAL_MODEMS){
        create_modem($sel, $modem, 5070);
        ++$count;
    }
    # and simulator modems
    for(; $count < $num_modems; ++$count){
        create_modem($sel, $SIMULATOR, $simulator_port);
        ++$simulator_port;
    }

    # add our tag
    create_tag($sel, $tag);

    # tag all modems
    tag_all_modems($sel, $tag);

    # Verify that all of our modems were added to that tag.
    select_tagged_modems($sel,$tag);
    
    cmp_ok(count_elements($sel,"Modems","Modem Label"),"==",$num_modems,
           "Created ${num_modems} modems with tag ${tag}");
}

##
# Tag all modems with the given tag
#
# @param[in] $sel - Selenium object
# @param[in] $tag - tag associate all modems with
#
sub tag_all_modems($$)
{
    my ($sel,$tag) = @_;

    $sel->click("link=MODEMS");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->click("link=Select All");
    $sel->wait_for_page_to_load($msTIMEOUT);
    tag_modems($sel,$tag);
}

##
# Tag the currently selected modem(s) with the given tag
#
# @param[in] $sel - Selenium object
# @param[in] $tag - tag associate modems with
sub tag_modems($$)
{
    my ($sel,$tag) = @_;
 
    $sel->click("link=Tag");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->select("realTag_tagID","label=${tag}");
    $sel->click("link=Tag");
    $sel->wait_for_page_to_load($msTIMEOUT);    
}

##
# create several modems via the GUI interface w/ CSV import 
#
# @param[in] $sel - Selenium object
# @param[in] $simulator - IP address where the modem simulator lives
# @param[in] $num_modems - total number of modems to create
# @param[in] $tag - tag to associate with the modem.
# @param[in] $simulator_port - initial port number to use on simulator
#                    (optional, default 5070)
# @param[in] \@modems - array of real modems to use
#
# This creates a CSV file consisting of all the REAL_MODEMS and then a
# bunch of simulator modems until we reach num_modems.
#
# @todo FIXME - this doesn't work through Selenium because we can't do
# file uploads.  See http://jira.openqa.org/browse/SEL-63. For now
# we're using WWW::Mechanize, and testing that it actually works
# through the browser manually.
#
sub batch_create_modems($$$$;$@)
{
    my ($sel,$simulator_ip,$num_modems,$tag,$simulator_port,@modems) = @_;

    &diag("Creating ${num_modems} modems on simulator ${simulator_ip} starting at ${simulator_port}.");

    # Create a temp file
    my ($temp_fh, $temp_name) = tempfile();

    if(!defined($simulator_port)){
        $simulator_port = PORT_BASE;
    }

    if (!defined($temp_fh)) {
        BAIL_OUT("Unable to create temporary file.");
    } else {
        # blow it out to a file

        # requisite header
        print $temp_fh "IPAddress,Port\n";

        # real modems
        my $count = 0;
        if(@modems){
            for my $modem (@modems){
                print $temp_fh "${modem},5070\n";
                ++$count;
            }
        }
        
        # simulated modems
        for(; $count < $num_modems; ++$count){
            print $temp_fh "${simulator_ip},${simulator_port}\n";
            ++$simulator_port;
        }

        close($temp_fh);

        # add our tag first, so we know it exists
        create_tag($sel, $tag);

        # Login from this (local) machine and upload the CSV file
        # since it does not work with the RC srunner.

        batch_create_modem_csv($temp_name,$tag);

        # back on the remote machine, do the rest of our tests
        # (including verifying that the modems were added)
        select_tagged_modems($sel,$tag);

        cmp_ok(count_elements($sel,"Modems","Modem Label"),"==",$num_modems,
               "Batch created ${num_modems} modems with tag ${tag}");

        # cleanup
        if(!unlink($temp_name)){
            &diag("Warning! Unable to remove temp file ${temp_name}");
        }
    }
}

##
# This waits for a location in a table to be set to a given value, or
# times out.
#
# @param[in] $sel - Selenium object
# @param[in] $location - Location of the item in the table
# @param[in] $value - Value which should be in that location
# @param[in] $timeout - how long to wait for that value, in seconds
#
# @note This is the same as wait_for_table_ok, except that it
# waits for the data to be correct, not just there.
#
sub wait_for_table_value($$$$)
{
    my ($sel,$location,$value,$timeout) = @_;

    my $data = wait_for_table($sel, $location, $timeout, $value);

    cmp_ok($data, "eq", $value, "get_table, ${location}, ${value}");
}

##
# This waits for a location in a table to exist and then verifies that
# it is set to a given value, or times out.
#
# @param[in] $sel - Selenium object
# @param[in] $location - Location of the item in the table
# @param[in] $value - Value which should be in that location
# @param[in] $timeout - how long to wait for that value, in seconds
#
# @note This is the same as wait_for_table_value, except that it
# doesn't wait for the data to be correct, merely waits for it to be
# there.
#
sub wait_for_table_ok($$$$)
{
    my ($sel,$location,$value,$timeout) = @_;

    my $data = wait_for_table($sel, $location, $timeout);

    cmp_ok($data, "eq", $value, "get_table, ${location}, ${value}");
}

##
# This waits for a location in a table to be present and (optionally)
# match a given value.
#
# @param[in] $sel - Selenium object
# @param[in] $location - Location of the item in the table
# @param[in] $timeout - how long to wait for that value, in seconds
# @param[in] $value - what the data should be (optional)
# @param[in] $invert - wait for the data to NOT be the given value
# (optional, default not)
#
# @returns The data from the table or undef if we time out.
#
sub wait_for_table($$$;$$)
{
    my ($sel,$location,$timeout,$value,$invert) = @_;

    my $count = 0;
    my $done = 0;
    my $data;

    if(!defined($invert)){
        $invert = 0;
    }

    while($count < $timeout && !$done){
        # this will either throw an exception (which will fail the
        # conditional) or return 1, which will pass.
        if (eval { $data = $sel->get_table("$location");
                   return 1;
               }) {
            if(defined($data)){
                if(defined($value)){
                    if($invert){
                        if($value ne $data){
                            $done = 1;
                        }
                    }
                    else {
                        if($value eq $data){
                            $done = 1;
                        }
                    }
                }
                else {
                    $done = 1;
                }
            } # else not done, sleep
        } # else not done, sleep

        if(!$done){
            sleep(1);
        }
        ++$count;
    }

    if(!$done){
        if(defined($value)){
            &diag("Timed out waiting for ${location} to be ${value}");
        }
        else{
            &diag("Timed out waiting for ${location} to be present");
        }
    }

    return $data;
}


##
# This waits for a given location in a table to be present and then
# checks to see that a given location in a table matches a given
# regexp
#
# @param[in] $sel - Selenium object
# @param[in] $location - Location of the item in the table
# @param[in] $regexp - Regexp which should match that location
# @param[in] $timeout - how long to wait for the regexp to match, in seconds
#
sub wait_for_table_regexp($$$$)
{
    my ($sel, $location, $regexp,$timeout) = @_;

    my $data = wait_for_table($sel, $location, $timeout);

    like($data, $regexp, "get_table, ${location}, ${regexp}" );
}

##
# Verifies and counts the contents of a table.
#
# This both counts all body elements in a table to try and find out
# how many of something there are (modems, etc.), and runs an optional
# callback. If the callback is not supplied, it just counts things.
#
# @param[in] $sel - Selenium object
# @param[in] $id - ID of the form containing the table
# @param[in] $stop - Name of the "stop" item - "Modem Label", "Job ID", etc.
# @param[in] $callback - Callback to use to run verification.
# @param[in] \@callback_args - Additional args (besides $sel and row)
# to pass to callback function.
#
# @returns Count of elements found
#
sub verify_table($$$;$@){
    my ($sel,$id,$stop,$callback,@callback_args) = @_;

    &diag("Verifying table ${id} (this may take awhile).");

    my $done = 0;
    my $count = 0;
    my $failures = 0;
    while(!$done){
        
        my $table_count_done = 0;
        my $row = 1; # starting at row 1 skips the top modem alias

        while(!$table_count_done){
            my $location = "//form[\@id='${id}']/table.${row}.1";
            my $cell = wait_for_table($sel,$location,$sLONG_TIMEOUT);
            # And here we use the bottom column label as our stop sentinel
            if(defined($cell) && $cell ne $stop){
                if(defined($callback)){
                    if(!$callback->($sel,$row,@callback_args)){
                        ++$failures;
                    }
                }
                ++$row;
                ++$count;
            }
            else{
                $table_count_done = 1;
            }
        }

        if($sel->get_table("pagenav.0.3") =~ /Next/){
            $sel->click("link=Next >");
            $sel->wait_for_page_to_load($msTIMEOUT);
        }
        else{
            $done = 1;
        }
    }

    cmp_ok($failures, "==", 0, "Table verified with 0 failures");

    return $count;
}

##
# Verifies and counts the contents of the jobs table.
#
# This both counts all body elements in the jobs table and builds a
# list of Job IDs in that table., and runs an optional callback. If
# the callback is not supplied, it just builds the list.
#
# @param[in] $sel - Selenium object
# @param[in] $callback - Callback to use to run verification.
#
# @returns Hash of Job ID's to counts
#
sub verify_jobs_table($;$@){
    my ($sel,$callback,@callback_args) = @_;

    my $stop = "Job ID";

    my %ids;

    &diag("Verifying jobs table (this may take awhile).");

    $sel->click("link=JOBS");
    $sel->wait_for_page_to_load($msTIMEOUT);

    my $done = 0;
    my $failures = 0;
    while(!$done){
        
        my $table_count_done = 0;
        my $row = 1; # starting at row 1 skips the top modem alias

        while(!$table_count_done){
            my $location = "//form[\@id='Jobs']/table.${row}.1";
            my $cell = wait_for_table($sel,$location,$sLONG_TIMEOUT);
            # And here we use the bottom column label as our stop sentinel
            if(defined($cell) && $cell ne $stop){
                if(exists($ids{$cell})){
                    $ids{$cell} += 1;
                }
                else {
                    $ids{$cell} = 1;
                }
                if(defined($callback)){
                    if(!$callback->($sel,$row,@callback_args)){
                        ++$failures;
                    }
                }
                ++$row;
            }
            else{
                $table_count_done = 1;
            }
        }

        if($sel->get_table("pagenav.0.3") =~ /Next/){
            $sel->click("link=Next >");
            $sel->wait_for_page_to_load($msTIMEOUT);
        }
        else{
            $done = 1;
        }
    }

    cmp_ok($failures, "==", 0, "Jobs table verified with 0 failures");

    return %ids;
}

##
# Verifies that the history table contains the appropriate ids and
# that the history entry is correct.
#
# @param[in] $sel - Selenium object
# @param[in] $callback - Callback to use to run verification.
# @param[in] %job_ids - ID's of jobs to check for.
#
sub verify_history_table($$%){
    my ($sel,$callback,%job_ids) = @_;

    my $stop = "Job ID";

    &diag("Verifying history table (this may take awhile).");

    # Check for history
    $sel->click("link=HISTORY");
    $sel->wait_for_page_to_load($msTIMEOUT);

    my $done = 0;
    my $failures = 0;
    while(!$done){
        
        my $table_count_done = 0;
        my $row = 1; # starting at row 1 skips the top modem alias

        while(!$table_count_done){
            my $location = "//form[\@id='History']/table.${row}.1";
            my $cell = wait_for_table($sel,$location,$sLONG_TIMEOUT);
            # And here we use the bottom column label as our stop sentinel
            if(defined($cell) && $cell ne $stop){
                if(exists($job_ids{$cell})){
                    if(defined($callback)){
                        if(!$callback->($sel,$row)){
                            ++$failures;
                        }
                    }
                    
                    if($job_ids{$cell} > 1){
                        $job_ids{$cell} -= 1;
                    }
                    else{
                        delete($job_ids{$cell});
                        # bust out clause - if we've found all our bits, stop.
                        if(scalar(keys(%job_ids)) == 0){
                            $table_count_done = 1;
                            $done = 1;
                        }
                    }
                }
                ++$row;
            }
            else{
                $table_count_done = 1;
            }
        }

        if($sel->get_table("pagenav.0.3") =~ /Next/){
            $sel->click("link=Next >");
            $sel->wait_for_page_to_load($msTIMEOUT);
        }
        else{
            $done = 1;
        }
    }

    cmp_ok($failures, "==", 0, "History table verified with 0 failures");
    my $outstanding = scalar(keys(%job_ids));
    if($outstanding > 0){
        &diag("Did not find the following jobs in the history table:");
        for my $job (keys(%job_ids)){
            &diag("     ${job}");
        }
    }
    cmp_ok($outstanding, "==", 0, "Found history entries for all jobs");
}

##
# Counts the contents of a table.
#
# This counts all body elements in a table to try and find out
# how many of something there are (modems, etc.)
#
# It is basically a code clarity wrapper for verify_table
#
# @param[in] $sel - Selenium object
# @param[in] $id - ID of the form containing the table
# @param[in] $stop - Name of the "stop" item - "Modem Label", "Job ID", etc.
#
# @returns Count of elements found
#
sub count_elements($$$)
{
    my ($sel, $id, $stop) = @_;
    return verify_table($sel,$id,$stop);
}

##
# Upload a CSV file to the server to create a pile of modems with the
# given tag.
#
# @param[in] $filename - the name of the file to upload
# @param[in] $tag - The name of the tag to use
#
# @todo FIXME - this has little to no error checking.
#
# @note This uses WWW::Mechanize. It does NOT do it through the
# browser under test.
#
sub batch_create_modem_csv($$)
{
    my ($filename,$tag) = @_;

    my $mech = WWW::Mechanize->new();

    $mech->get("${URL}/BVWeb/Login");
    $mech->submit_form(form_name => "Login",
                       fields => {
                                  userName => $USER,
                                  password => $PASS
                                 }
                      );
    
    $mech->get("${URL}/BVWeb/Modem/importModems");

    $mech->submit_form(form_name => "realImportModems",
                       fields => {
                                  csvFile => $filename,
                                  tagID => $tag,
                                  createJobs => "false",
                                  jobBatchName => ""
                                 }
                      );
}

##
# Set the polling interval for the given tag to the given interval
#
# @param[in] $sel - Selenium object
# @param[in] $tag - name of tag to modify
# @param[in] $interval - interval to set it to
#
# 
sub set_tag_polling_interval($$$)
{
    my ($sel, $tag, $interval) = @_;

    # change our polling interval
    $sel->click("link=TAGS");
    $sel->wait_for_page_to_load($msTIMEOUT);

    $sel->click("link=${tag}");
    $sel->wait_for_page_to_load($msTIMEOUT);

    $sel->click("link=Edit");
    $sel->wait_for_page_to_load($msTIMEOUT);

    $sel->type("update_tag_pollingIntervalMinutes",$interval);

    $sel->click("link=Save");
    $sel->wait_for_page_to_load($msTIMEOUT);
}

##
# Selects all modems with the given tag
# 
# @param[in] $sel - Selenium object
# @param[in] $tag - name of tagged modems to select
#
sub select_tagged_modems($$)
{
    my ($sel, $tag) = @_;
    
    $sel->click("link=MODEMS");
    $sel->wait_for_page_to_load($msTIMEOUT);
    $sel->click("link=Filter by Tag");
    $sel->click("link=${tag}");
    $sel->wait_for_page_to_load($msTIMEOUT);
}

##
# Selects a modem with the given IP address
#
# @param[in] $sel - Selenium object
# @param[in] $tag - name of tagged modems to select
#
# @returns 1 if success
# @returns 0 if failure
#
sub select_modem_by_ip($$)
{
    my ($sel,$ip) = @_;

    &diag("Looking for modem at ${ip}");

    $sel->click("link=MODEMS");
    $sel->wait_for_page_to_load($msTIMEOUT); 
    my $done = 0;
    my $success = 0;

    while(!$done){

        # Try to find the IP in the table, assume it's a link and
        # click on it.
        if($sel->is_text_present($ip)){
            $sel->click("link=${ip}");
            $sel->wait_for_page_to_load($msTIMEOUT);
            $done = 1;
            $success = 1;
        }
        else{
            # can't find it - try to go to the next page
            if($sel->get_table("pagenav.0.3") =~ /Next/){
                $sel->click("link=Next >");
                $sel->wait_for_page_to_load($msTIMEOUT);
            }
            else{
                # else, out of pages. Fail.
                $done = 1;
            }
        }
    }
    return $success;
}

##
# Helper for some diagnostic code I found myself overusing in
# callbacks for verify_table
#
# @param[in] $sel - Selenium object
# @param[in] $location - location to look fo
# @param[in] $value - value to check against
# @param[in] $timeout - timeout
# @param[in] $wait_for_value - whether or not to wait for $location to
# be $value (optional, default not)
#
sub verify_table_helper($$$$;$)
{
    my ($sel, $location, $value, $timeout, $wait_for_value) = @_;
    my $success = 1;

    my $data;

    if(!defined($wait_for_value)){
        $wait_for_value = 0;
    }

    if($wait_for_value){
        $data = wait_for_table($sel, $location, $msTIMEOUT, $value);
    }
    else {
        $data = wait_for_table($sel, $location, $msTIMEOUT);
    }
    
    if($data ne $value){
        &diag("FAIL: Expected ${value} at ${location}, got ${data}");
        $success = 0;
    }
    return $success;
}

##
# Get the ip address off the modem information page
#
# @param[in] $sel - Selenium object
#
sub get_switch_ip_address($)
{
    my ($sel) = @_;

    my $ip = $sel->get_table("//div[\@id='content']/div/table.6.1");

    # FIXME - this should probably validate a proper IP address

    return $ip;
}

##
# Clear any existing modem script
#
# @param[in] $sel - Selenium object
#
sub clear_existing_modem_script($)
{
    my ($sel) = @_;

    # if we're not in edit mode, it's because there is an existing
    # script. Get us into edit mode, delete the script, then save it,
    # then get us back into edit mode.
    if($sel->is_element_present("link=Edit Script")){
        $sel->click("link=Edit Script");
        $sel->wait_for_page_to_load($msTIMEOUT);
        $sel->type("updateConfig_configScript","");
        $sel->click("link=Save");
        $sel->wait_for_page_to_load($msTIMEOUT);
        $sel->click("link=Edit Script");
        $sel->wait_for_page_to_load($msTIMEOUT);
    }
}

##
# Wait for the given jobs to be gone from the jobs table.
#
# @param[in] $sel - Selenium object
# @param[in] $timeout - how long to wait for the jobs to complete (in seconds)
# @param[in] %job_ids - ID's of jobs to check for
#
sub wait_for_jobs($$%){
    my ($sel,$timeout,%job_ids) = @_;

    my $stop = "Job ID";
    my $page = "link=HISTORY";

    &diag("Waiting for jobs to complete (this may take awhile).");

    my @found_ids;

    # Check for jobs
    $sel->click($page);
    $sel->wait_for_page_to_load($msTIMEOUT);

    my $timeout_step = 10;

    my $done = 0;
    while(!$done){
        # Try to find the job ID in the table
        for my $key (keys(%job_ids)){
            if($sel->is_text_present($key)){
                push(@found_ids, $key);
            }
        }
        
        # next page
        if($sel->get_table("pagenav.0.3") =~ /Next/){
            $sel->click("link=Next >");
            $sel->wait_for_page_to_load($msTIMEOUT);
        }
        else{
            # out of pages, bookkeeping, test for exit, etc.
            for my $key (@found_ids){
                delete($job_ids{$key});
            }

            if(scalar(keys(%job_ids)) == 0){
                $done = 1;
            }
            else {
                # back to the first page.
                $sel->click($page);
                $sel->wait_for_page_to_load($msTIMEOUT);
            }
        }

        if($timeout == 0){
            $done = 1;
            &diag("Timed out waiting for jobs to be done");
        }
        else {
            if($timeout > $timeout_step){
                sleep($timeout_step);
                $timeout -= $timeout_step;
            }
            else{
                sleep($timeout);
            }
        }
    }
}

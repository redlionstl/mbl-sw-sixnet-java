<%--
 % Clear shutdown orders.
 % 
 % Jonathan Pearson
 % June 9, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="servers">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.servers" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/servers/breadcrumbs/clearShutdown.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/servers/leftnav/clearShutdown.ln.html" %>
							
							<div id="content">
								<s:actionerror/>
								
								<s:form action="realClearShutdown">
									<p>
										<s:text name="i18n.server.confirm.clearShutdown" />
									</p>
									
									<p>
										<s:if test="shutdownOrders == null || shutdownOrders.equals('')">
											<s:text name="i18n.server.info.noServerShuttingDown" />
										</s:if>
										<s:elseif test="shutdownOrders.equalsIgnoreCase('ALL')">
											<s:text name="i18n.server.info.allServersShuttingDown" />
										</s:elseif>
										<s:else>
											<s:text name="i18n.server.info.serverXShuttingDown">
												<s:property value="shutdownOrders" />
											</s:text>
										</s:else>
									</p>
								</s:form>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

/*
 * refresher.js
 * 
 * Requires that JQuery libraries have been loaded and that the following variables
 * are defined:
 * 
 *  - formID :: The ID of the form tag that needs to be serialized and submitted
 *      to retain selections, filters, pagination, ...
 *  - ajaxPage :: The name of the AJAX page to submit the form to (form action)
 *  - elemName :: When restoring checkboxes, if a checkbox cannot be found, create
 *      a hidden element with this name to hold the value
 * 
 * Alternatively, you may set 'noCheckboxes' to true and skip 'elemName'
 * 
 * Call 'cancelPageRefresher()' to stop the refresher.
 * 
 * Jonathan Pearson
 * June 19, 2009
 * 
 */

var selected;
var values;
var scrollTop;
var scrollLeft;

var showCheckboxes = (typeof(noCheckboxes) == "undefined" || !noCheckboxes);
var cancelRefresh = false;

// This is set up as a variable so it can be replaced without errors
var cancelPageRefresher = function() {
	cancelRefresh = true;
};

function refreshPage() {
	// Just stop now, if we're supposed to
	if (cancelRefresh) return;
	
	// Serialize the form (to retain selections, filters, page number...)
	var postData = $("#" + formID).serialize();

	// Post to the AJAX page to retrieve updates
	$.post(ajaxPage, postData, function(data) {
		// When we get the result back...
		// Save the scrolling position
		scrollTop = $("div.frame")[0].scrollTop;
		scrollLeft = $("div.frame")[0].scrollLeft;
		
		if (showCheckboxes) {
			// Make sure that the checkboxes remain checked; the user
			// may have selected some between the post and the response,
			// and we wouldn't want to lose them
			selected = new Array();
			values = new Array();
			
			$("input").filter(function(index) {
				return (this.type == "checkbox" && this.checked);
			}).each(function(index) {
				selected[selected.length] = this.id;
				values[values.length] = this.value;
			});
		}

		// Set the update function to run after the DOM has updated
		if (!cancelRefresh) {
			// Update the table
			$("#content").html(data);
			
			window.setTimeout(finishRefresh, 10);
		}
	}, "html");
}

function finishRefresh() {
	if (showCheckboxes) {
		// Restore the checkboxes
		for (var i = 0; i < selected.length; i++) {
			var elem = $("#" + selected[i]);
			if (elem.length > 0) {
				// Still exists as a checkbox
				elem[0].checked = true;
			} else {
				// Must have been pushed off of the screen, check
				// if the post caught it
				elem = $("#hidden_" + selected[i]);
				if (elem.length == 0) {
					// Value was not posted and saved, create a hidden for it
					elem = document.createElement("input");
					elem.setAttribute("type", "hidden");
					elem.setAttribute("name", elemName);
					elem.setAttribute("value", values[i]);
					elem.setAttribute("id", "hidden_" + selected[i]);
	
					$("#" + formID).appendChild(elem);
				}
			}
		}
	}

	// Restore the scroll position
	$("div.frame")[0].scrollTop = scrollTop;
	$("div.frame")[0].scrollLeft = scrollLeft;

	// And get ready to refresh again in 5 seconds
	window.setTimeout(refreshPage, 5000);
}

// When the document has finished loading...
$(document).ready(function() {
	// Set up to refresh in 5 seconds
	window.setTimeout(refreshPage, 5000);
});

<%--
 % Edit a setting.
 % 
 % Jonathan Pearson
 % June 9, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="settings">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.settings" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/settings/breadcrumbs/edit.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/settings/leftnav/edit.ln.html" %>
							
							<div id="content">
								<s:actionerror/>
								
								<s:form action="update">
									<s:hidden name="id" />
									
									<tr>
										<th><s:text name="i18n.setting.header.name" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<s:if test="settingName == null">
										<tr>
											<td><s:textfield name="settingName" theme="simple" />:</td>
											<td><s:textfield name="settingValue" theme="simple" /></td>
										</tr>
									</s:if>
									<s:else>
										<s:hidden name="settingName" />
										<s:textfield label="%{settingName}" name="settingValue" />
									</s:else>
									
									<tr>
										<td><s:text name="i18n.setting.value.null" /></td>
										<td><s:checkbox name="makeNull" fieldValue="true" theme="simple" /></td>
									</tr>
								</s:form>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

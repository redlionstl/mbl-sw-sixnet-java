<%--
 % Requests confirmation, and then undeletes batches.
 % 
 % Jonathan Pearson
 % June 4, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="batches">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.batches" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
				
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/batches/breadcrumbs/undelete.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/batches/leftnav/undelete.ln.html" %>
							
							<div id="content">
								<s:actionerror/>
								
								<s:set var="formName" value="'realUndelete'" />
								<s:set var="actionName" value="'realUndelete'" />
								
								<s:set var="doSelections" value="true" />
								
								<%@ include file="../fragments/pagenav.html" %>
								
								<div class="frame">
									<s:form action="realUndelete">
										<s:hidden name="selection" value="keep" theme="simple" />
										
										<s:set var="includeHidden" value="true" />
										
										<s:set var="section" value="'batches'" />
										<s:set var="sectionLink" value="'Batches'" />
										<s:set var="itemID" value="'batchID'" />
										<s:set var="selItemID" value="'selBatchID'" />
										
										<%@ include file="../fragments/list.html" %>
									</s:form>
								</div>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

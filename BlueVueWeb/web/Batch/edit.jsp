<%--
 % Edit details of a single batch.
 % 
 % Jonathan Pearson
 % June 4, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="batches">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.batches" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/batches/breadcrumbs/edit.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/batches/leftnav/edit.ln.html" %>
							
							<div id="content">
								<s:actionerror />
								
								<s:form action="update">
									<tr>
										<th><s:text name="i18n.property" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<s:hidden name="id" />
									<s:textfield label="%{getText('i18n.batch.property.name')}" name="batch.name" />
									
									<tr>
										<td><s:text name="i18n.batch.property.creator" />:</td>
										<td>
											<s:if test="getBatchUser(batch) == null">
												<i><s:text name="i18n.emptyProp" /></i>
											</s:if>
											<s:else>
												<s:property value="%{getBatchUser(batch).name}" />
											</s:else>
										</td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.batch.property.deleted" />:</td>
										<td><s:checkbox name="batch.deleted" fieldValue="true" theme="simple" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.batch.property.jobCount" />:</td>
										<td><s:property value="batch.jobCount" /></td>
									</tr>
								</s:form>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>

			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

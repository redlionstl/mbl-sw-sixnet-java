<%--
 % Generate a CSS patch based on the screen resolution.
 % 
 % Jonathan Pearson
 % June 12, 2009
 %
 --%>

<%@ page language="java" contentType="text/css; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%
	out.clearBuffer();
	
	out.write("@CHARSET \"UTF-8\";\n\n");
	
	String sWidth = request.getParameter("width");
	String sHeight = request.getParameter("height");
	
	int width = 1024;
	int height = 768;
	
	if (sWidth != null) {
		try {
			width = Integer.parseInt(sWidth);
		} catch (NumberFormatException nfe) { }
	}
	
	if (sHeight != null) {
		try {
			height = Integer.parseInt(sHeight);
		} catch (NumberFormatException nfe) { }
	}
	
	// We need to place a minimum limit on these values
	if (height < 530) height = 530;
	if (width < 1024) width = 1024;
	
	out.write("#leftNav {\n");
	out.write(String.format("\theight: %dpx\n", height - 197));
	out.write("}\n\n");
	
	out.write("#content {\n");
	out.write(String.format("\theight: %dpx\n", height - 247));
	out.write("}\n\n");
	
	out.write("div.frame {\n");
	out.write(String.format("\tmax-height: %dpx\n", height - 259));
	out.write("}\n\n");
	
	out.flush();
%>

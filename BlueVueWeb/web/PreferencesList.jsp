<%--
 % Display user preferences, allowing for editing and changing values.
 % 
 % Jonathan Pearson
 % June 15, 2009
 %
 --%>
<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.sixnetio.BVB.Common.*"%>
<%@ page import="com.sixnetio.BVB.Web.*"%>

<!DOCTYPE html PUBLIC
    "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<s:i18n name="preferences">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <%@ include file="fragments/includes.html"%>
        <title><s:text name="i18n.preferences" /> - <%=Server.getServerSettings().getStringValue(Settings.Setting.Title)%></title>
        <script type="text/javascript">
            var linkOptions = new Object();
            <s:iterator value="availableColumns" var="col">
                linkOptions.<s:property value="key" /> = new Array();
                var index = 0;

                var link = new Object();
                link.title = '<s:text name="i18n.preferences.link.nolink" />';
                link.value = "";
                linkOptions.<s:property value="key" />[index++] = link;

                <s:iterator value="value.links">
                    link = new Object();
                    link.title = '<s:property value="value.title" />';
                    link.value = '<s:property value="value.value" />';

                    linkOptions.<s:property value="#col.key" />[index++] = link;
                </s:iterator>
            </s:iterator>

            function clearTag(tag) {
                while (tag.firstChild) tag.removeChild(tag.childNodes[0]);
            }

            function displayLinks() {
                // Copy out the old value and build up a new 'linkto' div
                var linkDiv = $("#linkto")[0];

                if (!linkDiv) return; // Nothing to do

                // Copy out the old value
                var hidden = null;
                for (var i = 0; i < linkDiv.childNodes.length; i++) {
                    var node = linkDiv.childNodes[i];

                    if (node.type == "radio" &&
                        node.checked) {

                        hidden = $("<input type='hidden' name='" + node.name +
                          "' id='" + node.name +
                          "' value='" + node.value +
                          "' />");

                        break;
                    }
                }

                clearTag(linkDiv);

                if (hidden != null) {
                    hidden.appendTo($("#update")[0]);
                }

                // Grab the selected column
                var list = $("#update_chosenColumns")[0];
                var colIndex = list.selectedIndex;
                if (colIndex == -1) {
                    // No selected item?
                    return;
                }

                var colID = list.options[colIndex].value;
                if (!colID) {
                    // Not found?
                    return;
                }

                var links = linkOptions[colID];
                if (!links) {
                    // No link options?
                    return;
                }

                // Grab the current value for the link
                hidden = $("#link_" + colID)[0];

                var chosenLink = null;
                if (hidden != null) {
                    chosenLink = hidden.value;

                    // Remove the hidden element, since we are creating a
                    // new set of radio buttons to replace it
                    hidden.parentNode.removeChild(hidden);
                }

                // Create radio buttons for each
                for (var i = 0; i < links.length; i++) {
                    // Build a new radio button to describe this link
                    $("<input type='radio' name='link_" + colID +
                      "' id='link_" + colID +
                      "' value='" + links[i].value + "'" +
                      (links[i].value == chosenLink ?
                          " checked='checked'" :
                          "") + " />").appendTo(linkDiv);

                    // Also, the description for the radio button and a newline
                    $("<span>" + links[i].title + "</span>").appendTo(linkDiv);
                    $("<br />").appendTo(linkDiv);
                }
            }

            function selectAndSubmit() {
                var list = $("#update_chosenColumns")[0];
                if (list != null) {
                    list.multiple = true; // So we can actually select multiple items
                    for (var i = 0; i < list.options.length; i++) {
                        list.options[i].selected = true;
                    }
                }

                var form = $("#update")[0];
                form.submit();
            }

            function loadHandler() {
                // If there is a column list, update the linkto div
                displayLinks();
            }
        </script>
    </head>
    <body onload="loadHandler();">
        <%@ include file="fragments/header.html"%>

            <div id="bodyArea">
                <div id="main">
                    <%@ include file="fragments/preferences/breadcrumbs/list.bc.html"%>
                    <div id="twocolumn">
                        <%@ include file="fragments/preferences/leftnav/list.ln.html"%>
                        <div id="content">
                            <s:actionerror />
                            <div class="frame">
                                <s:form action="Preferences/update" name="update" id="update">
                                    <s:hidden name="section" />
                                    <%-- Create hidden fields for each column link --%>
                                    <s:iterator value="availableColumns" var="col">
                                        <%-- Figure out what the chosen link is --%>
                                        <s:set var="chosen" value="chosenLinks.get(key)" />
                                        <s:if test="#chosen == null">
                                            <s:iterator value="value.links">
                                                <s:if test="value.dflt">
                                                    <s:set var="chosen" value="value.value" />
                                                </s:if>
                                            </s:iterator>
                                            <s:if test="#chosen == null">
                                                <s:set var="chosen" value="''" />
                                            </s:if>
                                        </s:if>
                                        <s:hidden id="link_%{key}" name="link_%{key}" value="%{#chosen}" />
                                    </s:iterator>
                                    <%-- Display the basic preferences --%>
                                    <s:iterator value="prefs">
                                        <tr>
                                            <td><s:property value="value.title" />:</td>
                                            <td><s:textfield name="pref_%{value.id}" value="%{value.value}" theme="simple" /></td>
                                        </tr>
                                    </s:iterator>
                                    <%-- Display the column chooser --%>
                                    <s:if test="availableColumns.size() > 0">
                                        <tr>
                                            <td colspan="2">
                                                <s:optiontransferselect
                                                    list="unchosenColumns"
                                                    doubleList="chosenColumns"

                                                    listValue="%{availableColumns.get(toString()).title}"
                                                    doubleListValue="%{availableColumns.get(toString()).title}"

                                                    multiple="true"
                                                    doubleMultiple="false"

                                                    name="unchosenColumns"
                                                    doubleName="chosenColumns"

                                                    size="12"
                                                    doubleSize="12"

                                                    leftTitle="%{getText('i18n.preferences.label.availableCols')}"
                                                    rightTitle="%{getText('i18n.preferences.label.chosenCols')}"

                                                    doubleEmptyOption="false"
                                                    doubleOnchange="displayLinks()"

                                                    allowAddAllToLeft="false"
                                                    allowAddAllToRight="false"

                                                    allowAddToLeft="true"
                                                    allowAddToRight="true"

                                                    allowUpDownOnLeft="false"
                                                    allowUpDownOnRight="true"
                                                    rightUpLabel="%{getText('i18n.preferences.label.up')}"
                                                    rightDownLabel="%{getText('i18n.preferences.label.down')}"

                                                    allowSelectAll="false"
                                                    theme="simple" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <s:text name="i18n.preferences.header.linkto" />
                                            </td>
                                            <td>
                                                <div id="linkto"></div>
                                            </td>
                                        </tr>
                                    </s:if>
                                </s:form>
                            </div>
                        </div>
                    </div>
                    <p id="clearBottom" />
                </div>
            </div>

        <%@ include file="fragments/footer.html"%>
    </body>
</s:i18n>
</html>

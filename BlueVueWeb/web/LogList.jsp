<%--
 % Displays a list of logs. Should be activated by LogListAction.
 % 
 % Jonathan Pearson
 % May 13, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="logs">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="fragments/includes.html" %>
			
			<title><s:text name="i18n.logs" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
			
			<script type="text/javascript">
				var formID = "Logs";
				var ajaxPage = "LogsX";
				var noCheckboxes = true;
			</script>
			
			<script type="text/javascript" src="js/refresher.js"></script>
		</head>
	</s:i18n>
	
	<body>
		<%@ include file="fragments/header.html" %>
		
			<div id="bodyArea">
				<div id="main">
					<s:i18n name="logs">
						<%@ include file="fragments/logs/breadcrumbs/list.bc.html" %>
					</s:i18n>
					
					<div id="twocolumn">
						<s:i18n name="logs">
							<%@ include file="fragments/logs/leftnav/list.ln.html" %>
						</s:i18n>
						
						<div id="content">
							<%@ include file="LogList.ajax.jsp" %>
						</div>
					</div>
					
					<p id="clearBottom" />
				</div>
			</div>
			
		<%@ include file="fragments/footer.html" %>
	</body>
</html>

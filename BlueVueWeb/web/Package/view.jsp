<%--
 % Display details of a single package.
 % 
 % Jonathan Pearson
 % June 5, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="packages">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.packages" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/packages/breadcrumbs/view.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/packages/leftnav/view.ln.html" %>
							
							<div id="content">
								<s:actionerror/>
								
								<table>
									<tr>
										<th><s:text name="i18n.property" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<tr>
										<td><s:text name="i18n.package.property.description" />:</td>
										<td><s:property value="package.type.description" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.package.property.type" />:</td>
										<td><s:property value="package.type.typeString" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.package.property.version" />:</td>
										<td><s:property value="package.version" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.package.property.date" />:</td>
										<td><s:property value="%{formatData('format.date', package.date)}" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.package.property.info" />:</td>
										<td><s:property value="package.info" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.package.property.checksum" />:</td>
										<td><s:property value="package.checksum" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.package.property.refcount" />:</td>
										<td><s:property value="package.refCount" /></td>
									</tr>
								</table>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

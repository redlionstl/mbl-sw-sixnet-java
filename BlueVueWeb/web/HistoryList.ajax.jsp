<%--
 % Returns only the history list, used by AJAX refreshing.
 % 
 % Jonathan Pearson
 % June 19, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<s:i18n name="history">
	<s:set var="formName" value="'History'" />
	<s:set var="actionName" value="'History'" />
	
	<s:set var="doPages" value="true" />
	<s:set var="doRefresher" value="true" />
	
	<%@ include file="fragments/pagenav.html" %>
	
	<div class="frame">
		<s:form id="%{formName}" name="%{formName}" action="%{actionName}">
			<!-- Add in all of the query fields, so pagination works properly -->
			<s:if test="qHistoryID != null"><s:hidden name="qHistoryID" theme="simple" /></s:if>
			<s:if test="qJobID != null"><s:hidden name="qJobID" theme="simple" /></s:if>
			<s:if test="qBatchID != null"><s:hidden name="qBatchID" theme="simple" /></s:if>
			<s:if test="qModemID != null"><s:hidden name="qModemID" theme="simple" /></s:if>
			<s:if test="qUserID != null"><s:hidden name="qUserID" theme="simple" /></s:if>
			<s:if test="qJobType != null"><s:hidden name="qJobType" theme="simple" /></s:if>
			<s:if test="qResult != null"><s:hidden name="qResult" theme="simple" /></s:if>
			<s:if test="qLowDate != null"><s:hidden name="qLowDate" theme="simple" /></s:if>
			<s:if test="qHighDate != null"><s:hidden name="qHighDate" theme="simple" /></s:if>
			
			<s:hidden name="selection" value="keep" theme="simple" />
			
			<s:set var="section" value="'history'" />
			<s:set var="sectionLink" value="'History'" />
			<s:set var="itemID" value="'historyID'" />
			<s:set var="selItemID" value="'selHistoryID'" />
			
			<%@ include file="fragments/list.html" %>
		</s:form>
	</div>
</s:i18n>

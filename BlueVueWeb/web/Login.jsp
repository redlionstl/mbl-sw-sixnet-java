<%--
 % Provides a login page.
 % 
 % Jonathan Pearson
 % April 15, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="login">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="fragments/includes.html" %>
			
			<title><s:text name="i18n.login" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="fragments/header.html" %>

				<div id="bodyArea">
					<div id="main">
						<%@ include file="fragments/breadcrumbs/login.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="fragments/leftnav/login.ln.html" %>
							
							<div id="content">
								<s:form action="Login">
									<s:actionerror />
									
									<s:textfield label="%{getText('i18n.login.property.userName')}" name="userName" />
									<s:password label="%{getText('i18n.login.property.password')}" name="password"  />
									<s:submit />
								</s:form>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

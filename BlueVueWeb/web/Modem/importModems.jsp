<%--
 % Upload a CSV file containing modem definitions.
 % 
 % Jonathan Pearson
 % June 18, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="modems">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.modems" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/modems/breadcrumbs/importModems.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/modems/leftnav/importModems.ln.html" %>
							
							<div id="content">
								<s:actionerror />
								
								<s:form action="realImportModems" method="post" enctype="multipart/form-data">
									<tr>
										<th><s:text name="i18n.property" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<s:file name="csvFile" label="%{getText('i18n.modem.property.csvFile')}" />
									
									<s:select list="allTags" name="tagID"
									          listKey="tagID" listValue="name"
									          label="%{getText('i18n.modem.property.applyTag')}"
									          headerKey="-1"
									          headerValue="%{getText('i18n.modem.header.selectTag')}" />
									
									<tr>
										<td class="tdLabel"><label class="label"><s:text name="i18n.modem.property.createJobs" /></label></td>
										<td><s:checkbox name="createJobs" fieldValue="true" value="true" theme="simple" /></td>
									</tr>
									
									<s:textfield label="%{getText('i18n.modem.property.batchName')}" name="jobBatchName" />
								</s:form>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>

			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

<%--
 % Edit details of a single modem.
 % 
 % Jonathan Pearson
 % April 28, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="modems">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.modems" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/modems/breadcrumbs/edit.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/modems/leftnav/edit.ln.html" %>
							
							<div id="content">
								<s:actionerror />
								
								<s:form action="update">
									<tr>
										<th><s:text name="i18n.property" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<s:hidden name="id" />
									
									<tr>
										<td><s:text name="i18n.modem.property.alias" />:</td>
										<td><s:property value="modem.alias" /></td>
									</tr>
									
									<s:textfield label="%{getText('i18n.modem.property.devID')}" name="modem.deviceID" />
									
									<tr>
										<td><s:text name="i18n.modem.property.model" />:</td>
										<td><s:property value="modem.model" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.modem.property.firmwareRev" />:</td>
										<td><s:property value="modem.fwVersion" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.modem.property.cfgVersion" />:</td>
										<td><s:property value="modem.cfgVersion" /></td>
									</tr>
									
									<s:textfield label="%{getText('i18n.modem.property.ipAddress')}"   name="modem.ipAddress" />
									<s:textfield label="%{getText('i18n.modem.property.port')}"        name="modem.port" />
									
									<tr>
										<td><s:text name="i18n.modem.property.phone" />:</td>
										<td><s:property value="modem.phoneNumber" /></td>
									</tr>
									
									<tr>
										<td class="tdLabel"><s:text name="i18n.modem.property.serial" />:</td>
										<td><s:property value="%{modem.getProperty('serial')}" /></td>
									</tr>
									
									<s:if test="id == -1">
										<%-- Special options for a new modem --%>
										<s:select list="%{getAllTags()}"
										          listKey="tagID"
										          listValue="name"
										          headerKey="-1"
										          headerValue="%{getText('i18n.emptyProp')}"
										          label="%{getText('i18n.modem.property.applyTag')}"
										          name="applyTag" />
										
										<tr>
											<td class="tdLabel"><s:text name="i18n.modem.property.createJob" />:</td>
											<td><s:checkbox name="scheduleJob" fieldValue="true" theme="simple" /></td>
										</tr>
									</s:if>
									
									<tr>
										<th class="separator" colspan="2"><s:text name="i18n.modem.header.passwordSettings" /></th>
									</tr>
									
									<tr>
										<td><s:text name="i18n.modem.property.passwordEnabled" /></td>
										<td><s:checkbox name="passwordSettings.passwordEnabled" fieldValue="true" theme="simple" /></td>
									</tr>
									
									<s:select list="%{getApplicableInterfaces()}"
									          label="%{getText('i18n.modem.property.applicableInterfaces')}"
									          name="passwordSettings.applicableInterfaces" />
									
									<s:textfield label="%{getText('i18n.modem.property.password')}" name="passwordSettings.password" />
								</s:form>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>

			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

<%--
 % Requests confirmation, and then tags modems.
 % 
 % Jonathan Pearson
 % May 1, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="modems">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.modems" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
				
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/modems/breadcrumbs/tag.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/modems/leftnav/tag.ln.html" %>
							
							<div id="content">
								<s:set var="formName" value="'realTag'" />
								<s:set var="actionName" value="'tag'" />
								
								<s:set var="doPages" value="true" />
								<s:set var="doSelections" value="true" />
								
								<%@ include file="../fragments/pagenav.html" %>
								
								<div class="frame">
									<s:actionerror />
									
									<s:form action="realTag">
										<s:hidden name="selection" value="keep" theme="simple" />
										
										<tr>
											<td><s:text name="i18n.modem.header.addTag" /></td>
											<td>
												<s:select list="allTags"
												          name="tagID"
												          listKey="tagID"
												          listValue="name"
												          headerKey="-1"
												          headerValue="%{getText('i18n.modem.header.selectTag')}"
												          theme="simple" />
											</td>
										</tr>
										
										<s:set var="includeHidden" value="true" />
										
										<s:set var="section" value="'modems'" />
										<s:set var="sectionLink" value="'Modems'" />
										<s:set var="itemID" value="'modemID'" />
										<s:set var="selItemID" value="'selModemID'" />
										
										<%@ include file="../fragments/list.html" %>
									</s:form>
								</div>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

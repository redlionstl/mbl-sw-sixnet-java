<%--
 % Display a modem's user-defined variables.
 % 
 % Jonathan Pearson
 % June 8, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="modems">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.modems" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/modems/breadcrumbs/editVar.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/modems/leftnav/editVar.ln.html" %>
							
							<div id="content">
								<s:actionerror/>
								
								<s:form action="updateVariable">
									<s:hidden name="id" />
									
									<tr>
										<th><s:text name="i18n.modem.header.variable" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<s:if test="variableName == null">
										<tr>
											<td><s:textfield name="variableName" theme="simple" />:</td>
											<td><s:textfield name="variableValue" theme="simple" /></td>
										</tr>
									</s:if>
									<s:else>
										<s:hidden name="variableName" />
										<s:textfield label="%{variableName}" name="variableValue" />
									</s:else>
								</s:form>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

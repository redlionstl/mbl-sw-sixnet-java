<%--
 % Display details of a single modem.
 % 
 % Jonathan Pearson
 % April 27, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="modems">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.modems" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/modems/breadcrumbs/view.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/modems/leftnav/view.ln.html" %>
							
							<div id="content">
								<s:actionerror/>
								
								<div class="frame">
									<table>
										<tr>
											<th><s:text name="i18n.property" /></th>
											<th><s:text name="i18n.value" /></th>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.alias" />:</td>
											<td><s:property value="modem.alias" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.devID" />:</td>
											<td><s:property value="modem.niceDeviceID" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.model" />:</td>
											<td><s:property value="modem.model" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.firmwareRev" />:</td>
											<td><s:property value="modem.fwVersion" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.cfgVersion" />:</td>
											<td><s:property value="modem.cfgVersion" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.ipAddress" />:</td>
											<td><s:property value="modem.ipAddress" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.port" />:</td>
											<td><s:property value="modem.port" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.phone" />:</td>
											<td><s:property value="modem.phoneNumber" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.serial" />:</td>
											<td><s:property value="%{modem.getProperty('serial')}" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.appliedTags" />:</td>
											<td>
												<s:iterator value="appliedTags" status="rowstatus">
													<s:if test="!#rowstatus.isFirst()">,</s:if>
													<s:a href="%{documentRoot + '/Tag/view?id=' + tagID}">
														<s:property value="name" />
													</s:a>
												</s:iterator>
											</td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.modem.property.configScript" />:</td>
											
											<td>
												<s:if test="configScript == null">
													<s:if test="hasPermission('Enhanced')">
														<s:a href="editConfig?id=%{id}">
															<s:text name="i18n.modem.property.createConfigScript" />
														</s:a>
													</s:if>
													<s:else>
														<s:text name="i18n.modem.property.noConfigScript" />
													</s:else>
												</s:if>
												<s:else>
													<s:a href="viewConfig?id=%{id}">
														<s:text name="i18n.modem.property.viewConfigScript" />
													</s:a>
												</s:else>
											</td>
										</tr>
										
										<s:if test="hasPermission('Enhanced')">
											<tr>
												<th class="separator" colspan="2"><s:text name="i18n.modem.header.passwordSettings" /></th>
											</tr>
											
											<tr>
												<td><s:text name="i18n.modem.property.passwordEnabled" /></td>
												<td><s:property value="passwordSettings.passwordEnabled" /></td>
											</tr>
											
											<tr>
												<td><s:text name="i18n.modem.property.applicableInterfaces" /></td>
												<td><s:property value="passwordSettings.applicableInterfaces" /></td>
											</tr>
											
											<tr>
												<td><s:text name="i18n.modem.property.password" /></td>
												<td><s:property value="passwordSettings.password" /></td>
											</tr>
										</s:if>
										
										<tr>
											<th class="separator" colspan="2"><s:text name="i18n.modem.header.componentVersions" /></th>
										</tr>
										
										<s:iterator value="%{modem.getProperties('versions', null)}" var="props">
											<tr>
												<td><s:property value="#props[0]" />:</td>
												<td><s:property value="#props[1]" /></td>
											</tr>
										</s:iterator>
										
										<s:if test="getRecentContactDate(modem) != null">
											<tr>
												<th class="separator" colspan="2">
													<s:text name="i18n.modem.header.recentStatus">
														<s:param><s:property value="%{formatData('format.datetime', getRecentContactDate(modem))}" /></s:param>
													</s:text>
												</th>
											</tr>
											
											<s:iterator value="%{getRecentStatus(modem)}">
												<tr>
													<td><s:property value="key" /></td>
													<td><s:property value="value" escape="false" /></td>
												</tr>
											</s:iterator>
										</s:if>
									</table>
								</div>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

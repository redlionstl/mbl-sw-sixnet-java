<%--
 % Displays a list of tags. Should be activated by TagListAction.
 % 
 % Jonathan Pearson
 % May 12, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="tags">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="fragments/includes.html" %>
			
			<title><s:text name="i18n.tags" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
			
			<script type="text/javascript">
				var formID = "Tags";
				var ajaxPage = "TagsX";
				var elemName = "Tags_tagID";
			</script>
			
			<script type="text/javascript" src="js/refresher.js"></script>
		</head>
	</s:i18n>
	
	<body>
		<%@ include file="fragments/header.html" %>
		
			<div id="bodyArea">
				<div id="main">
					<s:i18n name="tags">
						<%@ include file="fragments/tags/breadcrumbs/list.bc.html" %>
					</s:i18n>
					
					<div id="twocolumn">
						<s:i18n name="tags">
							<%@ include file="fragments/tags/leftnav/list.ln.html" %>
						</s:i18n>
						
						<div id="content">
							<%@ include file="TagList.ajax.jsp" %>
						</div>
					</div>
					
					<p id="clearBottom" />
				</div>
			</div>
			
		<%@ include file="fragments/footer.html" %>
	</body>
</html>

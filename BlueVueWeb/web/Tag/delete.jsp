<%--
 % Requests confirmation, and then deletes tags.
 % 
 % Jonathan Pearson
 % June 5, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="tags">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.tags" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
				
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/tags/breadcrumbs/delete.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/tags/leftnav/delete.ln.html" %>
							
							<div id="content">
								<s:actionerror />
								
								<s:set var="formName" value="'realDelete'" />
								<s:set var="actionName" value="'delete'" />
								
								<s:set var="doSelections" value="true" />
								
								<%@ include file="../fragments/pagenav.html" %>
								
								<div class="frame">
									<s:form action="realDelete">
										<s:hidden name="selection" value="keep" theme="simple" />
										
										<s:set var="includeHidden" value="true" />
										
										<s:set var="section" value="'tags'" />
										<s:set var="sectionLink" value="'Tags'" />
										<s:set var="itemID" value="'tagID'" />
										<s:set var="selItemID" value="'selTagID'" />
										
										<%@ include file="../fragments/list.html" %>
									</s:form>
								</div>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

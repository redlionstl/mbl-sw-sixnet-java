<%--
 % Display and allow editing of details of a single tag.
 % 
 % Jonathan Pearson
 % June 5, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="tags">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.tags" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/tags/breadcrumbs/edit.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/tags/leftnav/edit.ln.html" %>
							
							<div id="content">
								<s:actionerror />
								
								<s:form action="update">
									<tr>
										<th><s:text name="i18n.property" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<s:hidden name="id" />
									<s:textfield label="%{getText('i18n.tag.property.name')}" name="tag.name" />
									<s:textfield label="%{getText('i18n.tag.property.position')}" name="tag.position" />
									<s:textfield label="%{getText('i18n.tag.property.pollingIntervalMins')}" name="tag.pollingIntervalMinutes" />
									
									<tr>
										<td><s:text name="i18n.tag.property.hasConfigScript" />:</td>
										<td><s:property value="%{tag.configScript != null}" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.tag.property.modemCount" /></td>
										<td><s:property value="tag.modemCount" /></td>
									</tr>
								</s:form>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

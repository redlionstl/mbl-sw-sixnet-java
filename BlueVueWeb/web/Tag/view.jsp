<%--
 % Display details of a single tag.
 % 
 % Jonathan Pearson
 % June 5, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="tags">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.tags" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/tags/breadcrumbs/view.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/tags/leftnav/view.ln.html" %>
							
							<div id="content">
								<table>
									<tr>
										<th><s:text name="i18n.property" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<tr>
										<td><s:text name="i18n.tag.property.name" />:</td>
										<td><s:property value="tag.name" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.tag.property.position" />:</td>
										<td><s:property value="tag.position" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.tag.property.pollingIntervalMins" />:</td>
										<td><s:property value="tag.pollingIntervalMinutes" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.tag.property.configScript" />:</td>
										
										<td>
											<s:if test="tag.configScript == null">
												<s:if test="hasPermission('Enhanced')">
													<s:a href="editConfig?id=%{id}">
														<s:text name="i18n.tag.property.createConfigScript" />
													</s:a>
												</s:if>
												<s:else>
													<s:text name="i18n.tag.property.noConfigScript" />
												</s:else>
											</s:if>
											<s:else>
												<s:a href="viewConfig?id=%{id}">
													<s:text name="i18n.tag.property.viewConfigScript" />
												</s:a>
											</s:else>
										</td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.tag.property.modemCount" /></td>
										<td>
											<s:a href="%{documentRoot + '/Modems?qTagID=' + id}">
												<s:property value="tag.modemCount" />
											</s:a>
										</td>
									</tr>
								</table>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

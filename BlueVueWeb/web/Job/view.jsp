<%--
 % Display details of a single job.
 % 
 % Jonathan Pearson
 % May 4, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="jobs">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.jobs" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/jobs/breadcrumbs/view.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/jobs/leftnav/view.ln.html" %>
							
							<div id="content">
								<s:if test="job == null">
									<s:text name="i18n.job.message.jobFinished">
										<s:param><s:property value="id" /></s:param>
									</s:text>
								</s:if>
								<s:else>
									<table>
										<tr>
											<th><s:text name="i18n.property" /></th>
											<th><s:text name="i18n.value" /></th>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.jobID" />:</td>
											<td><s:property value="job.jobData.jobID" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.batch" />:</td>
											<td>
												<s:if test="getJobBatch(job) == null">
													<i><s:text name="i18n.emptyProp" /></i>
												</s:if>
												<s:else>
													<s:a href="%{documentRoot + '/Batch/view?id=' + getJobBatch(job).batchID}">
														<s:property value="%{getJobBatch(job).name}" />
													</s:a>
												</s:else>
											</td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.modem" />:</td>
											<td>
												<s:a href="%{documentRoot + '/Modem/view?id=' + getJobModem(job).modemID}">
													<s:property value="%{getJobModem(job).label}" />
												</s:a>
											</td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.user" />:</td>
											<td>
												<s:if test="getJobUser(job) == null">
													<i><s:text name="i18n.emptyProp" /></i>
												</s:if>
												<s:else>
													<s:a href="%{documentRoot + '/User/view?id=' + getJobUser(job).userID}">
														<s:property value="%{getJobUser(job).name}" />
													</s:a>
												</s:else>
											</td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.schedule" />:</td>
											<td><s:property value="%{formatData('format.datetime', job.jobData.schedule)}" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.tries" />:</td>
											<td><s:property value="job.jobData.tries" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.type" />:</td>
											<td><s:property value="job.jobData.type" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.automatic" />:</td>
											<td><s:property value="job.jobData.automatic" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.mobileOriginated" />:</td>
											<td><s:property value="mobileOriginated" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.alwaysAvailable" />:</td>
											<td><s:property value="alwaysAvailable" /></td>
										</tr>
										
										<tr>
											<th class="separator" colspan="2"><s:text name="i18n.job.header.extraProperties" /></th>
										</tr>
										
										<s:iterator value="extraProperties">
											<tr>
												<td><s:property value="key" /></td>
												<td><s:property value="value" /></td>
											</tr>
										</s:iterator>
									</table>
								</s:else>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

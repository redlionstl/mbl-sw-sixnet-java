<%--
 % Display and allow editing of details of a single job.
 % 
 % Jonathan Pearson
 % May 4, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="jobs">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.jobs" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/jobs/breadcrumbs/edit.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/jobs/leftnav/edit.ln.html" %>
							
							<div id="content">
								<s:actionerror />
								
								<s:if test="job == null">
									<s:text name="i18n.job.message.jobFinished">
										<s:param><s:property value="id" /></s:param>
									</s:text>
								</s:if>
								<s:else>
									<s:form action="update">
										<tr>
											<th><s:text name="i18n.property" /></th>
											<th><s:text name="i18n.value" /></th>
										</tr>
										
										<s:hidden name="id" />
										<tr>
											<td><s:text name="i18n.job.property.jobID" />:</td>
											<td><s:property value="job.jobData.jobID" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.batch" />:</td>
											<td><s:property value="%{getJobBatch(job).name}" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.modem" />:</td>
											<td><s:property value="%{getJobModem(job).label}" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.user" />:</td>
											<td><s:property value="%{getJobUser(job).name}" /></td>
										</tr>
										
										<s:textfield label="%{getText('i18n.job.property.schedule')}" name="job.jobData.schedule" value="%{formatData('format.datetime', job.jobData.schedule)}" />
										
										<tr>
											<td><s:text name="i18n.job.property.tries" />:</td>
											<td><s:property value="job.jobData.tries" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.type" />:</td>
											<td><s:property value="job.jobData.type" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.automatic" />:</td>
											<td><s:checkbox name="job.jobData.automatic" fieldValue="true" theme="simple" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.mobileOriginated" />:</td>
											<td><s:checkbox name="mobileOriginated" id="mobileOriginated" fieldValue="true" theme="simple" onclick="mobileOriginatedChanged()" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.alwaysAvailable" />:</td>
											<td><s:checkbox name="alwaysAvailable" id="alwaysAvailable" fieldValue="true" theme="simple" /></td>
										</tr>

										<script type="text/javascript">
											function mobileOriginatedChanged() {
												$("#alwaysAvailable")[0].disabled = ! $("#mobileOriginated")[0].checked;
											}

											// Make sure the state is up-to-date at load
											mobileOriginatedChanged();
										</script>
									</s:form>
								</s:else>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

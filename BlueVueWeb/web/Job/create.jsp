<%--
 % Allow creation of multiple similar jobs.
 % 
 % Jonathan Pearson
 % May 4, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="jobs">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			<script type="text/javascript" src="<s:property value='documentRoot' />/js/datejs/date<s:property value='localeName' />.js"></script>
			
			<title><s:text name="i18n.jobs" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/jobs/breadcrumbs/create.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/jobs/leftnav/create.ln.html" %>
							
							<div id="content">
								<div class="frame">
									<s:actionerror />
									
									<s:form action="realCreate">
										<tr>
											<th><s:text name="i18n.property" /></th>
											<th><s:text name="i18n.value" /></th>
										</tr>
										
										<s:select label="%{getText('i18n.job.property.type')}"
										          list="availableJobTypes"
										          headerKey=""
										          headerValue=""
										          name="jobType" />
										
										<tr>
											<td><s:text name="i18n.job.property.schedule" /></td>
											<td>
												<%-- Need to take the user's timezone into account for proper display.
												     To do this, the schedule is passed in from the server through the 'schedule
												     hidden field. The javascript below will parse it into separate date/time
												     values and populate text fields for the user to edit. The 'scheduleAlt'
												     hidden makes the JQuery DatePicker widget work more reliably, as we are
												     displaying the month name in the user-visible control, but just numbers in
												     the hidden field. When the user controls change, the values are parsed
												     and reformatted back into a single date/time value in UTC that the server
												     can read, or "" if we can't parse it. --%>
												<s:hidden name="schedule" id="schedule" />
												<s:hidden name="scheduleAlt" id="scheduleAlt" />
												
												<s:textfield theme="simple"
												             name="scheduleDate"
												             id="scheduleDate" />
												<s:textfield theme="simple"
												             name="scheduleTime"
												             id="scheduleTime" />
											</td>
										</tr>
										
										<script type="text/javascript">
											// Split the schedule into date/time
											// Format will be the 'standard' format (ISO 8601 separate date/time without
											//   timezone, assumed to be UTC)
											var datetime = Date.parse($("#schedule")[0].value + " UTC");

											// Populate the separate fields
											$("#scheduleDate")[0].value = datetime.toString("MMMM d, yyyy");
											$("#scheduleTime")[0].value = datetime.toLocaleTimeString();

											$("#scheduleAlt")[0].value = datetime.getFullYear() + "-" + datetime.getMonth() + "-" + datetime.getDate();

											if (navigator.appVersion.indexOf("MSIE 7.") == -1) {
												// Not IE7 (this doesn't work for that browser)
												$("#scheduleDate").datepicker({
													altField: "scheduleAlt",
													altFormat: "yy-mm-dd",
													dateFormat: "MM d, yy",
													monthNames: Date.CultureInfo.monthNames,
													monthNamesShort: Date.CultureInfo.abbreviatedMonthNames
												});
											}
											
											// When the user changes one of the values, refresh the 'schedule' hidden field
											$("#scheduleDate, #scheduleTime").bind("change", function() {
												// Put the schedule back together in UTC time
												// Use this to populate the 'schedule' field
												var datetime = Date.parse($("#scheduleDate")[0].value + " " + $("#scheduleTime")[0].value);
												if (datetime == null) {
													// Unable to parse? Leave the field blank; if the user submits, an error
													// will come back along with the current date/time
													$("#schedule")[0].value = "";
												} else {
													// Parsed properly, update the field with UTC time
													// Watch out for getUTCMonth(), though, it returns 0-11 when we want 1-12
													$("#schedule")[0].value = datetime.getUTCFullYear() + "-" + (datetime.getUTCMonth() + 1) + "-" + datetime.getUTCDate() + " " +
													                          datetime.getUTCHours() + ":" + datetime.getUTCMinutes() + ":" + datetime.getUTCSeconds();

													// In case we ever let the user manually edit the date field, this will show the
													// change in the DatePicker widget
													$("#scheduleAlt")[0].value = datetime.toString("yyyy-mm-dd");
												}
											});
										</script>
										
										<tr>
											<td class="tdLabel"><s:text name="i18n.job.property.automatic" /></td>
											<td><s:checkbox name="automatic" fieldValue="true" theme="simple" /></td>
										</tr>
										
										<tr>
											<td class="tdLabel"><s:text name="i18n.job.property.mobileOriginated" /></td>
											<td><s:checkbox name="mobileOriginated" id="mobileOriginated" fieldValue="true" theme="simple" onclick="mobileOriginatedChanged()" /></td>
										</tr>
										
										<tr>
											<td><s:text name="i18n.job.property.alwaysAvailable" />:</td>
											<td><s:checkbox name="alwaysAvailable" id="alwaysAvailable" fieldValue="true" theme="simple" /></td>
										</tr>

										<script type="text/javascript">
											function mobileOriginatedChanged() {
												$("#alwaysAvailable")[0].disabled = ! $("#mobileOriginated")[0].checked;
											}

											// Make sure the state is up-to-date at load
											mobileOriginatedChanged();
										</script>
										
										<s:select label="%{getText('i18n.job.property.package')}"
										          list="availablePackages"
										          name="packageHash" />
										
										<tr>
											<td class="tdLabel"><s:text name="i18n.job.property.force" /></td>
											<td><s:checkbox name="force" theme="simple" /></td>
										</tr>
										
										<s:textfield label="%{getText('i18n.job.property.batch')}" name="batchName" />
										
										<tr>
											<th class="separator" colspan="2"><s:text name="i18n.job.header.applyTo" /></th>
										</tr>
										
										<s:i18n name="modems">
											<tr>
												<td colspan="2">
													<s:set var="formName" value="'realCreate'" />
													<s:set var="actionName" value="'create'" />
													
													<s:set var="doSelections" value="true" />
													<s:set var="doPages" value="true" />
													
													<%@ include file="../fragments/pagenav.html" %>
													
													<table>
														<s:hidden name="selection" value="keep" />
														
														<s:set var="includeHidden" value="true" />
														
														<s:set var="section" value="'modems'" />
														<s:set var="sectionLink" value="'Modems'" />
														<s:set var="itemID" value="'modemID'" />
														<s:set var="selItemID" value="'selModemID'" />
														<s:set var="items" value="items" />
														
														<%@ include file="../fragments/list.html" %>
													</table>
												</td>
											</tr>
										</s:i18n>
									</s:form>
								</div>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

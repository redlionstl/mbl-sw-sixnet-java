<%--
 % Return only the list of servers, used by AJAX refreshing.
 % 
 % Jonathan Pearson
 % June 19, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<s:i18n name="servers">
	<s:if test="shutdownOrders.equalsIgnoreCase('ALL')">
		<p>
			<s:text name="i18n.server.info.allServerOrder" />
		</p>
	</s:if>
	
	<div class="frame">
		<s:actionerror/>
		
		<table>
			<s:set var="drawShutdown" value="true" />
			
			<%@ include file="fragments/servers/serverList.html" %>
		</table>
	</div>
</s:i18n>

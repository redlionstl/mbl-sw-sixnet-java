<%--
 % Returns only the list of modems, used by AJAX refreshing.
 % 
 % Jonathan Pearson
 % June 18, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<s:i18n name="modems">
	<s:set var="formName" value="'Modems'" />
	<s:set var="actionName" value="'Modems'" />
	
	<s:set var="doPages" value="true" />
	<s:set var="doSelections" value="true" />
	<s:set var="doRefresher" value="true" />
	
	<%@ include file="fragments/pagenav.html" %>
	
	<div class="frame">
		<s:actionerror />
		
		<s:if test="createJobs">
			<p>
				<s:text name="i18n.modem.hint.createJobs" />
			</p>
		</s:if>
		
		<s:form id="%{formName}" name="%{formName}" action="%{actionName}">
			<!-- Add in all of the query fields, so pagination works properly -->
			<s:if test="qModemID != null"><s:hidden name="qModemID" /></s:if>
			<s:if test="qDeviceID != null"><s:hidden name="qDeviceID" /></s:if>
			<s:if test="qAlias != null"><s:hidden name="qAlias" /></s:if>
			<s:if test="qModel != null"><s:hidden name="qModel" /></s:if>
			<s:if test="qFWVersion != null"><s:hidden name="qFWVersion" /></s:if>
			<s:if test="qCfgVersion != null"><s:hidden name="qCfgVersion" /></s:if>
			<s:if test="qIPAddress != null"><s:hidden name="qIPAddress" /></s:if>
			<s:if test="qPort != null"><s:hidden name="qPort" /></s:if>
			<s:if test="qPhone != null"><s:hidden name="qPhone" /></s:if>
			<s:if test="qTagID != null"><s:hidden name="qTagID" /></s:if>
			
			<s:hidden name="createJobs" />
			
			<s:hidden name="selection" value="keep" />
			
			<s:set var="section" value="'modems'" />
			<s:set var="sectionLink" value="'Modems'" />
			<s:set var="itemID" value="'modemID'" />
			<s:set var="selItemID" value="'selModemID'" />
			
			<%@ include file="fragments/list.html" %>
		</s:form>
	</div>
</s:i18n>

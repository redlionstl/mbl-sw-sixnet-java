<%--
 % Allow enabling of a single user by setting the password.
 % 
 % Jonathan Pearson
 % June 2, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="users">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.users" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/users/breadcrumbs/enable.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/users/leftnav/enable.ln.html" %>
							
							<div id="content">
								<s:actionerror />
								
								<s:form action="realEnable">
									<tr>
										<th><s:text name="i18n.property" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<s:hidden name="id" />
									
									<tr>
										<td><s:text name="i18n.user.property.name" /></td>
										<td><s:property value="targetUser.name" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.user.property.type" /></td>
										<td><s:property value="targetUser.type" /></td>
									</tr>
									
									<s:password label="%{getText('i18n.user.property.password')}" name="newPassword" />
									<s:password label="%{getText('i18n.user.property.confirmPassword')}" name="confirm" />
								</s:form>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>

			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

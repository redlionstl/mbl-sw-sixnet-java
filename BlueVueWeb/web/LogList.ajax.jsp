<%--
 % Returns only the log list, used for AJAX refreshing.
 % 
 % Jonathan Pearson
 % June 19, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<s:i18n name="logs">
	<s:set var="formName" value="'Logs'" />
	<s:set var="actionName" value="'Logs'" />
	
	<s:set var="doPages" value="true" />
	<s:set var="doRefresher" value="true" />
	
	<%@ include file="fragments/pagenav.html" %>
	
	<div class="frame">
		<s:actionerror />
		
		<s:form id="%{formName}" name="%{formName}" action="%{actionName}">
			<!-- Add in all of the query fields, so pagination works properly -->
			<s:if test="qLogID != null"><s:hidden name="qLogID" theme="simple" /></s:if>
			<s:if test="qLowDate != null"><s:hidden name="qLowDate" theme="simple" /></s:if>
			<s:if test="qHighDate != null"><s:hidden name="qHighDate" theme="simple" /></s:if>
			<s:if test="qUserID != null"><s:hidden name="qUserID" theme="simple" /></s:if>
			<s:if test="qLogType != null"><s:hidden name="qLogType" theme="simple" /></s:if>
			
			<s:hidden name="selection" value="keep" theme="simple" />
			
			<s:set var="section" value="'logs'" />
			<s:set var="sectionLink" value="'Logs'" />
			<s:set var="itemID" value="'logID'" />
			<s:set var="selItemID" value="'selLogID'" />
			
			<%@ include file="fragments/list.html" %>
		</s:form>
	</div>
</s:i18n>

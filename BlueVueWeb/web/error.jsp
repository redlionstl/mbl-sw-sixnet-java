<%--
 % Displays action error messages
 % 
 % Jonathan Pearson
 % May 1, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="errors">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="fragments/includes.html" %>
			
			<title><s:text name="i18n.error" /></title>
		</head>
		
		<body>
			<%@ include file="fragments/header.html" %>
				
				<div id="bodyArea">
					<div id="main">
						<%@ include file="fragments/breadcrumbs/error.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="fragments/leftnav/error.ln.html" %>
							
							<div id="content">
								<ul class="errorMessage">
									<s:if test="exception.message != null">
										<li><span><s:property value="exception.message" /></span></li>
									</s:if>
								
									<s:iterator value="actionErrors">
										<li><span><s:property /></span></li>
									</s:iterator>
								</ul>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

<%--
 % Display details of a single history entry.
 % 
 % Jonathan Pearson
 % June 4, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="history">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.history" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/history/breadcrumbs/view.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/history/leftnav/view.ln.html" %>
							
							<div id="content">
								<s:actionerror/>
								
								<table>
									<tr>
										<th><s:text name="i18n.property" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<tr>
										<td><s:text name="i18n.history.property.historyID" />:</td>
										<td><s:property value="entry.historyID" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.history.property.job" />:</td>
										<td>
											<s:if test="getHistoryJob(entry) == null">
												<i><s:property value="%{formatData('format.number', entry.jobID)}" /></i>
											</s:if>
											<s:else>
												<s:a href="%{documentRoot + '/Job/view?id=' + getHistoryJob(entry).jobData.jobID}">
													<s:property value="%{getHistoryJob(entry).jobData.jobID}" />
												</s:a>
											</s:else>
										</td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.history.property.batch" />:</td>
										<td>
											<s:if test="getHistoryBatch(entry) == null">
												<i><s:text name="i18n.emptyProp" /></i>
											</s:if>
											<s:else>
												<s:a href="%{documentRoot + '/Batch/view?id=' + getHistoryBatch(entry).batchID}">
													<s:property value="%{getHistoryBatch(entry).name}" />
												</s:a>
											</s:else>
										</td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.history.property.modem" />:</td>
										<td>
											<s:a href="%{documentRoot + '/Modem/view?id=' + getHistoryModem(entry).modemID}">
												<s:property value="%{getHistoryModem(entry).label}" />
											</s:a>
										</td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.history.property.user" />:</td>
										<td>
											<s:if test="getHistoryUser(entry) == null">
												<i><s:text name="i18n.emptyProp" /></i>
											</s:if>
											<s:else>
												<s:a href="%{documentRoot + '/User/view?id=' + getHistoryUser(entry).id}">
													<s:property value="%{getHistoryUser(entry).name}" />
												</s:a>
											</s:else>
										</td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.history.property.timestamp" />:</td>
										<td><s:property value="%{formatData('format.datetime', entry.timestamp)}" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.history.property.result" />:</td>
										<td><s:property value="entry.result" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.history.property.type" />:</td>
										<td><s:property value="entry.type" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.history.property.message" />:</td>
										<td><s:property value="entry.message" /></td>
									</tr>
								</table>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

<%--
 % Returns only the list of batches, used by AJAX refreshing.
 % 
 % Jonathan Pearson
 % June 19, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<s:i18n name="batches">
	<s:set var="formName" value="'Batches'" />
	<s:set var="actionName" value="'Batches'" />
	
	<s:set var="doPages" value="true" />
	<s:set var="doSelections" value="true" />
	<s:set var="doRefresher" value="true" />
	
	<%@ include file="fragments/pagenav.html" %>
	
	<div class="frame">
		<s:form id="%{formName}" name="%{formName}" action="%{actionName}">
			<!-- Add in all of the query fields, so pagination works properly -->
			<s:if test="qBatchID != null"><s:hidden name="qBatchID" theme="simple" /></s:if>
			<s:if test="qUserID != null"><s:hidden name="qUserID" theme="simple" /></s:if>
			<s:if test="qName != null"><s:hidden name="qName" theme="simple" /></s:if>
			<s:if test="qDeleted != null"><s:hidden name="qDeleted" theme="simple" /></s:if>
			
			<s:hidden name="selection" value="keep" theme="simple" />
			
			<s:set var="section" value="'batches'" />
			<s:set var="sectionLink" value="'Batches'" />
			<s:set var="itemID" value="'batchID'" />
			<s:set var="selItemID" value="'selBatchID'" />
			
			<%@ include file="fragments/list.html" %>
		</s:form>
	</div>
</s:i18n>

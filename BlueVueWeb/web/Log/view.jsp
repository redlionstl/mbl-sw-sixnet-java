<%--
 % Display details of a single log entry.
 % 
 % Jonathan Pearson
 % June 4, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<s:i18n name="logs">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			
			<%@ include file="../fragments/includes.html" %>
			
			<title><s:text name="i18n.logs" /> - <%= Server.getServerSettings().getStringValue(Settings.Setting.Title) %></title>
		</head>
		
		<body>
			<%@ include file="../fragments/header.html" %>
			
				<div id="bodyArea">
					<div id="main">
						<%@ include file="../fragments/logs/breadcrumbs/view.bc.html" %>
						
						<div id="twocolumn">
							<%@ include file="../fragments/logs/leftnav/view.ln.html" %>
							
							<div id="content">
								<s:actionerror/>
								
								<table>
									<tr>
										<th><s:text name="i18n.property" /></th>
										<th><s:text name="i18n.value" /></th>
									</tr>
									
									<tr>
										<td><s:text name="i18n.log.property.logID" />:</td>
										<td><s:property value="entry.logID" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.log.property.type" />:</td>
										<td><s:property value="entry.type" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.log.property.user" />:</td>
										<td>
											<s:if test="getLogUser(entry) == null">
												<i><s:text name="i18n.emptyProp" /></i>
											</s:if>
											<s:else>
												<s:a href="%{documentRoot + '/User/view?id=' + getLogUser(entry).userID}">
													<s:property value="%{getLogUser(entry).name}" />
												</s:a>
											</s:else>
										</td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.log.property.timestamp" />:</td>
										<td><s:property value="%{formatData('format.datetime', entry.timestamp)}" /></td>
									</tr>
									
									<tr>
										<td><s:text name="i18n.log.property.description" />:</td>
										<td><s:property value="entry.description" /></td>
									</tr>
								</table>
							</div>
						</div>
						
						<p id="clearBottom" />
					</div>
				</div>
				
			<%@ include file="../fragments/footer.html" %>
		</body>
	</s:i18n>
</html>

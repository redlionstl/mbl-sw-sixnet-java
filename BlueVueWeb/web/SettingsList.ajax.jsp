<%--
 % Returns only the settings list, used for AJAX refreshing.
 % 
 % Jonathan Pearson
 % June 19, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<s:i18n name="settings">
	<div class="frame">
		<s:actionerror/>
		
		<table>
			<tr>
				<th><s:text name="i18n.setting.header.name" /></th>
				<th><s:text name="i18n.value" /></th>
			</tr>
			
			<s:iterator value="settings">
				<tr>
					<td><s:property value="key" /></td>
					<td>
						<s:if test="value.exportonly">
							<i><s:text name="i18n.setting.value.exportonly" /></i>
						</s:if>
						<s:elseif test="value.value == null">
							<i><s:text name="i18n.setting.value.empty" /></i>
						</s:elseif>
						<s:else>
							<s:property value="value.value" />
						</s:else>
					</td>
					
					<td>
						<s:if test="value.exportonly">
							<s:form action="Setting/export" name="export" id="Settings" theme="simple">
								<s:hidden name="settingName" value="%{key}" />
								<s:hidden name="settingValue" value="" />
								<s:hidden name="contentType" value="%{value.contentType}" />
								<s:hidden name="fileName" value="%{value.fileName}" />
								
								<s:submit value="%{getText('i18n.setting.action.export')}" />
							</s:form>
						</s:if>
						<s:elseif test="value.readonly">
							<s:text name="i18n.setting.action.readonly" />
						</s:elseif>
						<s:else>
							<s:form action="Setting/edit" name="edit" id="Settings" theme="simple">
								<s:hidden name="settingName" value="%{key}" />
								<s:hidden name="settingValue" value="%{value.value}" />
								<s:hidden name="delete" value="false" />
								
								<s:submit value="%{getText('i18n.setting.action.edit')}" />
								<s:submit value="%{getText('i18n.setting.action.delete')}" onclick="form.action='Setting/update'; form.delete.value='true'; return true;" />
							</s:form>
						</s:else>
					</td>
				</tr>
			</s:iterator>
		</table>
	</div>
</s:i18n>

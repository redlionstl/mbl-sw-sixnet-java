<%--
 % Returns only the tag list, used by AJAX refreshing.
 % 
 % Jonathan Pearson
 % June 19, 2009
 %
 --%>

<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.sixnetio.BVB.Common.*" %>
<%@ page import="com.sixnetio.BVB.Web.*" %>

<!DOCTYPE html PUBLIC
	"-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<s:i18n name="tags">
	<s:set var="formName" value="'Tags'" />
	<s:set var="actionName" value="'Tags'" />
	
	<s:set var="doPages" value="true" />
	<s:set var="doSelections" value="true" />
	<s:set var="doRefresher" value="true" />
	
	<%@ include file="fragments/pagenav.html" %>
	
	<div class="frame">
		<s:actionerror />
		
		<s:form id="%{formName}" name="%{formName}" action="%{actionName}">
			<!-- Add in all of the query fields, so pagination works properly -->
			<s:if test="qTagID != null"><s:hidden name="qTagID" theme="simple" /></s:if>
			<s:if test="qTagName != null"><s:hidden name="qTagName" theme="simple" /></s:if>
			<s:if test="qLowPollingInterval != null"><s:hidden name="qLowPollingInterval" theme="simple" /></s:if>
			<s:if test="qHighPollingInterval != null"><s:hidden name="qHighPollingInterval" theme="simple" /></s:if>
			<s:if test="qLowPosition != null"><s:hidden name="qLowPosition" theme="simple" /></s:if>
			<s:if test="qHighPosition != null"><s:hidden name="qHighPosition" theme="simple" /></s:if>
			<s:if test="qModemID != null"><s:hidden name="qModemID" theme="simple" /></s:if>
			
			<s:hidden name="selection" value="keep" theme="simple" />
			
			<s:set var="section" value="'tags'" />
			<s:set var="sectionLink" value="'Tags'" />
			<s:set var="itemID" value="'tagID'" />
			<s:set var="selItemID" value="'selTagID'" />
			
			<%@ include file="fragments/list.html" %>
		</s:form>
	</div>
</s:i18n>

/*
 * TTLScript.g
 *
 * Specifies a grammar used for parsing enhanced TTL scripts. This is the
 * first step in turning enhanced TTL scripts (our own format) into
 * standard TTL scripts supported by BlueTree modems (a stripped-down
 * version of what TerraTerm specifies; only supports send(ln) and
 * wait(ln)).
 *
 * To use: Run ANTLR from the command line to generate
 *   com.sixnetio.BVB.Modems.ScriptParser.TTLScript[Parser|Lexer).java
 * or run from Antlrv3IDE (available on SourceForge) to generate those files.
 * You may need to move the files that Antlrv3IDE generates into the proper
 * package.
 *
 * Note: This was written for a tab-width of 8
 *
 * Jonathan Pearson
 * March 11, 2009
*/

grammar TTLScript;

options {
	output=AST;
	ASTLabelType=CommonTree;
}

tokens {
	SEMI	= ';' ;
	COMMA	= ',' ;
	LPAREN	= '(' ;
	RPAREN	= ')' ;
	LBRACE	= '{';
	RBRACE	= '}';
	EQUALS = '=';
	CONCAT = '.';
	
	IF	= 'if' ;
	ELSE	= 'else' ;
	
	// Invisible symbols
	BLOCK;
	PROG;
}

@header {
	package com.sixnetio.BVB.Modems.ScriptParser;
}

@lexer::header {
	package com.sixnetio.BVB.Modems.ScriptParser;
}

@members {
	private ErrorHandler errorHandler = null;
	public void setErrorHandler(ErrorHandler errorHandler) {
		this.errorHandler = errorHandler;
	}
	
	public void emitErrorMessage(String msg) {
		errorHandler.handleError(msg);
	}
}

prog
	: stmts EOF				-> ^(PROG stmts?)
	;

stmts
	: stmt*
	;

stmt
	: SENDLN^ expr
	| WAITLN^ expr (COMMA! expr)*
	| ifstmt
	| VARIABLE EQUALS^ expr
	;

expr
	: atom (CONCAT^ expr)?
	;


ifstmt
	: IF^ test block elsestmt?
	;

test
	: LPAREN! testexpr RPAREN!
	;

testexpr
	: comparison^ (BOOLOP^ testexpr)?
	;

block
	: LBRACE stmts RBRACE			-> ^(BLOCK stmts?)
	;

elsestmt
	: ELSE! block
	| ELSE! ifstmt
	;

comparison
	: expr COMPAREOP^ expr
	;

atom
	: VARIABLE				-> VARIABLE
	| STRING				-> STRING
	;

LETTER
	: 'a'..'z'
	| 'A'..'Z'
	;

DIGIT
	: '0'..'9'
	;

SENDLN
	: 'send' 'ln'?
	;

WAITLN
	: 'wait' 'ln'?
	;

COMPAREOP
	: '=='
	| '!='
	| '==i'
	| '!=i'
	| '<'
	| '>'
	| '<='
	| '>='
	;
	
BOOLOP
	: '&&'
	| '||'
	;

// Characters allowed in a variable name after the initial character
VARCHAR
	: (LETTER | DIGIT | '_')
	;

VARIABLE
	: LETTER VARCHAR*
	;

STRING
	: '\'' ~('\'' | '\r' | '\n')* '\''
	| '"' ~('"' | '\r' | '\n')* '"'
	;

WS
	: ('\t' | ' ' | '\r' | '\n')+ { $channel = HIDDEN; }
	;

COMMENT
	: ';' ~('\r' | '\n')* ('\r'? '\n') { $channel = HIDDEN; }
	;


/*
 * StatusJob.java
 *
 * Performs status queries against modems.
 *
 * Jonathan Pearson
 * January 16, 2009
 *
 */

package com.sixnetio.BVB.Jobs.JobTypes;

import java.text.ParseException;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;

public class StatusJob
    extends Job
{

    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    static {
        Job.registerJobType("Status", StatusJob.class);
    }

    public StatusJob(JobData jobData, Database database)
    {
        super(jobData, database);

        NDC.push(String.format("jobID = %d", jobData.jobID));
        logger.debug("New StatusJob");
        NDC.pop();
    }

    @Override
    protected void runJob(ModemCommunicator comm, Modem modem, Date startTime)
        throws ModemCommunicationFailedException, DatabaseException,
        BadModemModuleException
    {

        // TODO: Support Thread.interrupt()

        boolean closeWhenDone = false;

        // If necessary, open a new connection to the modem
        if (comm == null) {
            comm = new SocketModemCommunicator();
            comm.connect(modem);
            closeWhenDone = true;
        }

        try {
            prepareCommunications(comm, modem);

            // Run the standard modem update
            logger.debug("Calling refreshModemData() for the modem");
            modemModule.refreshModemData(comm, modem);

            // Store the updated modem data back to the database
            getDatabase().updateModem(modem);

            // Run the extra update
            // But make sure we don't update data that was just written a moment
            // ago (within 1 minute)
            // We need to save the property names that were found, so we do not
            // try to store them into the database again (they would violate
            // uniqueness constraints), and we need to know the timestamp so we
            // can store the newer values with the same timestamp as the older
            // ones
            Collection<String> knownProps = new HashSet<String>();
            Date recentTimestamp =
                setRecentStatus(modem, modemModule, knownProps);

            if (recentTimestamp == null) {
                logger.debug("Properties not updated recently");
                recentTimestamp = startTime;
            }
            else if (logger.isDebugEnabled()) {
                StringBuilder msg = new StringBuilder();
                msg.append(String.format(
                    "Properties updated recently (@%s, now = %s):\n", Utils
                        .formatStandardDateTime(recentTimestamp, false), Utils
                        .formatStandardDateTime(new Date(), false)));

                for (String prop : knownProps) {
                    msg.append(String.format("  %s\n", prop));
                }

                logger.debug(msg.toString());
            }

            logger.debug("Calling getStatus() for the modem");
            Map<String, String> props = modemModule.getStatus(comm, modem);

            // Remove the properties that we have already stored
            // This prevents us from violating uniqueness constraints in the
            // database
            props.keySet().removeAll(knownProps);

            // Store those properties back into the database also
            // Use 'recentTimestamp' for this so that the values from earlier
            // and these values do not appear to be from separate transactions
            // That would likely cause a UI to only display the newest set,
            // which would miss a number of values
            logger.debug("Updating the database with modem status info");
            getDatabase().saveStatusValues(modem.modemID, recentTimestamp,
                props);
        }
        finally {
            if (closeWhenDone) {
                comm.close();
            }
        }
    }

    /**
     * Check for whether a status history update was made recently (within the
     * last minute). If so, give the names of the updated values to the provided
     * modem module, so that it may attempt to avoid requesting the same data
     * again.
     * 
     * @param modem The modem whose status is being updated.
     * @param mm The modem module responsible for updating that status.
     * @param propNames An output parameter that will contain the names of the
     *            recently updated status values for the given modem, if there
     *            were any.
     * @return The timestamp when the modem's status values were last updated,
     *         if they were updated within the last minute. Otherwise, returns
     *         <tt>null</tt>.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    private Date setRecentStatus(Modem modem, ModemModule mm,
                                 Collection<String> propNames)
        throws DatabaseException
    {
        Date ts = null;

        Map<String, String> existingStatus =
            getDatabase().getModemStatus(modem.modemID, true, null, null);

        if (existingStatus != null && !existingStatus.isEmpty()) {
            // Check the timestamp on them (all have the same timestamp)
            String key = existingStatus.keySet().iterator().next();
            String timestamp = key.substring(key.indexOf('@') + 1);

            try {
                Date d = Utils.parseStandardDateTime(timestamp);

                // Within the last minute?
                Date oneMinuteAgo =
                    new Date(System.currentTimeMillis() - 60 * 1000L);

                if (d.compareTo(oneMinuteAgo) >= 0) {
                    logger.debug(String.format(
                        "Status history was recent, adding %d properties",
                        existingStatus.size()));
                    ts = d;

                    // Within the last minute, set the recent property names
                    // on the modem module so it will try not to request them
                    for (String propName : existingStatus.keySet()) {
                        // Cut off the timestamp
                        propName = propName.substring(0, propName.indexOf('@'));

                        propNames.add(propName);
                    }

                    mm.addRecentProperties(propNames);
                }
                else {
                    logger.debug(String.format(
                        "Status history too old, from %s (now %s)", Utils
                            .formatStandardDateTime(d, false), Utils
                            .formatStandardDateTime(new Date(), false)));
                }
            }
            catch (ParseException pe) {
                logger.error("Status history timestamp not in normal format",
                    pe);
            }
        }

        return ts;
    }
}

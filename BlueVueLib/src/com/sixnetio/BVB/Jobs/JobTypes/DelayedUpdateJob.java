/*
 * DelayedUpdateJob.java
 *
 * Performs delayed updates on modems.
 *
 * Jonathan Pearson
 * June 2, 2009
 *
 */

package com.sixnetio.BVB.Jobs.JobTypes;

import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.BadJobException;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Modems.*;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class DelayedUpdateJob extends Job {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		// This is an internal-only job type, it shouldn't show up in most UIs
		// That's what the ! means
		Job.registerJobType("!DelayedUpdate", DelayedUpdateJob.class);
	}
	
	public DelayedUpdateJob(JobData jobData, Database database) {
		super(jobData, database);
		
		NDC.push(String.format("jobID = %d", jobData.jobID));
		logger.debug("New DelayedUpdateJob");
		NDC.pop();
	}
	
	@Override
	protected void runJob(ModemCommunicator comm, Modem modem, Date startTime)
		throws ModemCommunicationFailedException, DatabaseException,
			BadModemModuleException, BadJobException
	{
		modemModule = null; // We don't use it
		
		// Scan the 'modem' section of the parameters XML for updates to make to
		// the modem
		Map<String, String> status = new HashMap<String, String>();
		Date timestamp = null; // Must be set if any status values exist
		
		for (Tag tag : getJobData().parameters.getEachTag("modem", null)) {
			if (tag.getName().equals("deviceID")) {
				modem.deviceID = new DeviceID(tag.getLongContents());
			} else if (tag.getName().equals("alias")) {
				modem.alias = tag.getStringContents();
			} else if (tag.getName().equals("model")) {
				modem.model = tag.getStringContents();
			} else if (tag.getName().equals("fwVersion")) {
				modem.fwVersion = new Version(tag.getStringContents());
			} else if (tag.getName().equals("cfgVersion")) {
				modem.cfgVersion = tag.getStringContents();
			} else if (tag.getName().equals("ipAddress")) {
				modem.ipAddress = tag.getStringContents();
			} else if (tag.getName().equals("port")) {
				modem.port = tag.getIntContents();
			} else if (tag.getName().equals("phoneNumber")) {
				modem.phoneNumber = tag.getStringContents();
			} else if (tag.getName().equals("property")) {
				String propName = tag.getStringAttribute("name");
				String value = tag.getStringContents();
				modem.setProperty(propName, value);
			} else if (tag.getName().equals("status")) {
				String propName = tag.getStringAttribute("name");
				String value = tag.getStringContents();
				status.put(propName, value);
			} else if (tag.getName().equals("timestamp")) {
				timestamp = new Date(tag.getLongContents());
			} else {
				logger.warn("Unrecognized property name: " + tag.getName());
			}
		}
		
		logger.debug("Updating modem #" + modem.modemID);
		getDatabase().updateModem(modem);
		
		if (!status.isEmpty()) {
			if (timestamp == null) throw new IllegalStateException(
			                                                       "No timestamp provided with status values");
			
			logger.debug("Found status values, updating status history of modem #" + modem.modemID);
			getDatabase().saveStatusValues(modem.modemID, timestamp, status);
		}
	}
}

/*
 * ReconfigJob.java
 *
 * Performs reconfigurations of modems.
 *
 * Jonathan Pearson
 * February 9, 2009
 *
 */

package com.sixnetio.BVB.Jobs.JobTypes;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Common.ModemTag;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Modems.*;
import com.sixnetio.BVB.Modems.ScriptParser.ScriptCompiler;
import com.sixnetio.BVB.Modems.ScriptParser.ScriptFormatException;
import com.sixnetio.util.Utils;

public class ReconfigJob
    extends Job
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    static {
        Job.registerJobType("Reconfiguration", ReconfigJob.class);
    }

    public ReconfigJob(JobData jobData, Database database)
    {
        super(jobData, database);

        NDC.push(String.format("jobID = %d", jobData.jobID));
        logger.debug("New ReconfigJob");
        NDC.pop();
    }

    @Override
    protected void runJob(ModemCommunicator comm, Modem modem, Date startTime)
        throws ModemCommunicationFailedException, DatabaseException,
        BadModemModuleException
    {
        // TODO: Support Thread.interrupt()

        boolean closeWhenDone = false;

        // If necessary, open a new connection to the modem
        if (comm == null) {
            comm = new SocketModemCommunicator();
            comm.connect(modem);
            closeWhenDone = true;
        }

        try {
            prepareCommunications(comm, modem);

            // Run the commands in the tag scripts, in the order that they
            // should be applied (the order they are returned in)
            List<Integer> tags =
                getDatabase().getMatchingTags(null, null, null, null, null,
                    null, modem.modemID, 0, -1, null, null);

            boolean success = false;
            try {
                for (int tagID : tags) {
                    ModemTag tag = getDatabase().getTag(tagID);

                    if (tag.configScript != null) {
                        logger.info(String.format(
                            "Running commands for tag %s on modem %d",
                            tag.name, modem.modemID));

                        try {
                            String[] script =
                                ScriptCompiler.compileScript(tag.configScript,
                                    modem);
                            modemModule.runCommands(comm, modem, script);

                            checkForReboot(script);
                        }
                        catch (ScriptFormatException sfe) {
                            logger.error(String.format(
                                "Script for tag %s has compile errors",
                                tag.name), sfe);

                            throw new ModemCommunicationFailedException(
                                String
                                    .format(
                                        "Unable to compile script for Tag #%d and Modem #%d: %s",
                                        tag.tagID, modem.modemID, sfe
                                            .getMessage()), sfe);
                        }
                    }
                    else {
                        logger.info(String.format(
                            "Tag %s has no commands to run", tag.name));
                    }
                }

                // Also, if the modem has any special configuration data that
                // needs
                // to be applied, run those commands also
                String special = modem.getProperty("privateConfig");
                if (special != null && special.length() > 0) {
                    logger.info(String.format(
                        "Running modem %d's special commands", modem.modemID));

                    try {
                        String[] script =
                            ScriptCompiler.compileScript(special, modem);
                        modemModule.runCommands(comm, modem, script);

                        checkForReboot(script);
                    }
                    catch (ScriptFormatException sfe) {
                        logger.error(String.format(
                            "Private script for modem %d has compile errors",
                            modem.modemID), sfe);

                        throw new ModemCommunicationFailedException(String
                            .format(
                                "Unable to compile script for Modem #%d: %s",
                                modem.modemID, sfe.getMessage()), sfe);
                    }
                }

                // If we get here, everything succeeded
                success = true;
            }
            finally {
                // By not catching this exception, we allow Job to do its own
                // cleanup
                if (!success) {
                    // Failed on one of the script files, try to clear the modem
                    // configuration
                    logger.warn(String.format(
                        "Trying to reset modem %d's configuration using ATZ1",
                        modem.modemID));

                    try {
                        modemModule.runCommands(comm, modem, "sendln 'ATZ1'",
                            "waitln 'OK'");
                    }
                    catch (ModemCommunicationFailedException mcfe) {
                        logger.error(String.format(
                            "Unable to execute the ATZ1 command on modem %d",
                            modem.modemID), mcfe);
                    }
                }
            }
        }
        finally {
            if (closeWhenDone) {
                comm.close();
            }
        }

        // If necessary, schedule a new status query for the near future
        scheduleStatusQueryAsNecessary(modem);
    }

    /**
     * Search the given script for commands that would likely cause a modem
     * reboot. If found, call {@link #setModemLikelyRebooted(boolean)} with the
     * parameter <code>true</code>.
     * 
     * @param script The lines of the script.
     */
    private void checkForReboot(String[] script)
    {
        // If any of those commands were AT+BRESET or
        // AT+BRSTRT, we can expect the modem to reboot
        for (String s : script) {
            String uc = s.toUpperCase();
            if (uc != null &&
                (uc.contains("AT+BRESET") || uc.contains("AT+BRSTRT"))) {

                setModemLikelyRebooted(true);
                break;
            }
        }
    }
}

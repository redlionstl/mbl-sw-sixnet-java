/*
 * FirmwareJob.java
 *
 * Applies an update package to a modem. This is usually
 * going to be a firmware update (hence the name), but it
 * does not need to be.
 *
 * Jonathan Pearson
 * February 9, 2009
 *
 */

package com.sixnetio.BVB.Jobs.JobTypes;

import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;

public class FirmwareJob
    extends Job
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    static {
        Job.registerJobType("Firmware", FirmwareJob.class);
    }

    public FirmwareJob(JobData jobData, Database database)
    {
        super(jobData, database);

        NDC.push(String.format("jobID = %d", jobData.jobID));
        logger.debug("New FirmwareJob");
        NDC.pop();
    }

    @Override
    protected void runJob(ModemCommunicator comm, Modem modem, Date startTime)
        throws ModemCommunicationFailedException, DatabaseException,
        BadModemModuleException, BadJobException
    {

        // TODO: Support Thread.interrupt()

        // Grab the firmware image from the database
        String hash = getJobData().parameters.getStringContents("firmware");

        if (hash == null) {
            throw new BadJobException(
                "Job parameters missing 'firmware' property",
                getJobData().type, getJobData().jobID);
        }

        BlueTreeImage image;

        image = getDatabase().getPackage(hash, false);
        if (image == null) {
            logger.error("Could not find BlueTree Image '" + hash + "'");

            throw new BadJobException("Job references bad update image '" +
                                      hash + "'", getJobData().type,
                getJobData().jobID);
        }

        boolean closeWhenDone = false;

        // If necessary, open a new connection to the modem
        if (comm == null) {
            comm = new SocketModemCommunicator();
            comm.connect(modem);
            closeWhenDone = true;
        }

        try {
            prepareCommunications(comm, modem);

            if (modem.fwVersion.equals(image.getVersion())) {
                // We're trying to update firmware to the same version in the
                // modem
                // Skip out unless the user specifically said not to
                if (getJobData().parameters.getTag("force") == null) {
                    // Just in case we updated the version number
                    getDatabase().updateModem(modem);

                    return;
                }
            }

            // Grab the proper modem module
            modemModule = ModemModule.makeModule(modem.model, modem.fwVersion);

            modemModule.setHTTPServer(getHTTPServer());

            // Perform the update
            modemModule.loadPackage(comm, modem, image);
            setModemLikelyRebooted(true);

            // Clear the modem firmware version number so it'll be requested
            // next time (if the job failed, an exception would have jumped out
            // by now)
            modem.fwVersion = null;
            getDatabase().updateModem(modem);

            // If necessary, schedule a new status query for the near future
            scheduleStatusQueryAsNecessary(modem);
        }
        finally {
            if (closeWhenDone) {
                comm.close();
            }
        }
    }
}

/*
 * DebugJob.java
 *
 * A job for debugging. It does nothing, except consume some time and fail randomly.
 *
 * Jonathan Pearson
 * January 22, 2009
 *
 */

package com.sixnetio.BVB.Jobs.JobTypes;

import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class DebugJob extends Job {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		Job.registerJobType("Debug", DebugJob.class);
	}
	
	public DebugJob(JobData jobData, Database database) {
		super(jobData, database);
		
		NDC.push(String.format("jobID = %d", jobData.jobID));
		logger.debug("New DebugJob");
		NDC.pop();
	}
	
	@Override
	protected void runJob(ModemCommunicator comm, Modem modem, Date startTime)
		throws ModemCommunicationFailedException, DatabaseException,
			BadModemModuleException
	{
		modemModule = null; // We don't use it
		
		// Choose a random amount of time between 5 and 30 seconds to sleep
		int sleepFor = Utils.rand.nextInt(26) + 5; // [0, 26) + 5 = [5, 31) =
		// [5, 30]
		
		logger.debug(String.format("Debug job sleeping for %d seconds", sleepFor));
		Utils.sleep(sleepFor * 1000);
		
		// Choose whether to fail
		// 2% for each type of exception, to make sure everything gets exercised
		double chance = Utils.rand.nextDouble();
		if (chance < 0.02) {
			logger.debug("Debug job failing with ModemCommunicationFailedException");
			throw new ModemCommunicationFailedException("MCFE thrown for debugging purposes");
		} else if (chance < 0.04) {
			logger.debug("Debug job failing with DatabaseException");
			throw new DatabaseException("DE thrown for debugging purposes");
		} else if (chance < 0.06) {
			logger.debug("Debug job failing with BadModemModuleException");
			throw new BadModemModuleException("BMME thrown for debugging purposes", "debug",
			                                  new Version("debug"));
		}
		
		// Sleep for a little bit longer (5-30 seconds)
		sleepFor = Utils.rand.nextInt(26) + 5;
		
		logger.debug(String.format("Debug job sleeping for %d more seconds", sleepFor));
		Utils.sleep(sleepFor * 1000);
	}
}

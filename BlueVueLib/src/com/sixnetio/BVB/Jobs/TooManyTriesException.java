/*
 * TooManyTriesException.java
 *
 * Thrown when a job has surpassed the allowed number of attempts.
 *
 * Jonathan Pearson
 * August 11, 2009
 *
 */

package com.sixnetio.BVB.Jobs;

public class TooManyTriesException extends Exception {
	public TooManyTriesException() {
		super();
	}
	
	public TooManyTriesException(String msg) {
		super(msg);
	}
	
	public TooManyTriesException(String msg, Throwable cause) {
		super(msg, cause);
	}
}

/*
 * Job.java
 *
 * A generic 'job'.
 *
 * Jonathan Pearson
 * January 15, 2009
 *
 */

package com.sixnetio.BVB.Jobs;

import java.lang.reflect.Constructor;
import java.util.*;

import org.apache.log4j.*;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.BVB.Modems.*;
import com.sixnetio.server.http.HTTP;
import com.sixnetio.util.Utils;

public abstract class Job
    implements Runnable
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /**
     * A simple structure for representing a job's data without actually
     * constructing a Job and going through the process of making sure the type
     * is supported.
     * 
     * @author Jonathan Pearson
     */
    public static class JobData
    {
        public static final long INV_JOB = -1;

        public long jobID = INV_JOB;
        public Integer batchID;
        public int modemID;
        public Integer userID;
        public Date schedule;
        public int tries;
        public String type;
        public Tag parameters;
        public boolean automatic;

        // This is never written to the database from here, it is only for
        // convenience
        public String lockedBy;
        public Date lockedAt;

        /**
         * Construct an empty JobData structure.
         */
        public JobData()
        {
        }

        /**
         * Fill in the JobData structure.
         * 
         * @param jobID The Job ID of the job; pass {@link #INV_JOB} if creating
         *            a new one.
         * @param batchID The Batch ID of the job, or <tt>null</tt>.
         * @param modemID The Modem ID of the target of the job.
         * @param userID The User ID of the user that created the job, or
         *            <tt>null</tt>.
         * @param schedule The time at which the job was scheduled to run.
         * @param tries The number of attempts that have been made, so far, to
         *            run this job.
         * @param type The type of job.
         * @param parameters Extra parameters defining this job.
         * @param automatic Whether the job should automatically reschedule
         *            itself.
         */
        public JobData(long jobID, Integer batchID, int modemID,
                       Integer userID, Date schedule, int tries, String type,
                       Tag parameters, boolean automatic)
        {

            this(jobID, batchID, modemID, userID, schedule, tries, type,
                parameters, automatic, null, null);
        }

        /**
         * Fill in the JobData structure.
         * 
         * @param jobID The Job ID of the job.
         * @param batchID The Batch ID of the job, or <tt>null</tt>.
         * @param modemID The Modem ID of the target of the job.
         * @param userID The User ID of the user that created the job, or
         *            <tt>null</tt>.
         * @param schedule The time at which the job was scheduled to run.
         * @param tries The number of attempts that have been made, so far, to
         *            run this job.
         * @param type The type of job.
         * @param parameters Extra parameters defining this job.
         * @param automatic Whether the job should automatically reschedule
         *            itself.
         * @param lockedBy The owner of the job lock, or <tt>null</tt>. This
         *            should only be populated by a database query.
         * @param lockedAt When the owner of the job lock acquired the lock.
         *            This should only be populated by a database query.
         */
        public JobData(long jobID, Integer batchID, int modemID,
                       Integer userID, Date schedule, int tries, String type,
                       Tag parameters, boolean automatic, String lockedBy,
                       Date lockedAt)
        {

            this.jobID = jobID;
            this.batchID = batchID;
            this.modemID = modemID;
            this.userID = userID;
            this.schedule = new Date(schedule.getTime());
            this.tries = tries;
            this.type = type;
            this.parameters = parameters;
            this.automatic = automatic;

            this.lockedBy = lockedBy;

            if (lockedAt != null) {
                this.lockedAt = new Date(lockedAt.getTime());
            }
            else {
                this.lockedAt = null;
            }
        }
    }

    private static Map<String, Class<? extends Job>> jobTypes =
        new HashMap<String, Class<? extends Job>>();

    /**
     * Register a class that can perform the specified type of job.
     * 
     * @param type The type of job it can support.
     * @param jobClass The class that supports it.
     */
    public static void registerJobType(String type,
                                       Class<? extends Job> jobClass)
    {
        if (jobClass == null) {
            throw new NullPointerException("jobClass may not be null");
        }

        logger.debug(String.format("Registering job type '%s' with class '%s'",
            type, jobClass.getName()));

        synchronized (jobTypes) {
            if (jobTypes.put(type, jobClass) != null) {
                throw new DuplicateRegistrationError("Duplicate job type '" +
                                                     type + "'");
            }
        }
    }

    /**
     * Get a set of supported job types.
     */
    public static Set<String> getKnownJobTypes()
    {
        synchronized (jobTypes) {
            return new HashSet<String>(jobTypes.keySet());
        }
    }

    /**
     * Construct a new job.
     * 
     * @param jobData The data that defines the job.
     * @param db The Database object that the job should use for additional
     *            data/updates.
     * @return A new Job object.
     * @throws BadJobException If the job type is unsupported or cannot be
     *             created.
     * @throws NullPointerException If either <tt>jobData</tt> or <tt>db</tt> is
     *             <tt>null</tt>.
     */
    public static Job makeJob(JobData jobData, Database db)
        throws BadJobException
    {
        if (jobData == null) {
            throw new NullPointerException("jobData");
        }
        if (db == null) {
            throw new NullPointerException("db");
        }

        NDC.push(String.format("jobType = '%s'", jobData.type));

        try {
            logger.debug("Attempting to construct a new Job");

            Class<? extends Job> jobClass;
            synchronized (jobTypes) {
                jobClass = jobTypes.get(jobData.type);
            }

            if (jobClass == null) {
                logger.error("Job type not registered");

                throw new BadJobException("Unknown job type", jobData.type,
                    jobData.jobID);
            }

            try {
                logger.debug(String.format("Calling constructor of '%s'",
                    jobClass.getName()));

                Constructor<? extends Job> constructor =
                    jobClass.getConstructor(JobData.class, Database.class);
                return constructor.newInstance(jobData, db);
            }
            catch (Exception e) {
                // NoSuchMethodException, SecurityException (getConstructor)
                // InstantiationException or IllegalAccessException,
                // IllegalArgumentException, InvocationTargetException
                // (newInstance)
                logger.error(String.format("Unable to instantiate job class "
                                           + "'%s'", jobClass.getName()), e);

                throw new BadJobException("Unable to instantiate Job",
                    jobData.type, jobData.jobID, e);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Construct a generic job that cannot actually perform any actions. This is
     * useful if you want to create a job on a machine without job support to be
     * executed on another machine that does support it.
     * 
     * @param jobData The data specifying what the job will do.
     * @return A generic, non-functional job.
     * @throws NullPointerException If <tt>jobData</tt> is <tt>null</tt>.
     */
    public static Job makeGenericJob(JobData jobData)
    {
        if (jobData == null) {
            throw new NullPointerException("jobData");
        }

        logger.debug("Constructing a generic Job");

        return new Job(jobData, null) {
            @Override
            public void runJob(ModemCommunicator comm, Modem modem,
                               Date startTime)
            {
                // Default behavior: do nothing
            }
        };
    }

    private JobData jobData;
    private Modem modem;
    private Database database;
    private ModemCommunicator comm;
    private HTTP httpServer;
    private boolean unlockWhenDone = true;
    private boolean modemLikelyRebooted = false;

    /**
     * If the implementing class uses a modem module, it should save it here for
     * future reference.
     */
    protected ModemModule modemModule;

    /**
     * Construct a new <b>generic</b> Job object.
     * 
     * @param jobData The data describing this job.
     * @param database The Database object that this job should use.
     */
    protected Job(JobData jobData, Database database)
    {
        this.jobData = jobData;
        this.database = database;
    }

    /**
     * Get the data that defines this job. There is no point in changing the
     * 'type' at this point, it will not change the running job type, but it can
     * screw up the database.
     */
    public JobData getJobData()
    {
        return jobData;
    }

    /**
     * Get the database that this job uses.
     */
    protected Database getDatabase()
    {
        return database;
    }

    /**
     * Get the modem communicator set for use by this job. If <tt>null</tt>, a
     * new {@link SocketModemCommunicator} will be constructed if necessary when
     * the job executes.
     */
    public ModemCommunicator getModemCommunicator()
    {
        return comm;
    }

    /**
     * Set the modem communicator to use with this job. If set to <tt>null</tt>
     * (the default), the job will automatically construct a new
     * {@link SocketModemCommunicator} if it needs one.
     * 
     * @param comm The communicator to use.
     */
    public void setModemCommunicator(ModemCommunicator comm)
    {
        this.comm = comm;
    }

    /**
     * Get the HTTP server used for modem file downloads.
     * 
     * @return The HTTP server being used, or <tt>null</tt> if a 'push' method
     *         is being used instead.
     */
    public HTTP getHTTPServer()
    {
        return httpServer;
    }

    /**
     * Set the HTTP server to use for modem file downloads.
     * 
     * @param httpServer The HTTP server to use, or <tt>null</tt> if a 'push'
     *            method should be used.
     */
    public void setHTTPServer(HTTP httpServer)
    {
        this.httpServer = httpServer;
    }

    /**
     * Get the Modem object that is the target of this job.
     * 
     * @return The Modem object, may be <tt>null</tt>.
     */
    public Modem getModem()
    {
        return modem;
    }

    /**
     * Get the modem module that was used to interface with the modem targeted
     * by this job.
     * 
     * @return The modem module that was used, or <tt>null</tt> if none was
     *         used.
     */
    public ModemModule getModemModule()
    {
        return modemModule;
    }

    /**
     * Get the modem object that is the target of this job, retrieving it from
     * the database if necessary.
     * 
     * @return The modem object that is the target of this job.
     * @throws DatabaseException If there is a problem getting the modem from
     *             the database.
     */
    public Modem retrieveModem()
        throws DatabaseException
    {
        if (getModem() == null) {
            logger.debug("Requesting Modem object for job");
            setModem(getDatabase().getModem(getJobData().modemID));
        }

        return getModem();
    }

    /**
     * Set the Modem object that is the target of this job. If you have access
     * to it, you should call this before running the job. If not set, the job
     * will request it, which is a database access.
     * 
     * @param modem The modem to be used by this job.
     */
    public void setModem(Modem modem)
    {
        this.modem = modem;
    }

    @Override
    public final void run()
    {
        // FUTURE: Support Thread.interrupt()

        // Note: Every exit route from this procedure must unlock the modem, and
        // must either unlock or delete the job

        NDC.push(String.format("jobID = %d, modemID = %d", getJobData().jobID,
            getJobData().modemID));


        if (comm != null) {
            MDC.put("Modem", comm.toString());
        }
        else {
            MDC.put("Modem", "(Unconnected)");
        }

        if (getDatabase() == null) {
            // The only way for getDatabase() to return null is if this is a
            // 'generic' job, in which case this method should never have been
            // called

            // Additionally, we cannot unlock the modem/job because we do not
            // have access to the database

            logger.error("getDatabase() == null");

            NDC.pop();

            return;
        }

        Date startTime = new Date(System.currentTimeMillis());

        // Set this to true if you delete the job
        // The 'finally' clause checks this and the 'automatic' status, and
        // schedules a new instance if necessary
        boolean deleted = false;

        // Finally block cleans up locks and pops the NDC
        try {
            // Increment tries
            // If it has reached the max, this will throw an exception
            incrementTries();

            DBLogger.log(getJobData().userID, DBLogger.INFO, String.format(
                "Job #%d started, try number %d", getJobData().jobID,
                getJobData().tries));

            // Grab the modem
            Modem modem = retrieveModem();

            // Perform the job function (provided by subclasses)
            runJob(getModemCommunicator(), modem, startTime);

            // The job succeeded
            // Archive this job and delete it
            logger.debug("Moving this job to the History table as a success");
            getDatabase().archiveJob(getJobData().jobID, true,
                String.format("Job succeeded"), startTime);

            getDatabase().deleteJob(getJobData().jobID);
            deleted = true;

            DBLogger.log(null, DBLogger.INFO, String.format(
                "Archived Job #%d to History", getJobData().jobID));
        }
        // Now for a whole lot of error-specific messages
        // It'd be nice to just catch 'Throwable', but we wouldn't be able to
        // give as much feedback
        catch (ModemCommunicationFailedException mcfe) {
            logger.error("Unable to communicate with modem", mcfe);

            String msg =
                String.format("Job failed, unable to communicate "
                              + "with the device: %s", mcfe.getMessage());

            deleted = safeArchiveAsFailure(startTime, msg);
        }
        catch (BadModemModuleException bmme) {
            logger.error("Failed to get the proper modem module", bmme);

            String msg =
                String.format("Job failed, unable to find an "
                              + "appropriate driver for the device: %s", bmme
                    .getMessage());

            deleted = safeArchiveAsFailure(startTime, msg);
        }
        catch (BadJobException bje) {
            logger.error("Job failed to run due to bad parameters", bje);

            String msg =
                String.format("Job failed, unable to parse the job "
                              + "parameters: %s", bje.getMessage());

            deleted = safeArchiveAsFailure(startTime, msg);
        }
        catch (TooManyTriesException tmte) {
            logger.error("Job has been attempted too many times", tmte);

            String msg =
                String.format("Job failed, it has failed too many "
                              + "times and cannot be attempted again: " + "%s",
                    tmte.getMessage());

            deleted = safeArchiveAsFailure(startTime, msg);
        }
        catch (DatabaseException de) {
            logger.error("Database error prevented job from completing", de);

            String msg =
                String.format("Job failed, unable to communicate "
                              + "with the database: %s", de.getMessage());

            deleted = safeArchiveAsFailure(startTime, msg);
        }
        catch (Throwable th) {
            logger.error("Unexpected error during job execution", th);

            String msg =
                String.format("Job failed due to an unexpected "
                              + "exception %s: %s", th.getClass()
                    .getCanonicalName(), th.getMessage());

            deleted = safeArchiveAsFailure(startTime, msg);
        }
        finally {
            try {
                // Clean up
                // If deleted and automatic, schedule a new instance for the
                // future
                if (deleted && getJobData().automatic) {
                    // Reschedule this job for some point in the future
                    logger.debug("Job is automatic, scheduling a new "
                                 + "instance");

                    try {
                        rescheduleAutomaticJob();
                    }
                    catch (DatabaseException de) {
                        logger.error("Database error prevented scheduling "
                                     + "new instance of automatic job", de);

                        DBLogger.log(null, DBLogger.ERROR, String.format(
                            "Unable to schedule new "
                                + "instance of Job #%d due "
                                + "to a database error: %s",
                            getJobData().jobID, de.getMessage()));
                    }
                }
                else if (!deleted) {
                    if (unlockWhenDone) {
                        // Not deleted? Unlock the job
                        try {
                            getDatabase().unlockJob(getJobData().jobID);
                        }
                        catch (DatabaseException de) {
                            logger.error("Database error prevented job from "
                                         + "unlocking", de);

                            DBLogger.log(null, DBLogger.ERROR, String.format(
                                "Unable to unlock " + "Job #%d due to "
                                    + "database error: %s", getJobData().jobID,
                                de.getMessage()));
                        }
                    }
                }

                if (unlockWhenDone) {
                    try {
                        getDatabase().unlockModem(getJobData().modemID);
                    }
                    catch (DatabaseException de) {
                        logger.error("Database error prevented modem from "
                                     + "unlocking", de);

                        DBLogger.log(null, DBLogger.ERROR, String.format(
                            "Unable to unlock Modem #%d "
                                + "due to database error: %s",
                            getJobData().jobID, de.getMessage()));
                    }
                }
            }
            finally {
                NDC.pop();
                MDC.clear();

                DBLogger.log(getJobData().userID, DBLogger.INFO, String.format(
                    "Job #%d finished in %.3f " + "seconds",
                    getJobData().jobID, duration(startTime, new Date())));
            }
        }
    }

    /**
     * Perform the actions that are required for this job.
     * 
     * @param comm If non-<tt>null</tt>, will be used for communications with
     *            the modem. If <tt>null</tt>, a new
     *            {@link SocketModemCommunicator} will be opened for
     *            communicating with the modem.
     * @param modem The modem to act on.
     * @param startTime The time that the job started; this should be used for
     *            any timestamps going into the database, so that everything can
     *            be easily grouped.
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     * @throws DatabaseException If there is a problem accessing the database.
     * @throws BadModemModuleException If a modem module is requested for the
     *             modem but cannot be obtained.
     * @throws BadJobException If there is a problem with the job parameters.
     */
    protected abstract void runJob(ModemCommunicator comm, Modem modem,
                                   Date startTime)
        throws ModemCommunicationFailedException, DatabaseException,
        BadModemModuleException, BadJobException;

    /**
     * Called when a job fails. This will reschedule the job according to its
     * 'tries' counter.
     * 
     * @throws DatabaseException If there was a database communication error.
     * @throws TooManyTriesException If the job has been attempted too many
     *             times and was not rescheduled.
     */
    protected void rescheduleFailedJob()
        throws DatabaseException, TooManyTriesException
    {

        // Make sure we don't try to schedule beyond the tries limit
        int maxTries = Server.getServerSettings().getIntValue(Setting.MaxTries);
        if (getJobData().tries >= maxTries) {
            throw new TooManyTriesException(String.format(
                "Too many failed attempts for Job #%d", getJobData().jobID));
        }

        int rescheduleDelay;

        // If the job is marked as always available, reschedule for right now
        if (isAlwaysAvailable()) {
            logger.debug("Job is always available, rescheduling for now");
            rescheduleDelay = 0; // Schedule for right now
        }
        else {
            int retryTimeout =
                Server.getServerSettings().getIntValue(
                    Settings.Setting.RetryTimeout);

            int retryMultiplier =
                Server.getServerSettings().getIntValue(
                    Settings.Setting.RetryMultiplier);

            if (getJobData().tries == 0) {
                // It is possible that we may not want to count some failures as
                // failures of the job. In that case, the tries counter may
                // remain at zero, but we still need to add a delay, so count it
                // as a single failure (tries == 1)
                rescheduleDelay = retryTimeout;
            }
            else {
                rescheduleDelay =
                    retryTimeout + (getJobData().tries - 1) * retryMultiplier;
            }
        }

        logger.debug(String.format("Rescheduling the job for %d seconds in "
                                   + "the future", rescheduleDelay));

        // Get a Calendar for the current time
        Calendar cal = Calendar.getInstance();

        // Add the delay to it
        cal.add(Calendar.SECOND, rescheduleDelay);

        // Set it back as our scheduled time
        getJobData().schedule = cal.getTime();
        getDatabase().updateJob(this);

        // Let the user know
        DBLogger.log(null, DBLogger.WARNING, String.format(
            "Job #%d failed, rescheduled for %s", getJobData().jobID, Utils
                .formatStandardDateTime(getJobData().schedule, true)));
    }

    /**
     * Called when an automatic job has just been deleted (either because it
     * succeeded or because it failed too many times). Schedule a new instance,
     * according to the PollingInterval values on the tags of its modem.
     * 
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    protected void rescheduleAutomaticJob()
        throws DatabaseException
    {

        // If -1, job is not rescheduled
        int minimumPollingInterval;

        if (isAlwaysAvailable()) {
            logger.debug("Job is always available, rescheduling for now");
            minimumPollingInterval = 0;
        }
        else {
            logger.debug(String.format("Requesting tags associated with Modem"
                                       + " #%d", getJobData().modemID));
            List<Integer> tags =
                getDatabase().getMatchingTags(null, null, null, null, null,
                    null, getJobData().modemID, 0, -1, null, null);

            minimumPollingInterval = Integer.MAX_VALUE;
            for (int tagID : tags) {
                ModemTag tag = getDatabase().getTag(tagID);

                // A value less than one means to not use the polling interval
                // for
                // this tag
                if (tag.pollingInterval < 1) {
                    continue;
                }

                if (tag.pollingInterval < minimumPollingInterval) {
                    minimumPollingInterval = tag.pollingInterval;
                }
            }

            if (minimumPollingInterval == Integer.MAX_VALUE) {
                logger
                    .warn("No valid polling interval found, using the default");

                // If the modem has no tags, log it as a warning
                if (tags.size() == 0) {
                    DBLogger.log(null, DBLogger.WARNING, String.format(
                        "Modem #%d has no associated"
                            + " tags, scheduling new instance"
                            + " of automatic Job #%d using the"
                            + " default polling interval setting",
                        getJobData().modemID, getJobData().jobID));
                }

                minimumPollingInterval =
                    Server.getServerSettings().getIntValue(
                        Setting.NoTagPollingInterval);
            }

            // Those calls resulted in 0 meaning 'do not reschedule', so check
            // and alter the value as necessary to make -1 mean 'do not
            // reschedule' so 0 can mean 'reschedule for right now'
            if (minimumPollingInterval == 0) {
                minimumPollingInterval = -1;
                return;
            }
        }

        // Value of -1 means don't reschedule
        if (minimumPollingInterval == -1) {
            logger.info("Minimum polling interval was zero, not rescheduling "
                        + "the job");
        }

        // Add 'minimumPollingInterval' seconds to the last
        // scheduled job time and reschedule the job
        logger.debug(String.format("Scheduling the job for %d seconds in the "
                                   + "future", minimumPollingInterval));

        // Get a Calendar for the current scheduled time
        Calendar cal = Calendar.getInstance();

        // Add some seconds to it
        cal.add(Calendar.SECOND, minimumPollingInterval);

        // Set it back as our scheduled time
        getJobData().schedule = cal.getTime();

        // Since it's a new job, it should have a 'tries' value of 0
        getJobData().tries = 0;

        // Schedule this job again
        // This will automatically replace the Job ID of this job
        long oldJobID = getJobData().jobID;
        getDatabase().createJob(this);

        logger.debug(String.format("Job scheduled for %s as Job #%d", Utils
            .formatStandardDateTime(cal.getTime(), true), getJobData().jobID));

        DBLogger.log(null, DBLogger.INFO, String.format(
            "Created new instance of old Job #%d as " + "Job #%d", oldJobID,
            getJobData().jobID));
    }

    /**
     * If no status query job is schedule within the next
     * {@link Setting#UpdatedModemQueryDelay} seconds, this will schedule a new
     * (non-automatic) status query for that many seconds in the future.
     * 
     * @param modem The modem that the job will run against.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    protected void scheduleStatusQueryAsNecessary(Modem modem)
        throws DatabaseException
    {

        // Check for whether a new status query should be scheduled
        int scheduleDelay =
            Server.getServerSettings().getIntValue(
                Setting.UpdatedModemQueryDelay);

        if (scheduleDelay > 0) {
            // Create a Date object representing when the scheduled job
            // should fire
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.SECOND, scheduleDelay);
            Date scheduledTime = cal.getTime();

            // Check for an existing "Status" job against this modem before the
            // end of the timeout period
            Collection<Long> jobs =
                getDatabase().getMatchingJobs(null, null, modem.modemID, null,
                    "Status", null, scheduledTime, null, 0, -1, null, null);

            // If there are none, schedule a new one
            if (jobs.size() == 0) {
                JobData newData =
                    new JobData(JobData.INV_JOB, getJobData().batchID,
                        modem.modemID, getJobData().userID, scheduledTime, 0,
                        "Status", new Tag("job"), false);
                Job newJob = makeGenericJob(newData);

                // If this job is mobile-originated, the new one should be also
                if (getJobData().parameters
                    .getBooleanContents("mobileOriginated")) {
                    Tag moTag = new Tag("mobileOriginated");
                    moTag.addContent("" + true);

                    // Also, make it always available
                    moTag.setAttribute("alwaysAvailable", true);

                    newJob.getJobData().parameters.addContent(moTag);

                    // Schedule mobile-originated jobs for now instead of some
                    // time in the future, since the modem will not connect in
                    // until it is ready (as opposed to server-originated jobs,
                    // where we need to wait for the modem to be ready to accept
                    // the connection)
                    newJob.getJobData().schedule = new Date();
                }

                try {
                    getDatabase().createJob(newJob);

                    DBLogger.log(getJobData().userID, DBLogger.INFO, String
                        .format("Job #%d scheduled new "
                                + "followup status query "
                                + "Job #%d against Modem #%d",
                            getJobData().jobID, newJob.getJobData().jobID,
                            modem.modemID));
                }
                catch (DatabaseException de) {
                    logger.error("Unable to schedule followup job", de);

                    DBLogger.log(getJobData().userID, DBLogger.WARNING, String
                        .format("Job #%d unable to schedule "
                                + "followup status query job "
                                + "against Modem #%d: %s", getJobData().jobID,
                            modem.modemID, de.getMessage()));
                }
            }
        }
    }

    /**
     * Increments this job's 'tries' counter if it would not push it beyond the
     * maximum number of tries allowed.
     * 
     * @throws TooManyTriesException If no more tries are allowed.
     */
    private void incrementTries()
        throws TooManyTriesException
    {

        int maxTries =
            Server.getServerSettings().getIntValue(Settings.Setting.MaxTries);

        if (getJobData().tries >= maxTries) {
            throw new TooManyTriesException();
        }

        getJobData().tries++;
    }

    /**
     * Archive the job to history as a failure, catching any exceptions that may
     * be thrown in the process.
     * 
     * @param startTime The time at which the job was started.
     * @param msg The message to save in the history table.
     * @return True if the job was actually deleted because it had been
     *         attempted too many times, false if the job was simply rescheduled
     *         for a future attempt.
     */
    private boolean safeArchiveAsFailure(Date startTime, String msg)
    {
        // Archive the job as a failure
        try {
            getDatabase().archiveJob(getJobData().jobID, false, msg, startTime);
        }
        catch (DatabaseException de) {
            logger.error("Unable to archive job to history", de);
        }

        try {
            rescheduleFailedJob();
        }
        catch (DatabaseException de) {
            logger.error("Unable to reschedule failed job", de);
        }
        catch (TooManyTriesException tmte) {
            // Delete the job, too many tries
            try {
                getDatabase().deleteJob(getJobData().jobID);

                return true;
            }
            catch (DatabaseException de) {
                logger.error("Unable to delete job with too many tries", de);
            }
            catch (ObjectInUseException oiue) {
                // This should not happen
                logger.error("Job is in use by somebody else?", oiue);
            }
        }

        return false;
    }

    /**
     * Compute the duration (in seconds) of a date range.
     * 
     * @param start The starting date.
     * @param finish The ending date.
     * @return The time elapsed between the two dates, in seconds (with
     *         millisecond precision).
     */
    private float duration(Date start, Date finish)
    {
        long ms = finish.getTime() - start.getTime();

        return (ms / 1000.0f);
    }

    /** A common function for getting the ID number of the object. */
    public long getId()
    {
        return getJobData().jobID;
    }

    /** Check whether this job is marked as always available. */
    private boolean isAlwaysAvailable()
    {
        // It can only be marked 'always available' if it is mobile originated
        if (getJobData().parameters.getBooleanContents("mobileOriginated")) {
            return getJobData().parameters
                .getBooleanAttribute("mobileOriginated alwaysAvailable");
        }
        else {
            return false;
        }
    }

/**
     * Performs common startup code for most job types. Steps taken (may be out
     * of date, best to read the code):
     * <ol>
     * <li>If <code>null</code>, asks the modem for its model number.</li>
     * <li>Asks the modem for its firmware version, whether or not it is already
     * known.</li>
     * <li>Attempts to locate an appropriate {@link ModemModule} for the modem,
     * populating the {@link #modemModule} field with it.</li>
     * <li>Sets the HTTP server on the {@link #modemModule}.</li>
     * <li>If it is <code>null</code>, asks the modem for its device ID.</li>
     * <li>Verifies that the modem is who we think it is, using
     * {@link ModemModule#verifyModem(ModemCommunicator, Modem)}.</li>
     * <li>Clears duplicate IP addresses from the database using {@link #clearDuplicateIPs(Modem).</li>
     * </ol>
     * 
     * @param comm The communicator attached to the modem. Must be connected.
     * @param modem The modem.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     * @throws BadModemModuleException If unable to find an appropriate module
     *             to interface with the modem.
     * @throws DatabaseException If there was a database problem.
     */
    protected void prepareCommunications(ModemCommunicator comm, Modem modem)
        throws ModemCommunicationFailedException, BadModemModuleException,
        DatabaseException
    {
        // If no model was specified, grab it
        if (modem.model == null) {
            logger.debug("Requesting model number from modem");
            modem.model = ModemLib.getModel(comm);
        }

        // Verify the firmware revision (never know when it may change)
        logger.debug("Requesting firmware revision from modem");
        modem.fwVersion = ModemLib.getFWVersion(comm);

        // Get a modem module that can communicate properly
        logger.debug(String.format("Requesting a modem module for a '%s' "
                                   + "running FW '%s'", modem.model,
            modem.fwVersion));
        modemModule = ModemModule.makeModule(modem.model, modem.fwVersion);

        modemModule.setHTTPServer(getHTTPServer());

        // If the device ID is -1, request that
        if (modem.deviceID == null) {
            logger.debug("Requesting device ID for modem");
            modem.deviceID = ModemLib.getDeviceID(comm);
        }

        // Verify that we're talking to the correct modem
        logger.debug("Calling verifyModem() for the modem");
        try {
            modemModule.verifyModem(comm, modem);
        }
        catch (ModemCommunicationFailedException mcfe) {
            // Special case -- not the modem we expect to be talking to
            // Clear the IPAddress, as it is obviously incorrect
            if (Server.getServerSettings().getIntValue(
                Setting.ClearDuplicateIPs) == 1) {

                modem.ipAddress = null;
                getDatabase().updateModem(modem);
            }

            throw new ModemCommunicationFailedException(
                "IP Address points at wrong modem;" +
                    " cleared to avoid future confusion: " + mcfe.getMessage(),
                mcfe);
        }

        clearDuplicateIPs(modem);
    }

    /**
     * Clear the IP address field of all modems that currently have the same IP
     * address as the given modem. If the {@link Setting#ClearDuplicateIPs}
     * setting is 0, this will be a no-op.
     * 
     * @param skipModem The modem whose IP address should NOT be reset. All
     *            other modems with this same address will have theirs set to
     *            <code>null</code>.
     * @throws DatabaseException If there was a problem interacting with the
     *             database.
     */
    protected void clearDuplicateIPs(Modem skipModem)
        throws DatabaseException
    {
        if (Server.getServerSettings().getIntValue(Setting.ClearDuplicateIPs) == 0) {
            return;
        }

        if (skipModem.ipAddress == null) {
            // Don't even try; we'd clear the IP addresses of ALL of the modems
            return;
        }

        Collection<Integer> badModemIDs =
            getDatabase().getMatchingModems(null, null, null, null, null, null,
                skipModem.ipAddress, modem.port, null, null, 0, -1, null, null);
        badModemIDs.remove(skipModem.modemID);

        // I'd rather grab all the Modems, wasting the ones that are locked and
        // we cannot update, rather than grab them one at a time
        // This is not likely to ever be larger than a handful anyway
        Map<Integer, Modem> badModems = getDatabase().getModems(badModemIDs);

        for (Modem modem : badModems.values()) {
            // Best effort to clear IPs; missing one will do no damage, but may
            // may cause a future job to fail because the IP points at the wrong
            // modem
            // Only update modems that were unlocked; we do not want to mess up
            // another job running on this server
            if (getDatabase().lockModem(modem.modemID, 0) == LockResult.Acquired) {
                try {
                    modem.ipAddress = null;
                    getDatabase().updateModem(modem);
                }
                finally {
                    getDatabase().unlockModem(modem.modemID);
                }
            }
        }
    }

    /**
     * Tell the job whether to unlock itself and its modem in the database when
     * it has completed. By default, it will unlock these items.
     * 
     * @param val True = unlock the job/modem when complete, false = do not
     *            unlock the job/modem when complete. This does not affect
     *            whether the job will be deleted as part of its execution and
     *            cleanup. Any newly spawned copy of the job will not be locked.
     */
    public void setUnlockWhenDone(boolean val)
    {
        unlockWhenDone = val;
    }

    /**
     * Check whether the job will unlock itself and its modem when it completes
     * (the default is to unlock).
     */
    public boolean getUnlockWhenDone()
    {
        return unlockWhenDone;
    }

    /**
     * Set whether the modem likely rebooted as a result of this job running.
     * Default if this is never called is <code>false</code>.
     * 
     * @param val <code>true</code> if the modem likely rebooted, breaking its
     *            connection; <code>false</code> otherwise. It is better to set
     *            this to <code>true</code> than to leave it false if you are
     *            unsure.
     */
    public void setModemLikelyRebooted(boolean val)
    {
        modemLikelyRebooted = val;
    }

    /**
     * Check whether the modem likely rebooted as a result of this job running.
     * 
     * @return <code>true</code> if the modem likely rebooted, breaking its
     *         connection; <code>false</code> otherwise.
     */
    public boolean getModemLikelyRebooted()
    {
        return modemLikelyRebooted;
    }
}

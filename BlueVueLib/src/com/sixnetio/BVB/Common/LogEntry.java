/*
 * LogEntry.java
 *
 * Represents an entry in the database log.
 *
 * Jonathan Pearson
 * January 22, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.sql.Timestamp;
import java.util.Date;

public class LogEntry {
	/**
	 * Used to represent an unknown log entry; will never show up in the
	 * database.
	 */
	public static final long INV_LOG = -1;
	
	public long logID; // Will be set if loaded from the database; otherwise -1
	public final Timestamp timestamp;
	public final Integer userID;
	public final String type;
	public final String description;
	
	public LogEntry(java.util.Date timestamp, Integer userID, String type, String description) {
		this(INV_LOG, timestamp, userID, type, description);
	}
	
	public LogEntry(long logID, java.util.Date timestamp, Integer userID, String type,
	        String description) {
		this.logID = logID;
		this.timestamp = new Timestamp(timestamp.getTime());
		this.type = type;
		this.userID = userID;
		this.description = description;
	}
	
	@Override
	public String toString() {
		if (userID != null) {
			return String.format("[%1$tF %1$tT] (by user %2$d): %3$s: %4$s",
			                     new Date(timestamp.getTime()), userID, type, description);
		} else {
			return String.format("[%1$tF %1$tT] %2$s %3$s", new Date(timestamp.getTime()), type,
			                     description);
		}
	}
	
	/** A common function for getting the ID number of the object. */
	public long getId() {
		return logID;
	}
}

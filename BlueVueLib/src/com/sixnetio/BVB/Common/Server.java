/*
 * Server.java
 *
 * Represents the server itself, providing functions for querying properties of it.
 *
 * Jonathan Pearson
 * January 21, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.*;

import org.apache.log4j.*;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.Restrictions.Restriction;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

/**
 * General server-wide data to make everybody's life easier.
 *
 * @author Jonathan Pearson
 */
public class Server {
	/**
	 * Keeps the server settings up-to-date.
	 *
	 * @author Jonathan Pearson
	 */
	private static class SettingsRefresher extends Thread implements ShutdownListener {
		private Database db;
		private volatile boolean running;
		
		/**
		 * Construct a new Settings Refresher.
		 * 
		 * @param db The database that we will pull settings from.
		 */
		public SettingsRefresher(Database db) {
			super("Settings Refresher");
			
			this.db = db;
		}
		
		/**
		 * Public only as an implementation detail, do not call this.
		 */
		@Override
		public void run() {
			running = true;
			Server.registerShutdownListener(this);
			
			try {
				while (running) {
					try {
						Server.serverSettings = new Settings(db);
						
						// Make sure it keeps its date restriction up-to-date
						db.updateLicenseKeyDateRestriction();
					} catch (DatabaseException de) {
						logger.error("Failed to acquire fresh settings", de);
					}
					
					String serverShutdown =
					        Server.getServerSettings().getStringValue(Setting.ServerShutdown);
					if (serverShutdown != null) {
						boolean shutdown = false;
						
						if (serverShutdown.equals(Server.getName())) {
							// This server has been instructed to shut down
							shutdown = true;
							
							// Update the database so we won't shut down
							// immediately
							// next time we start up
							try {
								db.updateSetting(Setting.ServerShutdown.properName, null);
							} catch (DatabaseException de) {
								logger.error("Failed to clear the server shutdown setting", de);
							}
						} else if (serverShutdown.equalsIgnoreCase("all")) {
							// All servers have been instructed to shut down
							shutdown = true;
							
							// Don't update the database, since that would
							// prevent other servers from finding out about this
							// instruction
							
							// Log the message, in case someone is trying to
							// figure out why suddenly nothing works
							logger.warn("All servers instructed to shut down");
						}
						
						if (shutdown) {
							// Shut down the server
							Server.shutdown("Instructed to shut down");
							
							// We can speed up the shutdown like this
							running = false;
							continue;
						}
					}
					
					// If there is a date restriction on the license key
					String dateLimit = getLicenseKey().getRestrictions()
						.getStringValue(Restriction.DateLimit);
					
					Long expireMS = null;
					if (dateLimit != null) {
						// If we are within 30 days of timeout...
						try {
							expireMS = Utils.parseStandardDateTime(dateLimit)
								.getTime();
						} catch (ParseException pe) {
							// This is unexpected... Must have been an error on
							//   the side of the license issuer, though, so just
							//   pretend that there is no expiration
						}
					}
					
					if (expireMS != null) {
						long nowMS = System.currentTimeMillis();
						long timeLeftMS = expireMS - nowMS;
						long timeLeftDays = timeLeftMS / (1000 * 60 * 60 * 24);
						
						// If we are within 30 days of expiration, we want to
						//   log an info or warning message to the DB
						if (timeLeftDays <= 30) {
							// Only do it once per day, though
							// Check whether we are within one update timeout of
							//   midnight
							Calendar cal = Calendar.getInstance();
							if (cal.get(Calendar.HOUR_OF_DAY) == 0) {
								cal.add(Calendar.SECOND,
								        -Server.getServerSettings()
								        	.getIntValue(Setting
								        	             .SettingPollInterval));
								
								if (cal.get(Calendar.HOUR_OF_DAY) != 0) {
									// Subtracting the SettingPollInterval moved
									//   from today to yesterday, add the log
									//   message
									String messageType;
									if (timeLeftDays <= 7) {
										messageType = DBLogger.WARNING;
									} else {
										messageType = DBLogger.INFO;
									}
									
									DBLogger.log(null, messageType,
									             "Your BlueVue Group license " +
									             "will expire in " +
									             timeLeftDays + " days");
								}
							}
						}
					}
					
					sleepAsNecessary();
				}
			} catch (IllegalStateException ise) {
				// Database was probably shut down, we should probably shut down
				// too
				running = false;
				
				logger.debug("Caught an IllegalStateException, assuming database went down", ise);
			} finally {
				Server.unregisterShutdownListener(this);
			}
		}
		
		/**
		 * Sleeps for {@link Setting#SettingPollInterval} or shuts down the
		 * Settings Refresher thread as necessary.
		 */
		private void sleepAsNecessary() {
			// Keep trying every until we get a real value
			int sleepFor = Server.getServerSettings().getIntValue(Setting.SettingPollInterval);
			
			if (sleepFor == 0) {
				// Value specifies not to try again
				running = false;
			} else {
				Utils.sleep(sleepFor * 1000);
			}
		}
		
		@Override
		public void serverPanic() {
			running = false;
			interrupt();
			
			try {
				join();
			} catch (InterruptedException ie) {
				throw new InterruptedOperationException(ie);
			}
		}
		
		@Override
		public void serverShutdown() {
			stopRunning();
		}
		
		/**
		 * Simple function to stop the thread.
		 */
		public void stopRunning() {
			running = false;
			
			try {
				join();
			} catch (InterruptedException ie) {
				throw new InterruptedOperationException(ie);
			}
		}
		
		@Override
		public int getShutdownOrder() {
			return 0; // Not interested in shutdown order
		}
	}
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** The name of the server. */
	private static final String name;
	
	/** Each server has a specific type, this will hold that type as necessary. */
	private static String nameSuffix = null;
	
	/** If the server is shutting down, this gets set. */
	private static boolean shuttingDown = false;
	
	/** The settings refresher thread. */
	private static SettingsRefresher settingsRefresher;
	
	/** The Settings object that the Settings Refresher refreshes. */
	private static Settings serverSettings = new Settings();
	
	/**
	 * Directory for system-wide configuration files. On Linux this is
	 * "/etc/BlueVueBatch". On Windows, this is
	 * "ALLUSERSPROFILE/[Application Data]/BlueVueBatch" (with [Application
	 * Data] being the name of the directory after the user name in APPDATA).
	 * (All-caps words are names of environment variables.)
	 */
	public static final String CFG_SYSTEM;
	
	/**
	 * Directory for user override configuration files. On Linux, this is
	 * "~/.bvb". On Windows, this is "APPDATA/BlueVueBatch". (All-caps words are
	 * names of environment variables.)
	 */
	public static final String CFG_USER;
	
	/** List of shutdown listeners to notify on shutdown. */
	private static List<ShutdownListener> shutdownListeners = new Vector<ShutdownListener>();
	
	static {
		// Local copies of the static final variables
		// These make it easy to only assign to them once
		String l_name = "";
		String l_CFG_SYSTEM = "";
		String l_CFG_USER = "";
		
		try {
			l_name = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException uhe) {
			panic("Unable to acquire host name of local machine", uhe);
		}
		
		if (Utils.osIsWindows()) {
			// Grab the All Users directory
			String allUsers = System.getenv("ALLUSERSPROFILE");
			if (allUsers == null) {
				panic("ALLUSERSPROFILE environment variable not defined");
			}
			
			// The configuration directory should be All Users/Application
			// Data/BlueVueBatch
			l_CFG_SYSTEM = allUsers + File.separator +
				"Application Data" + File.separator +
				"BlueVueBatch";
			
			// If possible, set CFG_USER to %APPDATA%/BlueVueBatch
			// This will come back as null if we are running under a system
			//   account (like in a web server...)
			String curUserAppData = System.getenv("APPDATA");
			if (curUserAppData != null) {
				l_CFG_USER = curUserAppData + File.separator +
					"BlueVueBatch";
			}
		} else {
			// Ahh, a real OS... So much easier :-)
			l_CFG_SYSTEM = "/etc/BlueVueBatch";
			l_CFG_USER = System.getProperty("user.home") + File.separator +
				".bvb";
		}
		
		name = l_name;
		CFG_SYSTEM = l_CFG_SYSTEM;
		CFG_USER = l_CFG_USER;
	}
	
	/**
	 * Configure this program instance. This searches for a Log4j properties
	 * file and a program configuration file. If the Log4j properties file is
	 * found, it is read to configure Log4j and then checked for updates every 5
	 * seconds. If a program configuration file is found, it is returned.
	 * 
	 * Search order for the Log4j properties file is:
	 * <ol>
	 * <li>./log4j.properties</li>
	 * <li><tt>CFG_USER</tt>/<tt>progName</tt>.log4j.properties</li>
	 * <li><tt>CFG_SYSTEM</tt>/<tt>progName</tt>.log4j.properties</li>
	 * </ol>
	 * 
	 * Search order for the configuration file is:
	 * <ol>
	 * <li><tt>CFG_USER</tt>/<tt>progName</tt>.rc</li>
	 * <li><tt>CFG_SYSTEM</tt>/<tt>progName</tt>.rc</li>
	 * </ol>
	 * 
	 * @param progName The name of the program (prefixed to configuration file
	 *            names).
	 * @return The name of the main configuration file, or <tt>null</tt> if one
	 *         could not be found.
	 */
	public static String configureInstance(String progName) {
		// Configure Log4j
		String log4jNamedFile = File.separator + progName + ".log4j.properties";
		
		String log4jProperties =
		        Utils.getFirstExistingFile(new String[] {
		                "log4j.properties",
		                CFG_USER + log4jNamedFile,
		                CFG_SYSTEM + log4jNamedFile,
		        });
		
		if (log4jProperties == null) {
			// Disable logging
			Logger.getRootLogger().setLevel(Level.OFF);
			
			// But watch for user's home directory log file
			PropertyConfigurator.configureAndWatch(CFG_USER + log4jNamedFile,
			                                       15000);
		} else {
			PropertyConfigurator.configureAndWatch(log4jProperties, 15000);
		}
		
		String cfgFilePath =
		        Utils.getFirstExistingFile(new String[] {
		                CFG_USER + File.separator + progName + ".rc",
		                CFG_SYSTEM + File.separator + progName + ".rc"
		        });
		
		logger.debug(String.format("Config file location is '%s'", cfgFilePath));
		
		return cfgFilePath;
	}
	
	/**
	 * Before expecting the server settings to be up to date, start one of these
	 * to periodically request settings from the database.
	 * 
	 * @param db The database to request from.
	 */
	public static synchronized void startSettingsRefresher(Database db) {
		if (settingsRefresher != null) {
			settingsRefresher.stopRunning();
			settingsRefresher = null;
		}
		
		settingsRefresher = new SettingsRefresher(db);
		settingsRefresher.start();
	}
	
	/**
	 * If you are running outside of a server instance, you may wish to have
	 * accurate settings anyway; if that is the case, call this function.
	 * 
	 * @param db The database to read settings from.
	 * @throws DatabaseException If there is a problem communicating with the
	 *             database.
	 */
	public static void refreshSettingsNow(Database db) throws DatabaseException {
		serverSettings = new Settings(db);
		
		// Make sure the database knows when the license key will expire
		db.updateLicenseKeyDateRestriction();
	}
	
	/**
	 * Get the server settings object.
	 */
	public static Settings getServerSettings() {
		return serverSettings;
	}
	
	/**
	 * Get the license key from the settings. This requires that the server
	 * settings have been read.
	 * 
	 * @return The license key from the settings, or <tt>null</tt> if there
	 *         isn't a parseable/valid one (this will cause a server panic).
	 */
	public static LicenseKey getLicenseKey() {
		String licenseKeyData = getServerSettings().getStringValue(Setting.LicenseKey);
		
		Tag licenseKeyTag;
		
		try {
			licenseKeyTag = Utils.parseXML(licenseKeyData, false);
		} catch (IOException ioe) {
			logger.fatal("Database contains an improperly formatted license key",
			             ioe);
			
			Server.panic("Database contains an improperly formatted license key",
			             ioe);
			
			return null;
		}
		
		LicenseKey licenseKey = new LicenseKey(licenseKeyTag);
		
		if (!licenseKey.isValid()) {
			Server.panic("Database contains an invalid license key");
			
			return null;
		}
		
		return licenseKey;
	}
	
	/**
	 * Each server type should have a name suffix, allowing individual servers
	 * of different types to run on the same host without confusing the
	 * Capabilities table.
	 * 
	 * @param suffix The suffix for this server's name.
	 */
	public static void setNameSuffix(String suffix) {
		nameSuffix = suffix;
	}
	
	/**
	 * Get the name suffix for this server.
	 */
	public static String getNameSuffix() {
		return nameSuffix;
	}
	
	/**
	 * Get the name of the server.
	 * 
	 * @return The name of the server.
	 */
	public static String getName() {
		if (nameSuffix != null) {
			return String.format("%s-%s", name, nameSuffix);
		} else {
			return name;
		}
	}
	
	/**
	 * Shortcut for <tt>panic(msg, null)</tt>.
	 */
	public static void panic(String msg) {
		panic(msg, null);
	}
	
	/**
	 * Server panic - log and print a message to standard error and crash-land
	 * the VM.
	 * 
	 * @param msg The message to print (prepended with "Server panic: ").
	 * @param where If there was an exception of some sort available to describe
	 *            where the panic occurred or what caused it, otherwise just
	 *            pass <tt>null</tt>.
	 */
	public static void panic(final String msg, final Throwable where) {
		Thread th = new Thread(new Runnable() {
			public void run() {
				if (where != null) {
					logger.fatal(String.format("Server panic: %s", msg), where);
				} else {
					logger.fatal(String.format("Server panic: %s", msg));
				}
				
				System.err.printf("Server panic: %s\n", msg);
				
				if (where != null) {
					where.printStackTrace(System.err);
				}
				
				shuttingDown = true;
				
				// Shutdown listeners are kept in order by the register method
				synchronized (shutdownListeners) {
					for (ShutdownListener sl : shutdownListeners) {
						try {
							logger.debug(String.format("Telling a '%s' to panic", sl.getClass()
							                                                        .getName()));
							sl.serverPanic();
						} catch (Throwable th) {
							logger.error("Shutdown listener threw an exception", th);
						}
					}
				}
				
				// Print the message again, since the first one can get lost in
				// the jumble
				if (where != null) {
					logger.fatal(String.format("Server panic: %s", msg), where);
				} else {
					logger.fatal(String.format("Server panic: %s", msg));
				}
				System.exit(1);
			}
		}, "Server Panic");
		
		th.start();
	}
	
	/**
	 * Standard server shutdown - log a message and shutdown gracefully.
	 * 
	 * @param msgs A list of messages to concatenate into a single message and
	 *            log. Spaces will be placed between each message. This is
	 *            passed like this to support JSLWin, a Java Service Launcher
	 *            for Windows. You'll probably only want to pass a single
	 *            message here.
	 */
	public static void shutdown(String... msgs) {
		final String msg = Utils.combineStrings(msgs, " ");
		
		Thread th = new Thread(new Runnable() {
			public void run() {
				logger.info(String.format("Server is shutting down: %s", msg));
				
				shuttingDown = true;
				
				// Shutdown listeners are kept in order by the register method
				synchronized (shutdownListeners) {
					for (ShutdownListener sl : shutdownListeners) {
						try {
							if (sl instanceof Thread) {
								logger.debug(String.format("Shutting down '%s' thread",
								                           ((Thread)sl).getName()));
							} else {
								logger.debug(String.format("Telling a '%s' to shut down",
								                           sl.getClass().getName()));
							}
							
							sl.serverShutdown();
						} catch (Throwable th) {
							logger.error("Shutdown listener threw an exception", th);
						}
					}
				}
				
				// Print the message again, since that first one can get lost in
				// the jumble
				logger.info(String.format("Server is shutting down: %s", msg));
				logger.debug("Killing the VM");
				
				System.exit(0);
			}
		}, "Server Shutdown");
		
		th.start();
	}
	
	/**
	 * Register a listener for shutdown events. This will do nothing if the
	 * server is in the middle of a shutdown when this is called.
	 * 
	 * @param sl The ShutdownListener to register.
	 */
	public static void registerShutdownListener(ShutdownListener sl) {
		// Don't want to modify the list while shutting down, since there's an
		// iterator running over it that would crash
		if (shuttingDown) return;
		
		synchronized (shutdownListeners) {
			// Insert in order
			// There should never be very many listeners, so a simple linear
			// search
			// should be fine
			for (int i = 0; i < shutdownListeners.size(); i++) {
				if (shutdownListeners.get(i).getShutdownOrder() >= sl.getShutdownOrder()) {
					shutdownListeners.add(i, sl);
					return;
				}
			}
			
			// Didn't find a good insertion point? Add at the end
			shutdownListeners.add(sl);
		}
	}
	
	/**
	 * Unregister a listener for shutdown events. This will do nothing if the
	 * server is in the middle of a shutdown when this is called.
	 * 
	 * @param sl The ShutdownListener to unregister.
	 */
	public static void unregisterShutdownListener(ShutdownListener sl) {
		// Don't want to modify the list while shutting down, since there's an
		// iterator running over it that would crash
		if (shuttingDown) return;
		
		synchronized (shutdownListeners) {
			shutdownListeners.remove(sl);
		}
	}
	
	/**
	 * Get the current time.
	 */
	public static Date getCurrentTime() {
		return Calendar.getInstance().getTime();
	}
	
	/**
	 * Get a copyright banner string for the given program and version. This is
	 * mainly intended for CLI programs. Other programs (such as the Web UI)
	 * provide their own implementation. See
	 * BlueVueWeb/web/fragments/footer.html for the Web UI file that needs
	 * updating.
	 * 
	 * @param progName The program name.
	 * @param progVersion The program version.
	 * @return A copyright banner that looks something like this:
	 * <pre>
	 *   BlueVue Batch v. 2.1.25
	 *     Part of the BlueVue Group Suite
	 *     Copyright 2009 SIXNET, LLC
	 * </pre>
	 */
	public static String copyrightBanner(String progName, String progVersion) {
		return String.format("%s v. %s\n" +
		                     "  Part of the BlueVue Group Suite\n" +
		                     "  Copyright 2009 SIXNET, LLC",
		                     progName, progVersion);
    }
}

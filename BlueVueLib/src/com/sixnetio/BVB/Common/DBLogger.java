/*
 * DBLogger.java
 *
 * Logs log entries to the database. Only start this thread if you
 * actually want database logging from your program. You may leave
 * the thread un-started and still use the log function; no memory
 * leaking will occur, and the log messages will go out through
 * Log4j.
 *
 * Jonathan Pearson
 * January 22, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

public class DBLogger extends Thread implements ShutdownListener {
	/** Log type string indicating an error. */
	public static final String ERROR = "Error";
	
	/** Log type string indicating a warning. */
	public static final String WARNING = "Warning";
	
	/** Log type string indicating an informational message. */
	public static final String INFO = "Info";
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	private static final Logger offlineLogger =
	        Logger.getLogger(DBLogger.class.getName() + ".Offline");
	
	private static DBLogger theDBLogger;
	
	private Database db;
	private boolean running;
	
	// FIXME: Should this be a blocking queue of some sort?
	private Queue<LogEntry> logQueue = new LinkedList<LogEntry>();
	
	/**
	 * Construct a new logger for logging to the database.
	 * 
	 * @param db The database to log to.
	 */
	private DBLogger(Database db) {
		super("DBLogger");
		
		this.db = db;
	}
	
	/**
	 * Initialize and start the logger.
	 * 
	 * @param db The database to log against.
	 */
	public static synchronized void initialize(Database db) {
		if (theDBLogger != null) throw new IllegalStateException(
		                                                         "Cannot have multiple DBLogger objects");
		
		theDBLogger = new DBLogger(db);
		theDBLogger.start();
	}
	
	/**
	 * Get the one instance of this class that exists.
	 */
	public static DBLogger getDBLogger() {
		return theDBLogger;
	}
	
	/**
	 * A shortcut for {@link #getDBLogger()}.
	 * {@link #logMessage(Integer, String)}. The message will only go to the
	 * standard logger if there is no DBLogger object.
	 */
	public static void log(Integer userID, String type, String description) {
		DBLogger dbLogger = getDBLogger();
		
		if (dbLogger != null) {
			dbLogger.logMessage(userID, type, description);
		} else {
			// Send it to the logger, since it cannot go to the database
			LogEntry logEntry = new LogEntry(Server.getCurrentTime(), userID, type, description);
			
			logOffline(logEntry);
		}
	}
	
	/**
	 * Log a message attributed to a specific user. Wait until the thread has
	 * started before calling this, otherwise entries will not make it to the
	 * database.
	 * 
	 * @param userID The user who caused the action, or <tt>null</tt> if no user
	 *            is associated with it.
	 * @param description A description of the action.
	 */
	public void logMessage(Integer userID, String type, String description) {
		LogEntry logEntry = new LogEntry(Server.getCurrentTime(), userID, type, description);
		
		// Best to have it go both places
		logOffline(logEntry);
		
		// Note: There is a little bit of a race condition here:
		// If the thread is shut down normally (via setting 'running = false'),
		// some messages may have gotten queued for logging but will never make
		// it to the server
		if (running) {
			// No point in actually building up a queue if there is no drain on
			// it
			synchronized (logQueue) {
				logQueue.add(logEntry);
			}
		}
	}
	
	@Override
	public void run() {
		if (Server.getServerSettings().getIntValue(Setting.LogDumpInterval) == 0) {
			// Setting says that this thread should not run
			running = false;
			return;
		}
		
		Server.registerShutdownListener(this);
		
		try {
			while (running) {
				// Purge the database, if necessary
				int loggingCutoff = Server.getServerSettings().getIntValue(Setting.LoggingCutoff);
				if (loggingCutoff == -1) {
					// Logging to the database is disabled
					// Don't do anything but sleep for SettingPollInterval
					// seconds
					int settingPollInterval =
					        Server.getServerSettings().getIntValue(Setting.SettingPollInterval);
					
					if (settingPollInterval == 0) {
						// Not supposed to poll for settings, logging is
						// disabled... might as well kill the
						// thread, it isn't possible for the current state to
						// change
						running = false;
						continue;
					} else {
						Utils.sleep(settingPollInterval * 1000);
						continue;
					}
				} else if (loggingCutoff == 0) {
					// Never purge the database
				} else {
					// Purge logging entries older than 'loggingCutoff' seconds
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.SECOND, -loggingCutoff);
					
					try {
						db.purgeLog(cal.getTime());
					} catch (DatabaseException de) {
						logger.error("Unable to purge old log entries from the database", de);
						
						// Continue normally, though, perhaps we can still log
						// entries
					}
				}
				
				performUpdate();
				
				int sleepFor = Server.getServerSettings().getIntValue(Setting.LogDumpInterval);
				
				if (sleepFor == 0) {
					// Setting says this thread should not run
					running = false;
				} else {
					Utils.sleep(sleepFor * 1000);
				}
			}
			
			// Dump any remaining messages before actually stopping
			performUpdate();
		} catch (IllegalStateException ise) {
			// Database was probably shut down, we should probably shut down too
			running = false;
			
			logger.debug("Caught an IllegalStateException, assuming database went down", ise);
		} finally {
			Server.unregisterShutdownListener(this);
			
			// Just throw out anything left to avoid eating memory
			synchronized (logQueue) {
				logQueue.clear();
			}
		}
	}
	
	/**
	 * Dump all pending log entries to the database. If that fails, the queue
	 * will be retained, and we will try to dump it on the next time around.
	 */
	public void performUpdate() {
		Queue<LogEntry> pending = new LinkedList<LogEntry>();
		
		synchronized (logQueue) {
			pending.addAll(logQueue);
			logQueue.clear();
			
			logger.debug(String.format("Pulled %d entries out of the log queue, now contains %d",
			                           pending.size(), logQueue.size()));
		}
		
		try {
			db.log(pending);
		} catch (DatabaseException de) {
			logger.error("Unable to log entries to database", de);
			
			synchronized (logQueue) {
				// Add everything back to the standard queue and try to
				// dump it on the next round
				logQueue.addAll(pending);
				
				logger
				      .debug(String
				                   .format(
				                           "Dumped %d entries back into the log queue, now contains %d",
				                           pending.size(), logQueue.size()));
			}
		}
	}
	
	/**
	 * Called when the server panics. Shuts down the logging thread, but waits
	 * for it to finish.
	 */
	@Override
	public void serverPanic() {
		shutdown();
	}
	
	/**
	 * Called when the server is shutting down. Shuts down the logging thread
	 * and waits for it to finish.
	 */
	@Override
	public void serverShutdown() {
		shutdown();
	}
	
	/**
	 * Shut down this thread, and wait for it to terminate.
	 * 
	 * @throws InterruptedOperationException If this thread is interrupted while
	 *             waiting.
	 */
	public void shutdown() {
		running = false;
		
		try {
			join();
		} catch (InterruptedException ie) {
			throw new InterruptedOperationException(ie);
		}
	}
	
	/**
	 * This prevents the race condition where the thread is started and a log
	 * message is immediately passed in.
	 */
	@Override
	public void start() {
		running = true;
		
		super.start();
	}
	
	private static void logOffline(LogEntry logEntry) {
		if (logEntry.type.equals(ERROR)) {
			offlineLogger.error(logEntry.toString());
		} else if (logEntry.type.equals(WARNING)) {
			offlineLogger.warn(logEntry.toString());
		} else if (logEntry.type.equals(INFO)) {
			offlineLogger.info(logEntry.toString());
		} else {
			offlineLogger.debug(logEntry.toString());
		}
	}
	
	@Override
	public int getShutdownOrder() {
		// Go down after most other threads, but before the database
		return 100;
	}
}

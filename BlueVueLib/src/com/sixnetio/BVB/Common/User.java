/*
 * User.java
 *
 * Represents a user.
 *
 * Jonathan Pearson
 * April 13, 2009
 *
 */

package com.sixnetio.BVB.Common;

import org.jonp.xml.Tag;

import com.sixnetio.util.Utils;

public class User {
	/** Set the user's password to this to prevent logins using this account. */
	public static final String NOLOGON_PASSWORD = "*";
	
	/**
	 * A User ID that will never appear in the database, used for unset User
	 * IDs.
	 */
	public static final int INV_USER = -1;
	
	public static enum UserType {
		/**
		 * A user with nearly read-only privileges (can create one-off status
		 * jobs and edit his own preferences and password).
		 */
		Basic("Basic"),

		/** A user with some write privileges. */
		Enhanced("Enhanced"),

		/** A user with full access. */
		Root("Root");
		
		public final String typeName;
		
		private UserType(String typeName) {
			this.typeName = typeName;
		}
		
		@Override
		public String toString() {
			return typeName;
		}
		
		/**
		 * Get the UserType object with the specified type name.
		 * 
		 * @param typeName The type name.
		 * @return The UserType object, or <tt>null</tt> if no match was found.
		 */
		public static UserType find(String typeName) {
			for (UserType type : values()) {
				if (type.typeName.equals(typeName)) return type;
			}
			
			return null;
		}
	}
	
	/** The user ID number from the database. */
	public int userID;
	
	/** The name of the user. */
	public String name;
	
	/**
	 * The hashed password of the user (see
	 * {@link #hashPassword(String, String)}).
	 */
	public String hashedPassword;
	
	/** The type of user. */
	public UserType type;
	
	/** The user's preferences. */
	public Tag preferences;
	
	/** The number of jobs that this user owns. */
	public int jobCount;
	
	/** The number of batches that this user owns. */
	public int batchCount;
	
	/**
	 * Construct a new user.
	 * 
	 * @param userID The ID number of the user (pass {@link #INV_USER} if you
	 *            are creating a new user).
	 * @param name The name of the user.
	 * @param hashedPassword The hashed password for the user (see
	 *            {@link #hashPassword(String, String)}).
	 * @param type The type of user.
	 * @param preferences The user's preferences, or <tt>null</tt> if there are
	 *            none yet.
	 */
	public User(int userID, String name, String hashedPassword, UserType type, Tag preferences) {
		this(userID, name, hashedPassword, type, preferences, 0, 0);
	}
	
	/**
	 * Construct a new user, including statistical data. This should only be
	 * called by database retrieval methods.
	 * 
	 * @param userID The ID number of the user (pass -1 if you are creating a
	 *            new user).
	 * @param name The name of the user.
	 * @param hashedPassword The hashed password for the user (see
	 *            {@link #hashPassword(String, String)}).
	 * @param type The type of user.
	 * @param preferences The user's preferences, or <tt>null</tt> if there are
	 *            none yet.
	 * @param jobCount The number of active jobs that are owned by this user.
	 * @param batchCount The number of active batches that are owned by this
	 *            user.
	 */
	public User(int userID, String name, String hashedPassword, UserType type, Tag preferences,
	        int jobCount, int batchCount) {
		this.userID = userID;
		this.name = name;
		this.hashedPassword = hashedPassword;
		this.type = type;
		
		if (preferences == null) preferences = new Tag("prefs");
		this.preferences = preferences;
		
		this.jobCount = jobCount;
		this.batchCount = batchCount;
	}
	
	/**
	 * A shortcut to check whether this user is enabled.
	 * 
	 * @return True = the user should be able to log in, false = the user is
	 *         disabled and cannot log in.
	 */
	public boolean getEnabled() {
		return !this.hashedPassword.trim().equals(NOLOGON_PASSWORD);
	}
	
	/**
	 * Create a hashed password for a user.
	 * 
	 * @param userName The name of the user.
	 * @param plainPassword The password of the user.
	 * @return A SHA1 hash of the UTF-8 encoding of the string
	 *         "&lt;name&gt;:&lt;password&gt;".
	 */
	public static String hashPassword(String userName, String plainPassword) {
		return Utils.sha1sum(String.format("%s:%s", userName, plainPassword).getBytes());
	}
	
	/**
	 * A common function for retrieving object IDs.
	 */
	public int getId() {
		return userID;
	}
}

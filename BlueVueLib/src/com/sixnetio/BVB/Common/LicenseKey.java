/*
 * LicenseKey.java
 *
 * Represents a license key, along with functions for creating/checking keys.
 *
 * Jonathan Pearson
 * May 13, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.io.*;
import java.security.*;
import java.security.spec.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;

/**
 * Represents a license key.
 * 
 * <p>
 * To verify a license key, construct with either {@link #LicenseKey(File)} or
 * {@link #LicenseKey(Tag)} and call {@link #isValid()}.
 * </p>
 * 
 * <p>
 * To generate a new license key, construct with
 * {@link #LicenseKey(String, String, String, String, Tag, Tag)} and call
 * {@link #getLicense()}.
 * </p>
 * 
 * @author Jonathan Pearson
 */
public class LicenseKey {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private static final byte[] PUBLIC_KEY_DATA = new byte[] {
        (byte)0x30, (byte)0x82, (byte)0x02, (byte)0x22,
        (byte)0x30, (byte)0x0d, (byte)0x06, (byte)0x09,
        (byte)0x2a, (byte)0x86, (byte)0x48, (byte)0x86,
        (byte)0xf7, (byte)0x0d, (byte)0x01, (byte)0x01,
        (byte)0x01, (byte)0x05, (byte)0x00, (byte)0x03,
        (byte)0x82, (byte)0x02, (byte)0x0f, (byte)0x00,
        (byte)0x30, (byte)0x82, (byte)0x02, (byte)0x0a,
        (byte)0x02, (byte)0x82, (byte)0x02, (byte)0x01,
        (byte)0x00, (byte)0xdb, (byte)0x30, (byte)0x24,
        (byte)0x9b, (byte)0x74, (byte)0xfe, (byte)0xce,
        (byte)0xb7, (byte)0xb9, (byte)0xae, (byte)0xb6,
        (byte)0x21, (byte)0x41, (byte)0x7b, (byte)0xd3,
        (byte)0x94, (byte)0xa3, (byte)0x5b, (byte)0x85,
        (byte)0xb2, (byte)0x2c, (byte)0x2d, (byte)0x9d,
        (byte)0xdb, (byte)0xd2, (byte)0x63, (byte)0x15,
        (byte)0x7e, (byte)0x7b, (byte)0xd5, (byte)0x06,
        (byte)0x83, (byte)0xaa, (byte)0x6c, (byte)0xd6,
        (byte)0xc3, (byte)0x9b, (byte)0x2f, (byte)0x12,
        (byte)0xec, (byte)0x74, (byte)0x46, (byte)0x0e,
        (byte)0xc3, (byte)0x3e, (byte)0x5f, (byte)0xae,
        (byte)0x81, (byte)0x75, (byte)0xfe, (byte)0xa2,
        (byte)0xa2, (byte)0xf9, (byte)0x77, (byte)0x57,
        (byte)0x2f, (byte)0x92, (byte)0x61, (byte)0xd8,
        (byte)0x46, (byte)0x18, (byte)0x22, (byte)0x70,
        (byte)0xe7, (byte)0xb2, (byte)0x94, (byte)0xcb,
        (byte)0xb6, (byte)0x52, (byte)0xf2, (byte)0x2d,
        (byte)0x66, (byte)0x44, (byte)0x73, (byte)0x0b,
        (byte)0xb3, (byte)0xbe, (byte)0xe0, (byte)0x36,
        (byte)0xba, (byte)0x0e, (byte)0x09, (byte)0x16,
        (byte)0xd0, (byte)0x22, (byte)0xa8, (byte)0x36,
        (byte)0xdf, (byte)0x26, (byte)0xe3, (byte)0xc2,
        (byte)0xe7, (byte)0x9b, (byte)0xf2, (byte)0xc1,
        (byte)0xfe, (byte)0x79, (byte)0x68, (byte)0x07,
        (byte)0xe1, (byte)0xd0, (byte)0xfd, (byte)0x4a,
        (byte)0x62, (byte)0x5b, (byte)0x8e, (byte)0xa2,
        (byte)0x8a, (byte)0x6a, (byte)0xd4, (byte)0x19,
        (byte)0xeb, (byte)0xe9, (byte)0x37, (byte)0x14,
        (byte)0x1d, (byte)0x9b, (byte)0x72, (byte)0x85,
        (byte)0xf1, (byte)0x5f, (byte)0xf0, (byte)0xa3,
        (byte)0x4d, (byte)0xda, (byte)0x7f, (byte)0x53,
        (byte)0xbf, (byte)0x09, (byte)0xfb, (byte)0xdd,
        (byte)0x78, (byte)0xa1, (byte)0x4f, (byte)0x0b,
        (byte)0x31, (byte)0x4a, (byte)0x9e, (byte)0x8b,
        (byte)0xd0, (byte)0x44, (byte)0x7a, (byte)0x60,
        (byte)0xe9, (byte)0xd8, (byte)0x45, (byte)0xf6,
        (byte)0x0e, (byte)0xd6, (byte)0xfb, (byte)0x6c,
        (byte)0xdf, (byte)0x2d, (byte)0xcd, (byte)0xe8,
        (byte)0x02, (byte)0xa5, (byte)0x1e, (byte)0xb1,
        (byte)0xe9, (byte)0xf8, (byte)0x93, (byte)0x8c,
        (byte)0x75, (byte)0x6c, (byte)0x4b, (byte)0x89,
        (byte)0x03, (byte)0xf8, (byte)0x90, (byte)0xca,
        (byte)0xb5, (byte)0xdd, (byte)0xb2, (byte)0x7c,
        (byte)0xf2, (byte)0x4a, (byte)0xc7, (byte)0x73,
        (byte)0xe2, (byte)0x07, (byte)0x09, (byte)0x61,
        (byte)0x8d, (byte)0xf9, (byte)0x2b, (byte)0x6e,
        (byte)0xa4, (byte)0x52, (byte)0x99, (byte)0xff,
        (byte)0xfa, (byte)0x94, (byte)0x58, (byte)0x21,
        (byte)0x7a, (byte)0x98, (byte)0xa8, (byte)0x4b,
        (byte)0x9b, (byte)0x5e, (byte)0xc2, (byte)0x03,
        (byte)0x52, (byte)0xdc, (byte)0x89, (byte)0x43,
        (byte)0xf0, (byte)0xdb, (byte)0x9a, (byte)0xfb,
        (byte)0x89, (byte)0xca, (byte)0xa8, (byte)0x86,
        (byte)0x5e, (byte)0x21, (byte)0x45, (byte)0x6d,
        (byte)0xcb, (byte)0xa6, (byte)0x40, (byte)0x6b,
        (byte)0xfe, (byte)0xaf, (byte)0x39, (byte)0x80,
        (byte)0x10, (byte)0x75, (byte)0xca, (byte)0x10,
        (byte)0x8a, (byte)0x75, (byte)0xca, (byte)0xc7,
        (byte)0x88, (byte)0x84, (byte)0x06, (byte)0x1d,
        (byte)0x22, (byte)0x35, (byte)0x49, (byte)0xee,
        (byte)0x4c, (byte)0xdc, (byte)0xd9, (byte)0x0d,
        (byte)0xd9, (byte)0x33, (byte)0x4a, (byte)0x09,
        (byte)0xb9, (byte)0x77, (byte)0xa2, (byte)0xba,
        (byte)0x7a, (byte)0x3b, (byte)0x45, (byte)0x61,
        (byte)0xe0, (byte)0x7d, (byte)0xd2, (byte)0x4f,
        (byte)0x85, (byte)0x0c, (byte)0xf0, (byte)0xe4,
        (byte)0x35, (byte)0x10, (byte)0x44, (byte)0xc8,
        (byte)0xef, (byte)0x3f, (byte)0x19, (byte)0x1a,
        (byte)0x01, (byte)0x8e, (byte)0x8d, (byte)0xa2,
        (byte)0x54, (byte)0xe8, (byte)0xa8, (byte)0x27,
        (byte)0x17, (byte)0x6d, (byte)0xa8, (byte)0xd4,
        (byte)0x1f, (byte)0x2a, (byte)0xb7, (byte)0x65,
        (byte)0x59, (byte)0xa2, (byte)0x93, (byte)0xf0,
        (byte)0xe0, (byte)0x0f, (byte)0x52, (byte)0x39,
        (byte)0xc6, (byte)0x55, (byte)0x7b, (byte)0x52,
        (byte)0x7d, (byte)0x28, (byte)0xd4, (byte)0x31,
        (byte)0xe9, (byte)0xc2, (byte)0x12, (byte)0x3f,
        (byte)0xa0, (byte)0x3e, (byte)0x42, (byte)0x7b,
        (byte)0xf0, (byte)0xe5, (byte)0x2c, (byte)0xb1,
        (byte)0x57, (byte)0x95, (byte)0xb0, (byte)0xe8,
        (byte)0x26, (byte)0x31, (byte)0x5d, (byte)0xba,
        (byte)0xbe, (byte)0x7b, (byte)0xff, (byte)0x1a,
        (byte)0xfe, (byte)0xff, (byte)0xc8, (byte)0x4d,
        (byte)0x0b, (byte)0xcd, (byte)0xa1, (byte)0x92,
        (byte)0x60, (byte)0x91, (byte)0xed, (byte)0x59,
        (byte)0xf4, (byte)0xa1, (byte)0xf5, (byte)0x6f,
        (byte)0x99, (byte)0xdd, (byte)0xb0, (byte)0x22,
        (byte)0x6b, (byte)0x4f, (byte)0x7a, (byte)0x43,
        (byte)0x21, (byte)0x7f, (byte)0x06, (byte)0x9e,
        (byte)0xc9, (byte)0xe7, (byte)0x1b, (byte)0xee,
        (byte)0xe3, (byte)0xab, (byte)0x8c, (byte)0x01,
        (byte)0x4d, (byte)0xf5, (byte)0x44, (byte)0x4d,
        (byte)0x82, (byte)0x4c, (byte)0x53, (byte)0x8c,
        (byte)0xd8, (byte)0xc3, (byte)0xe3, (byte)0xc9,
        (byte)0x9f, (byte)0x37, (byte)0xc8, (byte)0x94,
        (byte)0xc4, (byte)0x44, (byte)0xf8, (byte)0x46,
        (byte)0x4f, (byte)0x10, (byte)0x73, (byte)0x3d,
        (byte)0x75, (byte)0x95, (byte)0x58, (byte)0x76,
        (byte)0xc0, (byte)0x00, (byte)0x50, (byte)0x41,
        (byte)0xc1, (byte)0xac, (byte)0x77, (byte)0xb3,
        (byte)0xde, (byte)0xe5, (byte)0x53, (byte)0x0c,
        (byte)0xd7, (byte)0x24, (byte)0xe3, (byte)0xf0,
        (byte)0x8c, (byte)0x6f, (byte)0xce, (byte)0x14,
        (byte)0x8b, (byte)0x41, (byte)0xb1, (byte)0x7d,
        (byte)0xba, (byte)0x40, (byte)0xa0, (byte)0x53,
        (byte)0x43, (byte)0xeb, (byte)0xd6, (byte)0xa6,
        (byte)0x45, (byte)0x4c, (byte)0x55, (byte)0x6d,
        (byte)0x8c, (byte)0x9c, (byte)0xeb, (byte)0x24,
        (byte)0x8c, (byte)0x43, (byte)0x24, (byte)0xff,
        (byte)0x0a, (byte)0x35, (byte)0xf7, (byte)0x31,
        (byte)0x1b, (byte)0x0f, (byte)0xf4, (byte)0xc8,
        (byte)0x97, (byte)0x51, (byte)0xe9, (byte)0x5c,
        (byte)0x38, (byte)0x2d, (byte)0xee, (byte)0x14,
        (byte)0xfb, (byte)0x04, (byte)0x75, (byte)0x09,
        (byte)0x9b, (byte)0xd5, (byte)0x69, (byte)0x67,
        (byte)0xc4, (byte)0xb7, (byte)0x23, (byte)0xb1,
        (byte)0x09, (byte)0x94, (byte)0x66, (byte)0x50,
        (byte)0xde, (byte)0xec, (byte)0xf9, (byte)0xa4,
        (byte)0xa6, (byte)0x07, (byte)0x3d, (byte)0xc2,
        (byte)0xdf, (byte)0x06, (byte)0x3b, (byte)0x93,
        (byte)0xff, (byte)0x3e, (byte)0xa5, (byte)0x28,
        (byte)0x6c, (byte)0xeb, (byte)0xda, (byte)0x58,
        (byte)0xc5, (byte)0x8c, (byte)0x85, (byte)0x96,
        (byte)0xa3, (byte)0x64, (byte)0x2b, (byte)0xa7,
        (byte)0x0a, (byte)0xca, (byte)0x86, (byte)0xd2,
        (byte)0x32, (byte)0xbe, (byte)0x50, (byte)0x3f,
        (byte)0xd2, (byte)0xd1, (byte)0x6c, (byte)0x8d,
        (byte)0x3b, (byte)0x02, (byte)0x03, (byte)0x01,
        (byte)0x00, (byte)0x01
	};
	
	private String customerName;
	private String customerAddress;
	private String customerPhone;
	private String customerEmail;
	
	private Tag restrictions;
	private Restrictions realRestrictions; // Lazy-evaluation in
	// getRestrictions()
	
	private String signature;
	
	private PublicKey publicKey;
	private Signature signer;
	private KeyFactory factory;
	
	/**
	 * Used internally to load the public key and set up the key factory and
	 * signer objects.
	 */
	private LicenseKey() {
		KeySpec pubKeySpec = new X509EncodedKeySpec(PUBLIC_KEY_DATA);
		
		try {
			signer = Signature.getInstance("SHA1withRSA");
		} catch (NoSuchAlgorithmException nsae) {
			logger.error("Signing is not available on this sytsem", nsae);
			
			throw new RuntimeException(
			                           "Unable to initialize license key: RSA signing is not available on this system: " +
			                                   nsae.getMessage(), nsae);
		}
		
		try {
			factory = KeyFactory.getInstance("RSA");
		} catch (NoSuchAlgorithmException nsae) {
			logger.error("Key type not available on this system", nsae);
			
			throw new RuntimeException(
			                           "Unable to initialize license key: Key type not available on this system: " +
			                                   nsae.getMessage(), nsae);
		}
		
		try {
			publicKey = factory.generatePublic(pubKeySpec);
		} catch (InvalidKeySpecException ikse) {
			logger.error("Stored public key is not in a legal format", ikse);
			
			throw new RuntimeException(
			                           "Unable to initialize license key: The public key stored in the program is invalid: " +
			                                   ikse.getMessage(), ikse);
		}
	}
	
	/**
	 * Create a new LicenseKey object to verify a license key, reading the
	 * license key from the given file.
	 * 
	 * @param keyFile The file containing the license key.
	 * @throws IOException If there is a problem reading the file.
	 */
	public LicenseKey(File keyFile) throws IOException {
		this();
		
		if (!keyFile.exists()) throw new IOException("License key file does not exist");
		if (!keyFile.isFile()) throw new IOException("License key file is not a file");
		if (!keyFile.canRead()) throw new IOException("License key file cannot be read");
		
		// Load the license key file
		Tag licenseKey = Utils.xmlParser.parseXML(keyFile, true, false);
		
		parseLicenseKey(licenseKey);
	}
	
	/**
	 * Create a new LicenseKey object to verify a license key, reading the
	 * license key from the given XML tag.
	 * 
	 * @param licenseKey The tag to read.
	 */
	public LicenseKey(Tag licenseKey) {
		this();
		
		parseLicenseKey(licenseKey);
	}
	
	/**
	 * Create a new LicenseKey object to generate a new license key for a
	 * customer.
	 * 
	 * @param customerName The name of the customer (like "SIXNET").
	 * @param customerAddress The address of the customer (like
	 *            "331 Ushers Rd.\nClifton Park, NY 12065\nUnited States").
	 * @param customerPhone The phone number of the customer (like
	 *            "+1-518-877-5173").
	 * @param customerEmail The primary email address for contacting the
	 *            customer (like "support@sixnet.com").
	 * @param restrictions Restrictions for this license key. These should be
	 *            documented in the manual.
	 * @param prvKeyData The PKCS#8 encoding of the private key for creating the
	 *            license key signature. It must be RSA.
	 * @throws InvalidKeyException If the provided private key is invalid.
	 */
	public LicenseKey(String customerName, String customerAddress, String customerPhone,
	        String customerEmail, Tag restrictions, byte[] prvKeyData) throws InvalidKeyException {
		this();
		
		this.customerName = customerName;
		this.customerAddress = customerAddress;
		this.customerPhone = customerPhone;
		this.customerEmail = customerEmail;
		this.restrictions = restrictions;
		
		Tag licenseBody = getLicenseBody();
		
		// Build the private key
		KeySpec prvKeySpec = new PKCS8EncodedKeySpec(prvKeyData);
		
		PrivateKey privateKey;
		try {
			privateKey = factory.generatePrivate(prvKeySpec);
		} catch (InvalidKeySpecException ikse) {
			logger.error("Unable to create private key", ikse);
			
			throw new RuntimeException(
			                           "Unable to generate new license key: Invalid private key specification: " +
			                                   ikse.getMessage(), ikse);
		}
		
		// Sign the customer data
		byte[] signature;
		
		signer.initSign(privateKey, Utils.srand);
		try {
			signer.update(licenseBody.toString(false).getBytes("UTF-8"));
			signature = signer.sign();
		} catch (SignatureException se) {
			logger.error("Signer not initialized properly", se);
			
			throw new RuntimeException("Signer not initialized properly: " + se.getMessage(), se);
		} catch (UnsupportedEncodingException uee) {
			throw new RuntimeException(
			                           "The Java environment is not providing the UTF-8 character set as called for in the specification: " +
			                                   uee.getMessage(), uee);
		}
		
		// Verify that it worked properly
		signer.initVerify(publicKey);
		try {
			signer.update(licenseBody.toString(false).getBytes("UTF-8"));
			
			if (!signer.verify(signature)) {
				throw new RuntimeException("Unable verify after signing");
			}
		} catch (SignatureException se) {
			logger.error("Signer not initialized properly", se);
			
			throw new RuntimeException("Signer not initialized properly: " + se.getMessage(), se);
		} catch (UnsupportedEncodingException uee) {
			throw new RuntimeException(
			                           "The Java environment is not providing the UTF-8 character set as called for in the specification: " +
			                                   uee.getMessage(), uee);
		}
		
		this.signature = Conversion.bytesToHex(signature);
	}
	
	/**
	 * Parse data out of an existing license key.
	 * 
	 * @param license The license key.
	 */
	private void parseLicenseKey(Tag license) {
		this.customerName = license.getStringContents("license/customer/name", "");
		this.customerAddress = license.getStringContents("license/customer/address", "");
		this.customerPhone = license.getStringContents("license/customer/phone", "");
		this.customerEmail = license.getStringContents("license/customer/email", "");
		
		this.restrictions = license.getTag("license/restrictions");
		if (this.restrictions == null) this.restrictions = new Tag("restrictions");
		
		this.signature = license.getStringContents("signature", "");
	}
	
	/**
	 * Get the complete license key represented by this object.
	 */
	public Tag getLicense() {
		Tag licenseBody = getLicenseBody();
		
		Tag signature = new Tag("signature");
		signature.addContent(getSignature());
		
		Tag root = new Tag("bvblicense");
		root.addContent(licenseBody);
		root.addContent(signature);
		
		return root;
	}
	
	/**
	 * Get the customer's name.
	 */
	public String getCustomerName() {
		return customerName;
	}
	
	/**
	 * Get the customer's address.
	 */
	public String getCustomerAddress() {
		return customerAddress;
	}
	
	/**
	 * Get the customer's phone number.
	 */
	public String getCustomerPhone() {
		return customerPhone;
	}
	
	/**
	 * Get the customer's email address.
	 */
	public String getCustomerEmail() {
		return customerEmail;
	}
	
	/**
	 * Get the restrictions placed on this license key as an XML tag.
	 */
	public Tag getRestrictionsTag() {
		return restrictions;
	}
	
	/**
	 * Get the restrictions placed on this license key as a Restrictions object.
	 */
	public Restrictions getRestrictions() {
		if (realRestrictions == null) {
			realRestrictions = new Restrictions(getRestrictionsTag());
		}
		
		return realRestrictions;
	}
	
	/**
	 * Get the SHA1 with DSA signature of this license key.
	 */
	public String getSignature() {
		return signature;
	}
	
	/**
	 * Check whether this license key is valid.
	 * 
	 * @return True = the license key was signed by a valid distributor, False =
	 *         the license key was not signed by a valid distributor.
	 */
	public boolean isValid() {
		Tag licenseBody = getLicenseBody();
		String signature = getSignature();
		
		try {
			signer.initVerify(publicKey);
		} catch (InvalidKeyException ike) {
			logger.error("Stored public key is not in a legal format", ike);
			
			throw new RuntimeException(
			                           "Unable to verify license key: The public key stored in the program is invalid: " +
			                                   ike.getMessage(), ike);
		}
		
		try {
			signer.update(licenseBody.toString(false).getBytes("UTF-8"));
			
			return signer.verify(Conversion.hexToBytes(signature));
		} catch (SignatureException se) {
			logger.error("Signer not initialized properly", se);
			
			throw new RuntimeException("Signer not initialized properly: " + se.getMessage(), se);
		} catch (UnsupportedEncodingException uee) {
			throw new RuntimeException(
			                           "The Java environment is not providing the UTF-8 character set as called for in the specification: " +
			                                   uee.getMessage(), uee);
		}
	}
	
	/**
	 * Get the body of the license (does not include the signed signature).
	 */
	private Tag getLicenseBody() {
		Tag licenseBody = new Tag("license");
		Tag temp;
		
		// Customer data
		{
			Tag temp2;
			
			licenseBody.addContent(temp = new Tag("customer"));
			
			temp.addContent(temp2 = new Tag("name"));
			temp2.addContent(getCustomerName());
			
			temp.addContent(temp2 = new Tag("address"));
			temp2.addContent(getCustomerAddress());
			
			temp.addContent(temp2 = new Tag("phone"));
			temp2.addContent(getCustomerPhone());
			
			temp.addContent(temp2 = new Tag("email"));
			temp2.addContent(getCustomerEmail());
		}
		
		// Restrictions
		licenseBody.addContent(getRestrictionsTag());
		
		return licenseBody;
	}
}

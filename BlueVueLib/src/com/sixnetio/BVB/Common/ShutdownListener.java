/*
 * ShutdownListener.java
 *
 * Implement this to be notified of server shutdown events.
 *
 * Jonathan Pearson
 * January 21, 2009
 *
 */

package com.sixnetio.BVB.Common;

public interface ShutdownListener {
	public static final long ONE_MINUTE_MS = 60000;
	
	/**
	 * Called when the server is shutting down normally. Gracefully clean up
	 * what you need to.
	 */
	public void serverShutdown();
	
	/**
	 * Called when the server is shutting down due to a panic. Clean up what you
	 * can as fast as possible and ignore thread interrupts.
	 */
	public void serverPanic();
	
	/**
	 * Used to order shutdown listener notifications. If you don't care, you
	 * should return 0. If you do care, coordinate with other objects that you
	 * depend on so that your number is lower than theirs.
	 * 
	 * @return A value indicating the order in which <tt>ShutdownListener</tt>s
	 *         will be notified of shutdown events. A higher numbered listener
	 *         will be notified after all lower numbered listeners.
	 */
	public int getShutdownOrder();
}

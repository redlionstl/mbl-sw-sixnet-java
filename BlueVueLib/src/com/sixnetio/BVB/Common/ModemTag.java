/*
 * ModemTag.java
 *
 * Represents a tag which can be applied to modems.
 *
 * Jonathan Pearson
 * January 20, 2009
 *
 */

package com.sixnetio.BVB.Common;

public class ModemTag {
	/**
	 * A tag ID that will never appear in the database, used for unset tag IDs.
	 */
	public static final int INV_TAG = -1;
	
	/** The ID number of the tag. */
	public int tagID;
	
	/**
	 * The position of this tag when applying scripts from multiple tags to a
	 * single modem.
	 */
	public int position;
	
	/** The name of this tag. */
	public String name;
	
	/** The configuration script associated with this tag, or <tt>null</tt>. */
	public String configScript;
	
	/**
	 * The polling interval (delay after a successful automatic job before the
	 * next instance should fire).
	 */
	public int pollingInterval;
	
	/**
	 * Number of modems that have this tag, if this was retrieved from a
	 * database, or 0 otherwise.
	 */
	public int modemCount;
	
	/**
	 * Construct a new ModemTag with no set values.
	 */
	public ModemTag() {}
	
	/**
	 * Construct a new ModemTag from known values. Defaults modemCount to 0.
	 * 
	 * @param tagID The Tag ID of this tag.
	 * @param position Used to sort a list of multiple tags applied to a single
	 *            modem.
	 * @param name The name of this tag.
	 * @param configScript The configuration script associated with this tag.
	 * @param pollingInterval The polling interval for modems with this tag.
	 */
	public ModemTag(int tagID, int position, String name, String configScript, int pollingInterval) {
		this(tagID, position, name, configScript, pollingInterval, 0);
	}
	
	/**
	 * Construct a new ModemTag from known values. This should only be called
	 * from a database retrieval method.
	 * 
	 * @param tagID The Tag ID of this tag.
	 * @param position Used to sort a list of multiple tags applied to a single
	 *            modem.
	 * @param name The name of this tag.
	 * @param configScript The configuration script associated with this tag.
	 * @param pollingInterval The polling interval for modems with this tag.
	 * @param modemCount The number of modems with this tag applied to them.
	 */
	public ModemTag(int tagID, int position, String name, String configScript, int pollingInterval,
	        int modemCount) {
		this.tagID = tagID;
		this.position = position;
		this.name = name;
		this.configScript = configScript;
		this.pollingInterval = pollingInterval;
		this.modemCount = modemCount;
	}
	
	/** A common function for getting the ID number of the object. */
	public int getId() {
		return tagID;
	}
	
	/**
	 * For convenience, get the polling interval as a minute value.
	 */
	public int getPollingIntervalMinutes() {
		return pollingInterval / 60;
	}
	
	/**
	 * For convenience, sets the polling interval from a value in minutes.
	 * 
	 * @param pollingInterval The new polling interval in minutes.
	 */
	public void setPollingIntervalMinutes(int pollingInterval) {
		this.pollingInterval = pollingInterval * 60;
	}
}

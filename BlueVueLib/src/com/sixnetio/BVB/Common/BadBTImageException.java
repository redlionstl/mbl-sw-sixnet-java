/*
 * BadBTImageException.java
 *
 * Thrown when a BlueTreeImage cannot be constructed because the data is bad.
 *
 * Jonathan Pearson
 * January 30, 2009
 *
 */

package com.sixnetio.BVB.Common;

public class BadBTImageException extends Exception {
	public BadBTImageException(String message) {
		super(message);
	}
	
	public BadBTImageException(String message, Throwable cause) {
		super(message, cause);
	}
}

/*
 * Capabilities.java
 *
 * Represents the capabilities of a job server.
 *
 * Jonathan Pearson
 * February 5, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.util.*;

import org.jonp.xml.Tag;

import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Modems.ModemModule;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class Capabilities {
	private String serverName;
	private Collection<String> databases;
	private Collection<String> jobs;
	private Collection<String> modems;
	private Tag extra;
	
	/**
	 * Construct a new Capabilities object with the specified capabilities.
	 * 
	 * @param serverName The name of the server that this applies to.
	 * @param databases The databases that the server supports, separated by
	 *            colons.
	 * @param jobs The job types that the server supports, separated by colons.
	 * @param modems The modem model/firmware combinations that the server
	 *            supports in the format 'model@firmware', separated by colons.
	 * @param extra Any extra data that is necessary; if <tt>null</tt>, an empty
	 *            'extra' tag will be used.
	 */
	public Capabilities(String serverName, Collection<String> databases, Collection<String> jobs,
	        Collection<String> modems, Tag extra) {
		
		this.serverName = serverName;
		
		// Copy these into new collections to prevent external modification
		this.databases = new HashSet<String>(databases);
		this.jobs = new HashSet<String>(jobs);
		this.modems = new HashSet<String>(modems);
		
		if (extra == null) {
			this.extra = new Tag("extra");
		} else {
			this.extra = extra;
		}
	}
	
	/**
	 * Construct a new Capabilities object given strings as they may be found in
	 * the capabilities table.
	 * 
	 * @param serverName The name of the server to reference.
	 * @param databases The databases provided by that server, separated by
	 *            colons.
	 * @param jobs The job types provided by that server, separated by colons.
	 * @param modems The modem model/firmware combinations provided by that
	 *            server, formatted like 'model@firmware' and separated by
	 *            colons.
	 * @param extra Any extra data that the server may provide.
	 */
	public Capabilities(String serverName, String databases, String jobs, String modems, Tag extra) {
		this.serverName = serverName;
		
		String[] temp;
		
		temp = Utils.tokenize(databases, ":", false);
		if (temp.length == 1 && temp[0].length() == 0) temp = new String[0];
		this.databases = Utils.makeHashSet(temp);
		
		temp = Utils.tokenize(jobs, ":", false);
		if (temp.length == 1 && temp[0].length() == 0) temp = new String[0];
		this.jobs = Utils.makeHashSet(temp);
		
		temp = Utils.tokenize(modems, ":", false);
		if (temp.length == 1 && temp[0].length() == 0) temp = new String[0];
		this.modems = Utils.makeHashSet(temp);
		
		if (extra == null) {
			this.extra = new Tag("extra");
		} else {
			this.extra = extra;
		}
	}
	
	/**
	 * Get the capabilities of the local machine as it stands right now.
	 */
	public static Capabilities getLocalCapabilities() {
		return new Capabilities(Server.getName(), getLocalDatabaseCapabilities(),
		                        getLocalJobCapabilities(), getLocalModemCapabilities(),
		                        new Tag("extra"));
	}
	
	/**
	 * Get the local database capabilities.
	 */
	public static Collection<String> getLocalDatabaseCapabilities() {
		return Database.getKnownProviders();
	}
	
	/**
	 * Get the local job capabilities.
	 */
	public static Collection<String> getLocalJobCapabilities() {
		return Job.getKnownJobTypes();
	}
	
	/**
	 * Get the local modem capabilities.
	 */
	public static Collection<String> getLocalModemCapabilities() {
		Set<String> modemCaps = new HashSet<String>();
		
		for (String model : ModemModule.getKnownModels()) {
			for (Version version : ModemModule.getKnownVersions(model)) {
				modemCaps.add(String.format("%s@%s", model, version.toString()));
			}
		}
		
		return modemCaps;
	}
	
	/**
	 * Get the name of the server that this capabilities object references.
	 */
	public String getServerName() {
		return serverName;
	}
	
	/**
	 * Get the names of the databases provided by the referenced server.
	 */
	public Collection<String> getDatabases() {
		return new ArrayList<String>(databases);
	}
	
	/**
	 * Get the job types provided by the referenced server.
	 */
	public Collection<String> getJobs() {
		return new ArrayList<String>(jobs);
	}
	
	/**
	 * Get the modem models/firmware revisions provided by the referenced server
	 * (in the format 'model@firmware').
	 */
	public Collection<String> getModems() {
		return new ArrayList<String>(modems);
	}
	
	/**
	 * Get any extra data that may exist for the referenced server. This is
	 * mutable.
	 */
	public Tag getExtra() {
		return extra;
	}
}

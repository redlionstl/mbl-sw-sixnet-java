/*
 * BadSettingException.java
 *
 * Thrown when Settings cannot return a legitimate value for a requested setting.
 *
 * Jonathan Pearson
 * January 20, 2009
 *
 */

package com.sixnetio.BVB.Common;

public class BadSettingException extends Exception {
	private final String settingName;
	
	public BadSettingException(String message, String settingName) {
		super(String.format("%s: %s", message, settingName));
		
		this.settingName = settingName;
	}
	
	public BadSettingException(String message, String settingName, Throwable cause) {
		super(String.format("%s: %s", message, settingName), cause);
		
		this.settingName = settingName;
	}
	
	public String getSettingName() {
		return settingName;
	}
}

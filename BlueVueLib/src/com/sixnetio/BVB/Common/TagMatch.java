/*
 * TagMatch.java
 *
 * Represents a match between a tag and a modem.
 *
 * Jonathan Pearson
 * February 6, 2009
 *
 */

package com.sixnetio.BVB.Common;

public class TagMatch {
	public final int tagID;
	public final int modemID;
	
	/**
	 * Construct a new TagMatch.
	 * 
	 * @param tagID The tag ID.
	 * @param modemID The modem ID.
	 */
	public TagMatch(int tagID, int modemID) {
		this.tagID = tagID;
		this.modemID = modemID;
	}
}

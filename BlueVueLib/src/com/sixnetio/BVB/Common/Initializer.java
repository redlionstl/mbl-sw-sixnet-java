/*
 * Initializer.java
 *
 * Since all of the programs in BVB have similar startup procedures,
 * this class provides a unified way to perform startup.
 *
 * Jonathan Pearson
 * April 7, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.io.File;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.util.*;

public class Initializer {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Load all classes specified by 'class' tags beneath the given tag.
	 * 
	 * @param tag The tag containing 'class' sub-tags.
	 * @throws ClassNotFoundException If any of the classes fails to load,
	 *             before moving on to the next.
	 */
	public static void loadClasses(Tag tag) throws ClassNotFoundException {
		if (tag == null) return;
		
		for (Tag clsName : tag.getEachTag("", "class")) {
			Class.forName(clsName.getStringContents());
		}
	}
	
	/**
	 * Set up a plugin watcher to watch the specified directories for new
	 * plugins. If the tag is <tt>null</tt>, does nothing.
	 * 
	 * @param tag The tag containing 'directory' sub-tags, each of which will be
	 *            monitored, and 'plugin' sub-tags, each of which will be loaded
	 *            directly.
	 * @throws IllegalArgumentException If any of the directories specified is
	 *             not a directory, or if any of the plugins specified is not a
	 *             plugin.
	 * @throws InterruptedOperationException If the thread is interrupted while
	 *             waiting for the first scan to complete.
	 */
	public static void loadPlugins(Tag tag) throws IllegalArgumentException,
	        InterruptedOperationException {
		
		if (tag == null) return;
		
		PluginLoader watcher = new PluginLoader("BlueVueBatch");
		
		for (Tag dirName : tag.getEachTag("", "directory")) {
			watcher.watchDirectory(new File(dirName.getStringContents()));
		}
		
		for (Tag pluginFile : tag.getEachTag("", "plugin")) {
			File f = new File(pluginFile.getStringContents());
			
			try {
				watcher.loadPlugin(f);
			} catch (Exception e) {
				throw new IllegalArgumentException(
				                                   String
				                                         .format(
				                                                 "Unable to load plugin file '%s': %s",
				                                                 f.getAbsolutePath(),
				                                                 e.getMessage()), e);
			}
		}
	}
}

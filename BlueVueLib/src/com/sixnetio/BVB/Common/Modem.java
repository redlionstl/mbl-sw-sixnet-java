/*
 * Modem.java
 *
 * A simple Modem object.
 *
 * Jonathan Pearson
 * January 15, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class Modem {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Represents the health of the modem.
	 * 
	 * @author Jonathan Pearson
	 */
	public static enum ModemHealth {
		/**
		 * The modem has had a successful communication within the warning
		 * timeout period. See {@link Setting#WarnTimeout}.
		 */
		Normal("normal"),

		/**
		 * The modem has had no communications (successful or otherwise) within
		 * the failure timeout period. See {@link Setting#FailureTimeout}.
		 */
		Stale("stale"),
		
		/**
		 * The modem has had a successful communication within the failure
		 * timeout period (see {@link Setting#FailureTimeout}) but not within
		 * the warning timeout period (see {@link Setting#WarnTimeout}).
		 */
		Warning("warning"),

		/**
		 * The modem has had no successful communications within the failure
		 * timeout period, but has had at least one failed communication within
		 * that period. See {@link Setting#FailureTimeout}.
		 */
		Failure("failure"),
		;
		
		public final String statusString;
		
		private ModemHealth(String statusString) {
			this.statusString = statusString;
		}
		
		public static ModemHealth find(String statusString) {
			for (ModemHealth mh : values()) {
				if (mh.statusString.equals(statusString)) return mh;
			}
			
			return null;
		}
		
		@Override
		public String toString() {
			return statusString;
		}
	}
	
	/**
	 * A modem ID that will never appear in the database, used for unset modem
	 * IDs.
	 */
	public static final int INV_MODEM = -1;
	
	/** Standard port number for modem communication. */
	public static final int PORT_STANDARD = 5070;
	
	/** The ID number of this modem, unique in the database. */
	public int modemID;
	
	/** The device ID of this modem (ESN or IMEI). */
	public DeviceID deviceID;
	
	/** The name of this modem. */
	public String alias;
	
	/** The model of this modem. */
	public String model;
	
	/** The version number of the firmware of this modem. */
	public Version fwVersion;
	
	/** The configuration version string of this modem. */
	public String cfgVersion;
	
	/** The IP address of this modem. */
	public String ipAddress;
	
	/** The port number for modem communications (port 5070 protocol). */
	public int port;
	
	/** The phone number of this modem. */
	public String phoneNumber;
	
	/** Extra status information about this modem. */
	public Tag status;
	
	/** The number of active jobs targeted at this modem. */
	public int jobCount;
	
	/** The number of tags applied to this modem. */
	public int tagCount;
	
	/** The health of the modem. */
	public ModemHealth modemHealth;
	
	/**
	 * Construct a Modem object given all (or as much as possible) of the data
	 * available in the database. Leaves statistical information blank.
	 * 
	 * @param modemID The unique number assigned to this modem by the database;
	 *            pass {@link #INV_MODEM} if creating a new one.
	 * @param deviceID The ESN or IMEI of the modem. Use <tt>null</tt> to
	 *            represent 'unknown'.
	 * @param alias An alternate name for this modem; may be <tt>null</tt>.
	 * @param model The model number of this modem; may be <tt>null</tt>.
	 * @param fwVersion The firmware version running in this modem; may be
	 *            <tt>null</tt> if unknown. {@link DeviceID#DEVID_UNKNOWN} will
	 *            be converted to <tt>null</tt>.
	 * @param cfgVersion The configuration version of the modem (versioned
	 *            settings).
	 * @param ipAddress The IP address of the modem. Communications are not
	 *            possible if this is <tt>null</tt>.
	 * @param port The port to connect on.
	 * @param phoneNumber The phone number of the modem; may be <tt>null</tt>.
	 * @param status Extra status information about the modem.
	 */
	public Modem(int modemID, DeviceID deviceID, String alias, String model, Version fwVersion,
	        String cfgVersion, String ipAddress, int port, String phoneNumber, Tag status) {
		
		this(modemID, deviceID, alias, model, fwVersion, cfgVersion, ipAddress, port, phoneNumber,
		     status, 0, 0, null);
	}
	
	/**
	 * Construct a Modem object including statistical information. Should only
	 * be called by database retrieval functions.
	 * 
	 * @param modemID The unique number assigned to this modem by the database.
	 * @param deviceID The ESN or IMEI of the modem. Use <tt>null</tt> to
	 *            represent 'unknown'. {@link DeviceID#DEVID_UNKNOWN} will be
	 *            converted to <tt>null</tt>.
	 * @param alias An alternate name for this modem; may be <tt>null</tt>.
	 * @param model The model number of this modem; may be <tt>null</tt>.
	 * @param fwVersion The firmware version running in this modem; may be
	 *            <tt>null</tt> if unknown.
	 * @param cfgVersion The configuration version of the modem (versioned
	 *            settings).
	 * @param ipAddress The IP address of the modem. Communications are not
	 *            possible if this is <tt>null</tt>.
	 * @param port The port to connect on.
	 * @param phoneNumber The phone number of the modem; may be <tt>null</tt>.
	 * @param status Extra status information about the modem.
	 * @param jobCount The number of active jobs against this modem.
	 * @param tagCount The number of tags applied to this modem.
	 * @param modemHealth The health of the modem.
	 */
	public Modem(int modemID, DeviceID deviceID, String alias, String model, Version fwVersion,
	        String cfgVersion, String ipAddress, int port, String phoneNumber, Tag status,
	        int jobCount, int tagCount, ModemHealth modemHealth) {
		this.modemID = modemID;
		if (deviceID == null || deviceID.equals(DeviceID.DEVID_UNKNOWN)) {
			this.deviceID = null;
		}
		else {
			this.deviceID = deviceID;
		}
		this.alias = alias;
		this.model = model;
		this.fwVersion = fwVersion;
		this.cfgVersion = cfgVersion;
		this.ipAddress = ipAddress;
		this.port = port;
		this.phoneNumber = phoneNumber;
		this.status = status;
		this.jobCount = jobCount;
		this.tagCount = tagCount;
		this.modemHealth = modemHealth;
	}
	
	/**
	 * Get a property out of the extra modem status.
	 * 
	 * @param name The name of the property (such as rfVersion).
	 * @return The value of the property, or <tt>null</tt> if not defined.
	 */
	public String getProperty(String name) {
		return status.getStringContents(name, null);
	}
	
	/**
	 * Set a property in the extra modem status. If it exists, it will be
	 * overwritten; if it does not exist, it will be created.
	 * 
	 * @param name The name of the property.
	 * @param val The value to set for the property. May be <tt>null</tt>, in
	 *            which case no action is taken.
	 */
	public void setProperty(String name, String val) {
		// Unknown value? This is done for convenience
		if (val == null) {
			logger.debug(String.format("Modem property '%s' not set (passed in as null)", name));
			return;
		}
		
		Tag tag = status.getTag(name);
		
		if (tag == null) {
			// Create all necessary tags to get there
			StringTokenizer tok = new StringTokenizer(name, "/");
			Tag prev = null;
			Tag cur = status;
			while (tok.hasMoreTokens()) {
				String tagName = tok.nextToken();
				
				prev = cur;
				cur = cur.getChild(tagName);
				
				if (cur == null) {
					cur = new Tag(tagName);
					prev.addContent(cur);
				}
			}
			
			tag = cur;
		} else {
			tag.removeContents();
		}
		
		tag.addContent(val);
	}
	
	public void deleteProperty(String name) {
		status.removeTag(name);
	}
	
	/**
	 * Get all (or all matching) properties in a specific level.
	 * 
	 * @param beneath The name of the level ("" for top-level).
	 * @param type The type of property, or <tt>null</tt> for all in that level.
	 * @return All properties that match the criteria, in [0] = name, [1] =
	 *         value pairs. Only returns tags that do not have child tags.
	 */
	public Collection<String[]> getProperties(String beneath, String type) {
		Tag[] matches = status.getEachTag(beneath, type);
		
		Collection<String[]> props = new Vector<String[]>(matches.length);
		for (Tag tag : matches) {
			if (tag.getEachTag("", null).length == 0) {
				props.add(new String[] {
				        tag.getName(), tag.getStringContents()
				});
			}
		}
		
		return props;
	}
	
	/**
	 * Get a formatted device ID.
	 */
	public String getNiceDeviceID() {
		if (deviceID == null) return null;
		
		return deviceID.toString();
	}
	
	/**
	 * Set the device ID from a formatted string.
	 */
	public void setNiceDeviceID(String deviceID) {
		if (deviceID == null || deviceID.trim().length() == 0) {
			this.deviceID = null;
		} else {
			this.deviceID = DeviceID.parseDeviceID(deviceID);
		}
	}
	
	/**
	 * Get the raw device ID.
	 */
	public DeviceID getDeviceID()
	{
		return deviceID;
	}
	
	/**
	 * Set the raw device ID.
	 */
	public void setDeviceID(DeviceID deviceID) {
		this.deviceID = deviceID;
	}
	
	/**
	 * Get a descriptive label for this modem.
	 * 
	 * @return The first of these that has a value: alias, deviceID
	 *         (well-formatted), "@ipAddress", "#modemID".
	 */
	public String getLabel() {
		if (alias != null) return alias;
		if (deviceID != null) return getNiceDeviceID();
		if (ipAddress != null) return String.format("@%s", ipAddress);
		
		return String.format("#%d", modemID);
	}
	
	/** A common function for getting the ID number of the object. */
	public int getId() {
		return modemID;
	}
}

/*
 * Settings.java
 *
 * Deals with settings from the Settings table, much like the Restrictions class.
 *
 * Jonathan Pearson
 * January 20, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.Utils;

public class Settings
{
    // Seconds in one minute
    private static final int ONE_MINUTE = 60;

    // Seconds in 1 hour
    private static final int ONE_HOUR = 60 * ONE_MINUTE;

    // Seconds in 1 day
    private static final int ONE_DAY = 24 * ONE_HOUR;

    // Seconds in 1 month (28 days)
    private static final int ONE_MONTH = 28 * ONE_DAY;

    public static enum Setting
    {
        /**
         * Whether the BEP server should create new modems when it cannot match
         * a BEP message with an existing modem. A value of 0 means that
         * messages from unknown modems will be ignored. A value of 1 means that
         * messages from unknown modems will cause new modems to be created.
         */
        BEPCreateModems("BEP Create Modems", 0, 0, 1),

        /**
         * If the BEP server is set to create modems when messages from unknown
         * modems arrive (see {@link #BEPCreateModems}), this controls whether
         * an automatic status job should be created for the new modems. A value
         * of 0 means that automatic status jobs will not be created, a value of
         * 1 means that automatic status jobs will be created. This setting is
         * ignored if {@link #BEPCreateModems} is set to 0. A status job will
         * not be created if there is no running server that has the ability to
         * process status query jobs.
         */
        BEPCreateStatusJob("BEP Create Status Job", 1, 0, 1),

        /**
         * How long (in seconds) to wait for BEP messages before pausing to
         * check for settings updates or server shutdown. Lower values mean
         * faster settings updates and server shutdowns, but slightly lower
         * responsiveness to BEP message handling.
         */
        BEPCycleTime("BEP Cycle Time", 15, 1, Integer.MAX_VALUE),

        /**
         * The maximum amount of time (in seconds) to wait for a BEP response
         * before killing the connection due to inactivity.
         */
        BEPFailureTimeout("BEP Failure Timeout", 15 * ONE_MINUTE, 1,
            Integer.MAX_VALUE),

        /**
         * How long (in seconds) a BEP thread should wait on a busy modem before
         * giving up and attempting to create a Delayed Update job (discarding
         * the data received by the message if such a job type is not supported
         * by a running job server). A value of 0 will give up immediately if
         * the modem is locked. A lower value will be more likely to create
         * Delayed Update jobs (or discard data if no server is able to process
         * them), but a higher value will cause the BEP server to be more likely
         * to run out of available threads in its thread pool.
         */
        BEPModemWaitTimeout("BEP Modem Wait Timeout", 20, 0, Integer.MAX_VALUE),

        /**
         * If new modems may be created by BEP messages (see
         * {@link #BEPCreateModems}), this is the tag that will be applied to
         * those new modems. If blank, no tag will be applied. If the tag does
         * not exist, it will be created with a blank script and a polling
         * interval of {@link #NoTagPollingInterval}.
         */
        BEPNewModemTag("BEP New Modem Tag", "BEP", false),

        /**
         * The port number that the BEP server(s) should listen on. All BEP
         * servers must be restarted after changing this setting.
         */
        BEPPort("BEP Port", 10020, 1, 65535),

        /**
         * When sending a BEP message over UDP, how long (in seconds) to wait
         * for a response before re-sending that message.
         */
        BEPRetryTimeout("BEP Retry Timeout", 30, 1, Integer.MAX_VALUE),

        /**
         * How long (in seconds) before an entry in the Capabilities table is
         * considered 'stale'. A stale entry is assumed to be from a dead
         * server. Locks owned by dead servers are cleared, and their entries
         * are removed from the table. A value of 0 means that entries are never
         * considered stale. If a server detects that it has not updated its
         * entry in at least this amount of time, it must assume that all of its
         * locks have been cleared and it should panic (shutdown forcefully).
         */
        CapabilitiesCutoff("Capabilities Cutoff", 90, 0, Integer.MAX_VALUE),

        /**
         * How long (in seconds) between updates to the Capabilities table. A
         * value of 0 means that the thread updating the Capabilities table will
         * not run. Changing the value away from 0 requires a server restart.
         */
        CapabilitiesUpdateInterval("Capabilities Update Interval", 45, 0,
            Integer.MAX_VALUE),

        /**
         * Whether to clear duplicate IP addresses when contacting modems. A
         * value of 0 means that known incorrect IP addresses will be left
         * alone. A value of 1 means that, upon verifying a modem has a correct
         * IP, all other modems with the same IP address will have their IP
         * address fields cleared. Also, a value of 1 means that, upon detecting
         * that a modem has an incorrect IP, that modem's IP address will be
         * cleared.
         */
        ClearDuplicateIPs("Clear Duplicate IPs", 1, 0, 1),

        /**
         * The version of the database format. If not equal to the expected
         * version, the program should not perform any database operations.
         */
        DatabaseVersion("Database Version", 0, 0, Integer.MAX_VALUE),

        /**
         * How long (in seconds) a modem may go without a job completing
         * successfully before it is marked with an error status.
         */
        FailureTimeout("Failure Timeout", ONE_DAY, 1, Integer.MAX_VALUE),

        /**
         * How long (in seconds) to wait for an FTP operation (such as uploading
         * an update package to a modem) before giving up. This is not a timeout
         * on communications, this is actually the amount of time to give the
         * entire data transfer, so you should probably set it relatively high.
         */
        FTPTimeout("FTP Timeout", 10 * ONE_MINUTE, 1, Integer.MAX_VALUE),

        /**
         * How long (in seconds) entries in the History table should remain
         * before being purged. A value of 0 indicates that entries should never
         * be purged.
         */
        HistoryCutoff("History Cutoff", ONE_MONTH, 0, Integer.MAX_VALUE),

        /**
         * How long (in seconds) between deleting old entries from the History
         * and Status History tables. A value of 0 means that the thread purging
         * the History and Status History tables will not run. Changing the
         * value away from 0 requires a server restart.
         */
        HistoryPurgeInterval("History Purge Interval", 60, 0, Integer.MAX_VALUE),

        /**
         * The number of queued connections to allow to the HTTP server before
         * refusing connections. A value of 0 means that the operating system
         * default value will be used.
         */
        HTTPBacklog("HTTP Backlog", 3, 0, Integer.MAX_VALUE),

        /**
         * The number of simultaneous downloads that the HTTP server will allow.
         */
        HTTPCapacity("HTTP Capacity", 25, 1, Integer.MAX_VALUE),

        /**
         * The port number for the internal HTTP server to run on. The internal
         * HTTP server is used by the BEP server to transfer files to a modem
         * without attempting to make an outgoing connection.
         */
        HTTPPort("HTTP Port", 10030, 1, 65535),

        /**
         * Whether the IP registration server should create new modems when it
         * cannot match an IP registration message with an existing modem. A
         * value of 0 means that messages from unknown modems will be ignored. A
         * value of 1 means that messages from unknown modems will cause new
         * modems to be created.
         */
        IPRegistrationCreateModems("IP Registration Create Modems", 0, 0, 1),

        /**
         * If the IP registration server is set to create modems when messages
         * from unknown modems arrive (see {@link #IPRegistrationCreateModems}),
         * this controls whether an automatic status job should be created for
         * the new modems. A value of 0 means that automatic status jobs will
         * not be created, a value of 1 means that automatic status jobs will be
         * created. This setting is ignored if
         * {@link #IPRegistrationCreateModems} is set to 0. A status job will
         * not be created if there is no running server that has the ability to
         * process status query jobs.
         */
        IPRegistrationCreateStatusJob("IP Registration Create Status Job", 1,
            0, 1),

        /**
         * How long (in seconds) to wait for IP registration messages before
         * pausing to check for settings updates or server shutdown. Lower
         * values mean faster settings updates and server shutdowns, but
         * slightly lower responsiveness to IP registration message handling.
         */
        IPRegistrationCycleTime("IP Registration Cycle Time", 15, 1,
            Integer.MAX_VALUE),

        /**
         * How long (in seconds) an IP registration thread should wait on a busy
         * modem before giving up and attempting to create a Delayed Update job
         * (discarding the data received by the message if such a job type is
         * not supported by a running job server). A value of 0 will give up
         * immediately if the modem is locked. A lower value will be more likely
         * to create Delayed Update jobs (or discard data if no server is able
         * to process them), but a higher value will cause the IP registration
         * server to be more likely to run out of available threads in its
         * thread pool.
         */
        IPRegistrationModemWaitTimeout("IP Registration Modem Wait Timeout",
            20, 0, Integer.MAX_VALUE),

        /**
         * If new modems may be created by IP registration messages (see
         * {@link #IPRegistrationCreateModems}), this is the tag that will be
         * applied to those new modems. If blank, no tag will be applied. If the
         * tag does not exist, it will be created with a blank script and a
         * polling interval of {@link #NoTagPollingInterval}.
         */
        IPRegistrationNewModemTag("IP Registration New Modem Tag", "IPReg",
            false),

        /**
         * The port number that the IP registration server(s) should listen on.
         * All IPReg servers must be restarted after changing this setting.
         */
        IPRegistrationPort("IP Registration Port", 7777, 1, 65535),

        /**
         * Time (in seconds) between database queries for new jobs to execute.
         * This cannot be disabled, as this is the whole reason the job server
         * exists.
         */
        JobPollingInterval("Job Polling Interval", 15, 1, Integer.MAX_VALUE),

        /**
         * Number of jobs to request at a time when requesting jobs from the
         * database.
         */
        JobPollingSize("Job Polling Size", 15, 1, Integer.MAX_VALUE),

        /**
         * The license key for this installation.
         */
        LicenseKey("License Key", "<bvblicense />", false),

        /**
         * How long (in seconds) between purging stale locks (see
         * {@link #LockTimeout} for what constitutes a 'stale' lock). A value of
         * 0 disables the lock purging thread. Changing the value away from 0
         * requires a server restart.
         */
        LockPurgeInterval("Lock Purge Interval", 60, 0, Integer.MAX_VALUE),

        /**
         * How long (in seconds) a lock may be held by a server before it is
         * considered 'stale'. A value of 0 indicates that locks are never
         * considered stale.
         */
        LockTimeout("Lock Timeout", 2 * ONE_HOUR, 0, Integer.MAX_VALUE),

        /**
         * How long (in seconds) entries in the Log table should remain before
         * being purged. A value of 0 indicates that entries should never be
         * purged. A value of -1 disables logging to the database from server
         * processes, but does not stop the database logging thread (allowing
         * logging to be re-enabled without a server restart).
         */
        LoggingCutoff("Logging Cutoff", 3 * ONE_MONTH, -1, Integer.MAX_VALUE),

        /**
         * How often (in seconds) log entries should be dumped to the database.
         * A value of 0 means that the database logging thread should not run.
         * See {@link #LoggingCutoff} to disable database logging without
         * killing the thread. Changing this value away from 0 requires a server
         * restart.
         */
        LogDumpInterval("Log Dump Interval", 5, 0, Integer.MAX_VALUE),

        /**
         * The number of open database connections to maintain in the database
         * connection pool.
         */
        MaxDBConnections("Maximum DB Connections", 5, 1, Integer.MAX_VALUE),

        /**
         * The maximum number of threads in a thread pool (Job processing, IP
         * registration connection processing, ...).
         */
        MaxThreadPoolSize("Maximum Thread Pool Size", 50, 1, Integer.MAX_VALUE),

        /**
         * How many attempts can be made against a job before canceling it.
         */
        MaxTries("Maximum Tries", 3, 1, Integer.MAX_VALUE),

        /**
         * The minimum number of threads in a thread pool (Job processing, IP
         * registration connection processing, ...).
         */
        MinThreadPoolSize("Minimum Thread Pool Size", 3, 0, Integer.MAX_VALUE),

        /**
         * The amount of time (in seconds) to use as the polling interval for an
         * automatic job running against a modem with no tags. A value of 0 will
         * prevent the automatic job from rescheduling itself, as if it had not
         * been marked as automatic. This is also used as the default time for
         * new tags created as a result of new modems being made by servers
         * (such as the IP Registration or BEP server).
         */
        NoTagPollingInterval("No Tag Polling Interval", ONE_HOUR, 0,
            Integer.MAX_VALUE),

        /**
         * How long (in seconds) to add for each additional job retry. The first
         * retry will occur <tt>RetryTimeout</tt> seconds after the first
         * failure, but the second retry should occur
         * <tt>RetryTimeout + RetryMultiplier</tt> seconds after the second
         * failure. The third retry adds another instance of
         * <tt>RetryMultiplier</tt>, and so on. This allows you to set up a
         * short wait before the first retry, but a significantly longer wait
         * until the next.
         */
        RetryMultiplier("Retry Multiplier", 60, 0, Integer.MAX_VALUE),

        /**
         * The initial amount of time to wait before the first retry. See
         * {@link #RetryMultiplier} for more information.
         */
        RetryTimeout("Retry Timeout", 60, 0, Integer.MAX_VALUE),

        /**
         * Instructs individual servers to shut down. If set to the name of a
         * server, that server will shut down the next time it updates its
         * settings. If set to "all" (not case sensitive), all servers will shut
         * down. You must clear the "all" setting before starting any servers,
         * otherwise they will immediately stop again. Relies on
         * {@link #SettingPollInterval} for reading this; if that is very large
         * or is set to 0, the server will take a long time to see that it
         * should shut down, or it will never know at all.
         */
        ServerShutdown("Shutdown Server", null, true),

        /**
         * How often (in seconds) settings should be polled from the database to
         * keep them fresh. A value of 0 indicates that settings should never be
         * polled beyond the first time. This is also used by other threads that
         * periodically check for settings that have changed and update
         * themselves. A value of 0 will prevent those updater threads from
         * running, and will require a server restart to change. Additionally, a
         * value of 0 will prevent the server from noticing when it has been
         * told to shut down, so it will need to be manually stopped.
         */
        SettingPollInterval("Setting Poll Interval", 30, 0, Integer.MAX_VALUE),

        /**
         * How long (in seconds) to wait for a response on a socket before
         * giving up and declaring the communication a failure.
         */
        SocketTimeout("Modem Socket Timeout", 50, 1, Integer.MAX_VALUE),

        /**
         * The number of threads that may be queued for execution before
         * disallowing more threads to be created (via jobs to be pulled from
         * the database, connections to the IP registration server, ...).
         * Changes to this will not take effect until the server restarts. This
         * is also the value used for the backlog size on incoming connection
         * servers.
         */
        ThreadCapacity("Thread Capacity", 25, 1, Integer.MAX_VALUE),

        /**
         * The amount of time idle threads are allowed to remain in the pool
         * before being dropped. A value of 0 indicates that threads beyond the
         * minimum pool size should never remain after completion. A value of -1
         * indicates that, once present, threads should stick around forever.
         */
        ThreadKeepalive("Thread Keepalive", 180, -1, Integer.MAX_VALUE),

        /**
         * The title for the Web UI in a browser.
         */
        Title("WebUI Title", "BlueVue Group", false),

        /**
         * How long (in seconds) after a modem update to schedule a new status
         * query job for, if one does not already exist within that time period.
         * A value of 0 means that a new status query should not be
         * automatically scheduled. A small value is likely to cause failed
         * jobs, as the modem may still be in the process of rebooting when the
         * query happens (at least, in the case of firmware updates).
         */
        UpdatedModemQueryDelay("Updated Modem Query Delay", 600, 0,
            Integer.MAX_VALUE),

        /**
         * How long (in seconds) a modem may go without a job completing
         * successfully before it is marked with a warning status.
         */
        WarnTimeout("Warning Timeout", 6 * ONE_HOUR, 1, Integer.MAX_VALUE);

        public static Setting fromString(String s)
        {
            for (Setting setting : values()) {
                if (setting.properName.equals(s)) {
                    return setting;
                }
            }

            return null;
        }

        public final String defaultValue;
        public final String properName;

        // If it's an integer setting, the minimum/maximum acceptable value
        public final int minValue;
        public final int maxValue;

        // If it's a string setting, can it be null?
        public final boolean canBeNull;

        private Setting(String dbName, int def, int min, int max)
        {
            this.properName = dbName;
            this.defaultValue = Integer.toString(def);
            this.minValue = min;
            this.maxValue = max;

            this.canBeNull = false;
        }

        private Setting(String dbName, String def, boolean canBeNull)
        {
            this.properName = dbName;
            this.defaultValue = def;
            this.canBeNull = canBeNull;

            this.minValue = Integer.MIN_VALUE;
            this.maxValue = Integer.MAX_VALUE;
        }

        @Override
        public String toString()
        {
            return properName;
        }
    }

    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /**
     * Convert from a collection of Setting objects to a set of the strings used
     * for actual database access.
     * 
     * @param settings The setting names to convert.
     * @return The database-access names for those settings.
     */
    public static Set<String> getNames(Collection<Setting> settings)
    {
        Set<String> settingNames = null;

        if (settings != null) {
            settingNames = new HashSet<String>();
            for (Setting name : settings) {
                settingNames.add(name.toString());
            }
        }

        return settingNames;
    }

    // HashMap permits null values
    // Read-only after construction
    private Map<String, String> values = new HashMap<String, String>();

    /**
     * Construct using the default settings values.
     */
    public Settings()
    {
        for (Setting setting : Setting.values()) {
            values.put(setting.properName, setting.defaultValue);
        }
    }

    /**
     * Import a group of settings.
     * 
     * @param values The raw settings to import. A map of setting proper names
     *            onto raw values. No checking is performed, so make sure to
     *            watch for {@link BadSettingException}s when retrieving values
     *            by name.
     */
    public Settings(Map<String, String> values)
    {
        this.values = new HashMap<String, String>(values);
    }

    /**
     * A shortcut for retrieving all settings from the database.
     * 
     * @param db The object to use for database access.
     * @throws DatabaseException If {@link Database#getSettings(Collection)}
     *             throws it.
     */
    public Settings(Database db)
        throws DatabaseException
    {
        this(db, null);
    }

    /**
     * Get the named settings from the database.
     * 
     * @param db The object to use for database access.
     * @param settings The settings to retrieve; pass <tt>null</tt> to retrieve
     *            all settings. See {@link #getNames(Collection)} to convert
     *            from a Collection of Setting objects. Settings that are
     *            requested but not returned from the database, and have known
     *            default values, will have those default values set in this
     *            object.
     * @throws DatabaseException If {@link Database#getSettings(Collection)}
     *             throws it.
     */
    public Settings(Database db, Collection<String> settings)
        throws DatabaseException
    {
        // If settings is null, this will retrieve all settings
        values = db.getSettings(settings);
    }

    /**
     * Get the stored value of the setting, with bounds checking. If the stored
     * value is not found in this object, or if the stored value fails the
     * bounds checking, the default will be used instead. This will PANIC if the
     * default value does not parse as an integer or fails the bounds checking.
     * 
     * @param setting The object representing the setting.
     * @return The stored value, or the default as necessary.
     */
    public int getIntValue(Setting setting)
    {
        int value;

        try {
            value = getIntValue(setting.properName);

            if (value < setting.minValue) {
                throw new BadSettingException("Value must be >= " +
                                              setting.minValue,
                    setting.properName);
            }
            if (value > setting.maxValue) {
                throw new BadSettingException("Value must be <= " +
                                              setting.maxValue,
                    setting.properName);
            }
        }
        catch (BadSettingException bse) {
            // Stored setting is not an integer, is out of range, or does not
            // exist at all

            // Can't possibly not exist at all, since it was passed in as a
            // Setting instead of as a String

            logger.error("Bad setting value", bse);

            try {
                // Use the default value (there's got to be one)
                value = Integer.parseInt(setting.defaultValue);

                if (value < setting.minValue) {
                    throw new BadSettingException("Value must be >= " +
                                                  setting.minValue,
                        setting.properName);
                }
                if (value > setting.maxValue) {
                    throw new BadSettingException("Value must be <= " +
                                                  setting.maxValue,
                        setting.properName);
                }
            }
            catch (Exception e) {
                // Default value programmed in is not legal
                Server.panic(String.format(
                    "Setting '%s' has a non-integer default value", setting
                        .name()), e);

                value = 0; // Never reached, prevents compiler errors
            }
        }

        return value;
    }

    /**
     * Get the stored value of the given setting. If the stored value is not
     * found in this object, the default value will be returned instead. This
     * will PANIC if the default value must be used but cannot be parsed as an
     * integer.
     * 
     * @param name The name of the setting as it appears in the database.
     * @return The stored value, or the default value if no stored value exists.
     * @throws BadSettingException If the stored value is not an integer, or if
     *             there is no stored value and a default value cannot be
     *             located.
     */
    public int getIntValue(String name)
        throws BadSettingException
    {
        if (values.containsKey(name)) {
            try {
                return Integer.parseInt(values.get(name));
            }
            catch (NumberFormatException nfe) {
                logger.error(String.format(
                    "Setting '%s' = '%s' not an integer", name, values
                        .get(name)));

                throw new BadSettingException(
                    "Stored setting value is not an integer", name, nfe);
            }
        }
        else {
            Setting sn = Setting.fromString(name);

            if (sn != null) {
                try {
                    return Integer.parseInt(sn.defaultValue);
                }
                catch (NumberFormatException nfe) {
                    // Default value programmed in is not legal
                    Server.panic(String.format(
                        "Default value of '%s' is not an integer", name), nfe);
                    return 0; // Never reached, prevents compiler errors
                }
            }
            else {
                throw new BadSettingException(
                    "Setting was not found in database and has no default value",
                    name);
            }
        }
    }

    /**
     * Get the specified string value. This will PANIC if the default value must
     * be used but violates the constraints specified in the Setting.
     * 
     * @param setting The setting to retrieve.
     * @return The explicitly defined value, of if not found, then the default
     *         specified in the provided Setting.
     */
    public String getStringValue(Setting setting)
    {
        String value;

        try {
            value = getStringValue(setting.properName);

            if (!setting.canBeNull && value == null) {
                throw new BadSettingException(
                    "Value cannot be null, but it is", setting.properName);
            }
        }
        catch (BadSettingException bse) {
            // Stored setting is not an integer, is out of range, or does not
            // exist at all

            // Can't possibly not exist at all, since it was passed in as a
            // Setting instead of as a String

            logger.error("Bad setting value", bse);

            try {
                // Use the default value (there's got to be one)
                value = setting.defaultValue;

                if (!setting.canBeNull && value == null) {
                    throw new BadSettingException(
                        "Value cannot be null, but it is", setting.properName);
                }
            }
            catch (BadSettingException e) {
                // Default value programmed in is not legal
                Server
                    .panic(
                        String
                            .format(
                                "Setting '%s' has a null default value, but that is not allowed",
                                setting.name()), e);

                value = null; // Never reached, prevents compiler errors
            }
        }

        return value;
    }

    /**
     * Get the named string value.
     * 
     * @param name The name of the value to get.
     * @return The value, first looking at the explicitly defined settings
     *         values, and then at the default settings values if not found.
     * @throws BadSettingException If there is no explicitly defined value nor a
     *             known default value for the named setting.
     */
    public String getStringValue(String name)
        throws BadSettingException
    {
        if (values.containsKey(name)) {
            return values.get(name);
        }
        else {
            Setting sn = Setting.fromString(name);

            if (sn != null) {
                return sn.defaultValue;
            }
            else {
                throw new BadSettingException(
                    "Setting was not found in database and has no default value",
                    name);
            }
        }
    }

    /**
     * Get the set of settings that have explicit values in this object.
     */
    public Set<String> getKnownSettingNames()
    {
        return new HashSet<String>(values.keySet());
    }
}

/*
 * PasswordSettings.java
 *
 * A simple holder class for password settings.
 *
 * Jonathan Pearson
 * January 16, 2009
 *
 */

package com.sixnetio.BVB.Common;

import org.jonp.xml.Tag;

public class PasswordSettings {
	public enum ApplicableInterfaces {
		WAN(0), LAN(2), BOTH(1);
		
		public final int value;
		
		private ApplicableInterfaces(int value) {
			this.value = value;
		}
		
		public static ApplicableInterfaces find(int value) {
			for (ApplicableInterfaces ifcs : values()) {
				if (ifcs.value == value) return ifcs;
			}
			
			return null;
		}
	}
	
	public boolean passwordEnabled;
	public ApplicableInterfaces applicableInterfaces;
	public String password;
	
	/**
	 * Construct with empty values.
	 */
	
	public PasswordSettings() {}
	
	/**
	 * Construct from a tag as would be found in the Status column of the Modems
	 * table.
	 * 
	 * @param tag The XML tag containing the necessary data.
	 */
	public PasswordSettings(Tag tag) {
		if (tag == null) {
			passwordEnabled = false;
			applicableInterfaces = ApplicableInterfaces.WAN;
			password = "";
		} else {
			passwordEnabled = tag.getBooleanContents("enabled", false);
			applicableInterfaces = ApplicableInterfaces.find(tag.getIntContents("ifcs", 0));
			password = tag.getStringContents("password", "");
		}
	}
	
	public Tag asTag() {
		Tag tag = new Tag("passwordSettings");
		
		Tag temp;
		tag.addContent(temp = new Tag("enabled"));
		temp.addContent("" + passwordEnabled);
		
		tag.addContent(temp = new Tag("ifcs"));
		temp.addContent("" + applicableInterfaces.value);
		
		tag.addContent(temp = new Tag("password"));
		temp.addContent(password);
		
		return tag;
	}
}

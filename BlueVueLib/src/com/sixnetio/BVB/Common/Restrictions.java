/*
 * Restrictions.java
 *
 * Provides known license restrictions and their default values, much like
 * the Settings class.
 *
 * Jonathan Pearson
 * May 14, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.util.Utils;

public class Restrictions {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public static enum Restriction {
		/**
		 * What date the program will stop working on.
		 */
		DateLimit("endDate", "", false),

		/**
		 * Whether this license is only valid inside of SIXNET. 0 means the
		 * license is valid anywhere, 1 means the license is only valid on a
		 * SIXNET machine with access to the internal network. Default is 1 to
		 * prevent accidental globally-valid license keys from being generated.
		 */
		InternalOnly("internalToSixnet", 1, 0, 1),

		/**
		 * The maximum number of modems that may exist in the system. A value of
		 * -1 means that there is no limit.
		 */
		MaxModems("maxModems", -1, -1, Integer.MAX_VALUE),

		/**
		 * The maximum number of inbound-connection servers that may exist in
		 * the system. A value of -1 means that there is no limit.
		 */
		MaxInboundServers("maxInboundServers", -1, -1, Integer.MAX_VALUE),

		/**
		 * The maximum number of outbound-connection servers that may exist in
		 * the system. A value of -1 means that there is no limit.
		 */
		MaxOutboundServers("maxOutboundServers", -1, -1, Integer.MAX_VALUE),

		;
		
		public static Restriction fromString(String s) {
			for (Restriction restriction : values()) {
				if (restriction.properName.equals(s)) return restriction;
			}
			
			return null;
		}
		
		public final String defaultValue;
		public final String properName;
		
		// If it's an integer restriction, the minimum/maximum acceptable value
		// Otherwise, null and null
		public final Integer minValue;
		public final Integer maxValue;
		
		// If it's a string restriction, can it be null?
		// Otherwise, this is false
		public final boolean canBeNull;
		
		private Restriction(String name, int def, int min, int max) {
			this.properName = name;
			this.defaultValue = Integer.toString(def);
			this.minValue = min;
			this.maxValue = max;
			
			this.canBeNull = false;
		}
		
		private Restriction(String name, String def, boolean canBeNull) {
			this.properName = name;
			this.defaultValue = def;
			this.canBeNull = canBeNull;
			
			this.minValue = null;
			this.maxValue = null;
		}
		
		@Override
		public String toString() {
			return properName;
		}
		
		public boolean isValidValue(String value) {
			if (minValue != null && maxValue != null) {
				// Should be an integer, parse and check the range
				try {
					int ival = Integer.parseInt(value);
					
					if (minValue <= ival && ival <= maxValue) {
						return true;
					} else {
						return false;
					}
				} catch (NumberFormatException nfe) {
					return false;
				}
			} else {
				// Needs to be a string, check for null
				if (canBeNull) {
					return true;
				} else {
					return (value != null);
				}
			}
		}
	}
	
	/**
	 * Convert from a collection of Restriction objects to a set of the strings
	 * used for actual license key files.
	 * 
	 * @param restrictions The restriction names to convert.
	 * @return The license key names for those restrictions.
	 */
	public static Set<String> getNames(Collection<Restriction> restrictions) {
		Set<String> restrictionNames = null;
		
		if (restrictions != null) {
			restrictionNames = new HashSet<String>();
			for (Restriction name : restrictions) {
				restrictionNames.add(name.toString());
			}
		}
		
		return restrictionNames;
	}
	
	// HashMap permits null values
	// Read-only after construction
	private Map<String, String> values = new HashMap<String, String>();
	
	/**
	 * Construct using the default restrictions values.
	 */
	public Restrictions() {
		for (Restriction restriction : Restriction.values()) {
			values.put(restriction.properName, restriction.defaultValue);
		}
	}
	
	/**
	 * Load all restrictions from the given restrictions XML tag.
	 * 
	 * @param tag The XML tag containing restrictions.
	 */
	public Restrictions(Tag tag) {
		for (Restriction restriction : Restriction.values()) {
			Tag child = tag.getChild(restriction.properName);
			
			if (child != null) {
				values.put(restriction.properName, child.getStringContents());
			}
		}
	}
	
	/**
	 * Get the stored value of the restriction, with bounds checking. If the
	 * stored value is not found in this object, or if the stored value fails
	 * the bounds checking, the default will be used instead. This will PANIC if
	 * the default value does not parse as an integer or fails the bounds
	 * checking.
	 * 
	 * @param restriction The object representing the restriction.
	 * @return The stored value, or the default as necessary.
	 */
	public int getIntValue(Restriction restriction) {
		if (restriction.minValue == null || restriction.maxValue == null) {
			throw new IllegalStateException(
			                                "Trying to read an int value from non-int restriction '" +
			                                        restriction.properName + "'");
		}
		
		int value;
		
		try {
			value = getIntValue(restriction.properName);
			
			if (value < restriction.minValue) {
				throw new BadRestrictionException("Value must be >= " + restriction.minValue,
				                                  restriction.properName);
			}
			if (value > restriction.maxValue) {
				throw new BadRestrictionException("Value must be <= " + restriction.maxValue,
				                                  restriction.properName);
			}
		} catch (BadRestrictionException bse) {
			// Stored restriction is not an integer, is out of range, or does
			// not
			// exist at all
			
			// Can't possibly not exist at all, since it was passed in as a
			// Restriction instead of as a String
			
			logger.error("Bad restriction value", bse);
			
			try {
				// Use the default value (there's got to be one)
				value = Integer.parseInt(restriction.defaultValue);
				
				if (value < restriction.minValue) {
					throw new BadRestrictionException("Value must be >= " + restriction.minValue,
					                                  restriction.properName);
				}
				if (value > restriction.maxValue) {
					throw new BadRestrictionException("Value must be <= " + restriction.maxValue,
					                                  restriction.properName);
				}
			} catch (Exception e) {
				// Default value programmed in is not legal
				Server.panic(String.format("Restriction '%s' has a non-integer default value",
				                           restriction.name()), e);
				
				value = 0; // Never reached, prevents compiler errors
			}
		}
		
		return value;
	}
	
	/**
	 * Get the stored value of the given restriction. If the stored value is not
	 * found in this object, the default value will be returned instead. This
	 * will PANIC if the default value must be used but cannot be parsed as an
	 * integer.
	 * 
	 * @param name The name of the restriction as it appears in the license key.
	 * @return The stored value, or the default value if no stored value exists.
	 * @throws BadRestrictionException If the stored value is not an integer, or
	 *             if there is no stored value and a default value cannot be
	 *             located.
	 */
	public int getIntValue(String name) throws BadRestrictionException {
		if (values.containsKey(name)) {
			try {
				return Integer.parseInt(values.get(name));
			} catch (NumberFormatException nfe) {
				logger.error(String.format("Restriction '%s' = '%s' not an integer", name,
				                           values.get(name)));
				
				throw new BadRestrictionException("Stored restriction value is not an integer",
				                                  name, nfe);
			}
		} else {
			Restriction sn = Restriction.fromString(name);
			
			if (sn != null) {
				if (sn.minValue == null || sn.maxValue == null) {
					throw new IllegalStateException(
					                                "Trying to read an int value from non-int restriction '" +
					                                        sn.properName + "'");
				}
				
				try {
					return Integer.parseInt(sn.defaultValue);
				} catch (NumberFormatException nfe) {
					// Default value programmed in is not legal
					Server.panic(String.format("Default value of '%s' is not an integer", name),
					             nfe);
					return 0; // Never reached, prevents compiler errors
				}
			} else {
				throw new BadRestrictionException(
				                                  "Restriction was not found in license key and has no default value",
				                                  name);
			}
		}
	}
	
	public String getStringValue(Restriction restriction) {
		String value;
		
		try {
			value = getStringValue(restriction.properName);
			
			if (!restriction.canBeNull && value == null) {
				throw new BadRestrictionException("Value cannot be null, but it is",
				                                  restriction.properName);
			}
		} catch (BadRestrictionException bse) {
			// Stored restriction is not an integer, is out of range, or does
			// not
			// exist at all
			
			// Can't possibly not exist at all, since it was passed in as a
			// Restriction instead of as a String
			
			logger.error("Bad restriction value", bse);
			
			try {
				// Use the default value (there's got to be one)
				value = restriction.defaultValue;
				
				if (!restriction.canBeNull && value == null) {
					throw new BadRestrictionException("Value cannot be null, but it is",
					                                  restriction.properName);
				}
			} catch (BadRestrictionException e) {
				// Default value programmed in is not legal
				Server
				      .panic(
				             String
				                   .format(
				                           "Restriction '%s' has a null default value, but that is not allowed",
				                           restriction.name()), e);
				
				value = null; // Never reached, prevents compiler errors
			}
		}
		
		return value;
	}
	
	public String getStringValue(String name) throws BadRestrictionException {
		if (values.containsKey(name)) {
			return values.get(name);
		} else {
			Restriction sn = Restriction.fromString(name);
			
			if (sn != null) {
				return sn.defaultValue;
			} else {
				throw new BadRestrictionException(
				                                  "Restriction was not found in database and has no default value",
				                                  name);
			}
		}
	}
}

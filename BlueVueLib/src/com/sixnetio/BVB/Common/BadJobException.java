/*
 * BadJobException.java
 *
 * Thrown when an attempt to construct a generic job is unsuccessful.
 *
 * Jonathan Pearson
 * January 15, 2009
 *
 */

package com.sixnetio.BVB.Common;

public class BadJobException extends Exception {
	private final String jobType;
	private final long jobID;
	
	public BadJobException(String msg, String jobType, long jobID) {
		super(msg);
		
		this.jobType = jobType;
		this.jobID = jobID;
	}
	
	public BadJobException(String msg, String jobType, long jobID, Throwable cause) {
		super(msg, cause);
		
		this.jobType = jobType;
		this.jobID = jobID;
	}
	
	public String getMessage() {
		return String.format("%s: %s (job ID %d)", super.getMessage(), getJobType(), getJobID());
	}
	
	public String getJobType() {
		return jobType;
	}
	
	public long getJobID() {
		return jobID;
	}
}

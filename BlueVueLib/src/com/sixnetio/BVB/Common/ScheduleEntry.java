/*
 * ScheduleEntry.java
 *
 * Represents a modem state at a specific time.
 *
 * Jonathan Pearson
 * August 12, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.util.*;

import com.sixnetio.util.Utils;

/**
 * Represents the state of a modem at a specific point in time. Scheduled modem
 * states are either relative or absolute. Relative entries are relative to
 * midnight Sunday of whatever week you are in. They are used to provide the
 * weekly schedules of modems, such as "This modem should be on from 8am to 5pm
 * every weekday." Absolute entries are used to specify special states, such as
 * vacations. Something like "Fred has that week off, and nobody will be driving
 * his truck, so we'll schedule the modem in that truck to remain off for the
 * week." When a relative entry and an absolute entry collide, the absolute
 * entry should be used.
 *
 * @author Jonathan Pearson
 */
public class ScheduleEntry {
	public static final int INV_SCHEDULE = -1;
	
	/** The ID number of this schedule entry. */
	public int scheduleID;
	
	/** The ID of the modem that this entry applies to. */
	public int modemID;
	
	/** The state that the modem is in during this time period. */
	public String state;
	
	/** The start of this time period. */
	public Date startTime;
	
	/** The end of this time period. */
	public Date endTime;
	
	/**
	 * If <tt>true</tt>, {@link #startTime} and {@link #endTime} are in
	 * milliseconds since midnight, Sunday morning, of this week. If
	 * <tt>false</tt>, they are absolute with respect to the epoch.
	 */
	public boolean relative;
	
	/**
	 * Construct a new schedule entry.
	 * 
	 * @param scheduleID The ID number.
	 * @param modemID The modem ID.
	 * @param state The state that the modem will be in during this time period.
	 * @param startTime The start of this time period.
	 * @param endTime The end of this time period.
	 * @param relative Whether the period is relative to the current week.
	 */
	public ScheduleEntry(int scheduleID, int modemID, String state,
			Date startTime, Date endTime, boolean relative) {
		
		this.scheduleID = scheduleID;
		this.modemID = modemID;
		this.state = state;
		this.startTime = startTime;
		this.endTime = endTime;
		this.relative = relative;
	}
	
	/**
	 * Check whether the given date falls into this scheduled time period.
	 * 
	 * @param date The date to check.
	 * @return <tt>true</tt> = <tt>date</tt> falls into this time period
	 *   (including the start, excluding the end), <tt>false</tt> =
	 *   <tt>date</tt> does not fall into this time period.
	 */
	public boolean contains(Date date) {
		// This will receive startTime as an absolute (relative to the epoch)
		Date absoluteStart;
		Date absoluteEnd;
		
		if (relative) {
			Date weekStart = getStartOfWeek(date);
			
			// Now add the start time
			// Although it would be a safe cast to int, assuming that startTime
			// was not greater than a week (it shouldn't be), let's just
			// convert to Milliseconds for the rest of our calculations
			
			// By adding the week start (in ms since the epoch) to the start
			// time of this entry (in ms since the week start), we get the
			// absolute time of this entry for this week, in ms since the epoch
			absoluteStart = new Date(weekStart.getTime() + startTime.getTime());
			
			// Similarly, calculate the absolute ending time
			absoluteEnd = new Date(weekStart.getTime() + endTime.getTime());
		} else {
			// Absolute, much easier, already in ms since the epoch
			absoluteStart = startTime;
			absoluteEnd = endTime;
		}
		
		// If the date we are examining is in between those absolutes, this
		// entry contains the given date
		return (absoluteStart.compareTo(date) <= 0 &&
		        absoluteEnd.compareTo(date) > 0);
	}
	
	/**
	 * Get a Date object representing midnight, Sunday morning, of the week
	 * containing the given date.
	 * 
	 * @param date The date contained in the week that we want the beginning of.
	 * @return A Date representing midnight, Sunday morning, of the week
	 * containing the given date.
	 */
	public static Date getStartOfWeek(Date date) {
		// Figure out when, exactly, the beginning of the week is (midnight,
		// Sunday morning)
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		// Move back to Sunday
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
			cal.add(Calendar.DAY_OF_WEEK, -1);
		}
		
		// Move back to midnight
		cal.add(Calendar.HOUR_OF_DAY, -cal.get(Calendar.HOUR_OF_DAY));
		cal.add(Calendar.MINUTE,      -cal.get(Calendar.MINUTE));
		cal.add(Calendar.SECOND,      -cal.get(Calendar.SECOND));
		cal.add(Calendar.MILLISECOND, -cal.get(Calendar.MILLISECOND));
		
		return cal.getTime();
	}
	
	/**
	 * Check if the given job type is allowed when the modem is governed by this
	 * schedule entry.
	 * 
	 * @param jobType The job type name.
	 * @return <tt>true</tt> if the job type is allowed during this period
	 *   (either included in the comma-separated {@link #state} value, or that
	 *   value is 'Online'), or <tt>false</tt> if the job type is not allowed
	 *   (not included in the {@link #state} value, of that value is 'Offline').
	 */
	public boolean allowJobType(String jobType) {
		if (state.equals("Offline")) {
			return false;
		} else if (state.equals("Online")) {
			return true;
		} else {
			List<String> allowedTypes =
				Utils.makeArrayList(Utils.tokenize(state, ",", true));
			
			return allowedTypes.contains(jobType);
		}
	}
}

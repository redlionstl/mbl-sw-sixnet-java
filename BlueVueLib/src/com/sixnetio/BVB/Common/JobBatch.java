/*
 * JobBatch.java
 *
 * Represents a Batch, which is a simple way to group jobs.
 *
 * Jonathan Pearson
 * February 27, 2009
 *
 */

package com.sixnetio.BVB.Common;

public class JobBatch {
	public static final int INV_BATCH = -1;
	
	/** ID number of the batch. */
	public int batchID;
	
	/** User ID associated with the batch, or <tt>null</tt>. */
	public Integer userID;
	
	/** Name of the batch. */
	public String name;
	
	/**
	 * Whether the batch has been deleted (since we cannot update the History
	 * table).
	 */
	public boolean deleted;
	
	/**
	 * Number of outstanding jobs in the batch (jobs still in the Jobs table).
	 * This should be 0 for batches that were not retrieved from a database.
	 */
	public int jobCount;
	
	/**
	 * Construct a new Job Batch. This will set <tt>jobCount</tt> to its default
	 * of -1 and <tt>deleted</tt> to its default of <tt>false</tt>.
	 * 
	 * @param batchID The batch ID; pass {@link #INV_BATCH} if creating a new
	 *            one.
	 * @param userID The user ID, or <tt>null</tt>.
	 * @param name The name.
	 */
	public JobBatch(int batchID, Integer userID, String name) {
		this(batchID, userID, name, false, 0);
	}
	
	/**
	 * Construct a new Job Batch. This should only be called by a database
	 * retrieval method.
	 * 
	 * @param batchID The batch ID.
	 * @param userID The user ID, or <tt>null</tt>.
	 * @param name The name.
	 * @param deleted Whether the batch has been deleted.
	 * @param jobCount The number of outstanding jobs in this batch.
	 */
	public JobBatch(int batchID, Integer userID, String name, boolean deleted, int jobCount) {
		this.batchID = batchID;
		this.userID = userID;
		this.name = name;
		this.deleted = deleted;
		this.jobCount = jobCount;
	}
	
	/**
	 * A function common to all of the major data types to get the ID number.
	 */
	public int getId() {
		return batchID;
	}
}

/*
 * DuplicateRegistrationError.java
 *
 * Thrown when multiple objects try to register themselves as handlers for the
 * same name.
 *
 * Jonathan Pearson
 * July 14, 2009
 *
 */

package com.sixnetio.BVB.Common;

public class DuplicateRegistrationError extends Error {
	public DuplicateRegistrationError(String message) {
		super(message);
	}
	
	public DuplicateRegistrationError(String message, Throwable cause) {
		super(message, cause);
	}
}

/*
 * BlueTreeImage.java
 *
 * Represents a firmware image/TTL script/... that can be
 * loaded using the standard firmware update mechanism.
 * Includes version information.
 *
 * Jonathan Pearson
 * January 28, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.io.IOException;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;

import com.sixnetio.util.*;

public class BlueTreeImage {
	public static enum ImageType {
		/** Kernel upgrade package for BT-4000/BT-5000 series. */
		Kernel4K_5K("KBT4K/5K", "OS Update for 4K/5K"),

		/** Bluex firmware upgrade package for BT-4000/BT-5000 series. */
		Bluex4K_5K("ABT4K/5K", "FW Update for 4K/5K"),

		/** Patch-up upgrade package for BT-4000/BT-5000 series (not used). */
		Patchup4K_5K("PBT4K/5K", "Patch for 4K/5K"),

		/** Config upgrade package for BT-4000/BT-5000 series (not used). */
		Config4K_5K("CBT4K/5K", "Config for 4K/5K"),

		/** U-Boot upgrade package for BT-4000/BT-5000 series. */
		UBoot4K_5K("UBT4K/5K", "UBoot Update for 4K/5K"),

		/** GPS FW upgrade package for BT-4000/BT-5000 series. */
		Gps4K_5K("GBT4K/5K", "GPS Update for 4K/5K"),

		/** Partner application for BT-4000/BT-5000 series. */
		Application4K_5K("APP4K/4K", "Partner application for 4K/5K"),

		/** Kernel upgrade package for BT-6000/BT-5000v2 series. */
		Kernel5Kv2_6K("KBT6K/6K", "OS Update for 5Kv2/6K"),

		/** Bluex firmware upgrade package for BT-6000/BT-5000v2 series. */
		Bluex5Kv2_6K("ABT6K/6K", "FW Update for 5Kv2/6K"),

		/** Patch-up upgrade package for BT-6000/BT-5000v2 series (not used). */
		Patchup5Kv2_6K("PBT6K/6K", "Patch for 5Kv2/6K"),

		/** Config upgrade package for BT-6000/BT-5000v2 series (not used). */
		Config5Kv2_6K("CBT6K/6K", "Config for 5Kv2/6K"),

		/** U-Boot upgrade package for BT-6000/BT-5000v2 series. */
		UBoot5Kv2_6K("UBT6K/6K", "Uboot Update for 5Kv2/6K"),

		/** GPS FW upgrade package for BT-6000/BT-5000v2 series. */
		Gps5Kv2_6K("GBT6K/6K", "GPS Update for 5Kv2/6K"),

		/** Partner application for BT-6000/BT-5000v2 series. */
		Application5Kv2_6K("APP6K/6K", "Partner application for 5Kv2/6K"),

		/** Configuration AT commands in TTL format package. */
		TTL("TTL_CONF", "TTL Script"),

		/** Patch package. */
		Patch("BXPATCH", "Patch"),

		/** PRL upgrade package. */
		PRL("PRL_UPD", "PRL Update"),

		;
		
		public final String typeString;
		public final String description;
		
		private ImageType(String typeString, String description) {
			this.typeString = typeString;
			this.description = description;
		}
		
		public static ImageType fromString(String s) {
			for (ImageType imageType : values()) {
				if (imageType.typeString.equals(s)) return imageType;
			}
			
			return null;
		}
		
		@Override
		public String toString() {
			return typeString;
		}
	}
	
	// Type of the image
	private ImageType imageType;
	
	// Firmware version represented by this image
	private Version version;
	
	// Date the firmware was built
	private Date date;
	
	// Extra info in the package
	private String info;
	
	// Checksum
	private String checksum;
	
	// The actual image data
	private byte[] imageData;
	
	// Set when read from the database, but never written
	private int refCount;
	
	/**
	 * Construct a metadata-only image (useful if you're curious what's in the
	 * database, but don't want to actually transfer megabytes of data to find
	 * out).
	 * 
	 * @param imageType The image type.
	 * @param version The version.
	 * @param date The date this image was built.
	 * @param info Extra information about this image.
	 * @param checksum The image checksum.
	 * @param refCount The number of jobs that refer to this image.
	 */
	public BlueTreeImage(ImageType imageType, Version version, Date date, String info,
	        String checksum, int refCount) {
		
		this.imageType = imageType;
		this.version = version;
		this.date = new Date(date.getTime());
		
		if (info == null) {
			this.info = "";
		} else {
			this.info = info;
		}
		
		this.checksum = checksum;
		this.imageData = null;
		this.refCount = refCount;
	}
	
	/**
	 * Construct a new image from the individual pieces.
	 * 
	 * @param imageType The type of image.
	 * @param version The version number of this image.
	 * @param date The date this image was built.
	 * @param info Extra information about this image (up to 16 chars). May be
	 *            <tt>null</tt>, but that will be recorded as "".
	 * @param data The actual image data.
	 */
	public BlueTreeImage(ImageType imageType, Version version, Date date, String info, byte[] data) {
		if (imageType == null) throw new NullPointerException("imageType cannot be null");
		if (version == null) throw new NullPointerException("version cannot be null");
		if (date == null) throw new NullPointerException("date cannot be null");
		if (info == null) info = "";
		if (data == null) throw new NullPointerException("data cannot be null");
		
		this.imageType = imageType;
		this.version = version;
		this.date = new Date(date.getTime());
		this.info = info;
		
		this.imageData = new byte[data.length];
		System.arraycopy(data, 0, this.imageData, 0, data.length);
		
		this.checksum = computeBlueTreeMD5(this.imageData);
		
		this.refCount = 0;
	}
	
	/**
	 * Construct a new image from package data. Reference count defaults to 0.
	 * 
	 * @param data The package data.
	 * @throws IOException If the data is not actually a package or is an
	 *             unrecognized type.
	 */
	public BlueTreeImage(byte[] data) throws BadBTImageException {
		this(data, 0);
	}
	
	/**
	 * Construct a new image from package data.
	 * 
	 * @param data The package data.
	 * @param refCount The number of references exist to this package in the
	 *            database.
	 * @throws IOException If the data is not actually a package or is an
	 *             unrecognized type.
	 */
	public BlueTreeImage(byte[] data, int refCount) throws BadBTImageException {
		this.refCount = refCount;
		
		// Pull out the package type, version, date, other info, and MD5
		byte[] bType = new byte[16];
		byte[] bVers = new byte[16];
		byte[] bDate = new byte[16];
		byte[] bInfo = new byte[16];
		byte[] bMD5 = new byte[64];
		
		try {
			System.arraycopy(data, 0, bType, 0, 16);
			System.arraycopy(data, 16, bVers, 0, 16);
			System.arraycopy(data, 32, bDate, 0, 16);
			System.arraycopy(data, 48, bInfo, 0, 16);
			System.arraycopy(data, 64, bMD5, 0, 64);
			
			imageData = new byte[data.length - 128];
			System.arraycopy(data, 128, imageData, 0, imageData.length);
		} catch (Exception e) {
			// Probably an ArrayIndexOutOfBoundsException
			throw new BadBTImageException("Not a proper package", e);
		}
		
		String sType = new String(bType).trim();
		String sVers = new String(bVers).trim();
		String sDate = new String(bDate).trim();
		String sInfo = new String(bInfo).trim();
		String sMD5 = new String(bMD5).trim();
		
		// Make sure it's a known type
		imageType = ImageType.fromString(sType);
		if (imageType == null) {
			throw new BadBTImageException("Not a recognized package type: " + sType);
		}
		
		version = new Version(sVers);
		
		// Date is MM/DD/YY
		try {
			DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US);
			date = df.parse(sDate);
		} catch (ParseException e) {
			throw new BadBTImageException("Bad date in package: " + sDate, e);
		}
		
		info = sInfo;
		
		// Compare MD5 sums
		checksum = sMD5;
		String blueTreeMD5 = computeBlueTreeMD5(imageData);
		
		if (!checksum.equalsIgnoreCase(blueTreeMD5)) {
			throw new BadBTImageException("Checksum does not match");
		}
	}
	
	/**
	 * Get a numeric ID representing this package (based on checksum).
	 */
	public BigInteger getPackageID() {
		return new BigInteger(getChecksum(), 16);
	}
	
	/**
	 * Get the target of this image (such as Kernel, RF, GPS, ...).
	 */
	public ImageType getType() {
		return imageType;
	}
	
	/**
	 * Get the version of this image.
	 */
	public Version getVersion() {
		return version;
	}
	
	/**
	 * Get the date that this package was built.
	 */
	public Date getDate() {
		return new Date(date.getTime());
	}
	
	/**
	 * Get the extra info from the package.
	 */
	public String getInfo() {
		return info;
	}
	
	/**
	 * Get the checksum of this image.
	 */
	public String getChecksum() {
		return checksum;
	}
	
	/**
	 * Get the number of references to this package existed in the database when
	 * it was read. If the package was not read from the database, this will
	 * return 0.
	 */
	public int getRefCount() {
		return refCount;
	}
	
	/**
	 * Get the actual image data.
	 */
	public byte[] getImageData() {
		byte[] data = new byte[imageData.length];
		System.arraycopy(imageData, 0, data, 0, data.length);
		return data;
	}
	
	/**
	 * Get the data as it would appear in a package file.
	 */
	public byte[] getPackageData() {
		byte[] data = new byte[128 + imageData.length];
		
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US);
		
		System.arraycopy(getPaddedBytes(imageType.toString(), 16, (byte)0x20), 0, data, 0, 16);
		System.arraycopy(getPaddedBytes(version.toString(), 16, (byte)0x20), 0, data, 16, 16);
		System.arraycopy(getPaddedBytes(df.format(date), 16, (byte)0x20), 0, data, 32, 16);
		System.arraycopy(getPaddedBytes(info, 16, (byte)0x20), 0, data, 48, 16);
		System.arraycopy(computeBlueTreeMD5(imageData).getBytes(), 0, data, 64, 64);
		
		System.arraycopy(imageData, 0, data, 128, imageData.length);
		
		return data;
	}
	
	private static byte[] getPaddedBytes(String s, int len, byte padWith) {
		byte[] result = new byte[len];
		
		byte[] copy = s.getBytes();
		if (copy.length > len) {
			System.arraycopy(copy, 0, result, 0, result.length);
		} else {
			Arrays.fill(result, padWith);
			System.arraycopy(copy, 0, result, 0, copy.length);
		}
		
		return result;
	}
	
	private static String computeBlueTreeMD5(byte[] data) {
		String realMD5 = Utils.md5sum(data);
		
		byte[] md5Bytes = realMD5.getBytes();
		
		// Apply BlueTree's custom twiddling to that
		final int[] order =
		        {
		                2, 10, 13, 26, 15, 8, 16, 11, 17, 29, 18, 12, 22, 0, 1, 23, 3, 24, 20, 27,
		                14, 21, 25, 19, 31, 5, 6, 28, 4, 30, 7, 9
		        };
		
		byte[] reordered = new byte[md5Bytes.length];
		for (int i = 0; i < md5Bytes.length; i++) {
			reordered[order[i]] = md5Bytes[i];
		}
		
		// Mask
		byte[] maskString = "Ling@BlueTree Wileless Data Inc.".getBytes();
		for (int i = 0; i < reordered.length; i++) {
			reordered[i] ^= maskString[i];
		}
		
		return Conversion.bytesToHex(reordered);
	}
	
	@Override
	public String toString() {
		return String.format("%s v. %s", imageType.description, version.toString());
	}
	
	/** A common function for getting the ID number of the object. */
	public BigInteger getId() {
		return getPackageID();
	}
}

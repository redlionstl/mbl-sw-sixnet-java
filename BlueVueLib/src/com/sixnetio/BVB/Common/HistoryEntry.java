/*
 * HistoryEntry.java
 *
 * Represents an item in the History table.
 *
 * Jonathan Pearson
 * February 11, 2009
 *
 */

package com.sixnetio.BVB.Common;

import java.util.Date;

public class HistoryEntry {
	/**
	 * Used to represent an unknown history entry; will never show up in the
	 * database.
	 */
	public static final long INV_HISTORY = -1;
	
	public final long historyID;
	public final long jobID;
	public final Integer batchID;
	public final int modemID;
	public final Integer userID;
	public final Date timestamp;
	public final String result;
	public final String type;
	public final String message;
	
	public HistoryEntry(long historyID, long jobID, Integer batchID, int modemID, Integer userID,
	        Date timestamp, String result, String type, String message) {
		
		this.historyID = historyID;
		this.jobID = jobID;
		this.batchID = batchID;
		this.modemID = modemID;
		this.userID = userID;
		this.timestamp = new Date(timestamp.getTime());
		this.result = result;
		this.type = type;
		this.message = message;
	}
	
	/** A common function for getting the ID number of the object. */
	public long getId() {
		return historyID;
	}
}

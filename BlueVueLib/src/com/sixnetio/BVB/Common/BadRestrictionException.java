/*
 * BadRestrictionException.java
 *
 * Analogous to BadSettingException, but for Restrictions.
 *
 * Jonathan Pearson
 * May 14, 2009
 *
 */

package com.sixnetio.BVB.Common;

public class BadRestrictionException extends Exception {
	private final String restrictionName;
	
	public BadRestrictionException(String message, String restrictionName) {
		super(String.format("%s: %s", message, restrictionName));
		
		this.restrictionName = restrictionName;
	}
	
	public BadRestrictionException(String message, String restrictionName, Throwable cause) {
		super(String.format("%s: %s", message, restrictionName), cause);
		
		this.restrictionName = restrictionName;
	}
	
	public String getRestrictionName() {
		return restrictionName;
	}
}

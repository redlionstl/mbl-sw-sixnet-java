/*
 * BadProviderException.java
 *
 * Thrown when it is not possible to create a connection to a specific provider's database. 
 *
 * Jonathan Pearson
 * January 15, 2009
 *
 */

package com.sixnetio.BVB.Database;

public class BadProviderException extends Exception {
	private String provider;
	
	public BadProviderException(String msg, String provider) {
		super(msg);
		
		this.provider = provider;
	}
	
	public BadProviderException(String msg, String provider, Throwable cause) {
		super(msg, cause);
		
		this.provider = provider;
	}
	
	public String toString() {
		return String.format("%s: %s", getMessage(), getProvider());
	}
	
	public String getProvider() {
		return provider;
	}
}

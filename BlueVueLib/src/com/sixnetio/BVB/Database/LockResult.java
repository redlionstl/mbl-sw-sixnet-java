/*
 * LockResult.java
 *
 * The possible outcomes when trying to acquire a lock.
 *
 * Jonathan Pearson
 * February 17, 2009
 *
 */

package com.sixnetio.BVB.Database;

public enum LockResult {
	/** If the lock was acquired by the locking action. */
	Acquired,

	/** If no change was necessary, as the lock was already held by this system. */
	NoChange,

	/** If the lock could not be acquired because it was held by another system. */
	Failed,
}

/*
 * DriverException.java
 *
 * Thrown when a database driver cannot be loaded. More specific than
 * a ClassNotFoundException.
 *
 * Jonathan Pearson
 * January 15, 2009
 *
 */

package com.sixnetio.BVB.Database;

public class DriverException extends Exception {
	public DriverException(String message) {
		super(message);
	}
	
	public DriverException(String message, Throwable cause) {
		super(message, cause);
	}
}

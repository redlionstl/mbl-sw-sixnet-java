/*
 * ObjectInUseException.java
 *
 * Thrown when a database action is attempted that
 * requires locked access to an object, but the object
 * has been locked by somebody else.
 *
 * Jonathan Pearson
 * February 11, 2009
 *
 */

package com.sixnetio.BVB.Database;

public class ObjectInUseException extends Exception {
	public ObjectInUseException(String message) {
		super(message);
	}
	
	public ObjectInUseException(String message, Throwable cause) {
		super(message, cause);
	}
}

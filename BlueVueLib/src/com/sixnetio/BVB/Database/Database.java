/*
 * Database.java
 *
 * An abstract interface to a database.
 *
 * Jonathan Pearson
 * January 14, 2009
 *
 */

package com.sixnetio.BVB.Database;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.sql.*;
import java.text.ParseException;
import java.util.*;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.Restrictions.Restriction;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.SortColumns.*;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.*;

/**
 * A generic database with connection pooling.
 * <p>
 * The connection pool works as a double-ended queue. Connections are pulled
 * from the head, used, and added to a second queue. A thread monitors this
 * second queue, pulling off each connection and verifying that it is still good
 * before pushing it onto the head of the connections queue. As a result, the
 * most recently used connections are the ones that get used again.
 * </p>
 * <p>
 * To keep the older connections from going stale and disconnecting on their
 * own, another thread periodically pulls a connection from the tail of the
 * connections queue (the least recently used connection) and verifies it. If
 * good, it goes onto the head.
 * </p>
 */
public abstract class Database
    implements ShutdownListener
{
    static final Logger logger = Logger.getLogger(Utils.thisClassName());

    /**
     * The current schema version. Make sure to update all instances of
     * {@link #upgradeDatabase(Connection)} if you change this.
     */
    public static final int EXPECTED_VERSION = 2;

    /**
     * A reason for connecting to the database. Used for enforcing license key
     * restrictions.
     * 
     * @author Jonathan Pearson
     */
    public static enum ConnectionReason
    {
        /** Connecting as a client. */
        Client,

        /** Connecting as an outbound-connection (job processing type) server. */
        OutboundServer,

        /**
         * Connecting as an inbound-connection (BEP or IP-Registration type)
         * server.
         */
        InboundServer;
    }

    /**
     * Runs in the background, moving connections from the
     * <code>returningConnections</code> queue to the <code>connections</code>
     * queue after testing each one for validity.
     */
    private class ConnectionReturner
        extends Thread
    {
        public ConnectionReturner()
        {
            super("Connection Returner");
        }

        @Override
        public void run()
        {
            while (connected) {
                Connection conn;
                synchronized (returningConnections) {
                    while (connected && returningConnections.isEmpty()) {
                        try {
                            returningConnections.wait(3000);
                        }
                        catch (InterruptedException ie) {
                            // Ignore it
                        }
                    }

                    conn = returningConnections.poll();
                }

                if (conn != null) {
                    boolean killit = !validate(conn);

                    if (killit) {
                        try {
                            conn.close();
                        }
                        catch (SQLException sqle) {
                            // Ignore it
                        }
                    }

                    synchronized (connections) {
                        if (killit) {
                            logger
                                .info("ConnectionReturner killing invalid connection");
                            curPoolSize--;
                        }
                        else {
                            logger
                                .debug("ConnectionReturner returning valid connection");
                            connections.addFirst(conn);
                        }

                        connections.notify();
                    }
                }
            }
        }
    }

    /**
     * Periodically tests connections from the <code>connections</code> queue,
     * discarding them if they are broken and keeping them if not.
     */
    private class ConnectionTester
        extends Thread
    {
        public ConnectionTester()
        {
            super("Connection Tester");
        }

        @Override
        public void run()
        {
            while (!disconnecting) {
                try {
                    Thread.sleep(5000);
                }
                catch (InterruptedException ie) {
                    // We should only be interrupted if disconnecting, no harm
                    // in looping around (worst case is an extra few seconds of
                    // waiting)
                    continue;
                }

                Connection conn;
                synchronized (connections) {
                    while (!disconnecting && connections.isEmpty()) {
                        try {
                            connections.wait(3000);
                        }
                        catch (InterruptedException ie) {
                            // Ignore it
                        }
                    }

                    if (disconnecting) {
                        // Don't bother testing another connection while
                        // disconnecting
                        conn = null;
                    }
                    else {
                        conn = connections.poll();
                    }
                }

                if (conn != null) {
                    boolean killit = !validate(conn);

                    if (killit) {
                        try {
                            conn.close();
                        }
                        catch (SQLException sqle) {
                            // Ignore it
                        }
                    }

                    synchronized (connections) {
                        if (killit) {
                            logger
                                .info("ConnectionTester killing invalid connection");
                            curPoolSize--;
                        }
                        else {
                            logger
                                .debug("ConnectionTester refreshed valid connection");
                            connections.addFirst(conn);
                        }

                        connections.notify();
                    }
                }
            }
        }
    }

    private static Map<String, Class<? extends Database>> knownProviders =
        new HashMap<String, Class<? extends Database>>();

    /**
     * Register a database class with a provider name.
     * 
     * @param provider The name of the provider of the database that this class
     *            can interface with.
     * @param dbClass The class that is able to interface with that provider's
     *            database.
     */
    public static void registerProvider(String provider,
                                        Class<? extends Database> dbClass)
    {
        if (dbClass == null) {
            throw new NullPointerException("dbClass may not be null");
        }

        logger.debug(String.format(
            "Registering database provider '%s' with class '%s'", provider,
            dbClass.getName()));

        synchronized (knownProviders) {
            if (knownProviders.put(provider, dbClass) != null) {
                throw new DuplicateRegistrationError(
                    "Duplicate database with provider '" + provider + "'");
            }
        }
    }

    /**
     * Get a set of known database provider names.
     */
    public static Set<String> getKnownProviders()
    {
        synchronized (knownProviders) {
            return new HashSet<String>(knownProviders.keySet());
        }
    }

    /**
     * Get a Database that can interface with the named provider.
     * 
     * @param provider The name of the database provider. See
     *            {@link #getKnownProviders()} for a list of providers for whom
     *            Database objects can be created.
     * @return A new Database.
     * @throws BadProviderException If there is no registered database for the
     *             named provider, or it was not possible to instantiate the
     *             database for the named provider.
     * @throws DriverException If the JDBC driver could not be loaded.
     */
    public static Database getInstance(String provider)
        throws BadProviderException, DriverException
    {
        NDC.push(String.format("provider = '%s'", provider));

        try {
            logger.debug("Attempting to construct a new database");

            Class<? extends Database> dbClass;
            synchronized (knownProviders) {
                dbClass = knownProviders.get(provider);
            }

            if (dbClass == null) {
                logger.error("Provider not registered");

                throw new BadProviderException("Unknown provider", provider);
            }

            try {
                logger.debug(String.format("Calling constructor of '%s'",
                    dbClass.getName()));

                Constructor<? extends Database> constructor =
                    dbClass.getConstructor(String.class);
                return constructor.newInstance(provider);
            }
            catch (Exception e) {
                // NoSuchMethodException, SecurityException (getConstructor)
                // InstantiationException or IllegalAccessException,
                // IllegalArgumentException, InvocationTargetException
                // (newInstance)

                if (e instanceof InvocationTargetException) {
                    if (e.getCause() instanceof DriverException) {
                        throw (DriverException)e.getCause();
                    }
                }

                logger.error(String.format(
                    "Unable to instantiate database class '%s'", dbClass
                        .getName()), e);

                throw new BadProviderException(
                    "Unable to instantiate Database", provider, e);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /** Name of database provider (SQL dialect). */
    private final String provider;

    /** Maximum connection pool size. */
    private int maxPoolSize;

    /** Current thread pool size. */
    volatile int curPoolSize;

    /** True while connected to the database, even if currently disconnecting. */
    volatile boolean connected = false;

    /**
     * True while disconnecting or disconnected from the database.
     */
    volatile boolean disconnecting = true;

    /**
     * Tests and returns connections to the connection pool as long as we are
     * connected.
     */
    private ConnectionReturner returner;

    /**
     * Tests the oldest connections from the connection pool and sticks the good
     * ones back in at the front, as long as we are not disconnecting.
     */
    private ConnectionTester tester;

    /**
     * If non-<tt>null</tt>, every access has the current time compared to this.
     * If that comparison shows we are past the date restriction, a server
     * shutdown request is pushed through. This is volatile because it is
     * updated regularly by the SettingsRefresher thread. If it is <tt>null</tt>
     * , no time restriction is enforced.
     */
    private volatile Date licenseKeyDateRestriction = null;

    /** The connection pool (only holds connections not in use). */
    Deque<Connection> connections = new LinkedList<Connection>();

    /**
     * Connections that have finished being used, but have not yet been tested
     * and returned to the pool.
     */
    Queue<Connection> returningConnections = new LinkedList<Connection>();

    /** Address (not URL) of the database server. */
    private String server;

    /** Name of the database on the database server. */
    private String dbName;

    /** Name of the RDBMS user as whom queries will be executed. */
    private String user;

    /** Password of the RDBMS user. */
    private String passwd;

    /**
     * Construct a new Database with the given provider name. It is acceptable
     * for subclass constructors to throw {@link DriverException}s. Defaults to
     * an initial maximum pool size of 1, but you can change that with
     * {@link #setMaxPoolSize(int)}.
     * 
     * @param provider The name of the database provider.
     */
    public Database(String provider)
    {
        this.provider = provider;
        this.maxPoolSize = 1;
    }

    /**
     * Get the name of the provider of the database that this object is set to
     * interface with.
     */
    public String getProvider()
    {
        return provider;
    }

    /**
     * Get the maximum number of open connections that are allowed.
     */
    public int getMaxPoolSize()
    {
        return maxPoolSize;
    }

    /**
     * Get the current pool size.
     */
    public int getCurrentPoolSize()
    {
        return curPoolSize;
    }

    /**
     * Set the maximum number of allowed connections.
     * 
     * @throws IllegalArgumentException if val is less than 1.
     */
    private void setMaxPoolSize(int val)
    {
        if (val < 1) {
            throw new IllegalArgumentException(
                "Not a legal maximum connection pool size: " + val);
        }

        this.maxPoolSize = val;
    }

    /**
     * Read the expiration date out of the Server's license key. This should be
     * called periodically by a settings-refresher thread.
     */
    public void updateLicenseKeyDateRestriction()
    {
        LicenseKey licenseKey = Server.getLicenseKey();

        if (licenseKey == null) {
            // The server is panicking, so just return
            return;
        }

        String dateRestriction =
            licenseKey.getRestrictions().getStringValue(Restriction.DateLimit);

        Date endDate;
        if (dateRestriction.length() > 0) {
            try {
                endDate = Utils.parseTZDateTime(dateRestriction + " UTC");
            }
            catch (ParseException pe) {
                logger.error("Unable to parse license key date restriction, "
                             + "assuming there is none");

                endDate = null;
            }
        }
        else {
            endDate = null;
        }

        licenseKeyDateRestriction = endDate;
    }

    /**
     * Get a connection from the connection pool, creating a new one if
     * necessary and possible.
     * 
     * @return The connection.
     * @throws DatabaseException If a new connection is attempted but fails for
     *             some reason.
     * @throws IllegalStateException If the database is not connected.
     */
    private Connection getConnection()
        throws DatabaseException
    {
        if (!connected) {
            logger.error("Cannot retrieve connection when not connected");
            throw new IllegalStateException("Not connected");
        }
        else if (disconnecting) {
            logger.error("Cannot retrieve connection when disconnecting");
            throw new IllegalStateException("Database is disconnecting");
        }

        // By setting a flag and dealing with it later, we actually manage to
        // acquire a connection and log the message to the database before
        // shutting down the server
        boolean passedLicenseExpiration = testLicenseKeyExpired();

        logger.debug("Requesting a database connection");
        final Connection conn;
        synchronized (connections) {
            while (!disconnecting && connections.isEmpty()) {
                // No available connections
                // Can we open a new one?
                if (curPoolSize < maxPoolSize) {
                    // Yes, open a new one
                    logger.debug("Pool is small, opening a new connection");

                    try {
                        connections.add(getNewConnection());
                        curPoolSize++;
                    }
                    catch (Exception e) {
                        logger.error(
                            "Unable to open a new database connection", e);

                        throw new DatabaseException(
                            "Unable to establish a new connection to the database: " +
                                e.getMessage(), e);
                    }
                }
                else {
                    // No, we need to wait for an old one
                    try {
                        connections.wait(3000);
                    }
                    catch (InterruptedException ie) {
                        // Ignore it
                    }
                }
            }

            conn = connections.poll();
        }

        if (conn == null) {
            // Must be disconnecting
            logger.error("Cannot retrieve connection when disconnecting");
            throw new IllegalStateException("Database is disconnecting");
        }

        if (passedLicenseExpiration) {
            try {
                log(conn, Utils.makeArrayList(new LogEntry(new Date(), null,
                    DBLogger.ERROR, "License key expired")));
            }
            catch (Exception e) {
                logger.error("Exception logging license expiration", e);
            }

            Server.shutdown("License key expired");

            returnConnection(conn);

            throw new DatabaseException("License key expired");
        }

        return conn;
    }

    /**
     * Test whether the license key has expired.
     * 
     * @return True if the license key has expired. False if not.
     */
    private boolean testLicenseKeyExpired()
    {
        final Date expirationDate = licenseKeyDateRestriction;
        if (expirationDate != null) {
            final Date now = new Date();
            return (now.compareTo(expirationDate) > 0);
        }
        else {
            return false;
        }
    }

    /**
     * Return a connection to the connection pool. This should be called once
     * for EVERY call to getConnection(), otherwise we will leak connections and
     * eventually deadlock. If the connection pool is larger that the max size,
     * the connection will be closed instead. This could happen if someone
     * called {@link #setMaxPoolSize(int)}.
     */
    private void returnConnection(Connection con)
    {
        logger.debug("Returning connection");

        // Make sure the pool is the proper size first
        if (Server.getServerSettings() != null) {
            // Don't want to do this if there are no settings
            // That can happen when initializing the database; need to keep the
            // connection pool at 1 for that
            setMaxPoolSize(Server.getServerSettings().getIntValue(
                Setting.MaxDBConnections));
        }

        synchronized (returningConnections) {
            returningConnections.add(con);
            returningConnections.notify();
        }
    }

    /**
     * Connect to the database, optionally initializing it. This is synchronized
     * to prevent multiple connect calls from running at the same time and
     * messing things up.
     * 
     * @param server The address of the server hosting the database.
     * @param dbName The name of the database on the server, or <tt>null</tt> to
     *            connect to the administrative or default database (used when
     *            initializing a new database).
     * @param userName The user name with which to authenticate.
     * @param password The password with which to authenticate.
     * @param setup If true, does not try to read settings from the database.
     *            You must provide a license key if you pass true. If false,
     *            reads settings from the database. You must provide a database
     *            name if you pass false.
     * @param licenseKey An external license key to use. If provided, does not
     *            attempt to read/validate the license key stored in the
     *            database.
     * @param reason The reason for the connection.
     * @throws SQLException If there was a problem connection, such as a bad
     *             name/password combination.
     * @throws IllegalStateException If you try to connect again before
     *             disconnecting.
     */
    public synchronized final void connect(String server, String dbName,
                                           String userName, String password,
                                           boolean setup,
                                           LicenseKey licenseKey,
                                           ConnectionReason reason)
        throws DatabaseException
    {

        if (connected) {
            logger.error("Cannot connect to a database when already connected");
            throw new IllegalStateException(
                "You cannot connect an already connected Database");
        }

        if (licenseKey != null && !licenseKey.isValid()) {
            throw new DatabaseException("Invalid external license key.");
        }

        logger.debug(String.format(
            "Connecting to database '%s' on server '%s'", dbName, server));

        this.server = server;
        this.dbName = dbName;
        this.user = userName;
        this.passwd = password;

        synchronized (connections) {
            connections.clear();
            returningConnections.clear();

            try {
                connections.add(getNewConnection());
            }
            catch (SQLException sqle) {
                throw new DatabaseException(
                    "Unable to get new connection to database: " +
                        sqle.getMessage(), sqle);
            }
        }

        curPoolSize = 1;
        connected = true;
        disconnecting = false;

        // We want to gracefully disconnect from the database
        Server.registerShutdownListener(this);

        // Without this, connection requests will hang
        returner = new ConnectionReturner();
        returner.start();

        tester = new ConnectionTester();
        tester.start();

        // Check the database version
        // If the initializing, skip this since there won't be a settings table
        if (!setup) {
            // Immediately, request settings
            Server.refreshSettingsNow(this);

            int version =
                Server.getServerSettings().getIntValue(Setting.DatabaseVersion);
            if (version != EXPECTED_VERSION) {
                disconnect();

                throw new DatabaseException(
                    String
                        .format(
                            "Database is not running the proper version. Expected %d, but found %d.",
                            EXPECTED_VERSION, version));
            }

            if (licenseKey == null) {
                licenseKey = Server.getLicenseKey();

                // Verify the license key from the database
                if (licenseKey == null) {
                    // The server is panicking, so just return
                    return;
                }
            }

            // Enforce license key restrictions on server counts (requires
            // database settings)
            switch (reason) {
                case Client: {
                    // No limit on clients

                    break;
                }
                case InboundServer: {
                    int maxInboundServers =
                        licenseKey.getRestrictions().getIntValue(
                            Restriction.MaxInboundServers);

                    if (maxInboundServers != -1) {
                        Collection<Capabilities> caps =
                            getCurrentCapabilities();

                        // Search through the capabilities for inbound servers
                        // (extra includes a type of 'inbound')
                        for (Iterator<Capabilities> itCaps = caps.iterator(); itCaps
                            .hasNext();) {
                            Capabilities cap = itCaps.next();

                            if (!cap.getExtra().getStringContents("type")
                                .equals("inbound")) {
                                itCaps.remove();
                            }
                        }

                        // Whatever's left are inbound servers, check the count
                        if (caps.size() >= maxInboundServers) {
                            disconnect();

                            throw new DatabaseException(
                                "Unable to connect to database: Application license inbound server limit reached.");
                        }
                    }

                    break;
                }
                case OutboundServer: {
                    int maxOutboundServers =
                        licenseKey.getRestrictions().getIntValue(
                            Restriction.MaxOutboundServers);
                    if (maxOutboundServers != -1) {
                        Collection<Capabilities> caps =
                            getCurrentCapabilities();

                        // Search through the capabilities for outbound servers
                        // (extra includes a type of 'outbound')
                        for (Iterator<Capabilities> itCaps = caps.iterator(); itCaps
                            .hasNext();) {
                            Capabilities cap = itCaps.next();

                            if (!cap.getExtra().getStringContents("type")
                                .equals("outbound")) {
                                itCaps.remove();
                            }
                        }

                        // Whatever's left are outbound servers, check the count
                        if (caps.size() >= maxOutboundServers) {
                            disconnect();

                            throw new DatabaseException(
                                "Unable to connect to database: Application license outbound server limit reached.");
                        }
                    }

                    break;
                }
            }
        }

        // Enforce the 'InternalOnly' restriction (doesn't require database
        // settings)
        if (licenseKey.getRestrictions().getIntValue(Restriction.InternalOnly) == 1) {
            // Make sure we can access svr1 and that the IP address is
            // correct
            try {
                InetAddress svr1 = InetAddress.getByName("svr1");
                if (!svr1.getHostAddress().equals("10.128.0.1")) {
                    throw new Exception();
                }
            }
            catch (Exception e) {
                disconnect();

                throw new DatabaseException(
                    "Unable to connect to database: Application license limits use to SIXNET internal computers.");
            }
        }

        String dateRestriction =
            licenseKey.getRestrictions().getStringValue(Restriction.DateLimit);
        if (dateRestriction.length() > 0) {
            // Dates are all in UTC, it's just a matter of which time zone was
            // used when the date was created
            Date endDate;

            try {
                // To prevent the local time zone from being used on a UTC time,
                // specify the UTC time zone
                endDate = Utils.parseTZDateTime(dateRestriction + " UTC");
            }
            catch (ParseException pe) {
                disconnect();

                throw new DatabaseException(
                    "Unable to parse license key date restriction");
            }

            // This stores the current time in UTC
            Date nowDate = new Date();

            // Compare the dates
            if (nowDate.compareTo(endDate) >= 0) {
                disconnect();

                throw new DatabaseException(
                    "Unable to connect to database: Application license limits use to dates before " +
                        Utils.formatStandardDateTime(endDate, false));
            }

            licenseKeyDateRestriction = endDate;
        }
    }

    /**
     * A wrapper around subConnect. If necessary, this will allow standard
     * options to be applied to each connection.
     * 
     * @throws SQLException If there is a problem communicating with the
     *             database.
     */
    private synchronized Connection getNewConnection()
        throws DatabaseException, SQLException
    {
        return subConnect(server, dbName, user, passwd);
    }

    /**
     * Initiate a connection to a database.
     * 
     * @param server The address of the server hosting the database.
     * @param dbName The name of the database on the server, or <tt>null</tt> to
     *            connect to the administrative/default database (used during
     *            database initialization).
     * @param userName The user name with which to authenticate.
     * @param password The password with which to authenticate.
     * @return The SQL Connection object that was created.
     * @throws SQLException If there was a problem connection, such as a bad
     *             name/password combination.
     */
    protected abstract Connection subConnect(String server, String dbName,
                                             String userName, String password)
        throws DatabaseException, SQLException;

    /**
     * Disconnect from the database. Does nothing if not connected. Blocks until
     * all connections are closed. If interrupted, you may call it again to
     * finish disconnecting.
     * 
     * @throws DatabaseException If there is a problem disconnecting. All
     *             connections will have had their 'close()' method called by
     *             the time this is thrown, though, so the database will be
     *             fully disconnected.
     * @throws InterruptedOperationException If the thread is interrupted while
     *             waiting for busy connections.
     */
    public void disconnect()
        throws DatabaseException
    {
        if (!connected) {
            logger.debug("Cannot disconnect when not connected");
            return; // Nothing to do
        }

        logger.debug("Disconnecting from the database");

        // If any close fails, set to true so we can throw an exception
        boolean failed = false;
        synchronized (connections) {
            // Prevent getNewConnection() from returning any more connections
            disconnecting = true;

            // Loop through connections in the pool, closing each one
            while (curPoolSize > 0) {
                Connection con = connections.poll();

                // If null, this connection is in use
                if (con == null) {
                    // Wait for it to return
                    try {
                        connections.wait();

                        // Loop back around and try to acquire it again
                        continue;
                    }
                    catch (InterruptedException ie) {
                        throw new InterruptedOperationException(
                            "Interrupted while disconnecting from the database");
                    }
                }

                // Close the connection
                try {
                    con.close();
                }
                catch (SQLException sqle) {
                    logger.error("Failed to close a connection", sqle);
                    failed = true;
                }

                // Decrement the current pool size
                curPoolSize--;
            }

            // We are now fully disconnected
            connected = false;
        }

        // Whether or not we 'failed', we are no longer connected at all
        Server.unregisterShutdownListener(this);

        while (returner.isAlive() || tester.isAlive()) {
            try {
                returner.join();
                tester.join();
            }
            catch (InterruptedException ie) {
                logger.warn("Interrupted waiting for cleanup threads to die",
                    ie);
            }
        }

        // At least one connection was not happy about having its 'close()'
        // method called
        if (failed) {
            throw new DatabaseException(
                "One or more connections threw a SQLException upon being closed");
        }
    }

    /**
     * Shut down the database gracefully.
     * 
     * @throws InterruptedOperationException If interrupted while shutting down.
     */
    @Override
    public void serverShutdown()
    {
        try {
            disconnect();
        }
        catch (DatabaseException de) {
            logger.error("Database failed to disconnect properly", de);
        }
    }

    /**
     * Shut down the database forcefully, ignoring all exceptions.
     */
    @Override
    public void serverPanic()
    {
        while (connected) {
            try {
                disconnect();
            }
            catch (Exception e) {
                logger.error("Error while disconnecting database", e);
            }
        }
    }

    @Override
    public int getShutdownOrder()
    {
        // Go down LAST
        return Integer.MAX_VALUE;
    }

    /**
     * Check whether any database connection is open.
     */
    public final boolean isConnected()
    {
        return connected;
    }

    // Reading the database

    /**
     * Get an individual Modem object, given its Modem ID. This should include
     * statistical information.
     * 
     * @param modemID The Modem ID of the modem to retrieve.
     * @return The associated modem, or <tt>null</tt> if none was found.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Modem getModem(int modemID)
        throws DatabaseException
    {
        NDC.push(String.format("modemID = %d", modemID));

        try {
            logger.debug("getModem");

            Connection con = getConnection();

            try {
                return getModems(con, Utils.makeArrayList(modemID))
                    .get(modemID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Get a bunch of Modem objects, given their Modem IDs. This should include
     * statistical information.
     * 
     * @param modemIDs The modem IDs of the modems to retrieve.
     * @return The associated modems, mapped by ID number.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Map<Integer, Modem> getModems(Collection<Integer> modemIDs)
        throws DatabaseException
    {

        logger.debug("getModems");

        Connection con = getConnection();

        try {
            return getModems(con, modemIDs);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<Integer, Modem> getModems(
                                                     Connection con,
                                                     Collection<Integer> modemIDs)
        throws DatabaseException, SQLException;

    /**
     * Get an individual Job object, given its Job ID.
     * 
     * @param jobID The Job ID of the job to retrieve.
     * @param generic If true, will return a 'generic' job (not able to be
     *            activated), allowing for viewing of jobs on a system without
     *            the necessary job types. If false, returns a real job that can
     *            be activated. If false and the job cannot be constructed, this
     *            will return <tt>null</tt>.
     * @return The associated job, or <tt>null</tt> if none was found. This
     *         should populate the LockedBy and LockedAt fields.
     * @throws DatabaseException If there was a problem communicating with the
     *             database
     */
    public Job getJob(long jobID, boolean generic)
        throws DatabaseException
    {

        NDC.push(String.format("jobID = %d", jobID));

        try {
            logger.debug("getJob");

            Connection con = getConnection();

            try {
                return getJobs(con, Utils.makeArrayList(jobID), generic).get(
                    jobID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Get a group of Job objects by their ID numbers.
     * 
     * @param jobIDs The ID numbers of the Job objects to retrieve.
     * @param generic If true, will return 'generic' jobs (not able to be
     *            activated), allowing for viewing of jobs on a system without
     *            the necessary job types. If false, returns real jobs that can
     *            be activated. If false and a job cannot be constructed, it
     *            will not be included.
     * @return A mapping of job IDs onto Job objects. Jobs which cannot be
     *         constructed (perhaps because the job type is not known by this
     *         server) will not be present in the map.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public Map<Long, Job> getJobs(Collection<Long> jobIDs, boolean generic)
        throws DatabaseException
    {

        logger.debug("getJobs");

        Connection con = getConnection();

        try {
            return getJobs(con, jobIDs, generic);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<Long, Job> getJobs(Connection con,
                                              Collection<Long> jobIDs,
                                              boolean generic)
        throws DatabaseException, SQLException;

    /**
     * Get jobs with scheduled times in the past and were not locked, against
     * modems that were not busy. Jobs that fail to load will be deleted. This
     * will not return jobs marked as "mobileOriginated", that are not of types
     * supported by this server, or against modems that have no IP address
     * except for !DelayedUpdate type jobs (no way to contact the modem).
     * 
     * @return A map of modem IDs onto jobs against those modems. Both the
     *         modems and the jobs will have been locked when the function
     *         returns.
     */
    public Map<Integer, Job> getReadyToRunJobs()
        throws DatabaseException
    {
        logger.debug("getReadyToRunJobs");

        Connection con = getConnection();

        try {
            return getReadyToRunJobs(con);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<Integer, Job> getReadyToRunJobs(Connection con)
        throws DatabaseException, SQLException;

    /**
     * Retrieve values from the Settings table.
     * 
     * @param settingNames The names of the settings to retrieve. If
     *            <tt>null</tt>, the entire contents of the Settings table will
     *            be returned.
     * @return A map of setting name to setting value for each setting
     *         retrieved. If a requested setting was not present in the table,
     *         its name will not exist in the returned map. This specifically
     *         returns a HashMap because that permits <tt>null</tt> values,
     *         which may be necessary depending on the settings.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public HashMap<String, String> getSettings(Collection<String> settingNames)
        throws DatabaseException
    {

        logger.debug("getSettings");

        Connection con = getConnection();

        try {
            if (settingNames == null) {
                return getSettings(con, null);
            }
            else {
                // Make sure there are no concurrent modifications
                synchronized (settingNames) {
                    return getSettings(con, settingNames);
                }
            }
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection. The lock on <tt>settingNames</tt> will be held by the current
     * thread when this is called.
     */
    protected abstract HashMap<String, String> getSettings(
                                                           Connection con,
                                                           Collection<String> settingNames)
        throws DatabaseException, SQLException;

    /**
     * Get a modem tag from the database. This should fill in statistics on the
     * tags.
     * 
     * @param tagID The ID of the tag to get.
     * @return The tag with that ID, or <tt>null</tt> if it could not be found.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public ModemTag getTag(int tagID)
        throws DatabaseException
    {
        NDC.push(String.format("tagID = %d", tagID));

        try {
            logger.debug("getModemTag");

            Connection con = getConnection();

            try {
                return getTags(con, Utils.makeArrayList(tagID)).get(tagID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Get a group of modem tags from the database. This should fill in
     * statistics on the tags.
     * 
     * @param tagIDs The ID numbers of the tags to retrieve.
     * @return A mapping of tag IDs onto ModemTag objects.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public Map<Integer, ModemTag> getTags(Collection<Integer> tagIDs)
        throws DatabaseException
    {

        logger.debug("getTags");

        Connection con = getConnection();

        try {
            return getTags(con, tagIDs);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<Integer, ModemTag> getTags(Connection con,
                                                      Collection<Integer> tagIDs)
        throws DatabaseException, SQLException;

    /**
     * Get the IDs of matching tags in the system. The tags will be ordered by
     * Position.
     * 
     * @param tagID The ID of the tag, or <tt>null</tt> to skip this criteria.
     * @param tagName The name of the tag, or <tt>null</tt> to skip this
     *            criteria.
     * @param lowPollingInterval Select only tags with at least this polling
     *            interval, or <tt>null</tt> to skip this criteria.
     * @param highPollingInterval Select only tags with at most this polling
     *            interval, or <tt>null</tt> to skip this criteria.
     * @param lowPosition Select only tags with at least this position, or
     *            <tt>null</tt> to skip this criteria.
     * @param highPosition Select only tags with at most this position, or
     *            <tt>null</tt> to skip this criteria.
     * @param modemID Select only tags against this modem, or <tt>null</tt> to
     *            skip this criteria.
     * @param start The index of the first table row to include. Tables begin at
     *            0. Ignored if limit is -1.
     * @param limit The maximum number of rows to return. Use -1 to return all
     *            rows.
     * @param sortOn A list of columns to sort on, in decreasing order of
     *            precedence, or <tt>null</tt> to sort by position ascending. If
     *            synchronization is necessary, it must be handled by the
     *            caller.
     * @param matchCount If this is not <tt>null</tt> an contains at least one
     *            element, the total number of matches, irrespective of the
     *            limit, will be written into [0].
     * @return A collection containing the tag IDs of the matching tags.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public List<Integer> getMatchingTags(Integer tagID, String tagName,
                                         Integer lowPollingInterval,
                                         Integer highPollingInterval,
                                         Integer lowPosition,
                                         Integer highPosition, Integer modemID,
                                         int start, int limit,
                                         List<TagColumn> sortOn,
                                         long[] matchCount)
        throws DatabaseException
    {

        // TODO: Figure out how to check for existing configScript; the user may
        // not want to check

        NDC
            .push(String
                .format(
                    "tagID = %d, tagName = %s, lowPollingInterval = %d, highPollingInterval = %d, lowPosition = %d, highPosition = %d, modemID = %d",
                    tagID, tagName, lowPollingInterval, highPollingInterval,
                    lowPosition, highPosition, modemID));

        try {
            logger.debug("getMatchingTags");

            Connection con = getConnection();

            try {
                return getMatchingTags(con, tagID, tagName, lowPollingInterval,
                    highPollingInterval, lowPosition, highPosition, modemID,
                    start, limit, sortOn, matchCount);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract List<Integer> getMatchingTags(
                                                     Connection con,
                                                     Integer tagID,
                                                     String tagName,
                                                     Integer lowPollingInterval,
                                                     Integer highPollingInterval,
                                                     Integer lowPosition,
                                                     Integer highPosition,
                                                     Integer modemID,
                                                     int start, int limit,
                                                     List<TagColumn> sortOn,
                                                     long[] matchCount)
        throws DatabaseException, SQLException;

    /**
     * Get matches between tags and modems.
     * 
     * @param tagID The tag ID to search for, or -1 to get all tags applied to
     *            the given modem.
     * @param modemID The modem ID to search for, or -1 to get all modems with
     *            the given tag.
     * @return A collection of tag matches. If both tagID and modemID are -1,
     *         returns the entire contents of the TagMatch table.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Collection<TagMatch> getTagMatch(int tagID, int modemID)
        throws DatabaseException
    {
        NDC.push(String.format("tagID = %d, modemID = %d", tagID, modemID));

        try {
            logger.debug("getTagMatch");

            Connection con = getConnection();

            try {
                return getTagMatch(con, tagID, modemID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Collection<TagMatch> getTagMatch(Connection con,
                                                        int tagID, int modemID)
        throws DatabaseException, SQLException;

    /**
     * Get a blue tree image from the database. This will populate the reference
     * count field.
     * 
     * @param hash The hash code of the image.
     * @param metadataOnly Pass true to only read the metadata but not the
     *            actual package (to save bandwidth); false will read
     *            everything.
     * @return The image, or <tt>null</tt> if it could not be found.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public BlueTreeImage getPackage(String hash, boolean metadataOnly)
        throws DatabaseException
    {
        NDC.push(String.format("hash = %s", hash));

        try {
            logger.debug("getPackage");

            Connection con = getConnection();

            try {
                return getPackages(con, Utils.makeArrayList(hash), metadataOnly)
                    .get(hash);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Get a group of BlueTree images from the database. This will populate the
     * reference count field.
     * 
     * @param hashes The hashes of the images to retrieve.
     * @param metadataOnly Pass true to only read the metadata but not the
     *            actual package (to save bandwidth); false will read
     *            everything.
     * @return A map of hashes onto the images.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Map<String, BlueTreeImage> getPackages(Collection<String> hashes,
                                                  boolean metadataOnly)
        throws DatabaseException
    {

        logger.debug("getPackages");

        Connection con = getConnection();

        try {
            return getPackages(con, hashes, metadataOnly);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<String, BlueTreeImage> getPackages(
                                                              Connection con,
                                                              Collection<String> hashes,
                                                              boolean metadataOnly)
        throws DatabaseException, SQLException;

    /**
     * Get the entries from the capabilities table which are not out of date,
     * according to the current {@link Setting#CapabilitiesCuttoff} value.
     * 
     * @return All current entries from the capabilities table.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Collection<Capabilities> getCurrentCapabilities()
        throws DatabaseException
    {
        logger.debug("getCurrentCapabilities");

        Connection con = getConnection();

        try {
            return getCurrentCapabilities(con);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Collection<Capabilities> getCurrentCapabilities(
                                                                       Connection con)
        throws DatabaseException, SQLException;

    /**
     * Get matching packages from the database.
     * 
     * @param type The type of package, or <tt>null</tt> to skip this criteria.
     * @param lowVersion Select packages with at least this version, or
     *            <tt>null</tt> to skip this criteria.
     * @param highVersion Select packages with at most this version, or
     *            <tt>null</tt> to skip this criteria.
     * @param lowDate Select packages with at least this date, or <tt>null</tt>
     *            to skip this criteria.
     * @param highDate Select packages with at most this date, or <tt>null</tt>
     *            to skip this criteria.
     * @param checksum The checksum of the package, or <tt>null</tt> to skip
     *            this criteria.
     * @param start The index of the first row to return. Tables begin at 0.
     *            Ignored if limit is -1.
     * @param limit The maximum number of rows to return. Use -1 to return all
     *            rows.
     * @param matchCount If this is not <tt>null</tt> an contains at least one
     *            element, the total number of matches, irrespective of the
     *            limit, will be written into [0].
     * @return Checksums of matching packages, sorted by version number
     *         ascending.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Collection<String> getMatchingPackages(String type,
                                                  Version lowVersion,
                                                  Version highVersion,
                                                  Date lowDate, Date highDate,
                                                  String checksum, int start,
                                                  int limit, long[] matchCount)
        throws DatabaseException
    {

        // NOTE: It is very difficult to implement arbitrary column sorting with
        // this, since databases do not have built-in version number ordering
        // rules. Therefore, we've chosen the value that the user is most likely
        // to want to sort by.

        NDC
            .push(String
                .format(
                    "type = %s, minVersion = %s, maxVersion = %s, minDate = %s, maxDate = %s, checksum = %s",
                    type, (lowVersion == null ? null : lowVersion.toString()),
                    (highVersion == null ? null : highVersion.toString()),
                    Utils.formatStandardDateTime(lowDate, false), Utils
                        .formatStandardDateTime(highDate, false), checksum));

        try {
            logger.debug("getMatchingPackages");

            Connection con = getConnection();

            try {
                return getMatchingPackages(con, type, lowVersion, highVersion,
                    lowDate, highDate, checksum, start, limit, matchCount);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Collection<String> getMatchingPackages(
                                                              Connection con,
                                                              String type,
                                                              Version lowVersion,
                                                              Version highVersion,
                                                              Date lowDate,
                                                              Date highDate,
                                                              String checksum,
                                                              int start,
                                                              int limit,
                                                              long[] matchCount)
        throws DatabaseException, SQLException;

    /**
     * Gets the most recent status from the StatusHistory table for the given
     * modem.
     * 
     * @param modemID The modem to get properties for.
     * @param mostRecent If true, ignore <tt>minDate</tt> and <tt>maxDate</tt>
     *            and only select status entries from the most recent update.
     * @param lowDate Select status entries no earlier than this date; pass
     *            <tt>null</tt> to skip this criteria.
     * @param highDate Select status entries no later than this date; pass
     *            <tt>null</tt> to skip this criteria.
     * @return The properties that were retrieved. The output format is
     *         "propname@timestamp" --&gt; "value", with the timestamp being in
     *         'standard' format (yyyy-mm-dd hh:mm:ss).
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public Map<String, String> getModemStatus(int modemID, boolean mostRecent,
                                              Date lowDate, Date highDate)
        throws DatabaseException
    {

        NDC.push(String.format(
            "modemID = %d, mostRecent = %b, minDate = %s, maxDate = %s",
            modemID, mostRecent, Utils.formatStandardDateTime(lowDate, false),
            Utils.formatStandardDateTime(highDate, false)));

        try {
            logger.debug("getRecentModemStatus");

            Connection con = getConnection();

            try {
                return getModemStatus(con, modemID, mostRecent, lowDate,
                    highDate);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<String, String> getModemStatus(Connection con,
                                                          int modemID,
                                                          boolean mostRecent,
                                                          Date lowDate,
                                                          Date highDate)
        throws DatabaseException, SQLException;

    /**
     * Get a collection containing all modem IDs of modems matching the
     * specified criteria.
     * 
     * @param modemID The modem ID, or <tt>null</tt> to not match this criteria.
     * @param deviceID The device ID, or <tt>null</tt> to not match this
     *            criteria. Use {@link DeviceID#DEVID_UNKNOWN} to match modems
     *            without device IDs.
     * @param alias The alias, or <tt>null</tt> to not match this criteria. Use
     *            "-" to match modems without aliases.
     * @param model The model, or <tt>null</tt> to not match this criteria. Use
     *            "-" to match modems without models.
     * @param fwVersion The firmware version, or <tt>null</tt> to not match this
     *            criteria. Use "-" to match modems without firmware versions.
     * @param cfgVersion The configuration version, or <tt>null</tt> to not
     *            match this criteria.
     * @param ipAddress The IP address, or <tt>null</tt> to not match this
     *            criteria. Use "-" to match modems without addresses.
     * @param port The port, or <tt>null</tt> to not match this criteria.
     * @param phone The phone number, or <tt>null</tt> to not match this
     *            criteria. Use "-" to match modems without phone numbers.
     * @param tagID Returned modems must be members of this tag, or
     *            <tt>null</tt> to not match this criteria.
     * @param start The index of the first row to retrieve. 0 is the beginning
     *            of the table. This is only used if the limit is not -1.
     * @param limit The maximum number of rows to retrieve, or -1 to not limit
     *            the number of rows retrieved.
     * @param sortOn A list of columns to sort on, in decreasing order of
     *            precedence, or <tt>null</tt> to use the default sorting. If
     *            synchronization is necessary, it must be handled by the
     *            caller.
     * @param matchCount If this is not <tt>null</tt> an contains at least one
     *            element, the total number of matches, irrespective of the
     *            limit, will be written into [0].
     * @return All modem IDs in the database matching the specified criteria.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Collection<Integer> getMatchingModems(Integer modemID,
                                                 DeviceID deviceID,
                                                 String alias, String model,
                                                 Version fwVersion,
                                                 String cfgVersion,
                                                 String ipAddress,
                                                 Integer port, String phone,
                                                 Integer tagID, int start,
                                                 int limit,
                                                 List<ModemColumn> sortOn,
                                                 long[] matchCount)
        throws DatabaseException
    {

        NDC
            .push(String
                .format(
                    "modemID = %d, deviceID = %s, alias = %s, model = %s, fwVersion = %s, cfgVersion = %s, ipAddress = %s, port = %d, phone = %s, tagID = %d",
                    modemID, deviceID, alias, model,
                    (fwVersion != null ? fwVersion.toString() : null),
                    cfgVersion, ipAddress, port, phone, tagID));

        try {
            logger.debug("getMatchingModems");

            Connection con = getConnection();

            try {
                return getMatchingModems(con, modemID, deviceID, alias, model,
                    fwVersion, cfgVersion, ipAddress, port, phone, tagID,
                    start, limit, sortOn, matchCount);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Collection<Integer> getMatchingModems(
                                                             Connection con,
                                                             Integer modemID,
                                                             DeviceID deviceID,
                                                             String alias,
                                                             String model,
                                                             Version fwVersion,
                                                             String cfgVersion,
                                                             String ipAddress,
                                                             Integer port,
                                                             String phone,
                                                             Integer tagID,
                                                             int start,
                                                             int limit,
                                                             List<ModemColumn> sortOn,
                                                             long[] matchCount)
        throws DatabaseException, SQLException;

    /**
     * Get a collection containing all job IDs of jobs matching the specified
     * criteria.
     * 
     * @param jobID The job ID, or <tt>null</tt> to not use this criteria.
     * @param batchID The batch ID, or <tt>null</tt> to not use this criteria.
     *            Use -1 to match jobs without batches.
     * @param modemID The modem ID, or <tt>null</tt> to not use this criteria.
     * @param userID The user ID, or <tt>null</tt> to not use this criteria. Use
     *            -1 to match jobs without users.
     * @param type The job type, or <tt>null</tt> to not use this criteria.
     * @param lowDate A low end for a date search, or <tt>null</tt> to not use
     *            this criteria.
     * @param highDate A high end for a date search, or <tt>null</tt> to not use
     *            this criteria.
     * @param automatic Whether selected jobs should be automatic, or
     *            <tt>null</tt> to not use this criteria.
     * @param start The index of the first row to return. 0 is the beginning of
     *            the table. This is only used if the limit is not -1.
     * @param limit The maximum number of rows to return, or -1 to return
     *            through to the end.
     * @param sortOn A list of columns, in order of decreasing precedence, to
     *            sort on, or <tt>null</tt> to sort by the database default. If
     *            synchronization is necessary, it must be handled by the
     *            caller.
     * @param matchCount If this is not <tt>null</tt> an contains at least one
     *            element, the total number of matches, irrespective of the
     *            limit, will be written into [0].
     * @return All matching job IDs.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Collection<Long> getMatchingJobs(Long jobID, Integer batchID,
                                            Integer modemID, Integer userID,
                                            String type, Date lowDate,
                                            Date highDate, Boolean automatic,
                                            int start, int limit,
                                            List<JobColumn> sortOn,
                                            long[] matchCount)
        throws DatabaseException
    {
        NDC
            .push(String
                .format(
                    "jobID = %d, batchID = %d, modemID = %d, userID = %d, type = %s, lowDate = %s, highDate = %s, automatic = %b",
                    jobID, batchID, modemID, userID, type, Utils
                        .formatStandardDateTime(lowDate, false), Utils
                        .formatStandardDateTime(highDate, false), automatic));

        try {
            logger.debug("getMatchingJobs");

            Connection con = getConnection();

            try {
                return getMatchingJobs(con, jobID, batchID, modemID, userID,
                    type, lowDate, highDate, automatic, start, limit, sortOn,
                    matchCount);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Collection<Long> getMatchingJobs(Connection con,
                                                        Long jobID,
                                                        Integer batchID,
                                                        Integer modemID,
                                                        Integer userID,
                                                        String type,
                                                        Date lowDate,
                                                        Date highDate,
                                                        Boolean automatic,
                                                        int start, int limit,
                                                        List<JobColumn> sortOn,
                                                        long[] matchCount)
        throws DatabaseException, SQLException;

    /**
     * Get a collection containing all history entries matching the criteria
     * provided.
     * 
     * @param historyID The history ID, or <tt>null</tt> to not use this
     *            criteria.
     * @param jobID The job ID, or <tt>null</tt> to not use this criteria.
     * @param batchID The batch ID, or <tt>null</tt> to not use this criteria.
     *            Use -1 to match entries without batches.
     * @param modemID The modem ID, or <tt>null</tt> to not use this criteria.
     * @param userID The user ID, or <tt>null</tt> to not use this criteria. Use
     *            -1 to match entries without users.
     * @param jobType The job type, or <tt>null</tt> to not use this criteria.
     * @param result The job result, or <tt>null</tt> to not use this criteria.
     * @param lowDate A low end for a date search, or <tt>null</tt> to not use
     *            this criteria.
     * @param highDate A high end for a date search, or <tt>null</tt> to not use
     *            this criteria.
     * @param start The index of the first row to return. 0 is the beginning of
     *            the table. This is only used if the limit is not -1.
     * @param limit The maximum number of rows to return, or -1 to not limit the
     *            number of retrieved rows.
     * @param sortOn A list of columns to sort on, in decreasing order of
     *            precedence, or <tt>null</tt> to sort by decreasing timestamp.
     *            If synchronization is necessary, it must be handled by the
     *            caller.
     * @param matchCount If this is not <tt>null</tt> an contains at least one
     *            element, the total number of matches, irrespective of the
     *            limit, will be written into [0].
     * @return History IDs of all matching history entries sorted by timestamp,
     *         descending (newest first).
     */
    public Collection<Long> getMatchingHistory(Long historyID, Long jobID,
                                               Integer batchID,
                                               Integer modemID, Integer userID,
                                               String jobType, String result,
                                               Date lowDate, Date highDate,
                                               int start, int limit,
                                               List<HistoryColumn> sortOn,
                                               long[] matchCount)
        throws DatabaseException
    {

        NDC
            .push(String
                .format(
                    "historyID = %d, jobID = %d, batchID = %d, modemID = %d, userID = %d, jobType = %s, result = %s, lowDate = %s, highDate = %s",
                    historyID, jobID, batchID, modemID, userID, jobType,
                    result, Utils.formatStandardDateTime(lowDate, false), Utils
                        .formatStandardDateTime(highDate, false)));

        try {
            logger.debug("getMatchingHistory");

            Connection con = getConnection();

            try {
                return getMatchingHistory(con, historyID, jobID, batchID,
                    modemID, userID, jobType, result, lowDate, highDate, start,
                    limit, sortOn, matchCount);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Collection<Long> getMatchingHistory(
                                                           Connection con,
                                                           Long historyID,
                                                           Long jobID,
                                                           Integer batchID,
                                                           Integer modemID,
                                                           Integer userID,
                                                           String jobType,
                                                           String result,
                                                           Date lowDate,
                                                           Date highDate,
                                                           int start,
                                                           int limit,
                                                           List<HistoryColumn> sortOn,
                                                           long[] matchCount)
        throws DatabaseException, SQLException;

    /**
     * Get an individual history entry.
     * 
     * @param historyID The ID number of the entry to retrieve.
     * @return The entry, or <tt>null</tt> if no match was found.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public HistoryEntry getHistoryEntry(long historyID)
        throws DatabaseException
    {
        NDC.push(String.format("historyID = %d", historyID));

        try {
            logger.debug("getHistoryEntry");

            Connection con = getConnection();

            try {
                return getHistoryEntries(con, Utils.makeArrayList(historyID))
                    .get(historyID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Get a group of history entries.
     * 
     * @param historyIDs The IDs of the entries to retrieve.
     * @return A map of IDs onto entries.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public Map<Long, HistoryEntry> getHistoryEntries(Collection<Long> historyIDs)
        throws DatabaseException
    {

        Connection con = getConnection();

        try {
            return getHistoryEntries(con, historyIDs);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<Long, HistoryEntry> getHistoryEntries(
                                                                 Connection con,
                                                                 Collection<Long> historyIDs)
        throws DatabaseException, SQLException;

    /**
     * Get logs matching the specified criteria.
     * 
     * @param logID Select only the log with this ID, or <tt>null</tt> to skip
     *            this criteria.
     * @param lowDate Select logs after this date, or <tt>null</tt> to skip this
     *            criteria.
     * @param highDate Select logs before this date, or <tt>null</tt> to skip
     *            this criteria.
     * @param userID Select logs with this user ID, or <tt>null</tt> to skip
     *            this criteria. Use -1 to select entries without user IDs.
     * @param logType Select logs of this type, or <tt>null</tt> to skip this
     *            criteria.
     * @param start The index of the first row to return. 0 is the beginning of
     *            the table. This is only used if the limit is not -1.
     * @param limit The maximum number of rows to return, or -1 to return to the
     *            end of the table.
     * @param sortOn A list of columns to sort on in decreasing order of
     *            precedence, or <tt>null</tt> to sort by decreasing timestamp.
     *            If synchronization is necessary, it must be handled by the
     *            caller.
     * @param matchCount If this is not <tt>null</tt> an contains at least one
     *            element, the total number of matches, irrespective of the
     *            limit, will be written into [0].
     * @return IDs of matching logs.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Collection<Long> getMatchingLogs(Long logID, Date lowDate,
                                            Date highDate, Integer userID,
                                            String logType, int start,
                                            int limit, List<LogColumn> sortOn,
                                            long[] matchCount)
        throws DatabaseException
    {

        NDC
            .push(String
                .format(
                    "logID = %d, lowDate = %s, highDate = %s, userID = %d, logType = %s",
                    logID, Utils.formatStandardDateTime(lowDate, false), Utils
                        .formatStandardDateTime(highDate, false), userID,
                    logType));

        try {
            logger.debug("getMatchingLogs");

            Connection con = getConnection();

            try {
                return getMatchingLogs(con, logID, lowDate, highDate, userID,
                    logType, start, limit, sortOn, matchCount);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Collection<Long> getMatchingLogs(Connection con,
                                                        Long logID,
                                                        Date lowDate,
                                                        Date highDate,
                                                        Integer userID,
                                                        String logType,
                                                        int start, int limit,
                                                        List<LogColumn> sortOn,
                                                        long[] matchCount)
        throws DatabaseException, SQLException;

    /**
     * Get a log entry from the database.
     * 
     * @param logID The ID of the log entry to retrieve.
     * @return The log entry, or <tt>null</tt> if no matching entry could be
     *         found.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public LogEntry getLogEntry(long logID)
        throws DatabaseException
    {
        NDC.push(String.format("logID = %d", logID));

        try {
            logger.debug("getLogEntry");

            Connection con = getConnection();

            try {
                return getLogEntries(con, Utils.makeArrayList(logID))
                    .get(logID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Get a group of log entries from the database.
     * 
     * @param logIDs The IDs of the entries to retrieve.
     * @return A mapping of log IDs onto LogEntry objects.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public Map<Long, LogEntry> getLogEntries(Collection<Long> logIDs)
        throws DatabaseException
    {

        logger.debug("getLogEntries");

        Connection con = getConnection();

        try {
            return getLogEntries(con, logIDs);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<Long, LogEntry> getLogEntries(Connection con,
                                                         Collection<Long> logIDs)
        throws DatabaseException, SQLException;

    /**
     * Get a collection of ID numbers for batches matching the given criteria.
     * 
     * @param batchID The batch ID to search for, or <tt>null</tt> to skip this
     *            criteria.
     * @param userID The user ID to search for, or <tt>null</tt> to skip this
     *            criteria. Pass -1 to select batches without user IDs.
     * @param name The name to search for, or <tt>null</tt> to skip this
     *            criteria.
     * @param deleted Whether the batch has been deleted, or <tt>null</tt> to
     *            skip this criteria.
     * @param start The index of the first row to return. 0 is the beginning of
     *            the table. This is only used if the limit is not -1.
     * @param limit The maximum number of rows to return. Use -1 to not limit
     *            the query.
     * @param sortOn A list of columns to sort on, or <tt>null</tt> to use the
     *            default sorting. List is in order of decreasing precedence. If
     *            synchronization is necessary, it must be handled by the
     *            caller.
     * @param matchCount If this is not <tt>null</tt> an contains at least one
     *            element, the total number of matches, irrespective of the
     *            limit, will be written into [0].
     * @return A collection of ID numbers for batches matching the provided
     *         criteria.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Collection<Integer> getMatchingBatches(Integer batchID,
                                                  Integer userID, String name,
                                                  Boolean deleted, int start,
                                                  int limit,
                                                  List<BatchColumn> sortOn,
                                                  long[] matchCount)
        throws DatabaseException
    {

        NDC.push(String.format(
            "batchID = %d, userID = %d, name = %s, deleted = %b", batchID,
            userID, name, deleted));

        try {
            logger.debug("getMatchingBatches");

            Connection con = getConnection();

            try {
                return getMatchingBatches(con, batchID, userID, name, deleted,
                    start, limit, sortOn, matchCount);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Collection<Integer> getMatchingBatches(
                                                              Connection con,
                                                              Integer batchID,
                                                              Integer userID,
                                                              String name,
                                                              Boolean deleted,
                                                              int start,
                                                              int limit,
                                                              List<BatchColumn> sortOn,
                                                              long[] matchCount)
        throws DatabaseException, SQLException;

    /**
     * Get a batch structure.
     * 
     * @param batchID The ID number of the structure to retrieve.
     * @return The structure, or <tt>null</tt> if none match the ID. This should
     *         include a count of the number of outstanding jobs in the batch.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public JobBatch getBatch(int batchID)
        throws DatabaseException
    {
        NDC.push(String.format("batchID = %d", batchID));

        try {
            logger.debug("getBatch");

            Connection con = getConnection();

            try {
                return getBatches(con, Utils.makeArrayList(batchID)).get(
                    batchID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Get a group of JobBatch objects.
     * 
     * @param batchIDs The IDs of the batches to retrieve.
     * @return A mapping of batch IDs onto JobBatch objects.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public Map<Integer, JobBatch> getBatches(Collection<Integer> batchIDs)
        throws DatabaseException
    {

        logger.debug("getBatches");

        Connection con = getConnection();

        try {
            return getBatches(con, batchIDs);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<Integer, JobBatch> getBatches(
                                                         Connection con,
                                                         Collection<Integer> batchIDs)
        throws DatabaseException, SQLException;

    /**
     * Get the user with the given ID number.
     * 
     * @param userID The ID number for the user to fetch.
     * @return The user, or <tt>null</tt> if no match was found. This should
     *         include statistical information about the user.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public User getUser(int userID)
        throws DatabaseException
    {
        NDC.push(String.format("userID = %d", userID));

        try {
            logger.debug("getUser");

            Connection con = getConnection();

            try {
                return getUsers(con, Utils.makeArrayList(userID)).get(userID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    public Map<Integer, User> getUsers(Collection<Integer> userIDs)
        throws DatabaseException
    {

        logger.debug("getUsers");

        Connection con = getConnection();

        try {
            return getUsers(con, userIDs);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<Integer, User> getUsers(Connection con,
                                                   Collection<Integer> userIDs)
        throws DatabaseException, SQLException;

    /**
     * Get a collection of user IDs for users matching the specified criteria.
     * 
     * @param userID The user ID to match, or <tt>null</tt> to skip this
     *            criteria.
     * @param name The user name to match, or <tt>null</tt> to skip this
     *            criteria.
     * @param type The user type to match, or <tt>null</tt> to skip this
     *            criteria.
     * @param start The index of the first table row to return (starts at 0).
     * @param limit The number of table rows to return, or -1 to run to the end
     *            of the table.
     * @param sortOn A list of columns to sort on, in descending order or
     *            priority. If synchronization is necessary, it must be handled
     *            by the caller.
     * @param matchCount If this is not <tt>null</tt> an contains at least one
     *            element, the total number of matches, irrespective of the
     *            limit, will be written into [0].
     * @return A collection of user IDs matching the specified criteria.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public Collection<Integer> getMatchingUsers(Integer userID, String name,
                                                User.UserType type, int start,
                                                int limit,
                                                List<UserColumn> sortOn,
                                                long[] matchCount)
        throws DatabaseException
    {
        NDC.push(String.format("userID = %d, name = %s, type = %s", userID,
            name, (type == null ? null : type.toString())));

        try {
            logger.debug("getMatchingUsers");

            Connection con = getConnection();

            try {
                return getMatchingUsers(con, userID, name, type, start, limit,
                    sortOn, matchCount);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Collection<Integer> getMatchingUsers(
                                                            Connection con,
                                                            Integer userID,
                                                            String name,
                                                            User.UserType type,
                                                            int start,
                                                            int limit,
                                                            List<UserColumn> sortOn,
                                                            long[] matchCount)
        throws DatabaseException, SQLException;

    /**
     * Get a collection of schedule IDs for schedule entries matching the
     * specified criteria.
     * 
     * @param scheduleID The schedule ID, or <tt>null</tt> to skip this
     *            criteria.
     * @param modemID The modem ID, or <tt>null</tt> to skip this criteria.
     * @param relative The value of 'relative', or <tt>null</tt> to skip this
     *            criteria.
     * @return A collection of matching schedule entries.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    public Collection<Integer> getMatchingSchedules(
                                                    Integer scheduleID,
                                                    Integer modemID,
                                                    Boolean relative,
                                                    int start,
                                                    int limit,
                                                    List<ScheduleColumn> sortOn,
                                                    long[] matchCount)
        throws DatabaseException
    {

        NDC.push(String.format("scheduleID = %d, modemID = %d, relative = %b",
            scheduleID, modemID, relative));

        try {
            logger.debug("getMatchingSchedules");

            Connection con = getConnection();

            try {
                return getMatchingSchedules(con, scheduleID, modemID, relative,
                    start, limit, sortOn, matchCount);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Collection<Integer> getMatchingSchedules(
                                                                Connection con,
                                                                Integer scheduleID,
                                                                Integer modemID,
                                                                Boolean relative,
                                                                int start,
                                                                int limit,
                                                                List<ScheduleColumn> sortOn,
                                                                long[] matchCount)
        throws DatabaseException, SQLException;

    /**
     * Get a single schedule entry from the database.
     * 
     * @param scheduleID The ID of the entry to retrieve.
     * @return The entry, or <tt>null</tt> if not found.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    public ScheduleEntry getScheduleEntry(int scheduleID)
        throws DatabaseException
    {

        NDC.push(String.format("scheduleID = %d", scheduleID));

        try {
            logger.debug("getScheduleEntry");

            Connection con = getConnection();

            try {
                return getScheduleEntries(con, Utils.makeArrayList(scheduleID))
                    .get(scheduleID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Retrieve a collection of schedule entries from the database.
     * 
     * @param scheduleIDs The IDs of the entries to retrieve.
     * @return A map of ID numbers onto the corresponding entries.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    public Map<Integer, ScheduleEntry> getScheduleEntries(
                                                          Collection<Integer> scheduleIDs)
        throws DatabaseException
    {

        logger.debug("getScheduleEntries");

        Connection con = getConnection();

        try {
            return getScheduleEntries(con, scheduleIDs);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract Map<Integer, ScheduleEntry> getScheduleEntries(
                                                                      Connection con,
                                                                      Collection<Integer> scheduleIDs)
        throws DatabaseException, SQLException;

    // Writing to the database

    /**
     * Attempt to acquire the lock on the modem (change LockedBy from
     * <tt>null</tt> to the server name).
     * 
     * @param modemID The Modem ID of the modem to attempt to lock.
     * @param ms How long to attempt for before giving up, in milliseconds. If
     *            0, will only attempt once.
     * @return <tt>Acquired</tt> if this locking action acquired the lock.
     *         <tt>NoChange</tt> if the modem was already locked by this system.
     *         <tt>Failed</tt> if the modem was already locked by another
     *         system.
     * 
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     * @throws InterruptedOperationException If the thread is interrupted while
     *             working.
     */
    public LockResult lockModem(int modemID, long ms)
        throws DatabaseException
    {
        NDC.push(String.format("modemID = %d, ms = %d", modemID, ms));

        try {
            logger.debug("lockModem");

            Connection con = getConnection();

            try {
                return lockModem(con, modemID, ms);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract LockResult lockModem(Connection con, int modemID, long ms)
        throws DatabaseException, SQLException;

    /**
     * Unlock the given modem (change LockedBy to <tt>null</tt>). This should
     * succeed even if the modem does not exist.
     * 
     * @param modemID The Modem ID of the modem to unlock.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public void unlockModem(int modemID)
        throws DatabaseException
    {
        NDC.push(String.format("modemID = %d", modemID));

        try {
            logger.debug("unlockModem");

            Connection con = getConnection();

            try {
                unlockModem(con, modemID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void unlockModem(Connection con, int modemID)
        throws DatabaseException, SQLException;

    /**
     * Attempt to acquire the lock on the job (change LockedBy from
     * <tt>null</tt> to the server name).
     * 
     * @param jobID The Job ID of the job to attempt to lock.
     * @param ms How long to attempt for before giving up, in milliseconds. If
     *            0, will only attempt once.
     * @return <tt>Acquired</tt> if this locking action acquired the lock.
     *         <tt>NoChange</tt> if the job was already locked by this system.
     *         <tt>Failed</tt> if the job was already locked by another system.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     * @throws InterruptedOperationException If the thread is interrupted while
     *             working.
     */
    public LockResult lockJob(long jobID, long ms)
        throws DatabaseException
    {
        NDC.push(String.format("jobID = %d, ms = %d", jobID, ms));

        try {
            logger.debug("lockJob");

            Connection con = getConnection();

            try {
                return lockJob(con, jobID, ms);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract LockResult lockJob(Connection con, long jobID, long ms)
        throws DatabaseException, SQLException;

    /**
     * Unlock the given job (change LockedBy to <tt>null</tt>). This should
     * succeed even if the job does not exist.
     * 
     * @param jobID The Job ID of the job to unlock.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public void unlockJob(long jobID)
        throws DatabaseException
    {
        NDC.push(String.format("jobID = %d", jobID));

        try {
            logger.debug("unlockJob");

            Connection con = getConnection();

            try {
                unlockJob(con, jobID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void unlockJob(Connection con, long jobID)
        throws DatabaseException, SQLException;

    /**
     * Purge job and modem locks that are older than the cutoff.
     * 
     * @param cutoff Purge locks older than this date.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public void purgeStaleLocks(Date cutoff)
        throws DatabaseException
    {
        NDC.push(String.format("cutoff = %s", Utils.formatStandardDateTime(
            cutoff, false)));

        try {
            logger.debug("purgeStaleLocks");

            Connection con = getConnection();

            try {
                purgeStaleLocks(con, cutoff);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void purgeStaleLocks(Connection con, Date cutoff)
        throws DatabaseException, SQLException;

    /**
     * Database update for a modem. The modem must have a Modem ID; every other
     * property in the structure will be written to the database. It is up to
     * the caller to lock the modem beforehand.
     * 
     * @param modem The modem to write to the database.
     */
    public void updateModem(Modem modem)
        throws DatabaseException
    {
        NDC.push(String.format("modem.modemID = %d", modem.modemID));

        try {
            logger.debug("updateModem");

            Connection con = getConnection();

            try {
                updateModem(con, modem);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection. This is allowed to throw a DatabaseException in case
     * converting the modem status to XML fails for some reason.
     */
    protected abstract void updateModem(Connection con, Modem modem)
        throws DatabaseException, SQLException;

    /**
     * Database update for a job. The job must have a Job ID; every other
     * property in the structure will be written to the database. It is up to
     * the caller to lock the job beforehand.
     * 
     * @param job The job to write to the database.
     */
    public void updateJob(Job job)
        throws DatabaseException
    {
        NDC.push(String.format("job.jobID = %d", job.getJobData().jobID));

        try {
            logger.debug("updateJob");

            Connection con = getConnection();

            try {
                updateJob(con, job);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection. This is allowed to throw a DatabaseException in case
     * converting the job parameters to XML fails for some reason.
     */
    protected abstract void updateJob(Connection con, Job job)
        throws DatabaseException, SQLException;

    /**
     * Create a History table entry representing an attempt (successful or
     * otherwise) of this job. Does not remove the job from the Jobs table.
     * 
     * @param jobID The Job ID of the job to archive.
     * @param success Whether the attempt was successful.
     * @param message You may provide a message to go along with the history
     *            table entry. If the job failed, you should probably describe
     *            the failure here.
     * @param timestamp The time at which the job started.
     */
    public void archiveJob(long jobID, boolean success, String message,
                           Date timestamp)
        throws DatabaseException
    {

        NDC.push(String.format("jobID = %d, success = %b", jobID, success));

        try {
            logger.debug("archiveJob");

            Connection con = getConnection();

            try {
                archiveJob(con, jobID, success, message, timestamp);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void archiveJob(Connection con, long jobID,
                                       boolean success, String message,
                                       Date timestamp)
        throws DatabaseException, SQLException;

    /**
     * Delete the specified job. This procedure will only delete the job if it
     * is able to acquire the lock, or if the local machine already holds the
     * lock.
     * 
     * 
     * @param jobID The Job ID of the job to delete.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     * @throws ObjectInUseException If the job lock is held by somebody else.
     */
    public void deleteJob(long jobID)
        throws DatabaseException, ObjectInUseException
    {
        NDC.push(String.format("jobID = %d", jobID));

        try {
            logger.debug("deleteJob");

            Connection con = getConnection();

            try {
                deleteJob(con, jobID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void deleteJob(Connection con, long jobID)
        throws DatabaseException, SQLException, ObjectInUseException;

    /**
     * Save status information to the Status History table.
     * 
     * @param modemID The Modem ID of the modem whose status is being saved.
     * @param timestamp The timestamp at which the status information was
     *            retrieved from the modem.
     * @param props The properties to save to the table. A map of property name
     *            to property value.
     */
    public void saveStatusValues(int modemID, Date timestamp,
                                 Map<String, String> props)
        throws DatabaseException
    {

        NDC.push(String.format("modemID = %d", modemID));

        try {
            logger.debug("saveStatusValues");

            Connection con = getConnection();

            try {
                saveStatusValues(con, modemID, timestamp, props);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void saveStatusValues(Connection con, int modemID,
                                             Date timestamp,
                                             Map<String, String> props)
        throws DatabaseException, SQLException;

    /**
     * Schedule a new job.
     * 
     * @param job The job to schedule. The Job ID will be set to the Job ID of
     *            the created job.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void createJob(Job job)
        throws DatabaseException
    {
        logger.debug("createJob");

        Connection con = getConnection();

        try {
            createJob(con, job);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void createJob(Connection con, Job job)
        throws DatabaseException, SQLException;

    /**
     * Creates a new modem in the database.
     * 
     * @param modem The modem to create. The Modem ID will be set to the Modem
     *            ID of the created modem.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void createModem(Modem modem)
        throws DatabaseException
    {
        logger.debug("createModem");

        Connection con = getConnection();

        try {
            // Check for license key limits
            LicenseKey licenseKey = Server.getLicenseKey();

            if (licenseKey == null) {
                // The server is panicking, so just return
                return;
            }

            int maxModems =
                licenseKey.getRestrictions().getIntValue(Restriction.MaxModems);
            if (maxModems != -1) {
                long modemCount[] = new long[1];

                getMatchingModems(con, null, null, null, null, null, null,
                    null, null, null, null, 0, 1, null, modemCount);

                if (modemCount[0] >= maxModems) {
                    throw new DatabaseException(
                        "Unable to create modem: Application license modem number limit reached");
                }
            }

            createModem(con, modem);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void createModem(Connection con, Modem modem)
        throws DatabaseException, SQLException;

    /**
     * Dump a group of log entries to the Log table of the database.
     * 
     * @param logEntries The log entries to dump.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void log(Collection<LogEntry> logEntries)
        throws DatabaseException
    {
        logger.debug("log");

        Connection con = getConnection();

        try {
            // Make sure there are no concurrent modifications
            synchronized (logEntries) {
                log(con, logEntries);
            }
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection. The lock on <tt>logEntries</tt> will be held by the current
     * thread when this is called.
     */
    protected abstract void log(Connection con, Collection<LogEntry> logEntries)
        throws DatabaseException, SQLException;

    /**
     * Purge log entries older that the cutoff date.
     * 
     * @param cutoff The cutoff date.
     * @throws DatabaseException If there is a problem accessing the database.
     */
    public void purgeLog(Date cutoff)
        throws DatabaseException
    {
        NDC.push(String.format("prior to [%1$tF %1$tT]", cutoff));

        try {
            logger.debug("purgeLog");

            Connection con = getConnection();

            try {
                purgeLog(con, cutoff);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void purgeLog(Connection con, Date cutoff)
        throws DatabaseException, SQLException;

    /**
     * Remove all modem/job locks held by the given server name.
     * 
     * @param name The name of the server whose locks to delete.
     * @throws DatabaseException If there is a problem accessing the database.
     */
    public void removeLocksBy(String name)
        throws DatabaseException
    {
        NDC.push(String.format("name = '%s'", name));

        try {
            logger.debug("removeLocksBy");

            Connection con = getConnection();

            try {
                removeLocksBy(con, name);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void removeLocksBy(Connection con, String name)
        throws DatabaseException, SQLException;

    /**
     * Create a modem tag.
     * 
     * @param tag The tag to create. The Tag ID will be set to the ID of the
     *            newly created tag. If the position is -1, the next highest
     *            available position will be chosen and the position value will
     *            be set to the chosen value.
     * @throws DatabaseException If there is a problem accessing the database.
     */
    public void createTag(ModemTag tag)
        throws DatabaseException
    {
        logger.debug("createTag");

        Connection con = getConnection();

        try {
            createTag(con, tag);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void createTag(Connection con, ModemTag tag)
        throws DatabaseException, SQLException;

    /**
     * Apply a tag to a modem. This should prevent a tag from being applied
     * multiple times to the same modem.
     * 
     * @param modemID The modem ID to apply the tag to.
     * @param tagID The tag ID to apply to the modem.
     * @throws DatabaseException If there is a problem accessing the database.
     */
    public void applyTag(int modemID, int tagID)
        throws DatabaseException
    {
        NDC.push(String.format("modemID = %d, tagID = %d", modemID, tagID));

        try {
            logger.debug("applyTag");

            Connection con = getConnection();

            try {
                applyTag(con, modemID, tagID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void applyTag(Connection con, int modemID, int tagID)
        throws DatabaseException, SQLException;

    /**
     * Remove a tag from a modem.
     * 
     * @param modemID The modem ID to remove the tag from.
     * @param tagID The tag ID to remove from the modem.
     * @throws DatabaseException If there is a problem accessing the database.
     */
    public void removeTag(int modemID, int tagID)
        throws DatabaseException
    {
        NDC.push(String.format("modemID = %d, tagID = %d", modemID, tagID));

        try {
            logger.debug("removeTag");

            Connection con = getConnection();

            try {
                removeTag(con, modemID, tagID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void removeTag(Connection con, int modemID, int tagID)
        throws DatabaseException, SQLException;

    /**
     * Set the value of a setting.
     * 
     * @param settingName The name of the setting to set.
     * @param settingValue The value to set for that setting.
     */
    public void updateSetting(String settingName, String settingValue)
        throws DatabaseException
    {
        NDC.push(String.format("settingName = '%s', settingValue = '%s'",
            settingName, settingValue));

        try {
            logger.debug("updateSetting");

            Connection con = getConnection();

            try {
                updateSetting(con, settingName, settingValue);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void updateSetting(Connection con, String settingName,
                                          String settingValue)
        throws DatabaseException, SQLException;

    /**
     * Write an entry to the Capabilities table.
     * 
     * @param caps The capabilities to write.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public void updateCapabilities(Capabilities caps)
        throws DatabaseException
    {
        logger.debug("updateCapabilities");

        Connection con = getConnection();

        try {
            updateCapabilities(con, caps);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void updateCapabilities(Connection con, Capabilities caps)
        throws DatabaseException, SQLException;

    public void purgeCapabilities(Date cutoff)
        throws DatabaseException
    {
        logger.debug("purgeCapabilities");

        Connection con = getConnection();

        try {
            purgeCapabilities(con, cutoff);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void purgeCapabilities(Connection con, Date cutoff)
        throws DatabaseException, SQLException;

    public void createPackage(BlueTreeImage image)
        throws DatabaseException
    {
        logger.debug("createPackage");

        Connection con = getConnection();

        try {
            createPackage(con, image);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void createPackage(Connection con, BlueTreeImage image)
        throws DatabaseException, SQLException;

    /**
     * Delete an update package.
     * 
     * @param hash The hash of the package to delete.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void deletePackage(String hash)
        throws DatabaseException
    {
        NDC.push(String.format("hash = %s", hash));

        try {
            logger.debug("deletePackage");

            Connection con = getConnection();

            try {
                deletePackage(con, hash);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void deletePackage(Connection con, String hash)
        throws DatabaseException, SQLException;

    /**
     * Update an existing modem tag in the database.
     * 
     * @param tag The tag to update.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void updateTag(ModemTag tag)
        throws DatabaseException
    {
        NDC.push(String.format("tagID = %d", tag.tagID));

        try {
            logger.debug("updateTag");

            Connection con = getConnection();

            try {
                updateTag(con, tag);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void updateTag(Connection con, ModemTag tag)
        throws DatabaseException, SQLException;

    /**
     * Deletes the specified tag. Also removes it from the TagMatch and TagOrder
     * tables.
     * 
     * @param tagID The tag to delete.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void deleteTag(int tagID)
        throws DatabaseException
    {
        NDC.push(String.format("tagID = %d", tagID));

        try {
            logger.debug("deleteTag");

            Connection con = getConnection();

            try {
                deleteTag(con, tagID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void deleteTag(Connection con, int tagID)
        throws DatabaseException, SQLException;

    /**
     * Deletes the specified modem. Also removes it from the TagMatch table,
     * deletes its history and status history, and removes all jobs against it.
     * This procedure will only delete the modem if it is able to acquire the
     * lock, or if the local machine already holds the lock.
     * 
     * @param modemID The modem to delete.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     * @throws ObjectInUseException If the modem lock is held by somebody else.
     */
    public void deleteModem(int modemID)
        throws DatabaseException, ObjectInUseException
    {
        NDC.push(String.format("modemID = %d", modemID));

        try {
            logger.debug("deleteModem");

            Connection con = getConnection();

            try {
                deleteModem(con, modemID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void deleteModem(Connection con, int modemID)
        throws DatabaseException, SQLException, ObjectInUseException;

    /**
     * Create a new database. Sets up permissions for up to two users to connect
     * to it.
     * 
     * @param dbName The database to create.
     * @param primaryUser The primary user.
     * @param secondaryUser An optional secondary user; pass <tt>null</tt> to
     *            not have one, or pass the primary user again to share.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void createDatabase(String dbName, String primaryUser,
                               String secondaryUser)
        throws DatabaseException
    {
        logger.debug("createDatabase");

        Connection con = getConnection();

        try {
            createDatabase(con, dbName, primaryUser, secondaryUser);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void createDatabase(Connection con, String dbName,
                                           String primaryUser,
                                           String secondaryUser)
        throws DatabaseException, SQLException;

    /**
     * Create all necessary objects (except users) and permissions to go from a
     * completely empty DBMS to having a BVB database set up. Should set default
     * values for all settings, except the database version which is handled by
     * the database implementor. Make sure the users are able to connect to the
     * database that you create.
     * 
     * @param dbName The name of the database to create.
     * @param primaryUser The name of the primary user (how the job server will
     *            log in). This user should have write permissions for all
     *            tables.
     * @param secondaryUser The name of the secondary user (how the Web UI will
     *            log in), or <tt>null</tt> to assign the necessary permissions
     *            to the world (not necessarily supported by all databases). If
     *            equal to the primary user, only that one user will have
     *            access. This user probably does not need write access on some
     *            tables.
     * @param licenseKey The license key to insert into the database.
     * @throws DatabaseException If there is a problem communicating with the
     *             database, or if you pass <tt>secondaryUser = null</tt> and
     *             the database does not support global privileges.
     */
    public void initializeNewDatabase(String dbName, String primaryUser,
                                      String secondaryUser,
                                      LicenseKey licenseKey)
        throws DatabaseException
    {
        logger.debug("initializeNewDatabase");

        Connection con = getConnection();

        try {
            initializeNewDatabase(con, dbName, primaryUser, secondaryUser,
                licenseKey);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void initializeNewDatabase(Connection con,
                                                  String dbName,
                                                  String primaryUser,
                                                  String secondaryUser,
                                                  LicenseKey licenseKey)
        throws DatabaseException, SQLException;

    /**
     * Create a database user (not a BVB user). The user should be able to log
     * into the DBMS.
     * 
     * @param userName The user to create.
     * @param password The password to set for the user.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void createDatabaseUser(String userName, String password)
        throws DatabaseException
    {
        NDC.push(String.format("userName = %s", userName));

        try {
            logger.debug("createDatabaseUser");

            Connection con = getConnection();

            try {
                createDatabaseUser(con, userName, password);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void createDatabaseUser(Connection con, String userName,
                                               String password)
        throws DatabaseException, SQLException;

    /**
     * Upgrade the database to the current {@link #EXPECTED_VERSION}.
     * 
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void upgradeDatabase()
        throws DatabaseException
    {
        logger.debug("upgradeDatabase");

        Connection con = getConnection();

        try {
            upgradeDatabase(con);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void upgradeDatabase(Connection con)
        throws SQLException;

    /**
     * Purge the History and StatusHistory tables of entries older than the
     * cutoff.
     * 
     * @param cutoff The cutoff date.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void purgeHistory(Date cutoff)
        throws DatabaseException
    {
        logger.debug("purgeHistory");

        Connection con = getConnection();

        try {
            purgeHistory(con, cutoff);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void purgeHistory(Connection con, Date cutoff)
        throws DatabaseException, SQLException;

    /**
     * Create a new batch.
     * 
     * @param batch The batch to create. The batchID field will be overwritten
     *            with the ID of the newly created batch.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void createBatch(JobBatch batch)
        throws DatabaseException
    {
        logger.debug("createBatch");

        Connection con = getConnection();

        try {
            createBatch(con, batch);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void createBatch(Connection con, JobBatch batch)
        throws DatabaseException, SQLException;

    /**
     * Delete a batch.
     * 
     * @param batchID The ID of the batch to delete.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void deleteBatch(int batchID)
        throws DatabaseException
    {
        NDC.push(String.format("batchID = %d", batchID));

        try {
            logger.debug("deleteBatch");

            Connection con = getConnection();

            try {
                deleteBatch(con, batchID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void deleteBatch(Connection con, int batchID)
        throws DatabaseException, SQLException;

    /**
     * Update the fields of a batch.
     * 
     * @param batch The batch to update. All fields except the ID will be
     *            written to the database.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void updateBatch(JobBatch batch)
        throws DatabaseException
    {
        logger.debug("updateBatch");

        Connection con = getConnection();

        try {
            updateBatch(con, batch);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL Query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void updateBatch(Connection con, JobBatch batch)
        throws DatabaseException, SQLException;

    /**
     * Create a new user.
     * 
     * @param user The settings for the user to create. The userID field will be
     *            reset with the ID of the created user.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void createUser(User user)
        throws DatabaseException
    {
        logger.debug("createUser");

        Connection con = getConnection();

        try {
            createUser(con, user);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void createUser(Connection con, User user)
        throws DatabaseException, SQLException;

    /**
     * Update the given user with new settings.
     * 
     * @param user The user to update. All fields will be set, except for the
     *            user ID.
     * @throws DatabaseException If there is a problem communicating with the
     *             database.
     */
    public void updateUser(User user)
        throws DatabaseException
    {
        logger.debug("updateUser");

        Connection con = getConnection();

        try {
            updateUser(con, user);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void updateUser(Connection con, User user)
        throws DatabaseException, SQLException;

    /**
     * Delete the given setting from the database.
     * 
     * @param settingName The name of the setting to delete.
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     */
    public void deleteSetting(String settingName)
        throws DatabaseException
    {
        NDC.push(String.format("settingName = %s", settingName));

        try {
            logger.debug("deleteSetting");

            Connection con = getConnection();

            try {
                deleteSetting(con, settingName);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL Query: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void deleteSetting(Connection con, String settingName)
        throws DatabaseException, SQLException;

    /**
     * Create a new schedule entry in the database.
     * 
     * @param entry The entry to create. The {@link ScheduleEntry#scheduleID}
     *            field will be updated with the ID of the newly created entry.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    public void createScheduleEntry(ScheduleEntry entry)
        throws DatabaseException
    {

        logger.debug("createScheduleEntry");

        Connection con = getConnection();

        try {
            createScheduleEntry(con, entry);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void createScheduleEntry(Connection con,
                                                ScheduleEntry entry)
        throws DatabaseException, SQLException;

    /**
     * Update a schedule entry in the database.
     * 
     * @param entry The entry to update. All fields except the ID will be
     *            written.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    public void updateScheduleEntry(ScheduleEntry entry)
        throws DatabaseException
    {

        logger.debug("updateScheduleEntry");

        Connection con = getConnection();

        try {
            updateScheduleEntry(con, entry);
        }
        catch (SQLException sqle) {
            throw new DatabaseException("Error executing SQL query: " +
                                        sqle.getMessage(), sqle);
        }
        finally {
            returnConnection(con);
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void updateScheduleEntry(Connection con,
                                                ScheduleEntry entry)
        throws DatabaseException, SQLException;

    /**
     * Delete a schedule entry from the database.
     * 
     * @param scheduleID The ID of the entry to delete.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    public void deleteScheduleEntry(int scheduleID)
        throws DatabaseException
    {

        NDC.push(String.format("scheduleID = %d", scheduleID));

        try {
            logger.debug("deleteScheduleEntry");

            Connection con = getConnection();

            try {
                deleteScheduleEntry(con, scheduleID);
            }
            catch (SQLException sqle) {
                throw new DatabaseException("Error executing SQL exception: " +
                                            sqle.getMessage(), sqle);
            }
            finally {
                returnConnection(con);
            }
        }
        finally {
            NDC.pop();
        }
    }

    /**
     * Implemented by subclasses to actually access the database using the given
     * connection.
     */
    protected abstract void deleteScheduleEntry(Connection con, int scheduleID)
        throws DatabaseException, SQLException;

    /**
     * Validate that the given connection still works.
     * 
     * @param conn The connection to validate.
     * @return True if it is valid. False if not.
     */
    protected static boolean validate(Connection conn)
    {
        try {
            // I would like to use "isValid" here, but it always
            // seems to return false
            // This query is standard SQL and should work anywhere
            Statement s = conn.createStatement();
            try {
                ResultSet rs = s.executeQuery("SELECT NULL");
                try {
                    if (!rs.next()) {
                        return false;
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }
        }
        catch (SQLException sqle) {
            logger.warn("Connection died", sqle);
            return false;
        }
        return true;
    }
}

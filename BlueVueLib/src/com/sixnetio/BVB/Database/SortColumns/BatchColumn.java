/*
 * BatchColumn.java
 *
 * Exposes the columns of the Batches table that may be sorted on.
 *
 * Jonathan Pearson
 * March 13, 2009
 *
 */

package com.sixnetio.BVB.Database.SortColumns;

public enum BatchColumn {
	BatchID, UserID, // Simple ID numbers
	UserName, // May require a join
	BatchName,
	Deleted,
	JobCount, // Will require an interesting clause
	Reverse, // If encountered, the next entry will be sorted in descending
	// order
}

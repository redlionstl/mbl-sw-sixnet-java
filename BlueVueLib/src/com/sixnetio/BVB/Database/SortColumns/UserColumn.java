/*
 * UserColumn.java
 *
 * Exposes the columns of the Users table that may be sorted on.
 *
 * Jonathan Pearson
 * April 13, 2009
 *
 */

package com.sixnetio.BVB.Database.SortColumns;

public enum UserColumn {
	UserID, UserName, Type, Reverse,
}

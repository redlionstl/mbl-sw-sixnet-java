/*
 * ScheduleColumn.java
 *
 * Exposes the columns of the Scheduling table that may be sorted on.
 *
 * Jonathan Pearson
 * August 13, 2009
 *
 */

package com.sixnetio.BVB.Database.SortColumns;

public enum ScheduleColumn {
	ScheduleID, ModemID, // Simple ID numbers
	ModemAlias, // May require a join
	State,
	StartTime,
	EndTime,
	Relative,
	Reverse, // If encountered, the next entry will be sorted in descending
	// order
}

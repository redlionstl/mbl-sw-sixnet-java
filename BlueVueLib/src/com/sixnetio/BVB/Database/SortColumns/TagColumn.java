/*
 * TagColumn.java
 *
 * Exposes the columns of the Tags table that may be sorted on.
 *
 * Jonathan Pearson
 * March 13, 2009
 *
 */

package com.sixnetio.BVB.Database.SortColumns;

public enum TagColumn {
	TagID, Position, TagName, PollingInterval, // Properties
	ModemCount, // Will require an interesting query
	Reverse, // If encountered, the next entry will be sorted in descending
	// order
}

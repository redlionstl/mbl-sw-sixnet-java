/*
 * HistoryColumn.java
 *
 * Exposes the columns of the History table that may be sorted on.
 *
 * Jonathan Pearson
 * March 13, 2009
 *
 */

package com.sixnetio.BVB.Database.SortColumns;

public enum HistoryColumn {
	HistoryID, JobID, BatchID, // Simple ID numbers
	BatchName, // May require a join
	ModemID,
	ModemAlias,
	UserID,
	UserName, // May require a join
	Timestamp,
	Result,
	Type,
	Reverse, // If encountered, the next entry will be sorted in descending
	// order
}

/*
 * LogColumn.java
 *
 * Description
 *
 * Jonathan Pearson
 * Mar 13, 2009
 *
 */

package com.sixnetio.BVB.Database.SortColumns;

public enum LogColumn {
	LogID, Timestamp, UserID, // Standard columns
	UserName, // May require a join
	Type,
	Reverse, // If encountered, the next entry will be sorted in descending
	// order
}

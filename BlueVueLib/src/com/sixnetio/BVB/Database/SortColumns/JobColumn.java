/*
 * JobColumn.java
 *
 * Exposes the columns of the Jobs table that may be sorted on.
 *
 * Jonathan Pearson
 * March 13, 2009
 *
 */

package com.sixnetio.BVB.Database.SortColumns;

public enum JobColumn {
	JobID, BatchID, // Simple ID numbers
	BatchName, // May require a join
	ModemID,
	ModemAlias,
	UserID, // Simple ID number
	UserName, // May require a join
	Schedule,
	Tries,
	Type,
	Automatic,
	Status,
	Reverse, // If encountered, the next entry will be sorted in descending
	// order
}

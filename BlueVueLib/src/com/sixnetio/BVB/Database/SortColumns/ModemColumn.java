/*
 * ModemColumn.java
 *
 * Exposes the columns of the Modems table that may be sorted on.
 *
 * Jonathan Pearson
 * March 13, 2009
 *
 */

package com.sixnetio.BVB.Database.SortColumns;

public enum ModemColumn {
	ModemID, DeviceID, Alias, Model, FWVersion, CfgVersion, IPAddress, Port, PhoneNumber, // Properties
	Health, // Requires extensive joins
	Reverse, // If encountered, the next entry will be sorted in descending
	// order
}

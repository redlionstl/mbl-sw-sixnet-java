/*
 * DatabaseException.java
 *
 * A generic exception that may occur when dealing with a database.
 *
 * Jonathan Pearson
 * January 15, 2009
 *
 */

package com.sixnetio.BVB.Database;

public class DatabaseException extends Exception {
	public DatabaseException(String message) {
		super(message);
	}
	
	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
	}
}

/*
 * MicrosoftConnector.java
 *
 * Interfaces with a Microsoft SQL Server database
 *
 * Jonathan Pearson
 * January 15, 2009
 *
 */

package com.sixnetio.BVB.Database.DatabaseProviders;

import java.sql.*;
import java.util.*;
import java.util.Date;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.User.UserType;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.BVB.Database.SortColumns.*;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Version;

public class MicrosoftDatabase
    extends Database
{
    static {
        Database.registerProvider("Microsoft SQL Server",
            MicrosoftDatabase.class);
    }

    public MicrosoftDatabase(String provider)
        throws DriverException
    {
        super(provider);

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        catch (ClassNotFoundException e) {
            throw new DriverException(
                "Cannot load Microsoft SQL Server JDBC driver: " +
                    e.getMessage(), e);
        }
    }

    @Override
    public Connection subConnect(String server, String dbName, String userName,
                                 String password)
        throws SQLException
    {

        if (dbName == null) {
            return DriverManager.getConnection(String.format(
                "jdbc:sqlserver://%s", server), userName, password);
        }
        else {
            return DriverManager.getConnection(String.format(
                "jdbc:sqlserver://%s;databaseName=%s", server, dbName),
                userName, password);
        }
    }

    @Override
    protected void applyTag(Connection con, int modemID, int tagID)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void archiveJob(Connection con, long jobID, boolean success,
                              String message, Date timestamp)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void createDatabase(Connection con, String dbName,
                                  String primaryUser, String secondaryUser)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void createDatabaseUser(Connection con, String userName,
                                      String password)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void createJob(Connection con, Job job)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void createModem(Connection con, Modem modem)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void createPackage(Connection con, BlueTreeImage image)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void createTag(Connection con, ModemTag tag)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void deleteJob(Connection con, long jobID)
        throws DatabaseException, SQLException, ObjectInUseException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void deleteModem(Connection con, int modemID)
        throws DatabaseException, SQLException, ObjectInUseException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void deletePackage(Connection con, String hash)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void deleteTag(Connection con, int tagID)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected Collection<Capabilities> getCurrentCapabilities(Connection con)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Map<Long, Job> getJobs(Connection con, Collection<Long> jobIDs,
                                     boolean generic)
        throws DatabaseException, SQLException
    {

        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Collection<Long> getMatchingLogs(Connection con, Long logID,
                                               Date lowDate, Date highDate,
                                               Integer userID, String logType,
                                               int start, int limit,
                                               List<LogColumn> sortOn,
                                               long[] matchCount)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Collection<Integer> getMatchingModems(Connection con,
                                                    Integer modemID,
                                                    DeviceID deviceID,
                                                    String alias, String model,
                                                    Version fwVersion,
                                                    String cfgVersion,
                                                    String ipAddress,
                                                    Integer port, String phone,
                                                    Integer tagID, int start,
                                                    int limit,
                                                    List<ModemColumn> sortOn,
                                                    long[] matchCount)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Collection<String> getMatchingPackages(Connection con,
                                                     String type,
                                                     Version minVersion,
                                                     Version maxVersion,
                                                     Date minDate,
                                                     Date maxDate,
                                                     String checksum,
                                                     int start, int limit,
                                                     long[] matchCount)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected List<Integer> getMatchingTags(Connection con, Integer tagID,
                                            String tagName,
                                            Integer lowPollingInterval,
                                            Integer highPollingInterval,
                                            Integer minPosition,
                                            Integer maxPosition,
                                            Integer modemID, int start,
                                            int limit, List<TagColumn> sortOn,
                                            long[] matchCount)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Map<Integer, Modem> getModems(Connection con,
                                            Collection<Integer> modemIDs)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Map<String, String> getModemStatus(Connection con, int modemID,
                                                 boolean mostRecent,
                                                 Date minDate, Date maxDate)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Map<String, BlueTreeImage> getPackages(Connection con,
                                                     Collection<String> hashes,
                                                     boolean metadataOnly)
        throws DatabaseException, SQLException
    {

        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Map<Integer, Job> getReadyToRunJobs(Connection con)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected HashMap<String, String> getSettings(
                                                  Connection con,
                                                  Collection<String> settingNames)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Map<Integer, ModemTag> getTags(Connection con,
                                             Collection<Integer> tagIDs)
        throws DatabaseException, SQLException
    {

        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Collection<TagMatch> getTagMatch(Connection con, int tagID,
                                               int modemID)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void initializeNewDatabase(Connection con, String dbName,
                                         String primaryUser,
                                         String secondaryUser,
                                         LicenseKey licenseKey)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected LockResult lockJob(Connection con, long jobID, long ms)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected LockResult lockModem(Connection con, int modemID, long ms)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void log(Connection con, Collection<LogEntry> logEntries)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void purgeCapabilities(Connection con, Date cutoff)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void purgeHistory(Connection con, Date cutoff)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void purgeLog(Connection con, Date cutoff)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void removeLocksBy(Connection con, String name)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void removeTag(Connection con, int modemID, int tagID)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void saveStatusValues(Connection con, int modemID,
                                    Date timestamp, Map<String, String> props)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void unlockJob(Connection con, long jobID)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void unlockModem(Connection con, int modemID)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void updateCapabilities(Connection con, Capabilities caps)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void updateJob(Connection con, Job job)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void updateModem(Connection con, Modem modem)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void updateSetting(Connection con, String settingName,
                                 String settingValue)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void updateTag(Connection con, ModemTag tag)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void createBatch(Connection con, JobBatch batch)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void deleteBatch(Connection con, int batchID)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected Map<Integer, JobBatch> getBatches(Connection con,
                                                Collection<Integer> batchIDs)
        throws DatabaseException, SQLException
    {

        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Collection<Integer> getMatchingBatches(Connection con,
                                                     Integer batchID,
                                                     Integer userID,
                                                     String name,
                                                     Boolean deleted,
                                                     int start, int limit,
                                                     List<BatchColumn> sortOn,
                                                     long[] matchCount)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Collection<Long> getMatchingHistory(Connection con,
                                                  Long historyID, Long jobID,
                                                  Integer batchID,
                                                  Integer modemID,
                                                  Integer userID,
                                                  String jobType,
                                                  String result, Date lowDate,
                                                  Date highDate, int start,
                                                  int limit,
                                                  List<HistoryColumn> sortOn,
                                                  long[] matchCount)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Collection<Long> getMatchingJobs(Connection con, Long jobID,
                                               Integer batchID,
                                               Integer modemID, Integer userID,
                                               String type, Date lowDate,
                                               Date highDate,
                                               Boolean automatic, int start,
                                               int limit,
                                               List<JobColumn> sortOn,
                                               long[] matchCount)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void updateBatch(Connection con, JobBatch batch)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void purgeStaleLocks(Connection con, Date cutoff)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected Map<Long, HistoryEntry> getHistoryEntries(
                                                        Connection con,
                                                        Collection<Long> historyIDs)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Map<Long, LogEntry> getLogEntries(Connection con,
                                                Collection<Long> logIDs)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void createUser(Connection con, User user)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected Map<Integer, User> getUsers(Connection con,
                                          Collection<Integer> userIDs)
        throws DatabaseException, SQLException
    {

        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void updateUser(Connection con, User user)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected Collection<Integer> getMatchingUsers(Connection con,
                                                   Integer userID, String name,
                                                   UserType type, int start,
                                                   int limit,
                                                   List<UserColumn> sortOn,
                                                   long[] matchCount)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void deleteSetting(Connection con, String settingName)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void createScheduleEntry(Connection con, ScheduleEntry entry)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected void deleteScheduleEntry(Connection con, int scheduleID)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    @Override
    protected Collection<Integer> getMatchingSchedules(
                                                       Connection con,
                                                       Integer scheduleID,
                                                       Integer modemID,
                                                       Boolean relative,
                                                       int start,
                                                       int limit,
                                                       List<ScheduleColumn> sortOn,
                                                       long[] matchCount)
        throws DatabaseException, SQLException
    {

        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Map<Integer, ScheduleEntry> getScheduleEntries(
                                                             Connection con,
                                                             Collection<Integer> scheduleIDs)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void updateScheduleEntry(Connection con, ScheduleEntry entry)
        throws DatabaseException, SQLException
    {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.sixnetio.BVB.Database.Database#upgradeDatabase(java.sql.Connection)
     */
    @Override
    protected void upgradeDatabase(Connection con)
        throws SQLException
    {
        // TODO Auto-generated method stub

    }
}

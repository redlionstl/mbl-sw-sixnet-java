/*
 * PostgreSQLConnector.java
 *
 * Interfaces with a PostgreSQL database.
 *
 * Jonathan Pearson
 * February 3, 2009
 *
 */

package com.sixnetio.BVB.Database.DatabaseProviders;

import java.io.IOException;
import java.sql.*;
import java.sql.Date;
import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.BlueTreeImage.ImageType;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Common.User.UserType;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.BVB.Database.SortColumns.*;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Jobs.Job.JobData;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class PostgreSQLDatabase
    extends Database
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    private static Calendar utcCalendar =
        Calendar.getInstance(TimeZone.getTimeZone("UTC"));

    static {
        Database.registerProvider("PostgreSQL", PostgreSQLDatabase.class);
    }

    public PostgreSQLDatabase(String provider)
        throws DriverException
    {
        super(provider);

        if (logger.isDebugEnabled()) {
            ClassLoader currentLoader =
                PostgreSQLDatabase.class.getClassLoader();
            StringBuilder builder = new StringBuilder();
            while (currentLoader != null) {
                builder.append(currentLoader.getClass().getName()).append(
                    " => ");

                currentLoader = currentLoader.getParent();
            }

            logger.debug("PostgreSQLDatabase classloader hierarchy: " +
                         builder.toString());
        }

        try {
            Class.forName("org.postgresql.Driver", true,
                PostgreSQLDatabase.class.getClassLoader());
        }
        catch (ClassNotFoundException e) {
            throw new DriverException("Cannot load PostgreSQL JDBC driver: " +
                                      e.getMessage(), e);
        }
    }

    @Override
    public Connection subConnect(String server, String dbName, String userName,
                                 String password)
        throws SQLException
    {

        logger.debug(String.format(
            "Trying to create a connection to database '%s' on server '%s'",
            (dbName == null ? "postgres" : dbName), server));

        return DriverManager.getConnection(String.format(
            "jdbc:postgresql://%s/%s", server, (dbName == null ? "postgres"
                                                              : dbName)),
            userName, password);
    }

    @Override
    protected Map<Long, Job> getJobs(Connection con, Collection<Long> jobIDs,
                                     boolean generic)
        throws SQLException
    {

        StringBuilder whereClause = new StringBuilder("WHERE FALSE");

        for (long id : jobIDs) {
            whereClause.append(" OR \"JobID\"=?");
        }

        PreparedStatement s =
            con.prepareStatement("SELECT \"JobID\", \"ModemID\", \"UserID\", " +
                                 "\"BatchID\", \"Schedule\", \"Tries\", " +
                                 "\"Type\", \"Parameters\", \"Automatic\", " +
                                 "\"LockedBy\", \"LockedAt\" FROM \"Jobs\" " +
                                 whereClause.toString(),
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

        try {
            int index = 1;
            for (long id : jobIDs) {
                s.setLong(index++, id);
            }

            ResultSet rs = s.executeQuery();

            try {
                Map<Long, Job> jobs = new HashMap<Long, Job>();

                while (rs.next()) {
                    // Found a job, pull out the data into a JobData object and
                    // pass it off to Job for construction

                    long jobID = rs.getLong(1);
                    int modemID = rs.getInt(2);

                    // Detect NULL User ID
                    Integer userID = rs.getInt(3);
                    if (rs.wasNull()) {
                        userID = null;
                    }

                    // Detect NULL Batch ID
                    Integer batchID = rs.getInt(4);
                    if (rs.wasNull()) {
                        batchID = null;
                    }

                    Timestamp schedule = rs.getTimestamp(5, utcCalendar);
                    int tries = rs.getInt(6);
                    String type = rs.getString(7);

                    // Parse the XML parameters
                    Tag params;
                    try {
                        params = Utils.parseXML(rs.getString(8), true);
                    }
                    catch (IOException ioe) {
                        logger
                            .error(String
                                .format(
                                    "Unable to parse job %d's parameters as XML, blanking",
                                    jobID));

                        params = new Tag("job");
                    }

                    boolean automatic = rs.getBoolean(9);

                    // Detect NULL LockedBy
                    String lockedBy = rs.getString(10);
                    if (rs.wasNull()) {
                        lockedBy = null;
                    }

                    // Detect NULL LockedAt
                    Timestamp lockedAt = rs.getTimestamp(11, utcCalendar);
                    if (rs.wasNull()) {
                        lockedAt = null;
                    }

                    Job.JobData data =
                        new Job.JobData(jobID, batchID, modemID, userID,
                            schedule, tries, type, params, automatic, lockedBy,
                            lockedAt);

                    Job job;
                    if (generic) {
                        job = Job.makeGenericJob(data);
                    }
                    else {
                        try {
                            job = Job.makeJob(data, this);
                        }
                        catch (BadJobException bje) {
                            logger.error(String.format(
                                "Unable to construct job, skipping", bje));

                            continue;
                        }
                    }

                    jobs.put(jobID, job);
                }

                return jobs;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Map<Integer, Modem> getModems(Connection con,
                                            Collection<Integer> modemIDs)
        throws SQLException
    {

        StringBuilder whereClause = new StringBuilder("WHERE FALSE");
        for (int id : modemIDs) {
            whereClause.append(" OR \"ModemID\"=?");
        }

        PreparedStatement s =
            con
                .prepareStatement(
                    "SELECT \"ModemID\", \"DeviceID\", \"Alias\", "
                        + "\"Model\", \"FWVersion\", \"CfgVersion\", "
                        + "\"IPAddress\", \"Port\", \"PhoneNumber\", "
                        + "\"Status\", "
                        +

                        "(SELECT COUNT(*) FROM \"Jobs\" "
                        + "  WHERE \"Jobs\".\"ModemID\"=\"Modems\".\"ModemID\"), "
                        +

                        "(SELECT COUNT(*) FROM \"TagMatch\" "
                        + "  WHERE \"TagMatch\".\"ModemID\"=\"Modems\".\"ModemID\"), "
                        +

                        "(SELECT CASE "
                        +
                        // If no history or status update within the failure
                        // threshold, stale
                        "  WHEN (SELECT COUNT(*)"
                        + // Count
                        "        FROM \"History\""
                        + "        WHERE \"History\".\"ModemID\"=\"Modems\".\"ModemID\""
                        + "          AND \"History\".\"Timestamp\" >= ?"
                        + "       ) = 0 AND"
                        + "     (SELECT COUNT(*)"
                        + "      FROM \"StatusHistory\""
                        + "        WHERE \"StatusHistory\".\"ModemID\"=\"Modems\".\"ModemID\""
                        + "          AND \"StatusHistory\".\"Timestamp\" >= ?"
                        + "      ) = 0 THEN '" +
                        Modem.ModemHealth.Stale.statusString +
                        "'" +
                        // Look for successes or status updates within the
                        // failure threshold
                        "  WHEN (SELECT COUNT(*)" +
                        // Count
                        "        FROM \"History\"" +
                        "        WHERE \"History\".\"ModemID\"=\"Modems\".\"ModemID\"" +
                        "          AND \"History\".\"Timestamp\" >= ?" +
                        "          AND \"History\".\"Result\" = 'Success'" +
                        "       ) = 0 AND" +
                        "     (SELECT COUNT(*)" +
                        "      FROM \"StatusHistory\"" +
                        "        WHERE \"StatusHistory\".\"ModemID\"=\"Modems\".\"ModemID\"" +
                        "          AND \"StatusHistory\".\"Timestamp\" >= ?" +
                        "      ) = 0 THEN '" +
                        Modem.ModemHealth.Failure.statusString +
                        "'" +
                        // Look for success or status update within the warning
                        // threshold
                        "  WHEN (SELECT COUNT(*)" +
                        // Count
                        "        FROM \"History\"" +
                        "        WHERE \"History\".\"ModemID\"=\"Modems\".\"ModemID\"" +
                        "          AND \"History\".\"Timestamp\" >= ?" +
                        "          AND \"History\".\"Result\" = 'Success'" +
                        "       ) = 0 AND" +
                        "       (SELECT COUNT(*)" +
                        "        FROM \"StatusHistory\"" +
                        "        WHERE \"StatusHistory\".\"ModemID\"=\"Modems\".\"ModemID\"" +
                        "          AND \"StatusHistory\".\"Timestamp\" >= ?" +
                        "       ) = 0 THEN '" +
                        Modem.ModemHealth.Warning.statusString +
                        "'" +
                        // Found recent success or status update, more recent
                        // that the warning threshold? Normal
                        "  ELSE '" +
                        Modem.ModemHealth.Normal.statusString +
                        "' " +
                        // End of the test
                        "END) FROM \"Modems\" " + whereClause.toString(),
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.SECOND, -Server.getServerSettings().getIntValue(
                Setting.FailureTimeout));

            int index = 1;

            // Check for any history or status updates
            s.setTimestamp(index++, new Timestamp(cal.getTime().getTime()),
                utcCalendar);
            s.setTimestamp(index++, new Timestamp(cal.getTime().getTime()),
                utcCalendar);

            // Check for success or status updates in failure timeout
            s.setTimestamp(index++, new Timestamp(cal.getTime().getTime()),
                utcCalendar);
            s.setTimestamp(index++, new Timestamp(cal.getTime().getTime()),
                utcCalendar);

            // Check for success or status update in warning timeout
            cal = Calendar.getInstance();
            cal.add(Calendar.SECOND, -Server.getServerSettings().getIntValue(
                Setting.WarnTimeout));
            s.setTimestamp(index++, new Timestamp(cal.getTime().getTime()),
                utcCalendar);
            s.setTimestamp(index++, new Timestamp(cal.getTime().getTime()),
                utcCalendar);

            // Add the modem IDs
            for (int id : modemIDs) {
                s.setInt(index++, id);
            }

            ResultSet rs = s.executeQuery();

            try {
                Map<Integer, Modem> modems = new HashMap<Integer, Modem>();

                while (rs.next()) {
                    // Found a modem, pull out the data into a Modem object
                    int modemID = rs.getInt(1);

                    DeviceID deviceID;
                    long devID = rs.getLong(2);
                    if (rs.wasNull()) {
                        deviceID = null;
                    }
                    else {
                        deviceID = new DeviceID(devID);
                    }

                    String name = rs.getString(3);
                    if (rs.wasNull()) {
                        name = null;
                    }

                    String model = rs.getString(4);
                    if (rs.wasNull()) {
                        model = null;
                    }

                    String sVersion = rs.getString(5);
                    Version fwVersion;
                    if (rs.wasNull()) {
                        fwVersion = null;
                    }
                    else {
                        fwVersion = new Version(sVersion);
                    }

                    String cfgVersion = rs.getString(6);
                    if (rs.wasNull()) {
                        cfgVersion = null;
                    }

                    String ipAddress = rs.getString(7);
                    if (rs.wasNull()) {
                        ipAddress = null;
                    }

                    int port = rs.getInt(8);

                    String phone = rs.getString(9);
                    if (rs.wasNull()) {
                        phone = null;
                    }

                    Tag status;
                    try {
                        status = Utils.parseXML(rs.getString(10), true);
                    }
                    catch (IOException ioe) {
                        logger
                            .error(String
                                .format(
                                    "Unable to parse modem %d's status as XML, blanking",
                                    modemID));

                        status = new Tag("modem");
                    }

                    int jobCount = rs.getInt(11);
                    int tagCount = rs.getInt(12);

                    Modem.ModemHealth modemHealth =
                        Modem.ModemHealth.find(rs.getString(13));

                    Modem modem =
                        new Modem(modemID, deviceID, name, model, fwVersion,
                            cfgVersion, ipAddress, port, phone, status,
                            jobCount, tagCount, modemHealth);

                    modems.put(modemID, modem);
                }

                return modems;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Map<Integer, Job> getReadyToRunJobs(Connection con)
        throws SQLException, DatabaseException
    {
        Timestamp now = new Timestamp(System.currentTimeMillis());

        // Build a filter so we only pull jobs of types that we support
        StringBuilder jobTypeFilter = new StringBuilder("(FALSE");
        for (String jobType : Capabilities.getLocalJobCapabilities()) {
            jobTypeFilter.append(" OR \"Jobs\".\"Type\"='").append(jobType)
                .append("'");
        }
        jobTypeFilter.append(")");

        PreparedStatement s =
            con
                .prepareStatement(
                    "SELECT \"Jobs\".\"JobID\", \"Jobs\".\"ModemID\", \"Jobs\".\"UserID\", " +
                        "\"Jobs\".\"Schedule\", \"Jobs\".\"Tries\", \"Jobs\".\"Type\", " +
                        "\"Jobs\".\"Parameters\", \"Jobs\".\"Automatic\", " +
                        "\"Jobs\".\"BatchID\", \"Jobs\".\"LockedBy\", " +
                        "\"Jobs\".\"LockedAt\" " +
                        "FROM \"Jobs\" INNER JOIN \"Modems\" " +
                        "ON \"Jobs\".\"ModemID\"=\"Modems\".\"ModemID\" " +
                        "WHERE \"Jobs\".\"Schedule\"<? " +
                        "AND \"Jobs\".\"LockedBy\" IS NULL " +
                        "AND \"Modems\".\"LockedBy\" IS NULL " +
                        "AND (\"Modems\".\"IPAddress\" IS NOT NULL " +
                        "     OR \"Jobs\".\"Type\"='!DelayedUpdate') " +
                        "AND position(lower('true</mobileOriginated>') " +
                        "  IN lower(\"Jobs\".\"Parameters\")) <= 0" +
                        "AND " +
                        jobTypeFilter.toString() +
                        " " +
                        "ORDER BY \"Jobs\".\"Schedule\" ASC LIMIT " +
                        Server.getServerSettings().getIntValue(
                            Setting.JobPollingSize),
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

        try {
            s.setTimestamp(1, now, utcCalendar);

            // Modem ID --> Job (to detect duplicate jobs against the same
            // modem)
            Map<Integer, Job> jobs = new HashMap<Integer, Job>();

            // Jobs that cannot be loaded get deleted
            List<Long> badJobs = new Vector<Long>();

            ResultSet rs = s.executeQuery();

            try {
                while (rs.next()) {
                    // Found a job, pull out the data into a JobData object and
                    // pass it off to Job for construction

                    // Detect NULL User ID
                    Integer userID;
                    userID = rs.getInt(3);
                    if (rs.wasNull()) {
                        userID = null;
                    }

                    // Parse the XML parameters
                    Tag params;
                    try {
                        params = Utils.parseXML(rs.getString(7), true);
                    }
                    catch (IOException ioe) {
                        logger.error(String.format(
                            "Unable to parse job %d's parameters as XML, "
                                + "deleting the job", rs.getLong(1)));

                        badJobs.add(rs.getLong(1));
                        continue;
                    }

                    int modemID = rs.getInt(2);
                    if (jobs.containsKey(modemID)) {
                        // Skip this one, we've already got a job for this modem
                        continue;
                    }

                    // Detect NULL Batch ID
                    Integer batchID = rs.getInt(9);
                    if (rs.wasNull()) {
                        batchID = null;
                    }

                    // Detect NULL LockedBy
                    String lockedBy = rs.getString(10);
                    if (rs.wasNull()) {
                        lockedBy = null;
                    }

                    // Detect NULL LockedAt
                    Timestamp lockedAt = rs.getTimestamp(11, utcCalendar);
                    if (rs.wasNull()) {
                        lockedAt = null;
                    }

                    Job.JobData data =
                        new Job.JobData(rs.getLong(1), batchID, modemID,
                            userID, rs.getTimestamp(4, utcCalendar), rs
                                .getInt(5), rs.getString(6), params, rs
                                .getBoolean(8), lockedBy, lockedAt);

                    try {
                        jobs.put(modemID, Job.makeJob(data, this));
                    }
                    catch (BadJobException bje) {
                        logger.error(String.format(
                            "Unable to construct job %d, deleting the job",
                            data.jobID));

                        // Delete the job (later)
                        badJobs.add(data.jobID);
                    }
                }

                // Do this BEFORE locking stuff, so we don't need to worry about
                // unlocking it if this fails
                List<LogEntry> badJobMessages =
                    new Vector<LogEntry>(badJobs.size());
                for (long badJobID : badJobs) {
                    try {
                        deleteJob(con, badJobID);

                        badJobMessages
                            .add(new LogEntry(new Timestamp(System
                                .currentTimeMillis()), null, "Error", String
                                .format("Unable to load job %d, deleted",
                                    badJobID)));
                    }
                    catch (ObjectInUseException oiue) {
                        badJobMessages
                            .add(new LogEntry(
                                new Timestamp(System.currentTimeMillis()),
                                null,
                                "Error",
                                String
                                    .format(
                                        "Unable to load job %d, unable to delete as it is busy",
                                        badJobID)));
                    }
                }

                log(con, badJobMessages);

                // Go through each job we have grabbed and lock it and its
                // associated modem
                Map<Integer, Job> lockedJobs = new HashMap<Integer, Job>();
                boolean success = false;

                try {
                    for (int modemID : jobs.keySet()) {
                        // Only try once
                        // We need to acquire the lock, not just have it on this
                        // machine
                        if (lockModem(con, modemID, 0) == LockResult.Acquired) {
                            // Try to lock the job
                            if (lockJob(con,
                                jobs.get(modemID).getJobData().jobID, 0) == LockResult.Acquired) {
                                // Locked the job
                                lockedJobs.put(modemID, jobs.get(modemID));
                            }
                            else {
                                // Unable to lock the job, release the modem
                                unlockModem(con, modemID);
                            }
                        }
                        else {
                            // Unable to lock the modem, perhaps another job is
                            // active against it, or another job server got
                            // there first
                        }
                    }

                    success = true;
                }
                finally {
                    if (!success) {
                        logger
                            .error("Failed to acquire all job/modem locks, unlocking those acquired");

                        List<LogEntry> failureMessages =
                            new LinkedList<LogEntry>();

                        // Something failed, unlock everything we managed to
                        // acquire before dropping out
                        for (int modemID : lockedJobs.keySet()) {
                            try {
                                unlockModem(con, modemID);
                            }
                            catch (SQLException sqle) {
                                logger
                                    .error(String.format(
                                        "Unable to unlock modem %d", modemID),
                                        sqle);

                                failureMessages.add(new LogEntry(new Timestamp(
                                    Server.getCurrentTime().getTime()), null,
                                    "Error", String.format(
                                        "Unable to unlock modem %d: %s",
                                        modemID, sqle.getMessage())));
                            }

                            try {
                                unlockJob(con, lockedJobs.get(modemID)
                                    .getJobData().jobID);
                            }
                            catch (SQLException sqle) {
                                logger.error(String.format(
                                    "Unable to unlock job %d", lockedJobs.get(
                                        modemID).getJobData().jobID), sqle);

                                failureMessages.add(new LogEntry(new Timestamp(
                                    Server.getCurrentTime().getTime()), null,
                                    "Error", String.format(
                                        "Unable to unlock job %d: %s", modemID,
                                        sqle.getMessage())));
                            }
                        }

                        try {
                            log(con, failureMessages);
                        }
                        catch (SQLException sqle) {
                            logger
                                .error("Unable to log failure messages", sqle);
                        }
                    }
                }

                logger.info(String
                    .format("Acquired %d jobs", lockedJobs.size()));

                return lockedJobs;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected LockResult lockModem(Connection con, int modemID, long ms)
        throws SQLException, DatabaseException
    {
        // Try every 50ms until the timeout is reached

        long startTime = System.currentTimeMillis();

        while (ms == 0 || startTime + ms > System.currentTimeMillis()) {
            PreparedStatement s =
                con
                    .prepareStatement("UPDATE \"Modems\" SET \"LockedBy\"=?, \"LockedAt\"=? "
                                      + "WHERE \"ModemID\"=? AND \"LockedBy\" IS NULL");

            try {
                s.setString(1, Server.getName());
                s.setTimestamp(2, new Timestamp(Server.getCurrentTime()
                    .getTime()), utcCalendar);
                s.setInt(3, modemID);

                int changes = s.executeUpdate();

                // If we made a change, then we must have acquired the lock
                if (changes == 1) {
                    return LockResult.Acquired;
                }
                else if (changes > 1) {
                    Server
                        .panic("Multiple database rows match the same modem: " +
                               modemID);
                }
            }
            finally {
                s.close();
            }

            // Check if the lock is already held by this system
            s =
                con
                    .prepareStatement("SELECT COUNT(*) FROM \"Modems\" WHERE \"ModemID\"=? AND \"LockedBy\"=?");

            try {
                s.setInt(1, modemID);
                s.setString(2, Server.getName());

                ResultSet rs = s.executeQuery();

                try {
                    // There will always be one result
                    rs.next();

                    int count = rs.getInt(1);

                    if (count == 1) {
                        return LockResult.NoChange;
                    }
                    else if (count > 1) {
                        throw new DatabaseException(
                            "Database contains multiple modems with modem ID " +
                                modemID);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }

            // Only allowed one try?
            if (ms == 0) {
                return LockResult.Failed;
            }

            // Wait a bit and try again
            // This would probably wait until we've run out of time, and then
            // not allow another try, which is why we reserve an extra 2ms
            Utils.sleep(Math.min(50, startTime + ms -
                                     System.currentTimeMillis() - 2));
        }

        return LockResult.Failed;
    }

    @Override
    protected LockResult lockJob(Connection con, long jobID, long ms)
        throws SQLException, DatabaseException
    {
        // Try every 50ms until the timeout is reached

        long startTime = System.currentTimeMillis();

        while (ms == 0 || startTime + ms > System.currentTimeMillis()) {
            PreparedStatement s =
                con
                    .prepareStatement("UPDATE \"Jobs\" SET \"LockedBy\"=?, \"LockedAt\"=? "
                                      + "WHERE \"JobID\"=? AND \"LockedBy\" IS NULL");

            try {
                s.setString(1, Server.getName());
                s.setTimestamp(2, new Timestamp(Server.getCurrentTime()
                    .getTime()), utcCalendar);
                s.setLong(3, jobID);

                int changes = s.executeUpdate();

                // If we made a change, then we must have acquired the lock
                if (changes == 1) {
                    return LockResult.Acquired;
                }
                else if (changes > 1) {
                    Server.panic("Multiple database rows match the same job: " +
                                 jobID);
                }
            }
            finally {
                s.close();
            }

            // Check if the lock is already held by this system
            s =
                con
                    .prepareStatement("SELECT COUNT(*) FROM \"Jobs\" WHERE \"JobID\"=? AND \"LockedBy\"=?");

            try {
                s.setLong(1, jobID);
                s.setString(2, Server.getName());

                ResultSet rs = s.executeQuery();

                try {
                    // There will always be one result
                    rs.next();

                    int count = rs.getInt(1);

                    if (count == 1) {
                        return LockResult.NoChange;
                    }
                    else if (count > 1) {
                        throw new DatabaseException(
                            "Database contains multiple jobs with jobID " +
                                jobID);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }

            // Only allowed one try?
            if (ms == 0) {
                return LockResult.Failed;
            }

            // Wait a bit and try again
            // This would probably wait until we've run out of time, and then
            // not allow another try, which is why we reserve an extra 2ms
            Utils.sleep(Math.min(50, startTime + ms -
                                     System.currentTimeMillis() - 2));
        }

        return LockResult.Failed;
    }

    @Override
    protected void unlockJob(Connection con, long jobID)
        throws SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Jobs\" SET \"LockedBy\"=NULL, \"LockedAt\"=NULL WHERE \"JobID\"=?");

        try {
            s.setLong(1, jobID);

            s.executeUpdate();

            logger.debug(String.format("Unlocked job %d", jobID));
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void unlockModem(Connection con, int modemID)
        throws SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Modems\" SET \"LockedBy\"=NULL, \"LockedAt\"=NULL WHERE \"ModemID\"=?");

        try {
            s.setInt(1, modemID);

            s.executeUpdate();

            logger.debug(String.format("Unlocked modem %d", modemID));
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void archiveJob(Connection con, long jobID, boolean success,
                              String message, java.util.Date timestamp)
        throws SQLException
    {

        // Job table has: JobID, BatchID, ModemID, UserID, Schedule, Tries,
        // LockedBy, Type, Parameters, Automatic

        // History table: JobID, BatchID, ModemID, UserID, Timestamp, Result,
        // Type, Message

        // Jobs.JobID --> History.JobID
        // Jobs.BatchID --> History.BatchID
        // Jobs.ModemID --> History.ModemID
        // Jobs.UserID --> History.UserID
        // timestamp --> History.Timestamp
        // success --> History.Result
        // Jobs.Type --> History.Type
        // message --> History.Message

        PreparedStatement s =
            con
                .prepareStatement("INSERT INTO \"History\" "
                                  + "(\"JobID\", \"BatchID\", \"ModemID\", \"UserID\", \"Timestamp\", \"Result\", \"Type\", \"Message\") "
                                  + "SELECT \"JobID\", \"BatchID\", \"ModemID\", \"UserID\", ?, ?, \"Type\", ? "
                                  + "FROM \"Jobs\" WHERE \"JobID\"=?");

        try {
            s.setTimestamp(1, new Timestamp(timestamp.getTime()), utcCalendar);

            s.setString(2, success ? "Success" : "Failure");
            s.setString(3, message);
            s.setLong(4, jobID);

            s.executeUpdate();

            logger.debug(String.format("Logged job %d into history", jobID));
        }
        finally {
            s.close();
        }
    }

    @Override
    protected HashMap<String, String> getSettings(
                                                  Connection con,
                                                  Collection<String> settingNames)
        throws SQLException
    {

        StringBuilder stmt = new StringBuilder();

        stmt
            .append("SELECT \"SettingName\", \"SettingValue\" FROM \"Settings\"");

        if (settingNames != null) {
            stmt.append(" WHERE FALSE");
            for (String name : settingNames) {
                stmt.append(" OR \"SettingName\"=?");
            }
        }

        PreparedStatement s =
            con.prepareStatement(stmt.toString(), ResultSet.TYPE_FORWARD_ONLY,
                ResultSet.CONCUR_READ_ONLY);

        try {
            if (settingNames != null) {
                // Ordering of each setting doesn't matter (could come out in a
                // different
                // order, depending on the Collection implementation)
                int counter = 1;
                for (String name : settingNames) {
                    s.setString(counter++, name);
                }
            }

            HashMap<String, String> settings = new HashMap<String, String>();

            ResultSet rs = s.executeQuery();

            try {
                while (rs.next()) {
                    String settingName = rs.getString(1);
                    String settingValue = rs.getString(2);
                    if (rs.wasNull()) {
                        settingValue = null;
                    }

                    settings.put(settingName, settingValue);
                }

                return settings;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void saveStatusValues(Connection con, int modemID,
                                    java.util.Date timestamp,
                                    Map<String, String> props)
        throws SQLException
    {

        PreparedStatement s =
            con
                .prepareStatement("INSERT INTO \"StatusHistory\" "
                                  + "(\"ModemID\", \"Timestamp\", \"PropertyName\", \"PropertyVal\") "
                                  + "VALUES (?, ?, ?, ?)");

        try {
            s.setInt(1, modemID);

            s.setTimestamp(2, new Timestamp(timestamp.getTime()), utcCalendar);

            for (Map.Entry<String, String> entry : props.entrySet()) {
                s.setString(3, entry.getKey());
                s.setString(4, entry.getValue());

                s.executeUpdate();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void updateJob(Connection con, Job job)
        throws DatabaseException, SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Jobs\" SET \"ModemID\"=?, \"UserID\"=?, \"Schedule\"=?, "
                                  + "\"Tries\"=?, \"Type\"=?, \"Parameters\"=?, \"Automatic\"=?, \"BatchID\"=? WHERE \"JobID\"=?");

        try {
            JobData jobData = job.getJobData();

            s.setInt(1, jobData.modemID);

            if (jobData.userID == null) {
                s.setNull(2, Types.INTEGER);
            }
            else {
                s.setInt(2, jobData.userID);
            }

            s.setTimestamp(3, new Timestamp(jobData.schedule.getTime()),
                utcCalendar);

            s.setInt(4, jobData.tries);
            s.setString(5, jobData.type);
            s.setString(6, jobData.parameters.toString(false));
            s.setBoolean(7, jobData.automatic);

            if (jobData.batchID == null) {
                s.setNull(8, Types.INTEGER);
            }
            else {
                s.setInt(8, jobData.batchID);
            }

            s.setLong(9, jobData.jobID);

            s.executeUpdate();

            logger.debug(String
                .format("Updated job %d", job.getJobData().jobID));
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void updateModem(Connection con, Modem modem)
        throws DatabaseException, SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Modems\" SET \"DeviceID\"=?, \"Alias\"=?, \"Model\"=?, "
                                  + "\"FWVersion\"=?, \"CfgVersion\"=?, \"IPAddress\"=?, \"Port\"=?, "
                                  + "\"PhoneNumber\"=?, \"Status\"=? WHERE \"ModemID\"=?");

        try {
            if (modem.deviceID == null) {
                s.setNull(1, Types.BIGINT);
            }
            else {
                s.setLong(1, modem.deviceID.asSharedLong());
            }

            s.setString(2, modem.alias);

            if (modem.model == null) {
                s.setNull(3, Types.VARCHAR);
            }
            else {
                s.setString(3, modem.model);
            }

            if (modem.fwVersion == null) {
                s.setNull(4, Types.VARCHAR);
            }
            else {
                s.setString(4, modem.fwVersion.toString());
            }

            if (modem.cfgVersion == null) {
                s.setNull(5, Types.VARCHAR);
            }
            else {
                s.setString(5, modem.cfgVersion);
            }

            if (modem.ipAddress == null) {
                s.setNull(6, Types.VARCHAR);
            }
            else {
                s.setString(6, modem.ipAddress);
            }

            s.setInt(7, modem.port);

            if (modem.phoneNumber == null) {
                s.setNull(8, Types.VARCHAR);
            }
            else {
                s.setString(8, modem.phoneNumber);
            }

            s.setString(9, modem.status.toString(false));

            s.setInt(10, modem.modemID);

            s.executeUpdate();

            logger.debug(String.format("Updated modem %d", modem.modemID));
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void createModem(Connection con, Modem modem)
        throws SQLException
    {
        // Unfortunately, PostgreSQL does not support RETURN_GENERATED_KEYS
        // However, it holds onto these values per-connection, so no need to
        // enter transaction mode

        PreparedStatement s =
            con
                .prepareStatement("INSERT INTO \"Modems\" "
                                  + "(\"DeviceID\", \"Alias\", \"Model\", \"FWVersion\", \"CfgVersion\", "
                                  + "\"IPAddress\", \"Port\", \"PhoneNumber\", \"Status\") "
                                  + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");

        try {
            if (modem.deviceID == null) {
                s.setNull(1, Types.BIGINT);
            }
            else {
                s.setLong(1, modem.deviceID.asSharedLong());
            }

            if (modem.alias == null) {
                s.setNull(2, Types.VARCHAR);
            }
            else {
                s.setString(2, modem.alias);
            }

            s.setString(3, modem.model);

            if (modem.fwVersion == null) {
                s.setNull(4, Types.VARCHAR);
            }
            else {
                s.setString(4, modem.fwVersion.toString());
            }

            if (modem.cfgVersion == null) {
                s.setNull(5, Types.VARCHAR);
            }
            else {
                s.setString(5, modem.cfgVersion);
            }

            if (modem.ipAddress == null) {
                s.setNull(6, Types.VARCHAR);
            }
            else {
                s.setString(6, modem.ipAddress);
            }

            s.setInt(7, modem.port);

            if (modem.phoneNumber == null) {
                s.setNull(8, Types.VARCHAR);
            }
            else {
                s.setString(8, modem.phoneNumber);
            }

            s.setString(9, modem.status.toString(false));

            s.executeUpdate();
        }
        finally {
            s.close();
        }

        s = con.prepareStatement("SELECT currval('\"Modems_ModemID_seq\"')");

        try {
            ResultSet rs = s.executeQuery();

            try {
                while (rs.next()) {
                    modem.modemID = rs.getInt(1);

                    logger.debug(String.format("Created new modem %d",
                        modem.modemID));

                    return;
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void createJob(Connection con, Job job)
        throws SQLException
    {
        // Unfortunately, RETURN_GENERATED_KEYS doesn't work with PostgreSQL
        // Luckily, it seems to hold onto this value per-connection, so we don't
        // need to enter transaction mode to update and read it

        PreparedStatement s =
            con
                .prepareStatement("INSERT INTO \"Jobs\" "
                                  + "(\"ModemID\", \"UserID\", \"Schedule\", \"Tries\", \"Type\", \"Parameters\", \"Automatic\", \"BatchID\") "
                                  + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

        try {
            s.setInt(1, job.getJobData().modemID);

            if (job.getJobData().userID == null) {
                s.setNull(2, Types.INTEGER);
            }
            else {
                s.setInt(2, job.getJobData().userID);
            }

            s
                .setTimestamp(3, new Timestamp(job.getJobData().schedule
                    .getTime()), utcCalendar);
            s.setInt(4, job.getJobData().tries);
            s.setString(5, job.getJobData().type);
            s.setString(6, job.getJobData().parameters.toString(false));
            s.setBoolean(7, job.getJobData().automatic);

            if (job.getJobData().batchID == null) {
                s.setNull(8, Types.INTEGER);
            }
            else {
                s.setInt(8, job.getJobData().batchID);
            }

            s.executeUpdate();
        }
        finally {
            s.close();
        }

        // Get the Job ID that the new job just took
        s = con.prepareStatement("SELECT currval('\"Jobs_JobID_seq\"')");

        try {
            ResultSet rs = s.executeQuery();

            try {
                while (rs.next()) {
                    job.getJobData().jobID = rs.getLong(1);

                    logger.debug(String.format("Scheduled new job %d", job
                        .getJobData().jobID));

                    return;
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void deleteJob(Connection con, long jobID)
        throws SQLException, ObjectInUseException, DatabaseException
    {

        // Acquire the job lock (okay if it's already held by this system)
        if (lockJob(con, jobID, 0) == LockResult.Failed) {
            throw new ObjectInUseException(String.format("Job %d is busy",
                jobID));
        }

        PreparedStatement s =
            con.prepareStatement("DELETE FROM \"Jobs\" WHERE \"JobID\"=?");

        try {
            s.setLong(1, jobID);

            s.executeUpdate();

            logger.debug(String.format("Deleted job %d", jobID));
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void log(Connection con, Collection<LogEntry> logEntries)
        throws SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("INSERT INTO \"Logs\" (\"Timestamp\", \"UserID\", \"Type\", \"Description\") "
                                  + "VALUES (?, ?, ?, ?)");

        try {
            for (LogEntry entry : logEntries) {
                s.setTimestamp(1, entry.timestamp, utcCalendar);

                if (entry.userID == null) {
                    s.setNull(2, Types.INTEGER);
                }
                else {
                    s.setInt(2, entry.userID);
                }

                s.setString(3, entry.type);

                s.setString(4, entry.description);

                try {
                    s.executeUpdate();
                }
                catch (SQLException sqle) {
                    logger.error("Failed to insert log entry", sqle);
                }
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void purgeLog(Connection con, java.util.Date cutoff)
        throws SQLException
    {
        PreparedStatement s =
            con.prepareStatement("DELETE FROM \"Logs\" WHERE \"Timestamp\"<?");

        try {
            s.setTimestamp(1, new Timestamp(cutoff.getTime()), utcCalendar);

            s.executeUpdate();

            logger.debug(String.format(
                "Purged log entries older than %1$tF %1$tT", cutoff));
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void removeLocksBy(Connection con, String name)
        throws SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Jobs\" SET \"LockedBy\"=NULL, \"LockedAt\"=NULL WHERE \"LockedBy\"=?");

        try {
            s.setString(1, name);

            s.executeUpdate();
        }
        finally {
            s.close();
        }

        s =
            con
                .prepareStatement("UPDATE \"Modems\" SET \"LockedBy\"=NULL, \"LockedAt\"=NULL WHERE \"LockedBy\"=?");

        try {
            s.setString(1, name);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void applyTag(Connection con, int modemID, int tagID)
        throws SQLException
    {
        // If it already exists, don't insert it
        PreparedStatement s =
            con.prepareStatement("SELECT COUNT(*) FROM \"TagMatch\" "
                                 + "WHERE \"TagID\"=? AND \"ModemID\"=?");

        try {
            s.setInt(1, tagID);
            s.setInt(2, modemID);

            ResultSet rs = s.executeQuery();

            try {
                // There will always be one result
                rs.next();

                int count = rs.getInt(1);

                // If it had something in it, no point in adding another; just
                // return now
                if (count > 0) {
                    return;
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }

        // If we get here, there is no matching row, insert it
        s =
            con
                .prepareStatement("INSERT INTO \"TagMatch\" (\"TagID\", \"ModemID\") "
                                  + "VALUES (?, ?)");

        try {
            s.setInt(1, tagID);
            s.setInt(2, modemID);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void createTag(Connection con, ModemTag tag)
        throws SQLException
    {
        // Unfortunately, PostrgeSQL does not support RETURN_GENERATED_KEYS
        // However, it does hold onto these values per-connection, so we don't
        // need to enter transaction mode

        // If the tag has a -1 position, we need to do something a little fancy

        PreparedStatement s;

        if (tag.position == -1) {
            // To summarize, INSERT INTO Tags (columns we want) values
            // (select the highest existing position + 1; if the table is
            // empty, the select will return null, so coalesce will move on
            // and return 0, then toss in the rest of the values)
            s =
                con
                    .prepareStatement("INSERT INTO \"Tags\" (\"Position\", \"TagName\", "
                                      + "\"ConfigScript\", \"PollingInterval\") "
                                      + "VALUES ((SELECT COALESCE((SELECT \"Position\" FROM "
                                      + "\"Tags\" ORDER BY \"Position\" DESC LIMIT 1) + 1, 0)), "
                                      + "?, ?, ?)");
        }
        else {
            s =
                con
                    .prepareStatement("INSERT INTO \"Tags\" (\"Position\", \"TagName\", "
                                      + "\"ConfigScript\", \"PollingInterval\") "
                                      + "VALUES (?, ?, ?, ?)");
        }

        try {
            int position = 1;

            if (tag.position != -1) {
                s.setInt(position++, tag.position);
            }

            s.setString(position++, tag.name);

            if (tag.configScript == null) {
                s.setNull(position++, Types.VARCHAR);
            }
            else {
                s.setString(position++, tag.configScript);
            }

            s.setInt(position++, tag.pollingInterval);

            s.executeUpdate();
        }
        finally {
            s.close();
        }

        s = con.prepareStatement("SELECT currval('\"Tags_TagID_seq\"')");

        try {
            ResultSet rs = s.executeQuery();

            try {
                while (rs.next()) {
                    tag.tagID = rs.getInt(1);

                    logger.debug(String.format("Created tag with ID %d",
                        tag.tagID));

                    break;
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }

        // If position is -1, we need to grab the position that we chose
        s =
            con
                .prepareStatement("SELECT \"Position\" FROM \"Tags\" WHERE \"TagID\"=?");

        try {
            s.setInt(1, tag.tagID);

            ResultSet rs = s.executeQuery();

            try {
                while (rs.next()) {
                    tag.position = rs.getInt(1);

                    break;
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void removeTag(Connection con, int modemID, int tagID)
        throws SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("DELETE FROM \"TagMatch\" WHERE \"TagID\"=? AND \"ModemID\"=?");

        try {
            s.setInt(1, tagID);
            s.setInt(2, modemID);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void updateSetting(Connection con, String settingName,
                                 String settingValue)
        throws SQLException
    {
        // Make sure there's nothing there already
        boolean exists = false;

        PreparedStatement s =
            con.prepareStatement("SELECT COUNT(*) FROM \"Settings\" "
                                 + "WHERE \"SettingName\"=?");

        try {
            s.setString(1, settingName);

            ResultSet rs = s.executeQuery();

            try {
                // There will always be one result
                rs.next();

                int count = rs.getInt(1);

                if (count > 0) {
                    exists = true;
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }

        if (exists) {
            s =
                con
                    .prepareStatement("UPDATE \"Settings\" SET \"SettingValue\"=? "
                                      + "WHERE \"SettingName\"=?");

            try {
                if (settingValue == null) {
                    s.setNull(1, Types.VARCHAR);
                }
                else {
                    s.setString(1, settingValue);
                }

                s.setString(2, settingName);

                s.executeUpdate();
            }
            finally {
                s.close();
            }
        }
        else {
            s =
                con
                    .prepareStatement("INSERT INTO \"Settings\" (\"SettingName\", \"SettingValue\") "
                                      + "VALUES (?, ?)");

            try {
                s.setString(1, settingName);

                if (settingValue == null) {
                    s.setNull(2, Types.VARCHAR);
                }
                else {
                    s.setString(2, settingValue);
                }

                s.executeUpdate();
            }
            finally {
                s.close();
            }
        }
    }

    @Override
    protected void updateCapabilities(Connection con, Capabilities caps)
        throws SQLException
    {

        // Can't just delete my row and replace it; what if in between deleting
        // and replacing, the server crashed? All of the locks would be orphaned

        // Check whether we have a row yet
        boolean exists = false;
        PreparedStatement s =
            con
                .prepareStatement("SELECT COUNT(*) FROM \"Capabilities\" WHERE \"Server\"=?");

        try {
            s.setString(1, caps.getServerName());

            ResultSet rs = s.executeQuery();

            try {
                // There will always be one result
                rs.next();

                int count = rs.getInt(1);

                if (count > 0) {
                    // We have a row, so we just need to update it
                    exists = true;
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }

        if (exists) {
            s =
                con
                    .prepareStatement("UPDATE \"Capabilities\" SET \"UpdatedAt\"=?, "
                                      + "\"Databases\"=?, \"Jobs\"=?, \"Modems\"=?,"
                                      + "\"Extra\"=? WHERE \"Server\"=?");

            try {
                s.setTimestamp(1, new Timestamp(Server.getCurrentTime()
                    .getTime()), utcCalendar);
                s.setString(2, Utils.makeString(caps.getDatabases(), ":"));
                s.setString(3, Utils.makeString(caps.getJobs(), ":"));
                s.setString(4, Utils.makeString(caps.getModems(), ":"));
                s.setString(5, caps.getExtra().toString(false));
                s.setString(6, caps.getServerName());

                s.executeUpdate();
            }
            finally {
                s.close();
            }
        }
        else {
            s =
                con
                    .prepareStatement("INSERT INTO \"Capabilities\" (\"UpdatedAt\", \"Server\","
                                      + "\"Databases\", \"Jobs\", \"Modems\", \"Extra\") VALUES ("
                                      + "?, ?, ?, ?, ?, ?)");

            try {
                s.setTimestamp(1, new Timestamp(Server.getCurrentTime()
                    .getTime()), utcCalendar);
                s.setString(2, caps.getServerName());
                s.setString(3, Utils.makeString(caps.getDatabases(), ":"));
                s.setString(4, Utils.makeString(caps.getJobs(), ":"));
                s.setString(5, Utils.makeString(caps.getModems(), ":"));
                s.setString(6, caps.getExtra().toString(false));

                s.executeUpdate();
            }
            finally {
                s.close();
            }
        }
    }

    @Override
    protected void purgeCapabilities(Connection con, java.util.Date cutoff)
        throws SQLException
    {
        // By doing this in-line, we don't need to worry about a dead server
        // coming online and acquiring a lock just before we clear it, since it
        // needs to update its capabilities entry before it can acquire any
        // locks

        // Clear the job locks for any dead servers
        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Jobs\" SET \"LockedBy\"=? WHERE \"LockedBy\" IN "
                                  + "(SELECT \"Server\" FROM \"Capabilities\" WHERE \"UpdatedAt\"<?)");

        try {
            s.setNull(1, Types.VARCHAR);
            s.setTimestamp(2, new Timestamp(cutoff.getTime()), utcCalendar);

            s.executeUpdate();
        }
        finally {
            s.close();
        }

        // Clear the modem locks for any dead servers
        s =
            con
                .prepareStatement("UPDATE \"Modems\" SET \"LockedBy\"=? WHERE \"LockedBy\" IN "
                                  + "(SELECT \"Server\" FROM \"Capabilities\" WHERE \"UpdatedAt\"<?)");

        try {
            s.setNull(1, Types.VARCHAR);
            s.setTimestamp(2, new Timestamp(cutoff.getTime()), utcCalendar);

            s.executeUpdate();
        }
        finally {
            s.close();
        }

        // Delete the entries in the capabilities table that we just cleared
        s =
            con
                .prepareStatement("DELETE FROM \"Capabilities\" WHERE \"UpdatedAt\"<?");

        try {
            s.setTimestamp(1, new Timestamp(cutoff.getTime()), utcCalendar);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Map<String, BlueTreeImage> getPackages(Connection con,
                                                     Collection<String> hashes,
                                                     boolean metadataOnly)
        throws SQLException
    {

        StringBuilder whereClause = new StringBuilder("WHERE FALSE");
        for (String hash : hashes) {
            whereClause.append(" OR \"Checksum\"=?");
        }

        PreparedStatement s;

        if (metadataOnly) {
            s =
                con
                    .prepareStatement("SELECT \"Checksum\", \"Type\", \"Version\", \"Date\", " +
                                      "\"Info\", " +
                                      "(SELECT COUNT(*) FROM \"Jobs\" " +
                                      "  WHERE position(lower('<firmware>' || " +
                                      "                       \"Packages\".\"Checksum\" ||" +
                                      "                       '</firmware>') " +
                                      "                 IN lower(\"Jobs\".\"Parameters\")) > 0) " +
                                      "FROM \"Packages\" " +
                                      whereClause.toString());
        }
        else {
            s =
                con.prepareStatement(
                // Although the checksum is included in the data, this will
                    // allow us to log a meaningful message if the data is
                    // corrupt
                    "SELECT \"Checksum\", \"Data\", " +
                        "(SELECT COUNT(*) FROM \"Jobs\" " +
                        "  WHERE position(lower('<firmware>' || " +
                        "                       \"Packages\".\"Checksum\" ||" +
                        "                       '</firmware>') " +
                        "                 IN lower(\"Jobs\".\"Parameters\")) > 0) " +
                        "FROM \"Packages\" " + whereClause.toString());
        }

        try {
            int index = 1;
            for (String hash : hashes) {
                s.setString(index++, hash);
            }

            ResultSet rs = s.executeQuery();

            try {
                Map<String, BlueTreeImage> images =
                    new HashMap<String, BlueTreeImage>();

                while (rs.next()) {
                    if (metadataOnly) {
                        String hash = rs.getString(1);
                        ImageType type = ImageType.fromString(rs.getString(2));
                        Version version = new Version(rs.getString(3));
                        Timestamp date = rs.getTimestamp(4, utcCalendar);
                        String info = rs.getString(5);
                        int jobCount = rs.getInt(6);

                        BlueTreeImage image =
                            new BlueTreeImage(type, version, date, info, hash,
                                jobCount);

                        images.put(hash, image);
                    }
                    else {
                        String hash = rs.getString(1);
                        byte[] data = rs.getBytes(2);
                        int jobCount = rs.getInt(3);

                        BlueTreeImage image;
                        try {
                            image = new BlueTreeImage(data, jobCount);
                        }
                        catch (BadBTImageException bbtie) {
                            logger.error("Image with checksum '" + hash +
                                         "' has bad data, skipping", bbtie);

                            continue;
                        }

                        images.put(image.getChecksum(), image);
                    }
                }

                return images;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void createPackage(Connection con, BlueTreeImage image)
        throws SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("INSERT INTO \"Packages\" (\"Type\", \"Version\", \"Date\", "
                                  + "\"Info\", \"Checksum\", \"Data\") VALUES (?, ?, ?, ?, ?, ?)");

        try {
            s.setString(1, image.getType().toString());
            s.setString(2, image.getVersion().toString());
            s.setDate(3, new Date(image.getDate().getTime()));
            s.setString(4, image.getInfo());
            s.setString(5, image.getChecksum());

            s.setBytes(6, image.getPackageData());

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void deletePackage(Connection con, String hash)
        throws SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("DELETE FROM \"Packages\" WHERE \"Checksum\"=?");

        try {
            s.setString(1, hash);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Collection<Capabilities> getCurrentCapabilities(Connection con)
        throws SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("SELECT \"Server\", \"Databases\", \"Jobs\", \"Modems\", \"Extra\" "
                                  + "FROM \"Capabilities\" WHERE \"UpdatedAt\">?");

        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.SECOND, -Server.getServerSettings().getIntValue(
                Setting.CapabilitiesCutoff));

            s
                .setTimestamp(1, new Timestamp(cal.getTimeInMillis()),
                    utcCalendar);

            ResultSet rs = s.executeQuery();

            try {
                Collection<Capabilities> caps = new LinkedList<Capabilities>();

                while (rs.next()) {
                    Tag extra;

                    try {
                        extra = Utils.parseXML(rs.getString(5), true);
                    }
                    catch (IOException ioe) {
                        logger.warn(String.format(
                            "Capabilities entry for '%s' has bad extra data",
                            rs.getString(1)), ioe);

                        extra = new Tag("extra");
                    }

                    Capabilities cap =
                        new Capabilities(rs.getString(1), rs.getString(2), rs
                            .getString(3), rs.getString(4), extra);
                    caps.add(cap);
                }

                return caps;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Map<Integer, ModemTag> getTags(Connection con,
                                             Collection<Integer> tagIDs)
        throws SQLException
    {

        StringBuilder whereClause = new StringBuilder("WHERE FALSE");
        for (int id : tagIDs) {
            whereClause.append(" OR \"TagID\"=?");
        }

        PreparedStatement s =
            con
                .prepareStatement("SELECT \"TagID\", \"Position\", \"TagName\", " +
                                  "\"ConfigScript\", \"PollingInterval\", " +
                                  "(SELECT COUNT(*) FROM \"TagMatch\" " +
                                  "  WHERE \"TagMatch\".\"TagID\"=\"Tags\".\"TagID\") " +
                                  "FROM \"Tags\" " + whereClause.toString());

        try {
            int index = 1;
            for (int id : tagIDs) {
                s.setInt(index++, id);
            }

            ResultSet rs = s.executeQuery();

            try {
                Map<Integer, ModemTag> tags = new HashMap<Integer, ModemTag>();

                while (rs.next()) {
                    int tagID = rs.getInt(1);
                    int position = rs.getInt(2);
                    String tagName = rs.getString(3);

                    String cfgScript = rs.getString(4);
                    if (rs.wasNull()) {
                        cfgScript = null;
                    }

                    int pollingInterval = rs.getInt(5);
                    int modemCount = rs.getInt(6);

                    ModemTag tag =
                        new ModemTag(tagID, position, tagName, cfgScript,
                            pollingInterval, modemCount);

                    tags.put(tagID, tag);
                }

                return tags;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void updateTag(Connection con, ModemTag tag)
        throws SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Tags\" SET \"Position\"=?, \"TagName\"=?, "
                                  + "\"ConfigScript\"=?, \"PollingInterval\"=? "
                                  + "WHERE \"TagID\"=?");

        try {
            s.setInt(1, tag.position);
            s.setString(2, tag.name);

            if (tag.configScript == null) {
                s.setNull(3, Types.VARCHAR);
            }
            else {
                s.setString(3, tag.configScript);
            }

            s.setInt(4, tag.pollingInterval);

            s.setInt(5, tag.tagID);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected List<Integer> getMatchingTags(Connection con, Integer tagID,
                                            String tagName,
                                            Integer lowPollingInterval,
                                            Integer highPollingInterval,
                                            Integer minPosition,
                                            Integer maxPosition,
                                            Integer modemID, int start,
                                            int limit, List<TagColumn> sortOn,
                                            long[] matchCount)
        throws SQLException
    {

        String tagMatchJoin = "";

        StringBuilder where = new StringBuilder();
        where.append("WHERE TRUE");

        if (tagID != null) {
            where.append(" AND \"Tags\".\"TagID\"=?");
        }

        if (tagName != null) {
            where.append(" AND \"Tags\".\"TagName\"=?");
        }

        if (lowPollingInterval != null) {
            where.append(" AND \"Tags\".\"PollingInterval\">=?");
        }

        if (highPollingInterval != null) {
            where.append(" AND \"Tags\".\"PollingInterval\"<=?");
        }

        if (minPosition != null) {
            where.append(" AND \"Tags\".\"Position\">=?");
        }

        if (maxPosition != null) {
            where.append(" AND \"Tags\".\"Position\"<=?");
        }

        if (modemID != null) {
            tagMatchJoin =
                "INNER JOIN \"TagMatch\" ON \"Tags\".\"TagID\"=\"TagMatch\".\"TagID\"";
            where.append(" AND \"TagMatch\".\"ModemID\"=?");
        }

        if (matchCount != null && matchCount.length > 0) {
            PreparedStatement s =
                con.prepareStatement("SELECT COUNT(*) FROM \"Tags\" " +
                                     tagMatchJoin + " " + where.toString());

            try {
                int position = 1;

                if (tagID != null) {
                    s.setInt(position++, tagID);
                }

                if (tagName != null) {
                    s.setString(position++, tagName);
                }

                if (lowPollingInterval != null) {
                    s.setInt(position++, lowPollingInterval);
                }

                if (highPollingInterval != null) {
                    s.setInt(position++, highPollingInterval);
                }

                if (minPosition != null) {
                    s.setInt(position++, minPosition);
                }

                if (maxPosition != null) {
                    s.setInt(position++, maxPosition);
                }

                if (modemID != null) {
                    s.setInt(position++, modemID);
                }

                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        matchCount[0] = rs.getLong(1);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }
        }

        String limitClause = "";
        if (limit > -1) {
            limitClause = String.format("LIMIT %d OFFSET %d", limit, start);
        }

        StringBuilder sortClause = new StringBuilder();
        if (sortOn != null && sortOn.size() > 0) {
            sortClause.append("ORDER BY ");

            boolean first = true;
            boolean reverse = false;
            for (TagColumn col : sortOn) {
                String colName = null;
                switch (col) {
                    case ModemCount:
                        colName =
                            "(SELECT COUNT(*) FROM \"TagMatch\" WHERE \"TagMatch\".\"TagID\"=\"Tags\".\"TagID\")";
                        break;
                    case PollingInterval:
                        colName = "\"Tags\".\"PollingInterval\"";
                        break;
                    case Position:
                        colName = "\"Tags\".\"Position\"";
                        break;
                    case Reverse:
                        reverse = !reverse;
                        continue;
                    case TagID:
                        colName = "\"Tags\".\"TagID\"";
                        break;
                    case TagName:
                        colName = "\"Tags\".\"TagName\"";
                        break;

                }

                if (first) {
                    first = false;
                }
                else {
                    sortClause.append(", ");
                }

                sortClause.append(colName);

                if (reverse) {
                    sortClause.append(" DESC");
                    reverse = false;
                }
                else {
                    sortClause.append(" ASC");
                }
            }
        }
        else {
            // Sort by position, ascending
            sortClause.append("ORDER BY \"Tags\".\"Position\" ASC");
        }

        PreparedStatement s =
            con.prepareStatement("SELECT \"Tags\".\"TagID\" FROM \"Tags\" " +
                                 tagMatchJoin + " " + where.toString() + " " +
                                 sortClause + " " + limitClause);

        try {
            int position = 1;

            if (tagID != null) {
                s.setInt(position++, tagID);
            }

            if (tagName != null) {
                s.setString(position++, tagName);
            }

            if (lowPollingInterval != null) {
                s.setInt(position++, lowPollingInterval);
            }

            if (highPollingInterval != null) {
                s.setInt(position++, highPollingInterval);
            }

            if (minPosition != null) {
                s.setInt(position++, minPosition);
            }

            if (maxPosition != null) {
                s.setInt(position++, maxPosition);
            }

            if (modemID != null) {
                s.setInt(position++, modemID);
            }

            ResultSet rs = s.executeQuery();

            try {
                List<Integer> tags = new LinkedList<Integer>();

                while (rs.next()) {
                    int tid = rs.getInt(1);

                    tags.add(tid);
                }

                return tags;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Collection<TagMatch> getTagMatch(Connection con, int tagID,
                                               int modemID)
        throws SQLException
    {
        StringBuilder where = new StringBuilder();

        where.append("WHERE ");

        if (tagID == -1 && modemID == -1) {
            // No where clause
            where.append("TRUE");
        }
        else if (tagID == -1) {
            // Only a modem ID
            where.append("\"ModemID\"=?");
        }
        else if (modemID == -1) {
            // Only a tag ID
            where.append("\"TagID\"=?");
        }
        else {
            // Both
            where.append("\"TagID\"=? AND \"ModemID\"=?");
        }

        PreparedStatement s =
            con
                .prepareStatement("SELECT \"TagID\", \"ModemID\" from \"TagMatch\" " +
                                  where.toString());

        try {
            if (tagID == -1 && modemID == -1) {
                // Nothing to set
            }
            else if (tagID == -1) {
                // Set the modem ID
                s.setInt(1, modemID);
            }
            else if (modemID == -1) {
                // Set the tag ID
                s.setInt(1, tagID);
            }
            else {
                // Set both
                s.setInt(1, tagID);
                s.setInt(2, modemID);
            }

            ResultSet rs = s.executeQuery();

            try {
                Collection<TagMatch> matches = new LinkedList<TagMatch>();

                while (rs.next()) {
                    matches.add(new TagMatch(rs.getInt(1), rs.getInt(2)));
                }

                return matches;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void deleteTag(Connection con, int tagID)
        throws SQLException
    {
        PreparedStatement s =
            con.prepareStatement("DELETE FROM \"Tags\" WHERE \"TagID\"=?");

        try {
            s.setInt(1, tagID);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void deleteModem(Connection con, int modemID)
        throws SQLException, ObjectInUseException, DatabaseException
    {

        boolean success = false;

        // Acquire the modem lock (okay if it's already held by this system)
        if (lockModem(con, modemID, 0) == LockResult.Failed) {
            throw new ObjectInUseException(String.format("Modem %d is busy",
                modemID));
        }

        try {
            // Delete the modem
            PreparedStatement s =
                con
                    .prepareStatement("DELETE FROM \"Modems\" WHERE \"ModemID\"=?");

            try {
                s.setInt(1, modemID);

                s.executeUpdate();
            }
            finally {
                s.close();
            }

            success = true;
        }
        finally {
            // If we were unable to delete the modem, at least unlock it
            if (!success) {
                unlockModem(con, modemID);
            }
        }
    }

    @Override
    protected Collection<String> getMatchingPackages(Connection con,
                                                     String type,
                                                     Version minVersion,
                                                     Version maxVersion,
                                                     java.util.Date minDate,
                                                     java.util.Date maxDate,
                                                     String checksum,
                                                     int start, int limit,
                                                     long[] matchCount)
        throws DatabaseException, SQLException
    {

        // Databases can't compare version numbers, so if the user specifies a
        // min/max version, we need to grab all known versions and manually use
        // '='; if not, the list of versions to select will be empty
        List<Version> versions = new LinkedList<Version>();
        if (minVersion != null || maxVersion != null) {
            PreparedStatement s =
                con
                    .prepareStatement("SELECT DISTINCT \"Version\" FROM \"Packages\"");

            try {
                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        Version v = new Version(rs.getString(1));
                        versions.add(v);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }

            // Sort into an array so we can do a simple search
            Version[] sortedVersions = versions.toArray(new Version[0]);
            Arrays.sort(sortedVersions);

            // Clear out 'versions' and fill it with only the versions that we
            // will want to select
            versions.clear();

            // If there is no minimum version, we want to immediately start
            // selecting values
            boolean selecting = (minVersion == null);
            for (Version v : sortedVersions) {
                if (selecting) {
                    if (maxVersion != null) {
                        if (maxVersion.compareTo(v) > 0) {
                            // Past the maximum version
                            break;
                        }
                    }

                    versions.add(v);
                }
                else {
                    // minVersion must not be null, since the value of selecting
                    // initially depends on that
                    if (minVersion.compareTo(v) >= 0) {
                        // Found the first match
                        selecting = true;
                        versions.add(v);
                    }
                }
            }
        }

        StringBuilder where = new StringBuilder();
        where.append("WHERE TRUE");

        if (type != null) {
            where.append(" AND \"Type\"=?");
        }

        if (!versions.isEmpty()) {
            where.append(" AND (FALSE");

            for (Version v : versions) {
                where.append(" OR \"Version\"=?");
            }

            where.append(")");
        }

        if (minDate != null) {
            where.append(" AND \"Date\">=?");
        }

        if (maxDate != null) {
            where.append(" AND \"Date\"<=?");
        }

        if (checksum != null) {
            where.append(" AND \"Checksum\"=?");
        }

        if (matchCount != null && matchCount.length > 0) {
            PreparedStatement s =
                con.prepareStatement("SELECT COUNT(*) FROM \"Packages\" " +
                                     where.toString());

            try {
                int position = 1;

                if (type != null) {
                    s.setString(position++, type);
                }

                for (Version v : versions) {
                    s.setString(position++, v.toString());
                }

                if (minDate != null) {
                    s.setTimestamp(position++,
                        new Timestamp(minDate.getTime()), utcCalendar);
                }

                if (maxDate != null) {
                    s.setTimestamp(position++,
                        new Timestamp(maxDate.getTime()), utcCalendar);
                }

                if (checksum != null) {
                    s.setString(position++, checksum);
                }

                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        matchCount[0] = rs.getLong(1);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }
        }

        PreparedStatement s =
            con
                .prepareStatement("SELECT \"Checksum\", \"Version\" FROM \"Packages\" " +
                                  where.toString());

        try {
            int position = 1;

            if (type != null) {
                s.setString(position++, type);
            }

            for (Version v : versions) {
                s.setString(position++, v.toString());
            }

            if (minDate != null) {
                s.setTimestamp(position++, new Timestamp(minDate.getTime()),
                    utcCalendar);
            }

            if (maxDate != null) {
                s.setTimestamp(position++, new Timestamp(maxDate.getTime()),
                    utcCalendar);
            }

            if (checksum != null) {
                s.setString(position++, checksum);
            }

            ResultSet rs = s.executeQuery();

            try {
                Map<Version, List<String>> packages =
                    new HashMap<Version, List<String>>();

                while (rs.next()) {
                    Version v = new Version(rs.getString(2));

                    List<String> matchingPackages = packages.get(v);
                    if (matchingPackages == null) {
                        matchingPackages = new LinkedList<String>();
                        packages.put(v, matchingPackages);
                    }

                    matchingPackages.add(rs.getString(1));
                }

                // Sort the packages by version number, ascending
                Version[] knownVersions =
                    packages.keySet().toArray(new Version[0]);
                Arrays.sort(knownVersions);

                // Pull out the package checksums in order
                Collection<String> checksums = new LinkedList<String>();
                for (Version v : knownVersions) {
                    checksums.addAll(packages.get(v));
                }

                // Cut it down to the provided limit
                if (limit != -1) {
                    for (Iterator<String> itChecksums = checksums.iterator(); itChecksums
                        .hasNext();) {
                        itChecksums.next();

                        if (start > 0) {
                            // Haven't hit the start row yet, remove this one
                            itChecksums.remove();
                            start--;
                        }
                        else if (limit == 0) {
                            // We've gone through the desired range, remove the
                            // rest
                            itChecksums.remove();
                        }
                        else {
                            // We're in the desired range, keep track of how
                            // many we've seen
                            limit--;
                        }
                    }
                }

                return checksums;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Map<String, String> getModemStatus(Connection con, int modemID,
                                                 boolean mostRecent,
                                                 java.util.Date minDate,
                                                 java.util.Date maxDate)
        throws DatabaseException, SQLException
    {

        Map<String, String> props = new HashMap<String, String>();

        StringBuilder where = new StringBuilder();
        where.append("WHERE \"ModemID\"=?");

        PreparedStatement s;

        Timestamp timestamp = null;

        if (mostRecent) {
            // Grab the timestamp of the most recent status information for this
            // modem
            s =
                con
                    .prepareStatement("SELECT \"Timestamp\" FROM \"StatusHistory\" WHERE \"ModemID\"=? "
                                      + "ORDER BY \"Timestamp\" DESC LIMIT 1");

            try {
                s.setInt(1, modemID);

                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        timestamp = rs.getTimestamp(1, utcCalendar);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }

            if (timestamp == null) {
                // There are no status entries in the table for this modem
                return props;
            }

            where.append(" AND \"Timestamp\"=?");
        }
        else {
            // The user may have specified a date range to search
            if (minDate != null) {
                where.append(" AND \"Timestamp\">=?");
            }

            if (maxDate != null) {
                where.append(" AND \"Timestamp\"<=?");
            }
        }

        // Select all status entries that were created at that time
        s =
            con
                .prepareStatement("SELECT \"PropertyName\", \"PropertyVal\", \"Timestamp\" " +
                                  "FROM \"StatusHistory\" " + where.toString());

        try {
            s.setInt(1, modemID);

            int position = 2;

            if (timestamp != null) {
                // The user can either select the most recent update...
                s.setTimestamp(position++, timestamp, utcCalendar);
            }
            else {
                // Or specify a date range, but not both
                if (minDate != null) {
                    s.setTimestamp(position++,
                        new Timestamp(minDate.getTime()), utcCalendar);
                }

                if (maxDate != null) {
                    s.setTimestamp(position++,
                        new Timestamp(maxDate.getTime()), utcCalendar);
                }
            }

            ResultSet rs = s.executeQuery();

            try {
                while (rs.next()) {
                    props.put(rs.getString(1) +
                              "@" +
                              Utils.formatStandardDateTime(rs.getTimestamp(3,
                                  utcCalendar), false), rs.getString(2));
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }

        return props;
    }

    @Override
    protected Collection<Integer> getMatchingModems(Connection con,
                                                    Integer modemID,
                                                    DeviceID deviceID,
                                                    String alias, String model,
                                                    Version fwVersion,
                                                    String cfgVersion,
                                                    String ipAddress,
                                                    Integer port, String phone,
                                                    Integer tagID, int start,
                                                    int limit,
                                                    List<ModemColumn> sortOn,
                                                    long[] matchCount)
        throws DatabaseException, SQLException
    {

        StringBuilder where = new StringBuilder();
        where.append("WHERE TRUE");

        if (modemID != null) {
            where.append(" AND \"Modems\".\"ModemID\"=?");
        }

        if (deviceID != null) {
            if (deviceID.equals(DeviceID.DEVID_UNKNOWN)) {
                where.append(" AND \"Modems\".\"DeviceID\" IS NULL");
            }
            else {
                where.append(" AND \"Modems\".\"DeviceID\"=?");
            }
        }

        if (alias != null) {
            if (alias.equals("-")) {
                where.append(" AND \"Modems\".\"Alias\" IS NULL");
            }
            else {
                where.append(" AND \"Modems\".\"Alias\"=?");
            }
        }

        if (model != null) {
            if (model.equals("-")) {
                where.append(" AND \"Modems\".\"Model\" IS NULL");
            }
            else {
                where.append(" AND \"Modems\".\"Model\"=?");
            }
        }

        if (fwVersion != null) {
            if (fwVersion.toString().equals("-")) {
                where.append(" AND \"Modems\".\"FWVersion\" IS NULL");
            }
            else {
                where.append(" AND \"Modems\".\"FWVersion\"=?");
            }
        }

        if (cfgVersion != null) {
            if (cfgVersion.equals("-")) {
                where.append(" AND \"Modems\".\"CfgVersion\" IS NULL");
            }
            else {
                where.append(" AND \"Modems\".\"CfgVersion\"=?");
            }
        }

        if (ipAddress != null) {
            if (ipAddress.equals("-")) {
                where.append(" AND \"Modems\".\"IPAddress\" IS NULL");
            }
            else {
                where.append(" AND \"Modems\".\"IPAddress\"=?");
            }
        }

        if (port != null) {
            where.append(" AND \"Modems\".\"Port\"=?");
        }

        if (phone != null) {
            if (phone.equals("-")) {
                where.append(" AND \"Modems\".\"PhoneNumber\" IS NULL");
            }
            else {
                where.append(" AND \"Modems\".\"PhoneNumber\"=?");
            }
        }

        // If tagID was provided, we also need to perform a join
        String tagMatchJoin = "";

        if (tagID != null) {
            tagMatchJoin =
                "INNER JOIN \"TagMatch\" ON \"Modems\".\"ModemID\"=\"TagMatch\".\"ModemID\"";
            where.append(" AND \"TagMatch\".\"TagID\"=?");
        }

        if (matchCount != null && matchCount.length > 0) {
            PreparedStatement s =
                con.prepareStatement("SELECT COUNT(*) FROM \"Modems\" " +
                                     tagMatchJoin.toString() + " " +
                                     where.toString());

            try {
                int position = 1;

                if (modemID != null) {
                    s.setInt(position++, modemID);
                }

                if (deviceID != null) {
                    if (deviceID.equals(DeviceID.DEVID_UNKNOWN)) {
                        // Do nothing
                    }
                    else {
                        s.setLong(position++, deviceID.asSharedLong());
                    }
                }

                if (alias != null) {
                    if (alias.equals("-")) {
                        // Do nothing
                    }
                    else {
                        s.setString(position++, alias);
                    }
                }

                if (model != null) {
                    if (model.equals("-")) {
                        // Do nothing
                    }
                    else {
                        s.setString(position++, model);
                    }
                }

                if (fwVersion != null) {
                    if (fwVersion.toString().equals("-")) {
                        // Do nothing
                    }
                    else {
                        s.setString(position++, fwVersion.toString());
                    }
                }

                if (cfgVersion != null) {
                    if (cfgVersion.equals("-")) {
                        // Do nothing
                    }
                    else {
                        s.setString(position++, cfgVersion);
                    }
                }

                if (ipAddress != null) {
                    if (ipAddress.equals("-")) {
                        // Do nothing
                    }
                    else {
                        s.setString(position++, ipAddress);
                    }
                }

                if (port != null) {
                    s.setInt(position++, port);
                }

                if (phone != null) {
                    if (phone.equals("-")) {
                        // Do nothing
                    }
                    else {
                        s.setString(position++, phone);
                    }
                }

                if (tagID != null) {
                    s.setInt(position++, tagID);
                }

                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        matchCount[0] = rs.getLong(1);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }
        }

        String limitClause = "";
        if (limit > -1) {
            limitClause = String.format("LIMIT %d OFFSET %d", limit, start);
        }

        StringBuilder sortClause = new StringBuilder();
        // Set to true to include warn/fail cutoffs
        boolean includeCutoffs = false;
        if (sortOn != null && sortOn.size() > 0) {
            sortClause.append("ORDER BY ");

            boolean first = true;
            boolean reverse = false;
            for (ModemColumn col : sortOn) {
                String colName = null;
                switch (col) {
                    case Alias:
                        colName = "\"Modems\".\"Alias\"";
                        break;
                    case CfgVersion:
                        colName = "\"Modems\".\"CfgVersion\"";
                        break;
                    case DeviceID:
                        colName = "\"Modems\".\"DeviceID\"";
                        break;
                    case FWVersion:
                        colName = "\"Modems\".\"FWVersion\"";
                        break;
                    case Health:
                        includeCutoffs = true;

                        // 1 = Normal
                        // 2 = Stale
                        // 3 = Warning
                        // 4 = Failure
                        colName =
                            "CASE"
                                // If no history within the failure
                                // threshold, stale
                                + "  WHEN (SELECT COUNT(*)" // Count
                                + "        FROM \"History\""
                                + "        WHERE \"History\".\"ModemID\"=\"Modems\".\"ModemID\""
                                + "          AND \"History\".\"Timestamp\" >= ?"
                                + "       ) = 0 THEN 2"
                                // Look for successes within the failure
                                // threshold
                                + "  WHEN (SELECT COUNT(*)" // Count
                                + "        FROM \"History\""
                                + "        WHERE \"History\".\"ModemID\"=\"Modems\".\"ModemID\""
                                + "          AND \"History\".\"Timestamp\" >= ?"
                                + "          AND \"History\".\"Result\" = 'Success'"
                                + "       ) = 0 THEN 4"
                                // Look for success within the warning
                                // threshold
                                + "  WHEN (SELECT COUNT(*)" // Count
                                + "        FROM \"History\""
                                + "        WHERE \"History\".\"ModemID\"=\"Modems\".\"ModemID\""
                                + "          AND \"History\".\"Timestamp\" >= ?"
                                + "          AND \"History\".\"Result\" = 'Success'"
                                + "       ) = 0 THEN 3"
                                // Found success within both thresholds?
                                // Normal
                                + "  ELSE 1 "
                                // End of the test
                                + "END";
                        break;
                    case IPAddress:
                        colName = "\"Modems\".\"IPAddress\"";
                        break;
                    case Model:
                        colName = "\"Modems\".\"Model\"";
                        break;
                    case ModemID:
                        colName = "\"Modems\".\"ModemID\"";
                        break;
                    case PhoneNumber:
                        colName = "\"Modems\".\"PhoneNumber\"";
                        break;
                    case Port:
                        colName = "\"Modems\".\"Port\"";
                        break;
                    case Reverse:
                        reverse = !reverse;
                        continue; // Don't output anything here

                }

                if (first) {
                    first = false;
                }
                else {
                    sortClause.append(", ");
                }

                sortClause.append(colName);

                if (reverse) {
                    sortClause.append(" DESC");
                    reverse = false;
                }
                else {
                    sortClause.append(" ASC");
                }
            }
        }
        else {
            // Default sort order
            sortClause.append("ORDER BY \"Modems\".\"ModemID\" DESC");
        }

        StringBuilder request = new StringBuilder();
        request.append("SELECT \"Modems\".\"ModemID\" FROM \"Modems\" ");
        request.append(tagMatchJoin.toString()).append(" ");
        request.append(where.toString()).append(" ");
        request.append(sortClause).append(" ");
        request.append(limitClause);

        logger.debug("SQL request: " + request.toString());

        PreparedStatement s = con.prepareStatement(request.toString());

        try {
            int position = 1;

            if (modemID != null) {
                s.setInt(position++, modemID);
            }

            if (deviceID != null) {
                if (deviceID.equals(DeviceID.DEVID_UNKNOWN)) {
                    // Do nothing
                }
                else {
                    s.setLong(position++, deviceID.asSharedLong());
                }
            }

            if (alias != null) {
                if (alias.equals("-")) {
                    // Do nothing
                }
                else {
                    s.setString(position++, alias);
                }
            }

            if (model != null) {
                if (model.equals("-")) {
                    // Do nothing
                }
                else {
                    s.setString(position++, model);
                }
            }

            if (fwVersion != null) {
                if (fwVersion.toString().equals("-")) {
                    // Do nothing
                }
                else {
                    s.setString(position++, fwVersion.toString());
                }
            }

            if (cfgVersion != null) {
                if (cfgVersion.equals("-")) {
                    // Do nothing
                }
                else {
                    s.setString(position++, cfgVersion);
                }
            }

            if (ipAddress != null) {
                if (ipAddress.equals("-")) {
                    // Do nothing
                }
                else {
                    s.setString(position++, ipAddress);
                }
            }

            if (port != null) {
                s.setInt(position++, port);
            }

            if (phone != null) {
                if (phone.equals("-")) {
                    // Do nothing
                }
                else {
                    s.setString(position++, phone);
                }
            }

            if (tagID != null) {
                s.setInt(position++, tagID);
            }

            if (includeCutoffs) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.SECOND, -Server.getServerSettings()
                    .getIntValue(Setting.FailureTimeout));

                // Check for any history
                s.setTimestamp(position++, new Timestamp(cal.getTime()
                    .getTime()), utcCalendar);

                // Check for success history in failure timeout
                s.setTimestamp(position++, new Timestamp(cal.getTime()
                    .getTime()), utcCalendar);

                cal = Calendar.getInstance();
                cal.add(Calendar.SECOND, -Server.getServerSettings()
                    .getIntValue(Setting.WarnTimeout));

                // Check for success history in warning timeout
                s.setTimestamp(position++, new Timestamp(cal.getTime()
                    .getTime()), utcCalendar);
            }

            ResultSet rs = s.executeQuery();

            try {
                Collection<Integer> modems = new LinkedList<Integer>();

                while (rs.next()) {
                    modems.add(rs.getInt(1));
                }

                return modems;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Collection<Long> getMatchingJobs(Connection con, Long jobID,
                                               Integer batchID,
                                               Integer modemID, Integer userID,
                                               String type,
                                               java.util.Date lowDate,
                                               java.util.Date highDate,
                                               Boolean automatic, int start,
                                               int limit,
                                               List<JobColumn> sortOn,
                                               long[] matchCount)
        throws DatabaseException, SQLException
    {

        StringBuilder where = new StringBuilder();
        where.append("WHERE TRUE");

        if (jobID != null) {
            where.append(" AND \"Jobs\".\"JobID\"=?");
        }

        if (batchID != null) {
            if (batchID == -1) {
                where.append(" AND \"Jobs\".\"BatchID\" IS NULL");
            }
            else {
                where.append(" AND \"Jobs\".\"BatchID\"=?");
            }
        }

        if (modemID != null) {
            where.append(" AND \"Jobs\".\"ModemID\"=?");
        }

        if (userID != null) {
            if (userID == -1) {
                where.append(" AND \"Jobs\".\"UserID\" IS NULL");
            }
            else {
                where.append(" AND \"Jobs\".\"UserID\"=?");
            }
        }

        if (type != null) {
            where.append(" AND \"Jobs\".\"Type\"=?");
        }

        if (lowDate != null) {
            where.append(" AND \"Jobs\".\"Schedule\">=?");
        }

        if (highDate != null) {
            where.append(" AND \"Jobs\".\"Schedule\"<=?");
        }

        if (automatic != null) {
            where.append(" AND \"Jobs\".\"Automatic\"=?");
        }

        if (matchCount != null && matchCount.length > 0) {
            PreparedStatement s =
                con.prepareStatement("SELECT COUNT(*) FROM \"Jobs\" " +
                                     where.toString());

            try {
                int position = 1;

                if (jobID != null) {
                    s.setLong(position++, jobID);
                }

                if (batchID != null) {
                    if (batchID == -1) {
                        // Don't actually add anything here
                    }
                    else {
                        s.setInt(position++, batchID);
                    }
                }

                if (modemID != null) {
                    s.setInt(position++, modemID);
                }

                if (userID != null) {
                    if (userID == -1) {
                        // Don't actually add anything here
                    }
                    else {
                        s.setInt(position++, userID);
                    }
                }

                if (type != null) {
                    s.setString(position++, type);
                }

                if (lowDate != null) {
                    s.setTimestamp(position++,
                        new Timestamp(lowDate.getTime()), utcCalendar);
                }

                if (highDate != null) {
                    s.setTimestamp(position++,
                        new Timestamp(highDate.getTime()), utcCalendar);
                }

                if (automatic != null) {
                    s.setBoolean(position++, automatic);
                }

                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        matchCount[0] = rs.getLong(1);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }
        }

        String limitClause = "";
        if (limit > -1) {
            limitClause = String.format("LIMIT %d OFFSET %d", limit, start);
        }

        String batchJoin = "";
        String modemJoin = "";
        String userJoin = "";

        StringBuilder sortClause = new StringBuilder();
        if (sortOn != null && sortOn.size() > 0) {
            sortClause.append("ORDER BY ");

            boolean first = true;
            boolean reverse = false;
            for (JobColumn col : sortOn) {
                String colName = null;
                switch (col) {
                    case Automatic:
                        colName = "\"Jobs\".\"Automatic\"";
                        break;
                    case BatchID:
                        colName = "\"Jobs\".\"BatchID\"";
                        break;
                    case BatchName:
                        batchJoin =
                            "LEFT JOIN \"Batches\" ON \"Jobs\".\"BatchID\"=\"Batches\".\"BatchID\"";
                        colName = "\"Batches\".\"BatchName\"";
                        break;
                    case JobID:
                        colName = "\"Jobs\".\"JobID\"";
                        break;
                    case ModemAlias:
                        modemJoin =
                            "LEFT JOIN \"Modems\" ON \"Jobs\".\"ModemID\"=\"Modems\".\"ModemID\"";
                        colName = "\"Modems\".\"Alias\"";
                        break;
                    case ModemID:
                        colName = "\"Jobs\".\"ModemID\"";
                        break;
                    case Reverse:
                        reverse = !reverse;
                        continue; // Don't output anything here
                    case Schedule:
                        colName = "\"Jobs\".\"Schedule\"";
                        break;
                    case Status:
                        colName = "\"Jobs\".\"LockedAt\"";
                        break;
                    case Tries:
                        colName = "\"Jobs\".\"Tries\"";
                        break;
                    case Type:
                        colName = "\"Jobs\".\"Type\"";
                        break;
                    case UserID:
                        colName = "\"Jobs\".\"UserID\"";
                        break;
                    case UserName:
                        userJoin =
                            "LEFT JOIN \"Users\" ON \"Jobs\".\"UserID\"=\"Users\".\"UserID\"";
                        colName = "\"Users\".\"UserName\"";
                        break;
                }

                if (first) {
                    first = false;
                }
                else {
                    sortClause.append(", ");
                }

                sortClause.append(colName);

                if (reverse) {
                    sortClause.append(" DESC");
                    reverse = false;
                }
                else {
                    sortClause.append(" ASC");
                }
            }
        }
        else {
            // Default sort order
            sortClause.append("ORDER BY \"Jobs\".\"JobID\" DESC");
        }

        PreparedStatement s =
            con.prepareStatement("SELECT \"Jobs\".\"JobID\" FROM \"Jobs\" " +
                                 batchJoin + " " + modemJoin + " " + userJoin +
                                 " " + where.toString() + " " + sortClause +
                                 " " + limitClause);

        try {
            int position = 1;

            if (jobID != null) {
                s.setLong(position++, jobID);
            }

            if (batchID != null) {
                if (batchID == -1) {
                    // Don't actually add anything here
                }
                else {
                    s.setInt(position++, batchID);
                }
            }

            if (modemID != null) {
                s.setInt(position++, modemID);
            }

            if (userID != null) {
                if (userID == -1) {
                    // Don't actually add anything here
                }
                else {
                    s.setInt(position++, userID);
                }
            }

            if (type != null) {
                s.setString(position++, type);
            }

            if (lowDate != null) {
                s.setTimestamp(position++, new Timestamp(lowDate.getTime()),
                    utcCalendar);
            }

            if (highDate != null) {
                s.setTimestamp(position++, new Timestamp(highDate.getTime()),
                    utcCalendar);
            }

            if (automatic != null) {
                s.setBoolean(position++, automatic);
            }

            ResultSet rs = s.executeQuery();

            try {
                Collection<Long> jobs = new LinkedList<Long>();
                while (rs.next()) {
                    jobs.add(rs.getLong(1));
                }

                return jobs;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Collection<Long> getMatchingHistory(Connection con,
                                                  Long historyID, Long jobID,
                                                  Integer batchID,
                                                  Integer modemID,
                                                  Integer userID,
                                                  String jobType,
                                                  String result,
                                                  java.util.Date lowDate,
                                                  java.util.Date highDate,
                                                  int start, int limit,
                                                  List<HistoryColumn> sortOn,
                                                  long[] matchCount)
        throws DatabaseException, SQLException
    {

        StringBuilder where = new StringBuilder();
        where.append("WHERE TRUE");

        if (historyID != null) {
            where.append(" AND \"History\".\"HistoryID\"=?");
        }

        if (jobID != null) {
            where.append(" AND \"History\".\"JobID\"=?");
        }

        if (batchID != null) {
            if (batchID == -1) {
                where.append(" AND \"History\".\"BatchID\" IS NULL");
            }
            else {
                where.append(" AND \"History\".\"BatchID\"=?");
            }
        }

        if (modemID != null) {
            where.append(" AND \"History\".\"ModemID\"=?");
        }

        if (userID != null) {
            if (userID == -1) {
                where.append(" AND \"History\".\"UserID\" IS NULL");
            }
            else {
                where.append(" AND \"History\".\"UserID\"=?");
            }
        }

        if (jobType != null) {
            where.append(" AND \"History\".\"Type\"=?");
        }

        if (result != null) {
            where.append(" AND \"History\".\"Result\"=?");
        }

        if (lowDate != null) {
            where.append(" AND \"History\".\"Timestamp\">=?");
        }

        if (highDate != null) {
            where.append(" AND \"History\".\"Timestamp\"<=?");
        }

        if (matchCount != null && matchCount.length > 0) {
            PreparedStatement s =
                con.prepareStatement("SELECT COUNT(*) FROM \"History\" " +
                                     where.toString());

            try {
                int position = 1;

                if (historyID != null) {
                    s.setLong(position++, historyID);
                }

                if (jobID != null) {
                    s.setLong(position++, jobID);
                }

                if (batchID != null) {
                    if (batchID == -1) {
                        // Don't actually add anything here
                    }
                    else {
                        s.setInt(position++, batchID);
                    }
                }

                if (modemID != null) {
                    s.setInt(position++, modemID);
                }

                if (userID != null) {
                    if (userID == -1) {
                        // Don't actually add anything here
                    }
                    else {
                        s.setInt(position++, userID);
                    }
                }

                if (jobType != null) {
                    s.setString(position++, jobType);
                }

                if (result != null) {
                    s.setString(position++, result);
                }

                if (lowDate != null) {
                    s.setTimestamp(position++,
                        new Timestamp(lowDate.getTime()), utcCalendar);
                }

                if (highDate != null) {
                    s.setTimestamp(position++,
                        new Timestamp(highDate.getTime()), utcCalendar);
                }

                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        matchCount[0] = rs.getLong(1);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }
        }

        String limitClause = "";
        if (limit > -1) {
            limitClause = String.format("LIMIT %d OFFSET %d", limit, start);
        }

        String batchJoin = "";
        String modemJoin = "";
        String userJoin = "";

        StringBuilder sortClause = new StringBuilder();
        if (sortOn != null && sortOn.size() > 0) {
            sortClause.append("ORDER BY ");

            boolean first = true;
            boolean reverse = false;
            for (HistoryColumn col : sortOn) {
                String colName = null;
                switch (col) {
                    case BatchID:
                        colName = "\"History\".\"BatchID\"";
                        break;
                    case BatchName:
                        batchJoin =
                            "LEFT JOIN \"Batches\" ON \"History\".\"BatchID\"=\"Batches\".\"BatchID\"";
                        colName = "\"Batches\".\"BatchName\"";
                        break;
                    case HistoryID:
                        colName = "\"History\".\"HistoryID\"";
                        break;
                    case JobID:
                        colName = "\"History\".\"JobID\"";
                        break;
                    case ModemAlias:
                        modemJoin =
                            "LEFT JOIN \"Modems\" ON \"History\".\"ModemID\"=\"Modems\".\"ModemID\"";
                        colName = "\"Modems\".\"Alias\"";
                        break;
                    case ModemID:
                        colName = "\"History\".\"ModemID\"";
                        break;
                    case Result:
                        colName = "\"History\".\"Result\"";
                        break;
                    case Reverse:
                        reverse = !reverse;
                        continue; // Don't output anything
                    case Timestamp:
                        colName = "\"History\".\"Timestamp\"";
                        break;
                    case Type:
                        colName = "\"History\".\"Type\"";
                        break;
                    case UserID:
                        colName = "\"History\".\"UserID\"";
                        break;
                    case UserName:
                        userJoin =
                            "LEFT JOIN \"Users\" ON \"History\".\"UserID\"=\"Users\".\"UserID\"";
                        colName = "\"Users\".\"UserName\"";
                        break;
                }

                if (first) {
                    first = false;
                }
                else {
                    sortClause.append(", ");
                }

                sortClause.append(colName);

                if (reverse) {
                    sortClause.append(" DESC");
                    reverse = false;
                }
                else {
                    sortClause.append(" ASC");
                }
            }
        }
        else {
            // Sort by descending timestamp
            sortClause.append("ORDER BY \"History\".\"Timestamp\" DESC");
        }

        PreparedStatement s =
            con
                .prepareStatement("SELECT \"History\".\"HistoryID\" FROM \"History\" " +
                                  batchJoin +
                                  " " +
                                  modemJoin +
                                  " " +
                                  userJoin +
                                  " " +
                                  where.toString() +
                                  " " +
                                  sortClause +
                                  " " + limitClause);

        try {
            int position = 1;

            if (historyID != null) {
                s.setLong(position++, historyID);
            }

            if (jobID != null) {
                s.setLong(position++, jobID);
            }

            if (batchID != null) {
                if (batchID == -1) {
                    // Don't actually add anything here
                }
                else {
                    s.setInt(position++, batchID);
                }
            }

            if (modemID != null) {
                s.setInt(position++, modemID);
            }

            if (userID != null) {
                if (userID == -1) {
                    // Don't actually add anything here
                }
                else {
                    s.setInt(position++, userID);
                }
            }

            if (jobType != null) {
                s.setString(position++, jobType);
            }

            if (result != null) {
                s.setString(position++, result);
            }

            if (lowDate != null) {
                s.setTimestamp(position++, new Timestamp(lowDate.getTime()),
                    utcCalendar);
            }

            if (highDate != null) {
                s.setTimestamp(position++, new Timestamp(highDate.getTime()),
                    utcCalendar);
            }

            ResultSet rs = s.executeQuery();

            try {
                Collection<Long> entries = new LinkedList<Long>();

                while (rs.next()) {
                    long hID = rs.getLong(1);

                    entries.add(hID);
                }

                return entries;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void createDatabaseUser(Connection con, String userName,
                                      String password)
        throws DatabaseException, SQLException
    {

        if (userName.indexOf(';') != -1 || userName.indexOf('"') != -1 ||
            userName.indexOf('\'') != -1 || userName.indexOf('`') != -1) {

            throw new DatabaseException("Not a legal user name");
        }

        Statement s = con.createStatement();

        try {
            s.execute("CREATE ROLE \"" + userName + "\" WITH LOGIN PASSWORD '" +
                      password + "'");
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void createDatabase(Connection con, String dbName,
                                  String primaryUser, String secondaryUser)
        throws DatabaseException, SQLException
    {
        // Make sure there's nothing weird in the user name
        if (primaryUser.indexOf(';') != -1 || primaryUser.indexOf('"') != -1 ||
            primaryUser.indexOf('\'') != -1 || primaryUser.indexOf('`') != -1) {

            throw new DatabaseException("Not a legal primary user name");
        }

        // Set to false if there is no secondary user
        boolean doSecondary = true;
        if (secondaryUser == null) {
            secondaryUser = "PUBLIC";
        }
        else if (secondaryUser.equals(primaryUser)) {
            doSecondary = false;
        }
        else {
            // Check for bad characters
            if (secondaryUser.indexOf(';') != -1 ||
                secondaryUser.indexOf('"') != -1 ||
                secondaryUser.indexOf('\'') != -1 ||
                secondaryUser.indexOf('`') != -1) {

                throw new DatabaseException("Not a legal secondary user name");
            }

            // This way we don't need to worry about PUBLIC vs. secondaryUser
            secondaryUser = "\"" + secondaryUser + "\"";
        }

        // Or in the database name
        if (dbName.indexOf(';') != -1 || dbName.indexOf('"') != -1 ||
            dbName.indexOf('\'') != -1 || dbName.indexOf('`') != -1) {

            throw new DatabaseException("Not a legal database name");
        }

        Statement s = con.createStatement();

        try {
            // Create the database
            s.execute("CREATE DATABASE \"" + dbName +
                      "\" WITH TEMPLATE = template0 OWNER = \"" + primaryUser +
                      "\" ENCODING = 'UTF8'");

            // Set permissions on the database
            s.execute("GRANT ALL ON DATABASE \"" + dbName + "\" TO \"" +
                      primaryUser + "\"");
            s.execute("REVOKE ALL ON DATABASE \"" + dbName + "\" FROM PUBLIC");

            if (doSecondary) {
                s.execute("REVOKE ALL ON DATABASE \"" + dbName + "\" FROM " +
                          secondaryUser);
                s.execute("GRANT CONNECT, TEMPORARY ON DATABASE \"" + dbName +
                          "\" TO " + secondaryUser);
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void initializeNewDatabase(Connection con, String dbName,
                                         String primaryUser,
                                         String secondaryUser,
                                         LicenseKey licenseKey)
        throws DatabaseException, SQLException
    {

        // Make sure there's nothing weird in the user name
        if (primaryUser.indexOf(';') != -1 || primaryUser.indexOf('"') != -1 ||
            primaryUser.indexOf('\'') != -1 || primaryUser.indexOf('`') != -1) {

            throw new DatabaseException("Not a legal primary user name");
        }

        // Set to false if there is no secondary user
        boolean doSecondary = true;
        if (secondaryUser == null) {
            secondaryUser = "PUBLIC";
        }
        else if (secondaryUser.equals(primaryUser)) {
            doSecondary = false;
        }
        else {
            // Check for bad characters
            if (secondaryUser.indexOf(';') != -1 ||
                secondaryUser.indexOf('"') != -1 ||
                secondaryUser.indexOf('\'') != -1 ||
                secondaryUser.indexOf('`') != -1) {

                throw new DatabaseException("Not a legal secondary user name");
            }

            // This way we don't need to worry about PUBLIC vs. secondaryUser
            secondaryUser = "\"" + secondaryUser + "\"";
        }

        Statement s = con.createStatement();

        try {
            // Create the tables and set their ownership and permissions
            // Create the Batches table
            s.execute("CREATE TABLE \"Batches\" (" // Create table
                      + "\"BatchID\" integer NOT NULL, " // BatchID
                      + "\"UserID\" integer, " // UserID
                      + "\"BatchName\" text NOT NULL, " // Password
                      + "\"Deleted\" boolean DEFAULT FALSE NOT NULL, " // Deleted
                      + "PRIMARY KEY (\"BatchID\")" // Primary on UserID
                      + ")");
            s.execute("ALTER TABLE public.\"Batches\" OWNER TO \"" +
                      primaryUser + "\"");

            s.execute("REVOKE ALL ON TABLE \"Batches\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"Batches\" FROM " +
                          secondaryUser);
                s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                          "ON TABLE \"Batches\" TO " + secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"Batches\" FROM \"" + primaryUser +
                      "\"");
            s
                .execute("GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE \"Batches\" TO \"" +
                         primaryUser + "\"");

            // Create the capabilities table
            s.execute("CREATE TABLE \"Capabilities\" (" // Create table
                      + "\"UpdatedAt\" timestamp without time zone NOT NULL, " // UpdatedAt
                      + "\"Server\" character varying(253) NOT NULL, " // Server
                      + "\"Databases\" text NOT NULL, " // Databases
                      + "\"Jobs\" text NOT NULL, " // Jobs
                      + "\"Modems\" text NOT NULL, " // Modems
                      + "\"Extra\" text DEFAULT '<extra />'::text NOT NULL, " // Extra
                      + "PRIMARY KEY (\"Server\")" // Primary on Server
                      + ")");
            s.execute("ALTER TABLE public.\"Capabilities\" OWNER TO \"" +
                      primaryUser + "\"");

            s.execute("REVOKE ALL ON TABLE \"Capabilities\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"Capabilities\" FROM " +
                          secondaryUser);
                s.execute("GRANT SELECT ON TABLE \"Capabilities\" TO " +
                          secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"Capabilities\" FROM \"" +
                      primaryUser + "\"");
            s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                      "ON TABLE \"Capabilities\" TO \"" + primaryUser + "\"");

            // Create the Modems table
            s.execute("CREATE TABLE \"Modems\" (" // Create table
                      + "\"ModemID\" integer NOT NULL, " // ModemID
                      + "\"DeviceID\" bigint, " // DeviceID
                      + "\"Alias\" text, " // Alias
                      + "\"Model\" character varying(64), " // Model
                      + "\"FWVersion\" character varying(64), " // FWVersion
                      + "\"CfgVersion\" character varying(32)," // CfgVersion
                      + "\"IPAddress\" character varying(253), " // IPAddress
                      + "\"Port\" integer DEFAULT 5070 NOT NULL, " // Port
                      + "\"PhoneNumber\" character varying(64), " // PhoneNumber
                      + "\"Status\" text NOT NULL, " // Status
                      + "\"LockedBy\" character varying(253), " // LockedBy
                      + "\"LockedAt\" timestamp without time zone, " // LocketAt
                      + "PRIMARY KEY (\"ModemID\")" // Primary on ModemID
                      + ")");
            s.execute("ALTER TABLE public.\"Modems\" OWNER TO \"" +
                      primaryUser + "\"");

            // FUTURE: With the Delete job type, there should be no need for the
            // delete permission here
            s.execute("REVOKE ALL ON TABLE \"Modems\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"Modems\" FROM " +
                          secondaryUser);
                s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                          "ON TABLE \"Modems\" TO " + secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"Modems\" FROM \"" + primaryUser +
                      "\"");
            s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                      "ON TABLE \"Modems\" TO \"" + primaryUser + "\"");

            // Create the History table
            s
                .execute("CREATE TABLE \"History\" ("// Create table
                         + "\"HistoryID\" bigint NOT NULL, " // HistoryID
                         + "\"JobID\" bigint NOT NULL, " // JobID
                         + "\"BatchID\" integer, " // BatchID
                         + "\"ModemID\" integer NOT NULL, " // ModemID
                         + "\"UserID\" integer, " // UserID
                         + "\"Timestamp\" timestamp without time zone NOT NULL, " // Timestamp
                         + "\"Result\" character varying(32) NOT NULL, " // Result
                         + "\"Type\" character varying(32) NOT NULL, " // Type
                         + "\"Message\" text, " // Message
                         + "PRIMARY KEY (\"HistoryID\"), " // Primary on
                         // HistoryID
                         // Foreign on ModemID
                         + "FOREIGN KEY (\"ModemID\") REFERENCES \"Modems\" ON DELETE CASCADE" // Foreign
                         + ")");
            s.execute("ALTER TABLE public.\"History\" OWNER TO \"" +
                      primaryUser + "\"");

            s.execute("REVOKE ALL ON TABLE \"History\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"History\" FROM " +
                          secondaryUser);
                s.execute("GRANT SELECT ON TABLE \"History\" TO " +
                          secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"History\" FROM \"" + primaryUser +
                      "\"");
            s.execute("GRANT SELECT,INSERT,DELETE " +
                      "ON TABLE \"History\" TO \"" + primaryUser + "\"");

            // Create the Jobs table
            s
                .execute("CREATE TABLE \"Jobs\" (" // Create table
                         + "\"JobID\" bigint NOT NULL, " // JobID
                         + "\"BatchID\" integer, " // BatchID
                         + "\"ModemID\" integer NOT NULL, " // ModemID
                         + "\"UserID\" integer, " // UserID
                         + "\"Schedule\" timestamp without time zone NOT NULL, " // Schedule
                         + "\"Tries\" integer DEFAULT 0 NOT NULL, " // Tries
                         + "\"LockedBy\" character varying(253), " // LockedBy
                         + "\"LockedAt\" timestamp without time zone, " // LockedAt
                         + "\"Type\" character varying(32) NOT NULL, " // Type
                         + "\"Parameters\" text NOT NULL, " // Parameters
                         + "\"Automatic\" boolean NOT NULL, " // Automatic
                         + "PRIMARY KEY (\"JobID\"), " // Primary on JobID
                         // Foreign on ModemID
                         + "FOREIGN KEY (\"ModemID\") REFERENCES \"Modems\" ON DELETE CASCADE" // Foreign
                         + ")");
            s.execute("ALTER TABLE public.\"Jobs\" OWNER TO \"" + primaryUser +
                      "\"");

            // FUTURE: With the Delete job type, there should be no 'delete'
            // permission here
            s.execute("REVOKE ALL ON TABLE \"Jobs\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"Jobs\" FROM " + secondaryUser);
                s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                          "ON TABLE \"Jobs\" TO " + secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"Jobs\" FROM \"" + primaryUser +
                      "\"");
            s
                .execute("GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE \"Jobs\" TO \"" +
                         primaryUser + "\"");

            // Create the Log table
            s.execute("CREATE TABLE \"Logs\" (" // Create table
                      + "\"LogID\" bigint NOT NULL, " // LogID
                      + "\"Timestamp\" timestamp without time zone NOT NULL, " // Timestamp
                      + "\"UserID\" integer, " // UserID
                      + "\"Type\" character varying(16) NOT NULL, " // Type
                      + "\"Description\" text NOT NULL, " // Description
                      + "PRIMARY KEY (\"LogID\")" // Primary on LogID
                      + ")");
            s.execute("ALTER TABLE public.\"Logs\" OWNER TO \"" + primaryUser +
                      "\"");

            s.execute("REVOKE ALL ON TABLE \"Logs\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"Logs\" FROM " + secondaryUser);
                s.execute("GRANT SELECT,INSERT ON TABLE \"Logs\" TO " +
                          secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"Logs\" FROM \"" + primaryUser +
                      "\"");
            s.execute("GRANT SELECT,INSERT,DELETE ON TABLE \"Logs\" TO \"" +
                      primaryUser + "\"");

            // Create the Packages table
            s.execute("CREATE TABLE \"Packages\" (" // Create table
                      + "\"Type\" character varying(16) NOT NULL, " // Type
                      + "\"Version\" character varying(16) NOT NULL, " // Version
                      + "\"Date\" timestamp without time zone NOT NULL, " // Date
                      + "\"Info\" character varying(16) NOT NULL, " // Info
                      + "\"Checksum\" character(64) NOT NULL, " // Checksum
                      + "\"Data\" bytea NOT NULL, " // Data
                      + "PRIMARY KEY (\"Checksum\")" // Primary on Checksum
                      + ")");
            s.execute("ALTER TABLE public.\"Packages\" OWNER TO \"" +
                      primaryUser + "\"");

            // No need for Update permission here, since the columns all reflect
            // what is in the data itself
            // If we allowed updates, they could get out of sync
            s.execute("REVOKE ALL ON TABLE \"Packages\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"Packages\" FROM " +
                          secondaryUser);
                s
                    .execute("GRANT SELECT,INSERT,DELETE ON TABLE \"Packages\" TO " +
                             secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"Packages\" FROM \"" + primaryUser +
                      "\"");
            s.execute("GRANT SELECT,INSERT,DELETE " +
                      "ON TABLE \"Packages\" TO \"" + primaryUser + "\"");

            // Create the Scheduling table
            s
                .execute("CREATE TABLE \"Scheduling\" (" // Create table
                         + "\"ScheduleID\" integer NOT NULL, " // ScheduleID
                         + "\"ModemID\" integer NOT NULL, " // ModemID
                         + "\"State\" text NOT NULL, " // State
                         + "\"StartTime\" timestamp without time zone NOT NULL, " // StartTime
                         + "\"EndTime\" timestamp without time zone NOT NULL, " // EndTime
                         + "\"Relative\" boolean NOT NULL, " // Relative
                         // Primary on ScheduleID
                         + "PRIMARY KEY (\"ScheduleID\"), " // Primary
                         // Foreign on ModemID
                         + "FOREIGN KEY (\"ModemID\") REFERENCES \"Modems\" ON DELETE CASCADE" // Foreign
                         + ")");
            s.execute("ALTER TABLE public.\"Scheduling\" OWNER TO \"" +
                      primaryUser + "\"");

            s.execute("REVOKE ALL ON TABLE \"Scheduling\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"Scheduling\" FROM " +
                          secondaryUser);
                s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                          "ON TABLE \"Scheduling\" TO " + secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"Scheduling\" FROM \"" +
                      primaryUser + "\"");
            s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                      "ON TABLE \"Scheduling\" TO \"" + primaryUser + "\"");

            // Create the Settings table
            s.execute("CREATE TABLE \"Settings\" (" // Create table
                      + "\"SettingName\" character varying(64) NOT NULL, " // SettingName
                      + "\"SettingValue\" text, " // SettingValue
                      + "PRIMARY KEY (\"SettingName\")" // Primary on
                      // SettingName
                      + ")");
            s.execute("ALTER TABLE public.\"Settings\" OWNER TO \"" +
                      primaryUser + "\"");

            s.execute("REVOKE ALL ON TABLE \"Settings\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"Settings\" FROM " +
                          secondaryUser);
                s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                          "ON TABLE \"Settings\" TO " + secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"Settings\" FROM \"" + primaryUser +
                      "\"");
            s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                      "ON TABLE \"Settings\" TO \"" + primaryUser + "\"");

            // Create the Status History table
            s
                .execute("CREATE TABLE \"StatusHistory\" (" // Create table
                         + "\"ModemID\" integer NOT NULL, " // ModemID
                         + "\"Timestamp\" timestamp without time zone NOT NULL, " // Timestamp
                         + "\"PropertyName\" character varying(32) NOT NULL, " // PropertyName
                         + "\"PropertyVal\" text NOT NULL, " // PropertyVal
                         // Foreign on ModemID
                         + "FOREIGN KEY (\"ModemID\") REFERENCES \"Modems\" ON DELETE CASCADE" // Foreign
                         + ")");
            s.execute("ALTER TABLE public.\"StatusHistory\" OWNER TO \"" +
                      primaryUser + "\"");

            s.execute("REVOKE ALL ON TABLE \"StatusHistory\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"StatusHistory\" FROM " +
                          secondaryUser);
                s.execute("GRANT SELECT ON TABLE \"StatusHistory\" TO " +
                          secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"StatusHistory\" FROM \"" +
                      primaryUser + "\"");
            s.execute("GRANT SELECT,INSERT,DELETE " +
                      "ON TABLE \"StatusHistory\" TO \"" + primaryUser + "\"");

            // Create the Tags table
            s.execute("CREATE TABLE \"Tags\" (" // Create table
                      + "\"TagID\" integer NOT NULL, " // TagID
                      + "\"Position\" integer NOT NULL, " // Position
                      + "\"TagName\" character varying(64) NOT NULL, " // TagName
                      + "\"ConfigScript\" text, " // ConfigScript
                      + "\"PollingInterval\" integer DEFAULT 3600 NOT NULL, " // PollingInterval
                      + "PRIMARY KEY (\"TagID\")" // Primary on TagID
                      + ")");
            s.execute("ALTER TABLE public.\"Tags\" OWNER TO \"" + primaryUser +
                      "\"");

            s.execute("REVOKE ALL ON TABLE \"Tags\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"Tags\" FROM " + secondaryUser);
                s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                          "ON TABLE \"Tags\" TO " + secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"Tags\" FROM \"" + primaryUser +
                      "\"");
            s
                .execute("GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE \"Tags\" TO \"" +
                         primaryUser + "\"");

            // Create the TagMatch table
            s
                .execute("CREATE TABLE \"TagMatch\" (" // Create table
                         + "\"TagID\" integer NOT NULL, " // TagID
                         + "\"ModemID\" integer NOT NULL, " // ModemID
                         // Foreign on both
                         + "FOREIGN KEY (\"TagID\") REFERENCES \"Tags\" ON DELETE CASCADE, " // Foreign
                         + "FOREIGN KEY (\"ModemID\") REFERENCES \"Modems\" ON DELETE CASCADE" // Foreign
                         + ")");
            s.execute("ALTER TABLE public.\"TagMatch\" OWNER TO \"" +
                      primaryUser + "\"");

            s.execute("REVOKE ALL ON TABLE \"TagMatch\" FROM PUBLIC");
            if (doSecondary) {
                s.execute("REVOKE ALL ON TABLE \"TagMatch\" FROM " +
                          secondaryUser);
                s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                          "ON TABLE \"TagMatch\" TO " + secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"TagMatch\" FROM \"" + primaryUser +
                      "\"");
            s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                      "ON TABLE \"TagMatch\" TO \"" + primaryUser + "\"");

            // Create the Users table
            s
                .execute("CREATE TABLE \"Users\" (" // Create table
                         + "\"UserID\" integer NOT NULL, " // UserID
                         + "\"UserName\" character varying(32) NOT NULL, " // UserName
                         + "\"Password\" character(128) NOT NULL, " // Password
                         + "\"Type\" character varying(24) NOT NULL, " // Type
                         + "\"Preferences\" text DEFAULT '<prefs />'::text NOT NULL, " // Preferences
                         + "PRIMARY KEY (\"UserID\")" // Primary on UserID
                         + ")");
            s.execute("ALTER TABLE public.\"Users\" OWNER TO \"" + primaryUser +
                      "\"");

            s.execute("REVOKE ALL ON TABLE \"Users\" FROM PUBLIC");
            if (doSecondary) {
                s
                    .execute("REVOKE ALL ON TABLE \"Users\" FROM " +
                             secondaryUser);
                s.execute("GRANT SELECT,INSERT,DELETE,UPDATE " +
                          "ON TABLE \"Users\" TO " + secondaryUser);
            }

            s.execute("REVOKE ALL ON TABLE \"Users\" FROM \"" + primaryUser +
                      "\"");
            s
                .execute("GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE \"Users\" TO \"" +
                         primaryUser + "\"");

            // Create the sequences, add them to the tables, and set their
            // ownership
            // Since primaryUser owns these, we do not need to grant privileges
            // there

            // Create the BatchID sequence
            s.execute("CREATE SEQUENCE \"Batches_BatchID_seq\" INCREMENT BY 1 "
                      + "NO MAXVALUE NO MINVALUE CACHE 1");
            s.execute("ALTER TABLE \"Batches_BatchID_seq\" OWNER TO \"" +
                      primaryUser + "\"");
            s
                .execute("ALTER SEQUENCE \"Batches_BatchID_seq\" OWNED BY \"Batches\".\"BatchID\"");
            s
                .execute("ALTER TABLE \"Batches\" ALTER COLUMN \"BatchID\" "
                         + "SET DEFAULT nextval('\"Batches_BatchID_seq\"'::regclass)");

            if (doSecondary) {
                s
                    .execute("REVOKE ALL ON SEQUENCE \"Batches_BatchID_seq\" FROM " +
                             secondaryUser);
                s
                    .execute("GRANT SELECT,UPDATE,USAGE ON SEQUENCE \"Batches_BatchID_seq\" TO " +
                             secondaryUser);
            }

            // Create the HistoryID sequence
            s
                .execute("CREATE SEQUENCE \"History_HistoryID_seq\" INCREMENT BY 1 "
                         + "NO MAXVALUE NO MINVALUE CACHE 1");
            s.execute("ALTER TABLE \"History_HistoryID_seq\" OWNER TO \"" +
                      primaryUser + "\"");
            s
                .execute("ALTER SEQUENCE \"History_HistoryID_seq\" OWNED BY \"History\".\"HistoryID\"");
            s
                .execute("ALTER TABLE \"History\" ALTER COLUMN \"HistoryID\" "
                         + "SET DEFAULT nextval('\"History_HistoryID_seq\"'::regclass)");

            if (doSecondary) {
                s
                    .execute("REVOKE ALL ON SEQUENCE \"History_HistoryID_seq\" FROM " +
                             secondaryUser);
                s
                    .execute("GRANT SELECT,UPDATE,USAGE ON SEQUENCE \"History_HistoryID_seq\" TO " +
                             secondaryUser);
            }

            // Create the JobID sequence
            s.execute("CREATE SEQUENCE \"Jobs_JobID_seq\" INCREMENT BY 1 "
                      + "NO MAXVALUE NO MINVALUE CACHE 1");
            s.execute("ALTER TABLE \"Jobs_JobID_seq\" OWNER TO \"" +
                      primaryUser + "\"");
            s
                .execute("ALTER SEQUENCE \"Jobs_JobID_seq\" OWNED BY \"Jobs\".\"JobID\"");
            s.execute("ALTER TABLE \"Jobs\" ALTER COLUMN \"JobID\" "
                      + "SET DEFAULT nextval('\"Jobs_JobID_seq\"'::regclass)");

            if (doSecondary) {
                s.execute("REVOKE ALL ON SEQUENCE \"Jobs_JobID_seq\" FROM " +
                          secondaryUser);
                s
                    .execute("GRANT SELECT,UPDATE,USAGE ON SEQUENCE \"Jobs_JobID_seq\" TO " +
                             secondaryUser);
            }

            // Create the LogID sequence
            s.execute("CREATE SEQUENCE \"Logs_LogID_seq\" INCREMENT BY 1 "
                      + "NO MAXVALUE NO MINVALUE CACHE 1");
            s.execute("ALTER TABLE \"Logs_LogID_seq\" OWNER TO \"" +
                      primaryUser + "\"");
            s
                .execute("ALTER SEQUENCE \"Logs_LogID_seq\" OWNED BY \"Logs\".\"LogID\"");
            s.execute("ALTER TABLE \"Logs\" ALTER COLUMN \"LogID\" "
                      + "SET DEFAULT nextval('\"Logs_LogID_seq\"'::regclass)");

            if (doSecondary) {
                s.execute("REVOKE ALL ON SEQUENCE \"Logs_LogID_seq\" FROM " +
                          secondaryUser);
                s
                    .execute("GRANT SELECT,UPDATE,USAGE ON SEQUENCE \"Logs_LogID_seq\" TO " +
                             secondaryUser);
            }

            // Create the ModemID sequence
            s.execute("CREATE SEQUENCE \"Modems_ModemID_seq\" INCREMENT BY 1 "
                      + "NO MAXVALUE NO MINVALUE CACHE 1");
            s.execute("ALTER TABLE \"Modems_ModemID_seq\" OWNER TO \"" +
                      primaryUser + "\"");
            s
                .execute("ALTER SEQUENCE \"Modems_ModemID_seq\" OWNED BY \"Modems\".\"ModemID\"");
            s
                .execute("ALTER TABLE \"Modems\" ALTER COLUMN \"ModemID\" "
                         + "SET DEFAULT nextval('\"Modems_ModemID_seq\"'::regclass)");

            if (doSecondary) {
                s
                    .execute("REVOKE ALL ON SEQUENCE \"Modems_ModemID_seq\" FROM " +
                             secondaryUser);
                s
                    .execute("GRANT SELECT,UPDATE,USAGE ON SEQUENCE \"Modems_ModemID_seq\" TO " +
                             secondaryUser);
            }

            // Create the ScheduleID sequence
            s
                .execute("CREATE SEQUENCE \"Scheduling_ScheduleID_seq\" INCREMENT BY 1 "
                         + "NO MAXVALUE NO MINVALUE CACHE 1");
            s.execute("ALTER TABLE \"Scheduling_ScheduleID_seq\" OWNER TO \"" +
                      primaryUser + "\"");
            s.execute("ALTER SEQUENCE \"Scheduling_ScheduleID_seq\" "
                      + "OWNED BY \"Scheduling\".\"ScheduleID\"");
            s
                .execute("ALTER TABLE \"Scheduling\" ALTER COLUMN \"ScheduleID\" "
                         + "SET DEFAULT nextval('\"Scheduling_ScheduleID_seq\"'::regclass)");

            if (doSecondary) {
                s
                    .execute("REVOKE ALL ON SEQUENCE \"Scheduling_ScheduleID_seq\" FROM " +
                             secondaryUser);
                s
                    .execute("GRANT SELECT,UPDATE,USAGE ON SEQUENCE \"Scheduling_ScheduleID_seq\" TO " +
                             secondaryUser);
            }

            // Create the TagID sequence
            s.execute("CREATE SEQUENCE \"Tags_TagID_seq\" INCREMENT BY 1 "
                      + "NO MAXVALUE NO MINVALUE CACHE 1");
            s.execute("ALTER TABLE \"Tags_TagID_seq\" OWNER TO \"" +
                      primaryUser + "\"");
            s
                .execute("ALTER SEQUENCE \"Tags_TagID_seq\" OWNED BY \"Tags\".\"TagID\"");
            s.execute("ALTER TABLE \"Tags\" ALTER COLUMN \"TagID\" "
                      + "SET DEFAULT nextval('\"Tags_TagID_seq\"'::regclass)");

            if (doSecondary) {
                s.execute("REVOKE ALL ON SEQUENCE \"Tags_TagID_seq\" FROM " +
                          secondaryUser);
                s
                    .execute("GRANT SELECT,UPDATE,USAGE ON SEQUENCE \"Tags_TagID_seq\" TO " +
                             secondaryUser);
            }

            // Create the UserID sequence
            s.execute("CREATE SEQUENCE \"Users_UserID_seq\" INCREMENT BY 1 "
                      + "NO MAXVALUE NO MINVALUE CACHE 1");
            s.execute("ALTER TABLE \"Users_UserID_seq\" OWNER TO \"" +
                      primaryUser + "\"");
            s
                .execute("ALTER SEQUENCE \"Users_UserID_seq\" OWNED BY \"Users\".\"UserID\"");
            s
                .execute("ALTER TABLE \"Users\" ALTER COLUMN \"UserID\" "
                         + "SET DEFAULT nextval('\"Users_UserID_seq\"'::regclass)");

            if (doSecondary) {
                s.execute("REVOKE ALL ON SEQUENCE \"Users_UserID_seq\" FROM " +
                          secondaryUser);
                s
                    .execute("GRANT SELECT,UPDATE,USAGE ON SEQUENCE \"Users_UserID_seq\" TO " +
                             secondaryUser);
            }

            // Create the indices
            // Batches table
            s
                .execute("CREATE INDEX \"Batches_UserID_idx\" ON \"Batches\" USING btree (\"UserID\")");

            // Capabilities table
            s
                .execute("CREATE INDEX \"Capabilities_UpdatedAt_idx\" ON \"Capabilities\" USING btree (\"UpdatedAt\")");

            // History table
            s
                .execute("CREATE INDEX \"History_JobID_idx\" ON \"History\" USING btree (\"JobID\")");
            s
                .execute("CREATE INDEX \"History_BatchID_idx\" ON \"History\" USING btree (\"BatchID\")");
            s
                .execute("CREATE INDEX \"History_ModemID_idx\" ON \"History\" USING btree (\"ModemID\")");
            s
                .execute("CREATE INDEX \"History_UserID_idx\" ON \"History\" USING btree (\"UserID\")");
            s
                .execute("CREATE INDEX \"History_Timestamp_idx\" ON \"History\" USING btree (\"Timestamp\")");
            s
                .execute("CREATE INDEX \"History_Result_idx\" ON \"History\" USING btree (\"Result\")");
            s
                .execute("CREATE INDEX \"History_Type_idx\" ON \"History\" USING btree (\"Type\")");

            // Jobs table
            s
                .execute("CREATE INDEX \"Jobs_BatchID_idx\" ON \"Jobs\" USING btree (\"BatchID\")");
            s
                .execute("CREATE INDEX \"Jobs_ModemID_idx\" ON \"Jobs\" USING btree (\"ModemID\")");
            s
                .execute("CREATE INDEX \"Jobs_UserID_idx\" ON \"Jobs\" USING btree (\"UserID\")");
            s
                .execute("CREATE INDEX \"Jobs_Schedule_idx\" ON \"Jobs\" USING btree (\"Schedule\")");
            s
                .execute("CREATE INDEX \"Jobs_Tries_idx\" ON \"Jobs\" USING btree (\"Tries\")");
            s
                .execute("CREATE INDEX \"Jobs_LockedBy_idx\" ON \"Jobs\" USING btree (\"LockedBy\")");
            s
                .execute("CREATE INDEX \"Jobs_LockedAt_idx\" ON \"Jobs\" USING btree (\"LockedAt\")");
            s
                .execute("CREATE INDEX \"Jobs_Type_idx\" ON \"Jobs\" USING btree (\"Type\")");
            s
                .execute("CREATE INDEX \"Jobs_Automatic_idx\" ON \"Jobs\" USING btree (\"Automatic\")");

            // Log table
            s
                .execute("CREATE INDEX \"Log_Timestamp_idx\" ON \"Logs\" USING btree (\"Timestamp\")");
            s
                .execute("CREATE INDEX \"Log_UserID_idx\" ON \"Logs\" USING btree (\"UserID\")");
            s
                .execute("CREATE INDEX \"Log_Type_idx\" ON \"Logs\" USING btree (\"Type\")");

            // Modems table
            s
                .execute("CREATE INDEX \"Modems_Model_idx\" ON \"Modems\" USING btree (\"Model\")");
            s
                .execute("CREATE INDEX \"Modems_FWVersion_idx\" ON \"Modems\" USING btree (\"FWVersion\")");
            s
                .execute("CREATE INDEX \"Modems_CfgVersion_idx\" ON \"Modems\" USING btree (\"CfgVersion\")");
            s
                .execute("CREATE INDEX \"Modems_LockedBy_idx\" ON \"Modems\" USING btree (\"LockedBy\")");
            s
                .execute("CREATE INDEX \"Modems_LockedAt_idx\" ON \"Modems\" USING btree (\"LockedAt\")");

            // Schedule table
            s
                .execute("CREATE INDEX \"Scheduling_ModemID_idx\" ON \"Scheduling\" USING btree (\"ModemID\")");
            s
                .execute("CREATE INDEX \"Scheduling_Relative_idx\" ON \"Scheduling\" USING btree (\"Relative\")");

            // StatusHistory table
            s
                .execute("CREATE INDEX \"StatusHistory_ModemID_idx\" ON \"StatusHistory\" USING btree (\"ModemID\")");
            s
                .execute("CREATE INDEX \"StatusHistory_Timestamp_idx\" ON \"StatusHistory\" USING btree (\"Timestamp\")");
            s
                .execute("CREATE INDEX \"StatusHistory_PropertyName_idx\" ON \"StatusHistory\" USING btree (\"PropertyName\")");

            // TagMatch table
            s
                .execute("CREATE INDEX \"TagMatch_TagID_idx\" ON \"TagMatch\" USING btree (\"TagID\")");
            s
                .execute("CREATE INDEX \"TagMatch_ModemID_idx\" ON \"TagMatch\" USING btree (\"ModemID\")");

            // Users table
            s
                .execute("CREATE INDEX \"Users_Type_idx\" ON \"Users\" USING btree (\"Type\")");

            // Create the unique indices
            s
                .execute("CREATE UNIQUE INDEX \"Modems_DeviceID_udx\" ON \"Modems\" (\"DeviceID\")");

            s
                .execute("CREATE UNIQUE INDEX \"Tags_Position_udx\" ON \"Tags\" (\"Position\")");

            s
                .execute("CREATE UNIQUE INDEX \"Tags_TagName_udx\" ON \"Tags\" (\"TagName\")");

            s
                .execute("CREATE UNIQUE INDEX \"Users_UserName_udx\" ON \"Users\" (\"UserName\")");
        }
        finally {
            s.close();
        }

        // Populate the Settings table
        for (Setting setting : Setting.values()) {
            if (setting == Setting.DatabaseVersion) {
                // Make sure the correct database format version gets written
                updateSetting(con, setting.properName, "" + EXPECTED_VERSION);
            }
            else if (setting == Setting.LicenseKey) {
                // This is the license key, make sure we write the correct one
                updateSetting(con, setting.properName, licenseKey.getLicense()
                    .toString(false));
            }
            else {
                updateSetting(con, setting.properName, setting.defaultValue);
            }
        }
    }

    @Override
    protected void upgradeDatabase(Connection con)
        throws SQLException
    {
        // Read the current version out
        Map<String, String> values =
            getSettings(con, Utils.makeArrayList(new String[] {
                Setting.DatabaseVersion.properName
            }));
        if (!values.containsKey(Setting.DatabaseVersion.properName)) {
            throw new SQLException("Database does not contain the setting " +
                                   Setting.DatabaseVersion.properName +
                                   ", which is required for an upgrade");
        }

        Settings settings = new Settings(values);
        int currentVersion;
        try {
            currentVersion =
                settings.getIntValue(Setting.DatabaseVersion.properName);
        }
        catch (BadSettingException bse) {
            throw new SQLException(
                "Unable to determine current database version: " +
                    bse.getMessage(), bse);
        }

        switch (currentVersion) {
            case 1:
                upgradeDatabase1(con);
                // Fall through
            case 2:
                // Current version
                break;
            default:
                throw new SQLException(
                    "No known upgrade path exists from database version " +
                        currentVersion + " to " + EXPECTED_VERSION);
        }
    }

    /**
     * Upgrade a version 1 database to a version 2 database.
     * 
     * @param con The connection through which to communicate.
     * @throws SQLException If there was a problem.
     */
    private void upgradeDatabase1(Connection con)
        throws SQLException
    {
        Statement s = con.createStatement();
        try {
            // Begin a transaction (don't want either the schema or the version
            // number altered without the other)
            s.execute("START TRANSACTION ISOLATION LEVEL SERIALIZABLE");

            // Delete the (IPAddress, Port) unique index
            s.execute("DROP INDEX \"Modems_IPAddress_Port_udx\"");

            // Add the new "ClearDuplicateIPs" setting
            s
                .execute("INSERT INTO \"Settings\" (\"SettingName\", \"SettingValue\") " +
                         "VALUES ('" +
                         Setting.ClearDuplicateIPs.properName +
                         "', '" + Setting.ClearDuplicateIPs.defaultValue + "')");

            // Update the database version
            // I would prefer to use a prepared statement, but that is not
            // possible with the transaction
            // Luckily, Setting.DatabaseVersion.properName is a compiled-in
            // constant, not a user-specified string
            s.execute("UPDATE \"Settings\" SET \"SettingValue\"='2'" +
                      " WHERE \"SettingName\"='" +
                      Setting.DatabaseVersion.properName + "'");

            s.execute("COMMIT");
        }
        catch (SQLException sqle) {
            s.execute("ROLLBACK");
            throw new SQLException("Upgrade failed, no changes made: " +
                                   sqle.getMessage(), sqle);
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Collection<Long> getMatchingLogs(Connection con, Long logID,
                                               java.util.Date lowDate,
                                               java.util.Date highDate,
                                               Integer userID, String logType,
                                               int start, int limit,
                                               List<LogColumn> sortOn,
                                               long[] matchCount)
        throws DatabaseException, SQLException
    {

        StringBuilder where = new StringBuilder();
        where.append("WHERE TRUE");

        if (logID != null) {
            where.append(" AND \"Logs\".\"LogID\"=?");
        }

        if (lowDate != null) {
            where.append(" AND \"Logs\".\"Timestamp\">=?");
        }

        if (highDate != null) {
            where.append(" AND \"Logs\".\"Timestamp\"<=?");
        }

        if (userID != null) {
            if (userID == -1) {
                where.append(" AND \"Logs\".\"UserID\" IS NULL");
            }
            else {
                where.append(" AND \"Logs\".\"UserID\"=?");
            }
        }

        if (logType != null) {
            where.append(" AND \"Logs\".\"Type\"=?");
        }

        if (matchCount != null && matchCount.length > 0) {
            PreparedStatement s =
                con.prepareStatement("SELECT COUNT(*) FROM \"Logs\" " +
                                     where.toString());

            try {
                int position = 1;

                if (logID != null) {
                    s.setLong(position++, logID);
                }

                if (lowDate != null) {
                    s.setTimestamp(position++,
                        new Timestamp(lowDate.getTime()), utcCalendar);
                }

                if (highDate != null) {
                    s.setTimestamp(position++,
                        new Timestamp(highDate.getTime()), utcCalendar);
                }

                if (userID != null) {
                    if (userID == -1) {
                        // Don't actually add anything here
                    }
                    else {
                        s.setInt(position++, userID);
                    }
                }

                if (logType != null) {
                    s.setString(position++, logType);
                }

                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        matchCount[0] = rs.getLong(1);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }
        }

        String limitClause = "";
        if (limit > -1) {
            limitClause = String.format("LIMIT %d OFFSET %d", limit, start);
        }

        String userJoin = "";

        StringBuilder sortClause = new StringBuilder();
        if (sortOn != null && sortOn.size() > 0) {
            sortClause.append("ORDER BY ");

            boolean first = true;
            boolean reverse = false;
            for (LogColumn col : sortOn) {
                String colName = null;
                switch (col) {
                    case LogID:
                        colName = "\"Logs\".\"LogID\"";
                        break;
                    case Reverse:
                        reverse = !reverse;
                        continue; // Don't output anything here
                    case Timestamp:
                        colName = "\"Logs\".\"Timestamp\"";
                        break;
                    case Type:
                        colName = "\"Logs\".\"Type\"";
                        break;
                    case UserID:
                        colName = "\"Logs\".\"UserID\"";
                        break;
                    case UserName:
                        userJoin =
                            "LEFT JOIN \"Users\" ON \"Logs\".\"UserID\"=\"Users\".\"UserID\"";
                        colName = "\"Users\".\"UserName\"";
                        break;
                }

                if (first) {
                    first = false;
                }
                else {
                    sortClause.append(", ");
                }

                sortClause.append(colName);

                if (reverse) {
                    sortClause.append(" DESC");
                    reverse = false;
                }
                else {
                    sortClause.append(" ASC");
                }
            }
        }
        else {
            // Sort by descending timestamp
            sortClause.append("ORDER BY \"Logs\".\"Timestamp\" DESC");
        }

        PreparedStatement s =
            con.prepareStatement("SELECT \"Logs\".\"LogID\" FROM \"Logs\" " +
                                 userJoin + " " + where.toString() + " " +
                                 sortClause + " " + limitClause);

        try {
            int position = 1;

            if (logID != null) {
                s.setLong(position++, logID);
            }

            if (lowDate != null) {
                s.setTimestamp(position++, new Timestamp(lowDate.getTime()),
                    utcCalendar);
            }

            if (highDate != null) {
                s.setTimestamp(position++, new Timestamp(highDate.getTime()),
                    utcCalendar);
            }

            if (userID != null) {
                if (userID == -1) {
                    // Don't actually add anything here
                }
                else {
                    s.setInt(position++, userID);
                }
            }

            if (logType != null) {
                s.setString(position++, logType);
            }

            ResultSet rs = s.executeQuery();

            try {
                Collection<Long> entries = new LinkedList<Long>();

                while (rs.next()) {
                    entries.add(rs.getLong(1));
                }

                return entries;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void purgeHistory(Connection con, java.util.Date cutoff)
        throws DatabaseException, SQLException
    {

        PreparedStatement s =
            con
                .prepareStatement("DELETE FROM \"History\" WHERE \"Timestamp\"<?");

        try {
            s.setTimestamp(1, new Timestamp(cutoff.getTime()), utcCalendar);

            s.executeUpdate();

            logger.debug(String.format(
                "Purged history entries older than %1$tF %1$tT", cutoff));
        }
        finally {
            s.close();
        }

        s =
            con
                .prepareStatement("DELETE FROM \"StatusHistory\" WHERE \"Timestamp\"<?");

        try {
            s.setTimestamp(1, new Timestamp(cutoff.getTime()), utcCalendar);

            s.executeUpdate();

            logger
                .debug(String.format(
                    "Purged status history entries older than %1$tF %1$tT",
                    cutoff));
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void createBatch(Connection con, JobBatch batch)
        throws DatabaseException, SQLException
    {

        // Unfortunately, RETURN_GENERATED_KEYS doesn't work with PostgreSQL
        // Luckily, it seems to hold onto this value per-connection, so we don't
        // need to enter transaction mode to update and read it

        PreparedStatement s =
            con.prepareStatement("INSERT INTO \"Batches\" "
                                 + "(\"UserID\", \"BatchName\") "
                                 + "VALUES (?, ?)");

        try {
            if (batch.userID == null) {
                s.setNull(1, Types.INTEGER);
            }
            else {
                s.setInt(1, batch.userID);
            }

            s.setString(2, batch.name);

            s.executeUpdate();
        }
        finally {
            s.close();
        }

        // Get the Batch ID that the new job just took
        s = con.prepareStatement("SELECT currval('\"Batches_BatchID_seq\"')");

        try {
            ResultSet rs = s.executeQuery();

            try {
                while (rs.next()) {
                    batch.batchID = rs.getInt(1);

                    logger.debug(String.format("Created new batch %d",
                        batch.batchID));

                    return;
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void deleteBatch(Connection con, int batchID)
        throws DatabaseException, SQLException
    {
        // Mark the batch as deleted
        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Batches\" SET \"Deleted\"=? WHERE \"BatchID\"=?");

        try {
            s.setBoolean(1, true);
            s.setInt(2, batchID);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Map<Integer, JobBatch> getBatches(Connection con,
                                                Collection<Integer> batchIDs)
        throws DatabaseException, SQLException
    {

        StringBuilder whereClause = new StringBuilder("WHERE FALSE");
        for (int id : batchIDs) {
            whereClause.append(" OR \"BatchID\"=?");
        }

        // Select the actual batch
        PreparedStatement s =
            con
                .prepareStatement("SELECT \"BatchID\", \"UserID\", " +
                                  "\"BatchName\", \"Deleted\", " +

                                  "(SELECT COUNT(*) FROM \"Jobs\" " +
                                  "  WHERE \"Jobs\".\"BatchID\"=\"Batches\".\"BatchID\")" +

                                  "FROM \"Batches\" " + whereClause.toString());

        try {
            int index = 1;
            for (int batchID : batchIDs) {
                s.setInt(index++, batchID);
            }

            ResultSet rs = s.executeQuery();

            try {
                Map<Integer, JobBatch> batches =
                    new HashMap<Integer, JobBatch>();

                while (rs.next()) {
                    int batchID = rs.getInt(1);

                    // Allow null userID
                    Integer userID = rs.getInt(2);
                    if (rs.wasNull()) {
                        userID = null;
                    }

                    String name = rs.getString(3);
                    boolean deleted = rs.getBoolean(4);
                    int jobCount = rs.getInt(5);

                    JobBatch batch =
                        new JobBatch(batchID, userID, name, deleted, jobCount);

                    batches.put(batchID, batch);
                }

                return batches;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Collection<Integer> getMatchingBatches(Connection con,
                                                     Integer batchID,
                                                     Integer userID,
                                                     String name,
                                                     Boolean deleted,
                                                     int start, int limit,
                                                     List<BatchColumn> sortOn,
                                                     long[] matchCount)
        throws DatabaseException, SQLException
    {

        StringBuilder where = new StringBuilder();
        where.append("WHERE TRUE");

        if (batchID != null) {
            where.append(" AND \"Batches\".\"BatchID\"=?");
        }

        if (userID != null) {
            if (userID == -1) {
                where.append(" AND \"Batches\".\"UserID\" IS NULL");
            }
            else {
                where.append(" AND \"Batches\".\"UserID\"=?");
            }
        }

        if (name != null) {
            where.append(" AND \"Batches\".\"BatchName\"=?");
        }

        if (deleted != null) {
            where.append(" AND \"Batches\".\"Deleted\"=?");
        }

        if (matchCount != null && matchCount.length > 0) {
            PreparedStatement s =
                con.prepareStatement("SELECT COUNT(*) FROM \"Batches\" " +
                                     where.toString());

            try {
                int position = 1;

                if (batchID != null) {
                    s.setInt(position++, batchID);
                }

                if (userID != null) {
                    if (userID == -1) {
                        // Do nothing in this case
                    }
                    else {
                        s.setInt(position++, userID);
                    }
                }

                if (name != null) {
                    s.setString(position++, name);
                }

                if (deleted != null) {
                    s.setBoolean(position++, deleted);
                }

                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        matchCount[0] = rs.getLong(1);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }
        }

        String limitClause = "";
        if (limit > -1) {
            limitClause = String.format("LIMIT %d OFFSET %d", limit, start);
        }

        String join = "";

        StringBuilder sortClause = new StringBuilder();
        if (sortOn != null && sortOn.size() > 0) {
            sortClause.append("ORDER BY ");

            boolean first = true;
            boolean reverse = false;
            for (BatchColumn col : sortOn) {
                String colName = null;
                switch (col) {
                    case BatchID:
                        colName = "\"Batches\".\"BatchID\"";
                        break;
                    case BatchName:
                        colName = "\"Batches\".\"BatchName\"";
                        break;
                    case Deleted:
                        colName = "\"Batches\".\"Deleted\"";
                        break;
                    case JobCount:
                        colName =
                            "(SELECT COUNT(*) FROM \"Jobs\" WHERE \"Jobs\".\"BatchID\"=\"Batches\".\"BatchID\")";
                        break;
                    case Reverse:
                        reverse = !reverse;
                        continue; // Don't output anything for this one
                    case UserID:
                        colName = "\"Batches\".\"UserID\"";
                        break;
                    case UserName:
                        join =
                            "LEFT JOIN \"Users\" ON \"Batches\".\"UserID\"=\"Users\".\"UserID\"";
                        colName = "\"Users\".\"UserName\"";
                        break;
                }

                if (first) {
                    first = false;
                }
                else {
                    sortClause.append(", ");
                }

                sortClause.append(colName);

                if (reverse) {
                    sortClause.append(" DESC");
                    reverse = false;
                }
                else {
                    sortClause.append(" ASC");
                }
            }
        }
        else {
            // Default sort order
            sortClause.append("ORDER BY \"Batches\".\"BatchName\" ASC");
        }

        PreparedStatement s =
            con
                .prepareStatement("SELECT \"Batches\".\"BatchID\" FROM \"Batches\" " +
                                  join +
                                  " " +
                                  where.toString() +
                                  " " +
                                  sortClause + " " + limitClause);

        try {
            int position = 1;

            if (batchID != null) {
                s.setInt(position++, batchID);
            }

            if (userID != null) {
                if (userID == -1) {
                    // Do nothing in this case
                }
                else {
                    s.setInt(position++, userID);
                }
            }

            if (name != null) {
                s.setString(position++, name);
            }

            if (deleted != null) {
                s.setBoolean(position++, deleted);
            }

            ResultSet rs = s.executeQuery();

            try {
                List<Integer> batches = new LinkedList<Integer>();

                while (rs.next()) {
                    int bid = rs.getInt(1);

                    batches.add(bid);
                }

                return batches;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void updateBatch(Connection con, JobBatch batch)
        throws DatabaseException, SQLException
    {

        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Batches\" SET \"UserID\"=?, \"BatchName\"=?, \"Deleted\"=? WHERE \"BatchID\"=?");

        try {
            if (batch.userID == null) {
                s.setNull(1, Types.INTEGER);
            }
            else {
                s.setInt(1, batch.userID);
            }

            s.setString(2, batch.name);
            s.setBoolean(3, batch.deleted);
            s.setInt(4, batch.batchID);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void purgeStaleLocks(Connection con, java.util.Date cutoff)
        throws DatabaseException, SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Jobs\" SET \"LockedBy\"=NULL, \"LockedAt\"=NULL "
                                  + "WHERE \"LockedAt\"<?");

        try {
            s.setTimestamp(1, new Timestamp(cutoff.getTime()), utcCalendar);

            s.executeUpdate();
        }
        finally {
            s.close();
        }

        s =
            con
                .prepareStatement("UPDATE \"Modems\" SET \"LockedBy\"=NULL, \"LockedAt\"=NULL "
                                  + "WHERE \"LockedAt\"<?");

        try {
            s.setTimestamp(1, new Timestamp(cutoff.getTime()), utcCalendar);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Map<Long, HistoryEntry> getHistoryEntries(
                                                        Connection con,
                                                        Collection<Long> historyIDs)
        throws DatabaseException, SQLException
    {

        StringBuilder whereClause = new StringBuilder("WHERE FALSE");
        for (long id : historyIDs) {
            whereClause.append(" OR \"HistoryID\"=?");
        }

        PreparedStatement s =
            con
                .prepareStatement("SELECT \"HistoryID\", \"JobID\", \"BatchID\", " +
                                  "\"ModemID\", \"UserID\", \"Timestamp\", " +
                                  "\"Result\", \"Type\", \"Message\" " +
                                  "FROM \"History\" " + whereClause.toString());

        try {
            int index = 1;
            for (long historyID : historyIDs) {
                s.setLong(index++, historyID);
            }

            ResultSet rs = s.executeQuery();

            try {
                Map<Long, HistoryEntry> entries =
                    new HashMap<Long, HistoryEntry>();

                while (rs.next()) {
                    long historyID = rs.getLong(1);
                    long jobID = rs.getLong(2);

                    // Allow a null batch
                    Integer batchID = rs.getInt(3);
                    if (rs.wasNull()) {
                        batchID = null;
                    }

                    int modemID = rs.getInt(4);

                    // Allow a null user
                    Integer userID = rs.getInt(5);
                    if (rs.wasNull()) {
                        userID = null;
                    }

                    Timestamp timestamp = rs.getTimestamp(6, utcCalendar);
                    String result = rs.getString(7);
                    String type = rs.getString(8);
                    String message = rs.getString(9);

                    HistoryEntry entry =
                        new HistoryEntry(historyID, jobID, batchID, modemID,
                            userID, timestamp, result, type, message);

                    entries.put(historyID, entry);
                }

                return entries;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Map<Long, LogEntry> getLogEntries(Connection con,
                                                Collection<Long> logIDs)
        throws DatabaseException, SQLException
    {

        StringBuilder whereClause = new StringBuilder("WHERE FALSE");
        for (long id : logIDs) {
            whereClause.append(" OR \"LogID\"=?");
        }

        PreparedStatement s =
            con
                .prepareStatement("SELECT \"LogID\", \"Timestamp\", \"UserID\", " +
                                  "\"Type\", \"Description\" " +
                                  "FROM \"Logs\" " + whereClause.toString());

        try {
            int index = 1;
            for (long logID : logIDs) {
                s.setLong(index++, logID);
            }

            ResultSet rs = s.executeQuery();

            try {
                Map<Long, LogEntry> entries = new HashMap<Long, LogEntry>();

                while (rs.next()) {
                    long logID = rs.getLong(1);
                    Timestamp timestamp = rs.getTimestamp(2, utcCalendar);

                    // Allow null userID
                    Integer userID = rs.getInt(3);
                    if (rs.wasNull()) {
                        userID = null;
                    }

                    String type = rs.getString(4);
                    String description = rs.getString(5);

                    LogEntry entry =
                        new LogEntry(logID, timestamp, userID, type,
                            description);

                    entries.put(logID, entry);
                }

                return entries;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void createUser(Connection con, User user)
        throws DatabaseException, SQLException
    {
        // Unfortunately, PostgreSQL does not support RETURN_GENERATED_KEYS
        // However, it holds onto these values per-connection, so no need to
        // enter transaction mode

        PreparedStatement s =
            con
                .prepareStatement("INSERT INTO \"Users\" "
                                  + "(\"UserName\", \"Password\", \"Type\", \"Preferences\") "
                                  + "VALUES (?, ?, ?, ?)");

        try {
            s.setString(1, user.name);
            s.setString(2, user.hashedPassword);
            s.setString(3, user.type.toString());
            s.setString(4, user.preferences.toString(false));

            s.executeUpdate();
        }
        finally {
            s.close();
        }

        s = con.prepareStatement("SELECT currval('\"Users_UserID_seq\"')");

        try {
            ResultSet rs = s.executeQuery();

            try {
                while (rs.next()) {
                    user.userID = rs.getInt(1);

                    logger.debug(String.format("Created new user %d",
                        user.userID));

                    return;
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Collection<Integer> getMatchingUsers(Connection con,
                                                   Integer userID, String name,
                                                   UserType type, int start,
                                                   int limit,
                                                   List<UserColumn> sortOn,
                                                   long[] matchCount)
        throws DatabaseException, SQLException
    {

        StringBuilder where = new StringBuilder();
        where.append("WHERE TRUE");

        if (userID != null) {
            where.append(" AND \"Users\".\"UserID\"=?");
        }

        if (name != null) {
            where.append(" AND \"Users\".\"UserName\"=?");
        }

        if (type != null) {
            where.append(" AND \"Users\".\"Type\"=?");
        }

        if (matchCount != null && matchCount.length > 0) {
            PreparedStatement s =
                con.prepareStatement("SELECT COUNT(*) FROM \"Users\" " +
                                     where.toString());

            try {
                int position = 1;

                if (userID != null) {
                    s.setInt(position++, userID);
                }

                if (name != null) {
                    s.setString(position++, name);
                }

                if (type != null) {
                    s.setString(position++, type.toString());
                }

                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        matchCount[0] = rs.getLong(1);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }
        }

        String limitClause = "";
        if (limit > -1) {
            limitClause = String.format("LIMIT %d OFFSET %d", limit, start);
        }

        StringBuilder sortClause = new StringBuilder();
        if (sortOn != null && sortOn.size() > 0) {
            sortClause.append("ORDER BY ");

            boolean first = true;
            boolean reverse = false;
            for (UserColumn col : sortOn) {
                String colName = null;
                switch (col) {
                    case Reverse:
                        reverse = !reverse;
                        continue;
                    case Type:
                        colName = "\"Users\".\"Type\"";
                        break;
                    case UserID:
                        colName = "\"Users\".\"UserID\"";
                        break;
                    case UserName:
                        colName = "\"Users\".\"UserName\"";
                        break;
                }

                if (first) {
                    first = false;
                }
                else {
                    sortClause.append(", ");
                }

                sortClause.append(colName);

                if (reverse) {
                    sortClause.append(" DESC");
                    reverse = false;
                }
                else {
                    sortClause.append(" ASC");
                }
            }
        }
        else {
            // Default sort order
            sortClause.append("ORDER BY \"Users\".\"UserName\" ASC");
        }

        PreparedStatement s =
            con.prepareStatement("SELECT \"Users\".\"UserID\" FROM \"Users\" " +
                                 where.toString() + " " + sortClause + " " +
                                 limitClause);

        try {
            int position = 1;

            if (userID != null) {
                s.setInt(position++, userID);
            }

            if (name != null) {
                s.setString(position++, name);
            }

            if (type != null) {
                s.setString(position++, type.toString());
            }

            ResultSet rs = s.executeQuery();

            try {
                Collection<Integer> users = new LinkedList<Integer>();

                while (rs.next()) {
                    users.add(rs.getInt(1));
                }

                logger.debug("getMatchingUsers() discovered " + users.size() +
                             " users");
                return users;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Map<Integer, User> getUsers(Connection con,
                                          Collection<Integer> userIDs)
        throws DatabaseException, SQLException
    {

        StringBuilder whereClause = new StringBuilder("WHERE FALSE");
        for (int id : userIDs) {
            whereClause.append(" OR \"UserID\"=?");
        }

        PreparedStatement s =
            con
                .prepareStatement("SELECT \"UserID\", \"UserName\", \"Password\", " +
                                  "\"Type\", \"Preferences\", " +

                                  "(SELECT COUNT(*) FROM \"Jobs\" " +
                                  "  WHERE \"Jobs\".\"UserID\"=\"Users\".\"UserID\"), " +

                                  "(SELECT COUNT(*) FROM \"Batches\" " +
                                  "  WHERE \"Batches\".\"UserID\"=\"Users\".\"UserID\") " +

                                  "FROM \"Users\" " + whereClause.toString());

        try {
            int index = 1;
            for (int userID : userIDs) {
                s.setInt(index++, userID);
            }

            ResultSet rs = s.executeQuery();

            try {
                Map<Integer, User> users = new HashMap<Integer, User>();

                while (rs.next()) {
                    int userID = rs.getInt(1);
                    String name = rs.getString(2);
                    String password = rs.getString(3).trim();
                    User.UserType type = User.UserType.find(rs.getString(4));

                    Tag prefs;
                    try {
                        prefs = Utils.parseXML(rs.getString(5), true);
                    }
                    catch (IOException ioe) {
                        logger
                            .error(
                                "Unable to parse user preferences as XML, blanking",
                                ioe);
                        prefs = new Tag("prefs");
                    }

                    int jobCount = rs.getInt(6);
                    int batchCount = rs.getInt(7);

                    User user =
                        new User(userID, name, password, type, prefs, jobCount,
                            batchCount);

                    users.put(userID, user);
                }

                return users;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void updateUser(Connection con, User user)
        throws DatabaseException, SQLException
    {
        PreparedStatement s =
            con
                .prepareStatement("UPDATE \"Users\" SET \"UserName\"=?, \"Password\"=?, "
                                  + "\"Type\"=?, \"Preferences\"=? WHERE \"UserID\"=?");

        try {
            s.setString(1, user.name);
            s.setString(2, user.hashedPassword);
            s.setString(3, user.type.toString());
            s.setString(4, user.preferences.toString(false));

            s.setInt(5, user.userID);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void deleteSetting(Connection con, String settingName)
        throws DatabaseException, SQLException
    {

        PreparedStatement s =
            con
                .prepareStatement("DELETE FROM \"Settings\" WHERE \"SettingName\"=?");

        try {
            s.setString(1, settingName);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void createScheduleEntry(Connection con, ScheduleEntry entry)
        throws DatabaseException, SQLException
    {

        PreparedStatement s =
            con.prepareStatement("INSERT INTO \"Scheduling\" "
                                 + "(\"ModemID\", \"State\", \"StartTime\", "
                                 + "\"EndTime\", \"Relative\") "
                                 + "VALUES (?, ?, ?, ?, ?)");

        try {
            s.setInt(1, entry.modemID);
            s.setString(2, entry.state);
            s.setTimestamp(3, new Timestamp(entry.startTime.getTime()),
                utcCalendar);
            s.setTimestamp(4, new Timestamp(entry.endTime.getTime()),
                utcCalendar);
            s.setBoolean(5, entry.relative);

            s.executeUpdate();
        }
        finally {
            s.close();
        }

        s =
            con
                .prepareStatement("SELECT currval('\"Scheduling_ScheduleID_seq\"')");

        try {
            ResultSet rs = s.executeQuery();

            try {
                while (rs.next()) {
                    entry.scheduleID = rs.getInt(1);

                    logger.debug(String.format("Created new schedule %d",
                        entry.scheduleID));

                    return;
                }
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void deleteScheduleEntry(Connection con, int scheduleID)
        throws DatabaseException, SQLException
    {

        PreparedStatement s =
            con.prepareStatement("DELETE FROM \"Scheduling\" "
                                 + "WHERE \"ScheduleID\"=?");

        try {
            s.setInt(1, scheduleID);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Collection<Integer> getMatchingSchedules(
                                                       Connection con,
                                                       Integer scheduleID,
                                                       Integer modemID,
                                                       Boolean relative,
                                                       int start,
                                                       int limit,
                                                       List<ScheduleColumn> sortOn,
                                                       long[] matchCount)
        throws DatabaseException, SQLException
    {

        StringBuilder where = new StringBuilder();
        where.append("WHERE TRUE");

        if (scheduleID != null) {
            where.append(" AND \"Scheduling\".\"ScheduleID\"=?");
        }

        if (modemID != null) {
            where.append(" AND \"Scheduling\".\"ModemID\"=?");
        }

        if (relative != null) {
            where.append(" AND \"Scheduling\".\"Relative\"=?");
        }

        if (matchCount != null && matchCount.length > 0) {
            PreparedStatement s =
                con.prepareStatement("SELECT COUNT(*) FROM \"Scheduling\" " +
                                     where.toString());

            try {
                int position = 1;

                if (scheduleID != null) {
                    s.setInt(position++, scheduleID);
                }

                if (modemID != null) {
                    s.setInt(position++, modemID);
                }

                if (relative != null) {
                    s.setBoolean(position++, relative);
                }

                ResultSet rs = s.executeQuery();

                try {
                    while (rs.next()) {
                        matchCount[0] = rs.getLong(1);
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }
        }

        String limitClause = "";
        if (limit > -1) {
            limitClause = String.format("LIMIT %d OFFSET %d", limit, start);
        }

        String join = "";

        StringBuilder sortClause = new StringBuilder();
        if (sortOn != null && sortOn.size() > 0) {
            sortClause.append("ORDER BY ");

            boolean first = true;
            boolean reverse = false;
            for (ScheduleColumn col : sortOn) {
                String colName = null;
                switch (col) {
                    case Reverse:
                        reverse = !reverse;
                        continue;
                    case EndTime:
                        colName = "\"Scheduling\".\"EndTime\"";
                        break;
                    case ModemAlias:
                        join =
                            "LEFT JOIN \"Modems\" ON "
                                + "\"Scheduling\".\"ModemID\"=\"Modems\".\"ModemID\"";
                        colName = "\"Modems\".\"Alias\"";
                        break;
                    case ModemID:
                        colName = "\"Scheduling\".\"ModemID\"";
                        break;
                    case Relative:
                        colName = "\"Scheduling\".\"Relative\"";
                        break;
                    case ScheduleID:
                        colName = "\"Scheduling\".\"ScheduleID\"";
                        break;
                    case StartTime:
                        colName = "\"Scheduling\".\"StartTime\"";
                        break;
                    case State:
                        colName = "\"Scheduling\".\"State\"";
                        break;
                }

                if (first) {
                    first = false;
                }
                else {
                    sortClause.append(", ");
                }

                sortClause.append(colName);

                if (reverse) {
                    sortClause.append(" DESC");
                    reverse = false;
                }
                else {
                    sortClause.append(" ASC");
                }
            }
        }
        else {
            // Default sort order
            sortClause.append("ORDER BY \"Scheduling\".\"ScheduleID\" ASC");
        }

        PreparedStatement s =
            con.prepareStatement("SELECT \"Scheduling\".\"ScheduleID\" " +
                                 "FROM \"Scheduling\" " + join + " " +
                                 where.toString() + " " + sortClause + " " +
                                 limitClause);

        try {
            int position = 1;

            if (scheduleID != null) {
                s.setInt(position++, scheduleID);
            }

            if (modemID != null) {
                s.setInt(position++, modemID);
            }

            if (relative != null) {
                s.setBoolean(position++, relative);
            }

            ResultSet rs = s.executeQuery();

            try {
                Collection<Integer> schedules = new LinkedList<Integer>();

                while (rs.next()) {
                    schedules.add(rs.getInt(1));
                }

                logger.debug("getMatchingSchedules() discovered " +
                             schedules.size() + " schedule entries");
                return schedules;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected Map<Integer, ScheduleEntry> getScheduleEntries(
                                                             Connection con,
                                                             Collection<Integer> scheduleIDs)
        throws DatabaseException, SQLException
    {

        StringBuilder whereClause = new StringBuilder("WHERE FALSE");
        for (int id : scheduleIDs) {
            whereClause.append(" OR \"ScheduleID\"=?");
        }

        PreparedStatement s =
            con
                .prepareStatement("SELECT \"ScheduleID\", \"ModemID\", \"State\", " +
                                  "\"StartTime\", \"EndTime\", \"Relative\"" +
                                  "FROM \"Scheduling\" " +
                                  whereClause.toString());

        try {
            int index = 1;
            for (int scheduleID : scheduleIDs) {
                s.setInt(index++, scheduleID);
            }

            ResultSet rs = s.executeQuery();

            try {
                Map<Integer, ScheduleEntry> schedules =
                    new HashMap<Integer, ScheduleEntry>();

                while (rs.next()) {
                    int scheduleID = rs.getInt(1);
                    int modemID = rs.getInt(2);
                    String state = rs.getString(3);
                    java.util.Date startTime =
                        new java.util.Date(rs.getTimestamp(4, utcCalendar)
                            .getTime());
                    java.util.Date endTime =
                        new java.util.Date(rs.getTimestamp(5, utcCalendar)
                            .getTime());
                    boolean relative = rs.getBoolean(6);

                    ScheduleEntry schedule =
                        new ScheduleEntry(scheduleID, modemID, state,
                            startTime, endTime, relative);

                    schedules.put(scheduleID, schedule);
                }

                return schedules;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    @Override
    protected void updateScheduleEntry(Connection con, ScheduleEntry entry)
        throws DatabaseException, SQLException
    {

        PreparedStatement s =
            con.prepareStatement("UPDATE \"Scheduling\" SET \"ModemID\"=?, "
                                 + "\"State\"=?, \"StartTime\"=?, "
                                 + "\"EndTime\"=?, \"Relative\"=? "
                                 + "WHERE \"ScheduleID\"=?");

        try {
            s.setInt(1, entry.modemID);
            s.setString(2, entry.state);
            s.setTimestamp(3, new Timestamp(entry.startTime.getTime()),
                utcCalendar);
            s.setTimestamp(4, new Timestamp(entry.endTime.getTime()),
                utcCalendar);
            s.setBoolean(5, entry.relative);

            s.setInt(6, entry.scheduleID);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }
}

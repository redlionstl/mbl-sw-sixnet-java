/*
 * ModemCommunicationFailedException.java
 *
 * Thrown when communication with a modem fails.
 *
 * Jonathan Pearson
 * January 21, 2009
 *
 */

package com.sixnetio.BVB.Modems;

import java.io.IOException;

public class ModemCommunicationFailedException extends IOException {
	public ModemCommunicationFailedException(String message) {
		super(message);
	}
	
	public ModemCommunicationFailedException(String message, Throwable cause) {
		super(message, cause);
	}
}

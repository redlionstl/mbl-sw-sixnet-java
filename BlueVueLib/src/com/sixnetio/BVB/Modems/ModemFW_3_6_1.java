/*
 * ModemFW_3_6_1.java
 *
 * Provides a standard implementation for interfacing with a modem running 3.6.1 firmware.
 *
 * Jonathan Pearson
 * February 23, 2009
 *
 */

package com.sixnetio.BVB.Modems;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import com.sixnetio.BVB.Common.BlueTreeImage;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public abstract class ModemFW_3_6_1
    extends ModemModule
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    public ModemFW_3_6_1(String model, Version fwVersion)
    {
        super(model, fwVersion);
    }

    @Override
    public Map<String, String> getStatus(ModemCommunicator comm, Modem modem)
        throws ModemCommunicationFailedException
    {

        NDC.push(String.format("model = '%s', fwver = '%s'", getModel(),
            getFWVersion()));

        try {
            Map<String, String> props = new Hashtable<String, String>();

            // Request:
            // - Analog inputs
            props.putAll(ModemLib.getAnalogInputs_after2_0_3(comm));

            // - Discrete inputs
            props.putAll(ModemLib.getDiscreteInputs_after2_0_3(comm));

            // - RSSI
            // - Roaming status
            // - Uptime
            // - GPS time (if available)
            // - Latitude (if available)
            // - Longitude (if available)
            // - WAN IP
            props.putAll(ModemLib.getDiagnostics_after3_6_1(comm));

            // Avoid calling these functions too many times, they may query the
            // modem
            boolean cdma = isCDMA(comm);
            boolean hspa = isHSPA(comm);
            if (cdma || hspa) {
                Map<String, String> tempProps;

                if (cdma) {
                    tempProps = ModemLib.getNetworkStatus_CDMA(comm);
                }
                else {
                    tempProps = ModemLib.getNetworkStatus_HSPA_after3_6_0(comm);
                }

                // Don't want to put all of that in, most of it is pretty ugly
                // - Service availability
                try {
                    props.put("Service Availability", tempProps
                        .get("Service Availability"));
                }
                catch (Exception e) {
                    // Ignore it
                }

                // - Network time
                try {
                    props.put("Network Time", tempProps.get("Network Time"));
                }
                catch (Exception e) {
                    // Ignore it
                }
            }
            else {
                // - Service availability
                // - Network time
                // Unavailable on this type of modem
            }

            // - Ground speed
            // - Heading
            // - Visible satellite count
            // ...
            if (supportsGPS(comm)) {
                props.putAll(ModemLib.getGPSData(comm));
            }

            return props;
        }
        finally {
            NDC.pop();
        }
    }

    @Override
    public void loadPackage(ModemCommunicator comm, Modem modem,
                            BlueTreeImage image)
        throws ModemCommunicationFailedException
    {

        // Make sure it's the right modem first
        verifyModem(comm, modem);

        // Upload the update package
        try {
            ModemLib.uploadPackage(modem, image);
        }
        catch (ModemCommunicationFailedException mcfe) {
            logger.info("Modem failed to accept a package upload, resetting",
                mcfe);

            comm.sendMessage("AT+BRESET=0");

            throw new ModemCommunicationFailedException(
                "Unable to upload package, reset modem for future attempts",
                mcfe);
        }

        // Tell it to perform an upgrade
        // Although this modem supports the AT+BFWUPS=3 method, make sure it
        // still will after the upgrade is done... It needs to clean up
        // after itself, after all
        // AT+BFWUPS=3 was introduced in 3.6.1
        if ((image.getType() == BlueTreeImage.ImageType.Bluex4K_5K || image
            .getType() == BlueTreeImage.ImageType.Bluex5Kv2_6K) &&
            image.getVersion().compareTo(new Version("3.6.1")) >= 0) {

            // We're upgrading to something that also supports AT+BFWUPS=3
            ModemLib.performPackageUpdate_after3_6_1(comm, modem);
        }
        else {
            // We're downgrading to something that doesn't support
            // AT+BFWUPS=3
            ModemLib.performPackageUpdate(comm, modem);
        }
    }

    @Override
    public void refreshModemData(ModemCommunicator comm, Modem modem)
        throws ModemCommunicationFailedException
    {
        NDC.push(String.format("model = '%s', fwver = '%s', modemID = %d",
            getModel(), getFWVersion(), modem.modemID));

        try {
            if (modem.deviceID == null) {
                try {
                    modem.deviceID = ModemLib.getDeviceID(comm);
                }
                catch (Exception e) {
                    if (e instanceof ModemCommunicationFailedException) {
                        throw (ModemCommunicationFailedException)e;
                    }

                    // Otherwise, log and ignore it
                    logger.debug("Unable to retrieve device ID", e);
                }
            }

            Map<String, String> props = new Hashtable<String, String>();

            // Request other properties:
            // CfgVersion
            modem.cfgVersion = ModemLib.getCfgVersion_after3_6_1(comm);

            // Password
            props.putAll(ModemLib.getPasswordSettings_after3_4_0(comm));

            // GPS module version
            // Serial number
            // RF module version
            // HW version
            props.putAll(ModemLib.getVersions(comm));

            // Alias
            // Phone number
            // PRL Version
            props.putAll(ModemLib.getInfo_after3_6_1(comm));

            // Set the properties of the modem structure
            modem.alias = props.get("Alias");
            modem.phoneNumber = props.get("Phone");

            modem.setProperty("passwordSettings/enabled", props
                .get("Password/Enabled"));
            modem.setProperty("passwordSettings/access", props
                .get("Password/Access"));
            modem.setProperty("passwordSettings/password", props
                .get("Password/Password"));
            modem.setProperty("versions/hw", props.get("Version/HW"));
            modem.setProperty("versions/rf", props.get("Version/RF"));
            modem.setProperty("versions/gps", props.get("Version/GPS"));
            modem.setProperty("versions/uboot", props.get("Version/U-Boot"));
            modem.setProperty("serial", props.get("Serial"));
            modem.setProperty("carrier", props.get("Carrier"));

            if (props.containsKey("Version/PRL")) {
                modem.setProperty("versions/prl", props.get("Version/PRL"));
            }
        }
        finally {
            NDC.pop();
        }

    }

    @Override
    public void runCommands(ModemCommunicator comm, Modem modem,
                            String... commands)
        throws ModemCommunicationFailedException
    {

        // FUTURE: Stick the commands into an update package and load it

        boolean locallyOpened = false;
        if (comm == null) {
            // Connect normally and run the commands
            comm = new SocketModemCommunicator();
            comm.connect(modem);
            locallyOpened = true;
        }

        try {
            // If we opened the connection ourselves, make sure we are talking
            // to the right modem
            if (locallyOpened) {
                verifyModem(comm, modem);
            }

            ModemLib.executeScript(comm, modem, commands);
        }
        finally {
            if (locallyOpened) {
                comm.close();
            }
        }
    }

    /**
     * Whether this modem supports GPS. Implemented by subclasses.
     * 
     * @param comm If necessary, the subclass may try to auto-detect using this
     *            connected communicator.
     * 
     * @return True = this modem supports GPS; False = this modem does not
     *         support GPS.
     * @throws ModemCommunicationFailedException If auto-detection fails
     *             catastrophically.
     */
    protected abstract boolean supportsGPS(ModemCommunicator comm)
        throws ModemCommunicationFailedException;

    /**
     * Whether this modem operates on the CDMA network. Implemented by
     * subclasses.
     * 
     * @param comm If necessary, the subclass may try to auto-detect using this
     *            connected communicator.
     * 
     * @return True = this modem operates on CDMA; False = this modem does not
     *         operate on CDMA.
     * @throws ModemCommunicationFailedException If auto-detection fails
     *             catastrophically.
     */
    protected abstract boolean isCDMA(ModemCommunicator comm)
        throws ModemCommunicationFailedException;

    /**
     * Whether this modem operates on the HSPA network. Implemented by
     * subclasses.
     * 
     * @param comm If necessary, the subclass may try to auto-detect using this
     *            connected communicator.
     * 
     * @return True = this modem operates on HSPA; False = this modem does not
     *         operate on HSPA.
     * @throws ModemCommunicationFailedException If auto-detection fails
     *             catastrophically.
     */
    protected abstract boolean isHSPA(ModemCommunicator comm)
        throws ModemCommunicationFailedException;
}

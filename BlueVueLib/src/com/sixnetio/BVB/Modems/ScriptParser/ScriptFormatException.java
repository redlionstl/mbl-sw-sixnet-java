/*
 * ScriptFormatException.java
 *
 * Thrown when a TTL script cannot be parsed.
 *
 * Jonathan Pearson
 * March 12, 2009
 *
 */

package com.sixnetio.BVB.Modems.ScriptParser;

import com.sixnetio.util.Utils;

public class ScriptFormatException extends Exception {
	private String[] messages;
	
	public ScriptFormatException(String message) {
		setMessages(new String[] {
			message
		});
	}
	
	public ScriptFormatException(String[] messages) {
		setMessages(messages);
	}
	
	public ScriptFormatException(String message, Throwable cause) {
		super(cause);
		
		setMessages(new String[] {
			message
		});
	}
	
	public ScriptFormatException(String[] messages, Throwable cause) {
		super(cause);
		
		setMessages(messages);
	}
	
	private void setMessages(String[] messages) {
		this.messages = new String[messages.length];
		System.arraycopy(messages, 0, this.messages, 0, messages.length);
	}
	
	@Override
	public String getMessage() {
		return Utils.combineLines(messages);
	}
	
	public String[] getMessages() {
		String[] messages = new String[this.messages.length];
		System.arraycopy(this.messages, 0, messages, 0, messages.length);
		
		return messages;
	}
}

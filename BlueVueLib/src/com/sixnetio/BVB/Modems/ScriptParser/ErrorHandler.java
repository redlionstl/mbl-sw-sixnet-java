/*
 * ErrorHandler.java
 *
 * Notified when errors occur in the parser. The parser only
 * supports a single error handler, so if you want more than
 * one object to be notified of errors, you'll need to handle
 * the multicasting yourself.
 *
 * Jonathan Pearson
 * June 5, 2009
 *
 */

package com.sixnetio.BVB.Modems.ScriptParser;

public interface ErrorHandler {
	public void handleError(String msg);
}

/*
 * ScriptCompiler.java
 *
 * Given an enhanced TTL script and a modem, compile the TTL
 * script and simplify it into a BlueTree standard TTL script
 * containing only send(ln) and wait(ln) statements.
 *
 * Jonathan Pearson
 * Mar 12, 2009
 *
 */

package com.sixnetio.BVB.Modems.ScriptParser;

import java.util.*;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.CommonTree;
import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.util.Utils;

public class ScriptCompiler {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Check that a script will compile. If the script compiles, the function
	 * returns successfully.
	 * 
	 * @param script The script to check.
	 * @throws ScriptFormatException If the script does not compile.
	 */
	public static void checkScript(String script) throws ScriptFormatException {
		try {
			generateParseTree(script);
		} catch (RecognitionException re) {
			logger.error("Bad script", re);
			
			throw new ScriptFormatException("Unable to compile script: " + re.getMessage(), re);
		}
	}
	
	/**
	 * Compile the given script for the given modem.
	 * 
	 * @param script The script to compile.
	 * @param modem The modem from which to pull variable values.
	 * @return The compiled script lines, compatible with BlueTree modem TTL
	 *         script parsers.
	 * @throws ScriptFormatException If there was a problem compiling the
	 *             script.
	 */
	public static String[] compileScript(String script, Modem modem) throws ScriptFormatException {
		// Compile the script into a parse tree
		CommonTree root;
		try {
			root = generateParseTree(script);
		} catch (RecognitionException re) {
			logger.error("Bad script", re);
			
			throw new ScriptFormatException("Unable to compile script for Modem #" + modem.modemID +
			                                ": " + re.getMessage(), re);
		} catch (ScriptFormatException sfe) {
			// Add another message to it and throw it again
			String[] oldMessages = sfe.getMessages();
			String[] messages = new String[1 + oldMessages.length];
			
			messages[0] = "Unable to compile script for Modem #" + modem.modemID + ":";
			System.arraycopy(oldMessages, 0, messages, 1, oldMessages.length);
			
			throw new ScriptFormatException(messages, sfe);
		}
		
		// 'Run' the script to generate a standard TTL script
		return runScript(root, modem);
	}
	
	private static CommonTree generateParseTree(String script) throws RecognitionException,
	        ScriptFormatException {
		// Read from the passed-in string
		ANTLRStringStream input = new ANTLRStringStream(script);
		
		// Lex the new input stream
		TTLScriptLexer lexer = new TTLScriptLexer(input);
		
		// Create a token stream out of what the lexer produces
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		
		// Parse the token stream
		TTLScriptParser parser = new TTLScriptParser(tokens);
		
		final List<String> errors = new LinkedList<String>();
		parser.setErrorHandler(new ErrorHandler() {
			@Override
			public void handleError(String msg) {
				errors.add(msg);
			}
		});
		
		// Run the whole thing to produce an AST
		TTLScriptParser.prog_return r = parser.prog();
		
		if (errors.size() > 0) {
			// Concatenate into a single string
			throw new ScriptFormatException(errors.toArray(new String[errors.size()]));
		}
		
		// Return the AST
		return (CommonTree)r.getTree();
	}
	
	private static String[] runScript(CommonTree tree, Modem modem) throws ScriptFormatException {
		// Initialize a variable table with data from the modem
		Map<String, String> variables = new Hashtable<String, String>();
		initializeVariables(variables, modem);
		
		// Run the program to generate a BlueTree compatible TTL script
		ScriptRunner runner = new ScriptRunner(variables);
		List<String> scriptLines = runner.runScript(tree);
		
		// Return the generated script
		return scriptLines.toArray(new String[scriptLines.size()]);
	}
	
	private static void initializeVariables(Map<String, String> variables, Modem modem) {
		if (modem.deviceID == null) {
			variables.put("deviceID", "?");
		} else {
			variables.put("deviceID", modem.deviceID.toString());
		}
		
		placeValue(variables, "alias", modem.alias);
		placeValue(variables, "model", modem.model);
		placeValue(variables, "versionFW", modem.fwVersion.toString());
		placeValue(variables, "cfgVersion", modem.cfgVersion);
		placeValue(variables, "ipAddress", modem.ipAddress);
		placeValue(variables, "port", "" + modem.port);
		placeValue(variables, "phoneNumber", modem.phoneNumber);
		
		// Grab the standard properties
		// Password settings
		placeValue(variables, "password", modem.getProperty("passwordSettings/password"), "");
		placeValue(variables, "passwordEnabled", modem.getProperty("passwordSettings/enabled"));
		placeValue(variables, "passwordAccess", modem.getProperty("passwordSettings/access"));
		
		// Serial number
		placeValue(variables, "serial", modem.getProperty("serial"));
		
		// Version numbers
		placeValue(variables, "versionHW", modem.getProperty("versions/hw"));
		placeValue(variables, "versionRF", modem.getProperty("versions/rf"));
		placeValue(variables, "versionGPS", modem.getProperty("versions/gps"));
		placeValue(variables, "versionUBoot", modem.getProperty("versions/uboot"));
		placeValue(variables, "versionPRL", modem.getProperty("versions/prl"));
		
		// User-defined variables
		if (modem.status.getTag("variables") != null) {
			for (Tag tag : modem.status.getEachTag("variables", "variable")) {
				placeValue(variables, tag.getStringAttribute("name"), tag.getStringContents());
			}
		}
	}
	
	private static void placeValue(Map<String, String> variables, String name, String value) {
		placeValue(variables, name, value, "?");
	}
	
	private static void placeValue(Map<String, String> variables, String name, String value,
	        String dflt) {
		if (value == null) {
			variables.put(name, dflt);
		} else {
			variables.put(name, value);
		}
	}
}

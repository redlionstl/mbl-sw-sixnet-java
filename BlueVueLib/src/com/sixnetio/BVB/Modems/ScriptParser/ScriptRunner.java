/*
 * ScriptRunner.java
 *
 * Actually runs a compiled script.
 *
 * Jonathan Pearson
 * March 13, 2009
 *
 */

package com.sixnetio.BVB.Modems.ScriptParser;

import java.util.*;

import org.antlr.runtime.tree.CommonTree;
import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

class ScriptRunner {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// If we ever introduce functions, this will not be acceptable
	// This is guarded by a single synchronized entry-point function
	private Map<String, String> variables;
	
	/**
	 * Construct a new ScriptRunner, initializing the variable lookup table with
	 * the given values.
	 * 
	 * @param variables The initial variable lookup table, or <tt>null</tt> to
	 *            use a blank one.
	 */
	public ScriptRunner(Map<String, String> variables) {
		if (variables == null) {
			this.variables = new Hashtable<String, String>();
		} else {
			this.variables = variables;
		}
	}
	
	/**
	 * Run a compiled script.
	 * 
	 * @param tree The root of the parse tree for the compiled script.
	 * @return A list of sendln/waitln commands that can be directly run against
	 *         a modem.
	 * @throws ScriptFormatException If there was a problem running the script.
	 */
	public synchronized List<String> runScript(CommonTree tree) throws ScriptFormatException {
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("Running this parse tree:\n%s", generateTree(tree, 0)));
		}
		
		List<String> lines = prog(tree);
		
		if (logger.isDebugEnabled()) {
			// Put together a final view of the variable table
			StringBuilder builder = new StringBuilder();
			
			for (String key : variables.keySet()) {
				builder.append(String.format("'%s' = '%s'\n", key, variables.get(key)));
			}
			
			logger.debug(String.format("Final variables table:\n%s", builder.toString()));
		}
		
		return lines;
	}
	
	private List<String> prog(CommonTree tree) throws ScriptFormatException {
		// Should be a 'PROG', containing a number of statements
		if (tree.getType() != TTLScriptParser.PROG) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) should contain a code block, but instead contains a '%s'",
			                                              tree.getLine(),
			                                              TTLScriptParser.tokenNames[tree.getType()]));
		}
		
		return stmts(tree);
	}
	
	private List<String> block(CommonTree tree) throws ScriptFormatException {
		// Should be a 'BLOCK', containing a number of statements
		if (tree.getType() != TTLScriptParser.BLOCK) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) should contain a code block, but instead contains a '%s'",
			                                              tree.getLine(),
			                                              TTLScriptParser.tokenNames[tree.getType()]));
		}
		
		return stmts(tree);
	}
	
	private List<String> stmts(CommonTree tree) throws ScriptFormatException {
		// Loop through each child and execute it
		// appropriately
		List<String> lines = new LinkedList<String>();
		
		if (tree.getChildren() == null) return lines; // No statements to run
		
		for (Object childObj : tree.getChildren()) {
			CommonTree child = (CommonTree)childObj;
			
			switch (child.getType()) {
				case TTLScriptParser.SENDLN:
					lines.add(sendln(child));
					break;
				case TTLScriptParser.WAITLN:
					lines.add(waitln(child));
					break;
				case TTLScriptParser.IF:
					lines.addAll(ifstmt(child));
					break;
				case TTLScriptParser.EQUALS:
					assignment(child);
					break;
				default:
					throw new ScriptFormatException(
					                                String
					                                      .format(
					                                              "Script (line %d) contains an unknown statement type '%s'",
					                                              child.getLine(),
					                                              TTLScriptParser.tokenNames[child
					                                                                              .getType()]));
			}
		}
		
		return lines;
	}
	
	private String sendln(CommonTree tree) throws ScriptFormatException {
		// There should be exactly one child
		if (tree.getChildCount() != 1) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains a sendln statement with the wrong number of arguments",
			                                              tree.getLine()));
		}
		
		// Turn it into a string and return a sendln command
		CommonTree child = (CommonTree)tree.getChild(0);
		String value = expr(child);
		
		return String.format("sendln %s", quoteValue(value, tree.getLine()));
	}
	
	private String waitln(CommonTree tree) throws ScriptFormatException {
		// There should be at least one but not more than 10 children
		if (tree.getChildCount() < 1) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains a waitln statement with no arguments",
			                                              tree.getLine()));
		} else if (tree.getChildCount() > 10) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains a waitln statement with more than 10 arguments",
			                                              tree.getLine()));
		}
		
		// Parse each child and append to a growing waitln command
		StringBuilder command = new StringBuilder("waitln ");
		
		boolean firstChild = true;
		for (Object childObj : tree.getChildren()) {
			CommonTree child = (CommonTree)childObj;
			
			// Convert to a string
			String value = expr(child);
			value = quoteValue(value, tree.getLine());
			
			if (firstChild) {
				firstChild = false;
			} else {
				command.append(", ");
			}
			
			command.append(value);
		}
		
		return command.toString();
	}
	
	private List<String> ifstmt(CommonTree tree) throws ScriptFormatException {
		// There should be two or three children
		if (tree.getChildCount() < 2 || tree.getChildCount() > 3) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains an if statement with the wrong number of pieces",
			                                              tree.getLine()));
		}
		
		List<String> lines = new LinkedList<String>();
		
		CommonTree testExpr = (CommonTree)tree.getChild(0);
		CommonTree trueBlock = (CommonTree)tree.getChild(1);
		CommonTree falseBlock = null;
		
		if (tree.getChildCount() == 3) {
			falseBlock = (CommonTree)tree.getChild(2);
		}
		
		// Evaluate testExpr
		if (evaluateTest(testExpr)) {
			lines.addAll(block(trueBlock));
		} else {
			// This could be a block, or another if
			if (falseBlock.getType() == TTLScriptParser.BLOCK) {
				lines.addAll(block(falseBlock));
			} else if (falseBlock.getType() == TTLScriptParser.IF) {
				lines.addAll(ifstmt(falseBlock));
			}
		}
		
		return lines;
	}
	
	private void assignment(CommonTree tree) throws ScriptFormatException {
		// There should be exactly two children
		if (tree.getChildCount() != 2) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains an assignment with the wrong number of arguments",
			                                              tree.getLine()));
		}
		
		CommonTree leftChild = (CommonTree)tree.getChild(0);
		CommonTree rightChild = (CommonTree)tree.getChild(1);
		
		// The first child should be a variable
		if (leftChild.getType() != TTLScriptParser.VARIABLE) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains an assignment to something that is not a variable",
			                                              tree.getLine()));
		}
		
		// The second child is an expression
		String value = expr(rightChild);
		
		// Assign the value to the variable
		variables.put(leftChild.getText(), value);
	}
	
	private String expr(CommonTree tree) throws ScriptFormatException {
		// Could be a string, a variable, or a concatenation
		// Turn it into a string
		String value;
		if (tree.getType() == TTLScriptParser.STRING) {
			// Should have no children
			if (tree.getChildCount() != 0) throw new ScriptFormatException(
			                                                               String
			                                                                     .format(
			                                                                             "Script (line %d) contains a string with parse tree children",
			                                                                             tree
			                                                                                 .getLine()));
			
			value = tree.getText();
			
			// Remove the surrounding quotes
			value = value.substring(1, value.length() - 1);
		} else if (tree.getType() == TTLScriptParser.VARIABLE) {
			// Should have no children
			if (tree.getChildCount() != 0) throw new ScriptFormatException(
			                                                               String
			                                                                     .format(
			                                                                             "Script (line %d) contains a variable with parse tree children",
			                                                                             tree
			                                                                                 .getLine()));
			
			// Make sure the variable is defined
			if (!variables.containsKey(tree.getText())) {
				throw new ScriptFormatException(
				                                String
				                                      .format(
				                                              "Script (line %d) contains a reference to the undefined variable '%s'",
				                                              tree.getLine(), tree.getText()));
			}
			
			value = variables.get(tree.getText());
		} else if (tree.getType() == TTLScriptParser.CONCAT) {
			value = concatenate(tree);
		} else {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains an unknown expression type '%s'",
			                                              tree.getLine(),
			                                              TTLScriptParser.tokenNames[tree.getType()]));
		}
		
		return value;
	}
	
	private String concatenate(CommonTree tree) throws ScriptFormatException {
		// There should be exactly two children
		if (tree.getChildCount() != 2) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains a concatenation with the wrong number of arguments",
			                                              tree.getLine()));
		}
		
		CommonTree leftChild = (CommonTree)tree.getChild(0);
		CommonTree rightChild = (CommonTree)tree.getChild(1);
		
		// Each is an expression
		// Turn them into strings and return a concatenation
		String leftValue = expr(leftChild);
		String rightValue = expr(rightChild);
		
		return String.format("%s%s", leftValue, rightValue);
	}
	
	private boolean evaluateTest(CommonTree tree) throws ScriptFormatException {
		// Could be a boolean operator or a comparison operator
		switch (tree.getType()) {
			case TTLScriptParser.BOOLOP:
				return boolOp(tree);
			case TTLScriptParser.COMPAREOP:
				return comparison(tree);
			default:
				throw new ScriptFormatException(
				                                String
				                                      .format(
				                                              "Script (line %d) contains a test expression with an unrecognized type",
				                                              tree.getLine()));
		}
	}
	
	private boolean boolOp(CommonTree tree) throws ScriptFormatException {
		// Should contain exactly two children
		if (tree.getChildCount() != 2) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains a boolean operation with the wrong number of arguments",
			                                              tree.getLine()));
		}
		
		CommonTree leftChild = (CommonTree)tree.getChild(0);
		CommonTree rightChild = (CommonTree)tree.getChild(1);
		
		// Evaluate each of them (they're both test expressions)
		boolean leftValue = evaluateTest(leftChild);
		boolean rightValue = evaluateTest(rightChild);
		
		// Check which type of boolean operation to perform
		if (tree.getText().equals("&&")) {
			return (leftValue && rightValue);
		} else if (tree.getText().equals("||")) {
			return (leftValue || rightValue);
		} else {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains an unrecognized boolean operator '%s'",
			                                              tree.getLine(), tree.getText()));
		}
	}
	
	private boolean comparison(CommonTree tree) throws ScriptFormatException {
		// Should contain exactly two children
		if (tree.getChildCount() != 2) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains a comparison with the wrong number of arguments",
			                                              tree.getLine()));
		}
		
		CommonTree leftChild = (CommonTree)tree.getChild(0);
		CommonTree rightChild = (CommonTree)tree.getChild(1);
		
		// Evaluate each of them (they're both string expressions)
		String leftValue = expr(leftChild);
		String rightValue = expr(rightChild);
		
		// Check which type of comparison to perform
		if (tree.getText().equals("==")) {
			return leftValue.equals(rightValue);
		} else if (tree.getText().equals("!=")) {
			return !leftValue.equals(rightValue);
		} else if (tree.getText().equals("==i")) {
			return leftValue.equalsIgnoreCase(rightValue);
		} else if (tree.getText().equals("!=i")) {
			return !leftValue.equalsIgnoreCase(rightValue);
		} else if (tree.getText().equals(">")) {
			Version leftVersion = new Version(leftValue);
			Version rightVersion = new Version(rightValue);
			
			return (leftVersion.compareTo(rightVersion) > 0);
		} else if (tree.getText().equals(">=")) {
			Version leftVersion = new Version(leftValue);
			Version rightVersion = new Version(rightValue);
			
			return (leftVersion.compareTo(rightVersion) >= 0);
		} else if (tree.getText().equals("<")) {
			Version leftVersion = new Version(leftValue);
			Version rightVersion = new Version(rightValue);
			
			return (leftVersion.compareTo(rightVersion) < 0);
		} else if (tree.getText().equals("<=")) {
			Version leftVersion = new Version(leftValue);
			Version rightVersion = new Version(rightValue);
			
			return (leftVersion.compareTo(rightVersion) <= 0);
		} else {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) contains an unrecognized comparison operator: %s",
			                                              tree.getLine(), tree.getText()));
		}
	}
	
	// Quotes a value using the proper quote type, or throws an exception if
	// both types of quotes are in use
	private String quoteValue(String value, int lineNumber) throws ScriptFormatException {
		// Check for quotes in the value
		if (value.indexOf('\'') != -1 && value.indexOf('"') != -1) {
			throw new ScriptFormatException(
			                                String
			                                      .format(
			                                              "Script (line %d) generates a string containing both single and double quotes, and is therefore not representable",
			                                              lineNumber));
		}
		
		if (value.indexOf('\'') == -1) {
			// Use single quotes
			return String.format("'%s'", value);
		} else {
			// Use double quotes
			return String.format("\"%s\"", value);
		}
	}
	
	private static String generateTree(CommonTree tree, int indentLevel) {
		StringBuilder builder = new StringBuilder();
		
		if (tree.getType() == -1) return ""; // Nothing here
		
		char[] indentChars = new char[indentLevel * 2];
		Arrays.fill(indentChars, ' ');
		String indent = new String(indentChars);
		
		builder.append(String.format("\n%s(%s[%s]", indent,
		                             TTLScriptParser.tokenNames[tree.getType()], tree.getText()));
		
		boolean hadGrandchildren = false;
		if (tree.getChildren() != null) {
			for (Object childObj : tree.getChildren()) {
				CommonTree child = (CommonTree)childObj;
				
				if (child.getChildCount() > 0 || child.getType() == TTLScriptParser.BLOCK) {
					hadGrandchildren = true;
					builder.append(generateTree(child, indentLevel + 1));
				} else {
					builder.append(String.format(" %s[%s]",
					                             TTLScriptParser.tokenNames[child.getType()],
					                             child.getText()));
				}
			}
		}
		
		if (hadGrandchildren) {
			builder.append(String.format("\n%s)", indent));
		} else {
			builder.append(")");
		}
		
		return builder.toString();
	}
}

/*
 * ModemFW_3_8_0.java
 *
 * Provides a standard implementation for interfacing with a modem running 3.8.0
 * firmware.
 *
 * Jonathan Pearson
 * October 2, 2009
 *
 */

package com.sixnetio.BVB.Modems;

import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.server.http.HTTP;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public abstract class ModemFW_3_8_0
    extends ModemModule
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    public ModemFW_3_8_0(String model, Version fwVersion)
    {
        super(model, fwVersion);
    }

    @Override
    public Map<String, String> getStatus(ModemCommunicator comm, Modem modem)
        throws ModemCommunicationFailedException
    {
        NDC.push(String.format("model = '%s', fwver = '%s'", getModel(),
            getFWVersion()));

        try {
            Map<String, String> props = new Hashtable<String, String>();

            // Request:
            // - Analog inputs
            if (!propertyUpdatedRecently("AI1")) {
                // We could also check AI2, AI3, or PWR
                props.putAll(ModemLib.getAnalogInputs_after2_0_3(comm));
            }
            else {
                logger.debug("Analog inputs are fresh, skipping");
            }

            // - Discrete inputs
            if (!propertyUpdatedRecently("DI1")) {
                // We could also check DI2-4 or IGN
                props.putAll(ModemLib.getDiscreteInputs_after2_0_3(comm));
            }
            else {
                logger.debug("Discrete inputs are fresh, skipping");
            }

            // Check for GPS now because the diagnostics check (next) gets the
            // properties that we would use to see if GPS data had been
            // collected already

            // GPS is interesting because not all properties may be reported in
            // a BEP message, but no more will be reported when we request, so
            // there's no point in requesting if any of them have been reported

            // But, rather than testing everything, let's just check for a
            // couple of common properties (Latitude and Longitude, the most
            // likely reason for using GPS)

            // - Ground speed
            // - Heading
            // - Visible satellite count
            // ...
            if (supportsGPS(comm)) {
                if (!propertiesUpdatedRecently(Utils.makeArrayList("Latitude",
                    "Longitude"))) {
                    props.putAll(ModemLib.getGPSData(comm));
                }
                else {
                    logger.debug("GPS data is fresh, skipping");
                }
            }

            // - RSSI
            // - Roaming status
            // - Uptime
            // - GPS time (if available)
            // - Latitude (if available)
            // - Longitude (if available)
            // - WAN IP
            if (!propertiesUpdatedRecently(Utils.makeArrayList("RSSI",
                "Roaming Status", "Uptime", "Latitude", "Longitude",
                "GPS Time", "WAN IP"))) {
                props.putAll(ModemLib.getDiagnostics_after3_6_1(comm));
            }
            else {
                logger.debug("Diagnostics are fresh, skipping");
            }

            // Update the recent properties list going into the next section
            addRecentProperties(props.keySet());

            // Avoid calling these functions too many times, they may query the
            // modem
            boolean cdma = isCDMA(comm);
            boolean hspa = isHSPA(comm);
            if (cdma || hspa) {
                Map<String, String> tempProps = new HashMap<String, String>();

                // These include a LOT of properties that we do not necessarily
                // want to hold onto, since they are not useful to the end user
                // Only test for properties that we know we need
                if (cdma) {
                    if (!propertiesUpdatedRecently(Utils.makeArrayList(
                        "Service Availability", "Network Time"))) {

                        /*
                         * These values are skipped: "RSSI" (requested earlier),
                         * "Roaming Status" (requested earlier), "RF State",
                         * "Service In Use", "Frequency", "Channel", "SID",
                         * "NID", "PN Offset", "Base Station",
                         * "Slot Cycle Index", "Ec/Io", "Tx Pwr", "Tx Adj", and
                         * "FER"
                         */

                        tempProps.putAll(ModemLib.getNetworkStatus_CDMA(comm));
                    }
                    else {
                        logger.debug("Network status is fresh, skipping");
                    }
                }
                else {
                    // Values which are marked as "Always N/A" are not included
                    // in this list
                    if (!propertiesUpdatedRecently(Utils
                        .makeArrayList("Network Time"))) {

                        /*
                         * These values are skipped: "RSSI" (requested earlier),
                         * "Roaming Status", (requested earlier),
                         * "Service In Use", "Band", "Channel",
                         * "Network String", "MNC", "PN Offset", "Cell ID",
                         * "Ec/Io", and "BER"
                         */
                        tempProps.putAll(ModemLib
                            .getNetworkStatus_HSPA_after3_6_0(comm));
                    }
                    else {
                        logger.debug("Network status is fresh, skipping");
                    }
                }

                // Don't want to put all of that in, most of it is pretty ugly
                // - Service availability
                try {
                    props.put("Service Availability", tempProps
                        .get("Service Availability"));
                }
                catch (Exception e) {
                    // Ignore it
                }

                // - Network time
                try {
                    props.put("Network Time", tempProps.get("Network Time"));
                }
                catch (Exception e) {
                    // Ignore it
                }
            }
            else {
                // - Service availability
                // - Network time
                // Unavailable on this type of modem
            }

            return props;
        }
        finally {
            NDC.pop();
        }
    }

    @Override
    public void loadPackage(ModemCommunicator comm, final Modem modem,
                            BlueTreeImage image)
        throws ModemCommunicationFailedException
    {
        // Make sure it's the right modem first
        verifyModem(comm, modem);

        // Only use the HTTP server method if we must and if the address of this
        // server is available
        List<ModemLib.ReportDestination> destinations =
            ModemLib.getReportDestinations_after2_0_3(comm);

        // Scan through for a MOM-type destination and grab the IP address
        String serverIP = null;
        for (ModemLib.ReportDestination dest : destinations) {
            if (dest.destinationType == ModemLib.ReportDestination.Type.MOM) {
                serverIP = dest.address;
                break;
            }
        }

        final HTTP httpServer = getHTTPServer();

        logger
            .debug("comm " +
                   (comm.connectSupported() ? "supports" : "does not support") +
                   " connect()");

        if (comm.connectSupported()) {
            // It is possible to do it the old-fashioned way, which is the most
            // reliable
            // Enable the FTP server
            ModemLib.enableFTP_after3_7_0(comm);

            // Upload the update package
            try {
                ModemLib.uploadPackage(modem, image);
            }
            catch (ModemCommunicationFailedException mcfe) {
                logger.info(
                    "Modem failed to accept a package upload, resetting", mcfe);

                comm.sendMessage("AT+BRESET=0");

                throw new ModemCommunicationFailedException(
                    "Unable to upload package, modem reset to help future attempts",
                    mcfe);
            }

            // Disable the FTP server
            ModemLib.disableFTP_after3_7_0(comm);
        }
        else {
            if (httpServer != null && serverIP != null) {
                // We need to use the HTTP method
                // But will that cause problems? Firmware before 3.6.1 did not
                // support AT+BFWUPS=3, which means we would need to connect to
                // the
                // modem to clean up with AT+BFWUPS=0, but connect() is not
                // supported on our communicator so we can't
                if ((image.getType() == BlueTreeImage.ImageType.Bluex4K_5K || image
                    .getType() == BlueTreeImage.ImageType.Bluex5Kv2_6K) &&
                    image.getVersion().compareTo(new Version("3.6.1")) < 0) {

                    throw new ModemCommunicationFailedException(
                        "Refusing to start a firmware update when it would be impossible to finish it");
                }

                // FUTURE: This could use a lot of unnecessary memory, if a
                // bunch of
                // modems all wanted to download the same update at the same
                // time

                // Publish the update package, named for this modem's device ID
                httpServer.addFile(modem.deviceID.toString(), image
                    .getPackageData());

                int port =
                    Server.getServerSettings().getIntValue(Setting.HTTPPort);
                ModemLib.downloadPackage_after3_8_0(comm, String.format(
                    "http://%s:%d/%s", serverIP, port, modem.deviceID
                        .toString()));

                // Stop sharing the file
                httpServer.removeFile(modem.deviceID.toString());
            }
            else {
                logger.error("httpServer is " +
                             (httpServer == null ? "null" : "not null"));
                logger.error("serverIP is " + serverIP);
                logger
                    .error("Unable to load firmware, no available connection method");

                throw new ModemCommunicationFailedException(
                    "There is no available method to load firmware onto this modem");
            }
        }

        // Tell it to perform an upgrade
        // Although this modem supports the AT+BFWUPS=3 method, make sure it
        // still will after the upgrade is done... It needs to clean up
        // after itself, after all
        if ((image.getType() == BlueTreeImage.ImageType.Bluex4K_5K || image
            .getType() == BlueTreeImage.ImageType.Bluex5Kv2_6K) &&
            image.getVersion().compareTo(new Version("3.6.1")) >= 0) {

            // We're upgrading to something that also supports AT+BFWUPS=3
            ModemLib.performPackageUpdate_after3_6_1(comm, modem);
        }
        else {
            // We're downgrading to something that doesn't support
            // AT+BFWUPS=3
            ModemLib.performPackageUpdate(comm, modem);
        }
    }

    @Override
    public void refreshModemData(ModemCommunicator comm, Modem modem)
        throws ModemCommunicationFailedException
    {
        NDC.push(String.format("model = '%s', fwver = '%s', modemID = %d",
            getModel(), getFWVersion(), modem.modemID));

        try {
            if (modem.deviceID == null) {
                try {
                    modem.deviceID = ModemLib.getDeviceID(comm);
                }
                catch (Exception e) {
                    if (e instanceof ModemCommunicationFailedException) {
                        throw (ModemCommunicationFailedException)e;
                    }

                    // Otherwise, log and ignore it
                    logger.debug("Unable to retrieve device ID", e);
                }
            }

            Map<String, String> props = new Hashtable<String, String>();

            // FUTURE: It would be nice to avoid re-requesting this data if it
            // was updated by BEP messages

            // Request other properties:
            // CfgVersion
            modem.cfgVersion = ModemLib.getCfgVersion_after3_6_1(comm);

            // Password
            props.putAll(ModemLib.getPasswordSettings_after3_4_0(comm));

            // GPS module version
            // Serial number
            // RF module version
            // HW version
            props.putAll(ModemLib.getVersions(comm));

            // Alias
            // Phone number
            // PRL Version
            props.putAll(ModemLib.getInfo_after3_6_1(comm));

            // Set the properties of the modem structure
            modem.alias = props.get("Alias");
            modem.phoneNumber = props.get("Phone");

            modem.setProperty("passwordSettings/enabled", props
                .get("Password/Enabled"));
            modem.setProperty("passwordSettings/access", props
                .get("Password/Access"));
            modem.setProperty("passwordSettings/password", props
                .get("Password/Password"));
            modem.setProperty("versions/hw", props.get("Version/HW"));
            modem.setProperty("versions/rf", props.get("Version/RF"));
            modem.setProperty("versions/gps", props.get("Version/GPS"));
            modem.setProperty("versions/uboot", props.get("Version/U-Boot"));
            modem.setProperty("serial", props.get("Serial"));
            modem.setProperty("carrier", props.get("Carrier"));

            if (props.containsKey("Version/PRL")) {
                modem.setProperty("versions/prl", props.get("Version/PRL"));
            }
        }
        finally {
            NDC.pop();
        }

    }

    @Override
    public void runCommands(ModemCommunicator comm, Modem modem,
                            String... commands)
        throws ModemCommunicationFailedException
    {
        // FUTURE: Stick the commands into an update package and load it

        boolean locallyOpened = false;
        if (comm == null) {
            // Connect normally and run the commands
            comm = new SocketModemCommunicator();
            comm.connect(modem);
            locallyOpened = true;
        }

        try {
            // If we opened the connection ourselves, make sure we are talking
            // to the right modem
            if (locallyOpened) {
                verifyModem(comm, modem);
            }

            ModemLib.executeScript(comm, modem, commands);
        }
        finally {
            if (locallyOpened) {
                comm.close();
            }
        }
    }

    /**
     * Whether this modem supports GPS. Implemented by subclasses.
     * 
     * @param comm If necessary, the subclass may try to auto-detect using this
     *            connected communicator.
     * 
     * @return True = this modem supports GPS; False = this modem does not
     *         support GPS.
     * @throws ModemCommunicationFailedException If auto-detection fails
     *             catastrophically.
     */
    protected abstract boolean supportsGPS(ModemCommunicator comm)
        throws ModemCommunicationFailedException;

    /**
     * Whether this modem operates on the CDMA network. Implemented by
     * subclasses.
     * 
     * @param comm If necessary, the subclass may try to auto-detect using this
     *            connected communicator.
     * 
     * @return True = this modem operates on CDMA; False = this modem does not
     *         operate on CDMA.
     * @throws ModemCommunicationFailedException If auto-detection fails
     *             catastrophically.
     */
    protected abstract boolean isCDMA(ModemCommunicator comm)
        throws ModemCommunicationFailedException;

    /**
     * Whether this modem operates on the HSPA network. Implemented by
     * subclasses.
     * 
     * @param comm If necessary, the subclass may try to auto-detect using this
     *            connected communicator.
     * 
     * @return True = this modem operates on HSPA; False = this modem does not
     *         operate on HSPA.
     * @throws ModemCommunicationFailedException If auto-detection fails
     *             catastrophically.
     */
    protected abstract boolean isHSPA(ModemCommunicator comm)
        throws ModemCommunicationFailedException;
}

/*
 * ModemModule.java
 *
 * Provides a standard interface which all modem modules must follow.
 * A number of public non-static functions in this class provide basic
 * functionality expected to be common across all modems; if you are
 * writing a new modem module for a modem that does not cooperate with
 * these functions, you should definitely override them with functional
 * versions.
 *
 * Jonathan Pearson
 * January 16, 2009
 *
 */

package com.sixnetio.BVB.Modems;

import java.lang.reflect.Constructor;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.server.http.HTTP;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public abstract class ModemModule
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public static final String ANY_MODEL = "*";
	public static final Version ANY_VERSION = new Version("*");
	
	private static Map<String, Map<Version, Class<? extends ModemModule>>> knownModels =
		new HashMap<String, Map<Version, Class<? extends ModemModule>>>();
	
	/**
	 * Register a modem module that can interface with a modem of the specified
	 * model/version combination.
	 * 
	 * @param model The model of the modem that the class can handle.
	 * @param fwVersion The firmware version that the class can handle.
	 * @param moduleClass The class that can handle modems of that type and
	 *            firmware version.
	 */
	public static void registerModule(String model, Version fwVersion,
			Class<? extends ModemModule> moduleClass)
	{
		if (moduleClass == null) {
			throw new NullPointerException("moduleClass may not be null");
		}
		
		logger.debug(String.format("Registering modem module model '%s' " +
		                           "version '%s' with class '%s'",
		                           model,
		                           fwVersion,
		                           moduleClass.getName()));
		
		synchronized (knownModels) {
			Map<Version, Class<? extends ModemModule>> knownVersions =
				knownModels.get(model);
			
			if (knownVersions == null) {
				knownVersions = new HashMap<Version, Class<? extends ModemModule>>();
				knownModels.put(model, knownVersions);
			}
			
			if (knownVersions.put(fwVersion, moduleClass) != null) {
				throw new DuplicateRegistrationError("Duplicate modem module with model '" +
				                                     model +
				                                     "' and firmware version '" +
				                                     fwVersion.toString() + "'");
			}
		}
	}
	
	/**
	 * Get a set of modem models which have registered modules.
	 */
	public static Set<String> getKnownModels()
	{
		synchronized (knownModels) {
			return new HashSet<String>(knownModels.keySet());
		}
	}
	
	/**
	 * Get a set of version numbers for the specified model which have
	 * registered modules.
	 * 
	 * @param model The model to check on.
	 * @return A set (possibly empty) of known versions for that model. Will
	 *         never return <tt>null</tt>.
	 */
	public static Set<Version> getKnownVersions(String model)
	{
		synchronized (knownModels) {
			Map<Version, Class<? extends ModemModule>> knownVersions =
				knownModels.get(model);
			
			if (knownVersions == null) {
				return new HashSet<Version>();
			}
			else {
				return knownVersions.keySet();
			}
		}
	}
	
	/**
	 * Get a modem module that can interface with the specified type of modem.
	 * 
	 * @param model The model number of the modem.
	 * @param fwVersion The firmware revision currently running in the modem.
	 * @return A modem module that can communicate with a modem of the specified
	 *         type.
	 * @throws BadModemModuleException If no module exists or there is a problem
	 *             creating it.
	 */
	public static ModemModule makeModule(String model, Version fwVersion)
		throws BadModemModuleException
	{
		NDC.push(String.format("model = '%s', fwVersion = '%s'", model, fwVersion));
		
		try {
			Map<Version, Class<? extends ModemModule>> knownVersions;
			
			// Try to find a matching version for the model we found
			Version useVersion;
			
			synchronized (knownModels) {
				// Try to find a matching model
				// This may return the wildcard model, but we will assume that
				// it returns a real one
				String useModel = findMatchingModel(knownModels.keySet(), model);
				
				// Nothing found (no match, no wildcard)? Fail
				if (useModel == null) {
					logger.error("Model not registered, no fallback available");
					
					throw new BadModemModuleException("Unknown modem model",
					                                  model, fwVersion);
				}
				
				knownVersions = knownModels.get(useModel);
				useVersion = findMatchingVersion(knownVersions.keySet(), fwVersion);
				
				// Nothing found (no match, no wildcard)?
				if (useVersion == null) {
					// Explicitly check the wildcard model
					// Worst case is we just checked this, but a little
					// duplicate code isn't going to hurt
					if (knownModels.containsKey(ANY_MODEL)) {
						// There's a wildcard model, let's try that
						useModel = ANY_MODEL;
					}
					else {
						// No wildcard to fall back on, fail
						logger.error("Version not registered, no fallback available");
						
						throw new BadModemModuleException("Unknown modem version",
						                                  model, fwVersion);
					}
					
					// We're now trying with the wildcard model to find a
					// matching
					// version
					knownVersions = knownModels.get(useModel);
					useVersion = findMatchingVersion(knownVersions.keySet(),
					                                 fwVersion);
					
					// Still nothing found? Fail
					if (useVersion == null) {
						// Simply nothing available that will work
						logger.error("Version not registered, no fallback available");
						
						throw new BadModemModuleException("Unknown modem version",
						                                  model, fwVersion);
					}
				}
			}
			
			// If we get here, we must have a model and version number selected
			Class<? extends ModemModule> moduleClass =
				knownVersions.get(useVersion);
			
			try {
				logger.debug(String.format("Calling constructor of '%s'",
				                           moduleClass.getName()));
				
				Constructor<? extends ModemModule> constructor =
					moduleClass.getConstructor(String.class, Version.class);
				
				// Construct with the passed-in model/version, not what we
				// selected
				return constructor.newInstance(model, fwVersion);
			}
			catch (Exception e) {
				// NoSuchMethodException, SecurityException (getConstructor)
				// InstantiationException or IllegalAccessException,
				// IllegalArgumentException, InvocationTargetException
				// (newInstance)
				logger.error(String.format("Unable to instantiate module class '%s'",
				                           moduleClass.getName()), e);
				
				throw new BadModemModuleException("Unable to instantiate modem module",
				                                  model, fwVersion, e);
			}
		}
		finally {
			NDC.pop();
		}
	}
	
	private static String findMatchingModel(Set<String> haystack, String needle)
	{
		// Search models for the exact match
		if (haystack.contains(needle)) {
			return needle;
		}
		
		// No exact match? Try the wildcard model
		if (haystack.contains(ANY_MODEL)) {
			return ANY_MODEL;
		}
		
		// Still no?
		return null;
	}
	
	private static Version findMatchingVersion(Set<Version> haystack, Version needle)
	{
		// Sort version numbers
		Version[] sortedVersions = haystack.toArray(new Version[0]);
		Arrays.sort(sortedVersions);
		
		// Search for the closest, lesser version number
		// This is a simple straight-through search
		// Since unsupported versions are likely to come from the top,
		// search backwards
		for (int i = sortedVersions.length - 1; i >= 0; i--) {
			// Don't include wildcards in the search (yet)
			if ( ! sortedVersions[i].equals(ANY_VERSION) &&
			    sortedVersions[i].compareTo(needle) <= 0) {
				
				// Accept this one
				return sortedVersions[i];
			}
		}
		
		// Didn't find one? Check for a wildcard version
		if (haystack.contains(ANY_VERSION)) {
			return ANY_VERSION;
		}
		else {
			return null;
		}
	}
	
	private String model;
	private Version fwVersion;
	private Set<String> recentProperties;
	private HTTP httpServer;
	
	/**
	 * Construct a new modem module.
	 * 
	 * @param model The model of the modems that this module will be
	 *            communicating with.
	 * @param fwVersion The firmware version of the modems that this module will
	 *            be communicating with.
	 */
	public ModemModule(String model, Version fwVersion)
	{
		this.model = model;
		this.fwVersion = fwVersion;
		this.recentProperties = new HashSet<String>();
	}
	
	/**
	 * Get the model of the modems that this module will be communicating with.
	 */
	public String getModel()
	{
		return model;
	}
	
	/**
	 * Get the firmware version of the modems that this module will be
	 * communicating with.
	 */
	public Version getFWVersion()
	{
		return fwVersion;
	}
	
	/**
	 * Get the HTTP server, if there is one running. This may be used to send
	 * files to modems that support HTTP downloads.
	 * 
	 * @return The HTTP server, if there is one. Returns <tt>null</tt>
	 *   otherwise.
	 */
	public HTTP getHTTPServer()
	{
		return httpServer;
	}
	
	/**
	 * Set the HTTP server to use for modem-pull data transfers.
	 * 
	 * @param httpServer The server to use, or <tt>null</tt> to use a 'push'
	 *   method for data transfers.
	 */
	public void setHTTPServer(HTTP httpServer)
	{
		this.httpServer = httpServer;
	}
	
	/**
	 * Known properties are not re-requested from the modem, so if you recently
	 * updated any of these (that go into the Status History), add their names
	 * here.
	 * 
	 * @param propNames The names of properties that have been updated recently.
	 */
	public void addRecentProperties(Collection<String> propNames)
	{
		recentProperties.addAll(propNames);
	}
	
	/**
	 * Known properties are not re-requested from the modem, so if you recently
	 * updated any of these (that go into the Status History), add their names
	 * here.
	 * 
	 * @param propName The name of a property that was updated recently.
	 */
	public void addRecentProperty(String propName)
	{
		recentProperties.add(propName);
	}
	
	/**
	 * Check whether the set of recently updated properties includes the given
	 * property.
	 * 
	 * @param propName The name of the property.
	 * @return <tt>true</tt> if the set contains the named property, meaning
	 *   that that property has been updated recently and does not need to be
	 *   requested from the modem; <tt>false</tt> otherwise. You should try to
	 *   avoid requesting properties whose values have recently been updated, to
	 *   save bandwidth.
	 */
	public boolean propertyUpdatedRecently(String propName)
	{
		return recentProperties.contains(propName);
	}
	
	/**
	 * Check whether the set of recently updated properties includes ALL of the
	 * given properties.
	 * 
	 * @param propNames The names of the properties.
	 * @return <tt>true</tt> if the set contains every named property, meaning
	 *   that all of those properties have been updated recently and do not need
	 *   to be requested from the modem; <tt>false</tt> otherwise. You should
	 *   try to avoid requesting properties whose values have recently been
	 *   updated, to save bandwidth.
	 */
	public boolean propertiesUpdatedRecently(Collection<String> propNames)
	{
		return recentProperties.containsAll(propNames);
	}
	
	/**
	 * Acquire modem status values including:
	 * <ul>
	 * <li>Analog inputs</li>
	 * <li>Discrete inputs</li>
	 * <li>Roaming status</li>
	 * <li>Modem uptime</li>
	 * <li>RSSI</li>
	 * <li>GPS latitude</li>
	 * <li>GPS longitude</li>
	 * <li>GPS ground speed</li>
	 * <li>GPS heading</li>
	 * <li>Number of visible GPS satellites</li>
	 * </ul>
	 * 
	 * @param comm The ModemCommunicator connected to the modem.
	 * @return A map of the names of the properties onto string representations
	 *         of their values.
	 * @throws ModemCommunicationFailedException If communications fail for some
	 *             reason.
	 * @throws UnsupportedOperationException If the operation is not supported
	 *             by this module.
	 */
	public abstract Map<String, String> getStatus(ModemCommunicator comm,
	                                              Modem modem)
		throws ModemCommunicationFailedException;
	
	/**
	 * Refreshes the data in the Modem structure. If its deviceID is set to -1,
	 * the device ID in the modem will be written into that field; if the
	 * deviceID is not -1 but does not match the value in the modem, an
	 * exception will be thrown before any values are written.
	 * 
	 * @param modem The modem structure to refresh.
	 * @param comm The ModemCommunicator connected to the modem.
	 * @throws ModemCommunicationFailedException If communications fail for some
	 *             reason.
	 * @throws UnsupportedOperationException If the operation is not supported
	 *             by this module.
	 */
	public abstract void refreshModemData(ModemCommunicator comm, Modem modem)
		throws ModemCommunicationFailedException;
	
	/**
	 * Perform a package load on the modem. The package may contain a firmware
	 * image, PRL, GPS firmware, TTL script, ...
	 * 
	 * @param comm The ModemCommunicator connected to the modem.
	 * @param modem The modem to load.
	 * @param image The firmware image to load.
	 * @throws ModemCommunicationFailedException If communications fail or the
	 *             modem's device ID does not match the value in the structure.
	 * @throws UnsupportedOperationException If the operation is not supported
	 *             by this module.
	 */
	public abstract void loadPackage(ModemCommunicator comm,
	                                 Modem modem,
	                                 BlueTreeImage image)
		throws ModemCommunicationFailedException;
	
	/**
	 * Run a series of commands on the modem.
	 * 
	 * @param comm The ModemCommunicator connected to the modem. If
	 *            <tt>null</tt>, will open a new connection using a
	 *            {@link SocketModemCommunicator}.
	 * @param modem The modem on which to run the commands.
	 * @param commands The commands to run.
	 * @throws ModemCommunicationFailedException If communications fail for some
	 *             reason, or if <tt>sock</tt> was <tt>null</tt> and
	 *             verifyModem() fails.
	 * @throws UnsupportedOperationException If the operation is not supported
	 *             by this module.
	 */
	public abstract void runCommands(ModemCommunicator comm,
	                                 Modem modem,
	                                 String... commands)
		throws ModemCommunicationFailedException;
	
	/**
	 * Verify that the device we are connected to is the device we think we are
	 * connected to.
	 * 
	 * @param comm The ModemCommunicator connected to the modem.
	 * @param modem The object representing the device.
	 * @throws ModemCommunicationFailedException If the device ID or model
	 *             number does not match. If <tt>modem.deviceID == -1</tt>, the
	 *             device ID check is skipped. If <tt>modem.model == null</tt>,
	 *             the model number check is skipped.
	 */
	public void verifyModem(ModemCommunicator comm, Modem modem)
		throws ModemCommunicationFailedException
	{
		if (modem.deviceID != null) {
			// Check device ID
			DeviceID devID;
			
			try {
				devID = ModemLib.getDeviceID(comm);
			}
			catch (Exception e) {
				if (e instanceof ModemCommunicationFailedException) {
					throw (ModemCommunicationFailedException)e;
				}
				
				// Otherwise, make a ModemCommunicationFailedException out of
				// it, since we need to check the device ID
				logger.debug("Unexpected exception thrown while retrieving device ID",
				             e);
				
				throw new ModemCommunicationFailedException("Unexpected error when retrieving device ID: " +
				                                            e.getMessage(), e);
			}
			
			if ( ! devID.equals(modem.deviceID)) {
				throw new ModemCommunicationFailedException(String.format("Modem reports incorrect device ID; found '%s', expected '%s'",
				                                                          devID.toString(),
				                                                          modem.deviceID.toString()));
			}
		}
		
		if (modem.model != null) {
			// Check model number
			String model;
			try {
				model = ModemLib.getModel(comm);
			}
			catch (Exception e) {
				if (e instanceof ModemCommunicationFailedException) {
					throw (ModemCommunicationFailedException)e;
				}
				
				// Otherwise, make a ModemCommunicationFailedException out of
				// it, since we need to check the device ID
				logger.debug("Unexpected exception thrown while retrieving model number",
				             e);
				
				throw new ModemCommunicationFailedException("Unexpected error when retrieving model number: " +
				                                            e.getMessage(), e);
			}
			
			if (!model.equals(modem.model)) {
				throw new ModemCommunicationFailedException(String.format("Modem reports incorrect model number; found '%s', expected '%s'",
				                                                          model,
				                                                          modem.model));
			}
		}
	}
}

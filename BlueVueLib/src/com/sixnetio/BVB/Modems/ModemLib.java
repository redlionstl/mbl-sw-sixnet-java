/*
 * ModemLib.java
 *
 * Contains function calls to run commands on a modem specific to
 * firmware revision or model
 *
 * Jonathan Pearson
 * February 9, 2009
 *
 */

package com.sixnetio.BVB.Modems;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.text.*;
import java.util.*;

import org.apache.commons.net.ftp.*;
import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.gps.GPSLib;
import com.sixnetio.gps.NMEAParser;
import com.sixnetio.util.*;

public class ModemLib
{
    static Logger logger = Logger.getLogger(Utils.thisClassName());

    /**
     * A structure used to represent a report destination as reported by the
     * AT+BRPRDS? command.
     * 
     * @author Jonathan Pearson
     */
    public static class ReportDestination
    {
        /**
         * Represents the value of the Type field of a Report Destination.
         * 
         * @author Jonathan Pearson
         */
        public static enum Type
        {
            /** This report destination is disabled. */
            Disabled(0),

            /** IP address reporting destination. */
            IPAddress(1),

            /** Serial port raw data reporting destination. */
            SerialPortRawData(2),

            /** MOM (Mobile Originated Management) reporting destination. */
            MOM(3),

            /** Reserved destination type. */
            Reserved4(4),

            /** Reserved destination type. */
            Reserved5(5),

            /** The destination type was not recognized. */
            Unknown(), ;

            /** The value used in a Report Destination data line. */
            public final int value;

            private Type(int value)
            {
                this.value = value;
            }

            private Type()
            {
                this.value = -1;
            }

            /**
             * Find a Report Destination Type by its numeric value.
             * 
             * @param value The value to search for.
             * @return The type, or {@link #Unknown} if not found.
             */
            public static Type find(int value)
            {
                for (Type t : values()) {
                    if (t.value == value) {
                        return t;
                    }
                }

                return Unknown;
            }
        }

        /**
         * Represents the value of the Port Type field of a Report Destination.
         * 
         * @author Jonathan Pearson
         */
        public static enum PortType
        {
            /** Represents a UDP port. */
            UDP(0),

            /** Represents a TCP port. */
            TCP(1),

            /** Indicates that the port type is not used. */
            NotUsed, ;

            /** The value used in a Report Destination data line. */
            public final int value;

            private PortType(int value)
            {
                this.value = value;
            }

            private PortType()
            {
                this.value = -1;
            }

            /**
             * Find a Report Destination Port Type by numeric value.
             * 
             * @param value The value to search for.
             * @return The port type, or {@link #NotUsed} if not found.
             */
            public static PortType find(int value)
            {
                for (PortType pt : values()) {
                    if (pt.value == value) {
                        return pt;
                    }
                }

                return NotUsed;
            }
        }

        /** The ID of this report destination. */
        public final int id;

        /** The type of this destination. */
        public final Type destinationType;

        /** The address of this destination. */
        public final String address;

        /** The port number of this destination. */
        public final int port;

        /** The port type of this destination. */
        public final PortType portType;

        /**
         * Construct a new Report Destination given a pre-split line.
         * 
         * @param pieces The pieces of a report destination line, split on
         *            commas and without the "+BRPRDS:" prefix.
         */
        protected ReportDestination(String[] pieces)
        {
            this.id = Integer.parseInt(pieces[0]);
            this.destinationType = Type.find(Integer.parseInt(pieces[1]));
            this.address = pieces[2];
            this.port = Integer.parseInt(pieces[3]);
            this.portType = PortType.find(Integer.parseInt(pieces[4]));
        }
    }

    /**
     * Get the model number of any BlueTree modem using the AT+GMM command.
     * Supported on all models/firmware.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The model number of the modem. This is NOT sanitized.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem or the format of the response
     *             was not recognized.
     */
    public static String getModel(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+GMM");
        String response = comm.receiveMessage();

        // Response will be "\n+GMM: <model number>\nOK\n" or similar
        // Find the +GMM: line and pull out the model number
        String line = comm.getLine(response, "+GMM");
        if (line == null) {
            throw new ModemCommunicationFailedException(
                "Bad response from modem: " + response);
        }

        String model = comm.stripResponse(line);

        // Strip off the carrier
        if (model == null) {
            throw new ModemCommunicationFailedException(
                "Bad response from modem: " + response);
        }

        if (model.indexOf(' ') != -1) {
            model = model.substring(0, model.indexOf(' '));
        }

        logger.debug(String.format("Modem is model '%s'", model));

        return model;
    }

    /**
     * Get the carrier of any BlueTree modem using the AT+GMM command. Supported
     * on all models/firmware.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The sanitized carrier of the modem, or null if none was found.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     */
    public static String getCarrier(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+GMM");
        String response = comm.receiveMessage();

        String line = comm.getLine(response, "+GMM");
        if (line == null) {
            throw new ModemCommunicationFailedException(
                "Bad response from modem: " + response);
        }

        String carrier = comm.stripResponse(line);

        // Strip off the model number
        if (carrier.indexOf(' ') != -1) {
            carrier = carrier.substring(carrier.indexOf(' ') + 1);
        }
        else {
            // Carrier not included
            carrier = null;
        }

        logger.debug(String.format("Modem has carrier '%s'", carrier));

        return sanitize(carrier);
    }

    /**
     * Get the firmware version number of any BlueTree modem using the AT+GMR
     * command. Supported on all models/firmware.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The firmware version of the modem.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem or the format of the response
     *             was not recognized.
     */
    public static Version getFWVersion(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+GMR");
        String response = comm.receiveMessage();

        String line = comm.getLine(response, "+GMR");
        if (line == null) {
            throw new ModemCommunicationFailedException(
                "Bad response from modem: '" + response + "'");
        }

        // Strip the response, but we still need to process it some more
        String data = comm.stripResponse(line);

        // Look for text after "FW:" but before a "("
        if (data == null || data.indexOf("FW:") == -1) {
            throw new ModemCommunicationFailedException(
                "No firmware version in response: " + response);
        }

        data = data.substring(data.indexOf("FW:") + 3);

        if (data.indexOf("(") == -1) {
            throw new ModemCommunicationFailedException(
                "No firmware version in response: " + response);
        }

        data = data.substring(0, data.indexOf("("));

        logger.debug(String.format("Modem is running fw v. '%s'", data));

        return new Version(data);
    }

    /**
     * Get analog inputs from a modem using the standard AT+BAIGET command.
     * Supported on all models since fw 2.0.3.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The analog inputs:
     *         <ul>
     *         <li>'AI?' = Analog input ? (a number)</li>
     *         <li>'PWR' = Power input</li>
     *         </ul>
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     */
    public static Map<String, String> getAnalogInputs_after2_0_3(
                                                                 ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BAIGET?");
        String response = comm.receiveMessage();

        Map<String, String> props = new Hashtable<String, String>();

        List<String> lines = comm.getLines(response, "+BAIGET:");
        for (String line : lines) {
            try {
                String[] kv = comm.parseValues(comm.stripResponse(line));

                if (kv != null && kv.length == 2) {
                    props.put(kv[0], kv[1]);
                }
            }
            catch (Exception e) {
                logger
                    .debug(
                        "Unexpected exception parsing analog inputs from AT+BAIGET",
                        e);
            }
        }

        return sanitize(props);
    }

    /**
     * Get discrete inputs from a modem using the standard AT+BDIGET command.
     * Supported on all models since fw 2.0.3.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The discrete inputs:
     *         <ul>
     *         <li>'DI?' = Discrete Input ? (a number)</li>
     *         <li>'IGN' = Ignition value</li>
     *         </ul>
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     */
    public static Map<String, String> getDiscreteInputs_after2_0_3(
                                                                   ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BDIGET?");
        String response = comm.receiveMessage();

        Map<String, String> props = new Hashtable<String, String>();

        List<String> lines = comm.getLines(response, "+BDIGET:");
        for (String line : lines) {
            try {
                String[] kv = comm.parseValues(comm.stripResponse(line));

                if (kv != null && kv.length == 2) {
                    props.put(kv[0], kv[1]);
                }
            }
            catch (Exception e) {
                logger
                    .debug(
                        "Unexpected exception parsing discrete inputs from AT+BDIGET",
                        e);
            }
        }

        return sanitize(props);
    }

    /**
     * Get standard diagnostics info, including roaming status, uptime, and
     * RSSI, using the +BMDIAG command. Supported on all models since fw 3.6.1.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return Named properties:
     *         <ul>
     *         <li>'Roaming Status' = roaming status</li>
     *         <li>'Uptime' = uptime (milliseconds)</li>
     *         <li>'RSSI' = Receive Signal Strength Indicator</li>
     *         <li>'GPS Time' = GPS Time in milliseconds since midnight, this
     *         morning</li>
     *         <li>'Latitude' = Latitude (degrees north)</li>
     *         <li>'Longitude' = Longitude (degrees east)</li>
     *         <li>'WAN IP' = IP address of the WAN interface</li>
     *         </ul>
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     */
    public static Map<String, String> getDiagnostics_after3_6_1(
                                                                ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BMDIAG");
        String response = comm.receiveMessage();

        Map<String, String> props = new Hashtable<String, String>();

        String roaming =
            comm.stripResponse(comm.getLine(response, "Registration:"));
        String uptime = comm.stripResponse(comm.getLine(response, "Uptime:"));
        String rssi = comm.stripResponse(comm.getLine(response, "RSSI:"));
        String gps = comm.stripResponse(comm.getLine(response, "GPS fix:"));
        String ip = comm.stripResponse(comm.getLine(response, "WAN IP:"));

        try {
            props.put("Roaming Status", roaming);
        }
        catch (Exception e) {
            // Could be a NullPointerException
            logger
                .debug(
                    "Unexpected exception getting Roaming Status from AT+BMDIAG",
                    e);
        }

        // Instead of failing if the format is weird, just skip the property
        try {
            long uptimeMS = comm.parseUptime(uptime);
            props.put("Uptime", "" + uptimeMS);
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing uptime from AT+BMDIAG",
                e);
        }

        // I have seen a modem 'skip' this property before
        // Also, might as well make sure it doesn't throw a
        // NumberFormatException
        try {
            float rssiDBm =
                Float.parseFloat(rssi.substring(0, rssi.indexOf(" dBm")));
            props.put("RSSI", "" + rssiDBm);
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing RSSI from AT+BMDIAG", e);
        }

        if (gps != null) {
            logger.debug("Parsing GPS data: " + gps);

            // Parse GPS time into a long value as MS since midnight, this
            // morning
            try {
                String gpsTime =
                    gps.substring(gps.indexOf("GPS_TIME:") +
                                  "GPS_TIME:".length());
                gpsTime = gpsTime.substring(0, gpsTime.indexOf(',')).trim();

                long ms = NMEAParser.parseUTC(gpsTime);
                props.put("GPS Time", "" + ms);
            }
            catch (Exception e) {
                // A NumberFormatException or a StringIndexOutOfBoundsException,
                // most likely
                logger.info("Unable to parse GPS time from string '" + gps +
                            "'", e);
            }

            // Parse latitude/longitude
            try {
                String latitude =
                    gps.substring(gps.indexOf("LAT:") + "LAT:".length());
                String ns = latitude.substring(latitude.indexOf(',') + 1);
                ns = ns.substring(0, ns.indexOf(','));
                latitude = latitude.substring(0, latitude.indexOf(',')).trim();

                float lat = NMEAParser.parseDegrees(latitude);

                if (ns.equalsIgnoreCase("S")) {
                    lat *= -1;
                }

                String longitude =
                    gps.substring(gps.indexOf("LON:") + "LON:".length());
                String ew = longitude.substring(longitude.indexOf(',') + 1);
                if (ew.indexOf(',') != -1) {
                    ew = ew.substring(0, ew.indexOf(','));
                }
                longitude =
                    longitude.substring(0, longitude.indexOf(',')).trim();

                float lon = NMEAParser.parseDegrees(longitude);

                if (ew.equalsIgnoreCase("W")) {
                    lon *= -1;
                }

                props.put("Latitude", "" + lat);
                props.put("Longitude", "" + lon);
            }
            catch (Exception e) {
                // A NumberFormatException or a StringIndexOutOfBoundsException,
                // most likely
                logger.info("Unable to parse latitude/longitude from string '" +
                            gps + "'", e);
            }
        }

        if (ip != null) {
            ip = ip.substring(0, ip.indexOf(',')).trim();

            try {
                props.put("WAN IP", ip);
            }
            catch (Exception e) {
                // Really unexpected, but better than letting the connection die
                logger.info("Unable to save IP address '" + ip + "'", e);
            }
        }

        return sanitize(props);
    }

    /**
     * Get GPS data from a modem using the AT+BGPSGT? command.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return Named properties:
     *         <ul>
     *         <li><b>Latitude</b> (float) - Latitude in degrees +N/-S</li>
     *         <li><b>Longitude</b> (float) - Longitude in degrees +E/-W</li>
     *         <li><b>GPS Time</b> (long) - Milliseconds since midnight, this
     *         morning</li>
     *         <li><b>GPS Fix Quality</b> (String) - GPS fix quality</li>
     *         <li><b>GPS Satellite Count</b> (int) - Number of satellites in
     *         view</li>
     *         <li><b>Altitude</b> (float) - Altitude of the antenna above/below
     *         mean sea level, in meters</li>
     *         <li><b>GPS Geoidal Separation</b> (float) - Difference between
     *         WGS-84 Earth ellipsoid and mean sea level, in meters</li>
     *         <li><b>GPS Differential Reference Station ID</b> (int) -
     *         Differential reference station ID</li>
     *         <li><b>GPS Manual Mode</b> (boolean) - True if manual mode, false
     *         otherwise</li>
     *         <li><b>GPS Fix Mode</b> (String) - Fix mode</li>
     *         <li><b>GPS Sat X ID</b> (int) - Satellite ID of satellite X used
     *         for the fix (one for each satellite used)</li>
     *         <li><b>GPS PDOP</b> (int) - Positional dilution of precision, in
     *         meters</li>
     *         <li><b>GPS HDOP</b> (int) - Horizontal dilution of precision, in
     *         meters</li>
     *         <li><b>GPS VDOP</b> (int) - Vertical dilution of precision, in
     *         meters</li>
     *         <li><b>GPS Sat X.Y PRN</b> (int) - PRN of satellite Y reported in
     *         the X message of this GSV sequence</li>
     *         <li><b>GPS Sat X.Y Elevation</b> (int) - Elevation of that
     *         satellite in degrees</li>
     *         <li><b>GPS Sat X.Y Azimuth</b> (int) - Azimuth of that satellite
     *         in degrees to true north (0-359)</li>
     *         <li><b>GPS Sat X.Y SNR</b> (int) - SNR of that satellite in dB
     *         (0-99)</li>
     *         <li><b>GPS Date</b> (long) - Milliseconds since the epoch, at
     *         midnight, this morning</li>
     *         <li><b>GPS Low Signal</b> (boolean) - A warning indicator that
     *         the GPS signal is too low to have confidence in the position</li>
     *         <li><b>GPS Speed km/h</b> (float) - Ground speed, in kilometers
     *         per hour</li>
     *         <li><b>GPS True Heading</b> (float) - Tracking direction, in
     *         degrees to true north</li>
     *         <li><b>GPS Magnetic Variation</b> (float) - Magnetic variation,
     *         in degrees</li>
     *         <li><b>GPS Magnetic Heading</b> (float) - Tracking direction, in
     *         degrees to magnetic north</li>
     *         <li><b>GPS Timezone Hour Correction</b> (int) - Time zone
     *         correction hours</li>
     *         <li><b>GPS Timezone Minute Correction</b> (int) - Time zone
     *         correction minutes</li>
     *         </ul>
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static Map<String, String> getGPSData(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BGPSGT?");
        String response = comm.receiveMessage();

        Map<String, String> props = new Hashtable<String, String>();

        // Check for OK
        if (comm.getLine(response, "OK") == null) {
            logger.debug("Bad response for AT+BGPSGT?: " + response);
            return props;
        }

        // Pull out the part between '+BGPSGT: "'" and the closing '"'
        try {
            response =
                response.substring(response.indexOf("+BGPSGT: \"") +
                                   "+BGPSGT: \"".length());
            response = response.substring(0, response.indexOf("\""));
        }
        catch (StringIndexOutOfBoundsException sioobe) {
            // No GPS data, I guess...
            // Return the empty map
            return props;
        }

        GPSLib.parseGPSData(response, props);

        return sanitize(props);
    }

    /**
     * Get the roaming status using the AT+CREG command. Supported on 1xRTT,
     * GSM, and HSPA modems for all firmware.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The roaming status as a descriptive, sanitized string.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     */
    public static String getRoamingStatus(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        // Roaming status
        comm.sendMessage("AT+CREG?");
        String response = comm.receiveMessage();

        try {
            String[] values =
                comm.parseValues(comm.stripResponse(comm.getLine(response,
                    "+CREG:")));

            int status = Integer.parseInt(values[1]);

            switch (status) {
                case 0:
                    return sanitize("No Service");
                case 1:
                    return sanitize("Home");
                case 2:
                    return sanitize("Searching");
                case 5:
                    return sanitize("Roaming");
                default:
                    return sanitize(("Unknown (" + status + ")"));
            }
        }
        catch (Exception e) {
            logger.warn("Unable to determine roaming status", e);
            return null;
        }
    }

    /**
     * Get the system uptime using the AT+BSUPTM command. Supported on all
     * models since fw 3.4.0.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The system uptime, in milliseconds.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     * @throws NumberFormatException If the format of the uptime was not
     *             recognized.
     */
    public static long getSystemUptime_after3_4_0(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        // Format examples:
        // 1970-01-18 00:48:31 Memory: 19304/29920 KB - 64% free - 51 processes
        // - loads: 0.00, 0.00, 0.00 - up 17 days 0 h 48 min 32 s
        // In this format, grab everything after " - up "

        // 18:41:31 up 3 days, 18:41, load average: 0.00, 0.00, 0.00
        // 00:09:59 up 9 min, load average: 0.00, 0.01, 0.00
        // In this format, grab the data up to the ','
        // Time is amount of time it was on today; 'up # XXX' may hold the
        // number of days it was up

        // Uptime
        comm.sendMessage("AT+BSUPTM?");
        String response = comm.receiveMessage();

        // Pull out the stuff between the quotes
        String uptime;

        try {
            response = response.substring(response.indexOf('"') + 1);
            response = response.substring(0, response.indexOf('"'));
            response = response.trim();

            if (response.indexOf(" - up ") != -1) {
                // Format 1
                uptime =
                    response.substring(response.indexOf(" - up ") +
                                       " - up ".length());
            }
            else {
                // Format 2
                // Pull out the beginning of the line up to the ','
                uptime = response.substring(0, response.indexOf(','));
            }
        }
        catch (StringIndexOutOfBoundsException sioobe) {
            logger.debug("Uptime format from AT+BSUPTM not recognized", sioobe);

            throw new NumberFormatException("Uptime format not recognized");
        }

        // Parse the uptime
        return comm.parseUptime(uptime);
    }

    /**
     * Get the RSSI using the AT+CSQ command. Documented as supported by all
     * models/firmware, although this is not true.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The RSSI in dBm.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     * @throws NumberFormatException If there was a problem parsing the RSSI.
     */
    public static float getRSSI(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        // RSSI
        comm.sendMessage("AT+CSQ?");
        String response = comm.receiveMessage();

        String[] values =
            comm.parseValues(comm
                .stripResponse(comm.getLine(response, "+CSQ:")));

        try {
            // Only interested in the RSSI
            float val = Float.parseFloat(values[0]);
            val = Conversion.toRSSI(val);

            return val;
        }
        catch (Exception e) {
            logger.debug("Unexpected error parsing RSSI from AT+CSQ", e);
            throw new NumberFormatException("Unexpected error parsing RSSI");
        }
    }

    /**
     * Get the Device ID from a modem using the AT+GSN command. Supported on all
     * models/firmware.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The device ID.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     * @throws NumberFormatException If there was a problem parsing the device
     *             ID.
     */
    public static DeviceID getDeviceID(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+GSN");
        String response = comm.receiveMessage();

        // Response may start with "+GSN:"; if so, use that line
        String line = comm.getLine(response, "+GSN:");

        if (line != null) {
            line = comm.stripResponse(line);
        }
        else {
            // It could also just start with numbers...
            String[] lines = Utils.getLines(response);

            for (String ln : lines) {
                if (ln.length() > 0) {
                    line = ln;
                    break;
                }
            }
        }

        if (line == null) {
            throw new NumberFormatException("Bad response from modem: '" +
                                            response + "'");
        }

        // No content, just "OK"?
        if (line.equalsIgnoreCase("OK")) {
            throw new NumberFormatException("Bad response from modem: '" +
                                            response + "'");
        }

        return DeviceID.parseDeviceID(line);
    }

    /**
     * Get the configuration version on the modem using the AT+BCFGV command.
     * Supported on all models since fw 3.6.1.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The sanitized communication version string, or <tt>null</tt> if
     *         it could not be retrieved or was blank.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     */
    public static String getCfgVersion_after3_6_1(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BCFGV?");
        String response = comm.receiveMessage();

        String cfgVersion =
            sanitize(comm.stripResponse(comm.getLine(response, "+BCFGV:")));

        if (cfgVersion.length() == 0) {
            return null;
        }

        return cfgVersion;
    }

    /**
     * Get the password on the modem using the AT+BRPSWD command. Supported on
     * all models since fw 3.4.0.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The password settings:
     *         <ul>
     *         <li>'Password/Enabled' = 0/1</li>
     *         <li>'Password/Access' = WAN/LAN/Both</li>
     *         <li>'Password/Password' = the password</li>
     *         <ul>
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     */
    public static Map<String, String> getPasswordSettings_after3_4_0(
                                                                     ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BRPSWD?");
        String response = comm.receiveMessage();
        response = comm.stripResponse(comm.getLine(response, "+BRPSWD:"));

        String[] values = comm.parseValues(response);

        // [0] = 0 (disabled) or 1 (enabled)
        // [1] = 0 (WAN), 1 (LAN and WAN), or 2 (LAN)
        // [2] = password

        Map<String, String> props = new HashMap<String, String>();

        try {
            props.put("Password/Enabled", values[0]);
            props.put("Password/Access", values[1]);
            props.put("Password/Password", values[2]);
        }
        catch (Exception e) {
            logger.debug(
                "Unexpected exception parsing password data from AT+BRPSWD", e);
        }

        return sanitize(props);
    }

    /**
     * Get version numbers from the modem, along with the serial number using
     * the AT+GMR command. Supported on all modems/firmware.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return A map of property names onto their values:
     *         <ul>
     *         <li>'Version/HW' = Hardware version</li>
     *         <li>'Version/FW' = Firmware version</li>
     *         <li>'Version/U-Boot' = U-Boot version</li>
     *         <li>'Version/GPS' = GPS module version</li>
     *         <li>'Version/RF' = RF Module version</li>
     *         <li>'Serial' = Serial number (returned by the same command)</li>
     *         </ul>
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static Map<String, String> getVersions(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        Map<String, String> props = new HashMap<String, String>();

        comm.sendMessage("AT+GMR");
        String response = comm.receiveMessage();
        response = comm.stripResponse(comm.getLine(response, "+GMR:"));

        // Pull out each piece
        // Search for specific version numbers that we expect to find
        // This string is not very predictable, unfortunately
        try {
            String hwVer =
                response.substring(response.indexOf("HW:") + "HW:".length());
            hwVer = hwVer.substring(0, hwVer.indexOf(',')).trim();
            props.put("Version/HW", hwVer);
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing HW version", e);
        }

        try {
            String fwVer =
                response.substring(response.indexOf("FW:") + "FW:".length());
            fwVer = fwVer.substring(0, fwVer.indexOf('(')).trim();
            props.put("Version/FW", fwVer);
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing FW version", e);
        }

        try {
            String ubootVer =
                response.substring(response.indexOf("U-Boot") +
                                   "U-Boot".length());
            ubootVer = ubootVer.substring(0, ubootVer.indexOf("FS")).trim();
            props.put("Version/U-Boot", ubootVer);
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing U-Boot version", e);
        }

        try {
            String serial =
                response.substring(response.indexOf("S/N:") + "S/N:".length());
            serial = serial.substring(0, serial.indexOf(',')).trim();
            props.put("Serial", serial);
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing Serial number", e);
        }

        try {
            String gpsVer =
                response.substring(response.indexOf("GPS:") + "GPS:".length());
            gpsVer = gpsVer.substring(0, gpsVer.indexOf(',')).trim();
            props.put("Version/GPS", gpsVer);
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing GPS version", e);
        }

        try {
            String rfVer =
                response.substring(response.indexOf("RF:") + "RF:".length());
            if (rfVer.indexOf("S/W") != -1) {
                rfVer = rfVer.substring(0, rfVer.indexOf("S/W")).trim();
            }
            else {
                rfVer = rfVer.substring(0, rfVer.indexOf(' ')).trim();
            }
            props.put("Version/RF", rfVer);
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing RF version", e);
        }

        return sanitize(props);
    }

    /**
     * Get modem information using the ATI1 command. Supported on all models
     * since fw 3.6.1.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return A map of names onto property values (any of these may be missing
     *         if the modem returns an empty string for it):
     *         <ul>
     *         <li>'Alias' = The name of the modem</li>
     *         <li>'Phone' = The phone number</li>
     *         <li>'Serial' = The serial number</li>
     *         <li>'Version/RF' = The RF module version</li>
     *         <li>'Version/HW' = The hardware version</li>
     *         <li>'Carrier' = The carrier name</li>
     *         </ul>
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static Map<String, String> getInfo_after3_6_1(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        Map<String, String> props = new HashMap<String, String>();

        comm.sendMessage("ATI1");
        String response = comm.receiveMessage();

        try {
            String name = comm.stripResponse(comm.getLine(response, "Name:"));

            // Cut off the trailing quote
            name = name.substring(0, name.length() - 1);
            if (name.length() > 0) {
                props.put("Alias", name);
            }
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing modem name", e);
        }

        try {
            String carrier =
                comm.stripResponse(comm.getLine(response, "Model:"));

            // If there's a carrier, pull it out and store it
            if (carrier.indexOf(' ') != -1) {
                carrier = carrier.substring(carrier.indexOf(' ') + 1);
                props.put("Carrier", carrier);
            }
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing carrier", e);
        }

        try {
            String phoneNumber =
                comm.stripResponse(comm.getLine(response, "Phone:"));
            if (phoneNumber.length() > 0) {
                props.put("Phone", phoneNumber);
            }
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing phone number", e);
        }

        try {
            String serialNumber =
                comm.stripResponse(comm.getLine(response, "S/N:"));
            if (serialNumber.length() > 0) {
                props.put("Serial", serialNumber);
            }
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing serial number", e);
        }

        try {
            String rfVer = comm.stripResponse(comm.getLine(response, "RF:"));
            if (rfVer.length() > 0) {
                props.put("Version/RF", rfVer);
            }
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing RF version", e);
        }

        try {
            String hwVer = comm.stripResponse(comm.getLine(response, "HW:"));
            if (hwVer.length() > 0) {
                props.put("Version/HW", hwVer);
            }
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing HW version", e);
        }

        // Not always present
        try {
            String prlVer = comm.stripResponse(comm.getLine(response, "PRL:"));
            if (prlVer != null && prlVer.length() > 0) {
                props.put("Version/PRL", prlVer);
            }
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing PRL version", e);
        }

        return sanitize(props);
    }

    /**
     * Get the phone number from a modem using the AT+CNUM command. Supported on
     * all models/firmware.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The sanitized phone number of the modem, or <tt>null</tt> if it
     *         responds with an empty string.
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     * @throws NumberFormatException If there is a problem parsing the phone
     *             number.
     */
    public static String getPhoneNumber(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+CNUM");
        String response = comm.receiveMessage();

        response = comm.stripResponse(comm.getLine(response, "+CNUM:"));

        // Format is "...","phone number",...
        try {
            String[] values = comm.parseValues(response);

            // No point returning an empty string
            if (values[1].length() == 0) {
                return null;
            }

            return sanitize(values[1]);
        }
        catch (Exception e) {
            logger.debug("Unexpected exception parsing phone number", e);

            throw new NumberFormatException(
                "Phone number not formatted properly");
        }
    }

    /**
     * Get the name of the modem using the AT+BMNAME command. Supported on all
     * models/firmware.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The sanitized name of the modem, or <tt>null</tt> if it responds
     *         improperly or with an empty string.
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static String getName(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BMNAME?");
        String response = comm.receiveMessage();

        String name =
            sanitize(comm.stripResponse(comm.getLine(response, "+BMNAME:")));

        if (name.length() == 0) {
            return null;
        }

        return name;
    }

    /**
     * Execute a TTL script (list of Terra-Term commands) line-by-line. Supports
     * the send/sendln and wait/waitln commands.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @param modem The modem being communicated with. Used to extract
     *            variables.
     * @param scriptLines The lines of the script.
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static void executeScript(ModemCommunicator comm, Modem modem,
                                     String[] scriptLines)
        throws ModemCommunicationFailedException
    {
        int lineNumber = 0;
        for (String command : scriptLines) {
            lineNumber++;

            // Could be a send, sendln, wait, or waitln
            // Send/sendln are equivalent, as are wait/waitln
            command = command.trim();

            if (command.startsWith("send ") || command.startsWith("sendln ")) {
                // It's one of the send commands
                // Cut off the beginning of the line and the quotes around
                // the command
                // Could be either type of quotes...
                int sq = command.indexOf('\'');
                int dq = command.indexOf('"');

                if (sq == -1) {
                    sq = Integer.MAX_VALUE;
                }
                if (dq == -1) {
                    dq = Integer.MAX_VALUE;
                }

                int lsq = command.lastIndexOf('\'');
                int ldq = command.lastIndexOf('"');

                if (sq < dq) {
                    // Single quotes win out
                    if (lsq == sq) {
                        throw new ModemCommunicationFailedException(
                            "Missing end quote in script line " + lineNumber);
                    }

                    command = command.substring(sq + 1, lsq);
                }
                else if (dq < sq) {
                    // Double quotes
                    if (ldq == dq) {
                        throw new ModemCommunicationFailedException(
                            "Missing end quote in script line " + lineNumber);
                    }

                    command = command.substring(dq + 1, ldq);
                }
                else {
                    // No quotes
                    // This is an error
                    throw new ModemCommunicationFailedException(
                        "Send command has no quoted arguments on script line " +
                            lineNumber);
                }

                comm.sendMessage(command);
            }
            else if (command.startsWith("wait ") ||
                     command.startsWith("waitln ")) {

                // It's one of the wait commands
                // Cut off the beginning of the line and get a list of the
                // possible responses
                // Responses are surrounded by quotes and are separated by
                // spaces
                command = command.substring(command.indexOf(' ') + 1).trim();
                String[] waitStrings = Utils.tokenize(command, " ", false);
                List<String> legalResponses = Utils.makeVector(waitStrings);

                // Read a message from the modem
                String message = comm.receiveMessage();

                // Check for legal responses
                boolean found = false;
                for (String s : legalResponses) {
                    if (comm.getLine(message, s) != null) {
                        // Found one, we can continue
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    throw new ModemCommunicationFailedException(
                        "Illegal response from modem: " + message);
                }
            }
            else {
                throw new ModemCommunicationFailedException(
                    "Illegal script command: " + command);
            }
        }
    }

    /**
     * Upload a package to the modem via FTP. On modems running firmware since
     * 3.4.8, the FTP server will not accept connections unless there is an
     * active connection to port 5070 or 6070.
     * 
     * @param modem The modem to upload to.
     * @param image The package to upload.
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static void uploadPackage(Modem modem, BlueTreeImage image)
        throws ModemCommunicationFailedException
    {
        // Open an FTP connection to the modem
        final FTPClient client = new FTPClient();

        try {
            logger.debug("Connecting to modem via FTP");
            client.connect(InetAddress.getByName(modem.ipAddress));
        }
        catch (IOException ioe) {
            logger.error("Unable to connect to modem", ioe);

            throw new ModemCommunicationFailedException(
                "Unable to connect to modem: " + ioe.getMessage(), ioe);
        }

        try {
            int reply = client.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                throw new ModemCommunicationFailedException(
                    "Modem FTP server refused connection");
            }

            // Log in and upload the image
            try {
                logger.debug("Logging into FTP");
                if (!client.login("ftp", "ftp")) {
                    throw new IOException("Bad FTP username/password");
                }

                // Set up a small thread to watch for a timeout sending the file
                Thread th = new Thread(new Runnable() {
                    private long endTime =
                        System.currentTimeMillis() +
                            Server.getServerSettings().getIntValue(
                                Setting.FTPTimeout) * 1000L;

                    public void run()
                    {
                        while (client.isConnected()) {
                            long sleepTime =
                                endTime - System.currentTimeMillis();

                            if (sleepTime > 0) {
                                Utils.sleep(sleepTime);
                            }
                            else {
                                break;
                            }
                        }

                        if (client.isConnected()) {
                            try {
                                client.abort();
                            }
                            catch (IOException ioe) {
                                logger
                                    .warn(
                                        "Unable to abort FTP transfer after timeout",
                                        ioe);
                            }

                            try {
                                client.disconnect();
                            }
                            catch (IOException ioe) {
                                logger
                                    .warn(
                                        "Unable to disconnect from modem FTP server after timeout",
                                        ioe);
                            }
                        }
                    }
                }, "FTP Timeout Watcher");

                logger.debug("Uploading file");
                client.setFileType(FTP.BINARY_FILE_TYPE);

                th.start(); // It will kill itself when we disconnect the client
                client.storeFile("/btfw.bin", new ByteArrayInputStream(image
                    .getPackageData()));
            }
            catch (IOException ioe) {
                logger.error("Unable to upload package", ioe);

                throw new ModemCommunicationFailedException(
                    "Unable to upload package: " + ioe.getMessage(), ioe);
            }
        }
        finally {
            // Disconnect
            if (client.isConnected()) {
                try {
                    logger.debug("Disconnecting FTP");
                    client.disconnect(); // Kills the timeout watcher thread
                }
                catch (IOException ioe) {
                    logger.warn("Unable to disconnect the FTP client", ioe);

                    // Ignore the error, the image was already uploaded
                }
            }
        }
    }

    /**
     * Perform an update from a package using the AT+BFWUPS=1/0 sequence.
     * 
     * @param comm The ModemCommunicator connected to the modem. If this
     *            supports the {@link ModemCommunicator#connect(Modem)} method,
     *            it will be called regularly until the connection is
     *            re-established. If it does not support the
     *            {@link ModemCommunicator#connect(Modem)} method, it will
     *            simply return.
     * @param modem The modem to communicate with.
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static void performPackageUpdate(ModemCommunicator comm, Modem modem)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BFWUPS=1");
        String response = comm.receiveMessage();

        String line = comm.getLine(response, "OK");
        if (line == null || !line.equals("OK")) {
            logger.error(String.format(
                "Modem responded to AT+BFWUPS=1 with '%s'", response));

            throw new ModemCommunicationFailedException(
                "Bad response from modem: '" + response + "'");
        }

        if (comm.connectSupported()) {
            comm.close();

            // Wait for a while, so the modem can perform the upgrade
            Utils.sleep(90000); // 1.5 minutes

            // Start trying to make a new connection to the modem
            // Allow for 8 minutes of attempts, then fail
            long endTime = System.currentTimeMillis() + (60000 * 8);

            while (!comm.isConnected() && System.currentTimeMillis() < endTime) {
                try {
                    comm.connect(modem);
                }
                catch (ModemCommunicationFailedException mcfe) {
                    // Ignore it
                    logger.info("Failed to connect during firmware load", mcfe);

                    Utils.sleep(20000);
                }
            }

            if (!comm.isConnected()) {
                // Never managed to connect
                throw new ModemCommunicationFailedException(
                    "Unable to connect to modem after 6 minutes");
            }
            else {
                // Complete the firmware update
                comm.sendMessage("AT+BFWUPS=0");

                response = comm.receiveMessage();
                line = comm.getLine(response, "OK");
                if (line == null || !line.equals("OK")) {
                    logger.error(String.format(
                        "Modem responded to AT+BFWUPS=0 with '%s'", response));

                    throw new ModemCommunicationFailedException(
                        "Bad response from modem: '" + response + "'");
                }
            }
        }
    }

    /**
     * Perform a package update using the AT+BFWUPS=3 command. Supported on all
     * models since fw 3.6.1.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @param modem The modem to update.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     */
    public static void performPackageUpdate_after3_6_1(ModemCommunicator comm,
                                                       Modem modem)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BFWUPS=3");

        // This may time out, accept that as a normal behavior
        String response = null;
        try {
            response = comm.receiveMessage();
        }
        catch (IOException ioe) {
            logger.info("Failed to receive response to AT+BFWUPS=3, assuming"
                        + " the modem rebooted itself");
        }

        if (response != null) {
            String line = comm.getLine(response, "OK");
            if (line == null) {
                logger.error(String.format(
                    "Modem responded to AT+BFWUPS=3 with '%s'", response));

                throw new ModemCommunicationFailedException(
                    "Bad response from modem: '" + response + "'");
            }
        }
    }

    /**
     * Get network status information for a CDMA modem using the AT+BNSTAT
     * command. Supported on CDMA modems for firmware versions.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return A map of names onto property values:
     *         <ul>
     *         <li>'RSSI' = The Received Signal Strength Indicator in dBm</li>
     *         <li>'Roaming Status' = The roaming status of the modem</li>
     *         <li>'RF State' = The RF module state</li>
     *         <li>'Service Availability' = What service is available</li>
     *         <li>'Service In Use' = What service is being used</li>
     *         <li>'Frequency' = The frequency (cellular or PCS) being used</li>
     *         <li>'Channel' = The channel ID number</li>
     *         <li>'SID' = The system ID number</li>
     *         <li>'NID' = The network ID number</li>
     *         <li>'PN Offset' = Pilot pseudo-noise offset</li>
     *         <li>'Base Station' = The base station ID (always N/A since it
     *         takes like 5 minutes to request it)</li>
     *         <li>'Slot Cycle Index' = The slot cycle index</li>
     *         <li>'Ec/Io' = The Ec/Io</li>
     *         <li>'Tx Pwr' = The transmit power (in dBm)</li>
     *         <li>'Tx Adj' = The transmit adjustment (in dBm)</li>
     *         <li>'FER' = The Frame Error Rate</li>
     *         <li>'Network Time' = The network time/date as milliseconds since
     *         the epoch, as if from the GMT timezone. It may be a local
     *         timezone, however, which may cause this to be off by a few hours,
     *         as there is no way to determine what the local timezone is.</li>
     *         </ul>
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static Map<String, String> getNetworkStatus_CDMA(
                                                            ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BNSTAT");
        String response = comm.receiveMessage();

        response = comm.stripResponse(comm.getLine(response, "+BNSTAT:"));
        if (response == null) {
            return new HashMap<String, String>();
        }

        String[] vals = comm.parseValues(response);

        Map<String, String> props = new HashMap<String, String>();

        try {
            // In dBm
            props.put("RSSI", vals[0]);

            {
                // 0 = not registered
                // 1 = home
                // 2 = roaming

                final String key = "Roaming Status";
                final int i = 1;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 0:
                            props.put(key, "Not Registered");
                            break;
                        case 1:
                            props.put(key, "Home");
                            break;
                        case 2:
                            props.put(key, "Roaming");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            {
                // 0 = no service
                // 1 = idle
                // 2 = dormant
                // 3 = in-use
                // 4 = incoming
                // 5 = calling

                final String key = "RF State";
                final int i = 2;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 0:
                            props.put(key, "No Service");
                            break;
                        case 1:
                            props.put(key, "Idle");
                            break;
                        case 2:
                            props.put(key, "Dormant");
                            break;
                        case 3:
                            props.put(key, "In Use");
                            break;
                        case 4:
                            props.put(key, "Incoming");
                            break;
                        case 5:
                            props.put(key, "Calling");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            {
                // 1 = PCS
                // 2 = IS-95
                // 3 = IS-95A
                // 4 = IS-95B
                // 5 = IS-95B
                // 6 = CDMA Rev. 0 (1xRTT)
                // 7 = CDMA Rev. 1
                // Sierra modules on BT-x600 modems will return "N/A"

                final String key = "Service Availability";
                final int i = 3;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 1:
                            props.put(key, "PCS");
                            break;
                        case 2:
                            props.put(key, "IS-95");
                            break;
                        case 3:
                            props.put(key, "IS-95A");
                            break;
                        case 4:
                            props.put(key, "IS-95B");
                            break;
                        case 5:
                            props.put(key, "IS-95B");
                            break;
                        case 6:
                            props.put(key, "CDMA Rev. 0 (1xRTT)");
                            break;
                        case 7:
                            props.put(key, "CDMA Rev. 1");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    if (vals[i].equals("N/A")) {
                        props.put(key, vals[i]);
                    }
                    else {
                        props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
            }

            {
                // 1 = PCS
                // 2 = IS-95
                // 3 = IS-95A
                // 4 = IS-95B
                // 5 = IS-95B
                // 6 = CDMA Rev. 0 (1xRTT)
                // 7 = CDMA Rev. 1
                // 81-87 = EVDO Rev-0
                // 91-97 = EVDO Rev-A

                final String key = "Service In Use";
                final int i = 4;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 1:
                            props.put(key, "PCS");
                            break;
                        case 2:
                            props.put(key, "IS-95");
                            break;
                        case 3:
                            props.put(key, "IS-95A");
                            break;
                        case 4:
                            props.put(key, "IS-95B");
                            break;
                        case 5:
                            props.put(key, "IS-95B");
                            break;
                        case 6:
                            props.put(key, "CDMA Rev. 0 (1xRTT)");
                            break;
                        case 7:
                            props.put(key, "CDMA Rev. 1");
                            break;
                        case 81:
                        case 82:
                        case 83:
                        case 84:
                        case 85:
                        case 86:
                        case 87:
                            props.put(key, "EVDO Rev-0");
                            break;
                        case 91:
                        case 92:
                        case 93:
                        case 94:
                        case 95:
                        case 96:
                        case 97:
                            props.put(key, "EVDO Rev-A");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            {
                // 0 = Cellular (800 MHz)
                // 1 = PCS (1900 MHz)

                final String key = "Frequency";
                final int i = 5;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 0:
                            props.put(key, "Cellular (800 MHz)");
                            break;
                        case 1:
                            props.put(key, "PCS (1900 MHz)");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            // Channel ID number
            props.put("Channel", vals[6]);

            // System ID number
            props.put("SID", vals[7]);

            // Network ID number
            props.put("NID", vals[8]);

            // Pilot pseudo-noise offset
            props.put("PN Offset", vals[9]);

            // Base station ID number (always N/A)
            props.put("Base Station", vals[10]);

            // Slot cycle index
            props.put("Slot Cycle Index", vals[11]);

            // Ec/Io
            props.put("Ec/Io", vals[12]);

            // Tx power in dBm
            props.put("Tx Pwr", vals[13]);

            // Tx Adj in dBm
            props.put("Tx Adj", vals[14]);

            // Frame error rate
            props.put("FER", vals[15]);

            // Network date/time in milliseconds since the epoch, or 0 if the
            // format is bad
            try {
                DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd");
                DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

                Date date = dateFormat.parse(vals[16]);
                Date time = timeFormat.parse(vals[17]);

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                // It's okay to cast, since the maximum value for a single day
                // is
                // 104,399,999, which is less than the 2G limit on integers
                cal.add(Calendar.MILLISECOND, (int)time.getTime());

                props.put("Network Time", "" + cal.getTimeInMillis());
            }
            catch (ParseException pe) {
                logger.debug("Unable to parse network date/time", pe);
            }
        }
        catch (Exception e) {
            // Only likely exception is an ArrayIndexOutOfBoundsException, which
            // would indicate that we had gone as far through the list as
            // possible and there's nothing left
            logger.debug("Unexpected exception parsing results from AT+BNSTAT",
                e);
        }

        // Whatever we gathered before the exception is included here
        return sanitize(props);
    }

    /**
     * Get network status information for a GSM modem using the AT+BNSTAT
     * command. Supported on GSM modems for firmware versions.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return A map of names onto property values:
     *         <ul>
     *         <li>'RSSI' = The Received Signal Strength Indicator in dBm</li>
     *         <li>'BER' = Bit Error Rate</li>
     *         <li>'MCC' = Mobile Country Code</li>
     *         <li>'MNC' = Mobile Network Code</li>
     *         <li>'LAC' = Location Area Code</li>
     *         <li>'CI' = Cell Identifier</li>
     *         <li>'BSIC' = Base Station Identification Code</li>
     *         <li>'BCCH Freq' = Broadcast Country Channel Absolute</li>
     *         <li>'RxLev' = Received Signal Level</li>
     *         <li>'RxLev Full' = Received Signal Level (average with DTX
     *         disabled)</li>
     *         <li>'RxLev Sub' = Received Signal Level (average with DTX
     *         enabled)</li>
     *         <li>'RxQual' = Received Signal Quality</li>
     *         <li>'RxQual Full' = Received Signal Quality (average with DTX
     *         disabled)</li>
     *         <li>'RxQual Sub' = Received Signal Quality (average with DTX
     *         enabled)</li>
     *         <li>'Idle TS' = Idle timeslots (for potential signal handoffs)</li>
     *         <li>'CREG Mode' = Network registration result code info</li>
     *         <li>'CREG Stat' = Network registration</li>
     *         <li>'COPS Mode' = Format of COPS</li>
     *         <li>'COPS Format' = Format of COPS</li>
     *         <li>'COPS Status' = Current status of oper</li>
     *         <li>'CGATT State' = CGATT state</li>
     *         </ul>
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static Map<String, String> getNetworkStatus_GSM(
                                                           ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BNSTAT");
        String response = comm.receiveMessage();

        response = comm.stripResponse(comm.getLine(response, "+BNSTAT:"));
        if (response == null) {
            return new HashMap<String, String>();
        }

        String[] vals = comm.parseValues(response);

        Map<String, String> props = new HashMap<String, String>();

        try {
            // In dBm
            props.put("RSSI", vals[0]);

            // Bit Error Rate
            props.put("BER", vals[1]);

            // Mobile Country Code
            props.put("MCC", vals[2]);

            // Mobile Network Code
            props.put("MNC", vals[3]);

            // Location Area Code
            props.put("LAC", vals[4]);

            // Cell Identifier
            props.put("CI", vals[5]);

            // Base Station Identification Code
            props.put("BSIC", vals[6]);

            // Broadcast Control Channel Absolute
            props.put("BCCH Freq", vals[7]);

            // Received Signal Level (average)
            props.put("RxLev", vals[8]);

            // Received Signal Level (average with DTX disabled)
            props.put("RxLev Full", vals[9]);

            // Received Signal Level (average with DTX enabled)
            props.put("RxLev Sub", vals[10]);

            // Received Signal Quality
            props.put("RxQual", vals[11]);

            // Received Signal Quality (average with DTX disabled)
            props.put("RxQual Full", vals[12]);

            // Received Signal Quality (average with DTX enabled)
            props.put("RxQual Sub", vals[13]);

            // Idle timeslots, used to scan neighboring cells for potential
            // handoffs
            props.put("Idle TS", vals[14]);

            {
                // 0 = Disable network registration unsolicited result code
                // 1 = Enable network registration code result code +CREG :
                // <stat>
                // 2 = Enable network registration and location information
                // unsolicited
                // result code +CREG: <stat>, <lac>, <ci> if there is a change
                // of
                // network cell

                final String key = "CREG Mode";
                final int i = 15;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 0:
                            props
                                .put(key,
                                    "Disable network registration unsolicited result code");
                            break;
                        case 1:
                            props
                                .put(key,
                                    "Enable network registration code result code +CREG : [stat]");
                            break;
                        case 2:
                            props
                                .put(key,
                                    "Enable network registration and location inforation unsolicited result code");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            {
                // 0 = Not registered, MS is not currently searching for a new
                // operator
                // 1 = Registered, home network
                // 2 = Not registered, MS currently searching for a base station
                // 4 = Unknown
                // 5 = Registered, roaming

                final String key = "CREG Stat";
                final int i = 16;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 0:
                            props
                                .put(key,
                                    "Not registered, not currently searching for a new operator");
                            break;
                        case 1:
                            props.put(key, "Registered, home network");
                            break;
                        case 2:
                            props
                                .put(key,
                                    "Not registered, currently searching for a base station");
                            break;
                        case 5:
                            props.put(key, "Registered, roaming");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            {
                // 0 = Automatic
                // 1 = Use long format alphanumeric <oper>
                // 2 = Use short format alphanumeric <oper>
                // 3 = Use numeric <oper>

                final String key = "COPS Mode";
                final int i = 17;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 0:
                            props.put(key, "Automatic");
                            break;
                        case 1:
                            props.put(key,
                                "Use long format alphanumeric [oper]");
                            break;
                        case 2:
                            props.put(key,
                                "Use short format alphanumeric [oper]");
                            break;
                        case 3:
                            props.put(key, "Use numeric [oper]");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            {
                // 0 = Long alphanumeric format <oper>
                // 1 = Short alphanumeric format <oper>
                // 2 = Numeric <oper>

                final String key = "COPS Format";
                final int i = 18;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 0:
                            props.put(key, "Long alphanumeric format [oper]");
                            break;
                        case 1:
                            props.put(key, "Short alphanumeric format [oper]");
                            break;
                        case 2:
                            props.put(key, "Numeric [oper]");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            {
                // 0 = Unknown
                // 1 = Available
                // 2 = Current
                // 3 = Forbidden

                final String key = "COPS Status";
                final int i = 19;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 1:
                            props.put(key, "Available");
                            break;
                        case 2:
                            props.put(key, "Current");
                            break;
                        case 3:
                            props.put(key, "Forbidden");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            {
                // 0 = Detached
                // 1 = Attached
                // 2 = Combined Detach (GPRS and GSM Detach performed in the
                // same
                // network request)

                final String key = "CGATT State";
                final int i = 20;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 0:
                            props.put(key, "Detached");
                            break;
                        case 1:
                            props.put(key, "Attached");
                            break;
                        case 2:
                            props.put(key, "Combined detach");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }
        }
        catch (Exception e) {
            // Only likely exception in an ArrayIndexOutOfBoundsException, which
            // would indicate that we had gone as far through the list as
            // possible and there's nothing left
            logger.debug("Unexpected exception parsing results from AT+BNSTAT",
                e);
        }

        // Whatever we gathered before the exception is included here
        return sanitize(props);
    }

    /**
     * Get network status information for a HSPA modem using the AT+BNSTAT
     * command. Supported on HSPA modems for firmware versions.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return A map of names onto property values:
     *         <ul>
     *         <li>'RSSI' = The Received Signal Strength Indicator in dBm</li>
     *         <li>'Roaming Status' = No service/home/roaming</li>
     *         <li>'RF State' = Always N/A</li>
     *         <li>'Service Availability' = Always N/A</li>
     *         <li>'Service In Use' = Current service in use</li>
     *         <li>'Band' = Frequency band being used</li>
     *         <li>'Channel' = Channel being used</li>
     *         <li>'Network String' = Network string</li>
     *         <li>'MNC' = Mobile network code</li>
     *         <li>'PN Offset' = Always N/A</li>
     *         <li>'Cell ID' = Cell ID</li>
     *         <li>'Slot Cycle Index' = Always N/A</li>
     *         <li>'Ec/Io' = Ec/Io measurement in -dB, or N/A for GPRS and EDGE</li>
     *         <li>'Tx Pwr' = Always N/A</li>
     *         <li>'Tx Adj' = Always N/A</li>
     *         <li>'BER' = Bit error rate for GPRS and EDGE, always N/A for
     *         HSDPA and HSUPA</li>
     *         <li>'Network Time' = Network date/time in milliseconds since the
     *         epoch</li>
     *         </ul>
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     */
    public static Map<String, String> getNetworkStatus_HSPA_after3_6_0(
                                                                       ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BNSTAT");
        String response = comm.receiveMessage();

        response = comm.stripResponse(comm.getLine(response, "+BNSTAT:"));
        if (response == null) {
            return new HashMap<String, String>();
        }

        String[] vals = comm.parseValues(response);

        Map<String, String> props = new HashMap<String, String>();

        try {
            // In dBm
            Float rssi = Float.parseFloat(vals[0]);
            rssi = Conversion.toRSSI(rssi);
            props.put("RSSI", "" + rssi);

            {
                // 0 = Not registered
                // 1 = Registered, home
                // 2 = Registered, roaming

                final String key = "Roaming Status";
                final int i = 1;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 0:
                            props.put(key, "Not registered");
                            break;
                        case 1:
                            props.put(key, "Registered, home");
                            break;
                        case 2:
                            props.put(key, "Registered, roaming");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            // Always N/A
            props.put("RF State", vals[2]);

            // Always N/A
            props.put("Service Availability", vals[3]);

            {
                // 100 = None
                // 101 = GPRS
                // 102 = EDGE
                // 103 = UMTS
                // 104 = HSDPA
                // 105 = HSUPA
                // 106 = HSPA (HSDPA + HSUPA)

                final String key = "Service In Use";
                final int i = 4;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 100:
                            props.put(key, "None");
                            break;
                        case 101:
                            props.put(key, "GPRS");
                            break;
                        case 102:
                            props.put(key, "EDGE");
                            break;
                        case 103:
                            props.put(key, "UMTS");
                            break;
                        case 104:
                            props.put(key, "HSDPA");
                            break;
                        case 105:
                            props.put(key, "HSUPA");
                            break;
                        case 106:
                            props.put(key, "HSPA");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            {
                // 000 = No service
                // 001 = GSM 850
                // 002 = GSM 900
                // 003 = GSM 1800
                // 004 = GSM 1900
                // 101 = WCDMA 2100
                // 102 = WCDMA 1900
                // 103 = WCDMA 850
                // 104 = WCDMA 800
                // 105 = WCDMA 1800
                // 106 = WCDMA 1700 (US)
                // 107 = WCDMA 2600
                // 108 = WCDMA 900
                // 109 = WCDMA 1700 (Japan)
                // 10A=FFFE = Reserved
                // FFFF = Invalid band

                final String key = "Band";
                final int i = 5;
                try {
                    int v = Integer.parseInt(vals[i]);
                    switch (v) {
                        case 0:
                            props.put(key, "No service");
                            break;
                        case 1:
                            props.put(key, "GSM 850");
                            break;
                        case 2:
                            props.put(key, "GSM 900");
                            break;
                        case 3:
                            props.put(key, "GSM 1800");
                            break;
                        case 4:
                            props.put(key, "GSM 1900");
                            break;
                        case 101:
                            props.put(key, "WCDMA 2100");
                            break;
                        case 102:
                            props.put(key, "WCDMA 1900");
                            break;
                        case 103:
                            props.put(key, "WCDMA 850");
                            break;
                        case 104:
                            props.put(key, "WCDMA 800");
                            break;
                        case 105:
                            props.put(key, "WCDMA 1800");
                            break;
                        case 106:
                            props.put(key, "WCDMA 1700 (US)");
                            break;
                        case 107:
                            props.put(key, "WCDMA 2600");
                            break;
                        case 108:
                            props.put(key, "WCDMA 900");
                            break;
                        case 109:
                            props.put(key, "WCDMA 1700 (Japan)");
                            break;
                        case 65535:
                            props.put(key, "(Invalid)");
                            break;
                        default:
                            props.put(key, "(Unknown: " + vals[i] + ")");
                    }
                }
                catch (NumberFormatException nfe) {
                    props.put(key, "(Unknown: " + vals[i] + ")");
                }
            }

            // Channel number
            props.put("Channel", vals[6]);

            // Network string
            props.put("Network String", vals[7]);

            // Mobile Network Code
            props.put("MNC", vals[8]);

            // Pilot pseudo-noise offset (always N/A)
            props.put("PN Offset", vals[9]);

            // Cell ID
            props.put("Cell ID", vals[10]);

            // Slot cycle index (always N/A)
            props.put("Slot Cycle Index", vals[11]);

            // Ec/Io measurement in -dB (in half-decibel increments, so 1 = -0.5
            // dB, 21 = -21.5 dB)
            // N/A for GPRS and EDGE
            props.put("Ec/Io", vals[12]);

            // Transmit power (always N/A)
            props.put("Tx Pwr", vals[13]);

            // Transmit adjustment (always N/A)
            props.put("Tx Adj", vals[14]);

            // Bit error rate for GPRS and EDGE
            // Always N/A for HSDPA and HSUPA
            props.put("BER", vals[15]);

            // Network date/time in milliseconds since the epoch, or 0 if the
            // format
            // is bad
            try {
                DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd");
                DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

                // By parsing in GMT, the millisecond value stored in the date
                // that
                // is parsed out of the string will be correct, since the string
                // is
                // in GMT and not local time
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                timeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

                Date date = dateFormat.parse(vals[16]);
                Date time = timeFormat.parse(vals[17]);

                // By sticking that millisecond value into the calendar, we can
                // easily add the two pieces
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                // It's okay to cast to an int, since the maximum value for a
                // single
                // day is 104,399,999, which is less than the 2G limit on
                // integers
                cal.add(Calendar.MILLISECOND, (int)time.getTime());

                props.put("Network Time", "" + cal.getTimeInMillis());
            }
            catch (ParseException pe) {
                logger.debug("Unable to parse network date/time", pe);

                props.put("Network Time", "0");
            }
        }
        catch (Exception e) {
            // Only likely exception in an ArrayIndexOutOfBoundsException, which
            // would indicate that we had gone as far through the list as
            // possible and there's nothing left
            logger.debug("Unexpected exception parsing results from AT+BNSTAT",
                e);
        }

        // Whatever we gathered before the exception is included here
        return sanitize(props);
    }

    /**
     * Sanitize all values in the given map so that they can be stored in XML or
     * a similar tag-based language. Converts less-than and greater-than signs
     * to their HTML-escaped versions.
     * 
     * @param values The map of values to sanitize. This will be modified.
     * @return The input map, after sanitizing (just for convenience).
     */
    public static Map<String, String> sanitize(Map<String, String> values)
    {
        for (Map.Entry<String, String> entry : values.entrySet()) {
            String s = entry.getValue();

            if (s == null) {
                continue;
            }

            s = sanitize(s);

            values.put(entry.getKey(), s);
        }

        return values;
    }

    /**
     * Sanitize an individual string (see {@link #sanitize(Map)} for details).
     * 
     * @param s The string to sanitize.
     * @return The sanitized string, or <tt>null</tt> if <tt>s == null</tt>.
     */
    public static String sanitize(String s)
    {
        if (s == null) {
            return null;
        }

        s = s.replaceAll("<", "&lt;");
        s = s.replaceAll(">", "&gt;");

        return s;
    }

    /**
     * Detect whether the modem is running on the GSM network.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return True = the modem is on GSM, false = probably on CDMA.
     */
    public static boolean detectGSM(ModemCommunicator comm)
    {
        // GSM returns about 20 values for AT+BNSTAT, CDMA/HSPA return about 17
        try {
            comm.sendMessage("AT+BNSTAT");
            String response = comm.receiveMessage();

            String[] values = comm.parseValues(response);

            if (values.length > 18) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (Exception e) {
            // Fall back to CDMA, since that's the majority of BlueTree's
            // business
            return false;
        }
    }

    /**
     * Enables the FTP server in the modem using the AT+BFTPE command.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static void enableFTP_after3_7_0(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        logger.debug("Enabling the TFTP server");

        comm.sendMessage("AT+BFTPE=1");
        String response = comm.receiveMessage();

        if (comm.getLine(response, "OK") == null) {
            throw new ModemCommunicationFailedException(
                "Bad response to 'AT+BFTPE=1' from modem: " + response);
        }
    }

    /**
     * Disables the FTP server in the modem using the AT+BFTPE command.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static void disableFTP_after3_7_0(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        logger.debug("Disabling the TFTP server");

        comm.sendMessage("AT+BFTPE=0");
        String response = comm.receiveMessage();

        if (comm.getLine(response, "OK") == null) {
            throw new ModemCommunicationFailedException(
                "Bad response to 'AT+BFTPE=0' from modem: " + response);
        }
    }

    /**
     * Detect whether there is a GPS unit in the modem using the AT+BGPSGT?
     * command and watching for an empty result string.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return True = the modem supports GPS, False = the modem does not support
     *         GPS or has no GPS information to report. Usually, a modem will
     *         respond with data containing nothing useful if it has a GPS unit,
     *         but a modem without a GPS unit will respond with a blank string.
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static boolean detectGPS(ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BGPSGT?");
        String response = comm.receiveMessage();
        String line = comm.getLine(response, "+BGPSGT");

        if (line == null || line.length() == 0 || line.equals("+BGPSGT: \"\"")) {

            logger.debug("Modem does not have a GPS module");

            return false;
        }
        else {
            logger.debug("Modem has a GPS module");

            return true;
        }
    }

    /**
     * Tell a modem to download an update package using the AT+BWGET command.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @param uri The URI of the file to download.
     * @throws ModemCommunicationFailedException If there was a problem
     *             communicating with the modem.
     */
    public static void downloadPackage_after3_8_0(ModemCommunicator comm,
                                                  String uri)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BWGET=\"" + uri + "\"");

        // Wait for the response
        String response = comm.receiveMessage();
        if (comm.getLine(response, "OK") == null) {
            throw new ModemCommunicationFailedException(
                "Bad response to 'AT+BWGET=\"" + uri + "\"' from modem: " +
                    response);
        }

        return;
    }

    /**
     * Request the list of report destinations from a modem using the AT+BRPRDS
     * command.
     * 
     * @param comm The ModemCommunicator connected to the modem.
     * @return The list of report destinations configured in the modem.
     * @throws ModemCommunicationFailedException If there is a problem
     *             communicating with the modem.
     */
    public static List<ReportDestination> getReportDestinations_after2_0_3(
                                                                           ModemCommunicator comm)
        throws ModemCommunicationFailedException
    {
        comm.sendMessage("AT+BRPRDS?");
        String response = comm.receiveMessage();

        // Split out into lines
        List<String> lines = comm.getLines(response, "+BRPRDS:");

        List<ReportDestination> destinations =
            new ArrayList<ReportDestination>(lines.size());
        for (String line : lines) {
            String[] pieces = comm.parseValues(comm.stripResponse(line));

            try {
                destinations.add(new ReportDestination(pieces));
            }
            catch (Exception e) {
                // NumberFormatException, ArrayIndexOutOfBoundsException, ...
                // Basically, the line was mal-formed, skip it
                logger.debug("Bad response line from AT+BRPRDS?: " + line, e);
            }
        }

        return destinations;
    }
}

/*
 * ModemFW_3_4_9.java
 *
 * Provides a standard implementation for interfacing with a modem running 3.4.9 firmware.
 *
 * Jonathan Pearson
 * February 23, 2009
 *
 */

package com.sixnetio.BVB.Modems;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import com.sixnetio.BVB.Common.BlueTreeImage;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public abstract class ModemFW_3_4_9 extends ModemModule {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public ModemFW_3_4_9(String model, Version fwVersion) {
		super(model, fwVersion);
	}
	
	@Override
	public Map<String, String> getStatus(ModemCommunicator comm, Modem modem)
	        throws ModemCommunicationFailedException {
		
		NDC.push(String.format("model = '%s', fwver = '%s'", getModel(), getFWVersion()));
		
		try {
			Map<String, String> props = new Hashtable<String, String>();
			
			// Request:
			// - Analog inputs
			props.putAll(ModemLib.getAnalogInputs_after2_0_3(comm));
			
			// - Discrete inputs
			props.putAll(ModemLib.getDiscreteInputs_after2_0_3(comm));
			
			// Avoid calling this function too many times, it may query the
			// modem
			boolean cdma = isCDMA(comm);
			if (cdma) {
				Map<String, String> tempProps = ModemLib.getNetworkStatus_CDMA(comm);
				
				// Don't want to put all of that in, most of it is pretty ugly
				try {
					// - RSSI
					props.put("RSSI", tempProps.get("RSSI"));
				} catch (Exception e) {
					// Ignore it
				}
				
				try {
					// - Roaming status
					props.put("Roaming Status", tempProps.get("Roaming Status"));
				} catch (Exception e) {
					// Ignore it
				}
				
				try {
					// - Service availability
					props.put("Service Availability", tempProps.get("Service Availability"));
				} catch (Exception e) {
					// Ignore it
				}
				
				try {
					// - Network time
					props.put("Network Time", tempProps.get("Network Time"));
				} catch (Exception e) {
					// Ignore it
				}
			} else {
				Map<String, String> tempProps = ModemLib.getNetworkStatus_GSM(comm);
				
				// Don't want to put all of that in, most of it is pretty ugly
				try {
					// - RSSI
					props.put("RSSI", tempProps.get("RSSI"));
				} catch (Exception e) {
					// Ignore it
				}
				
				try {
					// - Roaming status
					props.put("Roaming Status", tempProps.get("CREG Stat"));
				} catch (Exception e) {
					// Ignore it
				}
				
				// - Service availability
				// - Network time
				// Unavailable on this type of modem
			}
			
			try {
				// - Uptime
				props.put("Uptime", "" + ModemLib.getSystemUptime_after3_4_0(comm));
			} catch (Exception e) {
				if (e instanceof ModemCommunicationFailedException) {
					throw (ModemCommunicationFailedException)e;
				}
				
				// Otherwise, log and ignore it
				logger.debug("Unexpected exception getting uptime from modem", e);
			}
			
			// - Latitude
			// - Longitude
			// - Ground speed
			// - Heading
			// - Visible satellite count
			// ...
			if (supportsGPS(comm)) {
				props.putAll(ModemLib.getGPSData(comm));
			}
			
			return props;
		} finally {
			NDC.pop();
		}
	}
	
	@Override
	public void loadPackage(ModemCommunicator comm, Modem modem, BlueTreeImage image)
	        throws ModemCommunicationFailedException {
		
		// Make sure it's the right modem first
		verifyModem(comm, modem);
		
		// Upload the update package
		try {
			ModemLib.uploadPackage(modem, image);
		} catch (ModemCommunicationFailedException mcfe) {
			logger.info("Modem failed to accept a package upload, resetting", mcfe);
			
			comm.sendMessage("AT+BRESET=0");
			
			throw new ModemCommunicationFailedException(
			                                            "Unable to upload package, reset modem for future attempts",
			                                            mcfe);
		}
		
		// Tell it to perform an upgrade
		ModemLib.performPackageUpdate(comm, modem);
	}
	
	@Override
	public void refreshModemData(ModemCommunicator comm, Modem modem)
		throws ModemCommunicationFailedException
	{
		NDC.push(String.format("model = '%s', fwver = '%s', modemID = %d", getModel(),
		                       getFWVersion(), modem.modemID));
		
		try {
			if (modem.deviceID == null) {
				try {
					modem.deviceID = ModemLib.getDeviceID(comm);
				} catch (Exception e) {
					if (e instanceof ModemCommunicationFailedException) {
						throw (ModemCommunicationFailedException)e;
					}
					
					// Otherwise, log and ignore it
					logger.debug("Unable to retrieve device ID", e);
				}
			}
			
			Map<String, String> props = new Hashtable<String, String>();
			
			// Request other properties:
			// CfgVersion
			// This modem does not support CfgVersion
			modem.cfgVersion = null;
			
			// Password
			props.putAll(ModemLib.getPasswordSettings_after3_4_0(comm));
			
			// GPS module version
			// Serial number
			// RF module version
			// HW version
			props.putAll(ModemLib.getVersions(comm));
			
			try {
				// Alias
				modem.alias = ModemLib.getName(comm);
			} catch (Exception e) {
				if (e instanceof ModemCommunicationFailedException) {
					throw (ModemCommunicationFailedException)e;
				}
				
				// Otherwise, log and ignore it
				logger.debug("Unable to retrieve modem name", e);
			}
			
			try {
				// Phone number
				modem.phoneNumber = ModemLib.getPhoneNumber(comm);
			} catch (Exception e) {
				if (e instanceof ModemCommunicationFailedException) {
					throw (ModemCommunicationFailedException)e;
				}
				
				// Otherwise, log and ignore it
				logger.debug("Unable to retrieve modem phone number", e);
			}
			
			try {
				// Carrier
				props.put("Carrier", ModemLib.getCarrier(comm));
			} catch (Exception e) {
				if (e instanceof ModemCommunicationFailedException) {
					throw (ModemCommunicationFailedException)e;
				}
				
				// Otherwise, log and ignore it
				logger.debug("Unable to retrieve modem phone number", e);
			}
			
			// Set the properties of the modem structure
			modem.setProperty("passwordSettings/enabled", props.get("Password/Enabled"));
			modem.setProperty("passwordSettings/access", props.get("Password/Access"));
			modem.setProperty("passwordSettings/password", props.get("Password/Password"));
			modem.setProperty("versions/hw", props.get("Version/HW"));
			modem.setProperty("versions/rf", props.get("Version/RF"));
			modem.setProperty("versions/gps", props.get("Version/GPS"));
			modem.setProperty("versions/uboot", props.get("Version/U-Boot"));
			modem.setProperty("serial", props.get("Serial"));
			modem.setProperty("carrier", props.get("Carrier"));
		} finally {
			NDC.pop();
		}
	}
	
	@Override
	public void runCommands(ModemCommunicator comm, Modem modem, String... commands)
	        throws ModemCommunicationFailedException {
		
		boolean locallyOpened = false;
		if (comm == null) {
			// Connect normally and run the commands
			comm = new SocketModemCommunicator();
			comm.connect(modem);
			locallyOpened = true;
		}
		
		try {
			// If we opened the connection ourselves, make sure we are talking
			// to the right modem
			if (locallyOpened) verifyModem(comm, modem);
			
			ModemLib.executeScript(comm, modem, commands);
		} finally {
			if (locallyOpened) {
				comm.close();
			}
		}
	}
	
	/**
	 * Whether this modem supports GPS. Implemented by subclasses.
	 * 
	 * @param comm If necessary, the subclass may try to auto-detect using this
	 *            connected communicator.
	 * 
	 * @return True = this modem supports GPS; False = this modem does not
	 *         support GPS.
	 * @throws ModemCommunicationFailedException If auto-detection fails
	 *             catastrophically.
	 */
	protected abstract boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException;
	
	/**
	 * Whether this modem operates on the CDMA network. Implemented by
	 * subclasses.
	 * 
	 * @param comm If necessary, the subclass may try to auto-detect using this
	 *            connected communicator.
	 * 
	 * @return True = this modem operates on CDMA; False = this modem does not
	 *         operate on CDMA.
	 * @throws ModemCommunicationFailedException If auto-detection fails
	 *             catastrophically.
	 */
	protected abstract boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException;
}

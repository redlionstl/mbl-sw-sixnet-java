/*
 * ModemCommunicator.java
 *
 * Provides a generic send/receive message-based communication interface for a
 * modem.
 *
 * Jonathan Pearson
 * September 28, 2009
 *
 */

package com.sixnetio.BVB.Modems;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.util.Utils;

public abstract class ModemCommunicator {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Check whether the {@link #connect(Modem)} method is supported without
	 * calling it and catching {@link UnsupportedOperationException}.
	 */
	public abstract boolean connectSupported();
	
	/**
	 * Connect to the modem.
	 * 
	 * @param modem The modem to connect to.
	 * @throws UnsupportedOperationException If it is not possible to open a
	 *   connection of this type to the modem (for example, if we may only be
	 *   connected to).
	 * @throws ModemCommunicationFailedException If it was not possible to
	 *   connect/establish a normal communications channel with the modem.
	 */
	public abstract void connect(Modem modem)
		throws UnsupportedOperationException, ModemCommunicationFailedException;
	
	/**
	 * Check whether a connection to the modem is open.
	 */
	public abstract boolean isConnected();
	
	/**
	 * Close the connection to the modem, if one is open.
	 */
	public abstract void close();
	
	/**
	 * Send a message (likely an AT command) to the modem.
	 * 
	 * @param message The message string to send.
	 * @throws ModemCommunicationFailedException If there was a problem sending
	 *   the message.
	 */
	public abstract void sendMessage(String message)
		throws ModemCommunicationFailedException;
	
	/**
	 * Block and wait for a message to be received from the modem.
	 * 
	 * @return The message that was received.
	 * @throws ModemCommunicationFailedException If there was a problem
	 *   receiving the message.
	 */
	public abstract String receiveMessage()
		throws ModemCommunicationFailedException;
	
	/* Note: These functions could be static, but as they are most likely going
	 * to be used in close proximity to the above functions, for parsing the
	 * values returned from them, it is more convenient to have them be
	 * non-static.
	 */
	
	/**
	 * Parse a block of text as lines, and return the entire line beginning with
	 * the specified prefix.
	 * 
	 * @param text The block of text to parse.
	 * @param lineStart The line prefix to look for.
	 * @return The first matching line, or <tt>null</tt> if no match was found.
	 */
	public String getLine(String text, String lineStart)
	{
		String[] lines = Utils.getLines(text);
		
		for (String line : lines) {
			if (line.trim().startsWith(lineStart)) {
				return line;
			}
		}
		
		return null;
	}
	
	/**
	 * Parse a block of text as lines, and return a list of all lines beginning
	 * with the specified prefix.
	 * 
	 * @param text The block of text to parse.
	 * @param lineStart The line prefix to look for.
	 * @return All matching lines. May be empty if no matches are found.
	 */
	public List<String> getLines(String text, String lineStart)
	{
		String[] lines = Utils.getLines(text);
		List<String> matches = new LinkedList<String>();
		
		for (String line : lines) {
			if (line.trim().startsWith(lineStart)) {
				matches.add(line);
			}
		}
		
		return matches;
	}
	
	/**
	 * Strip the standard AT format response text off of a line to get only the
	 * payload.
	 * 
	 * @param s The text to parse.
	 * @return The text minus the AT response prefix, or <tt>null</tt> if the
	 *         line format was not recognized or <tt>s == null</tt>.
	 */
	public String stripResponse(String s) {
		if (s == null) return null;
		
		// Return everything beyond the ": "
		if (s.indexOf(":") != -1) {
			return s.substring(s.indexOf(":") + 1).trim();
		}
		else {
			return null;
		}
	}
	
	/**
	 * Given an uptime string, convert it to a value in milliseconds.
	 * 
	 * @param uptime The uptime as a string.
	 * @return The uptime in milliseconds.
	 * @throws NumberFormatException If the format is not recognized
	 * @throws NullPointerException If <tt>uptime</tt> is <tt>null</tt>.
	 */
	public long parseUptime(String uptime) {
		// There are two basic formats for this:
		// 1) [up ][[[# days ]# h ]# min ]# s (pieces are available as
		// necessary)
		// 2) ##:##:## up #[:#] [{day|days|min}]
		
		logger.debug(String.format("Uptime looks like '%s'", uptime));
		
		long days, hours, minutes, seconds;
		
		// If it's got a colon in it, it's format 2
		if (uptime.indexOf(':') != -1) {
			// Format 2
			Pattern pattern = Pattern.compile("(\\d+):(\\d+):(\\d+) up.*");
			Matcher matcher = pattern.matcher(uptime);
			
			if (matcher.matches()) {
				hours = Long.parseLong(matcher.group(1));
				minutes = Long.parseLong(matcher.group(2));
				seconds = Long.parseLong(matcher.group(3));
			}
			else {
				throw new NumberFormatException("Uptime format not recognized");
			}
			
			Pattern dayPattern = Pattern.compile(".*(\\d+) day.*");
			Matcher dayMatcher = dayPattern.matcher(uptime);
			
			if (dayMatcher.matches()) {
				days = Long.parseLong(matcher.group(1));
			}
			else {
				days = 0;
			}
		}
		else {
			// Format 1
			Pattern sPattern = Pattern.compile(".*?(\\d+) s.*");
			Pattern mPattern = Pattern.compile(".*?(\\d+) min.*");
			Pattern hPattern = Pattern.compile(".*?(\\d+) h.*");
			Pattern dPattern = Pattern.compile(".*?(\\d+) day.*");
			
			Matcher sMatcher = sPattern.matcher(uptime);
			if (sMatcher.matches()) {
				seconds = Long.parseLong(sMatcher.group(1));
			}
			else {
				// This one should always be present
				throw new NumberFormatException("Uptime format not recognized");
			}
			
			Matcher mMatcher = mPattern.matcher(uptime);
			if (mMatcher.matches()) {
				minutes = Long.parseLong(mMatcher.group(1));
			}
			else {
				minutes = 0;
			}
			
			Matcher hMatcher = hPattern.matcher(uptime);
			if (hMatcher.matches()) {
				hours = Long.parseLong(hMatcher.group(1));
			}
			else {
				hours = 0;
			}
			
			Matcher dMatcher = dPattern.matcher(uptime);
			if (dMatcher.matches()) {
				days = Long.parseLong(dMatcher.group(1));
			}
			else {
				days = 0;
			}
		}
		
		long ms = ((((days * 24 + hours) * 60 + minutes) * 60 + seconds) * 1000);
		
		logger.debug(String.format("'%s' is %d days, %d hours, %d minutes, %d seconds = %d ms",
		                           uptime, days, hours, minutes, seconds, ms));
		
		return ms;
	}
	
	/**
	 * Given an string containing comma-separated values, return those
	 * individual values as array elements.
	 * 
	 * @param s The string to parse.
	 * @return Either an array of individual elements, or <tt>null</tt> if there
	 *         was a parsing error.
	 */
	public String[] parseValues(String s) {
		try {
			return Utils.tokenize(s, ",", false);
		}
		catch (Exception e) {
			logger.error(String.format("Unable to parse value from '%s'", s), e);
			
			return null;
		}
	}
}

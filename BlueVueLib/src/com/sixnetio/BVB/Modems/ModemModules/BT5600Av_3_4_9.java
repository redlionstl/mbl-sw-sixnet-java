/*
 * BT5600Av_3_4_9.java
 *
 * Communicates with a BT-5600A running 3.4.9.
 *
 * Jonathan Pearson
 * February 19, 2009
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class BT5600Av_3_4_9 extends ModemFW_3_4_9 {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		ModemModule.registerModule("BT-5600A", new Version("3.4.9"), BT5600Av_3_4_9.class);
	}
	
	public BT5600Av_3_4_9(String model, Version fwVersion) {
		super(model, fwVersion);
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	protected boolean isHSPA() {
		return false;
	}
}

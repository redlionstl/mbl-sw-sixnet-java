/*
 * Fallback_3_7_0.java
 *
 * Interfaces with a modem of unknown model running 3.7.0.
 *
 * Jonathan Pearson
 * April 6, 2009
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class Fallback_3_7_0 extends ModemFW_3_7_0 {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		ModemModule.registerModule("*", new Version("3.7.0"), Fallback_3_7_0.class);
	}
	
	public Fallback_3_7_0(String model, Version fwVersion) {
		super(model, fwVersion);
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return !ModemLib.detectGSM(comm);
	}
	
	@Override
	protected boolean isHSPA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return ModemLib.detectGPS(comm);
	}
}

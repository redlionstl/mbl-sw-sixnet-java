/*
 * Fallback_3_4_9.java
 *
 * Interfaces with a modem of unknown model running 3.4.9.
 *
 * Jonathan Pearson
 * February 27, 2009
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class Fallback_3_4_9 extends ModemFW_3_4_9 {
	private static Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		ModemModule.registerModule("*", new Version("3.4.9"), Fallback_3_4_9.class);
	}
	
	public Fallback_3_4_9(String model, Version fwVersion) {
		super(model, fwVersion);
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return !ModemLib.detectGSM(comm);
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return ModemLib.detectGPS(comm);
	}
}

/*
 * BT6421v_3_8_0.java
 *
 * Provides an interface to a BT-6421 running fw version 3.8.0.
 *
 * Jonathan Pearson
 * March 26, 2010
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class BT6421v_3_8_0
	extends ModemFW_3_8_0
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		ModemModule.registerModule("BT-6421", new Version("3.8.0"), BT6421v_3_8_0.class);
	}
	
	public BT6421v_3_8_0(String model, Version fwVersion)
	{
		super(model, fwVersion);
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
	
	@Override
	protected boolean isHSPA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
}

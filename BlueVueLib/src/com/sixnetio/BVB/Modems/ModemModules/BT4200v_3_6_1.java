/*
 * BT4200v_3_6_1.java
 *
 * Interfaces with a BT4200 running 3.6.1 firmware.
 *
 * Jonathan Pearson
 * February 17, 2009
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class BT4200v_3_6_1 extends ModemFW_3_6_1 {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		ModemModule.registerModule("BT-4200", new Version("3.6.1"), BT4200v_3_6_1.class);
	}
	
	public BT4200v_3_6_1(String model, Version fwVersion) {
		super(model, fwVersion);
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	@Override
	protected boolean isHSPA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
}

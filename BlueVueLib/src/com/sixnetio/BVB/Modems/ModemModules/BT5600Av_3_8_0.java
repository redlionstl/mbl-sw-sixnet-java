/*
 * BT5600Av_3_8_0.java
 *
 * Interface with a BT-5600A running 3.8.0
 *
 * Jonathan Pearson
 * October 2, 2009
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class BT5600Av_3_8_0
	extends ModemFW_3_8_0
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		ModemModule.registerModule("BT-5600A", new Version("3.8.0"), BT5600Av_3_8_0.class);
	}
	
	public BT5600Av_3_8_0(String model, Version fwVersion)
	{
		super(model, fwVersion);
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	@Override
	protected boolean isHSPA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
}

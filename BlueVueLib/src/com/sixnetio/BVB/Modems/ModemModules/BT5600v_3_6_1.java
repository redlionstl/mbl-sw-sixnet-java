/*
 * BT5600v_3_6_1.java
 *
 * Provides an interface to a BT-5600 running fw version 3.6.1.
 *
 * Jonathan Pearson
 * March 26, 2010
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class BT5600v_3_6_1
	extends ModemFW_3_6_1
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		ModemModule.registerModule("BT-5600", new Version("3.6.1"), BT5600v_3_6_1.class);
	}
	
	public BT5600v_3_6_1(String model, Version fwVersion)
	{
		super(model, fwVersion);
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	@Override
	protected boolean isHSPA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
}

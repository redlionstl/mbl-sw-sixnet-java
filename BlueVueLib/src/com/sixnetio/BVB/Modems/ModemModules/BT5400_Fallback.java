/*
 * BT5400_Fallback.java
 *
 * Interfaces with a bare-bones BT5400.
 *
 * Jonathan Pearson
 * March 26, 2010
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

/**
 * Do not depend on this class always having the same superclass. As we add
 * support for older versions of firmware, this will continue to extend the
 * lowest supported version.
 * 
 * @author Jonathan Pearson
 */
public class BT5400_Fallback
	extends ModemFW_3_6_1
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		ModemModule.registerModule("BT-5400", new Version("*"), BT5400_Fallback.class);
	}
	
	public BT5400_Fallback(String model, Version fwVersion)
	{
		super(model, fwVersion);
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
	
	@Override
	protected boolean isHSPA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
}

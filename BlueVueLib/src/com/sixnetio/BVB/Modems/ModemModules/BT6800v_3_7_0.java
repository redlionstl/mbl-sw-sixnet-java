/*
 * BT6800v_3_7_0.java
 *
 * Provides an interface to a BT-6800 running fw version 3.7.0.
 *
 * Jonathan Pearson
 * March 26, 2010
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class BT6800v_3_7_0
	extends ModemFW_3_7_0
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		ModemModule.registerModule("BT-6800", new Version("3.7.0"), BT6800v_3_7_0.class);
	}
	
	public BT6800v_3_7_0(String model, Version fwVersion)
	{
		super(model, fwVersion);
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
	
	@Override
	protected boolean isHSPA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
}

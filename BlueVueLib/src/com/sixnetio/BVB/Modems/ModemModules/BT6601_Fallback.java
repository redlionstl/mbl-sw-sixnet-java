/*
 * BT6601_Fallback.java
 *
 * Interfaces with a bare-bones BT6601.
 *
 * Jonathan Pearson
 * March 26, 2010
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

/**
 * Do not depend on this class always having the same superclass. As we add
 * support for older versions of firmware, this will continue to extend the
 * lowest supported version.
 * 
 * @author Jonathan Pearson
 */
public class BT6601_Fallback
	extends ModemFW_3_6_1
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		ModemModule.registerModule("BT-6601", new Version("*"), BT6601_Fallback.class);
	}
	
	public BT6601_Fallback(String model, Version fwVersion)
	{
		super(model, fwVersion);
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	@Override
	protected boolean isHSPA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return false;
	}
}

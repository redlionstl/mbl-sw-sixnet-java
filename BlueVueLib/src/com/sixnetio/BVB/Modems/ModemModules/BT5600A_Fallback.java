/*
 * BT5600A_Fallback.java
 *
 * Interfaces with a bare-bones BT-5600A.
 *
 * Jonathan Pearson
 * February 27, 2009
 *
 */

package com.sixnetio.BVB.Modems.ModemModules;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Modems.*;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

/**
 * Do not depend on this class always having the same superclass. As we add
 * support for older versions of firmware, this will continue to extend the
 * lowest supported version.
 * 
 * @author Jonathan Pearson
 */
public class BT5600A_Fallback
	extends ModemFW_3_4_9
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		ModemModule.registerModule("BT-5600A", new Version("*"), BT5600A_Fallback.class);
	}
	
	public BT5600A_Fallback(String model, Version fwVersion) {
		super(model, fwVersion);
	}
	
	@Override
	protected boolean supportsGPS(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	@Override
	protected boolean isCDMA(ModemCommunicator comm)
		throws ModemCommunicationFailedException
	{
		return true;
	}
	
	protected boolean isHSPA() {
		return false;
	}
}

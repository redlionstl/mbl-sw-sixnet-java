/*
 * BadModemModuleException.java
 *
 * Thrown when a modem module matching a given model/firmware version cannot be created.
 *
 * Jonathan Pearson
 * January 16, 2009
 *
 */

package com.sixnetio.BVB.Modems;

import com.sixnetio.util.Version;

public class BadModemModuleException extends Exception {
	private final String model;
	private final Version fwVersion;
	
	public BadModemModuleException(String message, String model, Version fwVersion) {
		super(message);
		
		this.model = model;
		this.fwVersion = fwVersion;
	}
	
	public BadModemModuleException(String message, String model, Version fwVersion, Throwable cause) {
		super(message, cause);
		
		this.model = model;
		this.fwVersion = fwVersion;
	}
	
	public String getModel() {
		return model;
	}
	
	public Version getFWVersion() {
		return fwVersion;
	}
}

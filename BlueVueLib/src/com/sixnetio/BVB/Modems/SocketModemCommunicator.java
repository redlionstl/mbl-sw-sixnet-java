/*
 * SocketModemCommunicator.java
 *
 * Communicates with a modem over a TCP socket.
 *
 * Jonathan Pearson
 * September 28, 2009
 *
 */

package com.sixnetio.BVB.Modems;

import java.io.*;
import java.net.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;

/**
 * A modem communicator that works over a TCP socket. This is thread-safe.
 * 
 * @author Jonathan Pearson
 */
public class SocketModemCommunicator
    extends ModemCommunicator
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /**
     * The socket used for communications. This is <tt>null</tt> when not
     * connected.
     */
    protected Socket sock;

    /**
     * The socket input stream, as retrieved by {@link Socket#getInputStream()}.
     */
    protected DataInputStream sockIn;

    /**
     * The socket output stream, as retrieved by
     * {@link Socket#getOutputStream()}.
     */
    protected OutputStream sockOut;

    @Override
    public synchronized void close()
    {
        if (sock != null) {
            try {
                sock.close();
            }
            catch (IOException ioe) {
                logger.warn("Unable to close socket", ioe);
            }
            finally {
                sock = null;
                sockIn = null;
                sockOut = null;
            }
        }
    }

    @Override
    public boolean connectSupported()
    {
        return true;
    }

    @Override
    public synchronized void connect(Modem modem)
        throws ModemCommunicationFailedException
    {
        if (isConnected()) {
            throw new ModemCommunicationFailedException("Already connected");
        }

        logger.debug("Opening a connection");
        sock = openConnection(modem);

        // Grab the socket streams
        boolean success = false;
        try {
            sockIn = new DataInputStream(sock.getInputStream());
            sockOut = sock.getOutputStream();
            success = true;
        }
        catch (IOException ioe) {
            logger.error("Unable to retrieve socket streams", ioe);

            throw new ModemCommunicationFailedException(
                "Unable to retrieve socket input stream: " + ioe.getMessage(),
                ioe);
        }
        finally {
            if (!success) {
                close();
            }
        }

        logger.debug("Authenticating the connection");
        success = false;
        try {
            authenticateConnection(modem);
            success = true;
        }
        finally {
            if (!success) {
                close();
            }
        }
    }

    /**
     * Open a new connection to a modem.
     * 
     * @param modem The modem to connect to.
     * @return The socket connected to the modem.
     * @throws ModemCommunicationFailedException If it was not possible to
     *             connect to the modem.
     */
    protected Socket openConnection(Modem modem)
        throws ModemCommunicationFailedException
    {
        // Put together a socket address for the connection
        InetAddress addr;
        try {
            addr = InetAddress.getByName(modem.ipAddress);
        }
        catch (UnknownHostException uhe) {
            logger.error("Unable to get modem address", uhe);
            throw new ModemCommunicationFailedException(
                "Unable to get modem's address: " + uhe.getMessage(), uhe);
        }

        SocketAddress sockAddr = new InetSocketAddress(addr, modem.port);

        // Make a new (unconnected) socket
        Socket sock = new Socket();

        // Check the timeout for communicating with modems
        int timeout =
            Server.getServerSettings().getIntValue(Setting.SocketTimeout) * 1000;

        // Connect
        try {
            sock.connect(sockAddr, timeout);
        }
        catch (IOException ioe) {
            logger.error("Unable to connect", ioe);
            throw new ModemCommunicationFailedException(
                "Unable to connect to modem: " + ioe.getMessage(), ioe);
        }

        // Set the socket timeout
        boolean success = false;
        try {
            sock.setSoTimeout(timeout);
            success = true;
        }
        catch (SocketException se) {
            logger.error("Unable to set socket timeout", se);
            throw new ModemCommunicationFailedException(
                "Unable to set socket timeout: " + se.getMessage(), se);
        }
        finally {
            if (!success) {
                try {
                    sock.close();
                }
                catch (IOException ioe) {
                    logger.warn("Unable to close socket", ioe);
                }
            }
        }

        return sock;
    }

    /**
     * Read some data from a socket.
     * 
     * @param maxLength The maximum number of bytes to read.
     * @param timeout How long to wait for any data to arrive before giving up.
     *            Pass 0 to wait forever. Pass -1 to use the value already set
     *            on the socket.
     * @return The data that was read (may be less than <tt>maxLength</tt>
     *         bytes).
     * @throws IOException If there was a problem reading, or no data became
     *             available before the timeout.
     */
    protected byte[] readSocket(int maxLength, int timeout)
        throws IOException
    {
        if (!isConnected()) {
            throw new ModemCommunicationFailedException("Not connected");
        }

        int oldTimeout = sock.getSoTimeout();
        if (timeout >= 0) {
            // Don't get caught waiting for all the data
            sock.setSoTimeout(timeout);
        }

        try {
            byte[] data = new byte[maxLength];
            int bytes = sockIn.read(data);

            if (bytes == -1) {
                // The socket has been closed
                close();
                throw new IOException("Connection closed unexpectedly");
            }

            if (bytes < data.length) {
                byte[] realData = new byte[bytes];
                System.arraycopy(data, 0, realData, 0, bytes);
                data = realData;
            }

            return data;
        }
        finally {
            if (timeout >= 0) {
                sock.setSoTimeout(oldTimeout);
            }
        }
    }

    protected void authenticateConnection(Modem modem)
        throws ModemCommunicationFailedException
    {
        if (!isConnected()) {
            throw new ModemCommunicationFailedException("Not connected");
        }

        // If there is no password on record for the modem, assume that it does
        // not require one
        // If it does, we wouldn't be able to talk to it anyway, and this will
        // speed things up
        String password = modem.getProperty("passwordSettings/password");
        if (password == null || password.length() == 0) {
            logger
                .debug("No password on file for modem, skipping authentication");
            return;
        }

        // Also, check whether the modem is supposed to be asking for a password
        String enabled = modem.getProperty("passwordSettings/enabled");
        if (enabled == null || enabled.equals("0")) {
            logger
                .debug("Password authentication is disabled for this modem, skipping");
            return;
        }

        logger.debug("Authenticating modem connection");

        // Wait a bit to see if it will request a password
        int socketTimeout =
            Server.getServerSettings().getIntValue(Setting.SocketTimeout) * 1000;

        Utils.sleep(socketTimeout);

        // Read all available data

        byte[] data;

        try {
            data = readSocket(1500, -1);
        }
        catch (SocketTimeoutException ste) {
            logger
                .debug("Socket timed out, assuming no password required", ste);
            return;
        }
        catch (IOException ioe) {
            logger.error("Unable to read from modem", ioe);
            throw new ModemCommunicationFailedException(
                "Unable to read from modem: " + ioe.getMessage(), ioe);
        }

        // Scan for "password:"
        String response = new String(data);
        if (response.indexOf("password:") == -1) {
            // No password required
            logger.debug("No password required");
            return;
        }

        // Send the password
        byte[] msg = new byte[4 + 4 + password.length() + 1];
        Conversion.intToBytes(msg, 0, msg.length);
        Conversion.intToBytes(msg, 4, 0);
        System.arraycopy(password.getBytes(), 0, msg, 8, password.length());
        msg[msg.length - 1] = 0;

        try {
            sockOut.write(msg);
            sockOut.flush();
        }
        catch (IOException ioe) {
            logger.error("Unable to write to modem", ioe);
            throw new ModemCommunicationFailedException(
                "Unable to write to modem: " + ioe.getMessage(), ioe);
        }

        // Read again
        try {
            data = readSocket(1500, -1);
        }
        catch (IOException ioe) {
            logger.error("Unable to read from modem", ioe);
            throw new ModemCommunicationFailedException(
                "Unable to read from modem: " + ioe.getMessage(), ioe);
        }

        // Parse out the interesting part of the message
        // length (4), id (4), "\r\n\0"
        // Also, trim off any whitespace
        response = new String(data, 8, data.length - (4 + 4 + 3)).trim();

        if (response.equalsIgnoreCase("wrong")) {
            logger.error("Stored password was incorrect");
            throw new ModemCommunicationFailedException("Bad password in modem");
        }
        else if (response.equalsIgnoreCase("timeout")) {
            logger.error("Modem timed out password authentication");
            throw new ModemCommunicationFailedException(
                "Modem timed out password entry too quickly");
        }
        else if (response.equalsIgnoreCase("pass")) {
            logger.debug("Password authentication passed");
            return; // Success
        }
        else {
            logger
                .debug(String.format("Bad response from modem: %s", response));
            throw new ModemCommunicationFailedException(
                "Unexpected response to password authentication from modem: " +
                    response);
        }
    }

    @Override
    public synchronized boolean isConnected()
    {
        return (sock != null);
    }

    @Override
    public synchronized String receiveMessage()
        throws ModemCommunicationFailedException
    {
        if (!isConnected()) {
            throw new ModemCommunicationFailedException("Not connected");
        }

        try {
            byte[] data = new byte[4];

            // Read the message length
            sockIn.readFully(data);

            int msgLen = Conversion.bytesToInt(data, 0);

            // Set a limit on what we will accept from the modem
            // This is done because the modem responds without a standard header
            // when it is asking for a password, resulting in ASCII data being
            // treated like a number
            // Since the modem should never send a message so large as to
            // require the most-significant byte, fail if that is non-zero
            if ((msgLen & 0xff000000) != 0) {
                throw new ModemCommunicationFailedException(
                    "Modem responded "
                        + "badly: Invalid message length. Perhaps it is "
                        + "configured to require a password, but the password "
                        + "is not known?");
            }

            // Read and discard the message ID
            sockIn.readFully(data);

            // Read the rest of the message
            data = new byte[msgLen - 8];
            sockIn.readFully(data);

            // Parse into a string, cutting off the '\r\0' at the end
            String result = new String(data, 0, data.length - 2);

            logger.debug(String.format("Response from modem '%s'", result));

            return result;
        }
        catch (IOException ioe) {
            logger.error("Unable to read from modem", ioe);
            throw new ModemCommunicationFailedException(
                "Unable to read from modem: " + ioe.getMessage(), ioe);
        }
    }

    @Override
    public synchronized void sendMessage(String message)
        throws ModemCommunicationFailedException
    {
        if (!isConnected()) {
            throw new ModemCommunicationFailedException("Not connected");
        }

        logger.debug(String.format("Sending message '%s'", message));

        byte[] msgBytes = message.getBytes();
        byte[] data = new byte[4 + 4 + msgBytes.length + 2];

        Conversion.intToBytes(data, 0, data.length);
        Conversion.intToBytes(data, 4, 0);
        System.arraycopy(msgBytes, 0, data, 8, msgBytes.length);
        data[data.length - 2] = '\r';
        data[data.length - 1] = 0;

        try {
            sockOut.write(data);
            sockOut.flush();
        }
        catch (IOException ioe) {
            logger.error("Unable to write to modem", ioe);
            throw new ModemCommunicationFailedException(
                "Unable to write to modem: " + ioe.getMessage(), ioe);
        }
    }
}

package com.sixnetio.BVB.Jobs;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.jonp.xml.Tag;
import org.junit.*;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.BVB.Database.DatabaseProviders.MySQLDatabase;
import com.sixnetio.BVB.Database.DatabaseProviders.PostgreSQLDatabase;
import com.sixnetio.BVB.Modems.*;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Version;

/**
 * class JobTester
 * 
 * This is required for registering
 * job types. A default 'runJob' is
 * provided as needed.
 * 
 * Since this is only needed by the 'JobTest'
 * test class, it is not provided as 'public'.
 * 
 * @author daveq
 *
 */
class JobTester extends Job {
	
	//private static final Logger logger = Logger.getLogger(JobTester.class);
	static {
		Job.registerJobType("JobTester", JobTester.class);
	}
	
	public JobTester(JobData jobData, Database database) {
		super(jobData, database);
	}
	
	@Override
	public void runJob(ModemCommunicator comm, Modem modem, Date startTime) throws ModemCommunicationFailedException,
	        DatabaseException, BadModemModuleException {
		throw new ModemCommunicationFailedException("JobTester.runJob(): Intentional ModemCommunicationFailedException thrown.");
	}
}

/**
 * class JobTest
 * 
 * The test class for the class 'Job'.
 * 
 * @author daveq
 *
 */
public class JobTest {
	
	//FIXME: handle license file, user, ... better for automated test
	private static final String TEST_LICENSE_FILE = "/projects/java/BlueVueBatch/DaveLicense.blf";
	private static final String DB_USER_NAME = "daveq";
	private static final String DB_SERVER = "localhost";
	
	static Job job;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Try creating a valid Job object and use a
	 * bogus job type. this is expected to
	 * throw a BadJobException.
	 * @throws BadJobException
	 * 
	 */
	@Test(expected=BadJobException.class)
	public void testMakeJobMismatch() throws BadJobException{
		final String testType1 = "JobTester_01";
		final String testType2 = "JobTester_02";
		Job.JobData data =
	        new Job.JobData(1, 2, 3, 666,
	                        new Date(9999), 7,
	                        testType1,
	                        new Tag("tagme"),
	                        false);
		
		Database db = null;
		try {
			db = new MySQLDatabase("MySQL");
		}
		catch (DriverException e) {
			fail("testMakeJobMismatch(): DriverException caught while trying to create a Database object.");
		}
		if (db == null)
			fail("Unable to create MySQL database object.");
		Job.registerJobType(testType2, JobTester.class);
		job = Job.makeJob(data, db);
		if (job != null)
			fail("testMakeJobMismatch(): job != null when passed a mismatching job type.");
	}
	
	/**
	 * testMakeJobValid()
	 * 
	 * Uses a valid JobData object and Database
	 * and expects a valid 'job' to be created.
	 * A BadJobException should be thrown if
	 * a valid 'job' cannot be created.
	 * */
	@Test
	public void testMakeJobValid()
		throws BadJobException, DriverException{
		
		final String testType1 = "JobTester_01";
		Job.JobData data =
	        new Job.JobData(1, 2, 3, 666,
	                        new Date(3333), 7,
	                        testType1,
	                        new Tag("tagme"),
	                        false);
		
		Database db;
		db = new MySQLDatabase("MySQL");
		if (db == null)
			fail("Unable to create MySQL database object.");
		Job.registerJobType(testType1, JobTester.class);
		job = Job.makeJob(data, db);
		/*if the job object is null here, the
		BadJobException should have been thrown but
		wasn't.*/
		if (job == null)
			fail("testMakeJobValid(): obtained null Job without BadJobException thrown.");
	}
	
	/**
	 * testMakeGenericJobValid()
	 * 
	 * Attempts to create a 'Job'
	 * by calling makeGenericJob()
	 * with valid 'JobData'.
	 */
	@Test
	public void testMakeGenericJobValid(){
		Job.JobData data =
			new Job.JobData(1,2,3,777,
			new Date(45000),7,
			"JobTester_01",new Tag("tagme"),false);
		job = Job.makeGenericJob(data);
		if (job == null)
			fail("testMakeGenericJobValid(): failed to create a non-null 'Job'.");
	}
	
	/**
	 * testJobRun()
	 * 
	 * Creates a Job with a modem = null and
	 * runs the Job thread's run() method.
	 * 
	 *  Expects to return from run immediately.
	 */
	@Test
	public void testJobRun() throws IOException {
		File licFile = new File(TEST_LICENSE_FILE);
		if (!licFile.exists()){
			fail("A required file (" + TEST_LICENSE_FILE + ") was not found.");
		}
		LicenseKey licenseKey = new LicenseKey(licFile);

		PostgreSQLDatabase db = null;
		try {
			Database.registerProvider("postgres", PostgreSQLDatabase.class);
			db = (PostgreSQLDatabase) PostgreSQLDatabase.getInstance("postgres");
			db.connect(DB_SERVER, null, DB_USER_NAME, "", true, licenseKey, Database.ConnectionReason.Client);
		}
		catch (BadProviderException e1) {
			fail("testJobRun(): BadProviderException during initialization.");
		}
		catch (DriverException e1) {
			fail("testJobRun(): DriverException during initialization.");
		}
		catch (DatabaseException e) {
			fail("testJobRun(): DatabaseException during initialization.");
		}
		if (db != null){
			Job.JobData data =
				new Job.JobData(1,2,3,777,
					new Date(45000),7,
					"JobTester_01",new Tag("tagme"),false);
			try {
				job = Job.makeJob(data,db);
			}
			catch (BadJobException e) {
				fail("testJobRun(): BadJobException during Job.makeJob().");
			}
			if (job == null)
				fail("testJobRun(): failed to create a non-null 'Job'.");
			else{
				Modem mod = new Modem(0xBEEFDEAD, new DeviceID(0xDEADBEEF), "alias", "model", new Version("909.1004.2"), "1.0", "192.168.1.1", 509,
						"5188775173", new Tag("tagme"));
				job.setModem(mod);
				job.run();
			}
		}
		else
			fail("testJobRun(): db = null.");
	}
}

/**
 * 
 */
package com.sixnetio.BVB.Common;

import static org.junit.Assert.fail;

import org.jonp.xml.Tag;
import org.junit.*;

import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Version;

/**
 * ModemTest()
 * 
 * Exercises low level methods such
 * as get/setProperty(),...
 * @author daveq
 *
 */
public class ModemTest {
	
	Modem modem;
	
	static final int modemID = 606;
	static final DeviceID deviceID = new DeviceID(0xF1F2F3F4);
	static final String alias = "modem_alias";
	static final String model = "modem_model";
	static final Version fwVersion = new Version("8.9.4");
	static final String cfgVersion = "10.5.6";
	static final String ipAddress = "192.168.1.13";
	static final int port = 1594;
	static final String phoneNumber = "518-877-5173";
	static final Tag status = new Tag("TagTest");
	static final String testName1 = "testName1";
	static final String testVal1 = "testVal1";
	static final String testName2 = "testName2";
	static final String testVal2 = "testVal2";
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		modem = new Modem(modemID, deviceID, alias, model,
				fwVersion, cfgVersion, ipAddress, port, phoneNumber, status);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Two methods of setting the property are used.
	 * First, use setProperty(x,y) and check for y.
	 * Second, set the property manually by calling
	 * 		modem.status.setAttribute(x,y) then check
	 * 		for y.
	 * @param name
	 */
	@Test
	public void testGetProperty() {
		//set the property
		modem.setProperty(testName1, testVal1);
		//get it
		String result = modem.getProperty(testName1);
		if (result != null){
			if (result.compareTo(testVal1) != 0)
				fail("getProperty failed with result =  " + result + " expected " + testVal1);
		}
		else
			fail("getProperty failed with null result.");
		
		//now try filling in data without using setProperty() ...
		Tag testTag = new Tag(testName2);
		testTag.addContent(testVal2);
		modem.status.addContent(testTag);
		//see if it can find it:
		result = modem.getProperty(testName2);
		if (result != null){
			if (result.compareTo(testVal2) != 0)
				fail("getProperty failed with result = " + result + " expected " + testVal2);
		}
		else
			fail("getProperty failed with null result.");
	}

}

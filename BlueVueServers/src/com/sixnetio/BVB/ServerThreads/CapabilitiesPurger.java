/*
 * CapabilitiesPurger.java
 *
 * Removes dead servers from the Capabilities table in the database periodically.
 *
 * Jonathan Pearson
 * January 29, 2009
 *
 */

package com.sixnetio.BVB.ServerThreads;

import java.util.Date;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.Utils;

public class CapabilitiesPurger
		extends PurgerThread {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public CapabilitiesPurger(Database db) {
		super(db, "Capabilities Purger");
	}
	
	@Override
	protected int getInterval() {
		int value = Server.getServerSettings()
			.getIntValue(Setting.CapabilitiesUpdateInterval);
		
		// Semantics of CapabilitiesUpdateInterval are different from those
		//   desired by getUpdateInterval -- there is no Pause setting
		if (value == 0) return INTERVAL_STOP;
		
		return value;
	}
	
	@Override
	protected void executePurge(Date cutoff) {
		try {
			logger.debug(String.format("Purging the capabilities table " +
			                           "(earlier than %s)",
			                           Utils.formatStandardDateTime(cutoff,
			                                                    false)));
			
			db.purgeCapabilities(cutoff);
		} catch (DatabaseException de) {
			logger.error("Unable to purge the capabilities table", de);
		}
	}
	
	@Override
	protected int getPurgeTimeout() {
		return Server.getServerSettings()
			.getIntValue(Setting.CapabilitiesCutoff);
	}
}

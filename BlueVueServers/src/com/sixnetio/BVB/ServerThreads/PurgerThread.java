/*
 * PurgerThread.java
 *
 * A superclass to simplify threads that purge data from the database.
 * 
 * Retrieves the cycle time and lifetime from the subclass and acts on those
 * values appropriately.
 *
 * Jonathan Pearson
 * July 7, 2009
 *
 */

package com.sixnetio.BVB.ServerThreads;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Database.Database;
import com.sixnetio.util.Utils;

public abstract class PurgerThread
		extends ServerThread {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public PurgerThread(Database db, String name) {
		super(db, name);
	}
	
	@Override
	protected void execute() {
		int purgeTimeout = getPurgeTimeout();
		
		if (purgeTimeout > 0) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.SECOND, -purgeTimeout);
			Date cutoff = cal.getTime();
			
			executePurge(cutoff);
		}
	}
	
	/**
	 * Get the amount of time that the type of data that this purger purges
	 * should remain in the database, in seconds.
	 */
	protected abstract int getPurgeTimeout();
	
	/**
	 * Perform the database purge. Handle your own exceptions through Log4j.
	 * 
	 * @param cutoff The cutoff date -- anything earlier than this should be
	 *   purged.
	 */
	protected abstract void executePurge(Date cutoff);
}

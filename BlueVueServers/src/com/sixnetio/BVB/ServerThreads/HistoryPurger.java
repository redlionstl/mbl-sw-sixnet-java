/*
 * HistoryPurger.java
 *
 * Removes old entries from the two history tables periodically.
 *
 * Jonathan Pearson
 * February 16, 2009
 *
 */

package com.sixnetio.BVB.ServerThreads;

import java.util.Date;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.Utils;

public class HistoryPurger
    extends PurgerThread
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    public HistoryPurger(Database db)
    {
        super(db, "History Purger");
    }

    @Override
    public int getInterval()
    {
        int value =
            Server.getServerSettings()
                .getIntValue(Setting.HistoryPurgeInterval);

        // Semantics of HistoryPurgeInterval are different from those desired by
        // getUpdateInterval -- there is no Pause setting
        if (value == 0) {
            return INTERVAL_STOP;
        }

        return value;
    }

    @Override
    protected void executePurge(Date cutoff)
    {
        try {
            logger.debug(String.format("Purging the history tables "
                                       + "(earlier than %s)", Utils
                .formatStandardDateTime(cutoff, false)));

            db.purgeHistory(cutoff);
        }
        catch (DatabaseException de) {
            logger.error("Unable to purge the history tables", de);
        }
    }

    @Override
    protected int getPurgeTimeout()
    {
        return Server.getServerSettings().getIntValue(Setting.HistoryCutoff);
    }
}

/*
 * CapabilitiesUpdater.java
 *
 * Updates the Capabilities table in the database periodically.
 *
 * Jonathan Pearson
 * January 29, 2009
 *
 */

package com.sixnetio.BVB.ServerThreads;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.util.Utils;

/**
 * <p>
 * A thread that updates the Capabilities table periodically. You should call
 * {@link #execute()} before trying to acquire any locks, so that the
 * {@link CapabilitiesPurger} of this server or any other will not destroy those
 * locks immediately.
 * </p>
 * 
 * <p>
 * The capabilities table contains a description of what each server supports.
 * For example, you may want to run one server that handles everything except
 * firmware updates, and another that only handles firmware updates (since that
 * will require significant bandwidth). You would remove the corresponding
 * plugins from each server instance before starting them up, and then once they
 * are running, the capabilities table will reflect that server A supports
 * [Status, Reconfiguration, !DelayedUpdate] and server B supports [Firmware].
 * </p>
 * 
 * <p>
 * Then, from the Web UI, you may want to schedule a new job. What types of jobs
 * are supported by this installation? Read the capabilities table to find out.
 * Here is a second example to illustrate this: Perhaps you never want your
 * users to update the firmware in your modems, and you're afraid that one of
 * them might do it accidentally. You remove the "Firmware" job plugin from all
 * of your job servers. Now the Capabilities table will show that [Status,
 * Reconfiguration, !DelayedUpdate] jobs are supported. When the "Create Jobs"
 * view in the Web UI comes up, it will list "Status" and "Reconfiguration" as
 * job choices, but not "Firmware" (because it is not in the table) and not
 * "!DelayedUpdate" (because it is marked with a prepended '!', indicating that
 * it is an internal-only job type, not to be created by users).
 * </p>
 * 
 * <p>
 * Since the Web UI does not have access to the job type plugins (they may be on
 * one or more disparate machines), all it knows about what is supported by the
 * running servers is what is visible in the Capabilities table, and the table
 * contains only string descriptions.
 * </p>
 * 
 * @author Jonathan Pearson
 */
public abstract class CapabilitiesUpdater
		extends ServerThread {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public CapabilitiesUpdater(Database db) {
		super(db, "Capabilities Updater");
	}
	
	@Override
	public int getInterval() {
		int value = Server.getServerSettings()
			.getIntValue(Setting.CapabilitiesUpdateInterval);
		
		// Semantics of CapabilitiesUpdateInterval are different from those
		//   desired by getUpdateInterval -- there is no Pause setting
		if (value == 0) return INTERVAL_STOP;
		
		return value;
	}
	
	@Override
	public int getShutdownOrder() {
		// Shut down after most threads so that the capabilities table will
		// better reflect when this server went down
		return 5;
	}
}

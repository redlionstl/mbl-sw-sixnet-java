/*
 * LockPurger.java
 *
 * Clears out stale locks periodically.
 *
 * Jonathan Pearson
 * March 2, 2009
 *
 */

package com.sixnetio.BVB.ServerThreads;

import java.util.Date;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.Utils;

public class LockPurger
    extends PurgerThread
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    public LockPurger(Database db)
    {
        super(db, "Lock Purger");
    }

    @Override
    public int getInterval()
    {
        int value =
            Server.getServerSettings().getIntValue(Setting.LockPurgeInterval);

        // Semantics of LockPurgeInterval are different from those desired by
        // getUpdateInterval -- there is no Pause setting
        if (value == 0) {
            return INTERVAL_STOP;
        }

        return value;
    }

    @Override
    protected void executePurge(Date cutoff)
    {
        try {
            logger.debug(String.format("Purging stale locks (earlier than %s)",
                Utils.formatStandardDateTime(cutoff, false)));

            db.purgeStaleLocks(cutoff);
        }
        catch (DatabaseException de) {
            logger.error("Unable to purge stale locks", de);
        }
    }

    @Override
    protected int getPurgeTimeout()
    {
        return Server.getServerSettings().getIntValue(Setting.LockTimeout);
    }
}

/*
 * ThreadDispatcher.java
 *
 * A thread dispatcher with a thread pool. This is its own thread to
 * watch for system settings changes and update the actual thread
 * pool as necessary.
 *
 * Jonathan Pearson
 * January 21, 2009
 *
 */

package com.sixnetio.BVB.ServerThreads;

import java.util.concurrent.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

public class ThreadDispatcher
		extends ServerThread {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private ThreadPoolExecutor executor;
	
	/**
	 * Construct a new thread dispatcher.
	 * 
	 * @throws DatabaseException If unable to read database setting.
	 */
	public ThreadDispatcher() {
		super(null, "Thread Dispatcher");
		
		// The first three may be adjusted while running. The capacity is fixed
		// at creation time.
		int minPoolSize = Server.getServerSettings()
			.getIntValue(Setting.MinThreadPoolSize);
		
		int maxPoolSize = Server.getServerSettings()
			.getIntValue(Setting.MaxThreadPoolSize);
		
		int threadKeepalive = Server.getServerSettings()
			.getIntValue(Setting.ThreadKeepalive);
		
		int threadCapacity = Server.getServerSettings()
			.getIntValue(Setting.ThreadCapacity);
		
		BlockingQueue<Runnable> threadQueue =
			new LinkedBlockingQueue<Runnable>(threadCapacity);
		
		executor =
			new ThreadPoolExecutor(minPoolSize, maxPoolSize, threadKeepalive,
			                       TimeUnit.SECONDS, threadQueue);
		
		executor.prestartAllCoreThreads();
	}
	
	@Override
	public int getInterval() {
		int value = Server.getServerSettings()
			.getIntValue(Setting.SettingPollInterval);
		
		// Semantics of SettingPollInterval are different from those desired by
		//   getUpdateInterval -- there is no Pause setting
		if (value == 0) return INTERVAL_STOP;
		
		return value;
	}
	
	@Override
	public void execute() {
		int minPoolSize = Server.getServerSettings()
			.getIntValue(Setting.MinThreadPoolSize);
		
		int maxPoolSize = Server.getServerSettings()
			.getIntValue(Setting.MaxThreadPoolSize);
		
		int threadKeepalive = Server.getServerSettings()
			.getIntValue(Setting.ThreadKeepalive);
		
		executor.setCorePoolSize(minPoolSize);
		executor.setMaximumPoolSize(maxPoolSize);
		executor.setKeepAliveTime(threadKeepalive, TimeUnit.SECONDS);
	}
	
	/**
	 * Enqueue a thread for future execution.
	 * 
	 * @param thread The thread to enqueue.
	 * @throws RejectedExecutionException If the pool and queue are saturated.
	 *             The thread has not been enqueued if this is thrown.
	 */
	public void enqueueThread(Runnable thread)
			throws RejectedExecutionException {
		
		executor.execute(thread);
	}
	
	/**
	 * Called if the server panics. Immediately shuts down the thread pool,
	 * discarding jobs that have not yet started and interrupting (via
	 * Thread.interrupt()) jobs that have. This overrides
	 * {@link ServerThread#serverPanic()} because we need to shut down the
	 * executor as well as our own thread.
	 */
	@Override
	public void serverPanic() {
		// Interrupt all running jobs
		executor.shutdownNow();
		
		// Shut down the thread and wait for it to stop
		super.serverPanic();
		
		try {
			// Wait another minute for the executor to finish
			executor.awaitTermination(60000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException ie) {
			throw new InterruptedOperationException(ie);
		}
	}
	
	/**
	 * Called if the server is shutting down. This overrides
	 * {@link ServerThread#serverShutdown()} because we need to shut down the
	 * executor as well as our own thread.
	 */
	@Override
	public void serverShutdown() {
		// Tell the executor to stop
		executor.shutdown();
		
		// Shut down the thread and wait for it to stop
		super.serverShutdown();
		
		try {
			// Wait as long as possible for the executor to finish
			executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
		} catch (InterruptedException ie) {
			throw new InterruptedOperationException(ie);
		}
	}
	
	@Override
	public int getShutdownOrder() {
		// Go down after any services that might add new threads, but before the
		// database or logger
		return 10;
	}
}

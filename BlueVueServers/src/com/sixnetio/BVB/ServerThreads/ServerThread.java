/*
 * ServerThread.java
 *
 * A generic thread class implemented by all threads in the BVB servers.
 * 
 * The run method periodically calls execute(), implemented by a subclass, and
 * then sleeps for the remainder of the cycle time (provided by the subclass in
 * getInterval()).
 *
 * Jonathan Pearson
 * July 2, 2009
 *
 */

package com.sixnetio.BVB.ServerThreads;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Common.ShutdownListener;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

public abstract class ServerThread
		extends Thread
		implements ShutdownListener {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	protected static final int INTERVAL_STOP = -1;
	protected static final int INTERVAL_PAUSE = 0;
	
	protected Database db;
	protected volatile boolean running = false;
	
	/**
	 * Construct a new ServerThread.
	 * 
	 * @param db The database object to use.
	 * @param name The name of the thread.
	 */
	public ServerThread(Database db, String name) {
		super(name);
		
		this.db = db;
	}
	
	/**
	 * Public only as an implementation detail, do not call this.
	 */
	@Override
	public void run() {
		int updateInterval = getInterval();
		
		if (updateInterval == INTERVAL_STOP) {
			// Thread is not supposed to run
			running = false;
			return;
		}
		
		running = true;
		Server.registerShutdownListener(this);
		
		try {
			while (running) {
				updateInterval = getInterval();
				
				if (updateInterval == INTERVAL_STOP) {
					// Immediate shutdown of this thread
					running = false;
					continue;
				} else if (updateInterval == INTERVAL_PAUSE) {
					// Sleep for the system settings update interval, then loop
					int settingsRefreshTime = Server.getServerSettings()
						.getIntValue(Setting.SettingPollInterval);
					
					if (settingsRefreshTime == 0) {
						// Never refreshing settings, and we are set not to run
						//   until a settings update tells us otherwise... Just
						//   stop now, no point in continuing
						running = false;
						continue;
					} else {
						// It was in seconds, convert to milliseconds
						Utils.sleep(settingsRefreshTime * 1000L);
					}
				} else {
					long nextTime = System.currentTimeMillis() +
						updateInterval * 1000L;
					
					execute();
					
					long sleepFor = nextTime - System.currentTimeMillis();
					if (sleepFor > 0) {
						Utils.sleep(sleepFor);
					}
				}
			}
		} catch (IllegalStateException ise) {
			// Database was probably shut down, so we should shut down too
			running = false;
			
			logger.debug("Caught an IllegalStateException, assuming database went down", ise);
		} finally {
			running = false;
			Server.unregisterShutdownListener(this);
		}
	}
	
	/**
	 * The subclass should return a value indicating how often the thread should
	 * cycle.
	 * 
	 * @return <ul>
	 *   <li>{@link #INTERVAL_STOP} = Stop the thread immediately.</li>
	 *   <li>{@link #INTERVAL_PAUSE} = Sleep until the setting changes. If
	 *     {@link Setting#SettingPollInterval} is 0, the thread should shut down
	 *     because it will never be started up again.</li>
	 *   <li>&gt; 0 = Seconds to wait between cycles.</li>
	 * </ul>
	 */
	protected abstract int getInterval();
	
	/**
	 * Perform the task that this thread is responsible for.
	 */
	protected abstract void execute();
	
	/**
	 * Returns 0, indicating that shutdown order is not important to this
	 * thread. Subclasses may override this to return a more meaningful value.
	 */
	@Override
	public int getShutdownOrder() {
		return 0;
	}
	
	/**
	 * The server panics. This will set running to false, interrupt
	 * the thread, and wait up to one minute for the thread to die. If
	 * subclasses have need, they may override this.
	 */
	@Override
	public void serverPanic() {
		running = false;
		interrupt();
		
		try {
			join(ONE_MINUTE_MS);
		} catch (InterruptedException ie) {
			throw new InterruptedOperationException(ie);
		}
	}
	
	/**
	 * The server is shutting down normally. This will set running to false and
	 * wait indefinitely for the thread to die. If subclasses have need, they
	 * may override this.
	 */
	@Override
	public void serverShutdown() {
		running = false;
		
		try {
			join();
		} catch (InterruptedException ie) {
			throw new InterruptedOperationException(ie);
		}
	}
}

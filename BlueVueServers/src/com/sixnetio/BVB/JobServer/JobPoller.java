/*
 * JobPoller.java
 *
 * Polls the database periodically for jobs that are ready to run and passes
 * them off to the Job Dispatcher.
 *
 * Jonathan Pearson
 * January 21, 2009
 *
 */

package com.sixnetio.BVB.JobServer;

import java.util.Map;
import java.util.concurrent.RejectedExecutionException;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.DBLogger;
import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.ServerThreads.ServerThread;
import com.sixnetio.BVB.ServerThreads.ThreadDispatcher;
import com.sixnetio.util.Utils;

class JobPoller
		extends ServerThread {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private ThreadDispatcher dispatcher;
	
	/**
	 * Construct a new Job Poller
	 * 
	 * @param db The database to use for job polling.
	 */
	public JobPoller(Database db) {
		super(db, "Job Polling");
		
		dispatcher = new ThreadDispatcher();
	}
	
	@Override
	public int getInterval() {
		// This will always be > 0, no need to worry about stop/pause values
		return Server.getServerSettings()
			.getIntValue(Setting.JobPollingInterval);
	}
	
	@Override
	public void execute() {
		// Poll the database for jobs
		Map<Integer, Job> jobs;
		try {
			jobs = db.getReadyToRunJobs();
		} catch (DatabaseException de) {
			logger.error("Error polling database for jobs", de);
			
			DBLogger.log(null, DBLogger.ERROR,
			             String.format("Error polling database for jobs: %s",
			                           de.getMessage()));
			
			return;
		}
		
		// Pass the jobs off to the dispatcher
		for (int modemID : jobs.keySet()) {
			try {
				dispatcher.enqueueThread(jobs.get(modemID));
			} catch (RejectedExecutionException ree) {
				logger.warn("Job dispatcher saturated, unable to enqueue job",
				            ree);
				
				// Unlock the modem and the job
				try {
					db.unlockModem(modemID);
				} catch (DatabaseException de) {
					logger.error(String.format("Unable to unlock modem %d",
					                           modemID), de);
					
					DBLogger.log(null, DBLogger.ERROR,
					             String.format("Unable to unlock Modem #%d: %s",
					                           modemID, de.getMessage()));
				}
				
				try {
					db.unlockJob(jobs.get(modemID).getJobData().jobID);
				} catch (DatabaseException de) {
					long jobID = jobs.get(modemID).getJobData().jobID;
					
					logger.error(String.format("Unable to unlock job %d",
					                           jobID));
					
					DBLogger.log(null, DBLogger.ERROR,
					             String.format("Unable to unlock Job #%d: %s",
					                           jobID, de.getMessage()));
				}
			}
		}
	}
	
	/**
	 * Start the job poller and dispatcher threads.
	 */
	@Override
	public void start() {
		super.start();
		
		dispatcher.start();
	}
	
	@Override
	public int getShutdownOrder() {
		// Make sure this thread goes down FIRST
		// As soon as the server has been told to shut down, there should be no
		// more jobs scheduled
		return Integer.MIN_VALUE;
	}
}

/*
 * IPRegHandler.java
 *
 * A superclass for IP registration handlers. Provides the
 * parsing and database-access functionality that would otherwise
 * be duplicated between them.
 *
 * Jonathan Pearson
 * June 1, 2009
 *
 */

package com.sixnetio.BVB.IPRegServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Utils;

/**
 * A generic handler class for dealing with received IP Registration Messages.
 * It is up to the subclasses to define how those messages get into the system,
 * this class only defines how they are handled afterwards.
 *
 * @author Jonathan Pearson
 */
// Note: This does not implement ShutdownListener because it is a quick-update
//   handler thread, not a long-term update/sleep cycle thread
abstract class IPRegHandler
		extends Thread {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** The database that receives updates. */
	protected Database db;
	
	/**
	 * Construct a new IP registration handler.
	 * 
	 * @param db The database to communicate with.
	 * @param threadName The name of the thread.
	 */
	protected IPRegHandler(Database db, String threadName) {
		super(threadName);
		
		this.db = db;
	}
	
	/**
	 * Parse an IP registration message and store it to the database.
	 * 
	 * @param message The message to parse and store.
	 * @throws IOException If the message was badly formatted and could not be
	 *             parsed.
	 * @throws DatabaseException If there was a problem communicating with the
	 *             database.
	 * @throws ObjectInUseException If the modem lock could not be acquired (and
	 *             therefore the update could not be performed).
	 */
	public void parseAndStore(String message)
		throws IOException, DatabaseException, ObjectInUseException
	{
		try {
			// Format is "Device ID,Modem Name,WAN IP"
			
			String[] tokens = Utils.tokenize(message, ",", false);
			if (tokens.length != 3) {
				throw new RuntimeException("IP registration message must " +
				                           "have three comma-separated fields");
			}
			
			// ESN
			DeviceID devID = DeviceID.parseDeviceID(tokens[0]);
			
			// Modem Name
			String modemName = tokens[1];
			
			// WAN IP
			String ip = tokens[2];
			
			// Test the IP address to make sure it is in the proper format
			try {
				InetAddress.getByName(ip);
			} catch (UnknownHostException uhe) {
				// Throw a RuntimeException to be caught by the surrounding try
				throw new RuntimeException("The IP address is not properly " +
				                           "formatted: " + uhe.getMessage(),
				                           uhe);
			}
			
			logger.debug("Received an IP update for modem '" +
			             modemName + "' (" + devID.toString() +
			             "): " + ip);
			
			// Get the modem (go only by device ID)
			Collection<Integer> matchingModems =
				db.getMatchingModems(null, devID, null, null, null, null, null,
				                     null, null, null, 0, -1, null, null);
			
			if (matchingModems.size() < 1) {
				createIfNecessary(modemName, devID, ip);
			} else if (matchingModems.size() > 1) {
				throw new IOException("Multiple modems in the database match " +
				                      "Device ID = " +
				                      devID.toString() +
				                      ", unable to set IP to " + ip);
			} else {
				int modemID = matchingModems.iterator().next();
				
				updateModem(modemID, modemName, devID, ip);
			}
		} catch (RuntimeException re) {
			throw new IOException("Error parsing IP registration message '" +
			                      message + "': " + re.getMessage(), re);
		}
	}
	
	/**
	 * To simplify the {@link #parseAndStore(String)} function, this will check
	 * server settings for creating new modems, applying tags, and creating new
	 * status query jobs, or throw an IOException if IP Registration modem
	 * creation is disabled.
	 * 
	 * @param modemName The name of the modem that may be created.
	 * @param devID The device ID of the modem that may be created.
	 * @param ip The IP address of the modem that may be created.
	 * @throws IOException If new modem creation is disabled in the server
	 *             settings.
	 * @throws DatabaseException If a database exception is thrown while trying
	 *             to perform the necessary database operations.
	 */
	private void createIfNecessary(String modemName, DeviceID devID, String ip)
			throws IOException, DatabaseException {
		
		// Are we supposed to create a new modem based on the message?
		// Or just ignore it?
		if (Server.getServerSettings()
				.getIntValue(Setting.IPRegistrationCreateModems) == 1) {
			
			logger.debug("Modem with device ID " +
			             devID.toString() +
			             " does not exist, creating one");
			
			// Create a new modem
			Modem modem = new Modem(Modem.INV_MODEM, devID, modemName, null,
			                        null, null, ip, Modem.PORT_STANDARD, null,
			                        new Tag("status"));
			db.createModem(modem);
			
			DBLogger.log(null, DBLogger.INFO, "Created Modem #" +
			             modem.modemID +
			             " in response to an IP registration message");
			
			// Apply a tag?
			String tagName = Server.getServerSettings()
				.getStringValue(Setting.IPRegistrationNewModemTag);
			
			if (tagName.length() > 0) {
				logger.debug("Applying tag '" + tagName + "' to Modem #" +
				             modem.modemID);
				
				// Check whether the tag exists
				List<Integer> tags =
					db.getMatchingTags(null, tagName, null, null, null, null,
					                   null, 0, -1, null, null);
				
				int tagID = -1;
				if (tags.size() == 0) {
					logger.debug("Tag '" + tagName +
					             "' does not exist, creating a new one");
					
					// Create the tag
					ModemTag tag;
					tag = new ModemTag(ModemTag.INV_TAG, -1, tagName, "",
					                   Server.getServerSettings().getIntValue(Setting.NoTagPollingInterval));
					db.createTag(tag);
					tagID = tag.tagID;
					
					DBLogger.log(null, DBLogger.INFO, "Created Tag #" + tagID +
					             " for new modems created by IP " +
					             "registration messages");
				} else if (tags.size() == 1) {
					tagID = tags.get(0);
				} else {
					// This shouldn't happen, tag names should be unique
					DBLogger.log(null, DBLogger.ERROR,
					             "Database contains multiple tags named '" +
					             tagName + "'");
				}
				
				if (tagID != -1) {
					logger.debug("Appling Tag #" + tagID + " to Modem #" +
					             modem.modemID);
					
					db.applyTag(modem.modemID, tagID);
					
					DBLogger.log(null, DBLogger.INFO, "Applied Tag #" + tagID +
					             " to Modem #" + modem.modemID);
				}
			}
			
			// Create a new automatic status query?
			if (Server.getServerSettings()
					.getIntValue(Setting.IPRegistrationCreateStatusJob) == 1) {
				
				// Is there a server that supports status query jobs?
				Collection<Capabilities> caps = db.getCurrentCapabilities();
				boolean found = false;
				for (Capabilities cap : caps) {
					if (cap.getJobs().contains("Status")) {
						found = true;
						break;
					}
				}
				
				if (found) {
					// Create a new Status job
					logger.debug("Creating a new automatic status query for " +
					             "Modem #" + modem.modemID);
					
					Job.JobData data =
						new Job.JobData(Job.JobData.INV_JOB, null,
						                modem.modemID, null, new Date(), 0,
						                "Status", new Tag("job"), true, null,
						                null);
					Job job = Job.makeGenericJob(data);
					db.createJob(job);
					
					DBLogger.log(null, DBLogger.INFO,
					             "Created automatic status query Job #" +
					             job.getJobData().jobID + " for new Modem #" +
					             modem.modemID);
				} else {
					DBLogger.log(null, DBLogger.WARNING,
					             "Unable to create a new status query for " +
					             "new Modem #" + modem.modemID +
					             " as no servers support Status type jobs");
				}
			}
		} else {
			throw new IOException("No modems in the database match " +
			                      "Device ID = " +
			                      devID.toString() +
			                      ", unable to set IP to " + ip);
		}
	}
	
	/**
	 * To simplify the {@link #parseAndStore(String)} function, this will
	 * attempt to update the given modem with the specified properties, creating
	 * a DelayedUpdate job if the modem is busy and that job type is supported.
	 * 
	 * @param modemID The ID of the modem to update.
	 * @param modemName The name of the modem to update.
	 * @param devID The device ID of the modem to update.
	 * @param ip The IP address of the modem to update.
	 * @throws DatabaseException If there is a problem accessing the database.
	 * @throws ObjectInUseException If the modem is in use beyond the maximum
	 *             lock timeout, and the DelayedUpdate job type is not
	 *             supported.
	 */
	private void updateModem(int modemID, String modemName,
	                         DeviceID devID, String ip)
		throws DatabaseException, ObjectInUseException
	{
		// It would be helpful is times had a unit suffix in their name like "s"
		// or "ms" -- CLN
		long waitTime = Server.getServerSettings()
			.getIntValue(Setting.IPRegistrationModemWaitTimeout) * 1000L;
		
		// Wait until we can acquire the lock on the modem (wouldn't want to
		// update it while a status query is running, our change would be
		// immediately overwritten)
		LockResult lr = db.lockModem(modemID, waitTime);
		
		if (lr == LockResult.Acquired) {
			try {
				Modem modem = db.getModem(modemID);
				
				// Might as well update the name as well as the address, since
				//   it must be more recent than what we have in the database
				//   now (otherwise we wouldn't need to update the IP address
				//   either, since that would be up to date as well)
				// Since we pulled the modem from the database based on its
				//   device ID, though, there's no point in updating that
				modem.alias = modemName;
				modem.ipAddress = ip;
				
				db.updateModem(modem);
				
				DBLogger.log(null, DBLogger.INFO,
				             "IP Registration message updated Modem #" +
				             modemID + " '" + modemName +
				             "' (" + devID.toString() +
				             ") with IP address " + ip);
			} finally {
				db.unlockModem(modemID);
			}
		} else {
			logger.debug("Unable to acquire lock on Modem #" + modemID +
				", checking whether Delayed Update jobs are supported");
			
			// Schedule a Delayed Update job, if available
			Collection<Capabilities> caps = db.getCurrentCapabilities();
			boolean found = false;
			for (Capabilities cap : caps) {
				if (cap.getExtra().getStringContents("type")
						.equals("outbound")) {
					
					if (cap.getJobs().contains("!DelayedUpdate")) {
						found = true;
						break;
					}
				}
			}
			
			if (found) {
				logger.debug("Scheduling a new Delayed Update job to " +
				             "perform the update");
				
				Job.JobData data =
					new Job.JobData(Job.JobData.INV_JOB, null, modemID, null,
					                new Date(), 0, "!DelayedUpdate",
					                new Tag("job"), false, null, null);
				
				Tag modemTag = new Tag("modem");
				data.parameters.addContent(modemTag);
				
				// We just updated the IP address and the name
				Tag temp;
				modemTag.addContent(temp = new Tag("alias"));
				temp.addContent(modemName);
				
				modemTag.addContent(temp = new Tag("ipAddress"));
				temp.addContent(ip);
				
				Job job = Job.makeGenericJob(data);
				db.createJob(job);
				
				DBLogger.log(null, DBLogger.INFO,
				             "Scheduled Delayed Update Job #" +
				             job.getJobData().jobID + " to update Modem #" +
				             modemID +
				             " as the modem was busy when an IP " +
				             "registration message arrived");
			} else {
				DBLogger.log(null, DBLogger.WARNING,
				             "Discarding IP Registration message from Modem #" +
				             modemID +
				             " as the modem is busy and no servers support " +
				             "!DelayedUpdate type jobs");
				
				throw new ObjectInUseException("Unable to acquire modem lock " +
				                               "for Modem #" + modemID);
			}
		}
	}
}

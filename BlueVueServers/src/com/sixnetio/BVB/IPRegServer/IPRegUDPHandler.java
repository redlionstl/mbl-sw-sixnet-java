/*
 * IPRegUDPHandler.java
 *
 * Handles UDP IP registration messages.
 *
 * Jonathan Pearson
 * June 1, 2009
 *
 */

package com.sixnetio.BVB.IPRegServer;

import java.io.IOException;
import java.net.SocketAddress;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.DBLogger;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.util.Utils;

/**
 * An IPRegHandler that handles IP Registration messages received via UDP
 * packets.
 *
 * @author Jonathan Pearson
 */
class IPRegUDPHandler
		extends IPRegHandler {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** The data from the packet we received. */
	private byte[] buf;
	
	/** The address of the sender of the packet we received. */
	private SocketAddress from;
	
	/**
	 * Construct a new IP registration handler for reading an IP registration
	 * message from a UDP packet.
	 * 
	 * @param db The database to save the address to.
	 * @param from The SocketAddress that the packet came from.
	 * @param pkt The packet to read the message from.
	 */
	public IPRegUDPHandler(Database db, SocketAddress from, byte[] buf) {
		super(db, "IP Registration UDP Handler");
		
		this.buf = buf;
		this.from = from;
		
		logger.debug("Received a UDP IP Registration message from " +
		             from.toString());
	}
	
	/**
	 * Public as an implementation detail, you should not call this.
	 */
	@Override
	public void run() {
		// Read the remote address from the packet (mostly for debugging)
		String remoteAddress = from.toString();
		
		try {
			// Read the message
			String message = new String(buf).trim();
			
			logger.debug("Received message '" + message + "'");
			
			// Parse the message and save it to the database
			parseAndStore(message);
		} catch (IOException ioe) {
			logger.error("Unable to parse IP registration message from " +
			             remoteAddress, ioe);
			DBLogger.log(null, DBLogger.ERROR,
			             "Unable to parse IP registration message from " +
			             remoteAddress + ": " + ioe.getMessage());
		} catch (DatabaseException de) {
			logger.error("Unable to save IP registration message from " +
			             remoteAddress, de);
			DBLogger.log(null, DBLogger.ERROR,
			             "Unable to save IP registration message from " +
			             remoteAddress + ": " + de.getMessage());
		} catch (ObjectInUseException oiue) {
			logger.error("Unable to save IP registration message from " +
			             remoteAddress, oiue);
			DBLogger.log(null, DBLogger.ERROR,
			             "Unable to save IP registration message from " +
			             remoteAddress + ": " + oiue.getMessage());
		}
	}
}

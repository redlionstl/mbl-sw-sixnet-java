/*
 * CapabilitiesUpdater.java
 *
 * Updates the Capabilities table in the database periodically.
 *
 * Jonathan Pearson
 * January 29, 2009
 *
 */

package com.sixnetio.BVB.IPRegServer;

import java.util.Vector;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.Capabilities;
import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.ServerThreads.CapabilitiesPurger;
import com.sixnetio.util.Utils;

/**
 * A thread that updates the Capabilities table periodically. You should call
 * {@link #execute()} before trying to acquire any locks, so that the
 * {@link CapabilitiesPurger} of this server or any other will not destroy those
 * locks immediately.
 * 
 * @author Jonathan Pearson
 */
class CapabilitiesUpdater
		extends com.sixnetio.BVB.ServerThreads.CapabilitiesUpdater {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public CapabilitiesUpdater(Database db) {
		super(db);
	}
	
	/**
	 * Update the capabilities table for this server. This MUST be performed
	 * before any jobs are acquired. Before starting the job polling thread, you
	 * should call this.
	 */
	@Override
	public synchronized void execute() {
		// We need to build our own collection of modem data
		try {
			logger.debug("Updating the capabilities table");
			
			Capabilities localCapabilities =
				new Capabilities(Server.getName(),
				                 Capabilities.getLocalDatabaseCapabilities(),
				                 new Vector<String>(0),
				                 new Vector<String>(0),
				                 new Tag("extra"));
			
			// This is an 'inbound' type server
			Tag serverType = new Tag("type");
			serverType.addContent("inbound");
			localCapabilities.getExtra().addContent(serverType);
			
			db.updateCapabilities(localCapabilities);
		} catch (DatabaseException de) {
			logger.error("Error updating capabilities table", de);
		}
	}
}

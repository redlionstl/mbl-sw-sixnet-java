/*
 * IPRegListener.java
 *
 * Listens for new IP registration connections and spins
 * off threads to deal with them.
 *
 * Jonathan Pearson
 * May 29, 2009
 *
 */

package com.sixnetio.BVB.IPRegServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.concurrent.RejectedExecutionException;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Common.ShutdownListener;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.ServerThreads.ThreadDispatcher;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

/**
 * Listens on a single port (both TCP and UDP) for connections/packets, and
 * passes them off to the appropriate handlers.
 *
 * @author Jonathan Pearson
 */
// Note: Although it looks like this class could implement ServerThread, that is
//   actually not the case, as this uses a select() call to handle its cycle
//   time, and not Utils.sleep() like the other threads -- it is actually doing
//   work while it is waiting for the next cycle
class IPRegListener
		extends Thread
		implements ShutdownListener {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** The reserved size for UDP packets. */
	// The longest messages should be about 64 bytes (15 digit dev ID + 32 char
	// name + 15 char IP + commas), this leaves some room for expansion
	// Only used for UDP packets
	private static final int BUFFER_LENGTH = 128;
	
	/** The database to update. */
	private Database db;
	
	/** The thread dispatcher in charge of spinning off new handlers. */
	private ThreadDispatcher dispatcher;
	
	/** The selector that allows us to sleep on sockets waiting for data. */
	private Selector selector;
	
	/** The server socket channel for TCP connections. */
	private ServerSocketChannel tcpChannel;
	
	/** The datagram channel for UDP packets. */
	private DatagramChannel udpChannel;
	
	/** True while running, false to signal that we should quit. */
	private volatile boolean running;
	
	/**
	 * Construct a new IPRegListener.
	 * 
	 * @param db The database to post updates to.
	 * @throws IOException If there is a problem opening the necessary sockets
	 *             to listen for registrations.
	 */
	public IPRegListener(Database db)
			throws IOException {
		
		super("IP Registration Listener");
		
		this.db = db;
		
		dispatcher = new ThreadDispatcher();
		
		int port = Server.getServerSettings()
			.getIntValue(Setting.IPRegistrationPort);
		
		int threadCapacity = Server.getServerSettings()
			.getIntValue(Setting.ThreadCapacity);
		
		SocketAddress localAddress = new InetSocketAddress(port);
		
		tcpChannel = ServerSocketChannel.open();
		tcpChannel.configureBlocking(false);
		tcpChannel.socket().bind(localAddress, threadCapacity);
		logger.info("Created a listening TCP server socket on port " + port);
		
		udpChannel = DatagramChannel.open();
		udpChannel.configureBlocking(false);
		udpChannel.socket().bind(localAddress);
		logger.info("Created a listening UDP socket on port " + port);
		
		selector = Selector.open();
		tcpChannel.register(selector, SelectionKey.OP_ACCEPT);
		udpChannel.register(selector, SelectionKey.OP_READ);
	}
	
	/**
	 * Public only as an implementation detail, you should not call this.
	 */
	@Override
	public void run() {
		running = true;
		Server.registerShutdownListener(this);
		
		try {
			while (running) {
				// Check for settings updates
				long cycleTime = Server.getServerSettings()
					.getIntValue(Setting.IPRegistrationCycleTime) * 1000L;
				
				// Listen for new connections/messages
				int keys;
				try {
					logger.debug("Waiting for a connection/message...");
					keys = selector.select(cycleTime);
				} catch (IOException ioe) {
					logger.error("Failure in select", ioe);
					continue;
				}
				
				logger.debug("Connections/messages received on " + keys +
				             " channels");
				
				// Shortcut to avoid creating a new iterator
				if (keys == 0) continue;
				
				// Act on any new connections/messages
				for (Iterator<SelectionKey> itKeys =
						selector.selectedKeys().iterator();
				     itKeys.hasNext(); ) {
					
					SelectionKey key = itKeys.next();
					
					// Remove the key from the set
					// Even if we are unable to spin off a new thread to handle
					// the message, we don't want to leave it in the set for
					// next time
					itKeys.remove();
					
					IPRegHandler handler;
					try {
						if (key.channel() == tcpChannel) {
							// Spin off a new handler for TCP messages
							handler = new IPRegTCPHandler(db, tcpChannel
							                              .socket().accept());
						} else if (key.channel() == udpChannel) {
							// Spin off a new handler for UDP packets
							ByteBuffer buf = ByteBuffer.allocate(BUFFER_LENGTH);
							SocketAddress from = udpChannel.receive(buf);
							
							handler = new IPRegUDPHandler(db, from,
							                              buf.array());
						} else {
							logger.error("Unexpected source of selector exit: " +
							             key.channel().getClass().getName());
							continue;
						}
					} catch (IOException ioe) {
						logger.error("Unable to accept connection/read UDP packet",
						             ioe);
						continue;
					}
					
					try {
						dispatcher.enqueueThread(handler);
					} catch (RejectedExecutionException ree) {
						logger.warn("Unable to schedule thread for parsing " +
						            "IP registration message", ree);
					}
				}
			}
		} catch (IllegalStateException ise) {
			// Database was probably shut down, we should probably shut down too
			running = false;
			
			logger.debug("Caught an IllegalStateException, assuming " +
			             "database went down", ise);
		} finally {
			Server.unregisterShutdownListener(this);
		}
	}
	
	@Override
	public void serverPanic() {
		running = false;
		interrupt();
		
		try {
			join(60000);
		} catch (InterruptedException ie) {
			throw new InterruptedOperationException(ie);
		}
	}
	
	@Override
	public void serverShutdown() {
		running = false;
		
		try {
			join();
		} catch (InterruptedException ie) {
			throw new InterruptedOperationException(ie);
		}
	}
	
	/**
	 * Start this thread and the thread dispatcher that it uses.
	 */
	@Override
	public void start() {
		super.start();
		
		dispatcher.start();
	}
	
	@Override
	public int getShutdownOrder() {
		// Make sure this thread goes down FIRST
		// As soon as the server has been told to shut down, there should be no
		// more updates
		return Integer.MIN_VALUE;
	}
}

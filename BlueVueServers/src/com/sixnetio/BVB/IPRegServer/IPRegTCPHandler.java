/*
 * IPRegTCPHandler.java
 *
 * Handles TCP IP registration connections.
 *
 * Jonathan Pearson
 * June 1, 2009
 *
 */

package com.sixnetio.BVB.IPRegServer;

import java.io.IOException;
import java.net.Socket;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.DBLogger;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.util.Utils;

/**
 * An IPRegHandler that handles TCP connections to a port, reading and
 * processing IP Registration messages from those connections.
 *
 * @author Jonathan Pearson
 */
class IPRegTCPHandler
		extends IPRegHandler {
	
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** The socket that was created as a result of the connection. */
	private Socket sock;
	
	/**
	 * Construct a new IP registration handler for reading an IP registration
	 * message from a TCP socket.
	 * 
	 * @param db The database to save the address to.
	 * @param sock The socket to read the message from.
	 */
	public IPRegTCPHandler(Database db, Socket sock) {
		super(db, "IP Registration TCP Handler");
		
		this.sock = sock;
		
		logger.debug("Received a TCP IP Registration message from " +
		             sock.getInetAddress());
	}
	
	/**
	 * Public as an implementation detail, you should not call this.
	 */
	@Override
	public void run() {
		// Read the remote address from the socket (mostly for debugging)
		String remoteAddress = "(unknown)";
		try {
			try {
				remoteAddress = sock.getInetAddress().getHostAddress();
			} catch (Exception e) {
				logger.warn("Unable to read remote address from socket", e);
			}
			
			// Read from the socket until EOF
			String message;
			try {
				message = Utils.returnToEOF(sock.getInputStream()).trim();
			} catch (IOException ioe) {
				logger.warn("Unable to read IP registration from socket " +
				            "connected to " + remoteAddress, ioe);
				DBLogger.log(null, DBLogger.WARNING,
				             "Unable to read IP registration from socket " +
				             "connected to " + remoteAddress + ": " +
				             ioe.getMessage());
				return;
			}
			
			logger.debug("Received message '" + message + "'");
			
			// Parse the message and save it to the database
			parseAndStore(message);
		} catch (IOException ioe) {
			logger.error("Unable to parse IP registration message from " +
			             remoteAddress, ioe);
			DBLogger.log(null, DBLogger.ERROR,
			             "Unable to parse IP registration message from " +
			             remoteAddress + ": " + ioe.getMessage());
		} catch (Exception e) {
			logger.error("Unable to save IP registration message from " +
			             remoteAddress, e);
			DBLogger.log(null, DBLogger.ERROR,
			             "Unable to save IP registration message from " +
			             remoteAddress + ": " + e.getMessage());
		} finally {
			try {
				sock.close();
			} catch (IOException ioe) {
				logger.warn("Unable to close socket connected to "
				            + remoteAddress, ioe);
			}
		}
	}
}

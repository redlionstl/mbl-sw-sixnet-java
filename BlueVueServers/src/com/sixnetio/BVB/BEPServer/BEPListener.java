/*
 * BEPListener.java
 *
 * Listens for new BEP messages and spins off threads to deal with them.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.BVB.BEPServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;
import java.util.concurrent.RejectedExecutionException;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.Common.Server;
import com.sixnetio.BVB.Common.ShutdownListener;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.ServerThreads.ThreadDispatcher;
import com.sixnetio.bep.BEPMessage;
import com.sixnetio.server.http.HTTP;
import com.sixnetio.util.*;

/**
 * Listens on a single port (both TCP and UDP) for packets, and passes them off
 * to a handler.
 * 
 * @author Jonathan Pearson
 */
// Note: Although it looks like this class could implement ServerThread, that is
// actually not the case, as this uses a select() call to handle its cycle
// time, and not Utils.sleep() like the other threads -- it is actually doing
// work while it is waiting for the next cycle
class BEPListener
    extends Thread
    implements ShutdownListener
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /** The reserved size for UDP packets (set to theoretical maximum). */
    private static final int BUFFER_LENGTH = 65527;

    /** The database to update. */
    private Database db;

    /**
     * The HTTP server to use for transferring files to a modem via a modem-pull
     * method.
     */
    private HTTP httpServer;

    /** The thread dispatcher in charge of spinning off new handlers. */
    private ThreadDispatcher dispatcher;

    /** The selector that allows us to sleep on sockets waiting for data. */
    private Selector selector;

    /** The server socket channel for TCP connections. */
    private ServerSocketChannel tcpChannel;

    /** The datagram channel for UDP packets. */
    private DatagramChannel udpChannel;

    /**
     * UDP channels are safe for concurrent access by a single reader and a
     * single writer; this is the reader lock for {@link #udpChannel}.
     */
    private final Object l_udpChannelRead = new Object();

    /**
     * UDP channels are safe for concurrent access by a single reader and a
     * single writer; this is the writer lock for {@link #udpChannel}.
     */
    private final Object l_udpChannelWrite = new Object();

    /**
     * Since UDP is connectionless, and because we are already holding the
     * socket and need to be able to deal with messages from new devices, this
     * allows us to pass off responses to UDP BEP messages to the sender,
     * imitating a connection. It is the handler's responsibility to add and
     * remove itself from this map, and it must be prepared to field new
     * messages from a device, even if it thinks it has an ongoing conversation.
     */
    private Map<SocketAddress, BEPUDPHandler> udpHandlers =
        new HashMap<SocketAddress, BEPUDPHandler>();

    /** True while running, false to signal that we should quit. */
    private volatile boolean running;

    /**
     * Construct a new BEPListener.
     * 
     * @param db The database to post updates to.
     * @param httpServer The HTTP server to use for modem-pull data transfers.
     * @throws IOException If there is a problem opening the necessary sockets
     *             to listen for messages.
     */
    public BEPListener(Database db, HTTP httpServer)
        throws IOException
    {
        super("BEP Listener");

        if (db == null) {
            throw new NullPointerException("db is a required parameter");
        }

        if (httpServer == null) {
            throw new NullPointerException("httpServer is a required parameter");
        }

        this.db = db;
        this.httpServer = httpServer;

        dispatcher = new ThreadDispatcher();

        int port = Server.getServerSettings().getIntValue(Setting.BEPPort);

        int threadCapacity =
            Server.getServerSettings().getIntValue(Setting.ThreadCapacity);

        SocketAddress localAddress = new InetSocketAddress(port);

        tcpChannel = ServerSocketChannel.open();
        tcpChannel.configureBlocking(false);
        tcpChannel.socket().bind(localAddress, threadCapacity);
        logger.info("Created a listening TCP server socket on port " + port);

        udpChannel = DatagramChannel.open();
        udpChannel.configureBlocking(false);
        udpChannel.socket().bind(localAddress);
        logger.info("Created a listening UDP socket on port " + port);

        selector = Selector.open();
        tcpChannel.register(selector, SelectionKey.OP_ACCEPT);
        udpChannel.register(selector, SelectionKey.OP_READ);
    }

    /**
     * Public only as an implementation detail, you should not call this.
     */
    @Override
    public void run()
    {
        running = true;
        Server.registerShutdownListener(this);

        try {
            while (running) {
                // Check for settings updates
                long cycleTime_ms =
                    Server.getServerSettings()
                        .getIntValue(Setting.BEPCycleTime) * 1000L;

                // Listen for new connections/messages
                int keys;
                try {
                    logger.debug("Waiting for a connection/message...");
                    keys = selector.select(cycleTime_ms);
                }
                catch (IOException ioe) {
                    logger.error("Failure in select", ioe);
                    continue;
                }

                logger.debug("Connections/messages received on " + keys +
                             " channels");

                // Shortcut to avoid creating a new iterator
                if (keys == 0) {
                    continue;
                }

                // Act on any new connections/messages
                for (Iterator<SelectionKey> itKeys =
                    selector.selectedKeys().iterator(); itKeys.hasNext();) {

                    SelectionKey key = itKeys.next();

                    // Remove the key from the set
                    // Even if we are unable to spin off a new thread to handle
                    // the message, we don't want to leave it in the set for
                    // next time
                    itKeys.remove();

                    BEPHandler handler;
                    try {
                        if (key.channel() == tcpChannel) {
                            // Spin off a new handler for TCP messages
                            logger.debug("Spinning off a new BEP TCP Handler");

                            handler =
                                new BEPTCPHandler(this, db, tcpChannel.socket()
                                    .accept());
                        }
                        else if (key.channel() == udpChannel) {
                            // The channel will be used for two-way
                            // communications, but only one reader and one
                            // writer are allowed to use it at a given time
                            synchronized (l_udpChannelRead) {
                                // Receive the message
                                ByteBuffer buf =
                                    ByteBuffer.allocate(BUFFER_LENGTH);
                                SocketAddress from = udpChannel.receive(buf);

                                // Prepare for reading
                                buf.flip();

                                // Only bother trying to parse it if there is
                                // actually data there
                                if (buf.hasRemaining()) {
                                    byte[] bytes = new byte[buf.remaining()];
                                    buf.get(bytes);

                                    if (logger.isDebugEnabled()) {
                                        logger.debug("UDP bytes received: " +
                                                     Conversion
                                                         .bytesToHex(bytes));
                                    }

                                    // Check whether there is a registered
                                    // handler
                                    BEPUDPHandler udpHandler;
                                    synchronized (udpHandlers) {
                                        udpHandler = udpHandlers.get(from);
                                    }

                                    // If necessary, spin off a new handler for
                                    // UDP packets
                                    if (udpHandler == null) {
                                        if (logger.isDebugEnabled()) {
                                            // Verify that we can convert
                                            // to/from a BEPMessage without
                                            // issue
                                            BEPMessage tmsg =
                                                new BEPMessage(bytes, false);
                                            byte[] tbytes = tmsg.getBytes();

                                            if (!Arrays.equals(bytes, tbytes)) {
                                                logger
                                                    .error("Message generated a different byte array:");
                                                logger
                                                    .error("Original: " +
                                                           Conversion
                                                               .bytesToHex(bytes));
                                                logger
                                                    .error("New:      " +
                                                           Conversion
                                                               .bytesToHex(tbytes));

                                                BEPMessage alt;
                                                try {
                                                    alt =
                                                        new BEPMessage(tbytes,
                                                            false);
                                                    logger
                                                        .error("Original: " +
                                                               tmsg.toString());
                                                    logger
                                                        .error("New:      " +
                                                               alt.toString());
                                                }
                                                catch (IOException ioe) {
                                                    logger
                                                        .error(
                                                            "Unable to parse the new byte array",
                                                            ioe);
                                                }
                                            }
                                        }

                                        logger
                                            .debug("Spinning off a new BEP UDP Handler");

                                        handler =
                                            new BEPUDPHandler(this, db, from,
                                                bytes);
                                    }
                                    else {
                                        // There is already a thread, just
                                        // quietly
                                        // pass the packet over to it
                                        logger
                                            .debug("Passing the UDP message to an existing handler");

                                        handler = null;
                                        udpHandler
                                            .subsequentMessageReceived(bytes);
                                    }
                                }
                                else {
                                    // No bytes, no handler
                                    logger.debug("Empty datagram, discarding");
                                    handler = null;
                                }
                            }
                        }
                        else {
                            logger
                                .error("Unexpected source of selector exit: " +
                                       key.channel().getClass().getName());
                            continue;
                        }
                    }
                    catch (IOException ioe) {
                        logger.error(
                            "Unable to accept connection/read UDP packet", ioe);
                        continue;
                    }

                    if (handler != null) {
                        try {
                            // Enqueue a new dispatcher thread for the handler
                            dispatcher.enqueueThread(handler);
                        }
                        catch (RejectedExecutionException ree) {
                            logger.warn(
                                "Unable to schedule thread for parsing "
                                    + "IP registration message", ree);
                        }
                    }
                }
            }
        }
        catch (IllegalStateException ise) {
            // Database was probably shut down, we should probably shut down too
            running = false;

            logger.debug("Caught an IllegalStateException, assuming "
                         + "database went down", ise);
        }
        finally {
            Server.unregisterShutdownListener(this);
        }
    }

    /**
     * Send a UDP message using the existing UDP channel.
     * 
     * @param to The address to send to.
     * @param message The message data to send.
     * @throws IOException If there is a problem sending the message.
     */
    public void sendMessage(SocketAddress to, byte[] message)
        throws IOException
    {
        // The channel is non-blocking, so we need to make sure the byte array
        // does not change underneath this call
        byte[] copy = new byte[message.length];
        System.arraycopy(message, 0, copy, 0, copy.length);

        ByteBuffer buf = ByteBuffer.wrap(copy);

        // Only one writer is allowed concurrent access to the UDP channel
        synchronized (l_udpChannelWrite) {
            udpChannel.send(buf, to);
        }
    }

    /**
     * Register a UDP handler to be notified of more messages being received
     * from the given address.
     * 
     * @param address The address.
     * @param handler The handler.
     */
    public void registerUDPHandler(SocketAddress address, BEPUDPHandler handler)
    {
        synchronized (udpHandlers) {
            udpHandlers.put(address, handler);
        }
    }

    /**
     * Unregister a UDP handler from notification of more messages from the
     * given address.
     * 
     * @param address The address.
     */
    public void unregisterUDPHandler(SocketAddress address)
    {
        synchronized (udpHandlers) {
            udpHandlers.remove(address);
        }
    }

    /**
     * Get the HTTP server to use for modem-pull file transfers.
     */
    public HTTP getHTTPServer()
    {
        return httpServer;
    }

    @Override
    public void serverPanic()
    {
        running = false;
        interrupt();

        try {
            join(60000);
        }
        catch (InterruptedException ie) {
            throw new InterruptedOperationException(ie);
        }
    }

    @Override
    public void serverShutdown()
    {
        running = false;

        try {
            join();
        }
        catch (InterruptedException ie) {
            throw new InterruptedOperationException(ie);
        }
    }

    /**
     * Start this thread and the thread dispatcher that it uses.
     */
    @Override
    public void start()
    {
        super.start();

        dispatcher.start();
    }

    @Override
    public int getShutdownOrder()
    {
        // Make sure this thread goes down FIRST
        // As soon as the server has been told to shut down, there should be no
        // more updates
        return Integer.MIN_VALUE;
    }
}

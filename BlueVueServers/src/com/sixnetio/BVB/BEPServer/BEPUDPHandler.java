/*
 * BEPUDPHandler.java
 *
 * Handles BEP communications over UDP.
 *
 * Jonathan Pearson
 * September 30, 2009
 *
 */

package com.sixnetio.BVB.BEPServer;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.BVB.Modems.ModemCommunicationFailedException;
import com.sixnetio.BVB.Modems.ModemCommunicator;
import com.sixnetio.bep.*;
import com.sixnetio.util.*;

public class BEPUDPHandler
    extends BEPHandler
{
    static final Logger logger = Logger.getLogger(Utils.thisClassName());

    private class UDPModemCommunicator
        extends ModemCommunicator
    {
        boolean isOpen = true;
        private final String desc;

        public UDPModemCommunicator()
        {
            desc = from.toString();
        }

        @Override
        public String toString()
        {
            return desc;
        }

        @Override
        public void close()
        {
            if (!isOpen) {
                return;
            }

            logger.debug("Closing UDP BEP Communicator");

            // Respond to the last received message with a final ACK
            BEPMessage msg = lastReceivedMessage.buildACK(false);

            // Whether it actually gets sent, it was properly prepared and is
            // ready to be sent (or re-sent)
            lastSentMessage = msg;

            try {
                if (logger.isDebugEnabled()) {
                    logger.debug("Sending final BEP ACK:");
                    logger.debug(msg.toString());
                }

                listener.sendMessage(from, msg.getBytes());
            }
            catch (IOException ioe) {
                logger.warn("Error sending final ACK to modem", ioe);

                // Leave the last sent message alone, since we did not just send
                // a new one
                // It doesn't really matter anyway, though, since at this point
                // it will not be used again
            }

            isOpen = false;
        }

        @Override
        public void connect(Modem modem)
            throws UnsupportedOperationException,
            ModemCommunicationFailedException
        {
            throw new UnsupportedOperationException("'connect' not supported");
        }

        @Override
        public boolean connectSupported()
        {
            return false;
        }

        @Override
        public boolean isConnected()
        {
            return isOpen;
        }

        @Override
        public String receiveMessage()
            throws ModemCommunicationFailedException
        {
            // Wait for a message to become available, re-sending the last
            // message until we get our response

            // If we do not get anything back after retrying for this long, stop
            // trying
            long finalTimeout_ms =
                Server.getServerSettings().getIntValue(
                    Setting.BEPFailureTimeout) * 1000L;

            // Time between retries
            long messageTimeout_ms =
                Server.getServerSettings().getIntValue(Setting.BEPRetryTimeout) * 1000L;

            // Set an alarm to interrupt this thread once we run out of time
            Date tmDate =
                new Date(finalTimeout_ms + System.currentTimeMillis());
            Alarm.setAlarm(tmDate);

            BEPMessage message;
            synchronized (messages) {
                // Try to pull a message off
                message = messages.poll();

                // If we were unable, and as long as we do not get one, keep
                // trying
                while (message == null) {
                    try {
                        messages.wait(messageTimeout_ms);
                        message = messages.poll();

                        // Was a message received?
                        if (message == null) {
                            // No, re-send the last message since it was
                            // probably lost
                            resendPreviousMessage();
                        }
                        else {
                            // Check whether this is the next message we
                            // expected to receive by looking at the request ID
                            Short receivedRequestID = findRequestID(message);
                            if (receivedRequestID == null) {
                                receivedRequestID = 0;
                            }

                            Short expectedRequestID =
                                findRequestID(lastSentMessage);
                            if (expectedRequestID == null) {
                                expectedRequestID = 0;
                            }

                            if (!receivedRequestID.equals(expectedRequestID)) {
                                // The modem just re-sent its last response
                                // Since we have already seen that response, it
                                // the modem missed our last sent message, so we
                                // need to re-send it
                                resendPreviousMessage();

                                message = null;
                            }
                            else {
                                // This is a real new message, clear our alarm
                                Alarm.clearAlarm(tmDate);

                                // Make sure we weren't interrupted by that
                                // alarm anyway
                                Thread.interrupted();
                            }
                        }
                    }
                    catch (InterruptedException ie) {
                        if (System.currentTimeMillis() >= finalTimeout_ms) {
                            // The interrupt was caused by the alarm
                            // Clear the interrupt
                            Thread.interrupted();

                            // Close the connection
                            close();

                            // Throw a ModemCommunicationFailedException to
                            // indicate that the communications failed
                            throw new ModemCommunicationFailedException(
                                "Connection timed out");
                        }
                        else {
                            // Somebody else interrupted the thread
                            throw new InterruptedOperationException(ie);
                        }
                    }
                }
            }

            // Record that we received a new message
            lastReceivedMessage = message;

            // Pull out the AC1 data and return the response string
            String response = null;
            for (PayloadNode node : message) {
                if (node instanceof ActionPayload_v1) {
                    response = ((ActionPayload_v1)node).getAction();
                }
            }

            if (response == null) {
                // Nothing found? The modem just ended the communications, close
                // our end without sending an ACK
                listener.unregisterUDPHandler(from);
                isOpen = false;

                throw new ModemCommunicationFailedException(
                    "Client unexpectedly terminated communications");
            }

            return response;
        }

        @Override
        public void sendMessage(String command)
            throws ModemCommunicationFailedException
        {
            // Build an acknowledgment to the last message we received
            BEPMessage msg = lastReceivedMessage.buildACK(true);

            // Add a payload node for the command we are sending
            // What is the request ID from the last AC1?
            Short nextRequestID = findRequestID(lastReceivedMessage);
            if (nextRequestID == null) {
                nextRequestID = 0;
            }
            else {
                nextRequestID = (short)((nextRequestID + 1) & 0xffff);
            }

            msg.addInfoNode(new ActionPayload_v1(nextRequestID, command));

            // Sign the message, if we can
            /*
             * Something between this and the modem does not agree if (modem !=
             * null) { AuthenticationPayload_v1.signMessage(msg, makeNonce(),
             * modem.getProperty("passwordSettings/password")); }
             */

            // Whether it actually gets sent, it was properly prepared and is
            // ready to be sent (or re-sent)
            lastSentMessage = msg;

            // Send the message
            try {
                if (logger.isDebugEnabled()) {
                    logger.debug("Sending BEP ACK:");
                    logger.debug(msg.toString());
                }
                listener.sendMessage(from, msg.getBytes());
            }
            catch (IOException ioe) {
                throw new ModemCommunicationFailedException(
                    "Unable to send message: " + ioe.getMessage(), ioe);
            }
        }
    }

    /** The address that the first message came from. */
    final SocketAddress from;

    /** A queue of received messages. */
    final Queue<BEPMessage> messages = new LinkedList<BEPMessage>();

    /** If available, the modem with which we are communicating. */
    Modem modem;

    /** The last BEP message that we received. */
    BEPMessage lastReceivedMessage;

    /** The last BEP message that we sent. */
    BEPMessage lastSentMessage;

    /**
     * Construct a new BEP UDP Handler to handle a received message.
     * 
     * @param listener The listener that received the message, used for sending
     *            a response back to the source.
     * @param db The database to use for updates and information.
     * @param from The address that the message came from.
     * @param bytes The bytes (probably a BEP message) that was received.
     * @throws IOException If the message cannot be parsed.
     */
    public BEPUDPHandler(BEPListener listener, Database db, SocketAddress from,
                         byte[] bytes)
        throws IOException
    {
        super(listener, db, "BEP UDP Handler");

        MDC.put("Modem", from.toString());
        try {
            this.from = from;

            // If this fails to parse, an IOException will be thrown that will
            // prevent this thread from running, which is exactly what we want
            // This should not be an ACK
            BEPMessage message;
            try {
                message = new BEPMessage(bytes, false);
            }
            catch (IOException ioe) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Unable to parse BEP message", ioe);
                    logger.debug(Conversion.bytesToHex(bytes));
                }

                // Re-throw the exception to get out of here, we don't want the
                // handler code to run
                throw ioe;
            }

            messages.add(message);

            // Register as a handler for future messages from the same address
            listener.registerUDPHandler(from, this);

            if (logger.isDebugEnabled()) {
                logger.debug("Received a new UDP BEP message from " +
                             from.toString());
                logger.debug(message.toString());
            }
        }
        finally {
            MDC.clear();
        }
    }

    /**
     * Public only as an implementation detail.
     */
    @Override
    public void run()
    {
        MDC.put("Modem", "UDP " + from.toString());
        logger.info("New handler for UDP message running");

        // Every exit from this procedure MUST unregister from further messages
        try {
            // Read the remote address from the packet (mostly for debugging)
            String remoteAddress = from.toString();

            // There must be at least one message in the queue, pull it out and
            // parse it
            BEPMessage message;
            synchronized (messages) {
                message = messages.poll();
            }

            if (message == null) {
                // Shouldn't happen, if the caller is playing by the rules
                logger
                    .info("BEPUDPHandler constructed without an initial message");
                return;
            }

            lastReceivedMessage = message;

            // Pass the message to our parent for processing
            // Update the database with any new information received in the
            // message
            try {
                modem = parseAndStore(message);
            }
            catch (IOException ioe) {
                logger.warn("Bad BEP content", ioe);

                // No point in logging this to the database, it is virtually
                // meaningless
            }
            catch (DatabaseException de) {
                logger.error("Unable to communicate with database", de);

                DBLogger.log(null, DBLogger.ERROR,
                    "BEP server unable to update the database: " +
                        de.getMessage());
            }
            catch (ObjectInUseException oiue) {
                logger.error("Modem was busy, BEP data discarded", oiue);

                DBLogger.log(null, DBLogger.ERROR,
                    "BEP server discarding data due to a modem being busy: " +
                        oiue.getMessage());
            }

            // If the message was intended for mobile originated management, run
            // mobile-originated jobs on it
            if (message.getRequireACK()) {
                if (message.getApplicationID() == BEPMessage.APPID_MOM &&
                    modem != null) {

                    ModemCommunicator comm = new UDPModemCommunicator();
                    try {
                        /*
                         * Note: This is complicated The UDPModemCommunicator
                         * uses the fields of this object to communicate with
                         * the modem. It is passed to each of the
                         * mobile-originated jobs that are ready to run on the
                         * modem, so that they can do their stuff. All of this
                         * happens from within this thread, which means we are
                         * still responsible for unregistering from the
                         * BEPListener when we are finished.
                         */
                        runMobileOriginatedJobs(modem, comm);
                    }
                    catch (DatabaseException de) {
                        logger.error("Unable to communicate with database", de);

                        DBLogger.log(null, DBLogger.ERROR,
                            "BEP server unable to update the database: " +
                                de.getMessage());
                    }
                    finally {
                        comm.close();
                    }
                }
                else {
                    // It does not want to do MOM stuff, just send the ACK
                    BEPMessage ack = message.buildACK(false);
                    try {
                        listener.sendMessage(from, ack.getBytes());
                        lastSentMessage = ack;
                    }
                    catch (IOException ioe) {
                        logger.warn("Error sending ACK to modem", ioe);
                    }
                }
            }
        }
        finally {
            MDC.clear();

            listener.unregisterUDPHandler(from);
        }
    }

    /**
     * Called when the BEP listener receives another message intended for us.
     * 
     * @param bytes The bytes (probably a BEP message) received.
     */
    public void subsequentMessageReceived(byte[] bytes)
    {
        BEPMessage message;
        try {
            // This should NOT be an ACK; we ACK modem messages, but the modem
            // does not ACK our messages
            message = new BEPMessage(bytes, false);
        }
        catch (IOException ioe) {
            // Badly formatted message, don't count it
            logger.info("Non-BEP data was received", ioe);

            if (logger.isDebugEnabled()) {
                logger.debug("Bytes received:");
                logger.debug(Conversion.bytesToHex(bytes));
            }

            return;
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Received a BEP response:");
            logger.debug(message.toString());
        }

        if (modem != null) {
            if (!AuthenticationPayload_v1.authenticateMessage(message, modem
                .getProperty("passwordSettings/password"))) {
                logger.info("BEP message failed authentication");
                return;
            }
        }

        synchronized (messages) {
            messages.add(message);
            messages.notify();
        }
    }

    /**
     * Resend the last BEP message that we sent.
     */
    void resendPreviousMessage()
    {
        try {
            logger.info("Re-sending BEP message");
            listener.sendMessage(from, lastSentMessage.getBytes());
        }
        catch (IOException ioe) {
            logger.warn("Unable to re-send BEP message", ioe);
        }
    }
}

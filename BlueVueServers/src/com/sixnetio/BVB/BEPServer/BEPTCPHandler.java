/*
 * BEPTCPHandler.java
 *
 * Handles BEP communications over TCP.
 *
 * Jonathan Pearson
 * October 2, 2009
 *
 */

package com.sixnetio.BVB.BEPServer;

import java.io.*;
import java.net.*;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.BVB.Modems.ModemCommunicationFailedException;
import com.sixnetio.BVB.Modems.ModemCommunicator;
import com.sixnetio.bep.*;
import com.sixnetio.io.LoggedInputStream;
import com.sixnetio.util.*;

public class BEPTCPHandler
    extends BEPHandler
{
    static final Logger logger = Logger.getLogger(Utils.thisClassName());

    private class TCPModemCommunicator
        extends ModemCommunicator
    {
        private short lastRequestID = -1;
        private final String desc;

        public TCPModemCommunicator()
        {
            desc = sock.getInetAddress().getHostAddress();
        }

        @Override
        public String toString()
        {
            return desc;
        }

        @Override
        public synchronized void close()
        {
            if (sock == null) {
                return;
            }

            logger.debug("Closing TCP BEP Communicator");

            // Respond to the last received message with a final ACK
            BEPMessage msg = lastReceivedMessage.buildACK(false);

            try {
                if (logger.isDebugEnabled()) {
                    logger.debug("Sending final BEP ACK:");
                    logger.debug(msg.toString());
                }

                sock.getOutputStream().write(msg.getBytes());
                sock.close();
            }
            catch (IOException ioe) {
                logger.warn("Error sending final ACK to modem", ioe);
            }

            sock = null;
        }

        @Override
        public void connect(Modem modem)
            throws UnsupportedOperationException,
            ModemCommunicationFailedException
        {
            // We cannot create a new connection via BEP messages (only a modem
            // can initiate BEP message communications)
            throw new UnsupportedOperationException("'connect' not supported");
        }

        @Override
        public boolean connectSupported()
        {
            return false;
        }

        @Override
        public boolean isConnected()
        {
            return (sock != null);
        }

        @Override
        public String receiveMessage()
            throws ModemCommunicationFailedException
        {
            // Wait for a message to become available
            // No point in re-sending, TCP will take care of it

            // If we do not get anything back after waiting for this long, stop
            // trying
            long finalTimeout_ms =
                Server.getServerSettings().getIntValue(
                    Setting.BEPFailureTimeout) * 1000L;
            long stopTime = System.currentTimeMillis() + finalTimeout_ms;

            BEPMessage message = null;
            String response = null;

            try {
                while (message == null && System.currentTimeMillis() < stopTime) {
                    InputStream in = null;

                    try {
                        try {
                            sock.setSoTimeout((int)(stopTime - System
                                .currentTimeMillis()));
                        }
                        catch (IOException ioe) {
                            logger.warn(
                                "Unable to set a timeout on the socket", ioe);
                        }

                        in = sock.getInputStream();
                        if (logger.isDebugEnabled()) {
                            in = new LoggedInputStream(in);
                        }

                        // This should NOT be an ACK
                        message = new BEPMessage(in, false);

                        // Signal the monitor that everything is fine so far
                        monitor.checkin();

                        if (logger.isDebugEnabled()) {
                            LoggedInputStream lin = (LoggedInputStream)in;

                            logger.debug("Received a BEP response:");
                            logger.debug(Conversion.bytesToHex(lin.getBytes()));
                            logger.debug(message.toString());

                            lin.clear();
                        }

                        // No reason to check whether its request ID matches,
                        // since
                        // this is TCP, which takes care of lost packets and
                        // whatnot
                    }
                    catch (SocketTimeoutException ste) {
                        logger.error(
                            "Timed out waiting for message from modem", ste);

                        throw new ModemCommunicationFailedException(
                            "Timed out waiting for message from modem", ste);
                    }
                    catch (EOFException eofe) {
                        logger
                            .error(
                                "Unexpected EOF while reading modem response",
                                eofe);

                        if (in != null && in instanceof LoggedInputStream) {
                            LoggedInputStream lin = (LoggedInputStream)in;
                            logger.debug("Received bytes: " +
                                         Conversion.bytesToHex(lin.getBytes()));
                        }

                        throw new ModemCommunicationFailedException(
                            "Unexpected EOF while reading modem response", eofe);
                    }
                    catch (IOException ioe) {
                        logger.error("Error receiving message from modem", ioe);

                        if (in != null && in instanceof LoggedInputStream) {
                            LoggedInputStream lin = (LoggedInputStream)in;
                            logger.debug("Received bytes: " +
                                         Conversion.bytesToHex(lin.getBytes()));
                        }

                        String msg = ioe.getMessage();
                        if (msg == null) {
                            // This exception will likely show up in a
                            // user-facing log; make sure it doesn't say "null"
                            msg = ioe.getClass().getSimpleName();
                        }

                        throw new ModemCommunicationFailedException(
                            "Error receiving message from modem: " + msg, ioe);
                    }

                    if (modem != null) {
                        if (!AuthenticationPayload_v1.authenticateMessage(
                            message, modem
                                .getProperty("passwordSettings/password"))) {
                            logger
                                .info("BEP message did not pass authentication");

                            message = null;
                        }
                    }

                    // Make sure there is a payload node before breaking out
                    response = getActionResponse(message, lastRequestID);
                    if (response == null || response.length() == 0) {
                        logger.warn(String.format(
                            "Acknowledging and discarding"
                                + " message as it does not have"
                                + " an AC1 response with request" + " ID %d",
                            lastRequestID));

                        BEPMessage ack = message.buildACK(false);
                        try {
                            if (logger.isDebugEnabled()) {
                                logger
                                    .debug("Sending BEP ACK for unexpected message:");
                                logger.debug(ack.toString());
                            }

                            sock.getOutputStream().write(ack.getBytes());
                            sock.getOutputStream().flush();
                        }
                        catch (IOException ioe) {
                            logger.warn("Error sending ACK to modem", ioe);
                        }

                        message = null;
                    }
                }
            }
            finally {
                try {
                    sock.setSoTimeout(0);
                }
                catch (IOException ioe) {
                    logger.warn("Unable to clear the socket timeout", ioe);
                }
            }

            // Record that we received a new message
            lastReceivedMessage = message;

            if (response == null) {
                // Nothing found? The modem just ended the communications, close
                // our
                // end without sending an ACK
                sock = null;

                throw new ModemCommunicationFailedException(
                    "Client unexpectedly terminated communications");
            }

            return response;
        }

        private String getActionResponse(BEPMessage message, short requestID)
        {
            for (PayloadNode node : message) {
                if (node instanceof ActionPayload_v1) {
                    if (requestID == -1 ||
                        ((ActionPayload_v1)(node)).getRequestID() == requestID) {

                        return ((ActionPayload_v1)node).getAction();
                    }
                    else {
                        // There isn't going to be another AC1 payload, so no
                        // reason to keep searching when we know the request ID
                        // did not match
                        return null;
                    }
                }
            }

            return null;
        }

        @Override
        public void sendMessage(String command)
            throws ModemCommunicationFailedException
        {
            // Build an acknowledgment to the last message we received
            BEPMessage msg = lastReceivedMessage.buildACK(true);

            // Add a payload node for the command we are sending
            // What is the request ID from the last AC1?
            Short nextRequestID = findRequestID(lastReceivedMessage);
            if (nextRequestID == null) {
                nextRequestID = 0;
            }
            else {
                nextRequestID = (short)((nextRequestID + 1) & 0xffff);
            }

            msg.addInfoNode(new ActionPayload_v1(nextRequestID, command));

            // Sign the message, if we can
            /*
             * Something between this and the modem does not agree if (modem !=
             * null) { AuthenticationPayload_v1.signMessage(msg, makeNonce(),
             * modem.getProperty("passwordSettings/password")); }
             */

            // Send the message
            try {
                if (logger.isDebugEnabled()) {
                    logger.debug("Sending BEP ACK:");
                    logger.debug(msg.toString());
                }

                OutputStream out = sock.getOutputStream();
                out.write(msg.getBytes());
                out.flush();

                lastRequestID = nextRequestID;
            }
            catch (IOException ioe) {
                throw new ModemCommunicationFailedException(
                    "Unable to send message: " + ioe.getMessage(), ioe);
            }
        }
    }

    /** The socket used to communicate with the modem. */
    Socket sock;

    /** The modem that we are communicating with. */
    Modem modem;

    /** The last BEP message that we received. */
    BEPMessage lastReceivedMessage;

    /**
     * This thread has been prone to busy-lockups in the past. This should
     * either alleviate the problem, or locate it.
     */
    ThreadMonitor monitor;

    /**
     * Construct a new BEP TCP handler.
     * 
     * @param db The database.
     * @param sock The socket connected to a modem.
     */
    public BEPTCPHandler(BEPListener listener, Database db, final Socket _sock)
    {
        super(listener, db, "BEP TCP Handler");

        sock = _sock;

        try {
            // This guarantees that we will either get 1 byte or an exception
            // within the timeout period
            sock.setSoTimeout(60000);
        }
        catch (SocketException se) {
            logger.warn(
                "Unable to set socket timeout to 1 minute; leaving at default",
                se);
        }

        // This prevents the modem from sending 1 byte at a time, very very
        // slowly, by requiring that we check-in regularly; we check-in after
        // each entire BEP message is received
        monitor = new ThreadMonitor(this, 90000, new Runnable() {
            public void run()
            {
                String msg =
                    String
                        .format(
                            "Thread attached to %s (Modem %s) seems to be stuck, trying to break it out by killing the connection.",
                            sock.getInetAddress().getHostAddress(),
                            modem == null ? "(unknown)" : "#" + modem.getId());

                DBLogger.log(null, DBLogger.WARNING, msg);
                try {
                    BEPTCPHandler.this.sock.close();
                }
                catch (IOException ioe) {
                    logger.warn(
                        "Error closing socket from ThreadMonitor signaler: " +
                            ioe.getMessage(), ioe);
                }

                BEPTCPHandler.this.interrupt();
            }
        });

        logger.debug("Received a new TCP connection from " +
                     sock.getInetAddress());
    }

    /**
     * Public only as an implementation detail.
     */
    @Override
    public void run()
    {
        MDC.put("Modem", "TCP " + sock.getInetAddress().getHostAddress());
        logger.info("New handler for TCP connection running");

        monitor.start();

        // Read the remote address from the socket (mostly for debugging)
        String remoteAddress = "(unknown)";
        try {
            remoteAddress = sock.getInetAddress().getHostAddress();
        }
        catch (Exception e) {
            logger.warn("Unable to read remote address from socket", e);
        }

        try {
            // Read a BEP message from the socket
            BEPMessage bm;
            InputStream in = null;
            try {
                in = sock.getInputStream();
                if (logger.isDebugEnabled()) {
                    in = new LoggedInputStream(in);
                }

                // This should not be an ACK
                bm = new BEPMessage(in, false);

                monitor.checkin(); // Everything is working normally so far

                if (logger.isDebugEnabled()) {
                    LoggedInputStream lin = (LoggedInputStream)in;

                    logger.debug("Received a new BEP message");
                    logger.debug(Conversion.bytesToHex(lin.getBytes()));
                    logger.debug(bm.toString());

                    lin.clear();
                }
            }
            catch (SocketTimeoutException ste) {
                logger.error("Timed out waiting for message from modem", ste);

                DBLogger.log(null, DBLogger.WARNING,
                    "Timed out waiting for BEP message from socket" +
                        " connected to " + remoteAddress);

                return;
            }
            catch (IOException ioe) {
                logger.warn("Unable to read BEP message from socket " +
                            "connected to " + remoteAddress, ioe);

                if (in != null && in instanceof LoggedInputStream) {
                    LoggedInputStream lin = (LoggedInputStream)in;
                    logger.debug("Received bytes: " +
                                 Conversion.bytesToHex(lin.getBytes()));
                }

                if (ioe instanceof EOFException) {
                    DBLogger.log(null, DBLogger.WARNING,
                        "Unable to read BEP message from socket " +
                            "connected to " + remoteAddress +
                            ": Unexpected end of stream");
                }
                else {
                    String msg = ioe.getMessage();
                    if (msg == null) {
                        // This exception will likely show up in a
                        // user-facing log; make sure it doesn't say "null"
                        msg = ioe.getClass().getSimpleName();
                    }

                    DBLogger.log(null, DBLogger.WARNING,
                        "Unable to read BEP message from socket " +
                            "connected to " + remoteAddress + ": " + msg);
                }

                return;
            }
            lastReceivedMessage = bm;

            // Parse the message and save it to the database
            modem = parseAndStore(bm);

            // If the message was intended for mobile originated management, run
            // mobile-originated jobs on it
            if (bm.getRequireACK()) {
                if (bm.getApplicationID() == BEPMessage.APPID_MOM &&
                    modem != null) {

                    ModemCommunicator comm = new TCPModemCommunicator();
                    try {
                        /*
                         * Note: This is complicated The TCPModemCommunicator
                         * uses the fields of this object to communicate with
                         * the modem. It is passed to each of the
                         * mobile-originated jobs that are ready to run on the
                         * modem, so that they can do their stuff. All of this
                         * happens from within this thread.
                         */
                        runMobileOriginatedJobs(modem, comm);
                    }
                    catch (DatabaseException de) {
                        logger.error("Unable to communicate with database", de);

                        DBLogger.log(null, DBLogger.ERROR,
                            "BEP server unable to update the database: " +
                                de.getMessage());
                    }
                    finally {
                        comm.close();
                    }
                }
                else {
                    // It does not want to do MOM stuff, just send the ACK
                    BEPMessage ack = bm.buildACK(false);

                    try {
                        sock.getOutputStream().write(ack.getBytes());
                    }
                    catch (IOException ioe) {
                        logger.warn("Error sending ACK to modem", ioe);
                    }
                }
            }
        }
        catch (IOException ioe) {
            logger.warn("Bad BEP content", ioe);

            // No point in logging this to the database, it is virtually
            // meaningless (except if a modem is actually sending bad data,
            // and this is not just an arrant Internet packet...)
        }
        catch (DatabaseException de) {
            logger.error("Unable to communicate with database", de);

            DBLogger.log(null, DBLogger.ERROR,
                "BEP server unable to update the database: " + de.getMessage());
        }
        catch (ObjectInUseException oiue) {
            logger.error("Modem was busy, BEP data discarded", oiue);

            DBLogger.log(null, DBLogger.ERROR,
                "BEP server discarding data due to a modem being busy: " +
                    oiue.getMessage());
        }
        finally {
            monitor.halt();

            try {
                if (sock != null) {
                    sock.close();
                }
            }
            catch (IOException ioe) {
                logger.warn("Unable to close socket connected to " +
                            remoteAddress, ioe);
            }
        }
    }
}

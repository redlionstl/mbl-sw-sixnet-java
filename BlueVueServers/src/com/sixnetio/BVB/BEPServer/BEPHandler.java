/*
 * BEPHandler.java
 *
 * A superclass for BEP handlers. Provides the parsing and database-access
 * functionality that would otherwise be duplicated between them.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.BVB.BEPServer;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.BVB.Database.SortColumns.JobColumn;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.BVB.Modems.ModemCommunicator;
import com.sixnetio.BVB.Modems.ModemModule;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.bep.*;
import com.sixnetio.gps.GPSLib;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

/**
 * A generic handler class for dealing with received BEP Messages. It is up to
 * the subclasses to define how those messages get into the system, this class
 * only defines how they are handled afterwards.
 * 
 * @author Jonathan Pearson
 */
// Note: This does not implement ShutdownListener because it is a quick-update
// handler thread, not a long-term update/sleep cycle thread
abstract class BEPHandler
    extends Thread
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /**
     * The listener that received the first message. It knows the HTTP server,
     * and may be useful for other purposes (like sending UDP responses).
     */
    protected final BEPListener listener;

    /** The database that receives updates. */
    protected final Database db;

    /**
     * Construct a new BEP handler.
     * 
     * @param db The database to communicate with.
     * @param threadName The name of the thread.
     */
    protected BEPHandler(BEPListener listener, Database db, String threadName)
    {
        super(threadName);

        this.listener = listener;
        this.db = db;
    }

    /**
     * Parse a BEP message and store it to the database.
     * 
     * @param bm The message to parse and store.
     * @param ip The address of the modem that sent the message. Used in case a
     *            new modem needs to be added to the database.
     * @return The modem to which the update applied.
     * @throws IOException If the message was badly formatted and could not be
     *             parsed, or if it referenced a modem that did not exist and
     *             could not be created (likely due to settings restrictions).
     * @throws DatabaseException If there was a problem communicating with the
     *             database.
     * @throws ObjectInUseException If the modem lock could not be acquired (and
     *             therefore the update could not be performed).
     */
    public Modem parseAndStore(BEPMessage bm)
        throws IOException, DatabaseException, ObjectInUseException
    {
        DeviceID devID = bm.getDeviceID();

        // Get the modem (go only by device ID)
        Collection<Integer> matchingModems =
            db.getMatchingModems(null, devID, null, null, null, null, null,
                null, null, null, 0, -1, null, null);

        Modem modem;
        if (matchingModems.size() < 1) {
            // Cannot check message for authenticity, we do not know the modem's
            // password
            modem = createModemAsNecessary(devID);
        }
        else if (matchingModems.size() > 1) {
            throw new IOException("Multiple modems in the database match " +
                                  "Device ID = " + devID.toString());
        }
        else {
            int modemID = matchingModems.iterator().next();
            modem = db.getModem(modemID);

            if (!authenticateMessage(bm, modem)) {
                logger.info("Message did not pass authentication check.");
                throw new IOException("Message did not pass authentication");
            }
        }

        updateModem(modem, bm);

        return modem;
    }

    /**
     * Run all mobile-originated jobs that are due for the given modem.
     * 
     * @param modem The modem whose jobs to run.
     * @param comm The Modem Communicator to give to the job, so that it may
     *            communicate with the modem.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    public void runMobileOriginatedJobs(Modem modem, ModemCommunicator comm)
        throws DatabaseException
    {
        // Search out mobile-originated jobs that should run
        Collection<Long> jobIDs =
            db.getMatchingJobs(null, null, modem.modemID, null, null, null,
                new Date(), null, 0, -1, Utils
                    .makeArrayList(JobColumn.Schedule), null);

        logger.info(String.format("Retrieving %d jobs for Modem #%d", jobIDs
            .size(), modem.modemID));

        // No point in locking the modem unless there are actually jobs to do
        if (jobIDs.size() > 0) {
            // We need the actual, runnable jobs
            // This will skip any job types we do not understand
            Map<Long, Job> jobs = db.getJobs(jobIDs, false);

            // Acquire the modem lock
            if (db.lockModem(modem.modemID, 0) == LockResult.Acquired) {
                logger.debug(String.format("Locked Modem #%d", modem.modemID));

                try {
                    // Run through in order of ascending date (the order of
                    // jobIDs)
                    int count = 0;
                    for (long jobID : jobIDs) {
                        count++;
                        Job job = jobs.get(jobID);

                        // Is this job mobile-originated? This will also prevent
                        // Delayed Update jobs from running through this server,
                        // which is intended
                        if (!job.getJobData().parameters
                            .getBooleanContents("mobileOriginated")) {
                            // No? Skip it
                            logger.debug(String.format(
                                "Job #%d is not mobile originated", job
                                    .getJobData().jobID));
                            continue;
                        }

                        // It is mobile-originated, try to acquire the lock
                        if (db.lockJob(jobID, 0) == LockResult.Acquired) {
                            logger
                                .debug(String.format("Locked Job #%d", jobID));

                            // Make sure we unlock the job when we're done
                            try {
                                // Actually run the job
                                logger.debug("Running '" +
                                             job.getJobData().type +
                                             "' on modem '" + modem.deviceID +
                                             "'");

                                job.setModem(modem);
                                job.setModemCommunicator(comm);
                                job.setHTTPServer(listener.getHTTPServer());

                                // We'll handle unlocking the job/modem when
                                // it's complete
                                job.setUnlockWhenDone(false);

                                job.run();

                                // Special case: If the job updated the WAN IP
                                // property in the modem, save that value as the
                                // modem's IP address
                                ModemModule mm = job.getModemModule();
                                if (mm != null &&
                                    mm.propertyUpdatedRecently("WAN IP")) {
                                    // Grab the WAN IP and set it as the
                                    // modem IP
                                    Map<String, String> props =
                                        db.getModemStatus(modem.modemID, true,
                                            null, null);
                                    for (String key : props.keySet()) {
                                        if (key.startsWith("WAN IP@")) {
                                            modem.ipAddress = props.get(key);
                                            break;
                                        }
                                    }
                                }
                            }
                            finally {
                                db.unlockJob(jobID);

                                logger.debug(String.format("Done with Job #%d",
                                    jobID));
                            }
                        }
                        else {
                            logger.info(String.format(
                                "Unable to acquire lock for Job #%d", jobID));
                        }

                        if (job.getModemLikelyRebooted()) {
                            // Stop processing jobs for this modem, as the modem
                            // probably either just broke its connection, or is
                            // about to, and we don't want to see an exception
                            if (count < jobs.size()) {
                                DBLogger
                                    .log(
                                        null,
                                        DBLogger.INFO,
                                        String
                                            .format(
                                                "Postponing %d jobs for Modem #%d, as Job #%d probably rebooted it.",
                                                modem.modemID, jobs.size() -
                                                               count));
                            }

                            break;
                        }
                    }
                }
                finally {
                    db.unlockModem(modem.modemID);
                }
            }
            else {
                logger.info(String.format(
                    "Unable to acquire lock for Modem #%d", modem.modemID));
            }
        }
    }

    /**
     * To simplify the {@link #parseAndStore(String)} function, this will check
     * server settings for creating new modems, applying tags, and creating new
     * status query jobs, or throw an IOException if BEP modem creation is
     * disabled.
     * 
     * @param devID The device ID of the modem that may be created.
     * @return The modem ID of the modem that was created.
     * @throws IOException If new modem creation is disabled in the server
     *             settings.
     * @throws DatabaseException If a database exception is thrown while trying
     *             to perform the necessary database operations.
     */
    private Modem createModemAsNecessary(DeviceID devID)
        throws IOException, DatabaseException
    {
        // Are we supposed to create a new modem based on the message?
        // Or just ignore it?
        if (Server.getServerSettings().getIntValue(Setting.BEPCreateModems) == 1) {
            logger.debug("Modem with device ID " + devID.toString() +
                         " does not exist, creating one");

            // Create a new modem
            Modem modem =
                new Modem(Modem.INV_MODEM, devID, null, null, null, null, null,
                    Modem.PORT_STANDARD, null, new Tag("status"));
            db.createModem(modem);

            DBLogger.log(null, DBLogger.INFO, "Created Modem #" +
                                              modem.modemID +
                                              " in response to a BEP message");

            applyTagAsNecessary(modem.modemID);

            createStatusJobAsNecessary(modem.modemID);

            return modem;
        }
        else {
            throw new IOException("No modems in the database match " +
                                  "Device ID = " + devID.toString());
        }
    }

    /**
     * To simplify {@link #createModemAsNecessary(DeviceID, String)}, this will
     * check settings for whether to apply a tag to the modem, and apply it
     * (creating it if necessary).
     * 
     * @param modemID The modem ID of the modem to apply the tag to.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    private void applyTagAsNecessary(int modemID)
        throws DatabaseException
    {
        // Apply a tag?
        String tagName =
            Server.getServerSettings().getStringValue(Setting.BEPNewModemTag);

        if (tagName.length() > 0) {
            logger.debug("Applying tag '" + tagName + "' to Modem #" + modemID);

            // Check whether the tag exists
            List<Integer> tags =
                db.getMatchingTags(null, tagName, null, null, null, null, null,
                    0, -1, null, null);

            int tagID = -1;
            if (tags.size() == 0) {
                logger.debug("Tag '" + tagName +
                             "' does not exist, creating a new one");

                // Create the tag
                ModemTag tag;
                tag =
                    new ModemTag(ModemTag.INV_TAG, -1, tagName, "", Server
                        .getServerSettings().getIntValue(
                            Setting.NoTagPollingInterval));
                db.createTag(tag);
                tagID = tag.tagID;

                DBLogger
                    .log(null, DBLogger.INFO, "Created Tag #" + tagID +
                                              " for new modems created by BEP messages");
            }
            else if (tags.size() == 1) {
                tagID = tags.get(0);
            }
            else {
                // This shouldn't happen, tag names should be unique
                DBLogger.log(null, DBLogger.ERROR,
                    "Database contains multiple tags named '" + tagName + "'");
            }

            if (tagID != -1) {
                logger.debug("Appling Tag #" + tagID + " to Modem #" + modemID);

                db.applyTag(modemID, tagID);

                DBLogger.log(null, DBLogger.INFO, "Applied Tag #" + tagID +
                                                  " to Modem #" + modemID);
            }
        }
    }

    /**
     * To simplify {@link #createModemAsNecessary(DeviceID, String)}, this will
     * check settings for whether to create an automatic status query against a
     * modem, and create one.
     * 
     * @param modemID The modem ID of the modem to get the automatic status
     *            query.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    private void createStatusJobAsNecessary(int modemID)
        throws DatabaseException
    {
        // Create a new automatic status query?
        if (Server.getServerSettings().getIntValue(Setting.BEPCreateStatusJob) == 1) {
            // Is there a server that supports status query jobs?
            Collection<Capabilities> caps = db.getCurrentCapabilities();
            boolean found = false;
            for (Capabilities cap : caps) {
                if (cap.getJobs().contains("Status")) {
                    found = true;
                    break;
                }
            }

            if (found) {
                // Create a new Status job
                logger.debug("Creating a new automatic status query for " +
                             "Modem #" + modemID);

                // Created due to a mobile-originated message, so make it
                // mobile-originated
                Tag params = new Tag("params");
                params.addContent(new Tag("mobileOriginated", "true"));

                Job.JobData data =
                    new Job.JobData(Job.JobData.INV_JOB, null, modemID, null,
                        new Date(), 0, "Status", params, true, null, null);
                Job job = Job.makeGenericJob(data);
                db.createJob(job);

                DBLogger.log(null, DBLogger.INFO,
                    "Created automatic status query Job #" +
                        job.getJobData().jobID + " for new Modem #" + modemID);
            }
            else {
                DBLogger.log(null, DBLogger.WARNING,
                    "Unable to create a new status query for " + "new Modem #" +
                        modemID + " as no servers support Status type jobs");
            }
        }
    }

    /**
     * To simplify the {@link #parseAndStore(String)} function, this will
     * attempt to update the given modem with the specified properties, creating
     * a DelayedUpdate job if the modem is busy and that job type is supported.
     * 
     * @param modemID The ID of the modem to update.
     * @param bm The BEP Message that was received.
     * @throws DatabaseException If there is a problem accessing the database.
     * @throws ObjectInUseException If the modem is in use beyond the maximum
     *             lock timeout, and the DelayedUpdate job type is not
     *             supported.
     */
    private void updateModem(Modem modem, BEPMessage bm)
        throws DatabaseException, ObjectInUseException
    {
        long waitTime_ms =
            Server.getServerSettings().getIntValue(Setting.BEPModemWaitTimeout) * 1000L;

        // Wait until we can acquire the lock on the modem (wouldn't want to
        // update it while a status query is running, our change would be
        // immediately overwritten)
        LockResult lr = db.lockModem(modem.modemID, waitTime_ms);

        if (lr == LockResult.Acquired) {
            try {
                updateModemImmediate(modem, bm);
            }
            finally {
                db.unlockModem(modem.modemID);
            }
        }
        else {
            logger
                .debug("Unable to acquire lock on Modem #" + modem.modemID +
                       ", checking whether Delayed Update jobs are supported");

            // Schedule a Delayed Update job, if available
            Collection<Capabilities> caps = db.getCurrentCapabilities();
            boolean found = false;
            for (Capabilities cap : caps) {
                if (cap.getExtra().getStringContents("type").equals("outbound")) {

                    if (cap.getJobs().contains("!DelayedUpdate")) {
                        found = true;
                        break;
                    }
                }
            }

            if (found) {
                updateModemDelayed(modem, bm);
            }
            else {
                DBLogger.log(null, DBLogger.WARNING,
                    "Discarding BEP message from Modem #" + modem.modemID +
                        " as the modem is busy and no servers support " +
                        "!DelayedUpdate type jobs");

                throw new ObjectInUseException("Unable to acquire modem lock " +
                                               "for Modem #" + modem.modemID);
            }
        }
    }

    /**
     * Update a modem entry in the database right now. This assumes that the
     * lock on the modem has been acquired.
     * 
     * @param modemID The ID of the modem to update.
     * @param bm The BEP message with the info that should be used to perform
     *            the update.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    private void updateModemImmediate(Modem modem, BEPMessage bm)
        throws DatabaseException
    {
        Map<String, String> props = new HashMap<String, String>();

        // Read the nodes out of the message
        for (PayloadNode pn : bm) {
            if (pn instanceof ActionPayload_v1) {
                // Ignore it, we cannot do anything with this
            }
            else if (pn instanceof AuthenticationPayload_v1) {
                // Ignore this, it was done earlier
            }
            else if (pn instanceof GPSPayload_v1) {
                // Parse the GPS data
                handleGPSPayload1((GPSPayload_v1)pn, props);
            }
            else if (pn instanceof IOPayload_v1) {
                // Read out I/O data
                handleIOPayload1((IOPayload_v1)pn, props);
            }
            else if (pn instanceof IPPayload_v1) {
                // Read out IP data
                IPPayload_v1 pl = (IPPayload_v1)pn;
                modem.ipAddress = pl.getAddress().getHostAddress();
            }
            else if (pn instanceof ModemInfoPayload_v1) {
                // Read out Modem Info v1 data
                ModemInfoPayload_v1 pl = (ModemInfoPayload_v1)pn;
                modem.alias = pl.getDeviceName();
            }
            else if (pn instanceof ModemInfoPayload_v2) {
                // Read out ModemInfo v2 data
                ModemInfoPayload_v2 pl = (ModemInfoPayload_v2)pn;

                modem.fwVersion = new Version(pl.getFWVersion());
                modem.cfgVersion = pl.getCFGVersion();
                modem.setProperty("versions/prl", pl.getPRLVersion());

                // Ignore the model ID
            }
            else if (pn instanceof ModemInfoPayload_v3) {
                // Read out ModemInfo v3 data
                ModemInfoPayload_v3 pl = (ModemInfoPayload_v3)pn;

                modem.model = pl.getModel();
                modem.phoneNumber = pl.getPhoneNumber();
                modem.setProperty("serial", pl.getSerialNumber());
                modem.alias = pl.getDeviceName();
                modem.fwVersion = new Version(pl.getFwVersion());
                modem.cfgVersion = pl.getCfgVersion();
                modem.setProperty("versions/prl", pl.getPrlVersion());
                modem.setProperty("versions/gps", pl.getGpsVersion());
                modem.setProperty("versions/rf", pl.getRfVersion());
                modem.setProperty("versions/hw", pl.getHwVersion());
                modem.setProperty("versions/os", pl.getOsVersion());
                modem.setProperty("versions/uboot", pl.getBootVersion());
                modem.setProperty("simcard", pl.getSimCardNumber());

                // Ignore the model ID
            }
            else if (pn instanceof OdometerPayload_v1) {
                // Read out odometer data
                handleOdometerPayload1((OdometerPayload_v1)pn, props);
            }
            else if (pn instanceof RFPayload_v1) {
                // Read out RF v1 data
                handleRFPayload1((RFPayload_v1)pn, props);
            }
            else if (pn instanceof RFPayload_v2) {
                // Read out RF v2 data
                handleRFPayload2((RFPayload_v2)pn, props);
            }
            else {
                // Unrecognized/unsupported payload type
                logger.debug("Payload type '" + pn.getClass().getName() +
                             "' not supported");
            }
        }

        db.updateModem(modem);
        Date now = new Date();
        props.put("Contact Timestamp", Utils.formatStandardDateTime(now, true));
        db.saveStatusValues(modem.modemID, new Date(), props);

        DBLogger.log(null, DBLogger.INFO, "BEP message updated Modem #" +
                                          modem.modemID);
    }

    /**
     * Construct a Delayed Update job to update the database later. It is
     * assumed that delayed update jobs are supported.
     * 
     * @param modemID The ID of the modem to update.
     * @param bm The BEP message with the info that should be used to perform
     *            the update.
     * @throws DatabaseException If there was a problem accessing the database.
     */
    private void updateModemDelayed(Modem modem, BEPMessage bm)
        throws DatabaseException
    {
        logger
            .debug("Scheduling a new Delayed Update job to perform the update");

        Job.JobData data =
            new Job.JobData(Job.JobData.INV_JOB, null, modem.modemID, null,
                new Date(), 0, "!DelayedUpdate", new Tag("job"), false, null,
                null);

        Tag modemTag = new Tag("modem");
        data.parameters.addContent(modemTag);

        Map<String, String> props = new HashMap<String, String>();

        // Read the nodes out of the message
        for (PayloadNode pn : bm) {
            if (pn instanceof ActionPayload_v1) {
                // Ignore this, we cannot do anything with it
            }
            else if (pn instanceof AuthenticationPayload_v1) {
                // Ignore this, it was done earlier
            }
            else if (pn instanceof GPSPayload_v1) {
                // Read out GPS data
                handleGPSPayload1((GPSPayload_v1)pn, props);
            }
            else if (pn instanceof IOPayload_v1) {
                // Read out I/O data
                handleIOPayload1((IOPayload_v1)pn, props);
            }
            else if (pn instanceof IPPayload_v1) {
                // Read out IP data
                IPPayload_v1 pl = (IPPayload_v1)pn;

                Tag tag = new Tag("ipAddress");
                tag.addContent(pl.getAddress().getHostAddress());

                modemTag.addContent(tag);
            }
            else if (pn instanceof ModemInfoPayload_v1) {
                // Read out ModemInfo v1 data
                ModemInfoPayload_v1 pl = (ModemInfoPayload_v1)pn;

                Tag tag = new Tag("alias");
                tag.addContent(pl.getDeviceName());

                modemTag.addContent(tag);
            }
            else if (pn instanceof ModemInfoPayload_v2) {
                // Read out ModemInfo v2 data
                ModemInfoPayload_v2 pl = (ModemInfoPayload_v2)pn;

                Tag tag;

                modemTag.addContent(tag = new Tag("fwVersion"));
                tag.addContent(pl.getFWVersion());

                modemTag.addContent(tag = new Tag("cfgVersion"));
                tag.addContent(pl.getCFGVersion());

                modemTag.addContent(tag = new Tag("property"));
                tag.setAttribute("name", "versions/prl");
                tag.addContent(pl.getPRLVersion());

                // Ignore the model ID
            }
            else if (pn instanceof ModemInfoPayload_v3) {
                // Read out ModemInfo v3 data
                handleDelayedModemInfoPayload3((ModemInfoPayload_v3)pn,
                    modemTag);
            }
            else if (pn instanceof OdometerPayload_v1) {
                // Read out odometer data
                handleOdometerPayload1((OdometerPayload_v1)pn, props);
            }
            else if (pn instanceof RFPayload_v1) {
                // Read out RF v1 data
                handleRFPayload1((RFPayload_v1)pn, props);
            }
            else if (pn instanceof RFPayload_v2) {
                // Read out RF v2 data
                handleRFPayload2((RFPayload_v2)pn, props);
            }
            else {
                // Unrecognized/unsupported payload type
                logger.debug("Payload type '" + pn.getClass().getName() +
                             "' not supported");
            }
        }

        // Stick all of the properties into the job data
        for (Map.Entry<String, String> entry : props.entrySet()) {
            Tag tag = new Tag("status");
            tag.setAttribute("name", entry.getKey());
            tag.addContent(entry.getValue());

            modemTag.addContent(tag);
        }

        // Set the timestamp
        Tag timestamp = new Tag("timestamp");
        timestamp.addContent("" + System.currentTimeMillis());
        modemTag.addContent(timestamp);

        Job job = Job.makeGenericJob(data);
        db.createJob(job);

        DBLogger
            .log(null, DBLogger.INFO, "Scheduled Delayed Update Job #" +
                                      job.getJobData().jobID +
                                      " to update Modem #" + modem.modemID +
                                      " as the modem was busy when a BEP message arrived");
    }

    /**
     * Authenticate a BEP message.
     * 
     * @param bm The message to authenticate.
     * @param modem The modem to authenticate against.
     * @return <tt>true</tt> The message is authentic. <tt>false</tt> The
     *         message cannot be proven authentic.
     */
    private boolean authenticateMessage(BEPMessage bm, Modem modem)
    {
        return AuthenticationPayload_v1.authenticateMessage(bm, modem
            .getProperty("passwordSettings/password"));

    }

    /**
     * This will parse the GPS data from the payload and put the values into
     * <tt>props</tt>. See {@link GPSLib} for more info.
     * 
     * @param pl The payload.
     * @param props Output parameter to receive the properties.
     */
    private void handleGPSPayload1(GPSPayload_v1 pl, Map<String, String> props)
    {
        GPSLib.parseGPSData(pl.getGPSData(), props);
    }

    /**
     * Read out the I/O points in the payload and put them in <tt>props</tt>.
     * 
     * @param pl The payload to read.
     * @param props Output parameter to take the properties. This will set:
     *            <ul>
     *            <li><b>AI1</b> (float)</li>
     *            <li><b>AI2</b> (float)</li>
     *            <li><b>AI3</b> (float)</li>
     *            <li><b>PWR</b> (float)</li>
     *            <li><b>DI1</b> (int)</li>
     *            <li><b>DI2</b> (int)</li>
     *            <li><b>DI3</b> (int)</li>
     *            <li><b>DI4</b> (int)</li>
     *            <li><b>IGN</b> (int)</li>
     *            </ul>
     */
    private void handleIOPayload1(IOPayload_v1 pl, Map<String, String> props)
    {
        props.put("AI1", "" + pl.getAI1());
        props.put("AI2", "" + pl.getAI2());
        props.put("AI3", "" + pl.getAI3());
        props.put("PWR", "" + pl.getPower());

        props.put("DI1", pl.getDI1() ? "1" : "0");
        props.put("DI2", pl.getDI2() ? "1" : "0");
        props.put("DI3", pl.getDI3() ? "1" : "0");
        props.put("DI4", pl.getDI4() ? "1" : "0");
        props.put("IGN", pl.getIGN() ? "1" : "0");
    }

    /**
     * Builds the delayed-update data for the Modem Info v3 payload. A helper
     * function to shorten the building of a delayed update job.
     * 
     * @param pl The payload.
     * @param modemTag The 'modem' tag in the XML data that will receive the new
     *            tags.
     */
    private void handleDelayedModemInfoPayload3(ModemInfoPayload_v3 pl,
                                                Tag modemTag)
    {
        Tag tag;

        modemTag.addContent(tag = new Tag("model"));
        tag.addContent(pl.getModel());

        modemTag.addContent(tag = new Tag("phoneNumber"));
        tag.addContent(pl.getPhoneNumber());

        modemTag.addContent(tag = new Tag("property"));
        tag.setAttribute("name", "serial");
        tag.addContent(pl.getSerialNumber());

        modemTag.addContent(tag = new Tag("alias"));
        tag.addContent(pl.getDeviceName());

        modemTag.addContent(tag = new Tag("fwVersion"));
        tag.addContent(pl.getFwVersion());

        modemTag.addContent(tag = new Tag("cfgVersion"));
        tag.addContent(pl.getCfgVersion());

        modemTag.addContent(tag = new Tag("property"));
        tag.setAttribute("name", "versions/prl");
        tag.addContent(pl.getPrlVersion());

        modemTag.addContent(tag = new Tag("property"));
        tag.setAttribute("name", "versions/gps");
        tag.addContent(pl.getGpsVersion());

        modemTag.addContent(tag = new Tag("property"));
        tag.setAttribute("name", "versions/rf");
        tag.addContent(pl.getRfVersion());

        modemTag.addContent(tag = new Tag("property"));
        tag.setAttribute("name", "versions/hw");
        tag.addContent(pl.getHwVersion());

        modemTag.addContent(tag = new Tag("property"));
        tag.setAttribute("name", "versions/os");
        tag.addContent(pl.getOsVersion());

        modemTag.addContent(tag = new Tag("property"));
        tag.setAttribute("name", "versions/uboot");
        tag.addContent(pl.getBootVersion());

        modemTag.addContent(tag = new Tag("property"));
        tag.setAttribute("name", "simcard");
        tag.addContent(pl.getSimCardNumber());

        // Ignore the model ID
    }

    /**
     * Read out the Odometer payload info and put it into <tt>props</tt>.
     * 
     * @param pl The payload to read.
     * @param props Output parameter to take the properties. This will set:
     *            <ul>
     *            <li><b>Odometer KM</b> (float) - The odometer value, in
     *            kilometers</li>
     *            </ul>
     */
    private void handleOdometerPayload1(OdometerPayload_v1 pl,
                                        Map<String, String> props)
    {
        // The odometer value is reported in meters
        props.put("Odometer KM", "" + (pl.getOdometer() / 1000.0f));
    }

    /**
     * Read out the RF info and put it into <tt>props</tt>.
     * 
     * @param pl The payload to read.
     * @param props Output parameter to take the properties. This will set:
     *            <ul>
     *            <li><b>RSSI</b> (float) - The RSSI of the modem, in dBm</li>
     *            </ul>
     */
    private void handleRFPayload1(RFPayload_v1 pl, Map<String, String> props)
    {
        props.put("RSSI", "" + pl.getRSSI());
    }

    /**
     * Read out the RF v2 data and put it into <tt>props</tt>.
     * 
     * @param pl The payload to read.
     * @param props Output parameter to take the properties. This will set:
     *            <ul>
     *            <li><b>RSSI</b> (float) - The RSSI of the modem, in dBm</li>
     *            <li><b>Roaming Status</b> (String) - "Home" or "Roaming"</li>
     *            <li><b>Service Type</b> (String) - Service type in use</li>
     *            <li><b>Carrier</b> (String) - Carrier in use</li>
     *            </ul>
     */
    private void handleRFPayload2(RFPayload_v2 pl, Map<String, String> props)
    {
        props.put("RSSI", "" + pl.getRSSI());

        if (pl.getRoaming()) {
            props.put("Roaming Status", "Roaming");
        }
        else {
            props.put("Roaming Status", "Home");
        }

        props.put("Service Type", pl.getServiceType());
        props.put("Carrier", pl.getCarrier());
    }


    /**
     * Generates a random string to be used as a nonce, which is up to 128
     * characters long (the maximum allowed for a BEP message).
     */
    public static String makeNonce()
    {
        String nonce =
            String
                .format("%d", BigInteger.probablePrime(127 * 3, new Random()));
        if (nonce.length() > 128) {
            nonce = nonce.substring(0, 128);
        }

        return nonce;
    }

    /**
     * Find the request ID field from the given BEP message, if it has an AC1
     * payload.
     * 
     * @param msg The BEP message to search.
     * @return The request ID from the AC1 payload, or <tt>null</tt> if there is
     *         none.
     */
    public static Short findRequestID(BEPMessage msg)
    {
        ActionPayload_v1 ac1 = null;
        for (PayloadNode pn : msg) {
            if (pn instanceof ActionPayload_v1) {
                ac1 = (ActionPayload_v1)pn;
                break;
            }
        }

        if (ac1 != null) {
            return (short)(ac1.getRequestID() & 0xffff);
        }
        else {
            return null;
        }
    }
}

/*
 * Main.java
 *
 * Starts the BEP Server
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.BVB.BEPServer;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.BVB.ServerThreads.*;
import com.sixnetio.server.http.HTTP;
import com.sixnetio.util.Utils;

public class Main
{
    public static final String PROG_VERSION =
        "@bvg.major@.@bvg.minor@.@bvg.build@";

    private static final long SLEEP_BETWEEN_THREADS = 500;

    private static final Logger logger;

    private static final String CFGFILE;

    static {
        CFGFILE = Server.configureInstance("bvbepd");

        logger = Logger.getLogger(Utils.thisClassName());
    }

    private static void usage(PrintStream out)
    {
        out.println("Usage: bvbepd [-a] [-c <config file path>] [-v] [-h]");
        out
            .println(" -a  Load the classes in the 'autoload' section of the configuration file");
        out.println("       This will disable plugin scanning");
        out.println(" -c  Specify the configuration file to use");
        out
            .println("       If not specified, it is searched for in the current user's home");
        out
            .println("       directory and then the system-wide configuration directory");
        out.println(" -v  Display the program version and exit");
        out.println(" -h  Display this message and exit");

        if (CFGFILE != null) {
            out.println("       A configuration file was found in '" + CFGFILE +
                        "'");
        }
    }

    public static void main(String[] args)
    {
        Server.setNameSuffix("bepd");

        // Parse command-line arguments
        String cfgFilePath = CFGFILE; // Config file path
        boolean runAutoload = false; // Load classes in the 'autoload' section

        try {
            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("-c")) {
                    cfgFilePath = args[++i];
                }
                else if (args[i].equals("-a")) {
                    runAutoload = true;
                }
                else if (args[i].equals("-v")) {
                    System.out.println(Server.copyrightBanner(
                        "BlueVue BEP Server", PROG_VERSION));
                    return;
                }
                else if (args[i].equals("-h")) {
                    usage(System.out);
                    return;
                }
                else {
                    throw new Exception("Unknown option: " + args[i]);
                }
            }
        }
        catch (Exception e) {
            logger.fatal("Unable to start: Bad command line options", e);

            System.err.println(e.getMessage());
            usage(System.err);
            System.exit(1);
            return;
        }

        if (cfgFilePath == null) {
            logger.fatal("Unable to start: No configuration file");

            System.err.println("Unable to load configuration file: "
                               + "file does not exist");
            System.exit(1);
            return;
        }

        // Load the config file
        Tag config;
        try {
            config = Utils.xmlParser.parseXML(cfgFilePath, false, false);
        }
        catch (IOException ioe) {
            logger.fatal("Unable to start: Bad configuration file", ioe);

            System.err.println("Unable to load configuration file: " +
                               ioe.getMessage());
            System.exit(1);
            return;
        }

        // Load all the necessary classes specified in it
        if (runAutoload) {
            try {
                Initializer.loadClasses(config.getChild("autoload"));
            }
            catch (Throwable th) {
                logger.fatal("Unable to start: Cannot load class", th);

                System.err.println("Unable to load class: " + th.getMessage());
                System.exit(1);
                return;
            }
        }
        else {
            // Start up a plugin watcher and wait for it to perform an update
            try {
                Initializer.loadPlugins(config.getChild("plugins"));
            }
            catch (Throwable th) {
                logger.fatal("Unable to start", th);

                System.err.println("Unable to start: " + th.getMessage());
                System.exit(1);
                return;
            }
        }

        // Load the database
        Database db;
        try {
            db =
                Database.getInstance(config
                    .getStringContents("database/provider"));
        }
        catch (BadProviderException bpe) {
            logger.fatal("Unable to start: Bad database provider", bpe);

            System.err.println("Unable to load database: " + bpe.getMessage());
            System.exit(1);
            return;
        }
        catch (DriverException de) {
            logger.fatal("Unable to start: Cannot load database driver", de);

            System.err.println("Unable to load database driver: " +
                               de.getMessage());
            System.exit(1);
            return;
        }

        // Connect to the database
        try {
            db.connect(config.getStringContents("database/server"), config
                .getStringContents("database/name"), config
                .getStringContents("database/user"), config
                .getStringContents("database/password"), false, null,
                Database.ConnectionReason.InboundServer);
        }
        catch (DatabaseException de) {
            logger.fatal("Unable to start: Cannot connect to database", de);

            System.err.println("Unable to connect to database: " +
                               de.getMessage());
            System.exit(1);
            return;
        }

        // Make sure there isn't another active copy of this server
        Collection<Capabilities> caps;
        try {
            caps = db.getCurrentCapabilities();
        }
        catch (DatabaseException e) {
            Server.panic("Unable to start: Unable to read Capabilities table",
                e);
            return;
        }

        for (Capabilities cap : caps) {
            if (cap.getServerName().equals(Server.getName())) {
                Server.panic("Unable to start: There is already an active BEP "
                             + "server on this host");
                return;
            }
        }

        // To take better advantage of concurrency (and not cause flurries of
        // activity every X seconds), these threads are each started a bit after
        // the previous

        // Start the server's SettingsRefresher thread
        Server.startSettingsRefresher(db);

        Utils.sleep(SLEEP_BETWEEN_THREADS);

        // Start the logging thread
        DBLogger.initialize(db);

        DBLogger.log(null, DBLogger.INFO, String.format(
            "BEP server %s starting up", Server.getName()));

        // Start the capabilities updater thread
        CapabilitiesUpdater cUpdater = new CapabilitiesUpdater(db);

        // Make sure we're up-to-date so other servers don't clobber the first
        // job we acquire
        cUpdater.execute();
        cUpdater.start();

        Utils.sleep(SLEEP_BETWEEN_THREADS);

        // Start the capabilities purger thread
        CapabilitiesPurger cPurger = new CapabilitiesPurger(db);
        cPurger.start();

        Utils.sleep(SLEEP_BETWEEN_THREADS);

        // Start the HTTP server
        final HTTP httpServer;
        {
            InetSocketAddress localAddress =
                new InetSocketAddress(Server.getServerSettings().getIntValue(
                    Setting.HTTPPort));
            int backlog =
                Server.getServerSettings().getIntValue(Setting.HTTPBacklog);
            int capacity =
                Server.getServerSettings().getIntValue(Setting.HTTPCapacity);

            try {
                httpServer = new HTTP(localAddress, backlog, capacity);
            }
            catch (IOException ioe) {
                Server.panic(
                    "Unable to start: Unable to start the HTTP server: " +
                        ioe.getMessage(), ioe);
                return; // Keep the compiler happy
            }
        }
        httpServer.start();

        // Register a ShutdownListener to shut down the HTTP server when the
        // server goes down
        Server.registerShutdownListener(new ShutdownListener() {
            @Override
            public void serverShutdown()
            {
                httpServer.close();
            }

            @Override
            public void serverPanic()
            {
                // This should go down pretty quickly
                httpServer.close();
            }

            @Override
            public int getShutdownOrder()
            {
                // Go down after the Listener, but before most other threads
                // Existing requests will continue to be processed while other
                // threads go down, leading to as many successful jobs as
                // possible
                return -10;
            }
        });

        Utils.sleep(SLEEP_BETWEEN_THREADS);

        // Start the BEP server
        BEPListener server;
        try {
            server = new BEPListener(db, httpServer);
        }
        catch (IOException ioe) {
            logger.fatal("Unable to start BEP server " + Server.getName(), ioe);
            DBLogger.log(null, DBLogger.ERROR, String.format(
                "Unable to start IP registration server %s: %s", Server
                    .getName(), ioe.getMessage()));

            Server.panic("Unable to start: " + ioe.getMessage());
            return;
        }
        server.start();

        Utils.sleep(SLEEP_BETWEEN_THREADS);

        // Start the history purging thread
        HistoryPurger hPurger = new HistoryPurger(db);
        hPurger.start();

        Utils.sleep(SLEEP_BETWEEN_THREADS);

        // Start the lock purging thread
        LockPurger lPurger = new LockPurger(db);
        lPurger.start();
    }
}

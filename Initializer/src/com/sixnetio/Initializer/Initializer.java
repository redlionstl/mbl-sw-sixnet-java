/*
 * Initializer.java
 *
 * Connects to an ET2 base through the given COM port and initializes it by
 * creating and writing the necessary files.
 *
 * Jonathan Pearson
 * November 29, 2007
 *
 */

package com.sixnetio.Initializer;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeoutException;

import com.sixnetio.COM.ComPort;
import com.sixnetio.COM.UBootCommunicator;
import com.sixnetio.GUI.ProgressListener;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.server.tftp.TFTP;
import com.sixnetio.util.*;

public class Initializer extends Thread {
	/**
	 * Get the date code for today, as used by most (if not all) Sixnet devices.
	 * @return The current date code, encoded as a short with the high byte equal to the year minus 1900
	 *   and the low byte equal to the week within the current year, with 1 being the first week.
	 */
	public static short getCurrentDateCode() {
		// One byte for year, one byte for week (base 1)
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		
		int year = cal.get(Calendar.YEAR) - 1900;
		int week = cal.get(Calendar.WEEK_OF_YEAR);
		
		return (short)(((year & 0xFF) << 8) | (week & 0xFF));
	}
	
	private UserInterface ui;
	private DaxFile dax;
	private String comPath;
	
	private UDRLink handler;
	private UDRLib udr;
	private UBootCommunicator uboot;
	
	/**
	 * Construct a new Initializer object.
	 * @param ui The UserInterface used for displaying progress/error messages and asking the user for input.
	 * @param comPath The path to use when opening the communications port.
	 * @param dax The DaxFile containing the details for loading firmware and files into a device.
	 * @throws IOException If there is a problem initiating communications with the device.
	 * @throws TimeoutException If the DaxFile specifies a UBoot-based load method an we are unable to
	 *   catch UBoot within a reasonable amount of time.
	 */
	public Initializer(UserInterface ui, String comPath, DaxFile dax) throws IOException, TimeoutException {
		super("Initializer");
		
		if (ui == null) throw new RuntimeException("You must provide a UserInterface to Initializer");
		
		this.comPath = comPath;
		this.ui = ui;
		this.dax = dax;
		
		this.udr = new UDRLib(UDRMessage.STA_ANY, (byte)0);
		
		if (dax.getLoadMethod() == DaxFile.LoadMethod.UBoot) {
			ui.displayMessage(this, UserInterface.M_WARNING, "Catching UBoot...");
			
			openUBoot(true);
			
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Caught UBoot");
		} else {
			openUDR();
		}
	}
	
	/**
	 * Perform a firmware load and copy necessary files into the device. You will probably want to
	 * call this indirectly through Thread.start().
	 */
	@Override
	public void run() {
		try {
			// Load firmware based on the chosen method
			switch (dax.getLoadMethod()) {
				case Standard: {
	    			udrFirmwareLoad();
	    			
	    			break;
				}
				case UBoot: {
					ubootFirmwareLoad();
					
					ui.displayMessage(this, UserInterface.M_WARNING, "Booting the device");
					
					if (Utils.DEBUG) {
						if (!ui.confirm("Reset and continue loading the device?", "Continue?")) {
							throw new Exception("User canceled the initialization");
						}
					}
					
					uboot.writeln("reset"); // Can't use 'execCommand' here because there is no prompt afterwards
					
					closeUBoot();
					
					// Give the OS some time to close the COM port
					Utils.sleep(3000);
        			
        			// Open the COM port for UDR operations
        			// Note that this is done AFTER the UBoot load, since that needed to use the COM port
        			//   for non-UDR operations first
        			openUDR();
        			
        			udr.waitForResponse(UDRMessage.STA_ANY);
        			
        			ui.displayMessage(this, UserInterface.M_NORMAL, "  Done");
        			
        			break;
				}
				default:
					throw new RuntimeException("Unknown load method: " + dax.getLoadMethod().toString());
    		}
		
    		// Install the configuration files
			installConfigurationFiles();
			// Reset UDR-loaded stations
			// UBoot-loaded stations are already running the normal OS and don't need it
			// Plus, we can't easily tell what the serial number would be, and we can't send a reset
			//   message to an IPm station without a serial number attached
			if (dax.getLoadMethod() == DaxFile.LoadMethod.Standard) {
				resetStation(false);
				// Wait for the Station to reboot
				Utils.sleep(10000);
			}
		} catch (Exception e) {
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		} finally {
			closeUBoot();
			closeUDR();
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "Finished initializing device");
		
		ui.operationCompleted(this);
	}
	
	private void udrFirmwareLoad() throws IOException {
		boolean succeeded = false;
		
		// Try this three times
		for (int tries = 0; tries < 3; tries++) {
			if (tries > 0) ui.displayMessage(this, UserInterface.M_NORMAL, "Trying again...");
			
			try {
				// Format
				format();
				
				// Load firmware, if available
				copyFirmware();
			} catch (Exception e) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Failed");
				continue;
			}
			
			succeeded = true;
			break;
		}
		if (!succeeded) {
			throw new IOException("Station refuses to be formatted or to accept a firmware image");
		}
		else {
			for (int tries = 0; tries < 3; tries++) {
				if (tries > 0) ui.displayMessage(this, UserInterface.M_NORMAL, "Trying again...");
				
				try {
					// Load logic file, if available
					copyLogicFile();
				} catch (Exception e) {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Failed");
					continue;
				}
				
				succeeded = true;
				break;
			}
			if (!succeeded)throw new IOException("Station refuses to accept a logic image");
		}
	}
	
	private void ubootFirmwareLoad() throws IOException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Loading UBoot configuration");
		
		uboot.setRamAddress(dax.getRamRange().start);
		
		/* Note: The following UBoot variables are pretty much REQUIRED to make this work:
		 *   - serverIP -- IP address of this computer
		 *   - clientIP -- IP address that the device should use
		 *   - clientSubnet -- Subnet mask for the client device
		 *   - clientGateway -- Gateway IP for the client device
		 */
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Setting UBoot variables");
		
		// Erase the current UBoot variables, if there is an erase range specified
		if (dax.getVarRange() != null) {
			uboot.eraseRange(dax.getVarRange().start, dax.getVarRange().length);
			
			// Need to reset UBoot to clear out the variables now
			uboot.writeln("reset");
			
			Utils.sleep(1000);
			
			closeUBoot();
			
			try {
				openUBoot(false);
			} catch (TimeoutException te) {
				throw new IOException("Unable to re-capture UBoot", te);
			}
		}
		
		for (String var : dax.getUBootVariableNames()) {
    		uboot.setVariable(var, dax.getUBootVariable(var));
		}
		// Put off the flushVariables() call until after loading DAT files, since they may go into variables too
		
		// Check if any of the DAT files need to be loaded through UBoot
		for (DaxFile.DatFile df : dax) {
			switch (df.getDestinationType()) {
				case Filesystem:
					break; // Not interested in this one, it is handled elsewhere
				case Flash:
					ui.displayMessage(this, UserInterface.M_WARNING, "  Writing '" + df.getFile().getName() + "' to dataflash");
					uboot.writeBlock(dax.getBytes(df), Utils.parseLong(df.getFileDestination()));
					break;
				case UBootVar: {
					ui.displayMessage(this, UserInterface.M_WARNING, "  Writing '" + df.getFile().getName() + "' to UBoot variable '" + df.getFileDestination() + "'");
					String value = Conversion.bytesToHex(dax.getBytes(df));
					uboot.setVariable(df.getFileDestination(), value);
					break;
				}
				default:
					throw new RuntimeException("Unknown load method: " + df.getDestinationType().toString());
			}
		}
		
		uboot.flushVariables();
		
		// Load firmware images to the device
		// Turn off keepalive while doing longer operations
		uboot.stopKeepAlive();
		
		if (dax.getImageFiles().size() > 0) {
			ui.displayMessage(this, UserInterface.M_WARNING, "  Loading firmware files");
			
    		for (DaxFile.ImageFile imageFile : dax.getImageFiles()) {
    			final String fileName = imageFile.localPath;
    			
    			ui.displayMessage(this, UserInterface.M_WARNING, String.format("    Loading '%s' via TFTP -- 0.0%%", fileName));
    			
    			ProgressListener progressListener = new ProgressListener() {
    				public void updateProgress(float percent) {
    					ui.updateLastMessage(this, UserInterface.M_WARNING, String.format("    Loading '%s' via TFTP -- %.1f%%", fileName, percent * 100.0f));
                    }
    			};

    			uboot.loadFile(imageFile.localPath, true, -1, imageFile.destination.start, imageFile.destination.length, progressListener);
    			
    			// If it failed, the exception would break out before this line
    			ui.updateLastMessage(this, UserInterface.M_NORMAL, String.format("    Loading '%s' via TFTP -- 100.0%%", fileName));
    		}
		}
		
		if (TFTP.isRunning()) {
			ui.displayMessage(this, UserInterface.M_WARNING, "  Shutting down the TFTP server");
			TFTP.getTFTP().close();
		}
		
		// Done with the UBoot stuff
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Done");
	}
	
	private void installConfigurationFiles() throws IOException, TimeoutException {
		// Sort the files alphabetically according to their targets
		DaxFile.DatFile[] datFiles = dax.getFileArray();
		Arrays.sort(datFiles);
		
		if (datFiles.length > 0) ui.displayMessage(this, UserInterface.M_WARNING, "Writing files");
		
		// Put each file into the station
		for (int i = 0; i < datFiles.length; i++) {
			switch (datFiles[i].getDestinationType()) {
				case UBootVar:
					break; // Handled elsewhere
				case Flash:
					break; // Handled elsewhere
				case Filesystem: {
					byte[] data = dax.getBytes(datFiles[i]);
					final String path = datFiles[i].getFileDestination();
					
					ui.displayMessage(this, UserInterface.M_WARNING, String.format("  %s -- 0.0%%", path));
					
					ProgressListener progressListener = new ProgressListener() {
						public void updateProgress(float percent) {
							ui.updateLastMessage(this, UserInterface.M_WARNING, String.format("  %s -- %.1f%%", path, percent * 100.0f));
			            }
					};
					
					// Create and write the file
					int[] alias = new int[1];
					
					alias[0] = udr.createFile(UDRMessage.STA_ANY, path, data.length);
					udr.writeFile(UDRMessage.STA_ANY, path, data, 0, alias, progressListener);
					udr.closeAlias(UDRMessage.STA_ANY, alias[0]);
					
					// If it failed, the exception would break out before this line
					ui.updateLastMessage(this, UserInterface.M_NORMAL, String.format("  %s -- 100.0%%", path));
					
					break;
				}
				default:
					throw new IOException("Load method not recognized: " + datFiles[i].getDestinationType().toString());
			}
		}		
	}
	
	private void format() throws IOException, TimeoutException {
		String formatDir = dax.getFormatDir();
		if (formatDir != null) {
			ui.displayMessage(this, UserInterface.M_WARNING, "Formatting " + formatDir);
			
			udr.setMessageTimeout(15000); // This can be a relatively long operation
			udr.setTries(1); // Don't retry this one
			udr.createFile(UDRMessage.STA_ANY, formatDir, UDRLib.SZ_FORMAT);
			udr.setTries(3);
			udr.setMessageTimeout(3000);
			
			Utils.sleep(3000); // Give it time to finish the format
		} else {
			ui.displayMessage(this, UserInterface.M_WARNING, "No directory to format");
		}
	}
	
	private void copyFirmware() throws IOException, TimeoutException {
		if (dax.getFirmwareFile() != null && dax.getFirmwareTarget() != null) {
			ui.displayMessage(this, UserInterface.M_WARNING, "Copying firmware file '" + dax.getFirmwareFile() + "'");
			
			// First, make sure the file doesn't exist in the station
			// We expect this to fail
			boolean okay = true;
			try {
				udr.getAlias(UDRMessage.STA_ANY, dax.getFirmwareTarget());
				
				// It didn't fail?! Display an error message and retry the format
				ui.displayMessage(this, UserInterface.M_ERROR, "  Firmware file still exists after format");
				okay = false; // I hate to do it this way, but we can't throw an IOException in here
			} catch (IOException ioe) {
				// Good, it failed
			}
			
			if (!okay) throw new IOException("Firmware file still exists after format");
			
			// Grab the firmware file
			File firmfile = new File(dax.getFirmwareFile());
			if (!firmfile.isFile()) {
				firmfile = new File(dax.getLocalRoot() + File.separator + dax.getFirmwareFile());
			}
			
			if (!firmfile.isFile()) {
				throw new IOException("Unable to install firmware: '" + dax.getFirmwareFile() + "' not found or not a file.");
			}
			
			// Give this an extra try before breaking out
			boolean failed = false;
			try {
				copyFile(firmfile, dax.getFirmwareTarget());
			} catch (Exception e) {
				Utils.debug(e);
				failed = true;
			}
			
			if (failed) {
				// Reset the station and try again
				ui.displayMessage(this, UserInterface.M_ERROR, "    Failed, resetting and retrying");
				
				resetStation(true);
				
				copyFile(firmfile, dax.getFirmwareTarget());
			}
		}
	}

	private void copyLogicFile() throws IOException, TimeoutException {
		if (dax.getLogicFile() != null && dax.getLogicTarget() != null) {
			ui.displayMessage(this, UserInterface.M_WARNING, "Copying logic file '" + dax.getLogicFile() + "'");
			
			// First, make sure the file doesn't exist in the station
			// We expect this to fail
			boolean okay = true;
			try {
				udr.getAlias(UDRMessage.STA_ANY, dax.getLogicTarget());
				
				// It didn't fail?! Display an error message and retry the format
				ui.displayMessage(this, UserInterface.M_ERROR, "  Logic file still exists after format");
				okay = false; // I hate to do it this way, but we can't throw an IOException in here
			} catch (IOException ioe) {
				// Good, it failed
			}
			
			if (!okay) throw new IOException("Logic file still exists after format");
			
			// Grab the logic file
			File logicfile = new File(dax.getLogicFile());
			if (!logicfile.isFile()) {
				logicfile = new File(dax.getLocalRoot() + File.separator + dax.getLogicFile());
			}
			
			if (!logicfile.isFile()) {
				throw new IOException("Unable to install logic file: '" + dax.getLogicFile() + "' not found or not a file.");
			}
			
			// Give this an extra try before breaking out
			boolean failed = false;
			try {
				copyFile(logicfile, dax.getLogicTarget());
			} catch (Exception e) {
				Utils.debug(e);
				failed = true;
			}
			
			if (failed) {
				// Reset the station and try again
				ui.displayMessage(this, UserInterface.M_ERROR, "    Failed, resetting and retrying");
				
				resetStation(true);
				
				copyFile(logicfile, dax.getLogicTarget());
			}
		}
	}
	
	/**
	 * If the DaxFile associated with this object includes a Copy section, this will copy the files
	 * referenced by that section into the device.
	 */
	public void doCopy() {
		// Set up the message handler
		// If there is an IP address specified, set up a handler for that
		// Otherwise, set up the serial handler again
		boolean doSerial = false;
		
		if (dax.getCopyAddress() != null) {
			try {
				handler = new UDRLink(TransportMethod.UDP, dax.getCopyAddress());
				MessageDispatcher.getDispatcher().registerHandler(handler);
			} catch (IOException ioe) {
				Utils.debug(ioe);
				
				if (ui.confirm("Unable to copy files over ethernet: " + ioe.getMessage() + "; Copy files over serial?", "Network Error")) {
					doSerial = true;
				} else {
					ui.handleException(this, ioe);
					ui.operationFailed(this);
					return;
				}
			}
		} else {
			doSerial = true;
		}
		
		if (doSerial) {
			try {
				openUDR();		
			} catch (IOException ioe) {
				Utils.debug(ioe);
				
				ui.handleException(this, ioe);
				ui.operationFailed(this);
				return;
			}
		}
		
		// Increase these, since we're probably copying a lot of files
		int oldTries = udr.getTries();
		int oldDelay = udr.getFileDelay();
		
		udr.setTries(5);
		udr.setFileDelay(75);
		
		try {
			try {
				ui.displayMessage(this, UserInterface.M_WARNING, "Clearing " + dax.getCopyTarget());
				udr.deleteFile(UDRMessage.STA_ANY, dax.getCopyTarget());
			} catch (IOException ioe) {
				// Perhaps it was empty? Ignore and try to copy the real files
				ui.displayMessage(this, UserInterface.M_WARNING, "  Error clearing, attempting copy anyway");
			}
			
			File source = new File(dax.getLocalRoot() + File.separator + dax.getCopySource());
			if (!(source.isFile() || source.isDirectory())) {
				source = new File(dax.getCopySource());
			}
			
			// It'll throw an exception if there's still a problem with source
			ui.displayMessage(this, UserInterface.M_WARNING, "  Copying file from " + source);
			copyFiles(source, dax.getCopyTarget());
		} catch (Exception e) {
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		} finally {
			closeUDR();
			
			udr.setTries(oldTries);
			udr.setFileDelay(oldDelay);
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Finished copying files");
		ui.operationCompleted(this);
	}
	
	// Only throws IOException when reconnecting
	private void resetStation(boolean reconnect) throws IOException {
		udr.resetGateway(UDRMessage.STA_ANY);
		
		// Wait a moment for the message to dispatch
		Utils.sleep(100);
		
		// Close the COM port, since it will be closed by the station anyway
		// This will (hopefully) avoid the USB bug that kills the connection if
		//   the station resets while the COM port is open
		Utils.debug("Closing COM port");
		closeUDR();
		
		if (reconnect) {
			// Keep trying until the COM port opens
			while (true) {
				try {
					openUDR();
					
					break;
				} catch (IOException ioe) { }
			}
		}
	}
	
	/**
	 * Close any UBoot communicators that may be active in this object.
	 */
	public void closeUBoot() {
		if (uboot != null) {
			uboot.close();
			uboot = null;
		}
	}
	
	/**
	 * Close and unregister any UDR handlers that may be active in this object.
	 */
	public void closeUDR() {
		if (handler != null) {
			MessageDispatcher.getDispatcher().unregisterHandler(handler);
			
			handler.close();
			handler = null;
		}
	}
	
	/**
	 * Open the communications port associated with this object and attempt to catch UBoot as it loads.
	 *   Uses a 10-second timeout for catching UBoot.
	 * @param ask Whether to ask the user to disconnect/connect power to the device. Pass false if you
	 *   just gave it a reset command, so you can be sure it is coming up as this function is trying
	 *   to catch it.
	 * @throws IOException If there is a problem communicating or the user canceled the initialization.
	 * @throws TimeoutException If UBoot is not caught within the timeout period.
	 */
	public void openUBoot(boolean ask) throws IOException, TimeoutException {
		uboot = new UBootCommunicator(comPath, 9600, 8, ComPort.PARITY_NONE, ComPort.STOPBITS_1);
		
		if (ask) {
    		if (!ui.confirm("Please disconnect power from the device", "Power Off")) {
    			throw new IOException("User canceled the initialization");
    		}
		}
		
		uboot.startCatch();
		
		if (ask) {
    		if (!ui.confirm("Please apply power to the device", "Power On")) {
    			uboot.close();
    			throw new IOException("User canceled the initialization");
    		}
		}
		
		try {
    		while (!uboot.isCaught()) {
    			uboot.waitForCatch(10000);
    		}
    		
    		uboot.startKeepAlive();
		} catch (TimeoutException te) {
			uboot.close();
			throw te;
		}
	}
	
	/**
	 * Open the associated communications port and register a UDR handler for it.
	 * @throws IOException If there is a problem opening the communications port.
	 */
	private void openUDR() throws IOException {
		// Keep trying every 500ms for 30 seconds
		long startTime = System.currentTimeMillis();
		
		while (startTime + 30000 > System.currentTimeMillis()) {
    		try {
    			handler = new UDRLink(TransportMethod.Serial, comPath + " 9600-8-N-1");
    			MessageDispatcher.getDispatcher().registerHandler(handler);
    			
    			return;
    		} catch (IOException ioe) {
    			Utils.sleep(500);
    		}
		}
		
		throw new IOException("Unable to open COM port " + comPath);
	}
	
	/**
	 * Retrieve an existing setting from the attached device. A UDR or UBoot connection must be open for
	 *   this to succeed.
	 * @param destination Depends on <tt>destinationType</tt>:
	 *   <ul>
	 *     <li>
	 *       <tt>DaxFile.FileDestination.Filesystem</tt>: A file in the filesystem to be read using
	 *       UDR Filesys messages.
	 *     </li>
	 *     <li>
	 *       <tt>DaxFile.FileDestination.Flash</tt>: An address in DataFlash accessible through UBoot
	 *       commands.
	 *     </li>
	 *     <li>
	 *       <tt>DaxFile.FileDestination.UBootVar</tt>: The name of a UBoot variable.
	 *     </li>
	 *   </ul>
	 * @param destinationType See <tt>destination</tt>.
	 * @param offset The offset from the beginning of the provided destination where this variable lives.
	 * @param length The length, in bytes, of this variable.
	 * @return The contents of the specified variable, if it exists. Usually an IOException will be thrown if
	 *   the variable does not exist, but that condition may not be detectable in all cases; in that
	 *   situation, the return value will be all zeros.
	 * @throws IOException If the specified medium or variable does not exist, or a communication error
	 *   occurs.
	 * @throws TimeoutException If communications time out.
	 */
	public byte[] getValue(String destination, DaxFile.FileDestination destinationType, int offset, int length) throws IOException, TimeoutException {
		byte[] value;
		
		switch (destinationType) {
			case Filesystem: {
				value = udr.readFile(UDRMessage.STA_ANY, destination, offset, (short)length, null);
				
				break;
			}
			case Flash: {
   				value = uboot.readBlock(Utils.parseLong(destination) + offset, length);
   				
   				break;
			}
			case UBootVar: {
				String data = uboot.getVariable(destination);
				if (data == null) return null;
				
				// Convert to bytes
				byte[] temp = Conversion.hexToBytes(data);
				
				// Grab the part we want
				value = new byte[length];
				System.arraycopy(temp, offset, value, 0, length);
				
				break;
			}
			default:
				throw new RuntimeException("Unknown file destination type: " + destinationType.toString());
		}
		
		
		return value;
	}
	
	private void copyFiles(File source, String dest) throws IOException, TimeoutException {
		// Loop through all files/directories of the source, copying them
		if (source.isFile()) {
			copyFile(source, dax.getCopyTarget() + "/" + source.getName());
		} else if (source.isDirectory()) {
			File[] children = source.listFiles();
			
			Arrays.sort(children, new Comparator<File>() {
				public int compare(File f1, File f2) {
					return f1.getName().compareTo(f2.getName());
				}
			});
			
			for (int i = 0; i < children.length; i++) {
				if (children[i].isFile()) {
                    // Thumbs.db is created by Windows OS and should not be
                    // included
                    String sName = children[i].getName();
                    if (sName.contains("Thumbs.db")) {
                        ui.displayMessage(this, UserInterface.M_NORMAL,
                                "  Found Thumbs.db file - skipping this WinOS file.");
                    }
                    else {
                        copyFile(children[i], dest + "/" + children[i].getName());
                    }
				} else if (children[i].isDirectory()) {
					copyFiles(children[i], dest + "/" + children[i].getName());
				} else {
					throw new IOException("Cannot copy object that is not a file or directory: " + children[i].getAbsolutePath());
				}
			}
		} else {
			throw new IOException("Cannot copy object that is not a file or directory: " + source.getAbsolutePath());
		}
	}
	
	private void copyFile(File source, final String dest) throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, String.format("  Copying '%s' -- 0.0%%", dest));
		
		ProgressListener progressListener = new ProgressListener() {
			public void updateProgress(float percent) {
				ui.updateLastMessage(this, UserInterface.M_WARNING, String.format("  Copying '%s' -- %.1f%%", dest, percent * 100.0f));
            }
		};
		
		udr.copyFile(UDRMessage.STA_ANY, source, dest, progressListener);
		
		// If it failed, the exception would break out before this line
		ui.updateLastMessage(this, UserInterface.M_NORMAL, String.format("  Copying '%s' -- 100.0%%", dest));
	}
}

/*
 * DaxFile.java
 *
 * Represents a DAX file and its associated DAT files, and provides methods to
 * transform those files into data arrays to be loaded into a station.
 *
 * Jonathan Pearson
 * January 9, 2008
 *
 */

package com.sixnetio.Initializer;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sixnetio.P9500.P9500;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;

public class DaxFile
    implements Iterable<DaxFile.DatFile>
{
    // Loading methods for firmware
    public static enum LoadMethod
    {
        Standard, // EtherTrak 2 method
        UBoot, // IPm, managed switch, ...
    }

    // Destinations for .DAT files
    public static enum FileDestination
    {
        Filesystem, // Like '/base0/cfg/setup'
        Flash, // Like '0xd0000000'
        UBootVar, // Like 'module_factory_file'
    }

    // Legal sections in a DAX file
    private static enum Section
    {
        Files, // Contains .DAT files to load into the unit
        Images, // Contains firmware images to load into the unit and the load
        // method (Standard/UBoot)
        ExtraVars, // Contains descriptions of variables that may be used in
        // places other than .DAT files
        UBootVars, // Contains UBoot variables to set in the unit
        Test, // Contains the name of the test class to execute
        Copy, // Contains a directory to copy onto the device after
        // initialization
        CSV, // Contains the path to the CSV file to track this product, and
        // what each column contains
        MAC, // Contains the block size from which to allocate MAC addresses
        Label, // Contains the type of label to print for this unit
        GUI, // Contains GUI-specific data
        ;
    }

    public static class DatFile
        implements Comparable<DatFile>
    {
        private File datFile;
        private String fileDestination;
        private int size;
        private FileDestination destinationType;

        public DatFile(File datFile, String fileDestination, int size)
        {
            this.datFile = datFile;
            this.fileDestination = fileDestination;
            this.size = size;
            this.destinationType = FileDestination.Filesystem;
        }

        public int compareTo(DatFile o)
        {
            return fileDestination.compareTo(o.getFileDestination());
        }

        public File getFile()
        {
            return datFile;
        }

        public String getFileDestination()
        {
            return fileDestination;
        }

        public int getSize()
        {
            return size;
        }

        @Override
        public int hashCode()
        {
            return datFile.hashCode();
        }

        public FileDestination getDestinationType()
        {
            return destinationType;
        }

        protected void setDestinationType(FileDestination destinationType)
        {
            this.destinationType = destinationType;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (!(obj instanceof DatFile)) {
                return false;
            }

            DatFile d = (DatFile)obj;
            return d.getFile().equals(getFile());
        }
    }

    public static class Variable
        implements Comparable<Variable>
    {
        private String name;
        private DatFile datFile; // DAT file this variable appeared in
        private int line; // Line that it appeared on, for debugging
        private String comment;
        private int offset;
        private int length; // Use -1 for unlimited length Strings
        private byte[] value;
        private String stringValue;
        private byte[] defaultValue; // May be null
        private String defaultStringValue; // May be null

        // NOTE: This feature is intended for MAC addresses, and as such, is
        // only implemented for MAC addresses in the GUI Initializer code
        private int offsetBy; // If the variable is followed by (#), ask the
        // user for variable + #

        private boolean valSet;

        public Variable(String name, DatFile datFile, int line, String comment,
                        String defaultValue, int offset, int length,
                        int offsetBy)
        {
            this.name = name;
            this.datFile = datFile;
            this.line = line;
            this.comment = comment;

            if (length == -1) {
                this.defaultStringValue = defaultValue;
            }
            else if (defaultValue != null) {
                this.defaultValue = Conversion.hexToBytes(defaultValue);
            }

            this.offset = offset;
            this.length = length;
            this.offsetBy = offsetBy;

            this.value = null;
            this.stringValue = null;
            this.valSet = false;
        }

        public String getDescription()
        {
            String desc = getComment();
            if (desc.length() == 0) {
                desc = getName();
                desc =
                    desc.substring(0, 1).toUpperCase() +
                        desc.substring(1).toLowerCase();
            }

            return desc;
        }

        public int compareTo(Variable o)
        {
            return name.compareTo(o.getName());
        }

        public String getName()
        {
            return name;
        }

        public DatFile getDatFile()
        {
            return datFile;
        }

        public int getLine()
        {
            return line;
        }

        public String getComment()
        {
            return comment;
        }

        public int getOffset()
        {
            return offset;
        }

        public int getLength()
        {
            return length;
        }

        public byte[] getValue()
        {
            return value;
        }

        public byte[] getDefaultValue()
        {
            return defaultValue;
        }

        public String getStringValue()
        {
            return stringValue;
        }

        public String getDefaultStringValue()
        {
            return defaultStringValue;
        }

        public int getOffsetBy()
        {
            return offsetBy;
        }

        public boolean isSet()
        {
            return valSet;
        }

        public void setValue(byte[] value)
        {
            if (length == -1) {
                throw new IllegalStateException(
                    "Cannot set binary value of a string variable (" + name +
                        ")");
            }
            if (value.length != length) {
                throw new IllegalArgumentException("Bad value length: " +
                                                   value.length + ", must be " +
                                                   length + " (" + name + ")");
            }

            this.value = value;
            valSet = true;
        }

        public void setStringValue(String value)
        {
            if (length >= 0) {
                throw new IllegalStateException(
                    "Cannot set string value of a binary valiable (" + name +
                        ")");
            }

            this.stringValue = value;
            valSet = true;
        }

        public boolean isStringVariable()
        {
            return (length == -1);
        }

        @Override
        public int hashCode()
        {
            return name.hashCode();
        }

        @Override
        public boolean equals(Object obj)
        {
            if (!(obj instanceof Variable)) {
                return false;
            }

            Variable v = (Variable)obj;
            return v.getName().equals(getName());
        }
    }

    public static class MemoryRange
    {
        public final long start;
        public final int length;

        public MemoryRange(long start, int length)
        {
            this.start = start;
            this.length = length;
        }
    }

    public static class ImageFile
    {
        public final MemoryRange destination;
        public final String localPath;

        public ImageFile(MemoryRange destination, String localPath)
        {
            this.destination = destination;
            this.localPath = localPath;
        }
    }

    // Private data members
    private String localRoot; // Directory containing DAX file
    private LoadMethod loadMethod = LoadMethod.Standard;

    // For LM_Standard
    private String formatDir = null; // Directory to format before loading
    // firmware
    private String firmwareTarget = null; // Location in the device to place the
    // firmware image
    private String firmwareFile = null; // Location on the local machine to find
    // the firmware image
    private String logicTarget = null; // Location in the device to place the
    // logic image
    private String logicFile = null; // Location on the local machine to find
    // the logic image

    // For LM_UBoot
    private MemoryRange ram = null; // Where the RAM is mounted in UBoot
    private MemoryRange varRange = null; // Where the UBoot variables are
    // mounted (so we can clear them)
    private Map<String, String> ubootVars; // UBoot variables to set (read-only
    // after construction)
    private List<ImageFile> images = null; // Images to load into the unit
    // (read-only after construction)


    // Unrelated to load methods
    private Set<DatFile> datFiles; // Read-only after construction
    // Map from variable name to variable structure (read-only after
    // construction)
    private Map<String, Variable> variables;
    private int blockSize = -1; // Depends on the number of MAC addresses, or on
    // the MAC->blockSize value

    // If there is a GUI section
    private File logoImage = null; // Logo image, if provided

    // If there is a label section...
    private String labelType;
    // Map from parameter name to value, which may contain variable references
    // starting with $ (read-only after construction)
    private Map<String, String> labelContents;

    // If there is a test section...
    private String testClass;
    private String devName;

    // If there is a CSV section...
    private String csvFile;
    private Vector<String> csvValues;

    // If there is a Copy section...
    private String copySrc;
    private String copyDest;
    private String copyAddr;

    public DaxFile(String daxFile)
        throws IOException
    {
        this(new File(daxFile));
    }

    public DaxFile(File daxFile)
        throws IOException
    {
        datFiles = new HashSet<DatFile>();
        variables = new LinkedHashMap<String, Variable>(16, .75f, false); // So
        // it
        // will
        // always
        // display
        // in
        // the
        // same
        // order
        labelContents = new Hashtable<String, String>();
        csvValues = new Vector<String>();

        parseDAX(daxFile); // Get DAT file names
    }

    private void parseDAX(File file)
        throws IOException
    {
        if (!file.isFile()) {
            throw new IOException("DAX file (" + file.getAbsolutePath() +
                                  ") does not exist or is not a file");
        }

        localRoot = file.getParentFile().getAbsolutePath();

        BufferedReader in = openFile(file);


        try {
            String lineIn;
            Section section = null;
            int[] lineNum = new int[1];

            try {
                while ((lineIn = getNextLine(in, lineNum)) != null) {
                    // Section heading
                    if (lineIn.matches("^\\[.*\\]$")) {
                        String sectionName =
                            lineIn.substring(1, lineIn.length() - 1);

                        try {
                            section = Section.valueOf(sectionName);
                        }
                        catch (IllegalArgumentException iae) {
                            throw new IOException(String.format(
                                "Unknown section '%s'", sectionName));
                        }

                        continue;
                    }

                    switch (section) {
                        case CSV: {
                            if (lineIn.indexOf('=') == -1) {
                                throw new IOException(
                                    "CSV section must be made up of key=value pairs");
                            }

                            String key =
                                lineIn.substring(0, lineIn.indexOf('='));
                            String value =
                                lineIn.substring(lineIn.indexOf('=') + 1);

                            if (key.equals("file")) {
                                value = Utils.correctPathToOS(value);

                                if (Utils.DEBUG) {
                                    // Don't corrupt the real one if debugging
                                    File f = new File(value);
                                    csvFile = "C:\\Temp\\" + f.getName();
                                }
                                else {
                                    csvFile = value;
                                }
                            }
                            else if (key.startsWith("val")) {
                                int index =
                                    Integer.parseInt(key.substring("val"
                                        .length()));
                                if (index >= csvValues.size()) {
                                    csvValues.setSize(index + 1);
                                }
                                csvValues.set(index, value);
                            }

                            break;
                        }
                        case Copy: {
                            if (lineIn.indexOf('=') == -1) {
                                throw new IOException(
                                    "Copy section must be made up of key=value pairs");
                            }

                            String key =
                                lineIn.substring(0, lineIn.indexOf('='));
                            String value =
                                lineIn.substring(lineIn.indexOf('=') + 1);

                            if (key.equals("source")) {
                                value = Utils.correctPathToOS(value);

                                copySrc = value;
                            }
                            else if (key.equals("target")) {
                                copyDest = value;
                            }
                            else if (key.equals("address")) {
                                copyAddr = value;
                            }
                            else {
                                throw new IOException(String.format(
                                    "Unknown Copy section property: '%s'", key));
                            }

                            break;
                        }
                        case ExtraVars: {
                            Pattern pattern =
                                Pattern
                                    .compile("([\\S]*)\\[([sS]|[\\d]*)\\]=(.*?)(\\[(.*)\\])?");
                            Matcher matcher = pattern.matcher(lineIn);

                            if (!matcher.matches()) {
                                throw new IOException(
                                    "ExtraVars section must be made up of variable[length]=description lines");
                            }

                            String varName = matcher.group(1);
                            String varLength = matcher.group(2);
                            String varDesc = matcher.group(3);
                            String varDefault = matcher.group(5);

                            int length = -1;
                            if (!varLength.equalsIgnoreCase("s")) {
                                length = Integer.parseInt(varLength);
                            }

                            Variable var =
                                new Variable(varName, null, lineNum[0],
                                    varDesc, varDefault, 0, length, 0);
                            variables.put(varName, var);

                            break;
                        }
                        case Files: {
                            File datFile =
                                new File(file.getParentFile(), lineIn);
                            String stationFile = getFileDestination(datFile);
                            int size = getFileSize(datFile);

                            DatFile dat =
                                new DatFile(datFile, stationFile, size);
                            datFiles.add(dat);

                            findVariables(dat);

                            break;
                        }
                        case Images: {
                            if (lineIn.startsWith("method=")) {
                                String methodName =
                                    lineIn.substring("method=".length());

                                try {
                                    loadMethod = LoadMethod.valueOf(methodName);

                                    if (loadMethod == LoadMethod.UBoot) {
                                        ubootVars =
                                            new Hashtable<String, String>();
                                        images = new Vector<ImageFile>();
                                    }
                                }
                                catch (IllegalArgumentException iae) {
                                    // Make sure to update this message with new
                                    // load methods
                                    throw new IOException(String.format(
                                        "Unrecognized load method: '%s'",
                                        methodName));
                                }
                            }
                            else {
                                switch (loadMethod) {
                                    case Standard: {
                                        if (lineIn.startsWith("format=")) {
                                            formatDir =
                                                lineIn.substring("format="
                                                    .length());
                                        }
                                        else if (lineIn.startsWith("firmware=")) {
                                            firmwareFile =
                                                lineIn.substring("firmware="
                                                    .length());

                                            firmwareFile =
                                                Utils
                                                    .correctPathToOS(firmwareFile);
                                        }
                                        else if (lineIn
                                            .startsWith("firmtarget=")) {
                                            firmwareTarget =
                                                lineIn.substring("firmtarget="
                                                    .length());
                                        }
                                        else if (lineIn.startsWith("logic=")) {
                                            logicFile =
                                                lineIn.substring("logic="
                                                    .length());

                                            logicFile =
                                                Utils
                                                    .correctPathToOS(logicFile);
                                        }
                                        else if (lineIn
                                            .startsWith("logictarget=")) {
                                            logicTarget =
                                                lineIn.substring("logictarget="
                                                    .length());
                                        }
                                        else {
                                            throw new IOException(
                                                String
                                                    .format(
                                                        "Unknown property for Standard load method: '%s'",
                                                        lineIn));
                                        }

                                        break;
                                    }
                                    case UBoot: {
                                        if (lineIn.startsWith("ram=")) {
                                            ram =
                                                parseMemoryRange(lineIn
                                                    .substring("ram=".length()));
                                        }
                                        else {
                                            String range =
                                                lineIn.substring(0, lineIn
                                                    .indexOf(':'));
                                            String path =
                                                lineIn.substring(
                                                    lineIn.indexOf(':') + 1)
                                                    .trim();

                                            images.add(new ImageFile(
                                                parseMemoryRange(range), path));
                                        }

                                        break;
                                    }
                                    default:
                                        throw new IOException(
                                            "Load method not specified");
                                }
                            }

                            break;
                        }
                        case Label: {
                            if (lineIn.indexOf('=') == -1) {
                                throw new IOException(
                                    "Label section must be made up of key=value pairs");
                            }

                            String key =
                                lineIn.substring(0, lineIn.indexOf('='));
                            String value =
                                lineIn.substring(lineIn.indexOf('=') + 1);

                            if (key.equals("type")) {
                                labelType = value;
                            }
                            else {
                                labelContents.put(key, value);
                            }

                            break;
                        }
                        case MAC: {
                            if (lineIn.indexOf('=') == -1) {
                                throw new IOException(
                                    "MAC section must be made up of key=value pairs");
                            }

                            String key =
                                lineIn.substring(0, lineIn.indexOf('='));
                            String value =
                                lineIn.substring(lineIn.indexOf('=') + 1);

                            if (key.equals("blockSize")) {
                                blockSize = Integer.parseInt(value);
                                if (blockSize > 16) {
                                    throw new IOException(
                                        "Cannot have a MAC block size greater than 16");
                                }
                            }
                            else {
                                throw new IOException(String.format(
                                    "Illegal MAC section property: '%s'", key));
                            }

                            break;
                        }
                        case Test: {
                            if (lineIn.indexOf('=') == -1) {
                                throw new IOException(
                                    "Test section must be made up of key=value pairs");
                            }

                            String key =
                                lineIn.substring(0, lineIn.indexOf('='));
                            String value =
                                lineIn.substring(lineIn.indexOf('=') + 1);

                            if (key.equals("testClass")) {
                                testClass = value;
                            }
                            else if (key.equals("devName")) {
                                devName = value;
                            }
                            else {
                                throw new IOException(String.format(
                                    "Illegal Test section property: '%s'", key));
                            }

                            break;
                        }
                        case UBootVars: {
                            if (lineIn.indexOf('=') == -1) {
                                throw new IOException(
                                    "UBootVars section must be made up of key=value pairs");
                            }

                            String key =
                                lineIn.substring(0, lineIn.indexOf('='));
                            String value =
                                lineIn.substring(lineIn.indexOf('=') + 1);

                            if (key.startsWith("%erase")) {
                                varRange = parseMemoryRange(value);
                            }
                            else {
                                ubootVars.put(key, value);
                            }

                            break;
                        }
                        case GUI: {
                            if (lineIn.indexOf('=') == -1) {
                                throw new IOException(
                                    "GUI section must be made up of key=value pairs");
                            }

                            String key =
                                lineIn.substring(0, lineIn.indexOf('='));
                            String value =
                                lineIn.substring(lineIn.indexOf('=') + 1);

                            if (key.equals("logo")) {
                                logoImage =
                                    new File(Utils.correctPathToOS(value));

                                if (!logoImage.canRead()) {
                                    // FUTURE: Display a warning?
                                    logoImage = null;
                                }
                            }
                            else {
                                throw new IOException(String.format(
                                    "Illegal GUI section property: '%s'", key));
                            }

                            break;
                        }
                        default:
                            throw new IOException("Data not in a section");
                    }
                }
            }
            catch (Exception e) {
                throw new IOException(String.format(
                    "Bad file format (%s:%d): %s", file.getAbsolutePath(),
                    lineNum[0], e.getMessage()), e);
            }

            try {
                // A few consistency checks
                if (loadMethod == LoadMethod.Standard) {
                    if (firmwareFile == null ^ firmwareTarget == null) {
                        throw new IOException(
                            "'firmware' and 'firmtarget' properties in the 'Files' section must be provided as a pair or not at all");
                    }
                }
                else if (loadMethod == LoadMethod.UBoot) {
                    // No necessary checks here
                }

                if (copySrc == null ^ copyDest == null) {
                    throw new IOException(
                        "'source' and 'target' properties in the 'Copy' section must be provided as a pair or not at all");
                }
                else if (copySrc == null && copyDest == null &&
                         copyAddr != null) {
                    throw new IOException(
                        "'address' property in the 'Copy' section is meaningless without both 'source' and 'target' properties");
                }
                else if (!labelContents.isEmpty() && labelType == null) {
                    throw new IOException(
                        "label properties are meaningless without a 'type' property in the 'Label' section");
                }

                if (csvFile == null ^ csvValues.isEmpty()) {
                    throw new IOException(
                        "If a CSV file is provided, at least one value must also be provided");
                }
            }
            catch (Exception e) {
                throw new IOException(String.format("Bad file format (%s): %s",
                    file.getAbsolutePath(), e.getMessage()), e);
            }
        }
        finally {
            in.close();
        }
    }

    // Note: This will throw an IOException if the same variable name is
    // encountered more than once with different lengths
    private void findVariables(DatFile datFile)
        throws IOException
    {
        int[] lineNum = new int[1];
        int macAddrCount = 0;

        BufferedReader in = openFile(datFile.getFile());
        try {
            String lineIn;
            while ((lineIn = getNextLine(in, lineNum)) != null) {
                if (lineIn.startsWith("filename=")) {

                    datFile.setDestinationType(FileDestination.Filesystem);

                    continue; // Not interested in those lines
                }
                else if (lineIn.startsWith("flashAddr=")) {
                    datFile.setDestinationType(FileDestination.Flash);

                    continue;
                }
                else if (lineIn.startsWith("ubootVar=")) {
                    datFile.setDestinationType(FileDestination.UBootVar);

                    continue;
                }
                else if (lineIn.startsWith("size=")) {
                    continue;
                }

                // Tokenize the line
                StringTokenizer tok = new StringTokenizer(lineIn);

                if (tok.countTokens() < 3) {
                    throw new IOException("Bad file format (" +
                                          datFile.getFile().getName() + ":" +
                                          lineNum[0] +
                                          "): each line must contain offset, length, and value");
                }

                int offset = Integer.parseInt(tok.nextToken(), 16);
                int length = Integer.parseInt(tok.nextToken(), 16);
                String value = tok.nextToken();
                StringBuilder commentBuilder = new StringBuilder();

                while (tok.hasMoreTokens()) {
                    commentBuilder.append(tok.nextToken());
                    commentBuilder.append(" ");
                }

                String comment = commentBuilder.toString().trim();

                if (value.startsWith("$")) {
                    String varname = value.substring(1);
                    int offsetBy = 0;

                    if (varname.indexOf('(') != -1) {
                        String parens = varname.substring(varname.indexOf('('));
                        varname = varname.substring(0, varname.indexOf('('));

                        parens = parens.substring(1); // Cut off the (
                        if (parens.endsWith(")")) {
                            parens = parens.substring(0, parens.length() - 1);
                        }
                        else {
                            throw new IOException(
                                "Variable " + varname +
                                    " includes an '(' but no ')' on line " +
                                    lineNum[0] + " of " +
                                    datFile.getFile().getName());
                        }

                        offsetBy = Integer.parseInt(parens);
                    }

                    Variable variable =
                        new Variable(varname, datFile, lineNum[0], comment,
                            null, offset, length, offsetBy);
                    Variable old = variables.put(variable.getName(), variable);

                    if (variable.getName().startsWith("macaddr")) {
                        macAddrCount++;
                    }

                    if (old != null && old.getLength() != variable.getLength()) {
                        throw new IOException("Variable " +
                                              variable.getName() +
                                              " appears on line " +
                                              lineNum[0] +
                                              " of " +
                                              datFile.getFile().getName() +
                                              ", but first appeared on line " +
                                              old.getLine() +
                                              " of " +
                                              old.getDatFile().getFile()
                                                  .getName());
                    }
                }
            }
        }
        finally {
            in.close();
        }

        if (blockSize == -1 && macAddrCount > 0) {
            if (macAddrCount == 1) {
                blockSize = 1;
            }
            else if (macAddrCount <= 8) {
                blockSize = 8;
            }
            else if (macAddrCount <= 16) {
                blockSize = 16;
            }
            else {
                throw new IOException(
                    "Cannot have a MAC block size greater than 16");
            }
        }
    }

    public byte[] getBytes(DatFile datFile)
        throws IOException
    {
        byte[] ans = new byte[datFile.getSize()];

        Utils.debug("Parsing " + datFile.getFile().getName());
        int crc = 0;

        BufferedReader in = openFile(datFile.getFile());
        int[] lineNum = new int[1];
        try {
            String lineIn;
            while ((lineIn = getNextLine(in, lineNum)) != null) {
                if (lineIn.startsWith("size=") ||
                    lineIn.startsWith("filename=") ||
                    lineIn.startsWith("flashAddr=") ||
                    lineIn.startsWith("ubootVar=")) {

                    // Ignore these
                    continue;
                }

                // Tokenize the line
                StringTokenizer tok = new StringTokenizer(lineIn);

                if (tok.countTokens() < 3) {
                    throw new IOException("Bad file format (" +
                                          datFile.getFile().getName() + ":" +
                                          lineNum[0] +
                                          "): each line must contain offset, length, and value");
                }

                // First token = offset on the line
                int offset = Integer.parseInt(tok.nextToken(), 16);

                // Second token = length of the value
                int length = Integer.parseInt(tok.nextToken(), 16);

                if (offset < 0) {
                    throw new IOException("Bad file format (" +
                                          datFile.getFile().getName() + ":" +
                                          lineNum[0] + "): negative offset");
                }
                if (offset + length > ans.length) {
                    throw new IOException("Bad file format (" +
                                          datFile.getFile().getName() + ":" +
                                          lineNum[0] +
                                          "): Writing data past end of file");
                }

                // Third token can be %function, $varname, or xx...
                String value = tok.nextToken();

                Utils.debug(String.format("  %04x-%04x: %s", offset,
                    offset + length - 1, value));

                if (value.startsWith("%")) {
                    // Function call, figure out which
                    if (value.startsWith("%fill=")) {
                        // Fill the region with the specified byte value
                        byte b =
                            (byte)(Integer.parseInt(value.substring("%fill="
                                .length()), 16) & 0xff);

                        Utils.debug(String.format(
                            "    Filling %,d bytes with 0x%02x", length, b));

                        Arrays.fill(ans, offset, offset + length, b);
                    }
                    else if (value.startsWith("%crc")) {
                        // Fill the region with a CRC of all data up to this
                        // point
                        crc = Utils.crc32(ans, 0, offset);

                        Utils.debug(String
                            .format("    CRC of data up to 0x%02x: 0x%04x",
                                offset, crc));

                        Conversion.intToBytes(ans, offset, crc);
                    }
                    else {
                        throw new IOException("Bad file format (" +
                                              datFile.getFile().getName() +
                                              ":" + lineNum[0] +
                                              "): unknown function '" + value +
                                              "'");
                    }
                }
                else if (value.startsWith("$")) {
                    String varname = value.substring(1);
                    if (varname.indexOf('(') != -1) {
                        varname = varname.substring(0, varname.indexOf('('));
                    }

                    Variable variable = variables.get(varname);

                    // If the variable doesn't exist, it has been added since
                    // the first parse
                    if (variable == null) {
                        throw new IOException("File " +
                                              datFile.getFile().getName() +
                                              " has changed");
                    }

                    // If the variable has changed location/size, the file has
                    // changed since the first parse
                    if (variable.getOffset() != offset) {
                        throw new IOException("File " +
                                              datFile.getFile().getName() +
                                              " has changed");
                    }
                    if (variable.getLength() != length) {
                        throw new IOException("File " +
                                              datFile.getFile().getName() +
                                              " has changed");
                    }

                    // And of course, if it hasn't been set, that's a real
                    // problem...
                    if (!variable.isSet()) {
                        throw new IOException("Variable '" +
                                              variable.getName() +
                                              "' not set (" +
                                              datFile.getFile().getName() +
                                              ":" + lineNum[0] + ")");
                    }

                    if (Utils.DEBUG) {
                        Utils.debug("    Variable " + variable.getName() +
                                    " = " +
                                    Conversion.bytesToHex(variable.getValue()));
                    }

                    System.arraycopy(variable.getValue(), 0, ans, offset,
                        length);
                }
                else if (value.matches("[0-9a-zA-Z]+")) {
                    if ((value.length() & 1) == 1) {
                        value = "0" + value; // Prepend a 0 to make it even
                        // length, if necessary
                    }

                    Utils.debug("    Simple value: " + value);

                    for (int i = 0; i < value.length(); i += 2) {
                        byte b =
                            (byte)(Integer.parseInt(value.substring(i, i + 2),
                                16) & 0xff);
                        ans[offset++] = b;
                    }
                }
                else {
                    throw new IOException("Bad file format (" +
                                          datFile.getFile().getName() + ":" +
                                          lineNum[0] +
                                          "): value is not a %fill, $variable, or hex number");
                }
            }

            Utils.debug("  ", ans);

            return ans;
        }
        finally {
            in.close();
        }
    }

    public void setVariable(String name, byte[] value)
    {
        Variable variable = variables.get(name);
        if (variable == null) {
            throw new IllegalArgumentException("There is no variable named " +
                                               name);
            // Length check is done by Variable
        }

        // We do this so the user cannot accidentally change it after setting
        byte[] realVal = new byte[value.length];
        System.arraycopy(value, 0, realVal, 0, realVal.length);

        variable.setValue(realVal);
    }

    public void setVariable(String name, byte value)
    {
        byte[] realVal = new byte[1];
        realVal[0] = value;

        setVariable(name, realVal);
    }

    public void setVariable(String name, short value)
    {
        byte[] realVal = new byte[2];
        Conversion.shortToBytes(realVal, 0, value);

        setVariable(name, realVal);
    }

    public void setVariable(String name, int value)
    {
        byte[] realVal = new byte[4];
        Conversion.intToBytes(realVal, 0, value);

        setVariable(name, realVal);
    }

    public void setVariable(String name, long value)
    {
        byte[] realVal = new byte[8];
        Conversion.longToBytes(realVal, 0, value);

        setVariable(name, realVal);
    }

    public Variable getVariable(String name)
    {
        return variables.get(name);
    }

    public Set<String> getVariableNames()
    {
        return new HashSet<String>(variables.keySet());
    }

    // Iterates over DAT files
    public Iterator<DatFile> iterator()
    {
        return Utils.readOnlyIterator(datFiles.iterator());
    }

    public int getFileCount()
    {
        return datFiles.size();
    }

    public DatFile[] getFileArray()
    {
        return datFiles.toArray(new DatFile[datFiles.size()]);
    }

    public int getBlockSize()
    {
        return blockSize;
    }

    // Returns null if not set
    public String getFormatDir()
    {
        return formatDir;
    }

    public String getFirmwareFile()
    {
        return firmwareFile;
    }

    public String getFirmwareTarget()
    {
        return firmwareTarget;
    }
    public String getLogicFile()
    {
        return logicFile;
    }

    public String getLogicTarget()
    {
        return logicTarget;
    }
    
    public MemoryRange getRamRange()
    {
        return ram;
    }

    public MemoryRange getVarRange()
    {
        return varRange;
    }

    public List<ImageFile> getImageFiles()
    {
        return new Vector<ImageFile>(images);
    }

    public LoadMethod getLoadMethod()
    {
        return loadMethod;
    }

    public Set<String> getUBootVariableNames()
    {
        return new HashSet<String>(ubootVars.keySet());
    }

    public String getUBootVariable(String name)
    {
        String value = ubootVars.get(name);

        if (value.startsWith("$")) {
            // Variable name
            Variable var = variables.get(value.substring(1));

            if (var.isStringVariable()) {
                return var.getStringValue();
            }
            else {
                return Conversion.bytesToHex(var.getValue());
            }
        }
        else {
            return value;
        }
    }

    public boolean hasLabel()
    {
        return (labelType != null);
    }

    public void printLabel()
        throws IOException
    {
        // Figure out the label type so we know what values we need
        if (labelType == null) {
            throw new IOException("No Label section specified in file");
        }
        else if (labelType.equals("3mac")) {
            // Convert the last three bytes of the MAC address to a string
            String mac = labelContents.get("mac");

            if (mac == null) {
                throw new IOException(labelType +
                                      " label type requires 'mac' value to be set");
            }

            // Search for any variable names in the value string
            if (mac.startsWith("$")) {
                String macVar = mac.substring(1);
                int addTo = 0;

                if (macVar.indexOf('+') != -1) {
                    try {
                        addTo =
                            Integer.parseInt(macVar.substring(macVar
                                .indexOf('+') + 1));
                    }
                    catch (NumberFormatException nfe) {
                        throw new IOException(
                            "Bad MAC address specification for label");
                    }

                    macVar = macVar.substring(0, macVar.indexOf('+'));
                }

                Variable var = variables.get(macVar);
                if (var == null) {
                    throw new IOException(
                        "Label references non-existant variable " + mac);
                }

                // Don't want to change the real one
                byte[] macAddrReal = var.getValue();
                byte[] macAddr = new byte[macAddrReal.length];
                System.arraycopy(macAddrReal, 0, macAddr, 0, macAddr.length);

                Utils.debug("Incrementing byte 5 of " +
                            Conversion.bytesToHex(macAddr) + " from " +
                            (macAddr[5] & 0xff) + " to " +
                            ((macAddr[5] & 0xff) + addTo));

                macAddr[5] = (byte)(((macAddr[5] & 0xff) + addTo) & 0xff);
                mac = Conversion.bytesToHex(macAddr);
            }

            // Grab the last 6 chars of value and stick :s in after every 2
            if (mac.length() < 6) {
                throw new IOException(
                    "Value for 'mac' must be at least 6 hex chars long");
            }

            mac = mac.substring(mac.length() - 6 - 1);
            mac =
                mac.substring(0, 2) + ":" + mac.substring(2, 4) + ":" +
                    mac.substring(4);

            P9500.printLabel(mac);
        }
        else if (labelType.equals("serial+mac") ||
                 labelType.equals("serial+mac barcode")) {
            // Grab the serial and the mac, format them, print them
            String serial = labelContents.get("serial");
            String mac = labelContents.get("mac");

            if (serial == null) {
                throw new IOException(labelType +
                                      " label type requires 'serial' value to be set");
            }
            if (mac == null) {
                throw new IOException(labelType +
                                      " label type requires 'mac' value to be set");
            }

            if (serial.startsWith("$")) {
                Variable var = variables.get(serial.substring(1));
                if (var == null) {
                    throw new IOException(
                        "Label references non-existant variable " + serial);
                }

                serial =
                    "" +
                        Integer.parseInt(Conversion.bytesToHex(var.getValue()),
                            16);
            }

            if (mac.startsWith("$")) {
                String macVar = mac.substring(1);
                int addTo = 0;

                if (macVar.indexOf('+') != -1) {
                    try {
                        addTo =
                            Integer.parseInt(macVar.substring(macVar
                                .indexOf('+') + 1));
                    }
                    catch (NumberFormatException nfe) {
                        throw new IOException(
                            "Bad MAC address specification for label");
                    }

                    macVar = macVar.substring(0, macVar.indexOf('+'));
                }

                Variable var = variables.get(macVar);
                if (var == null) {
                    throw new IOException(
                        "Label references non-existant variable " + mac);
                }

                // Don't want to change the real one
                byte[] macAddrReal = var.getValue();
                byte[] macAddr = new byte[macAddrReal.length];
                System.arraycopy(macAddrReal, 0, macAddr, 0, macAddr.length);

                Utils.debug("Incrementing byte 5 of " +
                            Conversion.bytesToHex(macAddr) + " from " +
                            (macAddr[5] & 0xff) + " to " +
                            ((macAddr[5] & 0xff) + addTo));

                macAddr[5] = (byte)(((macAddr[5] & 0xff) + addTo) & 0xff);
                mac = Conversion.bytesToHex(macAddr);
            }

            if (mac.length() != 12) {
                throw new IOException(
                    "Value for 'mac' must be exactly 12 hex chars long");
            }

            mac =
                mac.substring(0, 2) + ":" + mac.substring(2, 4) + ":" +
                    mac.substring(4, 6) + ":" + mac.substring(6, 8) + ":" +
                    mac.substring(8, 10) + ":" + mac.substring(10);

            if (labelType.equals("serial+mac")) {
                P9500.printLabel(serial, mac);
            }
            else {
                P9500.printBarcode(serial, mac);
            }
        }
        else if (labelType.equals("forcecom")) {
            // Grab the serial and the mac, format them, print them
            String partnum = labelContents.get("partnum");
            String mac = labelContents.get("mac");

            if (partnum == null) {
                throw new IOException(labelType +
                                      " label type requires 'partnum' value to be set");
            }
            if (mac == null) {
                throw new IOException(labelType +
                                      " label type requires 'mac' value to be set");
            }

            if (mac.startsWith("$")) {
                String macVar = mac.substring(1);
                int addTo = 0;

                if (macVar.indexOf('+') != -1) {
                    try {
                        addTo =
                            Integer.parseInt(macVar.substring(macVar
                                .indexOf('+') + 1));
                    }
                    catch (NumberFormatException nfe) {
                        throw new IOException(
                            "Bad MAC address specification for label");
                    }

                    macVar = macVar.substring(0, macVar.indexOf('+'));
                }

                Variable var = variables.get(macVar);
                if (var == null) {
                    throw new IOException(
                        "Label references non-existant variable " + mac);
                }

                // Don't want to change the real one
                byte[] macAddrReal = var.getValue();
                byte[] macAddr = new byte[macAddrReal.length];
                System.arraycopy(macAddrReal, 0, macAddr, 0, macAddr.length);

                Utils.debug("Incrementing byte 5 of " +
                            Conversion.bytesToHex(macAddr) + " from " +
                            (macAddr[5] & 0xff) + " to " +
                            ((macAddr[5] & 0xff) + addTo));

                macAddr[5] = (byte)(((macAddr[5] & 0xff) + addTo) & 0xff);
                mac = Conversion.bytesToHex(macAddr);
            }

            if (mac.length() != 12) {
                throw new IOException(
                    "Value for 'mac' must be exactly 12 hex chars long");
            }

            mac =
                mac.substring(0, 2) + ":" + mac.substring(2, 4) + ":" +
                    mac.substring(4, 6) + ":" + mac.substring(6, 8) + ":" +
                    mac.substring(8, 10) + ":" + mac.substring(10);

            P9500.printLabel(partnum, mac);
        }
        else if (labelType.equals("text") || labelType.equals("barcode")) {

            String text = labelContents.get("text");
            String cut = labelContents.get("cut");

            if (text == null) {
                throw new IOException(labelType +
                                      " label type requires 'text' value to be set");
            }
            if (cut == null) {
                throw new IOException(labelType +
                                      " label type requires 'cut' value to be set");
            }

            boolean bCut;

            bCut = Boolean.parseBoolean(cut);

            if (labelType.equals("text")) {
                P9500.printText(text, bCut);
            }
            else {
                P9500.printBarcode(text, bCut);
            }
        }
        else {
            throw new IOException("Unrecognized label type: " + labelType);
        }
    }

    // If this is null, there is no test class, and therefore no tests to be run
    public String getTestClass()
    {
        return testClass;
    }

    // If this is null, use the file name (minus extension)
    public String getDevName()
    {
        return devName;
    }

    public String getCSV()
    {
        return csvFile;
    }

    // If there is no CSV file specified, does nothing
    public void updateCSV()
        throws IOException
    {
        if (csvFile == null) {
            return;
        }

        // Compute the line we will be writing
        int counter = 0;
        String[] values = new String[csvValues.size()];
        for (String val : csvValues) {
            if (val == null) {
                throw new IOException("val" + counter +
                                      " was not assigned a value");
            }

            if (val.startsWith("$")) {
                val =
                    "" +
                        Integer.parseInt(Conversion.bytesToHex(variables.get(
                            val.substring(1)).getValue()), 16);
            }

            // Check for double-quotes and escape them with double-quotes
            val.replaceAll("\"", "\"\"");

            values[counter++] = val;
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            builder.append("\"" + values[i] + "\"");

            if (i < values.length - 1) {
                builder.append(",");
            }
        }
        String line = builder.toString();

        File lockFile = new File(csvFile + ".lck");
        Utils.acquireLock(lockFile, 15);

        try {
            PrintStream out =
                new PrintStream(new FileOutputStream(csvFile, true));
            out.println(line);
            out.close();
        }
        finally {
            lockFile.delete();
        }
    }

    public String getCopySource()
    {
        return copySrc;
    }

    public String getCopyTarget()
    {
        return copyDest;
    }

    public String getCopyAddress()
    {
        return copyAddr;
    }

    public String getLocalRoot()
    {
        return localRoot;
    }

    public File getLogoImage()
    {
        return logoImage;
    }

    // *** Private Static functions ***
    private static String getFileDestination(File datFile)
        throws IOException
    {
        BufferedReader in = openFile(datFile);
        try {
            String lineIn;
            while ((lineIn = getNextLine(in, null)) != null) {
                if (lineIn.startsWith("filename=")) {
                    return lineIn.substring("filename=".length());
                }
                else if (lineIn.startsWith("flashAddr=")) {
                    return lineIn.substring("flashAddr=".length());
                }
                else if (lineIn.startsWith("ubootVar=")) {
                    return lineIn.substring("ubootVar=".length());
                }
            }

            throw new IOException("Bad file format (" + datFile.getName() +
                                  "): You must provide one of filename/flashAddr/ubootVar");
        }
        finally {
            in.close();
        }
    }

    private static int getFileSize(File datFile)
        throws IOException
    {
        BufferedReader in = openFile(datFile);
        try {
            String lineIn;
            while ((lineIn = getNextLine(in, null)) != null) {
                if (lineIn.startsWith("size=")) {
                    return Integer.parseInt(lineIn.substring("size=".length()),
                        16);
                }
            }

            throw new IOException("Bad file format (" + datFile.getName() +
                                  "): size is a required setting");
        }
        finally {
            in.close();
        }
    }

    private static BufferedReader openFile(File file)
        throws IOException
    {
        return new BufferedReader(new FileReader(file));
    }

    private static String getNextLine(BufferedReader in, int[] lineNum)
        throws IOException
    {
        String lineIn;

        if (lineNum == null) {
            lineNum = new int[1];
        }

        while ((lineIn = in.readLine()) != null) {
            lineNum[0]++;

            // Remove comments, but allow escaping
            int lastHash = 0;
            int pos;
            while ((pos = lineIn.indexOf('#', lastHash)) != -1) {
                if (pos == 0 || lineIn.charAt(pos - 1) != '\\') {
                    // Not escaped
                    lineIn = lineIn.substring(0, pos);
                }
                else {
                    // Escaped
                    // Remove the \, leave the #
                    lineIn =
                        lineIn.substring(0, pos - 1) + lineIn.substring(pos);

                    // Update lastHash; the # is now at pos - 1, so pos is the
                    // next char
                    lastHash = pos;
                }
            }

            // Remove leading/training whitespace
            lineIn = lineIn.trim();

            // If there is nothing left, don't bother parsing the line
            if (lineIn.length() == 0) {
                continue;
            }

            return lineIn;
        }

        return null;
    }

    private static MemoryRange parseMemoryRange(String range)
        throws IOException
    {
        if (range.indexOf('+') != -1) {
            String offset = range.substring(0, range.indexOf('+'));
            String length = range.substring(range.indexOf('+') + 1);

            return new MemoryRange(Utils.parseLong(offset), Utils
                .parseInt(length));
        }
        else if (range.indexOf('-') != -1) {
            String start = range.substring(0, range.indexOf('-'));
            String end = range.substring(range.indexOf('-') + 1);

            long offset = Utils.parseLong(start);
            int length = (int)(Utils.parseLong(end) - offset);

            return new MemoryRange(offset, length);
        }
        else {
            throw new IOException(String.format(
                "Bad memory range format: '%s'", range));
        }
    }
}

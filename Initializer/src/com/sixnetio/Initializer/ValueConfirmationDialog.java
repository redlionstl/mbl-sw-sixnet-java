/*
 * ValueConfirmationDialog.java
 *
 * Assists in confirming values existing in a station and getting new ones, if necessary.
 *
 * Jonathan Pearson
 * January 18, 2008
 *
 */

package com.sixnetio.Initializer;

import com.sixnetio.util.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

public class ValueConfirmationDialog extends JDialog implements ActionListener {
	public static class AcquireNew {
		public boolean allowAcquireNew() {
			return false;
		}
		
		public boolean requireNew(byte[] value) {
			return false;
		}
		
		public void getNew(byte[][] out_vals) throws IOException {
			throw new IllegalStateException("Cannot call getNew() on an instance of AcquireNew that does not support it");
		}
		
		public String describe(byte[] value, int index) {
			if (value.length == 1) {
				return ("" + (value[0] & 0xff));
			} else if (value.length == 2) {
				return ("" + (Conversion.bytesToShort(value, 0) & 0xffff));
			} else if (value.length == 4) {
				return ("" + Conversion.bytesToInt(value, 0));
			} else if (value.length == 8) {
				return ("" + Conversion.bytesToLong(value, 0));
			} else {
				// Not a normal length, use hex
				return ("0x" + Conversion.bytesToHex(value));
			}
		}
		
		public byte[] parse(String text, int length, int index) {
			if (text.startsWith("0x")) {
				return Conversion.hexToBytes(text.substring(2));
			} else {
				byte[] ans = new byte[length];
				
				if (length == 1) {
					ans[0] = Byte.parseByte(text);
				} else if (length == 2) {
					Conversion.shortToBytes(ans, 0, Short.parseShort(text));
				} else if (length == 4) {
					Conversion.intToBytes(ans, 0, Integer.parseInt(text));
				} else if (length == 8) {
					Conversion.longToBytes(ans, 0, Long.parseLong(text));
				} else {
					throw new IllegalArgumentException("Cannot parse a non-hex " + length + "-byte number");
				}
				
				return ans;
			}
		}
	}
	
	// Allows easy alphabetical sorting by description
	private static class Input implements Comparable<Input> {
		public DaxFile.Variable var;
		public JTextField field;
		
		public Input(DaxFile.Variable var, JTextField field) {
			this.var = var;
			this.field = field;
		}
		
		public int compareTo(Input o) {
			return var.getDescription().compareTo(o.var.getDescription());
		}
	}
	
	private Vector<Input> inputs;
	
	private JButton btnOkay,
	                btnAcquireNew,
	                btnCancel;
	AcquireNew acquireNew;
	private boolean success;
	
	public ValueConfirmationDialog(Vector<DaxFile.Variable> vars, String title, AcquireNew acq) {
		super((Dialog)null, title, true);
		
		if (acq != null) {
			acquireNew = acq;
		} else {
			acquireNew = new AcquireNew();
		}
		
		inputs = new Vector<Input>();
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				btnCancel.doClick();
			}
		});
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		setLayout(new BorderLayout());
		
		JPanel center = new JPanel();
		center.setLayout(new GridLayout(0, 1));
		add(center, BorderLayout.CENTER);
		
		// For the text boxes, so pressing 'Enter' will submit
		ActionListener al = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOkay.doClick();
			}
		};
		
		Input[] tempInputs = new Input[vars.size()]; // So we can sort it
		for (int i = 0; i < vars.size(); i++) {
			DaxFile.Variable var = vars.get(i);
			
			JPanel row = new JPanel();
			row.setLayout(new FlowLayout(FlowLayout.LEFT));
			center.add(row);
			
			JLabel lbl = new JLabel(var.getDescription());
			
			String val = acq.describe(var.getValue(), i);
			
			JTextField txt = new JTextField(val, 12);
			txt.addActionListener(al);
			
			JLabel lbl2 = new JLabel("(" + var.getLength() + " bytes)");
			
			row.add(lbl);
			row.add(txt);
			row.add(lbl2);
			
			tempInputs[i] = new Input(var, txt);
		}
		
		Arrays.sort(tempInputs);
		for (int i = 0; i < tempInputs.length; i++) {
			inputs.add(tempInputs[i]);
		}
		
		// Buttons
		{
			JPanel row = new JPanel();
			row.setLayout(new FlowLayout());
			add(row, BorderLayout.SOUTH);
			
			btnOkay = new JButton("Okay");
			btnOkay.addActionListener(this);
			row.add(btnOkay);
			
			if (acq.allowAcquireNew()) { // If it supports it, add the button
				btnAcquireNew = new JButton("Acquire New");
				btnAcquireNew.addActionListener(this);
				row.add(btnAcquireNew);
			}
			
			btnCancel = new JButton("Cancel");
			btnCancel.addActionListener(this);
			row.add(btnCancel);
		}
		
		pack();
	}
	
	public boolean showDialog() {
		setVisible(true);
		
		return success;
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnOkay) {
			// Verify values being set
			// Special case: Holding Ctrl+Alt+Shift when clicking will skip this
			if ((e.getModifiers() & (ActionEvent.CTRL_MASK | ActionEvent.ALT_MASK | ActionEvent.SHIFT_MASK)) != (ActionEvent.CTRL_MASK | ActionEvent.ALT_MASK | ActionEvent.SHIFT_MASK)) {
				for (int i = 0; i < inputs.size(); i++) {
					Input inp = inputs.get(i);
					DaxFile.Variable var = inp.var;
					JTextField field = inp.field;
					
					byte[] val;
					
					try {
						val = acquireNew.parse(field.getText(), var.getLength(), i);
					} catch (NumberFormatException nfe) {
						Utils.debug(nfe);
						JOptionPane.showMessageDialog(this, var.getDescription() + ": Not a legal value: " + nfe.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
						field.requestFocusInWindow();
						return;
					}
					
					if (acquireNew.requireNew(val)) {
						JOptionPane.showMessageDialog(this, var.getDescription() + ": Not a legal value", "Error", JOptionPane.WARNING_MESSAGE);
						field.requestFocusInWindow();
						return;
					}
				}
			}
			
			// Parse values
			for (int i = 0; i < inputs.size(); i++) {
				Input inp = inputs.get(i);
				DaxFile.Variable var = inp.var;
				JTextField field = inp.field;
				
				byte[] val;
				
				// Already tested for a NumberFormatException here
				val = acquireNew.parse(field.getText(), var.getLength(), i);
				var.setValue(val);
			}
			
			success = true;
			setVisible(false);
			dispose();
		} else if (e.getSource() == btnCancel) {
			success = false;
			setVisible(false);
			dispose();
		} else if (e.getSource() == btnAcquireNew) {
			byte[][] vals = new byte[inputs.size()][];
			for (int i = 0; i < inputs.size(); i++) {
				vals[i] = new byte[inputs.get(i).var.getLength()];
			}
			
			try {
				acquireNew.getNew(vals);
			} catch (IOException ioe) {
				Utils.debug(ioe);
				JOptionPane.showMessageDialog(this, "Unable to acquire new values: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			for (int i = 0; i < inputs.size(); i++) {
				inputs.get(i).var.setValue(vals[i]);
				inputs.get(i).field.setText(acquireNew.describe(vals[i], i));
			}
		} else {
			throw new IllegalArgumentException("Unexpected source of action event: " + e.getSource());
		}
	}
}

/*
 * Tester.java
 *
 * Any class that is to be dynamically loaded for automatic testing must implement
 * this interface.
 *
 * Jonathan Pearson
 * January 29, 2008
 *
 */

package com.sixnetio.Initializer;

import com.sixnetio.util.*;

import java.io.*;
import java.util.*;

/**
 * This interface must be implemented by any automatic testing class. Any
 * implementing class MUST have a public no-argument constructor.
 */
public interface Tester extends Runnable {
	/**
	 * Set up a new test to be run. Be ready for this to be called multiple
	 *   with different parameters.
	 * @param ui The class that will handle all user interactions needed by this
	 *   testing class.
	 * @param comPort The COM port used to connect to the device when it was
	 *   being initialized.
	 * @param devName The name of the product.
	 * @throws IOException If there is a problem establishing a connection, or a
	 *   similar error occurs, or if the device name is not recognized.
	 * @throws IllegalArgumentException If the ui is null.
	 */
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException;
	
	/**
	 * Does this class support test jumping? That is, can a specific test be
	 *   run while skipping all others?
	 * @return True = this class supports test jumping; false = this class does
	 *   not support test jumping. If this class returns true here, it is
	 *   obligated to return meaningful values from the test jumping functions.
	 */
	public boolean supportsTestJump();

	public boolean runVerifyTests(boolean verifyTest);
	
	/**
	 * Returns a list of meaningful test names. These will be presented to the
	 *   user if he wants to perform a test jump. May return null if
	 *   {@link #supportsTestJump()} returns false.
	 * @return A list of meaningful test names. One of these may be passed into
	 *   {@link #jumpToTest(String)}.
	 */
	public List<String> getTests();
	
	/**
	 * Perform the necessary setup to jump to a specific test. The {@link #run()}
	 *   method will be called next to execute the chosen test.
	 * @param testName The name of the test (from the list returned by
	 *   {@link #getTests()} to execute when run() is called. Pass <tt>null</tt>
	 *   to de-select and run all tests.
	 * @throws IllegalArgumentException If the given test name was not in the
	 *   list of available tests and was not <tt>null</tt>. This may be a
	 *   case-sensitive test.
	 * @throws IllegalStateException If test jumping is not supported and
	 *   testName is not <tt>null</tt>.
	 */
	public void jumpToTest(String testName);
	
	/**
	 * Returns the test that will be jumped to, or null if none is selected.
	 * @return The test to jump to.
	 */
	public String getJumpTest();
	
	/**
	* Execute all tests or the chosen test, depending on whether
	*   {@link #jumpToTest(String)} has been called. All user interaction is performed
	*   through the ui object passed to
	*   {@link #setupTest(UserInterface, String, String)}, including exception
	*   handling. This will likely be called by a new thread.
	*/
	public void run();

}

/*
 * InitializerGUI.java
 *
 * A GUI for the product Initializer.
 *
 * Jonathan Pearson
 * January 8, 2008
 *
 */

package com.sixnetio.Initializer;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.concurrent.TimeoutException;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jonp.xml.Tag;
import org.jonp.xml.XML;

import com.sixnetio.GUI.FourButtonDialog;
import com.sixnetio.GUI.ListChooserDialog;
import com.sixnetio.GUI.Suicide;
import com.sixnetio.sixnet.MacAddress;
import com.sixnetio.sixnet.MacAddressFactory;
import com.sixnetio.sixnet.MacAddressRanges;
import com.sixnetio.sixnet.MacDB;
import com.sixnetio.sixnet.SerialNumber;
import com.sixnetio.sixnet.SerialNumberFactory;
import com.sixnetio.sixnet.SerialNumberRanges;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Ref;
import com.sixnetio.util.UserInterface;
import com.sixnetio.util.Utils;

// TODO: Add a 'retry file copy' button or pseudo test case

public class InitializerGUI
    extends JFrame
    implements ActionListener, ItemListener, UserInterface
{
    private static final String PROG_VERSION = "1.9.5";

    private static final String DATA_PATH = "DAX" + File.separator;
    private static final String SETTINGS_FILE =
        System.getProperty("user.home") + File.separator + ".baseinitrc";

    private static final Color C_PASS = new Color(0, 128, 0);
    private static final Color C_FAIL = new Color(128, 0, 0);

    static final Color C_GREEN = new Color(0, 128, 0);

    static final Color C_YELLOW = new Color(128, 128, 0);

    static final Color C_RED = new Color(128, 0, 0);

    private static class LogoCanvas
        extends Canvas
    {
        private Image logoImage;

        public LogoCanvas()
        {
            // Nothing to do
        }

        public void setLogo(File logo)
        {
            if (logo == null) {
                this.logoImage = null;
            }
            else {
                this.logoImage =
                    getToolkit().createImage(logo.getAbsolutePath());
            }
        }

        @Override
        public void paint(Graphics g)
        {
            if (logoImage == null) {
                g.clearRect(0, 0, getWidth(), getHeight());
            }
            else {
                g.drawImage(logoImage, 0, 0, this);
            }
        }
    }


    // main()
    public static void main(String[] args)
    {
        // javax.swing.plaf.metal.MetalLookAndFeel.setCurrentTheme(new
        // javax.swing.plaf.metal.DefaultMetalTheme());
        Logger.getRootLogger().setLevel(Level.OFF);
        PropertyConfigurator.configureAndWatch("log4j.properties", 5000);

        new InitializerGUI();
    }

    // Private data members
    private JLabel lblPass;
    private LogoCanvas cnvLogo;
    private JTextField txtComPort;
    private JComboBox cmbProduct;
    private JPanel productChoice; // Contains cmbProduct and all variables that
    // need to be set (minus a few automatics)
    private JButton btnLoad, btnTest, btnRunAllTests;

    JButton btnQuit;
    JList lstMessages;

    DaxFile dax;
    private Tester tester;
    Initializer initializer;
    private Map<String, JTextField> varValues;
    private int state;

    private long operationStartTime; // So we can print how long the operation
    // took at the end

    // Used to implement the
    // UserInterface.getPersistentValue()/setPersistentValue() functions
    private Map<String, String> persistentValues =
        new Hashtable<String, String>();

    Thread runningThread = null; // So hitting the 'X' button can

    // interrupt a thread

    // Constructor
    public InitializerGUI()
    {
        super("Product Initializer v" + PROG_VERSION +
              (Utils.DEBUG ? " (DEBUG)" : ""));

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            // Force quit if the user clicks the X button twice inside of 3
            // seconds
            long lastClick = 0;

            @Override
            public void windowClosing(WindowEvent e)
            {
                if (runningThread != null) {
                    runningThread.interrupt();
                }
                else {
                    btnQuit.doClick();
                }

                if (lastClick + 3000 > System.currentTimeMillis()) {
                    setVisible(false);
                    saveSettings();
                    dispose();
                    new Suicide();
                }

                lastClick = System.currentTimeMillis();
            }
        });

        setLayout(new BorderLayout());

        // Top: User-provided information and buttons
        {
            JPanel top = new JPanel();
            top.setLayout(new BorderLayout());

            add(top, BorderLayout.NORTH);

            // A useful little listener
            FocusListener f = new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e)
                {
                    JTextField field = (JTextField)e.getSource();
                    field.selectAll();
                }
            };

            // Top-Top: Com port, product choice, and Pass/Fail label
            {
                JPanel tt = new JPanel();
                tt.setLayout(new BorderLayout());

                top.add(tt, BorderLayout.NORTH);

                // Left: Logo image
                {
                    JPanel ttl = new JPanel();
                    ttl.setLayout(new FlowLayout());

                    tt.add(ttl, BorderLayout.WEST);

                    cnvLogo = new LogoCanvas();
                    cnvLogo.setPreferredSize(new Dimension(150, 61));

                    ttl.add(cnvLogo);
                }

                // Center: Com port and product choice
                {
                    JPanel ttc = new JPanel();
                    ttc.setLayout(new GridLayout(0, 1));

                    tt.add(ttc, BorderLayout.CENTER);

                    // Serial port
                    {
                        JPanel row = new JPanel();
                        row.setLayout(new FlowLayout());
                        ttc.add(row);

                        JLabel l = new JLabel("Serial Port: ");
                        row.add(l);

                        String comPort;
                        if (System.getProperty("os.name").toLowerCase()
                            .indexOf("windows") != -1) {
                            comPort = "COM1";
                        }
                        else {
                            comPort = "/dev/ttyS0";
                        }
                        txtComPort = new JTextField(comPort, 10);
                        txtComPort.addFocusListener(f);
                        row.add(txtComPort);
                    }

                    // Product choice
                    {
                        JPanel row = new JPanel();
                        row.setLayout(new FlowLayout());

                        ttc.add(row);

                        JLabel l = new JLabel("Product: ");
                        row.add(l);

                        cmbProduct = new JComboBox();
                        cmbProduct.setEditable(false);
                        cmbProduct.addItemListener(this);
                        row.add(cmbProduct);
                    }
                }

                // Right: Pass/Fail indicator
                {
                    JPanel ttr = new JPanel();
                    ttr.setLayout(new FlowLayout());

                    tt.add(ttr, BorderLayout.EAST);

                    lblPass = new JLabel("PASS"); // Make sure it's large enough
                    lblPass.setForeground(lblPass.getBackground()); // So you
                    // can't see
                    // it until
                    // a job is
                    // done
                    lblPass.setFont(new Font(lblPass.getFont().getFontName(),
                        lblPass.getFont().getStyle(), lblPass.getFont()
                            .getSize() + 12));

                    ttr.add(lblPass);
                }
            }

            // Top-Center: User-provided information
            {
                productChoice = new JPanel();
                productChoice.setBorder(BorderFactory
                    .createTitledBorder("Device Settings"));
                productChoice.setLayout(new GridLayout(0, 1));

                top.add(productChoice, BorderLayout.CENTER);
            }

            // Top-Right: Buttons
            {
                JPanel right = new JPanel();
                right.setLayout(new GridLayout(0, 1));

                top.add(right, BorderLayout.EAST);

                // Load button
                {
                    JPanel row = new JPanel();
                    row.setLayout(new FlowLayout());

                    right.add(row);

                    btnLoad = new JButton("Load & Test");
                    btnLoad.addActionListener(this);
                    row.add(btnLoad);
                }

                // Test button
                {
                    JPanel row = new JPanel();
                    row.setLayout(new FlowLayout());

                    right.add(row);

                    btnTest = new JButton("Run Individual Tests");
                    btnTest.addActionListener(this);
                    btnTest.setEnabled(false); // Only enabled if there is a
                    // tester and it supports test
                    // jumping
                    row.add(btnTest);
                }
                // Run All Tests button
                {
                    JPanel row = new JPanel();
                    row.setLayout(new FlowLayout());

                    right.add(row);

                    btnRunAllTests = new JButton("Run All Tests");
                    btnRunAllTests.addActionListener(this);
                    btnRunAllTests.setEnabled(false); // Only enabled if there
                                                      // is a tester
                    row.add(btnRunAllTests);
                }
                // Quit Button
                {
                    JPanel row = new JPanel();
                    row.setLayout(new FlowLayout());

                    right.add(row);

                    btnQuit = new JButton("Quit");
                    btnQuit.addActionListener(this);
                    row.add(btnQuit);
                }
            }
        }

        // Center: List box of messages
        {
            JPanel center = new JPanel();
            center.setBorder(BorderFactory.createTitledBorder("Messages"));
            center.setLayout(new BorderLayout());

            add(center, BorderLayout.CENTER);

            lstMessages = new JList(new DefaultListModel());
            lstMessages.setCellRenderer(new MyCellRenderer());

            JScrollPane scroller = new JScrollPane(lstMessages);
            scroller.setPreferredSize(new Dimension(640, 480));
            center.add(scroller, BorderLayout.CENTER);
        }

        populateProducts();
        loadSettings();

        pack();
        setVisible(true);
    }

    // actionPerformed()
    public void actionPerformed(ActionEvent e)
    {
        operationStartTime = System.nanoTime(); // Easiest place to put it

        if (e.getSource() == btnLoad) {
            // {{{
            setLabel("...", Color.BLACK);

            initializer = null;
            if (tester != null) {
                tester.jumpToTest(null);
            }
            state = 0;

            DefaultListModel model = (DefaultListModel)lstMessages.getModel();
            model.clear();

            enableControls(false);

            // Load the device
            try {
                // Special variables that the user isn't allowed to set (yet,
                // anyway; we need to look inside the base first)
                if (!parseVariables()) {
                    // On failure, return so we don't do something stupid
                    setLabel("FAIL", C_FAIL);
                    enableControls(true);
                    return;
                }

                // Set the MacDB path before we go any further
                {
                    DaxFile.Variable macdb = dax.getVariable("macdb");
                    String path;
                    if (macdb == null) {
                        path = null;
                    }
                    else {
                        path = macdb.getStringValue();
                    }

                    MacDB.setDatabasePath(path);
                }

                initializer = new Initializer(this, txtComPort.getText(), dax);

                // Now that we have an initializer, we can get values for any
                // remaining variables
                if (!handleSpecialVars()) {
                    // Failed? Return before we do something stupid
                    setLabel("FAIL", C_FAIL);
                    enableControls(true);

                    initializer.closeUBoot();
                    initializer.closeUDR();

                    return;
                }

                if (dax.hasLabel()) {
                    FourButtonDialog dlg =
                        new FourButtonDialog("Print Label",
                            "Would you like to print a label for this unit?",
                            "Print & Initialize", "Initialize Only",
                            "Print & Cancel", "Cancel");
                    int response = dlg.showDialog();

                    // A quick little thread to print labels (so the program
                    // won't [appear to] hang if the printer malfunctions)
                    Thread printer = new Thread(new Runnable() {
                        public void run()
                        {
                            try {
                                dax.printLabel();
                            }
                            catch (IOException ioe) {
                                JOptionPane
                                    .showMessageDialog(InitializerGUI.this,
                                        "Error printing label: " +
                                            ioe.getMessage(), "Printing Error",
                                        JOptionPane.WARNING_MESSAGE);
                            }
                        }
                    });

                    if (response == FourButtonDialog.OPTION1) { // Print &
                        // Initialize
                        printer.start();
                    }
                    else if (response == FourButtonDialog.OPTION2) { // Initialize
                        // Only
                        // Do nothing, initialization will follow
                    }
                    else if (response == FourButtonDialog.OPTION3) { // Print &
                        // Cancel
                        printer.start();

                        setLabel("DONE", Color.BLACK);
                        enableControls(true);
                        return;
                    }
                    else if (response == FourButtonDialog.CANCELED) { // Cancel
                        setLabel("FAIL", C_FAIL);
                        enableControls(true);
                        return;
                    }
                }
                tester.runVerifyTests(false);
                runningThread = new Thread(initializer);
                runningThread.start();
            }
            catch (Exception e2) {
                Utils.debug(e2);

                if (initializer != null) {
                    initializer.closeUBoot();
                    initializer.closeUDR();
                }

                displayMessage("R " + e2.getMessage());

                setLabel("FAIL", C_FAIL);
                enableControls(true);
            }
        }
        else if (e.getSource() == btnTest) {
            state = -1; // Don't go doing something after this

            setLabel("...", Color.BLACK);
            enableControls(false);

            DefaultListModel model = (DefaultListModel)lstMessages.getModel();
            model.clear();

            java.util.List<String> testNames = tester.getTests();

            // Ask the user to choose one
            ListChooserDialog lcd =
                new ListChooserDialog(testNames, "Choose a Test");
            if (lcd.showDialog() == ListChooserDialog.OKAY_OPTION) {
                String result = lcd.getChoice().toString();
                try {
                    String devName = dax.getDevName();
                    if (devName == null) {
                        // No alternate device name was provided
                        devName = cmbProduct.getSelectedItem().toString();
                    }

                    tester.setupTest(this, txtComPort.getText(), devName);
                }
                catch (IOException ioe) {
                    Utils.debug(ioe);

                    displayMessage("R Unable to initialize the tester: " +
                                   ioe.getMessage());

                    setLabel("FAIL", C_FAIL);
                    enableControls(true);
                    return;
                }
                tester.runVerifyTests(false);
                tester.jumpToTest(result);
                runningThread = new Thread(tester);
                runningThread.start();
            }
            else {
                setLabel("", Color.BLACK);
                enableControls(true);
            }
        }
        else if (e.getSource() == btnRunAllTests) {
            state = -1; // Don't go doing something after this

            setLabel("...", Color.BLACK);
            enableControls(false);

            DefaultListModel model = (DefaultListModel)lstMessages.getModel();
            model.clear();

            try {
                String devName = dax.getDevName();
                if (devName == null) {
                    // No alternate device name was provided
                    devName = cmbProduct.getSelectedItem().toString();
                }

                tester.setupTest(this, txtComPort.getText(), devName);
            }
            catch (IOException ioe) {
                Utils.debug(ioe);

                displayMessage("R Unable to initialize the tester: " +
                               ioe.getMessage());

                setLabel("FAIL", C_FAIL);
                enableControls(true);
                return;
            }
            tester.jumpToTest(null);
            tester.runVerifyTests(true);
            runningThread = new Thread(tester);
            runningThread.start();
        }
        else if (e.getSource() == btnQuit) {
            setVisible(false);
            saveSettings();
            dispose();
            new Suicide();
        }
        else {
            throw new IllegalArgumentException(
                "Unexpected source of action event: " + e.getSource());
        }
    }

    // itemStateChanged()
    public void itemStateChanged(ItemEvent e)
    {
        if (e.getSource() == cmbProduct) {
            loadDax(DATA_PATH + cmbProduct.getSelectedItem().toString() +
                    ".dax");

            pack();
        }
        else {
            throw new IllegalArgumentException(
                "Unexpected source of item event: " + e.getSource());
        }
    }

    // loadDax()
    private void loadDax(String daxFile)
    {
        // Clear out the panel and re-populate
        productChoice.removeAll();

        try {
            dax = new DaxFile(daxFile);
            btnLoad.setEnabled(true);
        }
        catch (IOException ioe) {
            dax = null;
            btnLoad.setEnabled(false);

            JOptionPane.showMessageDialog(this, "Unable to process file " +
                                                daxFile + ": " +
                                                ioe.getMessage());
            return;
        }

        // Handle variables from the DAX file
        List<String> vars = new ArrayList<String>(dax.getVariableNames());

        // Sort by description
        Collections.sort(vars, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2)
            {
                String d1 = dax.getVariable(o1).getDescription();
                String d2 = dax.getVariable(o2).getDescription();

                return d1.compareTo(d2);
            }
        });

        varValues = new LinkedHashMap<String, JTextField>();

        // Keep fields without recommended values around and add them at the end
        // This keeps the user from needing to search through a list of fields
        // that may or may not need data
        Vector<JPanel> unknownValueFields = new Vector<JPanel>();

        for (String varname : vars) {
            DaxFile.Variable variable = dax.getVariable(varname);

            // Create a label and text field for the variable
            JLabel lbl = new JLabel(variable.getDescription());
            JTextField field = new JTextField(20);

            if (varname.startsWith("macaddr")) {
                // Don't display these, but mark them as special
                varname = "!" + varname;
            }
            else if (varname.equals("serial")) {
                // Same with this one
                varname = "!" + varname;
            }
            else if (varname.equals("highserial")) {
                // And this one...
                varname = "!" + varname;
            }
            else if (varname.equals("date")) {
                // Even more special -- the user is presented with 'year' and
                // 'week' boxes, but if there is a legal
                // station value for 'date', which covers the same two bytes,
                // then we need to use that instead
                varname = "!" + varname;
            }
            else {
                JPanel row = new JPanel();
                row.setLayout(new FlowLayout(FlowLayout.LEFT));

                row.add(lbl);

                field.setText(getRecommendedVal(variable));
                row.add(field);

                JLabel len;
                if (variable.getLength() == -1) {
                    len = new JLabel("(Text)");
                }
                else {
                    len = new JLabel("(" + variable.getLength() + " bytes)");
                }
                row.add(len);

                if (field.getText().length() > 0) {
                    productChoice.add(row);
                }
                else {
                    unknownValueFields.add(row);
                }
            }

            // Add to the table
            varValues.put(varname, field);
        }

        for (JPanel row : unknownValueFields) {
            productChoice.add(row);
        }

        // Handle the possibility that there is a tester specified
        tester = null;
        btnTest.setEnabled(false);
        btnRunAllTests.setEnabled(false);
        String testClass = dax.getTestClass();

        if (testClass != null) {
            try {
                tester = loadTester(testClass);
            }
            catch (Exception e) {
                Utils.debug(e);
                displayMessage("R Unable to load the tester: " + e.getMessage());
            }

            if (tester != null && tester.supportsTestJump()) {
                btnTest.setEnabled(true);
                btnRunAllTests.setEnabled(true);
            }
        }
        else {
            displayMessage("R Error reading test class from DAX file!");
        }

        // Display the logo, if there is one
        if (dax.getLogoImage() != null) {
            cnvLogo.setLogo(dax.getLogoImage());
        }
        else {
            cnvLogo.setLogo(null);
        }

        cnvLogo.repaint();
    }

    private void displayMessage(final String msg)
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                DefaultListModel model =
                    (DefaultListModel)lstMessages.getModel();
                model.addElement(msg);
                lstMessages.ensureIndexIsVisible(model.getSize() - 1);
                repaint();
            }
        });
    }

    private void updateLastMessage(final String msg)
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                DefaultListModel model =
                    (DefaultListModel)lstMessages.getModel();
                model.remove(model.getSize() - 1);
                model.addElement(msg);
                lstMessages.ensureIndexIsVisible(model.getSize() - 1);
                repaint();
            }
        });
    }

    public void displayMessage(Object from, int importance, String msg)
    {
        switch (importance) {
            case UserInterface.M_NORMAL:
                msg = "G " + msg;
                break;
            case UserInterface.M_WARNING:
                msg = "Y " + msg;
                break;
            case UserInterface.M_ERROR:
                msg = "R " + msg;
                break;
        }

        displayMessage(msg);
    }

    public void updateLastMessage(Object from, int importance, String msg)
    {
        switch (importance) {
            case UserInterface.M_NORMAL:
                msg = "G " + msg;
                break;
            case UserInterface.M_WARNING:
                msg = "Y " + msg;
                break;
            case UserInterface.M_ERROR:
                msg = "R " + msg;
                break;
        }

        updateLastMessage(msg);
    }

    // handleException()
    public void handleException(Object from, Exception e)
    {
        displayMessage("R Error(s):");

        if (e instanceof InterruptedOperationException ||
            e instanceof InterruptedException) {

            displayMessage("R User canceled the test");
        }
        else {
            displayMessage("R " + e.getMessage());
        }

        Utils.debug(e);
    }

    // operationCompleted()
    public void operationCompleted(Object from)
    {
        if (doNextAction(from)) {
            if (tester == null) {
                setLabel("FAIL", C_FAIL);

                try {
                    dax.updateCSV();
                }
                catch (IOException ioe) {
                    displayMessage("Y Unable to update CSV file " +
                                   dax.getCSV() + ": " + ioe.getMessage());
                }
            }
            else if ((tester != null) && (tester.getJumpTest() == null)) {
                setLabel("PASS", C_PASS);
            }
            else if (tester.getJumpTest() != null) {
                setLabel("DONE", Color.BLACK);
            }

            long operationStopTime = System.nanoTime();
            long timeDiff = operationStopTime - operationStartTime;

            int hours = (int)(timeDiff / 60000000000L / 60000000000L);
            timeDiff -= (hours * 60000000000L * 60000000000L);

            int minutes = (int)(timeDiff / 60000000000L);
            timeDiff -= (minutes * 60000000000L);

            int seconds = (int)(timeDiff / 1000000000L);
            timeDiff -= (seconds * 1000000000L);

            int ms = (int)(timeDiff / 1000000L);

            displayMessage(String.format(
                "G Operation took %01d:%02d:%02d.%03d", hours, minutes,
                seconds, ms));

            runningThread = null; // Just in case doNextAction() forgot
            enableControls(true);
        }
    }

    // Return true when there were no actions to start
    // Return false when we start a new action, whether or not it succeeds
    // If starting the new action fails, set up the labels and whatnot before
    // returning
    private boolean doNextAction(Object justCompleted)
    {
        Thread copier = new Thread(new Runnable() {
            public void run()
            {
                initializer.doCopy();
            }
        });

        switch (state) {
            case 0: // Initializer just ran, start tester
                state = 0;

                if (tester != null) {
                    try {
                        state = 1;

                        String devName = dax.getDevName();
                        if (devName == null) {
                            // No alternate device name was provided
                            devName = cmbProduct.getSelectedItem().toString();
                        }

                        tester.setupTest(this, txtComPort.getText(), devName);
                        tester.jumpToTest(null);

                        runningThread = new Thread(tester);
                        runningThread.start();
                        return false; // Started a new job, don't change the
                        // label
                    }
                    catch (IOException ioe) {
                        Utils.debug(ioe);

                        state = -1;

                        displayMessage("R Unable to initialize the tester: " +
                                       ioe.getMessage());

                        setLabel("FAIL", C_FAIL);
                        enableControls(true);

                        runningThread = null;
                        return false; // We just changed the label, so don't do
                        // it elsewhere
                    }
                }

                // Otherwise, fall through
            case 1: // Tester just ran, start copier
                state = 1;

                if (dax.getCopySource() != null && dax.getCopyTarget() != null) {
                    if (!Utils.DEBUG || confirm("Load Files?", "Continue")) {
                        state = 2;

                        runningThread = copier;
                        copier.start();
                        return false;
                    }
                }

                // Otherwise, fall through
            case 2: // Copier just ran
                state = 2;

                // If any more tasks need to run, put them here
            default:
                runningThread = null;
                state = -1;
                return true; // Nothing left to do
        }
    }

    // operationFailed()
    public void operationFailed(Object from)
    {
        if (from instanceof Initializer) {
            ((Initializer)from).closeUBoot();
            ((Initializer)from).closeUDR();

            displayMessage("R Initialization failed");
        }
        else if (from instanceof Tester) {
            displayMessage("R Test failed");
        }

        runningThread = null;

        setLabel("FAIL", C_FAIL);
        enableControls(true);
    }

    // confirm()
    public boolean confirm(String msg, String title)
    {
        return (JOptionPane.showConfirmDialog(this, msg, title,
            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION);
    }

    public String requestString(String msg, String title, String defaultVal)
    {
        return (String)JOptionPane.showInputDialog(this, msg, title,
            JOptionPane.QUESTION_MESSAGE, null, null, defaultVal);
    }

    public String getPersistentValue(String name)
    {
        synchronized (persistentValues) {
            return persistentValues.get(name);
        }
    }

    public void setPersistentValue(String name, String value)
    {
        synchronized (persistentValues) {
            persistentValues.put(name, value);
        }
    }

    // loadTester()
    private Tester loadTester(String className)
        throws ClassNotFoundException, InstantiationException,
        IllegalAccessException
    {
        ClassLoader loader = getClass().getClassLoader();
        Class<?> clazz = loader.loadClass(className);
        return (Tester)clazz.newInstance();
    }

    // populateProducts()
    private void populateProducts()
    {
        // List the contents of Data_Path
        // Add any DAX files we find to cmbProduct
        // Note: JOptionPane has 'null' for parent because the frame is still
        // being constructed
        File dir = new File(DATA_PATH);
        if (!dir.isDirectory()) {
            JOptionPane
                .showMessageDialog(
                    null,
                    "Unable to locate list of products, please make sure the .dax files are in " +
                        dir.getAbsolutePath(), "Error",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);
            return;
        }

        File[] candidates = dir.listFiles(new FileFilter() {
            public boolean accept(File file)
            {
                // Only looking for files
                if (!file.isFile()) {
                    return false;
                }

                // Check the extension
                if (file.getName().indexOf('.') == -1) {
                    return false; // No extension
                }
                String ext =
                    file.getName().substring(
                        file.getName().lastIndexOf('.') + 1).toLowerCase();

                if (!ext.equals("dax")) {
                    return false;
                }

                return true;
            }
        });

        if (candidates.length == 0) {
            JOptionPane
                .showMessageDialog(
                    null,
                    "Unable to locate any products, please make sure the .dax files are in " +
                        dir.getAbsolutePath(), "Error",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);
            return;
        }

        // Sort the product list
        Arrays.sort(candidates, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2)
            {
                return o1.getName().compareTo(o2.getName());
            }
        });

        for (int i = 0; i < candidates.length; i++) {
            cmbProduct.addItem(candidates[i].getName().substring(0,
                candidates[i].getName().lastIndexOf('.')));
        }
    }

    // parseVariables()
    private boolean parseVariables()
    {
        // Set the variables from the text fields
        for (Map.Entry<String, JTextField> entry : varValues.entrySet()) {
            String varname = entry.getKey();
            JTextField field = entry.getValue();

            // If it starts with ! then it is not displayed on the main frame
            if (varname.startsWith("!")) {
                continue;
            }

            // In case there is an error
            DaxFile.Variable variable = dax.getVariable(varname);

            if (field.getText().length() == 0) {
                JOptionPane.showMessageDialog(this,
                    "Please enter a value in the '" +
                        variable.getDescription() + "' field.", "Error",
                    JOptionPane.WARNING_MESSAGE);
                field.requestFocusInWindow();
                return false;
            }

            if (variable.isStringVariable()) {
                variable.setStringValue(field.getText());
            }
            else {
                byte[] val;
                try {
                    // Special case: the Year field needs to have 1900
                    // subtracted from it
                    if (varname.equals("year")) {
                        int year = Integer.parseInt(field.getText());
                        year -= 1900;
                        val = parseNumber(String.format("%d", year));
                    }
                    else {
                        val = parseNumber(field.getText());
                    }
                }
                catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(this,
                        "The '" + variable.getDescription() +
                            "' field does not parse properly.", "Error",
                        JOptionPane.WARNING_MESSAGE);
                    field.requestFocusInWindow();
                    return false;
                }

                int expectedLength = variable.getLength();
                if (expectedLength > val.length) {
                    // Pad on the left with 0s
                    byte[] realVal = new byte[expectedLength];
                    System.arraycopy(val, 0, realVal, expectedLength -
                                                      val.length, val.length);
                    val = realVal;
                }
                else if (expectedLength < val.length) {
                    // Truncate from the left
                    byte[] realVal = new byte[expectedLength];
                    System.arraycopy(val, val.length - expectedLength, realVal,
                        0, realVal.length);
                    val = realVal;
                }

                variable.setValue(val);
            }
        }

        return true;
    }

    // parseNumber()
    private byte[] parseNumber(String s)
    {
        // Try to determine what format the string is in
        // If it starts with "0x", parse it as hex
        // If it contains :s, parse it as individual bytes in hex, separated by
        // :s
        // Otherwise, parse in decimal (no octal here)
        s = s.toLowerCase(); // Easier to work with
        if (s.startsWith("0x")) {
            s = s.substring(2);
            return Conversion.hexToBytes(s);
        }
        else if (s.indexOf(':') != -1) {
            // Combine into a long hex string
            String hex = "";
            StringTokenizer tok = new StringTokenizer(s, ":");
            while (tok.hasMoreTokens()) {
                String t = tok.nextToken();
                if (t.length() == 0) {
                    hex += "00";
                }
                else if (t.length() == 1) {
                    hex += "0" + t;
                }
                else if (t.length() == 2) {
                    hex += t;
                }
                else {
                    throw new NumberFormatException(
                        "Colon-separated values must contain 0, 1, or 2 hex digits between each colon");
                }
            }

            return Conversion.hexToBytes(hex);
        }
        else {
            // Need to use Long.parseLong, unless later on there is a need to
            // bring in the big guns -- BigInteger
            long value = Long.parseLong(s);
            byte[] ans = new byte[8];
            Conversion.longToBytes(ans, 0, value);
            return ans;
        }
    }

    // getRecommendedVal
    private String getRecommendedVal(DaxFile.Variable variable)
    {
        // Some variables have a good default value
        String ans = null;

        if (variable.getName().equals("year")) {
            int dateCode = Initializer.getCurrentDateCode();
            ans = String.format("%d", ((dateCode >> 8) & 0xff) + 1900);
        }
        else if (variable.getName().equals("week")) {
            int dateCode = Initializer.getCurrentDateCode();
            ans = String.format("%d", dateCode & 0xff);
        }
        else if (variable.getName().equals("computerIP")) {
            return "10.3.0.1";
        }
        else if (variable.getName().equals("smid")) {
            ans = "0";
        }

        // Overwrite with defaults provided in the DAX file, as that is more
        // configurable than this code
        if (variable.getDefaultValue() != null) {
            ans = Conversion.bytesToHex(variable.getDefaultValue());
        }
        else if (variable.getDefaultStringValue() != null) {
            ans = variable.getDefaultStringValue();
        }

        // Finally make any special-case adjustments
        if (ans != null) {
            if (variable.getName().equals("alloc_macs")) {
                ans = Utils.correctPathToOS(ans);
            }
            else if (variable.getName().equals("alloc_sernum")) {
                ans = Utils.correctPathToOS(ans);
            }
            else if (variable.getName().equals("macdb")) {
                ans = Utils.correctPathToOS(ans);
            }
        }
        else {
            // Prevent NPEs
            ans = "";
        }

        return ans;
    }

    // saveSettings()
    void saveSettings()
    {
        // Save a few settings that the user might find useful
        Tag root = new Tag("baseinit");
        root.setAttribute("version", 1.0);

        {
            Tag serial = new Tag("serial");
            serial.addContent(txtComPort.getText());
            root.addContent(serial);
        }

        {
            Tag product = new Tag("product");
            product.addContent(cmbProduct.getSelectedItem().toString());
            root.addContent(product);
        }

        synchronized (persistentValues) {
            // Persistent values
            for (String key : persistentValues.keySet()) {
                String value = persistentValues.get(key);

                Tag valTag = new Tag("persistentValue");
                valTag.setAttribute("name", key);
                valTag.addContent(value);

                root.addContent(valTag);
            }
        }
        OutputStream out = null;
        try {
            out = new FileOutputStream(SETTINGS_FILE);
            root.toStream(out, true);
        }
        catch (IOException ioe) {
            // Do nothing, the user isn't likely to even notice
            Utils.debug(ioe);
        }
        finally {
            try {
                if (out != null) {
                    out.close();
                }
            }
            catch (IOException ioe) {
                // Ignore it
            }
        }
    }

    // loadSettings()
    private void loadSettings()
    {
        XML parser = Utils.xmlParser;

        synchronized (persistentValues) {
            persistentValues.clear();

            InputStream in = null;
            try {
                in = new FileInputStream(SETTINGS_FILE);
                Tag root = parser.parseXML(in, false, false);

                if (!root.getName().equals("baseinit")) {
                    throw new IOException("Bad root tag name"); // Bad file
                    // format
                }
                if (root.getFloatAttribute("version") != 1.0) {
                    throw new IOException("Bad version"); // Bad version
                }

                {
                    Tag serial = root.getTag("serial");
                    if (serial != null) {
                        txtComPort.setText(serial.getStringContents());
                    }
                }

                {
                    Tag product = root.getTag("product");
                    if (product != null) {
                        cmbProduct.setSelectedItem(product.getStringContents());
                    }
                }

                // Persistent values
                Tag[] persistentTags = root.getEachTag("", "persistentValue");
                for (int i = 0; i < persistentTags.length; i++) {
                    String name = persistentTags[i].getStringAttribute("name");
                    String value = persistentTags[i].getStringContents();

                    Utils.debug("Loaded persistent value '%s' = '%s'\n", name,
                        value);

                    persistentValues.put(name, value);
                }
            }
            catch (IOException ioe) {
                // Ignore it, the user probably won't even notice
                Utils.debug(ioe);
            }
            finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                }
                catch (IOException ioe) {
                    // Ignore it
                }
            }
        }
    }

    // handleSpecialVars()
    private boolean handleSpecialVars()
    {
        // Variables other than MAC addresses
        Vector<DaxFile.Variable> vars = new Vector<DaxFile.Variable>();

        // Special case -- MAC addresses, because we want to get them in a block
        Vector<DaxFile.Variable> macs = new Vector<DaxFile.Variable>();

        // Look through the variables
        for (String varname : varValues.keySet()) {
            // Figure out where it is in the station and ask for its current
            // value
            if (varname.startsWith("!")) {
                // Non-displayed variables are all of interest here, since the
                // user
                // has not yet given them values
                varname = varname.substring(1);
            }
            else {
                // The only displayed value we are interested in is the batch
                // number, and only when
                // the user specifies it as 0
                // If it is specified as 0, we need to use the value in the
                // base, if there is one
                if (varname.equals("batch")) {
                    try {
                        short batch =
                            Conversion.bytesToShort(dax.getVariable(varname)
                                .getValue(), 0);

                        if (batch == 0) {
                            // Allow it through, we need to look into the
                            // station
                        }
                        else {
                            // Ignore this variable, the user provided a value
                            // and we don't want to overwrite
                            continue;
                        }
                    }
                    catch (Exception e) {
                        // Could be a NumberFormatException or an
                        // ArrayIndexOutOfBoundsException, I think
                        Utils.debug(e);

                        JOptionPane.showMessageDialog(this,
                            "Error parsing '" +
                                dax.getVariable(varname).getDescription() +
                                "' field", "Error", JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                }
                else {
                    // Just ignore anything else
                    continue;
                }
            }

            // Get the value in the station for this variable
            DaxFile.Variable variable = dax.getVariable(varname);

            byte[] currentValue;

            try {
                Utils.debug("Retrieving variable " + variable.getName() +
                            " from the device");

                currentValue =
                    initializer.getValue(variable.getDatFile()
                        .getFileDestination(), variable.getDatFile()
                        .getDestinationType(), variable.getOffset(), variable
                        .getLength());
            }
            catch (IOException ioe) {
                Utils.debug(ioe);
                currentValue = new byte[variable.getLength()]; // All 0s
            }
            catch (TimeoutException te) {
                displayMessage("R " + te.getMessage());
                return false;
            }

            // Examine the result to determine if we need to do more work
            if (currentValue != null &&
                currentValue.length == variable.getLength()) {
                variable.setValue(currentValue);
            }
            else {
                variable.setValue(new byte[variable.getLength()]);
            }

            if (variable.getName().equals("date")) {
                Utils.debug("Examining date code in the device");

                // Check if it is a legal value (year >= 100 and week <= 53
                // (little bit of leeway))
                int year = variable.getValue()[0] & 0xff;
                int week = variable.getValue()[1] & 0xff;
                if (year < 100 || week > 53) {
                    // It's not a legal value, populate it with the 'year' and
                    // 'week' variables
                    // It should come after them, so it will overwrite their
                    // values
                    try {
                        byte yr = dax.getVariable("year").getValue()[0];
                        byte wk = dax.getVariable("week").getValue()[0];

                        Utils
                            .debug(String
                                .format(
                                    "  Not a legal date code, setting it to 0x%02x%02x",
                                    yr, wk));

                        byte[] newVal = {
                            yr, wk
                        };
                        variable.setValue(newVal);
                    }
                    catch (NullPointerException npe) {
                        JOptionPane
                            .showMessageDialog(
                                this,
                                "Badly formatted DAT file: If there is a 'date' variable, there must be preceeding 'year' and 'week' variables covering the same byte range",
                                "Error", JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                }
                else {
                    // Otherwise, keep the current date code instead of changing
                    // it
                    Utils.debug(String.format(
                        "  Date code legal, keeping 0x%02x%02x", variable
                            .getValue()[0], variable.getValue()[1]));
                }
            }
            else if (variable.getName().equals("batch")) {
                Utils.debug("Examining batch number in the device");

                // If the value in the device is non-zero, keep it
                int batch = Conversion.bytesToShort(variable.getValue(), 0);
                if (batch == 0) {
                    // This is a problem... The user said the batch number was
                    // 0, meaning
                    // use whatever is in the device, but the device does not
                    // have one
                    vars.add(variable);
                }
                else {
                    // All is well, the value in the station looks good, but
                    // let's pass it by the user anyway
                    // The only reason this is in a separate clause is because
                    // this may not be the most desirable behavior
                    vars.add(variable);
                }
            }
            else if (variable.getName().startsWith("macaddr")) {
                macs.add(variable);
            }
            else {
                vars.add(variable);
            }
        }

        // Ask about each individually, except for the MAC addresses which go in
        // a group
        for (DaxFile.Variable var : vars) {
            Vector<DaxFile.Variable> single = new Vector<DaxFile.Variable>();
            single.add(var);

            ValueConfirmationDialog.AcquireNew acq = null;

            // We know how to acquire new serial numbers
            if (var.getName().equals("serial") ||
                var.getName().equals("highserial")) {

                final boolean highSerial = var.getName().equals("highserial");

                int valueAsInt = Conversion.bytesToInt(var.getValue(), 0);
                if (valueAsInt == 0 || valueAsInt == -1) {
                    // If the current value is 0 or -1 then we should populate
                    // it automatically
                    Utils.debug("Auto-generating serial number");

                    SerialNumber newSerial;

                    try {
                        if (highSerial) {
                            newSerial =
                                SerialNumberFactory
                                    .getHighSerialNumber(InitializerGUI.this);
                        }
                        else {
                            newSerial =
                                SerialNumberFactory
                                    .getSerialNumber(InitializerGUI.this);
                        }
                    }
                    catch (IOException ioe) {
                        JOptionPane.showMessageDialog(InitializerGUI.this,
                            "Error acquiring new serial number: " +
                                ioe.getMessage(), "Error",
                            JOptionPane.ERROR_MESSAGE);
                        return false;
                    }

                    var.setValue(newSerial.getBytes());
                }
                else {
                    // This is how to acquire new serial numbers on demand
                    acq = new ValueConfirmationDialog.AcquireNew() {
                        @Override
                        public boolean allowAcquireNew()
                        {
                            return true;
                        }

                        @Override
                        public boolean requireNew(byte[] value)
                        {
                            SerialNumber serial = new SerialNumber(value);
                            try {
                                if (highSerial) {
                                    SerialNumberRanges ranges =
                                        new SerialNumberRanges(dax.getVariable(
                                            "alloc_sernum").getStringValue());
                                    return !SerialNumberFactory
                                        .isLegalHighSerial(serial, ranges);
                                }
                                else {
                                    return !SerialNumberFactory
                                        .isLegalSerial(serial);
                                }
                            }
                            catch (IOException ioe) {
                                return true;
                            }
                        }

                        @Override
                        public void getNew(byte[][] ans)
                            throws IOException
                        {
                            if (ans.length != 1) {
                                throw new IllegalArgumentException(
                                    "There should only be one required serial number");
                            }
                            if (ans[0].length != 4) {
                                throw new IllegalArgumentException(
                                    "The length of a serial number must be 4 bytes");
                            }

                            SerialNumber sn;
                            if (highSerial) {
                                sn =
                                    SerialNumberFactory
                                        .getHighSerialNumber(InitializerGUI.this);
                            }
                            else {
                                sn =
                                    SerialNumberFactory
                                        .getSerialNumber(InitializerGUI.this);
                            }

                            System.arraycopy(sn.getBytes(), 0, ans[0], 0, 4);
                        }

                        // Default describe/parse methods are fine, these should
                        // be displayed as decimal integers
                    };

                    ValueConfirmationDialog dlg =
                        new ValueConfirmationDialog(single, var.getComment(),
                            acq);
                    if (!dlg.showDialog()) {
                        return false;
                    }
                }
                // We know how to verify new batch numbers
            }
            else if (var.getName().equals("batch")) {
                acq = new ValueConfirmationDialog.AcquireNew() {
                    @Override
                    public boolean requireNew(byte[] value)
                    {
                        // Require a new one if it is 0
                        short batch = Conversion.bytesToShort(value, 0);
                        return (batch == 0);
                    }
                };

                ValueConfirmationDialog dlg =
                    new ValueConfirmationDialog(single, var.getComment(), acq);
                if (!dlg.showDialog()) {
                    return false;
                    // Anything else gets the default
                }
            }
            else { // Not a serial number, use the defaults (batch numbers fall
                // in here)
                ValueConfirmationDialog dlg =
                    new ValueConfirmationDialog(single, var.getComment(), null);
                if (!dlg.showDialog()) {
                    return false;
                }
            }
        }

        // Ask for the MAC addresses, if there are any
        if (macs.size() > 0) {
            // Sort the MAC addresses first, though, by variable name
            Collections.sort(macs);

            // Before making the dialog, if there is only one MAC address then
            // check if it's all 0s
            // If so, just automatically grab a new one
            MacAddress tempMac = new MacAddress(macs.get(0).getValue());
            if (macs.size() == 1 &&
                (MacAddress.ALL_ZEROS.equals(tempMac) || MacAddress.ALL_ONES
                    .equals(tempMac))) {

                // There is only one and it is all 0s or all 1s, auto-generate a
                // new one
                Utils.debug("Auto-generating MAC address");

                MacAddress newMac;

                try {
                    MacAddress[] addresses =
                        MacAddressFactory.getMACAddresses(InitializerGUI.this,
                            dax.getBlockSize());

                    newMac = addresses[0];
                }
                catch (IOException ioe) {
                    JOptionPane.showMessageDialog(InitializerGUI.this,
                        "Error acquiring new MAC address: " + ioe.getMessage(),
                        "Error", JOptionPane.ERROR_MESSAGE);
                    return false;
                }

                macs.get(0).setValue(newMac.getBytes());
            }
            else {
                // Either there is more than one MAC, or the one MAC is
                // non-zero, ask the user for input
                final int macCount = macs.size();
                final Vector<DaxFile.Variable> macVars = macs;
                ValueConfirmationDialog.AcquireNew acq =
                    new ValueConfirmationDialog.AcquireNew() {
                        @Override
                        public boolean allowAcquireNew()
                        {
                            return true;
                        }

                        @Override
                        public boolean requireNew(byte[] value)
                        {
                            try {
                                MacAddressRanges ranges =
                                    new MacAddressRanges(dax.getVariable(
                                        "alloc_macs").getStringValue());
                                return !MacAddressFactory.isLegalAddress(
                                    new MacAddress(value), new Ref<Integer>(),
                                    ranges);
                            }
                            catch (IOException ioe) {
                                return true;
                            }
                        }

                        @Override
                        public void getNew(byte[][] ans)
                            throws IOException
                        {
                            if (ans.length != macCount) {
                                throw new IllegalArgumentException(
                                    "There should only be " + macCount +
                                        " required MAC addresses");
                            }

                            for (int i = 0; i < ans.length; i++) {
                                if (ans[i].length != 6) {
                                    throw new IllegalArgumentException(
                                        "MAC addresses must be 6 bytes long");
                                }
                            }

                            // It is okay to return too many here, only the
                            // first X will be used, where X is macCount
                            MacAddress[] temp =
                                MacAddressFactory.getMACAddresses(
                                    InitializerGUI.this, dax.getBlockSize());

                            for (int i = 0; i < ans.length; i++) {
                                System.arraycopy(temp[i].getBytes(), 0, ans[i],
                                    0, ans[i].length);
                            }
                        }

                        @Override
                        public String describe(byte[] value, int index)
                        {
                            return new MacAddress(value).incrementBy(
                                macVars.get(index).getOffsetBy()).toString();
                        }

                        @Override
                        public byte[] parse(String text, int length, int index)
                        {
                            MacAddress addr = new MacAddress(text);

                            // Offset according to this address's position
                            addr =
                                addr.incrementBy(-macVars.get(index)
                                    .getOffsetBy());
                            return addr.getBytes();
                        }
                    };

                ValueConfirmationDialog dlg =
                    new ValueConfirmationDialog(macs, "MAC Addresses", acq);
                if (!dlg.showDialog()) {
                    return false;
                }
            }
        }

        return true;
    }

    // setLabel()
    private void setLabel(String msg, Color color)
    {
        lblPass.setText(msg);
        lblPass.setForeground(color);
    }

    // enableButtons()
    private void enableControls(boolean enabled)
    {
        txtComPort.setEnabled(enabled);
        cmbProduct.setEnabled(enabled);
        btnLoad.setEnabled(enabled);
        if (enabled && tester != null && tester.supportsTestJump()) {
            btnTest.setEnabled(true);
            btnRunAllTests.setEnabled(true);
        }
        else {
            btnTest.setEnabled(false);
            btnRunAllTests.setEnabled(false);
        }
        btnQuit.setEnabled(enabled);

        // Enable/disable the variable text fields
        productChoice.setEnabled(enabled);
    }

    // MyCellRenderer class
    private class MyCellRenderer
        implements ListCellRenderer
    {
        public MyCellRenderer()
        {
            // Nothing to do
        }

        public Component getListCellRendererComponent(JList list, Object value,
                                                      int index,
                                                      boolean isSelected,
                                                      boolean cellHasFocus)
        {

            JPanel panel = new JPanel();

            panel.setOpaque(true);
            panel.setLayout(new GridLayout(0, 1));

            String text = value.toString();

            Color background;
            Color foreground;

            if (text.startsWith("G ")) {
                foreground = C_GREEN;
                text = text.substring(2);
            }
            else if (text.startsWith("Y ")) {
                foreground = C_YELLOW;
                text = text.substring(2);
            }
            else if (text.startsWith("R ")) {
                foreground = C_RED;
                text = text.substring(2);
            }
            else {
                foreground = Color.BLACK;
            }

            background = Color.WHITE;

            panel.setBackground(background);

            StringTokenizer tok = new StringTokenizer(text, "\n");
            while (tok.hasMoreTokens()) {
                JLabel lbl = new JLabel(tok.nextToken());
                lbl.setOpaque(false);
                lbl.setForeground(foreground);
                panel.add(lbl);
            }

            return panel;
        }
    }
}

/*
 * Base_4.java
 *
 * Tests E2-MIX20884 bases.
 *
 * Jeff Collins
 * December 14, 2012
 *
 */

package com.sixnetio.Initializer.ModTest;

import com.sixnetio.Initializer.*;
import com.sixnetio.Station.UDRLib;
//import com.sixnetio.UDR.UDRHandler;

import java.io.*;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.util.*;

public class Base_4 implements Tester {
	
	private static final String T_RESISTOR = "Resistor Test";
    
    private static final String[] TEST_NAMES = {
    	T_RESISTOR
    };
    
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	
	public Base_4() { }
	
	public String getJumpTest() {
		return chosenTest;
    }
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}	
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
    }
	
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
    }
	
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for Base_1_2 must not be null");
		
		// Make sure the device name is recognized
		// Must start with EB-Base or E2-Base
		// Must end with -4 or -MIX20884
		if (!((devName.startsWith("EB-Base"))||(devName.startsWith("E2-")) &&
		      ((devName.endsWith("-4"))||(devName.endsWith("-MIX20884-B (base)"))))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)113, (byte)0);
		testLib = new TestLib(udr, ui);
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
    }
	
	public boolean supportsTestJump() {
		return true;
    }
	
	public void run() {
		MultiIOException mioe = new MultiIOException();
		
		if (chosen(T_RESISTOR)) {
			
  		try { 
    			// Now start doing tests
    			// Resistor Test
    			try {
    				if (chosen(T_RESISTOR)) resistorTest();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		}
		}
		
		if (!mioe.isEmpty()) {
			ui.handleException(this, mioe);
			ui.operationFailed(this);
		} else {
			ui.operationCompleted(this);
		}
    }
	
	private void resistorTest() throws Exception, IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AI Resistance Accuracy");
		
		// No Resistors on E2-Base-MIX20884 so resistance should be infinite or Over Limit "OL"
		if (!ui.confirm("Measure the E2-Base-MIX20884 AI input series resistance from the terminals provided on the Initializer Module.\n" +
			    "Is the measured resistance OL (0pen circuit: No resistors in E2-Base-MIX20884)?", "AI Resistance Accuracy Check")) {
			mioe.add(new IOException("AI Resistance Accuracy Check Failed"));
		}
		if (!mioe.isEmpty()) throw mioe;		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  AI Resistance Accuracy Check Passed");
    }
	
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
}

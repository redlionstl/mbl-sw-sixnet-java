/*
 * Mod16ISOAI_TC.java
 *
 * Tests a combination 16ISOTC and 16ISOAI module.
 *
 * Jeff Collins
 * January 24, 2011
 *
 */

package com.sixnetio.Initializer.ModTest;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.Initializer.Tester;
import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.util.*;

public class Mod16ISOAI_TC implements Tester {
	// Constants
	// Locations of the configuration files
	// These get loaded into the AI base
	private static final String DIR_AIBASE = "TestData" + File.separator + "ISOAI_TC" + File.separator + "Base1";	
	private static final String DIR_WATCHDOG_OFF = "TestData" + File.separator + "ISOAI_TC" + File.separator + "WatchdogOff";

	// These get loaded into the TC base
	private static final String DIR_TCBASE_16 = "TestData" + File.separator + "ISOAI_TC" + File.separator + "Base2_16ISOTC";
	private static final String DIR_TCBASE_8 = "TestData" + File.separator + "ISOAI_TC" + File.separator + "Base2_8ISOTC";
	
	private static final String T_DIAG1 = "Diagnostics Test (Base 1)",
								T_POE = "PoE Test",
								T_WATCHDOG = "Watchdog Test",
								T_AICal = "4-20mA AI Calibration",
							    T_AITEST = "4-20mA AI Range Test",
							    T_DIAG2 = "Diagnostics Test (Base 2)",
	                            T_READTEMPSENSORS = "Read Temperature Sensors Test",
	                            T_MVRANGES = "AI Millivolt Range and Channel Independence Test",
	                            T_CROSSTALK = "AI Crosstalk Test";
	
	private static final String[] TEST_NAMES = {
		// Base 1 using Eth1 and USB 
		T_DIAG1,
		T_POE,
		T_WATCHDOG,
		T_AICal,
		T_AITEST,
		
		// Base 2 using Eth2, and RS485 to 32DO24
		T_DIAG2,
		T_READTEMPSENSORS,
		T_MVRANGES,
		T_CROSSTALK
		
	};
	
	// Try to keep every test module using a different set of IP addresses, just in case we figure out
	//   a way to run multiple fixtures from the same machine at the same time
	private static final String IP_BASE1 = "10.1.13.1";
	private static final String IP_BASE2 = "10.1.13.2";
	
	private static final short S_AIBASE = 1; // AI Module Verification
	private static final short S_TCBASE = 2; // TC Module Verification
	private static final short S_DOMODULE = 3; // E2-32DO24 in charge of controlling Relays
	
	// Which AI the watchdog output terminal is plugged into
	private static final short AI_WATCHDOG = 1; // (0-based)
	
	// Which AI the POE output terminal is plugged into
	private static final short AI_POE = 0; // 0-based
	
	// Diagnostic DI offsets from DI_DiagStart
	private static final short DI_DIAG_START = 32; // The first diagnostic DI
	private static final short DIAG_SELFTEST = 0, // Module Selftest
	                           DIAG_POWER1 = 1, // Power 1
	                           DIAG_POWER2 = 2, // Power 2
	                           DIAG_IOSTATUS = 3, // I/O Status
	                           DIAG_ETH1LINK = 4, // Ethernet 1 has link
	                           DIAG_ETH2LINK = 5, // Ethernet 2 has link
	                           DIAG_RING = 6, // Ring is complete
	                           DIAG_POE = 7; // PoE is available
	
	// Analog values (to avoid hardcoding them in multiple places)
	private static final short EXP_WATCHDOG = (short)3830; // Expected watchdog input value; 3830 = 5.87mA = 23.2V across 3950 Ohms
	private static final short EXP_POE = (short)4248; // Expected PoE input value; 4248 ~ 6mA = .3V across a 51.1 Ohm resistor
	private static short AI_225_0mV = (short)29490; // Expected millivolt AI input value for 250mV range tests
//	private static final short AI_0_9V = (short)27878; // Expected 0.9000V AI value
	private static final short AI_ZERO = (short)0; // Expected 0.0mV AI value
    private static final short AI_4_4MA = (short)826; // Expected 4.4mA AI input value for 4-20mA calibration (225.00mV from Base 5 Power Supply 2)
    private static final short AI_17_6MA = (short)27878; // Expected 17.612mA AI input value for 4-20mA cal and test (0.9000V from Base 5 Power Supply 1)

	// Tolerance for analog values
    // Used to check uncalibrated AI readings are reasonable for calibration
	private static final int TOLERANCE_UNCALIBRATED = 2050; // approximately +/- 1 mA from expected value
	
	// If two analog values should be equal, values within this distance on either side will be accepted
	private static final int TOLERANCE_ANALOG = 12;
	
	// Similar to AnalogTolerance, this is used to make sure consecutive scans during an average are close enough
	// Since values are allowed to be off by 12 in either direction of the target value, a difference of 24 covers
	//   the situation where one scan sees a value 12 below and the next scan sees 12 above, then we add a little
	//   extra to avoid spurious errors
	private static final int TOLERANCE_AVERAGE = 25;
	
	// Allowed tolerance for multiple readings on same TC Sensor channel 
	private static final int TOLERANCE_TC_AVERAGE = 1;
	
	// Used to check how far cold junction temperature sensor readings are from the mean.  7 = .7 degrees C.
	// Only checking that all sensors are read, and values are reasonable so added 2 to what is used in E2-Base-x test
	private static final int TOLERANCE_CJC = 7;
	
	// When testing for channel independence, this is the tolerance for the channel under test when set to 225.00mV
	private static final int TOLERANCE_INDEPENDENCE = 25;
	
	// When testing for channel independence, this is the tolerance for the 15 channels not under test when set to 0.0mV
	private static final short AI_ZERO_TOLmV = (short)10; // +/- 10 counts tolerance for expected 0.0mV AI value	
	
	// Used to check that the Watchdog input is close to what it should be
	private static final int TOLERANCE_WATCHDOG = 450;
	
	// Used to check that the PoE input is close to what it should be
	private static final int TOLERANCE_POE = 600;
	
	// Base directory in a module where we will find the configuration files
	// We expect a slash on the end of this
	private static final String DIR_MODULECONFIG = "/module0/arc/";
	
    // Offset/length of the AI calibration section of the module cals file
    private static final int OFFSET_AI_CALIBRATION = 0x100;
    private static final int LENGTH_AI_CALIBRATION = 0x80;
	
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private UDRLink activeHandler;
	private boolean initialized = false;
	private boolean individualTest = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	public Mod16ISOAI_TC() { }
	
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for Mod16ISOAI_TC must not be null");
		
		// Make sure the device name is recognized
		// Must start with E2 or EB
		// Must end with 16ISO20M, 16ISOTC, or 8ISOTC
		if (!((devName.startsWith("E2") || devName.startsWith("EB")) &&
		      (devName.endsWith("16ISO20M") || devName.endsWith("16ISOTC") || devName.endsWith("8ISOTC")))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)111, (byte)0);
		testLib = new TestLib(udr, ui);
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
	}
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
	}
	
	public boolean supportsTestJump() {
		return true;
	}
	
	public void jumpToTest(String testName) {
		if (testName != null) {
			individualTest = true;
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}else individualTest = false;
		
		chosenTest = testName;
	}
	
	public String getJumpTest() {
		return chosenTest;
	}
	
	public void run() {
		MultiIOException mioe = new MultiIOException();
		
		if (chosen(T_DIAG1) ||
				chosen(T_POE) ||
				chosen(T_WATCHDOG) ||
				chosen (T_AICal) ||
				chosen (T_AITEST)){ 
			
			if (!ui.confirm("Please plug the module into the first base", "Module Check")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "User canceled the test");
				ui.operationFailed(this);
				return;
			}
			
    		// Register a UDP handler for base 1
    		try {
    			activeHandler = testLib.registerUDPHandler(IP_BASE1);
    			
    			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
    			udr.waitForResponse(S_AIBASE);
    			
    			// Make sure it works
    			testLib.loadBaseFiles(S_AIBASE, DIR_AIBASE);
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		}
    		
    		try { // Finally block unregisters the handler
    			// Now start doing tests
    			// Diagnostics test 1
    			try {
    				if (chosen(T_DIAG1)) checkDiags1();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    			
    			// POE test
    			try {
    				if (chosen(T_POE)) checkPOE();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    			
    			// Watchdog test
    			try {
    				if (chosen(T_WATCHDOG)) checkWatchdog();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
                // AI Calibration/test
                try {
                    if (chosen(T_AICal)&&(!runVerifyTest)) calibrateAIs();
                    if (chosen(T_AITEST)) testAIs(); // If the calibration fails, don't do the test
                } catch (IOException ioe) {
                    mioe.add(ioe);
                }
    			
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		} finally {
    			testLib.unregisterHandler(activeHandler);
    		}
		}
		
		if (chosen(T_DIAG2) ||
				chosen(T_READTEMPSENSORS) ||
				chosen(T_MVRANGES) ||
				chosen(T_CROSSTALK)){
			
			while (!ui.confirm("Please move the module to the second base", "Move the Module")) {
				if (ui.confirm("Really cancel the test?", "Cancel")) {
					ui.displayMessage(this, UserInterface.M_ERROR, "User canceled the test");
					ui.operationFailed(this);
					return;
				}
			}
			
    		// Register a UDP handler for station 2
    		try {
    			activeHandler = testLib.registerUDPHandler(IP_BASE2);
    			
    			// Make sure it works
    			if (devName.endsWith("8ISOTC")){
    			testLib.loadBaseFiles(S_TCBASE, DIR_TCBASE_8);
    			}else testLib.loadBaseFiles(S_TCBASE, DIR_TCBASE_16);
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		}
    		
    		try { // Finally block unregisters the handler
    			// Diagnostics 2 test
    			try {
    				if (chosen(T_DIAG2)) checkDiags2();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    			
    			// Read CJC temp sensor test
    			try {
    				if (chosen(T_READTEMPSENSORS)) readTempSensors();
       			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    			// mV Range Test
    			try {
    				if (chosen(T_MVRANGES)) mVRangeTest();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    			// Test crosstalk between channels
    			try {
    				if (chosen(T_CROSSTALK)) testCrosstalk();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		} finally {
    			testLib.unregisterHandler(activeHandler);
    		}
		}
		
        if (chosenTest == null) {
            // null is all tests
            // write the pass flag
            if (writePassedFlag(true)){
                ui.displayMessage(this, UserInterface.M_NORMAL,
                "Module tests passed");
            }
            else{
                ui.handleException(this, new IOException("Failed to write the All Tests Passed flag to module."));
                ui.operationFailed(this);
                return;
            }
        }
        else {
            ui.displayMessage(this, UserInterface.M_NORMAL, "Module test '" +
                                                            chosenTest +
                                                            "' passed");
        }

        if (!mioe.isEmpty()) {
            ui.handleException(this, mioe);
            ui.operationFailed(this);
        } else {
            ui.operationCompleted(this);
        }

	}
	
    private boolean writePassedFlag(boolean pass)
    //  throws IOException, TimeoutException
    {
        boolean ret = false;
        byte[] flagbyte = new byte[1]; 
        flagbyte[0] = 0; // must be 0 if failed
        if(pass) flagbyte[0] = 1; // must be 1 if pass
        try {
            activeHandler = testLib.registerUDPHandler(IP_BASE2);
            udr.writeFile(S_TCBASE, "/module0/arc/factory", flagbyte, 0x4B, null);
            // Disconnect the handler
            activeHandler = testLib.unregisterHandler(activeHandler);
            ret = true;
        }
        catch (IOException e) {
            ret = false;
        }
        catch (TimeoutException e) {
            ret = false;
        }
        
        return ret;
    }

	private void checkDiags1() throws Exception, IOException, TimeoutException {
		final short stationID = S_AIBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		boolean[] dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected on)
		if (dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 off");
			mioe.add(new IOException("Module reports no power on power 1"));
		}
		
		// Power 2 (expected off)
		if (!dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 off");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 on");
			mioe.add(new IOException("Module reports power on power 2"));
		}
		
		// Ethernet 1 (expected on)
		if (dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 1. Please verify the ethernet cable from the power injector to Ethernet Port 1 is secure. Continue the test?", "Ethernet 1 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 1 link");
			} else {
				dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH1LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 1"));
				}
			}
		}
		
		// Ethernet 2 (expected off)
		if (!dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 not linked");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 linked");
			throw new Exception("Module has a link on ethernet 2");
		}
		
		// POE (expected on if EB, off if E2)
		if (devName.startsWith("EB") && !dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  No POE");
			mioe.add(new IOException("Module reports no POE, although it is a POE module and ethernet 1 has a link"));
		} else if (devName.startsWith("E2") && dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE on non-EB module");
			mioe.add(new IOException("Module reports POE is available, although it is not an EB module"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks for Base 1 complete");
	}
	
	private void checkDiags2() throws Exception, IOException, TimeoutException {
		final short stationID = S_TCBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		boolean[] dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected off)
		if (!dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 off");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 on");
			mioe.add(new IOException("Module reports power on power 1"));
		}
		
		// Power 2 (expected on)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 off");
			mioe.add(new IOException("Module reports no power on power 2"));
		}
		
		// Ethernet 1 (expected off)
		if (!dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 not linked");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 linked");
			throw new Exception("Module has a link on ethernet 1");
		}
		
		// Ethernet 2 (expected on)
		if (dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 2. Please verify the ethernet cable from the switch to Ethernet Port 2 is secure. Continue the test?", "Ethernet 2 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 2 link");
			} else {
				dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH2LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 2"));
				}
			}
		}
		
		// POE (expected off, since ethernet 1 is not linked)
		if (!dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE powered with no Ethernet 1 link");
			mioe.add(new IOException("Module reports POE is available when there is no Ethernet 1 link"));
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks for Base 2 complete");
	}
	
	private void readTempSensors() throws IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		double sum_cjc = 0;// Sum of CJC values for 4 temperature sensors
		double mean_cjc = 0;// Average of CJC values for 4 temperature sensors

		ui.displayMessage(this, UserInterface.M_WARNING, "Reading Cold Junction Compensation Temperature Sensors");
		Utils.sleep(2000);
		// Figure out how many cold junction compensation temperature sensors to read
		int cjcCount, cjcStart;
		if (devName.endsWith("16ISO20M") || devName.endsWith("16ISOTC")) {
			cjcCount = 4;
			cjcStart = 16;
		} else if (devName.endsWith("8ISOTC")) {
			cjcCount = 2;
			cjcStart = 8;
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unrecognized device name: " + devName);
			throw new IOException("Unrecognized device name: " + devName);
		}
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * cjcCount + 200);
		
		// Grab the results
		Utils.sleep(testLib.getScanDelay() * 2);
		short[] cjc = udr.getA(S_TCBASE, (short)cjcStart, (short)cjcCount);

		// Calculate the sum of the CJ temperature sensor readings
		for (int i = 0; i < cjc.length; i++) {
			sum_cjc += cjc[i];
		}

// Calculate the mean for the CJ temp sensor values read from the base
		mean_cjc = sum_cjc/cjcCount;	
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Mean Cold Junction Compensation Sensor reading = " + (int)mean_cjc );
		if(mean_cjc < 190 || mean_cjc > 260){
			String msg = String.format("Is the approximate room ambient temperature really %.1f degrees C or %.1f degrees F", mean_cjc/10, ((mean_cjc/10)*9/5+32));
			if (!ui.confirm(msg, "Confirm Approximate Ambient Room Temperature")) {
				mioe.add(new IOException("Mean Cold Junction Sensor reading not within normal ambient room temperature range!"));
			}	
		}		
// Check that all of the CJ temperature sensors are read and that the readings are reasonable
		for (int i = 0; i < cjc.length; i++) {
			String msg = formatRangeMessage("CJC Sensor AI", i+cjcStart, cjc[i], (short) mean_cjc, TOLERANCE_CJC);
			if((Math.abs((int)cjc[i]-(int)mean_cjc) >TOLERANCE_CJC)||cjc[i]==0){
				mioe.add(new IOException(msg));
				ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
			}
		}		
	
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Read Cold Junction Compensation Temperature Sensor test complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}

    private void calibrateAIs() throws Exception {
        MultiIOException mioe = new MultiIOException();
        
        ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating AIs");
        
        // Clear any calibration data that may exist
        ui.displayMessage(this, UserInterface.M_WARNING, "  Clearing current calibration");
        
        if (testLib.zeroRange(S_AIBASE, DIR_MODULECONFIG + "cals", OFFSET_AI_CALIBRATION, LENGTH_AI_CALIBRATION)) {
            ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
            testLib.resetStation(S_AIBASE);
            Utils.sleep(4000);
        }
        
        ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");
        
        int aiCount = 16;
        
        // Set the scan delay
        // Documentation says 100ms per active channel, we add a little to be careful
        testLib.setScanDelay(100 * aiCount + 200);
        
        short[] ais = new short[aiCount];
        
        // Calibration results will end up in these arrays
        short[] spans = new short[ais.length];
        short[] zeros = new short[ais.length];
        
		// Turn off all DOs on 32DO Module to start
		boolean[] dos = new boolean[20]; // NOTE: This is the only place where the count is specified
		Arrays.fill(dos, false);
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
		
        // Set AI input to 4.4mA (0.22500V) reference value 0-2V power supply.
    	dos[19] = true; // Set Power Supply input at Relay 10 to mV side.
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
//        if (!ui.confirm("Please set the voltage source to output 225.00 mV (+/- .5 mV)", "Set Voltage")) {
//            throw new Exception("User canceled the test");
//        }
//        String usrinput = null; 
//        usrinput = ui.requestString("Please set the voltage source to output 225.00 mV (+/- .010 VDC) and enter the reading from the calibrated test meter (e.g. 225.00)", "Voltage Reference Reading", usrinput);
//        ui.displayMessage(this, UserInterface.M_NORMAL, "  usrinput = " + usrinput );
//        float meterValue = Float.parseFloat(usrinput);
//        short AI_225mV = (short)((meterValue / 51.1 * 1000 - 4) / 16 * 32767);
        
        // Wait for it to take effect
        Utils.sleep(testLib.getScanDelay() * 4);
        
        // Grab the results
        short[] aisLow = testLib.getAverageAnalogs(S_AIBASE, (short)0, (short)ais.length, TOLERANCE_AVERAGE);
        aisLow[0]=aisLow[2];
        aisLow[1]=aisLow[2];        
        // Set 17.625mA (0.9000V) reference value from power supply.
    	dos[19] = false; // Set Power Supply input at Relay 10 to 0.9000V side.
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
//        if (!ui.confirm("Please set the voltage source to output 0.9000 VDC (+/- .010 VDC)", "Set Voltage")) {
//            throw new Exception("User canceled the test");
//        }
//        usrinput = null; 
//        usrinput = ui.requestString("Please set the voltage source to output 0.9000 VDC (+/- .010 VDC) and enter the reading from the calibrated test meter (e.g. 0.9000)", "Voltage Reference Reading", usrinput);
        
        // Wait for it to take effect
        Utils.sleep(testLib.getScanDelay() * 4);
        
        // Grab the results
        short[] aisHigh = testLib.getAverageAnalogs(S_AIBASE, (short)0, (short)ais.length, TOLERANCE_AVERAGE);
        aisHigh[0]=aisHigh[2];
        aisHigh[1]=aisHigh[2];
        // Compute the span and zero for each AI channel
        computeAICurrentCalibration(aisLow, aisHigh, AI_4_4MA, AI_17_6MA, zeros, spans);
        
        // Write the corrections to the config file
        ui.displayMessage(this, UserInterface.M_WARNING, "  Writing calibration data to station");
        
        byte[] calsData = new byte[ais.length * 4]; // 2 values of 2 bytes apiece for each input
        int offset = 0;
        for (int i = 0; i < ais.length; i++) {
            Conversion.shortToBytes(calsData, offset, zeros[i]);
            offset += 2;
            
            Conversion.shortToBytes(calsData, offset, spans[i]);
            offset += 2;
        }
        
        udr.writeFile(S_AIBASE, DIR_MODULECONFIG + "cals", calsData, OFFSET_AI_CALIBRATION, null);
        
        ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
        
        // Reset the station
        testLib.resetStation(S_AIBASE);
        
        ui.displayMessage(this, UserInterface.M_NORMAL, "  AI calibration complete");
        
        if (!mioe.isEmpty()) throw mioe;
    }	
	private void testAIs() throws Exception, IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing 4-20mA AIs");
		
		// Figure out how many AIs there are
		int aiCount;
		if (devName.endsWith("16ISO20M") || devName.endsWith("16ISOTC")) {
			aiCount = 16;
		} else if (devName.endsWith("8ISOTC")) {
			aiCount = 8;
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unrecognized device name: " + devName);
			throw new IOException("Unrecognized device name: " + devName);
		}
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 200);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing all AIs at once");
		{
			// Turn off all DOs on 32DO Module to start
			boolean[] dos = new boolean[20]; // NOTE: This is the only place where the count is specified
			Arrays.fill(dos, false);
			udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
			
			// Set the voltage at 0.9000 VDC
	    	dos[19] = false; // Set Power Supply input at Relay 10 to 0.9000V side.
			udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
//			if (!ui.confirm("Please set the voltage source to output 0.9000 VDC (+/- .010 VDC)", "Set Voltage")) {
//				throw new Exception("User canceled the test");
//			}
//			// Get reference value from user.
//			String usrinput = null; 
//			usrinput = ui.requestString("Please set the voltage source to output 0.9000 VDC (+/- .010 VDC)and enter the reading from the calibrated test meter (e.g. 0.9000)", "Voltage Reference Reading", usrinput);
//			ui.displayMessage(this, UserInterface.M_NORMAL, "  usrinput = " + usrinput );
//			float meterValue = Float.parseFloat(usrinput);
//			short AI_0_9V = (short)((meterValue / 51.1 * 1000 - 4) / 16 * 32767);
//			short AI_0_9V = (short)(meterValue * 32767);
			
			// Read all AI channels
			Utils.sleep(testLib.getScanDelay() * 2);
			short[] ais = testLib.getAverageAnalogs(S_AIBASE, (short)2, (short)(aiCount-2), TOLERANCE_AVERAGE);
			
			// Compare
			// Every channel should be approximately AI_0_9v
			for (int i = 0; i < ais.length; i++) {
				String msg = formatRangeMessage("AI", i+2, ais[i], AI_17_6MA, TOLERANCE_ANALOG);
				if (Math.abs(ais[i] - AI_17_6MA) > TOLERANCE_ANALOG) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  4-20mA AI tests complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	
	private void mVRangeTest() throws Exception, IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AIs in 250mV range using 225mV");
		
		// Figure out how many AIs there are
		int aiCount;
		if (devName.endsWith("16ISO20M") || devName.endsWith("16ISOTC")) {
			aiCount = 16;
		} else if (devName.endsWith("8ISOTC")) {
			aiCount = 8;
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unrecognized device name: " + devName);
			throw new IOException("Unrecognized device name: " + devName);
		}
		// Turn off all DOs on 32DO Module to start
		boolean[] dos = new boolean[20]; // NOTE: This is the only place where the count is specified
		Arrays.fill(dos, false);
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);

		
		// Set input voltage to 225.0mV on all AI channels 
		for (int i = 0; i < aiCount; i++) {
			dos[i] = true;
		}
		// This is temporarily needed until the 2 dedicated 0-2 VDC test fixture power supplies are implemented. 
	//	dos[19] = true; // Set Power Supply input at Relay 10 to mV side.
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 200);
		
		// Set all channels low/high and check that they are each where they should be
/*		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing all AIs at once");
		{
			// Ask the user to set the voltage at 225.0mV
			if (!ui.confirm("Please set the voltage source to output 225.00 mV (+/- .5 mV)", "Set Voltage")) {
				throw new Exception("User canceled the test");
			}
			// Get milivolt reference value from user.
			String usrinput = null; 
			usrinput = ui.requestString("Please set the voltage source to output 225.00 mV (+/- .5 mV) and enter the reading from the calibrated test meter (e.g. 225.00)", "Millivolt Reference Reading", usrinput);
			ui.displayMessage(this, UserInterface.M_NORMAL, "  usrinput = " + usrinput );
			float meterValue = (float) 225.00; //Float.parseFloat(usrinput);
			AI_225_0mV = (short)(meterValue / 250 * 32767);
			AI_225_0mV = (short)((meterValue / 51.1 * 1000 - 4) / 16 * 32767);

			{	//Read AIs
				Utils.sleep(testLib.getScanDelay() * 2);
				short[] ais = testLib.getAverageAnalogs(S_TCBASE, (short)0, (short)aiCount, TOLERANCE_AVERAGE);

				// Compare
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i, ais[i], AI_225_0mV, TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - AI_225_0mV) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
				}
			}
		}

		ui.displayMessage(this, UserInterface.M_NORMAL, "  Millivolt range test complete");
*/		
		//Test Each AI channel independently to ensure they are not tied together.
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing AI channel independence");
		{
		// Turn off all DOs on 32DO Module to start
		Arrays.fill(dos, false);
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);	
//		dos[19] = true;	// Set Power Supply input at Relay 10 to mV side.
//		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);	
		// Walk through all AIs individually to check channel independence(ON = mV, OFF = Gnd)
			for (int i = 0; i < aiCount; i++) {
				int zeroChannels = 0; // Used to add decimal values of channels not under test.
				// Turn off the previous DO and turn on the current one
				if (i > 0) dos[i - 1] = false;
				dos[i] = true;
				udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
				
				// Sleep for a moment to let it stabilize
				Utils.sleep(200);
				
				{	//Read AIs
					Utils.sleep(testLib.getScanDelay() * 2);
					short[] ais = udr.getA(S_TCBASE, (short)0, (short)aiCount);			
	
					// Compare
					for (int j = 0; j < ais.length; j++) {
						String msg = formatRangeMessage("AI", i, ais[i], AI_225_0mV, TOLERANCE_ANALOG);
						if (j==i){
							if (Math.abs(ais[i] - AI_225_0mV) > TOLERANCE_ANALOG) {
								mioe.add(new IOException(msg));
								ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
							} else {
								ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
							}
						}else if (ais[j] > AI_ZERO_TOLmV){
							msg = formatRangeMessage("AI", j, ais[j], AI_ZERO, AI_ZERO_TOLmV);
							mioe.add(new IOException(msg));
							ui.displayMessage(this, UserInterface.M_ERROR, "  " + (msg));
						} 
					}
				}
			}
		}
		// Test complete, turn off DOs on 32DO Module
		Arrays.fill(dos, false);
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);		
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Channel Independence test complete");

		if (!mioe.isEmpty()) throw mioe;
	}
	private void testCrosstalk() throws Exception, IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		boolean[] dos = new boolean[20]; // NOTE: This is the only place where the count is specified
		// This keeps us from hard-coding this number everywhere
		// Figure out how many AIs there are
		int aiCount;
		if (devName.endsWith("16ISO20M") || devName.endsWith("16ISOTC")) {
			aiCount = 16;
		} else if (devName.endsWith("8ISOTC")) {
			aiCount = 8;
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unrecognized device name: " + devName);
			throw new IOException("Unrecognized device name: " + devName);
		}
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing Crosstalk on even number channels (0-based)");
		
		// Turn off DOs on 32DO Module to start
		Arrays.fill(dos, false);
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);

/*		// Ask the user to set the voltage at 225.0mV
		if (!ui.confirm("Please set the voltage source to output 225.00 mV (+/- .5 mV)", "Set Voltage")) {
			throw new Exception("User canceled the test");
		}
		// Get milivolt reference value from user.
		String usrinput = null; 
        usrinput = ui.requestString("Please set the voltage source to output 225.00 mV (+/- .5 mV) and enter the reading from the calibrated test meter (e.g. 225.00)", "Millivolt Reference Reading", usrinput);
		ui.displayMessage(this, UserInterface.M_NORMAL, "  usrinput = " + usrinput );
		float meterValue = (float) 225.00; //Float.parseFloat(usrinput);
		AI_225_0mV = (short)(meterValue / 250 * 32767);
		AI_225_0mV = (short)((meterValue / 51.1 * 1000 - 4) / 16 * 32767);
*/		
		//Set even number channels to 225.00V 
		for (int i = 0; i < aiCount; i+=2) {
			dos[i] = true;
		}
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
		Utils.sleep(200); //let the relays activate
		
		dos[16]=true; //set CH+ voltage to 24V for odd number channels (0-based)
		dos[17]=true; //set CH- voltage to 24V for odd number channels (0-based)
		// This is temporarily needed until the 2 dedicated 0-2 VDC test fixture power supplies are implemented. 
//		dos[19] = true;	// Set Power Supply input at Relay 10 to mV side.
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 200);
		
		// Read all AI channels
		
		Utils.sleep(testLib.getScanDelay() * 3);
		
		short[] ais = udr.getA(S_TCBASE, (short)0, (short)aiCount);			

		// Check accuracy of even channels (0-based)
		for (int i = 0; i < aiCount; i+=2) {
			String msg = formatRangeMessage("AI", i, ais[i], AI_225_0mV, TOLERANCE_ANALOG);
			if (Math.abs(ais[i] - AI_225_0mV) > TOLERANCE_ANALOG) {
				mioe.add(new IOException(msg));
				ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
			}
		}

		ui.displayMessage(this, UserInterface.M_WARNING, "Testing Crosstalk on odd number channels (0-based)");
		
		// Turn off DOs on 32DO Module to start
		Arrays.fill(dos, false);
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
	
		//Set odd number channels to 225.0V 
		for (int i = 1; i < aiCount+1; i+=2) {
			dos[i] = true;
		}
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
		Utils.sleep(200); //let the relays activate

		dos[16]=true; //set CH+ voltage to 24V for even number channels (0-based)
		dos[18]=true; //set CH- voltage to 24V for even number channels (0-based)
		// This is temporarily needed until the 2 dedicated 0-2 VDC test fixture power supplies are implemented. 
//		dos[19] = true;	// Set Power Supply input at Relay 10 to mV side.
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);

		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 200);
		
		// Read all AI channels
		
		Utils.sleep(testLib.getScanDelay() * 3);
		ais = udr.getA(S_TCBASE, (short)0, (short)aiCount);			
	
		// Check accuracy of odd channels (0-based)
		for (int i = 1; i < aiCount+1; i+=2) {
			String msg = formatRangeMessage("AI", i, ais[i], AI_225_0mV, TOLERANCE_ANALOG);
			if (Math.abs(ais[i] - AI_225_0mV) > TOLERANCE_ANALOG) {
				mioe.add(new IOException(msg));
				ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
			}
		}
		// Test complete, turn off DOs on 32DO Module
		Arrays.fill(dos, false);
		udr.putD(S_DOMODULE, (short)0, (short)dos.length, dos);
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Crosstalk test complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	private void checkPOE() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the PoE output");
		
		// If it's an EB base, check AI_POE for a value, otherwise expect something small (-8192, probably)
		short[] vals = udr.getA(S_AIBASE, AI_POE, (short)1);
		
		if (devName.startsWith("EB")) {
			if (Math.abs(EXP_POE - vals[0]) > TOLERANCE_POE) {
				throw new IOException(String.format("AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_NOTMEMBEROF, EXP_POE, TOLERANCE_POE));
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_MEMBEROF, EXP_POE, TOLERANCE_POE));
			}
		} else {
			if (vals[0] > 0) {
				throw new IOException("It appears that there is a PoE board on this module, but it has been initialized as an E2.");
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  As expected, there is no PoE board on this module.");
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  PoE test complete");
	}
	
	private void checkWatchdog() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the Watchdog output");
		
		// Give it a couple seconds in case it was rebooted recently
		Utils.sleep(10000);
		
		short[] vals = udr.getA(S_AIBASE, AI_WATCHDOG, (short)1);
		
		if (Math.abs(EXP_WATCHDOG - vals[0]) > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output): is off when it should be on", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is on as expected", AI_WATCHDOG));
		}
		
		// Make sure the watchdog output is off
		testLib.loadBaseFiles(S_AIBASE, DIR_WATCHDOG_OFF);
		
		// Give it a couple extra seconds
		Utils.sleep(10000);
		
		vals = udr.getA(S_AIBASE, AI_WATCHDOG, (short)1);
		
		if (vals[0] > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output) is on when it should be off", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is off as expected", AI_WATCHDOG));
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Watchdog test complete");
	}
	
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
	
	private String formatRangeMessage(String type, int channel, short found, short expected, int tolerance) {
		String op = TestLib.CH_MEMBEROF;
		if (Math.abs(expected - found) > tolerance) op = TestLib.CH_NOTMEMBEROF;
		
		return String.format("%s Channel %,d: %s%,d [%,d %s (%,d +/- %,d)]", type, channel, TestLib.CH_DELTA, found - expected, found, op, expected, tolerance);
	}
	// 4-20mA AI Calibration Computations
	private void computeAICurrentCalibration(short[] aisLow, short[] aisHigh,
			short expectedLow, short expectedHigh, short[] out_zeros,
			short[] out_spans) throws IOException {
		if (aisLow.length != aisHigh.length) {
			throw new RuntimeException(
					"Cannot compute calibration from differing length arrays");
		}

		int count = aisLow.length;

		if (out_zeros == null || out_zeros.length != count) {
			throw new RuntimeException(
					"Provided zeros array is null or incorrect length");
		}
		if (out_spans == null || out_spans.length != count) {
			throw new RuntimeException(
					"Provided spans array is null or incorrect length");
		}

		MultiIOException mioe = new MultiIOException();

		double partial1 = expectedHigh - (expectedLow + 1.0);

		ui.displayMessage(this, UserInterface.M_WARNING, "  Verifying uncalibrated AI readings are within range for calibration");
		for (int i = 0; i < count; i++) {
			double partial2 = (aisHigh[i] - aisLow[i]);
			
			String msg = formatRangeMessage(" AI" , i, aisLow[i], expectedLow, TOLERANCE_UNCALIBRATED);
			if (Math.abs(aisLow[i] - expectedLow) > TOLERANCE_UNCALIBRATED) {
				mioe.add(new IOException(msg));
				ui.displayMessage(this, UserInterface.M_ERROR, "  " + "Low reading out of range failure! Cannot calibrate" + msg);
			}else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  " + "Low reading for" + msg);
			}
			msg = formatRangeMessage(" AI", i, aisHigh[i], expectedHigh, TOLERANCE_UNCALIBRATED);
			if (Math.abs(aisHigh[i] - expectedHigh) > TOLERANCE_UNCALIBRATED) {
				mioe.add(new IOException(msg));
				ui.displayMessage(this, UserInterface.M_ERROR, "  " + "High reading out of range failure! Cannot calibrate" + msg);
			}else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  " + "High reading for" + msg);
			}

			double chanspan = ((partial1 / partial2) - 1.0) * 65536.0;
			double partial3 = (partial1 / partial2) - 1.0;
			double chanzero = (32.0 * (826.0 - aisLow[i] - (aisLow[i] + 8191.0) //826.0 = 4.4mA = (.225V / 51.1 ohms * 1000 - 4) / 16 * 32767
					* partial3)) / 25.0;

			out_zeros[i] = (short) chanzero;
			out_spans[i] = (short) chanspan;
		}

		if (!mioe.isEmpty()) {
			throw mioe;
		}
	}
}

/*
 * Mod2488x.java
 *
 * Tests a 24880 or 24882 ET2 module.
 *
 * Jonathan Pearson
 * January 29, 2008
 *
 */

package com.sixnetio.Initializer.ModTest;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.Initializer.Tester;
import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.MultiIOException;
import com.sixnetio.util.UserInterface;
import com.sixnetio.util.Utils;

public class Mod2488x
    implements Tester
{
    // Note: We use UDP communications because we will likely need to reset the
    // station
    // numerous times, and that way we don't need to worry about re-establishing
    // the
    // connection each time

    // Constants
    // Try to keep every test module using a different set of IP addresses, just
    // in case we figure out
    // a way to run multiple modules from the same machine at the same time
    private static final String IP_CALBASE = "10.1.0.1";
    private static final String IP_TESTBASE = "10.1.0.2";

    // Station numbers
    private static final short S_CALBASE = 1; // Calibration, testing of DIs
    private static final short S_TESTBASE = 2; // Verification, testing of DOs
    private static final short S_ANALOGOUTS = 3; // Analog outputs to assist
    // with calibration
    private static final short S_OUTPUTS16_23 = 4; // Analog outputs to test
    // discrete inputs
    private static final short S_OUTPUTS8_15 = 5; // Analog outputs to test
    // discrete inputs
    private static final short S_OUTPUTS0_7 = 6; // Analog outputs to test
    // discrete inputs
    private static final short S_OUTPUTS24_31 = 7; // Analog outputs to test
    // isolated discrete input

    // Locations of the configuration files
    // Starting configurations
    /*
     * Changes from default setup: Ethernet: - Manual IP address (10.1.0.1 for
     * A, 10.1.0.2 for B) - Force ethernet pass-thru for A - Force two networks
     * for B RS-485: - SIXNET Universal and Modbus RTU/Binary Passthru, 9600 8N1
     * Discrete Options: - Do not use last 8 discrete channels as outputs for A
     * - Force DI- for A - Force DI+ for B Analog Input Options: - Feature 1 set
     * to "- Below 4ma" for first 8 inputs for A
     */
    private static final String DIR_CALBASE =
        "TestData" + File.separator + "2488x" + File.separator + "BaseA";
    private static final String DIR_TESTBASE =
        "TestData" + File.separator + "2488x" + File.separator + "BaseB";

    // Loaded into the calibration base
    // Set analog inputs 0 and 1 to 10-bit high speed mode
    private static final String DIR_10BIT =
        "TestData" + File.separator + "2488x" + File.separator + "10bit";

    // Set to use high speed counters on discrete inputs 0 and 1
    private static final String DIR_COUNTERS =
        "TestData" + File.separator + "2488x" + File.separator + "Counters";

    // Set to use high speed counters on discrete inputs 0 thru 3
    private static final String DIR_4COUNTERS =
        "TestData" + File.separator + "2488x" + File.separator + "4Counters";

    // Set to use high speed counters on discrete input 0
    private static final String DIR_ISO_CNTR_THRES =
        "TestData" + File.separator + "2488x" + File.separator + "IsoCntrThreshold";

    // Loaded into the verification base
    // Set to use high speed counters on discrete inputs 0 and 1
    private static final String DIR_ISOLATED =
        "TestData" + File.separator + "2488x" + File.separator + "Isolated";

    // Make sure there is a watchdog failure
    private static final String DIR_WATCHDOG_OFF =
        "TestData" + File.separator + "2488x" + File.separator + "WatchdogOff";


    // Which AI the watchdog output terminal is plugged into
    private static final short AI_WATCHDOG = 4; // The last one is 7 (0-based)

    // Which AI the POE output terminal is plugged into
    private static final short AI_POE = 5;

    // Base directory in a module where we will find the configuration files
    // We expect a slash on the end of this
    private static final String DIR_MODULECONFIG = "/module0/arc/";

    // Offsets for a couple of things
    private static final int OFFSET_AI_CALIBRATION = 0x100; // In the module
    // cals file
    private static final int OFFSET_AO_CALIBRATION = 0x180; // Also in the
    // module cals file

    // Lengths for the same things (so we can zero them as necessary)
    private static final int LENGTH_AI_CALIBRATION = 0x80;
    private static final int LENGTH_AO_CALIBRATION = 0x80;

    // Analog values (to avoid hard-coding them in multiple places)
    private static final short EXP_WATCHDOG = (short)3686; // Expected watchdog
    // input value; 3686
    // = 5.8mA = 23.2V
    // across 4000 Ohms
    private static final short EXP_POE = (short)4096; // Expected PoE input
    // value; 4096 = 6mA = .6V
    // across a 100 Ohm
    // resistor
    private static final short AO_4MA = (short)0; // To output 4mA from an AO
    // module 
    private static final short AO_5MA = (short)2047; // To output 5mA from an AO
    // module in Base 3
    private static final short AO_18MA = (short)28671; // To output 18mA from an
    // AO module in Base 3
    private static final short AO_20MA = (short)32767; // To output 20mA from an
    // AO module in Base 3
    private static final short AO_1_5V = (short)1720; // To output 1.50V from
    // AO modules in Base 4, Base 5 and Base 6
    private static final short AO_2_47V = (short)5050; // To output 2.47V from an
    // AO modules in Base 4, Base 5 and Base 6
    private static final short AO_4_7V = (short)12863; // To output 4.7V from an
    // AO modules in Base 4, Base 5 and Base 6
    private static final short AO_5_1V = (short)14255; // To output 5.1V from an
    // AO modules in Base 4, Base 5 and Base 6
    private static final short AO_8_5V = (short)26081; // To output 8.5V from an
    // AO modules in Base 4, Base 5 and Base 6
    private static final short AO_0V = (short)0; // To output 0V from
    // AO modules in Base 4, Base 5 and Base 6
    private static final short AO_10_6V = (short)32767; // To output 10.6V from an
    // AO modules in Base 4, Base 5 and Base 6    
    //private static final short AO_2V_ISO = (short)4400; // To output 2V from an
    // AO module in Base 3 to an
    // isolated input
    // channel
    //private static final short AO_3V_ISO = (short)18000; // To output 3V from an
    // AO module in Base 3 to an
    // isolated input
    // channel

    // Tolerance for analog values
    // If two analog values should be equal, values within this distance on
    // either side will be accepted
    private static final int TOLERANCE_ANALOG = 12;

    // Similar to AnalogTolerance, this is used to make sure consecutive scans
    // during an average are close enough
    // Since values are allowed to be off by 12 in either direction of the
    // target value, a difference of 24 covers
    // the situation where one scane sees a value 12 below and the next scan
    // sees 12 above, then we add a little
    // extra to avoid spurious errors
    private static final int TOLERANCE_AVERAGE = 25;

    // When testing for channel independence (all get different values), this is
    // the tolerance for each channel
    private static final int TOLERANCE_INDEPENDENCE = 25;

    // Used to check that the Watchdog input is close to what it should be
    private static final int TOLERANCE_WATCHDOG = 450;

    // Used to check that the PoE input is close to what it should be
    private static final int TOLERANCE_POE = 600;

    // Same as Analog/Average/Independence Tolerance, but these are used for
    // 10-bit high-speed
    // mode, which is wildly less accurate (6 fewer bits means +/- 64 for one
    // analog point)
    // Allowed to be 12 analog points off, so 768 makes sense (12 * 64)
    private static final int TOLERANCE_HIGHSPEED_ANALOG = 768;

    // 768 below, then 768 above means 2 * 768 range across consecutive values
    private static final int TOLERANCE_HIGHSPEED_AVERAGE = 1536;

    // IndependenceTolerance is equal to AverageTolerance, so same here
    private static final int TOLERANCE_HIGHSPEED_INDEPENDENCE = 1536;

    // These are all of the test names that may be jumped to
    // They are in the order in which they would be run in a complete test
    private static final String T_DI = "DI Test", T_COUNTER = "Counter Test",
        T_AI_CAL = "AI Calibration", T_AI = "AI Test", T_ISO_INPUT_THRES = "Isolated Input Threshold Test",
        T_DI_SOURCE = "DI Sourcing Test", T_DO = "DO Test",
        T_AO_CAL = "AO Calibration", T_AO = "AO Test", T_POE = "PoE Test",
        T_WATCHDOG = "Watchdog Test", T_ISOLATED = "Isolated Counter Test";

    private static final String[] TEST_NAMES = {
        // Base 1
        T_DI, T_COUNTER, T_AI_CAL, T_AI, T_ISO_INPUT_THRES,

        // Base 2
        T_DI_SOURCE, T_DO, T_AO_CAL, T_AO, T_POE, T_WATCHDOG, T_ISOLATED
    };

    // Private data members
    private String chosenTest;
    private UserInterface ui;
    private String devName;
    private UDRLink activeHandler;
    private boolean initialized = false;
    private boolean runVerifyTest = false;
    private UDRLib udr;
    private TestLib testLib;
    private int modRev;
    private short AO_offVal = AO_5_1V;
    private short AO_onVal = AO_8_5V;

    // Constructor
    public Mod2488x()
    {
    }

    // Required by Tester
    // public void setupTest(UserInterface ui, String comPort, String devName)
    // throws IOException
    public void setupTest(UserInterface ui, String comPort, String devName)
        throws IOException
    {
        if (ui == null) {
            throw new IllegalArgumentException(
                "The UserInterface passed to setupTest for Mod2488x must not be null");
        }

        // Make sure the device name is recognized
        // Must start with E2, EB or 10 (REM)
        // Must end with 24880, 24882 or 7612 (REM)
        if (!((devName.startsWith("E2") || devName.startsWith("EB") || devName
            .startsWith("10")) && (devName.endsWith("24880") ||
                                   devName.endsWith("24882") || devName
            .endsWith("7612")))) {
            throw new IOException("Unrecognized device name: " + devName);
        }

        this.ui = ui;
        // These tests run over IP, no need for the COM port
        this.devName = devName; // Need to know which 88x module it is

        udr = new UDRLib((short)100, (byte)0);
        testLib = new TestLib(udr, ui);

        if (initialized) {
            return; // That's it on the changes that may take place between
            // calls
        }

        initialized = true;
    }

    // public boolean supportsTestJump()
    public boolean supportsTestJump()
    {
        return true;
    }
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
    // public List<String> getTests()
    public List<String> getTests()
    {
        return Utils.makeVector(TEST_NAMES);
    }

    // public void jumpToTest(String testName)
    public void jumpToTest(String testName)
    {
        if (testName != null) {
            List<String> tests = getTests();
            if (!tests.contains(testName)) {
                throw new IllegalArgumentException(
                    "Test name not recognized: " + testName);
            }
        }

        chosenTest = testName;
    }

    // public String getJumpTest()
    public String getJumpTest()
    {
        return chosenTest;
    }

    // public void run()
    public void run()
    {
        if (chosen(T_DI) || chosen(T_COUNTER) || chosen(T_AI_CAL) ||
                chosen(T_AI) || chosen(T_ISO_INPUT_THRES)) {
            	
                // Make sure the module is in the first base
          	if (!ui.confirm("Please plug the module into the first base",
                "Module Check")) {
                ui.displayMessage(this, UserInterface.M_ERROR,
                    "User canceled the test");
                ui.operationFailed(this);
                return;
            }
            // Register a UDP handler for test base A
            try {
                activeHandler = testLib.registerUDPHandler(IP_CALBASE);

                ui.displayMessage(this, UserInterface.M_WARNING,
                    "Waiting for device to respond");
                udr.waitForResponse(S_CALBASE);
            }
            catch (Exception e) {
                ui.handleException(this, e);
                ui.operationFailed(this);
                return;
            }
            try { // Finally block unregisters activeHandler
                try {
                    // Load the default configuration to the base so we know
                    // what we're working with
                    testLib.loadBaseFiles(S_CALBASE, DIR_CALBASE);
                }
                catch (Exception e) {
                    ui.handleException(this, e);
                    ui.operationFailed(this);
                    return;
                }

                try {
                    MultiIOException mioe = new MultiIOException();
                    byte[] rev =
                        udr.readFile(S_CALBASE, "/module0/arc/factory", 22, 2,
                            null);
                    String strValue = Conversion.bytesToHex(rev, "");
                    modRev = Integer.parseInt(strValue);
                    ui.displayMessage(this, UserInterface.M_NORMAL, "Module Revision = 0" + modRev);

                    // Walk the AOs of an external device across the DIs of the
                    // device under test
                    try {
                        if (chosen(T_DI)) {
                            walkDIs();
                        }
                    }
                    catch (IOException ioe) {
                        // This one does not print its own error messages, so
                        // summarize for the user
                        ui.displayMessage(this, UserInterface.M_ERROR, ioe
                            .getMessage());
                        mioe.add(ioe);
                    }

                    // Toggle our inputs a few times to make sure the counters
                    // activate properly
                    try {
                        if (chosen(T_COUNTER)) {
                            checkCounters();
                        }
                    }
                    catch (IOException ioe) {
                        // This one does not print its own error messages, so
                        // summarize for the user
                        ui.displayMessage(this, UserInterface.M_ERROR, ioe
                            .getMessage());
                        mioe.add(ioe);
                    }

                    // Calibrate the AIs of the DOT with the AOs of an external
                    // device
                    try {
                        if (chosen(T_AI_CAL)&&(!runVerifyTest)) {
                            calibrateAIs();
                        }
                        if (chosen(T_AI)) {
                            testAIs(); // If the calibration fails, don't do the
                            // test
                        }
                    }
                    catch (IOException ioe) {
                        mioe.add(ioe);
                    }
                    
                    // Confirm the the isolated input turns on and off at the 
                    // right voltage level
                    try {
                        if (chosen(T_ISO_INPUT_THRES)) {
                            checkIsolatedThreshold();
                        }
                    }
                    catch (IOException ioe) {
                        mioe.add(ioe);
                    }


                    
                    if (!mioe.isEmpty()) {
                        throw mioe;
                    }
                }
                catch (Exception e) {
                    ui.handleException(this, e);
                    ui.operationFailed(this);
                    return;
                }
            }
	        finally {
	            // Disconnect the handler
	            activeHandler = testLib.unregisterHandler(activeHandler);
	        }
        }


        if (chosen(T_DI_SOURCE) || chosen(T_DO) || chosen(T_AO_CAL) ||
            chosen(T_AO) || chosen(T_POE) || chosen(T_WATCHDOG) ||
            chosen(T_ISOLATED)) {
            // Tell the user to move the module to the second base
            while (!ui.confirm("Please move the module to the second base",
                "Move the Module")) {
                if (ui.confirm("Really cancel the test?", "Cancel")) {
                    ui.displayMessage(this, UserInterface.M_ERROR,
                        "User canceled the test");
                    ui.operationFailed(this);
                    return;
                }
            }

            // Connect a new handler
            try {
                activeHandler = testLib.registerUDPHandler(IP_TESTBASE);
            }
            catch (IOException ioe) {
                ui.handleException(this, ioe);
                ui.operationFailed(this);
                return;
            }

            try { // Finally block unregisters the handler
                try {
                    // Load the default configuration to the base so we know
                    // what we're working with
                    testLib.loadBaseFiles(S_TESTBASE, DIR_TESTBASE);
                }
                catch (Exception e) {
                    ui.handleException(this, e);
                    ui.operationFailed(this);
                    return;
                }

                try {
                    MultiIOException mioe = new MultiIOException();

                    // Make sure that the sourced outputs (tied to ground) are
                    // all on
                    try {
                        if (chosen(T_DI_SOURCE)) {
                            testSourcing();
                        }
                    }
                    catch (IOException ioe) {
                        // This one does not print its own error messages, so
                        // summarize for the user
                        ui.displayMessage(this, UserInterface.M_ERROR, ioe
                            .getMessage());
                        mioe.add(ioe);
                    }

                    // Walk our own DOs, which feed into our DIs to test the DOs
                    try {
                        if (chosen(T_DO)) {
                            walkDOs();
                        }
                    }
                    catch (IOException ioe) {
                        // This one does not print its own error messages, so
                        // summarize for the user
                        ui.displayMessage(this, UserInterface.M_ERROR, ioe
                            .getMessage());
                        mioe.add(ioe);
                    }

                    // Calibrate AOs from our newly-calibrated AIs
                    try {
                        if (chosen(T_AO_CAL)&&(!runVerifyTest)) {
                            calibrateAOs();
                        }
                        if (chosen(T_AO)) {
                            testAOs(); // If the calibration fails, don't do the
                            // test
                        }
                    }
                    catch (IOException ioe) {
                        mioe.add(ioe);
                    }

                    // Check the PoE input to make sure the base has it (if it's
                    // an EB) and that it doesn't have it (if it's an E2)
                    try {
                        if (chosen(T_POE)) {
                            checkPOE();
                        }
                    }
                    catch (IOException ioe) {
                        mioe.add(ioe);
                    }

                    // Check the Watchdog input
                    try {
                        if (chosen(T_WATCHDOG)) {
                            checkWatchdog();
                        }
                    }
                    catch (IOException ioe) {
                        mioe.add(ioe);
                    }

                    // Check the isolated counter
                    try {
                        if (chosen(T_ISOLATED)) {
                            checkIsolated();
                        }
                    }
                    catch (IOException ioe) {
                        mioe.add(ioe);
                    }

                    if (!mioe.isEmpty()) {
                        throw mioe;
                    }
                }
                catch (Exception e) {
                    ui.handleException(this, e);
                    ui.operationFailed(this);
                    return;
                }
            }
            finally {
                // Disconnect the handler
                activeHandler = testLib.unregisterHandler(activeHandler);
            }
        }

        if (chosenTest == null) {
            // null is all tests
            // write the pass flag
            if (writePassedFlag(true)){
                ui.displayMessage(this, UserInterface.M_NORMAL,
                "Module tests passed");
            }
            else{
                ui.handleException(this, new IOException("Failed to write the All Tests Passed flag to module."));
                ui.operationFailed(this);
                return;
            }
        }
        else {
            ui.displayMessage(this, UserInterface.M_NORMAL, "Module test '" +
                                                            chosenTest +
                                                            "' passed");
        }

        ui.operationCompleted(this);
    }

    private boolean writePassedFlag(boolean pass)
//        throws IOException, TimeoutException
    {
        boolean ret = false;
        byte[] flagbyte = new byte[1]; 
        flagbyte[0] = 0; // must be 0 if failed
        if(pass) flagbyte[0] = 1; // must be 1 if pass
        try {
            activeHandler = testLib.registerUDPHandler(IP_TESTBASE);
            udr.writeFile(S_TESTBASE, "/module0/arc/factory", flagbyte, 0x4B, null);
            // Disconnect the handler
            activeHandler = testLib.unregisterHandler(activeHandler);
            ret = true;
        }
        catch (IOException e) {
            ret = false;
        }
        catch (TimeoutException e) {
            ret = false;
        }
    
        return ret;
    }
    
    // Testing functions
    // private void walkDIs() throws IOException, TimeoutException
    private void walkDIs()
        throws IOException, TimeoutException
    {
        ui.displayMessage(this, UserInterface.M_WARNING, "Testing DIs");

        // Different DI ON/OFF values for REM (10-7612), and DI OFF value for modRev 101
        if (modRev == 101) {
            AO_offVal = AO_4_7V;
        }else AO_offVal = AO_5_1V;
        
        if (devName.endsWith("7612")) {
            AO_onVal = AO_2_47V;
            AO_offVal = AO_1_5V;
        }
        // Zero all outputs
        short[] zero = new short[8];
        Arrays.fill(zero, AO_offVal);

        udr.putA(S_OUTPUTS16_23, (short)0, (short)8, zero);
        udr.putA(S_OUTPUTS8_15, (short)0, (short)8, zero);
        udr.putA(S_OUTPUTS0_7, (short)0, (short)8, zero);

        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Please watch for 24 walking LEDs");

        // Give the user time to read the message and move his eyes to the
        // module
        Utils.sleep(2500);

        // Walk the outputs
        boolean done = false;
        while (!done) {
            // There are 24 DIs, with input coming from AOs on fixed stations B,
            // C, and D
            // First 8 come from D
            // Test for REM 10-7612 (Channels 0-3, Guaranteed on @ 2.5V,
            // Guaranteed off @ 1.5V)
            if (devName.endsWith("7612")) {
                ui.displayMessage(this, UserInterface.M_NORMAL,
                    "  REM DIs 0-3 Guaranteed ON(2.5V)/OFF(1.5V)");
                REMwalkAOs(S_OUTPUTS0_7, (short)4, S_CALBASE, (short)0,
                    (short)24, AO_offVal, AO_onVal);
                ui.displayMessage(this, UserInterface.M_NORMAL,
                    "  REM DIs 0-3 tests complete");
                REMwalkAOs(S_OUTPUTS0_7, (short)4, S_CALBASE, (short)4,
                    (short)24, AO_5_1V, AO_8_5V);
            }
            else {
                walkAOs(S_OUTPUTS0_7, (short)8, S_CALBASE, (short)0, (short)24,
                    AO_offVal, AO_8_5V);
            }
            // Next 8 come from C
            walkAOs(S_OUTPUTS8_15, (short)8, S_CALBASE, (short)8, (short)24,
                AO_5_1V, AO_8_5V);

            // Last 8 come from B
            walkAOs(S_OUTPUTS16_23, (short)8, S_CALBASE, (short)16, (short)24,
                AO_5_1V, AO_8_5V);

            if (ui.confirm(
                "Did you watch for walking DI LEDs (there were 24)?",
                "Walking LEDs")) {
                if (!ui.confirm("Did they all light?", "Walking LEDs")) {
                    throw new IOException("Missing DI LEDs during walk");
                }
                else {
                    done = true;
                }
            }
            else {
                Utils.sleep(1500); // Give the user a change to look back over
                // there
                done = false;
            }
        }

        ui.displayMessage(this, UserInterface.M_NORMAL, "  DI tests complete");
    }

    // private void checkCounters() throws IOException, TimeoutException
    private void checkCounters()
        throws IOException, TimeoutException
    {
        // The 24880 module revision 0101 has ONE high-speed counter register
        // that we can test
        // here (cnt2)
        // The 24880 module revision 0102 has THREE high-speed counter register
        // that we can test
        // here (cnt2, cnt3, and cnt4)
        // The 24882 module revision 0101 has TWO high-speed counter registers
        // (cnt1 and cnt2)
        // The 24882 module and (REM)10-7612 module revision 0102 has FOUR
        // high-speed counter registers
        // (cnt1, cnt2, cnt3, and cnt4)
        
        String BASE_FILE;
        int ao_count;
        int counter1 = 3;
        int counter2 = 7;
        int counter3 = 3;
        int counter4 = 7;
        
        // Initialize these for modRev 101
        BASE_FILE = DIR_COUNTERS;
        ao_count = 2;        

        // Initialize these for modRev 102 or higher
        if (modRev > 101) {
            BASE_FILE = DIR_4COUNTERS;
            ao_count = 4;
        }
        // Different DI ON/OFF values for REM (10-7612), and DI OFF value for modRev 101 
//        if (modRev == 101) {
//            AO_offVal = AO_4_7V;
//        }else AO_offVal = AO_5_1V;
        
//        if (devName.endsWith("7612")) {
//            AO_onVal = AO_10_6V;
//            AO_offVal = AO_0V;
//        }
          AO_onVal = AO_10_6V;
          AO_offVal = AO_0V;
         // Turn off the outputs feeding the counters
        short[] aos = new short[ao_count];
        Arrays.fill(aos, AO_offVal);
        udr.putA(S_OUTPUTS0_7, (short)0, (short)aos.length, aos);

        Utils.sleep(1000); // Make sure the off value takes effect

        // Turn on counters
        if (!testLib.loadBaseFiles(S_CALBASE, BASE_FILE)) {
            // Make sure the counters are zeroed
            testLib.resetStation(S_CALBASE);
        }

        ui.displayMessage(this, UserInterface.M_WARNING,
            "Testing counter registers");
        ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  Testing %d High Speed Counters at DI Channels 1 - %d. ",
                    ao_count,ao_count));

        // Toggle DIs 1, 2, 3, and 4 a few times each (different number for
        // adjacent channels to
        // make sure there is no cross-talk)
        // DIs 1, 2, 3, and 4 are connected to fixed base D AOs 1, 2, 3, and 4

        int i = 0;
        while (i < counter1 || i < counter2) {
            // Turn them off
            aos[0] = aos[1] = AO_offVal;
            if (modRev > 101) {
                aos[2] = aos[3] = AO_offVal;
            }
            udr.putA(S_OUTPUTS0_7, (short)0, (short)aos.length, aos);

            // Wait a moment for it to register
            Utils.sleep(1000);

            // Turn them on
            if (i < counter1) {
                aos[0] = AO_onVal;
            }

            if (i < counter2) {
                aos[1] = AO_onVal;
            }
            if (modRev > 101) {
                if (i < counter3) {
                    aos[2] = AO_onVal;
                }

                if (i < counter4) {
                    aos[3] = AO_onVal;
                }
            }

            udr.putA(S_OUTPUTS0_7, (short)0, (short)aos.length, aos);

            // And wait another moment...
            Utils.sleep(1000);

            // Increment i and loop around
            i++;
        }

        // Grab analog inputs 9 thru 12, they hold the counts
        short[] ais = udr.getA(S_CALBASE, (short)8, (short)aos.length);

        // For modRev 0101
        // If this is a 24880, only check AI10; if its a 24882, check both AI9
        // and AI10
        if (devName.endsWith("24880")) {// For 24880 look at AI10
            if (ais[1] != counter2) {
                throw new IOException(String
                    .format("Expected cnt2 = %d, but found cnt2 = %d",
                        counter2, ais[1]));
            }
            if (modRev > 101) {// For 24880 modRev 0102 also look at AI11 and
                // AI12
                if (ais[2] != counter3 || ais[3] != counter4) {
                    throw new IOException(
                        String
                            .format(
                                "Expected cnt3 = %d and cnt4 = %d, but found cnt3 = %d and cnt4 = %d",
                                counter3, counter4, ais[2], ais[3]));
                }
            }
        }
        else {// For 24882 look at AI9 and AI10
            if (ais[0] != counter1 || ais[1] != counter2) {
                throw new IOException(
                    String
                        .format(
                            "Expected cnt1 = %d and cnt2 = %d, but found cnt1 = %d and cnt2 = %d",
                            counter1, counter2, ais[0], ais[1]));
            }
            if (modRev > 101) {// For 24882 modRev 0102 also look at AI11 and
                // AI12
                if (ais[2] != counter3 || ais[3] != counter4) {
                    throw new IOException(
                        String
                            .format(
                                "Expected cnt3 = %d and cnt4 = %d, but found cnt3 = %d and cnt4 = %d",
                                counter3, counter4, ais[2], ais[3]));
                }
            }
        }
        // Turn off the outputs feeding the counters
        aos = new short[ao_count];
        Arrays.fill(aos, AO_offVal);
        udr.putA(S_OUTPUTS0_7, (short)0, (short)aos.length, aos);        

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  Counter tests complete");
    }

    // private void calibrateAIs() throws IOException, TimeoutException
    private void calibrateAIs()
        throws IOException, TimeoutException
    {
        MultiIOException mioe = new MultiIOException();

        ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating AIs");

        // Clear any calibration data that may exist
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Clearing current calibration");

        if (testLib.zeroRange(S_CALBASE, DIR_MODULECONFIG + "cals",
            OFFSET_AI_CALIBRATION, LENGTH_AI_CALIBRATION)) {
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Resetting station");
            testLib.resetStation(S_CALBASE);
        }

        ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");

        // There are 8 AIs coming from AOs on fixed station A
        short[] aos = new short[8];

        // Set the scan delay
        // setup file sets 100ms per active channel, we add a little to be
        // careful
        testLib.setScanDelay(100 * aos.length + 200);

        // Calibration results will end up in these arrays
        short[] spans = new short[aos.length];
        short[] zeros = new short[aos.length];

        // Write 5mA to all input channels
        Arrays.fill(aos, AO_5MA);

        udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);

        // Need to wait for the RM and for the E2
        // let the analog out setpoint settle in.  we need it to be stable before using it for cal
        Utils.sleep(3000);
        // give the module time scan all the channels with the now stable input
        Utils.sleep(testLib.getScanDelay()); 

        // Grab the results
        short[] aisLow =
            testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aos.length,
                TOLERANCE_AVERAGE);

        // Write 18mA to all input channels
        Arrays.fill(aos, AO_18MA);
        udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);

        // Need to wait for the RM and for the E2
        // let the analog out setpoint settle in.  we need it to be stable before using it for cal
        Utils.sleep(3000);
        // give the module time scan all the channels with the now stable input
        Utils.sleep(testLib.getScanDelay()); 

        // Grab the results
        short[] aisHigh =
            testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aos.length,
                TOLERANCE_AVERAGE);

        // Compute the module span and zero
        testLib.computeAICurrentCalibration(aisLow, aisHigh, AO_5MA, AO_18MA,
            zeros, spans);

        // Write the corrections to the config file
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Writing calibration data to station");

        byte[] calsData = new byte[aos.length * 4]; // 2 values of 2 bytes
        // apiece for each input
        int offset = 0;
        for (int i = 0; i < aos.length; i++) {
            Conversion.shortToBytes(calsData, offset, zeros[i]);
            offset += 2;

            Conversion.shortToBytes(calsData, offset, spans[i]);
            offset += 2;
        }

        udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", calsData,
            OFFSET_AI_CALIBRATION, null);

        ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");

        // Reset the station
        testLib.resetStation(S_CALBASE);

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  AI calibration calculations complete and written to unit");

        // Set all channels low/high and check that they are each where they
        // should be
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Testing all AIs at once");
        {
            aos = new short[8];
            testLib.setScanDelay(100 * aos.length + 200);

            // Write 5mA to all channels
            {
                Arrays.fill(aos, AO_5MA);
                udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);
                Utils.sleep(testLib.getScanDelay() * 2);

                short[] ais =
                    testLib.getAverageAnalogs(S_CALBASE, (short)0,
                        (short)aos.length, TOLERANCE_AVERAGE);

                // Compare
                for (int i = 0; i < ais.length; i++) {
                    String msg =
                        formatRangeMessage("AI", i, ais[i], aos[i],
                            TOLERANCE_ANALOG);
                    if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
                        mioe.add(new IOException(msg));
                        ui.displayMessage(this, UserInterface.M_ERROR, "  " +
                                                                       msg);
                    }
                    else {
                        ui.displayMessage(this, UserInterface.M_NORMAL, "  " +
                                                                        msg);
                    }
                }
            }

            // Write 18mA to all channels
            {
                Arrays.fill(aos, AO_18MA);
                udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);
                Utils.sleep(testLib.getScanDelay() * 2);

                short[] ais =
                    testLib.getAverageAnalogs(S_CALBASE, (short)0,
                        (short)aos.length, TOLERANCE_AVERAGE);

                // Compare
                for (int i = 0; i < ais.length; i++) {
                    String msg =
                        formatRangeMessage("AI", i, ais[i], aos[i],
                            TOLERANCE_ANALOG);
                    if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
                        mioe.add(new IOException(msg));
                        ui.displayMessage(this, UserInterface.M_ERROR, "  " +
                                                                       msg);
                    }
                    else {
                        ui.displayMessage(this, UserInterface.M_NORMAL, "  " +
                                                                        msg);
                    }
                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }
    }

    // private void testAIs() throws IOException, TimeoutException
    private void testAIs()
        throws IOException, TimeoutException
    {
        MultiIOException mioe = new MultiIOException();

        ui.displayMessage(this, UserInterface.M_WARNING,
            "Testing AIs in 16-bit integrating mode");

        // Test independence of channels
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Testing channel independence");

        {
            short[] aos = new short[8];
            // Changed to check channel independence around 18.000mA calibration point.
            // The previous method stepped through full range (5.000mA to 18.000mA) in 1.625mA (3328 count)increments
            // resulted in frequent errors with the middle AI channels.  This is
            // due to non-linearity of the AOs on the test modules used to source the AIs.    
            short AOStep = 27031; //Start AOStep at 17.200mA (27031)and increment for each channel by .2mA (410)
            for (int i = 0; i < aos.length; i++) {
            	aos[i] = AOStep;
            	AOStep = (short) (AOStep + 410);
            }
            udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);
            Utils.sleep(testLib.getScanDelay() * 2);

            short[] ais =
                testLib.getAverageAnalogs(S_CALBASE, (short)0,
                    (short)aos.length, TOLERANCE_AVERAGE);

            // Compare
            for (int i = 0; i < aos.length; i++) {
                // A small amount of influence is allowed; a difference of 1mA
                // is equal to an analog difference of about 2048, so as long
                // as IndependenceTolerance is smaller than that, all is well
                String msg =
                    formatRangeMessage("AI", i, ais[i], aos[i],
                        TOLERANCE_INDEPENDENCE);
                if (Math.abs(ais[i] - aos[i]) > TOLERANCE_INDEPENDENCE) {
                    mioe.add(new IOException(msg));
                    ui
                        .displayMessage(this, UserInterface.M_ERROR, "    " +
                                                                     msg);
                }
                else {
                    ui.displayMessage(this, UserInterface.M_NORMAL, "    " +
                                                                    msg);
                }
            }
        }

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  16-bit AI tests complete");

        // Switch to 10-bit mode and test the calibration again on individual
        // channels
        testLib.loadBaseFiles(S_CALBASE, DIR_10BIT);

        ui.displayMessage(this, UserInterface.M_WARNING,
            "Testing AIs in 10-bit high-speed mode");

        // Special case: There is a bug on the board that we are allowing
        // through, but it
        // is likely to cause failures here, so we need to allow the user to
        // retry
        MultiIOException mioe2;

        do {
            mioe2 = new MultiIOException();

            // Set all channels low/high and check that they are each where they
            // should be
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Testing all AIs at once");
            {
                short[] aos = new short[2];

                // Leave the scan delay as it is, since there are still 6 other
                // inputs
                // These inputs are supposed to be high speed, but the outputs
                // feeding them may not be

                // Write 5mA to all channels
                {
                    Arrays.fill(aos, AO_5MA);
                    udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);
                    Utils.sleep(testLib.getScanDelay() * 2);

                    short[] ais =
                        testLib.getAverageAnalogs(S_CALBASE, (short)0,
                            (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);

                    // Compare
                    for (int i = 0; i < ais.length; i++) {
                        String msg =
                            formatRangeMessage("AI", i, ais[i], aos[i],
                                TOLERANCE_HIGHSPEED_ANALOG);
                        if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
                            mioe2.add(new IOException(msg));
                            ui.displayMessage(this, UserInterface.M_ERROR,
                                "  " + msg);
                        }
                        else {
                            ui.displayMessage(this, UserInterface.M_NORMAL,
                                "  " + msg);
                        }
                    }
                }

                // Write 18mA to all channels
                {
                    Arrays.fill(aos, AO_18MA);
                    udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);
                    Utils.sleep(testLib.getScanDelay() * 2);

                    short[] ais =
                        testLib.getAverageAnalogs(S_CALBASE, (short)0,
                            (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);

                    // Compare
                    for (int i = 0; i < ais.length; i++) {
                        String msg =
                            formatRangeMessage("AI", i, ais[i], aos[i],
                                TOLERANCE_HIGHSPEED_ANALOG);
                        if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
                            mioe2.add(new IOException(msg));
                            ui.displayMessage(this, UserInterface.M_ERROR,
                                "  " + msg);
                        }
                        else {
                            ui.displayMessage(this, UserInterface.M_NORMAL,
                                "  " + msg);
                        }
                    }
                }
            }

            // Test for channel independence
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Testing channel independence");
            {
                short[] aos = new short[2];
                for (int i = 0; i < aos.length; i++) {
                    aos[i] =
                        (short)((AO_18MA - AO_5MA) * i / aos.length + AO_5MA);
                }
                udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);
                Utils.sleep(testLib.getScanDelay() * 2);

                // Get the average value
                short[] ais =
                    testLib.getAverageAnalogs(S_CALBASE, (short)0,
                        (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);

                // Compare
                for (int i = 0; i < aos.length; i++) {
                    // A small amount of influence is allowed; a difference of
                    // 1mA
                    // is equal to an analog difference of about 2048, so as
                    // long
                    // as IndependenceTolerance is smaller than that, all is
                    // well
                    String msg =
                        formatRangeMessage("AI", i + 1, ais[i], aos[i],
                            TOLERANCE_HIGHSPEED_INDEPENDENCE);
                    if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_INDEPENDENCE) {
                        mioe2.add(new IOException(msg));
                        ui.displayMessage(this, UserInterface.M_ERROR, "    " +
                                                                       msg);
                    }
                    else {
                        ui.displayMessage(this, UserInterface.M_NORMAL, "    " +
                                                                        msg);
                    }
                }
            }
        } while (!mioe2.isEmpty() &&
                 ui.confirm("High-speed analog input test failed, retry?",
                     "Retry 10-bit AI Test"));

        if (!mioe2.isEmpty()) {
            mioe.add(mioe2);
        }

        if (!ui.confirm("Are all of the AI LEDs lit?", "AI LEDs")) {
            mioe.add(new IOException("Not all AI LEDs light"));
        }

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  10-bit AI tests complete");

        if (!mioe.isEmpty()) {
            throw mioe;
        }
    }

    // private void testSourcing() throws IOException, TimeoutException
    private void testSourcing()
        throws IOException, TimeoutException
    {
        MultiIOException mioe = new MultiIOException();

        ui
            .displayMessage(this, UserInterface.M_WARNING,
                "Testing Sourcing DIs");

        // Grab the first 8 inputs (the sourcing ones)
        // They are all tied to ground, so they should all be on
        boolean[] dis = udr.getD(S_TESTBASE, (short)0, (short)8);

        // Check them
        for (int i = 0; i < dis.length; i++) {
            if (!dis[i]) {
                mioe
                    .add(new IOException("Sourcing DI " + i +
                                         " (0-based) should be on but it is off"));
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  Sourcing DI tests complete");
    }

    // private void walkDOs() throws IOException, TimeoutException
    private void walkDOs()
        throws IOException, TimeoutException
    {
        MultiIOException mioe = new MultiIOException();

        ui.displayMessage(this, UserInterface.M_WARNING, "Testing DOs");

        // There are 8 DOs, with input coming from our own outputs
        boolean[] dos = new boolean[8];

        // Start out by setting everything off
        Arrays.fill(dos, false);
        udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);

        // Loop through, turning each on and then off
        for (int i = 0; i <= dos.length; i++) {
            // Turn off the previous bit
            if (i > 0) {
                dos[i - 1] = false;
            }

            // Turn on the current bit
            if (i < dos.length) {
                dos[i] = true;
            }

            udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);

            // Wait for the electronics to do their thing and for the user to
            // see it
            Utils.sleep(1000);

            // Read the DIs to see if they match
            // DOs 0-7 are plugged into DIs 8-15 (they will also show up in DIs
            // 16-23, since those double as DOs 0-7)
            boolean[] dis = udr.getD(S_TESTBASE, (short)8, (short)dos.length);

            for (int j = 0; j < dos.length; j++) {
                if (dis[j] != dos[j]) {
                    if (dis[j]) {
                        mioe.add(new IOException(
                            "Station reports DI " + (j + 8) +
                                " (0-based) is on when corresponding DO " + i +
                                " is off"));
                    }
                    else {
                        mioe.add(new IOException(
                            "Station reports DI " + (j + 8) +
                                " (0-based) is off when corresponding DO " + i +
                                " is on"));
                    }
                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  Finished testing DOs");
    }

    // private void calibrateAOs() throws IOException, TimeoutException
    private void calibrateAOs()
        throws IOException, TimeoutException
    {
        // If this is a 24882, there are 2 analog outputs
        // Otherwise, assume there are no analog outputs
        if (!devName.endsWith("24882")&& !devName.endsWith("7612")) {
            ui.displayMessage(this, UserInterface.M_NORMAL,
                "There are no AOs to calibrate on this module");
            return;
        }

        final short aoCount = 2;

        ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating AOs");

        // Clear any calibration data that may exist
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Clearing current calibration");

        if (testLib.zeroRange(S_TESTBASE, DIR_MODULECONFIG + "cals",
            OFFSET_AO_CALIBRATION, LENGTH_AO_CALIBRATION)) {
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Resetting station");
            testLib.resetStation(S_TESTBASE);
        }

        ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");

        // There are 2 AOs feeding our first 2 AIs
        short[] aos = new short[aoCount];

        // Set the scan delay
        // set to 100ms per active channel, we add a little to be
        // careful
        // We are using 8 channels for the scan delay because there are 8 AIs,
        // even though there
        // are only 2 AOs
        testLib.setScanDelay(100 * 8 + 200);


        Arrays.fill(aos, AO_4MA);
        udr.putA(S_TESTBASE, (short)0, aoCount, aos);

        // Sleep for a bit so it can stabilize
        Utils.sleep(testLib.getScanDelay() * 2);

        // Read back what it is actually outputting
        short[] aisLow =
            testLib.getAverageAnalogs(S_TESTBASE, (short)0, aoCount,
                TOLERANCE_AVERAGE);

        // Write 18mA to all input channels
        Arrays.fill(aos, AO_18MA);
        udr.putA(S_TESTBASE, (short)0, aoCount, aos);

        // Wait for it to take effect
        Utils.sleep(testLib.getScanDelay() * 2);

        // Grab the results
        short[] aisHigh =
            testLib.getAverageAnalogs(S_TESTBASE, (short)0, aoCount,
                TOLERANCE_AVERAGE);

        // Compute the module span and zero
        short[] zeros = new short[aoCount];
        short[] spans = new short[aoCount];

        testLib.computeAOCurrentCalibration(aisLow, aisHigh, zeros, spans);

        // Write the corrections to the config file
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Writing calibration data to station");

        byte[] calsData = new byte[aoCount * 4]; // 2 values of 2 bytes apiece
        // for each input
        int offset = 0;
        for (int i = 0; i < aos.length; i++) {
            Conversion.shortToBytes(calsData, offset, zeros[i]);
            offset += 2;

            Conversion.shortToBytes(calsData, offset, spans[i]);
            offset += 2;
        }

        udr.writeFile(S_TESTBASE, DIR_MODULECONFIG + "cals", calsData,
            OFFSET_AO_CALIBRATION, null);

        ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");

        // Reset the station
        testLib.resetStation(S_TESTBASE);

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  AO calibration complete");
    }

    // private void testAOs() throws IOException, TimeoutException
    private void testAOs()
        throws IOException, TimeoutException
    {
        // If this is a 24882 or 10-7612, there are 2 analog outputs
        // Otherwise, assume there are no analog outputs
        if (!devName.endsWith("24882")&& !devName.endsWith("7612")) {
            ui.displayMessage(this, UserInterface.M_NORMAL,
                "There are no AOs to test on this module");
            return;
        }

        MultiIOException mioe = new MultiIOException();

        ui.displayMessage(this, UserInterface.M_WARNING, "Testing AOs");

        // Test channels individually
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Testing channels individually");
        // Write 4mA to each channel individually, with the rest set to 18mA
        short[] aos = new short[2];

        // We are using 8 channels for the scan delay because there are 8 AIs,
        // even though there
        // are only 2 AOs
        testLib.setScanDelay(100 * 8 + 200);

        Arrays.fill(aos, AO_18MA);

        for (int i = 0; i < aos.length; i++) {
            // Verify low
            // Set the previous channel to 18mA
            if (i > 0) {
                aos[i - 1] = AO_18MA;
            }

            // Set this channel to 5mA
            aos[i] = AO_5MA;

            // Write it to the station
            udr.putA(S_TESTBASE, (short)0, (short)aos.length, aos);

            // Sleep so it can stabilize
            Utils.sleep(testLib.getScanDelay() * 2);

            // Get the average values
            short[] ais =
                testLib.getAverageAnalogs(S_TESTBASE, (short)0,
                    (short)aos.length, TOLERANCE_AVERAGE);

            // Compare
            String msg =
                formatRangeMessage("AO", i + 1, ais[i], aos[i],
                    TOLERANCE_ANALOG);
            if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
                mioe.add(new IOException(msg));
                ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
            }
            else {
                ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
            }

            // Verify high
            // Set this channel to 18mA
            aos[i] = AO_18MA;

            // Write it to the station
            udr.putA(S_TESTBASE, (short)0, (short)aos.length, aos);

            // Sleep so it can stabilize
            Utils.sleep(testLib.getScanDelay() * 2);

            // Get the average value
            ais =
                testLib.getAverageAnalogs(S_TESTBASE, (short)0,
                    (short)aos.length, TOLERANCE_AVERAGE);

            // Compare
            msg =
                formatRangeMessage("AO", i + 1, ais[i], aos[i],
                    TOLERANCE_ANALOG);
            if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
                mioe.add(new IOException(msg));
                ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
            }
            else {
                ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
            }
        }

        // Test channel independence
        {
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Testing channel independence");
            for (int i = 0; i < aos.length; i++) {
                aos[i] = (short)((AO_18MA - AO_5MA) * i / aos.length + AO_5MA);
            }

            udr.putA(S_TESTBASE, (short)0, (short)aos.length, aos);

            // Sleep so it can stabilize
            Utils.sleep(testLib.getScanDelay() * 2);

            // Get the average value
            short[] ais =
                testLib.getAverageAnalogs(S_TESTBASE, (short)0,
                    (short)aos.length, TOLERANCE_AVERAGE);

            // Compare
            for (int i = 0; i < aos.length; i++) {
                // A small amount of influence is allowed; a difference of 1mA
                // is equal to an analog difference of about 2048, so as long
                // as IndependenceTolerance is smaller than that, all is well
                String msg =
                    formatRangeMessage("AO", i + 1, ais[i], aos[i],
                        TOLERANCE_INDEPENDENCE);
                if (Math.abs(ais[i] - aos[i]) > TOLERANCE_INDEPENDENCE) {
                    mioe.add(new IOException(msg));
                    ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
                }
                else {
                    ui.displayMessage(this, UserInterface.M_NORMAL, "    " +
                                                                    msg);
                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }

        ui.displayMessage(this, UserInterface.M_NORMAL, "  AO tests complete");
    }

    // private void checkPOE() throws IOException, TimeoutException
    private void checkPOE() throws IOException, TimeoutException {
        ui.displayMessage(this, UserInterface.M_WARNING, "Testing the PoE output");

        // If it's an EB base, check AI_POE for a value, otherwise expect something small (-8192, probably)
        short[] vals = udr.getA(S_TESTBASE, AI_POE, (short)1);

        if (devName.startsWith("EB")) {
            if (Math.abs(EXP_POE - vals[0]) > TOLERANCE_POE) {
                throw new IOException(String.format("AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_NOTMEMBEROF, EXP_POE, TOLERANCE_POE));
            }else {ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_MEMBEROF, EXP_POE, TOLERANCE_POE));
            }
        }
        else {
            if (vals[0] > 0) {
                throw new IOException("It appears that there is a PoE board on this module, but it has been initialized as an E2.");
            }
            else {ui.displayMessage(this, UserInterface.M_NORMAL, "  As expected, there is no PoE board on this module.");
            }
        }

        ui.displayMessage(this, UserInterface.M_NORMAL, "  PoE test complete");
    }

    // private void checkWatchdog() throws IOException, TimeoutException
    private void checkWatchdog()
        throws IOException, TimeoutException
    {
        ui.displayMessage(this, UserInterface.M_WARNING,
            "Testing the Watchdog output");

        MultiIOException mioe = new MultiIOException();

        // Give it a couple seconds in case it was rebooted recently
        Utils.sleep(10000);

        short[] vals = udr.getA(S_TESTBASE, AI_WATCHDOG, (short)1);

        if (Math.abs(EXP_WATCHDOG - vals[0]) > TOLERANCE_WATCHDOG) {
            throw new IOException(
                String
                    .format(
                        "AI Channel %d (connected to Watchdog output): is off when it should be on - read %d expected %d +/- %d",
                        AI_WATCHDOG,vals[0],EXP_WATCHDOG,TOLERANCE_WATCHDOG));
            
        }
        else {
            ui
                .displayMessage(
                    this,
                    UserInterface.M_NORMAL,
                    String
                        .format(
                            "  AI Channel %d (connected to Watchdog output) is on as expected",
                            AI_WATCHDOG));
        }

        // Make sure the watchdog output is off
        testLib.loadBaseFiles(S_TESTBASE, DIR_WATCHDOG_OFF);

        // Give it a couple extra seconds
        Utils.sleep(10000);

        vals = udr.getA(S_TESTBASE, AI_WATCHDOG, (short)1);

        if (vals[0] > TOLERANCE_WATCHDOG) {
            throw new IOException(
                String
                    .format(
                        "AI Channel %d (connected to Watchdog output) is on when it should be off. Read %d expected between 0 to %d",
                        AI_WATCHDOG,vals[0],TOLERANCE_WATCHDOG));
        }
        else {
            ui
                .displayMessage(
                    this,
                    UserInterface.M_NORMAL,
                    String
                        .format(
                            "  AI Channel %d (connected to Watchdog output) is off as expected",
                            AI_WATCHDOG));
        }
        
        if (!mioe.isEmpty()) {
            throw mioe;
        }

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  Watchdog test complete");
    }

    // private void checkIsolated() throws IOException, TimeoutException
    private void checkIsolated()
        throws IOException, TimeoutException
    {
        // The 24880 module has one isolated counter register
        // If this is not a 24880, just return
        if (!devName.endsWith("24880")) {
            ui.displayMessage(this, UserInterface.M_NORMAL,
                "This module does not have an isolated counter, skipping test");
            return;
        }
        
        // Turn on counters
        if (!testLib.loadBaseFiles(S_TESTBASE, DIR_ISOLATED)) {
            // Make sure the counters take affect
            testLib.resetStation(S_TESTBASE);
        }

        // Turn off the outputs feeding the counters
		boolean[] dos = new boolean[1];
		dos[0] = false;
		udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);
        Utils.sleep(100); // Make sure the off value takes effect

        ui.displayMessage(this, UserInterface.M_WARNING,
            "Testing isolated counter register");

        // get a read of the counter before we toggle the input so we 
        // know where we started at.
        short[] aistart = new short[1];
        aistart = udr.getA(S_TESTBASE, (short)8, (short)aistart.length);

        // Toggle the counter this many times
        int counter1 = 10;
        short[] ai = new short[1];
        int i = 0;
        while (i < counter1) {
            // Turn it on
            dos[0] = true;
            udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);
            // Wait a moment for it to register
            Utils.sleep(100);

            // Turn it off
            dos[0] = false;
            udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);
            // Wait a moment for it to register
            Utils.sleep(100);

//read of AI8 below will serve as the delay removed above
            // Grab analog input 8, it holds the count
            ai = udr.getA(S_TESTBASE, (short)8, (short)ai.length);

            if ((ai[0] - aistart[0]) != (i+1)) {
                throw new IOException(String.format(
                    "  Expected count = %d, but found count = %d", (i+1), (ai[0] - aistart[0])));
            }

            // Increment i and loop around
            i++;
        }

        // Grab analog input 8, it holds the count
        ai = udr.getA(S_TESTBASE, (short)8, (short)ai.length);

        if ((ai[0] - aistart[0]) != counter1) {
            throw new IOException(String.format(
                "  Expected cnt1 = %d, but found cnt1 = %d", counter1, (ai[0] - aistart[0])));
        }

        ui.displayMessage(this, UserInterface.M_NORMAL,String.format(
                "  Expected count increase of %d and count increased %d", counter1, (ai[0] - aistart[0])));
        ui.displayMessage(this, UserInterface.M_NORMAL,
                "  Isolated counter test complete.");
    }

    
    // private void checkIsolatedThreshold() throws IOException, TimeoutException
    private void checkIsolatedThreshold()
        throws IOException, TimeoutException
    {
        // The 24880 module has one isolated counter register
        // If this is not a 24880, just return
        if (!devName.endsWith("24880")) {
            ui.displayMessage(this, UserInterface.M_NORMAL,
                "This module does not have an isolated counter, skipping test");
            return;
        }
        
        ui.displayMessage(this, UserInterface.M_WARNING,
        "Testing isolated input on/off threshold");

        // Turn on isolated counter so the DI0 is on from the isolated input 
        if (!testLib.loadBaseFiles(S_CALBASE, DIR_ISO_CNTR_THRES)) {
            // Make sure the change takes affect
            testLib.resetStation(S_CALBASE);
        }
        
        boolean[] di = new boolean[1];
        di[0] = false;
        int counter = 10;
        short[] aos = new short[1];
        short[] aoSetPoint = new short[10];
        double[] aoSetVolts = new double[10];
        // These values are determined experimentally and 
        // will vary with the affects of the input circuitry.
        aoSetPoint[0] = 4875; 
        aoSetVolts[0] = (double)1.75; // approx. voltage for setpoint
        aoSetPoint[1] = 6500; 
        aoSetVolts[1] = (double)2.00; // approx. voltage for setpoint
        aoSetPoint[2] = 8100; 
        aoSetVolts[2] = (double)2.25; // approx. voltage for setpoint
        aoSetPoint[3] = 11000; 
        aoSetVolts[3] = (double)2.50; // approx. voltage for setpoint
        aoSetPoint[4] = 14500; 
        aoSetVolts[4] = (double)2.75; // approx. voltage for setpoint
        aoSetPoint[5] = 17400; 
        aoSetVolts[5] = (double)3.00; // approx. voltage for setpoint
        aoSetPoint[6] = 19100; 
        aoSetVolts[6] = (double)3.25; // approx. voltage for setpoint
        aoSetPoint[7] = 20200; 
        aoSetVolts[7] = (double)3.50; // approx. voltage for setpoint
        aoSetPoint[8] = 21300; 
        aoSetVolts[8] = (double)3.75; // approx. voltage for setpoint
        aoSetPoint[9] = 22200; 
        aoSetVolts[9] = (double)4.00; // approx. voltage for setpoint
        

        int j = 0;
    	short[] output = new short[1];
        boolean found = false;
        double turnOnVolts = (double)0;
        // check where it turns on
        while ((j < counter) && !found) {
        	output[0] = aoSetPoint[j];
            udr.putA(S_OUTPUTS24_31, (short)0, (short)output.length, output);
            Utils.sleep(500);
            di= udr.getD(S_CALBASE, (short)0, (short)di.length);
            if (di[0] == true) {
            	found = true;
            	turnOnVolts = aoSetVolts[j];
            }
        	j++;
        }
        if (!found){
            throw new IOException(String.format("  Isolated Counter input did not turn on before 4 Volts"));
        }
        else {
            if ((turnOnVolts < (double)2.25) || (turnOnVolts > (double)3.00)){
                ui.displayMessage(this, UserInterface.M_ERROR, String.format(
                		"  Isolated Input turned on at %1.2f volts which is the outside expected range of 2.25-3.00 volts",turnOnVolts));
            }
            else {
        		ui.displayMessage(this, UserInterface.M_NORMAL,String.format(
            		"  Isolated input turned on at approximately %1.2f volts - pass",turnOnVolts));
    	        // check where it turns off
    	        // starting where we left the voltage (j)
            	j--; // remove the last increment to know where we left off in the array
            	found = false;
                double turnOffVolts = (double)0;
    	        while ((j >= 0) && !found) {
    	        	output[0] = aoSetPoint[j];
    	            udr.putA(S_OUTPUTS24_31, (short)0, (short)output.length, output);
    	            Utils.sleep(500);
    	            di = udr.getD(S_CALBASE, (short)0, (short)di.length);
    	            if (di[0] == false) {
    	            	found = true;
    	            	turnOffVolts = aoSetVolts[j];
    	            }
    	        	j--;
    	        }
    	        if (!found){
    	        	throw new IOException(String.format("  Isolated Counter input did not turn off before approx. 1.75 Volts"));
    	        }
    	        else {
    	            if ((turnOffVolts < (double)2.00) || (turnOffVolts > (double)2.75)){
    	            	ui.displayMessage(this, UserInterface.M_ERROR, String.format(
    	                		"  Isolated Input turned off at %1.2f volts which is the outside expected range of 2.00-2.75 volts",turnOffVolts));
    	            }
    	            else {
    	            	ui.displayMessage(this, UserInterface.M_NORMAL,String.format(
    		                	"  Isolated input turned off at approximately %1.2f volts - pass",turnOffVolts));
    	            }
            	}
            }
        } 
        output[0] = 0;
        udr.putA(S_OUTPUTS24_31, (short)0, (short)output.length, output); // set it back to zero (4mA,~.8V)
        
        ui.displayMessage(this, UserInterface.M_NORMAL,	"  Isolated input threshold test complete");
    }

    // Testing helper functions
    // private void walkAOs(short stationID, int count, short testStationID,
    // short offset, short totalCount, short offVal, short onVal) throws
    // IOException, TimeoutException
    // stationID: The station with the AOs
    // count: The number of AOs to walk through (starts at 0)
    // testStationID: The station with the DIs
    // offset: The offset of the DI connected to the first AO (so we can make
    // sure they turn on)
    // totalCount: The total number of DIs on the station (so we can make sure
    // the rest stay off)
    // offVal: The AO value that should turn the DI off
    // onVal: The AO value that should turn the DI on
    private void walkAOs(short stationID, short count, short testStationID,
                         short offset, short totalCount, short offVal,
                         short onVal)
        throws IOException, TimeoutException
    {
        // One-by-one, turn on the outputs from 0 to count on stationID
        // Each time, read count inputs from testStationID at offset to see if
        // they turn on/off as expected

        MultiIOException mioe = new MultiIOException();

        short[] aos = new short[count];

        // Start out by setting everything to 0
        Arrays.fill(aos, offVal);
        udr.putA(stationID, (short)0, count, aos);
        Utils.sleep(250);

        // Make sure everything reads as 0
        boolean[] dis = udr.getD(testStationID, (short)0, totalCount);
        if (dis.length != totalCount) {
            throw new IOException(
                "Station did not respond to GetD message with the proper number of registers");
        }
        for (int i = 0; i < totalCount; i++) {
            if (dis[i]) {
                mioe.add(new IOException("After turning off all inputs, DI " +
                                         i + " (0-based) is on"));
            }
        }

        // Walk through, setting each to just below the guaranteed ON and
        // backing down the previous to just above the guaranteed OFF
        for (short i = 0; i <= count; i++) {
            // Set the previous output to the off value
            if (i > 0) {
                aos[i - 1] = offVal;
            }

            // Set the current output to the on value
            if (i < aos.length) {
                aos[i] = onVal;
            }

            udr.putA(stationID, (short)0, count, aos);

            Utils.sleep(250);

            dis = udr.getD(testStationID, (short)0, totalCount);

            for (int j = 0; j < dis.length; j++) {
                if (offset + i == j && !dis[j]) {
                    if (i < aos.length) {
                        mioe
                            .add(new IOException("After turning on DI " +
                                                 (offset + i) +
                                                 " (0-based), station reports it is off"));
                    }
                    else {
                        // We didn't turn anything on for this run, just turned
                        // the last one off
                    }
                }
                else if (offset + i != j && dis[j]) {
                    if (i < aos.length) {
                        mioe.add(new IOException(
                            "After turning on DI " + (offset + i) +
                                " (0-based), station reports DI " + j +
                                " is also on"));
                    }
                    else {
                        mioe.add(new IOException(
                            "After turning off all DIs, station reports DI " +
                                j + " (0-based) is on"));
                    }
                }
                else if (offset + i == j && !dis[j]) {

                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }
    }

    private void REMwalkAOs(short stationID, short count, short testStationID,
                            short offset, short totalCount, short offVal,
                            short onVal)
        throws IOException, TimeoutException
    {
        // One-by-one, turn on the outputs from 0 to count on stationID
        // Each time, read count inputs from testStationID at offset to see if
        // they turn on/off as expected

        MultiIOException mioe = new MultiIOException();

        short[] aos = new short[count];

        // Start out by setting everything to 0
        Arrays.fill(aos, offVal);
        udr.putA(stationID, offset, count, aos);
        Utils.sleep(250);

        // Make sure everything reads as 0
        boolean[] dis = udr.getD(testStationID, (short)0, totalCount);
        if (dis.length != totalCount) {
            throw new IOException(
                "Station did not respond to GetD message with the proper number of registers");
        }
        for (int i = 0; i < totalCount; i++) {
            if (dis[i]) {
                mioe.add(new IOException("After turning off all inputs, DI " +
                                         i + " (0-based) is on"));
            }
        }

        // Walk through, setting each to just below the guaranteed ON and
        // backing down the previous to just above the guaranteed OFF
        for (short i = 0; i <= count; i++) {
            // Set the previous output to the off value
            if (i > 0) {
                aos[i - 1] = offVal;
            }

            // Set the current output to the on value
            if (i < aos.length) {
                aos[i] = onVal;
            }

            udr.putA(stationID, offset, count, aos);

            Utils.sleep(250);

            dis = udr.getD(testStationID, (short)0, totalCount);

            for (int j = 0; j < dis.length; j++) {
                if (offset + i == j && !dis[j]) {
                    if (i < aos.length) {
                        mioe
                            .add(new IOException("After turning on DI " +
                                                 (offset + i) +
                                                 " (0-based), station reports it is off"));
                    }
                    else {
                        // We didn't turn anything on for this run, just turned
                        // the last one off
                    }
                }
                else if (offset + i != j && dis[j]) {
                    if (i < aos.length) {
                        mioe.add(new IOException(
                            "After turning on DI " + (offset + i) +
                                " (0-based), station reports DI " + j +
                                " is also on"));
                    }
                    else {
                        mioe.add(new IOException(
                            "After turning off all DIs, station reports DI " +
                                j + " (0-based) is on"));
                    }
                }
                else if (offset + i == j && !dis[j]) {

                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }
    }

    // private boolean chosen(String testName)
    private boolean chosen(String testName)
    {
        return (chosenTest == null || chosenTest.equals(testName));
    }

    // private String formatRangeMessage(String type, int channel, short
    // expected, short found)
    private String formatRangeMessage(String type, int channel, short found,
                                      short expected, int tolerance)
    {
        String op = TestLib.CH_MEMBEROF;
        if (Math.abs(expected - found) > tolerance) {
            op = TestLib.CH_NOTMEMBEROF;
        }

        return String.format("%s Channel %,d: %s%,d [%,d %s (%,d +/- %,d)]",
            type, channel, TestLib.CH_DELTA, found - expected, found, op,
            expected, tolerance);
    }
}

/*
 * TestLib.java
 *
 * A library of functions common to multiple testing modules.
 *
 * Jonathan Pearson
 * May 12, 2008
 *
 */

package com.sixnetio.Initializer.ModTest;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

import com.sixnetio.Station.MessageDispatcher;
import com.sixnetio.Station.TransportMethod;
import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.MultiIOException;
import com.sixnetio.util.UserInterface;
import com.sixnetio.util.Utils;

class TestLib {
	// Special characters for messages
	public static final String CH_DELTA = "\u0394"; // Greek character Delta
	public static final String CH_MEMBEROF = "\u2208"; // Mathematical
														// 'member-of' set
														// operator
	public static final String CH_NOTMEMBEROF = "\u2209"; // Mathematical
															// 'not-a-member-of'
															// set operator
	public static final String CH_LESS_EQUAL = "\u2264"; // Single-character
															// equivalent of <=
	public static final String CH_GREATER_EQUAL = "\u2265"; // Single-character
															// equivalent of >=

	private UDRLib udr;
	private UserInterface ui;

	// When averaging input values...
	private int scanCount = 3; // Perform three scans and average over them
	private long scanDelay = 1000; // And wait this many ms between scans

	// Other configurable options
	private String baseConfigDir = "/base0/cfg/"; // Directory to load config
													// changes to, must end with
													// a /
	private boolean ipmStation = false; // Determines which type of reset
										// message to use
	private int baseSerial = 1; // When ipmStation == true, this is the serial
								// used for ResetIPm messages

	// NOTE: IPm reset messages are always for 'soft' resets

	public TestLib(UDRLib udr, UserInterface ui) {
		this.udr = udr;
		this.ui = ui;
	}

	// Accessors
	public int getScanCount() {
		return scanCount;
	}

	public void setScanCount(int scanCount) {
		this.scanCount = scanCount;
	}

	public long getScanDelay() {
		return scanDelay;
	}

	public void setScanDelay(long scanDelay) {
		this.scanDelay = scanDelay;
	}

	public String getBaseConfigDir() {
		return baseConfigDir;
	}

	public void setBaseConfigDir(String baseConfigDir) {
		this.baseConfigDir = baseConfigDir;
	}

	public boolean isIpmStation() {
		return ipmStation;
	}

	public void setIpmStation(boolean ipmStation) {
		this.ipmStation = ipmStation;
	}

	public int getBaseSerial() {
		return baseSerial;
	}

	public void setBaseSerial(int baseSerial) {
		this.baseSerial = baseSerial;
	}

	// Dealing with UDRHandlers
	public UDRLink registerTCPHandler(String address) throws IOException {
		MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();

		UDRLink handler = new UDRLink(TransportMethod.TCP, address);
		dispatcher.registerHandler(handler);

		return handler;
	}

	public UDRLink registerUDPHandler(String address) throws IOException {
		MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();

		UDRLink handler = new UDRLink(TransportMethod.UDP, address);
		dispatcher.registerHandler(handler);

		return handler;
	}

	// Always returns null
	public UDRLink unregisterHandler(UDRLink handler) {
		MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();

		dispatcher.unregisterHandler(handler);
		return null;
	}

	// Testing for connectivity
	public void verifyConnectivity(short stationID) throws IOException,
			TimeoutException {
		udr.nop(stationID);
	}

	// Changing base settings
	public void resetStation(short stationID) throws IOException,
			TimeoutException {
		resetStation(stationID, false);
	}

	// If it is an IPm station, 'hard' controls what kind of reset
	// 'hard' is ignored for ET2 stations
	public void resetStation(short stationID, boolean hard) throws IOException,
			TimeoutException {
		long startTime;

		if (ipmStation) {
			udr.resetIPm(stationID, baseSerial, hard);

			startTime = System.nanoTime();

			// Connectivity workaround (should be 4 seconds for either)
			if (hard) {
				Utils.sleep(41000); // Give it a chance to actually reset
			} else {
				Utils.sleep(7000);
			}
		} else {
			udr.resetGateway(stationID);

			startTime = System.nanoTime();
		}

		udr.waitForResponse(stationID, 60000);
		long endTime = System.nanoTime();

		if (Utils.DEBUG) {
			ui.displayMessage(this, UserInterface.M_ERROR, String.format(
					"Device took %,d seconds to respond",
					(endTime - startTime) / 1000000000));
		}
	}

	// Returns true if the station was reset
	public boolean loadBaseFiles(short stationID, String dir)
			throws IOException, TimeoutException {
		return loadBaseFiles(stationID, dir, stationID);
	}

	// Returns true if the station was reset
	// Allows you to wait for a different station than the one you loaded
	// Useful if you want to make sure you will have connectivity to a
	// connected station, over RS485 for example
	public boolean loadBaseFiles(short stationID, String dir, short waitForID)
			throws IOException, TimeoutException {
		return loadBaseFiles(stationID, dir, waitForID, false);
	}

	public boolean loadBaseFiles(short stationID, String dir, short waitForID,
			boolean hardReset) throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Setting up station "
				+ stationID);

		// Verify the data in the files in the base against the data that should
		// be in them
		// If anything is different, load new data and reset the station
		if (verifyAndLoad(stationID, dir, baseConfigDir)) {
			// Something changed, reset the station
			ui.displayMessage(this, UserInterface.M_WARNING,
					"  Settings have changed, resetting the station");

			resetStation(stationID, hardReset);
			udr.waitForResponse(waitForID);

			ui.displayMessage(this, UserInterface.M_NORMAL, "  Finished");

			return true;
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Finished");
			return false;
		}
	}

	// Returns true if anything changed, false if nothing changed
	private boolean verifyAndLoad(short stationID, String localDir,
			String remoteDir) throws IOException, TimeoutException {
		boolean changed = false; // Set to true if we need to write anything
									// back to the station

		File f = new File(localDir);
		if (!f.isDirectory()) {
			throw new IOException(localDir + " is not a directory");
		}

		// For each file in f, read the station and compare
		// If anything is different, write the changes to the station
		// Tradeoff: Larger blocks are faster, but cause more modifications to
		// flash
		// Solution: Use a block size of the maximum message length; it will be
		// as fast as possible,
		// and (hopefully) not cause everything to be re-written
		File[] files = f.listFiles(new FileFilter() {
			public boolean accept(File file) {
				return file.isFile();
			}
		});

		for (int i = 0; i < files.length; i++) {
			// Open the file
			InputStream in = new FileInputStream(files[i]);
			byte[] fileData;

			try {
				// Read the local file
				fileData = new byte[(int) files[i].length()];

				{
					int offset = 0;
					while (offset < fileData.length) {
						int bytesRead = in.read(fileData, offset,
								fileData.length - offset);
						if (bytesRead == -1) {
							throw new IOException(
									"Unexpected end of file for '"
											+ files[i].getAbsolutePath() + "'");
						}

						offset += bytesRead;
					}
				}
			} finally {
				in.close();
			}

			// Read the file in the station
			// FUTURE: At the moment, there are no files large enough to cause a
			// problem, but if any get to be over 32K, one may develop
			byte[] stationData = udr.readFile(stationID, remoteDir
					+ files[i].getName(), 0, (short) fileData.length, null);

			// Compare the two files, fixing the bits that don't match
			String path = remoteDir + files[i].getName();
			int[] alias = new int[1];
			alias[0] = udr.getAlias(stationID, path);

			for (int offset = 0; offset < fileData.length; offset++) {
				if (fileData[offset] != stationData[offset]) {
					// Write an entire block's worth of data, just so we don't
					// need to worry about it
					byte[] data = new byte[Math.min(UDRLib.SZ_BLOCK,
							fileData.length - offset)];
					System.arraycopy(fileData, offset, data, 0, data.length);

					udr.writeFile(stationID, path, data, offset, alias);

					System.arraycopy(stationData, offset, data, 0, data.length);

					changed = true;
				}
			}

			udr.closeAlias(stationID, alias[0]);
		}

		return changed;
	}

	public boolean zeroRange(short stationID, String path, int offset,
			int length) throws IOException, TimeoutException {
		boolean changed = false;
		int[] alias = new int[1];
		alias[0] = udr.getAlias(stationID, path);

		byte[] currentData = udr.readFile(stationID, path, offset,
				(short) length, alias);

		for (int i = 0; i < length; i++) {
			if (currentData[i] != 0) {
				byte[] block = new byte[Math.min(UDRLib.SZ_BLOCK, length - i)];
				Arrays.fill(block, (byte) 0);

				// Block is all zeros, write it to the file
				udr.writeFile(stationID, path, block, offset + i, alias);

				System.arraycopy(block, 0, currentData, i, block.length);

				changed = true;
			}
		}

		udr.closeAlias(stationID, alias[0]);

		return changed;
	}

	// I/O Functions
	public short[] getAverageAnalogs(short stationID, short start, short count,
			int tolerance) throws IOException, TimeoutException {
		// Grab what the station sees ScanCount times, once every scanDelay ms,
		// averaging the results
		// Make sure there are no significant differences between values
		// Use ints because we will add the results before dividing
		MultiIOException mioe = new MultiIOException();

		int[] accumulator = new int[count];
		short[] lastVal = null;
		int errCount = 0; // If it hits 3, break out

		for (int i = 0; i < scanCount; i++) {
			short[] reading = udr.getA(stationID, start, count);

			if (reading.length != count) {
				errCount++;

				if (errCount >= 3) {
					throw new IOException(
							"Station returned wrong number of values: expected "
									+ count + ", received " + reading.length);
				}

				// Retry
				i--;
				continue;
			}

			errCount = 0;

			// Add to what we have so far and make sure these values are not
			// significantly different from the previous
			for (int j = 0; j < count; j++) {
				accumulator[j] += reading[j];
				if (lastVal != null
						&& Math.abs(reading[j] - lastVal[j]) > tolerance) {
					mioe
							.add(new IOException(
									String
											.format(
													"Consecutive passes over the same input value on AI channel %d (0-based) yielded %,d and %,d, which are more than %d apart",
													j, reading[j], lastVal[j],
													tolerance)));
					ui
							.displayMessage(
									this,
									UserInterface.M_ERROR,
									String
											.format(
													"Consecutive passes over the same input value on AI channel %d (0-based) yielded %,d and %,d, which are more than %d apart",
													j, reading[j], lastVal[j],
													tolerance));
				}

			}

			lastVal = reading;

			// Wait a bit before taking the next reading
			Utils.sleep(scanDelay);
		}

		// Average the results
		short[] analogs = new short[count];
		for (int i = 0; i < count; i++) {
			analogs[i] = (short) (accumulator[i] / scanCount);
		}

		if (!mioe.isEmpty()) {
			throw mioe;
		}

		return analogs;
	}

	// Calibration
	public void computeAICurrentCalibration(short[] aisLow, short[] aisHigh,
			short expectedLow, short expectedHigh, short[] out_zeros,
			short[] out_spans) throws IOException {
		computeAICurrentCalibration(aisLow, aisHigh, expectedLow, expectedHigh,
				out_zeros, out_spans, (short) 0);
	}

	// Calibration
	public void computeAICurrentCalibration(short[] aisLow, short[] aisHigh,
			short expectedLow, short expectedHigh, short[] out_zeros,
			short[] out_spans, short aiOffset) throws IOException {
		if (aisLow.length != aisHigh.length) {
			throw new RuntimeException(
					"Cannot compute calibration from differing length arrays");
		}

		int count = aisLow.length;

		if (out_zeros == null || out_zeros.length != count) {
			throw new RuntimeException(
					"Provided zeros array is null or incorrect length");
		}
		if (out_spans == null || out_spans.length != count) {
			throw new RuntimeException(
					"Provided spans array is null or incorrect length");
		}

		MultiIOException mioe = new MultiIOException();

		double partial1 = expectedHigh - (expectedLow + 1.0);

		ui.displayMessage(this, UserInterface.M_WARNING, "  Verifying uncalibrated AI readings are within range for calibration");
		for (int i = 0; i < count; i++) {
			double partial2 = (aisHigh[i] - aisLow[i]);
			int TOLERANCE_UNCALIBRATED = 2050; 
			String msg = formatRangeMessage(" AI" , i+1, aisLow[i], expectedLow, TOLERANCE_UNCALIBRATED);
			if (Math.abs(aisLow[i] - expectedLow) > TOLERANCE_UNCALIBRATED) {
				mioe.add(new IOException(msg));
				ui.displayMessage(this, UserInterface.M_ERROR, "  " + "Low reading out of range failure! Cannot calibrate" + msg);
			}else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  " + "Low reading for" + msg);
			}
			msg = formatRangeMessage(" AI", i+1, aisHigh[i], expectedHigh, TOLERANCE_UNCALIBRATED);
			if (Math.abs(aisHigh[i] - expectedHigh) > TOLERANCE_UNCALIBRATED) {
				mioe.add(new IOException(msg));
				ui.displayMessage(this, UserInterface.M_ERROR, "  " + "High reading out of range failure! Cannot calibrate" + msg);
			}else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  " + "High reading for" + msg);
			}

			double chanspan = ((partial1 / partial2) - 1.0) * 65536.0;
			double partial3 = (partial1 / partial2) - 1.0;
			double chanzero = (32.0 * (2048.0 - aisLow[i] - (aisLow[i] + 8191.0)
					* partial3)) / 25.0;

			out_zeros[i] = (short) chanzero;
			out_spans[i] = (short) chanspan;
		}

		if (!mioe.isEmpty()) {
			throw mioe;
		}
	}

    // Calibration done with measured expected values instead of fixed values.
	// Use this function when the source is measured for each channel
    public void computeAICurrentCalibrationWithMeasuredSetpoint(short[] aisLow, short[] aisHigh,
            short[] expectedLow, short[] expectedHigh, short[] out_zeros,
            short[] out_spans) throws IOException {
        if ((aisLow.length != aisHigh.length)||(expectedHigh.length < aisHigh.length)||(expectedLow.length < aisLow.length)) {
            throw new RuntimeException(
                    "Cannot compute calibration from differing length arrays");
        }

        int count = aisLow.length;

        if (out_zeros == null || out_zeros.length != count) {
            throw new RuntimeException(
                    "Provided zeros array is null or incorrect length");
        }
        if (out_spans == null || out_spans.length != count) {
            throw new RuntimeException(
                    "Provided spans array is null or incorrect length");
        }

        MultiIOException mioe = new MultiIOException();


        ui.displayMessage(this, UserInterface.M_WARNING, "  Verifying uncalibrated AI readings are within range for calibration");
        for (int i = 0; i < count; i++) {
            double partial1 = expectedHigh[i] - (expectedLow[i] + 1.0);
            double partial2 = (aisHigh[i] - aisLow[i]);
            int TOLERANCE_UNCALIBRATED = 2000; 
            String msg = formatRangeMessage(" AI" , i+1, aisLow[i], expectedLow[i], TOLERANCE_UNCALIBRATED);
            // check that the low values are close to each other
            if (Math.abs(aisLow[i] - expectedLow[i]) > TOLERANCE_UNCALIBRATED) {
                mioe.add(new IOException(msg));
                ui.displayMessage(this, UserInterface.M_ERROR, "  " + "Low reading out of range failure! Cannot calibrate" + msg);
            }else {
                ui.displayMessage(this, UserInterface.M_NORMAL, "  " + "Low reading for" + msg);
            }
            msg = formatRangeMessage(" AI", i+1, aisHigh[i], expectedHigh[i], TOLERANCE_UNCALIBRATED);
            // check that the high values are close to each other
            if (Math.abs(aisHigh[i] - expectedHigh[i]) > TOLERANCE_UNCALIBRATED) {
                mioe.add(new IOException(msg));
                ui.displayMessage(this, UserInterface.M_ERROR, "  " + "High reading out of range failure! Cannot calibrate" + msg);
            }else {
                ui.displayMessage(this, UserInterface.M_NORMAL, "  " + "High reading for" + msg);
            }
            //check for an acceptable minimal range between low readings and high readings
            // > 20000 is good enough for the check here
            if ((Math.abs(expectedHigh[i] - expectedLow[i]) < 20000)) { 
                mioe.add(new IOException(msg));
                ui.displayMessage(this, UserInterface.M_ERROR, "  " + 
                    "Range between channel " + String.format("%,d", i+1) + " Low and High reading out of range failure! Cannot calibrate."); 
                ui.displayMessage(this, UserInterface.M_ERROR, "  " + 
                    "Expected >20000 for the span but span was " + String.format("%,d", expectedHigh[i] - expectedLow[i]));
            }else {
                ui.displayMessage(this, UserInterface.M_NORMAL, "  " + 
                    "Range between channel " + String.format("%,d", i+1) + " of " + 
                    String.format("%,d", expectedHigh[i] - expectedLow[i]) + " is acceptable.");
            }
            double chanspan = ((partial1 / partial2) - 1.0) * 65536.0;
            double partial3 = (partial1 / partial2) - 1.0;
            double chanzero = (32.0 * (expectedLow[i] - aisLow[i] - (aisLow[i] + 8192.0)
                    * partial3)) / 25.0;

            out_zeros[i] = (short) chanzero;
            out_spans[i] = (short) chanspan;
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }
    }

    // This requires that you took your readings at 4ma and 18ma
	public void computeAOCurrentCalibration(short[] aosLow, short[] aosHigh,
			short[] out_zeros, short[] out_spans) throws IOException {
		computeAOCurrentCalibration(aosLow, aosHigh, out_zeros, out_spans,
				(short) 0);
	}

	// This requires that you took your readings at 4ma and 18ma
	public void computeAOCurrentCalibration(short[] aosLow, short[] aosHigh,
			short[] out_zeros, short[] out_spans, short aoOffset)
			throws IOException {
		if (aosLow.length != aosHigh.length) {
			throw new RuntimeException(
					"Cannot compute calibration from differeng length arrays");
		}

		int count = aosLow.length;

		if (out_zeros == null || out_zeros.length != count) {
			throw new RuntimeException(
					"Provided zeros array is null or incorrect length");
		}
		if (out_spans == null || out_spans.length != count) {
			throw new RuntimeException(
					"Provided spans array is null or incorrect length");
		}

		MultiIOException mioe = new MultiIOException();

		// These are the values that we expected to see, in mA
		double d1 = 4.0;
		double d2 = 18.0;

		for (int i = 0; i < count; i++) {
			double m1 = 4.0 + aosLow[i] / 2048.0; // Value we saw (in mA)
			double m2 = 4.0 + aosHigh[i] / 2048.0; // Value we saw (in mA)

			if (Math.abs(m2 - m1) < 1.0) {
				mioe
						.add(new IOException(
								String
										.format(
												"Measured values at 4mA and 18mA are virtually the same for AO channel %d",
												i+1 + aoOffset)));
			}

			double g = (d2 - d1) / (m2 - m1);
			double part1 = (g - 1.0) * 65536.0;

			out_spans[i] = (short) part1;

			double part2 = (4.0 * (g - 1.0) - (m1 - (d1 / g))) * 32767.0 / 25.0;

			out_zeros[i] = (short) part2;
		}

		if (!mioe.isEmpty()) {
			throw mioe;
		}
	}

	public void computeAIVoltageCalibration(short[] aisLow, short[] aisHigh,
			short expectedLow, short expectedHigh, short[] out_zeros,
			short[] out_spans) throws IOException {
		computeAIVoltageCalibration(aisLow, aisHigh, expectedLow, expectedHigh,
				out_zeros, out_spans, (short) 0);
	}

	public void computeAIVoltageCalibration(short[] aisLow, short[] aisHigh,
			short expectedLow, short expectedHigh, short[] out_zeros,
			short[] out_spans, short aiOffset) throws IOException {
		if (aisLow.length != aisHigh.length) {
			throw new RuntimeException(
					"Cannot compute calibration from differing length arrays");
		}

		int count = aisLow.length;

		if (out_zeros == null || out_zeros.length != count) {
			throw new RuntimeException(
					"Provided zeros array is null or incorrect length");
		}
		if (out_spans == null || out_spans.length != count) {
			throw new RuntimeException(
					"Provided spans array is null or incorrect length");
		}

		MultiIOException mioe = new MultiIOException();

		double partial1 = (double) expectedHigh - (double) expectedLow;

		for (int i = 0; i < count; i++) {
			double partial2 = (aisHigh[i] - aisLow[i]);

			if (partial2 == 0) {
				mioe
						.add(new IOException(
								String
										.format(
												"Channel %d's zero and span test readings were the same value",
												i + 1 + aiOffset)));
			}

			double chanspan = ((partial1 / partial2) - 1.0) * 65536.0;
			double partial3 = partial1 / partial2;
			double chanzero = (expectedLow - (aisLow[i] * partial3));

			out_zeros[i] = (short) chanzero;
			out_spans[i] = (short) chanspan;
		}

		if (!mioe.isEmpty()) {
			throw mioe;
		}
	}

	// Modify a setting in a text config file, returning the value found there
	public String setTextConfigValue(short to, String path, String section,
			String name, String newValue) throws IOException, TimeoutException {
		// Read the entire file as a bunch of lines of text
		int[] size = new int[1];
		udr.statFile(to, path, size, null, null, null);
		byte[] fileData = udr.readFile(to, path, 0, size[0], null);
		String[] lines = Utils.getLines(new String(fileData));

		// Search for the proper section
		int sectionStart = -1, sectionEnd = -1;
		for (int i = 0; i < lines.length; i++) {
			if (lines[i].equals("[" + section + "]")) {
				sectionStart = i;
			} else if (sectionStart > -1 && lines[i].startsWith("[")) {
				sectionEnd = i;
				break;
			}
		}

		if (sectionStart == -1) {
			throw new IOException(String.format(
					"Section '%s' not found in file '%s'", section, path));
		}

		// Section is the last in the file
		if (sectionEnd == -1) {
			sectionEnd = lines.length;
		}

		// Search for the line with that value
		int valueLine = -1;
		for (int i = sectionStart; i < sectionEnd; i++) {
			if (lines[i].startsWith(name)) {
				valueLine = i;
				break;
			}
		}

		if (valueLine == -1) {
			throw new IOException(
					String
							.format(
									"Section '%s' of file '%s' does not contain a value named '%s'",
									section, path, name));
		}
		if (lines[valueLine].indexOf('=') == -1) {
			throw new IOException(
					String
							.format(
									"Line containing value '%s' within section '%s' of file '%s' does not contain an '=' sign",
									name, section, path));
		}

		// Grab the old value and modify the line to contain the new value
		String oldValue = lines[valueLine].substring(lines[valueLine]
				.indexOf('=') + 1);
		lines[valueLine] = lines[valueLine].substring(0, lines[valueLine]
				.indexOf('=') + 1)
				+ newValue;

		// Convert back to bytes and write back to the file
		fileData = Utils.combineLines(lines).getBytes();
		udr.writeFile(to, path, fileData, 0, null);

		return oldValue;
	}

	public void clearARP(String ipAddr) throws Exception {
		ui.displayMessage(this, UserInterface.M_WARNING,
				"Clearing the ARP table");

		boolean clearedARP = false;
		if (Utils.osIsWindows()) {
			// Windows doesn't have the permission issues that Linux does for
			// this command
			try {
				Process p = Runtime.getRuntime().exec(
						"C:\\Windows\\System32\\arp.exe -d " + ipAddr);
				if (p.waitFor() == 0) {
					clearedARP = true;
				}
			} catch (IOException ioe) {
				Utils.debug(ioe);
			} catch (InterruptedException ie) {
				throw new InterruptedOperationException(ie);
			}
		}

		if (!clearedARP) {
			String msg = "Please remove the entry for " + ipAddr
					+ " from your ARP table.\n";

			if (Utils.osIsWindows()) {
				msg += "To do this, open a command prompt (Start->Run, cmd) and type 'arp -d "
						+ ipAddr + "'\n";
			} else {
				msg += "To do this, open a terminal (Alt+F2, xterm) and type 'sudo arp -d "
						+ ipAddr + "'\n";
			}

			msg += "If you have any difficulties, please ask for assistance.";

			while (!ui.confirm(msg, "Clear ARP")) {
				if (ui.confirm("Really cancel the test?", "Cancel")) {
					throw new Exception("User canceled the test");
				}
			}
		}
	}
	private String formatRangeMessage(String type, int channel, short found, short expected, int tolerance) {
		String op = TestLib.CH_MEMBEROF;
		if (Math.abs(expected - found) > tolerance) op = TestLib.CH_NOTMEMBEROF;
		
		return String.format("%s Channel %,d: %s%,d [%,d %s (%,d +/- %,d)]", type, channel, TestLib.CH_DELTA, found - expected, found, op, expected, tolerance);
	}
}

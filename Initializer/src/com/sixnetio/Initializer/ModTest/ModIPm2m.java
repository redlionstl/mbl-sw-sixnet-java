/*
 * ModIPm2m.java
 *
 * Tests an IPm2m-101, -201, -101-SMID-3853, or -101-SMID-3854 module.
 *
 * Jonathan Pearson
 * September 17, 2008
 *
 */

package com.sixnetio.Initializer.ModTest;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.Initializer.Tester;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.util.*;

/*
 * After writing quite a few of these, I am trying to follow a few better conventions here:
 * - Descriptive base names instead of numbers
 * - Differentiate between...
 *    - A 'test': do something, look at the result
 *    - A 'check': read status inputs and make sure they are as expected
 */

public class ModIPm2m implements Tester {
	// Constants
	// Locations of the configuration files
	
	// Loaded into the calibration base
	private static final String DIR_CONFIG_ROOT = "TestData" + File.separator + "IPm2m" + File.separator;
	
	private static final String DIRNAME_CALBASE = "CalBase";
	private static final String DIRNAME_10BIT = "10bit";
	private static final String DIRNAME_WATCHDOG = "Watchdog";
	private static final String DIRNAME_IOTRANSFERS = "IOTransfers"; // .6tl files for turning on/off I/O transfers during sertest 
	
	// Loaded into the verification base
	private static final String DIRNAME_TESTBASE = "TestBase";
	private static final String DIRNAME_COUNTERS = "Counters";
	private static final String DIRNAME_10BIT2 = "10bit2"; // Only present for 3853 version, others don't need it
	
	// These are the actual paths, created when setupTest() is called
	private String Dir_Config;
	private String Dir_CalBase;
	private String Dir_10bit;
	private String Dir_Watchdog;
	private String Dir_IOTransfers;
	private String Dir_TestBase;
	private String Dir_Counters;
	private String Dir_10bit2;
	
	// Try to order these so that short tests are first, to avoid a long lag
	//   time before finding something simple
	private static final String T_SIZES = "Device Sizes Check",
	                            T_POE = "PoE Test",
	                            T_NVRAM = "NVRAM Test",
	                            T_USB = "USB Test",
	                            T_WATCHDOG = "Watchdog Test",
	                            T_CLOCK = "Real-Time Clock Test",
	                            T_DIS = "Walk DIs",
	                            T_CURRENT_AICAL = "AI Calibration (Current)",
	                            T_VOLTAGE_AICAL = "AI Calibration (Voltage)",
	                            T_CURRENT_AICAL_CHECK = "AI Calibration Check (Current)",
	                            T_VOLTAGE_AICAL_CHECK = "AI Calibration Check (Voltage)",
	                            T_CURRENT_AOCAL = "AO Calibration (Current)",
	                            T_CURRENT_AOCAL_CHECK = "AO Calibration Check (Current)",
	                            
	                            T_SERIAL = "Serial Test",
	                            T_DOS = "Walk DOs",
	                            T_DISOURCE = "DI Sourcing Test",
	                            T_COUNTER = "Counter Test",
	                            T_VOLTAGE_AI_INDCHECK = "AI Independence Check (Voltage)";
	
	private static final String[] TEST_NAMES = {
		// Calibration base
		T_SIZES,
        T_POE,
        T_NVRAM,
        T_USB,
        T_WATCHDOG,
        T_CLOCK,
        T_DIS,
    	T_CURRENT_AICAL,
        T_VOLTAGE_AICAL,
        T_CURRENT_AICAL_CHECK,
        T_VOLTAGE_AICAL_CHECK,
        T_CURRENT_AOCAL,
        T_CURRENT_AOCAL_CHECK,
        
        // Verification base
        T_SERIAL,
        T_DOS,
        T_DISOURCE,
        T_COUNTER,
        T_VOLTAGE_AI_INDCHECK
	};
	
	// These were determined by the default IP address that comes up on a new module
	private static final String IP_CALBASE = "10.1.0.1";
	private static final String IP_TESTBASE = "10.1.0.1";
	private static final String IP_STRAKIO = "10.1.0.3";
	
	private static final short S_CALBASE = 1; // Calibration
	private static final short S_TESTBASE = 1; // Verification (may change this to 2, currently 1 due to a discrepancy between the base setup and the module general.config data)
	private static final short S_STRAKIO = 3; // Sixtrak I/O for ST-GT-1210 (Base-2:AX0-AX7, Base-3:AY0-AY7, and Base-1: AY8-AY15) 
	private static final short S_OUTPUTS = 4; // AOs (AY0-AY3 on ET-8AI-4AO2)to calibrate current AIs (AX0-AX3) by
	
	
	// Which AI the POE output terminal is plugged into
	private static final short AI_POE = 2; // 0-based on S_Outputs
	
	// Which AI the Watchdog output terminal is plugged into
	// NOTE: The watchdog turns ON when any of the alarm conditions is met
	private static final short AI_WATCHDOG = 3; // 0-based on S_Outputs
	
	// Analog values (to avoid hardcoding them in multiple places)
	private static final short EXP_WATCHDOG = (short)3686; // Expected watchdog input value; 3686 = 5.8mA = 23.2V across 4000 Ohms
	private static final short EXP_POE = (short)4096; // Expected PoE input value; 4096 = 6mA = .6V across a 100 Ohm resistor
	private static final short AO_4MA = (short)0; // To output 4mA from an AO module
	private static final short AO_5MA = (short)2047; // To output 5mA from an AO module
	private static final short AO_18MA = (short)28671; // To output 18mA from an AO module
	private static final short AO_20MA = (short)32767; // To output 20mA from an AO module
	private static final short AI_0_2V = (short)655; // Expected 0.2V AI value
	private static final short AI_9_8V = (short)32078; // Expected 9.8V AI value
	private static final short AO_5_5V = (short)18002; // Expected 5.5V AO value
	private static final short AO_8_5V = (short)27822; // Expected 8.5V AO value
	
	// Tolerance for analog values
	// If two analog values should be equal, values within this distance on either side will be accepted
	private static final int TOLERANCE_ANALOG = 12;
	
	// Similar to AnalogTolerance, this is used to make sure consecutive scans during an average are close enough
	// Since values are allowed to be off by 12 in either direction of the target value, a difference of 24 covers
	//   the situation where one scan sees a value 12 below and the next scan sees 12 above, then we add a little
	//   extra to avoid spurious errors
	private static final int TOLERANCE_AVERAGE = 25;
	
	// When testing for channel independence (all get different values), this is the tolerance for each channel
	private static final int TOLERANCE_INDEPENDENCE = 25;
	
	// Used to check that the Watchdog input is close to what it should be
	private static final int TOLERANCE_WATCHDOG = 450;
	
	// Used to check that the PoE input is close to what it should be
	private static final int TOLERANCE_POE = 600;
	
	// Same as Analog/Average/Independence Tolerance, but these are used for 10-bit high-speed
	//   mode, which is wildly less accurate (6 fewer bits means +/- 64 for one analog point)
	// Allowed to be 12 analog points off, so 768 makes sense (12 * 64)
	private static final int TOLERANCE_HIGHSPEED_ANALOG = 768;
	
	// 768 below, then 768 above means 2 * 768 range across consecutive values
	private static final int TOLERANCE_HIGHSPEED_AVERAGE = 1536;
	
	// IndependenceTolerance is equal to AverageTolerance, so same here
	private static final int TOLERANCE_HIGHSPEED_INDEPENDENCE = 1536;
	
	// Base directory in a module where we will find the configuration files
	// We expect a slash on the end of this
	private static final String DIR_MODULECONFIG = "/module/arc/";
	
	// Poke this with a 0 and then a 1 to cause the I/O to reset (for example,
	//   after switching the DI-COM setting)
	private static final String DEV_RESET = "/sys/devices/platform/sxni_gpio/pa8";
	
	// Offset/length of the AI calibration section of the module cals file
	private static final int OFFSET_TIMESTAMP = 0x54; // Location of calibration timestamp
	private static final int OFFSET_AI_CALIBRATION = 0x100;
	private static final int LENGTH_AI_CALIBRATION = 0x10;
	
	// Offset/length of the AO calibration section of the module cals file
	private static final int OFFSET_AO_CALIBRATION = 0x180;
	private static final int LENGTH_AO_CALIBRATION = 0x80;
	
	// Expected data when reading /proc/mtd
	private static final String[] MTD_DATA = {
		"dev:    size   erasesize  name",
		"mtd0: 00050000 00004000 \"u-boot\"",
		"mtd1: 00020000 00004000 \"environment\"",
		"mtd2: 00010000 00004000 \"reserved\"",
		"mtd3: 00200000 00004000 \"boot\"",
		"mtd4: 01d80000 00004000 \"root\"",
		"mtd5: 00002100 00000108 \"base_reserved\"",
		"mtd6: 00105f00 00000108 \"cfg\"",
		"mtd7: 00002000 00002000 \"spi0.0-nvram\""
	};
	
	// Expected memory size as read out of /proc/meminfo
	private static final String MEM_TOTAL = "MemTotal:        29896 kB";
	
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private UDRLink activeHandler;
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	
	public ModIPm2m() { }
	
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for ModIPm2m must not be null");
		
		// Make sure the device name is recognized
		// Must start with EB-IPm2m or VT-IPm2m
		// Must end with 101-M, 101-M-SMID-3853, 101-M-SMID-3854, or 201-M
		if (!((devName.startsWith("EB-IPm2m") || (devName.startsWith("VT-IPm2m"))) &&
		      (devName.endsWith("101-M") || devName.endsWith("101-M-SMID-3853") || devName.endsWith("101-M-SMID-3854") || devName.endsWith("201-M")))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)108, (byte)0);
		testLib = new TestLib(udr, ui);
		testLib.setBaseConfigDir("/base/cfg/");
		testLib.setIpmStation(true);
		
		if (devName.endsWith("101-M")) {
			Dir_Config = DIR_CONFIG_ROOT + "101" + File.separator;
		} else if (devName.endsWith("101-M-SMID-3853")) {
			Dir_Config = DIR_CONFIG_ROOT + "3853" + File.separator;
		} else if (devName.endsWith("101-M-SMID-3854")) {
			Dir_Config = DIR_CONFIG_ROOT + "3854" + File.separator;
		} else if (devName.endsWith("201-M")) {
			Dir_Config = DIR_CONFIG_ROOT + "201" + File.separator;
		} else {
			// Shouldn't happen unless these tests don't match the ones above
			throw new RuntimeException("Passed name test, but could not match name");
		}
		
		Dir_CalBase = Dir_Config + DIRNAME_CALBASE;
		Dir_10bit = Dir_Config + DIRNAME_10BIT;
		Dir_Watchdog = Dir_Config + DIRNAME_WATCHDOG;
		Dir_IOTransfers = Dir_Config + DIRNAME_IOTRANSFERS;
		Dir_TestBase = Dir_Config + DIRNAME_TESTBASE;
		Dir_Counters = Dir_Config + DIRNAME_COUNTERS;
		Dir_10bit2 = Dir_Config + DIRNAME_10BIT2;
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
	}
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
	}
	
	public boolean supportsTestJump() {
		return true;
	}
	
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
	}
	
	public String getJumpTest() {
		return chosenTest;
	}
	
	public void run() {
		UDRLink gatewayHandler = null;
		
		// Register a UDP handler for the gateway
		try {
			gatewayHandler = testLib.registerUDPHandler(IP_STRAKIO);
			
			// Make sure it works
			ui.displayMessage(this, UserInterface.M_WARNING, "Verifying connectivity to the I/O concentrator");
			testLib.verifyConnectivity(S_STRAKIO);
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Done");
		} catch (Exception e) {
			if (gatewayHandler != null) testLib.unregisterHandler(gatewayHandler);
			
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		}
		
		try {
			// Finally block (end of the function) resets text config
			//   values and unregisters gatewayHandler
			String eth0Passthru;
			String s3Passthru;
			
			MultiIOException mioe = new MultiIOException();
			
			if (chosen(T_SIZES) ||
			    chosen(T_POE) ||
			    chosen(T_NVRAM) ||
			    chosen(T_USB) ||
			    chosen(T_WATCHDOG) ||
			    chosen(T_CLOCK) ||
			    chosen(T_DIS) ||
			    chosen(T_CURRENT_AICAL) ||
			    chosen(T_CURRENT_AICAL_CHECK) ||
			    chosen(T_VOLTAGE_AICAL) ||
			    chosen(T_VOLTAGE_AICAL_CHECK) ||
			    chosen(T_CURRENT_AOCAL) ||
			    chosen(T_CURRENT_AOCAL_CHECK)) {
    			
    			if (!ui.confirm("Please plug the module into the first base", "Module Check")) {
    				ui.displayMessage(this, UserInterface.M_ERROR, "User canceled the test");
    				ui.operationFailed(this);
    				return;
    			}
    			
    			// This base has serial number 1
    			testLib.setBaseSerial(1);
    			
    			// Clear the entry from the ARP table
    			// This will cause a new ARP message to be broadcast from the computer
    			// The switch will broadcast the ARP message and learn the new location of the module
    			// Without this, the switch will keep sending data to the old location
    			try {
					testLib.clearARP(IP_CALBASE);
				} catch (Exception e) {
					ui.handleException(this, e);
					ui.operationFailed(this);
					return;
				}
    			
        		// Register a UDP handler for the calibration base
    			activeHandler = null;
        		try {
        			activeHandler = testLib.registerUDPHandler(IP_CALBASE);
        			
        			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
        			udr.waitForResponse(S_CALBASE);
        			ui.displayMessage(this, UserInterface.M_NORMAL, "  Device is responding");
        			
        			// Load files
        			// Other settings that are not stored in the base
        			ui.displayMessage(this, UserInterface.M_WARNING, "Reconfiguring module");
        			eth0Passthru = testLib.setTextConfigValue(S_CALBASE, "/etc/stacfg/porteth0.config", "settings", "passthru_port", "/dev/ttyS3");
        			s3Passthru = testLib.setTextConfigValue(S_CALBASE, "/etc/stacfg/portS3.config", "settings", "passthru_port", "/dev/eth0");
        			
        			// RS485 takes a little longer to come up, wait for S_Outputs to show up
        			if (!testLib.loadBaseFiles(S_CALBASE, Dir_CalBase, S_OUTPUTS, true)) {
        				// We reconfigured the module, so we need to reset even if the base was okay
        				ui.displayMessage(this, UserInterface.M_WARNING, "Resetting the device");
        				testLib.resetStation(S_CALBASE, true);
        			}
        		} catch (Exception e) {
        			if (activeHandler != null) testLib.unregisterHandler(activeHandler);
        			
        			ui.handleException(this, e);
        			ui.operationFailed(this);
        			return;
        		}
        		
        		try { // Finally block unregisters the handler
        			// Now start doing tests
        			// Note: Only the calibration checks will display error messages in-process
        			
        			// Device sizes check
        			try {
        				if (chosen(T_SIZES)) checkSizes();
        			} catch (IOException ioe) {
        				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        				mioe.add(ioe);
        			}
        			
        			// POE test
        			try {
        				if (chosen(T_POE)) checkPOE();
        			} catch (IOException ioe) {
        				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        				mioe.add(ioe);
        			}
        			
        			// USB test
        			try {
        				if (chosen(T_USB)) testUSB();
        			} catch (IOException ioe) {
        				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        				mioe.add(ioe);
        			}
        			
        			// NVRAM test
        			try {
        				if (chosen(T_NVRAM)) testNVRAM();
        			} catch (IOException ioe) {
        				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        				mioe.add(ioe);
        			}
        			
        			// Watchdog test
        			try {
        				if (chosen(T_WATCHDOG)) testWatchdog();
        			} catch (IOException ioe) {
        				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        				mioe.add(ioe);
        			}
        			
        			// Test the real-time clock
        			try {
        				if (chosen(T_CLOCK)) testClock();
        			} catch (IOException ioe) {
        				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        				mioe.add(ioe);
        			}
        			
        			// Walk across the DIs
        			try {
        				if (chosen(T_DIS)) testDIs();
        			} catch (IOException ioe) {
        				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        				mioe.add(ioe);
        			}
        			
        			// Current AI Calibration/test
        			try {
        				try {
        					if (chosen(T_CURRENT_AICAL)&&(!runVerifyTest)) calibrateCurrentAIs();
        				} catch (IOException ioe) {
        					ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        					throw ioe;
        				}

        				// This really does belong here
        				// This makes sure that the AIs work properly with the calibration that we just made
        				// There is an 'independence' check on the other base that makes sure that none of
        				//   the AI channels are bussed together
        				if (chosen(T_CURRENT_AICAL_CHECK)) testCurrentAICalibration();
        			} catch (IOException ioe) {
        				mioe.add(ioe);
        			}
        			
        			// Voltage AI Calibration/test
        			try {
        				try {
        					if (chosen(T_VOLTAGE_AICAL)) calibrateVoltageAIs();
        				} catch (IOException ioe) {
        					// This way, we can print error messages and skip the test
        					ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        					throw ioe;
        				}
        				
        				if (chosen(T_VOLTAGE_AICAL_CHECK)) testVoltageAICalibration();
        			} catch (IOException ioe) {
        				mioe.add(ioe);
        			}
        			
        			// Current AO Calibration/test
        			try {
        				try {
        					if (chosen(T_CURRENT_AOCAL)&&(!runVerifyTest)) calibrateCurrentAOs();
        				} catch (IOException ioe) {
        					ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        					throw ioe;
        				}
        				
        				if (chosen(T_CURRENT_AOCAL_CHECK)) testCurrentAOCalibration();
        			} catch (IOException ioe) {
        				mioe.add(ioe);
        			}
        		} catch (Exception e) {
        			ui.handleException(this, e);
        			ui.operationFailed(this);
        			return;
        		} finally {
        			try {
        				ui.displayMessage(this, UserInterface.M_WARNING, "Cleaning up module configuration");
        				
	                    testLib.setTextConfigValue(S_CALBASE, "/etc/stacfg/porteth0.config", "settings", "passthru_port", eth0Passthru);
	                    testLib.setTextConfigValue(S_CALBASE, "/etc/stacfg/portS3.config", "settings", "passthru_port", s3Passthru);
	                    
	                    ui.displayMessage(this, UserInterface.M_NORMAL, "  Done");
                    } catch (Exception e) {
                    	Utils.debug(e);
                    	
	                    ui.displayMessage(this, UserInterface.M_ERROR, "Unable to clean up module settings");
                    }
        			
        			testLib.unregisterHandler(activeHandler);
        		}
    		}
    		
    		// If there was a problem, bail out now instead of continuing through the second base
    		if (!mioe.isEmpty()) {
    			ui.handleException(this, mioe);
    			ui.operationFailed(this);
    			return;
    		}
    		
    		if (chosen(T_SERIAL) ||
    			chosen(T_DOS) ||
    			chosen(T_DISOURCE) ||
    			chosen(T_COUNTER) ||
    			chosen(T_VOLTAGE_AI_INDCHECK)) {
    			
    			while (!ui.confirm("Please move the module to the second base.", "Move the Module")) {
    				if (ui.confirm("Really cancel the test?", "Cancel")) {
    					ui.displayMessage(this, UserInterface.M_ERROR, "User canceled the test");
    					ui.operationFailed(this);
    					return;
    				}
    			}
    			
    			// This base has serial number 2
    			testLib.setBaseSerial(2);
    			
				try {
					testLib.clearARP(IP_CALBASE);
				} catch (Exception e) {
					ui.handleException(this, e);
					ui.operationFailed(this);
					return;
				}
    			
        		// Register a UDP handler for station 2
    			activeHandler = null;
        		try {
        			activeHandler = testLib.registerUDPHandler(IP_TESTBASE);
        			
        			if (chosenTest == null) Utils.sleep(41000); // Connectivity workaround
        			
        			udr.waitForResponse(S_TESTBASE);
        			
        			// Make sure it works
        			testLib.loadBaseFiles(S_TESTBASE, Dir_TestBase);
        		} catch (Exception e) {
        			if (activeHandler != null) testLib.unregisterHandler(activeHandler);
        			
        			ui.handleException(this, e);
        			ui.operationFailed(this);
        			return;
        		}
        		
        		try { // Finally block unregisters the handler
        			// Test the serial lines
        			try {
        				if (chosen(T_SERIAL)) testSerial();
        			} catch (IOException ioe) {
        				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        				mioe.add(ioe);
        			}
        			
        			// Walk the DOs across some DIs
        			try {
        				if (chosen(T_DOS)) testDOs();
        			} catch (IOException ioe) {
        				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        				mioe.add(ioe);
        			}
        			
        			// Make sure that the sourced outputs (tied to ground) are all on
    				try {
    					if (chosen(T_DISOURCE)) testSourcing();
    				} catch (IOException ioe) {
    					// This one does not print its own error messages, so summarize for the user
    					ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
    					mioe.add(ioe);
    				}
    				
        			// High Speed Counters test
        			try {
        				if (chosen(T_COUNTER)) testCounters();
        			} catch (IOException ioe) {
        				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
        				mioe.add(ioe);
        			}
        			
        			// Voltage AI independence check
        			try {
        				if (chosen(T_VOLTAGE_AI_INDCHECK)) testVoltageAIIndependence();
        			} catch (IOException ioe) {
        				// Error messages are printed by the function
        				
        				mioe.add(ioe);
        			}
        		} catch (Exception e) {
        			ui.handleException(this, e);
        			ui.operationFailed(this);
        			return;
        		} finally {
        			testLib.unregisterHandler(activeHandler);
        		}
    		}
    		
    		if (!mioe.isEmpty()) {
    			ui.handleException(this, mioe);
    			ui.operationFailed(this);
    		} else {
    			ui.operationCompleted(this);
    		}
		} catch (Exception e) {
			// Try not to let anything fall through to here, this is only for debugging
			Utils.debug(e);
			ui.handleException(this, e);
			ui.operationFailed(this);
		} finally {
			testLib.unregisterHandler(gatewayHandler);
		}
	}
	
	private void checkSizes() throws IOException, TimeoutException  {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing memory sizes");
		
		// Read /proc/mtd
		udr.executeCommand(S_CALBASE, "/bin/cat /proc/mtd >/tmp/mtd.out", 15000);
    	String data = new String(udr.readFile(S_CALBASE, "/tmp/mtd.out", 0, -1, null));
    	String[] lines = Utils.getLines(data);
    	
    	// Compare lines to mtdData
    	if (!Arrays.equals(MTD_DATA, lines)) {
    		if (ui.confirm("Device reports different memory sizes than expected, allow to pass?", "Size Mismatch")) {
    			ui.displayMessage(this, UserInterface.M_ERROR, "  DEVICE ALLOWED TO PASS AFTER MEMORY SIZE MISMATCH");
    		} else {
    			throw new IOException("Device memory sizes do not match expected");
    		}
    	}
    	
    	// Read /proc/meminfo
    	udr.executeCommand(S_CALBASE, "/bin/cat /proc/meminfo >/tmp/meminfo.out", 15000);
    	data = new String(udr.readFile(S_CALBASE, "/tmp/meminfo.out", 0, -1, null));
    	lines = Utils.getLines(data);
    	
    	// Search for a line matching 'memTotal'; if none, wrong memory size
    	boolean found = false;
    	for (int i = 0; i < lines.length; i++) {
    		if (lines[i].equals(MEM_TOTAL)) {
    			found = true;
    			break;
    		}
    	}
    	
    	if (!found) {
    		if (ui.confirm("Device reports a different RAM size than expected, allow to pass?", "Size Mismatch")) {
    			ui.displayMessage(this, UserInterface.M_ERROR, "  DEVICE ALLOWED TO PASS AFTER RAM SIZE MISMATCH");
    		} else {
    			throw new IOException("Device RAM size does not match expected");
    		}
    	}
    	
    	ui.displayMessage(this, UserInterface.M_NORMAL, "  Memory size test complete");
	}
	
	private void checkPOE() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the PoE output");
		
		// If it's an EB base, check AI_POE for a value, otherwise expect something small (-8192, probably)
		short[] vals = udr.getA(S_OUTPUTS, AI_POE, (short)1);
		
		if (devName.startsWith("EB")) {
			if (Math.abs(EXP_POE - vals[0]) > TOLERANCE_POE) {
				throw new IOException(String.format("AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_NOTMEMBEROF, EXP_POE, TOLERANCE_POE));
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_MEMBEROF, EXP_POE, TOLERANCE_POE));
			}
		} else if (devName.startsWith("VT")){
			if (vals[0] > 0) {
				throw new IOException("It appears that there is a PoE board on this module, but it has been initialized as an E2.");
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  As expected, there is no PoE board on this module.");
			}
		} else {
			throw new RuntimeException("Unrecognized part number: " + devName);
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  PoE test complete");
	}
	
	private void testNVRAM() throws Exception, IOException, TimeoutException {
	    ui.displayMessage(this, UserInterface.M_WARNING, "Testing NVRAM");
		
	    ui.displayMessage(this, UserInterface.M_WARNING, "  Determining NVRAM device file");
	    
	    // Read /proc/mtd to determine what the device name for nvram is
	    // Unfortunately, being a proc file, we can't read it directly
	    // Default values in case we can't find it
	    String nvramDevice = "/dev/mtd7";
	    int size = 0x2000;
	    
	    {
	    	udr.executeCommand(S_CALBASE, "/bin/cat /proc/mtd >/tmp/mtd.out");
	    	String procMtd = new String(udr.readFile(S_CALBASE, "/tmp/mtd.out", 0, -1, null));
	    	
	    	String[] lines = Utils.getLines(procMtd);
	    	
	    	boolean found = false;
	    	for (int i = 0; i < lines.length; i++) {
	    		if (lines[i].toLowerCase().indexOf("nvram") != -1) {
	    			nvramDevice = "/dev/" + lines[i].substring(0, lines[i].indexOf(':'));
	    			
	    			String sSize = lines[i].substring(lines[i].indexOf(':') + 2);
	    			sSize = sSize.substring(0, sSize.indexOf(' '));
	    			
	    			size = Integer.parseInt(sSize, 16);
	    			
	    			found = true;
	    			break;
	    		}
	    	}
	    	
	    	if (found) {
	    		ui.displayMessage(this, UserInterface.M_NORMAL, "    Using device file '" + nvramDevice + "'");
	    	} else {
	    		ui.displayMessage(this, UserInterface.M_WARNING, "    Unable to determine device file, using '" + nvramDevice + "'");
	    	}
	    }
	    
	    ui.displayMessage(this, UserInterface.M_WARNING, "  Shutting down conflicting Sixnet services");
	    udr.executeCommand(S_CALBASE, "/usr/bin/killall sxspecfeat", 10000);
	    
	    Utils.sleep(5000); // Wait for the processes to end
	    
	    ui.displayMessage(this, UserInterface.M_WARNING, "  Writing file data to station");
	    
	    // Load a file with binary data to the station
	    // This data should be generated so that neighboring bytes, words, and (if they exist)
	    //   addressable sectors are different
	    // Binary prime numbers are a good choice, just leave out 0-bytes (maybe allow one)
	    byte[] fileData = new byte[size];
	    {
	    	File file = new File(DIR_CONFIG_ROOT + "nvram.bin");
	    	
	    	if (!file.isFile()) throw new IOException("Unable to open '" + file.getAbsolutePath() + "' as it does not exist or is not a file");
	    	
    		FileInputStream fin = new FileInputStream(file);
        	try {
        		int offset = 0;
        		while (offset < fileData.length) {
        			int bytesRead = fin.read(fileData, offset, fileData.length - offset);
        			if (bytesRead == -1) throw new IOException(String.format("The local file '%s' is not large enough, needs to be %,d bytes long", file.getAbsolutePath(), size));
        			
        			offset += bytesRead;
        		}
        	} finally {
        		fin.close();
        	}
	    }
        
	    // Sometimes it doesn't like to open the file
	    udr.executeCommand(S_CALBASE, "/bin/touch /tmp/nvram.bin");
	    udr.writeFile(S_CALBASE, "/tmp/nvram.bin", fileData, 0, size, 0, null);
	    
	    // dd that file into nvram
	    ui.displayMessage(this, UserInterface.M_WARNING, "  Writing NVRAM");
	    
		udr.executeCommand(S_CALBASE, "/bin/dd if=/tmp/nvram.bin of=" + nvramDevice + " >/dev/null 2>&1");
		
		try { // Finally block clears NVRAM
    		ui.displayMessage(this, UserInterface.M_WARNING, "  Reading NVRAM");
    		
    		// Read back to see if it's correct
    		udr.executeCommand(S_CALBASE, "/bin/dd if=" + nvramDevice + " of=/tmp/nvram.out >/dev/null 2>&1");
    		
    		// Just read the input file back this way also
    		ui.displayMessage(this, UserInterface.M_WARNING, "  Comparing");
    		
    		byte[] readBack = udr.readFile(S_CALBASE, "/tmp/nvram.out", 0, -1, null);
    		
    		// Compare
    		if (!Arrays.equals(fileData, readBack)) {
    			Utils.debug("Wrote: " + Conversion.bytesToHex(fileData));
    			Utils.debug("Read : " + Conversion.bytesToHex(readBack));
    			
    			throw new IOException("Station NVRAM data differs from what was written to it");
    		}
		} finally {
    		ui.displayMessage(this, UserInterface.M_WARNING, "  Cleaning up");
    		
    		// Zero out the nvram
    		udr.executeCommand(S_CALBASE, "/bin/dd if=/dev/zero of=" + nvramDevice + " bs=" + size + " count=1");
    		
    		// No reason to delete the files we put on the station
    		// They're in /tmp, which gets cleared on reboot
		}
		
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  NVRAM test completed");
    }
	
	private void testWatchdog() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the Watchdog output");
		
		if (chosenTest != null) {
			// Hard reset the station, since sxioprocessor may have already been killed
			ui.displayMessage(this, UserInterface.M_WARNING, "  Hard reset of station to verify state");
			testLib.resetStation(S_CALBASE, true);
		}
		
		testLib.loadBaseFiles(S_CALBASE, Dir_Watchdog, S_OUTPUTS, true);
		
		// Give it a couple seconds in case it was rebooted recently
		ui.displayMessage(this, UserInterface.M_WARNING, "  Giving the Watchdog a chance to refresh");
		Utils.sleep(6000);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Reading the Watchdog output");
		short[] vals = udr.getA(S_OUTPUTS, AI_WATCHDOG, (short)1);
		
		if (Math.abs(EXP_WATCHDOG - vals[0]) > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output): is off when it should be on (value = %,d)", AI_WATCHDOG, vals[0]));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is on as expected", AI_WATCHDOG));
		}
		
		// Make sure the watchdog output is off
		ui.displayMessage(this, UserInterface.M_WARNING, "  Killing sxioprocessor so the Watchdog will turn off");
		udr.executeCommand(S_CALBASE, "/usr/bin/killall sxioprocessor", 10000);
		
		// Give it a couple extra seconds
		ui.displayMessage(this, UserInterface.M_WARNING, "  Giving the Watchdog a chance to refresh");
		Utils.sleep(6000);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Reading the Watchdog output");
		vals = udr.getA(S_OUTPUTS, AI_WATCHDOG, (short)1);
		
		if (vals[0] > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output) is on when it should be off", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is off as expected", AI_WATCHDOG));
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Watchdog test complete");
	}
	
	private void testClock() throws Exception, IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing Real-Time Clock");
		
	    // Set the clock
		ui.displayMessage(this, UserInterface.M_WARNING, "  Setting the clock");
		udr.setClock(S_CALBASE, (int)(System.currentTimeMillis() / 1000));
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Powering off for 10 seconds");
		
		long powerDownAt;
		
		while (true) {
			if (!ui.confirm("Please power off the device", "Disconnect Power")) {
				if (ui.confirm("Really cancel the test?", "Cancel")) {
					throw new Exception("User canceled the test");
				}
			} else {
				powerDownAt = System.currentTimeMillis();
				
				try {
					testLib.verifyConnectivity(S_CALBASE);
					
					continue; // The user didn't listen
				} catch (Exception e) {
					// This is good, the user listened
					break;
				}
			}
		}
		
		// Delay for 10 seconds, minus the time spent verifying that the user actually powered it down
		Utils.sleep(10000 - (System.currentTimeMillis() - powerDownAt));
		
		while (!ui.confirm("Please power on the device", "Reconnect Power")) {
			if (ui.confirm("Really cancel the test?", "Cancel")) throw new Exception("User canceled the test");
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Waiting for the device to boot");
		Utils.sleep(60000); // Connectivity workaround
		
		boolean succeeded = false; // More connectivity workaround
		while (!succeeded) {
    		try {
    			udr.waitForResponse(S_CALBASE);
    			
    			succeeded = true;
    		} catch (TimeoutException te) {
    			while (!ui.confirm("Error: " + te.getMessage() + "\nPlease power cycle the device", "Power Cycle")) {
    				if (ui.confirm("Really cancel the test?", "Cancel")) throw new Exception("User canceled the test");
    			}
    			
    			succeeded = false;
    		}
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Checking clock");
		
		int result = udr.getClock(S_CALBASE);
		int expected = (int)(System.currentTimeMillis() / 1000);
		
		// Allow a 1-second leeway for rounding errors
		if (Math.abs(result - expected) > 1) throw new IOException(String.format("Real-time clock reports time as %d, should be %d", result, expected));
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  RTC Test Complete");
    }
	
	private void testUSB() throws Exception, IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing USB Port");
	    
		UDRLink usbHandler = null;
		
		String comPath = ui.getPersistentValue("IPm2m.defaultComPath");
		if (comPath == null) comPath = "";
		
		// Disconnect the active handler and register one for the USB port
		// This closes the handler, so we might as well discard the reference to it
		activeHandler = testLib.unregisterHandler(activeHandler);
		
		boolean succeeded = false; // Keep trying until the user says to stop
		
		try {
			do {
    			while ((comPath = ui.requestString("Please connect the USB cable and enter the name of the serial port associated with the USB connection to the device", "USB Port", comPath)) == null) {
    				if (ui.confirm("Really cancel the test?", "Cancel")) {
    					throw new Exception("User canceled the test");
    				}
    				
    				comPath = "";
    			}
    			
    			ui.setPersistentValue("IPm2m.defaultComPath", comPath);
    			
    			usbHandler = new UDRLink(TransportMethod.Serial, comPath + " 9600-8-N-1");
    			MessageDispatcher.getDispatcher().registerHandler(usbHandler);
    			
    			try {
    				testLib.verifyConnectivity(S_CALBASE);
    				
    				succeeded = true;
    			} catch (Exception e) {
    				Utils.debug(e);
    				
    				succeeded = false;
    			}
    		} while (!succeeded && ui.confirm("Unable to communicate with the device over USB, try again?", "Failed"));
		} finally {
			testLib.unregisterHandler(usbHandler);
			
			// Re-create the UDP handler
			activeHandler = testLib.registerUDPHandler(IP_CALBASE);
		}
		
		if (!succeeded) {
			throw new IOException("Unable to communicate with the device over USB");
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  USB test complete");
	}
	
	private void testDIs() throws Exception, IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		short[] aos = new short[8]; // NOTE: This is the only place where the count is specified
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing DIs");
		
		// Make sure voltage analog outputs on base3 are all off
		Arrays.fill(aos, AO_5_5V);
		udr.putA(S_STRAKIO, (short)0, (short)aos.length, aos);
		
		// There are 8 DIs, with input coming from Voltage AOs on Base3
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing guaranteed ON/OFF. You should see " + aos.length + " walking LEDs");
		Utils.sleep(2500); // Give the user time to look at the fixture
		
		// Walk the voltage analog outputs at low and high voltage
		// Loop until the user watches the LEDs
		boolean done = false;
		while (!done) {
			// We do a <= here to make sure the last input turns back off
			for (int i = 0; i <= aos.length; i++) {
				// Turn off the previous output and turn on the current one
				if (i > 0) aos[i - 1] = AO_5_5V;
				if (i < aos.length) aos[i] = AO_8_5V;
				
				udr.putA(S_STRAKIO, (short)0, (short)aos.length, aos);
				
				// Wait a moment for it to turn on
				Utils.sleep(400);
				
				// Read the inputs to make sure they look right
				boolean[] dis = udr.getD(S_CALBASE, (short)0, (short)aos.length);
				for (int j = 0; j < aos.length; j++) {
					if (dis[j]) {
						// It's on... should it be?
						if (i != j) {
							// Make sure the message isn't confusing on the last loop (8)
							if (i == dis.length) {
								mioe.add(new IOException("After turning off all outputs, DI " + j + " (0-based) is on"));
							} else {
								mioe.add(new IOException("After turning on AO " + i + " (0-based), DI " + j + " is on"));
							}
						}
					} else {
						// It's off... should it be?
						if (i == j) {
							// No need to worry about the last loop, since everything should be off
							mioe.add(new IOException("After turning on AO " + i + " (0-based), DI " + j + " is off"));
						}
					}
				}
			}
			
			// Make sure the user watched
			if (ui.confirm("Did you watch for walking DI LEDs (there were " + aos.length + ")?", "Walking LEDs")) {
				if (!ui.confirm("Did they all light?", "Walking LEDs")) mioe.add(new IOException("Missing DI LEDs during walk"));
				done = true;
			} else {
				// Don't want to display the same errors twice
				mioe.clear();
				Utils.sleep(2000); // Give the user a chance to look back over there
				done = false;
			}
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  DI tests complete");
    }
	
	private void calibrateCurrentAIs() throws IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating Current AIs");
		
		final short aiOffset;
		final short aiCount;
		
		if (devName.endsWith("101-M")) {
			aiOffset = 0;
			aiCount = 4;
		} else if (devName.endsWith("101-M-SMID-3853")) {
			aiOffset = 0;
			aiCount = 0;
		} else if (devName.endsWith("101-M-SMID-3854")) {
			aiOffset = 0;
			aiCount = 8;
		} else if (devName.endsWith("201-M")) {
			aiOffset = 0;
			aiCount = 6;
		} else {
			throw new RuntimeException("Unknown device name: " + devName);
		}
		
		if (aiCount == 0) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  No current AIs on this device");
			return;
		}
		
		// Clear any calibration data that may exist
		ui.displayMessage(this, UserInterface.M_WARNING, "  Clearing current calibration");
		
		// Write this to the 'timestamp' offset to cause the module to re-calibrate
		final byte[] recalTimestamp = { (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff };
		
		if (testLib.zeroRange(S_CALBASE, DIR_MODULECONFIG + "cals", OFFSET_AI_CALIBRATION + (aiOffset * 4), aiCount * 4)) {
			// Write all 1s to calibration timestamp to cause the module to re-calibrate
			udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", recalTimestamp, OFFSET_TIMESTAMP, null);
			
			ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
			
			// Reset the station
			testLib.resetStation(S_CALBASE, true);
			udr.waitForResponse(S_OUTPUTS);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		// There are 8 AOs on the output module, no matter how many AIs we have
		testLib.setScanDelay(100 * 8 + 200);
		
		// There are aiCount AIs coming from AOs on station 4
		short[] aos = new short[aiCount];
		
		// Calibration results will end up in these arrays
		short[] spans = new short[aos.length];
		short[] zeros = new short[aos.length];
		
		// Write 5mA to all input channels
		Arrays.fill(aos, AO_5MA);
		udr.putA(S_OUTPUTS, aiOffset, aiCount, aos);
		
		// Wait for it to take effect
		Utils.sleep(testLib.getScanDelay() * 4);
		
		// Grab the results
		short[] aisLow = testLib.getAverageAnalogs(S_CALBASE, aiOffset, aiCount, TOLERANCE_AVERAGE);
		
		// Write 18mA to all input channels
		Arrays.fill(aos, AO_18MA);
		udr.putA(S_OUTPUTS, aiOffset, aiCount, aos);
		
		// Wait for it to take effect
		Utils.sleep(testLib.getScanDelay() * 4);
		
		// Grab the results
		short[] aisHigh = testLib.getAverageAnalogs(S_CALBASE, aiOffset, aiCount, TOLERANCE_AVERAGE);
		
		// Compute the module span and zero
		testLib.computeAICurrentCalibration(aisLow, aisHigh, AO_5MA, AO_18MA, zeros, spans, aiOffset);
		
		// Write the corrections to the config file
		ui.displayMessage(this, UserInterface.M_WARNING, "  Writing calibration data to station");
		
		byte[] calsData = new byte[aiCount * 4]; // 2 values of 2 bytes apiece for each input
		int offset = 0;
		for (int i = 0; i < aiCount; i++) {
			Conversion.shortToBytes(calsData, offset, zeros[i]);
			offset += 2;
			
			Conversion.shortToBytes(calsData, offset, spans[i]);
			offset += 2;
		}
		
		udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", calsData, OFFSET_AI_CALIBRATION + (aiOffset * 4), null);
		
		// Write all 1s to calibration timestamp to cause the module to re-calibrate
		udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", recalTimestamp, OFFSET_TIMESTAMP, null);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
		
		// Reset the station
		testLib.resetStation(S_CALBASE, true);
		udr.waitForResponse(S_OUTPUTS);
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Current AI calibration complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	
	private void testCurrentAICalibration() throws IOException, TimeoutException {
		udr.waitForResponse(S_OUTPUTS);
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing current AIs in 16-bit integrating mode");
		
		final short aiOffset;
		final short aiCount;
		
		if (devName.endsWith("101-M")) {
			aiOffset = 0;
			aiCount = 4;
		} else if (devName.endsWith("101-M-SMID-3853")) {
			aiOffset = 0;
			aiCount = 0;
		} else if (devName.endsWith("101-M-SMID-3854")) {
			aiOffset = 0;
			aiCount = 8;
		} else if (devName.endsWith("201-M")) {
			aiOffset = 0;
			aiCount = 6;
		} else {
			throw new RuntimeException("Unknown device name: " + devName);
		}
		
		if (aiCount == 0) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  No current AIs on this device");
			return;
		}
		
		// Reset the device to make sure it will respond properly
		ui.displayMessage(this, UserInterface.M_WARNING, "  Hard reset to verify settings");
		testLib.resetStation(S_CALBASE, true);
		udr.waitForResponse(S_OUTPUTS);
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		// There are 8 aos, no matter how many AIs we have
		testLib.setScanDelay(100 * 8 + 200);
		
		// Set all channels low/high and check that they are each where they should be
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing all AIs at once");
		{
			short[] aos = new short[aiCount];
			
			// Write 5mA to all channels
			{
				Arrays.fill(aos, AO_5MA);
				udr.putA(S_OUTPUTS, aiOffset, aiCount, aos);
				Utils.sleep(testLib.getScanDelay() * 4);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, aiOffset, aiCount, TOLERANCE_AVERAGE);
				
				// Compare
				for (int i = 0; i < aiCount; i++) {
					String msg = formatRangeMessage("AI", i + aiOffset, ais[i], aos[i], TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
					}
				}
			}
			
			// Write 18mA to all channels
			{
				Arrays.fill(aos, AO_18MA);
				udr.putA(S_OUTPUTS, aiOffset, aiCount, aos);
				Utils.sleep(testLib.getScanDelay() * 4);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, aiOffset, aiCount, TOLERANCE_AVERAGE);
				
				// Compare
				for (int i = 0; i < aiCount; i++) {
					String msg = formatRangeMessage("AI", i + aiOffset, ais[i], aos[i], TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
					}
				}
			}
		}
		
		// Test independence of channels
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing channel independence");
		
		{
			short[] aos = new short[aiCount];
			for (int i = 0; i < aiCount; i++) {
				aos[i] = (short)((AO_18MA - AO_5MA) * i / aos.length + AO_5MA);
			}
			udr.putA(S_OUTPUTS, aiOffset, aiCount, aos);
			Utils.sleep(testLib.getScanDelay() * 4);
			
			short[] ais = testLib.getAverageAnalogs(S_CALBASE, aiOffset, aiCount, TOLERANCE_AVERAGE);
			
			// Compare
			for (int i = 0; i < aiCount; i++) {
				// A small amount of influence is allowed; a difference of 1mA
				//   is equal to an analog difference of about 2048, so as long
				//   as IndependenceTolerance is smaller than that, all is well
				String msg = formatRangeMessage("AI", i + aiOffset, ais[i], aos[i], TOLERANCE_INDEPENDENCE);
				if (Math.abs(ais[i] - aos[i]) > TOLERANCE_INDEPENDENCE) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  16-bit AI tests complete");
		
		if (!mioe.isEmpty()) throw mioe;
		
		if (aiOffset > 0) {
			// There are no 10-bit AI tests here
			return;
		}
		
		// Switch to 10-bit mode and test the calibration again on individual channels
		if (!testLib.loadBaseFiles(S_CALBASE, Dir_10bit, S_OUTPUTS, true)) {
			testLib.resetStation(S_CALBASE, true);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing current AIs in 10-bit high-speed mode");
		
		// Set all channels low/high and check that they are each where they should be
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing all AIs at once");
		{
			// High-speed are always the first two analogs
			short[] aos = new short[2];
			
			// Write 5mA to all channels
			{
				Arrays.fill(aos, AO_5MA);
				udr.putA(S_OUTPUTS, aiOffset, (short)aos.length, aos);
				Utils.sleep(testLib.getScanDelay() * 4);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, aiOffset, (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);
				
				// Compare
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i + aiOffset, ais[i], aos[i], TOLERANCE_HIGHSPEED_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
					}
				}
			}
			
			// Write 18mA to all channels
			{
				Arrays.fill(aos, AO_18MA);
				udr.putA(S_OUTPUTS, aiOffset, (short)aos.length, aos);
				Utils.sleep(testLib.getScanDelay() * 4);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, aiOffset, (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);
				
				// Compare
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i + aiOffset, ais[i], aos[i], TOLERANCE_HIGHSPEED_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
					}
				}
			}
		}
		
		// Test for channel independence
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing channel independence");
		{
			short[] aos = new short[2];
			for (int i = 0; i < aos.length; i++) {
				aos[i] = (short)((AO_18MA - AO_5MA) * i / aos.length + AO_5MA);
			}
			udr.putA(S_OUTPUTS, aiOffset, (short)aos.length, aos);
			Utils.sleep(testLib.getScanDelay() * 4);
			
			// Get the average value
			short[] ais = testLib.getAverageAnalogs(S_CALBASE, aiOffset, (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);
			
			// Compare
			for (int i = 0; i < aos.length; i++) {
				// A small amount of influence is allowed; a difference of 1mA
				//   is equal to an analog difference of about 2048, so as long
				//   as IndependenceTolerance is smaller than that, all is well
				String msg = formatRangeMessage("AI", i + aiOffset, ais[i], aos[i], TOLERANCE_HIGHSPEED_INDEPENDENCE);
				if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_INDEPENDENCE) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
				}
			}
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  10-bit AI tests complete");
	}
	
	private void calibrateVoltageAIs() throws Exception, IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating Voltage AIs");
		
		final short aiOffset;
		final short aiCount;
		
		if (devName.endsWith("101-M")) {
			aiOffset = 4;
			aiCount = 4;
		} else if (devName.endsWith("101-M-SMID-3853")) {
			aiOffset = 0;
			aiCount = 8;
		} else if (devName.endsWith("101-M-SMID-3854")) {
			aiOffset = 0;
			aiCount = 0;
		} else if (devName.endsWith("201-M")) {
			aiOffset = 0;
			aiCount = 0;
		} else {
			throw new RuntimeException("Unknown device name: " + devName);
		}
		
		if (aiCount == 0) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  No voltage AIs on this device");
			return;
		}
		
		// Write this to the timestamp offset of the cals file to cause the module to re-calibrate
		final byte[] recalTimestamp = { (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff };
		
		// Clear any calibration data that may exist
		ui.displayMessage(this, UserInterface.M_WARNING, "  Clearing current calibration");
		
		if (testLib.zeroRange(S_CALBASE, DIR_MODULECONFIG + "cals", OFFSET_AI_CALIBRATION + (aiOffset * 4), aiCount * 4)) {
			// Write all 1s to calibration timestamp to cause the module to re-calibrate
			udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", recalTimestamp, OFFSET_TIMESTAMP, null);
			
			ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
			testLib.resetStation(S_CALBASE, true);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		// There are 8 AI channels, even if there are fewer voltage AIs
		testLib.setScanDelay(100 * 8 + 200);
		
		// Calibration results will end up in these arrays
		short[] spans = new short[aiCount];
		short[] zeros = new short[aiCount];
		
		// Ask the user to set the input to 9.8V
		while (!ui.confirm("Please set the voltage regulator to output 9.800 V", "Set Voltage")) {
			if (ui.confirm("Really cancel the test?", "Cancel")) {
				throw new Exception("User canceled the test");
			}
		}
		
		// Grab the results
		short[] aisHigh = testLib.getAverageAnalogs(S_CALBASE, aiOffset, aiCount, TOLERANCE_AVERAGE);
		
		// Ask the user to set the input to 0.2V
		while (!ui.confirm("Please set the voltage regulator to output 0.200 V", "Set Voltage")) {
			if (ui.confirm("Really cancel the test?", "Cancel")) {
				throw new Exception("User canceled the test");
			}
		}
		
		// Grab the results
		short[] aisLow = testLib.getAverageAnalogs(S_CALBASE, aiOffset, aiCount, TOLERANCE_AVERAGE);
		
		// Compute the module span and zero
		testLib.computeAIVoltageCalibration(aisLow, aisHigh, AI_0_2V, AI_9_8V, zeros, spans, aiOffset);
		
		// Write the corrections to the config file
		ui.displayMessage(this, UserInterface.M_WARNING, "  Writing calibration data to station");
		
		byte[] calsData = new byte[aiCount * 4]; // 2 values of 2 bytes apiece for each input
		int offset = 0;
		for (int i = 0; i < aiCount; i++) {
			Conversion.shortToBytes(calsData, offset, zeros[i]);
			offset += 2;
			
			Conversion.shortToBytes(calsData, offset, spans[i]);
			offset += 2;
		}
		
		udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", calsData, OFFSET_AI_CALIBRATION + (aiOffset * 4), null);
		
		// Write all 1s to calibration timestamp to cause the module to re-calibrate
		udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", recalTimestamp, OFFSET_TIMESTAMP, null);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
		
		// Reset the station
		testLib.resetStation(S_CALBASE, true);
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Voltage AI calibration complete");
		
		if (!mioe.isEmpty()) throw mioe;
    }
	
	private void testVoltageAICalibration() throws Exception, IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing voltage AIs in 16-bit integrating mode");
		
		final short aiOffset;
		final short aiCount;
		
		if (devName.endsWith("101-M")) {
			aiOffset = 4;
			aiCount = 4;
		} else if (devName.endsWith("101-M-SMID-3853")) {
			aiOffset = 0;
			aiCount = 8;
		} else if (devName.endsWith("101-M-SMID-3854")) {
			aiOffset = 0;
			aiCount = 0;
		} else if (devName.endsWith("201-M")) {
			aiOffset = 0;
			aiCount = 0;
		} else {
			throw new RuntimeException("Unknown device name: " + devName);
		}
		
		if (aiCount == 0) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  No voltage AIs on this device");
			return;
		}
		
		// Reset the device to make sure it will respond properly
		ui.displayMessage(this, UserInterface.M_WARNING, "  Hard reset to verify settings");
		testLib.resetStation(S_CALBASE, true);
		udr.waitForResponse(S_OUTPUTS);
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		// There are 8 AI channels, even if there are fewer voltage AIs
		testLib.setScanDelay(100 * 8 + 200);
		
		// Set all channels low/high and check that they are each where they should be
		{
			// First try at 0.2V, since that's probably where the regulator is still set
			// Ask the user to set the input to 0.2V
			ui.displayMessage(this, UserInterface.M_WARNING, "  Testing at 0.200 V");
			while (!ui.confirm("Please set the voltage regulator to output 0.200 V", "Set Voltage")) {
				if (ui.confirm("Really cancel the test?", "Cancel")) {
					throw new Exception("User canceled the test");
				}
			}
			
			// Read all channels
			{
				Utils.sleep(testLib.getScanDelay() * 4);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, aiOffset, aiCount, TOLERANCE_AVERAGE);
				
				// Compare
				// Every channel should be approximately AI_0_2v
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i + aiOffset, ais[i], AI_0_2V, TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - AI_0_2V) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
					}
				}
			}
			
			// Switch to 9.8V and try again
			ui.displayMessage(this, UserInterface.M_WARNING, "  Testing at 9.800 V");
			while (!ui.confirm("Please set the voltage regulator to output 9.800 V", "Set Voltage")) {
				if (ui.confirm("Really cancel the test?", "Cancel")) {
					throw new Exception("User canceled the test");
				}
			}
			
			// Read all channels
			{
				Utils.sleep(testLib.getScanDelay() * 4);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, aiOffset, aiCount, TOLERANCE_AVERAGE);
				
				// Compare
				// Every channel should be approximately AI_9_8v
				for (int i = 0; i < aiCount; i++) {
					String msg = formatRangeMessage("AI", i + aiOffset, ais[i], AI_9_8V, TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - AI_9_8V) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
					}
				}
			}
		}
						
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  16-bit AI tests complete");
		
		if (aiOffset > 0) {
			// There will be no 10-bit mode here
			return;
		}
		
		// Switch to 10-bit mode and test the calibration again on individual channels
		if (!testLib.loadBaseFiles(S_CALBASE, Dir_10bit, S_OUTPUTS, true)) {
			testLib.resetStation(S_CALBASE, true);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing current AIs in 10-bit high-speed mode");
		
		// Set all channels low/high and check that they are each where they should be
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing all AIs at once");
		{
			// High-speed are always the first two analogs
			short[] aos = new short[2];
			
			// Test at 0.2V
			{
				while (!ui.confirm("Please set the voltage regulator to 0.200 V", "Voltage")) {
					if (ui.confirm("Really cancel the test?", "Cancel")) {
						throw new Exception("User canceled the test");
					}
				}
				Arrays.fill(aos, AI_0_2V); // For simplicity
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, aiOffset, (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);
				
				// Compare
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i + aiOffset, ais[i], aos[i], TOLERANCE_HIGHSPEED_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
					}
				}
			}
			
			// Test at 9.8V
			{
				while (!ui.confirm("Please set the voltage regulator to 9.800 V", "Voltage")) {
					if (ui.confirm("Really cancel the test?", "Cancel")) {
						throw new Exception("User canceled the test");
					}
				}
				Arrays.fill(aos, AI_9_8V);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, aiOffset, (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);
				
				// Compare
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i + aiOffset, ais[i], aos[i], TOLERANCE_HIGHSPEED_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
					}
				}
			}
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  10-bit AI tests complete");
    }
	
	private void calibrateCurrentAOs() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating AOs");
		
		final short aoCount;
		
		if (devName.endsWith("201-M")) {
			aoCount = 2;
		} else {
			aoCount = 0;
		}
		
		if (aoCount == 0) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  No AOs on this device");
			return;
		}
		
		// Write this to the timestamp offset of the cals file to cause the module to re-calibrate
		final byte[] recalTimestamp = { (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff };
		
		// Clear any calibration data that may exist
		ui.displayMessage(this, UserInterface.M_WARNING, "  Clearing current calibration");
		
		if (testLib.zeroRange(S_CALBASE, DIR_MODULECONFIG + "cals", OFFSET_AO_CALIBRATION, LENGTH_AO_CALIBRATION)) {
			// Write all 1s to calibration timestamp to cause the module to re-calibrate
			udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", recalTimestamp, OFFSET_TIMESTAMP, null);
			
			ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
			testLib.resetStation(S_CALBASE, true);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");
		
		short[] aos = new short[aoCount];
		
		// Calibration results will end up in these arrays
		short[] spans = new short[aoCount];
		short[] zeros = new short[aoCount];
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		// We are using 8 channels for the scan delay because there are 8 AIs, even if there
		//   are fewer AOs
		testLib.setScanDelay(100 * 8 + 200);
		
		// Write 4ma to each channel and read the results
		Arrays.fill(aos, AO_4MA);
		udr.putA(S_CALBASE, (short)0, aoCount, aos);
		
		// Sleep for a bit so it can stabilize
		Utils.sleep(testLib.getScanDelay() * 4);
		
		// Read back what it is actually outputting
		short[] aisLow = testLib.getAverageAnalogs(S_OUTPUTS, (short)0, aoCount, TOLERANCE_AVERAGE);
		
		// Write 18mA to all input channels
		Arrays.fill(aos, AO_18MA);
		udr.putA(S_CALBASE, (short)0, aoCount, aos);
		
		// Wait for it to take effect
		Utils.sleep(testLib.getScanDelay() * 4);
		
		// Grab the results
		short[] aisHigh = testLib.getAverageAnalogs(S_OUTPUTS, (short)0, aoCount, TOLERANCE_AVERAGE);
		
		// Compute the module span and zero
		testLib.computeAOCurrentCalibration(aisLow, aisHigh, zeros, spans);
		
		// Write the corrections to the config file
		ui.displayMessage(this, UserInterface.M_WARNING, "  Writing calibration data to station");
		
		byte[] calsData = new byte[aoCount * 4]; // 2 values of 2 bytes apiece for each input
		int offset = 0;
		for (int i = 0; i < aos.length; i++) {
			Conversion.shortToBytes(calsData, offset, zeros[i]);
			offset += 2;
			
			Conversion.shortToBytes(calsData, offset, spans[i]);
			offset += 2;
		}
		
		udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", calsData, OFFSET_AO_CALIBRATION, null);
		
		// Write all 1s to calibration timestamp to cause the module to re-calibrate
		udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", recalTimestamp, OFFSET_TIMESTAMP, null);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
		
		// Reset the station
		testLib.resetStation(S_CALBASE, true);
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  AO calibration complete");
	}
	
	private void testCurrentAOCalibration() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AOs");
		
		final short aoCount;
		
		if (devName.endsWith("201-M")) {
			aoCount = 2;
		} else {
			aoCount = 0;
		}
		
		if (aoCount == 0) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  No AOs on this device");
			return;
		}
		
		MultiIOException mioe = new MultiIOException();
		
		short[] aos = new short[aoCount];
		
		// Set all channels low/high and check that they are each where they should be
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing all AOs at once");
		{
			// There are 8 AIs reading the AOs, even if there are fewer AOs
			testLib.setScanDelay(100 * 8 + 200);
			
			// Write 5mA to all channels
			ui.displayMessage(this, UserInterface.M_WARNING, "    Low");
			{
				Arrays.fill(aos, AO_5MA);
				udr.putA(S_CALBASE, (short)0, aoCount, aos);
				
				Utils.sleep(testLib.getScanDelay() * 4);
				
				short[] ais = testLib.getAverageAnalogs(S_OUTPUTS, (short)0, aoCount, TOLERANCE_AVERAGE);
				
				// Compare
				for (int i = 0; i < aoCount; i++) {
					String msg = formatRangeMessage("AO", i, ais[i], aos[i], TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "      " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "      " + msg);
					}
				}
			}
			
			// Write 18mA to all channels
			ui.displayMessage(this, UserInterface.M_WARNING, "    High");
			{
				Arrays.fill(aos, AO_18MA);
				udr.putA(S_CALBASE, (short)0, aoCount, aos);
				
				Utils.sleep(testLib.getScanDelay() * 4);
				
				short[] ais = testLib.getAverageAnalogs(S_OUTPUTS, (short)0, aoCount, TOLERANCE_AVERAGE);
				
				// Compare
				for (int i = 0; i < aoCount; i++) {
					String msg = formatRangeMessage("AO", i, ais[i], aos[i], TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "      " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "      " + msg);
					}
				}
			}
		}
		
		// Test channel independence
		{
			ui.displayMessage(this, UserInterface.M_WARNING, "  Testing channel independence");
			for (int i = 0; i < aoCount; i++) {
				aos[i] = (short)((AO_18MA - AO_5MA) * i / aoCount + AO_5MA);
			}
			
			udr.putA(S_CALBASE, (short)0, (short)aos.length, aos);
			
			// Sleep so it can stabilize
			Utils.sleep(testLib.getScanDelay() * 4);
			
			// Get the average value
			short[] ais = testLib.getAverageAnalogs(S_OUTPUTS, (short)0, aoCount, TOLERANCE_AVERAGE);
			
			// Compare
			for (int i = 0; i < aoCount; i++) {
				// A small amount of influence is allowed; a difference of 1mA
				//   is equal to an analog difference of about 2048, so as long
				//   as IndependenceTolerance is smaller than that, all is well
				String msg = formatRangeMessage("AO", i + 1, ais[i], aos[i], TOLERANCE_INDEPENDENCE);
				if (Math.abs(ais[i] - aos[i]) > TOLERANCE_INDEPENDENCE) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
				}
			}
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  AO tests complete");
	}
	
	private void testSerial() throws Exception, IOException, TimeoutException {
		// We will be changing these settings, so back them up now
		long oldTimeout = udr.getMessageTimeout();
		
		MultiIOException mioe = new MultiIOException();
	    
	    ui.displayMessage(this, UserInterface.M_WARNING, "Testing Serial I/O Lines");
	    
	    // Load an I/O transfer into the base to grab inputs/push outputs to the ST stations
	    ui.displayMessage(this, UserInterface.M_WARNING, "  Loading action.6tl");
	    
	    udr.copyFile(S_TESTBASE, new File(Dir_IOTransfers + File.separator + "SerTestTransfers.6tl"), "/tmp/actions");
	    udr.executeCommand(S_TESTBASE, "cp -f /tmp/actions /etc/stacfg/action.6tl");
	    testLib.resetStation(S_TESTBASE); // Cause it to re-read action.6tl
	    
	    ui.displayMessage(this, UserInterface.M_WARNING, "  Waiting for station to respond");
	    
	    udr.waitForResponse(S_TESTBASE);
	    
	    ui.displayMessage(this, UserInterface.M_NORMAL, "    Done");
	    
	    // Load the sertest program into the device
    	ui.displayMessage(this, UserInterface.M_WARNING, "  Loading the sertest program into the station");
    	udr.copyFile(S_TESTBASE, new File(DIR_CONFIG_ROOT + "sertest.arm9"), "/tmp/sertest");
	    
	    // Make it executable
	    udr.executeCommand(S_TESTBASE, "/bin/chmod 755 /tmp/sertest");
	    
	    ui.displayMessage(this, UserInterface.M_NORMAL, "    Done");
	    
	    try { // Finally block clears the I/O transfer
    	    // Lengthen the message timeout and don't re-send commands
    	    udr.setMessageTimeout(30000);
    	    
    	    ui.displayMessage(this, UserInterface.M_WARNING, "  Running the sertest program");
    	    
    	    // Run the program
    	    udr.executeCommand(S_TESTBASE, "/tmp/sertest /dev/ttyS2 LOOPBACK DTR CD CTS RTS DSRRI MAXTRIES=50 >/tmp/sertest.out 2>&1");
    	    
    	    // Read the results
    	    ui.displayMessage(this, UserInterface.M_WARNING, "  Examining results");
    	    
    	    int[] len = new int[1];
    	    udr.statFile(S_TESTBASE, "/tmp/sertest.out", len, null, null, null);
    	    byte[] data = udr.readFile(S_TESTBASE, "/tmp/sertest.out", 0, len[0], null);
    	    
    	    // Throw an exception if the output is not as expected
    	    String[] lines = Utils.getLines(new String(data));
    	    for (int i = 0; i < lines.length; i++) {
    	    	if (lines[i].indexOf("FAIL") != -1) {
    	    		mioe.add(new IOException("Complete output:\n" + new String(data)));
    	    		break;
    	    	} else if (lines[i].indexOf("No such file") != -1 ||
    	    			   lines[i].indexOf("Text file busy") != -1 ||
    	    			   lines[i].indexOf("usage") != -1) {
    	    		
    	    		mioe.add(new IOException("Error running test program"));
    	    	}
    	    }
    	    
    	    if (mioe.isEmpty()) {
    	    	ui.displayMessage(this, UserInterface.M_NORMAL, "    Done");
    	    }
	    } catch (IOException ioe) {
	    	Utils.debug("Error running sertest");
	    	Utils.debug(ioe);
	    	
	    	mioe.add(ioe);
	    } finally {
	    	udr.setMessageTimeout(oldTimeout);
	    }
	    
	    try {
    	    ui.displayMessage(this, UserInterface.M_WARNING, "  Cleaning up");
        	
        	udr.copyFile(S_TESTBASE, new File(Dir_IOTransfers + File.separator + "ClearTransfers.6tl"), "/tmp/actions");
        	udr.executeCommand(S_TESTBASE, "cp -f /tmp/actions /etc/stacfg/action.6tl");
	    } catch (IOException ioe) {
	    	Utils.debug("Error cleaning up from sertest");
	    	Utils.debug(ioe);
	    	mioe.add(ioe);
	    }
    	
    	// Don't worry about resetting the station so it takes effect, it'll happen on the next boot
    	// Also no point in clearing out /tmp, since it will clear itself on next boot
	    
	    if (!mioe.isEmpty()) throw mioe;
	    
	    ui.displayMessage(this, UserInterface.M_NORMAL, "  Serial Test Complete");
    }
	
	private void testSourcing() throws Exception, IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing Sourcing DIs");
		
		// Only testing the sourcing DIs, which are the last 4
		final short diOffset = 4;
		final short diCount = 4;
		
		// Set DI-COM switch to DC+ (Sinking)
		while (!ui.confirm("Please set the DI-COM switch to the DC+ position", "DI-COM Switch")) {
			if (ui.confirm("Really cancel the test?", "Cancel")) {
				throw new Exception("User canceled the test");
			}
		}
		
		// Poke PA8 to cause a reset of the I/O
		ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting the I/O processor");
		
		udr.executeCommand(S_TESTBASE, "echo 0 >" + DEV_RESET, 15000);
		Utils.sleep(500);
		udr.executeCommand(S_TESTBASE, "echo 1 >" + DEV_RESET, 15000);
		Utils.sleep(8000); // This takes a moment
		
		// Grab the discrete inputs
		ui.displayMessage(this, UserInterface.M_WARNING, "  Checking the DIs");
		
		boolean[] dis = udr.getD(S_TESTBASE, diOffset, diCount);
		
		// Check them
		for (int i = 0; i < diCount; i++) {
			if (!dis[i]) {
				mioe.add(new IOException(String.format("Sourcing DI %d (0-based) should be on but it is off", i + diOffset)));
			}
		}
		
		// Ask user to check DI LEDs on module
		if (!ui.confirm(String.format("Are DI LEDs %d-%d ON", diOffset + 1, diOffset + diCount), "Sinking LEDs")) {
			mioe.add(new IOException("Missing LEDs in sourcing test"));
		}
		
		// Set DI-COM switch to DC- (Sourcing)
		while (!ui.confirm("Please set the DI-COM switch to the DC- position", "DI-COM Switch")) {
			if (ui.confirm("Really cancel the test?", "Cancel")) {
				throw new Exception("User canceled the test");
			}
		}
		
		// Poke PA8 to cause a reset of the I/O
		ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting the I/O processor");
		
		udr.executeCommand(S_TESTBASE, "echo 0 >" + DEV_RESET, 15000);
		Utils.sleep(500);
		udr.executeCommand(S_TESTBASE, "echo 1 >" + DEV_RESET, 15000);
		Utils.sleep(8000); // This takes a moment
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Sourcing DI tests complete");
	}
	
	private void testDOs() throws IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing DOs");
		
		// There are 4 DOs, with output going to our own inputs
		final short doOffset = 0;
		final short doCount = 4;
		
		boolean[] dos = new boolean[doCount];
		
		// Start out by setting everything off
		Arrays.fill(dos, false);
		udr.putD(S_TESTBASE, doOffset, doCount, dos);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Please watch for " + doCount + " walking DO LEDs");
		Utils.sleep(2500); // Give the user a chance to look
		
		// Loop through, turning each on and then off
		boolean done = false;
		while (!done) {
    		for (int i = 0; i <= doCount; i++) {
    			// Turn off the previous bit
    			if (i > 0) dos[i - 1] = false;
    			
    			// Turn on the current bit
    			if (i < dos.length) dos[i] = true;
    			
    			udr.putD(S_TESTBASE, doOffset, doCount, dos);
    			
    			// Wait for the electronics to do their thing and for the user to see it
    			Utils.sleep(500);
    			
    			// Read the DIs to see if they match
    			// This assumes that DO 0 links to DI 0, 1 to 1, and so on
    			boolean[] dis = udr.getD(S_TESTBASE, doOffset, doCount);
    			
    			for (int j = 0; j < doCount; j++) {
    				if (dis[j] != dos[j]) {
    					if (dis[j]) {
    						mioe.add(new IOException(String.format("Station reports DI %d (0-based) is on when corresponding DO %d is off", j + doOffset, i + doOffset)));
    					} else {
    						mioe.add(new IOException(String.format("Station reports DI %d (0-based) is off when corresponding DO %d is on", j + doOffset, i + doOffset)));
    					}
    				}
    	    	}	
    		}
			
    		// Make sure the user watched
    		if (ui.confirm("Did you watch for walking DO LEDs (there were " + doCount + ")?", "Walking LEDs")) {
    			if (!ui.confirm("Did they all light?", "Walking LEDs")) mioe.add(new IOException("Missing DO LEDs during walk"));
    			
    			done = true;
    		} else {
    			Utils.sleep(2000); // Give the user a chance to look back over there
    			done = false;
			}
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Finished testing DOs");
	}
	
	// private void checkCounters() throws IOException, TimeoutException
	private void testCounters() throws IOException, TimeoutException {
		// Turn off the outputs feeding the counters
		boolean[] dos = new boolean[2];
		Arrays.fill(dos, false);
		udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);
		
		Utils.sleep(1000); // Make sure the off value takes effect
		
		// Turn on counters
		if (!testLib.loadBaseFiles(S_TESTBASE, Dir_Counters, S_TESTBASE, true)) {
			// Make sure the counters are zeroed
			testLib.resetStation(S_TESTBASE, true);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing counter registers");
		
		// Toggle DIs 1 and 2 a few times each (different number for each to make sure there is no cross-talk)
		// DIs 1 and 2 are connected to fixed Onboard DOs 1 and 2
		final int counter1 = 3;
		final int counter2 = 7;
		
		int i = 0;
		while (i < counter1 || i < counter2) {
			// Turn them off
			Arrays.fill(dos, false);
			udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);
			
			// Wait a moment for it to register
			Utils.sleep(100);
			
			// Turn them on
			if (i < counter1) {
				dos[0] = true; // ON
			}
			
			if (i < counter2) {
				dos[1] = true; // ON
			}
			
			udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);
			
			// And wait another moment...
			Utils.sleep(100);
			
			// Increment i and loop around
			i++;
		}
		
		short[] ais;
		if (devName.endsWith("201-M")) {
			// Grab analog inputs 6 and 7, they hold the counts
			ais = udr.getA(S_TESTBASE, (short)6, (short)dos.length);
		} else {
			// Grab analog inputs 8 and 9, they hold the counts
			ais = udr.getA(S_TESTBASE, (short)8, (short)dos.length);
		}
		
		// Allow leeway for one extra count, due to a spike during power-on
		if ((ais[0] != counter1 && ais[0] != counter1 + 1) ||
			(ais[1] != counter2 && ais[1] != counter2 + 1)) {
			
			throw new IOException(String.format("Expected cnt1 = %d (or +1) and cnt2 = %d (or +1), but found cnt1 = %d and cnt2 = %d", counter1, counter2, ais[0], ais[1]));
		}
				
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Counter tests complete");
	}
	
	private void testVoltageAIIndependence() throws Exception, IOException, TimeoutException {
	    MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing voltage AIs in 16-bit integrating mode");
		
		final short aiOffset;
		final short aiCount;
		
		if (devName.endsWith("101-M")) {
			aiOffset = 4;
			aiCount = 4;
		} else if (devName.endsWith("101-M-SMID-3853")) {
			aiOffset = 0;
			aiCount = 8;
		} else if (devName.endsWith("101-M-SMID-3854")) {
			aiOffset = 0;
			aiCount = 0;
		} else if (devName.endsWith("201-M")) {
			aiOffset = 0;
			aiCount = 0;
		} else {
			throw new RuntimeException("Unknown device name: " + devName);
		}
		
		if (aiCount == 0) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  No voltage AIs on this device");
			return;
		}
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		// There are 8 analog inputs, even if fewer are being tested here
		testLib.setScanDelay(100 * 8 + 200);
		
		// Set all channels high and check that they are each where they should be
		{
			// Ask the user to set the voltage at 9.8V
			// Since it will still be there from the calibration check if the user is doing a usual test,
			//   only ask if he is doing this one specifically
			if (chosenTest != null) {
    			if (!ui.confirm("Please set the voltage regulator to output 9.800 V", "Set Voltage")) {
    				throw new Exception("User canceled the test");
    			}
			}
			
			// Read all channels
			{
				Utils.sleep(testLib.getScanDelay() * 4);
				
				short[] ais = testLib.getAverageAnalogs(S_TESTBASE, aiOffset, aiCount, TOLERANCE_AVERAGE);
				
				// Note: This works with a series of resistors that do not quite provide a linear
				//   step-down voltage across each channel, so we use some extra "fudge factors" to
				//   make up for it
				//   - leeway acts as an increasing extra bit of tolerance to make up for the non-linearity
				//   - the first channel is the absolute expected value from the voltage source, so its
				//       "delta" value will not be on the same curve as the rest of the channels
				//   - each channel's expected value is based on the previous channel
				//   - the last channel is checked to make sure the curve or the last channel did not drop
				//       too quickly
				
				// NOTE: This is written specifically to test multiple blocks of values that should
				//   approximately mirror each other
				
				// Compare the lower half (or everything, if there aren't so many voltage AIs)
				int blockCount = (aiCount == 8 ? 2 : 1);
				
				for (int blockIndex = 1; blockIndex <= blockCount; blockIndex++) {
					int startOfBlock = (blockIndex - 1) * aiCount / blockCount;
					int endOfBlock = blockIndex * aiCount / blockCount;
					
    				int maxValidValue = AI_9_8V + TOLERANCE_INDEPENDENCE;
    				int leeway = 0;
    				int leewayChange = 12;
    				for (int i = startOfBlock; i < endOfBlock; i++) {
    					String op = (ais[i] <= maxValidValue ? TestLib.CH_LESS_EQUAL : ">");
    					String msg = String.format("AI Channel %,d (0-based): %s%,d [%,d (%s %,d)]", i + aiOffset, TestLib.CH_DELTA, maxValidValue - ais[i], ais[i], op, maxValidValue);
    					if (ais[i] > maxValidValue) {
    						mioe.add(new IOException(msg));
    						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
    					} else {
    						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
    					}
    					
    					// Each channel should be approximately 7945 below the previous
    					maxValidValue = ais[i] - 7945 + TOLERANCE_ANALOG + leeway;
    					leeway += leewayChange;
    				}
    				
    				// Make sure the last channel is reasonable
    				if (ais[aiCount - 1] < 7945) {
    					String msg = String.format("AI Channel %,d (0-based) is too low, check all channel values", aiOffset + aiCount - 1);
    					ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
    					mioe.add(new IOException(msg));
    				}
				}
			}
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		if (aiOffset > 0) {
			// There are no 10-bit AIs to test
			return;
		}
		
		// Switch to 10-bit mode and test the calibration again on individual channels
		if (!testLib.loadBaseFiles(S_TESTBASE, Dir_10bit2, S_TESTBASE, true)) {
			testLib.resetStation(S_CALBASE, true);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AIs in 10-bit high-speed mode");
		
		// Set all channels low/high and check that they are each where they should be
		{
			// The voltage regulator is already at 9.8V, so no need to change it
			// Now that we're only interested in the first two channels (the only ones with 10-bit mode),
			//   reset aiCount
			
			// Read the channels
			{
				Utils.sleep(testLib.getScanDelay() * 4);
				
				short[] ais = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)2, TOLERANCE_HIGHSPEED_AVERAGE);
				
				// Compare
				int maxValidValue = AI_9_8V + TOLERANCE_HIGHSPEED_ANALOG;
				int leeway = 0;
				int leewayChange = 3;
				for (int i = 0; i < ais.length; i++) {
					String op = (ais[i] <= maxValidValue ? TestLib.CH_LESS_EQUAL : ">");
					String msg = String.format("AI Channel %,d (0-based): %s%,d [%,d (%s %,d)]", i, TestLib.CH_DELTA, maxValidValue - ais[i], ais[i], op, maxValidValue);
					if (ais[i] > maxValidValue) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
					
					// Each channel should be approximately 995 below the previous
					maxValidValue = ais[i] - 995 + TOLERANCE_HIGHSPEED_ANALOG + leeway;
					leeway += leewayChange;
				}
				
				// Make sure the last channel is reasonable
				if (ais[aiCount - 1] < 30100) {
					String msg = String.format("AI Channel %,d (0-based) is too low, check all channel values", aiCount - 1);
					ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					mioe.add(new IOException(msg));
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  10-bit AI tests complete");
		
		if (!mioe.isEmpty()) throw mioe;
    }
	
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
	
	private String formatRangeMessage(String type, int channel, short found, short expected, int tolerance) {
		String op = TestLib.CH_MEMBEROF;
		if (Math.abs(expected - found) > tolerance) op = TestLib.CH_NOTMEMBEROF;
		
		return String.format("%s Channel %,d: %s%,d [%,d %s (%,d +/- %,d)]", type, channel, TestLib.CH_DELTA, found - expected, found, op, expected, tolerance);
	}
}

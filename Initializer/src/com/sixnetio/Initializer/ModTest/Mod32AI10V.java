/*
 * Mod32AI10V.java
 *
 * Tests a 32AI10V module.
 *
 * Jonathan Pearson
 * August 19, 2008
 *
 */

package com.sixnetio.Initializer.ModTest;

import com.sixnetio.Initializer.*;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class Mod32AI10V implements Tester {
	// Constants
	// Locations of the configuration files
	// These get loaded into the calibration base
	private static final String DIR_CALBASE = "TestData" + File.separator + "AI10V" + File.separator + "Base1";
	private static final String DIR_WATCHDOG_OFF = "TestData" + File.separator + "AI10V" + File.separator + "WatchdogOff";
	private static final String DIR_10BIT1 = "TestData" + File.separator + "AI10V" + File.separator + "10bit1";
	
	// These get loaded into the verification base
	private static final String DIR_TESTBASE = "TestData" + File.separator + "AI10V" + File.separator + "Base2";
	private static final String DIR_10BIT2 = "TestData" + File.separator + "AI10V" + File.separator + "10bit2";
	
	private static final String T_DIAG1 = "Diagnostics Test (Base 1)",
	                            T_AICAL = "AI Calibration",
	                            T_CAL_CHECK = "Calibration Check",
	                            T_POE = "PoE Test",
	                            T_WATCHDOG = "Watchdog Test",
	                            T_DIAG2 = "Diagnostics Test (Base 2)",
	                            T_IND_CHECK = "Independence Check";
	                            
	
	private static final String[] TEST_NAMES = {
		// Base 1
		T_DIAG1,
		T_AICAL,
		T_CAL_CHECK,
		T_POE,
		T_WATCHDOG,
		
		// Base 2
		T_DIAG2,
		T_IND_CHECK
	};
	
	// Try to keep every test module using a different set of IP addresses, just in case we figure out
	//   a way to run multiple fixtures from the same machine at the same time
	private static final String IP_CALBASE = "10.1.10.1";
	private static final String IP_TESTBASE = "10.1.10.2";
	
	private static final short S_CALBASE = 1; // Calibration
	private static final short S_TESTBASE = 2; // Verification
	private static final short S_DIAGS = 3; // 16AI in charge of reading POE and Watchdog outputs
	
	
	// Which AI the watchdog output terminal is plugged into
	private static final short AI_WATCHDOG = 1; // (0-based)
	
	// Which AI the POE output terminal is plugged into
	private static final short AI_POE = 0; // 0-based
	
	// Diagnostic DI offsets from DI_DiagStart
	private static final short DI_DIAG_START = 32; // The first diagnostic DI
	private static final short DIAG_SELFTEST = 0, // Module Selftest
	                           DIAG_POWER1 = 1, // Power 1
	                           DIAG_POWER2 = 2, // Power 2
	                           DIAG_IOSTATUS = 3, // I/O Status
	                           DIAG_ETH1LINK = 4, // Ethernet 1 has link
	                           DIAG_ETH2LINK = 5, // Ethernet 2 has link
	                           DIAG_RING = 6, // Ring is complete
	                           DIAG_POE = 7; // PoE is available
	
	// Analog values (to avoid hard-coding them in multiple places)
	private static final short EXP_WATCHDOG = (short)3686; // Expected watchdog input value; 3686 = 5.8mA = 23.2V across 4000 Ohms
	private static final short EXP_POE = (short)4096; // Expected PoE input value; 4096 = 6mA = .6V across a 100 Ohm resistor
	private static final short AI_0_2V = (short)655; // Expected 0.200V AI value
	private static final short AI_9_8V = (short)32113; // Expected 9.800V AI value
	
	// Tolerance for analog values
	// If two analog values should be equal, values within this distance on either side will be accepted
	private static final int TOLERANCE_ANALOG = 25;
	
	// Similar to AnalogTolerance, this is used to make sure consecutive scans during an average are close enough
	private static final int TOLERANCE_AVERAGE = 50;
	
	// Used to check that the Watchdog input is close to what it should be
	private static final int TOLERANCE_WATCHDOG = 450;
	
	// Used to check that the PoE input is close to what it should be
	private static final int TOLERANCE_POE = 600;
	
	// Same as Analog/Average/Independence Tolerance, but these are used for 10-bit high-speed
	//   mode, which is wildly less accurate (6 fewer bits means +/- 64 for one analog point)
	// Allowed to be 12 analog points off, so 768 makes sense (12 * 64)
	private static final int TOLERANCE_HIGHSPEED_ANALOG = 768;
	
	// 768 below, then 768 above means 2 * 768 range across consecutive values
	private static final int TOLERANCE_HIGHSPEED_AVERAGE = 1536;
	
	// Base directory in a module where we will find the configuration files
	// We expect a slash on the end of this
	private static final String DIR_MODULECONFIG = "/module0/arc/";
	
	// Offset/length of the AI calibration section of the module cals file
	private static final int OFFSET_AI_CALIBRATION = 0x100;
	private static final int LENGTH_AI_CALIBRATION = 0x80;
	
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private UDRLink activeHandler;
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	
	public Mod32AI10V() { }
	
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for Mod32AI10V must not be null");
		
		// Make sure the device name is recognized
		// Must start with E2 or EB
		// Must end with 32AI10V
		if (!((devName.startsWith("E2") || devName.startsWith("EB")) &&
		      devName.endsWith("32AI10V"))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)106, (byte)0);
		testLib = new TestLib(udr, ui);
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
	}
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
	}
	
	public boolean supportsTestJump() {
		return true;
	}
	
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
	}
	
	public String getJumpTest() {
		return chosenTest;
	}
	
	public void run() {
		MultiIOException mioe = new MultiIOException();
		
		if (chosen(T_DIAG1) ||
			chosen(T_AICAL) ||
			chosen(T_CAL_CHECK) ||
			chosen(T_POE) ||
			chosen(T_WATCHDOG)) {
			
			if (!ui.confirm("Please plug the module into the first base", "Module Check")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "User canceled the test");
				ui.operationFailed(this);
				return;
			}
			
    		// Register a UDP handler for base 1
    		try {
    			activeHandler = testLib.registerUDPHandler(IP_CALBASE);
    			
    			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
    			udr.waitForResponse(S_CALBASE);
    			
    			// Make sure it works
    			testLib.loadBaseFiles(S_CALBASE, DIR_CALBASE);
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		}
    		
    		try { // Finally block unregisters the handler
    			// Now start doing tests
    			// Diagnostics test 1
    			try {
    				if (chosen(T_DIAG1)) checkDiags1();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    			
    			// AI Calibration/test
    			try {
    				if (chosen(T_AICAL)&&(!runVerifyTest)) calibrateAIs();
    				
    				// This really does belong here
    				// This makes sure that the AIs work properly with the calibration that we just made
    				// There is an 'independence' check on the other base that makes sure that none of
    				//   the AI channels are bussed together
    				if (chosen(T_CAL_CHECK)) testCalibration(); // If the calibration fails, don't do the test
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    			
    			// POE test
    			try {
    				if (chosen(T_POE)) checkPOE();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    			
    			// Watchdog test
    			try {
    				if (chosen(T_WATCHDOG)) checkWatchdog();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		} finally {
    			testLib.unregisterHandler(activeHandler);
    		}
		}
		
		if (chosen(T_DIAG2) ||
			chosen(T_IND_CHECK)) {
			
			while (!ui.confirm("Please move the module to the second base", "Move the Module")) {
				if (ui.confirm("Really cancel the test?", "Cancel")) {
					ui.displayMessage(this, UserInterface.M_ERROR, "User canceled the test");
					ui.operationFailed(this);
					return;
				}
			}
			
    		// Register a UDP handler for station 2
    		try {
    			activeHandler = testLib.registerUDPHandler(IP_TESTBASE);
    			
    			// Make sure it works
    			testLib.loadBaseFiles(S_TESTBASE, DIR_TESTBASE);
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		}
    		
    		try { // Finally block unregisters the handler
    			// Diagnostics 2 test
    			try {
    				if (chosen(T_DIAG2)) checkDiags2();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    			
    			// AI Test
    			try {
    				if (chosen(T_IND_CHECK)) testIndependence();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		} finally {
    			testLib.unregisterHandler(activeHandler);
    		}
		}
		
		if (chosenTest == null) {
            // null is all tests
            // write the pass flag
            if (writePassedFlag(true)){
                ui.displayMessage(this, UserInterface.M_NORMAL,
                "Module tests passed");
            }
            else{
                ui.handleException(this, new IOException("Failed to write the All Tests Passed flag to module."));
                ui.operationFailed(this);
                return;
            }
        }
        else {
            ui.displayMessage(this, UserInterface.M_NORMAL, "Module test '" +
                                                            chosenTest +
                                                            "' passed");
        }

		if (!mioe.isEmpty()) {
			ui.handleException(this, mioe);
			ui.operationFailed(this);
		} else {
			ui.operationCompleted(this);
		}
	}
	
    private boolean writePassedFlag(boolean pass)
    //  throws IOException, TimeoutException
    {
        boolean ret = false;
        byte[] flagbyte = new byte[1]; 
        flagbyte[0] = 0; // must be 0 if failed
        if(pass) flagbyte[0] = 1; // must be 1 if pass
        try {
            activeHandler = testLib.registerUDPHandler(IP_TESTBASE);
            udr.writeFile(S_TESTBASE, "/module0/arc/factory", flagbyte, 0x4B, null);
            // Disconnect the handler
            activeHandler = testLib.unregisterHandler(activeHandler);
            ret = true;
        }
        catch (IOException e) {
            ret = false;
        }
        catch (TimeoutException e) {
            ret = false;
        }
        
        return ret;
    }

	private void checkDiags1() throws Exception, IOException, TimeoutException {
		final short stationID = S_CALBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		boolean[] dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected on)
		if (dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 off");
			mioe.add(new IOException("Module reports no power on power 1"));
		}
		
		// Power 2 (expected off)
		if (!dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 off");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 on");
			mioe.add(new IOException("Module reports power on power 2"));
		}
		
		// Ethernet 1 (expected on)
		if (dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 1. Please verify the ethernet cable from the power injector to Ethernet Port 1 is secure. Continue the test?", "Ethernet 1 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 1 link");
			} else {
				dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH1LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 1"));
				}
			}
		}
		
		// Ethernet 2 (expected off)
		if (!dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 not linked");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 linked");
			throw new Exception("Module has a link on ethernet 2");
		}
		
		// POE (expected on if EB, off if E2)
		if (devName.startsWith("EB") && !dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  No POE");
			mioe.add(new IOException("Module reports no POE, although it is a POE module and ethernet 1 has a link"));
		} else if (devName.startsWith("E2") && dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE on non-EB module");
			mioe.add(new IOException("Module reports POE is available, although it is not an EB module"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks for Base 1 complete");
	}
	
	private void checkDiags2() throws Exception, IOException, TimeoutException {
		final short stationID = S_TESTBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		boolean[] dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected off)
		if (!dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 off");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 on");
			mioe.add(new IOException("Module reports power on power 1"));
		}
		
		// Power 2 (expected on)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 off");
			mioe.add(new IOException("Module reports no power on power 2"));
		}
		
		// Ethernet 1 (expected off)
		if (!dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 not linked");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 linked");
			throw new Exception("Module has a link on ethernet 1");
		}
		
		// Ethernet 2 (expected on)
		if (dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 2. Please verify the ethernet cable from the switch to Ethernet Port 2 is secure. Continue the test?", "Ethernet 2 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 2 link");
			} else {
				dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH2LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 2"));
				}
			}
		}
		
		// POE (expected off, since ethernet 1 is not linked)
		if (!dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE powered with no Ethernet 1 link");
			mioe.add(new IOException("Module reports POE is available when there is no Ethernet 1 link"));
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks for Base 2 complete");
	}
	
	private void calibrateAIs() throws Exception, IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating AIs");
		
		// Clear any calibration data that may exist
		ui.displayMessage(this, UserInterface.M_WARNING, "  Clearing current calibration");
		
		if (testLib.zeroRange(S_CALBASE, DIR_MODULECONFIG + "cals", OFFSET_AI_CALIBRATION, LENGTH_AI_CALIBRATION)) {
			ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
			testLib.resetStation(S_CALBASE);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");
		
		// There are 32 AIs on this module, but this will keep us from hardcoding that in multiple places
		int aiCount = 32;
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 200);
		
		// Calibration results will end up in these arrays
		short[] spans = new short[aiCount];
		short[] zeros = new short[aiCount];
		
		// Ask the user to set the input to 0.200V
		while (!ui.confirm("Please set the voltage regulator to output 0.200 V", "Set Voltage")) {
			if (ui.confirm("Really cancel the test?", "Cancel")) {
				throw new Exception("User canceled the test");
			}
		}
		
		// Wait for it to take effect
		Utils.sleep(testLib.getScanDelay() * 2); // Convenient that the scan delay is exactly what we want here...
		
		// Grab the results
		short[] aisLow = testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aiCount, TOLERANCE_AVERAGE);
		
		// Ask the user to set the input to 9.800V
		while (!ui.confirm("Please set the voltage regulator to output 9.800 V", "Set Voltage")) {
			if (ui.confirm("Really cancel the test?", "Cancel")) {
				throw new Exception("User canceled the test");
			}
		}
		
		// Wait for it to take effect
		Utils.sleep(testLib.getScanDelay() * 2);
		
		// Grab the results
		short[] aisHigh = testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aiCount, TOLERANCE_AVERAGE);
		
		// Compute the module span and zero
		testLib.computeAIVoltageCalibration(aisLow, aisHigh, AI_0_2V, AI_9_8V, zeros, spans);
		
		// Write the corrections to the config file
		ui.displayMessage(this, UserInterface.M_WARNING, "  Writing calibration data to station");
		
		byte[] calsData = new byte[aiCount * 4]; // 2 values of 2 bytes apiece for each input
		int offset = 0;
		for (int i = 0; i < aiCount; i++) {
			Conversion.shortToBytes(calsData, offset, zeros[i]);
			offset += 2;
			
			Conversion.shortToBytes(calsData, offset, spans[i]);
			offset += 2;
		}
		
		udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", calsData, OFFSET_AI_CALIBRATION, null);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
		
		// Reset the station
		testLib.resetStation(S_CALBASE);
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  AI calibration complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	
	private void testCalibration() throws Exception, IOException, TimeoutException {
		// This keeps us from hard-coding this number everywhere
		int aiCount = 32;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AIs in 16-bit integrating mode");
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 200);
		
		// Set all channels low/high and check that they are each where they should be
		{
			// Ask the user to set the voltage at 9.800V
			// Since it will still be there from the calibration if the user is doing a usual test,
			//   only ask if he is doing this one specifically
			if (chosenTest != null) {
    			if (!ui.confirm("Please set the voltage regulator to output 9.800 V", "Set Voltage")) {
    				throw new Exception("User canceled the test");
    			}
			}
			
			ui.displayMessage(this, UserInterface.M_WARNING, "  Testing at 9.800 V");
			
			// Read all channels
			{
				Utils.sleep(testLib.getScanDelay() * 2);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aiCount, TOLERANCE_AVERAGE);
				
				// Compare
				// Every channel should be approximately AI_9_8v
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i, ais[i], AI_9_8V, TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - AI_9_8V) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
				}
			}
			
			// Switch to 0.200V ant try again
			while (!ui.confirm("Please set the voltage regulator to output 0.200 V", "Set Voltage")) {
				if (ui.confirm("Really cancel the test?", "Cancel")) {
					throw new Exception("User canceled the test");
				}
			}
			
			ui.displayMessage(this, UserInterface.M_WARNING, "  Testing at 0.200V");
			
			// Read all channels
			{
				Utils.sleep(testLib.getScanDelay() * 2);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aiCount, TOLERANCE_AVERAGE);
				
				// Compare
				// Every channel should be approximately AI_0_2v
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i, ais[i], AI_0_2V, TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - AI_0_2V) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
				}
			}
		}
		
		// Switch to 10-bit mode and test the calibration again on individual channels
		testLib.loadBaseFiles(S_CALBASE, DIR_10BIT1);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AIs in 10-bit high-speed mode");
		
		// Set all channels low/high and check that they are each where they should be
		{
			// The voltage regulator is already at 0.2V, so no need to change it
			// Now that we're only interested in the first two channels (the only ones with 10-bit mode),
			//   reset aiCount
			aiCount = 2;
			
			ui.displayMessage(this, UserInterface.M_WARNING, "  Testing at 0.2V");
			
			// Read the channels
			{
				Utils.sleep(testLib.getScanDelay() * 2);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aiCount, TOLERANCE_HIGHSPEED_AVERAGE);
				
				// Compare
				// Everything should be approximately AI_0_2v
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i, ais[i], AI_0_2V, TOLERANCE_HIGHSPEED_ANALOG);
					if (Math.abs(ais[i] - AI_0_2V) > TOLERANCE_HIGHSPEED_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
				}
			}
			
			// Switch to 9.800V ant try again
			while (!ui.confirm("Please set the voltage regulator to output 9.800 V", "Set Voltage")) {
				if (ui.confirm("Really cancel the test?", "Cancel")) {
					throw new Exception("User canceled the test");
				}
			}
			
			ui.displayMessage(this, UserInterface.M_WARNING, "  Testing at 9.800 V");
			
			// Read the channels
			{
				Utils.sleep(testLib.getScanDelay() * 2);
				
				short[] ais = testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aiCount, TOLERANCE_HIGHSPEED_AVERAGE);
				
				// Compare
				// Everything should be approximately AI_0_2v
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i, ais[i], AI_9_8V, TOLERANCE_HIGHSPEED_ANALOG);
					if (Math.abs(ais[i] - AI_9_8V) > TOLERANCE_HIGHSPEED_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  AI calibration check complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	
	private void testIndependence() throws Exception, IOException, TimeoutException {
		// This keeps us from hard-coding this number everywhere
		int aiCount = 32;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AIs in 16-bit integrating mode");
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 200);
		
		// Set all channels high and check that they are each where they should be
		{
			// Ask the user to set the voltage at 9.800V
			// Since it will still be there from the calibration check if the user is doing a usual test,
			//   only ask if he is doing this one specifically
			if (chosenTest != null) {
    			if (!ui.confirm("Please set the voltage regulator to output 9.800 V", "Set Voltage")) {
    				throw new Exception("User canceled the test");
    			}
			}
			
			// Read all channels
			{
				Utils.sleep(testLib.getScanDelay() * 2);
				
				short[] ais = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aiCount, TOLERANCE_AVERAGE);
				
				// Note: This works with a series of resistors that do not quite provide a linear
				//   step-down voltage across each channel, so we use some extra "fudge factors" to
				//   make up for it
				//   - leeway acts as an increasing extra bit of tolerance to make up for the non-linearity
				//   - the first channel is the absolute expected value from the voltage source, so its
				//       "delta" value will not be on the same curve as the rest of the channels
				//   - each channel's expected value is based on the previous channel
				//   - the last channel is checked to make sure the curve or the last channel did not drop
				//       too quickly
				
				// Compare
				int maxValidValue = AI_9_8V + TOLERANCE_ANALOG;
				int leeway = 0;
				int leewayChange = 3;
				for (int i = 0; i < ais.length; i++) {
					String op = (ais[i] <= maxValidValue ? TestLib.CH_LESS_EQUAL : ">");
					String msg = String.format("AI Channel %,d: %s%,d [%,d (%s %,d)]", i, TestLib.CH_DELTA, maxValidValue - ais[i], ais[i], op, maxValidValue);
					if (ais[i] > maxValidValue) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
					
					// Each channel should be approximately 995 below the previous
					maxValidValue = ais[i] - 995 + TOLERANCE_ANALOG + leeway;
					leeway += leewayChange;
				}
				
				// Make sure the last channel is reasonable
				if (ais[aiCount - 1] < 900) {
					String msg = String.format("AI Channel %,d is too low, check all channel values", aiCount - 1);
					ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					mioe.add(new IOException(msg));
				}
			}
		}
		
		// Switch to 10-bit mode and test the calibration again on individual channels
		testLib.loadBaseFiles(S_TESTBASE, DIR_10BIT2);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AIs in 10-bit high-speed mode");
		
		// Set all channels low/high and check that they are each where they should be
		{
			// The voltage regulator is already at 9.8V, so no need to change it
			// Now that we're only interested in the first two channels (the only ones with 10-bit mode),
			//   reset aiCount
			aiCount = 2;
			
			// Read the channels
			{
				Utils.sleep(testLib.getScanDelay() * 2);
				
				short[] ais = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aiCount, TOLERANCE_HIGHSPEED_AVERAGE);
				
				// Compare
				int maxValidValue = AI_9_8V + TOLERANCE_HIGHSPEED_ANALOG;
				int leeway = 0;
				int leewayChange = 3;
				for (int i = 0; i < ais.length; i++) {
					String op = (ais[i] <= maxValidValue ? TestLib.CH_LESS_EQUAL : ">");
					String msg = String.format("AI Channel %,d: %s%,d [%,d (%s %,d)]", i, TestLib.CH_DELTA, maxValidValue - ais[i], ais[i], op, maxValidValue);
					if (ais[i] > maxValidValue) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
					
					// Each channel should be approximately 995 below the previous
					maxValidValue = ais[i] - 995 + TOLERANCE_HIGHSPEED_ANALOG + leeway;
					leeway += leewayChange;
				}
				
				// Make sure the last channel is reasonable
				if (ais[aiCount - 1] < 30100) {
					String msg = String.format("AI Channel %,d is too low, check all channel values", aiCount - 1);
					ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					mioe.add(new IOException(msg));
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  10-bit AI tests complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	
	private void checkPOE() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the PoE output");
		
		// If it's an EB base, check AI_POE for a value, otherwise expect something small (-8192, probably)
		short[] vals = udr.getA(S_DIAGS, AI_POE, (short)1);
		
		if (devName.startsWith("EB")) {
			if (Math.abs(EXP_POE - vals[0]) > TOLERANCE_POE) {
				throw new IOException(String.format("AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_NOTMEMBEROF, EXP_POE, TOLERANCE_POE));
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_MEMBEROF, EXP_POE, TOLERANCE_POE));
			}
		} else {
			if (vals[0] > 0) {
				throw new IOException("It appears that there is a PoE board on this module, but it has been initialized as an E2.");
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  As expected, there is no PoE board on this module.");
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  PoE test complete");
	}
	
	private void checkWatchdog() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the Watchdog output");
		
		// Give it a couple seconds in case it was rebooted recently
		Utils.sleep(10000);
		
		short[] vals = udr.getA(S_DIAGS, AI_WATCHDOG, (short)1);
		
		if (Math.abs(EXP_WATCHDOG - vals[0]) > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output): is off when it should be on", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is on as expected", AI_WATCHDOG));
		}
		
		// Make sure the watchdog output is off
		testLib.loadBaseFiles(S_CALBASE, DIR_WATCHDOG_OFF);
		
		// Give it a couple extra seconds
		Utils.sleep(10000);
		
		vals = udr.getA(S_DIAGS, AI_WATCHDOG, (short)1);
		
		if (vals[0] > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output) is on when it should be off", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is off as expected", AI_WATCHDOG));
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Watchdog test complete");
	}
	
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
		
	private String formatRangeMessage(String type, int channel, short found, short expected, int tolerance) {
		String op = TestLib.CH_MEMBEROF;
		if (Math.abs(expected - found) > tolerance) op = TestLib.CH_NOTMEMBEROF;
		
		return String.format("%s Channel %,d: %s%,d [%,d %s (%,d +/- %,d)]", type, channel, TestLib.CH_DELTA, found - expected, found, op, expected, tolerance);
	}
}

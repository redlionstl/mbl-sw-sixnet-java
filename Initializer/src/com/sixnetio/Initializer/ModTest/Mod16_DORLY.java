/*
 * Mod16_DORLY.java
 *
 * Tests a 16DORLY module.
 *
 * Jeff Collins
 * April 6, 2011
 *
 */

package com.sixnetio.Initializer.ModTest;

import java.io.IOException;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.Initializer.Tester;
import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.util.*;

public class Mod16_DORLY implements Tester {
	// Constants
	// Locations of the configuration files
	// Loaded into the test runner
	private static final String DIR_TESTRUNNER = "TestData" + File.separator + "16_DORLY" + File.separator + "Base2"; // E2-MIX24880 module
	
	// Loaded into the test base
	private static final String DIR_TESTBASE = "TestData" + File.separator + "16_DORLY" + File.separator + "Base1"; // 16DORLY module
	private static final String DIR_WATCHDOG_OFF = "TestData" + File.separator + "16_DORLY" + File.separator + "WatchdogOff"; // Make sure there is a watchdog failure (DO module)
	
	private static final String T_DIAG = "Diagnostics Test",
	                            T_DO_Relay = "DO Relay Test",
	                            T_POE = "PoE Test",
	                            T_WATCHDOG = "Watchdog Test";
	
	private static final String[] TEST_NAMES = {
		// Base 1
		T_DIAG,
		T_DO_Relay,
		T_POE,
		T_WATCHDOG
	};
	
	// Try to keep every test module using a different set of IP addresses, just in case we figure out
	//   a way to run multiple modules from the same machine at the same time
	private static final String IP_TESTRUNNER = "10.1.14.2"; // B interface
	private static final String IP_TESTBASEA = "10.1.14.1";
	private static final String IP_TESTBASEB = "10.1.15.1";
	
	private static final short S_TESTRUNNER = 2; // E2-MIX24880 module running the test
	private static final short S_TESTBASE = 1; // 16 DORLY module being tested
	
	// Which E2-MIX24880 AI the watchdog output terminal is plugged into
	private static final short AI_WATCHDOG = 1; // (0-based)
	
	// Which E2-MIX24880 AI the POE output terminal is plugged into
	private static final short AI_POE = 0; // 0-based
	
	// Diagnostic DI offsets from DI_DiagStart
	private static final short DI_DIAGSTART = 32; // The first diagnostic DI
	private static final short DIAG_SELFTEST = 0, // Module Selftest
	                           DIAG_POWER1 = 1, // Power 1
	                           DIAG_POWER2 = 2, // Power 2
	                           DIAG_IOSTATUS = 3, // I/O Status
	                           DIAG_ETH1LINK = 4, // Ethernet 1 has link
	                           DIAG_ETH2LINK = 5, // Ethernet 2 has link
	                           DIAG_RING = 6, // Ring is complete
	                           DIAG_POE = 7; // PoE is available
	
	// Analog values (to avoid hardcoding them in multiple places)
	private static final short EXP_WATCHDOG = (short)3686; // Expected watchdog input value; 3686 = 5.8mA = 23.2V across 4000 Ohms
	private static final short EXP_POE = (short)4096; // Expected PoE input value; 4096 = 6mA = .6V across a 100 Ohm resistor
	
	// Used to check that the Watchdog input is close to what it should be
	private static final int TOLERANCE_WATCHDOG = 450;
	
	// Used to check that the PoE input is close to what it should be
	private static final int TOLERANCE_POE = 600;
	
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private UDRLink activeHandler;
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	
	// Constructor
	public Mod16_DORLY() { }
	
	// Required by Tester
	// public void setupTest(UserInterface ui, String comPort, String devName) throws IOException
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for Mod16_DORLY must not be null");
		
		// Make sure the device name is recognized
		// Must start with E2 or EB
		// Must end with 16DORLY
		if (!((devName.startsWith("E2") || devName.startsWith("EB")) &&
		      (devName.endsWith("16DORLY")))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)115, (byte)0);
		testLib = new TestLib(udr, ui);
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
	}
	
	// public boolean supportsTestJump()
	public boolean supportsTestJump() {
		return true;
	}
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
	// public List<String> getTests()
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
	}
	
	// public void jumpToTest(String testName)
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
	}
	
	// public String getJumpTest()
	public String getJumpTest() {
		return chosenTest;
	}
	
	// public void run()
	public void run() {
		MultiIOException mioe = new MultiIOException();
		
		// Connect
		try {
			// Use ethernet port 2 for about half of the tests, then switch to port 1
			// Start with port 2 because it should always be plugged in, so we are more likely
			//   to get a connection and then have the diagnostics test fail with a descriptive
			//   message if ethernet 1 is not plugged in
			activeHandler = testLib.registerUDPHandler(IP_TESTBASEB);
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Using Ethernet2 port for first half of production test.");
			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
			udr.waitForResponse(S_TESTBASE);
			
			// Make sure the connection works
			testLib.loadBaseFiles(S_TESTBASE, DIR_TESTBASE);
		} catch (Exception e) {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unable to connect through the second ethernet port");
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		}
		
		try { // Finally block unregisters activeHandler
			// Check the diagnostic DIs
			try {
				if (chosen(T_DIAG)) checkDiags();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			// checkDiags() may also throw a plain old Exception if the user
			//   cancels, so no more tests will run
			
			// Walk the DOs of this module across the DIs of the other one
			try {
				if (chosen(T_DO_Relay)) walkDOs();
			} catch (IOException ioe) {
				// This doesn't print its own error messages
				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
				mioe.add(ioe);
			}
		} catch (Exception e) {
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		} finally {
			activeHandler = testLib.unregisterHandler(activeHandler);
		}
		
		// Reconnect
		// This is about halfway through, switch to the other ethernet port to continue
		try {
			// Switch to port 1
			activeHandler = testLib.registerUDPHandler(IP_TESTBASEA);
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Using Ethernet1 port for second half of production test.");
			// Make sure the connection works
			testLib.verifyConnectivity(S_TESTBASE);
		} catch (Exception e) {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unable to connect through the first ethernet port");
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		}
		
		try { // Finally block unregisters activeHandler
			// Check the PoE output pin to make sure it's doing what we expect
			try {
				if (chosen(T_POE)) checkPOE();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			
			// Check the Watchdog output pin to make sure it's doing what we expect
			try {
				if (chosen(T_WATCHDOG)) checkWatchdog();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
		} catch (Exception e) {
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		} finally {
			activeHandler = testLib.unregisterHandler(activeHandler);
		}
		
        if (chosenTest == null) {
            // null is all tests
            // write the pass flag
            if (writePassedFlag(true)){
                ui.displayMessage(this, UserInterface.M_NORMAL,
                "Module tests passed");
            }
            else{
                ui.handleException(this, new IOException("Failed to write the All Tests Passed flag to module."));
                ui.operationFailed(this);
                return;
            }
        }
        else {
            ui.displayMessage(this, UserInterface.M_NORMAL, "Module test '" +
                                                            chosenTest +
                                                            "' passed");
        }

		if (!mioe.isEmpty()) {
			ui.handleException(this, mioe);
			ui.operationFailed(this);
		} else {
			ui.operationCompleted(this);
		}
	}
	
    private boolean writePassedFlag(boolean pass)
//  throws IOException, TimeoutException
{
  boolean ret = false;
  byte[] flagbyte = new byte[1]; 
  flagbyte[0] = 0; // must be 0 if failed
  if(pass) flagbyte[0] = 1; // must be 1 if pass
  try {
      activeHandler = testLib.registerUDPHandler(IP_TESTBASEA);
      udr.writeFile(S_TESTBASE, "/module0/arc/factory", flagbyte, 0x4B, null);
      // Disconnect the handler
      activeHandler = testLib.unregisterHandler(activeHandler);
      ret = true;
  }
  catch (IOException e) {
      ret = false;
  }
  catch (TimeoutException e) {
      ret = false;
  }

  return ret;
}

	// Testing functions
	// private void checkDiags() throws Exception, IOException, TimeoutException
	private void checkDiags() throws Exception, IOException, TimeoutException {
		final short stationID = S_TESTBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		// Turn off all DOs on E2-16DORLY Module to start
		boolean[] dos = new boolean[16]; // NOTE: This is the only place where the count is specified
		Arrays.fill(dos, false);
		udr.putD(stationID, (short)0, (short)dos.length, dos);
		boolean [] dosTestModule = new boolean[8]; // DOs on E2-MIX24880 test module
		
		Arrays.fill(dosTestModule, false);	// Set all DOs on E2-MIX24880 test module Array to off.
		dosTestModule[2] = true; // Set DO ON to turn on P2 power input on the E2-16DORLY module
		udr.putD(S_TESTRUNNER, (short)0, (short)dosTestModule.length, dosTestModule);
		
		boolean[] dis = udr.getD(stationID, DI_DIAGSTART, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected on)
		if (dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 off");
			mioe.add(new IOException("Module reports no power on power 1"));
		}
		
		// Power 2 (expected on)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 off");
			mioe.add(new IOException("Module reports no power on power 2"));
		}
		
		// Ethernet 1 (expected on)
		if (dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 1. Please verify the PoE ethernet cable is plugged into the DO side of the fixture. Continue the test?", "Ethernet 1 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 1 link");
			} else {
				dis = udr.getD(stationID, DI_DIAGSTART, (short)8);
				
				if (dis[DIAG_ETH1LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 1"));
				}
			}
		}
		
		// Ethernet 2 (expected on)
		if (dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 2. Please verify the ethernet cable is secure on both the DO side and the switch. Continue the test?", "Ethernet 2 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
				throw new Exception("Module has no ethernet 2 link");
			} else {
				dis = udr.getD(stationID, DI_DIAGSTART, (short)8);
				
				if (dis[DIAG_ETH2LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 2"));
				}
			}
		}
		
		// POE (expected on if EB, off if E2)
		if (devName.startsWith("EB") && !dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  No POE");
			mioe.add(new IOException("Module reports no POE, although it is a POE module and ethernet 1 has a link"));
		} else if (devName.startsWith("E2") && dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE on non-EB module");
			mioe.add(new IOException("Module reports POE is available, although it is not an EB module"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		}
		
		dosTestModule[2] = false;	// Set DO OFF to turn off P2 power input on the E2-16DORLY module
		udr.putD(S_TESTRUNNER, (short)0, (short)dosTestModule.length, dosTestModule);
		
		dis = udr.getD(stationID, DI_DIAGSTART, (short)8);
		
		// Power 2 (expected off)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 on");
			mioe.add(new IOException("Module reports power on power 2, the custom board may have a short"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 off");
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks complete");
	}
	
	// private void walkDOs() throws IOException, TimeoutException
	private void walkDOs() throws IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		// Turn off all DOs
		boolean[] dos = new boolean[16]; // NOTE: This is the only place where the count is specified
		Arrays.fill(dos, false);
		udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);
		
		// Tell the user to watch
		ui.displayMessage(this, UserInterface.M_WARNING, "  Please watch for " + (dos.length) + " walking LEDs");
		
		// Sleep long enough for him to read and turn his head towards the fixture
		Utils.sleep(2500);
		
		// Walk the outputs
		boolean done = false;
		while (!done) {
			ui.displayMessage(this, UserInterface.M_WARNING, "Testing Form A DO Relay Channels 1-14");		
			// We do a <= here to make sure the last input turns back off
			for (int i = 0; i <= dos.length; i++) {
				// Turn off the previous DO and turn on the current one
				if (i > 0) dos[i - 1] = false;
				if (i < dos.length) dos[i] = true;
				
				udr.putD(S_TESTBASE, (short)0, (short)(dos.length-2), dos);
				
				// Sleep for a moment to let it stabilize
				Utils.sleep(200);

				// Read the DIs to make sure everything is as it should be
				boolean[] dis = udr.getD(S_TESTRUNNER, (short)0, (short)dos.length);
				for (int j = 0; j < dis.length-2; j++) {
					if (dis[j]) {
						// It's on... should it be?
						if (i != j) {
							// Make sure the message isn't confusing on the last loop
							if (i == dos.length-2) {
								mioe.add(new IOException("After turning off all outputs, DO " + j + " (0-based) is on"));
							} else {
								mioe.add(new IOException("After turning on DO " + i + " (0-based), DO " + j + " (0-based) is on"));
							}
						}
					} else {
						// It's off... should it be?
						if (i == j) {
							// No need to worry about the last loop, since everything should be off
							mioe.add(new IOException("DO " + i + " (0-based) did not turn on"));
						}
					}
				}
			}
			// Test the form C DO Relays at channels 15 and 16.
			ui.displayMessage(this, UserInterface.M_WARNING, "Testing Form C DO Relay Channels 15-16");
			for(int j=14; j < dos.length; j++){
				// The NO (Normally Open), and NC (Normally Closed) terminals for DO Relay channels 14 and 15 (0-based)
				// are attached to DIs on the E2-MIX24880 Test Module as follows:
				// DO Relay 14 - NO to DI14, NC to DI16 (0-base)
				// DO Relay 15 - NO to DI15, NC to DI17 (0-base)
				
				// Normally Open test		
				// Set the Form C Do Relay to on
				dos[j] = true;
				
				// Write DOs to set Form C Relay(0-based) to NO 
				udr.putD(S_TESTBASE, (short)0, (short)(dos.length), dos);
	
				// Sleep for a moment to let it stabilize
				Utils.sleep(200);
				
				// Read DIs from E2-MIX24880 test module
				boolean []dis = udr.getD(S_TESTRUNNER, (short)0, (short)(dos.length+2));
				
				// Check that NO DI is on and NC DI is off
					if (dis[j]) {
						// NO DI on as expected
						if (!dis[j+2]) {
							// NC DI off as expected
						}else {
							mioe.add(new IOException("DI " + (j+3) + " on E2-MIX module on when should be off for DO " + j + " (0-based) Normally Open Test"));
						}
						
					} else {
						mioe.add(new IOException("DI " + (j+1) + "  on E2-MIX module off when should be on for DO " + j + " (0-based) Normally Open Test"));
						
					}
		
					// Normally Closed test		
					// Set the Form C Do Relay to off 
					dos[j] = false;
					
					// Write DOs to set Form C Relay to NC 
					udr.putD(S_TESTBASE, (short)0, (short)(dos.length), dos);
					
					// Read DIs from E2-MIX24880 test module
					dis = udr.getD(S_TESTRUNNER, (short)0, (short)(dos.length+2));
					
					// Check that NO DIs are off and NC DIs are on
					if (!dis[j]) {
						// NO DI off as expected
						if (dis[j+2]) {
							// NC DI on as expected
						}else {
							mioe.add(new IOException("DI " + (j+3) + " on E2-MIX module off when should be on for DO " + j + " (0-based) Normally Closed Test"));
						}
						
					} else {
						mioe.add(new IOException("DI " + (j+1) + " on E2-MIX module on when should be off for DO " + j + " (0-based) Normally Closed Test"));
						
					}
				}				
				// Make sure the user watched
				if (ui.confirm("Did you watch for walking DO LEDs (there were " + (dos.length) + ")?", "Walking LEDs")) {
					if (!ui.confirm("Did they all light?", "Walking LEDs")) mioe.add(new IOException("Missing DO LEDs during walk"));
					done = true;
				} else {
					Utils.sleep(2000); // Give the user a chance to look back over there
					done = false;
				}
		}
	
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  DO Relay tests complete");
	}
	
	// private void checkPOE() throws IOException, TimeoutException
	private void checkPOE() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the PoE output");
		
		// If it's an EB base, check AI_POE for a value, otherwise expect something small (-8192, probably)
		short[] vals = udr.getA(S_TESTRUNNER, AI_POE, (short)1);
		
		if (devName.startsWith("EB")) {
			if (Math.abs(EXP_POE - vals[0]) > TOLERANCE_POE) {
				throw new IOException(String.format("AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_NOTMEMBEROF, EXP_POE, TOLERANCE_POE));
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_MEMBEROF, EXP_POE, TOLERANCE_POE));
			}
		} else {
			if (vals[0] > 0) {
				throw new IOException("It appears that there is a PoE board on this module, but it has been initialized as an E2.");
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  As expected, there is no PoE board on this module.");
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  PoE test complete");
	}
	
	// private void checkWatchdog() throws IOException, TimeoutException
	private void checkWatchdog() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the Watchdog output");
		
		// Give it a couple seconds in case it was rebooted recently
		Utils.sleep(10000);
		
		short[] vals = udr.getA(S_TESTRUNNER, AI_WATCHDOG, (short)1);
		
		if (Math.abs(EXP_WATCHDOG - vals[0]) > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output): is off [%d] when it should be on [>%d+/-%d]", AI_WATCHDOG,vals[0],EXP_WATCHDOG,TOLERANCE_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is on as expected", AI_WATCHDOG));
		}
		
		// Make sure the watchdog output is off
		testLib.loadBaseFiles(S_TESTBASE, DIR_WATCHDOG_OFF);
		
		// Give it a couple extra seconds
		Utils.sleep(10000);
		
		vals = udr.getA(S_TESTRUNNER, AI_WATCHDOG, (short)1);
		
		if (vals[0] > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output) is on when it should be off", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is off as expected", AI_WATCHDOG));
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Watchdog test complete");
	}
	
	// Testing helper functions
	// private boolean chosen(String testName)
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
}

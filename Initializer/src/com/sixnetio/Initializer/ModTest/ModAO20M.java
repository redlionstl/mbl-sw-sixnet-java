/*
 * ModAO20M.java
 *
 * Tests a 16AI8AO20M, 8AO20M, or 4AO20M module.
 *
 * Jonathan Pearson
 * November 3, 2008
 *
 */

package com.sixnetio.Initializer.ModTest;

import com.sixnetio.Initializer.*;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class ModAO20M implements Tester {
	// Constants
	// Locations of the configuration files
	private static final String DIR_START = "TestData" + File.separator + "AO20M" + File.separator + "Start";
	private static final String DIR_10BIT = "TestData" + File.separator + "AO20M" + File.separator + "10bit";
	
	private static final String T_DIAG = "Diagnostics Test",
	                            T_AICAL = "AI Calibration",
	                            T_AOCAL = "AO Calibration",
	                            T_AI = "AI Test",
	                            T_AO = "AO Test",
	                            T_POE = "PoE Test",
	                            T_WATCHDOG = "Watchdog Test";
	
	private static final String[] TEST_NAMES = {
		T_DIAG,
		T_AICAL,
		T_AOCAL,
		T_AI,
		T_AO,
		T_POE,
		T_WATCHDOG
	};
	
	// fixture RZ-34 IP addresses/station numbers
	private static final String IP_CALIBRATION = "10.1.19.1";
	
	//station numbers below changed to match the new test fixture RZ-33
	private static final short S_TESTBASE = 1; // 16/32AI20M being tested
	private static final short S_DIAGS = 3; // 16AI8AO in charge of reading POE and Watchdog outputs
	private static final short S_MEAS_OUTS = 3; // 16AI8AO AIs that read the UUT Watchdog & PoE outputs 
	private static final short S_OUTPUTS0_7 = 3; // 16AI8AO to source AIs 0-7
	private static final short S_OUTPUTS8_15 = 4; // 8AO to source AIs 8-15
	private static final short S_MEAS_AO0_15 = 2; // 16ISO20M to measure AO sources for UUT AIs 0-15
	private static final short S_MEAS_UUT_AO0_7 = 5; // 16ISO20M to measure UUT AOs 0-7
	
	// Which AI the watchdog output terminal is connected to on 16AI8AO
	private static final short AI_WATCHDOG = 2; // (0-based address)
	
	// Which AI the POE output terminal is connected to on 16AI8AO
	private static final short AI_POE = 0; // 0-based address
	
    // Which AI terminal sampling fixture power is plugged into on 16AI8AO
    private static final short AI_24V = 1; // The last one is 7 (0-based address)

    // Which DI is used for the watchdog heartbeat set in the setup file
    private static final short HEARTBEAT_DI = 0; // (0-based)
	
	// Diagnostic DI offsets from DI_DiagStart
	private static final short DI_DIAG_START = 32; // The first diagnostic DI
	private static final short DIAG_SELFTEST = 0, // Module Selftest
	                           DIAG_POWER1 = 1, // Power 1
	                           DIAG_POWER2 = 2, // Power 2
	                           DIAG_IOSTATUS = 3, // I/O Status
	                           DIAG_ETH1LINK = 4, // Ethernet 1 has link
	                           DIAG_ETH2LINK = 5, // Ethernet 2 has link
	                           DIAG_RING = 6, // Ring is complete
	                           DIAG_POE = 7; // PoE is available
	
	// Analog values (to avoid hardcoding them in multiple places)
    private static final short EXP_24V = (short)4096; // value if Fixture PWR1 is at 24V 
    // input value; 4096
    // = 6.0mA = 24.0V
    // across 4000 Ohms (3.9K + 100 [AI input res.])
    private static final short EXP_WATCHDOG = (short)3686; // Expected watchdog
    // input value; 3686
    // = 5.8mA = 23.2V
    // across 4000 Ohms (3.9K + 100 [AI input res.])
	private static final short EXP_POE = (short)4096; // Expected PoE PWROUT value
    // input value; 4096
    // = 6.0mA = 24.0V
    // across 4000 Ohms (3.9K + 100 [AI input res.])
	private static final short AO_4MA = (short)0; // To output 4mA from an AO module
	private static final short AO_5MA = (short)2047; // To output 5mA from an AO module
	private static final short AO_18MA = (short)28671; // To output 18mA from an AO module
	private static final short AO_20MA = (short)32767; // To output 20mA from an AO module
	
	// Tolerance for analog values
	// If two analog values should be equal, values within this distance on either side will be accepted
	private static final int TOLERANCE_ANALOG = 12;
	
	// Similar to AnalogTolerance, this is used to make sure consecutive scans during an average are close enough
	// Since values are allowed to be off by 12 in either direction of the target value, a difference of 24 covers
	//   the situation where one scane sees a value 12 below and the next scan sees 12 above, then we add a little
	//   extra to avoid spurious errors
	private static final int TOLERANCE_AVERAGE = 25;
	
	// When testing for channel independence (all get different values), this is the tolerance for each channel
	private static final int TOLERANCE_INDEPENDENCE = 25;
	
	// Used to check that the Watchdog input is close to what it should be
	private static final int TOLERANCE_WATCHDOG = 450;
	
	// Used to check that the PoE input is close to what it should be
	private static final int TOLERANCE_POE = 600;
	
	// Same as Analog/Average/Independence Tolerance, but these are used for 10-bit high-speed
	//   mode, which is wildly less accurate (6 fewer bits means +/- 64 for one analog point)
	// Allowed to be 12 analog points off, so 768 makes sense (12 * 64)
	private static final int TOLERANCE_HIGHSPEED_ANALOG = 768;
	
	// 768 below, then 768 above means 2 * 768 range across consecutive values
	private static final int TOLERANCE_HIGHSPEED_AVERAGE = 1536;
	
	// IndependenceTolerance is equal to AverageTolerance, so same here
	private static final int TOLERANCE_HIGHSPEED_INDEPENDENCE = 1536;
	
	// Base directory in a module where we will find the configuration files
	// We expect a slash on the end of this
	private static final String DIR_MODULECONFIG = "/module0/arc/";
	
	// Offset/length of the AI calibration section of the module cals file
	private static final int OFFSET_AICALIBRATION = 0x100;
	private static final int LENGTH_AICALIBRATION = 0x80;
	
	// Offset/length of the AO calibration section of the module cals file
	private static final int OFFSET_AOCALIBRATION = 0x180;
	private static final int LENGTH_AOCALIBRATION = 0x80;
	
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private UDRLink[] activeHandler = new UDRLink[2]; // One for the E2, one for the ET
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	
	public ModAO20M() { }
	
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for ModAO20M must not be null");
		
		// Make sure the device name is recognized
		// Must start with E2 or EB
		// Must end with 16AI20M-8AO20M, 8AO20M, or 4AO20M
		if (!((devName.startsWith("E2") || devName.startsWith("EB")) &&
		      (devName.endsWith("16AI20M-8AO20M") || devName.endsWith("8AO20M") || devName.endsWith("4AO20M")))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)112, (byte)0);
		testLib = new TestLib(udr, ui);
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
	}
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
	}
	
	public boolean supportsTestJump() {
		return true;
	}
	
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
	}
	
	public String getJumpTest() {
		return chosenTest;
	}
	
	public void run() {
		// Register UDP handler for base 1 (the UUT module passes all comms to other stations via RS485)
		Arrays.fill(activeHandler, null); // In case something fails, so we can unregister those that succeed
		
		try {
			activeHandler[0] = testLib.registerUDPHandler(IP_CALIBRATION);
		} catch (IOException ioe) {
			unregisterHandlers();
			
			ui.handleException(this, ioe);
			ui.operationFailed(this);
			return;
		}
		
		MultiIOException mioe = new MultiIOException();
		
		try { // Finally block unregisters the handlers
			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
			udr.waitForResponse(S_TESTBASE);
			
			// First, load the files into the base
			// Will only write memory and reset if data changes
			if (testLib.loadBaseFiles(S_TESTBASE, DIR_START)){
				// load returned true if it loaded the file and reset
				ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for restart...");
			}
			//ensure the UUT is fully running after the reset or has completed the bootup if firmware was loaded prior
			Utils.sleep(10000); 
			
			// Now start doing tests
			// Diagnostics
			try {
				if (chosen(T_DIAG)) checkDiags();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			
			// POE test
			try {
				if (chosen(T_POE)) checkPOE();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			
			// Watchdog test
			try {
				if (chosen(T_WATCHDOG)) checkWatchdog();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}

			// AI Calibration/test
			try {
				if (chosen(T_AICAL)&&(!runVerifyTest)) calibrateAIs();
				if (chosen(T_AI)) testAIs(); // If the calibration fails, don't do the test
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			
			// AO Calibration/test
			try {
				if (chosen(T_AOCAL)&&(!runVerifyTest)) calibrateAOs();
				if (chosen(T_AO)) testAOs(); // If the calibration fails, don't do the test
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			
		} catch (Exception e) {
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		} finally {
			unregisterHandlers();
		}
		
        if (chosenTest == null) {
            // null is all tests
            // write the pass flag
            if (writePassedFlag(true)){
                ui.displayMessage(this, UserInterface.M_NORMAL,
                "Module tests passed");
            }
            else{
                ui.handleException(this, new IOException("Failed to write the All Tests Passed flag to module."));
                ui.operationFailed(this);
                return;
            }
        }
        else {
        	if(mioe.isEmpty()){
        		ui.displayMessage(this, UserInterface.M_NORMAL, "Module test '" + chosenTest + "' passed");
        	} else {
                ui.displayMessage(this, UserInterface.M_ERROR, "Error during module test '" + chosenTest + "'");
        	}
        }

		if (!mioe.isEmpty()) {
			ui.handleException(this, mioe);
			ui.operationFailed(this);
		} else {
			ui.operationCompleted(this);
		}
	}
	
    private boolean writePassedFlag(boolean pass)
    //  throws IOException, TimeoutException
    {
        boolean ret = false;
        byte[] flagbyte = new byte[1]; 
        flagbyte[0] = 0; // must be 0 if failed
        if(pass) flagbyte[0] = 1; // must be 1 if pass
        try {
            activeHandler[1] = testLib.registerUDPHandler(IP_CALIBRATION);
            udr.writeFile(S_TESTBASE, "/module0/arc/factory", flagbyte, 0x4B, null);
            // Disconnect the handler
            activeHandler[1] = testLib.unregisterHandler(activeHandler[1]);
            ret = true;
        }
        catch (IOException e) {
            ret = false;
        }
        catch (TimeoutException e) {
            ret = false;
        }
        
        return ret;
    }

	private void checkDiags() throws Exception, IOException, TimeoutException {
		final short stationID = S_TESTBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		// Make sure that P2 will be on
		// This relies on the DIR_START setup file that is loaded upon starting the test
		// Will only write memory and reset if data changes
		if (testLib.loadBaseFiles(S_TESTBASE, DIR_START)){
			// load returned true if it loaded the file and reset
			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for restart...");
		}
		
		// toggle heartbeat so Power 2 (connected to Watchdog output) will be on
		// when we read the diagnostic registers
		boolean[] data = new boolean[1];
		data[0] = true;
		udr.putD(stationID, UDRMessage.T_D_DIN, HEARTBEAT_DI, (short)1, data);
		data[0] = false;
		Utils.sleep(100);
		udr.putD(stationID, UDRMessage.T_D_DIN, HEARTBEAT_DI, (short)1, data);
		Utils.sleep(200); // allow for the diag registers to get set before reading them
		
		boolean[] dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected on)
		if (dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 off");
			mioe.add(new IOException("Module reports no power on power 1"));
		}
		
		// Power 2 (expected on)
		// Power 2 is connected to the watchdog output to source it.
		// The watchdog output will provide power when the heartbeat DI0 is
		// toggled and keep it powered until the heartbeat timeout (6 sec) expires.
		// Since the heartbeat was previously toggled the Watchdog out will be ~23.2V 
		// so the power 2 input will have ~22.4V (on) due to connecting diode drop.
		// The watchdog/heartbeat settings are part of the starting base setup file
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 on as expected");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 off when it was expected to be on" + 
				"\nNote the Power 2 input is sourced by the watchdog output." + 
				"\nErrors in the Watchdog output will affect this test.");
			mioe.add(new IOException("Module reports no power on power 2"));
		}
		
		// Ethernet 1 (expected on)
		if (dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 1. Please verify the ethernet cable from the power injector to Ethernet Port 1 is secure. Continue the test?", "Ethernet 1 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 1 link");
			} else {
				dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH1LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 1"));
				}
			}
		}
		
		// Ethernet 2 (expected on)
		if (dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 2. Please verify the ethernet cable from Ethernet Port 2 to the EtherTRAK module is secure. Continue the test?", "Ethernet 2 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
				throw new Exception("Module has no ethernet 2 link");
			} else {
				dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH2LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 2"));
				}
			}
		}
		
		// POE (expected on if EB, off if E2)
		if (devName.startsWith("EB") && !dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  No POE");
			mioe.add(new IOException("Module reports no POE, although it is a POE module and ethernet 1 has a link"));
		} else if (devName.startsWith("E2") && dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE on non-EB module");
			mioe.add(new IOException("Module reports POE is available, although it is not an EB module"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		}
		
		// Power 2 (expected off)
		// - wait for the heartbeat timeout (6 sec) so the watchdog output sourcing it changes to off.		
		Utils.sleep(7000);
		
		dis = udr.getD(stationID, DI_DIAG_START, (short)8);
		
		// Power 2 (expected off)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 on when it was expected to be off");
			mioe.add(new IOException("Module reports power on power 2 when it was expected to be off"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 off as expected");
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks complete");
	}
	
	private void calibrateAIs() throws IOException, TimeoutException {
		// Figure out how many AIs there are
		if (!devName.endsWith("16AI20M-8AO20M")) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "No AIs to calibrate");
			return;
		}
		
		final short aiCount = 16;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating AIs");
		
		// Clear any calibration data that may exist
		ui.displayMessage(this, UserInterface.M_WARNING, "  Clearing current calibration");
		
		if (testLib.zeroRange(S_TESTBASE, DIR_MODULECONFIG + "cals", OFFSET_AICALIBRATION, LENGTH_AICALIBRATION)) {
			ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
			testLib.resetStation(S_TESTBASE);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");
		
		// Set the scan delay
		// Documentation says 110ms per active channel, we add a little to be careful
		testLib.setScanDelay(110 * aiCount + 200);
		
		// There are aiCount AIs coming from AOs on stations 3, 4, 5, and 6 (only 3 and 4 for the 16)
		short[] aos = new short[aiCount];
		
		// Calibration results will end up in these arrays
		short[] spans = new short[aiCount];
		short[] zeros = new short[aiCount];
		
		// Write 5mA to all input channels
		Arrays.fill(aos, AO_5MA);
		setAOs(aos);
		
        // Need to wait for the analog out setpoint to settle in. 
        // We need it to be stable before using it for cal
        Utils.sleep(3000);
        // give the module time to scan all the channels with the now stable input
        Utils.sleep(testLib.getScanDelay());
		
		// Grab the results
		short[] aisLow = new short[aiCount];
        short[] aisMeasLow = new short[aiCount];
        getAvgOfAllAIs(aisLow, aisMeasLow);
		
		// Write 18mA to all input channels
		Arrays.fill(aos, AO_18MA);
		setAOs(aos);
		
        // Need to wait for the analog out setpoint to settle in. 
        // We need it to be stable before using it for cal
        Utils.sleep(3000);
        // give the module time to scan all the channels with the now stable input
        Utils.sleep(testLib.getScanDelay());
		
		// Grab the results
		short[] aisHigh = new short[aiCount];
        short[] aisMeasHigh = new short[aiCount];
        getAvgOfAllAIs(aisHigh, aisMeasHigh);
		
        // Compute the module span and zero
        testLib.computeAICurrentCalibrationWithMeasuredSetpoint(aisLow, aisHigh, aisMeasLow, aisMeasHigh,
            zeros, spans);
		
		// Write the corrections to the config file
		ui.displayMessage(this, UserInterface.M_WARNING, "  Writing calibration data to station");
		
		byte[] calsData = new byte[aiCount * 4]; // 2 values of 2 bytes apiece for each input
		int offset = 0;
		for (int i = 0; i < aiCount; i++) {
			Conversion.shortToBytes(calsData, offset, zeros[i]);
			offset += 2;
			
			Conversion.shortToBytes(calsData, offset, spans[i]);
			offset += 2;
		}
		
		udr.writeFile(S_TESTBASE, DIR_MODULECONFIG + "cals", calsData, OFFSET_AICALIBRATION, null);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
		
		// Reset the station
		testLib.resetStation(S_TESTBASE);
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  AI calibration complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	
	private void testAIs() throws IOException, TimeoutException {
		// Figure out how many AIs there are
		if (!devName.endsWith("16AI20M-8AO20M")) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "No AIs to test");
			return;
		}
		
		final short aiCount = 16;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AIs in 16-bit integrating mode");
		
		// Set the scan delay
		// Documentation says 110ms per active channel, we add a little to be careful
		testLib.setScanDelay(110 * aiCount + 200);
		
		// Set all channels low/high and check that they are each where they should be
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing all AIs with the same value");
		
		{
			short[] aos = new short[aiCount];
			
			// Write 5mA to all channels
			ui.displayMessage(this, UserInterface.M_WARNING, "    5mA value first");
			{
				Arrays.fill(aos, AO_5MA);
				setAOs(aos);
		        // Need to wait for the analog out setpoint to settle in. 
		        // We need it to be stable before using it for cal
		        Utils.sleep(3000);
		        // give the module time to scan all the channels with the now stable input
		        Utils.sleep(testLib.getScanDelay());
				
				// Grab the results
				short[] aisUUT = new short[aiCount];
		        short[] aisMeasRef = new short[aiCount];
		        getAvgOfAllAIs(aisUUT, aisMeasRef);
				
				// Compare
				for (int i = 0; i < aiCount; i++) {
					String msg = formatRangeMessage("AI", i+1, aisUUT[i], aisMeasRef[i], TOLERANCE_ANALOG);
					if (Math.abs(aisUUT[i] - aisMeasRef[i]) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "      " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "      " + msg);
					}
				}
			}
			
			// Write 18mA to all channels
			ui.displayMessage(this, UserInterface.M_WARNING, "    18mA value second");
			{
				Arrays.fill(aos, AO_18MA);
				setAOs(aos);
		        // Need to wait for the analog out setpoint to settle in. 
		        // We need it to be stable before using it for cal
		        Utils.sleep(3000);
		        // give the module time to scan all the channels with the now stable input
		        Utils.sleep(testLib.getScanDelay());
				
				// Grab the results
				short[] aisUUT = new short[aiCount];
		        short[] aisMeasRef = new short[aiCount];
		        getAvgOfAllAIs(aisUUT, aisMeasRef);
				
				// Compare
				for (int i = 0; i < aiCount; i++) {
					String msg = formatRangeMessage("AI", i+1, aisUUT[i], aisMeasRef[i], TOLERANCE_ANALOG);
					if (Math.abs(aisUUT[i] - aisMeasRef[i]) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "      " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "      " + msg);
					}
				}
			}
		}
		
		// Test independence of channels
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing channel independence");

		{
			short[] aos = new short[aiCount];
			for (int i = 0; i < aiCount; i++) {
				aos[i] = (short)((AO_18MA - AO_5MA) * i / aiCount + AO_5MA);
			}
			setAOs(aos);
	        // Need to wait for the analog out setpoint to settle in. 
	        // We need it to be stable before using it for cal
	        Utils.sleep(3000);
	        // give the module time to scan all the channels with the now stable input
	        Utils.sleep(testLib.getScanDelay());
			
			// Grab the results
			short[] aisUUT = new short[aiCount];
	        short[] aisMeasRef = new short[aiCount];
	        getAvgOfAllAIs(aisUUT, aisMeasRef);
			
			// Compare
			for (int i = 0; i < aiCount; i++) {
				String msg = formatRangeMessage("AI", i+1, aisUUT[i], aisMeasRef[i], TOLERANCE_INDEPENDENCE);
				if (Math.abs(aisUUT[i] - aisMeasRef[i]) > TOLERANCE_INDEPENDENCE) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  16-bit AI tests complete");
		
		// Switch to 10-bit mode and test the calibration again on individual channels
		testLib.loadBaseFiles(S_TESTBASE, DIR_10BIT);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AI channels 1 & 2 in 10-bit high-speed mode");
		
		// Set channels low/high and check that they are each where they should be
		short[] aos = new short[2];
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing Ch1 at 5mA & Ch2 at 18mA");
		{
			aos[0] = AO_5MA;
			aos[1] = AO_18MA;
			setAOs(aos);

			// Need to wait for the analog out setpoint to settle in. 
	        // We need it to be stable before using it for cal
	        Utils.sleep(3000);
	        // give the module time to scan all the channels with the now stable input
	        Utils.sleep(testLib.getScanDelay());
			
			// Grab the results
			short[] aisUUT = new short[aiCount];
	        short[] aisMeasRef = new short[aiCount];
	        getAvgOfAllAIs(aisUUT, aisMeasRef, TOLERANCE_HIGHSPEED_ANALOG);
			
			// Compare
			for (int i = 0; i < 2; i++) {
				String msg = formatRangeMessage("AI", i+1, aisUUT[i], aisMeasRef[i], TOLERANCE_HIGHSPEED_ANALOG);
				if (Math.abs(aisUUT[i] - aisMeasRef[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing Ch1 at 18mA & Ch2 at 5mA");
		{
			aos[0] = AO_18MA;
			aos[1] = AO_5MA;
			setAOs(aos);
			
			// Need to wait for the analog out setpoint to settle in. 
	        // We need it to be stable before using it for cal
	        Utils.sleep(3000);
	        // give the module time to scan all the channels with the now stable input
	        Utils.sleep(testLib.getScanDelay());
			
			// Grab the results
			short[] aisUUT = new short[aiCount];
	        short[] aisMeasRef = new short[aiCount];
	        getAvgOfAllAIs(aisUUT, aisMeasRef, TOLERANCE_HIGHSPEED_ANALOG);
			
			// Compare
			for (int i = 0; i < 2; i++) {
				String msg = formatRangeMessage("AI", i+1, aisUUT[i], aisMeasRef[i], TOLERANCE_HIGHSPEED_ANALOG);
				if (Math.abs(aisUUT[i] - aisMeasRef[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  10-bit AI tests complete");
		
		// Make sure the LEDs are all lit
		if (!ui.confirm("Are all of the AI LEDs lit?", "AI LEDs")) {
			mioe.add(new IOException("Not all AI LEDs light"));
		}
		
		
		if (!mioe.isEmpty()) throw mioe;
	}
	
	private void calibrateAOs() throws IOException, TimeoutException {
		final short aoCount;
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating AOs");
		
		if (devName.endsWith("8AO20M") || devName.endsWith("16AI20M-8AO20M")) {
			aoCount = 8;
		} else if (devName.endsWith("4AO20M")){
			aoCount = 4;
		} else {
			throw new IOException("Unrecognized device name: " + devName);
		}
		
		// Clear any calibration data that may exist
		ui.displayMessage(this, UserInterface.M_WARNING, "  Clearing current AO calibration");
		
		if (testLib.zeroRange(S_TESTBASE, DIR_MODULECONFIG + "cals", OFFSET_AOCALIBRATION, LENGTH_AOCALIBRATION)) {
			ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
			testLib.resetStation(S_TESTBASE); 
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");
		
		short[] aos = new short[aoCount];
		
		// Calibration results will end up in these arrays
		short[] spans = new short[aos.length];
		short[] zeros = new short[aos.length];
		
		// Set the scan delay
		// Documentation says 110ms per active channel, we add a little to be careful.
		// The AI module should be set to only read the 8 channels and any extra AIs are disabled.
		// We are using 8 channels for the scan delay because there are 8 AIs, even if there
		//   are fewer AOs.
		testLib.setScanDelay(110 * 8 + 200);
		
		// Write 4ma to each channel and read the results
		Arrays.fill(aos, AO_4MA);
		udr.putA(S_TESTBASE, (short)0, aoCount, aos);
		
        // Need to wait for the analog out setpoint to settle in. 
        // We need it to be stable before using it for cal
        Utils.sleep(3000);
        // give the module time to scan all the channels with the now stable input
        Utils.sleep(testLib.getScanDelay());
		
		// Read back what it is actually outputting
		short[] aisLow = testLib.getAverageAnalogs(S_MEAS_UUT_AO0_7, (short)0, aoCount, TOLERANCE_AVERAGE);
		
		// Write 18mA to all input channels
		Arrays.fill(aos, AO_18MA);
		udr.putA(S_TESTBASE, (short)0, aoCount, aos);
		
        // Need to wait for the analog out setpoint to settle in. 
        // We need it to be stable before using it for cal
        Utils.sleep(3000);
        // give the module time to scan all the channels with the now stable input
        Utils.sleep(testLib.getScanDelay());
		
		// Grab the results
		short[] aisHigh = testLib.getAverageAnalogs(S_MEAS_UUT_AO0_7, (short)0, aoCount, TOLERANCE_AVERAGE);
		
		// Compute the module span and zero
		testLib.computeAOCurrentCalibration(aisLow, aisHigh, zeros, spans);
		
		// Write the corrections to the config file
		ui.displayMessage(this, UserInterface.M_WARNING, "  Writing calibration data to station");
		
		byte[] calsData = new byte[aoCount * 4]; // 2 values of 2 bytes apiece for each input
		int offset = 0;
		for (int i = 0; i < aos.length; i++) {
			Conversion.shortToBytes(calsData, offset, zeros[i]);
			offset += 2;
			
			Conversion.shortToBytes(calsData, offset, spans[i]);
			offset += 2;
		}
		
		udr.writeFile(S_TESTBASE, DIR_MODULECONFIG + "cals", calsData, OFFSET_AOCALIBRATION, null);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
		
		// Reset the station
		testLib.resetStation(S_TESTBASE);
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  AO calibration complete");
	}
	
	private void testAOs() throws IOException, TimeoutException {
		final short aoCount;
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AOs");

		if (devName.endsWith("8AO20M")|| devName.endsWith("16AI20M-8AO20M")) {
			aoCount = 8;
		} else if (devName.endsWith("4AO20M")){
			aoCount = 4;
		} else {
			throw new IOException("Unrecognized device name: " + devName);
		}
		
		MultiIOException mioe = new MultiIOException();
		
		short[] aos = new short[aoCount];
		
		// Set all channels low/high and check that they are each where they should be
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing all AOs with the same value");
		{
			// There are 8 AIs reading the AOs, even if there are fewer AOs
			// The AI module should be set to only read the 8 channels and any extra AIs are disabled.
			testLib.setScanDelay(110 * 8 + 200);
			
			// Write 5mA to all channels
			ui.displayMessage(this, UserInterface.M_WARNING, "    Testing all channels at 5mA");
			{
				Arrays.fill(aos, AO_5MA);
				udr.putA(S_TESTBASE, (short)0, aoCount, aos);
				
		        // Need to wait for the analog out setpoint to settle in. 
		        // We need it to be stable before using it for cal
		        Utils.sleep(3000);
		        // give the module time to scan all the channels with the now stable input
		        Utils.sleep(testLib.getScanDelay());
				
				short[] ais = testLib.getAverageAnalogs(S_MEAS_UUT_AO0_7, (short)0, aoCount, TOLERANCE_AVERAGE);
				
				// Compare
				for (int i = 0; i < aoCount; i++) {
					String msg = formatRangeMessage("AO", i + 1, ais[i], aos[i], TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "      " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "      " + msg);
					}
				}
			}
			
			// Write 18mA to all channels
			ui.displayMessage(this, UserInterface.M_WARNING, "    Testing all channels at 18mA");
			{
				Arrays.fill(aos, AO_18MA);
				udr.putA(S_TESTBASE, (short)0, aoCount, aos);
				
		        // Need to wait for the analog out setpoint to settle in. 
		        // We need it to be stable before using it for cal
		        Utils.sleep(3000);
		        // give the module time to scan all the channels with the now stable input
		        Utils.sleep(testLib.getScanDelay());
				
				short[] ais = testLib.getAverageAnalogs(S_MEAS_UUT_AO0_7, (short)0, aoCount, TOLERANCE_AVERAGE);
				
				// Compare
				for (int i = 0; i < aoCount; i++) {
					String msg = formatRangeMessage("AO", i + 1, ais[i], aos[i], TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "      " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "      " + msg);
					}
				}
			}
		}
		
		// Test channel independence
		{
			ui.displayMessage(this, UserInterface.M_WARNING, "  Testing channel independence");
			for (int i = 0; i < aoCount; i++) {
				aos[i] = (short)((AO_18MA - AO_5MA) * i / aoCount + AO_5MA);
			}
			
			udr.putA(S_TESTBASE, (short)0, (short)aos.length, aos);
			
	        // Need to wait for the analog out setpoint to settle in. 
	        // We need it to be stable before using it for cal
	        Utils.sleep(3000);
	        // give the module time to scan all the channels with the now stable input
	        Utils.sleep(testLib.getScanDelay());
			
			// Get the average value
			short[] ais = testLib.getAverageAnalogs(S_MEAS_UUT_AO0_7, (short)0, aoCount, TOLERANCE_AVERAGE);
			
			// Compare
			for (int i = 0; i < aoCount; i++) {
				// A small amount of influence is allowed; a difference of 1mA
				//   is equal to an analog difference of about 2048, so as long
				//   as IndependenceTolerance is smaller than that, all is well
				String msg = formatRangeMessage("AO", i + 1, ais[i], aos[i], TOLERANCE_INDEPENDENCE);
				if (Math.abs(ais[i] - aos[i]) > TOLERANCE_INDEPENDENCE) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
				}
			}
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  AO tests complete");
	}
	
	private void checkPOE() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the PoE output");
		
		// If it's an EB base, check AI_POE for a value, otherwise expect something small (-8192, probably)
		short[] vals = udr.getA(S_DIAGS, AI_POE, (short)1);
		
		if (devName.startsWith("EB")) {
			if (Math.abs(EXP_POE - vals[0]) > TOLERANCE_POE) {
				throw new IOException(String.format("AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_NOTMEMBEROF, EXP_POE, TOLERANCE_POE));
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_MEMBEROF, EXP_POE, TOLERANCE_POE));
			}
		} else {
			if (vals[0] > 0) {
				throw new IOException("It appears that there is a PoE board on this module, but it has been initialized as an E2.");
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  As expected, there is no PoE board on this module.");
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  PoE test complete");
	}
	
	private void checkWatchdog() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the Watchdog output");
		
		// This relies on the DIR_START setup file that has the 
		// heartbeat setup for the watchdog as DI0 with 6 second timeout
		// Will only write memory and reset if data changes
		if (testLib.loadBaseFiles(S_TESTBASE, DIR_START)){
			// load returned true if it loaded the file and reset
			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for restart...");
			Utils.sleep(10000); //ensure the UUT is fully running after the reset
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the Watchdog output for ON...");
		// toggle the heartbeat register to have the UUT Watchdog output turn on
		boolean[] data = new boolean[1];
		data[0] = true;
		udr.putD(S_TESTBASE, UDRMessage.T_D_DIN, HEARTBEAT_DI, (short)1, data);
		data[0] = false;
		Utils.sleep(100);
		udr.putD(S_TESTBASE, UDRMessage.T_D_DIN, HEARTBEAT_DI, (short)1, data);
		Utils.sleep(2000); //chan. scan interval for the 16AI8AO is ~1.6 seconds)
		
		//The Analog outs module is a 16AI-8AO so it has the inputs for this check
		short[] vals = udr.getA(S_MEAS_OUTS, AI_24V, (short)1);
		short powerOffset = (short)(vals[0] - EXP_24V); // check to see how far off from 24V the power is to the UUT

		vals = udr.getA(S_MEAS_OUTS, AI_WATCHDOG, (short)1);
		// calc approximate equivalent voltages
		float measVolts = (float)((((float)(vals[0] / 32768.0) * 0.016) + 0.004) * 4000.0 ); 
		float expVolts = (float)((((float)((EXP_WATCHDOG + powerOffset) / 32768.0) * 0.016) + 0.004) * 4000.0 ); 
		float tolVolts = (float)(((float)(TOLERANCE_WATCHDOG / 32768.0) * 0.016)  * 4000.0 ); 

		// we compensate for the amount the fixture power is away from the nominal 24V since the output voltage the 
		// watchdog will output is starts with the fixture PWR1 input voltage
		if (Math.abs(EXP_WATCHDOG - (vals[0] + powerOffset)) > TOLERANCE_WATCHDOG) {
			ui.displayMessage(this, UserInterface.M_ERROR, String.format("Watchdog output measured %d [approx. %f volts] and expected %d [%f volts] +/- %d [%f volts]", vals[0], measVolts, (EXP_WATCHDOG + powerOffset), expVolts, TOLERANCE_WATCHDOG, tolVolts));
			String errmsg = String.format("Error: Watchdog output (connected to Base3 AI Channel %d): is off when it should be on. Watchdog output measured approx. %f volts and expected %f +/- %f", AI_WATCHDOG, measVolts, expVolts, tolVolts);
			throw new IOException(errmsg);
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  Watchdog output on measures within acceptable limits at approx. %f volts.", measVolts));
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the Watchdog output for OFF...");
		// Make sure the watchdog output is off
		// wait for the heartbeat timeout to expire and trigger the Watchdog output to go low and the 16AI8AO module to read it the new value.
		Utils.sleep(8000);
		
		vals = udr.getA(S_DIAGS, AI_WATCHDOG, (short)1);
		
		if (vals[0] > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output) is on when it should be off", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  Watchdog output was off when expected", AI_WATCHDOG));
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Watchdog test complete");
	}
	
	private void unregisterHandlers() {
		for (int i = 0; i < activeHandler.length; i++) {
			if (activeHandler[i] != null) {
				testLib.unregisterHandler(activeHandler[i]);
				activeHandler[i] = null;
			}
		}
	}
	
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
	
	private void setAOs(short[] aos) throws IOException, TimeoutException {
		// Loop through aos in blocks of 8
		for (int i = 0; i < aos.length; i += 8) {
			short stationID;
			
			switch (i) {
				case 0:
					stationID = S_OUTPUTS0_7;
					break;
				case 8:
					stationID = S_OUTPUTS8_15;
					break;
			default:
					throw new IOException("Cannot write more than 16 AOs");
			}
			
			int outputCount = 8;
			if (outputCount > aos.length - i) outputCount = aos.length - i;
			
			short[] block = new short[outputCount];
			System.arraycopy(aos, i, block, 0, outputCount);
			
			udr.putA(stationID, (short)0, (short)outputCount, block);
		}
	}
	
	private String formatRangeMessage(String type, int channel, short found, short expected, int tolerance) {
		String op = TestLib.CH_MEMBEROF;
		if (Math.abs(expected - found) > tolerance) op = TestLib.CH_NOTMEMBEROF;
		
		return String.format("%s Channel %,d: %s%,d [%,d %s (%,d +/- %,d)]", type, channel, TestLib.CH_DELTA, found - expected, found, op, expected, tolerance);
	}
	private void getAvgOfAllAIs(short[] UUT_AIs, short[] meas_AIs) throws IOException, TimeoutException {
		getAvgOfAllAIs(UUT_AIs, meas_AIs, TOLERANCE_AVERAGE);
	}
	private void getAvgOfAllAIs(short[] UUT_AIs, short[] meas_AIs, int consecutiveTol) throws IOException, TimeoutException {
		// reads the AI channels for the UUT and the Measurement AI modules 3 times and returns the avg for each.
		// Uses scanDelay for the time interval between reads
		
		// Grab what the station sees ScanCount times, once every scanDelay ms,
		// averaging the results
		// Make sure there are no significant differences between values
		// Use ints because we will add the results before dividing
		MultiIOException mioe = new MultiIOException();

		short count = (short)UUT_AIs.length;
		short scanCount = 3;
		int[] UUTaccumulator = new int[count];
		int[] MeasAccumulator = new int[count];
		short[] lastVal = null;
		int errCount = 0; // If it hits 2, break out
		boolean readError = false;
		int j = 0; // loop counter
		for (int i = 0; i < scanCount; i++) {
			readError = false;
			short[] UUTreading = udr.getA(S_TESTBASE, (short)0, (short)count);
			if (UUTreading.length != count) {
				readError = true;
			}
			short[] MeasReading1 = udr.getA(S_MEAS_AO0_15, (short)0, (short)16);
			if (MeasReading1.length != 16) {
				readError = true;
			}

			if(readError){
				errCount++;
			} else {
				errCount = 0;
			}
			
			if (errCount >= 2) {
				throw new IOException("Failed to read AI channel data correctly multiple times. ");
			}

			// try this scan again if error but errCount hasn't expired
			if (readError && (errCount < 2)){
				i--;
				continue;
			}

			// Add to what we have so far and make sure UUT values are not
			// significantly different from the previous and
			// add measured values from the measurement module for its avg too.
			for (j = 0; j < count; j++) {
				UUTaccumulator[j] += UUTreading[j];
				if (lastVal != null
						&& Math.abs(UUTreading[j] - lastVal[j]) > consecutiveTol) {
					mioe.add(new IOException(String	.format(
													"Consecutive passes over the same input value on AI channel %d (0-based) yielded %,d and %,d, which are more than %d apart",
													j, UUTreading[j], lastVal[j], consecutiveTol)));
					ui.displayMessage(this,	UserInterface.M_ERROR, String.format(
													"Consecutive passes over the same input value on AI channel %d (0-based) yielded %,d and %,d, which are more than %d apart",
													j, UUTreading[j], lastVal[j], consecutiveTol));
				}
				
				MeasAccumulator[j] += MeasReading1[j];
			}

			lastVal = UUTreading;

			// Wait a full UUT module scan interval before taking the next reading
			if((i + 1) < scanCount){ // don't need to wait on the last time through
				Utils.sleep(testLib.getScanDelay());
			}
		}

		// Average the results
		for (int i = 0; i < count; i++) {
			UUT_AIs[i] = (short) (UUTaccumulator[i] / scanCount);
			meas_AIs[i] = (short) (MeasAccumulator[i] / scanCount);
		}

		if (!mioe.isEmpty()) {
			throw mioe;
		}

		return;
		
	}
}

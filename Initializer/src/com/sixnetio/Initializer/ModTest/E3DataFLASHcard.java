/*
 * E3DataFLASHcard.java
 *
 * Tests for E3DataFLASHcard.  
 * No extra tests are required but the initializer's structure needs one to call.
 *
 * Keith Dymond
 * November 14, 2014
 *
 */

package com.sixnetio.Initializer.ModTest;

import com.sixnetio.COM.UBootCommunicator;
import com.sixnetio.Initializer.*;
import com.sixnetio.Station.MessageDispatcher;
import com.sixnetio.Station.TransportMethod;
import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRLink;
//import com.sixnetio.UDR.UDRHandler;

import java.io.*;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.util.*;

public class E3DataFLASHcard implements Tester {
	
    private static final String IP_FIXTURE_ETH1 = "10.1.0.1";
    private static final short STA = 1;

//    private static final String T_NETWORK_JUMPER = "Network Jumper Test";
    private static final String T_NOTEST = "No Test";
    
    private static final String[] TEST_NAMES = {
//        T_NETWORK_JUMPER,
        T_NOTEST
    };
    
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
    private UDRLink activeHandler;
    private String comPath;
    private UDRLink handler;

	public E3DataFLASHcard() { }
	
	public String getJumpTest() {
		return chosenTest;
    }
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}	
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
    }
	
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
    }
	
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for E3DataFLASHcard must not be null");
		
		// Make sure the device name is recognized
		if (!(devName.contains("E3DataFLASHcard"))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)113, (byte)0);
		testLib = new TestLib(udr, ui);
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
    }
	
	public boolean supportsTestJump() {
		return true;
    }
	
	public void run() {
		MultiIOException mioe = new MultiIOException();
		
/*        // Connect
        try {
            // Use ethernet port 2 for about half of the tests, then switch to
            // port 1
            // Start with port 2 because it should always be plugged in, so we
            // are more likely
            // to get a connection and then have the diagnostics test fail with
            // a descriptive
            // message if ethernet 1 is not plugged in
            activeHandler = testLib.registerUDPHandler(IP_FIXTURE_ETH1);

            // Make sure the connection works
            ui.displayMessage(this, UserInterface.M_WARNING,
                "Waiting for device to respond");
            udr.waitForResponse(STA);

        }
        catch (Exception e) {
            ui.displayMessage(this, UserInterface.M_ERROR,
                "Unable to connect through the Ethernet1 port");
            ui.handleException(this, e);
            ui.operationFailed(this);
            return;
        }
*/
/*        if (chosen(T_NETWORK_JUMPER)) {
            try { 
                // Now start doing tests
                // Network Jumper Test
                try {
                    TestNetworkJumper();
                } catch (IOException ioe) {
                    mioe.add(ioe);
                }
            } catch (Exception e) {
                ui.handleException(this, e);
                ui.operationFailed(this);
                return;
            }
        }
*/        
        if (chosen(T_NOTEST)) {
            
            try { 
                // Final Test
                try {
                    if (chosen(T_NOTEST)) NoTest();
                } catch (IOException ioe) {
                    mioe.add(ioe);
                }
            } catch (Exception e) {
                ui.handleException(this, e);
                ui.operationFailed(this);
                return;
            }
        }
        // Disconnect the handler
//        activeHandler = testLib.unregisterHandler(activeHandler);
        
		if (!mioe.isEmpty()) {
			ui.handleException(this, mioe);
			ui.operationFailed(this);
		} else {
	        ui.displayMessage(this, UserInterface.M_NORMAL, "Separate paired unit load via ModuleCheck.exe still needed.");
			ui.operationCompleted(this);
		}
    }
	
	// Tests the Network Jumper by confirming it reads as PassThru (the card needs to be set to passthru)
    private void TestNetworkJumper() throws Exception, IOException, TimeoutException {
        MultiIOException mioe = new MultiIOException();
//This is incomplete at this time
//USB
/*        ui.displayMessage(this, UserInterface.M_WARNING, "Testing USB Port");
        
        UDRLink usbHandler = null;
        
        String comPath = ui.getPersistentValue("IPm2m.defaultComPath");
        if (comPath == null) comPath = "";
        
        // Disconnect the active handler and register one for the USB port
        // This closes the handler, so we might as well discard the reference to it
            activeHandler = testLib.unregisterHandler(activeHandler);
        
        boolean succeeded = false; // Keep trying until the user says to stop
        
        try {
            do {
                while ((comPath = ui.requestString("Please connect the USB cable and enter the name of the serial port associated with the USB connection to the device", "USB Port", comPath)) == null) {
                    if (ui.confirm("Really cancel the test?", "Cancel")) {
                        throw new Exception("User canceled the test");
                    }
                    
                    comPath = "";
                }
                
                ui.setPersistentValue("IPm2m.defaultComPath", comPath);
                
                usbHandler = new UDRLink(TransportMethod.Serial, comPath + " 9600-8-N-1");
                MessageDispatcher.getDispatcher().registerHandler(usbHandler);
                
                try {
                    testLib.verifyConnectivity(STA);
                    
                    succeeded = true;
                } catch (Exception e) {
                    Utils.debug(e);
                    
                    succeeded = false;
                }
            } while (!succeeded && ui.confirm("Unable to communicate with the device over USB, try again?", "Failed"));
        } finally {
            testLib.unregisterHandler(usbHandler);
            
            // Re-create the UDP handler
 //               activeHandler = testLib.registerUDPHandler(IP_FIXTURE_ETH1);
        }
        
        if (!succeeded) {
            throw new IOException("Unable to communicate with the device over USB");
        }
        
        ui.displayMessage(this, UserInterface.M_NORMAL, "  USB test complete");*/
// USB
        //OpenUDR-USB
        // Keep trying every 500ms for 30 seconds
        long startTime = System.currentTimeMillis();
        
        while (startTime + 30000 > System.currentTimeMillis()) {
            try {
                handler = new UDRLink(TransportMethod.Serial, comPath + " 9600-8-N-1");
                MessageDispatcher.getDispatcher().registerHandler(handler);
                
                return;
            } catch (IOException ioe) {
                Utils.sleep(500);
            }
        }

       // Increase these, since we're probably copying a lot of files
        //int oldTries = udr.getTries();
        //int oldDelay = udr.getFileDelay();
        
        //udr.setTries(5);
        //udr.setFileDelay(75);
        
/*        try {
            try {
                // do the test
                ui.displayMessage(this, UserInterface.M_WARNING, "Clearing " );
            } catch (IOException ioe) {
                // Perhaps it was empty? Ignore and try to copy the real files
                ui.displayMessage(this, UserInterface.M_WARNING, "  Error clearing, attempting copy anyway");
            }
            
            
        } catch (Exception e) {
            ui.handleException(this, e);
            ui.operationFailed(this);
            return;
        } 
*/        
        ui.displayMessage(this, UserInterface.M_NORMAL, "  Finished copying files");
        ui.operationCompleted(this);

        ui.displayMessage(this, UserInterface.M_NORMAL, "Testing Network Jumper set to PassThru.");
        try {
            byte[] networkbyte = new byte[1]; 
            // Read the network jumper setting the firmware read
            networkbyte[0] = -1;
            networkbyte = udr.readFile((short) 1, "/base0/cfg/setup", 0x1A, 1, null);
            //0x00 = Pass-thru
            //0x01 = Ring Switch
            //0x02 = Two Networks
            if (networkbyte[0] == 0){ // it's the passthru setting so pass it
                ui.displayMessage(this, UserInterface.M_NORMAL, "Network Jumper confirmed set to PassThru.");
                ui.displayMessage(this, UserInterface.M_NORMAL, "  The Jumper could be moved to the Two Networks position in preparation for most products.");
                ui.displayMessage(this, UserInterface.M_NORMAL, "  Initial load complete but unit still needs to be paired with the unit it ships with.");
                ui.confirm("Did they all light?", "Walking LEDs");
            }
            else{ // it's not the expected passthru setting
                String position;
                switch (networkbyte[0]){
                    case 0x02:  position = "0x02 (Two Networks)";
                    break;
                    case 0x01:  position = "0x01 (Ring Switch)";
                    break;
                    default:  position = String.format("0x%02X (Unknown)", networkbyte[0]);
                    break;
                }
                
                if (ui.confirm(String.format("The Network Jumper was not found to be set to 'PASS-THRU'." +
                        "Found value %s" + 
                        "\nDo you wish to retry the test?" +
                        "\nTo do so you must do the following before answering yes to this dialog." +
                        "\npower down the unit," +
                        "\nunplug the dataflash card, " +
                        "\nset the jumper to PASS-THRU," +
                        "\nreconnect the dataflash card, " +
                        "\npower the unit back up.", position),"Network Jumper Position")) {
                    TestNetworkJumper(); // this should be okay since the user isn't likely to retry too many times
                }
            }
            
        } catch (Exception e) {
            ui.handleException(this, e);
            ui.operationFailed(this);
            return;
        }finally {
            // close UDR-USB
            if (handler != null) {
                MessageDispatcher.getDispatcher().unregisterHandler(handler);
                
                handler.close();
                handler = null;
            }
            
            //udr.setTries(oldTries);
            //udr.setFileDelay(oldDelay);
        }

        if (!mioe.isEmpty()) throw mioe;        
        ui.displayMessage(this, UserInterface.M_WARNING, "  Initial load complete but unit still needs to be paired with the unit it ships with.");
    }
    
    private void NoTest() throws Exception, IOException, TimeoutException {
        MultiIOException mioe = new MultiIOException();
        ui.displayMessage(this, UserInterface.M_NORMAL, "Separate paired unit load via TE3_Final_Assembly_Process.r##.doc still needed.");
        

        if (!mioe.isEmpty()) throw mioe;        
        ui.displayMessage(this, UserInterface.M_WARNING, "  Initial load complete but unit still needs to be paired with the unit it ships with.");
    }
    
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
}

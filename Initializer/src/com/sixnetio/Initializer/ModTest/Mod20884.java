/*
 * Mod20884.java
 *
 * Tests a MIX20884 ET2 module.
 *
 * Jeff Collins
 * December 14, 2012
 *
 */

package com.sixnetio.Initializer.ModTest;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.Initializer.Tester;
import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.MultiIOException;
import com.sixnetio.util.UserInterface;
import com.sixnetio.util.Utils;

public class Mod20884
    implements Tester
{
    // Note: We use UDP communications because we will likely need to reset the
    // station
    // numerous times, and that way we don't need to worry about re-establishing
    // the
    // connection each time

    // Constants
    // Try to keep every test module using a different set of IP addresses, just
    // in case we figure out
    // a way to run multiple modules from the same machine at the same time
    private static final String IP_CALBASE = "10.1.15.1";
    private static final String IP_TESTBASE = "10.1.15.2";

    // Station numbers
    private static final short S_CALBASE = 1; // Calibration, testing of DIs
    private static final short S_TESTBASE = 2; // Verification, testing of DOs
    private static final short S_ANALOGOUTS = 3; // Analog outputs to assist
    // with calibration (E2-16AI8A0)
    private static final short S_MEASANALOG = 4; // Analog inputs to assist
    // with calibration (E2-16ISO20M)
    private static final short S_OUTPUTS16_23 = 7; // Analog outputs to test
    // discrete inputs
    private static final short S_OUTPUTS8_15 = 6; // Analog outputs to test
    // discrete inputs
    private static final short S_OUTPUTS0_7 = 5; // Analog outputs to test
    // discrete inputs

    // Locations of the configuration files
    // Starting configurations
    /*
     * Changes from default setup: Ethernet: - Manual IP address (10.1.0.1 for
     * A, 10.1.0.2 for B) - Force ethernet pass-thru for A - Force two networks
     * for B RS-485: - SIXNET Universal and Modbus RTU/Binary Passthru, 9600 8N1
     * Discrete Options: - Do not use last 8 discrete channels as outputs for A
     * - Force DI- for A - Force DI+ for B Analog Input Options: - Feature 1 set
     * to "- Below 4ma" for first 8 inputs for A
     */
    private static final String DIR_CALBASE =
        "TestData" + File.separator + "MIX20884" + File.separator + "BaseA";
    private static final String DIR_TESTBASE =
        "TestData" + File.separator + "MIX20884" + File.separator + "BaseB";

    // Loaded into the calibration base
    // Set analog inputs 0 and 1 to 10-bit high speed mode
    private static final String DIR_10BIT =
        "TestData" + File.separator + "MIX20884" + File.separator + "10bit";

    // Make sure there is a watchdog failure
    private static final String DIR_WATCHDOG_OFF =
        "TestData" + File.separator + "MIX20884" + File.separator +
            "WatchdogOff";

    // Which AI the watchdog output terminal is plugged into
    private static final short AI_WATCHDOG = 1; // The last one is 7 (0-based)

    // Which AI terminal sampling fixture power is plugged into
    private static final short AI_24V = 0; // The last one is 7 (0-based)

    // Which AI the POE output terminal is plugged into
    // private static final short AI_POE = 5;

    // Base directory in a module where we will find the configuration files
    // We expect a slash on the end of this
    private static final String DIR_MODULECONFIG = "/module0/arc/";

    // Offsets for a couple of things
    private static final int OFFSET_AI_CALIBRATION = 0x100; // In the module
    // cals file
    private static final int OFFSET_AO_CALIBRATION = 0x180; // Also in the
    // module cals file

    // Lengths for the same things (so we can zero them as necessary)
    private static final int LENGTH_AI_CALIBRATION = 0x80;
    private static final int LENGTH_AO_CALIBRATION = 0x80;

	// Diagnostic DI offsets from DI_DiagStart
	private static final short DI_DIAG_START = 32; // The first diagnostic DI
	private static final short DIAG_SELFTEST = 0, // Module Selftest
	                           DIAG_POWER1 = 1, // Power 1
	                           DIAG_POWER2 = 2, // Power 2
	                           DIAG_IOSTATUS = 3, // I/O Status
	                           DIAG_ETH1LINK = 4, // Ethernet 1 has link
	                           DIAG_ETH2LINK = 5, // Ethernet 2 has link
	                           DIAG_RING = 6, // Ring is complete
	                           DIAG_POE = 7; // PoE is available
    
    // Analog values (to avoid hard-coding them in multiple places)
    private static final short EXP_24V = (short)4096; // Expected watchdog
    // input value; 4096
    // = 6.0mA = 24.0V
    // across 4000 Ohms
    private static final short EXP_WATCHDOG = (short)3686; // Expected watchdog
    // input value; 3686
    // = 5.8mA = 23.2V
    // across 4000 Ohms
    private static final short AO_900mV = (short)5898; // To output 0.9volt from UUT AO
    // module
    private static final short AI_900mV_on_mA = (short)27878; // expected input value 
    // of 0.9volt on the 16ISO20M module
    // input value; 0.9V, 51.1 ohms = 17.6125mA = 27878 counts 
    // [((0.9/51.1)Amps - .004Amps) / .016Amps * 32768 = 27878]
    // Since this is measuring a voltage output the 51.1 ohm res is replaced 
    // with a 20K (max load for the voltage output.
    private static final short AO_4MA = (short)0; // To output 4mA from an AO
    // module
    private static final short AO_5MA = (short)2047; // To output 5mA from an AO
    // module in Base 3
    private static final short AO_18MA = (short)28671; // To output 18mA from an
    // AO module in Base 3
    private static final short AO_1_75V = (short)2495; // To output 1.75V from
    // AO modules in Base 5
    private static final short AO_ISO_DI_ON = (short)20000; // To output a voltage
    // of ~3.25V on the rev A & B PCBA and ~3.82V for rev C PCBA AO modules in Base 5
     private static final short AO_5_1V = (short)13650; // To output 5.1V from an
    // AO modules in Base 5, Base 6 and Base 7
    private static final short AO_8_5V = (short)28445; // To output 8.5V from an
    // AO modules in Base 5, Base 6 and Base 7
    private static final short AO_0V = (short)0; // To output 0V from
    // AO modules in Base 5, Base 6 and Base 7

    // Tolerance for analog values
    // If two analog values should be equal, values within this distance on
    // either side will be accepted
    private static final int TOLERANCE_ANALOG = 12;

    // Similar to AnalogTolerance, this is used to make sure consecutive scans
    // during an average are close enough
    // Since values are allowed to be off by 12 in either direction of the
    // target value, a difference of 24 covers
    // the situation where one scane sees a value 12 below and the next scan
    // sees 12 above, then we add a little
    // extra to avoid spurious errors
    private static final int TOLERANCE_AVERAGE = 25;

    // When testing for channel independence (all get different values), this is
    // the tolerance for each channel
    private static final int TOLERANCE_INDEPENDENCE = 25;

    // When testing for AO channel at 5V range, this is
    // the tolerance for at the 4-20 input used to measure it.
    // +/- 0.025V = 1002counts -> 0.489mA = 0.03v/51.1ohm
    private static final int TOLERANCE_5V_RANGE = 1002; 

    // Used to check that the Watchdog input is close to what it should be
    private static final int TOLERANCE_WATCHDOG = 450;
    // Used to check that the Watchdog input is close to what it should be
    private static final int TOLERANCE_DOUT = 250;

    // Same as Analog/Average/Independence Tolerance, but these are used for
    // 10-bit high-speed
    // mode, which is wildly less accurate (6 fewer bits means +/- 64 for one
    // analog point)
    // Allowed to be 12 analog points off, so 768 makes sense (12 * 64)
    private static final int TOLERANCE_HIGHSPEED_ANALOG = 768;

    // 768 below, then 768 above means 2 * 768 range across consecutive values
    private static final int TOLERANCE_HIGHSPEED_AVERAGE = 1536;

    // IndependenceTolerance is equal to AverageTolerance, so same here
//    private static final int TOLERANCE_HIGHSPEED_INDEPENDENCE = 1536;

    // These are all of the test names that may be jumped to
    // They are in the order in which they would be run in a complete test
    private static final String T_DIAG1 = "Diagnostics Test (Base 1)",
    	T_DI = "DI Test", 
        T_AI_CAL = "AI Calibration", T_AI = "AI Test",
        T_DIAG2 = "Diagnostics Test (Base 2)", T_DI_SOURCE = "DI Sourcing Test",
        T_DO = "DO Test",
        T_AO_CAL = "AO mA Calibration", T_AO = "AO mA Test", T_AO_5V = "AO 5V Range Test",
        T_WATCHDOG = "Watchdog Test" ;

    private static final String[] TEST_NAMES = {
        // Base 1
    	T_DIAG1, T_DI, T_AI_CAL, T_AI, T_AO_CAL, T_AO, 

        // Base 2
    	T_DIAG2, T_DI_SOURCE, T_DO, T_AO_5V, T_WATCHDOG};

    // Private data members
    private String chosenTest;
    private UserInterface ui;
    private String devName;
    private UDRLink activeHandler;
    private boolean initialized = false;
    private boolean runVerifyTest = false;
    private UDRLib udr;
    private TestLib testLib;
    private int modRev;
    private short AO_offVal = AO_5_1V;
    private short AO_onVal = AO_8_5V;

    // Constructor
    public Mod20884()
    {
    }

    // Required by Tester
    // public void setupTest(UserInterface ui, String comPort, String devName)
    // throws IOException
    public void setupTest(UserInterface ui, String comPort, String devName)
        throws IOException
    {
        if (ui == null) {
            throw new IllegalArgumentException(
                "The UserInterface passed to setupTest for Mod20884 must not be null");
        }

        // Make sure the device name is recognized
        // Must be based on the E2-MIX20884 
        if (!(devName.contains("20884") ||  // E2-MIX20884 & EMIX20884
              devName.contains("10-7618")) ) {  // REMs 10-7618
            throw new IOException("Unrecognized device name: " + devName);
        }
        // ###what is REMs version part number?
        this.ui = ui;
        // These tests run over IP, no need for the COM port
        this.devName = devName; // Need to know which 88x module it is

        udr = new UDRLib((short)100, (byte)0);
        testLib = new TestLib(udr, ui);

        if (initialized) {
            return; // That's it on the changes that may take place between
            // calls
        }

        initialized = true;
    }

    // public boolean supportsTestJump()
    public boolean supportsTestJump()
    {
        return true;
    }

    public boolean runVerifyTests(boolean verifyTest)
    {
        runVerifyTest = verifyTest;
        return verifyTest;
    }

    // public List<String> getTests()
    public List<String> getTests()
    {
        return Utils.makeVector(TEST_NAMES);
    }

    // public void jumpToTest(String testName)
    public void jumpToTest(String testName)
    {
        if (testName != null) {
            List<String> tests = getTests();
            if (!tests.contains(testName)) {
                throw new IllegalArgumentException(
                    "Test name not recognized: " + testName);
            }
        }

        chosenTest = testName;
    }

    // public String getJumpTest()
    public String getJumpTest()
    {
        return chosenTest;
    }

    // public void run()
    public void run()
    {
        if (devName.endsWith("10-7618")) { // check the hardware rev for the REM board only
            // Make sure the module is in the first base
            if (ui.confirm("Units older than 1.20 (i.e. 1.03) must be replaced with 1.20 or newer per" +
                "\nagreement with REM/Spartan.  Test will abort if an older unit is reported (answer NO)." +
                "\nThe revision number can be found on the module's side label." +
                "\nUnits still under warranty should be charged $0 for this service and " +
                "\nunits not covered by warranty should be charged $233 [($623-$40)*.4] for this service." + 
                "\n\nIs the hardware rev of the module under test 1.20 or newer?",
                "Module Revision Check")) {
            }
            else{
                ui.displayMessage(this, UserInterface.M_ERROR,
                    "Hardware revision older than 1.20 was reported.");
                ui.displayMessage(this, UserInterface.M_ERROR,
                    "Tests cancelled since the product will be replaced per agreement with REM.");
                ui.operationFailed(this);
                return;
            }
        }
        // Tests begin
        if (chosen(T_DIAG1) || chosen(T_DI) || chosen(T_AI_CAL) ||
            chosen(T_AI) || chosen(T_AO_CAL) || chosen(T_AO)) {

            // Make sure the module is in the first base
            if (!ui.confirm("Please plug the module into the first base",
                "Module Check")) {
                ui.displayMessage(this, UserInterface.M_ERROR,
                    "User cancelled the test");
                ui.operationFailed(this);
                return;
            }
            // Register a UDP handler for test base A
            try {
                activeHandler = testLib.registerUDPHandler(IP_CALBASE);

                ui.displayMessage(this, UserInterface.M_WARNING,
                    "Waiting for device to respond");
                udr.waitForResponse(S_CALBASE);
            }
            catch (Exception e) {
                ui.handleException(this, e);
                ui.operationFailed(this);
                return;
            }
            try { // Finally block unregisters activeHandler
                try {
                    // Load the default configuration to the base so we know
                    // what we're working with
                    testLib.loadBaseFiles(S_CALBASE, DIR_CALBASE);
                }
                catch (Exception e) {
                    ui.handleException(this, e);
                    ui.operationFailed(this);
                    return;
                }

                try {
                    MultiIOException mioe = new MultiIOException();
                    //byte[] rev =
                    //    udr.readFile(S_CALBASE, "/module0/arc/factory", 22, 2,
                    //        null);
                    //String strValue = Conversion.bytesToHex(rev, "");
                    //modRev = Integer.parseInt(strValue);
                    //ui.displayMessage(this, UserInterface.M_NORMAL,
                    //    "Module Revision = 0" + modRev);

	    			// Diagnostics test 1
	    			try {
	    				if (chosen(T_DIAG1)) checkDiags1();
	    			} catch (IOException ioe) {
	    				mioe.add(ioe);
	    			}
                    // Walk the AOs of an external device across the DIs of the
                    // device under test
                    try {
                        if (chosen(T_DI)) {
                            walkDIs();
                        }
                    }
                    catch (IOException ioe) {
                        // This one does not print its own error messages, so
                        // summarize for the user
                        ui.displayMessage(this, UserInterface.M_ERROR, ioe
                            .getMessage());
                        mioe.add(ioe);
                    }

                    // Calibrate the AIs of the DOT with the AOs of an external
                    // device
                    try {
                        if (chosen(T_AI_CAL) && (!runVerifyTest)) {
                            calibrateAIs();
                        }
                        if (chosen(T_AI)) {
                            testAIs(); // If the calibration fails, don't do the
                            // test
                        }
                    }
                    catch (IOException ioe) {
                        mioe.add(ioe);
                    }

                    // Calibrate AOs from our newly-calibrated AIs
                    try {
                        if (chosen(T_AO_CAL) && (!runVerifyTest)) {
                            calibrateAOs();
                        }
                        if (chosen(T_AO)) {
                            testAOs(); // If the calibration fails, don't do the
                            // test
                        }
                    }
                    catch (IOException ioe) {
                        mioe.add(ioe);
                    }

                    if (!mioe.isEmpty()) {
                        throw mioe;
                    }

                }
                catch (Exception e) {
                    ui.handleException(this, e);
                    ui.operationFailed(this);
                    return;
                }
            }
            finally {
                // Disconnect the handler
                activeHandler = testLib.unregisterHandler(activeHandler);
            }
        }


        if (chosen(T_DIAG2) || chosen (T_DI_SOURCE) || chosen(T_DO) || chosen(T_AO_5V) || chosen(T_WATCHDOG)) {
            // Tell the user to move the module to the second base
            while (!ui.confirm("Please move the module to the second base",
                "Move the Module")) {
                if (ui.confirm("Really cancel the test?", "Cancel")) {
                    ui.displayMessage(this, UserInterface.M_ERROR,
                        "User canceled the test");
                    ui.operationFailed(this);
                    return;
                }
            }

            // Connect a new handler
            try {
                activeHandler = testLib.registerUDPHandler(IP_TESTBASE);
            }
            catch (IOException ioe) {
                ui.handleException(this, ioe);
                ui.operationFailed(this);
                return;
            }

            try { // Finally block unregisters the handler
                try {
                    // Load the default configuration to the base so we know
                    // what we're working with
                    testLib.loadBaseFiles(S_TESTBASE, DIR_TESTBASE);
                }
                catch (Exception e) {
                    ui.handleException(this, e);
                    ui.operationFailed(this);
                    return;
                }

                try {
                    MultiIOException mioe = new MultiIOException();

	    			// Diagnostics 2 test
	    			try {
	    				if (chosen(T_DIAG2)) checkDiags2();
	    			} catch (IOException ioe) {
	    				mioe.add(ioe);
	    			}
	    			
                    // Make sure that the sourced outputs (tied to ground) are
                    // all on
                    try {
                        if (chosen(T_DI_SOURCE)) {
                            testSourcing();
                        }
                    }
                    catch (IOException ioe) {
                        // This one does not print its own error messages, so
                        // summarize for the user
                        ui.displayMessage(this, UserInterface.M_ERROR, ioe
                            .getMessage());
                        mioe.add(ioe);
                    }
                    
                    // Walk our own DOs, which feed into our DIs to test the DOs
                    try {
                        if (chosen(T_DO)) {
                            walkDOs();
                        }
                    }
                    catch (IOException ioe) {
                        // This one does not print its own error messages, so
                        // summarize for the user
                        ui.displayMessage(this, UserInterface.M_ERROR, ioe
                            .getMessage());
                        mioe.add(ioe);
                    }

                    // Check 5V range AOs 
                    try {
                        if (chosen(T_AO_5V)) {
                            test5vAOs(); // check the 5V range AOs; uses mA cal for this range too
                            // test
                        }
                    }
                    catch (IOException ioe) {
                        mioe.add(ioe);
                    }

                    // Check the Watchdog input
                    try {
                        if (chosen(T_WATCHDOG)) {
                            checkWatchdog();
                        }
                    }
                    catch (IOException ioe) {
                        mioe.add(ioe);
                    }

                    if (!mioe.isEmpty()) {
                        throw mioe;
                    }
                }
                catch (Exception e) {
                    ui.handleException(this, e);
                    ui.operationFailed(this);
                    return;
                }
            }
            finally {
                // Disconnect the handler
                activeHandler = testLib.unregisterHandler(activeHandler);
            }
        }

        if (chosenTest == null) {
            // null is all tests
            // write the pass flag
            if (writePassedFlag(true)){
                ui.displayMessage(this, UserInterface.M_NORMAL,
                "Module tests passed");
            }
            else{
                ui.handleException(this, new IOException("Failed to write the All Tests Passed flag to module."));
                ui.operationFailed(this);
                return;
            }
        }
        else {
            ui.displayMessage(this, UserInterface.M_NORMAL, "Module test '" +
                                                            chosenTest +
                                                            "' passed");
        }

        ui.operationCompleted(this);
    }

    private boolean writePassedFlag(boolean pass)
    {
        boolean ret = false;
        byte[] flagbyte = new byte[1]; 
        flagbyte[0] = 0; // must be 0 if failed
        if(pass) flagbyte[0] = 1; // must be 1 if pass
        try {
            activeHandler = testLib.registerUDPHandler(IP_TESTBASE);
            udr.writeFile(S_TESTBASE, "/module0/arc/factory", flagbyte, 0x4B, null);
            // Disconnect the handler
            activeHandler = testLib.unregisterHandler(activeHandler);
            ret = true;
        }
        catch (IOException e) {
            ret = false;
        }
        catch (TimeoutException e) {
            ret = false;
        }
    
        return ret;
    }

    // Testing functions
	private void checkDiags1() throws Exception, IOException, TimeoutException {
		final short stationID = S_CALBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		boolean[] dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected off)
		if (!dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 off");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 on");
			mioe.add(new IOException("Module reports power on power 1"));
		}
		
		// Power 2 (expected on)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 off");
			mioe.add(new IOException("Module reports no power on power 2"));
		}
		
		// Ethernet 1 (expected on)
		if (dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 1. Please verify the ethernet cable from the power injector to Ethernet Port 1 is secure. Continue the test?", "Ethernet 1 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 1 link");
			} else {
				dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH1LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 1"));
				}
			}
		}
		
		// Ethernet 2 (expected off)
		if (!dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 not linked");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 linked");
			throw new Exception("Module has a link on ethernet 2");
		}
		
		// POE (expected on if EB, off if E2)
		if (devName.startsWith("EB") && !dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  No POE");
			mioe.add(new IOException("Module reports no POE, although it is a POE module and ethernet 1 has a link"));
		} else if (devName.startsWith("E2") && dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE on non-EB module");
			mioe.add(new IOException("Module reports POE is available, although it is not an EB module"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks for Base 1 complete");
	}
	
	private void checkDiags2() throws Exception, IOException, TimeoutException {
		final short stationID = S_TESTBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		boolean[] dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected on)
		if (dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 off");
			mioe.add(new IOException("Module reports no power on power 1"));
		}
		
		// Power 2 (expected off)
		if (!dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 off");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 on");
			mioe.add(new IOException("Module reports power on power 2"));
		}
		
		// Ethernet 1 (expected off)
		if (!dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 not linked");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 linked");
			throw new Exception("Module has a link on ethernet 1");
		}
		
		// Ethernet 2 (expected on)
		if (dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 2. Please verify the ethernet cable from the switch to Ethernet Port 2 is secure. Continue the test?", "Ethernet 2 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 2 link");
			} else {
				dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH2LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 2"));
				}
			}
		}
		
		// POE (expected off, since ethernet 1 is not linked)
		if (!dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE powered with no Ethernet 1 link");
			mioe.add(new IOException("Module reports POE is available when there is no Ethernet 1 link"));
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks for Base 2 complete");
	}
    // private void walkDIs() throws IOException, TimeoutException
    private void walkDIs()
        throws IOException, TimeoutException
    {
        ui.displayMessage(this, UserInterface.M_WARNING, "Testing DIs");

        // Zero all outputs
        short[] zero = new short[8];
        Arrays.fill(zero, AO_0V);

        udr.putA(S_OUTPUTS16_23, (short)0, (short)8, zero);
        udr.putA(S_OUTPUTS8_15, (short)0, (short)8, zero);
        udr.putA(S_OUTPUTS0_7, (short)0, (short)8, zero);

        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Please watch for 20 walking LEDs");

        if (ui.confirm("Are you ready to watch for the 20 walking DI LEDs?",
        "Walking 20 DI LEDs")) {
	        // do nothing
        }
        else {
        	throw new IOException("User canceled the test."); 
        }      
        // Give the user time to look back over at the DI LEDs on the module
        Utils.sleep(1500);

        // Walk the outputs
        boolean done = false;
        while (!done) {
            // There are 20 DIs, with input coming from AOs on fixed stations 5,
            // 6, and 7
            // Test for Isolated DIs (Channels 0-3, Guaranteed off @ 1.5V, Guaranteed on @ 4.0V )
            //  - KD 10/7/14 Guaranteed on for chan. 0-3 was originally 3.0V but the rev C PCBA turns 
            //  - on at a higher level.  PCB rev A & B threshold ~2.6, PCB revC threshold ~3.25V
            //  - the test value yields different voltages since the input circuit is different on
            //  - the different revs. and a current source is providing the input voltage.
            //  - The test voltages are ~3.25v (A&B) & ~3.8v (C).
            // (Channels 4-23, Guaranteed off @ 5V, Guaranteed on @ 9V, threshold ~7V )

        	// First 4 come from S_OUTPUTS0_7 (lower off value for Isolated DIs)
            walkAOs(S_OUTPUTS0_7, (short)4, S_CALBASE, (short)0,(short)20,
            		AO_1_75V, AO_ISO_DI_ON);

            // Next 4 come from S_OUTPUTS0_7
            Offset_walkAOs(S_OUTPUTS0_7, (short)4, S_CALBASE, (short)4,(short)20,
            		AO_5_1V, AO_8_5V);

            // Next 8 come from S_OUTPUTS8_15
            walkAOs(S_OUTPUTS8_15, (short)8, S_CALBASE, (short)8, (short)20,
            		AO_5_1V, AO_8_5V);

            // Last 4 come from S_OUTPUTS16_23
            walkAOs(S_OUTPUTS16_23, (short)4, S_CALBASE, (short)16, (short)20,
            		AO_5_1V, AO_8_5V);

            if (ui.confirm(
                "Did you see all 20 DI LEDs turn on one at a time?",
                "Walking LEDs")) {
            	done = true;
            }
            else {            	
	            if (ui.confirm("Would you like to retry the independant channel test?", "Walking LEDs")) {
	            	Utils.sleep(1500); // Give the user time to look back over at the DI LEDs on the module
	            }
                else {
	                done = true;
                	throw new IOException("One or more DI LEDs not turning on during walk test!");
                }
            }
            if (devName.endsWith("10-7618")) { // check the input threshold for the REM board only
                CheckDIOnThreshold(S_OUTPUTS0_7, (short)4, S_CALBASE, (short)0,(short)4);
            }
         }

        ui.displayMessage(this, UserInterface.M_NORMAL, "  DI tests complete");
        
    }
 
    // private void calibrateAIs() throws IOException, TimeoutException
    private void calibrateAIs()
        throws IOException, TimeoutException
    {
        MultiIOException mioe = new MultiIOException();

        ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating AIs");

        // Clear any calibration data that may exist
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Clearing current calibration");

        if (testLib.zeroRange(S_CALBASE, DIR_MODULECONFIG + "cals",
            OFFSET_AI_CALIBRATION, LENGTH_AI_CALIBRATION)) {
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Resetting station");
            testLib.resetStation(S_CALBASE);
        }

        ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");

        // There are 8 AIs coming from AOs from Base3(16AI8A) and measured by Base4 (16ISO20M)
        short[] aos = new short[8];

        // Set the scan delay
        // setup file sets 100ms per active channel, we add a little to be
        // careful
        testLib.setScanDelay(100 * aos.length + 200);

        // Calibration results will end up in these arrays
        short[] spans = new short[aos.length];
        short[] zeros = new short[aos.length];

        // Write 5mA to all input channels
        Arrays.fill(aos, AO_5MA);

        udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);

        // Need to wait for the analog out setpoint settle in. 
        // We need it to be stable before using it for cal
        Utils.sleep(3000);
        // give the module time scan all the channels with the now stable input
        Utils.sleep(testLib.getScanDelay());

        // Grab the results
        short[] aisLow =
            testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aos.length,
                TOLERANCE_ANALOG);

        short[] aisMeasLow =
            testLib.getAverageAnalogs(S_MEASANALOG, (short)0, (short)aos.length,
                TOLERANCE_ANALOG);

        // Write 18mA to all input channels
        Arrays.fill(aos, AO_18MA);
        udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);

        // Need to wait for the analog out setpoint settle in. 
        // We need it to be stable before using it for cal
        Utils.sleep(3000);
        // give the module time scan all the channels with the now stable input
        Utils.sleep(testLib.getScanDelay());

        // Grab the results
        short[] aisHigh =
            testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aos.length,
                TOLERANCE_ANALOG);

        short[] aisMeasHigh =
            testLib.getAverageAnalogs(S_MEASANALOG, (short)0, (short)aos.length,
                TOLERANCE_ANALOG);

        // Compute the module span and zero
        testLib.computeAICurrentCalibrationWithMeasuredSetpoint(aisLow, aisHigh, aisMeasLow, aisMeasHigh,
            zeros, spans);

        // Write the corrections to the config file
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Writing calibration data to station");

        byte[] calsData = new byte[aos.length * 4]; // 2 values of 2 bytes
        // apiece for each input
        int offset = 0;
        for (int i = 0; i < aos.length; i++) {
            Conversion.shortToBytes(calsData, offset, zeros[i]);
            offset += 2;

            Conversion.shortToBytes(calsData, offset, spans[i]);
            offset += 2;
        }

        udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", calsData,
            OFFSET_AI_CALIBRATION, null);

        ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");

        // Reset the station
        testLib.resetStation(S_CALBASE);

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  AI calibration calculations complete and written to unit");

        // Set all channels low/high and check that they are each where they
        // should be
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Testing all AIs with the same setpoint");
        {
            aos = new short[8];
            testLib.setScanDelay(100 * aos.length + 200);

            // 18mA already written to all channels
            {
                short[] ais =
                    testLib.getAverageAnalogs(S_CALBASE, (short)0,
                        (short)aos.length, TOLERANCE_ANALOG);
                
                short[] aisMeas =
                    testLib.getAverageAnalogs(S_MEASANALOG, (short)0, 
                        (short)aos.length, TOLERANCE_ANALOG);

                // Compare
                for (int i = 0; i < ais.length; i++) {
                    String msg =
                        formatRangeMessage("AI", i, ais[i], aisMeas[i],
                            TOLERANCE_ANALOG);
                    if (Math.abs(ais[i] - aisMeas[i]) > TOLERANCE_ANALOG) {
                        mioe.add(new IOException(msg));
                        ui.displayMessage(this, UserInterface.M_ERROR, "  " +
                                                                       msg);
                    }
                    else {
                        ui.displayMessage(this, UserInterface.M_NORMAL, "  " +
                                                                        msg);
                    }
                }
            }
            // Write 5mA to all channels
            {
                Arrays.fill(aos, AO_5MA);
                udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);
                Utils.sleep(testLib.getScanDelay() * 2);

                short[] ais =
                    testLib.getAverageAnalogs(S_CALBASE, (short)0,
                        (short)aos.length, 10);
                short[] aisMeas =
                    testLib.getAverageAnalogs(S_MEASANALOG, (short)0, 
                        (short)aos.length, 10);

                // Compare
                for (int i = 0; i < ais.length; i++) {
                    String msg =
                        formatRangeMessage("AI", i, ais[i], aisMeas[i],
                            TOLERANCE_ANALOG);
                    if (Math.abs(ais[i] - aisMeas[i]) > TOLERANCE_ANALOG) {
                        mioe.add(new IOException(msg));
                        ui.displayMessage(this, UserInterface.M_ERROR, "  " + 
                                                                       msg);
                    }
                    else {
                        ui.displayMessage(this, UserInterface.M_NORMAL, "  " + 
                                                                        msg);
                    }
                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }
    }

    // private void testAIs() throws IOException, TimeoutException
    private void testAIs()
        throws IOException, TimeoutException
    {
        MultiIOException mioe = new MultiIOException();

        ui.displayMessage(this, UserInterface.M_WARNING,
            "Testing AIs in 16-bit integrating mode");

        // Test independence of channels
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Testing channel independence");

        {
            short[] aos = new short[8];
            for (int i = 0; i < aos.length; i++) {
//                aos[i] = (short)((AO_18MA - AO_5MA) * i / aos.length + AO_5MA);
                aos[i] = (short)(32767 * i / aos.length);
            }
            udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);
            testLib.setScanDelay(100 * aos.length + 200);
            Utils.sleep(testLib.getScanDelay() * 2);

            short[] aisMeas =
                testLib.getAverageAnalogs(S_MEASANALOG, (short)0, 
                    (short)aos.length, TOLERANCE_ANALOG);
            short[] ais =
                testLib.getAverageAnalogs(S_CALBASE, (short)0,
                    (short)aos.length, TOLERANCE_ANALOG);

            // Compare
            for (int i = 0; i < aos.length; i++) {
                // Check the device under test's read value against the 
                // calibrated device's for equality to an analog difference
                // of about 12.
                String msg =
                    formatRangeMessage("AI", i, ais[i], aisMeas[i],
                        TOLERANCE_ANALOG);
                if (Math.abs(ais[i] - aisMeas[i]) > TOLERANCE_ANALOG) {
                    mioe.add(new IOException(msg));
                    ui
                        .displayMessage(this, UserInterface.M_ERROR, "    " +
                                                                     msg);
                }
                else {
                    ui.displayMessage(this, UserInterface.M_NORMAL, "    " +
                                                                    msg);
                }
            }
        }

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  16-bit AI tests complete");

        // Switch to 10-bit mode and test the calibration again on individual
        // channels.  This verifies the two independent input pins to the CPU
        testLib.loadBaseFiles(S_CALBASE, DIR_10BIT);

        ui.displayMessage(this, UserInterface.M_WARNING,
            "Testing AI Channels 1 & 2 in 10-bit high-speed mode");

         MultiIOException mioe2;

        do {
            mioe2 = new MultiIOException();
            
            // Test for channel independence
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Testing channel 1 @ 18mA & channel 2 @ 5mA");

            {
                short[] aos = new short[2];
                aos[0] = AO_18MA;
                aos[1] = AO_5MA;
                udr.putA(S_ANALOGOUTS, (short)0, (short)aos.length, aos);
                Utils.sleep(testLib.getScanDelay() * 2);

                // Get the average value
                short[] ais =
                    testLib.getAverageAnalogs(S_CALBASE, (short)0,
                        (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);
                
                short[] aisMeas =
                    testLib.getAverageAnalogs(S_MEASANALOG, (short)0, 
                        (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);

                // Compare
                for (int i = 0; i < aos.length; i++) {
                    // Check the device under test's read value against the 
                    // calibrated device's for equality to an analog difference
                    // of about 768.
                    String msg =
                        formatRangeMessage("AI", i + 1, ais[i], aisMeas[i],
                            TOLERANCE_HIGHSPEED_ANALOG);
                    if (Math.abs(ais[i] - aisMeas[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
                        mioe2.add(new IOException(msg));
                        ui.displayMessage(this, UserInterface.M_ERROR, "    " +
                                                                       msg);
                    }
                    else {
                        ui.displayMessage(this, UserInterface.M_NORMAL, "    " +
                                                                        msg);
                    }
                }
            }
        } while (!mioe2.isEmpty() &&
                 ui.confirm("High-speed analog input test failed, retry?",
                     "Retry 10-bit AI Test"));

        if (!mioe2.isEmpty()) {
            mioe.add(mioe2);
        }

        if (!ui.confirm("Are all of the AI LEDs lit?", "AI LEDs")) {
            mioe.add(new IOException("Not all AI LEDs light"));
        }

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  10-bit AI tests complete");

        if (!mioe.isEmpty()) {
            throw mioe;
        }
    }

    // private void testSourcing() throws IOException, TimeoutException
    private void testSourcing()
        throws IOException, TimeoutException
    {
        MultiIOException mioe = new MultiIOException();

        ui
            .displayMessage(this, UserInterface.M_WARNING,
                "Testing Sourcing DIs");

        // Grab the sourcing DIs (DIs 4-11 0 based)
        // They are all tied to ground, so they should all be on
        boolean[] dis = udr.getD(S_TESTBASE, (short)4, (short)8);

        // Check them
        for (int i = 0; i < dis.length; i++) {
            if (!dis[i]) {
                mioe
                    .add(new IOException("Sourcing DI " + i +
                                         " (0-based) should be on but it is off"));
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  Sourcing DI tests complete");
    }

    // private void walkDOs() throws IOException, TimeoutException
    private void walkDOs()
        throws IOException, TimeoutException
    {
        MultiIOException mioe = new MultiIOException();

        ui.displayMessage(this, UserInterface.M_WARNING, "Testing DOs");

        // There are 8 DOs, with input coming from our own outputs
        boolean[] dos = new boolean[8];

        // Start out by setting everything off
        Arrays.fill(dos, false);
        udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);
        
        // vals read from base 3 (16AI8AO): AI0 for 24V power value and AI2-AI5 for DO4-DO7 values
        short vals[] = new short[6];
        vals = udr.getA(S_ANALOGOUTS, AI_24V, (short)1);
        short EXP_DOUT = (short) vals[0]; // P1 PWR value at AI0 on 16AI8AO should be close to expected DO values          
        
        // Loop through, turning each on and then off
        for (int i = 0; i <= dos.length; i++) {
            // Turn off the previous bit
            if (i > 0) {
                dos[i - 1] = false;
            }

            // Turn on the current bit
            if (i < dos.length) {
                dos[i] = true;
            }

            udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);

            // Wait for the electronics to do their thing and for the user to
            // see it
            Utils.sleep(1000);

            // Read the DIs to see if they match
            // DOs 0-3 are plugged into DIs 0-3 on Base 2 (E2-MIX20884 UUT)
            // DOs 4-7 are plugged into AIs 2-5 on Base 3 (16AI8AO)

            boolean[] dis = udr.getD(S_TESTBASE, (short)0, (short)dos.length);
            vals = udr.getA(S_ANALOGOUTS, (short)2, (short)4);
            for (int k = 0; k < 4; k++){
            	if (vals[k] > (EXP_DOUT-TOLERANCE_DOUT) && vals[k] < (EXP_DOUT+TOLERANCE_DOUT) ){
            		dis[k+4] = true;
            		if (dos[k+4] == false){
            			String msg = formatRangeMessage("AI for DO", k+4, vals[k], EXP_DOUT,
                                TOLERANCE_DOUT);
                            mioe.add(new IOException(msg));
                            ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
                        
            		}
            	}
        		else {
        			dis[k+4] = false;
            		if (dos[k+4] == true){
            			String msg = formatRangeMessage("AI for DO", k+4, vals[k], EXP_DOUT,
                                TOLERANCE_DOUT);
                            mioe.add(new IOException(msg));
                            ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
            			
            		}
        		}
            }
            for (int j = 0; j < dos.length; j++) {
                if (dis[j] != dos[j]) {
                    if (dis[j]) {
                        mioe.add(new IOException(
                            "Station reports DI " + (j) +
                                " (0-based) is on when corresponding DO " + i +
                                " is off"));
                    }
                    else {
                        mioe.add(new IOException(
                            "Station reports DI " + (j) +
                                " (0-based) is off when corresponding DO " + i +
                                " is on"));
                    }
                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }
        // Turn all DOs off after test completes
        Arrays.fill(dos, false);
        udr.putD(S_TESTBASE, (short)0, (short)dos.length, dos);
        
        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  Finished testing DOs");
    }

    // private void calibrateAOs() throws IOException, TimeoutException
    private void calibrateAOs()
        throws IOException, TimeoutException
    {
        final short aoCount = 4;

        ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating AOs");

        // Clear any calibration data that may exist
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Clearing current calibration");

        if (testLib.zeroRange(S_CALBASE, DIR_MODULECONFIG + "cals",
            OFFSET_AO_CALIBRATION, LENGTH_AO_CALIBRATION)) {
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Resetting station");
            testLib.resetStation(S_CALBASE);
        }

        ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");

        // There are 4 AOs feeding the last for AIs on Base-4 (ET-16ISO20M)
        short[] aos = new short[aoCount];

        // Set the scan delay
        // set to 100ms per active channel, we add a little to be
        // careful
        // We are using 16 channels for the scan delay because there are 16 AIs,
        // in the measuring module even though there
        // are only 4 AOs here
        testLib.setScanDelay(100 * 16 + 200);


        Arrays.fill(aos, AO_4MA);
        udr.putA(S_CALBASE, (short)0, aoCount, aos);

        // Sleep for a bit so it can stabilize
        Utils.sleep(testLib.getScanDelay() * 2);

        // Read back what it is actually outputting
        short[] aisLow =
            testLib.getAverageAnalogs(S_MEASANALOG, (short)8, aoCount,
                TOLERANCE_AVERAGE);

        // Write 18mA to all input channels
        Arrays.fill(aos, AO_18MA);
        udr.putA(S_CALBASE, (short)0, aoCount, aos);

        // Wait for it to take effect
        Utils.sleep(testLib.getScanDelay() * 2);

        // Grab the results
        short[] aisHigh =
            testLib.getAverageAnalogs(S_MEASANALOG, (short)8, aoCount,
                TOLERANCE_AVERAGE);

        // Compute the module span and zero
        short[] zeros = new short[aoCount];
        short[] spans = new short[aoCount];

        testLib.computeAOCurrentCalibration(aisLow, aisHigh, zeros, spans);

        // Write the corrections to the config file
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Writing calibration data to station");

        byte[] calsData = new byte[aoCount * 8]; // 4 values of 2 bytes apiece
        // for each input
        int offset = 0;
        for (int i = 0; i < aos.length; i++) {
            Conversion.shortToBytes(calsData, offset, zeros[i]);
            offset += 2;

            Conversion.shortToBytes(calsData, offset, spans[i]);
            offset += 2;
        }

        udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", calsData,
            OFFSET_AO_CALIBRATION, null);

        ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");

        // Reset the station
        testLib.resetStation(S_CALBASE);

        // Set channels to 18mA
        Arrays.fill(aos, AO_18MA);

        // Write it to the station
        udr.putA(S_CALBASE, (short)0, (short)aos.length, aos);
        
        // Sleep so it can stabilize
        Utils.sleep(testLib.getScanDelay());

        // Get the average values
        short[] ais =
            testLib.getAverageAnalogs(S_MEASANALOG, (short)8,
                (short)aos.length, TOLERANCE_AVERAGE);

        // ensure 18mA is outputting 18mA
        MultiIOException mioe = new MultiIOException();
        for (int i = 0; i < aos.length; i++) {
            String msg =
                formatRangeMessage("AO", i + 1, ais[i], aos[i],
                    TOLERANCE_ANALOG);
            if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
                mioe.add(new IOException(msg));
                ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
            }
            else {
                ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
            }
        }
 

        // Verify high
        // Set channels to 5mA
        Arrays.fill(aos, AO_5MA);

        // Write it to the station
        udr.putA(S_CALBASE, (short)0, (short)aos.length, aos);

        // Sleep so it can stabilize
        Utils.sleep(testLib.getScanDelay() * 2);

        // Get the average value
        ais =
            testLib.getAverageAnalogs(S_MEASANALOG, (short)8,
                (short)aos.length, TOLERANCE_AVERAGE);

        // ensure 5mA is outputting 5mA
        for (int i = 0; i < aos.length; i++) {
            String msg =
                formatRangeMessage("AO", i + 1, ais[i], aos[i],
                    TOLERANCE_ANALOG);
            if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
                mioe.add(new IOException(msg));
                ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
            }
            else {
                ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
            }
        }
        
        
        if (!mioe.isEmpty()) {
            throw mioe;
        }
        
        ui.displayMessage(this, UserInterface.M_NORMAL,
        "  AO calibration complete");
    }

    // private void testAOs() throws IOException, TimeoutException
    private void testAOs()
        throws IOException, TimeoutException
    {

        MultiIOException mioe = new MultiIOException();

        ui.displayMessage(this, UserInterface.M_WARNING, "Testing AOs");

        // Test channels individually
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Testing channels individually");
        // Write 4mA to each channel individually, with the rest set to 18mA
        short[] aos = new short[4];

        // We are using 16 channels for the scan delay because there are 16 AIs
        // in the measuring module, even though there
        // are only 4 AOs
        testLib.setScanDelay(100 * 16 + 200);


        // Test channel independence
        {
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Testing channel independence");
            for (int i = 0; i < aos.length; i++) {
                aos[i] = (short)((AO_18MA - AO_5MA) * i / aos.length + AO_5MA);
            }

            udr.putA(S_CALBASE, (short)0, (short)aos.length, aos);

            // Sleep so it can stabilize
            Utils.sleep(testLib.getScanDelay() * 2);

            // Get the average value
            short[] ais =
                testLib.getAverageAnalogs(S_MEASANALOG, (short)8,
                    (short)aos.length, TOLERANCE_AVERAGE);

            // Compare
            for (int i = 0; i < aos.length; i++) {
                // A small amount of influence is allowed; a difference of 1mA
                // is equal to an analog difference of about 2048, so as long
                // as IndependenceTolerance is smaller than that, all is well
                String msg =
                    formatRangeMessage("AO", i + 1, ais[i], aos[i],
                        TOLERANCE_INDEPENDENCE);
                if (Math.abs(ais[i] - aos[i]) > TOLERANCE_INDEPENDENCE) {
                    mioe.add(new IOException(msg));
                    ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
                }
                else {
                    ui.displayMessage(this, UserInterface.M_NORMAL, "  " +
                                                                    msg);
                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }

        ui.displayMessage(this, UserInterface.M_NORMAL, "  AO tests complete");
    }
    
    // private void test5vAOs() throws IOException, TimeoutException\
    // Write 0.9v to each channel, can't go above 1 volt due to measuring 
    // module is scaled to a 4-20mA input expecting a 51.1 ohm resistor at it's 
    // input.  It really has a 20K which is the max load value for the voltage
    // output.  1 volt will yield a full scale report.
    private void test5vAOs()
        throws IOException, TimeoutException
    {

        MultiIOException mioe = new MultiIOException();

        ui.displayMessage(this, UserInterface.M_WARNING, "Testing 5V range AOs");

        // Test channels individually
        ui.displayMessage(this, UserInterface.M_WARNING,
            "  Testing Analog Outputs 0-5v range");
        // Write 0.9v to each channel
        short[] aos = new short[4];

        // We are using 16 channels for the scan delay because there are 16 AIs
        // in the measuring module, even though there
        // are only 4 AOs
        testLib.setScanDelay(100 * 16 + 200);


        // Test channels 
        {
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Testing channels at 0.9V +/- 0.025V");
            for (int i = 0; i < aos.length; i++) {
                aos[i] = AO_900mV ;
            }

            udr.putA(S_TESTBASE, (short)0, (short)aos.length, aos);

            // Sleep so it can stabilize
            Utils.sleep(testLib.getScanDelay() * 2);

            // Get the average value
            short[] ais =
                testLib.getAverageAnalogs(S_MEASANALOG, (short)12,
                    (short)aos.length, TOLERANCE_AVERAGE);
            
            // Compare
            for (int i = 0; i < aos.length; i++) {
                // A small amount of influence is allowed; a difference of 1mA
                // is equal to an analog difference of about 2048, so as long
                // as IndependenceTolerance is smaller than that, all is well
                String msg =
                    formatRangeMessage("  AO_5V_range", i + 1, ais[i], AI_900mV_on_mA,
                        TOLERANCE_5V_RANGE);
                if (Math.abs(ais[i] - AI_900mV_on_mA) > TOLERANCE_5V_RANGE) {
                    mioe.add(new IOException(msg));
                    ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
                }
                else {
                    ui.displayMessage(this, UserInterface.M_NORMAL, "    " +
                                                                    msg);
                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }

        ui.displayMessage(this, UserInterface.M_NORMAL, "  AO tests complete");
    }

    // private void checkWatchdog() throws IOException, TimeoutException
    private void checkWatchdog()
        throws IOException, TimeoutException
    {
        ui.displayMessage(this, UserInterface.M_WARNING,
            "Testing the Watchdog output");

        // Give it a couple seconds in case it was rebooted recently
        Utils.sleep(5000);

        //The Analog outs module is a 16AI-8AO so it has the inputs for this check
        short[] vals = udr.getA(S_ANALOGOUTS, AI_24V, (short)1);
        short powerOffset = (short)(vals[0] - EXP_24V); // check to see how far off the power is to the UUT
        
        vals = udr.getA(S_ANALOGOUTS, AI_WATCHDOG, (short)1);

        // adjust the watchdog value by the amount the power is off
        if (Math.abs(EXP_WATCHDOG - (vals[0] + powerOffset)) > TOLERANCE_WATCHDOG) {
            throw new IOException(String.format(
                "AI Channel %d (connected to Watchdog output): is off when it should be on",
                AI_WATCHDOG));
        }
        else {
            ui.displayMessage(this,UserInterface.M_NORMAL,String.format(
                "  AI Channel %d (connected to Watchdog output) is on as expected",
                AI_WATCHDOG));
        }

        // Make sure the watchdog output is off
        testLib.loadBaseFiles(S_TESTBASE, DIR_WATCHDOG_OFF);

        // Give it a couple extra seconds
        Utils.sleep(10000);

        vals = udr.getA(S_ANALOGOUTS, AI_WATCHDOG, (short)1);

        // adjust the watchdog value by the amount the power is off
        if ((vals[0] + powerOffset) > TOLERANCE_WATCHDOG) {
            throw new IOException(String.format(
                "AI Channel %d (connected to Watchdog output) is on when it should be off",
                AI_WATCHDOG));
        }
        else {
            ui.displayMessage(this,UserInterface.M_NORMAL,String.format(
                "  AI Channel %d (connected to Watchdog output) is off as expected",
                AI_WATCHDOG));
        }

        ui.displayMessage(this, UserInterface.M_NORMAL,
            "  Watchdog test complete");
    }

    // Testing helper functions
    // private void walkAOs(short stationID, int count, short testStationID,
    // short offset, short totalCount, short offVal, short onVal) throws
    // IOException, TimeoutException
    // stationID: The station with the AOs
    // count: The number of AOs to walk through (starts at 0)
    // testStationID: The station with the DIs
    // offset: The offset of the DI connected to the first AO (so we can make
    // sure they turn on)
    // totalCount: The total number of DIs on the station (so we can make sure
    // the rest stay off)
    // offVal: The AO value that should turn the DI off
    // onVal: The AO value that should turn the DI on
    private void walkAOs(short stationID, short count, short testStationID,
                         short offset, short totalCount, short offVal,
                         short onVal)
        throws IOException, TimeoutException
    {
        // One-by-one, turn on the outputs from 0 to count on stationID
        // Each time, read count inputs from testStationID at offset to see if
        // they turn on/off as expected

        MultiIOException mioe = new MultiIOException();

        short[] aos = new short[count];

        // Start out by setting everything to 0
        Arrays.fill(aos, offVal);
        udr.putA(stationID, (short)0, count, aos);
        Utils.sleep(250);

        // Make sure everything reads as 0
        boolean[] dis = udr.getD(testStationID, (short)0, totalCount);
        if (dis.length != totalCount) {
            throw new IOException(
                "Station did not respond to GetD message with the proper number of registers");
        }
        for (int i = 0; i < totalCount; i++) {
            if (dis[i]) {
                mioe.add(new IOException("After turning off all inputs, DI " +
                                         i + " (0-based) is on"));
            }
        }

        // Walk through, setting each to just below the guaranteed ON and
        // backing down the previous to just above the guaranteed OFF
        for (short i = 0; i <= count; i++) {
            // Set the previous output to the off value
            if (i > 0) {
                aos[i - 1] = offVal;
            }

            // Set the current output to the on value
            if (i < aos.length) {
                aos[i] = onVal;
            }

            udr.putA(stationID, (short)0, count, aos);

            Utils.sleep(250);

            dis = udr.getD(testStationID, (short)0, totalCount);

            for (int j = 0; j < dis.length; j++) {
                if (offset + i == j && !dis[j]) {
                    if (i < aos.length) {
                        mioe
                            .add(new IOException("After turning on DI " +
                                                 (offset + i) +
                                                 " (0-based), station reports it is off"));
                    }
                    else {
                        // We didn't turn anything on for this run, just turned
                        // the last one off
                    }
                }
                else if (offset + i != j && dis[j]) {
                    if (i < aos.length) {
                        mioe.add(new IOException(
                            "After turning on DI " + (offset + i) +
                                " (0-based), station reports DI " + j +
                                " is also on"));
                    }
                    else {
                        mioe.add(new IOException(
                            "After turning off all DIs, station reports DI " +
                                j + " (0-based) is on"));
                    }
                }
                else if (offset + i == j && !dis[j]) {

                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }
    }
    
    // Testing helper functions
    // This will check the turn on threshold and, for the 10-7618, confirm with the user 
    // that the unit is old hardware in on RMA and not new production. 
    // private void CheckDIOnThreshold(short stationID, int count, short testStationID,
    // short offset, short totalCount, short offVal, short onVal) throws
    // IOException, TimeoutException
    // stationID: The station with the AOs
    // count: The number of AOs to walk through (starts at 0)
    // testStationID: The station with the DIs
    // offset: The offset of the DI connected to the first AO (so we can make
    // sure they turn on)
    // totalCount: The total number of DIs on the station (so we can make sure
    // the rest stay off)
     private void CheckDIOnThreshold(short stationID, short count, short testStationID,
                         short offset, short totalCount)
        throws IOException, TimeoutException
    {
        // One-by-one, turn on the outputs from 0 to count on stationID
        // Each time, read count inputs from testStationID at offset to see if
        // they turn on/off as expected

        MultiIOException mioe = new MultiIOException();

        short[] aos = new short[count];

        // Start out by setting everything to 0 
        Arrays.fill(aos, (short)0); 
        udr.putA(stationID, (short)0, count, aos);
        Utils.sleep(250);

        // Make sure everything reads as 0
        boolean[] dis = udr.getD(testStationID, (short)0, totalCount);
        if (dis.length != totalCount) {
            throw new IOException(
                "Station did not respond to GetD message with the proper number of registers");
        }
        for (int i = 0; i < totalCount; i++) {
            if (dis[i]) {
                mioe.add(new IOException("After turning off all inputs, DI " +
                                         i + " (0-based) is on"));
            }
        }

        // Setting everything to 12300 should only turn on the old rev PCB DI
        // This level could let some rev A/B escape as revC but any higher value  
        // definitely will have some good revC units fail when they shouldn't.
        // (Rev A&B PCB input circuit = ~2.70V) designed turn on = 2.7
        // (Rev C PCB input circuit = ~2.91) designed turn on = 3.25
        Arrays.fill(aos, (short)12300); 
        udr.putA(stationID, (short)0, count, aos);
        Utils.sleep(250);

        dis = udr.getD(testStationID, (short)0, totalCount);
        boolean diON = false;
        for (int j = 0; j < dis.length; j++) {
            if (dis[j]) {
                diON = true;
            }
        }
        if (diON){
            // ask the user the build rev to determine if it is a fail or not
            ui.displayMessage(this, UserInterface.M_WARNING,
                "  Found isolated DIs on at the lower threshold. Operator confirmation required");

            if (ui.confirm("This module's isolated DI channels turned on with the lower input voltage." +
                    "\n\nTurn on thresholds lower than 2.9V are only permitted on revs" +
                    "\nlower than 1.20 and should only be found on units returned for repair." +
                    "\nThis unit should not be used for new product shipment." +
                    "\n\nIs this module hardware revision 1.20 or newer (higher#)?",
                    "Hardware Revision Confirmation")) {
                ui.displayMessage(this, UserInterface.M_NORMAL,
                "  Operator confirmed rev is 1.20 or newer.");
                for (int k = 0; k < dis.length; k++) {
                    if (dis[k]) {
                        mioe.add(new IOException("DI" + (k) +
                                                 " (0-based)is ON at the lower input voltage of 2.9v" +
                                                 " which is not allowed on rev 1.20 & newer"));
                    }  
                }
            }
            else {
                ui.displayMessage(this, UserInterface.M_NORMAL,
                "  Operator confirmed rev is before 1.20.");
                ui.displayMessage(this, UserInterface.M_NORMAL,
                "  Lower threshold is acceptable on revs below 1.20.");
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }
    }

    private void Offset_walkAOs(short stationID, short count, short testStationID,
                            short offset, short totalCount, short offVal,
                            short onVal)
        throws IOException, TimeoutException
    {
        // One-by-one, turn on the outputs from 0 to count on stationID
        // Each time, read count inputs from testStationID at offset to see if
        // they turn on/off as expected

        MultiIOException mioe = new MultiIOException();

        short[] aos = new short[count];

        // Start out by setting everything to 0
        Arrays.fill(aos, offVal);
        udr.putA(stationID, offset, count, aos);
        Utils.sleep(250);

        // Make sure everything reads as 0
        boolean[] dis = udr.getD(testStationID, (short)0, totalCount);
        if (dis.length != totalCount) {
            throw new IOException(
                "Station did not respond to GetD message with the proper number of registers");
        }
        for (int i = 0; i < totalCount; i++) {
            if (dis[i]) {
                mioe.add(new IOException("After turning off all inputs, DI " +
                                         i + " (0-based) is on"));
            }
        }

        // Walk through, setting each to just below the guaranteed ON and
        // backing down the previous to just above the guaranteed OFF
        for (short i = 0; i <= count; i++) {
            // Set the previous output to the off value
            if (i > 0) {
                aos[i - 1] = offVal;
            }

            // Set the current output to the on value
            if (i < aos.length) {
                aos[i] = onVal;
            }

            udr.putA(stationID, offset, count, aos);

            Utils.sleep(250);

            dis = udr.getD(testStationID, (short)0, totalCount);

            for (int j = 0; j < dis.length; j++) {
                if (offset + i == j && !dis[j]) {
                    if (i < aos.length) {
                        mioe
                            .add(new IOException("After turning on DI " +
                                                 (offset + i) +
                                                 " (0-based), station reports it is off"));
                    }
                    else {
                        // We didn't turn anything on for this run, just turned
                        // the last one off
                    }
                }
                else if (offset + i != j && dis[j]) {
                    if (i < aos.length) {
                        mioe.add(new IOException(
                            "After turning on DI " + (offset + i) +
                                " (0-based), station reports DI " + j +
                                " is also on"));
                    }
                    else {
                        mioe.add(new IOException(
                            "After turning off all DIs, station reports DI " +
                                j + " (0-based) is on"));
                    }
                }
                else if (offset + i == j && !dis[j]) {

                }
            }
        }

        if (!mioe.isEmpty()) {
            throw mioe;
        }
    }

    // private boolean chosen(String testName)
    private boolean chosen(String testName)
    {
        return (chosenTest == null || chosenTest.equals(testName));
    }

    // private String formatRangeMessage(String type, int channel, short
    // expected, short found)
    private String formatRangeMessage(String type, int channel, short found,
                                      short expected, int tolerance)
    {
        String op = TestLib.CH_MEMBEROF;
        if (Math.abs(expected - found) > tolerance) {
            op = TestLib.CH_NOTMEMBEROF;
        }
    
        return String.format("%s Channel %,d: %s%,d [%,d %s (%,d +/- %,d)]",
            type, channel, TestLib.CH_DELTA, found - expected, found, op,
            expected, tolerance);
    }
}

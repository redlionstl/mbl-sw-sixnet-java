/*
 * Mod32DI.java
 *
 * Tests a 32DI24 module.
 * Note: This shares a test fixture with the 32DO24 tests (Mod32DO.java)
 *
 * Jonathan Pearson
 * May 9, 2008
 *
 */

package com.sixnetio.Initializer.ModTest;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.Initializer.Tester;
import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.util.*;

public class Mod32DI implements Tester {
	// Constants
	// Locations of the configuration files
	// Test base files
	private static final String DIR_TESTBASE = "TestData" + File.separator + "32DI" + File.separator + "Base1"; // DI module
	private static final String DIR_HIGHISO = "TestData" + File.separator + "32DI" + File.separator + "HighIso"; // DI module
	private static final String DIR_WATCHDOG_OFF = "TestData" + File.separator + "32DI" + File.separator + "WatchdogOff"; // Make sure there is a watchdog failure (DI module)
	
	// Test runner files
	private static final String DIR_TESTRUNNER = "TestData" + File.separator + "32DI" + File.separator + "Base2"; // DO module
	
	private static final String T_DIAG = "Diagnostics Test",
	                            T_DI = "DI Test",
	                            T_ISO = "Isolated Counter Test",
	                            T_HIGHSPEED = "High Speed Counter Test",
	                            T_DISOURCE = "DI Sourcing Test",
	                            T_POE = "PoE Test",
	                            T_WATCHDOG = "Watchdog Test";
	
	private static final String[] TEST_NAMES = {
		// Base 1
		T_DIAG,
		T_DI,
		T_ISO,
		T_HIGHSPEED,
		T_DISOURCE,
		T_POE,
		T_WATCHDOG
	};
	
	// Try to keep every test module using a different set of IP addresses, just in case we figure out
	//   a way to run multiple modules from the same machine at the same time
	private static final String IP_TESTBASEA = "10.1.1.1";
	private static final String IP_TESTBASEB = "10.1.2.1";
	private static final String IP_TESTRUNNER = "10.1.4.1"; // B interface
	
	private static final short S_TESTBASE = 1; // 32 DI module being tested
	private static final short S_TESTRUNNER = 2; // 32 DO module running the test
	private static final short S_OUTPUTS = 3; // 8AO in charge of DO output, relays, and the isolated counter
	private static final short S_DIAGS = 4; // 8AI watching the POE output
	
	// Which AI the watchdog output terminal is plugged into
	private static final short AI_WATCHDOG = 2; // (0-based)
	
	// Which AI the POE output terminal is plugged into
	private static final short AI_POE = 1; // (0-based)
	
	// The AO wired to the isolated input, and the AI to read for the count
	private static final short AO_ISO = 2; // 0-based
	private static final short AI_ISO = 0;
	
	// The AI to read for cnt1
	private static final short AI_HIGHSPEED = 1;
	
	// Which AO controls the power output of the DOs of Base2
	private static final short AO_BASE2 = 0; // 0-based
	
	// Which AO controls the relay switching
	private static final short AO_SWITCH = 1; // 0-based
	
	// Diagnostic DI offsets from DI_DiagStart
	private static final short DI_DIAG_START = 32; // The first diagnostic DI
	private static final short DIAG_SELFTEST = 0, // Module Selftest
	                           DIAG_POWER1 = 1, // Power 1
	                           DIAG_POWER2 = 2, // Power 2
	                           DIAG_IOSTATUS = 3, // I/O Status
	                           DIAG_ETH1LINK = 4, // Ethernet 1 has link
	                           DIAG_ETH2LINK = 5, // Ethernet 2 has link
	                           DIAG_RING = 6, // Ring is complete
	                           DIAG_POE = 7; // PoE is available
	
	// Analog values (to avoid hardcoding them in multiple places)
	private static final short EXP_WATCHDOG = (short)3686; // Expected watchdog input value; 3686 = 5.8mA = 23.2V across 4000 Ohms
	private static final short EXP_POE = (short)4096; // Expected PoE input value; 4096 = 6mA = .6V across a 100 Ohm resistor
	private static final short AO_5_5V = (short)2850; // To have 5.5V on the DO channels
	private static final short AO_8_5V = (short)8265; // To have 8.5V on the DO channels
	private static final short AO_MIN_ISO = (short)0; // To output 0V from an AO module to an isolated input channel
	private static final short AO_2V_ISO = (short)4400; // To output 2V from an AO module to an isolated input channel
	private static final short AO_3V_ISO = (short)18000; // To output 3V from an AO module to an isolated input channel
	private static final short AO_MAX_ISO = (short)32767; // To output ~5-6V from an AO module to an isolated input channel
	private static final short SWITCH_OFF = (short)0; // To turn off the relay switch
	private static final short SWITCH_ON = (short)10000; // To turn on the relay switch
	
	// Used to check that the Watchdog input is close to what it should be
	private static final int TOLERANCE_WATCHDOG = 450;
	
	// Used to check that the PoE input is close to what it should be
	private static final int TOLERANCE_POE = 600;
	
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private UDRLink activeHandler;
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	
	// Constructor
	public Mod32DI() { }
	
	// Required by Tester
	// public void setupTest(UserInterface ui, String comPort, String devName) throws IOException
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for Mod32DI must not be null");
		
		// Make sure the device name is recognized
		// Must start with E2 or EB
		// Must end with 32DI24
		if (!((devName.startsWith("E2") || devName.startsWith("EB")) &&
		      (devName.endsWith("32DI24")))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)101, (byte)0);
		testLib = new TestLib(udr, ui);
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
	}
	
	// public boolean supportsTestJump()
	public boolean supportsTestJump() {
		return true;
	}
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
	// public List<String> getTests()
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
	}
	
	// public void jumpToTest(String testName)
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
	}
	
	// public String getJumpTest()
	public String getJumpTest() {
		return chosenTest;
	}
	
	// public void run()
	public void run() {
		// Set up station 2 so it listens on RS485
		// We can't have two master stations on the same RS485 line, this way one will be a slave
		try {
			setupStation2();
		} catch (Exception e) {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unable to load the DO module with its configuration");
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		}
		
		MultiIOException mioe = new MultiIOException();
		
		// Connect
		try {
			// Use ethernet port 2 for about half of the tests, then switch to port 1
			// Start with port 2 because it should always be plugged in, so we are more likely
			//   to get a connection and then have the diagnostics test fail with a descriptive
			//   message if ethernet 1 is not plugged in
			activeHandler = testLib.registerUDPHandler(IP_TESTBASEB);
			
			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
			udr.waitForResponse(S_TESTBASE);
			
			// Make sure the connection works
			testLib.loadBaseFiles(S_TESTBASE, DIR_TESTBASE);
		} catch (Exception e) {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unable to connect through the second ethernet port");
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		}
		
		try { // Finally block unregisters activeHandler
			// Check the diagnostic DIs
			try {
				if (chosen(T_DIAG)) checkDiags();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			// checkDiags() may also throw a plain old Exception if the user
			//   cancels, so no more tests will run
			
			// Walk the DOs of the other module across the DIs of this one
			try {
				if (chosen(T_DI)) walkDIs();
			} catch (IOException ioe) {
				// This doesn't print its own error messages
				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
				mioe.add(ioe);
			}
			
			// Toggle the isolated input a few times to test the counter
			try {
				if (chosen(T_ISO)) checkIsolated();
			} catch (IOException ioe) {
				// This one doesn't print its own error messages
				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
				mioe.add(ioe);
			}
			
			// Toggle DI1 (0-based) a few times to test the counter
			try {
				if (chosen(T_HIGHSPEED)) checkHighSpeed();
			} catch (IOException ioe) {
				// This one doesn't print its own error messages
				ui.displayMessage(this, UserInterface.M_ERROR, ioe.getMessage());
				mioe.add(ioe);
			}
		} catch (Exception e) {
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		} finally {
			activeHandler = testLib.unregisterHandler(activeHandler);
		}
		
		// Reconnect
		// This is about halfway through, switch to the other ethernet port to continue
		try {
			// Switch to port 1
			activeHandler = testLib.registerUDPHandler(IP_TESTBASEA);
			
			// Make sure the connection works
			testLib.verifyConnectivity(S_TESTBASE);
		} catch (Exception e) {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unable to connect through the first ethernet port");
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		}
		
		try { // Finally block unregisters activeHandler
			// Switch to sourcing DI mode and see if the DIs all turn on when tied to ground
			try {
				if (chosen(T_DISOURCE)) testSourcing();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			
			// Check the PoE output pin to make sure it's doing what we expect
			try {
				if (chosen(T_POE)) checkPOE();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			
			// Check the Watchdog output pin to make sure it's doing what we expect
			try {
				if (chosen(T_WATCHDOG)) checkWatchdog();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
		} catch (Exception e) {
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		} finally {
			activeHandler = testLib.unregisterHandler(activeHandler);
		}
		
		if (!mioe.isEmpty()) {
			ui.handleException(this, mioe);
			ui.operationFailed(this);
		} else {
	        if (chosenTest == null) {
	            // null is all tests
	            // write the pass flag
	            if (writePassedFlag(true)){
	                ui.displayMessage(this, UserInterface.M_NORMAL,
	                "Module tests passed");
	            }
	            else{
	                ui.handleException(this, new IOException("Failed to write the All Tests Passed flag to module."));
	                ui.operationFailed(this);
	                return;
	            }
	        }
	        else {
	            ui.displayMessage(this, UserInterface.M_NORMAL, "Module test '" +
	                                                            chosenTest +
	                                                            "' passed");
	        }
			ui.operationCompleted(this);
		}
	}
	
    private boolean writePassedFlag(boolean pass)
    {
        boolean ret = false;
        byte[] flagbyte = new byte[1]; 
        flagbyte[0] = 0; // must be 0 if failed
        if(pass) flagbyte[0] = 1; // must be 1 if pass
        try {
            activeHandler = testLib.registerUDPHandler(IP_TESTBASEA);
            udr.writeFile(S_TESTBASE, "/module0/arc/factory", flagbyte, 0x4B, null);
            // Disconnect the handler
            activeHandler = testLib.unregisterHandler(activeHandler);
            ret = true;
        }
        catch (IOException e) {
            ret = false;
        }
        catch (TimeoutException e) {
            ret = false;
        }
    
        return ret;
    }

	// Testing functions
	// private void checkDiags() throws Exception, IOException, TimeoutException
	private void checkDiags() throws Exception, IOException, TimeoutException {
		final short stationID = S_TESTBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		setSwitch(false); // Power input 1
		
		boolean[] dis = udr.getD(stationID, DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected on)
		if (dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 off");
			mioe.add(new IOException("Module reports no power on power 1"));
		}
		
		// Power 2 (expected off)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 on");
			mioe.add(new IOException("Module reports power on power 2, the custom board may have a short"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 off");
		}
		
		// Ethernet 1 (expected on)
		if (dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 1. Please verify the PoE ethernet cable is plugged into the DI side of the fixture. Continue the test?", "Ethernet 1 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");

				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 1 link");
			} else {
				dis = udr.getD(stationID, DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH1LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 1"));
				}
			}
		}
		
		// Ethernet 2 (expected on)
		if (dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 2. Please verify the ethernet cable is secure on both the DI side and the switch. Continue the test?", "Ethernet 2 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
				throw new Exception("Module has no ethernet 2 link");
			} else {
				dis = udr.getD(stationID, DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH2LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 2"));
				}
			}
		}
		
		// POE (expected on if EB, off if E2)
		if (devName.startsWith("EB") && !dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  No POE");
			mioe.add(new IOException("Module reports no POE, although it is a POE module and ethernet 1 has a link"));
		} else if (devName.startsWith("E2") && dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE on non-EB module");
			mioe.add(new IOException("Module reports POE is available, although it is not an EB module"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		}
		
		setSwitch(true); // Switch to power input 2, among other things
		
		dis = udr.getD(stationID, DI_DIAG_START, (short)8);
		
		// Power 1 (expected off)
		if (dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 on");
			mioe.add(new IOException("Module reports power on power 1, the custom board may have a short"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 off");
		}
		
		// Power 2 (expected on)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 off");
			mioe.add(new IOException("Module reports no power on power 2"));
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks complete");
	}
	
	// private void walkDIs() throws IOException, TimeoutException
	private void walkDIs() throws IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing DIs");
		
		setSwitch(false); // Make sure the module is in DC- mode
		
		// Note: This is done in an off loop and then an on loop so the AO doesn't
		//   need to settle twice for each DI
		
		// Prepare for walking the outputs at low voltage
		boolean[] dos = new boolean[32]; // NOTE: This is the only place where the count is specified
		Arrays.fill(dos, false);
		udr.putD(S_TESTRUNNER, (short)0, (short)dos.length, dos);
		
		// Lower the output of the AO so when we turn on a DO it won't actually output enough to turn on a DI yet
		short[] aos = new short[1];
		Arrays.fill(aos, AO_5_5V);
		udr.putA(S_OUTPUTS, AO_BASE2, (short)aos.length, aos);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing guaranteed OFF point");
		
		// Give the AO time to settle
		Utils.sleep(500);
		
		// Walk the outputs at low voltage (everything should remain off)
		// There are 32 DIs, with input coming from DOs on Base2
		for (int i = 0; i < dos.length; i++) {
			// Turn off the previous DO and turn on the current one
			if (i > 0) dos[i - 1] = false;
			dos[i] = true;
			udr.putD(S_TESTRUNNER, (short)0, (short)dos.length, dos);
			
			// Sleep for a moment to let it stabilize
			Utils.sleep(200);
			
			// Read the DIs to make sure they are all off
			boolean[] dis = udr.getD(S_TESTBASE, (short)0, (short)dos.length);
			for (int j = 0; j < dis.length; j++) {
				if (dis[j]) {
					mioe.add(new IOException("DI " + j + " (0-based) turned on at 5.5V"));
				}
			}
		}
		
		// Prepare for walking again at high voltage
		// Turn off the last DO to avoid confusing the user
		dos[dos.length - 1] = false;
		udr.putD(S_TESTRUNNER, (short)0, (short)dos.length, dos);
		
		// Turn up the AO and walk again
		aos[0] = AO_8_5V;
		udr.putA(S_OUTPUTS, AO_BASE2, (short)aos.length, aos);
		
		// Let the user know he should watch this time
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing guaranteed ON point. You should see " + dos.length + " walking LEDs");
		Utils.sleep(2500); // Give the user time to look at the fixture
		
		// Walk the outputs at high voltage
		// Loop until the user watches the LEDs
		boolean done = false;
		while (!done) {
			// We do a <= here to make sure the last input turns back off
			for (int i = 0; i <= dos.length; i++) {
				// Turn off the previous output and turn on the current one
				if (i > 0) dos[i - 1] = false;
				if (i < dos.length) dos[i] = true;
				
				udr.putD(S_TESTRUNNER, (short)0, (short)dos.length, dos);
				
				// Wait a moment for it to turn on
				Utils.sleep(200);
				
				// Read the inputs to make sure they look right
				boolean[] dis = udr.getD(S_TESTBASE, (short)0, (short)dos.length);
				for (int j = 0; j < dis.length; j++) {
					if (dis[j]) {
						// It's on... should it be?
						if (i != j) {
							// Make sure the message isn't confusing on the last loop (32)
							if (i == dos.length) {
								mioe.add(new IOException("After turning off all outputs, DI " + j + " (0-based) is on"));
							} else {
								mioe.add(new IOException("After turning on DO " + i + " (0-based), DI " + j + " is on"));
							}
						}
					} else {
						// It's off... should it be?
						if (i == j) {
							// No need to worry about the last loop, since everything should be off
							mioe.add(new IOException("After turning on DO " + i + " (0-based), DI " + j + " is off"));
						}
					}
				}
			}
			
			// Make sure the user watched
			if (ui.confirm("Did you watch for walking DI LEDs (there were " + dos.length + ")?", "Walking LEDs")) {
				if (!ui.confirm("Did they all light?", "Walking LEDs")) mioe.add(new IOException("Missing DI LEDs during walk"));
				done = true;
			} else {
				Utils.sleep(2000); // Give the user a chance to look back over there
				done = false;
			}
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  DI tests complete");
	}
	
	// private void checkIsolated() throws IOException, TimeoutException
	private void checkIsolated() throws IOException, TimeoutException {
		// Turn off the outputs feeding the counters
		short[] aos = new short[1];
		short[] cnt_start = new short[1];
		short[] cnt_end = new short[1];
		Arrays.fill(aos, AO_MIN_ISO);
		udr.putA(S_OUTPUTS, AO_ISO, (short)aos.length, aos);
		
		// We could sleep for a moment, but we're about to reset the station, so don't bother
		
		// DC+/- mode does not matter here
		
		// Turn on counters
		if (!testLib.loadBaseFiles(S_TESTBASE, DIR_HIGHISO)) {
			// Make sure the counters are zeroed
			testLib.resetStation(S_TESTBASE);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing isolated counter register");
	
		// Get a reading of the counters to start - fixture causes a count upon startup
		cnt_start = udr.getA(S_TESTBASE, AI_ISO, (short)1);

		// Toggle the counter a few times
		final int counter0 = 3; // Number of times to toggle it
		
		int i = 0;
		while (i < counter0) {
			// Turn it on - fixture outputs ~2V
			aos[0] = AO_MAX_ISO;
			udr.putA(S_OUTPUTS, AO_ISO, (short)aos.length, aos);
			
			// And wait another moment...
			Utils.sleep(1000);

			// Turn it off - fixture outputs ~3V
			// counts on this edge
			aos[0] = AO_MIN_ISO;
			udr.putA(S_OUTPUTS, AO_ISO, (short)aos.length, aos);
			
			// Wait a moment for it to register
			Utils.sleep(1000);
			
			
			// Increment i and loop around
			i++;
		}
		
		// Grab the analog input that holds the count and test
		cnt_end = udr.getA(S_TESTBASE, AI_ISO, (short)1);
		
		// Check the count
		if ((cnt_end[0] - cnt_start[0]) != counter0) throw new IOException(String.format("Expected cnt0 = %d, but found cnt0 = %d", counter0, (cnt_end[0] - cnt_start[0])));
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Isolated counter test complete");
	}
	
	// private void checkHighSpeed() throws IOException, TimeoutException
	private void checkHighSpeed() throws IOException, TimeoutException {
		// Turn off the outputs feeding the counters
		boolean[] dos = new boolean[1];
		Arrays.fill(dos, false);
		udr.putD(S_TESTRUNNER, (short)1, (short)dos.length, dos);
		
		// But make sure we can turn them on...
		short[] aos = new short[1];
		Arrays.fill(aos, AO_8_5V);
		udr.putA(S_OUTPUTS, AO_BASE2, (short)aos.length, aos);
		
		// We could sleep for a moment, but we're about to reset the station, so don't bother
		
		// Must be in DC- mode
		setSwitch(false);
		
		// Turn on counters
		if (!testLib.loadBaseFiles(S_TESTBASE, DIR_HIGHISO)) {
			// Make sure the counters are zeroed
			testLib.resetStation(S_TESTBASE);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing high-speed counter register");
		
		// Toggle the counter a few times
		final int counter1 = 7; // Number of times to toggle it
		
		int i = 0;
		while (i < counter1) {
			// Turn it off
			Arrays.fill(dos, false);
			udr.putD(S_TESTRUNNER, (short)1, (short)dos.length, dos);
			
			// This is a high-speed counter, so we don't need to go too slowly
			Utils.sleep(50);
			
			// Turn it on
			if (i < counter1) dos[0] = true;
			udr.putD(S_TESTRUNNER, (short)1, (short)dos.length, dos);
			
			// And wait another moment...
			Utils.sleep(50);
			
			// Increment i and loop around
			i++;
		}
		
		// Grab the analog input that holds the count and test
		short[] ais = udr.getA(S_TESTBASE, AI_HIGHSPEED, (short)1);
		
		// Check the count
		if (ais[0] != counter1) throw new IOException(String.format("Expected cnt1 = %d, but found cnt1 = %d", counter1, ais[0]));
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  High-speed counter test complete");
	}
	
	// private void testSourcing() throws IOException, TimeoutException
	private void testSourcing() throws IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing inputs in sourcing mode");
		
		setSwitch(true); // Make sure the module is in DC+ mode
		
		// The first group of inputs on the 32 is sourced
		// The caller of this function is expected to skip this test for the 16 and to make sure the switch has been flipped to sourcing
		// Reset the station
		testLib.resetStation(S_TESTBASE);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing DIs in sourcing mode");
		
		// Get the first 8 DIs, make sure they're all on
		boolean[] dis = udr.getD(S_TESTBASE, (short)0, (short)8);
		for (int i = 0; i < dis.length; i++) {
			if (!dis[i]) {
				mioe.add(new IOException("DI " + i + " (0-based) should be on in sourcing mode, but it is off"));
			}
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  DI sourcing test complete");
	}
	
	// private void checkPOE() throws IOException, TimeoutException
	private void checkPOE() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the PoE output");
		
		// DC+/- mode doesn't matter here
		
		// If it's an EB base, check AI_POE for a value, otherwise expect something small (-8192, probably)
		short[] vals = udr.getA(S_DIAGS, AI_POE, (short)1);
		
		if (devName.startsWith("EB")) {
			if (Math.abs(EXP_POE - vals[0]) > TOLERANCE_POE) {
				throw new IOException(String.format("AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_NOTMEMBEROF, EXP_POE, TOLERANCE_POE));
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_MEMBEROF, EXP_POE, TOLERANCE_POE));
			}
		} else {
			if (vals[0] > 0) {
				throw new IOException("It appears that there is a PoE board on this module, but it has been initialized as an E2.");
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  As expected, there is no PoE board on this module.");
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  PoE test complete");
	}
	
	// private void checkWatchdog() throws IOException, TimeoutException
	private void checkWatchdog() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the Watchdog output");
		
		setSwitch(true); // Need power 1 to be off
		
		// Give it a couple seconds in case it was rebooted recently
		Utils.sleep(10000);
		
		short[] vals = udr.getA(S_DIAGS, AI_WATCHDOG, (short)1);
		
		if (Math.abs(EXP_WATCHDOG - vals[0]) > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output): is off when it should be on", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is on as expected", AI_WATCHDOG));
		}
		
		// Make sure the watchdog output is off
		testLib.loadBaseFiles(S_TESTBASE, DIR_WATCHDOG_OFF);
		
		// Give it a couple extra seconds
		Utils.sleep(10000);
		
		vals = udr.getA(S_DIAGS, AI_WATCHDOG, (short)1);
		
		if (vals[0] > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output) is on when it should be off", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is off as expected", AI_WATCHDOG));
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Watchdog test complete");
	}
	
	// Testing helper functions
	// private void setupStation2() throws IOException, TimeoutException
	private void setupStation2() throws IOException, TimeoutException {
		UDRLink station2Handler = testLib.registerTCPHandler(IP_TESTRUNNER);
		
		try {
			testLib.loadBaseFiles(S_TESTRUNNER, DIR_TESTRUNNER);
		} finally {
			testLib.unregisterHandler(station2Handler);
		}
	}
	
	// private boolean chosen(String testName)
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
	
	// private void setSwitch(boolean on) throws IOException, TimeoutException
	private void setSwitch(boolean on) throws IOException, TimeoutException {
		short val = SWITCH_OFF;
		if (on) val = SWITCH_ON;
		
		// Only bother throwing the switch when it's necessary
		short[] currentVal = udr.getA(S_OUTPUTS, UDRMessage.T_D_AOUT, AO_SWITCH, (short)1);
		if (currentVal[0] != val) {
			ui.displayMessage(this, UserInterface.M_WARNING, "  Switching relays...");
			short[] switchVal = {val};
			udr.putA(S_OUTPUTS, AO_SWITCH, (short)switchVal.length, switchVal);
			
			Utils.sleep(200); // Give the relays a chance to flip
			
			// Reset station 1, since its sourcing/sinking mode just got flipped
			ui.displayMessage(this, UserInterface.M_WARNING, "    Resetting station 1");
			testLib.resetStation(S_TESTBASE);
		}
	}
}

/*
 * Mod10RTD.java
 *
 * Tests a 10RTD module.
 *
 * Jeff Collins
 * February 3, 2009
 *
 */

package com.sixnetio.Initializer.ModTest;

import com.sixnetio.Initializer.*;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class Mod10RTD implements Tester {
	// Constants
	// Locations of the configuration files
	// These get loaded into the calibration base
	private static final String DIR_CALBASE = "TestData" + File.separator + "10RTD" + File.separator + "Base1";
	private static final String DIR_WATCHDOG_OFF = "TestData" + File.separator + "10RTD" + File.separator + "WatchdogOff";
	private static final String DIR_CALIBRATE_RTDS = "TestData" + File.separator + "10RTD" + File.separator + "CalibrateRTDs";
	
	// These get loaded into the verification base
	private static final String DIR_TESTBASE = "TestData" + File.separator + "10RTD" + File.separator + "Base2";
	
	//private static final String DIR_MODBASE = "TestData" + File.separator + "10RTD" + File.separator + "Base3";
	
	private static final String T_DIAG1 = "Diagnostics Test (Base 1)",
	                            T_CALRTD = "RTD Calibration",
	                            T_RTD_CAL_CHECK = "RTD Calibration Check",
	                            T_POE = "PoE Test",
	                            T_WATCHDOG = "Watchdog Test",
	                            T_DIAG2 = "Diagnostics Test (Base 2)",
	                            T_LEAD_RESISTANCE = "Lead Resistance Compensation Check";
	                            
	
	private static final String[] TEST_NAMES = {
		// Base 1
		T_DIAG1,
		T_CALRTD,
		T_RTD_CAL_CHECK,
		T_POE,
		T_WATCHDOG,
		
		// Base 2
		T_DIAG2,
		T_LEAD_RESISTANCE
	};
	
	// Try to keep every test module using a different set of IP addresses, just in case we figure out
	//   a way to run multiple fixtures from the same machine at the same time
	private static final String IP_CALBASE = "10.1.12.1";
	private static final String IP_TESTBASE = "10.1.12.2";
	
	private static final short S_CALBASE = 1; // Calibration
	private static final short S_TESTBASE = 2; // Verification
	private static final short S_DIAGS = 3; // 16AI in charge of reading POE and Watchdog outputs
	
	
	// Which AI the watchdog output terminal is plugged into
	private static final short AI_WATCHDOG = 1; // (0-based)
	
	// Which AI the POE output terminal is plugged into
	private static final short AI_POE = 0; // 0-based
	
	// Diagnostic DI offsets from DI_DiagStart
	private static final short DI_DIAG_START = 32; // The first diagnostic DI
	private static final short DIAG_SELFTEST = 0, // Module Selftest
	                           DIAG_POWER1 = 1, // Power 1
	                           DIAG_POWER2 = 2, // Power 2
	                           DIAG_IOSTATUS = 3, // I/O Status
	                           DIAG_ETH1LINK = 4, // Ethernet 1 has link
	                           DIAG_ETH2LINK = 5, // Ethernet 2 has link
	                           DIAG_RING = 6, // Ring is complete
	                           DIAG_POE = 7; // PoE is available
	
	// Analog values (to avoid hard-coding them in multiple places)
	private static final short EXP_WATCHDOG = (short)3686; // Expected watchdog input value; 3686 = 5.8mA = 23.2V across 4000 Ohms
	private static final short EXP_POE = (short)4096; // Expected PoE input value; 4096 = 6mA = .6V across a 100 Ohm resistor
	private static final short RAW_VALUE = (short)6553; // Raw linear value for 100 ohms
	
	// Arrays of expected alpha .00385 European value readings at each RTD channel on Base1 and Base2
	private static final short [] BASE1_EXPVALS_C = {-1512,-2,1313,4046,7952,-1512,-2,1317,4047,7950};
	private static final short [] BASE2_EXPVALS_C = {7291,7300,7304,7295,7316,7276,7296,7273,7285,7304};
	private static final short [] BASE1_EXPVALS_B = {-1543, -51, 1284, 4002, 8023, -1540, -51, 1288, 3993, 7876};
	private static final short [] BASE2_EXPVALS_B = {7288, 7217, 7208, 7206, 7228, 7229, 7288, 7263, 7334, 7232};
	private static final short [] BASE1_EXPVALS_A = {-1547, -36, 1342, 4011, 8019, -1534, -32, 1349, 4016, 7957};
	private static final short [] BASE2_EXPVALS_A = {7327, 7328, 7339, 7335, 7270, 7328, 7354, 7331, 7377, 7349};
	private static short [] BASE1_EXPVALS = new short[10];
	private static short [] BASE2_EXPVALS = new short[10];

	// Register numbers  for RTD channels AX4 and AX9 used to calibrate 2 100 ohm reference resistors
	private static final short [] CAL_CHANNEL = {4,9};
	//Actual resistance values of calChannels AX4 and AX9
	private static final double [] CAL_POINT_C = {374.085, 374.015};
	private static final double [] CAL_POINT_B = {376.201, 371.803};
	private static final double [] CAL_POINT_A = {376.066, 374.237};
	private static double [] CAL_POINT = new double[2];
	
	// Tolerance for analog values
	// If two analog values should be equal, values within this distance on either side will be accepted
	private static final int TOLERANCE_EXP_VAL = 100;
	private static final int TOLERANCE_RTD = 5;
	private static final int TOLERANCE_LEAD_RES = 20;
	
	// Similar to RTDTolerance, this is used to make sure consecutive scans during an average are close enough
	private static final int TOLERANCE_AVERAGE = 10;
	
	// Used to check that the Watchdog input is close to what it should be
	private static final int TOLERANCE_WATCHDOG = 450;
	
    // Used to determine which fixture is connected
    private static final int TOLERANCE_FIXTURE = 1500;

    // Used to check that the PoE input is close to what it should be
	private static final int TOLERANCE_POE = 600;
	
	// Base directory in a module where we will find the configuration files
	// We expect a slash on the end of this
	private static final String DIR_MODULECONFIG = "/module0/arc/";
	
	// Offset/length of the RTD default span calibration section of the module cals file 
	private static final int OFFSET_CAL_START_ADDR = 0x60;
	private static final int OFFSET_DEFAULT_SPAN1 = 0x62;
	private static final int LENGTH_DEFAULT_SPAN1 = 0x02;
	private static final int OFFSET_DEFAULT_SPAN2 = 0x66;
	private static final int LENGTH_DEFAULT_SPAN2 = 0x02;
	
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private UDRLink activeHandler;
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	
	public Mod10RTD() { }
	
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for Mod10RTD must not be null");
		
		// Make sure the device name is recognized
		// Must start with E2 or EB
		// Must end with 10RTD
		if (!((devName.startsWith("E2") || devName.startsWith("EB")) &&
		      devName.endsWith("10RTD"))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)110, (byte)0);
		testLib = new TestLib(udr, ui);
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
	}
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
	}
	
	public boolean supportsTestJump() {
		return true;
	}
	
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
	}
	
	public String getJumpTest() {
		return chosenTest;
	}
	
	public void run() {
		MultiIOException mioe = new MultiIOException();
		
		boolean fixtureDetermined = false; // set true if valid test fixture is found
		try{
		activeHandler = testLib.registerUDPHandler(IP_CALBASE);
		fixtureDetermined = determineFixture();
		} catch (Exception e) {
			ui.handleException(this, e);
			ui.operationFailed(this);
		} finally {
			testLib.unregisterHandler(activeHandler);
		}
		if (fixtureDetermined) {
			if (chosen(T_DIAG1) ||
				chosen(T_CALRTD) ||
				chosen(T_RTD_CAL_CHECK) ||
				chosen(T_POE) ||
				chosen(T_WATCHDOG)) {
				
				if (!ui.confirm("Please plug the module into the first base", "Module Check")) {
					ui.displayMessage(this, UserInterface.M_ERROR, "User canceled the test");
					ui.operationFailed(this);
					return;
				}
				
	    		// Register a UDP handler for base 1
	    		try {
	    			activeHandler = testLib.registerUDPHandler(IP_CALBASE);
	    			
	    			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
	    			udr.waitForResponse(S_CALBASE);
	    			
	    			// Make sure it works
	    			testLib.loadBaseFiles(S_CALBASE, DIR_CALBASE);
	    		} catch (Exception e) {
	    			ui.handleException(this, e);
	    			ui.operationFailed(this);
	    			return;
	    		}
	    		
	    		try { // Finally block unregisters the handler
	    			// Now start doing tests
	    			// Diagnostics test 1
	    			try {
	    				if (chosen(T_DIAG1)) checkDiags1();
	    			} catch (IOException ioe) {
	    				mioe.add(ioe);
	    			}
	    			
	    			// RTD Calibration/test
	    			try {
	    				if (chosen(T_CALRTD)&&(!runVerifyTest)) calibrateRTDs();
	    				
	    				// This really does belong here
	    				// This makes sure that the RTDs work properly with the calibration that we just made
	    				// There is a 'leadResistance' compensation check on the other base that verifies each  
	    				// channel can compensate for a 100 ohm lead resistance.
	    				if (chosen(T_RTD_CAL_CHECK)) testRTDCalibration(); // If the calibration fails, don't do the test
	    			} catch (IOException ioe) {
	    				mioe.add(ioe);
	    			}
	    			
	    			// POE test
	    			try {
	    				if (chosen(T_POE)) checkPOE();
	    			} catch (IOException ioe) {
	    				mioe.add(ioe);
	    			}
	    			
	    			// Watchdog test
	    			try {
	    				if (chosen(T_WATCHDOG)) checkWatchdog();
	    			} catch (IOException ioe) {
	    				mioe.add(ioe);
	    			}
	    		} catch (Exception e) {
	    			ui.handleException(this, e);
	    			ui.operationFailed(this);
	    			return;
	    		} finally {
	    			testLib.unregisterHandler(activeHandler);
	    		}
			}
			
			if (chosen(T_DIAG2) ||
				chosen(T_LEAD_RESISTANCE)) {
			    
			    if (!fixtureDetermined) {
			        while (!ui.confirm("Please move the module to the first base", "Move the Module")) {
		                if (ui.confirm("Really cancel the test?", "Cancel")) {
		                    ui.displayMessage(this, UserInterface.M_ERROR, "User canceled the test");
		                    ui.operationFailed(this);
		                    return;
		                }
		            }
			        
			        determineFixtureHelper();
			        fixtureDetermined = true;
			    }
				
				while (!ui.confirm("Please move the module to the second base", "Move the Module")) {
					if (ui.confirm("Really cancel the test?", "Cancel")) {
						ui.displayMessage(this, UserInterface.M_ERROR, "User canceled the test");
						ui.operationFailed(this);
						return;
					}
				}
				
	    		// Register a UDP handler for station 2
	    		try {  
	    			activeHandler = testLib.registerUDPHandler(IP_TESTBASE);
	    			    			
	    			// Make sure it works
	    			testLib.loadBaseFiles(S_TESTBASE, DIR_TESTBASE);    			
	    		} catch (Exception e) {
	    			ui.handleException(this, e);
	    			ui.operationFailed(this);
	    			return;
	    		}
	    		
	    		try { // Finally block unregisters the handler
	    			// Diagnostics 2 test
	    			try {
	    				if (chosen(T_DIAG2)) checkDiags2();
	    			} catch (IOException ioe) {
	    				mioe.add(ioe);
	    			}
	    			
	    			// RTD Lead Resistance Compensation Test
	    			try {
	    				if (chosen(T_LEAD_RESISTANCE)) testLeadResistance();
	    			} catch (IOException ioe) {
	    				mioe.add(ioe);
	    			}
	    		} catch (Exception e) {
	    			ui.handleException(this, e);
	    			ui.operationFailed(this);
	    			return;
	    		} finally {
	    			testLib.unregisterHandler(activeHandler);
	    		}
			}
		}else mioe.add(new IOException("Failed to determine Test Fixture version."));

        if (chosenTest == null) {
            // null is all tests
            // write the pass flag
            if (writePassedFlag(true)){
                ui.displayMessage(this, UserInterface.M_NORMAL,
                "Module tests passed");
            }
            else{
                ui.handleException(this, new IOException("Failed to write the All Tests Passed flag to module."));
                ui.operationFailed(this);
                return;
            }
        }
        else {
            if (mioe.isEmpty()) {
                ui.displayMessage(this, UserInterface.M_NORMAL, "Module test '" + 
                                                                chosenTest + 
                                                                "' passed");
            }
            else {
                ui.displayMessage(this, UserInterface.M_ERROR, "Module test '" +
                                                               chosenTest +
                                                               "' failed");
            }
        }

		if (!mioe.isEmpty()) {
			ui.handleException(this, mioe);
			ui.operationFailed(this);
		} else {
			ui.operationCompleted(this);
		}
		
		fixtureDetermined = false;
	}
	
    private boolean writePassedFlag(boolean pass)
    //  throws IOException, TimeoutException
    {
        boolean ret = false;
        byte[] flagbyte = new byte[1]; 
        flagbyte[0] = 0; // must be 0 if failed
        if(pass) flagbyte[0] = 1; // must be 1 if pass
        try {
            activeHandler = testLib.registerUDPHandler(IP_TESTBASE);
            udr.writeFile(S_TESTBASE, "/module0/arc/factory", flagbyte, 0x4B, null);
          // Disconnect the handler
            activeHandler = testLib.unregisterHandler(activeHandler);
            ret = true;
        }
        catch (IOException e) {
            ret = false;
        }
        catch (TimeoutException e) {
            ret = false;
        }
        
        return ret;
    }

	private void checkDiags1() throws Exception, IOException, TimeoutException {
		final short stationID = S_CALBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		boolean[] dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected on)
		if (dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 off");
			mioe.add(new IOException("Module reports no power on power 1"));
		}
		
		// Power 2 (expected off)
		if (!dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 off");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 on");
			mioe.add(new IOException("Module reports power on power 2"));
		}
		
		// Ethernet 1 (expected on)
		if (dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 1. Please verify the ethernet cable from the power injector to Ethernet Port 1 is secure. Continue the test?", "Ethernet 1 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 1 link");
			} else {
				dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH1LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 1"));
				}
			}
		}
		
		// Ethernet 2 (expected off)
		if (!dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 not linked");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 linked");
			throw new Exception("Module has a link on ethernet 2");
		}
		
		// POE (expected on if EB, off if E2)
		if (devName.startsWith("EB") && !dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  No POE");
			mioe.add(new IOException("Module reports no POE, although it is a POE module and ethernet 1 has a link"));
		} else if (devName.startsWith("E2") && dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE on non-EB module");
			mioe.add(new IOException("Module reports POE is available, although it is not an EB module"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks for Base 1 complete");
	}
	
	private void checkDiags2() throws Exception, IOException, TimeoutException {
		final short stationID = S_TESTBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		boolean[] dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected off)
		if (!dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 off");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 on");
			mioe.add(new IOException("Module reports power on power 1"));
		}
		
		// Power 2 (expected on)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 off");
			mioe.add(new IOException("Module reports no power on power 2"));
		}
		
		// Ethernet 1 (expected off)
		if (!dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 not linked");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 linked");
			throw new Exception("Module has a link on ethernet 1");
		}
		
		// Ethernet 2 (expected on)
		if (dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 2. Please verify the ethernet cable from the switch to Ethernet Port 2 is secure. Continue the test?", "Ethernet 2 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 2 link");
			} else {
				dis = udr.getD(stationID, (short)DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH2LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 2"));
				}
			}
		}
		
		// POE (expected off, since ethernet 1 is not linked)
		if (!dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE powered with no Ethernet 1 link");
			mioe.add(new IOException("Module reports POE is available when there is no Ethernet 1 link"));
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks for Base 2 complete");
	}
	
	private void calibrateRTDs() throws Exception, IOException, TimeoutException {
        MultiIOException mioe = new MultiIOException();
        // There are 10 RTD Inputs on this module, but this will keep us from hardcoding that in multiple places
		int aiCount = 10;
		int refResCount = 2; // Number of 100 ohm reference resistors on this module
				
		// Convert resistance values of Calibration points AX4 and AX9 to raw Identity Curve expected values
		// expVal[]s are constant as long as the calPoint registers or resistances do not change.
		// This is here so that the expVals[]s do not have to be recalculated by hand if above changes are made.
		short [] expVal = new short[refResCount];
		for (int i = 0; i < refResCount; i++) {
			expVal[i] = (short)((CAL_POINT[i]/100)*RAW_VALUE);
		}	
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 300);
		ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating RTD Inputs");
				
		// Set Temperature Reporting Mode to 0.1 Deg. C, and
		// write Identity Curve Value Range setting (0x61) to Analog Input Range settings in base setup file for all RTD Channels. 
		testLib.loadBaseFiles(S_CALBASE, DIR_CALIBRATE_RTDS);
		// Wait for it to take effect
		Utils.sleep(testLib.getScanDelay() * 2); // Convenient that the scan delay is exactly what we want here...				
		ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");
						
		// Read average raw Identity Curve values for all RTD inputs
		
		Utils.sleep(testLib.getScanDelay() * 2);
		
		short[] actualRTDVals = testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aiCount, TOLERANCE_AVERAGE);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Verifying calibration channel values");
		
		// Make sure actualRTDVals used for calibration points are reasonably close 
		// to known expected values (+/- 100 counts). 
        boolean calSuccess = false; // flag set to true when the calibration is successful
        boolean doCal = true; // flag to stop the calibration stuff if the initial readings are bad
		{
			for (int i = 0; i < refResCount; i++) {
				String msg = formatRangeMessage("RTD", CAL_CHANNEL [i], actualRTDVals[CAL_CHANNEL [i]], expVal[i], TOLERANCE_EXP_VAL);
				if (Math.abs(actualRTDVals[CAL_CHANNEL [i]] - expVal[i]) > TOLERANCE_EXP_VAL) {
				    doCal = false; // don't cal the unit if the initial readings are bad
                    mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
				}
			}
		}
        if (doCal) { // continue if readings are good
            // RTD Default Gains set to 0x6664 during full production test
            // Current gain calibration settings are read here so we can
            // adjust the calibration setting
    		byte [] gain1 = udr.readFile(S_CALBASE, DIR_MODULECONFIG + "cals", OFFSET_DEFAULT_SPAN1, LENGTH_DEFAULT_SPAN1, null);
    		short span1 = Conversion.bytesToShort(gain1, 0);
    		byte [] gain2 = udr.readFile(S_CALBASE, DIR_MODULECONFIG + "cals", OFFSET_DEFAULT_SPAN2, LENGTH_DEFAULT_SPAN2, null);
    		short span2 = Conversion.bytesToShort(gain2, 0);
    		
    		// Calculate new Gain (Span) values
    		short newGain1 = (short)(span1*((float)expVal[0]/(float)actualRTDVals[4]));
    		short newGain2 = (short)(span2*((float)expVal[1]/(float)actualRTDVals[9]));
    		
    		// Write the new gain values to the module calibrations configuration file
    		ui.displayMessage(this, UserInterface.M_WARNING, "  Writing calibration data to station");
    		
    		byte[] calsData = new byte[refResCount * 4]; // 2 values of 2 bytes apiece for each gain
    		int offset = 2;
    		Conversion.shortToBytes(calsData, offset, newGain1);
    		offset += 4;
    		Conversion.shortToBytes(calsData, offset, newGain2);
    				
    		udr.writeFile(S_CALBASE, DIR_MODULECONFIG + "cals", calsData, OFFSET_CAL_START_ADDR, null);
    		
            calSuccess = true; // cal values were written
        }
		ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
		
		// Reset the station
		testLib.resetStation(S_CALBASE);
		
        if (calSuccess) {
            ui.displayMessage(this, UserInterface.M_NORMAL,
                "  RTD calibration values written to memory");
        }
        else {
            ui.displayMessage(this, UserInterface.M_ERROR,
                "  RTD calibration was unsuccessful");
        }
		
		if (!mioe.isEmpty()) throw mioe;
   	}
	
	private void testRTDCalibration() throws Exception, IOException, TimeoutException {
		// This keeps us from hard-coding this number everywhere
		int aiCount = 10;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing RTDs in 16-bit integrating mode");
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 300);
		
		// Set Temperature Reporting Mode to 0.1 Deg. C, and
		// write Alpha .00385 European Value Range setting (0x62) to base setup file for all RTD Channels. 
		testLib.loadBaseFiles(S_CALBASE, DIR_CALBASE);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing RTD Accuracy");
		
		// Read all RTD channels and check accuracy to within +/- .5 Deg. C
		{
			Utils.sleep(testLib.getScanDelay() * 2);
			
			short[] ais = testLib.getAverageAnalogs(S_CALBASE, (short)0, (short)aiCount, TOLERANCE_AVERAGE);
			
			// Compare
			// Every channel should be within +/- 0.5 Deg. C of expected value
			for (int i = 0; i < ais.length; i++) {
				String msg = formatRangeMessage("RTD", i, ais[i], BASE1_EXPVALS[i], TOLERANCE_RTD);
				if (Math.abs(ais[i] - BASE1_EXPVALS[i]) > TOLERANCE_RTD) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
				}
			}
		}
			
		// Make sure the LEDs are all lit
		if (!ui.confirm("Are all 10 of the RTD LEDs lit?", "RTD LEDs")) {
			mioe.add(new IOException("Not all RTD LEDs light"));
			ui.displayMessage(this, UserInterface.M_ERROR, "  Not all RTD LEDs light" );
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  RTD LED test complete");
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  RTD accuracy check complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	
	private void testLeadResistance() throws Exception, IOException, TimeoutException {
		// This keeps us from hard-coding this number everywhere
		int aiCount = 10;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing RTDs lead resistance compensation accuracy");
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 300);
		
		// Set Temperature Reporting Mode to 0.1 Deg. C, and
		// write Alpha .00385 European Value Range setting (0x62) to base setup file for all RTD Channels. 
		testLib.loadBaseFiles(S_TESTBASE, DIR_TESTBASE);
		ui.displayMessage(this, UserInterface.M_WARNING, "  RTD Lead Resistance Accuracy");
		
		// Read all RTD channels and check accuracy to within +/- 2.00 Deg. C
		{
			Utils.sleep(testLib.getScanDelay() * 2);
			
			short[] ais = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aiCount, TOLERANCE_AVERAGE);
			
			// Compare
			// Every channel should be within +/- 2.00 Deg. C of expected value
			for (int i = 0; i < ais.length; i++) {
				String msg = formatRangeMessage("RTD", i, ais[i], BASE2_EXPVALS[i], TOLERANCE_LEAD_RES);
				if (Math.abs(ais[i] - BASE2_EXPVALS[i]) > TOLERANCE_LEAD_RES) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  RTD lead resistance accuracy check complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	// Determine what test fixture is being used and determine calibration values accordingly, since 
    // finding exact resistor values to three decimal places is somewhat difficult.
	//determineFixture();
	private boolean determineFixture() throws Exception, IOException, TimeoutException {
	    boolean foundFixture = false;
		int aiCount = 16;
	    int calCount = 10;
        
	    MultiIOException mioe = new MultiIOException();
        
        ui.displayMessage(this, UserInterface.M_WARNING, "Determining test fixture version");
        
        // Set the scan delay
        // Documentation says 100ms per active channel, we add a little to be careful
        testLib.setScanDelay(100 * aiCount + 200);
        
        // Read in analog values
        ui.displayMessage(this, UserInterface.M_WARNING, "Reading values");
        Utils.sleep(testLib.getScanDelay() * 2);
        short[] aiValues = udr.getA(S_DIAGS, (short)0, (short)aiCount);
        
        ui.displayMessage(this, UserInterface.M_WARNING, "Comparing versions");
        // Using Watchdog tolerance values, as both the AI inputs determining fixture version AND Watchdog
        // inputs have 3.92k 1% resistors in wire. Using a wider acceptance tolerance since we are
        // looking for which AI has the connection not what the value really is.
        if (Math.abs(EXP_WATCHDOG - aiValues[13]) < TOLERANCE_FIXTURE) {
            for (int i = 0; i < calCount; i++) {
                BASE1_EXPVALS[i] = BASE1_EXPVALS_C[i];
                BASE2_EXPVALS[i] = BASE2_EXPVALS_C[i];
            }
            CAL_POINT[0] = CAL_POINT_C[0];
            CAL_POINT[1] = CAL_POINT_C[1];
            ui.displayMessage(this, UserInterface.M_NORMAL, "  Test Fixture C");
            foundFixture = true;
        } 
        else if (Math.abs(EXP_WATCHDOG - aiValues[14]) < TOLERANCE_FIXTURE) {
            for (int i = 0; i < calCount; i++) {
                BASE1_EXPVALS[i] = BASE1_EXPVALS_B[i];
                BASE2_EXPVALS[i] = BASE2_EXPVALS_B[i];
            }
            CAL_POINT[0] = CAL_POINT_B[0];
            CAL_POINT[1] = CAL_POINT_B[1];
            ui.displayMessage(this, UserInterface.M_NORMAL, "  Test Fixture B");
            foundFixture = true;
        } 
        else if (Math.abs(EXP_WATCHDOG - aiValues[15]) < TOLERANCE_FIXTURE) {
            for (int i = 0; i < calCount; i++) {
                BASE1_EXPVALS[i] = BASE1_EXPVALS_A[i];
                BASE2_EXPVALS[i] = BASE2_EXPVALS_A[i];
            }
            CAL_POINT[0] = CAL_POINT_A[0];
            CAL_POINT[1] = CAL_POINT_A[1];
            ui.displayMessage(this, UserInterface.M_NORMAL, "  Test Fixture A");
            foundFixture = true;
        }
        if (foundFixture) {
        	ui.displayMessage(this, UserInterface.M_NORMAL, "  Test Fixture version determined");
        }
        else {
            ui.displayMessage(this, UserInterface.M_ERROR,
                    "  Unable to determine Test Fixture version - check that the fixture is powered with 24.0V.");
            ui.displayMessage(this, UserInterface.M_ERROR,
                    "    - Unit under test must be in Base 1 & use Ethernet to determine Test Fixture.");
        }
        if (!mioe.isEmpty()) throw mioe;
        return foundFixture;
	}
	private void determineFixtureHelper() {
	    //This is here to populate the correct RTD values into the arrays in the case of an individual test on base 2.
	    try {
            activeHandler = testLib.registerUDPHandler(IP_CALBASE);
            
            ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
            udr.waitForResponse(S_CALBASE);
            
            // Make sure it works
            testLib.loadBaseFiles(S_CALBASE, DIR_CALBASE);
            
            // Determine what test fixture is being used and determine calibration values accordingly, since 
            // finding exact resistor values to three decimal places is somewhat difficult.
            determineFixture();            
        } catch (Exception e) {
            ui.handleException(this, e);
            ui.operationFailed(this);
            return;
        }
	}
	private void checkPOE() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the PoE output");
		
		// If it's an EB base, check AI_POE for a value, otherwise expect something small (-8192, probably)
		short[] vals = udr.getA(S_DIAGS, AI_POE, (short)1);
		
		if (devName.startsWith("EB")) {
			if (Math.abs(EXP_POE - vals[0]) > TOLERANCE_POE) {
				throw new IOException(String.format("AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_NOTMEMBEROF, EXP_POE, TOLERANCE_POE));
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_MEMBEROF, EXP_POE, TOLERANCE_POE));
			}
		} else {
			if (vals[0] > 0) {
				throw new IOException("It appears that there is a PoE board on this module, but it has been initialized as an E2.");
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  As expected, there is no PoE board on this module.");
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  PoE test complete");
	}
	
	private void checkWatchdog() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the Watchdog output");
		
		// Give it a couple seconds in case it was rebooted recently
		Utils.sleep(10000);
		
		short[] vals = udr.getA(S_DIAGS, AI_WATCHDOG, (short)1);
		
		if (Math.abs(EXP_WATCHDOG - vals[0]) > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output): is off when it should be on", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is on as expected", AI_WATCHDOG));
		}
		
		// Make sure the watchdog output is off
		testLib.loadBaseFiles(S_CALBASE, DIR_WATCHDOG_OFF);
		
		// Give it a couple extra seconds
		Utils.sleep(10000);
		
		vals = udr.getA(S_DIAGS, AI_WATCHDOG, (short)1);
		
		if (vals[0] > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output) is on when it should be off", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is off as expected", AI_WATCHDOG));
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Watchdog test complete");
	}
	
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
		
	private String formatRangeMessage(String type, int channel, short found, short expected, int tolerance) {
		String op = TestLib.CH_MEMBEROF;
		if (Math.abs(expected - found) > tolerance) op = TestLib.CH_NOTMEMBEROF;
		
		return String.format("%s Channel %,d: %s%d [%d %s (%d +/- %d)]", type, channel, TestLib.CH_DELTA, found - expected, found, op, expected, tolerance);
	}
}

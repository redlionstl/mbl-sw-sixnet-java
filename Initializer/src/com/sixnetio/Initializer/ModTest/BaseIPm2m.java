/*
 * BaseIPm2m.java
 *
 * Tests IPm2m bases.
 *
 * Jonathan Pearson
 * October 7, 2008
 *
 */

package com.sixnetio.Initializer.ModTest;

import com.sixnetio.Initializer.*;
import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRLink;

import java.io.*;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.util.*;

public class BaseIPm2m implements Tester {
	// Where test data is stored
	private static final String DIR_CONFIG = "TestData" + File.separator + "IPm2mBase" + File.separator;
	
	private static final String T_BATTERY = "Battery Test";
    
    private static final String[] TEST_NAMES = {
        T_BATTERY
    };
    
    // Try to keep every test module using a different set of IP addresses, just in case we figure out
    //   a way to run multiple fixtures from the same machine at the same time
    private static final String IP_BASE = "10.1.0.1";
    
    private static final short S_BASE = 1;
    
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private UDRLink activeHandler;
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	
	public BaseIPm2m() { }
	
	public String getJumpTest() {
		return chosenTest;
    }
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
    }
	
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
    }
	
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for ModIPm2mBase must not be null");
		
		// Make sure the device name is recognized
		// Must start with VT-IPm2m
		// Must end with -B
		if (!((devName.startsWith("VT-IPm2m")) &&
		      (devName.endsWith("-B")))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)109, (byte)0);
		testLib = new TestLib(udr, ui);
		testLib.setBaseConfigDir("/base/cfg/");
		testLib.setIpmStation(true);
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
    }
	
	public boolean supportsTestJump() {
		return true;
    }
	
	public void run() {
		MultiIOException mioe = new MultiIOException();
		
		if (chosen(T_BATTERY)) {
			// Register a UDP handler for the base
			activeHandler = null;
    		try {
    			// In case the MAC address changed for some reason
    			testLib.clearARP(IP_BASE);
    			
    			activeHandler = testLib.registerUDPHandler(IP_BASE);
    			
    			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
    			udr.waitForResponse(S_BASE);
    			ui.displayMessage(this, UserInterface.M_NORMAL, "  Device is responding");
    		} catch (Exception e) {
    			if (activeHandler != null) testLib.unregisterHandler(activeHandler);
    			
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		}
    		
    		try { // Finally block unregisters the handler
    			// Now start doing tests
    			// Test the battery
    			try {
    				if (chosen(T_BATTERY)) batteryTest();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		} finally {
    			testLib.unregisterHandler(activeHandler);
    		}
		}
		
		if (!mioe.isEmpty()) {
			ui.handleException(this, mioe);
			ui.operationFailed(this);
		} else {
			ui.operationCompleted(this);
		}
    }
	
	private void batteryTest() throws Exception, IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the backup battery");
		
		// Set the clock
		udr.setClock(S_BASE, (int)(System.currentTimeMillis() / 1000));
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Powering off for 10 seconds");
		
		long powerDownAt;
		
		while (true) {
			if (!ui.confirm("Please power off the device", "Disconnect Power")) {
				if (ui.confirm("Really cancel the test?", "Cancel")) {
					throw new Exception("User canceled the test");
				}
			} else {
				powerDownAt = System.currentTimeMillis();
				
				try {
					testLib.verifyConnectivity(S_BASE);
					
					continue; // The user didn't listen
				} catch (Exception e) {
					// This is good, the user listened
					break;
				}
			}
		}
		
		// Delay for 10 seconds, minus the time spent verifying that the user actually powered it down
		Utils.sleep(10000 - (System.currentTimeMillis() - powerDownAt));
		
		while (!ui.confirm("Please power on the device", "Reconnect Power")) {
			if (ui.confirm("Really cancel the test?", "Cancel")) throw new Exception("User canceled the test");
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Waiting for the device to boot");
		
		udr.waitForResponse(S_BASE);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Checking clock");
		
		int result = udr.getClock(S_BASE);
		int expected = (int)(System.currentTimeMillis() / 1000);
		
		// Allow a 1-second leeway for rounding errors
		if (Math.abs(result - expected) > 1) throw new IOException(String.format("Real-time clock reports time as %d, should be %d", result, expected));
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Battery Test Complete");
    }
	
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
}

/*
 * Mod16_32AI20M.java
 *
 * Tests a 16AI20M or 32AI20M module.
 *
 * Jonathan Pearson
 * August 19, 2008
 *
 */

package com.sixnetio.Initializer.ModTest;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.Initializer.Tester;
import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.util.*;

public class Mod16_32AI20M_hon implements Tester {
	// Constants
	// Locations of the configuration files
	private static final String DIR_START = "TestData" + File.separator + "AI20M_HON" + File.separator + "Start";
	private static final String DIR_WATCHDOG_OFF = "TestData" + File.separator + "AI20M_HON" + File.separator + "WatchdogOff";
	private static final String DIR_10BIT = "TestData" + File.separator + "AI20M_HON" + File.separator + "10bit";
	
	private static final String T_DIAG = "Diagnostics Test",
	                            T_AICal = "AI Calibration",
	                            T_AI = "AI Test",
	                            T_POE = "PoE Test",
	                            T_WATCHDOG = "Watchdog Test";
	
	private static final String[] TEST_NAMES = {
		T_DIAG,
		T_AICal,
		T_AI,
		T_POE,
		T_WATCHDOG
	};
	
	// Try to keep every test module using a different set of IP addresses, just in case we figure out
	//   a way to run multiple fixtures from the same machine at the same time
	private static final String IP_BASE1 = "10.1.9.1";
	private static final String IP_BASE2 = "10.1.9.2";
	
	private static final short S_TESTBASE = 1; // 16/32AI20M being tested
	private static final short S_DIAGS = 2; // 16AI in charge of reading POE and Watchdog outputs
	private static final short S_OUTPUTS0_7 = 3; // 8AO in charge of AIs 0-7
	private static final short S_OUTPUTS8_15 = 4; // 8AO in charge of AIs 8-15
	private static final short S_OUTPUTS16_23 = 5; // 8AO in charge of AIs 16-23
	private static final short S_OUTPUTS24_31 = 6; // 8AO in charge of AIs 24-31
	
	
	// Which AI the watchdog output terminal is plugged into
	private static final short AI_WATCHDOG = 1; // (0-based)
	
	// Which AI the POE output terminal is plugged into
	private static final short AI_POE = 0; // 0-based
	
	// Diagnostic DI offsets from DI_DiagStart
	private static final short DI_DIAG_START = 32; // The first diagnostic DI
	private static final short DIAG_SELFTEST = 0, // Module Selftest
	                           DIAG_POWER1 = 1, // Power 1
	                           DIAG_POWER2 = 2, // Power 2
	                           DIAG_IOSTATUS = 3, // I/O Status
	                           DIAG_ETH1LINK = 4, // Ethernet 1 has link
	                           DIAG_ETH2LINK = 5, // Ethernet 2 has link
	                           DIAG_RING = 6, // Ring is complete
	                           DIAG_POE = 7; // PoE is available
	
	// Analog values (to avoid hardcoding them in multiple places)
	private static final short EXP_WATCHDOG = (short)3686; // Expected watchdog input value; 3686 = 5.8mA = 23.2V across 4000 Ohms
	private static final short EXP_POE = (short)4096; // Expected PoE input value; 4096 = 6mA = .6V across a 100 Ohm resistor
	private static final short AO_5MA = (short)2047; // To output 5mA from an AO module
	private static final short AO_18MA = (short)28671; // To output 18mA from an AO module
	
	// Tolerance for analog values
	// If two analog values should be equal, values within this distance on either side will be accepted
	private static final int TOLERANCE_ANALOG = 12;
	
	// Similar to AnalogTolerance, this is used to make sure consecutive scans during an average are close enough
	// Since values are allowed to be off by 12 in either direction of the target value, a difference of 24 covers
	//   the situation where one scane sees a value 12 below and the next scan sees 12 above, then we add a little
	//   extra to avoid spurious errors
	private static final int TOLERANCE_AVERAGE = 25;
	
	// When testing for channel independence (all get different values), this is the tolerance for each channel
	private static final int TOLERANCE_INDEPENDENCE = 25;
	
	// Used to check that the Watchdog input is close to what it should be
	private static final int TOLERANCE_WATCHDOG = 450;
	
	// Used to check that the PoE input is close to what it should be
	private static final int TOLERANCE_POE = 600;
	
	// Same as Analog/Average/Independence Tolerance, but these are used for 10-bit high-speed
	//   mode, which is wildly less accurate (6 fewer bits means +/- 64 for one analog point)
	// Allowed to be 12 analog points off, so 768 makes sense (12 * 64)
	private static final int TOLERANCE_HIGHSPEED_ANALOG = 768;
	
	// 768 below, then 768 above means 2 * 768 range across consecutive values
	private static final int TOLERANCE_HIGHSPEED_AVERAGE = 1536;
	
	// IndependenceTolerance is equal to AverageTolerance, so same here
	private static final int TOLERANCE_HIGHSPEED_INDEPENDENCE = 1536;
	
	// Base directory in a module where we will find the configuration files
	// We expect a slash on the end of this
	private static final String DIR_MODULECONFIG = "/module0/arc/";
	
	// Offset/length of the AI calibration section of the module cals file
	private static final int OFFSET_AI_CALIBRATION = 0x100;
	private static final int LENGTH_AI_CALIBRATION = 0x80;
	
	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private UDRLink[] activeHandler = new UDRLink[2]; // One for the E2, one for the ET
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	
	public Mod16_32AI20M_hon() { }
	
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for Mod16_32AI20M must not be null");
		
		// Make sure the device name is recognized
		// Must start with E2 or EB
		// Must end with 16AI20M or 32AI20M
		if (!((devName.startsWith("E2") || devName.startsWith("EB")) &&
		      (devName.endsWith("16AI20M") || devName.endsWith("32AI20M")))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)105, (byte)0);
		testLib = new TestLib(udr, ui);
		
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
	}
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
	}
	
	public boolean supportsTestJump() {
		return true;
	}
	
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
	}
	
	public String getJumpTest() {
		return chosenTest;
	}
	
	public void run() {
		// Register UDP handlers for bases 1 and 2 (the E2 AI module and the ET AI module)
		Arrays.fill(activeHandler, null); // In case something fails, so we can unregister those that succeed
		
		try {
			activeHandler[0] = testLib.registerUDPHandler(IP_BASE1);
			activeHandler[1] = testLib.registerUDPHandler(IP_BASE2);
		} catch (IOException ioe) {
			unregisterHandlers();
			
			ui.handleException(this, ioe);
			ui.operationFailed(this);
			return;
		}
		
		MultiIOException mioe = new MultiIOException();
		
		try { // Finally block unregisters the handlers
			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
			udr.waitForResponse(S_TESTBASE);
			
			// First, load the files into the base
			testLib.loadBaseFiles(S_TESTBASE, DIR_START);
			
			// Now start doing tests
			// Diagnostics
			try {
				if (chosen(T_DIAG)) checkDiags();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			
			// AI Calibration/test
			try {
				if (chosen(T_AICal)&&(!runVerifyTest)) calibrateAIs();
				if (chosen(T_AI)) testAIs(); // If the calibration fails, don't do the test
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			
			// POE test
			try {
				if (chosen(T_POE)) checkPOE();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
			
			// Watchdog test
			try {
				if (chosen(T_WATCHDOG)) checkWatchdog();
			} catch (IOException ioe) {
				mioe.add(ioe);
			}
		} catch (Exception e) {
			ui.handleException(this, e);
			ui.operationFailed(this);
			return;
		} finally {
			unregisterHandlers();
		}
		
        if (!mioe.isEmpty()) {
			ui.handleException(this, mioe);
			ui.operationFailed(this);
			return;
		} 
        else {
	        if (chosenTest == null) {
	            // null is all tests
	            // write the pass flag
	            if (writePassedFlag(true)){
	                ui.displayMessage(this, UserInterface.M_NORMAL,
	                "Module tests passed");
	            }
	            else{
	                ui.handleException(this, new IOException("Failed to write the All Tests Passed flag to module."));
	                ui.operationFailed(this);
	                return;
	            }
	        }
	        else {
	            ui.displayMessage(this, UserInterface.M_NORMAL, "Module test '" +
	                                                            chosenTest +
	                                                            "' passed");
	        }

			ui.operationCompleted(this);
		}
	}
	
    private boolean writePassedFlag(boolean pass)
    {
        boolean ret = false;
        byte[] flagbyte = new byte[1]; 
        flagbyte[0] = 0; // must be 0 if failed
        if(pass) flagbyte[0] = 1; // must be 1 if pass
        try {
            activeHandler[1] = testLib.registerUDPHandler(IP_BASE1);
            udr.writeFile(S_TESTBASE, "/module0/arc/factory", flagbyte, 0x4B, null);
            // Disconnect the handler
            unregisterHandlers();
            ret = true;
        }
        catch (IOException e) {
            ret = false;
        }
        catch (TimeoutException e) {
            ret = false;
        }
    
        return ret;
    }

	private void checkDiags() throws Exception, IOException, TimeoutException {
		final short stationID = S_TESTBASE;
		
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Checking diagnostics");
		
		// Make sure that P2 will be on
		if (!ui.confirm("Please set the P2 switch to the ON position", "P2 Switch")) {
			throw new Exception("User canceled the test");
		}
		
		boolean[] dis = udr.getD(stationID, DI_DIAG_START, (short)8);
		
		// Self test (expected on)
		if (dis[DIAG_SELFTEST]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Self test passed");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Self test failed");
			mioe.add(new IOException("Module reports self-test failure"));
		}
		
		// Power 1 (expected on)
		if (dis[DIAG_POWER1]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 1 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 1 off");
			mioe.add(new IOException("Module reports no power on power 1"));
		}
		
		// Power 2 (expected on)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 on");
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 off");
			mioe.add(new IOException("Module reports no power on power 2"));
		}
		
		// Ethernet 1 (expected on)
		if (dis[DIAG_ETH1LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 1. Please verify the ethernet cable from the power injector to Ethernet Port 1 is secure. Continue the test?", "Ethernet 1 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
				
				// Throw an Exception instead of IOException so the caller can tell when the user cancels
				throw new Exception("Module has no ethernet 1 link");
			} else {
				dis = udr.getD(stationID, DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH1LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 1 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 1 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 1"));
				}
			}
		}
		
		// Ethernet 2 (expected on)
		if (dis[DIAG_ETH2LINK]) {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
		} else {
			if (!ui.confirm("The module reports that there is no ethernet link on port 2. Please verify the ethernet cable from Ethernet Port 2 to the EtherTRAK module is secure. Continue the test?", "Ethernet 2 Link")) {
				ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
				throw new Exception("Module has no ethernet 2 link");
			} else {
				dis = udr.getD(stationID, DI_DIAG_START, (short)8);
				
				if (dis[DIAG_ETH2LINK]) {
					ui.displayMessage(this, UserInterface.M_NORMAL, "  Ethernet 2 linked");
				} else {
					ui.displayMessage(this, UserInterface.M_ERROR, "  Ethernet 2 not linked");
					mioe.add(new IOException("Module reports no link on ethernet port 2"));
				}
			}
		}
		
		// POE (expected on if EB, off if E2)
		if (devName.startsWith("EB") && !dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  No POE");
			mioe.add(new IOException("Module reports no POE, although it is a POE module and ethernet 1 has a link"));
		} else if (devName.startsWith("E2") && dis[DIAG_POE]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  POE on non-EB module");
			mioe.add(new IOException("Module reports POE is available, although it is not an EB module"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  POE good");
		}
		
		// Ask the user to switch off power 2
		if (!ui.confirm("Please set the P2 switch to the OFF position", "P2 Switch")) {
			throw new Exception("User canceled the test");
		}
		
		dis = udr.getD(stationID, DI_DIAG_START, (short)8);
		
		// Power 2 (expected off)
		if (dis[DIAG_POWER2]) {
			ui.displayMessage(this, UserInterface.M_ERROR, "  Power 2 on");
			mioe.add(new IOException("Module reports power on power 2"));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  Power 2 off");
		}
		
		if (!mioe.isEmpty()) throw mioe;
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Diagnostic checks complete");
	}
	
	private void calibrateAIs() throws IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Calibrating AIs");
		
		// Clear any calibration data that may exist
		ui.displayMessage(this, UserInterface.M_WARNING, "  Clearing current calibration");
		
		if (testLib.zeroRange(S_TESTBASE, DIR_MODULECONFIG + "cals", OFFSET_AI_CALIBRATION, LENGTH_AI_CALIBRATION)) {
			ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
			testLib.resetStation(S_TESTBASE);
		}
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Calibrating");
		
		// Figure out how many AIs there are
		int aiCount;
		if (devName.endsWith("16AI20M")) {
			aiCount = 16;
		} else if (devName.endsWith("32AI20M")) {
			aiCount = 32;
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unrecognized device name: " + devName);
			throw new IOException("Unrecognized device name: " + devName);
		}
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 200);
		
		// There are aiCount AIs coming from AOs on stations 3, 4, 5, and 6 (only 3 and 4 for the 16)
		short[] aos = new short[aiCount];
		
		// Calibration results will end up in these arrays
		short[] spans = new short[aos.length];
		short[] zeros = new short[aos.length];
		
		// Write 5mA to all input channels
		Arrays.fill(aos, AO_5MA);
		setAOs(aos);
		
		// Wait for it to take effect
		Utils.sleep(testLib.getScanDelay() * 4);
		
		// Grab the results
		short[] aisLow = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aos.length, TOLERANCE_AVERAGE);
		
		// Write 18mA to all input channels
		Arrays.fill(aos, AO_18MA);
		setAOs(aos);
		
		// Wait for it to take effect
		Utils.sleep(testLib.getScanDelay() * 4);
		
		// Grab the results
		short[] aisHigh = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aos.length, TOLERANCE_AVERAGE);
		
		// Compute the module span and zero
		testLib.computeAICurrentCalibration(aisLow, aisHigh, AO_5MA, AO_18MA, zeros, spans);
		
		// Write the corrections to the config file
		ui.displayMessage(this, UserInterface.M_WARNING, "  Writing calibration data to station");
		
		byte[] calsData = new byte[aos.length * 4]; // 2 values of 2 bytes apiece for each input
		int offset = 0;
		for (int i = 0; i < aos.length; i++) {
			Conversion.shortToBytes(calsData, offset, zeros[i]);
			offset += 2;
			
			Conversion.shortToBytes(calsData, offset, spans[i]);
			offset += 2;
		}
		
		udr.writeFile(S_TESTBASE, DIR_MODULECONFIG + "cals", calsData, OFFSET_AI_CALIBRATION, null);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "  Resetting station");
		
		// Reset the station
		testLib.resetStation(S_TESTBASE);
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  AI calibration complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	
	private void testAIs() throws IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AIs in 16-bit integrating mode");
		
		// Figure out how many AIs there are
		int aiCount;
		if (devName.endsWith("16AI20M")) {
			aiCount = 16;
		} else if (devName.endsWith("32AI20M")) {
			aiCount = 32;
		} else {
			ui.displayMessage(this, UserInterface.M_ERROR, "Unrecognized device name: " + devName);
			throw new IOException("Unrecognized device name: " + devName);
		}
		
		// Set the scan delay
		// Documentation says 100ms per active channel, we add a little to be careful
		testLib.setScanDelay(100 * aiCount + 200);
		
		// Set all channels low/high and check that they are each where they should be
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing all AIs at once");
		{
			short[] aos = new short[aiCount];
			
			// Write 5mA to all channels
			{
				Arrays.fill(aos, AO_5MA);
				setAOs(aos);
				Utils.sleep(testLib.getScanDelay() * 2);
				
				short[] ais = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aos.length, TOLERANCE_AVERAGE);
				
				// Compare
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i, ais[i], aos[i], TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
				}
			}
			
			// Write 18mA to all channels
			{
				Arrays.fill(aos, AO_18MA);
				setAOs(aos);
				Utils.sleep(testLib.getScanDelay() * 2);
				
				short[] ais = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aos.length, TOLERANCE_AVERAGE);
				
				// Compare
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i, ais[i], aos[i], TOLERANCE_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
				}
			}
		}
		
		// Make sure the LEDs are all lit
		if (!ui.confirm("Are all of the AI LEDs lit?", "AI LEDs")) {
			mioe.add(new IOException("Not all AI LEDs light"));
		}
		
		// Test independence of channels
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing channel independence");
		
		{
			short[] aos = new short[aiCount];
			for (int i = 0; i < aos.length; i++) {
				aos[i] = (short)((AO_18MA - AO_5MA) * i / aos.length + AO_5MA);
			}
			setAOs(aos);
			Utils.sleep(testLib.getScanDelay() * 2);
			
			short[] ais = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aos.length, TOLERANCE_AVERAGE);
			
			// Compare
			for (int i = 0; i < aos.length; i++) {
				// A small amount of influence is allowed; a difference of 1mA
				//   is equal to an analog difference of about 2048, so as long
				//   as IndependenceTolerance is smaller than that, all is well
				String msg = formatRangeMessage("AI", i, ais[i], aos[i], TOLERANCE_INDEPENDENCE);
				if (Math.abs(ais[i] - aos[i]) > TOLERANCE_INDEPENDENCE) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  16-bit AI tests complete");
		
		// Switch to 10-bit mode and test the calibration again on individual channels
		testLib.loadBaseFiles(S_TESTBASE, DIR_10BIT);
		
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing AIs in 10-bit high-speed mode");
		
		// Set all channels low/high and check that they are each where they should be
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing all AIs at once");
		{
			short[] aos = new short[2];
			
			// Write 5mA to all channels
			{
				Arrays.fill(aos, AO_5MA);
				setAOs(aos);
				Utils.sleep(testLib.getScanDelay() * 2);
				
				short[] ais = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);
				
				// Compare
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i, ais[i], aos[i], TOLERANCE_HIGHSPEED_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
				}
			}
			
			// Write 18mA to all channels
			{
				Arrays.fill(aos, AO_18MA);
				setAOs(aos);
				Utils.sleep(testLib.getScanDelay() * 2);
				
				short[] ais = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);
				
				// Compare
				for (int i = 0; i < ais.length; i++) {
					String msg = formatRangeMessage("AI", i, ais[i], aos[i], TOLERANCE_HIGHSPEED_ANALOG);
					if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_ANALOG) {
						mioe.add(new IOException(msg));
						ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
					} else {
						ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
					}
				}
			}
		}
		
		// Test for channel independence
		ui.displayMessage(this, UserInterface.M_WARNING, "  Testing channel independence");
		{
			short[] aos = new short[2];
			for (int i = 0; i < aos.length; i++) {
				aos[i] = (short)((AO_18MA - AO_5MA) * i / aos.length + AO_5MA);
			}
			setAOs(aos);
			Utils.sleep(testLib.getScanDelay() * 2);
			
			// Get the average value
			short[] ais = testLib.getAverageAnalogs(S_TESTBASE, (short)0, (short)aos.length, TOLERANCE_HIGHSPEED_AVERAGE);
			
			// Compare
			for (int i = 0; i < aos.length; i++) {
				// A small amount of influence is allowed; a difference of 1mA
				//   is equal to an analog difference of about 2048, so as long
				//   as IndependenceTolerance is smaller than that, all is well
				String msg = formatRangeMessage("AI", i + 1, ais[i], aos[i], TOLERANCE_HIGHSPEED_INDEPENDENCE);
				if (Math.abs(ais[i] - aos[i]) > TOLERANCE_HIGHSPEED_INDEPENDENCE) {
					mioe.add(new IOException(msg));
					ui.displayMessage(this, UserInterface.M_ERROR, "    " + msg);
				} else {
					ui.displayMessage(this, UserInterface.M_NORMAL, "    " + msg);
				}
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  10-bit AI tests complete");
		
		if (!mioe.isEmpty()) throw mioe;
	}
	
	private void checkPOE() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the PoE output");
		
		// If it's an EB base, check AI_POE for a value, otherwise expect something small (-8192, probably)
		short[] vals = udr.getA(S_DIAGS, AI_POE, (short)1);
		
		if (devName.startsWith("EB")) {
			if (Math.abs(EXP_POE - vals[0]) > TOLERANCE_POE) {
				throw new IOException(String.format("AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_NOTMEMBEROF, EXP_POE, TOLERANCE_POE));
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to PoE output): %s%,d [%,d %s (%,d +/- %,d)]", AI_POE, TestLib.CH_DELTA, vals[0] - EXP_POE, vals[0], TestLib.CH_MEMBEROF, EXP_POE, TOLERANCE_POE));
			}
		} else {
			if (vals[0] > 0) {
				throw new IOException("It appears that there is a PoE board on this module, but it has been initialized as an E2.");
			} else {
				ui.displayMessage(this, UserInterface.M_NORMAL, "  As expected, there is no PoE board on this module.");
			}
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  PoE test complete");
	}
	
	private void checkWatchdog() throws IOException, TimeoutException {
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing the Watchdog output");
		
		// If there is a chosen test, then the user did not go through the diagnostics test,
		//   which leaves the P2 switch in the OFF position (required for this to succeed)
		if (chosenTest != null) {
    		// Ask the user to switch off power 2
    		if (!ui.confirm("Please set the P2 switch to the OFF position", "P2 Switch")) {
    			throw new IOException("User canceled the test");
    		}
		}
		
		// Give it a couple seconds in case it was rebooted recently
		Utils.sleep(10000);
		
		short[] vals = udr.getA(S_DIAGS, AI_WATCHDOG, (short)1);
		
		if (Math.abs(EXP_WATCHDOG - vals[0]) > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output): is off when it should be on", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is on as expected", AI_WATCHDOG));
		}
		
		// Make sure the watchdog output is off
		testLib.loadBaseFiles(S_TESTBASE, DIR_WATCHDOG_OFF);
		
		// Give it a couple extra seconds
		Utils.sleep(10000);
		
		vals = udr.getA(S_DIAGS, AI_WATCHDOG, (short)1);
		
		if (vals[0] > TOLERANCE_WATCHDOG) {
			throw new IOException(String.format("AI Channel %d (connected to Watchdog output) is on when it should be off", AI_WATCHDOG));
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, String.format("  AI Channel %d (connected to Watchdog output) is off as expected", AI_WATCHDOG));
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Watchdog test complete");
	}
	
	private void unregisterHandlers() {
		for (int i = 0; i < activeHandler.length; i++) {
			if (activeHandler[i] != null) {
				testLib.unregisterHandler(activeHandler[i]);
				activeHandler[i] = null;
			}
		}
	}
	
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
	
	private void setAOs(short[] aos) throws IOException, TimeoutException {
		// Loop through aos in blocks of 8
		for (int i = 0; i < aos.length; i += 8) {
			short stationID;
			
			switch (i) {
				case 0:
					stationID = S_OUTPUTS0_7;
					break;
				case 8:
					stationID = S_OUTPUTS8_15;
					break;
				case 16:
					stationID = S_OUTPUTS16_23;
					break;
				case 24:
					stationID = S_OUTPUTS24_31;
					break;
				default:
					throw new IOException("Cannot write more than 32 AOs");
			}
			
			int outputCount = 8;
			if (outputCount > aos.length - i) outputCount = aos.length - i;
			
			short[] block = new short[outputCount];
			System.arraycopy(aos, i, block, 0, outputCount);
			
			udr.putA(stationID, (short)0, (short)outputCount, block);
		}
	}
	
	private String formatRangeMessage(String type, int channel, short found, short expected, int tolerance) {
		String op = TestLib.CH_MEMBEROF;
		if (Math.abs(expected - found) > tolerance) op = TestLib.CH_NOTMEMBEROF;
		
		return String.format("%s Channel %,d: %s%,d [%,d %s (%,d +/- %,d)]", type, channel, TestLib.CH_DELTA, found - expected, found, op, expected, tolerance);
	}
}

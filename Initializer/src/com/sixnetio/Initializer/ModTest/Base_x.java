/*
 * Base_x.java
 *
 * Tests E2/EB-ISOTC bases.
 *
 * Jeff Collins
 * January 21, 2011
 *
 */

package com.sixnetio.Initializer.ModTest;

//import com.sixnetio.Station.MessageDispatcher;
import com.sixnetio.Initializer.*;
import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRLink;

import java.io.*;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.sixnetio.util.*;

public class Base_x implements Tester {

	private static final String T_CJCTEST = "Cold Junction Compensation Test";
    
    private static final String[] TEST_NAMES = {
    	T_CJCTEST
    };
    
    // Default IP address set in default base configuration.
    private static final String IP_BASE = "10.1.0.1";
    
    private static final short S_BASE = 1;
    private static final int TOLERANCE_CJC = 5;  // Allowed tolerance for +/- .5 degrees C from average (mean) CJ temp sensor reading
    											 // Chose this value based on "Temperature Error vs Temperature" curve on temp sensor
    											 // datasheet(TI, TMP112).  This value may need to be reevaluated after implementation
    											 // in production if frequent errors occur due to this tolerance.
    private static final int TOLERANCE_STANDEV = 3;// Allowed tolerance for standard deviation calculation of CJ temp sensor readings   

	// Private data members
	private String chosenTest;
	private UserInterface ui;
	private String devName;
	private UDRLink activeHandler;
	private boolean initialized = false;
	private boolean runVerifyTest = false;
	private UDRLib udr;
	private TestLib testLib;
	
	public Base_x() { }
	
	public String getJumpTest() {
		return chosenTest;
    }
	public boolean runVerifyTests(boolean verifyTest) {
		runVerifyTest = verifyTest;
		return verifyTest;
	}
	public List<String> getTests() {
		return Utils.makeVector(TEST_NAMES);
    }
	
	public void jumpToTest(String testName) {
		if (testName != null) {
			List<String> tests = getTests();
			if (!tests.contains(testName)) throw new IllegalArgumentException("Test name not recognized: " + testName);
		}
		
		chosenTest = testName;
    }
	
	public void setupTest(UserInterface ui, String comPort, String devName) throws IOException {
		if (ui == null) throw new IllegalArgumentException("The UserInterface passed to setupTest for Base_x must not be null");
		
		// Make sure the device name is recognized
		// Must start with EB-Base
		// Must end with -X
		if (!((devName.startsWith("E2-Base")) &&
		      (devName.endsWith("-TC-1"))||(devName.endsWith("-TC-2")))) throw new IOException("Unrecognized device name: " + devName);
		
		this.ui = ui;
		// These tests run over IP, no need for the COM port
		this.devName = devName; // Need to know which module it is
		
		udr = new UDRLib((short)114, (byte)0);
		testLib = new TestLib(udr, ui);
		if (initialized) return; // That's it on the changes that may take place between calls
		
		initialized = true;
    }
	
	public boolean supportsTestJump() {
		return true;
    }
	
	public void run() {
		MultiIOException mioe = new MultiIOException();
		
		if (chosen(T_CJCTEST)) {

			// Register a UDP handler for the base
			activeHandler = null;
			try {
    			// In case the MAC address changed for some reason
				testLib.clearARP(IP_BASE);
				activeHandler = testLib.registerUDPHandler(IP_BASE);
    			
    			ui.displayMessage(this, UserInterface.M_WARNING, "Waiting for device to respond");
    			udr.waitForResponse(S_BASE);
    			ui.displayMessage(this, UserInterface.M_NORMAL, "  Device is responding");
    		} catch (Exception e) {
    			if (activeHandler != null) testLib.unregisterHandler(activeHandler);
    			
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
  			}

    		
    		try { // Finally block unregisters the handler
    			// Now start doing tests
    			try {
    				if (chosen(T_CJCTEST)) coldJuncCompTest();
    			} catch (IOException ioe) {
    				mioe.add(ioe);
    			}
    		} catch (Exception e) {
    			ui.handleException(this, e);
    			ui.operationFailed(this);
    			return;
    		}
    		finally {
    			testLib.unregisterHandler(activeHandler);
    		}
		}
		
		if (!mioe.isEmpty()) {
			ui.handleException(this, mioe);
			ui.operationFailed(this);
		} else {
			ui.operationCompleted(this);
		}
    }
	
	private void coldJuncCompTest() throws Exception, IOException, TimeoutException {
		MultiIOException mioe = new MultiIOException();
		ui.displayMessage(this, UserInterface.M_WARNING, "Testing Cold Junction Compensation Temperature Sensors");
		double sum_ais = 0;// Sum of CJC values for 4 temperature sensors
		double mean_ais = 0;// Average of CJC values for 4 temperature sensors
		double sum_of_variance = 0;// Sum of variances for 4 temperature sensors
		double Tot_variance = 0;// Calculated variance (sum_of_variance/(number of variance values -1))
		double standev = 0;// Sample standard deviation from mean for 4 temperature sensors
		
		// Read 4 temperature sensor AIs on base at AX16-AX19 (0-based)	
		// Sleep for a moment to let it stabilize
		Utils.sleep(5000);
		short[] ais = udr.getA(S_BASE, (short)16, (short)4);			
		// Calculate the sum of the 4 CJ temp sensor readings
		for (int i = 0; i < ais.length; i++) {
			sum_ais += ais[i];
		}

// Calculate the mean and standard deviation for the 4 CJ temp sensor values read from the base
		mean_ais = sum_ais/4;	
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Mean Cold Junction Sensor reading = " + (int)mean_ais );
		if(mean_ais < 190 || mean_ais > 260){
			String msg = String.format("Is the approximate room ambient temperature really %.1f degrees C or %.1f degrees F", mean_ais/10, ((mean_ais/10)*9/5+32));
			if (!ui.confirm(msg, "Confirm Approximate Ambient Room Temperature")) {
				mioe.add(new IOException("Mean Cold Junction Sensor reading not within normal ambient room temperature range!"));
			}	
		}
		for (int i = 0; i < ais.length; i++) {
			sum_of_variance += (ais[i] - mean_ais)*(ais[i] - mean_ais);
		}
		Tot_variance = sum_of_variance/4;
		standev = Math.sqrt(Tot_variance);
		
// Check that the CJ temperature sensor readings are within the acceptable TOLERANCE_CJC from the mean (mean_ais)
		for (int i = 0; i < ais.length; i++) {
			String msg = formatRangeMessage("CJC Sensor AI", i+16, ais[i], (short) mean_ais, TOLERANCE_CJC);
            if((Math.abs((int)ais[i]-(int)mean_ais) >TOLERANCE_CJC)||ais[i]==0){
                mioe.add(new IOException(msg));
                ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
            } else {
                ui.displayMessage(this, UserInterface.M_NORMAL, "  " + msg);
            }
            if(ais[i] < 0){ // looking for a error code
                msg = String.format("Unknown error code (%d) present for CJC Sensor for channel group %d", ais[i], i+1);
                 // looking for a known -1000, -2000, -3000 or -4000 error code
                 /* -1000 DEAD – Non-responsive sensor
                    -2000 FAULTY – Reading is outside of any plausible  values
                    -3000 ERANGE – Outside of the specified tolerance
                    -4000 INDETERMINATE – Not enough good sensors found to make any determination about this sensor
                 */
                if(ais[i] ==  -1000){ 
                    msg = String.format("Error code present for CJC Sensor for channel group %d. (-1000 = sensor not responding)", i+1);
                } else {
                    if(ais[i] ==  -2000){ 
                        msg = String.format("Error code present for CJC Sensor for channel group %d. (-2000 = Reading is outside of -50C to 100C values)", i+1);
                    } else {
                        if(ais[i] ==  -3000){ 
                            msg = String.format("Error code present for CJC Sensor for channel group %d. (-3000 = sensor value far from neighboring sensors)", i+1);
                        } else {
                            if(ais[i] ==  -4000){ 
                                msg = String.format("Error code present for CJC Sensor for channel group %d. (-4000 = Can't determine the validity of the sensor reading)", i+1);
                            }
                        }
                    }
                }
                //Display the error msg
                mioe.add(new IOException(msg));
                ui.displayMessage(this, UserInterface.M_ERROR, "  " + msg);
            } 
		}
// Check that the standard deviation value for the CJ temp sensor readings is less than TOLERANCE_STANDEV. 		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  Standard Deviation = " + standev );	
		if(standev > TOLERANCE_STANDEV){
			mioe.add(new IOException("Standard deviation error exceeds maximum value!"));
			ui.displayMessage(this, UserInterface.M_ERROR, "  " + TOLERANCE_STANDEV + " is the maximum standard deviation error for CJ temp sensor readings");
		} else {
			ui.displayMessage(this, UserInterface.M_NORMAL, "  " + "CJ temp sensor readings are within allowed standard deviation from mean");
		}
		
		ui.displayMessage(this, UserInterface.M_NORMAL, "  CJC Compensation test complete");
		if (!mioe.isEmpty()) throw mioe;
		ui.displayMessage(this, UserInterface.M_NORMAL, "  All tests passed");
    }
	
	private boolean chosen(String testName) {
		return (chosenTest == null || chosenTest.equals(testName));
	}
	
	private String formatRangeMessage(String type, int channel, short found, short expected, int tolerance) {
		String op = TestLib.CH_MEMBEROF;
		if (Math.abs(expected - found) > tolerance) op = TestLib.CH_NOTMEMBEROF;
		
		return String.format("%s Channel %,d: %s%,d [%,d %s (%,d +/- %,d)]", type, channel, TestLib.CH_DELTA, found - expected, found, op, expected, tolerance);
	}
}

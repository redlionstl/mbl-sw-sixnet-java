/*
 * UBootUpdater.java
 *
 * Provides an interface by which a user may update UBoot in a device.
 *
 * Jonathan Pearson
 * June 26, 2009
 *
 */

package com.sixnetio.bootload;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.SocketException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import com.sixnetio.COM.UBootCommunicator;
import com.sixnetio.GUI.Suicide;
import com.sixnetio.uboot.UBootLoader;
import com.sixnetio.io.INIParser;
import com.sixnetio.io.UnbufferedInputStream;
import com.sixnetio.net.IfcUtil;

/**
 * <p>
 * Provides an interface (both GUI and CLI) by which a user may update UBoot in
 * a device. The UBoot image should be placed in the top-level Java package
 * location and named "data/uboot.img". There should also be a file called
 * "data/uboot.ini" in the same place, which contains information about memory
 * addresses for the load.
 * </p>
 * 
 * <p>
 * Here is an example uboot.ini:<br />
 * 
 * <pre>
 * [UBoot]
 * offset = 0x50000
 * length = 0x30000
 * 
 * [Verify]
 * varname = regex
 * </pre>
 * 
 * In the verify section, specify any variables that need to have specific
 * values to identify a compatible product for updating. Provide regular
 * expressions to match their values.
 * </p>
 * 
 * <p>
 * The user is responsible for providing the serial port and local IP.
 * </p>
 *
 * @author Jonathan Pearson
 */
public class UBootUpdater extends JFrame implements UBootLoader.UBootLoaderUI {
    private static final String PROG_VERSION = "0.0.1x";
	
    private static boolean gui = true;
    private static UBootLoader loader = new UBootLoader();
    private static Map<String, String> verifications = new HashMap<String, String>();
	
    private static void usage(PrintStream out) {
        out.println("UBootUpdater v. " + PROG_VERSION + " (c) 2009 SIXNET, LLC");
		
        out.println("Usage: UBootUpdater [-n] [-l <local IP>] [-s <serial port>]");
        out.println(" -n  Run in the command line, do not display any windows");
        out.println(" -l  Provide the IP address of the local machine; default is " + loader.getLocalAddress());
        out.println(" -s  Provide the serial port to use; default is " + loader.getSerialPort());
    }
	
    public static void main(String[] args) {
        // Load the image and ini files before bothering with arguments, to be sure we have them
        URL imageURL = UBootUpdater.class.getResource("/data/uboot.img");
        URL iniURL = UBootUpdater.class.getResource("/data/uboot.ini");
		
        if (imageURL == null) {
            System.err.println("No '/data/uboot.img' found!");
            System.exit(1);
            return;
        }
		
        if (iniURL == null) {
            System.err.println("No /data/uboot.ini found!");
            System.exit(1);
            return;
        }
		
        // Parse the INI file
        INIParser parser;
        try {
            InputStream in = iniURL.openStream();
    		
            try {
                parser = new INIParser(in);
            } finally {
                in.close();
            }
        } catch (IOException ioe) {
            System.err.println("Unable to parse '/data/uboot.ini': " + ioe.getMessage());
            System.exit(1);
            return;
        }
		
        // Grab the NAND offset/length for updating UBoot
        Long offset = parser.getLongValue("UBoot", "offset", null);
        Integer length = parser.getIntValue("UBoot", "length", null);
		
        if (offset == null) {
            System.err.println("No 'offset' provided in section 'UBoot' of '/data/uboot.ini'");
            System.exit(1);
            return;
        }
		
        if (length == null) {
            System.err.println("No 'length' provided in section 'UBoot' of '/data/uboot.ini'");
            System.exit(1);
            return;
        }
		
        for (String varName : parser.getKeys("Verify")) {
            verifications.put(varName, parser.getValue("Verify", varName));
        }
		
        // Parse arguments
        try {
            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("-n")) {
                    gui = false;
                } else if (args[i].equals("-l")) {
                    loader.setLocalAddress(args[++i]);
                } else if (args[i].equals("-s")) {
                    loader.setSerialPort(args[++i]);
                } else if (args[i].equals("-h")) {
                    usage(System.out);
                    return;
                } else {
                    throw new Exception("Unrecognized option: " + args[i]);
                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            usage(System.err);
            System.exit(1);
            return;
        }
		
        UBootUpdater updater;
        try {
            updater = new UBootUpdater();
        } catch (SocketException se) {
            if (gui) {
                JOptionPane.showMessageDialog(null, "Unable to detect local IP addresses: " + se.getMessage());
            } else {
                // Souldn't happen, but we'll deal with it anyway
                System.err.println("Unable to detect local IP addresses: " + se.getMessage());
            }
	        
            System.exit(1);
            return;
        }
		
        if (!gui) {
            loader.loadUBoot(updater);
            System.out.println();
            System.out.println("Update complete.");
        }
    }
	
    private JProgressBar progressBar;
	
    public UBootUpdater() throws SocketException {
        super("UBoot Updater");
		
        if (!gui) {
            // Nothing to do
            return;
        }
		
        // Set up the window
        setLayout(new BorderLayout());
		
        progressBar = new JProgressBar();
        final JComboBox cmbIP = new JComboBox(IfcUtil.discoverLocalAddresses(false).keySet().toArray());
        final JTextField txtSerial = new JTextField(10);
        final JButton btnGo = new JButton("Go");
		
        addWindowListener(new WindowAdapter() {
                @Override
                    public void windowClosing(WindowEvent e) {
                    if (!btnGo.isEnabled()) {
                        // In the middle of a load, confirm with the user
                        int response = JOptionPane.showConfirmDialog(UBootUpdater.this, "An update is in progress. Canceling the update could leave your device in an irrepairable state. Are you sure that you want to cancel?", "Really Quit?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
					
                        if (response == JOptionPane.NO_OPTION) {
                            // User said don't quit
                            return;
                        }
                    }
				
                    // Quit
                    setVisible(false);
                    dispose();
                    new Suicide();
                }
            });
		
        // Top: IP address and serial port selection
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout());
            add(panel, BorderLayout.NORTH);
			
            txtSerial.setText(loader.getSerialPort());
			
            panel.add(new JLabel("Local IP:"));
            panel.add(cmbIP);
			
            panel.add(new JLabel("Serial Port:"));
            panel.add(txtSerial);
        }
		
        // Bottom: Progress bar and 'Go' button
        {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout());
            add(panel, BorderLayout.SOUTH);
			
            progressBar.setMinimum(0);
            progressBar.setMaximum(1000);
            progressBar.setValue(0);
			
            panel.add(progressBar);
            panel.add(btnGo);
        }
		
        // Add the action listener for the Go button
        btnGo.addActionListener(new ActionListener() {
                @Override
                    public void actionPerformed(ActionEvent e) {
                    btnGo.setEnabled(false);
				
	            loader.setLocalAddress(cmbIP.getSelectedItem().toString());
	            loader.setSerialPort(txtSerial.getText());
	            
	            progressBar.setIndeterminate(true);
	            
	            Thread th = new Thread(new Runnable() {
                            @Override
                                public void run() {
                                loader.loadUBoot(UBootUpdater.this);
                                btnGo.setEnabled(true);
                            }
                        });
	            
	            th.start();
                }
            });
		
        SwingUtilities.invokeLater(new Runnable() {
                @Override
                    public void run() {
                    pack();
                    setVisible(true);
                }
            });
    }
	
    @Override
        public void handleException(final Exception e) {
        if (gui) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
	            	@Override
                            public void run() {
                            JOptionPane.showMessageDialog(UBootUpdater.this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	                	
                            progressBar.setValue(0);
                            progressBar.setIndeterminate(false);
	                }
	            });
            } catch (InterruptedException ex) {
                // Ignore it, not much we can do and we aren't interrupting the thread
            } catch (InvocationTargetException ex) {
            	// Ignore it, it shouldn't happen
            }
        } else {
            System.err.println(e.getMessage());
        }
    }

    @Override
        public void powerOff() {
        if (gui) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
	            	@Override
                            public void run() {
                            JOptionPane.showMessageDialog(UBootUpdater.this, "Please disconnect power from the device.", "Power", JOptionPane.WARNING_MESSAGE);
	                }
	            });
            } catch (InterruptedException ex) {
            	// Ignore it, nothing we can do and we don't interrupt threads here
            } catch (InvocationTargetException ex) {
            	// Ignore it, it shouldn't happen
            }
        } else {
            System.out.println("Please disconnect power from the device and press enter.");
            UnbufferedInputStream in = new UnbufferedInputStream(System.in);
	    	
            try {
                in.readLine();
            } catch (IOException ioe) {
                System.err.println("Unable to read from stdin, aborting: " + ioe.getMessage());
                System.exit(1);
            }
        }
    }

    @Override
        public void powerOn() {
        if (gui) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
	            	@Override
                            public void run() {
                            JOptionPane.showMessageDialog(UBootUpdater.this, "Please connect power to the deice.", "Power", JOptionPane.WARNING_MESSAGE);
	                }
	            });
            } catch (InterruptedException ex) {
                // Ignore it, we don't interrupt threads
            } catch (InvocationTargetException ex) {
                // Ignore it, it shouldn't happen
            }
        } else {
            System.out.println("Please connect power to the device.");
        }
    }

    @Override
        public boolean verifyDevice(UBootCommunicator uc) throws IOException {
        // Search for each verification variable and see if the regex matches
        // Mismatch/missing variable = return false
        for (Map.Entry<String, String> entry : verifications.entrySet()) {
            // If the variable does not exist, this will throw an IOException
            // Since that works just as well as returning false... Just let it go
            String varValue = uc.getVariable(entry.getKey());
			
            if (!varValue.matches(entry.getValue())) {
                return false;
            }
        }
		
        return true;
    }

    @Override
        public void updateProgress(final float percent) {
        if (gui) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
	            	@Override
                            public void run() {
                            progressBar.setValue((int)(percent * 1000));
                            progressBar.setIndeterminate(false);
	                }
	            });
            } catch (InterruptedException ex) {
                // Ignore it, it's not an important update
            } catch (InvocationTargetException ex) {
                // Ignore it, it shouldn't happen
            }
        } else {
            System.out.printf("\rProgress: %.1f%%", percent * 100.0f);
        }
    }
}

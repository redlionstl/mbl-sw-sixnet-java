/*
 * TarballView.java
 *
 * Displays the files and contents of those files for a single tarball.
 *
 * Jonathan Pearson
 * December 5, 2008
 *
 */

package com.sixnetio.ConfigDiff;

import java.awt.BorderLayout;
import java.awt.Font;
import java.io.File;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.Position;

import com.sixnetio.fs.generic.*;
import com.sixnetio.fs.tar.Tarball;
import com.sixnetio.util.Utils;

public class TarballView extends JPanel {
	private static class TarredFileDisplayer {
		public final FSObject tarredFile;
		
		public TarredFileDisplayer(FSObject tarredFile) {
			this.tarredFile = tarredFile;
		}
		
		// FSObject.toString() returns types and paths, but we only want file
		// names
		@Override
		public String toString() {
			//return tarredFile.getFileName();
			return tarredFile.getName();
		}
	}
	
	private ConfigDiff owner;
	private String name;
	private File localFile;
	private Tarball tarball;
	private JList fileList;
	private JEditorPane fileContents;
	private JLabel lblStatus;
	
	private FSObject selectedFile;
	private boolean dataChanged = false; // Set to true when the data changes, set to false when saved
	
	public TarballView(ConfigDiff owner, String name, File localFile, Tarball tarball) {
		this.owner = owner;
		this.name = name;
		this.localFile = localFile;
		this.tarball = tarball;
		
		fileList = makeFileList(tarball);
		
		// Make sure only one selection at a time is allowed
		fileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		// Create a new, empty editor pane
		fileContents = new JEditorPane("text/plain", "");
		fileContents.setFont(new Font("Courier New", Font.PLAIN, 12));
		fileContents.setEditable(true);
		
		// Create a status bar
		lblStatus = new JLabel("");
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);
		
		// Select the first file in the list and display its contents in the editor pane
		fileList.setSelectedIndex(0);
		selectedFile = ((TarredFileDisplayer)this.fileList.getSelectedValue()).tarredFile;
		
		fileContents.setText(getFileContents(selectedFile));
		
		if (selectedFile instanceof com.sixnetio.fs.generic.File) {
			fileContents.setEditable(true);
		}
		else {
			// Only files are editable
			fileContents.setEditable(false);
		}
		
		// Set up a listener on the file list so we can see when the user changes something
		fileList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
            	// Grab the newly selected displayer
            	TarredFileDisplayer newlySelectedDisplayer = (TarredFileDisplayer)fileList.getSelectedValue();
            	
            	// Pass the name of the file from that displayer down to activate()
            	activate(newlySelectedDisplayer.toString(), 0);
            }
		});
		
		// Set up a listener on the file contents pane to track cursor movements
		this.fileContents.addCaretListener(new CaretListener() {
            public void caretUpdate(CaretEvent e) {
            	int lineNumber = getLineNumber(e.getDot());
            	int column = e.getDot() - getDotPosition(lineNumber);
            	
            	String text = String.format("Line: %,d; Col: %,d", lineNumber + 1, column + 1);
            	
            	if (e.getMark() != e.getDot()) {
            		text += String.format("; Sel: %,d ch", Math.abs(e.getMark() - e.getDot()));
            	}
            	
            	lblStatus.setText(text);
            }
		});
		
		// Lay out this panel
		setLayout(new BorderLayout());
		
		JPanel rightSide = new JPanel();
		rightSide.setLayout(new BorderLayout());
		rightSide.add(new JScrollPane(this.fileContents), BorderLayout.CENTER);
		rightSide.add(lblStatus, BorderLayout.SOUTH);
		
		add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, fileList, rightSide), BorderLayout.CENTER);
	}
	
	private static JList makeFileList(Tarball tarball) {
		// Separate the files from the directories
		Directory root = tarball.getRoot();
		List<FSObject> fileList = new LinkedList<FSObject>();
		
		for (FSObject obj : root.getAllObjects()) {
			if ( ! (obj instanceof Directory)) {
				fileList.add(obj);
			}
		}
		
		// Make that into an array
		FSObject[] files = fileList.toArray(new FSObject[fileList.size()]);
		
		// Make an array of TarredFileDisplayer objects to actually throw into the list
		TarredFileDisplayer[] displayers = new TarredFileDisplayer[files.length];
		for (int i = 0; i < files.length; i++) {
			displayers[i] = new TarredFileDisplayer(files[i]);
		}
		
		// Sort by displayed name
		Arrays.sort(displayers, new Comparator<TarredFileDisplayer>() {
            public int compare(TarredFileDisplayer o1, TarredFileDisplayer o2) {
            	return o1.toString().compareTo(o2.toString());
            }
		});
		
		// Base the list on that
		return new JList(displayers);
	}
	
	public Tarball getTarball() {
		return tarball;
	}
	
	public void activate(String fileName, int line) {
		// Figure out where that file is in the list
		int position = fileList.getNextMatch(fileName, 0, Position.Bias.Forward);
		if (position == -1) throw new IllegalArgumentException("File not found in list: " + fileName);
		
		fileList.setSelectedIndex(position);
		
		FSObject newlySelectedFile =
			((TarredFileDisplayer)fileList.getModel().getElementAt(position)).tarredFile;
    	
    	// If the user really did choose something new
    	if (newlySelectedFile != selectedFile) {
    		// Make sure the tarball is up-to-date
    		updateTarball();
    		
    		// Select the new file
    		selectedFile = newlySelectedFile;
    		
        	// Display the new file data
        	fileContents.setText(getFileContents(selectedFile));
        	
        	if (selectedFile instanceof com.sixnetio.fs.generic.File) {
        		fileContents.setEditable(true);
        	}
        	else {
        		// Only files are editable
        		fileContents.setEditable(false);
        	}
    	}
    	
    	// Figure out the cursor position necessary to hit the specified line
    	int dotPosition = getDotPosition(line);
    	
    	// Jump to that position
    	this.fileContents.requestFocusInWindow();
    	this.fileContents.getCaret().setDot(dotPosition);
	}
	
	public boolean getChanged() {
		return dataChanged;
	}
	
	public void clearChanged() {
		dataChanged = false;
		
		owner.updateName(this, getName());
	}
	
	@Override
    public String getName() {
		return name;
	}
	
	@Override
    public void setName(String name) {
		this.name = name;
	}
	
	@Override
    public String toString() {
		return ((getChanged() ? "*" : "") + getName());
	}
	
	public File getLocalFile() {
		return localFile;
	}
	
	public void setLocalFile(File file) {
		this.localFile = file;
	}
	
	public void updateTarball() {
		// Updates in the fileContents pane are put back into the tarball
		// Make sure we really need to do the update, though
		byte[] data = fileContents.getText().getBytes();
		
		// If the contents of the previously chosen file has changed
		if (selectedFile instanceof com.sixnetio.fs.generic.File) {
			if ( ! Arrays.equals(data,
			                     ((com.sixnetio.fs.generic.File)selectedFile).getData())) {
				// Save the old data
				((com.sixnetio.fs.generic.File)selectedFile).setData(data);
				
				// Mark this view as having been changed
				dataChanged = true;
				owner.updateName(this, getName());
			}
		}
	}
	
	// Translate a line number into the dot position to hit the beginning of that line
	private int getDotPosition(int line) {
		String[] lines = Utils.getLines(fileContents.getText());
    	int dotPosition = 0;
    	for (int i = 0; i < line; i++) {
    		dotPosition += lines[i].length() + 1; // Add one for the newline
    	}
    	
    	return dotPosition;
	}
	
	// Translate a dot position into the line number containing it
	private int getLineNumber(int dot) {
		String[] lines = Utils.getLines(fileContents.getText());
		int line = 0;
		while (dot > 0) {
			dot -= lines[line++].length() + 1; // Add one for the newline
		}
		
		if (dot < 0) line--;
		
		return line;
	}
	
	private String getFileContents(FSObject obj)
	{
		// Directories have been excluded
		if (selectedFile instanceof com.sixnetio.fs.generic.File) {
			return new String(((com.sixnetio.fs.generic.File)selectedFile).getData());
		}
		else if (selectedFile instanceof Symlink) {
			return "Symlink Target: " + ((Symlink)selectedFile).getLinkName();
		}
		else if (selectedFile instanceof FIFO) {
			return "(FIFO)";
		}
		else if (selectedFile instanceof Device) {
			return ("Major: " + ((Device)selectedFile).getMajor() + "\n" +
			        "Minor: " + ((Device)selectedFile).getMinor());
		}
		
		return "";
	}
}

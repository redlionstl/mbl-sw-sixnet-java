/*
 * TarballDiffView.java
 *
 * Displays the differences found between two tarballs.
 *
 * Jonathan Pearson
 * December 5, 2008
 *
 */

package com.sixnetio.ConfigDiff;

import java.awt.BorderLayout;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

import com.sixnetio.fs.tar.TarballDifferenceGenerator.Difference;

public class TarballDiffView extends JPanel {
	private static class DiffFileDisplayer {
		public final String path; // Key into differencesByFile map
		public final String name; // So we don't need to substring every path
		
		public DiffFileDisplayer(String path) {
			this.path = path;
			
			if (path.indexOf('/') != -1) {
				this.name = path.substring(path.lastIndexOf('/') + 1);
			} else {
				this.name = path;
			}
		}
		
		@Override
        public String toString() {
			//return name;
			return path;
		}
	}
	
	private class DifferenceLinkListener implements HyperlinkListener {
        public void hyperlinkUpdate(HyperlinkEvent e) {
        	if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
        		// Cut off the protocol ('tardiff://')
    			String address = e.getDescription().substring(e.getDescription().indexOf("://") + 3);
    			
    			// Tab name is the 'host name' portion, up to the first '/'
    			String tabName = address.substring(0, address.indexOf('/'));
    			address = address.substring(tabName.length() + 1); // Cut off the '/' also
    			
    			// File name is the 'path' portion, up to the last '/'
    			String fileName = address.substring(0, address.lastIndexOf('/'));
    			address = address.substring(fileName.length() + 1); // Cut off the '/' also
    			
    			// Line number is the 'file name' portion, after the last '/'
    			int lineNumber = Integer.parseInt(address); // All that's left
    			
    			TarballView tbv = owner.getTarballView(tabName);
    			if (tbv == null) {
    				JOptionPane.showMessageDialog(owner, "That view is no longer open", "Error", JOptionPane.WARNING_MESSAGE);
    				return;
    			}
    			
    			// Switch to the tab and activate the file at the proper line
    			owner.activateTab(tbv);
    			tbv.activate(fileName, lineNumber);
        	}
        }
	}
	
	private ConfigDiff owner;
	private JList fileList;
	private JEditorPane fileContents;
	
	// Read-only after construction
	private Map<String, Difference> differencesByFile;
	
	private String selectedPath;
	
	public TarballDiffView(ConfigDiff owner, List<Difference> differences) {
		this.owner = owner;
		
		// Populate the map with files containing differences pointing to the actual difference(s)
		// At the same time, create an array of DiffFileDisplayer objects to show in the list
		differencesByFile = new HashMap<String, Difference>();
		
		DiffFileDisplayer[] displayers;
		synchronized (differences) {
    		displayers = new DiffFileDisplayer[differences.size()];
    		int index = 0;
    		for (Difference diff : differences) {
    			differencesByFile.put(diff.path, diff);
    			
    			displayers[index++] = new DiffFileDisplayer(diff.path);
    		}
		}
		
		// Sort the array by displayed name
		Arrays.sort(displayers, new Comparator<DiffFileDisplayer>() {
            public int compare(DiffFileDisplayer o1, DiffFileDisplayer o2) {
            	return o1.toString().compareTo(o2.toString());
            }
		});
		
		// Construct the list of different files
		fileList = new JList(displayers);
		
		// Create the file contents editor pane
		fileContents = new JEditorPane("text/html", "");
		fileContents.addHyperlinkListener(new DifferenceLinkListener());
		fileContents.setEditable(false);
		
		// Select the first item in the list
		fileList.setSelectedIndex(0);
		
		// Populate the differences for that item
		populateDifferences(fileList.getSelectedIndex());
		
		// Add a listener to the file list so we can display other files as the user clicks them
		fileList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
            	// Pass the index to display to populateDifferences
            	populateDifferences(fileList.getSelectedIndex());
            }
		});
		
		// Set up the layout
		setLayout(new BorderLayout());
		
		add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true,
		                   fileList, new JScrollPane(fileContents)),
		    BorderLayout.CENTER);
	}
	
	private void populateDifferences(int index) {
		// Grab the relevant object
		DiffFileDisplayer displayer = (DiffFileDisplayer)fileList.getModel().getElementAt(index);
		
		// Get the differences for that file
		Difference diff = differencesByFile.get(displayer.path);
		
		// Get the names of the two tarballs
		String name1 = owner.getTarballView(diff.tb1).getName();
		String name2 = owner.getTarballView(diff.tb2).getName();
		
		String fileName = diff.path;
		
		/*
		if (fileName.indexOf('/') != -1) {
			fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
		}
		*/
		
		// Build the HTML content in here
		StringBuilder html = new StringBuilder();
		
		html.append("<html><body style='font: 12pt \"Courier New\"'>");
		
		// Display the differences in the editor pane, in color, with hyperlinks to jump to other views
		switch (diff.type) {
			case Added:
				html.append(String.format("<p>Missing from '%s'</p>", name1));
				html.append(String.format("<p>Exists in '<a href='tardiff://%s/%s/0'>%s</a>'</p>", name2, diff.path, name2));
				break;
			case ContentsChanged:
				// Generate a table of lines
				
				// Figure out where the last line of each file is
				// Last line of the first file will be the last '-' or '='
				// Last line of the second file will be the last '+' or '='
				int lastLine1 = 0;
				int lastLine2 = 0;
				for (int i = 0; i < diff.lineDiff.length; i++) {
					if (diff.lineDiff[i].startsWith("-")) {
						lastLine1 = i;
					} else if (diff.lineDiff[i].startsWith("+")) {
						lastLine2 = i;
					} else {
						lastLine1 = lastLine2 = i;
					}
				}
				
				html.append(String.format("<p>File 1: '<a href='tardiff://%s/%s/0'>%s</a>'</p>",
				                          name1, fileName, name1));
				html.append(String.format("<p>File 2: '<a href='tardiff://%s/%s/0'>%s</a>'</p>",
				                          name2, fileName, name2));
				
				html.append("<p>");
				html.append("<table border='1'>");
				html.append("<tr>");
				html.append("<td>Line of 1</td>");
				html.append("<td>Line of 2</td>");
				html.append("<td width='*'>Text</td>");
				html.append("</tr>");
				
				for (int i = 0; i < diff.lineDiff.length; i++) {
					String lineDiff = diff.lineDiff[i];
					String line = lineDiff.substring(lineDiff.indexOf("> ") + 2);
					
					int line1 = Integer.parseInt(lineDiff.substring(1, lineDiff.indexOf(',')));
					int line2 = Integer.parseInt(lineDiff.substring(
					    lineDiff.indexOf(',') + 1, lineDiff.indexOf('>')));
					
					html.append("<tr>");
					
					if (lineDiff.startsWith("+")) {
						// Added to second (removed from first)
						html.append("<td style='background: #ffaaaa'>");
					} else if (lineDiff.startsWith("-")) {
						// Removed from second (added to first)
						html.append("<td style='background: #aaffaa'>");
					} else {
						// Equal on both sides
						html.append("<td>");
					}
					
					if ((line1 == 0 || line1 >= lastLine1) && lineDiff.startsWith("+")) {
						// Prepended/appended to the file, don't display a line number here
						html.append("--</td>");
					} else {
						html.append(String.format("<a href='tardiff://%s/%s/%d'>%d</a></td>",
						                          name1, fileName, line1, line1 + 1));
					}
					
					if (lineDiff.startsWith("+")) {
						// Added to second
						html.append("<td style='background: #aaffaa'>");
					} else if (lineDiff.startsWith("-")) {
						// Removed from second
						html.append("<td style='background: #ffaaaa'>");
					} else {
						// Equal on both sides
						html.append("<td>");
					}
					
					if ((line2 == 0 || line2 >= lastLine2) && lineDiff.startsWith("-")) {
						// Removed from the beginning/end of the file, don't display a line number here
						html.append("--</td>");
					} else {
						html.append(String.format("<a href='tardiff://%s/%s/%d'>%d</a></td>",
						                          name2, fileName, line2, line2 + 1));
					}
					
					// Text of the line in the same color as the second cell
					if (lineDiff.startsWith("+")) {
						html.append("<td style='background: #aaffaa'>");
					} else if (lineDiff.startsWith("-")) {
						html.append("<td style='background: #ffaaaa'>");
					} else {
						html.append("<td>");
					}
					
					html.append(line + "</td>");
					
					html.append("</tr>");
				}
				
				html.append("</table>");
				html.append("</p>");
				break;
			case Deleted:
				html.append(String.format("<p>Exists in '<a href='tardiff://%s/%s/0'>%s</a>'</p>", name1, diff.path, name1));
				html.append(String.format("<p>Missing from '%s'</p>", name2));
				break;
			case DeviceChanged:
				html.append(String.format("<p>Device numbers changed from '%s' in '<a href='tardiff://%s/%s/0'>%s</a>'</p>", diff.lineDiff[0], name1, diff.path, name1));
				html.append(String.format("<p>Device numbers changed to '%s' in '<a href='tardiff://%s/%s/0'>%s</a>'</p>", diff.lineDiff[1], name2, diff.path, name2));
				break;
			case TargetChanged:
				html.append(String.format("<p>Link target changed from '%s' in '<a href='tardiff://%s/%s/0'>%s</a>'</p>", diff.lineDiff[0], name1, diff.path, name1));
				html.append(String.format("<p>Link target changed to '%s' in '<a href='tardiff://%s/%s/0'>%s</a>'</p>", diff.lineDiff[1], name2, diff.path, name2));
				break;
			case TypeChanged:
				html.append(String.format("<p>Type changed from '%s' in '<a href='tardiff://%s/%s/0'>%s</a>'</p>", diff.lineDiff[0], name1, diff.path, name1));
				html.append(String.format("<p>Type changed to '%s' in '<a href='tardiff://%s/%s/0'>%s</a>'</p>", diff.lineDiff[1], name2, diff.path, name2));
				break;
			
		}
		
		html.append("</body></html>");
		
		fileContents.setText(html.toString());
		fileContents.getCaret().setDot(0); // Bring the view to the top
		fileContents.requestFocusInWindow();
	}
	
	@Override
    public String toString() {
		return "Differences";
	}
}

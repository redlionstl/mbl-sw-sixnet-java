/*
 * Main.java
 *
 * Entry point for ConfigDiff program.
 *
 * Jonathan Pearson
 * December 8, 2008
 *
 */

package com.sixnetio.ConfigDiff;

public class Main {
	public static void main(String[] args) {
		new ConfigDiff();
	}
}

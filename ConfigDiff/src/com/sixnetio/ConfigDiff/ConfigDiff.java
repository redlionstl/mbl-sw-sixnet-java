/*
 * ConfigDiff.java
 *
 * Allows for viewing and editing of multiple configuration checkpoints,
 * as well as viewing of differences between those checkpoints (actually,
 * this can be used for any tarballs, but its main use will be for
 * configuration checkpoints).
 *
 * Jonathan Pearson
 * December 5, 2008
 *
 */

package com.sixnetio.ConfigDiff;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;

import com.sixnetio.GUI.*;
import com.sixnetio.Switch.SwitchConfig;
import com.sixnetio.Switch.WebUI;
import com.sixnetio.fs.tar.*;

public class ConfigDiff extends JFrame {
	public static final String PROG_VERSION = "0.1.5 Alpha";
	
	// Must contain at least one
	// First is the default when saving
	private static final String[] ACCEPTED_EXTENSIONS = {
		".tgz", ".tar.gz", ".tar"
	};
	
	private JTabbedPane viewTabs;
	
	private JFileChooser fileChooser = new JFileChooser(); // To retain the current directory between calls
	
	public ConfigDiff() {
		super("Configuration Editor/Difference Viewer v" + PROG_VERSION);
		
		// Common properties on the file chooser
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.addChoosableFileFilter(new FileFilter() {
            public boolean accept(File f) {
	            if (f.isDirectory()) return true;
	            
	            for (String extension : ACCEPTED_EXTENSIONS) {
	            	if (f.getName().endsWith(extension)) return true;
	            }
	            
	            return false;
            }
            
            public String getDescription() {
	            return "Configuration Checkpoints";
            }
		});
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		
		// Set up the window close handler
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
		
		// Keep the preferred size equal to the current size when resizing
		// This will prevent extraneous pack() calls from resizing the window
		addHierarchyBoundsListener(new HierarchyBoundsAdapter() {
			public void ancestorResized(HierarchyEvent e) {
				setPreferredSize(getSize());
			}
		});
		
		// Set up the menu
		buildMenu();
		
		// Set up an empty tab view
		viewTabs = new JTabbedPane();
		
		// Add a listener to close tabs on middle-click
		viewTabs.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) { }
            public void mouseEntered(MouseEvent e) { }
            public void mouseExited(MouseEvent e) { }
            public void mouseReleased(MouseEvent e) { }
            
            public void mousePressed(MouseEvent e) {
            	if (e.getButton() == MouseEvent.BUTTON2) {
            		closeActiveTab();
            	}
            }
		});
		
		// Put together the window
		setLayout(new BorderLayout());
		add(viewTabs, BorderLayout.CENTER);
		
		// Display the window
		setPreferredSize(new Dimension(600, 400));
		pack();
		setVisible(true);
	}
	
	public void close() {
		// Loop through open files checking for changes
		for (int i = 0; i < viewTabs.getComponentCount(); i++) {
			JPanel panel = (JPanel)viewTabs.getComponentAt(i);
			
			if (panel instanceof TarballView) {
				TarballView tbv = (TarballView)panel;
				tbv.updateTarball();
				
				if (tbv.getChanged()) {
					FourButtonDialog dlg = new FourButtonDialog("Save?",
					                                            "Contents of file '" + tbv.getName() + "' has changed, save?",
					                                            "Save to File",
					                                            "Export to Switch",
					                                            "Discard Changes",
					                                            "Cancel");
					int choice = dlg.showDialog();
					
					switch (choice) {
						case FourButtonDialog.OPTION1:
							if (!saveFile(tbv)) return;
							break;
						case FourButtonDialog.OPTION2:
							if (!exportFile(tbv)) return;
							break;
						case FourButtonDialog.OPTION3:
							continue;
						case FourButtonDialog.CANCELED:
							return;
					}
				}
			}
		}
		
		System.exit(0);
	}
	
	private void buildMenu() {
		JMenuBar bar = new JMenuBar();
		
		JMenu mnuFile = new JMenu("File");
		mnuFile.setMnemonic(KeyEvent.VK_F);
		bar.add(mnuFile);
		
		JMenuItem mnuFileOpen = new JMenuItem("Open...");
		mnuFileOpen.setMnemonic(KeyEvent.VK_O);
		mnuFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
		mnuFileOpen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	openFile();
            }
		});
		mnuFile.add(mnuFileOpen);
		
		JMenuItem mnuFileImport = new JMenuItem("Import...");
		mnuFileImport.setMnemonic(KeyEvent.VK_I);
		mnuFileImport.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));
		mnuFileImport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				importFile();
			}
		});
		mnuFile.add(mnuFileImport);
		
		mnuFile.addSeparator();
		
		JMenuItem mnuFileSave = new JMenuItem("Save");
		mnuFileSave.setMnemonic(KeyEvent.VK_S);
		mnuFileSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		mnuFileSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveFile();
			}
		});
		mnuFile.add(mnuFileSave);
		
		JMenuItem mnuFileSaveAs = new JMenuItem("Save As...");
		mnuFileSaveAs.setMnemonic(KeyEvent.VK_A);
		mnuFileSaveAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveFileAs();
			}
		});
		mnuFile.add(mnuFileSaveAs);
		
		JMenuItem mnuFileExport = new JMenuItem("Export...");
		mnuFileExport.setMnemonic(KeyEvent.VK_E);
		mnuFileExport.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_DOWN_MASK));
		mnuFileExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exportFile();
			}
		});
		mnuFile.add(mnuFileExport);
		
		mnuFile.addSeparator();
		
		JMenuItem mnuFileClose = new JMenuItem("Close");
		mnuFileClose.setMnemonic(KeyEvent.VK_C);
		mnuFileClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
		mnuFileClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeActiveTab();
			}
		});
		mnuFile.add(mnuFileClose);
		
		mnuFile.addSeparator();
		
		JMenuItem mnuFileExit = new JMenuItem("Exit");
		mnuFileExit.setMnemonic(KeyEvent.VK_X);
		mnuFileExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		mnuFile.add(mnuFileExit);
		
		// View menu
		JMenu mnuView = new JMenu("View");
		mnuView.setMnemonic(KeyEvent.VK_V);
		bar.add(mnuView);
		
		JMenuItem mnuViewDiff = new JMenuItem("Differences");
		mnuViewDiff.setMnemonic(KeyEvent.VK_D);
		mnuViewDiff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generateDiffTab();
			}
		});
		mnuView.add(mnuViewDiff);
		
		setJMenuBar(bar);
	}
	
	private void openFile() {
		fileChooser.setDialogTitle("Open a Checkpoint");
		
		int choice = fileChooser.showOpenDialog(this);
		
		if (choice == JFileChooser.CANCEL_OPTION) return;
		
		// Attempt to open the file
		File f = fileChooser.getSelectedFile();
		
		int extIndex = 0;
		while (!f.isFile() && extIndex < ACCEPTED_EXTENSIONS.length) {
			f = new File(fileChooser.getSelectedFile().getAbsolutePath() + ACCEPTED_EXTENSIONS[extIndex++]);
		}
		
		if (!f.isFile()) {
			// None of the default extensions works, give up
			JOptionPane.showMessageDialog(this,
			                              "File '" + fileChooser.getSelectedFile().getAbsolutePath() + "' does not exist",
			                              "Error",
			                              JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		try {
			openFile(f);
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(this,
			                              "Unable to open '" + f.getAbsolutePath() + "': " + ioe.getMessage(),
			                              "Error",
			                              JOptionPane.WARNING_MESSAGE);
			return;
		}
	}
	
	public void openFile(File f) throws IOException {
		// Try to open it as a GZipped tarball
		Tarball tarball = null;
		try {
			FileInputStream in = new FileInputStream(f);
			try {
				tarball = new GZippedTarball(in);
			} finally {
				in.close();
			}
		} catch (IOException ioe) {
			// Not a gzipped tarball...
			// Try it as a regular tarball
			try {
				FileInputStream in = new FileInputStream(f);
				try {
					tarball = new Tarball(in);
				} finally {
					in.close();
				}
			} catch (IOException ioe2) {
				// Not a regular tarball either...
				throw new IOException("Not a tarball or gzipped tarball");
			}
		}
		
		addTab(f.getName(), f, tarball);
	}
	
	private void importFile() {
		LoginDialog dlg = new LoginDialog("Switch Login");
		dlg.setUsername("admin");
		dlg.setPassword("admin");
		dlg.setServer("10.2.0.1");
		
		int choice = dlg.showDialog();
		if (choice == LoginDialog.CANCEL_OPTION) return;
		
		Cursor cursor = getCursor();
		setCursor(new Cursor(Cursor.WAIT_CURSOR));
		
		try {
			importFile(dlg.getServer(), dlg.getUsername(), dlg.getPassword());
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(this, "Unable to import: " + ioe.getMessage());
			return;
		} finally {
			setCursor(cursor);
		}
	}
	
	public void importFile(String address, String userName, String password) throws IOException {
		SwitchConfig swcfg = WebUI.readSwitchConfig(address, userName, password);
        addTab("Config from " + address, null, swcfg);
	}
	
	private void addTab(String name, File localFile, Tarball tarball) {
		// At this point, 'tarball' exists and has been loaded
		// Create a new view for it
		TarballView view = new TarballView(this, name, localFile, tarball);
		
		// Add it to the tab panel
		viewTabs.add(view);
		
		// Select it
		viewTabs.setSelectedComponent(view);
		
		// Make sure there are no name collisions
		updateName(view, name);
		
		// Need to pack to get everything laid out properly, but do a setPreferredSize() first
		//   to keep from losing that
		setPreferredSize(getSize());
		
		// Also, if maximized, remember so we can restore that
		int state = getExtendedState();
		
		pack();
		
		setExtendedState(state);
	}
	
	private void saveFile() {
		JPanel panel = (JPanel)viewTabs.getSelectedComponent();
		
		if (panel instanceof TarballView) {
			TarballView tbv = (TarballView)panel;
			
			saveFile(tbv);
		}
	}
	
	private boolean saveFile(TarballView tbv) {
		if (tbv.getLocalFile() == null) {
			return saveFileAs(tbv);
		} else {
			try {
				tbv.updateTarball();
				
				saveFile(tbv.getLocalFile(), tbv.getTarball());
				
				tbv.clearChanged();
				
				return true;
    		} catch (IOException ioe) {
    			JOptionPane.showMessageDialog(this,
    			                              "Unable to save: " + ioe.getMessage(),
    			                              "Error",
    			                              JOptionPane.WARNING_MESSAGE);
    			return false;
    		}
		}
	}
	
	public void saveFile(File file, Tarball tarball) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		
		try {
			out.write(tarball.getData());
		} finally {
			out.close();
		}
	}
	
	private void saveFileAs() {
		JPanel panel = (JPanel)viewTabs.getSelectedComponent();
		
		if (panel instanceof TarballView) {
			TarballView tbv = (TarballView)panel;
			saveFileAs(tbv);
		}
	}
	
	private boolean saveFileAs(TarballView tbv) {
		if (tbv.getLocalFile() == null) {
			fileChooser.setSelectedFile(new File(fileChooser.getCurrentDirectory(), tbv.getName()));
		} else {
			fileChooser.setSelectedFile(tbv.getLocalFile());
		}
		
		fileChooser.setDialogTitle("Save a Checkpoint");
		
		boolean done = false;
		while (!done) {
    		int choice = fileChooser.showSaveDialog(this);
    		
    		if (choice == JFileChooser.CANCEL_OPTION) return false;
    		
    		File file = fileChooser.getSelectedFile();
    		if (file.getName().indexOf('.') == -1) {
    			file = new File(file.getAbsolutePath() + ACCEPTED_EXTENSIONS[0]);
    		}
    		
    		if (file.exists()) {
    			choice = JOptionPane.showConfirmDialog(this,
    			                                       "Overwrite file '" + file.getName() + "'?",
    			                                       "Overwrite?",
    			                                       JOptionPane.YES_NO_CANCEL_OPTION,
    			                                       JOptionPane.QUESTION_MESSAGE);
    			
    			if (choice == JOptionPane.YES_OPTION) {
    				done = true;
    			} else if (choice == JOptionPane.NO_OPTION) {
    				// Loop back around and try again
    				done = false;
    			} else {
    				// Canceled
    				return false;
    			}
    		} else {
    			done = true;
    		}
		}
		
		tbv.setLocalFile(fileChooser.getSelectedFile());
		
		try {
			tbv.updateTarball();
			
			saveFile(tbv.getLocalFile(), tbv.getTarball());
			
			// Update to match the new file name
			String name = tbv.getLocalFile().getName();
			
			updateName(tbv, name.substring(0, name.lastIndexOf('.')));
			
			return true;
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(this,
			                              "Unable to save: " + ioe.getMessage(),
			                              "Error",
			                              JOptionPane.WARNING_MESSAGE);
			return false;
		}
	}
	
	private void exportFile() {
		JPanel panel = (JPanel)viewTabs.getSelectedComponent();
		
		if (panel instanceof TarballView) {
			TarballView tbv = (TarballView)panel;
			
			exportFile(tbv);
		}
	}
	
	private boolean exportFile(TarballView tbv) {
		LoginDialog dlg = new LoginDialog("Switch Login");
		dlg.setUsername("admin");
		dlg.setPassword("admin");
		dlg.setServer("10.2.0.1");
		
		int choice = dlg.showDialog();
		if (choice == LoginDialog.CANCEL_OPTION) return false;
		
		try {
			exportFile(dlg.getServer(), dlg.getUsername(), dlg.getPassword(), tbv.getTarball());
			
			// Update the view name to match the export
			updateName(tbv, "Config from " + dlg.getServer());
			tbv.setLocalFile(null);
			
			return true;
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(this, "Unable to export: " + ioe.getMessage());
			return false;
		}
	}
	
	public void exportFile(String server, String userName,
	                       String password, Tarball tarball) throws IOException {
		
		SwitchConfig config;
		
		if (tarball instanceof SwitchConfig) {
			config = (SwitchConfig)tarball;
		} else if (tarball instanceof GZippedTarball) {
			config = new SwitchConfig(System.currentTimeMillis(), tarball.getData());
		} else {
			// Plain old tarball
			GZippedTarball gz = new GZippedTarball(GZippedTarball.gzip(tarball.getData()));
			config = new SwitchConfig(System.currentTimeMillis(), gz.getData());
		}
		
		WebUI.writeSwitchConfig(config, server, userName, password);
	}
	
	private void generateDiffTab() {
		// Put together a list of the open tarballs, a list of the names of the tabs, and
		//   a map to go from one to the other
		List<Tarball> tarballs = new LinkedList<Tarball>();
		List<String> names = new Vector<String>();
		Map<String, Tarball> nameToTarball = new HashMap<String, Tarball>();
		
		// While looping, make sure every TarballView has written any changes back to its tarball
		for (int i = 0; i < viewTabs.getComponentCount(); i++) {
			JPanel panel = (JPanel)viewTabs.getComponentAt(i);
			
			if (panel instanceof TarballView) {
				TarballView tbv = (TarballView)panel;
				
				tbv.updateTarball();
				
				tarballs.add(tbv.getTarball());
				names.add(tbv.getName());
				nameToTarball.put(tbv.getName(), tbv.getTarball());
			}
		}
		
		if (tarballs.size() < 2) {
			JOptionPane.showMessageDialog(this,
			                              "Cannot generate differences without at least two open configuration checkpoints.",
			                              "Error",
			                              JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		// Need to ask the user which ones to diff (and which order)
		// For now, use a pair of ListChooserDialogs
		// FUTURE: Create a DoubleListChooserDialog so the user can choose both items at once
		Tarball tarball1, tarball2;
		
		ListChooserDialog dlg = new ListChooserDialog(names, "Choose the first tarball");
		int choice = dlg.showDialog();
		
		if (choice == ListChooserDialog.CANCEL_OPTION) {
			return;
		}
		
		tarball1 = nameToTarball.get((String)dlg.getChoice());
		
		// Remove that tarball as a choice and ask for the other
		names.remove((String)dlg.getChoice());
		
		dlg = new ListChooserDialog(names, "Choose the second tarball");
		choice = dlg.showDialog();
		
		if (choice == ListChooserDialog.CANCEL_OPTION) {
			return;
		}
		
		tarball2 = nameToTarball.get((String)dlg.getChoice());
		
		// Generate differences between the tarballs
		List<TarballDifferenceGenerator.Difference> differences =
			TarballDifferenceGenerator.compareTarballs(tarball1, tarball2);
		
		// Generate an output panel similar to tarball panels to display the diff
		JPanel diffView = new TarballDiffView(this, differences);
		viewTabs.add("Differences", diffView);
		
		// Select the new tab
		viewTabs.setSelectedComponent(diffView);
	}
	
	public TarballView getTarballView(Tarball tarball) {
		for (int i = 0; i < viewTabs.getComponentCount(); i++) {
			JPanel panel = (JPanel)viewTabs.getComponentAt(i);
			
			if (panel instanceof TarballView) {
				TarballView tbv = (TarballView)panel;
				
				if (tbv.getTarball() == tarball) {
					return tbv;
				}
			}
		}
		
		return null;
	}
	
	public TarballView getTarballView(String name) {
		for (int i = 0; i < viewTabs.getComponentCount(); i++) {
			JPanel panel = (JPanel)viewTabs.getComponentAt(i);
			
			if (panel instanceof TarballView) {
				TarballView tbv = (TarballView)panel;
				
				if (tbv.getName().equals(name)) {
					return tbv;
				}
			}
		}
		
		return null;
	}
	
	public void updateName(TarballView tbv, String nameRoot) {
		// Make sure there are no name collisions, since that would screw up diffing
		boolean succeeded = false;
		
		int counter = 0;
		
		while (!succeeded) {
			String name = nameRoot;
			if (counter > 0) name += " (" + counter + ")";
			
    		for (int i = 0; i < viewTabs.getComponentCount(); i++) {
    			JPanel panel = (JPanel)viewTabs.getComponentAt(i);
    			
    			if (panel == tbv) continue; // No point trying to match the current choice
    			
    			if (panel instanceof TarballView) {
    				TarballView tbv2 = (TarballView)panel;
    				
    				if (tbv2.getName().equals(name)) {
    					counter++;
    					succeeded = false;
    					break;
    				}
    			}
    		}
    		
    		succeeded = true;
		}
		
		if (counter > 0) nameRoot = nameRoot + " (" + counter + ")";
		tbv.setName(nameRoot);
		
		// Update the title of the tab
		for (int i = 0; i < viewTabs.getComponentCount(); i++) {
			if (viewTabs.getComponentAt(i) == tbv) {
				viewTabs.setTitleAt(i, tbv.toString());
				break;
			}
		}
	}
	
	public void activateTab(TarballView tbv) {
		viewTabs.setSelectedComponent(tbv);
	}
	
	public void closeActiveTab() {
		// Figure out which tab this is
		Component c = viewTabs.getSelectedComponent();
		
		if (c == null) {
			return;
		}
		
		if (c instanceof TarballView) {
			TarballView tbv = (TarballView)c;
			
			tbv.updateTarball();
			
			if (tbv.getChanged()) {
				FourButtonDialog dlg = new FourButtonDialog("Save?",
				                                            "Contents of file '" + tbv.getName() + "' has changed, save?",
				                                            "Save to File",
				                                            "Export to Switch",
				                                            "Discard Changes",
				                                            "Cancel");
				int choice = dlg.showDialog();
				
				switch (choice) {
					case FourButtonDialog.OPTION1:
						if (!saveFile(tbv)) return;
						break;
					case FourButtonDialog.OPTION2:
						if (!exportFile(tbv)) return;
						break;
					case FourButtonDialog.OPTION3:
						break;
					case FourButtonDialog.CANCELED:
						return;
				}
			}
		}
		
		viewTabs.remove(c);
	}
}

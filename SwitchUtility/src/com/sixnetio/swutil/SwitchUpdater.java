/*
 * SwitchUpdater.java
 *
 * Performs the Switch Utility specified update on a single switch.
 *
 * Jonathan Pearson
 * December 22, 2009
 *
 */

package com.sixnetio.swutil;

import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import com.sixnetio.COM.ComPort;
import com.sixnetio.COM.UBootCommunicator;
import com.sixnetio.Switch.*;
import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;


/**
 * Performs the Switch Utility specified update on a single switch. When started
 * as a thread, you should probably set an uncaught exception handler (see
 * {@link Thread#setUncaughtExceptionHandler(UncaughtExceptionHandler)}), as
 * this will throw descriptive {@link RuntimeException}s wrapping problem
 * causes when things go wrong.
 *
 * @author Jonathan Pearson
 */
class SwitchUpdater
    extends Thread
    implements FirmwareLoaderUI
{
    private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
    private final SwitchUtility program;
    private final SwitchUtilityUI ui;
    private final Switch swtch;
	
    private int lastState = 0;
	
    /**
     * Construct a new Switch Updater.
     * 
     * @param program The Switch Utility that knows all the details about what
     *   we are supposed to do.
     * @param ui The user interface to use for power cycling and whatnot.
     * @param swtch The switch to update.
     */
    public SwitchUpdater(SwitchUtility program, SwitchUtilityUI ui, Switch swtch)
    {
        super("Switch Updater for " + swtch.getSwitchIP());
        this.program = program;
        this.ui = ui;
        this.swtch = swtch;
    }
	
    /** Get the switch assigned to this updater. */
    public Switch getSwitch()
    {
        return swtch;
    }
	
    @Override
	public void run()
    {
        // Set up the serial loader before moving on, so we aren't spending
        // time while the switch is at the UBoot prompt
        SerialLoader loader = new SerialLoader(this,
                                               swtch.getSwitchIP(),
                                               swtch.getSwitchSubnet(),
                                               swtch.getSwitchGateway(),
                                               swtch.getSerialPath(),
                                               swtch.getBaudRate(),
                                               program.getIgnoreVendor());
        loader.setLoadBothSides(true);
		
        // Do not wait for responses - this is broken under
        // Windows 7 and some flavors of Windows XP. We don't
        // have time to fix it, and we also don't really
        // care. See #5000
        loader.setResponseTimeout(-1);

        // Open a UBoot communicator and catch UBoot
        logger.debug("Opening UBoot communicator on " + swtch.getSerialPath());
        UBootCommunicator uboot = new UBootCommunicator(swtch.getSerialPath(),
                                                        swtch.getBaudRate(),
                                                        8,
                                                        ComPort.PARITY_NONE,
                                                        ComPort.STOPBITS_1);
        // User can pass command line flag to have program
        // display the u-boot script sent to unit.
        uboot.scriptPrint = program.getShowScript();
		
        try {
            logger.debug("Catching UBoot");
            catchUBoot(uboot);
			
            // Before making ANY changes to the switch, make sure it's the right
            // kind
            if ( ! program.getIgnoreVendor()) {
                String vendor = loader.getSwitchVendor(uboot);
                String compareTo = program.getBundle().getFWVendor().toLowerCase();
                if ( ! vendor.equals(compareTo)) {
                    uboot.writeln("reset");

                    throw new IOException("Vendor mismatch");					
                }
            }

	    // Figure out large vs small page NAND
	    if (uboot.getNandSize() == 128) {
		program.setIsLargePage(true);
	    }

	    if (program.getIsLargePage()) {
		String version = program.getBundleVersion();
		// It may have an X on the end of it; remove that:
		version = version.replaceAll("X","");
		// It's not a string it's a regexp, hence the escaped dot.
		String[] parts = version.split("\\.");
		int major = Integer.parseInt(parts[0]);
		int minor = Integer.parseInt(parts[1]);
		int build = Integer.parseInt(parts[2]);

		// Minimum versions are 5.3.177 or 5.2.164.

		// Any major version after 5 is fine.
		if (!((major > 5) ||
		      // If the major version is 5, and the minor
		      // version is greater than 3, that's fine too.
		      ((major == 5) && (minor > 3)) ||
		      // If the version is 5.3, build 177 and later
		      // are also fine. If the version is 5.2, build
		      // 164 and later are also fine.
		      ((major == 5) && (minor == 3) && (build >= 177)) ||
		      ((major == 5) && (minor == 2) && (build >= 164)))) {
		    throw new IOException("Update failed. This switch requires at least firmware " +
					  "version 5.3.177 or 5.2.164 in order to function properly.");
		}
	    }
	    // else - it's small page. All versions work.

	    // Preliminary check - make sure this switch can use this firmware bundle

            // Always remap the flash partition layout, since there is no harm
            // in doing it, and it can prevent weird problems if the switch was
            // in an odd state that the customer didn't know about
            logger.debug("Remapping flash partition layout");
            remapFlash(uboot, loader);
        }
        catch (IOException ioe) {
            throw new RuntimeException("Error updating switch: " +
                                       ioe.getMessage());
        }
        finally {
            uboot.close();
        }
			
        // Perform a firmware load in the usual way
        logger.debug("Loading firmware");
        loader.loadFirmware(program.getBundle(), program.getShowScript());
		
        logger.debug("Done loading firmware");
    }
	
    private void remapFlash(UBootCommunicator uboot, SerialLoader loader)
    {
        // Which partition layout are we going with?
        int layoutVersion = swtch.getForcedLayout();
		
        if (layoutVersion == 0) {
            // Bundle specifies layout
            layoutVersion = program.getPartitionLayoutVersion();
        }
		
        try {
            int arch;
            String sxid;
            logger.debug("Remapping to layout version " + Integer.toString(layoutVersion));
            switch (layoutVersion) {
            case 1:
                // IPms only support one partitioning scheme - the factory one. So, don't go messing about with it.
                if (!program.bundleIsForIPm()) {
                    uboot.delVariable("layoutVersion");
                    uboot.delVariable("mrootargs.1");
                    uboot.delVariable("mrootargs.2");
                    uboot.delVariable("mtdparts.1");
                    uboot.delVariable("mtdparts.2");
                    uboot.delVariable("mtdparts.linux");
                    uboot.delVariable("prepareFallback");
                    uboot.delVariable("mrootargsFallback");
                    uboot.delVariable("mtdpartsFallback");
                    uboot.delVariable("activeInstall");
                    uboot.delVariable("alternateInstall");
                    uboot.delVariable("nextBoot");
                    uboot.delVariable("runningInstall");
                                                
                    uboot.setVariable("mrootargs", "set bootargs \\$(bootargs) root=/dev/mtdblock4 rootflags=noatime rootfstype=jffs2 ip=off");
                    uboot.setVariable("mtdparts", "mtdparts=mem.0:2M@4M(boot)\\;sxni9260.0:2M@512K(boot),-(root)");
                    // FIXME - the 20007fc0 should be a define.
                    uboot.setVariable("mtdboot", "run commonbootargs\\; run mrootargs\\; fsload 0x20007fc0 uImage\\; bootm");
                                                
                }
                // Just in case, but this should not be necessary
                loader.setApplyConfigImage(false);
					
                break;
            case 2:
            case 99:
                // Verify that this switch can have layout version 2
                // Only ARM9 switches support it
                arch = loader.getArch(uboot);
                if (arch != FirmwareBundle.T_ARCH_ARM9) {
                    throw new RuntimeException("The switch does not support this type of firmware");
                }
					
                // Additionally, the ET-9MS (SXID beginning with "0190_9MS-") 
                // does not - it's only on the solo_5ms prototype
                sxid = uboot.getVariable("sxid");
                if (sxid.startsWith("sxid=")) {
                    sxid = sxid.substring(5);
                }
					
                if (sxid.startsWith("0190_9MS")) {
                    throw new RuntimeException("The switch does not support this type of firmware");
                }
					
                uboot.setVariable("layoutVersion", Integer.toString(layoutVersion));
                uboot.setVariable("mrootargs.1", "set bootargs \\$(bootargs) root=/dev/mtdblock5 \\$(mtdparts.linux) rootflags=noatime rootfstype=jffs2 ip=off");
                if(layoutVersion == 2){
                    uboot.setVariable("mrootargs.2", "set bootargs \\$(bootargs) root=/dev/mtdblock7 \\$(mtdparts.linux) rootflags=noatime rootfstype=jffs2 ip=off");
                    // FIXME - the 512 here is how much space U-Boot takes up. I should be a constant, not a hardcoded number
                    uboot.setVariable("mtdparts.1",
                                      String.format("mtdparts=mem.0:%dK@4M(boot)\\;sxni9260.0:%dK@%dK(boot),%dK(root)",
                                                    program.getBootSize(),
                                                    program.getBootSize(),
                                                    512 + program.getConfigSize(),
                                                    program.getRootSize()));
                    // FIXME - Same as above.
                    uboot.setVariable("mtdparts.2",
                                      String.format("mtdparts=mem.0:%dK@4M(boot)\\;sxni9260.0:%dK@%dK(boot),%dK(root)",
                                                    program.getBootSize(),
                                                    program.getBootSize(),
                                                    512 + program.getConfigSize() + program.getBootSize() + program.getRootSize(),
                                                    program.getRootSize()));

		    if (program.getIsLargePage()) {
                    	uboot.setVariable("mtdparts.linux",
                                      String.format("mtdparts=sxni9260.0:256K@0(u-boot),128K(environment1),128K(environment2),%dK(config),%dK(boot1),%dK(root1),%dK(boot2),%dK(root2)",
                                                    program.getConfigSize(),
                                                    program.getBootSize(),
                                                    program.getRootSize(),
                                                    program.getBootSize(),
                                                    program.getRootSize()));
		    }
		    else {
                    	uboot.setVariable("mtdparts.linux",
                                      String.format("mtdparts=sxni9260.0:320K@0(u-boot),128K(environment),64K(reserved),%dK(config),%dK(boot1),%dK(root1),%dK(boot2),%dK(root2)",
                                                    program.getConfigSize(),
                                                    program.getBootSize(),
                                                    program.getRootSize(),
                                                    program.getBootSize(),
                                                    program.getRootSize()));
		    }
                }
                else {
                    // FIXME - the 512 here is how much space U-Boot takes up. I should be a constant, not a hardcoded number
                    // FIXME - root can be expanded with -(root).
                    // Note that this doesn't currently work because, later on, we read the partition size back out and need to parse a number there.
                    // the root size * 2 + bootsize is because we're going to expand it to fill the unused secondary boot and root images
                    uboot.setVariable("mtdparts.1",
                                      String.format("mtdparts=mem.0:%dK@4M(boot)\\;sxni9260.0:%dK@%dK(boot),%dK(root)",
                                                    program.getBootSize(),
                                                    program.getBootSize(),
                                                    512 + program.getConfigSize(),
                                                    program.getRootSize()*2 + program.getBootSize()));
		    if (program.getIsLargePage()) {
                        uboot.setVariable("mtdparts.linux",
                                      String.format("mtdparts=sxni9260.0:256K@0(u-boot),128K(environment1),128K(environment2),%dK(config),%dK(boot1),%dK(root1)",
                                                    program.getConfigSize(),
                                                    program.getBootSize(),
                                                    program.getRootSize()*2 + program.getBootSize()));
		    }
		    else {
                        uboot.setVariable("mtdparts.linux",
                                      String.format("mtdparts=sxni9260.0:320K@0(u-boot),128K(environment),64K(reserved),%dK(config),%dK(boot1),%dK(root1)",
                                                    program.getConfigSize(),
                                                    program.getBootSize(),
                                                    program.getRootSize()*2 + program.getBootSize()));
		    }
                }
					
                if(layoutVersion == 2){
                    // Prepare for software fallback
                    uboot.setVariable("mrootargsFallback", "setenv x setenv x setenv mrootargs \\\\\\$(mrootargs.\\$(alternateInstall))\\; run x\\; run x\\; setenv x\\;");
                    uboot.setVariable("mtdpartsFallback", "setenv x setenv x setenv mtdparts \\\\\\$(mtdparts.\\$(alternateInstall))\\; run x\\; run x\\; setenv x\\;");
                    uboot.setVariable("prepareFallback", "run.all mrootargsFallback\\; run.all mtdpartsFallback\\; setenv runningInstall \\$(nextBoot)\\; setenv nextBoot \\$(alternateInstall)\\; saveenv\\;");
                }
                else {
                    // this doesn't actually prepare a fallback for layout 99, but this is where
                    // it hooks in to set runninginstall, etc.
                    uboot.setVariable("prepareFallback", "setenv runningInstall \\$(nextBoot)\\; saveenv\\;");
                }
                // FIXME - the 20007fc0 should be a define.
                uboot.setVariable("mtdboot", "run commonbootargs\\; run mrootargs\\; fsload 0x20007fc0 uImage\\; run.all prepareFallback\\; bootm");

                uboot.setVariable("activeInstall", "1");
                if(layoutVersion == 2){
                    uboot.setVariable("alternateInstall", "2");
                }
                uboot.setVariable("nextBoot", "1");
                uboot.setVariable("mtdparts", "$(mtdparts.1)");
                uboot.setVariable("mrootargs", "$(mrootargs.1)");
					
                // Tell the loader to apply the config image, if it exists
                loader.setApplyConfigImage(true);
					
                break;
            case 3:
                // Verify that this switch can have layout version 3
                // Only ARM9 switches support it
                arch = loader.getArch(uboot);
                if (arch != FirmwareBundle.T_ARCH_ARM9) {
                    throw new RuntimeException("The switch does not support this type of firmware");
                }
					
                // Additionally, the ET-9MS does not (SXID beginning with
                // "0190_9MS-")
                sxid = uboot.getVariable("sxid");
                if (sxid.startsWith("sxid=")) {
                    sxid = sxid.substring(5);
                }
					
                if (sxid.startsWith("0190_9MS")) {
                    throw new RuntimeException("The switch does not support this type of firmware");
                }
					
                uboot.setVariable("layoutVersion", Integer.toString(layoutVersion));

                // clean up other layout versions
                uboot.delVariable("mrootargs.1");
                uboot.delVariable("mrootargs.2");
                uboot.delVariable("mtdparts.1");
                uboot.delVariable("mtdparts.2");
                uboot.delVariable("prepareFallback");
                uboot.delVariable("mrootargsFallback");
                uboot.delVariable("mtdpartsFallback");
                uboot.delVariable("activeInstall");
                uboot.delVariable("alternateInstall");
                uboot.delVariable("nextBoot");
                uboot.delVariable("runningInstall");
					
                uboot.setVariable("mrootargs", "set bootargs \\$(bootargs) root=ubi0:rootfs ubi.mtd=root1 \\$(mtdparts.linux) rootfstype=ubifs ip=off");
                // FIXME - root can be expanded with -(root).
                // Note that this doesn't currently work because, later on, we read the partition size back out and need to parse a number there.
                uboot.setVariable("mtdparts",
                                  String.format("mtdparts=mem.0:%dK@4M(boot)\\;sxni9260.0:%dK@%dK(boot1),%dK(root1)",
                                                program.getBootSize(),
                                                program.getBootSize(),
                                                512 + program.getConfigSize(),
                                                program.getRootSize()*2 + program.getBootSize()));
		if (program.getIsLargePage()) {
                    uboot.setVariable("mtdparts.linux",
                                  String.format("mtdparts=sxni9260.0:256K@0(u-boot),128K(environment1),128KK(environment2),%dK(config),%dK(boot1),%dK(root1)",
                                                program.getConfigSize(),
                                                program.getBootSize(),
                                                program.getRootSize()*2 + program.getBootSize()));
		}
		else {
                    uboot.setVariable("mtdparts.linux",
                                  String.format("mtdparts=sxni9260.0:320K@0(u-boot),128K(environment),64K(reserved),%dK(config),%dK(boot1),%dK(root1)",
                                                program.getConfigSize(),
                                                program.getBootSize(),
                                                program.getRootSize()*2 + program.getBootSize()));
		}
					
                // FIXME - the 20007fc0 should be a define.
                uboot.setVariable("mtdboot", "run commonbootargs\\; run mrootargs\\; fsload 0x20007fc0 uImage\\; bootm");

                // Tell the loader to apply the config image, if it exists
                loader.setApplyConfigImage(true);
					
                break;
            case 4:
                // Layout version is our 8xxx product line. 
                // It is consists of 2 images which are Root and Config
                // For right now, we just set the layoutVersion in U-Boot - everything else is fine at the
                // defaults we ship with.
                uboot.setVariable("layoutVersion", Integer.toString(layoutVersion));
                break;
            case 5:
                // u-boot has load scripts, set to load everything in bundle.
                loader.setApplyConfigImage(true);
            }
			
            uboot.flushVariables();
        }
        catch (IOException ioe) {
            throw new RuntimeException("Error updating UBoot: " +
                                       ioe.getMessage(), ioe);
        }
    }
	
    private void catchUBoot(UBootCommunicator uboot)
    {
        try {
            turnOffSwitch();
        }
        catch (Exception e) {
            throw new RuntimeException("Unable to turn off switch: " +
                                       e.getMessage(), e);
        }
		
        // Give the switch a moment to finish turning off
        Utils.sleep(1000);
		
        uboot.startCatch();
		
        try {
            turnOnSwitch();
        }
        catch (Exception e) {
            throw new RuntimeException("Unable to turn on switch: " +
                                       e.getMessage(), e);
        }
		
        try {
            uboot.waitForCatch(60000);
        }
        catch (TimeoutException te) {
            throw new RuntimeException("Timed out trying to catch UBoot: " +
                                       te.getMessage(), te);
        }
        catch (IOException ioe) {
            throw new RuntimeException("Unable to catch UBoot: " +
                                       ioe.getMessage(), ioe);
        }
    }
	
    private void turnOffSwitch()
        throws IOException, TimeoutException
    {
        if (program.getDOIP() == null || swtch.getDOPoints().size() == 0) {
            ui.turnOffSwitch(swtch);
        }
        else {
            program.turnOffSwitch(swtch);
        }
    }
	
    private void turnOnSwitch()
        throws IOException, TimeoutException
    {
        if (program.getDOIP() == null || swtch.getDOPoints().size() == 0) {
            ui.turnOnSwitch(swtch);
        }
        else {
            program.turnOnSwitch(swtch);
        }
    }
	
    private int checkLayoutVersion(UBootCommunicator uboot)
    {
        logger.debug("Retrieving 'layoutVersion' variable");
		
        String version;
        try {
            version = uboot.getVariable("layoutVersion");
        }
        catch (IOException ioe) {
            return 1;
        }
		
        logger.debug("Variable 'layoutVersion' = '" + version + "'");
		
        try {
            return Integer.parseInt(version);
        }
        catch (NumberFormatException nfe) {
            throw new RuntimeException("Bad response from switch: '" + version +
                                       "'", nfe);
        }
    }

    /* (non-Javadoc)
     * @see com.sixnetio.Switch.FirmwareLoaderUI#getTFTPAddr()
     */
    @Override
	public String getTFTPAddr()
    {
        return swtch.detectLocalIP();
    }

    /* (non-Javadoc)
     * @see com.sixnetio.Switch.FirmwareLoaderUI#getTFTPDir()
     */
    @Override
	public String getTFTPDir()
    {
        return program.getTFTPDir();
    }

    /* (non-Javadoc)
     * @see com.sixnetio.Switch.FirmwareLoaderUI#handleException(java.lang.Exception, java.lang.String, boolean)
     */
    @Override
	public void handleException(Exception e, String extraMessage, boolean mt)
    {
        if (e instanceof InterruptedOperationException) {
            // Ignore it, the user canceled the update
            return;
        }
		
        String msg = e.getMessage();
        if (extraMessage != null && extraMessage.length() != 0) {
            msg += " (" + extraMessage + ")";
        }
		
        getUncaughtExceptionHandler().uncaughtException(this,
                                                        new Exception("Error during load: " +
                                                                      msg,
                                                                      e));
    }

    /* (non-Javadoc)
     * @see com.sixnetio.Switch.FirmwareLoaderUI#saveSwitchState(com.sixnetio.Switch.SwitchConfig)
     */
    @Override
	public void saveSwitchState(SwitchConfig state)
    {
        // Not doing this, not supported by SerialLoader
    }

    /* (non-Javadoc)
     * @see com.sixnetio.Switch.FirmwareLoaderUI#tftpProgress(float)
     */
    @Override
	public void tftpProgress(float percentage)
    {
        swtch.setPercentTransferred(percentage);
        ui.progressUpdated(lastState, percentage);
    }

    /* (non-Javadoc)
     * @see com.sixnetio.Switch.FirmwareLoaderUI#updateState(int)
     */
    @Override
	public void updateState(int state)
    {
        lastState = state;
		
        float progress = -1;
		
        switch (state) {
        case FirmwareLoaderUI.S_APPLY_POWER:
            // Ignore it
            // It wants to break into UBoot, but we are already there
            break;
        case FirmwareLoaderUI.S_CONFIG_IMAGE:
            swtch.setImageInProgress(SwitchUtility.FirmwareImage.Config);
            swtch.setPercentTransferred(0);
            progress = 0;
            break;
        case FirmwareLoaderUI.S_LOAD_BOOT_IMAGE:
            swtch.setImageInProgress(SwitchUtility.FirmwareImage.ImageB);
            swtch.setPercentTransferred(0);
            progress = 0;
            break;
        case FirmwareLoaderUI.S_LOAD_IMAGE:
            swtch.setImageInProgress(SwitchUtility.FirmwareImage.Image);
            swtch.setPercentTransferred(0);
            progress = 0;
            break;
        case FirmwareLoaderUI.S_WAIT:
            swtch.setImageInProgress(null);
            break;
        default:
            // Not interested in the rest of the states
        }
		
        ui.progressUpdated(state, progress);
    }
}

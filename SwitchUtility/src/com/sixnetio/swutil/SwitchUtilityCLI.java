/*
 * SwitchUtilityCLI.java
 *
 * Provides a command-line interface for any necessary user interaction with the
 * switch utility program.
 *
 * Jonathan Pearson
 * December 23, 2009
 *
 */

package com.sixnetio.swutil;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Provides a command-line interface for any necessary user interaction with
 * {@link SwitchUtility}.
 *
 * @author Jonathan Pearson
 */
public class SwitchUtilityCLI
    implements SwitchUtilityUI
{
    private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
    private final SwitchUtility program;
	
    /**
     * Construct a new Switch Utility CLI interface.
     * 
     * @param program The SwitchUtility for which we are providing a UI.
     */
    public SwitchUtilityCLI(SwitchUtility program)
    {
        this.program = program;
    }
	
    /* (non-Javadoc)
     * @see com.sixnetio.swutil.SwitchUtilityUI#switchFailed(com.sixnetio.swutil.Switch, java.lang.Throwable)
     */
    @Override
	public void switchFailed(Switch swtch, Throwable why)
    {
        System.err.printf("Failed to update switch %s: %s\n",
                          swtch.getSwitchIP(), why.getMessage());
		
        logger.warn("Switch " + swtch.getSwitchIP() + " failed", why);
    }
	
    /* (non-Javadoc)
     * @see com.sixnetio.swutil.SwitchUtilityUI#turnOffSwitch(com.sixnetio.swutil.Switch)
     */
    @Override
	public void turnOffSwitch(Switch swtch)
    {
        System.console().readLine("Please turn OFF switch %s (connected to %s) and then press Enter",
                                  swtch.getSwitchIP(), swtch.getSerialPath());
    }
	
    /* (non-Javadoc)
     * @see com.sixnetio.swutil.SwitchUtilityUI#turnOnSwitch(com.sixnetio.swutil.Switch)
     */
    @Override
	public void turnOnSwitch(Switch swtch)
    {
        System.console().readLine("Please turn ON switch %s (connected to %s) and then press Enter",
                                  swtch.getSwitchIP(), swtch.getSerialPath());
    }

    /* (non-Javadoc)
     * @see com.sixnetio.swutil.SwitchUtilityUI#start()
     */
    @Override
	public int start()
    {
        int retVal = 0;
        try {
            program.performUpdate(this);
        }
        catch (Exception e) {
            System.err.printf("Failed to perform update: " + e.getMessage());
			
            logger.error("Failed to perform update", e);

            // If we encounter a fatal error we set retVal to 
            // a failing return code.
            retVal = 1;
        }
        return retVal;
    }

    /* (non-Javadoc)
     * @see com.sixnetio.swutil.SwitchUtilityUI#progressUpdated(int, float)
     */
    @Override
	public void progressUpdated(int state, float progress)
    {
        // Ignore it, not interested
    }
}

/*
 * SwitchUtilityUI.java
 *
 * An interface implemented by Switch Utility user interfaces.
 *
 * Jonathan Pearson
 * December 22, 2009
 *
 */

package com.sixnetio.swutil;

import com.sixnetio.Switch.FirmwareLoaderUI;

/**
 * An interface implemented by Switch Utility user interfaces.
 * 
 * @author jonathan
 */
public interface SwitchUtilityUI
{
    /** Start the UI. This may block or return immediately, no matter. */
    public int start();
	
    /** Prompt the user to turn off the given switch, blocking until done. */
    public void turnOffSwitch(Switch swtch);
	
    /** Prompt the user to turn on the given switch, blocking until done. */
    public void turnOnSwitch(Switch swtch);
	
    /** The given switch failed to load due to the given Throwable. */
    public void switchFailed(Switch swtch, Throwable why);
	
    /**
     * A step in firmware loading has been completed, or a step with progress
     * information has received an update.
     * 
     * @param state The current step (see {@link FirmwareLoaderUI} for steps).
     * @param progress Progress within the current step, or a negative number
     *   if progress information is unavailable.
     */
    public void progressUpdated(int state, float progress);
}

/*
 * Switch.java
 * 
 * Represents a switch for the Switch Utility program.
 * 
 * Jonathan Pearson
 * December 22, 2009
 * 
 */
package com.sixnetio.swutil;

import java.net.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.net.IfcUtil;
import com.sixnetio.util.Utils;

/**
 * Represents a switch to be updated when the
 * {@link SwitchUtility#performUpdate()} method is called.
 *
 * @author Jonathan Pearson
 */
class Switch
{
    private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
    private String serialPath = SwitchUtility.D_SERIAL_PATH;
    private int baudRate = SwitchUtility.D_BAUD_RATE;
    private String localIP = null;
    private String swIP = SwitchUtility.D_SWITCH_IP;
    private String swSubnet = SwitchUtility.D_SWITCH_SUBNET;
    private String swGateway = SwitchUtility.D_SWITCH_GATEWAY;
    private List<Short> doPoints = new LinkedList<Short>();
    private int forcedLayout = 0; // 0 means use bundle layout
	
    // Used to show progress while loading
    // These are both written by one thread and read by another
    private volatile SwitchUtility.FirmwareImage imageInProgress;
    private volatile float percentTransferred;
	
    public Switch()
    {
        // Nothing to do
    }
	

    /** Get the IP address of the local machine, may be <tt>null</tt>. */
    public String getLocalIP()
    {
        return localIP;
    }
	
    /**
     * Get the IP address that will be used for the local machine.
     * 
     * @return If the local IP address is not <tt>null</tt> (see
     *   {@link #getLocalIP()}), that address. If the local IP address is
     *   <tt>null</tt>, the switch's IP address and subnet mask will be used
     *   to determine the local address most likely to be able to
     *   communicate with the switch and return that; if no address will be able to
     *   communicate with the switch, this will return <tt>null</tt>.
     */
    public String detectLocalIP()
    {
        if (localIP == null) {
            try {
                Map<String, InterfaceAddress> addresses =
                IfcUtil.discoverLocalAddresses(false);
				
                InetAddress swAddress = InetAddress.getByName(swIP);
                short swPrefix = Utils.prefixFromSubnet(swSubnet);
				
                // Search for the first one on the same subnet as the switch
                for (Map.Entry<String, InterfaceAddress> entry : addresses.entrySet()) {
                    InetAddress localAddress = InetAddress.getByName(entry.getKey());
					
                    if (IfcUtil.sameSubnet(localAddress, swAddress,
                                           entry.getValue().getNetworkPrefixLength())) {
                        // To prevent weird connectivity problems where the
                        // computer can reach the switch, but the switch
                        // cannot reach the computer, make sure the switch's
                        // subnet would work too
                        if (IfcUtil.sameSubnet(localAddress, swAddress, swPrefix)) {
                            return entry.getKey();
                        }
                    }
                }
            }
            catch (SocketException se) {
                logger.error("Unable to match subnet", se);
            }
            catch (UnknownHostException uhe) {
                logger.error("Unable to look up host by IP", uhe);
            }
			
            return null;
        }
        else {
            return localIP;
        }
    }
	
    /** Set the IP address of the local machine, may be <tt>null</tt>. */
    public void setLocalIP(String localIP)
    {
        this.localIP = localIP;
    }
	
    /** Get the IP address of the switch, not <tt>null</tt>. */
    public String getSwitchIP()
    {
        return swIP;
    }
	
    /** Set the IP address of the switch, not <tt>null</tt>. */
    public void setSwitchIP(String swIP)
    {
        if (swIP == null) {
            throw new NullPointerException("swIP may not be null");
        }
		
        this.swIP = swIP;
    }
	
    /** Get the subnet mask of the switch, not <tt>null</tt>. */
    public String getSwitchSubnet()
    {
        return swSubnet;
    }
	
    /** Set the subnet mask of the switch, not <tt>null</tt>. */
    public void setSwitchSubnet(String swSubnet)
    {
        if (swSubnet == null) {
            throw new NullPointerException("swSubnet may not be null");
        }
		
        this.swSubnet = swSubnet;
    }
	
    /** Get the gateway address of the switch, may be <tt>null</tt>. */
    public String getSwitchGateway()
    {
        return swGateway;
    }
	
    /** Set the gateway address of the switch, may be <tt>null</tt>. */
    public void setSwitchGateway(String swGateway)
    {
        this.swGateway = swGateway;
    }
	
    /** Get the path to the serial port, not <tt>null</tt>. */
    public String getSerialPath()
    {
        return serialPath;
    }
	
    /** Set the path to the serial port, not <tt>null</tt>. */
    public void setSerialPath(String serialPath)
    {
        if (serialPath == null) {
            throw new NullPointerException("serialPath may not be null");
        }
		
        this.serialPath = serialPath;
    }

    /** Set the baud rate on the serial port */
    public void setBaudRate(int baud)
    {
        this.baudRate = baud;
    }

    /** Set the baud rate on the serial port */
    public int getBaudRate()
    {
        return this.baudRate;
    }
	
    /** Get the DO points controlling power to the switch, not <tt>null</tt>. */
    public List<Short> getDOPoints()
    {
        return new ArrayList<Short>(doPoints);
    }
	
    /** Add a DO point for this switch. */
    public void addDOPoint(short point)
    {
        doPoints.add(point);
    }
	
    /** Remove a DO point. */
    public void removeDOPoint(short point)
    {
        doPoints.remove(point);
    }
	
    /** Check which image is being transferred. */
    SwitchUtility.FirmwareImage getImageInProgress()
    {
        return imageInProgress;
    }
	
    /** Set the image being transferred. */
    void setImageInProgress(SwitchUtility.FirmwareImage imageInProgress)
    {
        this.imageInProgress = imageInProgress;
    }
	
    /** Check the percentage of the image that has been sent. */
    float getPercentTransferred()
    {
        return percentTransferred;
    }
	
	
    /** Set the percentage of the image that has been sent. */
    void setPercentTransferred(float percentTransferred)
    {
        this.percentTransferred = percentTransferred;
    }
	
    /** Get the forced layout version, 0 means use bundle layout. */
    int getForcedLayout()
    {
        return forcedLayout;
    }
	
    /** Set the forced layout version, 0 means use bundle layou. */
    void setForcedLayout(int forcedLayout)
    {
        this.forcedLayout = forcedLayout;
    }
}

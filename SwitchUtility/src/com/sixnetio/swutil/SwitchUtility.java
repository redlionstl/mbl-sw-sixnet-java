/*
 * SwitchUtility.java
 * 
 * Executable entry point for the Switch Utility program. This program allows a
 * user to repartition a switch to use the old layout (single boot/root) or the
 * new layout (config + dual boot/root), along with loading firmware onto the
 * switch.
 * 
 * Jonathan Pearson
 * December 21, 2009
 */

package com.sixnetio.swutil;

import java.io.*;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.InterfaceAddress;
import java.util.*;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.*;

import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.fs.generic.FSObject;
import com.sixnetio.fs.generic.File;
import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.net.IfcUtil;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;


public class SwitchUtility
{
    private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
    public static final String PROG_VERSION = "@swutil.major@.@swutil.minor@.@swutil.build@";
	
    /**
     * The default path to the serial port if the user does not specify any
     * switches on the command line.
     */
    protected static final String D_SERIAL_PATH;
    protected static final int D_BAUD_RATE = 9600;
	
    /** The default switch IP address. */
    protected static final String D_SWITCH_IP = "192.168.0.1";
	
    /** The default switch subnet mask. */
    protected static final String D_SWITCH_SUBNET = "255.255.255.0";
	
    /** The default switch gateway address. */
    protected static final String D_SWITCH_GATEWAY = "";
	
	
    static
    {
        Logger.getRootLogger().setLevel(Level.OFF);
        PropertyConfigurator.configureAndWatch("log4j.properties", 5000);
		
        if (Utils.osIsWindows()) {
            D_SERIAL_PATH = "COM1";
        }
        else {
            D_SERIAL_PATH = "/dev/ttyS0";
        }
    }
	
    // Command-line arguments
    private String bundlePath = null;
    private String doIP = null;
    private String tftpDir = null;
    private int timeout = 90;
    private boolean showGUI = true;
    private boolean showProgress = false;
    private boolean ignoreVendor = false;
    private int displayWidth = 80;
    private int configSize = -1;
    private int bootSize = -1;
    private int rootSize = -1;
    private int configSize_sp = 512;
    private int bootSize_sp = 2048;
    private int rootSize_sp = 13824;
    private int configSize_lp = 2048;
    private int bootSize_lp = 8192;
    private int rootSize_lp = 55296;
    private boolean isLargePage = false;
    private boolean showScript = false;
	
    private List<Switch> switches = new LinkedList<Switch>();
	
    // While performing an update, these are in use
    static enum FirmwareImage
    {
        ImageB,
        Image,
        Config,
        ;
    }
	
    private UDRLink udrHandler;
    private UDRLib udr;
    private FirmwareBundle bundle;
    private int layoutVersion;
    private Map<Switch, SwitchUpdater> updaters;
    private Thread progressThread;
	
    private void usage(PrintStream out)
    {
        out.println("Switch Utility Program version " + PROG_VERSION);
        out.println();
        out.println("Switch Utility usage: [OPTIONS]...");
        out.println("  -f <path>  Specify the path to the firmware bundle to load (required with -x)");
        out.println("  -A <ip>    Specify the IP address of a DO module controlling power to all of");
        out.println("               the switches (optional)");
        out.println("  -d <path>  Specify the shared directory of a running TFTP server (optional)");
        out.println("  -w <secs>  Specify the timeout (in seconds) to wait for the switch's web");
        out.println("               server to start accepting connections, or 0 to finish");
        out.println("               immediately. Use 'forever' to never time out (default: '90')");
        out.println("  -x         Do not display the GUI");
        out.println("  -p         Display progress information for TFTP transfers");
        out.println("  --width    Set the display width for -p (default: " + displayWidth + ")");
        out.println("  -L         Display a list of externally visible IP addresses on this machine");
        out.println("               and terminate");
        out.println("  -h         Display this help information and terminate");
        out.println("  -s <path>  Begin defining a switch to update, specify the path of the serial");
        out.println("               port connected to the switch (required if defining a non-default");
        out.println("               switch; otherwise defaults to '" + D_SERIAL_PATH + "'). The rest");
        out.println("               of the options finish defining the current switch");
        out.println("  --baud <baud> Baud Rate.  (default: " + D_BAUD_RATE + ")");
        out.println("  -i <ip>    Specify the IP address that the switch may use to reach this");
        out.println("               machine (default: autodetect based on switch address)");
        out.println("  -a <ip>    Specify the IP address of the switch (default: '" + D_SWITCH_IP + "')");
        out.println("  -b <mask>  Specify the subnet mask to give the switch (default:");
        out.println("               '" + D_SWITCH_SUBNET + "')");
        out.println("  -g <ip>    Specify the gateway address for the switch (optional)");
        out.println("  -D <pnt>   Specify a DO point (1-based) controlling power to the switch");
        out.println("               (optional, may be specified multiple times)");
        out.println("  -l <ver>   Force a specific layout version, rather than using the layout");
        out.println("               specified by the firmware bundle; 0 means use bundle layout.");
        out.println("               Layout 1 is single image, 2 is dual-image (default: 0).");
        out.println("               Layout 3 is single image with UBIFS filesystem ");
        out.println("                   and a config partition.");
        out.println("               Layout 99 is special development layout - a layout 2 bundle");
        out.println("               loaded as a single image, so there is free space.");
        out.println("               This option is intended for testing and should not be used on");
        out.println("               production switches");
        out.println("  --script   Print out the uboot commands sent to unit, prefixed with '+'");
		
        /* Hidden options:
         * -V     Ignore vendor mismatch
         * These next options change individual partition sizes, which will push
         * the following partitions further out (these only apply for
         * layoutVersion 2)
         * --cs  Set configure partition size in K (default 512)
         * --bs  Set boot 1 partition size in K (default 2048)
         * --rs  Set root 1 partition size in K (default 13824)
         */
    }
	
    public static void main(String[] args)
    {
        SwitchUtility program = new SwitchUtility(args);
        SwitchUtilityUI ui;
        int retVal;

        if (program.getShowGUI()) {
            ui = new SwitchUtilityGUI(program);
        }
        else {
            ui = new SwitchUtilityCLI(program);
        }

        retVal = ui.start();

        if (ui instanceof SwitchUtilityCLI) {
            System.exit(retVal);
        }
    }
	
    private SwitchUtility(String[] args) {
        parseArguments(args);
    }
	
    /**
     * Parses the argument list passed to the program, storing the results in
     * class fields. This function will call {@link System#exit(int)} if the
     * argument parsing has a problem or a specified argument calls for
     * immediate program termination.
     * 
     * @param args The arguments passed to the program.
     */
    private void parseArguments(String[] args)
    {
        // Parse arguments
        try {
            Switch currentSwitch = null;
            String lastDOIP = null;
			
            // XXX: This loop modifies 'i' in-flight, as it is actually more
            // clear and concise than any other method I know. This violates the
            // coding standard rule "Only update the loop control in the
            // expected place"
            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("-a")) {
                    if (currentSwitch == null) {
                        throw new Exception("No switch defined, pass -s prior to -a");
                    }
					
                    currentSwitch.setSwitchIP(args[++i]);
                }
                else if (args[i].equals("-A")) {
                    doIP = args[++i];
                }
                else if (args[i].equals("-b")) {
                    if (currentSwitch == null) {
                        throw new Exception("No switch defined, pass -s prior to -b");
                    }
					
                    currentSwitch.setSwitchSubnet(args[++i]);
                }
                else if (args[i].equals("--bs")) {
                    i++;
					
                    int value;
                    try {
                        value = Integer.parseInt(args[i]);
                    }
                    catch (NumberFormatException nfe) {
                        throw new Exception("Boot size '" + args[i] +
                                            "' is not a number", nfe);
                    }
					
                    bootSize = value;
                }
                else if (args[i].equals("--cs")) {
                    i++;
					
                    int value;
                    try {
                        value = Integer.parseInt(args[i]);
                    }
                    catch (NumberFormatException nfe) {
                        throw new Exception("Config size '" + args[i] +
                                            "' is not a number", nfe);
                    }
					
                    configSize = value;
                }
                else if (args[i].equals("-d")) {
                    tftpDir = args[++i];
                }
                else if (args[i].equals("-D")) {
                    i++;
					
                    short point;
                    try {
                        point = Short.parseShort(args[i]);
                    }
                    catch (NumberFormatException nfe) {
                        throw new Exception("DO point '" + args[i] +
                                            "' is not a number", nfe);
                    }
					
                    if (currentSwitch == null) {
                        throw new Exception("No switch defined, pass -s prior to -D");
                    }
					
                    // Convert to 0-based
                    currentSwitch.addDOPoint((short)(point - 1));
                }
                else if (args[i].equals("-f")) {
                    bundlePath = args[++i];
                }
                else if (args[i].equals("-g")) {
                    if (currentSwitch == null) {
                        throw new Exception("No switch defined, pass -s prior to -g");
                    }
					
                    currentSwitch.setSwitchGateway(args[++i]);
                }
                else if (args[i].equals("-h")) {
                    usage(System.out);
                    System.exit(0);
                }
                else if (args[i].equals("-i")) {
                    if (currentSwitch == null) {
                        throw new Exception("No switch defined, pass -s prior to -i");
                    }
					
                    currentSwitch.setLocalIP(args[++i]);
                }
                else if (args[i].equals("-L")) {
                    // Do not include IPv6 addresses because we need to
                    // interface with UBoot, which does not support IPv6
                    Map<String, InterfaceAddress> localAddresses =
                    IfcUtil.discoverLocalAddresses(false);
					
                    System.out.println("Available addresses:");
					
                    for (Map.Entry<String, InterfaceAddress> entry : localAddresses.entrySet()) {
                        String addr = entry.getKey();
                        InterfaceAddress ifcAddr = entry.getValue();
						
                        System.out.printf("  %s/%d\n", addr, ifcAddr.getNetworkPrefixLength());
                    }
					
                    System.exit(0);
                }
                else if (args[i].equals("-p")) {
                    showProgress = true;
                }
                else if (args[i].equals("--rs")) {
                    i++;
					
                    int value;
                    try {
                        value = Integer.parseInt(args[i]);
                    }
                    catch (NumberFormatException nfe) {
                        throw new Exception("Root size '" + args[i] +
                                            "' is not a number", nfe);
                    }
					
                    rootSize = value;
                }
                else if (args[i].equals("-s")) {
                    currentSwitch = new Switch();
                    switches.add(currentSwitch);
                    currentSwitch.setSerialPath(args[++i]);
                }
                else if (args[i].equals("--baud")) {
                    if (currentSwitch == null) {
                        throw new Exception("No switch defined, pass -s prior to -b");
                    }
                    currentSwitch.setBaudRate(Integer.parseInt(args[++i]));
                }
                else if (args[i].equals("-l")) {
                    int forcedLayout;
					
                    i++;
					
                    try {
                        forcedLayout = Integer.parseInt(args[i]);
                    }
                    catch (NumberFormatException nfe) {
                        throw new Exception("Forced layout '" + args[i] +
                                            "' is not a number", nfe);
                    }
					
                    if ((forcedLayout < 0 || forcedLayout > 3) && forcedLayout != 99) {
                        throw new Exception("Forced layout '" + args[i] +
                                            "' must be 0 - 3 or 99");
                    }
					
                    if (currentSwitch == null) {
                        throw new Exception("No switch defined, pass -s prior to -l");
                    }
					
                    currentSwitch.setForcedLayout(forcedLayout);
                }
                else if (args[i].equals("-V")) {
                    ignoreVendor = true;
                }
                else if (args[i].equals("-w")) {
                    i++;
					
                    try {
                        timeout = Integer.parseInt(args[i]);
                    }
                    catch (NumberFormatException nfe) {
                        throw new Exception("Timeout '" + args[i] +
                                            "' is not a number", nfe);
                    }
                }
                else if (args[i].equals("--width")) {
                    i++;
					
                    try {
                        displayWidth = Integer.parseInt(args[i]);
                    }
                    catch (NumberFormatException nfe) {
                        throw new Exception("Display width '" + args[i] +
                                            "' is not a number", nfe);
                    }
                }
                else if (args[i].equals("-x")) {
                    showGUI = false;
                }
                else if (args[i].equals("--script")) {
                    showScript = true;
                }
                else {
                    throw new Exception("Unrecognized argument: " + args[i]);
                }
            }
			
            if (bundlePath == null && ! showGUI) {
                throw new Exception("Firmware bundle path is required without a GUI");
            }
			
            if (switches.size() == 0) {
                // The user may go with a single, default switch (likely for the
                // GUI)
                switches.add(new Switch());
            }
			
            // Make sure that if no DO address was provided, there are no
            // switches depending on a DO
            if (doIP == null) {
                for (Switch s : switches) {
                    if (s.getDOPoints().size() != 0) {
                        throw new Exception("Switch '" + s.getSwitchIP() +
                                            "' has at least one DO point" +
                                            " defined, but there was no DO" +
                                            " address provided with -A");
                    }
                }
            }
			
            if (displayWidth <= 0) {
                throw new Exception("Display width must be greater than zero");
            }
        }
        catch (Exception e) {
            // ArrayIndexOutOfBoundsException is thrown when the user forgets an
            // argument at the end of the command line
            if (e instanceof ArrayIndexOutOfBoundsException) {
                ArrayIndexOutOfBoundsException aioobe = (ArrayIndexOutOfBoundsException)e;
                e = new Exception("Option '" +
                                  args[args.length - 1] +
                                  "' requires an argument", e);
                e.fillInStackTrace();
            }
			
            System.err.println(e.getMessage());
            System.err.println();
            usage(System.err);
            logger.error("Error parsing arguments", e);
            System.exit(1);
        }
    }
	
    /** Get the path to the firmware bundle, not <tt>null</tt>. */
    public String getBundlePath()
    {
        return bundlePath;
    }
	
    /** Set the path to the firmware bundle, not <tt>null</tt>. */
    public void setBundlePath(String bundlePath)
    {
        if (bundlePath == null) {
            throw new NullPointerException("bundlePath may not be null");
        }
		
        this.bundlePath = bundlePath;
    }
	
    /** Get the IP address of the DO module, may be <tt>null</tt>. */
    public String getDOIP()
    {
        return doIP;
    }
	
    /** Set the IP address of the DO module, may be <tt>null</tt>. */
    public void setDOIP(String doIP)
    {
        this.doIP = doIP;
    }
	
    /** Get the external TFTP server shared directory, may be <tt>null</tt>. */
    public String getTFTPDir()
    {
        return tftpDir;
    }
	
    /** Set the external TFTP server shared directory, may be <tt>null</tt>. */
    public void setTFTPDir(String tftpDir)
    {
        this.tftpDir = tftpDir;
    }
	
    /**
     * Get the number of seconds to wait for the switch to come back online.
     * 
     * @return -1 = wait forever, 0 = do not wait, above 0 = number of seconds
     *   to wait.
     */
    public int getTimeout()
    {
        return timeout;
    }
	
    /**
     * Set the number of seconds to wait for the switch to come back online.
     * 
     * @param timeout Below 0 to wait forever, 0 to not wait, above 0 to wait
     *   for that many seconds.
     */
    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }
	
    /** Check whether to show the GUI. */
    public boolean getShowGUI()
    {
        return showGUI;
    }

    /** Check whether to display u-boot script as executed. */
    public boolean getShowScript()
    {
        return showScript;
    }
	
    /** Set whether to show the GUI. */
    public void setShowGUI(boolean showGUI)
    {
        this.showGUI = showGUI;
    }
	
    /** Check whether to show TFTP progress on the CLI. */
    public boolean getShowProgress()
    {
        return showProgress;
    }
	
    /** Get a list containing all of the configured switches. */
    public List<Switch> getSwitches()
    {
        return new ArrayList<Switch>(switches);
    }
	
    /** Add a new switch. */
    public void addSwitch(Switch swtch)
    {
        switches.add(swtch);
    }
	
    /** Remove a switch. */
    public void removeSwitch(Switch swtch)
    {
        switches.remove(swtch);
    }
	
    /** Set whether to show TFTP progress on the CLI. */
    public void setShowProgress(boolean showProgress)
    {
        this.showProgress = showProgress;
    }
	
    /** Check whether the vendor string will be ignored when loading. */
    public boolean getIgnoreVendor()
    {
        return ignoreVendor;
    }
	
    /** Set whether to ignore the vendor string when loading. */
    public void setIgnoreVendor(boolean ignoreVendor)
    {
        this.ignoreVendor = ignoreVendor;
    }
	
    /** Get the display width for progress transfer updates. */
    public int getDisplayWidth()
    {
        return displayWidth;
    }
	
    /** Set the display width for progress transfer updates. */
    public void setDisplayWidth(int displayWidth)
    {
        this.displayWidth = displayWidth;
    }
	
    /** Get the size of the config partition in K. */
    public int getConfigSize()
    {
	if (configSize >= 0)
	{
	    // this was explicitly set; return it
	    return configSize;
	}
	else if(isLargePage)
	{
	    // return large page default
	    return configSize_lp;
	}
	else
	{
	    // return small page default
	    return configSize_sp;
	}
    }
	
    /** Set the size of the config partition in K (default 512). */
    public void setConfigSize(int configSize) {
        this.configSize = configSize;
    }
	
    /** Get the size of the boot partitions in K. */
    public int getBootSize()
    {
	if (bootSize >= 0) 
	{
	    // this was explicitly set; return it
	    return bootSize;
	}
	else if(isLargePage)
	{
	    // return large page default
	    return bootSize_lp;
	}
	else
	{
	    // return small page default
	    return bootSize_sp;
	}
    }
	
    /** Set the size of the boot partitions in K (default 2048). */
    public void setBootSize(int bootSize)
    {
        this.bootSize = bootSize;
    }
	
    /** Get the size of the root partitions in K. */
    public int getRootSize()
    {
	if (rootSize >= 0)
	{
	    // this was explicitly set; return it
	    return rootSize;
	}
	else if(isLargePage)
	{
	    // return large page default
	    return rootSize_lp;
	}
	else
	{
	    // return small page default
	    return rootSize_sp;
	}
    }
	
    /** Set the size of the root partitions in K (default 13824). */
    public void setRootSize(int rootSize)
    {
        this.rootSize = rootSize;
    }

    /** Get whether or not it is large page NAND */
    public boolean getIsLargePage()
    {
	return isLargePage;
    }

    /** Set whether or not it is large page NAND */
    public void setIsLargePage(boolean isLargePage)
    {
	this.isLargePage = isLargePage;
    }
	
    /** Provides the current bundle to the updater threads. */
    FirmwareBundle getBundle()
    {
        return bundle;
    }
	
    public synchronized void performUpdate(final SwitchUtilityUI ui)
        throws Exception
    {
        // Open the bundle and examine the firmware version
        // That will determine which type of partition layout to use
        logger.debug("Opening firmware bundle " + getBundlePath());
        InputStream in = new FileInputStream(getBundlePath());
        try {
            bundle = new FirmwareBundle(in);
        }
        finally {
            in.close();
        }
		
        logger.debug("Checking for NONE/layoutVersion");
        FSObject layoutVersion = bundle.getRoot().locate("NONE/layoutVersion");
        if (layoutVersion == null) {
            // Old fashioned bundle not containing that file
            logger.debug("  Not found, using layout version 1");
            this.layoutVersion = 1;
        }
        else if (layoutVersion instanceof File) {
            // Read the file to make sure we support the layout version
            String fileContents = new String(((File)layoutVersion).getData());
			
            try {
                this.layoutVersion = Integer.parseInt(fileContents.trim());
                logger.debug("  Found, using layout version " + this.layoutVersion);
            }
            catch (NumberFormatException nfe) {
                logger.debug("  Found, but not expected format", nfe);
				
                throw new Exception("Unable to determine partition layout version required by firmware",
                                    nfe);
            }
        }
        else {
            // Layout version object is not a file
            logger.debug("  Found, but not a file");
			
            throw new Exception("Unable to determine partition layout version required by firmware");
        }
		
        if (doIP != null) {
            logger.debug("Preparing for UDR power control");
			
            MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();
            udrHandler = new UDRLink(TransportMethod.UDP, doIP);
            dispatcher.registerHandler(udrHandler);
            udr = new UDRLib(UDRMessage.STA_ANY, (byte)0);
        }
        // Start a thread to print transfer progress, if necessary
        startProgressThread();
        // Start a thread for each switch being updated
        UncaughtExceptionHandler exHandler = new UncaughtExceptionHandler() {
                // This uses the logger from the parent class, since there is no
                // point in defining its own
                @SuppressWarnings("synthetic-access")
                    @Override
                    public void uncaughtException(Thread thread, Throwable ex)
                {
                    if (thread instanceof SwitchUpdater) {
                        SwitchUpdater updater = (SwitchUpdater)thread;
                        ui.switchFailed(updater.getSwitch(), ex);
                    }
                    else {
                        logger.error("Unexpected source of uncaught exception: " +
                                     thread.getName());
                    }
                    System.exit(1);
                }
            };
		
        // Hashtable is thread-safe, which may be necessary
        logger.debug("Starting " + switches.size() + " updater threads");
        updaters = new Hashtable<Switch, SwitchUpdater>();
        for (Switch swtch : switches) {
            SwitchUpdater updater = new SwitchUpdater(this, ui, swtch);
            updaters.put(swtch, updater);
			
            updater.setUncaughtExceptionHandler(exHandler);
            updater.start();
        }
		
        // Wait for them all to finish
        logger.debug("Waiting for switch updater threads to complete");
        try {
            for (Switch swtch : switches) {
                updaters.get(swtch).join();
            }
        }
        catch (InterruptedException ie) {
            // Got canceled by the user, interrupt all sub-threads
            for (Switch swtch : switches) {
                updaters.get(swtch).interrupt();
            }
        }
		
        killProgressThread();
		
        logger.debug("Done updating switches");
    }
	
    private void startProgressThread()
    {
        if (showProgress) {
            logger.debug("Starting progress updater thread");
			
            progressThread = new Thread() {
                    @Override
                        public void run()
                    {
                        List<Switch> switches = getSwitches();
					
                        boolean running = true;
                        while (running) {
                            List<String> lines = new ArrayList<String>(switches.size());
						
                            for (Switch swtch : switches) {
                                if (swtch.getImageInProgress() != null) {
                                    String image;
                                    switch (swtch.getImageInProgress()) {
                                    case ImageB:
                                        image = "imageb";
                                        break;
                                    case Image:
                                        image = "image";
                                        break;
                                    case Config:
                                        image = "config";
                                        break;
                                    default:
                                        throw new RuntimeException("Unrecognized image type: " +
                                                                   swtch.getImageInProgress().name());
                                    }
								
                                    String line;
								
                                    if (swtch.getPercentTransferred() > -0.0001f) {
                                        line = String.format("%s-%s(%.1f%%)",
                                                             swtch.getSwitchIP(),
                                                             image,
                                                             swtch.getPercentTransferred());
                                    }
                                    else {
                                        line = String.format("%s-%s(done)",
                                                             swtch.getSwitchIP(),
                                                             image);
                                    }
								
                                    lines.add(line);
                                }
                            }
						
                            System.out.print("\r");
                            for (String line : lines) {
                                System.out.printf("%-" + getDisplayWidth() + "s",
                                                  line);
                            }
						
                            System.out.flush();
						
                            // Wait a bit before the next update
                            try {
                                Utils.sleep(100);
                            }
                            catch (InterruptedOperationException ioe) {
                                // Stop running the loop
                                running = false;
                            }
                        }
					
                        // End the line we were using for output
                        System.out.println();
                    }
                };
            progressThread.setName("Progress Updates");
            progressThread.setDaemon(true);
            progressThread.start();
        }
        else {
            logger.debug("No progress updater thread necessary");
			
            progressThread = null;
        }
    }
	
    private void killProgressThread()
    {
        if (progressThread != null) {
            logger.debug("Killing progress updater thread");
			
            progressThread.interrupt();
			
            try {
                progressThread.join();
            }
            catch (InterruptedException ie) {
                logger.warn("Unexpected interruption while killing progress thread");
            }
        }
    }
	
    void turnOffSwitch(Switch swtch)
        throws IOException, TimeoutException
    {
        boolean[] mask = buildPointMask(swtch.getDOPoints());
        udr.clrD(UDRMessage.STA_ANY, (short)0, (short)mask.length, mask);
    }
	
    void turnOnSwitch(Switch swtch)
        throws IOException, TimeoutException
    {
        boolean[] mask = buildPointMask(swtch.getDOPoints());
        udr.setD(UDRMessage.STA_ANY, (short)0, (short)mask.length, mask);
    }
	
    /**
     * Build a point mask containing <tt>true</tt> values only in those
     * positions where the given list has a value.
     * 
     * @param points The points to make into a mask.
     * @return An array of booleans consisting of <tt>false</tt> values except
     *   where <tt>points</tt> had a value, then it will have a <tt>true</tt>.
     *   For example, the list <tt>{ 1, 4, 5 }</tt> would turn into the array
     *   <tt>{ f, t, f, f, t, t }</tt>.
     */
    private boolean[] buildPointMask(List<Short> points)
    {
        // What is the highest numbered point?
        short high = -1;
        for (short s : points) {
            if (high < s) {
                high = s;
            }
        }
		
        // Build that mask (this will still work if high == -1)
        boolean[] mask = new boolean[high + 1];
        for (short s : points) {
            mask[s] = true;
        }
		
        return mask;
    }
	
    /**
     * Get the partition layout version.
     * 
     * @return The partition layout version (1, 2, 3 or 99).
     */
    int getPartitionLayoutVersion()
    {
        return layoutVersion;
		
    }

    /**
     * Get the firmware bundle type
     * 
     * @return The firmware bundle type
     */
    int getBundleType()
    {
        return bundle.getFWType();
    }

    /**
     * See if the bundle is for an IPm
     *
     * @return true if it is for an IPm
     * @return false if it is not for an IPm
     */
    boolean bundleIsForIPm() 
    {
        boolean retVal = false;
        switch (this.getBundleType()) {
        case FirmwareBundle.T_TYPE_VT_IPM:
        case FirmwareBundle.T_TYPE_VT_UIPM:
        case FirmwareBundle.T_TYPE_VT6_IPM:
        case FirmwareBundle.T_TYPE_ST_IPM:
        case FirmwareBundle.T_TYPE_ST6_IPM:
        case FirmwareBundle.T_TYPE_ET_GT_ST_3:
        case FirmwareBundle.T_TYPE_ST_GT_1210:
            retVal = true;
            break;
        default:
            retVal = false;
            break;
        }
        return retVal;
    }  

    /**
     * Get the firmware version contained within our bundle
     *
     * @return The version of firmware contained within that bundle, or
     *   <tt>null</tt> if there is a problem figuring it out.
     */
    public String getBundleVersion()
    {
	FirmwareBundle bundle = this.getBundle();
	return bundle.getFWVersion().replaceAll("_", ".");
    }

    /**
     * Get the firmware version contained within the given bundle.
     * 
     * @param path The path to the bundle.
     * @return The version of firmware contained within that bundle, or
     *   <tt>null</tt> if there is a problem figuring it out.
     */
    public static String getBundleVersion(String path)
    {
        if (path == null) {
            return null;
        }
		
        try {
            InputStream in = new FileInputStream(path);
			
            try {
                FirmwareBundle bundle = new FirmwareBundle(in);
                return bundle.getFWVersion().replaceAll("_", ".");
            }
            finally {
                in.close();
            }
        }
        catch (IOException ioe) {
            logger.warn("Unable to determine bundle version", ioe);
            return null;
        }
    }
}

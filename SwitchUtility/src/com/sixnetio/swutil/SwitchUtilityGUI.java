/*
 * SwitchUtilityGUI.java
 *
 * Provides a GUI for the Switch Utility program.
 *
 * Jonathan Pearson
 * December 23, 2009
 *
 */

package com.sixnetio.swutil;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.COM.ComPort;
import com.sixnetio.GUI.Suicide;
import com.sixnetio.Switch.FirmwareLoaderUI;
import com.sixnetio.net.IfcUtil;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

/**
 * Provides a GUI for {@link SwitchUtility}.
 *
 * @author Jonathan Pearson
 */
public class SwitchUtilityGUI implements SwitchUtilityUI {
    static final Logger logger = Logger.getLogger(Utils.thisClassName());

    final SwitchUtility program;

    private class SwitchWorker extends SwingWorker<Object, Object> {
        private JFrame frame;
        private JButton btnLoad;
        Throwable exception;

        public SwitchWorker(JFrame frame, JButton btnLoad) {
            this.frame = frame;
            this.btnLoad = btnLoad;
        }

        @Override
        protected Object doInBackground() throws Exception {
            try {
                program.performUpdate(SwitchUtilityGUI.this);
            } catch (Exception ex) {
                exception = ex;
            }

            return null;
        }

        @Override
        protected void done() {
            worker = null;
            btnLoad.setEnabled(true);

            if (exception != null) {
                logger.error("Unable to update switch", exception);

                prgBar.setIndeterminate(false);
                prgBar.setValue(prgBar.getMinimum());

                String msg = exception.getMessage();
                if (msg == null) {
                    // Try to determine by exception
                    // type
                    if ((exception instanceof InterruptedException)
                            || (exception instanceof InterruptedOperationException)) {

                        msg = "Operation canceled";
                    } else {
                        msg = "(Unknown cause)";
                    }
                }

                JOptionPane.showMessageDialog(frame, "Unable to update switch: " + msg, "Error",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                if (!isCancelled()) {
                    JOptionPane.showMessageDialog(frame, "Load completed successfully.\n"
                            + "Please allow a few seconds\n" + "for the switch to restart.", "Done",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }

        protected void setWorkerProgress(int progress) {
            super.setProgress(progress);
        }
    }

    // Used so we can cancel a running task
    SwitchWorker worker;

    // Used when a failure is detected, to clear progress
    JProgressBar prgBar;

    // Saved options from previous runs (may be empty)
    Tag config;

    /**
     * Construct a new GUI for the Switch Utility program.
     * 
     * @param program
     *            The SwitchUtility for which we are providing a UI.
     */
    public SwitchUtilityGUI(SwitchUtility program) {
        this.program = program;

        if (program.getSwitches().size() > 1) {
            throw new RuntimeException("GUI does not support multiple switches");
        }

        // Load the config file (saved when the user hits the Load button)
        try {
            String cfgPath = getConfigFilePath();
            config = Utils.xmlParser.parseXML(cfgPath, false, false);
        } catch (Exception e) {
            // No file or unable to read for some reason
            logger.warn("Unable to read options file", e);

            config = new Tag("swutil");
        }
    }

    /**
     * Get the path to the config file (saved options, really).
     * 
     * @return The path to the config file.
     */
    static String getConfigFilePath() {
        String homedir = System.getProperty("user.home");
        String cfgPath = String.format("%s%c%s", homedir, File.separatorChar, ".swutilrc");

        return cfgPath;
    }

    static void updateFirmwareFileVersion(JTextField txtFirmwareFile, JLabel lblVersion) {
        File f = new File(txtFirmwareFile.getText());
        if (f.canRead()) {
            String bundleVersion = SwitchUtility.getBundleVersion(f.getAbsolutePath());
            if (bundleVersion != null) {
                lblVersion.setText("Version: " + bundleVersion);
            } else {
                lblVersion.setText("(Unable to read firmware)");
            }
        } else {
            lblVersion.setText("(File not found)");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.sixnetio.swutil.SwitchUtilityUI#start()
     */
    @Override
    public int start() {
        int retVal = 0;
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    String title = "Switch Utility ";
                    if (SwitchUtility.PROG_VERSION.contains("@.@")) {
                        title += "(Unbuilt development instance)";
                    } else {
                        title += SwitchUtility.PROG_VERSION;
                    }

                    final JFrame frame = new JFrame(title);
                    frame.setResizable(false);
                    frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

                    // Choose the best look & feel
                    UIManager.LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
                    String preferredLAF;
                    if (Utils.osIsWindows()) {
                        preferredLAF = "Windows";
                    } else {
                        preferredLAF = "GTK+";
                    }

                    for (UIManager.LookAndFeelInfo laf : lafs) {
                        if (laf.getName().equals(preferredLAF)) {
                            try {
                                UIManager.setLookAndFeel(laf.getClassName());
                            } catch (Exception e) {
                                logger.warn("Unable to set look & feel", e);
                            }
                            break;
                        }
                    }

                    // Grab the Switch object for populating controls (there
                    // can only be one)
                    Switch swtch = program.getSwitches().get(0);

                    // Fields for the user to fill in
                    String bundlePath = program.getBundlePath();
                    if (bundlePath == null) {
                        bundlePath = config.getStringContents("bundle", "");
                    }

                    final JTextField txtFirmwareFile = new JTextField(bundlePath, 30);
                    final JComboBox cmbIPAddress;
                    {
                        JComboBox temp;
                        try {
                            Vector<String> addresses = new Vector<String>(
                                    IfcUtil.discoverLocalAddresses(false).keySet());
                            temp = new JComboBox(new DefaultComboBoxModel(addresses));
                        } catch (SocketException e) {
                            temp = new JComboBox();
                        }

                        cmbIPAddress = temp;

                        String localIP = swtch.detectLocalIP();
                        if (localIP == null) {
                            localIP = "";
                        }

                        cmbIPAddress.setSelectedItem(localIP);
                    }

                    String[] serialPorts;
                    try {
                        ComPort conxn = new ComPort();
                        serialPorts = conxn.getSerialPorts();
                    } catch (IOException ioe) {
                        logger.warn("Error getting list of serial ports", ioe);
                        serialPorts = null;
                    }

                    final JComboBox cmbSerialPort;
                    if (serialPorts == null) {
                        cmbSerialPort = new JComboBox();
                    } else {
                        cmbSerialPort = new JComboBox(serialPorts);
                    }

                    final JTextField txtSwitchIP = new JTextField(swtch.getSwitchIP(), 15);
                    final JTextField txtSwitchSubnet = new JTextField(swtch.getSwitchSubnet(), 15);
                    final JTextField txtSwitchGateway = new JTextField(swtch.getSwitchGateway(), 15);

                    final JComboBox cmbBaudRate = new JComboBox(new String[] { "9600", "115200" });

                    cmbBaudRate.setSelectedItem(String.format("%d", swtch.getBaudRate()));

                    // Control buttons
                    final JButton btnBrowse = new JButton("...");
                    final JButton btnLoad = new JButton("Load");
                    final JButton btnCancel = new JButton("Cancel");

                    // Random other controls
                    final JLabel lblVersion = new JLabel("Version: ?.?.?");
                    prgBar = new JProgressBar(0, 100);

                    // Panels
                    final JPanel pnlFirmwareFile = new JPanel();
                    final JPanel pnlThisComputer = new JPanel();
                    final JPanel pnlSwitchSettings = new JPanel();
                    final JPanel pnlControl = new JPanel();

                    String bundleVersion = SwitchUtility.getBundleVersion(program.getBundlePath());
                    if (bundleVersion != null) {
                        lblVersion.setText("Version: " + bundleVersion);
                    }

                    cmbIPAddress.setEditable(true);
                    cmbSerialPort.setEditable(true);

                    cmbSerialPort.setSelectedItem(swtch.getSerialPath());

                    prgBar.setStringPainted(true);

                    // Set the combo box sizes
                    FontMetrics metrics = cmbIPAddress.getFontMetrics(cmbIPAddress.getFont());
                    Dimension minSize = new Dimension(15 * metrics.charWidth('X'), 0);
                    cmbIPAddress.setMinimumSize(minSize);
                    cmbSerialPort.setMinimumSize(minSize);
                    cmbBaudRate.setMinimumSize(minSize);

                    pnlFirmwareFile.setBorder(BorderFactory.createTitledBorder("Firmware File"));
                    pnlThisComputer.setBorder(BorderFactory.createTitledBorder("This Computer"));
                    pnlSwitchSettings.setBorder(BorderFactory.createTitledBorder("Switch Settings"));
                    pnlControl.setBorder(BorderFactory.createTitledBorder("Program Control"));

                    // Labels with no visible text are used as spacers
                    buildLayout(frame, pnlFirmwareFile, txtFirmwareFile, btnBrowse, null, lblVersion, null,
                            pnlThisComputer, null, "IP Address:", cmbIPAddress, null, "Serial Port:", cmbSerialPort,
                            null, "Baud Rate: ", cmbBaudRate, null, pnlSwitchSettings, "IP Address:", txtSwitchIP, null,
                            "Subnet Mask:", txtSwitchSubnet, null, "Gateway IP (optional):", txtSwitchGateway, null,
                            pnlControl, null, " ", btnLoad, btnCancel, null, " ", null, "Progress:", prgBar, null);

                    txtFirmwareFile.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyReleased(KeyEvent e) {
                            updateFirmwareFileVersion(txtFirmwareFile, lblVersion);
                        }
                    });

                    // Make sure the label has the right version too
                    updateFirmwareFileVersion(txtFirmwareFile, lblVersion);

                    btnBrowse.addActionListener(new ActionListener() {
                        private JFileChooser chooser = new JFileChooser();
                        private boolean initialized = false;

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (!initialized) {
                                initialized = true;

                                chooser.setFileFilter(new FileNameExtensionFilter("Firmware Files", "fwb"));
                            }

                            int result = chooser.showOpenDialog(frame);
                            if (result == JFileChooser.APPROVE_OPTION) {
                                // Examine the bundle to determine the version
                                String bundleVersion = SwitchUtility
                                        .getBundleVersion(chooser.getSelectedFile().getAbsolutePath());
                                if (bundleVersion != null) {
                                    txtFirmwareFile.setText(chooser.getSelectedFile().getAbsolutePath());
                                    lblVersion.setText("Version: " + bundleVersion);
                                }
                            }
                        }
                    });

                    btnLoad.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            // Verify that all necessary properties are set
                            if (!verifyRequiredProperties()) {
                                return;
                            }

                            // Verify that the switch and computer will be able
                            // to talk to each other
                            if (!verifySubnets()) {
                                return;
                            }

                            // Spin off a new thread to save the config file
                            // This is in a separate thread because the current
                            // thread is in charge of GUI updates, and we really
                            // don't want to delay it more than necessary
                            Thread cfgSaver = new Thread("Config Saver") {
                                @Override
                                public void run() {
                                    config.removeContents();

                                    Tag bundleTag = new Tag("bundle");
                                    bundleTag.addContent(txtFirmwareFile.getText());
                                    config.addContent(bundleTag);

                                    try {
                                        Utils.xmlParser.writeXMLFile(config, getConfigFilePath());
                                    } catch (Exception e) {
                                        logger.warn("Unable to save config file", e);
                                    }
                                }
                            };
                            cfgSaver.start();

                            program.setBundlePath(txtFirmwareFile.getText());

                            // There is only one switch defined
                            Switch swtch = program.getSwitches().get(0);
                            swtch.setLocalIP(cmbIPAddress.getSelectedItem().toString());
                            swtch.setSerialPath(cmbSerialPort.getSelectedItem().toString());
                            swtch.setSwitchIP(txtSwitchIP.getText());
                            swtch.setBaudRate(Integer.parseInt(cmbBaudRate.getSelectedItem().toString()));
                            swtch.setSwitchSubnet(txtSwitchSubnet.getText());
                            swtch.setSwitchGateway(txtSwitchGateway.getText());

                            prgBar.setValue(0);
                            prgBar.setIndeterminate(true);

                            worker = new SwitchWorker(frame, btnLoad);
                            worker.addPropertyChangeListener(new PropertyChangeListener() {
                                @Override
                                public void propertyChange(PropertyChangeEvent e) {
                                    if (e.getPropertyName().equals("progress")) {
                                        prgBar.setIndeterminate(false);
                                        prgBar.setValue((Integer) e.getNewValue());
                                    }
                                }
                            });

                            btnLoad.setEnabled(false);
                            worker.execute();

                            // Make sure the config saver finished (probably has
                            // already)
                            try {
                                cfgSaver.join();
                            } catch (InterruptedException ie) {
                                logger.warn("Interrupted waiting for cfgSaver thread");
                            }
                        }

                        /**
                         * Verify that the user has specified all required
                         * properties. This alerts the user to any problems
                         * before returning.
                         * 
                         * @return <tt>true</tt> all necessary properties have
                         *         been provided; <tt>false</tt> at least one property
                         *         was missing.
                         */
                        private boolean verifyRequiredProperties() {
                            if (txtFirmwareFile.getText().length() == 0) {
                                txtFirmwareFile.requestFocusInWindow();
                                JOptionPane.showMessageDialog(frame, "The firmware file must be specified", "Error",
                                        JOptionPane.WARNING_MESSAGE);
                                return false;
                            } else if (cmbIPAddress.getSelectedItem() == null
                                    || cmbIPAddress.getSelectedItem().toString().length() == 0) {
                                cmbIPAddress.requestFocusInWindow();
                                JOptionPane.showMessageDialog(frame, "The computer's IP address must be specified",
                                        "Error", JOptionPane.WARNING_MESSAGE);
                                return false;
                            } else if (cmbSerialPort.getSelectedItem() == null
                                    || cmbSerialPort.getSelectedItem().toString().length() == 0) {
                                cmbSerialPort.requestFocusInWindow();
                                JOptionPane.showMessageDialog(frame, "The computer's serial port must be specified",
                                        "Error", JOptionPane.WARNING_MESSAGE);
                                return false;
                            } else if (txtSwitchIP.getText().length() == 0) {
                                txtSwitchIP.requestFocusInWindow();
                                JOptionPane.showMessageDialog(frame, "The switch's IP address must be specified",
                                        "Error", JOptionPane.WARNING_MESSAGE);
                                return false;
                            } else if (txtSwitchSubnet.getText().length() == 0) {
                                txtSwitchSubnet.requestFocusInWindow();
                                JOptionPane.showMessageDialog(frame, "The switch's subnet mask must be specified",
                                        "Error", JOptionPane.WARNING_MESSAGE);
                                return false;
                            }

                            return true;
                        }

                        /**
                         * Make sure that the computer and switch are on
                         * compatible subnets. This alerts the user to any
                         * problems before returning.
                         * 
                         * @return <tt>true</tt> = compatible subnets,
                         *         <tt>false</tt> = incompatible subnets.
                         */
                        private boolean verifySubnets() {
                            // This is the error message that will be displayed
                            // if the addresses are incompatible
                            String errmsg = "The configured addresses for the" + " computer and switch are"
                                    + " incompatible:\n" + "The computer will be unable to"
                                    + " communicate with the switch" + " using the chosen address.";

                            InetAddress computerAddress;
                            InetAddress switchAddress;
                            try {
                                computerAddress = InetAddress.getByName(cmbIPAddress.getSelectedItem().toString());
                                switchAddress = InetAddress.getByName(txtSwitchIP.getText());
                            } catch (UnknownHostException uhe) {
                                logger.error("Unable to look up InetAddresses for computer/switch", uhe);

                                JOptionPane.showMessageDialog(frame,
                                        "Unable to determine addresses of computer and switch: " + uhe.getMessage(),
                                        "Error", JOptionPane.ERROR_MESSAGE);
                                return false;
                            }

                            short switchNetworkPrefixLength = Utils.prefixFromSubnet(txtSwitchSubnet.getText());

                            // Make sure the computer can reach the switch
                            // This may not be possible if the user typed in an
                            // address, as we will have no subnet information on
                            // it
                            try {
                                Map<String, InterfaceAddress> localAddresses;

                                localAddresses = IfcUtil.discoverLocalAddresses(false);

                                InterfaceAddress chosenAddress = localAddresses
                                        .get(cmbIPAddress.getSelectedItem().toString());

                                // Skip the compatibility test if the network
                                // prefix length is 128; that is a Windows
                                // specific JVM bug, and it would always cause
                                // the test to fail (or possibly throw an
                                // exception, if the two addresses were equal)
                                if (chosenAddress != null && chosenAddress.getNetworkPrefixLength() != 128) {

                                    logger.debug("Comparing addresses with computer subnet "
                                            + chosenAddress.getNetworkPrefixLength());
                                    if (!IfcUtil.sameSubnet(computerAddress, switchAddress,
                                            chosenAddress.getNetworkPrefixLength())) {
                                        logger.error("Computer's subnet restricts access to switch's address");
                                        JOptionPane.showMessageDialog(frame, errmsg, "Error",
                                                JOptionPane.WARNING_MESSAGE);
                                        return false;
                                    }
                                }
                            } catch (SocketException se) {
                                logger.warn("Unable to determine subnet for the computer's chosen IP address", se);

                                // Allow this to continue, don't want to prevent
                                // the user from doing something
                            }

                            // Now, make sure the switch can reach the computer
                            logger.debug("Comparing addresses with switch subnet " + switchNetworkPrefixLength);
                            if (!IfcUtil.sameSubnet(computerAddress, switchAddress, switchNetworkPrefixLength)) {
                                logger.error("Switch's subnet restricts access to computer's address");
                                JOptionPane.showMessageDialog(frame, errmsg, "Error", JOptionPane.WARNING_MESSAGE);
                                return false;
                            }

                            return true;
                        }
                    });

                    btnCancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (worker != null) {
                                worker.cancel(true);
                                worker = null;
                                btnLoad.setEnabled(true);

                                JOptionPane.showMessageDialog(frame, "The running update has been canceled", "Canceled",
                                        JOptionPane.WARNING_MESSAGE);
                            } else {
                                frame.setVisible(false);
                                frame.dispose();

                                new Suicide();
                            }
                        }
                    });

                    frame.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent e) {
                            btnCancel.doClick();
                        }
                    });

                    frame.pack();
                    frame.setVisible(true);
                }

                /**
                 * Build the layout for the given frame.
                 * 
                 * @param frame
                 *            The frame to build.
                 * @param controls
                 *            Contains panels and rows of controls. The
                 *            controls following a panel will be placed within that
                 *            panel. End a row with a <tt>null</tt> (don't forget to
                 *            include one at the end of the last row). Strings will be
                 *            automatically converted into labels. A <tt>null</tt>
                 *            immediately after a panel will end the row of panels.
                 */
                private void buildLayout(JFrame frame, Object... controls) {
                    GridBagLayout frameLayout = new GridBagLayout();
                    frame.setLayout(frameLayout);

                    GridBagConstraints gc = new GridBagConstraints();
                    gc.anchor = GridBagConstraints.WEST;

                    JPanel currentPanel = null;
                    GridBagLayout currentLayout = null;

                    for (int i = 0; i < controls.length; i++) {
                        Object c = controls[i];

                        if (c == null) {
                            logger.debug("End of row");
                            continue;
                        } else if (c instanceof String) {
                            // Turn it into a label
                            logger.debug("Converting a String into a JLabel");
                            c = new JLabel((String) c);
                        }

                        if (controls[i + 1] == null) {
                            // Last item in a row, fill to the end
                            gc.gridwidth = GridBagConstraints.REMAINDER;
                        } else {
                            gc.gridwidth = 1;
                        }

                        if (c instanceof JPanel) {
                            logger.debug("New panel");

                            currentPanel = (JPanel) c;
                            currentLayout = new GridBagLayout();
                            currentPanel.setLayout(currentLayout);

                            // Panels fill their allocated area
                            gc.fill = GridBagConstraints.BOTH;

                            frameLayout.setConstraints(currentPanel, gc);
                            frame.add(currentPanel);
                        } else if (currentPanel == null) {
                            logger.debug("No current panel, failing");
                            throw new RuntimeException("Panel not set yet");
                        } else {
                            logger.debug("Control: " + c.getClass().getName());

                            // Components remain at their preferred size
                            gc.fill = GridBagConstraints.NONE;

                            // Add to the panel
                            currentLayout.setConstraints((Component) c, gc);
                            currentPanel.add((Component) c);
                        }
                    }
                }
            });
        } catch (Exception e) {
            logger.error("Error initializing GUI", e);

            if (e instanceof InvocationTargetException && e.getCause() instanceof UnsatisfiedLinkError) {

                // Special case to give the user more information
                logger.error("java.library.path = " + System.getProperty("java.library.path"));
                JOptionPane.showMessageDialog(null, "Unable to load the comport library.", "Error",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Unable to initialize interface", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
            // If we encounter a fatal error we set retVal to
            // a failing return code.
            retVal = 1;
        }
        return retVal;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.sixnetio.swutil.SwitchUtilityUI#switchFailed(com.sixnetio.swutil.Switch,
     * java.lang.Throwable)
     */
    @Override
    public void switchFailed(Switch swtch, final Throwable why) {
        logger.error("Failed to load switch", why);

        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    prgBar.setIndeterminate(false);
                    prgBar.setValue(prgBar.getMinimum());

                    worker.exception = why;

                    JOptionPane.showMessageDialog(null, why.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            });
        } catch (Exception e) {
            // This should not happen, just percolate a failure upwards
            logger.error("Unexpected exception", e);
            throw new RuntimeException("Unexpected exception: " + e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.sixnetio.swutil.SwitchUtilityUI#turnOffSwitch(com.sixnetio.swutil.Switch)
     */
    @Override
    public void turnOffSwitch(Switch swtch) {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    JOptionPane.showMessageDialog(null, "Please turn OFF the switch and then click OK", "Power Control",
                            JOptionPane.WARNING_MESSAGE);
                }
            });
        } catch (Exception e) {
            // This should not happen, just percolate a failure upwards
            logger.error("Unexpected exception", e);
            throw new RuntimeException("Unexpected exception: " + e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.sixnetio.swutil.SwitchUtilityUI#turnOnSwitch(com.sixnetio.swutil.Switch)
     */
    @Override
    public void turnOnSwitch(Switch swtch) {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    JOptionPane.showMessageDialog(null, "Please turn ON the switch and then click OK", "Power Control",
                            JOptionPane.WARNING_MESSAGE);
                }
            });
        } catch (Exception e) {
            // This should not happen, just percolate a failure upwards
            logger.error("Unexpected exception", e);
            throw new RuntimeException("Unexpected exception: " + e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.sixnetio.swutil.SwitchUtilityUI#progressUpdated(int, float)
     */
    @Override
    public void progressUpdated(int state, float progress) {
        if (worker == null) {
            // Not interested if there is no worker (this should not happen)
            return;
        }

        // This is the value that we would set if SwingWorker would accept
        // ranges other than 0-100
        // This gets scaled later
        int value;

        switch (state) {
        case FirmwareLoaderUI.S_APPLY_POWER:
            value = 2;
            break;
        case FirmwareLoaderUI.S_ARCH:
            value = 4;
            break;
        case FirmwareLoaderUI.S_EXTRACT_FILES:
            value = 6;
            break;
        case FirmwareLoaderUI.S_SETUP:
            value = 8;
            break;
        case FirmwareLoaderUI.S_LOAD_IMAGE:
            if (progress >= -0.0001) {
                value = 10 + (int) progress;
            } else {
                value = 110;
            }
            break;
        case FirmwareLoaderUI.S_FLASH_IMAGE:
            value = 110;
            break;
        case FirmwareLoaderUI.S_LOAD_BOOT_IMAGE:
            if (progress >= -0.0001) {
                value = 112 + (int) progress;
            } else {
                value = 212;
            }
            break;
        case FirmwareLoaderUI.S_FLASH_BOOT_IMAGE:
            value = 214;
            break;
        case FirmwareLoaderUI.S_WAIT:
            value = 216;
            break;
        case FirmwareLoaderUI.S_COMPLETE:
            value = 218;
            break;
        default:
            logger.debug("Unknown state: " + state);
            value = 0;
            break;
        }

        worker.setWorkerProgress(100 * value / 218);
    }
}

@echo off
set /p LICENSEKEY= Enter the path to the license file to install:

java -jar BVBatch.jar -s localhost -p PostgreSQL -d BlueVueBatch -u postgres -x --init --license "%LICENSEKEY%" -g "%ALLUSERSPROFILE%\Application Data\BlueVueBatch" --plugin "%ALLUSERSPROFILE%\Application Data\BlueVueBatch\BVBDatabases\PostgreSQLDatabase.jar"

pause
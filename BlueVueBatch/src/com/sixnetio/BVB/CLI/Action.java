/*
 * Action.java
 *
 * An action that may be taken by the CLI.
 *
 * Jonathan Pearson
 * February 4, 2009
 *
 */

package com.sixnetio.BVB.CLI;

import java.io.*;
import java.lang.reflect.Constructor;
import java.text.ParseException;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import com.sixnetio.BVB.Common.DuplicateRegistrationError;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public abstract class Action {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private static Map<String, Class<? extends Action>> actions =
	        new HashMap<String, Class<? extends Action>>();
	
	/**
	 * Register a class that can perform the specified action.
	 * 
	 * @param name The name of the action it can support.
	 * @param actionClass The class that supports it.
	 */
	public static void registerAction(String name, Class<? extends Action> actionClass) {
		if (actionClass == null) {
			throw new NullPointerException("actionClass may not be null");
		}
		
		logger.debug(String.format("Registering action '%s' with class '%s'", name,
		                           actionClass.getName()));
		
		synchronized (actions) {
			if (actions.put(name, actionClass) != null) {
				throw new DuplicateRegistrationError(
				              "Duplicate action with name '" + name + "'");
			}
		}
	}
	
	/**
	 * Get a set of supported actions.
	 */
	public static Set<String> getKnownActions() {
		synchronized (actions) {
			return new HashSet<String>(actions.keySet());
		}
	}
	
	/**
	 * Construct a new action.
	 * 
	 * @param name The name of the action.
	 * @param db A connected database object.
	 * @return A new Action.
	 * @throws BadActionException If the action is unsupported or cannot be
	 *             created.
	 */
	public static Action makeAction(String name) throws BadActionException {
		NDC.push(String.format("name = '%s'", name));
		
		try {
			logger.debug("Attempting to construct a new Action");
			
			Class<? extends Action> actionClass;
			synchronized (actions) {
				actionClass = actions.get(name);
			}
			
			if (actionClass == null) {
				logger.error("Action not registered");
				
				throw new BadActionException("Unknown action", name);
			}
			
			try {
				logger.debug(String.format("Calling constructor of '%s'", actionClass.getName()));
				
				Constructor<? extends Action> constructor =
				        actionClass.getConstructor(String.class);
				return constructor.newInstance(name);
			} catch (Exception e) {
				// NoSuchMethodException, SecurityException (getConstructor)
				// InstantiationException or IllegalAccessException,
				// IllegalArgumentException, InvocationTargetException
				// (newInstance)
				logger.error(String.format("Unable to instantiate action class '%s'", name), e);
				
				throw new BadActionException("Unable to instantiate action", name, e);
			}
		} finally {
			NDC.pop();
		}
	}
	
	private String actionName;
	
	/**
	 * Construct a new action object.
	 * 
	 * @param name The name of the action.
	 */
	public Action(String name) {
		this.actionName = name;
	}
	
	/**
	 * Get the action name.
	 */
	public String getActionName() {
		return actionName;
	}
	
	/**
	 * Returns "Action: [action name]".
	 */
	@Override
	public String toString() {
		return String.format("Action: %s", getActionName());
	}
	
	/**
	 * Get a description of the extra usage arguments required by this action.
	 * These must be bare arguments, no option switches.
	 */
	public abstract String getUsage();
	
	/**
	 * Run this action.
	 * 
	 * @param db The database to act against.
	 * @param args Contains all bare arguments that were passed on the command
	 *            line, in the order they were passed.
	 */
	public abstract void activate(Database db, String[] args) throws BadParameterException,
	        DatabaseException;
	
	protected LineNumberReader makeReader(String fileName)
		throws BadParameterException {
		    LineNumberReader in;
		    if (fileName.equals("-")) {
		        in = new LineNumberReader(new InputStreamReader(System.in));
		    }
		    else {
		        try {
		    	in = new LineNumberReader(new FileReader(fileName));
		        } catch (IOException ioe) {
		    	throw new BadParameterException(String.format(
		    		"Unable to open file '%s': %s", fileName, ioe
		    			.getMessage()), ioe);
		        }
		    }
		    return in;
		}

	protected Integer parseIntColumn(String columnName, boolean required, CSVParser parser,
            String name)
            throws BadParameterException {
            	Integer value;
            	
            	String s = nullAsNecessary(parser.getColumn(columnName));
            	if (s == null) {
            		if (required) {
            			throw new BadParameterException(String.format(
            					"Bad file format: line %d had no %s",
            					parser.getLineNumber(),name));
            		}
            		else {
            			value = null;
            		}
            	}
            	else {
            		try {
            			value = Integer.parseInt(s);
            		} catch (NumberFormatException nfe) {
            			throw new BadParameterException(
            			        String.format("Bad file format: line %d has a non-numeric %s",
            			                        parser.getLineNumber(), name));
            		}
            	}
            	return value;
            }

	protected Long parseLongColumn(String columnName, boolean required, CSVParser parser,
            String name)
            throws BadParameterException {
            	Long value;
            	
            	String s = nullAsNecessary(parser.getColumn(columnName));
            	if (s == null) {
            		if (required) {
            			throw new BadParameterException(String.format(
            					"Bad file format: line %d had no %s",
            					parser.getLineNumber(),name));
            		}
            		else {
            			value = null;
            		}
            	}
            	else {
            		try {
            			value = Long.parseLong(s);
            		} catch (NumberFormatException nfe) {
            			throw new BadParameterException(
            			        String.format("Bad file format: line %d has a non-numeric %s",
            			                        parser.getLineNumber(), name));
            		}
            	}
            	return value;
            }

	protected Boolean parseBoolColumn(String columnName, boolean required, CSVParser parser,
            String name)
            throws BadParameterException {
            	Boolean value;
            	
            	String s = nullAsNecessary(parser.getColumn(columnName));
            	if (s == null) {
            		if (required) {
            			throw new BadParameterException(String.format(
            					"Bad file format: line %d had no %s",
            					parser.getLineNumber(),name));
            		}
            		else {
            			value = false;
            		}
            	}
            	else {
            		value = Boolean.parseBoolean(s);
            	}
            	return value;
            }

	protected String parseStringColumn(String columnName, boolean required, CSVParser parser,
            String name)
            throws BadParameterException {
            	String value;
            	
            	String s = nullAsNecessary(parser.getColumn(columnName));
            	if (s == null) {
            		if (required) {
            			throw new BadParameterException(String.format(
            					"Bad file format: line %d had no %s",
            					parser.getLineNumber(),name));
            		}
            		else {
            			value = null;
            		}
            	}
            	else {
            		value = s; // No parsing necessary.
            	}
            	return value;
            }

	protected Date parseDateColumn(String columnName, boolean required, CSVParser parser,
            String name)
            throws BadParameterException {
            	Date value;
            	String s = nullAsNecessary(parser.getColumn(columnName));
            	if (s == null) {
            		if (required) {
            			throw new BadParameterException(String.format(
            					"Bad file format: line %d had no %s",
            					parser.getLineNumber(), name));
            		}
            		else {
            			value = null;
            		}
            	}
            	else {
            		try {
            			value = Utils.parseStandardDateTime(s);
            		} catch (ParseException e) {
            			throw new BadParameterException(
            					String.format("Bad file format: line %d had a badly formatted %s",
            							parser.getLineNumber(), name));
            		}
            	}
            	return value;
            }

	/**
	 * If s is <tt>null</tt> or empty, returns <tt>null</tt>.
	 */
	protected static String nullAsNecessary(String s) {
		if (s == null || s.length() == 0) s = null;
		
		return s;
	}
}

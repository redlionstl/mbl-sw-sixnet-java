/*
 * BVBatch.java
 *
 * The driver for the CLI. Performs command-line
 * parsing and calls out to other classes to perform
 * operations.
 *
 * Jonathan Pearson
 * February 4, 2009
 *
 */

package com.sixnetio.BVB.CLI;

import java.io.*;
import java.util.List;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.martiansoftware.jsap.*;
import com.martiansoftware.util.StringUtils;
import com.sixnetio.BVB.Common.*;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.io.PasswordReader;
import com.sixnetio.io.UnbufferedInputStream;
import com.sixnetio.util.Utils;

public class BVBatch
{
    private static final Logger logger;

    public static final String PROG_VERSION =
        "@bvg.major@.@bvg.minor@.@bvg.build@";

    // Set to true to read passwords straight from stdin
    private static boolean plainPasswd = false;

    private static String dbProvider;
    private static String dbServer;
    private static String dbName;
    private static String dbUser;
    private static String dbPasswd;

    private static final String CFGFILE;

    static {
        CFGFILE = Server.configureInstance("bvbatch");

        logger = Logger.getLogger(Utils.thisClassName());
    }

    public static void main(String[] args)
    {
        SimpleJSAP jsap;

        try {
            jsap = createJsap();
        }
        catch (JSAPException jsape) {
            logger.fatal("JSAP failed to initialize.", jsape);
            System.exit(1);
            return;
        }

        JSAPResult params = jsap.parse(args);

        // If a basic parameter (like version) is handled, we're done.
        if (basicParameter(jsap, params)) {
            return;
        }

        boolean initializing = params.getBoolean("init");
        boolean upgrading = params.getBoolean("dbupgrade");
        String licenseKeyFile = params.getString("licenseKey");

        if (initializing && licenseKeyFile == null) {
            logger.fatal("Unable to start: Initializing a new installation "
                         + "without a license key.");

            System.err.println("You must provide a license key with --license "
                               + "to initialize a new installation.");
            System.err.println("Use the --help switch for usage information");

            System.exit(1);
            return;
        }

        if (initializing && upgrading) {
            logger.fatal("Cannot both initialize and upgrade a database at"
                         + " the same time.");
            System.err.println("You cannot both initialize and upgrade a"
                               + " database at the same time.");
            System.err.println("Use the --help switch for usage information");

            System.exit(1);
            return;
        }

        // If not initializing or upgrading, but the user specified a license
        // key, we are replacing the existing license key with a new one
        boolean replacingKey =
            (!initializing && licenseKeyFile != null && !upgrading);


        // Require a configuration file, unless we are initializing
        if (params.getString("cfgfile") == null && !initializing) {
            logger.fatal("Unable to start: No configuration file found");

            System.err.println("Unable to locate a configuration file.");
            System.err.println("Use the --help switch for usage information");

            System.exit(1);
            return;
        }

        // Load the configuration file, if there is one
        Tag config = getConfig(params);
        if (config == null) {
            // If there was an error, it was printed by getConfig()
            return;
        }

        if (!handlePlugins(params, initializing, config)) {
            return;
        }

        // This cannot be done by a plugin because the plugins directory may
        // have been empty -- result would be an error (action not found) rather
        // than an empty list, letting the user know that the configuration is
        // screwed up
        if (params.getBoolean("listActions")) {
            // Just list the available actions and exit
            System.out.println("Available actions:");

            for (String name : Utils.sortStrings(Action.getKnownActions())) {
                System.out.printf("  %s\n", name);
            }

            return;
        }

        // If we get here, make sure the user specified an action to take, is
        // initializing a new database, or is replacing the license key
        String actionName = params.getString("action");
        if (actionName == null && !initializing && !replacingKey && !upgrading) {
            System.err.println("You must provide the name of an action, "
                               + "or pass '-l' to list");
            System.err.println("Use the --help switch for usage information");

            System.exit(1);
            return;
        }

        if (!getDbParams(params, config)) {
            return;
        }

        // Construct the database
        Database db = dbConstruct();
        if (db == null) {
            return;
        }

        // Perform the action
        // Problem is, once we connect to the database, we don't want to just
        // call System.exit because that won't let us disconnect from the
        // database in the finally block
        int exitCode = 0;

        try {
            if (initializing) {
                initialize(params, licenseKeyFile, db);
            }
            else if (upgrading) {
                upgradeDatabase(params, licenseKeyFile, db);
            }
            else if (replacingKey) {
                replaceKey(params, licenseKeyFile, db);
            }
            else {
                if (!takeAction(params, actionName, db)) {
                    throw new BadParameterException("Action not found: " +
                                                    actionName);
                }
            }
        }
        catch (BadParameterException bpe) {
            logger.fatal("Unable to perform action", bpe);

            System.err.printf("Unable to perform action: %s\n", bpe
                .getMessage());
            System.err.println("Use the --help switch for usage information");
            exitCode = 1;
        }
        catch (DatabaseException de) {
            logger.fatal("Database error while performing action", de);

            System.err.printf("Database error while performing action: %s\n",
                de.getMessage());
            exitCode = 1;
        }
        catch (IOException ioe) {
            logger.fatal("IO error while performing action", ioe);

            System.err.printf("Error: %s\n", ioe.getMessage());
            exitCode = 1;
        }
        finally {
            try {
                db.disconnect();
            }
            catch (DatabaseException e) {
                // Ignore it
            }
        }

        System.exit(exitCode);
    }

    /**
     * Upgrade the given database (not yet connected) to the expected schema.
     * 
     * @param params The JSAP parameters, in case any additional information is
     *            necessary.
     * @param licensekeyFile The license key file path. This is necessary, as
     *            the only guaranteed setting in the database between schemas is
     *            the version.
     * @param db The database.
     * @throws DatabaseException If there was a problem connecting to or
     *             upgrading the database.
     * @throws BadParameterException If the user did not provide a license key.
     */
    private static void upgradeDatabase(JSAPResult params,
                                        String licenseKeyFile, Database db)
        throws DatabaseException, BadParameterException
    {
        if (licenseKeyFile == null) {
            throw new BadParameterException(
                "License key required to perform database upgrade");
        }

        LicenseKey licenseKey = getLicense(licenseKeyFile);
        if (licenseKey == null) {
            return;
        }
        db.connect(dbServer, dbName, dbUser, dbPasswd, true, licenseKey,
            Database.ConnectionReason.Client);

        db.upgradeDatabase();

        System.out.println("Database upgraded successfully");
    }

    // Grab the database connectivity information
    private static boolean getDbParams(JSAPResult params, Tag config)
    {
        plainPasswd = params.getBoolean("plainPasswd");

        dbProvider =
            getParameter(params, "provider", config, "database/provider",
                "Database provider not specified");

        dbServer =
            getParameter(params, "server", config, "database/server",
                "Database server not specified");

        dbName =
            getParameter(params, "database", config, "database/name",
                "Database name not specified");

        dbUser =
            getParameter(params, "dbuser", config, "database/user",
                "Database user not specified");

        dbPasswd = getDbPasswd(params, config);
        if (dbPasswd == null) {
            return false;
        }
        return true;
    }

    private static boolean takeAction(JSAPResult params, String actionName,
                                      Database db)
        throws DatabaseException, BadParameterException
    {

        Action action = makeAction(params, actionName);
        if (action == null) {
            return false;
        }

        if (params.getBoolean("actionHelp")) {
            // User just wants help with this action
            System.out.println(action.getUsage());
            return true;
        }

        db.connect(dbServer, dbName, dbUser, dbPasswd, false, null,
            Database.ConnectionReason.Client);
        // Make sure we're using accurate server settings
        Server.refreshSettingsNow(db);

        action.activate(db, params.getStringArray("params"));
        return true;
    }

    /**
     * Handle basic parameters (or parameter failure)
     * 
     * @param jsap
     * @param params Parameters to test and process
     * @return True if parameter is handled (called should likely return), false
     *         otherwise.
     */
    private static boolean basicParameter(SimpleJSAP jsap, JSAPResult params)
    {
        if (!params.success()) {
            // Screen width of 80 is standard, and we can't detect what it is
            // actually set at, so go with 80
            jsap.setScreenWidth(80);

            String usage = getUsage(jsap);
            System.err.println(usage);
            System.err.println(jsap.getHelp());
            System.exit(1);
            return true;
        }
        else if (params.getBoolean("help")) {
            // Output in this case was handled by JSAP without any extra work
            return true;
        }
        else if (params.getBoolean("version")) {
            System.out.println(Server.copyrightBanner("BlueVue Batch",
                PROG_VERSION));
            return true;
        }
        return false;
    }

    private static LicenseKey getLicense(String licenseKeyFile)
    {
        LicenseKey licenseKey;
        // Load the license file
        try {
            licenseKey = new LicenseKey(new File(licenseKeyFile));
        }
        catch (IOException ioe) {
            logger.fatal("Unable to read license key", ioe);

            System.err.println("Unable to read the license key: " +
                               ioe.getMessage());
            System.err.println("Use the --help switch for usage information");
            System.exit(1);
            return null;
        }

        if (!licenseKey.isValid()) {
            logger.fatal("License key is invalid");

            System.err.println("Unable to validate license key");
            System.err.println("Please contact the provider for a new one");
            System.exit(1);
            return null;
        }
        return licenseKey;
    }

    private static Action makeAction(JSAPResult params, String actionName)
    {
        Action action;
        try {
            action = Action.makeAction(actionName);
        }
        catch (BadActionException bae) {
            logger.fatal("Unable to create action", bae);

            System.err.println("Unable to perform action: " + bae.getMessage());
            System.err.println("Use the --help switch for usage information");
            System.exit(1);
            return null;
        }

        return action;
    }

    private static boolean handlePlugins(JSAPResult params,
                                         boolean initializing, Tag config)
    {

        // If a plugin is specified, only load that one
        // Otherwise, we could have duplicate plugin errors from loading the
        // configuration file
        if (initializing && params.getString("pluginfile") != null) {
            // Trick Initializer into loading this extra plugin file
            Tag tag = new Tag("plugins");
            Tag subTag = new Tag("plugin");
            subTag.addContent(params.getString("pluginfile"));

            tag.addContent(subTag);

            try {
                Initializer.loadPlugins(tag);
            }
            catch (Throwable th) {
                logger.fatal(
                    "Unable to start: Cannot load user-specified plugin", th);

                System.err.println("Unable to load specified plugin file: " +
                                   th.getMessage());

                System.err
                    .println("Use the --help switch for usage information");

                System.exit(1);
                return false;
            }
        }
        else if (params.getBoolean("autoload")) {
            // Load all the necessary classes specified in the configuration
            // file
            try {
                Initializer.loadClasses(config.getChild("autoload"));
            }
            catch (Throwable th) {
                logger.fatal("Unable to start: Cannot load class", th);

                System.err.println("Unable to load class: " + th.getMessage());
                System.exit(1);
                return false;
            }
        }
        else {
            // Start up a plugin watcher and wait for it to perform an update
            try {
                Initializer.loadPlugins(config.getChild("plugins"));
            }
            catch (Throwable th) {
                logger.fatal("Unable to start: Cannot load plugins", th);

                System.err
                    .println("Unable to load plugins: " + th.getMessage());
                System.exit(1);
                return false;
            }
        }

        return true;
    }

    private static SimpleJSAP createJsap()
        throws JSAPException
    {

        SimpleJSAP jsap;
        jsap =
            new SimpleJSAP(
                "BVBatch",
                "Command-line control of the BVB program suite",
                new Parameter[] {
                    // Provide a special config file
                    // instead of the standard
                    // This may include connection data
                    // for the database
                    new FlaggedOption("cfgfile", JSAP.STRING_PARSER, CFGFILE,
                        JSAP.NOT_REQUIRED, 'c', "cfgfile",
                        "Specify the path to the configuration file."),

                    new Switch(
                        "autoload",
                        JSAP.NO_SHORTFLAG,
                        "autoload",
                        "Load classes in the 'autoload' section of "
                            + "the config file, instead of scanning for plugins."),

                    new Switch("plainPasswd", JSAP.NO_SHORTFLAG, "plainPasswd",
                        "Passwords will be read straight from stdin, "
                            + "without any obfuscation of the characters."),

                    new Switch("version", 'v', "version",
                        "Display the version number of this program "
                            + "and exit"),

                    // Arguments for connecting to a
                    // database server
                    // The password will be requested
                    // interactively if not provided
                    new FlaggedOption("server", JSAP.STRING_PARSER,
                        JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 's', "dbserver",
                        "Specify the database server."),

                    new FlaggedOption("provider", JSAP.STRING_PARSER,
                        JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'p', "dbprovider",
                        "Specify the provider of the database."),

                    new FlaggedOption("database", JSAP.STRING_PARSER,
                        JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'd', "database",
                        "Specify the name of the database."),

                    new FlaggedOption("dbuser", JSAP.STRING_PARSER,
                        JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'u', "dbuser",
                        "Specify the user name for the database."),

                    new FlaggedOption("dbpasswd", JSAP.STRING_PARSER,
                        JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'w', "dbpasswd",
                        "Specify the database password on the "
                            + "command line, rather than trying to "
                            + "find it in the config file first. If "
                            + "this is omitted and the config file "
                            + "does not include the password, it "
                            + "will be requested."),

                    new Switch("provideDBPasswd", 'x', "askForPassword",
                        "Specify that the database password should be "
                            + "asked for interactively."),

                    // Arguments for actual actions that
                    // may be taken
                    new FlaggedOption("action", JSAP.STRING_PARSER,
                        JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'a', "action",
                        "Specify the action to perform. See "
                            + "the '-l' option for more info."),

                    new UnflaggedOption("params", JSAP.STRING_PARSER,
                        JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, true,
                        "Specify extra parameters to the action."),

                    new Switch("listActions", 'l', "listActions",
                        "List available actions and stop."),

                    new Switch("actionHelp", 'e', "actionHelp",
                        "Get usage information for the action named "
                            + "by '-a'."),

                    new Switch("init", JSAP.NO_SHORTFLAG, "init",
                        "Initialize a new database. The user that you "
                            + "specify must be a database superuser (must "
                            + "have permission to  create databases and "
                            + "users). You must provide a license key file "
                            + "(see the --license option) to use this."),

                    new FlaggedOption("licenseKey", JSAP.STRING_PARSER,
                        JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, JSAP.NO_SHORTFLAG,
                        "license", "Specify the license key to use for "
                                   + "this installation. If --init is not "
                                   + "specified, the existing license key "
                                   + "will be replaced with the key "
                                   + "specified. The key you specify must "
                                   + "be valid."),

                    new FlaggedOption("cfgdir", JSAP.STRING_PARSER,
                        JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'g', "gencfg",
                        "If initializing, automatically "
                            + "generate configuration files and "
                            + "place them in the specified directory."),

                    new FlaggedOption("pluginfile", JSAP.STRING_PARSER,
                        JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, JSAP.NO_SHORTFLAG,
                        "plugin", "If initializing without a "
                                  + "configuration file, pass in the full "
                                  + "path to the database plugin file with "
                                  + "this option. You must still provide "
                                  + "the -p option."),

                    new Switch("dbupgrade", JSAP.NO_SHORTFLAG, "dbupgrade",
                        "Update an existing, older format"
                            + " database to the current" + " version."),
                });

        return jsap;
    }

    private static Tag getConfig(JSAPResult params)
    {
        Tag config;
        if (params.getString("cfgfile") == null) {
            // Empty config
            config = new Tag("bvb");
        }
        else {
            try {
                config =
                    Utils.xmlParser.parseXML(params.getString("cfgfile"),
                        false, false);
            }
            catch (IOException ioe) {
                logger.fatal("Unable to start: Bad configuration file", ioe);

                System.err.println("Unable to load configuration file: " +
                                   ioe.getMessage());
                System.err
                    .println("Use the --help switch for usage information");

                System.exit(1);
                return null;
            }
        }
        return config;
    }

    private static String getDbPasswd(JSAPResult params, Tag config)
    {
        String dbPasswd;
        if (params.getBoolean("provideDBPasswd")) {
            // User said he'd provide the password when asked
            // Make sure he didn't also provide it on the command line
            if (params.getString("dbpasswd") != null) {
                System.err
                    .println("Choose one of the -w and -x options, not both.");
                System.err
                    .println("Use the --help switch for usage information");
                System.exit(1);
                return null;
            }

            dbPasswd = readPassword("Database password: ");
        }
        else if (params.getString("dbpasswd") != null) {
            // User specified the password on the command line
            dbPasswd = params.getString("dbpasswd");
        }
        else {
            // No password provided, check the configuration file
            dbPasswd = config.getStringContents("database/password");

            if (dbPasswd == null) {
                // Not in the config file, ask for it
                dbPasswd = readPassword("Database password: ");
            }
        }
        return dbPasswd;
    }

    private static Database dbConstruct()
    {
        Database db;
        try {
            db = Database.getInstance(dbProvider);
        }
        catch (BadProviderException bpe) {
            logger.fatal("Unable to start: Bad database provider", bpe);

            System.err.println("Unable to load database: " + bpe.getMessage());
            System.exit(1);
            return null;
        }
        catch (DriverException de) {
            logger.fatal("Unable to start: Cannot load database driver", de);

            System.err.println("Unable to load database driver: " +
                               de.getMessage());
            System.exit(1);
            return null;
        }
        return db;
    }

    private static void initialize(JSAPResult params, String licenseKeyFile,
                                   Database db)
        throws DatabaseException, IOException
    {
        LicenseKey licenseKey = getLicense(licenseKeyFile);
        if (licenseKey == null) {
            return;
        }
        db.connect(dbServer, null, dbUser, dbPasswd, true, licenseKey,
            Database.ConnectionReason.Client);

        // Ask for the name of the server user
        String serverUser = readLine("Server user name: ");
        String serverPassword = null;

        // Ask whether we need to create the user
        boolean createServerUser = getConsoleYesNo("Create server user?", true);

        if (createServerUser) {
            while (serverPassword == null) {
                serverPassword = confirmPassword("Server user password: ");

                if (!serverPassword.equals(serverPassword.trim())) {
                    System.out
                        .println("The password may not start or end with a "
                                 + "space character");
                    serverPassword = null;
                }
            }

            System.out.print("Creating the server user...");
            db.createDatabaseUser(serverUser, serverPassword);
            System.out.println("done.");
        }

        // Ask whether a client user should exist
        boolean separateClientUser =
            getConsoleYesNo("Separate client/server accounts?", true);

        String clientUser = null;
        String clientPassword = null;

        if (separateClientUser) {
            clientUser = readLine("Client user name: ");

            boolean createClientUser =
                getConsoleYesNo("Create client user?", true);

            if (createClientUser) {
                while (clientPassword == null) {
                    clientPassword = confirmPassword("Client user password: ");

                    if (!clientPassword.equals(clientPassword.trim())) {
                        System.out
                            .println("The password may not start or end with "
                                     + "a space character");
                        clientPassword = null;
                    }
                }

                System.out.print("Creating the client user...");
                db.createDatabaseUser(clientUser, clientPassword);
                System.out.println("done.");
            }
        }
        else {
            clientUser = serverUser;
        }

        // Create the database
        System.out.print("Creating database...");
        db.createDatabase(dbName, serverUser, clientUser);
        System.out.println("done.");

        // Disconnect/reconnect
        System.out.print("Connecting to the new database...");
        db.disconnect();
        // There is no license key in the database yet, so we need to
        // continue passing it in
        db.connect(dbServer, dbName, dbUser, dbPasswd, true, licenseKey,
            Database.ConnectionReason.Client);
        System.out.println("done.");

        System.out.print("Creating tables...");
        db.initializeNewDatabase(dbName, serverUser, clientUser, licenseKey);
        System.out.println("done.");

        // Ask for a password for the root user
        String rootPassword =
            confirmPassword("Please choose a root password for the web interface: ");

        System.out.print("Creating a 'Root' user...");

        User user =
            new User(User.INV_USER, "Root", User.hashPassword("Root",
                rootPassword), User.UserType.Root, new Tag("userprefs"));

        // Create the user
        db.createUser(user);

        // Provide the name/password to the user running the program
        System.out.println("done.");

        System.out.println("Database initialized successfully.");

        String cfgDir = params.getString("cfgdir");
        if (cfgDir != null) {
            File dir = new File(cfgDir);
            if (!dir.isDirectory()) {
                throw new IOException(
                    "Unable to generate configuration files: '" + cfgDir +
                        "' is not a directory");
            }

            System.out.println("Generating configuration files in '" + cfgDir +
                               "'.");

            // Build up an XML structure for a generic configuration
            // file
            Tag rootTag = new Tag("bvb");

            // Database data
            Tag dbTag = new Tag("database");
            {
                Tag temp;

                dbTag.addContent(temp = new Tag("provider"));
                temp.addContent(dbProvider);

                dbTag.addContent(temp = new Tag("server"));
                temp.addContent(dbServer);

                dbTag.addContent(temp = new Tag("name"));
                temp.addContent(dbName);

                // Contents of user/password tags are handled below
                dbTag.addContent(new Tag("user"));
                dbTag.addContent(new Tag("password"));

                rootTag.addContent(dbTag);
            }

            // Autoload section is not used any more

            // Plugins
            Tag pluginsTag = new Tag("plugins");
            rootTag.addContent(pluginsTag);

            // These are not all present for every program
            Tag databasePlugins = new Tag("directory");
            Tag modemPlugins = new Tag("directory");
            Tag jobPlugins = new Tag("directory");
            Tag actionPlugins = new Tag("directory");

            databasePlugins.addContent(Server.CFG_SYSTEM + File.separator +
                                       "BVBDatabases");
            modemPlugins.addContent(Server.CFG_SYSTEM + File.separator +
                                    "BVBModems");
            jobPlugins.addContent(Server.CFG_SYSTEM + File.separator +
                                  "BVBJobs");
            actionPlugins.addContent(Server.CFG_SYSTEM + File.separator +
                                     "BVBActions");

            // Set to true if the user needs to edit the configuration
            // files to provide the password
            boolean passwordMessage = false;

            try {
                // Server apps
                dbTag.getTag("user").removeContents();
                dbTag.getTag("password").removeContents();

                dbTag.getTag("user").addContent(serverUser);

                if (serverPassword == null) {
                    dbTag.getTag("password").addContent(
                        "(Write in the Database user password here)");

                    passwordMessage = true;
                }
                else {
                    dbTag.getTag("password").addContent(serverPassword);
                }

                // Note: Although it would be slightly more efficient to avoid
                // removing everything before re-adding pieces that were already
                // present, it is more clear and less prone to errors
                // (especially copy/paste errors) to do it this way

                // bvipregd: databases
                pluginsTag.removeContents();
                pluginsTag.addContent(databasePlugins);
                Utils.xmlParser.writeXMLFile(rootTag, new File(dir,
                    "bvipregd.rc"));

                // bvjobd: databases, modems, jobs
                pluginsTag.removeContents();
                pluginsTag.addContent(databasePlugins);
                pluginsTag.addContent(modemPlugins);
                pluginsTag.addContent(jobPlugins);
                Utils.xmlParser.writeXMLFile(rootTag,
                    new File(dir, "bvjobd.rc"));

                // bvbepd: databases, modems, jobs
                pluginsTag.removeContents();
                pluginsTag.addContent(databasePlugins);
                pluginsTag.addContent(modemPlugins);
                pluginsTag.addContent(jobPlugins);
                Utils.xmlParser.writeXMLFile(rootTag,
                    new File(dir, "bvbepd.rc"));

                // Client apps
                if (clientUser != null) {
                    dbTag.getTag("user").removeContents();
                    dbTag.getTag("password").removeContents();

                    dbTag.getTag("user").addContent(clientUser);

                    if (clientPassword == null) {
                        dbTag.getTag("password").addContent(
                            "(Write in the Database user password here)");

                        passwordMessage = true;
                    }
                    else {
                        dbTag.getTag("password").addContent(clientPassword);
                    }
                }

                // bvbatch: databases, actions
                pluginsTag.removeContents();
                pluginsTag.addContent(databasePlugins);
                pluginsTag.addContent(actionPlugins);
                Utils.xmlParser.writeXMLFile(rootTag, new File(dir,
                    "bvbatch.rc"));

                // bvweb: databases
                pluginsTag.removeContents();
                pluginsTag.addContent(databasePlugins);
                Utils.xmlParser
                    .writeXMLFile(rootTag, new File(dir, "bvweb.rc"));
            }
            catch (IOException ioe) {
                logger.error("Unable to generate configuration files", ioe);

                throw new IOException(
                    "Unable to generate configuration files: " +
                        ioe.getMessage(), ioe);
            }

            if (passwordMessage) {
                System.out
                    .println("You will need to edit the configuration files "
                             + "to write in the proper passwords.");
            }

            System.out.println("Done generating configuration files.");

            if (!cfgDir.equals(Server.CFG_SYSTEM)) {
                System.out
                    .println("You should probably move the configuration " +
                             "files into '" + Server.CFG_SYSTEM + "'.");
            }
        }
    }

    private static void replaceKey(JSAPResult params, String licenseKeyFile,
                                   Database db)
        throws DatabaseException, IOException
    {

        LicenseKey licenseKey = getLicense(licenseKeyFile);
        if (licenseKey == null) {
            return;
        }

        db.connect(dbServer, dbName, dbUser, dbPasswd, true, licenseKey,
            Database.ConnectionReason.Client);

        db.updateSetting(Settings.Setting.LicenseKey.properName, licenseKey
            .getLicense().toString(false));
    }

    private static String getParameter(JSAPResult params, String jsapID,
                                       Tag config, String tagID, String errMsg)
    {

        String val = params.getString(jsapID);

        if (val == null) {
            val = config.getStringContents(tagID);

            if (val == null) {
                System.err.println(errMsg);
                System.exit(1);
                return null; // Prevent a compiler error
            }
        }

        return val;
    }

    // StringUtils doesn't use generics, so this suppression is necessary for us
    // to cast it without a warning
    @SuppressWarnings("unchecked")
    private static String getUsage(SimpleJSAP jsap)
    {
        List<String> usage =
            StringUtils.wrapToList("BVBatch " + jsap.getUsage(), jsap
                .getScreenWidth());

        StringBuilder builder = new StringBuilder();
        builder.append("Usage:\n");

        for (String line : usage) {
            builder.append("  ");
            builder.append(line);
            builder.append("\n");
        }

        return builder.toString();
    }

    public static boolean getConsoleYesNo(String prompt, boolean dflt)
    {
        boolean answer = false;

        do {
            String yesno = readLine("%s [%s] ", prompt, (dflt ? "Y/n" : "y/N"));

            if (yesno.length() == 0) {
                answer = dflt;
            }
            else if (yesno.trim().toLowerCase().startsWith("y")) {
                answer = true;
            }
            else if (yesno.trim().toLowerCase().startsWith("n")) {
                answer = false;
            }
            else {
                // Jump to the beginning of the loop and try again
                System.out.println("Please choose a legal value");
                continue;
            }
        } while (false); // Drop out as soon as possible

        return answer;
    }

    private static boolean warningPrinted = false;

    public static String readLine(String prompt, Object... args)
    {
        if (System.console() == null) {
            if (!warningPrinted) {
                logger.warn("There is no console object");
                warningPrinted = true;
            }

            System.out.printf(prompt, args);

            UnbufferedInputStream in = new UnbufferedInputStream(System.in);
            try {
                return in.readLine();
            }
            catch (IOException ioe) {
                // This should not happen
                throw new IOError(ioe);
            }
        }
        else {
            return System.console().readLine(prompt, args);
        }
    }

    public static String readPassword(String prompt, Object... args)
    {
        if (plainPasswd) {
            return readLine(prompt, args);
        }
        else if (System.console() == null) {
            if (!warningPrinted) {
                logger.warn("There is no console object");
                warningPrinted = true;
            }

            PasswordReader pr = new PasswordReader();
            return pr.readPassword(prompt, args);
        }
        else {
            return new String(System.console().readPassword(prompt, args));
        }
    }

    public static String confirmPassword(String prompt, Object... args)
    {
        while (true) {
            String password = readPassword(prompt, args);
            String confirm = readPassword("Confirm: ");

            if (!password.equals(confirm)) {
                System.out.println("Passwords do not match");
            }
            else {
                return password;
            }
        }
    }
}

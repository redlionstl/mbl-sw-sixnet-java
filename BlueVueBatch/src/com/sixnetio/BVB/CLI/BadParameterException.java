/*
 * BadParameterException.java
 *
 * Thrown when the parameters required by an action
 * do not match what is provided, or similar (provided
 * file does not exist).
 *
 * Jonathan Pearson
 * February 4, 2009
 *
 */

package com.sixnetio.BVB.CLI;

public class BadParameterException extends Exception {
	public BadParameterException(String message) {
		super(message);
	}
	
	public BadParameterException(String message, Throwable cause) {
		super(message, cause);
	}
}

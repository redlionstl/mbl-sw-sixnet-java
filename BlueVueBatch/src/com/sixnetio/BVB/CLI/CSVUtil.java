/*
 * CSVUtil.java
 *
 * A simple command-line utility to perform database-like operations on on CSV tables.
 *
 * Jonathan Pearson
 * March 2, 2009
 *
 */

// TODO: Remove duplicate columns when possible (target column of a join)
package com.sixnetio.BVB.CLI;

import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.martiansoftware.jsap.*;
import com.martiansoftware.util.StringUtils;
import com.sixnetio.BVB.Common.Server;
import com.sixnetio.util.Utils;

public class CSVUtil {
	private static final Logger logger;
	
	public static final String PROG_VERSION = "@bvg.major@.@bvg.minor@.@bvg.build@";
	
	static {
		// Not interested in any configuration files
		Server.configureInstance("csvutil");
		
		logger = Logger.getLogger(Utils.thisClassName());
	}
	
	private static enum CSVCommand {
		Intersect(true, true),
		Union(true, true),
		Add(true, true),
		Subtract(true, true),
		LeftJoin(true, true),
		RightJoin(true, true),
		AddColumn(false, false),
		RemoveColumn(false, false),
		Select(true, false),
		RenameColumn(false, false),
		;
		
		public final boolean requireIndex;
		public final boolean requireRightTable;
		
		private CSVCommand(boolean requireIndex, boolean requireRightTable) {
			this.requireIndex = requireIndex;
			this.requireRightTable = requireRightTable;
		}
	}
	
	private static CSVCommand chosenCommand = null;
	
	public static void main(String[] args) {
		SimpleJSAP jsap;
		
		try {
			jsap = createJsap();
		} catch (JSAPException jsape) {
			logger.fatal("Error", jsape);
			System.err.println("Error initializing JSAP");
			System.exit(1);
			return;
		}
		
		JSAPResult params = jsap.parse(args);
		
		if (!params.success()) {
			jsap.setScreenWidth(80);
			
			String usage = getUsage(jsap);
			System.err.println(usage);
			System.err.println(jsap.getHelp());
			System.exit(1);
			return;
		}
		else if (params.getBoolean("help")) {
			return;
		}
		else if (params.getBoolean("version")) {
			System.out.println(Server.copyrightBanner("BlueVue CSV Utility",
			                                          PROG_VERSION));
			return;
		}
		
		try {
			performAction(params);
		} catch (Exception e) {
			logger.fatal("Error", e);
			System.err.println("Error: " + e.getMessage());
			System.err.println("You must specify exactly one action.");
			System.err.println("Use the --help switch for usage information");
			System.exit(1);
		}
	}
	
	private static SimpleJSAP createJsap()
			throws JSAPException {
		
		SimpleJSAP jsap;
		jsap = new SimpleJSAP(
			"CSVUtil",
			"Perform one operation on one or two CSV tables. " +
			"The resulting CSV table will be output through stdout.",
			new Parameter[] {
				new Switch("version", 'v', "version",
				           "Display the version number of this program " +
				           "and exit"),
				
				new Switch("intersect", JSAP.NO_SHORTFLAG, "intersect",
				           "Perform an intersection between two tables. " +
				           "This is equivalent to an inner join."),
				
				new Switch("union", JSAP.NO_SHORTFLAG, "union",
				           "Perform a union between two tables. Missing " +
				           "rows from either side will cause empty " +
				           "columns."),
				
				new Switch("add", JSAP.NO_SHORTFLAG, "add",
				           "Perform an addition between two tables. This " +
				           "is a special type of union that avoids " +
				           "duplicating columns because it requires that " +
				           "all columns are equal. Provided column names " +
				           "are ignored, as all fields are compared for " +
				           "equality."),
				
				new Switch("subtract", JSAP.NO_SHORTFLAG, "subtract",
				           "Perform a subtraction of the right table " +
				           "from the left."),
				
				new Switch("leftJoin", JSAP.NO_SHORTFLAG, "left-join",
				           "Perform a left outer join of two tables. " +
				           "Missing rows from the right side will cause " +
				           "empty columns."),
				
				new Switch("rightJoin", JSAP.NO_SHORTFLAG, "right-join",
				           "Perform a right outer join of two tables. " +
				           "Missing rows from the left side will cause " +
				           "empty columns."),
				
				new FlaggedOption("addColumn", JSAP.STRING_PARSER,
				                  JSAP.NO_DEFAULT, false, JSAP.NO_SHORTFLAG,
				                  "add-column",
				                  "Add a column to a single table using " +
				                  "a constant value for each row."),
				
				new Switch("remColumn", JSAP.NO_SHORTFLAG, "remove-column",
				           "Remove a column from a single table."),
				
				new FlaggedOption("select", JSAP.STRING_PARSER, JSAP.NO_DEFAULT,
				                  false, JSAP.NO_SHORTFLAG, "select",
				                  "Select rows from a single table whose " +
				                  "value is equal to the specified value."),
				
				new FlaggedOption("renameColumn", JSAP.STRING_PARSER,
				                  JSAP.NO_DEFAULT, false, JSAP.NO_SHORTFLAG,
				                  "rename-column",
				                  "Rename a column from a single table to " +
				                  "the specified value."),
				
				new FlaggedOption("leftTable", JSAP.STRING_PARSER, "-", true,
				                  'l', "left-table",
				                  "Specify a file containing the left table. " +
				                  "Use '-' to read from stdin. Only one " +
				                  "table may be read from stdin."),
				
				new FlaggedOption("rightTable", JSAP.STRING_PARSER, "-", false,
				                  'r', "right-table",
				                  "Specify a file containing the right " +
				                  "table, if necessary. Use '-' to read " +
				                  "from stdin. Only one table may be read " +
				                  "from stdin."),
				
				new UnflaggedOption("leftColumn", JSAP.STRING_PARSER,
				                    JSAP.NO_DEFAULT, false, false,
				                    "Specify the name of the column in the " +
				                    "left table to use. If not specified, " +
				                    "uses the first column."),
				
				new UnflaggedOption("rightColumn", JSAP.STRING_PARSER,
				                    JSAP.NO_DEFAULT, false, false,
				                    "Specify the name of the column in the " +
				                    "right table to use. If not specified, " +
				                    "uses the same column as was chosen for " +
				                    "the left table."),
			}
		);
		
		return jsap;
	}
	
	private static void performAction(JSAPResult params)
			throws Exception {
		
		// Figure out which command the user specified
		if (params.getString("addColumn") != null) {
			setCommand(CSVCommand.AddColumn);
		}
		if (params.getBoolean("remColumn")) {
			setCommand(CSVCommand.RemoveColumn);
		}
		if (params.getString("renameColumn") != null) {
			setCommand(CSVCommand.RenameColumn);
		}
		if (params.getString("select") != null) {
			setCommand(CSVCommand.Select);
		}
		if (params.getBoolean("intersect")) {
			setCommand(CSVCommand.Intersect);
		}
		if (params.getBoolean("union")) {
			setCommand(CSVCommand.Union);
		}
		if (params.getBoolean("add")) {
			setCommand(CSVCommand.Add);
		}
		if (params.getBoolean("subtract")) {
			setCommand(CSVCommand.Subtract);
		}
		if (params.getBoolean("leftJoin")) {
			setCommand(CSVCommand.LeftJoin);
		}
		if (params.getBoolean("rightJoin")) {
			setCommand(CSVCommand.RightJoin);
		}
		
		if (chosenCommand == null) {
			throw new Exception("You must choose a command.");
		}
		
		// There will always be one table
		LinkedList<String[]> leftTable = readCSVTable(params
		                                              .getString("leftTable"));
		String[] leftHeader = leftTable.removeFirst();
		
		// And there will always be one column name (although we may need to
		// pull this out from the table)
		String leftColumnName = params.getString("leftColumn");
		if (leftColumnName == null) {
			if (leftHeader.length > 0) {
				leftColumnName = leftHeader[0];
			}
			else {
				throw new Exception("No left column name found");
			}
		}
		
		// Find the right column name
		String rightColumnName = params.getString("rightColumn");
		if (rightColumnName == null) {
			// Not there? Use the left column name
			rightColumnName = leftColumnName;
		}
		
		// Do we need to grab a right table?
		LinkedList<String[]> rightTable = null;
		String[] rightHeader = null;
		
		if (chosenCommand.requireRightTable) {
			rightTable = readCSVTable(params.getString("rightTable"));
			rightHeader = rightTable.removeFirst();
		}
		
		// Do we need to build an index (or two)?
		Map<String, List<String[]>> indexedLeftTable = null;
		Map<String, List<String[]>> indexedRightTable = null;
		
		
		if (chosenCommand.requireIndex) {
			indexedLeftTable =
				indexTable(leftHeader, leftTable, leftColumnName);
			indexedRightTable =
				indexTable(rightHeader, rightTable, rightColumnName);
		}
		
		// Perform the specified action
		switch (chosenCommand) {
			case Add:
				performAddition(leftHeader, leftTable, rightHeader, rightTable);
				break;
			case AddColumn:
				performAddColumn(leftHeader, leftTable, leftColumnName,
				                 params.getString("addColumn"));
				break;
			case Intersect:
				performIntersection(leftHeader, indexedLeftTable,
				                    leftColumnName, rightHeader,
				                    indexedRightTable, rightColumnName);
				break;
			case LeftJoin:
				performLeftJoin(leftHeader, indexedLeftTable, leftColumnName,
				                rightHeader, indexedRightTable,
				                rightColumnName);
				break;
			case RemoveColumn:
				performRemoveColumn(leftHeader, leftTable, leftColumnName);
				break;
			case RenameColumn:
				performRenameColumn(leftHeader, leftTable, leftColumnName,
				                    params.getString("renameColumn"));
				break;
			case RightJoin:
				performRightJoin(leftHeader, indexedLeftTable, leftColumnName,
				                 rightHeader, indexedRightTable,
				                 rightColumnName);
				break;
			case Select:
				performSelect(leftHeader, indexedLeftTable,
				              params.getString("select"));
				break;
			case Subtract:
				performSubtraction(leftHeader, indexedLeftTable, leftColumnName,
				                   rightHeader, indexedRightTable,
				                   rightColumnName);
				break;
			case Union:
				performUnion(leftHeader, indexedLeftTable, leftColumnName,
				             rightHeader, indexedRightTable, rightColumnName);
				break;
			default:
				throw new Exception("Unknown command: " + chosenCommand.name());
		}
	}
	
	private static void performIntersection(String[] leftHeader,
			Map<String, List<String[]>> indexedLeftTable,
			String leftColumnName, String[] rightHeader,
			Map<String, List<String[]>> indexedRightTable,
			String rightColumnName)
			throws Exception {
		
		// Join the headers together
		// There may be overlaps, so rename the right side to right.X in that
		// case
		List<String> headerRow = Utils.makeLinkedList(leftHeader);
		for (String header : rightHeader) {
			if (headerRow.contains(header)) {
				String name = "right." + header;
				headerRow.add(name);
			}
			else {
				headerRow.add(header);
			}
		}
		
		printCSVRow(headerRow);
		
		for (Map.Entry<String, List<String[]>> entry :
		     indexedLeftTable.entrySet()) {
			
			String value = entry.getKey();
			List<String[]> rightRows = indexedRightTable.get(value);
			
			if (rightRows == null) {
				continue;
			}
			
			List<String[]> leftRows = entry.getValue();
			
			logger.debug(String.format("Joining %d rows from the left " +
			                           "with %d rows from the right\n",
			                           leftRows.size(), rightRows.size()));
			
			for (String[] leftRow : leftRows) {
				for (String[] rightRow : rightRows) {
					// Combine these two rows and output
					List<String> row = Utils.makeLinkedList(leftRow);
					row.addAll(Utils.makeLinkedList(rightRow));
					
					printCSVRow(row);
				}
			}
		}
	}
	
	private static void performUnion(String[] leftHeader,
			Map<String, List<String[]>> indexedLeftTable,
			String leftColumnName, String[] rightHeader,
			Map<String, List<String[]>> indexedRightTable,
			String rightColumnName)
			throws Exception {
		
		// Join the headers together
		// There may be overlaps, so rename X on the right side to right.X in
		// that case
		List<String> headerRow = Utils.makeLinkedList(leftHeader);
		for (String header : rightHeader) {
			if (headerRow.contains(header)) {
				String name = "right." + header;
				headerRow.add(name);
			}
			else {
				headerRow.add(header);
			}
		}
		
		// Make a couple of blanks, in case there are missing rows
		List<String> leftBlank = new LinkedList<String>();
		for (int i = 0; i < leftHeader.length; ++i) {
			leftBlank.add("");
		}
		
		List<String> rightBlank = new LinkedList<String>();
		for (int i = 0; i < rightHeader.length; ++i) {
			rightBlank.add("");
		}
		
		printCSVRow(headerRow);
		
		// Print out the left rows, including those on the right when available
		for (Map.Entry<String, List<String[]>> entry :
		     indexedLeftTable.entrySet()) {
			
			String value = entry.getKey();
			List<String[]> leftRows = entry.getValue();
			
			List<String[]> rightRows = indexedRightTable.get(value);
			
			// Remove from the right so we can print those without overlap next
			indexedRightTable.remove(value);
			
			if (rightRows == null) {
				// Output the left rows with blanks for the columns on the right
				for (String[] leftRow : leftRows) {
					List<String> row = Utils.makeLinkedList(leftRow);
					row.addAll(rightBlank);
					
					printCSVRow(row);
				}
			}
			else {
				// Output all matches between the two rows
				for (String[] leftRow : leftRows) {
					for (String[] rightRow : rightRows) {
						// Combine these two rows and output
						List<String> row = Utils.makeLinkedList(leftRow);
						row.addAll(Utils.makeLinkedList(rightRow));
						
						printCSVRow(row);
					}
				}
			}
		}
		
		// Print out the remaining right rows, none of which will have a
		// matching left row
		for (Map.Entry<String, List<String[]>> entry :
			indexedRightTable.entrySet()) {
			List<String[]> rightRows = entry.getValue();
			
			for (String[] rightRow : rightRows) {
				List<String> row = new LinkedList<String>(leftBlank);
				row.addAll(Utils.makeLinkedList(rightRow));
				
				printCSVRow(row);
			}
		}
	}
	
	private static void performAddition(String[] leftHeader,
			List<String[]> leftTable, String[] rightHeader,
			List<String[]> rightTable)
			throws Exception {
		
		// Make sure the two headers are equal
		List<String> leftHeaderList = Utils.makeLinkedList(leftHeader);
		List<String> rightHeaderList = Utils.makeLinkedList(rightHeader);
		
		if ( ! leftHeaderList.containsAll(rightHeaderList)
				|| ! rightHeaderList.containsAll(leftHeaderList)) {
			// They are not equal
			throw new Exception("Tables being added do not have the same " +
			                    "format.");
		}
		
		printCSVRow(leftHeaderList);
		
		// The headers are equal, but they may not be in the same order
		// Too bad we don't have easy access to CSVParser from here...
		Map<String, String> leftValues = new HashMap<String, String>();
		
		for (String[] leftRow : leftTable) {
			// Print out this row
			printCSVRow(leftRow);
			
			// Set up for searching for an equal row on the right side, so we
			// don't print the same row twice
			for (int i = 0; i < leftHeader.length; i++) {
				leftValues.put(leftHeader[i], leftRow[i]);
			}
			
			// Search the right table for an equal row
			for (Iterator<String[]> itRightTable = rightTable.iterator();
			     itRightTable.hasNext();) {
				
				String[] rightRow = itRightTable.next();
				
				// Check each of the values in this row for equality
				boolean found = true;
				for (int i = 0; i < rightHeader.length; i++) {
					if (!leftValues.get(rightHeader[i]).equals(rightRow[i])) {
						found = false;
						break;
					}
				}
				
				if (found) {
					// Remove it so we don't hit it again in the other direction
					itRightTable.remove();
				}
			}
		}
		
		// Now print out all of the remaining rows from the right side, with the
		// column order of the left side
		Map<String, String> rightValues = new HashMap<String, String>();
		
		for (String[] rightRow : rightTable) {
			// Put them into the map with the proper names
			for (int i = 0; i < rightHeader.length; i++) {
				rightValues.put(rightHeader[i], rightRow[i]);
			}
			
			// Pull them back out in the proper order
			String[] row = new String[rightHeader.length];
			for (int i = 0; i < leftHeader.length; i++) {
				row[i] = rightValues.get(leftHeader[i]);
			}
			
			printCSVRow(row);
		}
	}
	
	private static void performSubtraction(String[] leftHeader,
			Map<String, List<String[]>> indexedLeftTable,
			String leftColumnName, String[] rightHeader,
			Map<String, List<String[]>> indexedRightTable,
			String rightColumnName)
			throws Exception {
		
		printCSVRow(leftHeader);
		
		for (Map.Entry<String, List<String[]>> entry :
			indexedLeftTable.entrySet()) {
			String value = entry.getKey();
			
			if (indexedRightTable.containsKey(value)) {
				// Don't print this one
				continue;
			}
			else {
				for (String[] rowArray : entry.getValue()) {
					printCSVRow(rowArray);
				}
			}
		}
	}
	
	private static void performLeftJoin(String[] leftHeader,
			Map<String, List<String[]>> indexedLeftTable,
			String leftColumnName, String[] rightHeader,
			Map<String, List<String[]>> indexedRightTable,
			String rightColumnName)
			throws Exception {
		
		// Join the headers together
		// There may be overlaps, so rename X in the right side to right.X in
		// that case
		List<String> headerRow = Utils.makeLinkedList(leftHeader);
		for (String header : rightHeader) {
			if (headerRow.contains(header)) {
				String name = "right." + header;
				headerRow.add(name);
			}
			else {
				headerRow.add(header);
			}
		}
		
		// Make a blank, in case there are missing rows
		List<String> rightBlank = new LinkedList<String>();
		for (int i = 0; i < rightHeader.length; ++i) {
			rightBlank.add("");
		}
		
		printCSVRow(headerRow);
		
		// Print out the left rows, including those on the right when available
		for (Map.Entry<String, List<String[]>> entry :
		     indexedLeftTable.entrySet()) {
			
			String value = entry.getKey();
			List<String[]> leftRows = entry.getValue();
			
			List<String[]> rightRows = indexedRightTable.get(value);
			
			if (rightRows == null) {
				// Output the left rows with blanks for the columns on the right
				for (String[] leftRow : leftRows) {
					List<String> row = Utils.makeLinkedList(leftRow);
					row.addAll(rightBlank);
					
					printCSVRow(row);
				}
			}
			else {
				// Output all matches between the two rows
				for (String[] leftRow : leftRows) {
					for (String[] rightRow : rightRows) {
						// Combine these two rows and output
						List<String> row = Utils.makeLinkedList(leftRow);
						row.addAll(Utils.makeLinkedList(rightRow));
						
						printCSVRow(row);
					}
				}
			}
		}
	}
	
	private static void performRightJoin(String[] leftHeader,
			Map<String, List<String[]>> indexedLeftTable,
			String leftColumnName, String[] rightHeader,
			Map<String, List<String[]>> indexedRightTable,
			String rightColumnName)
			throws Exception {
		
		// Join the headers together
		// There may be overlaps, so rename X in the right side to right.X in
		// that case
		List<String> headerRow = Utils.makeLinkedList(leftHeader);
		for (String header : rightHeader) {
			if (headerRow.contains(header)) {
				String name = "right." + header;
				headerRow.add(name);
			}
			else {
				headerRow.add(header);
			}
		}
		
		// Make a blank, in case there are missing rows
		List<String> leftBlank = new LinkedList<String>();
		for (int i = 0; i < leftHeader.length; ++i) {
			leftBlank.add("");
		}
		
		printCSVRow(headerRow);
		
		// Print out the right rows, including those on the left when available
		for (Map.Entry<String, List<String[]>> entry :
		     indexedRightTable.entrySet()) {
			
			String value = entry.getKey();
			List<String[]> leftRows = indexedLeftTable.get(value);
			
			List<String[]> rightRows = entry.getValue();
			
			if (leftRows == null) {
				// Output the right rows with blanks for the columns on the
				// right
				for (String[] rightRow : rightRows) {
					List<String> row = new LinkedList<String>(leftBlank);
					row.addAll(Utils.makeLinkedList(rightRow));
					
					printCSVRow(row);
				}
			}
			else {
				// Output all matches between the two rows
				for (String[] leftRow : leftRows) {
					for (String[] rightRow : rightRows) {
						// Combine these two rows and output
						List<String> row = Utils.makeLinkedList(leftRow);
						row.addAll(Utils.makeLinkedList(rightRow));
						
						printCSVRow(row);
					}
				}
			}
		}
	}
	
	private static void performSelect(String[] headerRow,
			Map<String, List<String[]>> indexedTable, String columnValue) {
		
		printCSVRow(headerRow);
		
		// Only print the rows with an equal value in the given column
		List<String[]> matchingRows = indexedTable.get(columnValue);
		
		for (String[] rowArray : matchingRows) {
			printCSVRow(rowArray);
		}
	}
	
	private static void performRemoveColumn(String[] headerRow,
			LinkedList<String[]> table, String columnName) {
		
		List<String> row = Utils.makeVector(headerRow);
		int index = row.indexOf(columnName);
		
		// If the column header does not exist, print a warning but continue
		// anyway
		if (index == -1) {
			System.err.printf("Column '%s' does not exist in the given table.",
			                  columnName);
		}
		else {
			row.remove(index);
		}
		
		printCSVRow(row);
		
		for (String[] rowArray : table) {
			row = Utils.makeVector(rowArray);
			
			if (index != -1) {
				row.remove(index);
			}
			
			printCSVRow(row);
		}
	}
	
	private static void performAddColumn(String[] headerRow,
			LinkedList<String[]> table, String columnName, String columnValue)
			throws Exception {
		
		List<String> row = Utils.makeLinkedList(headerRow);
		
		// If it's already there, print a warning and continue normally
		boolean exists = (row.indexOf(columnName) != -1);
		
		if (exists) {
			System.err.println("Column '%s' already exists in the given " +
			                   "table.");
		}
		else {
			row.add(columnName);
		}
		
		printCSVRow(row);
		
		for (String[] rowArray : table) {
			row = Utils.makeLinkedList(rowArray);
			
			if ( ! exists) {
				row.add(columnValue);
			}
			
			printCSVRow(row);
		}
	}
	
	private static void performRenameColumn(String[] headerRow,
			LinkedList<String[]> table, String oldName, String newName) {
		
		List<String> row = Utils.makeVector(headerRow);
		
		int index = row.indexOf(oldName);
		
		if (index == -1) {
			System.err.println("Column '%s' does not exist in the given " +
			                   "table.");
		}
		else {
			row.set(index, newName);
		}
		
		printCSVRow(row);
		
		for (String[] rowArray : table) {
			printCSVRow(rowArray);
		}
	}
	
	// StringUtils doesn't use generics, so this suppression is necessary for us
	// to cast it without a warning
	@SuppressWarnings("unchecked")
	private static String getUsage(SimpleJSAP jsap) {
		List<String> usage = StringUtils.wrapToList("CSVUtil " +
		                                            jsap.getUsage(),
		                                            jsap.getScreenWidth());
		
		StringBuilder builder = new StringBuilder();
		builder.append("Usage:\n");
		
		for (String line : usage) {
			builder.append("  ");
			builder.append(line);
			builder.append("\n");
		}
		
		return builder.toString();
	}
	
	private static LinkedList<String[]> readCSVTable(String fileName)
			throws Exception {
		
		LinkedList<String[]> rows = new LinkedList<String[]>();
		BufferedReader in;
		
		if (fileName.equals("-")) {
			// Read from stdin
			in = new BufferedReader(new InputStreamReader(System.in));
		}
		else {
			in = new BufferedReader(new FileReader(fileName));
		}
		
		try {
			String line;
			while ((line = in.readLine()) != null) {
				if (line.length() == 0) {
					continue;
				}
				
				String[] row = Utils.tokenize(line, ",", false);
				rows.add(row);
			}
		} finally {
			in.close();
		}
		
		return rows;
	}
	
	private static Map<String, List<String[]>> indexTable(String[] headerRow,
	                                                      List<String[]> table,
	                                                      String columnName)
			throws Exception {
		
		// Map of value --> list of rows with that value in the specified column
		Map<String, List<String[]>> indexedTable =
			new HashMap<String, List<String[]>>();
		
		// Find the index of the column we are indexing on
		int index = 0;
		while (index < headerRow.length) {
			if (headerRow[index].equals(columnName)) {
				break;
			}
			
			index++;
		}
		
		if (index == headerRow.length) {
			throw new Exception(String.format("Column name '%s' does not " +
			                                  "exist in the table",
			                                  columnName));
		}
		
		for (String[] row : table) {
			String value = row[index];
			List<String[]> equalRows = indexedTable.get(value);
			
			if (equalRows == null) {
				equalRows = new LinkedList<String[]>();
				indexedTable.put(value, equalRows);
			}
			
			equalRows.add(row);
		}
		
		return indexedTable;
	}
	
	private static void printCSVRow(List<String> row) {
		printCSVRow(row.toArray(new String[0]));
	}
	
	private static void printCSVRow(String[] row) {
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("Printing a row of %d elements\n",
			                           row.length));
			
			Throwable th = new Throwable();
			th.fillInStackTrace();
			logger.debug("Called from", th);
		}
		
		for (int i = 0; i < row.length; i++) {
			if (i > 0) {
				System.out.printf(",");
			}
			
			System.out.printf("\"%s\"", row[i]);
		}
		
		System.out.println();
	}
	
	private static void setCommand(CSVCommand command)
			throws Exception {
		
		if (chosenCommand == null) {
			chosenCommand = command;
		} else {
			throw new Exception("You must specify exactly one action.");
		}
	}
}

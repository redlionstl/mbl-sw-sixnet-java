/*
 * ViewLogsAction.java
 *
 * View the Log table.
 *
 * Jonathan Pearson
 * February 16, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.LogEntry;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class ViewLogsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		Action.registerAction("View_Logs", ViewLogsAction.class);
	}
	
	public ViewLogsAction(String name) {
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Long> logs = new LinkedList<Long>();
		
		// Parse arguments
		if (args.length > 0) {
			// User specified a file name, read info to display from there
			String fileName;
			
			fileName = args[0];
			
			LineNumberReader in = makeReader(fileName);
			
			// Read the CSV file line-by-line
			CSVParser parser = new CSVParser(in);
			
			try {
				parseInput(db, args, logs, in, parser);
			}
			catch (IOException ioe) {
				logger.debug("Unable to read file", ioe);
				
				throw new BadParameterException(String.format("Unable to read file '%s': %s",
				                                              fileName, ioe.getMessage()), ioe);
			}
			finally {
				try {
					parser.close();
				}
				catch (IOException ioe) {
					// Ignore it
					logger.debug("Close failed", ioe);
				}
			}
		}
		else {
			// Get all history
			logs = db.getMatchingLogs(null, null, null, null, null, 0, -1, null, null);
		}
		
		// Dump
		System.out.println("\"LogID\",\"Timestamp\",\"UserID\",\"LogType\",\"Message\"");
		
		for (long logID : logs) {
			try {
				LogEntry entry = db.getLogEntry(logID);
				
				System.out.printf("\"%d\",\"%s\",\"%s\",\"%s\",\"%s\"\n",
				                  logID,
				                  Utils.formatStandardDateTime(entry.timestamp, false),
				                  (entry.userID == null ? "" : "" + entry.userID),
				                  entry.type,
				                  entry.description);
			}
			catch (Exception e) {
				logger.error("Unable to load log entry from the database", e);
				
				System.err.printf("Unable to load log entry %d from the database: %s\n",
				                  logID, e.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args, Collection<Long> logs,
			LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("LogID", "LowDate", "HighDate", "UserID",
			                  "LogType");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sUserID = nullAsNecessary(parser.getColumn("UserID"));
			String sLogType = nullAsNecessary(parser.getColumn("LogType"));
			
			Long logID = parseLongColumn("LogID",false,parser,"log ID");
			Date lowDate = parseDateColumn("LowDate",false,parser,"low date");
			Date highDate = parseDateColumn("HighDate",false,parser,"high date");
			
			Integer userID;
			if (sUserID == null) {
				userID = null;
			}
			else if (sUserID.equals("-")) {
				userID = -1;
			}
			else {
				try {
					userID = Integer.parseInt(sUserID);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d has a non-numeric user ID",
					                                              parser.getLineNumber()));
				}
			}
			
			// Type is a string
			
			if (logID == null && lowDate == null && highDate == null &&
			    sUserID == null && sLogType == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			// Grab the matching log entries
			// We'll write them all to the console once we're sure
			// everything is good
			logs.addAll(db.getMatchingLogs(logID, lowDate, highDate, userID,
			                               sLogType, 0, -1, null, null));
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage:" + getActionName() + "  [<file> [hdr]]\n" +
		       "[<file>]  Optional name of the file containing log patterns, or '-' to read\n" +
		       "  from stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    LogID (optional; display only the log with this ID)\n" +
		       "    LowDate (optional; display only logs on or after this date)\n" +
		       "    HighDate (optional; display only logs on or before this date)\n" +
		       "    UserID (optional; display only logs related to this user; use '-' to\n" +
		       "      match entries with no user)\n" +
		       "    LogType (optional; display only logs of this type)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data describing specified logs, or if no file is specified,\n" +
		       "  describes all logs in the system. Includes a header row.";
	}
}

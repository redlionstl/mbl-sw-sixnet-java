/*
 * UpdateBatchesAction.java
 *
 * Update batches.
 *
 * Jonathan Pearson
 * February 27, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.JobBatch;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class UpdateBatchesAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Update_Batches", UpdateBatchesAction.class);
	}
	
	public UpdateBatchesAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		List<JobBatch> batches = new LinkedList<JobBatch>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, in, batches, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Update the batches in the database, printing out a little identifying
		// info about each
		System.out.println("\"BatchID\",\"UserID\",\"BatchName\",\"RemainingJobs\"");
		
		for (JobBatch batch : batches) {
			try {
				db.updateBatch(batch);
				
				System.out.printf("\"%d\",\"%s\",\"%s\",\"%d\"\n", // Format
				                  batch.batchID, // BatchID
				                  (batch.userID == null ? "" : "" + batch.userID), // UserID
				                  batch.name, // Name
				                  batch.jobCount); // JobCount
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to update batch %d: %s\n",
				                  batch.batchID, de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args, LineNumberReader in,
			List<JobBatch> batches, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",",false));
		}
		else {
			parser.addColumns("BatchID", "UserID", "BatchName");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			// Check for required fields and parse as necessary
			int batchID = parseIntColumn("BatchID",true,parser,"batch ID");
			
			// Grab the tag from the database
			// We'll update fields in the local object as we go
			JobBatch batch = db.getBatch(batchID);
			
			String sBatchName = nullAsNecessary(parser.getColumn("BatchName"));
			if (sBatchName != null) {
				batch.name = sBatchName;
			}
			
			String sUserID = nullAsNecessary(parser.getColumn("UserID"));
			if (sUserID != null) {
				if ("-".equals(sUserID)) {
					batch.userID = null;
				}
				else {
					batch.userID = parseIntColumn("UserID", false,
					                              parser, "user ID");
				}
			}
			
			// We'll write them all to the database once we're sure
			// everything is good
			batches.add(batch);
		}
	}
	
	@Override
	public String getUsage() {
		return ("Usage: " + getActionName() + " <file> [hdr]\n" +
		        "<file>  Name of the file containing batch definitions, or '-' to read from\n" +
		        "  stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    BatchID\n" +
		        "    UserID (optional; use '-' to set to none)\n" +
		        "    BatchName (optional)\n" +
		        "  All columns must exist, but optional columns may be blank.\n" +
		        "    Optional columns that are left blank will keep the current value.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "Output: CSV data describing the batches that were modified, including a header\n" + "  row.");
	}
}

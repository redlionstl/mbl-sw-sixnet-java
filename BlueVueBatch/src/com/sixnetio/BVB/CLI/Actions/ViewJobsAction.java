/*
 * ViewJobsAction.java
 *
 * View jobs.
 *
 * Jonathan Pearson
 * February 11, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class ViewJobsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("View_Jobs", ViewJobsAction.class);
	}
	
	public ViewJobsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Job> jobs = new LinkedList<Job>();
		
		// Parse arguments
		if (args.length > 0) {
			// User specified a file name, read info to display from there
			String fileName;
			
			fileName = args[0];
			
			LineNumberReader in = makeReader(fileName);
			
			// Read the CSV file line-by-line
			CSVParser parser = new CSVParser(in);
			
			try {
				parseInput(db, args, jobs, in, parser);
			}
			catch (IOException ioe) {
				logger.debug("Unable to read file", ioe);
				
				throw new BadParameterException(String.format("Unable to read file '%s': %s",
				                                              fileName,
				                                              ioe.getMessage()),
				                                ioe);
			}
			finally {
				try {
					parser.close();
				}
				catch (IOException ioe) {
					// Ignore it
					logger.debug("Close failed", ioe);
				}
			}
		}
		else {
			// Get a collection of jobs from the database
			Collection<Long> jobIDs =
				db.getMatchingJobs(null, null, null, null, null, null, null,
				                   null, 0, -1, null, null);
			
			for (long jobID : jobIDs) {
				try {
					Job job = db.getJob(jobID, true);
					if (job == null) {
						logger.error(String.format("Unable to find job %d in the database",
						                           jobID));
						
						System.err.printf("Unable to find job %d in the database: %s\n",
						                  jobID);
						
						continue;
					}
					
					jobs.add(job);
				}
				catch (DatabaseException de) {
					logger.error("Database exception", de);
					
					System.err.printf("Unable to load job %d from the database: %s\n",
					                  jobID, de.getMessage());
				}
			}
		}
		
		// Dump
		// Sort by job ID
		Job[] sortedJobs = jobs.toArray(new Job[0]);
		Arrays.sort(sortedJobs, new Comparator<Job>() {
			public int compare(Job o1, Job o2)
			{
				long v = (o1.getJobData().jobID - o2.getJobData().jobID);
				
				if (v < 0) {
					return -1;
				}
				else if (v > 0) {
					return 1;
				}
				else {
					return 0;
				}
			}
		});
		
		System.out.println("\"JobID\",\"BatchID\",\"ModemID\",\"UserID\"," +
		                   "\"Schedule\",\"Tries\",\"JobType\",\"Automatic\"," +
		                   "\"JobStatus\"");
		
		for (Job job : sortedJobs) {
			System.out.printf("\"%d\",\"%s\",\"%d\",\"%s\",\"%s\",\"%d\"," +
			                  "\"%s\",\"%b\",\"%s\"\n",
			                  job.getJobData().jobID,
			                  (job.getJobData().batchID == null ? "" : "" + job.getJobData().batchID),
			                  job.getJobData().modemID,
			                  (job.getJobData().userID == null ? "" : "" + job.getJobData().userID),
			                  Utils.formatStandardDateTime(job.getJobData().schedule, false),
			                  job.getJobData().tries,
			                  job.getJobData().type,
			                  job.getJobData().automatic,
			                  (job.getJobData().lockedBy == null ? "Scheduled" : "Started at " + Utils.formatStandardDateTime(job.getJobData().lockedAt, false) + " by " + job.getJobData().lockedBy));
			
		}
	}
	
	private void parseInput(Database db, String[] args, Collection<Job> jobs,
			LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("JobID", "BatchID", "ModemID", "UserID", "JobType",
			                  "LowDate", "HighDate", "Automatic");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sUserID = nullAsNecessary(parser.getColumn("UserID"));
			String sJobType = nullAsNecessary(parser.getColumn("JobType"));
			String sAutomatic = nullAsNecessary(parser.getColumn("Automatic"));
			
			// Check for required fields and parse as necessary
			
			Long jobID = parseLongColumn("JobID",false,parser,"job ID");
			Integer batchID = parseIntColumn("BatchID",false,parser,"batch ID");
			Integer modemID = parseIntColumn("ModemID",false,parser,"modem ID");
			
			Integer userID;
			if (sUserID == null) {
				userID = null;
			}
			else if (sUserID.equals("-")) {
				userID = -1;
			} else {
				try {
					userID = Integer.parseInt(sUserID);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d has a non-numeric user ID",
					                                              parser.getLineNumber()));
				}
			}
			
			// Type is a string
			
			Date lowDate = parseDateColumn("LowDate",false,parser,"low date");
			Date highDate = parseDateColumn("HighDate",false,parser,"high date");
			
			Boolean automatic;
			if (sAutomatic == null) {
				automatic = null;
			}
			else {
				automatic = Boolean.parseBoolean(sAutomatic);
			}
			
			if (jobID == null && batchID == null && modemID == null &&
			    sUserID == null && sJobType == null && lowDate == null &&
			    highDate == null && sAutomatic == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty row at line %d",
				                                              parser.getLineNumber()));
			}
			
			// Grab the matching jobs
			// We'll write them all to the console once we're sure
			// everything is good
			Collection<Long> matchingJobs =
				db.getMatchingJobs(jobID, batchID, modemID, userID, sJobType,
				                   lowDate, highDate, automatic, 0, -1, null,
				                   null);
			
			for (long matchedJobID : matchingJobs) {
				Job job = db.getJob(matchedJobID, true);
				if (job == null) {
					logger.debug(String.format("Unable to find job %d in the database",
					                           matchedJobID));
					
					System.err.printf("Unable to find job %d in the database\n",
					                  matchedJobID);
					
					continue;
				}
				
				jobs.add(job);
			}
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " [<file> [hdr]]\n" +
		       "[<file>]  Optional name of the file containing job identifiers, or '-' to read\n" +
		       "  from stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    JobID (optional; display only the job with this JobID)\n" +
		       "    BatchID (optional; display only jobs from this batch)\n" +
		       "    ModemID (optional; display only jobs that apply to this modem)\n" +
		       "    UserID (optional; display only jobs created by this user; use '-' to match\n" +
		       "      jobs with no user)\n" +
		       "    JobType (optional; display only jobs of this type)\n" +
		       "    LowDate (optional; display jobs scheduled for this date or later)\n" +
		       "    HighDate (optional; display jobs scheduled for this date or earlier)\n" +
		       "    Automatic (optional; 'true' = select only automatic jobs, 'false' =\n" +
		       "      select only non-automatic jobs)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data describing specified jobs, or if no file is specified,\n" +
		       "  describes all jobs in the system. Includes a header row.";
	}
}

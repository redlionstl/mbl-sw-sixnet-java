/*
 * ViewCapabilitiesAction.java
 *
 * Description
 *
 * Jonathan Pearson
 * February 5, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.Capabilities;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.Utils;

public class ViewCapabilitiesAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("View_Capabilities", ViewCapabilitiesAction.class);
	}
	
	public ViewCapabilitiesAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Capabilities> caps = db.getCurrentCapabilities();
		
		// Sort by server name
		Capabilities[] sorted = caps.toArray(new Capabilities[0]);
		Arrays.sort(sorted, new Comparator<Capabilities>() {
			public int compare(Capabilities o1, Capabilities o2)
			{
				return o1.getServerName().compareTo(o2.getServerName());
			}
		});
		
		System.out.println("\"Server Name\",\"Supported Databases\"," +
		                   "\"Supported Job Types\",\"Supported Modems\"," +
		                   "\"Extra Data\"");
		for (Capabilities cap : sorted) {
			System.out.printf("\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"\n",
			                  cap.getServerName(),
			                  Utils.makeString(cap.getDatabases(), ":"),
			                  Utils.makeString(cap.getJobs(), ":"),
			                  Utils.makeString(cap.getModems(), ":"),
			                  cap.getExtra().toString(false));
		}
	}
	
	@Override
	public String getUsage() {
		return "Usage: " + getActionName() + "\n" +
		       "Output: CSV data describing the capabilities of all active job servers, with a\n" +
		       "  header row.";
	}
}

/*
 * ViewTagMatchesAction.java
 *
 * Dumps data about matches between tags and modems.
 *
 * Jonathan Pearson
 * February 6, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.TagMatch;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class ViewTagMatchesAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("View_TagMatch", ViewTagMatchesAction.class);
	}
	
	public ViewTagMatchesAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<TagMatch> matches = new LinkedList<TagMatch>();
		
		// Parse arguments
		if (args.length > 0) {
			// User specified a file name, read tag/modem IDs to display from
			// there
			String fileName;
			
			fileName = args[0];
			
			LineNumberReader in = makeReader(fileName);
			
			// Read the CSV file line-by-line
			CSVParser parser = new CSVParser(in);
			
			try {
				parseInput(db, args, matches, in, parser);
			}
			catch (IOException ioe) {
				logger.debug("Unable to read file", ioe);
				
				throw new BadParameterException(String.format("Unable to read file '%s': %s",
				                                              fileName, ioe.getMessage()), ioe);
			}
			finally {
				try {
					parser.close();
				}
				catch (IOException ioe) {
					// Ignore it
					logger.debug("Close failed", ioe);
				}
			}
		}
		else {
			// Get a collection of tags from the database
			matches = db.getTagMatch(-1, -1);
		}
		
		// Sort by tag ID and then modem ID
		TagMatch[] sorted = matches.toArray(new TagMatch[0]);
		Arrays.sort(sorted, new Comparator<TagMatch>() {
			public int compare(TagMatch o1, TagMatch o2)
			{
				if (o1.tagID == o2.tagID) {
					// Same tag ID, go by the modem ID
					return (o1.modemID - o2.modemID);
				} else {
					// Different tag IDs, go by that
					return (o1.tagID - o2.tagID);
				}
			}
		});
		
		// Dump
		System.out.println("\"TagID\",\"ModemID\"");
		
		for (TagMatch match : sorted) {
			System.out.printf("\"%d\",\"%d\"\n", // Format
			                  match.tagID, // Tag ID
			                  match.modemID); // Modem ID
		}
	}
	
	private void parseInput(Database db, String[] args,
			Collection<TagMatch> matches, LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("TagID", "ModemID");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			Integer tagID = parseIntColumn("TagID",false,parser,"tag ID");
			Integer modemID = parseIntColumn("ModemID",false,parser,"modem id");
			
			// Check for required fields and parse as necessary
			if (tagID == null && modemID == null) {
				throw new BadParameterException(String.format("Bad file format: "
				                                              +"line %d had neither tag nor modem ID",
				                                              parser.getLineNumber()));
			}
			else if (tagID == null) {
				tagID = -1;
			}
			else if (modemID == null) {
				modemID = -1;
			}
			
			// Grab the matches from the database
			// We'll write them all to the database once we're sure
			// everything is good
			matches.addAll(db.getTagMatch(tagID, modemID));
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " [<file> [hdr]]\n" +
		       "[<file>]  Optional name of the file containing tag/modem identifiers, or '-' to\n" +
		       "  read from stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    TagID (optional)\n" +
		       "    ModemID (optional)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data describing matches between tags and modems, with a header row.\n" +
		       "  If there is input data...\n" +
		       "    - If an input row contains both a tag ID and a modem ID, will output one\n" +
		       "        line if the modem has that tag, or no lines if there is no such match.\n" +
		       "    - If an input row contains only a tag ID, a row will be output for each\n" +
		       "        modem with that tag.\n" +
		       "    - If an input row contains only a modem ID, a row will be output for each\n" +
		       "        tag applied to that modem.\n" +
		       "  If there is no input data, all tag/modem matches will be output.";
	}
}

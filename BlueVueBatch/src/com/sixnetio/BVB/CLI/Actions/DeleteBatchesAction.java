/*
 * DeleteBatchesAction.java
 *
 * Delete batches.
 *
 * Jonathan Pearson
 * February 27, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class DeleteBatchesAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Delete_Batches", DeleteBatchesAction.class);
	}
	
	public DeleteBatchesAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Integer> batches = new LinkedList<Integer>();
		
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, batches, in, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Delete and output
		// Sort by batch ID
		Integer[] sortedBatches = batches.toArray(new Integer[0]);
		Arrays.sort(sortedBatches);
		
		System.out.printf("\"%s\"\n", "BatchID");
		
		for (int batchID : sortedBatches) {
			// Delete the tag
			try {
				db.deleteBatch(batchID);
				
				System.out.printf("\"%d\"\n", batchID);
			}
			catch (DatabaseException de) {
				logger.error("Unable to delete batch", de);
				
				System.err.printf("Unable to delete batch %d: %s\n", batchID,
				                  de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args,
			Collection<Integer> batches, LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("BatchID", "UserID", "BatchName");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			// Check for required fields and parse as necessary
			Integer batchID = parseIntColumn("BatchID", false, parser,
			                                 " batch ID");
			Integer userID = parseIntColumn("UserID", false, parser,
			                                "user ID");
			String batchName = parseStringColumn("BatchName", false, parser,
			                                     "batch name");
			
			// At least one must be specified.
			if (batchID == null && userID == null && batchName == null) {
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			
			// Grab batch tag IDs from the database
			// We'll delete & write them all to the console once we're sure
			// everything is good
			// Only select non-deleted batches; no point in re-deleting
			batches.addAll(db.getMatchingBatches(batchID, userID, batchName,
			                                     false, 0, -1, null, null));
		}
	}
	
	@Override
	public String getUsage() {
		return "Usage: " + getActionName() + " <file> [hdr]\n" +
		       "<file>  Name of the file containing batch identifiers, or '-' to read from\n" +
		       "  stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    BatchID (optional; only delete this batch)\n" +
		       "    UserID  (optional; only delete batches associated with this user; use -1 to\n" +
		       "      delete batches with no user association)\n" +
		       "    BatchName (optional; only delete this batch)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data listing the IDs of batches that were deleted. Includes a\n" +
		       "  header row.";
	}
}

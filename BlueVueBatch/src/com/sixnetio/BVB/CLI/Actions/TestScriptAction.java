/*
 * TestScriptAction.java
 *
 * Reads and runs a configuration script against one modem, allowing the user to
 * see what commands would actually be run against a specific modem. Does not
 * interface with a real modem, only uses the data stored in the database for the
 * specified modem.
 *
 * Jonathan Pearson
 * March 13, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Modems.ScriptParser.ScriptCompiler;
import com.sixnetio.BVB.Modems.ScriptParser.ScriptFormatException;
import com.sixnetio.util.Utils;

public class TestScriptAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Test_Script", TestScriptAction.class);
	}
	
	public TestScriptAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException {
		
		if (args.length != 2) {
			throw new BadParameterException(getActionName() +
			                                " requires two arguments");
		}
		
		String scriptFile = args[0];
		int modemID;
		
		try {
			modemID = Integer.parseInt(args[1]);
		}
		catch (NumberFormatException e) {
			throw new BadParameterException("Passed modem ID '" + args[1] +
			                                "' is not an integer", e);
		}
		
		Modem modem = db.getModem(modemID);
		if (modem == null) {
			throw new BadParameterException(String.format("Modem %d not found",
			                                              modemID));
		}
		
		// Read the script file
		String script;
		
		try {
			InputStream in = new FileInputStream(scriptFile);
			
			try {
				script = Utils.returnToEOF(in);
			}
			finally {
				in.close();
			}
		}
		catch (IOException ioe) {
			logger.error("Unable to read script file", ioe);
			
			throw new BadParameterException("Unable to read script file: " +
			                                ioe.getMessage(), ioe);
		}
		
		String[] compiled;
		try {
			compiled = ScriptCompiler.compileScript(script, modem);
		}
		catch (ScriptFormatException sfe) {
			logger.error("Script file failed to compile", sfe);
			
			throw new BadParameterException("Bad script file: " +
			                                sfe.getMessage(), sfe);
		}
		
		for (String line : compiled) {
			System.out.println(line);
		}
	}
	
	@Override
	public String getUsage() {
		return "Usage: " + getActionName() + " <script file> <modem ID>\n" +
		       "  Compiles a reconfiguration script as if it were run against a specific\n" +
		       "    modem. No communications are made with the modem.\n" +
		       "Output: The compiled script, consisting of only 'sendln' and 'waitln'\n" +
		       "  commands, or an error if the script could not be compiled.";
	}
}

/*
 * CreateJobsAction.java
 *
 * Creates jobs from a CSV file.
 *
 * Jonathan Pearson
 * February 5, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class CreateJobsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Create_Jobs", CreateJobsAction.class);
	}
	
	public CreateJobsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		List<Job> jobs = new LinkedList<Job>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(args, in, jobs, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Add the modems to the database, printing out a little identifying
		// info about each
		System.out.println("\"JobID\",\"BatchID\",\"ModemID\",\"UserID\"," +
		                   "\"Schedule\",\"JobType\",\"Parameters\"," +
		                   "\"Automatic\"");
		
		for (Job job : jobs) {
			try {
				db.createJob(job);
				
				System.out.printf("\"%d\",\"%s\",\"%d\",\"%s\",\"%s\",\"%s\"," +
				                  "\"%s\",\"%b\"\n", // Format
				                  job.getJobData().jobID, // Job ID
				                  (job.getJobData().batchID == null ? "" : "" + job.getJobData().modemID), // BatchID
				                  job.getJobData().modemID, // Modem ID
				                  (job.getJobData().userID == null ? "" : "" + job.getJobData().userID), // User // ID
				                  Utils.formatStandardDateTime(job.getJobData().schedule, false), // Schedule
				                  job.getJobData().type, // Type
				                  job.getJobData().parameters.toString(false), // Parameters
				                  job.getJobData().automatic); // Automatic
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to create job: %s\n", de.getMessage());
			}
		}
	}
	
	private void parseInput(String[] args, LineNumberReader in, List<Job> jobs,
			CSVParser parser)
		throws IOException, BadParameterException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("BatchID", "ModemID", "UserID", "Schedule",
			                  "JobType", "Parameters", "Automatic");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			// Check for required fields and parse as necessary
			Integer batchID = parseIntColumn("BatchID",false,parser,"batch ID");
			int modemID = parseIntColumn("ModemID",true,parser,"modem ID");
			Integer userID = parseIntColumn("UserID",false,parser,"user ID");
			Date schedule = parseDateColumn("Schedule",true,parser,"scheduled time");
			String sType = parseStringColumn("JobType",true,parser,"job type");
			boolean automatic = parseBoolColumn("Automatic",false,parser,"");
			
			String sParams = nullAsNecessary(parser.getColumn("Parameters"));
			Tag params;
			if (sParams == null) {
				params = new Tag("job");
			}
			else {
				params = Utils.parseXML(sParams, true);
			}
			
			// Make a job out of it
			// We'll write them all to the database once we're sure
			// everything is good
			Job.JobData jobData = new Job.JobData(Job.JobData.INV_JOB, batchID,
			                                      modemID, userID, schedule, 0,
			                                      sType, params, automatic);
			jobs.add(Job.makeGenericJob(jobData));
		}
	}
	
	@Override
	public String getUsage()
	{
		return ("Usage: " + getActionName() + " <file> [hdr]\n" +
		        "<file>  Name of the file containing job definitions, or '-' to read from\n" +
		        "  stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    BatchID (optional; default = none)\n" +
		        "    ModemID\n" +
		        "    UserID (optional; default = none)\n" +
		        "    Schedule (format like '2009-02-05 16:08:52')\n" +
		        "    JobType (see the 'View_Capabilities' command)\n" +
		        "    Parameters (optional; default = empty)\n" +
		        "    Automatic (optional; default = false)\n" +
		        "  All columns must exist, but optional columns may be blank.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" + "Output: CSV data describing the jobs that were created, including a header row.");
	}
}

/*
 * DeleteModemsAction.java
 *
 * Allows deletion of modems.
 *
 * Jonathan Pearson
 * February 9, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class DeleteModemsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Delete_Modems", DeleteModemsAction.class);
	}
	
	public DeleteModemsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Integer> modems = new LinkedList<Integer>();
		
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, modems, in, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Delete and print
		System.out.println("\"ModemID\"");
		
		for (int modemID : modems) {
			try {
				if (db.lockModem(modemID, 2000) != LockResult.Acquired) {
					System.err.printf("Timed out trying to lock modem %d\n",
					                  modemID);
				}
				else {
					try {
						db.deleteModem(modemID);
						System.out.printf("\"%d\"\n", modemID);
					}
					finally {
						// This will succeed whether or not we were able to
						// delete the modem
						db.unlockModem(modemID);
					}
				}
			}
			catch (ObjectInUseException oiue) {
				logger.error(String.format("Unable to delete modem %d",
				                           modemID), oiue);
				
				System.err.printf("Unable to delete modem %d: %s\n", modemID,
				                  oiue.getMessage());
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to delete modem %d: %s\n", modemID,
				                  de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args,
			Collection<Integer> modems, LineNumberReader in,
			CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("ModemID", "DeviceID", "Alias", "Model",
			                  "FirmwareRev", "CfgVersion", "IPAddress", "Port",
			                  "Phone");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sDeviceID = nullAsNecessary(parser.getColumn("DeviceID"));
			String sAlias = nullAsNecessary(parser.getColumn("Alias"));
			String sModel = nullAsNecessary(parser.getColumn("Model"));
			String sFirmwareRev = nullAsNecessary(parser.getColumn("FirmwareRev"));
			String sCfgVersion = nullAsNecessary(parser.getColumn("CfgVersion"));
			String sIPAddress = nullAsNecessary(parser.getColumn("IPAddress"));
			String sPhone = nullAsNecessary(parser.getColumn("Phone"));
			
			// Check for required fields and parse as necessary
			
			Integer modemID = parseIntColumn("ModemID",false,parser,"modem ID");
			
			DeviceID deviceID;
			if (sDeviceID == null) {
				deviceID = null;
			}
			else if (sDeviceID.equals("-")) {
				deviceID = DeviceID.DEVID_UNKNOWN;
			}
			else {
				deviceID = DeviceID.parseDeviceID(sDeviceID);
			}
			
			// Alias is a string
			// Model is a string
			
			Version firmwareRev;
			if (sFirmwareRev == null) {
				firmwareRev = null;
			}
			else {
				firmwareRev = new Version(sFirmwareRev);
			}
			
			// CfgVersion is a string
			// IPAddress is a string
			
			Integer port = parseIntColumn("Port",false,parser,"port");
			
			// Phone is a string
			
			Integer tagID = parseIntColumn("TagID",false,parser,"tag ID");
			
			if (modemID == null && deviceID == null && sAlias == null &&
			    sModel == null && sFirmwareRev == null && sCfgVersion == null &&
			    sIPAddress == null && port == null && sPhone == null &&
			    tagID == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			
			// Grab the matching modems from the database
			// We'll write them all to the console once we're sure
			// everything is good
			modems.addAll(db.getMatchingModems(modemID, deviceID, sAlias,
			                                   sModel, firmwareRev, sCfgVersion,
			                                   sIPAddress, port, sPhone, tagID,
			                                   0, -1, null, null));
		}
	}
	
	@Override
	public String getUsage()
	{
		return ("Usage: " + getActionName() + " [<file> [hdr]]\n" +
		        "[<file>]  Optional name of the file containing modem identifiers, or '-' to\n" +
		        "  read from stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    ModemID (optional; delete only the modem with this ModemID)\n" +
		        "    DeviceID (optional; delete only modems with this device ID; use '-' to\n" +
		        "      match modems with no device ID)\n" +
		        "    Alias (optional; delete only modems with this alias; use '-' to match\n" +
		        "      modems with no alias)\n" +
		        "    Model (optional; delete only modems of this model; use '-' to match modems\n" +
		        "      with no model)\n" +
		        "    FirmwareRev (optional; delete only modems with this firmware version; use\n" +
		        "      '-' to match modems with no firmware version)\n" +
		        "    CfgVersion (optional; delete only modems with this configuration version)\n" +
		        "    IPAddress (optional; delete only modems with this address; use '-' to match\n" +
		        "      modems with no address)\n" +
		        "    Port (optional; delete only modems with this port number)\n" +
		        "    Phone (optional; delete only modems with this phone number; use '-' to\n" +
		        "      match modems with no phone number)\n" +
		        "  You must specify at least one column per row. All columns must exist, but any\n" +
		        "    may be blank.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "Output: CSV data describing specified modems, or if no file is specified,\n" +
		        "  describes all modems in the system. Includes a header row.");
	}
}

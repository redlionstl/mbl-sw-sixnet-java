/*
 * UpdatePasswordsAction.java
 *
 * Allows alteration of password settings for a modem.
 *
 * Jonathan Pearson
 * February 9, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.*;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class UpdatePasswordsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Update_Passwords", UpdatePasswordsAction.class);
	}
	
	public UpdatePasswordsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in;
		
		if (fileName.equals("-")) {
			in = new LineNumberReader(new InputStreamReader(System.in));
		}
		else {
			try {
				in = new LineNumberReader(new FileReader(fileName));
			}
			catch (IOException ioe) {
				throw new BadParameterException(String.format("Unable to open file '%s': %s",
				                                              fileName,
				                                              ioe.getMessage()),
				                                ioe);
			}
		}
		
		List<Modem> modems = new LinkedList<Modem>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, in, modems, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Update the tags in the database, printing out a little identifying
		// info about each
		boolean printPassword = false;
		if (args.length > 1 && args[1].equals("pswd") || args.length > 2 &&
		    args[2].equals("pswd")) {
			
			printPassword = true;
		}
		
		if (printPassword) {
			System.out.println("\"ModemID\",\"PasswordEnabled\",\"PasswordAccess\",\"Password\"");
		}
		else {
			System.out.println("\"ModemID\",\"PasswordEnabled\",\"PasswordAccess\"");
		}
		
		for (Modem modem : modems) {
			try {
				db.updateModem(modem);
				
				if (printPassword) {
					System.out.printf("\"%d\",\"%s\",\"%s\",\"%s\"\n", // Format
					                  modem.modemID, // Modem ID
					                  modem.getProperty("passwordSettings/enabled"), // Enabled
					                  modem.getProperty("passwordSettings/access"), // Access
					                  modem.getProperty("passwordSettings/password")); // Password
				}
				else {
					System.out.printf("\"%d\",\"%s\",\"%s\"\n", // Format
					                  modem.modemID, // Modem ID
					                  modem.getProperty("passwordSettings/enabled"), // Enabled
					                  modem.getProperty("passwordSettings/access")); // Access
				}
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to update modem %d: %s\n",
				                  modem.modemID, de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args, LineNumberReader in,
			List<Modem> modems, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",",false));
		}
		else {
			parser.addColumns("ModemID", "PasswordEnabled", "PasswordAccess",
			                  "Password");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sPasswordEnabled = nullAsNecessary(parser.getColumn("PasswordEnabled"));
			String sPasswordAccess = nullAsNecessary(parser.getColumn("PasswordAccess"));
			String sPassword = nullAsNecessary(parser.getColumn("Password"));
			
			// Check for required fields and parse as necessary
			int modemID = parseIntColumn("ModemID",true,parser,"modem ID");
			
			// Grab the modem from the database
			// We'll update fields in the local object as we go
			Modem modem = db.getModem(modemID);
			if (modem == null) {
				logger.debug(String.format("Unable to load modem %d from the database",
				                           modemID));
				
				System.err.printf("Unable to load modem %d from the database: %s\n",
				                  modemID);
				
				continue;
			}
			
			if (sPasswordEnabled == null) {
				// Do nothing
			}
			else {
				modem.setProperty("passwordSettings/enabled", sPasswordEnabled);
			}
			
			if (sPasswordAccess == null) {
				// Do nothing
			}
			else {
				modem.setProperty("passwordSettings/access", sPasswordAccess);
			}
			
			if (sPassword == null) {
				// Do nothing
			}
			else if (sPassword.equals("-")) {
				modem.setProperty("passwordSettings/password", "");
			}
			else if (sPassword.equals("_")) {
				String newPassword = BVBatch.readPassword("Password for %d: ",
				                                          modemID);
				modem.setProperty("passwordSettings/password", newPassword);
			}
			else {
				modem.setProperty("passwordSettings/password", sPassword);
			}
			
			// We'll write them all to the database once we're sure
			// everything is good
			modems.add(modem);
		}
	}
	
	@Override
	public String getUsage() {
		return ("Usage: " + getActionName() + " <file> [hdr] [pswd]\n" +
		        "<file>  Name of the file containing password information, or '-' to read from stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    ModemID\n" +
		        "    PasswordEnabled (optional; 0 = disabled, 1 = enabled)\n" +
		        "    PasswordApplies (optional; 0 = WAN, 1 = both, 2 = LAN)\n" +
		        "    Password (optional; '-' to clear, '_' to read secretly from stdin)\n" +
		        "  All columns must exist, but optional columns may be blank.\n" +
		        "    Optional columns that are left blank will keep the current value.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "[pswd]  If you have 'pswd' after 'hdr' (or after the file name if 'hdr' was not\n" +
		        "  passed), modem passwords will be included in the output.\n" +
		        "Output: CSV data describing the modems that were modified, including a header\n" +
		        "  row.\n" +
		        "  Passwords will not be included in the output unless the 'pswd' option is\n" +
		        "    specified.");
	}
}

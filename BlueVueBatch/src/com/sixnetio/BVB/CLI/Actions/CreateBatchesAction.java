/*
 * CreateBatchesAction.java
 *
 * Creates batches.
 *
 * Jonathan Pearson
 * February 27, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.JobBatch;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class CreateBatchesAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Create_Batches", CreateBatchesAction.class);
	}
	
	public CreateBatchesAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in= makeReader(fileName);
		
		List<JobBatch> batches = new LinkedList<JobBatch>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(args, in, batches, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Add the tags to the database, printing out a little identifying
		// info about each
		System.out.println("\"BatchID\",\"UserID\",\"BatchName\"");
		
		for (JobBatch batch : batches) {
			try {
				db.createBatch(batch);
				
				System.out.printf("\"%d\",\"%s\",\"%s\"\n", // Format
				                  batch.batchID, // Batch ID
				                  (batch.userID == null ? "" : "" + batch.userID), // User ID
				                  batch.name); // Batch name
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to create batch '%s': %s\n",
				                  batch.name, de.getMessage());
			}
		}
	}
	
	private void parseInput(String[] args, LineNumberReader in,
			List<JobBatch> batches, CSVParser parser)
		throws IOException, BadParameterException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("UserID", "BatchName");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sUserID = nullAsNecessary(parser.getColumn("UserID"));
			String sBatchName = nullAsNecessary(parser.getColumn("BatchName"));
			
			// Check for required fields and parse as necessary
			Integer userID;
			if (sUserID == null) {
				userID = null;
			}
			else {
				try {
					userID = Integer.parseInt(sUserID);
				} catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d has a non-numeric user ID",
					                                              parser.getLineNumber()));
				}
			}
			
			if (sBatchName == null) {
				throw new BadParameterException(String.format("Bad file format: line %d had no batch name",
				                                              parser.getLineNumber()));
			}
			
			// Make a tag out of it
			// We'll write them all to the database once we're sure
			// everything is good
			JobBatch batch = new JobBatch(JobBatch.INV_BATCH, userID, sBatchName);
			batches.add(batch);
		}
	}
	
	@Override
	public String getUsage() {
		return ("Usage: " + getActionName() + " <file> [hdr]\n" +
		        "<file>  Name of the file containing batch definitions, or '-' to read from\n" +
		        "  stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    UserID (optional; default = none)\n" +
		        "    BatchName\n" +
		        "  All columns must exist, but optional columns may be blank.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "Output: CSV data describing the batches that were created, including a header\n" + "  row.");
	}
}

package com.sixnetio.BVB.CLI.Actions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.Capabilities;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Utils;

/**
 * Loops through all modems in the database, searching for device IDs that
 * resolve to the same String format. Updates the one with the smallest Modem ID
 * to have the correct device ID, and deletes all the others.
 */
public class RemoveDuplicateDevIDs
    extends Action
{
    private static final Logger log = Logger.getLogger(Utils.thisClassName());

    /** Number of modems to process at a time. */
    private static final int CHUNK_SIZE = 128;

    static {
        Action.registerAction("RemoveDuplicateDevIDs",
            RemoveDuplicateDevIDs.class);
    }

    /**
     * Construct a new DetectDuplicateDevIDs action.
     * 
     * @param name The name that was used to call the action.
     */
    public RemoveDuplicateDevIDs(String name)
    {
        super(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.sixnetio.BVB.CLI.Action#activate(com.sixnetio.BVB.Database.Database,
     * java.lang.String[])
     */
    @Override
    public void activate(Database db, String[] args)
        throws BadParameterException, DatabaseException
    {
        if (args.length > 0) {
            throw new BadParameterException(getActionName() +
                                            " takes no arguments");
        }

        // Cannot run this action while any servers are active
        enforceNoServers(db);

        Map<String, List<Integer>> map = performSearch(db);
        removeDuplicates(db, map);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.sixnetio.BVB.CLI.Action#getUsage()
     */
    @Override
    public String getUsage()
    {
        return "Usage: " +
               getActionName() +
               "\n" +
               "Output: CSV data listing the deleted modem IDs. Includes a header row.\n" +
               "No servers may be running when this command is activated.";
    }

    private void enforceNoServers(Database db)
        throws DatabaseException
    {
        Collection<Capabilities> caps = db.getCurrentCapabilities();
        if (caps.size() > 0) {
            throw new DatabaseException(
                "Cannot run RemoveDuplicateDevIDs action when servers are active");
        }
    }

    private Map<String, List<Integer>> performSearch(Database db)
        throws DatabaseException
    {
        List<Integer> ids =
            new ArrayList<Integer>(db.getMatchingModems(null, null, null, null,
                null, null, null, null, null, null, 0, -1, null, null));

        log.info("Processing " + ids.size() + " modems");

        Map<String, Integer> seenSoFar = new HashMap<String, Integer>();
        Map<String, List<Integer>> duplicates =
            new HashMap<String, List<Integer>>();

        // Work in clumps, to avoid a possible out-of-memory condition
        List<Integer> chunk = new ArrayList<Integer>(CHUNK_SIZE);
        int pos = 0;
        while (pos < ids.size()) {
            int end = pos + CHUNK_SIZE;
            if (end > ids.size()) {
                end = ids.size();
            }

            chunk.clear();
            chunk.addAll(ids.subList(pos, end));

            log.debug("Processing modems [" + pos + ", " + end + ")");

            pos = end;

            Map<Integer, Modem> modems = db.getModems(chunk);
            for (Modem modem : modems.values()) {
                if (modem.deviceID != null) {
                    String id = modem.deviceID.toString();
                    if (seenSoFar.containsKey(id)) {
                        List<Integer> dups = duplicates.get(id);
                        if (dups == null) {
                            dups = new LinkedList<Integer>();
                            dups.add(seenSoFar.get(id));
                            duplicates.put(id, dups);
                        }

                        dups.add(modem.modemID);
                    }
                    else {
                        seenSoFar.put(id, modem.modemID);
                    }
                }
            }
        }

        return duplicates;
    }

    private void removeDuplicates(Database db,
                                  Map<String, List<Integer>> duplicates)
        throws DatabaseException
    {
        System.out.printf("\"%s\"\n", "ModemID");

        for (List<Integer> dups : duplicates.values()) {
            Integer[] array = dups.toArray(new Integer[dups.size()]);
            Arrays.sort(array);

            Integer keep = array[0];

            // Delete the duplicates
            for (int i = 1; i < array.length; i++) {
                try {
                    db.deleteModem(array[i]);
                }
                catch (ObjectInUseException oiue) {
                    System.err
                        .println("Something else is accessing modems, aborting");
                    System.err.println("Did not update device ID of modem " +
                                       keep);
                    throw new DatabaseException(
                        "Modem is locked, but no servers should be running",
                        oiue);
                }

                System.out.printf("%d\n", array[i]);
            }

            // Update the existing modem with the correct device ID
            Modem modem = db.getModem(keep);
            modem.deviceID = DeviceID.parseDeviceID(modem.deviceID.toString());
            db.updateModem(modem);
        }
    }
}

/*
 * CreatePackagesAction.java
 *
 * Inserts update packages into the Packages table.
 *
 * Jonathan Pearson
 * February 10, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.BadBTImageException;
import com.sixnetio.BVB.Common.BlueTreeImage;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class CreatePackagesAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Create_Packages", CreatePackagesAction.class);
	}
	
	public CreatePackagesAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		List<BlueTreeImage> images = new LinkedList<BlueTreeImage>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(args, in, images, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Add the modems to the database, printing out a little identifying
		// info about each
		System.out.println("\"PackageType\",\"PackageVersion\",\"PackageDate\"," +
		                   "\"PackageInfo\",\"PackageChecksum\"");
		
		for (BlueTreeImage image : images) {
			try {
				db.createPackage(image);
				
				System.out.printf("\"%s\",\"%s\",\"%tF\",\"%s\",\"%s\"\n", // Format
				                  image.getType(), // Type
				                  image.getVersion().toString(), // Version
				                  image.getDate(), // Date
				                  image.getInfo(), // Info
				                  image.getChecksum()); // Checksum
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to create package '%s': %s\n",
				                  image.getChecksum(), de.getMessage());
			}
		}
	}
	
	private void parseInput(String[] args, LineNumberReader in,
			List<BlueTreeImage> images, CSVParser parser)
		throws IOException, BadParameterException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("PackageFile");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sPackageFile = parseStringColumn("PackageFile", true ,parser,
			                                        "package file");
			
			// Make a package out of it
			// We'll write them all to the database once we're sure
			// everything is good
			try {
				FileInputStream fin = new FileInputStream(sPackageFile);
				
				try {
					byte[] data = Utils.bytesToEOF(fin);
					BlueTreeImage image = new BlueTreeImage(data);
					
					images.add(image);
				}
				finally {
					fin.close();
				}
			}
			catch (IOException ioe) {
				logger.debug("Unable to read package", ioe);
				
				System.err.printf("Unable to read package file '%s': %s\n",
				                  sPackageFile, ioe.getMessage());
			}
			catch (BadBTImageException bttie) {
				logger.debug("Unable to parse package", bttie);
				
				System.err.printf("Bad package file '%s': %s\n", sPackageFile,
				                  bttie.getMessage());
			}
		}
	}
	
	@Override
	public String getUsage()
	{
		return ("Usage: " + getActionName() + " <file> [hdr]\n" +
		        "<file>  Name of the file containing packages, or '-' to read from stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    PackageFile (file on the local machine to inject into the system)\n" +
		        "  All columns are required.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "Output: CSV data describing the packages that were created, including a header\n" +
		        "  row.");
	}
}

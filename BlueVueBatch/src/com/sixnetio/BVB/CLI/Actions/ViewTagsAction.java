/*
 * ViewTagsAction.java
 *
 * Dumps information about all tags in the system.
 *
 * Jonathan Pearson
 * February 6, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.ModemTag;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class ViewTagsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("View_Tags", ViewTagsAction.class);
	}
	
	public ViewTagsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Integer> tags = new LinkedList<Integer>();
		Map<Integer, String> outFiles = new HashMap<Integer, String>();
		
		// Parse arguments
		if (args.length > 0) {
			// User specified a file name, read tag IDs to display from there
			String fileName;
			
			fileName = args[0];
			
			LineNumberReader in = makeReader(fileName);
			
			// Read the CSV file line-by-line
			CSVParser parser = new CSVParser(in);
			
			try {
				parseInput(db, args, tags, outFiles, in, parser);
			}
			catch (IOException ioe) {
				logger.debug("Unable to read file", ioe);
				
				throw new BadParameterException(String.format("Unable to read file '%s': %s",
				                                              fileName, ioe.getMessage()), ioe);
			}
			finally {
				try {
					parser.close();
				}
				catch (IOException ioe) {
					// Ignore it
					logger.debug("Close failed", ioe);
				}
			}
		}
		else {
			// Get a collection of tags from the database
			tags = db.getMatchingTags(null, null, null, null, null, null, null,
			                          0, -1, null, null);
		}
		
		// Dump
		// Sort by tag ID
		Integer[] sortedTags = tags.toArray(new Integer[0]);
		Arrays.sort(sortedTags);
		
		System.out.printf("\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"\n",
		                  "TagID", "TagName", "Position", "PollingInterval", "CfgFile",
		"ModemCount");
		
		for (int tagID : sortedTags) {
			try {
				// Get the tag from the database
				ModemTag tag = db.getTag(tagID);
				
				// If necessary, write the configuration file to disk
				String cfgFile = outFiles.get(tag);
				
				if (cfgFile != null) {
					try {
						OutputStream out = new FileOutputStream(cfgFile);
						
						try {
							out.write(tag.configScript.getBytes());
						}
						finally {
							out.close();
						}
					}
					catch (IOException ioe) {
						logger.debug("Unable to write to file", ioe);
						
						System.err.printf("Unable to save configuration file of tag %d to file '%s': %s\n",
						                  tag.tagID, cfgFile, ioe.getMessage());
						
						// So it prints nicely
						cfgFile = "";
					}
				}
				else {
					// So it prints nicely
					cfgFile = "";
				}
				
				System.out.printf("\"%d\",\"%s\",\"%d\",\"%d\",\"%s\",\"%d\"\n",
				                  tag.tagID, tag.name, tag.position, tag.pollingInterval,
				                  cfgFile, tag.modemCount);
			}
			catch (DatabaseException de) {
				logger.error("Unable to retrieve tag", de);
				
				System.err.printf("Unable to retrieve tag %d: %s\n", tagID, de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args,
			Collection<Integer> tags, Map<Integer, String> outFiles,
			LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("TagID", "TagName", "LowPollingInterval",
			                  "HighPollingInterval", "LowPosition", "HighPosition",
			                  "ModemID", "CfgFile");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sTagName = nullAsNecessary(parser.getColumn("TagName"));
			String sCfgFile = nullAsNecessary(parser.getColumn("CfgFile"));
			
			// Check for required fields and parse as necessary
			
			// TagName is a string
			
			Integer tagID = parseIntColumn("TagID",false,parser,"tag ID");
			Integer lowPollingInterval = parseIntColumn("LowPollingInterval",false,parser,"low polling interval");
			Integer highPollingInterval = parseIntColumn("HighPollingInterval",false,parser,"high polling interval");
			Integer lowPosition = parseIntColumn("LowPosition",false,parser,"low position");
			Integer highPosition = parseIntColumn("HighPosition",false,parser,"high position");
			Integer modemID = parseIntColumn("ModemID",false,parser,"modem ID");
			
			// This doesn't test sCfgFile -- CLN
			if (tagID == null && sTagName == null && lowPollingInterval == null
					&& highPollingInterval == null && lowPosition == null
					&& highPosition == null && modemID == null) {
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			
			// The next two exceptions appear to refer to the same file
			// differently. Then again, the second one seems unreachable. -- CLN
			if (sCfgFile != null) {
				if (tagID == null) {
					throw new BadParameterException(String.format("Bad file format: line %d has an output file but no tag ID",
					                                              parser.getLineNumber()));
				}
				
				outFiles.put(tagID, sCfgFile);
			}
			
			if (sCfgFile != null && tagID == null) {
				throw new BadParameterException(String.format("Bad file format: CfgFile specified on line %d with no TagID",
				                                              parser.getLineNumber()));
			}
			
			
			// Grab the tag IDs from the database
			// We'll write them all to the console once we're sure
			// everything is good
			tags.addAll(db.getMatchingTags(tagID, sTagName, lowPollingInterval,
			                               highPollingInterval, lowPosition, highPosition,
			                               modemID, 0, -1, null, null));
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " [<file> [hdr]]\n" +
		       "[<file>]  Optional name of the file containing tag identifiers, or '-' to read\n" +
		       "  from stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    TagID (optional; only display this tag)\n" +
		       "    TagName (optional; only display this tag)\n" +
		       "    LowPollingInterval (optional; only display tags with at least this polling\n" +
		       "      interval)\n" +
		       "    HighPollingInterval (optional; only display tags with at most this polling\n" +
		       "      interval)\n" +
		       "    LowPosition (optional; only display tags with at least this position)\n" +
		       "    HighPosition (optional; only display tags with at most this position)\n" +
		       "    ModemID (optional; only display tags applied to this modem)\n" +
		       "    CfgFile (optional; will receive the configuration file if one exists; only\n" +
		       "      allowed if TagID is specified)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data describing specified tags, or if no file is specified,\n" +
		       "  describes all tags in the system. Includes a header row.";
	}
}

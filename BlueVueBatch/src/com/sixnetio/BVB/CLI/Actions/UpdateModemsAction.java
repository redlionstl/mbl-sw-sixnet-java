/*
 * UpdateModemsAction.java
 *
 * Updates modem data.
 *
 * Jonathan Pearson
 * February 6, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class UpdateModemsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Update_Modems", UpdateModemsAction.class);
	}
	
	public UpdateModemsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		List<Modem> modems = new LinkedList<Modem>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, in, modems, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Update the modems in the database, printing out a little identifying
		// info about each
		System.out.println("\"ModemID\",\"Alias\",\"Model\",\"FirmwareRev\"," +
		                   "\"IPAddress\",\"Phone\"");
		
		for (Modem modem : modems) {
			try {
				LockResult lr = db.lockModem(modem.modemID, 2000);
				
				// It's possible that the local machine is running a server
				// Don't want to interfere with it if it is holding the modem
				// lock
				if (lr == LockResult.Acquired) {
					try {
						db.updateModem(modem);
						
						System.out.printf("\"%d\",\"%s\",\"%s\",\"%s\",\"%s:%d\",\"%s\"\n", // Format
						                  modem.modemID, // Modem ID
						                  (modem.alias == null ? "" : modem.alias), // Alias
						                  (modem.model == null ? "" : modem.model), // Model
						                  (modem.fwVersion == null ? "" : modem.fwVersion), // Firmware
						                  (modem.ipAddress == null ? "" : modem.ipAddress), // IP
						                  modem.port, // Port number
						                  (modem.phoneNumber == null ? "" : modem.phoneNumber)); // Phone
					}
					finally {
						db.unlockModem(modem.modemID);
					}
				}
				else {
					System.err.printf("Timed out trying to lock modem %d, it was not updated\n",
					                  modem.modemID);
				}
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to update modem %d: %s\n",
				                  modem.modemID, de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args, LineNumberReader in,
			List<Modem> modems, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",",false));
		}
		else {
			parser.addColumns("ModemID", "Alias", "Model", "FirmwareRev",
			                  "IPAddress", "Port", "Phone");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sAlias = nullAsNecessary(parser.getColumn("Alias"));
			String sModel = nullAsNecessary(parser.getColumn("Model"));
			String sFirmwareRev = nullAsNecessary(parser.getColumn("FirmwareRev"));
			String sIPAddress = nullAsNecessary(parser.getColumn("IPAddress"));
			String sPort = nullAsNecessary(parser.getColumn("Port"));
			String sPhone = nullAsNecessary(parser.getColumn("Phone"));
			
			// Check for required fields and parse as necessary
			int modemID = parseIntColumn("ModemID",true,parser,"modem ID");
			
			// Grab the modem from the database
			// We'll update fields in the local object as we go
			Modem modem = db.getModem(modemID);
			if (modem == null) {
				logger.debug(String.format("Unable to find modem %d in the database",
				                           modemID));
				System.err.printf("Unable to find modem %d in the database\n",
				                  modemID);
				continue;
			}
			
			if (sAlias == null) {
				// Do nothing
			}
			else if (sAlias.equals("-")) {
				modem.alias = null;
			}
			else {
				modem.alias = sAlias;
			}
			
			if (sModel == null) {
				// Do nothing
			}
			else if (sModel.equals("-")) {
				modem.model = null;
			}
			else {
				modem.model = sModel;
			}
			
			if (sFirmwareRev == null) {
				// Do nothing
			}
			else if (sFirmwareRev.equals("-")) {
				modem.fwVersion = null;
			}
			else {
				modem.fwVersion = new Version(sFirmwareRev);
			}
			
			if (sIPAddress == null) {
				// Do nothing
			}
			else if (sIPAddress.equals("-")) {
				modem.ipAddress = null;
			}
			else {
				modem.ipAddress = sIPAddress;
			}
			
			if (sPort == null) {
				// Do nothing
			}
			else if (sPort.equals("-")) {
				modem.port = Modem.PORT_STANDARD;
			}
			else {
				try {
					modem.port = Integer.parseInt(sPort);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d has non-numeric port number",
					                                              parser.getLineNumber()),
					                                nfe);
				}
			}
			
			if (sPhone == null) {
				// Do nothing
			}
			else if (sPhone.equals("-")) {
				modem.phoneNumber = null;
			}
			else {
				modem.phoneNumber = sPhone;
			}
			
			// We'll write them all to the database once we're sure
			// everything is good
			modems.add(modem);
		}
	}
	
	@Override
	public String getUsage()
	{
		return ("Usage: " + getActionName() + " <file> [hdr]\n" +
		        "<file>  Name of the file containing modem definitions, or '-' to read from\n" +
		        "  stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    ModemID\n" +
		        "    Alias (optional; '-' to clear current setting)\n" +
		        "    Model (optional; '-' to clear current setting)\n" +
		        "    FirmwareRev (optional; '-' to clear current setting)\n" +
		        "    IPAddress (optional; '-' to clear current setting)\n" +
		        "    Port (optional; '-' to set to default of " + Modem.PORT_STANDARD + ")\n" +
		        "    Phone (optional; '-' to clear current setting)\n" +
		        "  All columns must exist, but optional columns may be blank.\n" +
		        "    Optional columns that are left blank will keep the current value.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "Output: CSV data describing the modems that were modified, including a header\n" + "  row.");
	}
}

/*
 * ViewBatchesAction.java
 *
 * View batches.
 *
 * Jonathan Pearson
 * February 27, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.JobBatch;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class ViewBatchesAction
	extends Action
{
	private static Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("View_Batches", ViewBatchesAction.class);
	}
	
	public ViewBatchesAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Integer> batches = new LinkedList<Integer>();
		
		// Parse arguments
		if (args.length > 0) {
			// User specified a file name, read tag IDs to display from there
			String fileName;
			
			fileName = args[0];
			
			LineNumberReader in = makeReader(fileName);
			
			// Read the CSV file line-by-line
			CSVParser parser = new CSVParser(in);
			
			try {
				parseInput(db, args, batches, in, parser);
			}
			catch (IOException ioe) {
				logger.debug("Unable to read file", ioe);
				
				throw new BadParameterException(String.format("Unable to read file '%s': %s",
				                                              fileName,
				                                              ioe.getMessage()),
				                                ioe);
			}
			finally {
				try {
					parser.close();
				}
				catch (IOException ioe) {
					// Ignore it
					logger.debug("Close failed", ioe);
				}
			}
		}
		else {
			// Get a collection of tags from the database
			batches = db.getMatchingBatches(null, null, null, false, 0, -1,
			                                null, null);
		}
		
		// Dump
		// Sort by batch ID
		Integer[] sortedBatches = batches.toArray(new Integer[0]);
		Arrays.sort(sortedBatches);
		
		System.out.printf("\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"\n",
		                  "BatchID",
		                  "UserID",
		                  "BatchName",
		                  "Deleted",
		                  "RemainingJobs");
		
		for (int batchID : sortedBatches) {
			try {
				// Get the tag from the database
				JobBatch batch = db.getBatch(batchID);
				
				System.out.printf("\"%d\",\"%s\",\"%s\",\"%b\",\"%d\"\n", // Format
				                  batch.batchID, // BatchID
				                  (batch.userID == null ? "" : "" + batch.userID), // UserID
				                  batch.name, // Name
				                  batch.deleted, // Deleted
				                  batch.jobCount); // JobCount
			}
			catch (DatabaseException de) {
				logger.error("Unable to retrieve batch", de);
				
				System.err.printf("Unable to retrieve batch %d: %s\n",
				                  batchID, de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args,
			Collection<Integer> batches, LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("BatchID", "UserID", "BatchName", "Deleted");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			// Check for required fields and parse as necessary
			
			Integer batchID = parseIntColumn("BatchID",false,parser,"batch ID");
			Integer userID = parseIntColumn("UserID",false,parser,"user ID");
			
			// BatchName is a string
			String sBatchName = nullAsNecessary(parser.getColumn("BatchName"));
			
			String sDeleted = nullAsNecessary(parser.getColumn("Deleted"));
			Boolean deleted;
			if (sDeleted == null) {
				deleted = null;
			}
			else {
				deleted = Boolean.parseBoolean(sDeleted);
			}
			
			if (batchID == null && userID == null && sBatchName == null &&
			    sDeleted == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			
			// Grab the batch IDs from the database
			// We'll write them all to the console once we're sure
			// everything is good
			batches.addAll(db.getMatchingBatches(batchID, userID, sBatchName,
			                                     deleted, 0, -1, null, null));
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " [<file> [hdr]]\n" +
		       "[<file>]  Optional name of the file containing batch identifiers, or '-' to\n" +
		       "  read from stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    BatchID (optional; only display this batch)\n" +
		       "    UserID (optional; only display batches created by this user; use '-' to\n" +
		       "      select batches without associated users)\n" +
		       "    BatchName (optional; only display this batch)\n" +
		       "    Deleted (optional; 'true' = select only deleted batches, 'false' = select\n" +
		       "      only non-deleted batches)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data describing specified batches, or if no file is specified,\n" +
		       "  describes all non-deleted batches in the system. Includes a header row.";
	}
}

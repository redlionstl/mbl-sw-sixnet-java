/*
 * UpdateTagsAction.java
 *
 * Updates tags.
 *
 * Jonathan Pearson
 * February 5, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.ModemTag;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class UpdateTagsAction
	extends Action
{
	private static Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Update_Tags", UpdateTagsAction.class);
	}
	
	public UpdateTagsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		List<ModemTag> tags = new LinkedList<ModemTag>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, in, tags, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Update the tags in the database, printing out a little identifying
		// info about each
		System.out.println("\"TagID\",\"TagName\",\"PollingInterval\"");
		
		for (ModemTag tag : tags) {
			try {
				db.updateTag(tag);
				
				System.out.printf("\"%d\",\"%s\",\"%d\"\n", // Format
				                  tag.tagID, // Tag ID
				                  tag.name, // Tag name
				                  tag.pollingInterval); // Polling interval
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to update tag %d: %s\n", tag.tagID, de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args, LineNumberReader in,
			List<ModemTag> tags, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("TagID", "TagName", "CfgFile", "PollingInterval");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sTagName = nullAsNecessary(parser.getColumn("TagName"));
			String sCfgScript = nullAsNecessary(parser.getColumn("CfgFile"));
			String sPollingInterval = nullAsNecessary(parser.getColumn("PollingInterval"));
			
			// Check for required fields and parse as necessary
			int tagID = parseIntColumn("TagID",true,parser,"tag ID");
			
			// Grab the tag from the database
			// We'll update fields in the local object as we go
			ModemTag tag = db.getTag(tagID);
			
			if (sTagName != null) {
				tag.name = sTagName;
			}
			
			if (sCfgScript == null) {
				// No change
			}
			else if (sCfgScript.equals("-")) {
				// Clear
				tag.configScript = null;
			}
			else {
				// Name of a file to read from
				try {
					InputStream fin = new FileInputStream(sCfgScript);
					
					try {
						tag.configScript = Utils.returnToEOF(fin);
					}
					finally {
						fin.close();
					}
				}
				catch (IOException ioe) {
					throw new BadParameterException(String.format("Bad file format: line %d references a file that cannot be read: %s",
					                                              parser.getLineNumber(),
					                                              ioe.getMessage()),
					                                ioe);
				}
			}
			
			if (sPollingInterval == null) {
				// No change
			}
			else {
				try {
					tag.pollingInterval = Integer.parseInt(sPollingInterval);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d has a non-numeric polling interval",
					                                              parser.getLineNumber()));
				}
			}
			
			// We'll write them all to the database once we're sure
			// everything is good
			tags.add(tag);
		}
	}
	
	@Override
	public String getUsage()
	{
		return ("Usage: " + getActionName() + " <file> [hdr]\n" +
		        "<file>  Name of the file containing tag definitions, or '-' to read from stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    TagID\n" +
		        "    TagName (optional)\n" +
		        "    CfgFile (optional; file on local system to set as the configuration\n" +
		        "      script; '-' to clear current setting)\n" +
		        "    PollingInterval (optional)\n" +
		        "  All columns must exist, but optional columns may be blank.\n" +
		        "    Optional columns that are left blank will keep the current value.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "Output: CSV data describing the tags that were modified, including a header\n" +
		        "  row.");
	}
}

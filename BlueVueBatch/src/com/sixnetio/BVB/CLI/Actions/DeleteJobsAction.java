/*
 * DeleteJobsAction.java
 *
 * Deletes jobs matching the given pattern(s).
 *
 * Jonathan Pearson
 * February 11, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Database.*;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class DeleteJobsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Delete_Jobs", DeleteJobsAction.class);
	}
	
	public DeleteJobsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Long> jobs = new LinkedList<Long>();
		
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, jobs, in, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Delete and print
		System.out.println("\"JobID\"");
		
		for (long jobID : jobs) {
			try {
				if (db.lockJob(jobID, 2000) != LockResult.Acquired) {
					System.err.printf("Timed out trying to lock job %d\n", jobID);
				}
				else {
					try {
						db.deleteJob(jobID);
						System.out.printf("\"%d\"\n", jobID);
					}
					finally {
						// This will succeed whether or not we were able to
						// delete the job
						db.unlockJob(jobID);
					}
				}
			}
			catch (ObjectInUseException oiue) {
				logger.error(String.format("Unable to delete job %d", jobID), oiue);
				
				System.err.printf("Unable to delete job %d: %s\n", jobID, oiue.getMessage());
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to delete job %d: %s\n", jobID,
				                  de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args, Collection<Long> jobs,
			LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("JobID", "BatchID", "ModemID", "UserID",
			                  "JobType", "LowDate", "HighDate", "Automatic");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sJobType = nullAsNecessary(parser.getColumn("JobType"));
			String sAutomatic = nullAsNecessary(parser.getColumn("Automatic"));
			
			// Check for required fields and parse as necessary
			
			Long jobID = parseLongColumn("JobID",false,parser,"job ID");
			Integer batchID = parseIntColumn("BatchID",false,parser,"batch ID");
			Integer modemID = parseIntColumn("ModemID",false,parser,"modem ID");
			Integer userID = parseIntColumn("UserID",false,parser,"user ID");
			
			// Type is a string
			
			Date lowDate = parseDateColumn("LowDate",false,parser,"low date");
			Date highDate = parseDateColumn("HighDate",false,parser,"high date");
			
			Boolean automatic = parseBoolColumn("Automatic",false,parser,"");
			
			if (jobID == null && batchID == null && modemID == null &&
			    userID == null && sJobType == null && lowDate == null &&
			    highDate == null && sAutomatic == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty row at line %d",
				                                              parser.getLineNumber()));
			}
			
			// Grab the matching jobs
			// We'll write them all to the console once we're sure
			// everything is good
			jobs.addAll(db.getMatchingJobs(jobID, batchID, modemID, userID,
			                               sJobType, lowDate, highDate,
			                               automatic, 0, -1, null, null));
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " <file> [hdr]\n" +
		       "<file>  Name of the file containing job identifiers, or '-' to read from\n" +
		       "  stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    JobID (optional; match only the job with this JobID)\n" +
		       "    BatchID (optional; match only jobs from this batch)\n" +
		       "    ModemID (optional; match only jobs that apply to this modem)\n" +
		       "    UserID (optional; match only jobs created by this user; use '-' to match\n" +
		       "      jobs with no user)\n" +
		       "    JobType (optional; match only jobs of this type)\n" +
		       "    LowDate (optional; delete jobs scheduled for this date or later)\n" +
		       "    HighDate (optional; delete jobs scheduled for this date or earlier)\n" +
		       "    Automatic (optional; 'true' = delete only automatic jobs, 'false' = delete\n" +
		       "      only non-automatic jobs)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data containing job IDs of jobs that were deleted. Includes a header\n" +
		       "  row.";
	}
}

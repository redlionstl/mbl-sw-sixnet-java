/*
 * ViewStatusAction.java
 *
 * View current modem status.
 *
 * Jonathan Pearson
 * February 11, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class ViewStatusAction extends Action {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("View_Status", ViewStatusAction.class);
	}
	
	public ViewStatusAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Set<String> columnNames = new HashSet<String>();
		Set<Date> timestamps = new HashSet<Date>();
		
		Map<Integer, Map<String, String>> properties =
			new HashMap<Integer, Map<String, String>>();
		
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, columnNames, timestamps, properties, in,
			           parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s", fileName,
			                                              ioe.getMessage()), ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Sort the column names alphabetically
		String[] sortedColumns = columnNames.toArray(new String[0]);
		Arrays.sort(sortedColumns);
		
		// Sort the timestamps
		Date[] sortedTimestamps = timestamps.toArray(new Date[0]);
		Arrays.sort(sortedTimestamps);
		
		// Sort the modems by ID
		Integer[] sortedModems = properties.keySet().toArray(new Integer[0]);
		Arrays.sort(sortedModems);
		
		// Print the column headers
		System.out.printf("\"ModemID\",\"Timestamp\"");
		for (String column : sortedColumns) {
			System.out.printf(",\"%s\"", column);
		}
		
		System.out.println();
		
		// Dump the data
		// Format is: for each modem, for each timestamp that exists for that
		// modem's data, dump the properties that were saved for that
		// modem/timestamp
		for (int modemID : sortedModems) {
			for (Date timestamp : sortedTimestamps) {
				// If no properties exist for this modem at this timestamp,
				// don't bother outputting this line
				int propCount = 0;
				
				StringBuilder line = new StringBuilder();
				
				// Modem ID & timestamp
				line.append(String.format("\"%d\",\"%s\"", modemID,
				                          Utils.formatStandardDateTime(timestamp, false)));
				
				for (String columnName : sortedColumns) {
					String key =
						String.format("%s@%s", columnName, Utils.formatStandardDateTime(timestamp, false));
					String val = properties.get(modemID).get(key);
					
					if (val != null) {
						propCount++;
						
						// There are a couple of special cases that need extra
						// parsing
						if (columnName.equals("Network Time")) {
							// This is milliseconds since the epoch
							try {
								long ms = Long.parseLong(val);
								
								// Convert to a date
								Date d = new Date(ms);
								
								// Convert to a string
								val = Utils.formatStandardDateTime(d, false);
							}
							catch (NumberFormatException nfe) {
								logger.warn(String.format("Network Time for modem %d is not a long",
								                          modemID), nfe);
								
								// Just let the value in there past
							}
						}
						else if (columnName.equals("Uptime")) {
							// This is milliseconds
							try {
								long ms = Long.parseLong(val);
								
								// Convert to a date
								Date d = new Date(ms);
								
								// Convert to a string
								val = Utils.formatTimeDurationShort(d);
							}
							catch (NumberFormatException nfe) {
								logger.warn(String.format("Uptime for modem %d is not a long",
								                          modemID), nfe);
								
								// Just let the value in there past
							}
						}
						
						line.append(String.format(",\"%s\"", val));
					}
					else {
						line.append(",");
					}
				}
				
				if (propCount > 0) {
					System.out.println(line.toString());
				}
			}
		}
	}
	
	private void parseInput(Database db, String[] args,
			Set<String> columnNames, Set<Date> timestamps,
			Map<Integer, Map<String, String>> properties, LineNumberReader in,
			CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("ModemID", "MostRecent", "LowDate", "HighDate");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sMostRecent = nullAsNecessary(parser.getColumn("MostRecent"));
			
			// Check for required fields and parse as necessary
			
			
			Integer modemID = parseIntColumn("ModemID",true,parser,"modem ID");
			
			boolean mostRecent;
			if (sMostRecent != null) {
				mostRecent = Boolean.parseBoolean(sMostRecent);
			}
			else {
				mostRecent = false;
			}
			
			Date lowDate = parseDateColumn("LowDate",false,parser,"low date");
			Date highDate = parseDateColumn("HighDate",false,parser,"high date");
			
			if (modemID == null && sMostRecent == null &&
			    lowDate == null && highDate == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			
			// Grab the matching properties
			// We'll display them once we have all the data
			Map<String, String> props =
				db.getModemStatus(modemID, mostRecent, lowDate, highDate);
			
			// Pull out column names and timestamps from the property names
			for (String key : props.keySet()) {
				String propName = key.substring(0, key.lastIndexOf('@'));
				String timestamp = key.substring(key.lastIndexOf('@') + 1);
				
				columnNames.add(propName);
				
				try {
					timestamps.add(Utils.parseStandardDateTime(timestamp));
				}
				catch (ParseException pe) {
					logger.fatal("Database returned a badly formatted property name: " +
					             key, pe);
					
					throw new DatabaseException("Database returned a badly formatted property name: " +
					                            key, pe);
				}
			}
			
			properties.put(modemID, props);
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " <file> [hdr]\n" +
		       "<file>  Name of the file containing modem identifiers, or '-' to read from\n" +
		       "  stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    ModemID (required; display status of only this modem)\n" +
		       "    MostRecent (optional; 'true' displays only the most recent status, 'false'\n" +
		       "      displays all status; default is 'false')\n" +
		       "    LowDate (optional; if MostRecent is 'false', display entries no earlier\n" +
		       "      than this date)\n" +
		       "    HighDate (optional; if MostRecent is 'false', display entries no later than\n" +
		       "      this date)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data describing the status of the modems. Includes a header row.\n" +
		       "  The number and type of columns may change between calls, depending on\n" +
		       "    available information.";
	}
}

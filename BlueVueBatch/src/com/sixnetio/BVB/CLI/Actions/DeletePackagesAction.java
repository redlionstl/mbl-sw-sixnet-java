/*
 * DeletePackagesAction.java
 *
 * Delete packages from the CLI.
 *
 * Jonathan Pearson
 * February 10, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.BlueTreeImage;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class DeletePackagesAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Delete_Packages", DeletePackagesAction.class);
	}
	
	public DeletePackagesAction(String name) {
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<String> packages = new LinkedList<String>();
		
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, packages, in, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Forcing?
		boolean force;
		if (args.length > 0 && args[args.length - 1].equals("force")) {
			force = true;
		}
		else {
			force = false;
		}
		
		// Delete & dump
		System.out.println("\"PackageChecksum\"");
		
		for (String checksum : packages) {
			if (force) {
				// Just delete it
				db.deletePackage(checksum);
			}
			else {
				// Grab the package and check its reference count before
				// deleting
				BlueTreeImage pkg = db.getPackage(checksum, true);
				if (pkg == null) {
					logger.error("Unable to retrieve package '" + checksum + "'");
					
					System.err.printf("Unable to retrieve package '%s'\n",
					                  checksum);
					
					continue;
				}
				
				if (pkg.getRefCount() > 0) {
					System.err.printf("Reference count of package '%s' is %d\n", checksum,
					                  pkg.getRefCount());
					
					continue;
				}
				
				db.deletePackage(checksum);
			}
			
			// If we get here, the package was deleted
			System.out.printf("\"%s\"\n", checksum);
		}
	}
	
	private void parseInput(Database db, String[] args,
			Collection<String> packages, LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("PackageType", "LowVersion", "HighVersion",
			                  "LowDate", "HighDate", "PackageChecksum");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sPackageType = nullAsNecessary(parser.getColumn("PackageType"));
			String sLowVersion = nullAsNecessary(parser.getColumn("LowVersion"));
			String sHighVersion = nullAsNecessary(parser.getColumn("HighVersion"));
			String sPackageChecksum = nullAsNecessary(parser.getColumn("PackageChecksum"));
			
			// Check for required fields and parse as necessary
			
			// Type is a string
			
			Version lowVersion =
				(sLowVersion == null) ? null : new Version(sLowVersion);
			Version highVersion =
				(sHighVersion == null) ? null : new Version(sHighVersion);
			
			Date lowDate = parseDateColumn("LowDate",false,parser,"low date");
			Date highDate = parseDateColumn("HighDate",false,parser,"high date");
			
			// Checksum is a string
			
			if (sPackageType == null && sLowVersion == null
					&& sHighVersion == null && lowDate == null
					&& highDate == null && sPackageChecksum == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			
			// Grab the matching packages
			// We'll delete them once we're sure everything is good
			packages.addAll(db.getMatchingPackages(sPackageType, lowVersion,
			                                       highVersion, lowDate,
			                                       highDate, sPackageChecksum,
			                                       0, -1, null));
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " <file> [hdr] [force]\n" +
		       "<file>  Name of the file containing packages, or '-' to read from stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    PackageType (optional; delete only packages of this type)\n" +
		       "    LowVersion (optional; delete only packages with at least this version)\n" +
		       "    HighVersion (optional; delete only packages with at most this version)\n" +
		       "    LowDate (optional; delete only packages built on or after this date)\n" +
		       "    HighDate (optional; delete only packages built on or before this date)\n" +
		       "    PackageChecksum (optional; delete only packages with this checksum)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "[force]  If you do not provide the string 'force' after the other arguments,\n" +
		       "  packages with a reference count > 0 will not be deleted.\n" +
		       "Output: CSV data listing package checksums that were deleted.";
	}
}

/*
 * ViewSettingsAction.java
 *
 * Get settings.
 *
 * Jonathan Pearson
 * February 11, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class ViewSettingsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("View_Settings", ViewSettingsAction.class);
	}
	
	public ViewSettingsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Map<String, String> settings;
		
		// Parse arguments
		if (args.length > 0) {
			// User specified a file name, read setting names to display from
			// there
			String fileName;
			
			Collection<String> settingNames = new LinkedList<String>();
			
			fileName = args[0];
			
			LineNumberReader in = makeReader(fileName);
			
			// Read the CSV file line-by-line
			CSVParser parser = new CSVParser(in);
			
			try {
				if (args.length > 1 && args[1].equals("hdr")) {
					// Read column header names from the file
					parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
				}
				else {
					parser.addColumns("SettingName");
				}
				
				while (parser.next()) {
					parser.dumpWarnings(System.err);
					
					String settingName = nullAsNecessary(parser.getColumn("SettingName"));
					
					// Check for required fields and parse as necessary
					if (settingName == null) {
						throw new BadParameterException(String.format("Bad file format: line %d has no setting name",
						                                              parser.getLineNumber()));
					}
					
					// Hold onto the name, grab all at once later
					settingNames.add(settingName);
				}
				
				settings = db.getSettings(settingNames);
			}
			catch (IOException ioe) {
				logger.debug("Unable to read file", ioe);
				
				throw new BadParameterException(String.format("Unable to read file '%s': %s",
				                                              fileName,
				                                              ioe.getMessage()),
				                                              ioe);
			}
			finally {
				try {
					parser.close();
				}
				catch (IOException ioe) {
					// Ignore it
					logger.debug("Close failed", ioe);
				}
			}
		}
		else {
			// Get all settings from the database
			settings = db.getSettings(null);
		}
		
		// Dump
		// Sort by setting name
		String[] sortedNames = settings.keySet().toArray(new String[0]);
		Arrays.sort(sortedNames);
		
		System.out.println("\"SettingName\",\"SettingValue\"");
		
		for (String settingName : sortedNames) {
			String value = settings.get(settingName);
			
			if (value == null) {
				value = ""; // So it prints well
			}
			
			System.out.printf("\"%s\",\"%s\"\n", settingName, value);
		}
	}
	
	// It seems that all the file-based actions have the same help except for
	// the list of fields (and maybe whether the fields are required). It'd be
	// nice to have a FilebasedAction.getUsage(String[]) which handled that. -- CLN
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " [<file> [hdr]]\n" +
		       "[<file>]  Optional name of the file containing setting names, or '-' to read\n" +
		       "  from stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    SettingName\n" +
		       "  All columns are required.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data describing the specified settings, or if no file is specified,\n" +
		       "  describes all settings in the system. Includes a header row.";
	}
}

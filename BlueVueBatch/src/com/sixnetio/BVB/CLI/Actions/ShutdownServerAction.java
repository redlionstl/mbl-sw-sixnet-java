/*
 * ShutdownServerAction.java
 *
 * Allows the user to shutdown one or more servers at once, or to clear a shutdown command.
 *
 * Jonathan Pearson
 * February 5, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.Settings;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.Utils;

public class ShutdownServerAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Shutdown_Server", ShutdownServerAction.class);
	}
	
	public ShutdownServerAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Read the arguments for server names
		// Watch for 'ALL' and 'NONE' special values
		for (String s : args) {
			if (s.equals("ALL")) {
				db.updateSetting(Setting.ServerShutdown.properName, "all");
				
				System.out.println("All servers instructed to shut down");
				
				return;
			}
			else if (s.equals("NONE")) {
				db.updateSetting(Setting.ServerShutdown.properName, null);
				
				System.out.println("Server shutdown order cleared");
				
				return;
			}
			else {
				System.out.printf("%s...", s);
				db.updateSetting(Setting.ServerShutdown.properName, s);
				
				while (true) {
					Utils.sleep(1000);
					Settings settings = new Settings(db,
						Utils.makeVector(new String[] {
							Setting.ServerShutdown.properName
					}));
					
					if (settings.getStringValue(Setting.ServerShutdown) == null) {
						break;
					}
				}
				
				System.out.printf("done.\n");
			}
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " {<server name> [<server name> ...]|ALL|NONE}\n" +
		       "  Shuts down each named server.\n" +
		       "  Waits for each server to notice the shutdown request before moving on.\n" +
		       "  If 'ALL' is specified, all servers will be instructed to shut down. Returns\n" +
		       "    immediately.\n" +
		       "    You must manually clear an 'ALL' shutdown order before starting any\n" +
		       "      servers.\n" + "  If 'NONE' is specified, shutdown orders are cleared.\n" +
		       "Output: The name of each server as the shutdown order is made, followed by\n" +
		       "  'done.' when the server has acknowledged the order.\n" +
		       "  An 'ALL' or 'NONE' order will simply output a message when the update has\n" +
		       "    completed.";
	}
}

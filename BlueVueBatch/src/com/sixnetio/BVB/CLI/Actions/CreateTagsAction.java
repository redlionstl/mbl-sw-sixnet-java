/*
 * CreateTagAction.java
 *
 * An action that creates tags.
 *
 * Jonathan Pearson
 * February 5, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.ModemTag;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class CreateTagsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Create_Tags", CreateTagsAction.class);
	}
	
	public CreateTagsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		List<ModemTag> tags = new LinkedList<ModemTag>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(args, in, tags, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Add the tags to the database, printing out a little identifying
		// info about each
		System.out.println("\"TagID\",\"Position\",\"TagName\"," +
		                   "\"PollingInterval\"");
		
		for (ModemTag tag : tags) {
			try {
				db.createTag(tag);
				
				System.out.printf("\"%d\",\"%d\",\"%s\",\"%d\"\n", // Format
				                  tag.tagID, // Tag ID
				                  tag.position, // Position
				                  tag.name, // Tag name
				                  tag.pollingInterval); // Polling interval
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to create tag '%s': %s\n", tag.name,
				                  de.getMessage());
			}
		}
	}
	
	private void parseInput(String[] args, LineNumberReader in,
			List<ModemTag> tags, CSVParser parser)
		throws IOException, BadParameterException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("Position", "TagName", "CfgFile",
			                  "PollingInterval");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sCfgFile = nullAsNecessary(parser.getColumn("CfgFile"));
			
			// Check for required fields and parse as necessary
			Integer position = parseIntColumn("Position", false, parser,
			                                  "position");
			if (position == null) {
				position = -1;
			}
			
			String tagName = parseStringColumn("TagName", true, parser,
			                                   "tag name");
			
			String cfgScript;
			if (sCfgFile == null) {
				cfgScript = null;
			}
			else {
				try {
					InputStream fin = new FileInputStream(sCfgFile);
					
					try {
						cfgScript = Utils.returnToEOF(fin);
					}
					finally {
						fin.close();
					}
				}
				catch (IOException ioe) {
					throw new BadParameterException(String.format("Bad file format: line %d references a file that cannot be read: %s",
					                                              parser.getLineNumber(),
					                                              ioe .getMessage()),
					                                              ioe);
				}
			}
			
			Integer pollingInterval = parseIntColumn("PollingInterval", false,
			                                         parser, "polling interval");
			if (pollingInterval == null) {
				pollingInterval = 3600;
			}
			
			// Make a tag out of it
			// We'll write them all to the database once we're sure
			// everything is good
			ModemTag tag = new ModemTag(-1, position, tagName, cfgScript,
			                            pollingInterval);
			tags.add(tag);
		}
	}
	
	@Override
	public String getUsage()
	{
		return ("Usage: " + getActionName() + " <file> [hdr]\n" +
		        "<file>  Name of the file containing tag definitions, or '-' to read from stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    Position (optional; if specified, must be unique among all existing tags;\n" +
		        "      leave blank to choose the next available value)\n" +
		        "    TagName\n" +
		        "    CfgFile (optional; file containing configuration script)\n" +
		        "    PollingInterval (optional; defaults to 3600 seconds = 1 hour)\n" +
		        "  All columns must exist, but optional columns may be blank.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "Output: CSV data describing the tags that were created, including a header row.");
	}
}

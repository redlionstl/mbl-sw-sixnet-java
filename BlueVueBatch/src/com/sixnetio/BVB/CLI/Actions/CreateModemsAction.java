/*
 * CreateModemsAction.java
 *
 * Creates modems from a CSV file.
 *
 * Jonathan Pearson
 * February 4, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class CreateModemsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Create_Modems", CreateModemsAction.class);
	}
	
	public CreateModemsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		List<Modem> modems = new LinkedList<Modem>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(args, in, modems, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read " +
			                                              "file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                              ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Add the modems to the database, printing out a little identifying
		// info about each
		System.out.println("\"ModemID\",\"DeviceID\",\"Alias\",\"Model\"," +
		                   "\"FirmwareRev\",\"CfgVersion\",\"IPAddress\"," +
		                   "\"Port\",\"Phone\"");
		
		for (Modem m : modems) {
			try {
				db.createModem(m);
				
				System.out.printf("\"%d\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"," +
				                  "\"%s\",\"%d\",\"%s\"\n",
				                  m.modemID,
				                  (m.deviceID == null) ? "" : m.deviceID.toString(),
				                  (m.alias == null ? "" : m.alias),
				                  (m.model == null ? "" : m.model),
				                  (m.fwVersion == null ? "" : m.fwVersion),
				                  (m.cfgVersion == null ? "" : m.cfgVersion),
				                  (m.ipAddress == null ? "" : m.ipAddress),
				                  m.port,
				                  (m.phoneNumber == null ? "" : m.phoneNumber));
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to create modem: %s\n",
				                  de.getMessage());
			}
		}
	}
	
	private void parseInput(String[] args, LineNumberReader in,
			List<Modem> modems, CSVParser parser)
		throws IOException, BadParameterException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("DeviceID", "Alias", "Model", "FirmwareRev",
			                  "IPAddress", "Port", "Phone");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sDevID = nullAsNecessary(parser.getColumn("DeviceID"));
			DeviceID devID;
			if (sDevID == null) {
				devID = null;
			}
			else {
				try {
					devID = DeviceID.parseDeviceID(sDevID);
				}
				catch (NumberFormatException nfe) {
					logger.debug(String.format("Device ID '%s' does not " +
					                           "parse properly", sDevID),
					                           nfe);
					
					throw new BadParameterException(String.format("Bad file format: line %d has a non-numeric device ID",
					                                              parser.getLineNumber()),
					                                nfe);
				}
			}
			
			String sAlias = nullAsNecessary(parser.getColumn("Alias"));
			String sModel = nullAsNecessary(parser.getColumn("Model"));
			String sFWVer = nullAsNecessary(parser.getColumn("FirmwareRev"));
			String sIPAddr = nullAsNecessary(parser.getColumn("IPAddress"));
			String sPort = nullAsNecessary(parser.getColumn("Port"));
			String sPhone = nullAsNecessary(parser.getColumn("Phone"));
			
			if (sDevID == null && sAlias == null && sModel == null &&
			    sFWVer == null && sIPAddr == null && sPhone == null) {
				
				logger.debug("User tried to create a modem with no data");
				
				throw new BadParameterException(String.format("Bad file format: line %d had all empty columns",
				                                              parser.getLineNumber()));
			}
			
			// Make a modem out of it
			// We'll write them all to the database once we're sure
			// everything is good
			
			Version fwver;
			if (sFWVer == null) {
				fwver = null;
			}
			else {
				fwver = new Version(sFWVer);
			}
			
			int port = Modem.PORT_STANDARD;
			if (sPort == null) {
				// Nothing special
			}
			else {
				try {
					port = Integer.parseInt(sPort);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d has non-numeric port number",
					                                              parser.getLineNumber()),
					                                nfe);
				}
			}
			
			Modem modem = new Modem(Modem.INV_MODEM, devID, sAlias, sModel,
			                        fwver, null, sIPAddr, port, sPhone,
			                        new Tag("modem"));
			modems.add(modem);
		}
	}
	
	@Override
	public String getUsage() {
		return ("Usage: " + getActionName() + " <file> [hdr]\n" +
		        "<file>  Name of the file containing modem definitions, or '-' to read from\n" +
		        "  stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    DeviceID (optional)\n" +
		        "    Alias (optional)\n" +
		        "    Model (optional)\n" +
		        "    FirmwareRev (optional)\n" +
		        "    IPAddress (optional; with just this, the rest of these fields may be\n" +
		        "      queried from a modem with a Status job)\n" +
		        "    Port (optional; defaults to " + Modem.PORT_STANDARD + ")\n" +
		        "    Phone (optional)\n" +
		        "  You must specify at least one column per modem. All columns must exist, but\n" +
		        "    any may be blank.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "Output: CSV data describing the modems that were created, including a header\n" +
		        "  row.");
	}
}

/*
 * ViewModemsAction.java
 *
 * View modem status.
 *
 * Jonathan Pearson
 * February 10, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class ViewModemsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("View_Modems", ViewModemsAction.class);
	}
	
	public ViewModemsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Modem> modems = new LinkedList<Modem>();
		
		// Parse arguments
		if (args.length > 0) {
			// User specified a file name, read modem IDs to display from there
			String fileName;
			
			fileName = args[0];
			
			LineNumberReader in = makeReader(fileName);
			
			// Read the CSV file line-by-line
			CSVParser parser = new CSVParser(in);
			
			try {
				parseInput(db, args, modems, in, parser);
			}
			catch (IOException ioe) {
				logger.debug("Unable to read file", ioe);
				
				throw new BadParameterException(String.format("Unable to read file '%s': %s",
				                                              fileName,
				                                              ioe.getMessage()),
				                                              ioe);
			}
			finally {
				try {
					parser.close();
				}
				catch (IOException ioe) {
					// Ignore it
					logger.debug("Close failed", ioe);
				}
			}
		}
		else {
			// Get all modems from the database
			Collection<Integer> modemIDs =
				db.getMatchingModems(null, null, null, null, null, null, null,
				                     null, null, null, 0, -1, null, null);
			
			for (int modemID : modemIDs) {
				try {
					Modem modem = db.getModem(modemID);
					if (modem == null) {
						logger.error(String.format("Unable to find modem %d in the database",
						                           modemID));
						System.err.printf("Unable to find modem %d in the " +
						                  "database\n", modemID);
						continue;
					}
					
					modems.add(modem);
				}
				catch (DatabaseException de) {
					logger.error("Database exception", de);
					
					System.err.printf("Unable to load modem %d from the " +
					                  "database: %s\n",
					                  modemID,
					                  de.getMessage());
				}
			}
		}
		
		// Dump
		// Sort by modem ID
		Modem[] sorted = modems.toArray(new Modem[0]);
		Arrays.sort(sorted, new Comparator<Modem>() {
			public int compare(Modem o1, Modem o2)
			{
				return (o1.modemID - o2.modemID);
			}
		});
		
		System.out.println("\"ModemID\",\"DeviceID\",\"Alias\",\"Model\"," +
		                   "\"FirmwareRev\",\"CfgVersion\",\"IPAddress\"," +
		                   "\"Port\",\"Phone\"");
		
		for (Modem modem : sorted) {
			System.out.printf("\"%d\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"," +
			                  "\"%s\",\"%d\",\"%s\"\n",
			                  modem.modemID,
			                  (modem.deviceID == null) ? "" : modem.deviceID.toString(),
			                  (modem.alias == null ? "" : modem.alias),
			                  (modem.model == null ? "" : modem.model),
			                  (modem.fwVersion == null ? "" : modem.fwVersion),
			                  (modem.cfgVersion == null ? "" : modem.cfgVersion),
			                  (modem.ipAddress == null ? "" : modem.ipAddress),
			                  modem.port,
			                  (modem.phoneNumber == null ? "" : modem.phoneNumber));
		}
	}
	
	private void parseInput(Database db, String[] args,
			Collection<Modem> modems, LineNumberReader in,
			CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("ModemID", "DeviceID", "Alias", "Model",
			                  "FirmwareRev", "CfgVersion", "IPAddress", "Port",
			                  "Phone");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sAlias = nullAsNecessary(parser.getColumn("Alias"));
			String sModel = nullAsNecessary(parser.getColumn("Model"));
			String sFirmwareRev = nullAsNecessary(parser.getColumn("FirmwareRev"));
			String sCfgVersion = nullAsNecessary(parser.getColumn("CfgVersion"));
			String sIPAddress = nullAsNecessary(parser.getColumn("IPAddress"));
			String sPhone = nullAsNecessary(parser.getColumn("Phone"));
			
			// Check for required fields and parse as necessary
			
			Integer modemID = parseIntColumn("ModemID",false,parser,"modem ID");
			
			String sDeviceID = nullAsNecessary(parser.getColumn("DeviceID"));
			DeviceID deviceID;
			if (sDeviceID == null) {
				deviceID = null;
			}
			else if (sDeviceID.equals("-")) {
				deviceID = DeviceID.DEVID_UNKNOWN;
			}
			else {
				try {
					deviceID = DeviceID.parseDeviceID(sDeviceID);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d has a non-numeric device ID",
					                                              parser.getLineNumber()));
				}
			}
			
			// Alias is a string
			
			// Model is a string
			
			Version firmwareRev;
			if (sFirmwareRev == null) {
				firmwareRev = null;
			}
			else {
				firmwareRev = new Version(sFirmwareRev);
			}
			
			// CfgVersion is a string
			// IPAddress is a string
			
			Integer port = parseIntColumn("Port",false,parser,"port");
			
			// Phone is a string
			
			Integer tagID = parseIntColumn("TagID",false,parser,"tag ID");
			
			if (modemID == null && sDeviceID == null && sAlias == null &&
			    sModel == null && sFirmwareRev == null && sCfgVersion == null &&
			    sIPAddress == null && port == null && sPhone == null &&
			    tagID == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			
			// Grab the matching modems from the database
			// We'll write them all to the console once we're sure
			// everything is good
			Collection<Integer> matchingModems =
				db.getMatchingModems(modemID, deviceID, sAlias, sModel,
				                     firmwareRev, sCfgVersion, sIPAddress, port,
				                     sPhone, tagID, 0, -1, null, null);
			
			for (int matchedModemID : matchingModems) {
				Modem modem = db.getModem(matchedModemID);
				if (modem == null) {
					logger.debug(String.format("Unable to find modem %d in " +
					                           "the database", matchedModemID));
					System.err.printf("Unable to find modem %d in the " +
					                  "database\n", matchedModemID);
					continue;
				}
				
				modems.add(modem);
			}
		}
	}
	
	@Override
	public String getUsage()
	{
		return ("Usage:" + getActionName() + "  [<file> [hdr]]\n" +
		        "[<file>]  Optional name of the file containing modem identifiers, or '-' to\n" +
		        "  read from stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    ModemID (optional; display only the modem with this ModemID)\n" +
		        "    DeviceID (optional; display only modems with this device ID; use '-' to\n" +
		        "      match modems with no device ID)\n" +
		        "    Alias (optional; display only modems with this alias; use '-' to match\n" +
		        "      modems with no alias)\n" +
		        "    Model (optional; display only modems of this model; use '-' to match modems\n" +
		        "      with no model)\n" +
		        "    FirmwareRev (optional; display only modems with this firmware version; use\n" +
		        "      '-' to match modems with no firmware version)\n" +
		        "    CfgVersion (optional; display only modems with this configuration version)\n" +
		        "    IPAddress (optional; display only modems with this address; use '-' to\n" +
		        "      match modems with no address)\n" +
		        "    Port (optional; display only modems with this port number)\n" +
		        "    Phone (optional; display only modems with this phone number; use '-' to\n" +
		        "      match modems with no phone number)\n" +
		        "    TagID (optional; display only modems with this tag)\n" +
		        "  You must specify at least one column per row. All columns must exist, but any\n" +
		        "    may be blank.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "Output: CSV data describing specified modems, or if no file is specified,\n" +
		        "  describes all modems in the system. Includes a header row.");
	}
}

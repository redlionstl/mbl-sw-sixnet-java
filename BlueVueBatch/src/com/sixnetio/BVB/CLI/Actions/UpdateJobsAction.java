/*
 * UpdateJobsAction.java
 *
 * Update jobs.
 *
 * Jonathan Pearson
 * February 27, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.BVB.Jobs.Job;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class UpdateJobsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Update_Jobs", UpdateJobsAction.class);
	}
	
	public UpdateJobsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		List<Job> jobs = new LinkedList<Job>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, in, jobs, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Update the jobs in the database, printing out a little identifying
		// info about each
		
		// Sort by Job ID
		Job[] sortedJobs = jobs.toArray(new Job[0]);
		Arrays.sort(sortedJobs, new Comparator<Job>() {
			public int compare(Job o1, Job o2) {
				if (o1.getJobData().jobID < o2.getJobData().jobID) {
					return -1;
				}
				else if (o1.getJobData().jobID > o2.getJobData().jobID) {
					return 1;
				}
				else {
					return 0;
				}
			}
		});
		
		System.out.println("\"JobID\",\"BatchID\",\"ModemID\",\"UserID\","
		                   + "\"Schedule\",\"Tries\",\"JobType\",\"Automatic\"");
		
		for (Job job : sortedJobs) {
			try {
				db.updateJob(job);
				
				System.out.printf("\"%d\",\"%s\",\"%d\",\"%s\",\"%s\",\"%d\",\"%s\",\"%b\"\n",
				                  job.getJobData().jobID,
				                  (job.getJobData().batchID == null ? "" : "" + job.getJobData().batchID),
				                  job.getJobData().modemID,
				                  (job.getJobData().userID == null ? "" : "" + job.getJobData().userID),
				                  Utils.formatStandardDateTime(job.getJobData().schedule, false),
				                  job.getJobData().tries, job.getJobData().type,
				                  job.getJobData().automatic);
			}
			catch (DatabaseException de) {
				logger.error("Unable to update job", de);
				
				System.err.printf("Unable to update job %d: %s\n",
				                  job.getJobData().jobID,
				                  de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args, LineNumberReader in,
			List<Job> jobs, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",",false));
		}
		else {
			parser.addColumns("JobID", "BatchID", "UserID", "Schedule", "Automatic");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sBatchID = nullAsNecessary(parser.getColumn("BatchID"));
			String sUserID = nullAsNecessary(parser.getColumn("UserID"));
			String sSchedule = nullAsNecessary(parser.getColumn("Schedule"));
			String sAutomatic = nullAsNecessary(parser.getColumn("Automatic"));
			
			// Check for required fields and parse as necessary
			
			long jobID = parseLongColumn("JobID",true,parser,"job ID");
			
			// Grab the job from the database
			// We'll update fields in the local object as we go
			Job job = db.getJob(jobID, true);
			if (job == null) {
				System.err.printf("Unable to find job %d in the database\n",
				                  jobID);
				continue;
			}
			
			if (sBatchID == null) {
				// Do nothing
			}
			else if (sBatchID.equals("-")) {
				job.getJobData().batchID = null;
			}
			else {
				try {
					job.getJobData().batchID = Integer.parseInt(sBatchID);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d has non-numeric batch ID",
					                                              parser.getLineNumber()),
					                                nfe);
				}
			}
			
			if (sUserID == null) {
				// Do nothing
			}
			else if (sUserID.equals("-")) {
				job.getJobData().userID = null;
			}
			else {
				try {
					job.getJobData().userID = Integer.parseInt(sUserID);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d has non-numeric user ID",
					                                              parser.getLineNumber()),
					                                nfe);
				}
			}
			
			if (sSchedule == null) {
				// Do nothing
			}
			else {
				try {
					job.getJobData().schedule = Utils.parseStandardDateTime(sSchedule);
				}
				catch (ParseException pe) {
					throw new BadParameterException(String.format("Bad file format: line %d has a bad schedule; must look like 2009-02-27 4:47:03",
					                                              parser.getLineNumber()),
					                                pe);
				}
			}
			
			if (sAutomatic == null) {
				// Do nothing
			}
			else {
				job.getJobData().automatic = Boolean.parseBoolean(sAutomatic);
			}
			
			// We'll write them all to the database once we're sure
			// everything is good
			jobs.add(job);
		}
	}
	
	@Override
	public String getUsage() {
		return ("Usage: " + getActionName() + " <file> [hdr]\n" +
		        "<file>  Name of the file containing job definitions, or '-' to read from stdin\n" +
		        "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		        "    JobID\n" +
		        "    BatchID (optional; '-' to clear current setting)\n" +
		        "    UserID (optional; '-' to clear current setting)\n" +
		        "    Schedule (optional)\n" +
		        "    Automatic (optional)\n" +
		        "  All columns must exist, but optional columns may be blank.\n" +
		        "    Optional columns that are left blank will keep the current value.\n" +
		        "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		        "  read from the first line in the file, allowing for reordering.\n" +
		        "Output: CSV data describing the jobs that were modified, including a header\n" + "  row.");
	}
}

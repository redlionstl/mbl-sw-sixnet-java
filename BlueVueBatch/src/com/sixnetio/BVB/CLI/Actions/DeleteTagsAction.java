/*
 * DeleteTagsAction.java
 *
 * Deletes tags.
 *
 * Jonathan Pearson
 * February 6, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class DeleteTagsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Delete_Tags", DeleteTagsAction.class);
	}
	
	public DeleteTagsAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Integer> tags = new LinkedList<Integer>();
		
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(db, args, tags, in, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Delete and output
		// Sort by tag ID
		Integer[] sortedTags = tags.toArray(new Integer[0]);
		Arrays.sort(sortedTags);
		
		System.out.printf("\"%s\"\n", "TagID");
		
		for (int tagID : sortedTags) {
			// Delete the tag
			try {
				db.deleteTag(tagID);
				
				System.out.printf("\"%d\"\n", tagID);
			}
			catch (DatabaseException de) {
				logger.error("Unable to delete tag", de);
				
				System.err.printf("Unable to delete tag %d: %s\n", tagID,
				                  de.getMessage());
			}
		}
	}
	
	private void parseInput(Database db, String[] args,
			Collection<Integer> tags, LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("TagID", "TagName", "LowPollingInterval",
			                  "HighPollingInterval", "LowCfgVersion",
			                  "HighCfgVersion", "LowPosition", "HighPosition",
			                  "ModemID");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sTagID = nullAsNecessary(parser.getColumn("TagID"));
			String sTagName = nullAsNecessary(parser.getColumn("TagName"));
			String sLowPollingInterval = nullAsNecessary(parser.getColumn("LowPollingInterval"));
			String sHighPollingInterval = nullAsNecessary(parser.getColumn("HighPollingInterval"));
			String sLowPosition = nullAsNecessary(parser.getColumn("LowPosition"));
			String sHighPosition = nullAsNecessary(parser.getColumn("HighPosition"));
			String sModemID = nullAsNecessary(parser.getColumn("ModemID"));
			
			// Check for required fields and parse as necessary
			
			if (sTagID == null && sTagName == null &&
			    sLowPollingInterval == null && sHighPollingInterval == null &&
			    sLowPosition == null && sHighPosition == null &&
			    sModemID == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			
			Integer tagID = parseIntColumn("TagID",false,parser,"tag ID");
			
			// TagName is a string
			
			Integer lowPollingInterval = parseIntColumn("LowPollingInterval",
			                                            false,
			                                            parser,
			                                            "low polling interval");
			Integer highPollingInterval = parseIntColumn("HighPollingInterval",
			                                             false,
			                                             parser,
			                                             "high polling interval");
			
			Integer lowCfgVersion = parseIntColumn("LowCfgVersion",
			                                       false,
			                                       parser,
			                                       "low configuration version");
			Integer highCfgVersion = parseIntColumn("HighCfgVersion",
			                                        false,
			                                        parser,
			                                        "high configuration version");
			
			Integer lowPosition = parseIntColumn("LowPosition", false, parser,
			                                     "low position");
			Integer highPosition = parseIntColumn("HighPosition", false, parser,
			                                      "high position");
			Integer modemID = parseIntColumn("ModemID", false, parser,
			                                 "modem ID");
			
			// Grab the tag IDs from the database
			// We'll write them all to the console once we're sure
			// everything is good
			tags.addAll(db.getMatchingTags(tagID, sTagName, lowPollingInterval,
			                               highPollingInterval, lowPosition,
			                               highPosition, modemID, 0, -1, null,
			                               null));
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " <file> [hdr]\n" +
		       "<file>  Name of the file containing tag identifiers, or '-' to read from\n" +
		       "  stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    TagID (optional; only delete this tag)\n" +
		       "    TagName (optional; only delete this tag)\n" +
		       "    LowPollingInterval (optional; only delete tags with at least this polling\n" +
		       "      interval)\n" +
		       "    HighPollingInterval (optional; only delete tags with at most this polling\n" +
		       "      interval)\n" +
		       "    LowPosition (optional; only delete tags with at least this position)\n" +
		       "    HighPosition (optional; only delete tags with at most this position)\n" +
		       "    ModemID (optional; only delete tags applied to this modem)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data listing the IDs of tags that were deleted. Includes a header\n" +
		       "  row.";
	}
}

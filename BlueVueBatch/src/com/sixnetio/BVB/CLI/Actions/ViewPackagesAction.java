/*
 * ViewPackagesAction.java
 *
 * View packages in the Packages table.
 *
 * Jonathan Pearson
 * February 10, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.BlueTreeImage;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;
import com.sixnetio.util.Version;

public class ViewPackagesAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("View_Packages", ViewPackagesAction.class);
	}
	
	public ViewPackagesAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<String> packages = new LinkedList<String>();
		
		// Checksum --> File
		Map<String, String> outFiles = new HashMap<String, String>();
		
		// Parse arguments
		if (args.length > 0) {
			// User specified a file name, read info to display from there
			String fileName;
			
			fileName = args[0];
			
			LineNumberReader in = makeReader(fileName);
			
			// Read the CSV file line-by-line
			CSVParser parser = new CSVParser(in);
			
			try {
				parseInput(db, args, packages, outFiles, in, parser);
			}
			catch (IOException ioe) {
				logger.debug("Unable to read file", ioe);
				
				throw new BadParameterException(String.format("Unable to read file '%s': %s",
				                                              fileName, ioe.getMessage()), ioe);
			}
			finally {
				try {
					parser.close();
				}
				catch (IOException ioe) {
					// Ignore it
					logger.debug("Close failed", ioe);
				}
			}
		}
		else {
			// Grab everything
			packages.addAll(db.getMatchingPackages(null, null, null, null, null,
			                                       null, 0, -1, null));
		}
		
		// Dump
		System.out.println("\"PackageType\",\"PackageVersion\",\"PackageDate\"," +
		                   "\"PackageInfo\",\"RefCount\",\"PackageChecksum\"," +
		                   "\"PackageFile\"");
		
		for (String checksum : packages) {
			// Grab the package
			BlueTreeImage pkg;
			
			// Only request the package metadata if we don't need to dump it
			pkg = db.getPackage(checksum, !outFiles.containsKey(checksum));
			if (pkg == null) {
				logger.error("Unable to retrieve package '" + checksum + "'");
				
				System.err.printf("Unable to retrieve package '%s'\n", checksum);
				
				continue;
			}
			
			String fileName = outFiles.get(checksum);
			if (fileName != null) {
				try {
					OutputStream out = new FileOutputStream(fileName);
					
					try {
						out.write(pkg.getPackageData());
					} finally {
						out.close();
					}
				}
				catch (IOException ioe) {
					logger.error("Unable to write data to disk", ioe);
					
					System.err.printf("Unable to write package '%s' data to file '%s': %s\n",
					                  checksum, fileName, ioe.getMessage());
					
					// So it prints nicely
					fileName = "";
				}
			}
			else {
				// So it prints nicely
				fileName = "";
			}
			
			// Dump the info
			System.out.printf("\"%s\",\"%s\",\"%s\",\"%s\",\"%d\",\"%s\",\"%s\"\n",
			                  pkg.getType(),
			                  pkg.getVersion().toString(),
			                  Utils.formatStandardDateTime(pkg.getDate(), false),
			                  pkg.getInfo(),
			                  pkg.getRefCount(),
			                  pkg.getChecksum(),
			                  fileName);
		}
	}
	
	private void parseInput(Database db, String[] args,
			Collection<String> packages, Map<String, String> outFiles,
			LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("PackageType", "LowVersion", "HighVersion", "LowDate",
			                  "HighDate", "PackageChecksum", "PackageFile");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sPackageType = nullAsNecessary(parser.getColumn("PackageType"));
			String sLowVersion = nullAsNecessary(parser.getColumn("LowVersion"));
			String sHighVersion = nullAsNecessary(parser.getColumn("HighVersion"));
			String sPackageChecksum = nullAsNecessary(parser.getColumn("PackageChecksum"));
			String sPackageFile = nullAsNecessary(parser.getColumn("PackageFile"));
			
			// Check for required fields and parse as necessary
			
			if (sPackageFile != null && sPackageChecksum == null) {
				throw new BadParameterException(String.format("Bad file format: PackageFile specified on line %d without PackageChecksum",
				                                              parser.getLineNumber()));
			}
			
			// Type is a string
			
			Version lowVersion;
			if (sLowVersion == null) {
				lowVersion = null;
			}
			else {
				lowVersion = new Version(sLowVersion);
			}
			
			Version highVersion;
			if (sHighVersion == null) {
				highVersion = null;
			}
			else {
				highVersion = new Version(sHighVersion);
			}
			
			Date lowDate = parseDateColumn("LowDate",false,parser,"low date");
			Date highDate = parseDateColumn("HighDate",false,parser,"high date");
			
			// Checksum is a string
			
			if (sPackageFile != null) {
				outFiles.put(sPackageChecksum, sPackageFile);
			}
			
			if (sPackageType == null && sLowVersion == null &&
				sHighVersion == null && lowDate == null &&
				highDate == null && sPackageChecksum == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			
			// Grab the matching packages
			// We'll display them once we're sure everything is good
			packages.addAll(db.getMatchingPackages(sPackageType,
			                                       lowVersion,
			                                       highVersion,
			                                       lowDate,
			                                       highDate,
			                                       sPackageChecksum,
			                                       0,
			                                       -1,
			                                       null));
		}
	}
	
	@Override
	public String getUsage() {
		return "Usage: " + getActionName() + " [<file> [hdr]]\n" +
		       "<file>  Name of the file containing packages, or '-' to read from stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    PackageType (optional; display only packages of this type)\n" +
		       "    LowVersion (optional; display only packages with at least this version)\n" +
		       "    HighVersion (optional; display only packages with at most this version)\n" +
		       "    LowDate (optional; display only packages built on or after this date)\n" +
		       "    HighDate (optional; display only packages built on or before this date)\n" +
		       "    PackageChecksum (optional; display only packages with this checksum)\n" +
		       "    PackageFile (optional; dump the package to this file on the local machine;" +
		       "      you must specify PackageChecksum if you specify this)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" + "  If no file is specified, all packages are displayed.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data describing packages in the system.";
	}
}

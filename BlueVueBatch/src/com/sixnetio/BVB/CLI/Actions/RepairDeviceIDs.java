package com.sixnetio.BVB.CLI.Actions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.Capabilities;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.Modem.DeviceID;
import com.sixnetio.util.Utils;

/**
 * Loops through all modems in the database, altering their device IDs to the
 * new format (see commit f02a5e3055fd45b10ea0ae46dc8d8464e879ad9c).
 */
public class RepairDeviceIDs
    extends Action
{
    private static final Logger log = Logger.getLogger(Utils.thisClassName());

    /** Number of modems to process at a time. */
    private static final int CHUNK_SIZE = 128;

    static {
        Action.registerAction("RepairDeviceIDs", RepairDeviceIDs.class);
    }

    /**
     * Construct a new RepairDeviceIDs action.
     * 
     * @param name The name that was used to call the action.
     */
    public RepairDeviceIDs(String name)
    {
        super(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.sixnetio.BVB.CLI.Action#activate(com.sixnetio.BVB.Database.Database,
     * java.lang.String[])
     */
    @Override
    public void activate(Database db, String[] args)
        throws BadParameterException, DatabaseException
    {
        if (args.length > 0) {
            throw new BadParameterException(getActionName() +
                                            " takes no arguments");
        }

        // Cannot run this action while any servers are active
        enforceNoServers(db);
        performUpdate(db);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.sixnetio.BVB.CLI.Action#getUsage()
     */
    @Override
    public String getUsage()
    {
        return "Usage: " +
               getActionName() +
               "\n" +
               "Output: CSV data listing the updated modem IDs. Includes a header row.\n" +
               "No servers may be running when this command is activated.";
    }

    private void enforceNoServers(Database db)
        throws DatabaseException
    {
        Collection<Capabilities> caps = db.getCurrentCapabilities();
        if (caps.size() > 0) {
            throw new DatabaseException(
                "Cannot run RepairDeviceIDs action when servers are active");
        }
    }

    private void performUpdate(Database db)
        throws DatabaseException
    {
        List<Integer> ids =
            new ArrayList<Integer>(db.getMatchingModems(null, null, null, null,
                null, null, null, null, null, null, 0, -1, null, null));

        log.info("Processing " + ids.size() + " modems");

        System.out.printf("\"%s\"\n", "ModemID");

        // Work in clumps, to avoid a possible out-of-memory condition
        List<Integer> chunk = new ArrayList<Integer>(CHUNK_SIZE);
        int pos = 0;
        while (pos < ids.size()) {
            int end = pos + CHUNK_SIZE;
            if (end > ids.size()) {
                end = ids.size();
            }

            chunk.clear();
            chunk.addAll(ids.subList(pos, end));

            log.debug("Processing modems [" + pos + ", " + end + ")");

            pos = end;

            Map<Integer, Modem> modems = db.getModems(chunk);
            for (Modem modem : modems.values()) {
                if (modem.deviceID != null) {
                    modem.deviceID =
                        DeviceID.parseDeviceID(modem.deviceID.toString());

                    // Unfortunately, there is no batch-save yet, so do this
                    // here
                    db.updateModem(modem);

                    System.out.printf("\"%d\"\n", modem.modemID);
                }
            }
        }
    }
}

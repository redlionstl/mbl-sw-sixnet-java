/*
 * ApplyTagsAction.java
 *
 * Applies tags to and removes tags from modems.
 *
 * Jonathan Pearson
 * February 5, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class ApplyTagsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// The literal strings for the commands are duplicated below. Should be
	// constants. -- CLN
	static {
		Action.registerAction("Apply_Tags", ApplyTagsAction.class);
		Action.registerAction("Remove_Tags", ApplyTagsAction.class);
	}
	
	public ApplyTagsAction(String name) {
		super(name);
	}
	
	/**
	 * @param args
	 *            Should contain one and only one argument: the file to process.
	 *            May be "-" to read data from stdin.
	 * 
	 *  Each line of input (file or stdin) is "tagID,modemID"
	 */
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		// [0] = Tag ID
		// [1] = Modem ID
		List<int[]> pairs = new LinkedList<int[]>();
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(args, in, pairs, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Update the tags pairings in the database, printing out a little
		// identifying info about each
		System.out.println("\"TagID\",\"ModemID\"");
		
		if (getActionName().equals("Apply_Tags")) {
			addTags(db, pairs);
		}
		// This is weak.  Should check for remove command? -- CLN
		else {
			removeTags(db, pairs);
		}
	}
	
	private void parseInput(String[] args, LineNumberReader in,
			List<int[]> pairs, CSVParser parser)
		throws IOException, BadParameterException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("TagID", "ModemID");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sTagID = nullAsNecessary(parser.getColumn("TagID"));
			String sModemID = nullAsNecessary(parser.getColumn("ModemID"));
			
			// Check for required fields and parse as necessary
			int tagID;
			if (sTagID == null) {
				throw new BadParameterException(String.format("Bad file format: line %d had no tag ID",
				                                              parser.getLineNumber()));
			}
			else {
				try {
					tagID = Integer.parseInt(sTagID);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d had a non-numeric tag ID",
					                                              parser.getLineNumber()),
					                                nfe);
				}
			}
			
			int modemID;
			if (sModemID == null) {
				throw new BadParameterException(String.format("Bad file format: line %d had no modem ID",
				                                              parser.getLineNumber()));
			}
			else {
				try {
					modemID = Integer.parseInt(sModemID);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d had a non-numeric modem ID",
					                                              parser.getLineNumber()),
					                                nfe);
				}
			}
			
			// We'll write them all to the database once we're sure
			// everything is good
			pairs.add(new int[] { tagID, modemID });
		}
	}
	
	private void addTags(Database db, List<int[]> pairs)
	{
		for (int[] pair : pairs) {
			try {
				db.applyTag(pair[1], pair[0]);
				
				System.out.printf("\"%d\",\"%d\"\n", // Format
				                  pair[0], // Tag ID
				                  pair[1]); // Modem ID
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to apply tag %d to modem %d: %s\n",
				                  pair[0], pair[1], de.getMessage());
			}
		}
	}
	
	private void removeTags(Database db, List<int[]> pairs)
	{
		for (int[] pair : pairs) {
			try {
				db.removeTag(pair[1], pair[0]);
				
				System.out.printf("\"%d\",\"%d\"\n", // Format
				                  pair[0], // Tag ID
				                  pair[1]); // Modem ID
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to remove tag %d from modem %d: %s\n",
				                  pair[0], pair[1], de.getMessage());
			}
		}
	}
	
	@Override
	public String getUsage() {
		String applyRemove;
		
		if (getActionName().equals("Apply_Tags")) {
			applyRemove = "applied";
		}
		else {
			applyRemove = "removed";
		}
		
		return (String.format("Usage: " + getActionName() + " <file> [hdr]\n" +
		                      "<file>  Name of the file containing tag/modem pairs, or '-' to read from stdin\n" +
		                      "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		                      "    TagID\n" +
		                      "    ModemID\n" +
		                      "  All columns are required.\n" +
		                      "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		                      "  read from the first line in the file, allowing for reordering.\n" +
		                      "Output: CSV data describing the tags that were %s, including a header row.",
		                      applyRemove));
	}
}

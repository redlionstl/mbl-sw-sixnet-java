/*
 * UpdateSettingsAction.java
 *
 * Updates settings.
 *
 * Jonathan Pearson
 * February 11, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.Settings.Setting;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class UpdateSettingsAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static {
		Action.registerAction("Update_Settings", UpdateSettingsAction.class);
	}
	
	public UpdateSettingsAction(String name) {
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Parse arguments
		String fileName;
		
		// HashMap allows for null values
		HashMap<String, String> settings = new HashMap<String, String>();
		
		try {
			fileName = args[0];
		}
		catch (ArrayIndexOutOfBoundsException aioobe) {
			throw new BadParameterException("File name is required", aioobe);
		}
		
		LineNumberReader in = makeReader(fileName);
		
		// Read the CSV file line-by-line
		CSVParser parser = new CSVParser(in);
		
		try {
			parseInput(args, settings, in, parser);
		}
		catch (IOException ioe) {
			logger.debug("Unable to read file", ioe);
			
			throw new BadParameterException(String.format("Unable to read file '%s': %s",
			                                              fileName,
			                                              ioe.getMessage()),
			                                ioe);
		}
		finally {
			try {
				parser.close();
			}
			catch (IOException ioe) {
				// Ignore it
				logger.debug("Close failed", ioe);
			}
		}
		
		// Update and print
		System.out.println("\"SettingName\",\"SettingValue\"");
		
		for (Map.Entry<String, String> entry : settings.entrySet()) {
			try {
				String key = entry.getKey();
				String value = entry.getValue();
				
				db.updateSetting(key, value);
				
				if (value == null) {
					value = ""; // For prettier output
				}
				
				System.out.printf("\"%s\",\"%s\"\n", key, value);
			}
			catch (DatabaseException de) {
				logger.error("Database exception", de);
				
				System.err.printf("Unable to update setting '%s': %s\n",
				                  entry.getKey(),
				                  de.getMessage());
			}
		}
	}
	
	private void parseInput(String[] args, HashMap<String, String> settings,
			LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("SettingName", "SettingValue");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String settingName = nullAsNecessary(parser.getColumn("SettingName"));
			String settingValue = nullAsNecessary(parser.getColumn("SettingValue"));
			
			// Check for required fields and parse as necessary
			if (settingName == null) {
				throw new BadParameterException(String.format("Bad file format: line %d has no setting name",
				                                              parser.getLineNumber()));
			}
			
			if (settingValue == null) {
				throw new BadParameterException(String.format("Bad file format: line %d has no setting value",
				                                              parser.getLineNumber()));
			}
			
			// Hold onto the name, grab all at once later
			if (settingValue.equals("-")) {
				settings.put(settingName, null);
			}
			else if (settingValue.equals("_")) {
				Setting s = Setting.fromString(settingName);
				
				if (s == null) {
					logger.error(String.format("No setting named %s, cannot set default value",
					                           settingName));
					
					System.err.printf("No setting named %s, cannot set default value\n",
					                  settingName);
					continue;
				}
				
				settings.put(settingName, s.defaultValue);
			}
			else {
				settings.put(settingName, settingValue);
			}
		}
	}
	
	@Override
	public String getUsage()
	{
		return "Usage: " + getActionName() + " <file> [hdr]\n" +
		       "<file>  Name of the file containing settings, or '-' to read from stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    SettingName\n" +
		       "    SettingValue (use '-' to set to null; use '_' to set to default)\n" +
		       "  All columns are required.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data describing the specified settings. Includes a header row.";
	}
}

/*
 * ViewHistoryAction.java
 *
 * View history.
 *
 * Jonathan Pearson
 * February 11, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.HistoryEntry;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.io.CSVParser;
import com.sixnetio.util.Utils;

public class ViewHistoryAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("View_History", ViewHistoryAction.class);
	}
	
	public ViewHistoryAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		Collection<Long> history = new LinkedList<Long>();
		
		// Parse arguments
		if (args.length > 0) {
			// User specified a file name, read info to display from there
			String fileName;
			
			fileName = args[0];
			
			LineNumberReader in = makeReader(fileName);
			
			// Read the CSV file line-by-line
			CSVParser parser = new CSVParser(in);
			
			try {
				parseInput(db, args, history, in, parser);
			}
			catch (IOException ioe) {
				logger.debug("Unable to read file", ioe);
				
				throw new BadParameterException(String.format("Unable to read file '%s': %s",
				                                              fileName,
				                                              ioe.getMessage()),
				                                ioe);
			}
			finally {
				try {
					parser.close();
				}
				catch (IOException ioe) {
					// Ignore it
					logger.debug("Close failed", ioe);
				}
			}
		}
		else {
			// Get all history
			history = db.getMatchingHistory(null, null, null, null, null, null,
			                                null, null, null, 0, -1, null, null);
		}
		
		// Dump
		System.out.println("\"HistoryID\",\"Timestamp\",\"JobID\",\"BatchID\"," +
		                   "\"ModemID\",\"UserID\",\"JobType\",\"Result\"," +
		                   "\"Message\"");
		
		for (long historyID : history) {
			try {
				HistoryEntry entry = db.getHistoryEntry(historyID);
				
				System.out.printf("\"%d\",\"%s\",\"%d\",\"%s\",\"%d\",\"%s\",\"%s\",\"%s\",\"%s\"\n",
				                  entry.historyID, // History ID
				                  Utils.formatStandardDateTime(entry.timestamp, false), // Timestamp
				                  entry.jobID, // Job ID
				                  (entry.batchID == null ? "" : "" + entry.batchID), // BatchID
				                  entry.modemID, // Modem ID
				                  (entry.userID == null ? "" : "" + entry.userID), // UserID
				                  entry.type, // Job type
				                  entry.result, // Result
				                  entry.message); // Message
			}
			catch (DatabaseException de) {
				logger.error("Unable to retrieve history entry", de);
				
				System.err.printf("Unable to retrieve entry for history id %d\n", historyID);
			}
		}
	}
	
	private void parseInput(Database db, String[] args,
			Collection<Long> history, LineNumberReader in, CSVParser parser)
		throws IOException, BadParameterException, DatabaseException
	{
		if (args.length > 1 && args[1].equals("hdr")) {
			// Read column header names from the file
			parser.addColumns(Utils.tokenize(in.readLine(), ",", false));
		}
		else {
			parser.addColumns("HistoryID", "JobID", "BatchID", "ModemID", "UserID",
			                  "JobType", "Result", "LowDate", "HighDate");
		}
		
		while (parser.next()) {
			parser.dumpWarnings(System.err);
			
			String sUserID = nullAsNecessary(parser.getColumn("UserID"));
			String sJobType = nullAsNecessary(parser.getColumn("JobType"));
			String sResult = nullAsNecessary(parser.getColumn("Result"));
			
			// Check for required fields and parse as necessary
			
			Long historyID = parseLongColumn("HistoryID",false,parser,"history ID");
			Long jobID = parseLongColumn("JobID",false,parser,"job ID");
			Integer batchID = parseIntColumn("BatchID",false,parser,"batch ID");
			Integer modemID = parseIntColumn("ModemID",false,parser,"modem ID");
			
			Integer userID;
			if (sUserID == null) {
				userID = null;
			}
			else if (sUserID.equals("-")) {
				userID = -1;
			}
			else {
				try {
					userID = Integer.parseInt(sUserID);
				}
				catch (NumberFormatException nfe) {
					throw new BadParameterException(String.format("Bad file format: line %d has a non-numeric user ID",
					                                              parser.getLineNumber()));
				}
			}
			
			// Type is a string
			// Result is a string
			
			Date lowDate = parseDateColumn("LowDate",false,parser,"low date");
			Date highDate = parseDateColumn("HighDate",false,parser,"high date");
			
			if (historyID == null && jobID == null && batchID == null &&
			    modemID == null && sUserID == null && sJobType == null &&
			    sResult == null && lowDate == null && highDate == null) {
				
				throw new BadParameterException(String.format("Bad file format: empty line %d",
				                                              parser.getLineNumber()));
			}
			// Grab the matching history entries
			// We'll write them all to the console once we're sure
			// everything is good
			history.addAll(db.getMatchingHistory(historyID, jobID, batchID, modemID,
			                                     userID, sJobType, sResult, lowDate,
			                                     highDate, 0, -1, null, null));
		}
	}
	
	@Override
	public String getUsage() {
		return "Usage: " + getActionName() + " [<file> [hdr]]\n" +
		       "[<file>]  Optional name of the file containing history patterns, or '-' to read\n" +
		       "  from stdin\n" +
		       "  File must be CSV formatted. Here are the expected columns, in order:\n" +
		       "    HistoryID (optional; display only this history entry)\n" +
		       "    JobID (optional; display only the history of this job)\n" +
		       "    BatchID (optional; display only the history of this batch)\n" +
		       "    ModemID (optional; display only the history of jobs against this modem)\n" +
		       "    UserID (optional; display only the history of jobs by this user; use '-' to\n" +
		       "      match entries with no user)\n" +
		       "    JobType (optional; display only the history of jobs of this type)\n" +
		       "    Result (optional; display only the history of jobs with this result)\n" +
		       "    LowDate (optional; specify a low end for date searching)\n" +
		       "    HighDate (optional; specify a high end for date searching)\n" +
		       "  You must specify at least one column per row. All columns must exist, but any\n" +
		       "    may be blank.\n" +
		       "[hdr]  If you have the string 'hdr' after the file name, column headers will be\n" +
		       "  read from the first line in the file, allowing for reordering.\n" +
		       "Output: CSV data describing specified history, or if no file is specified,\n" +
		       "  describes all history in the system. Includes a header row.";
	}
}

package com.sixnetio.BVB.CLI.Actions;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.Action;
import com.sixnetio.BVB.CLI.BadParameterException;
import com.sixnetio.BVB.Common.Modem;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.Utils;

/**
 * Loops through all modems in the database, searching for device IDs that
 * resolve to the same String format.
 */
public class DetectDuplicateDevIDs
    extends Action
{
    private static final Logger log = Logger.getLogger(Utils.thisClassName());

    /** Number of modems to process at a time. */
    private static final int CHUNK_SIZE = 128;

    static {
        Action.registerAction("DupDevIDChk", DetectDuplicateDevIDs.class);
    }

    /**
     * Construct a new DetectDuplicateDevIDs action.
     * 
     * @param name The name that was used to call the action.
     */
    public DetectDuplicateDevIDs(String name)
    {
        super(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.sixnetio.BVB.CLI.Action#activate(com.sixnetio.BVB.Database.Database,
     * java.lang.String[])
     */
    @Override
    public void activate(Database db, String[] args)
        throws BadParameterException, DatabaseException
    {
        if (args.length > 0) {
            throw new BadParameterException(getActionName() +
                                            " takes no arguments");
        }

        performSearch(db);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.sixnetio.BVB.CLI.Action#getUsage()
     */
    @Override
    public String getUsage()
    {
        return "Usage: " +
               getActionName() +
               "\n" +
               "Output: CSV data listing sets of modem IDs that resolve to the same device ID.\n" +
               "Includes a header row.";
    }

    private void performSearch(Database db)
        throws DatabaseException
    {
        List<Integer> ids =
            new ArrayList<Integer>(db.getMatchingModems(null, null, null, null,
                null, null, null, null, null, null, 0, -1, null, null));

        log.info("Processing " + ids.size() + " modems");

        Map<String, Integer> seenSoFar = new HashMap<String, Integer>();
        Map<String, List<Integer>> duplicates =
            new HashMap<String, List<Integer>>();

        // Work in clumps, to avoid a possible out-of-memory condition
        List<Integer> chunk = new ArrayList<Integer>(CHUNK_SIZE);
        int pos = 0;
        while (pos < ids.size()) {
            int end = pos + CHUNK_SIZE;
            if (end > ids.size()) {
                end = ids.size();
            }

            chunk.clear();
            chunk.addAll(ids.subList(pos, end));

            log.debug("Processing modems [" + pos + ", " + end + ")");

            pos = end;

            Map<Integer, Modem> modems = db.getModems(chunk);
            for (Modem modem : modems.values()) {
                if (modem.deviceID != null) {
                    String id = modem.deviceID.toString();
                    if (seenSoFar.containsKey(id)) {
                        List<Integer> dups = duplicates.get(id);
                        if (dups == null) {
                            dups = new LinkedList<Integer>();
                            dups.add(seenSoFar.get(id));
                            duplicates.put(id, dups);
                        }

                        dups.add(modem.modemID);
                    }
                    else {
                        seenSoFar.put(id, modem.modemID);
                    }
                }
            }
        }

        System.out.printf("\"%s\"\n", "ModemID Set");

        for (List<Integer> dups : duplicates.values()) {
            System.out.printf("\"");

            boolean first = true;
            for (Integer id : dups) {
                if (first) {
                    first = false;
                }
                else {
                    System.out.printf(",");
                }

                System.out.printf("%d", id);
            }

            System.out.printf("\"\n");
        }
    }
}

/*
 * ChangeUserPasswordAction.java
 *
 * Allows the user to change the password of a BVB user account.
 *
 * Jonathan Pearson
 * July 28, 2009
 *
 */

package com.sixnetio.BVB.CLI.Actions;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.sixnetio.BVB.CLI.*;
import com.sixnetio.BVB.Common.User;
import com.sixnetio.BVB.Database.Database;
import com.sixnetio.BVB.Database.DatabaseException;
import com.sixnetio.util.Utils;

public class ChangeUserPasswordAction
	extends Action
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	static
	{
		Action.registerAction("Change_Password", ChangeUserPasswordAction.class);
	}
	
	public ChangeUserPasswordAction(String name)
	{
		super(name);
	}
	
	@Override
	public void activate(Database db, String[] args)
		throws BadParameterException, DatabaseException
	{
		// Read the arguments for users
		for (String userName : args) {
			// Get the user with this name from the database
			Collection<Integer> userIDs = db.getMatchingUsers(null, userName,
			                                                  null, 0, -1, null,
			                                                  null);
			
			if (userIDs.size() == 0) {
				System.err.printf("Unable to locate user '%s'\n", userName);
				continue;
			}
			else if (userIDs.size() >= 2) {
				System.err.printf("Database corruption error: " +
				                  "Multiple users named '%s'\n", userName);
				
				continue;
			}
			
			User user = db.getUser(userIDs.iterator().next());
			
			String newPassword = BVBatch.confirmPassword("Enter new password for user '%s': ",
			                                             userName);
			
			user.hashedPassword = User.hashPassword(user.name, newPassword);
			db.updateUser(user);
			
			System.out.printf("Password updated for user '%s'\n", userName);
		}
	}
	
	@Override
	public String getUsage() {
		return "Usage: " + getActionName() + " [<user name> [<user name> [...]]]\n" +
		       "  Asks for and confirms a new password for each named user.\n" +
		       "Output: A request for each password, followed by a request for\n" +
		       "  confirmation.";
	}
}

/*
 * BadActionException.java
 *
 * Thrown when an action cannot be instantiated.
 *
 * Jonathan Pearson
 * February 4, 2009
 *
 */

package com.sixnetio.BVB.CLI;

public class BadActionException extends Exception {
	private final String name;
	
	public BadActionException(String message, String name) {
		super(message);
		
		this.name = name;
	}
	
	public BadActionException(String message, String name, Throwable cause) {
		super(message, cause);
		
		this.name = name;
	}
	
	public String getMessage() {
		return String.format("%s: %s", super.getMessage(), name);
	}
}

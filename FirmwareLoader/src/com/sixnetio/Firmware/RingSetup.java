/*
 * RingSetup.java
 *
 * A window that helps the user design a project for loading firmware to a ring of switches.
 *
 * Jonathan Pearson
 * December 21, 2007
 *
 */

package com.sixnetio.Firmware;

import com.sixnetio.GUI.*;
import com.sixnetio.util.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.filechooser.*;

import java.io.*;
import java.util.*;

public class RingSetup extends JFrame implements ActionListener, ProjectUI {
	// class RingDiagram
	private class RingDiagram extends Canvas implements MouseListener {
		private static final int MARGIN = 10; // Distance from the nearest edge to the ring, in pixels
		private static final int TICK_RADIUS = 6; // Radius of a tick mark
		
		public RingDiagram() {
			addMouseListener(this);
			setPreferredSize(new Dimension(300, 300));
		}
		
		// paint()
		public void paint(Graphics g) {
			// Draw a white square over the canvas
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, getWidth(), getHeight());
			
			// Draw a black circle as large as will look attractive
			int radius = (Math.min(getWidth(), getHeight()) - MARGIN * 2) / 2;
			int centerX = getWidth() / 2;
			int centerY = getHeight() / 2;
			
			g.setColor(Color.BLACK);
			g.drawOval(centerX - radius, centerY - radius, radius * 2, radius * 2);
			
			// Move around the circle, drawing tick marks and IP addresses for each switch
			int counter = 0;
			for (Project.Switch sw : switches) {
				int[] coords = getTickCoords(counter, switches.size(), radius);
				
				// Transpose to the center of the circle
				int x = coords[0] + centerX;
				int y = coords[1] + centerY;
				
				// Draw the tick
				if (rootIndex == counter) {
					g.setColor(Color.RED);
				} else {
					g.setColor(Color.WHITE);
				}
				
				if (selectedIndex == counter) {
					g.setColor(Color.BLACK);
				}
				
				g.fillOval(x - TICK_RADIUS, y - TICK_RADIUS, TICK_RADIUS * 2, TICK_RADIUS * 2);
				
				g.setColor(Color.RED);
				g.drawOval(x - TICK_RADIUS, y - TICK_RADIUS, TICK_RADIUS * 2, TICK_RADIUS * 2);
				g.drawOval(x - TICK_RADIUS + 1, y - TICK_RADIUS + 1, TICK_RADIUS * 2 - 2, TICK_RADIUS * 2 - 2);
				
				// Draw the IP address
				// Do some more arc-based choices to place the text correctly
				String ip = sw.getAddress();
				Rectangle2D bounds = g.getFontMetrics().getStringBounds(ip, g);
				
				// Draw right on top of the tick
				int ipX = x - (int)bounds.getWidth() / 2;
				int ipY = y + (int)bounds.getHeight() / 2 + TICK_RADIUS + 3;
				
				// But correct for drawing outside the canvas
				if (ipX < 0) ipX = 0;
				if (ipX + (int)bounds.getWidth() > getWidth()) ipX = getWidth() - (int)bounds.getWidth();
				
				if (ipY - (int)bounds.getHeight() < 0) ipY = (int)bounds.getHeight();
				if (ipY > getHeight()) ipY = getHeight();
				
				g.setColor(Color.BLACK);
				g.drawString(ip, ipX, ipY);
				
				counter++;
			}
		}
		
		// getTickCoords()
		// Note: You still need to transpose to the center of the circle
		private int[] getTickCoords(int curTick, int tickCount, int radius) {
			// Figure out where this switch will fall
			// Start at 12 o'clock and work clockwise
			double arc = 2 * Math.PI / tickCount; // Arc between two neighboring switches
			double curArc = curTick * arc + Math.PI / 2;
			curArc %= 2 * Math.PI;
			
			int x = (int)Math.abs(radius * Math.cos(curArc));
			int y = (int)Math.abs(radius * Math.sin(curArc));
			
			// Correct for signing problems
			if (curArc < Math.PI / 2) {
				// Quadrant 1
				// No change (x and y are both positive)
			} else if (curArc < Math.PI) {
				// Quadrant 2
				x = -x;
			} else if (curArc < Math.PI * 3 / 2) {
				// Quadrant 3
				x = -x;
				y = -y;
			} else {
				// Quadrant 4
				y = -y;
			}
			
			// Work clockwise
			x = -x;
			
			// Correct for inverted y coordinates
			y = -y;
			
			int[] ans = {x, y};
			return ans;
		}
		
		public void mouseClicked(MouseEvent e) { }
		public void mouseEntered(MouseEvent e) { }
		public void mouseExited(MouseEvent e) { }
		public void mouseReleased(MouseEvent e) { }
		
		// mousePressed
		public void mousePressed(MouseEvent e) {
			// Determine which switch was clicked on
			// Set selectedIndex, tell the outer class to fill the boxes
			
			// Draw a black circle as large as will look attractive
			int radius = (Math.min(getWidth(), getHeight()) - MARGIN * 2) / 2;
			int centerX = getWidth() / 2;
			int centerY = getHeight() / 2;
			
			// Move around the circle, checking whether each tick mark contains the coordinate where the user clicked
			int counter = 0;
			selectedIndex = -1;
			for (Project.Switch sw : switches) {
				// Figure out where this switch fell
				// Start at 12 o'clock and work clockwise
				int[] coords = getTickCoords(counter, switches.size(), radius);
				
				// Transpose to match the center of the circle
				int x = coords[0] + centerX;
				int y = coords[1] + centerY;
				
				// Compare this tick to the click
				if (Math.sqrt(Math.pow(x - e.getX(), 2) + Math.pow(y - e.getY(), 2)) <= TICK_RADIUS) {
					selectedIndex = counter;
					break;
				}
				
				counter++;
			}
			
			populateSelectedSwitch();
			repaint();
		}
	}
	
	// Private data members
	private JTabbedPane tabMain;
	
	JPanel switchSettings;
	
	// Automatic method
	private JTextField txtStartIP,
	                   txtEndIP,
	                   txtIncrement;
	private JButton btnAddRange;
	
	// Manual method
	private JTextField txtIPAddress;
	private JButton btnAddSwitch;
	
	// Common fields
	private JTextField txtSubnet,
	                   txtGateway,
	                   txtUser;
	private JPasswordField txtPassword,
	                       txtConfirm;
	
	// Ring diagram
	private RingDiagram ringDiagram;
	
	// Save button
	private JButton btnSave;
	
	// Switch settings
	private JTextField txtSwitchIP,
	                   txtSwitchSubnet,
	                   txtSwitchGateway;
	private JCheckBox chkRootSwitch;
	private JButton btnMoveClockwise,
	                btnMoveCounter,
	                btnApply,
	                btnRemoveSwitch;
	
	private Project project;
	private int selectedIndex = -1;
	private int rootIndex = 0;
	private Vector<Project.Switch> switches; // For easy access; we can order them later
	
	private FirmwareLoaderGUI2 owner;
	
	// Constructor
	public RingSetup(FirmwareLoaderGUI2 owner) {
		this.owner = owner;
		
		setIconImage((new ImageIcon(this.getClass().getClassLoader().getResource("images/firmload/ring.gif"))).getImage());
		
		switches = new Vector<Project.Switch>();
		
		// Set up the project so it has the necessary Ring Loading options
		// Any other useful data, just copy from the other project
		project = new Project(this);
		project.setSaveConfig(true);
		project.setForceLoad(true);
		project.setParallel(true);
		project.setBottomToTop(true);
		project.setProbeAfter(false);
		
		project.setIPAddress(owner.getConfig().getIPAddress());
		project.setFirmwareFile(owner.getConfig().getFirmwareFile());
		project.setUser(owner.getConfig().getUser());
		project.setPassword(owner.getConfig().getPassword());
		project.setSerial(owner.getConfig().getSerial());
		project.setProtectedFile(owner.getProtectFiles());
		
		
		setLayout(new GridLayout(1, 1));
		
		tabMain = new JTabbedPane();
		add(tabMain);
		
		// Add Switches panel
		{
			JPanel p = new JPanel();
			tabMain.addTab("Add Switches", null, p, "Add switches to your ring");
			
			p.setLayout(new BorderLayout());
			
			// Two methods:
			// Automatic: choose a starting and ending IP address and an amount to increment by, we add switches in between
			// Manual: One-by-one, set the IP address and click "Add"
			// Both methods are always available, neither removes anything (but it will refuse to add duplicates)
			// So you can use Automatic for ranges, interspersed with Manual for random weirdness
			
			// Also, there is a diagram of a circle, with marks accompanied by IP addresses to show the user
			// Click on a mark to edit/move/delete it
			
			// West (everything but the diagram)
			{
				JPanel west = new JPanel();
				p.add(west, BorderLayout.WEST);
				
				GridBagLayout gbl = new GridBagLayout();
				GridBagConstraints gc = new GridBagConstraints();
				gc.fill = GridBagConstraints.HORIZONTAL;
				gc.gridwidth = GridBagConstraints.REMAINDER;
				
				west.setLayout(gbl);
				
				// Automatic method
				{
					JPanel automatic = new JPanel();
					automatic.setBorder(BorderFactory.createTitledBorder("Automatic"));
					gbl.setConstraints(automatic, gc);
					west.add(automatic);
					
					JLabel lblStartIP = new JLabel("First IP: ");
					txtStartIP = new JTextField(12);
					
					JLabel lblEndIP = new JLabel("Last IP: ");
					txtEndIP = new JTextField(12);
					
					JLabel lblIncrement = new JLabel("Increment by: ");
					txtIncrement = new JTextField(4);
					txtIncrement.setText("1");
					
					btnAddRange = new JButton("Add Range");
					
					// Set the action listeners
					btnAddRange.addActionListener(this);
					
					// Add to the panel
					automatic.setLayout(new BorderLayout());
					
					{
						JPanel note = new JPanel();
						note.setLayout(new GridLayout(0, 1));
						automatic.add(note, BorderLayout.NORTH);
						
						JLabel lblNote = new JLabel("Note: Only for use when adding a range of IP");
						note.add(lblNote);
						
						lblNote = new JLabel("addresses with the first three octets equal");
						note.add(lblNote);
					}
					
					JPanel center = new JPanel();
					center.setLayout(new GridLayout(0, 2));
					automatic.add(center, BorderLayout.CENTER);
					
					center.add(lblStartIP);
					center.add(txtStartIP);
					
					center.add(lblEndIP);
					center.add(txtEndIP);
					
					center.add(lblIncrement);
					center.add(txtIncrement);
					
					JPanel south = new JPanel();
					south.setLayout(new FlowLayout());
					automatic.add(south, BorderLayout.SOUTH);
					
					south.add(btnAddRange);
				}
				
				// Manual method
				{
					JPanel manual = new JPanel();
					manual.setBorder(BorderFactory.createTitledBorder("Manual"));
					gbl.setConstraints(manual, gc);
					west.add(manual);
					
					JLabel lblIPAddress = new JLabel("IP Address: ");
					txtIPAddress = new JTextField(12);
					
					btnAddSwitch = new JButton("Add Switch");
					
					// Set the action listeners
					btnAddSwitch.addActionListener(this);
					
					// Add to the panel
					manual.setLayout(new GridLayout(0, 2));
					
					manual.add(lblIPAddress);
					manual.add(txtIPAddress);
					
					manual.add(new JPanel()); // Spacer
					manual.add(btnAddSwitch);
				}
				
				// Common fields
				{
					JPanel common = new JPanel();
					common.setBorder(BorderFactory.createTitledBorder("Common"));
					gbl.setConstraints(common, gc);
					west.add(common);
					
					JLabel lblSubnet = new JLabel("Subnet Mask: ");
					txtSubnet = new JTextField(12);
					txtSubnet.setText("255.0.0.0");
					
					JLabel lblGateway = new JLabel("Gateway: ");
					txtGateway = new JTextField(12);
					
					JLabel lblUser = new JLabel("User: ");
					txtUser = new JTextField(project.getUser(), 12);
					
					JLabel lblPassword = new JLabel("Password: ");
					txtPassword = new JPasswordField(project.getPassword(), 12);
					
					JLabel lblConfirm = new JLabel("Confirm: ");
					txtConfirm = new JPasswordField(project.getPassword(), 12);
					
					// Add to the panel
					common.setLayout(new GridLayout(0, 2));
					
					common.add(lblSubnet);
					common.add(txtSubnet);
					
					common.add(lblGateway);
					common.add(txtGateway);
					
					common.add(lblUser);
					common.add(txtUser);
					
					common.add(lblPassword);
					common.add(txtPassword);
					
					common.add(lblConfirm);
					common.add(txtConfirm);
				}
				
				// Switch settings
				{
					switchSettings = new JPanel();
					switchSettings.setBorder(BorderFactory.createTitledBorder("Switch Settings"));
					gbl.setConstraints(switchSettings, gc);
					west.add(switchSettings);
					
					JLabel lblSwitchIP = new JLabel("IP Address: ");
					txtSwitchIP = new JTextField(12);
					
					JLabel lblSwitchSubnet = new JLabel("Subnet Mask: ");
					txtSwitchSubnet = new JTextField(12);
					
					JLabel lblSwitchGateway = new JLabel("Gateway: ");
					txtSwitchGateway = new JTextField(12);
					
					chkRootSwitch = new JCheckBox("Root switch");
					
					btnMoveClockwise = new JButton("Move Clockwise");
					btnMoveCounter = new JButton("Move Counter-Clockwise");
					
					btnApply = new JButton("Apply");
					btnRemoveSwitch = new JButton("Remove");
					
					// Set action listeners
					btnMoveClockwise.addActionListener(this);
					btnMoveCounter.addActionListener(this);
					btnApply.addActionListener(this);
					btnRemoveSwitch.addActionListener(this);
					
					// Add to the panel
					switchSettings.setLayout(new GridLayout(0, 2));
					
					switchSettings.add(lblSwitchIP);
					switchSettings.add(txtSwitchIP);
					
					switchSettings.add(lblSwitchSubnet);
					switchSettings.add(txtSwitchSubnet);
					
					switchSettings.add(lblSwitchGateway);
					switchSettings.add(txtSwitchGateway);
					
					switchSettings.add(chkRootSwitch);
					switchSettings.add(new JPanel()); // Spacer
					
					switchSettings.add(btnMoveClockwise);
					switchSettings.add(btnMoveCounter);
					
					switchSettings.add(btnApply);
					switchSettings.add(btnRemoveSwitch);
					
					switchSettings.setEnabled(false);
				}
			}
			
			// Center (diagram of a ring, save button)
			{
				JPanel center = new JPanel();
				center.setLayout(new BorderLayout());
				p.add(center, BorderLayout.CENTER);
				
				ringDiagram = new RingDiagram();
				center.add(ringDiagram, BorderLayout.CENTER);
				
				btnSave = new JButton("Save");
				btnSave.addActionListener(this);
				center.add(btnSave, BorderLayout.SOUTH);
			}
		}
		
		setTitle("Ring Setup");
		pack();
		setVisible(true);
	}
	
	// Action Performed
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnAddRange) {
			// {{{
			try {
				// Parse the start/end IP addresses
				int[] start = Utils.parseIP(txtStartIP.getText());
				int[] end = Utils.parseIP(txtEndIP.getText());
				int[] ip = new int[4];
				int incr;
				
				try {
					incr = Integer.parseInt(txtIncrement.getText());
					if (incr <= 0) {
						JOptionPane.showMessageDialog(this, "Increment must be greater than 0", "Error", JOptionPane.ERROR_MESSAGE);
						return;
					}
				} catch (NumberFormatException nfe) {
					Utils.debug(nfe);
					JOptionPane.showMessageDialog(this, "Not a number: " + txtIncrement.getText(), "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				int[] subnet = Utils.parseIP(txtSubnet.getText());
				int[] gateway;
				if (txtGateway.getText().length() > 0) {
					gateway = Utils.parseIP(txtGateway.getText());
				} else {
					gateway = null; // Optional
				}
					
				
				System.arraycopy(start, 0, ip, 0, ip.length);
				
				for (int i = start[3]; i <= end[3]; i += incr) {
					ip[3] = i;
					addSwitch(ip, subnet, gateway);
				}
				ringDiagram.repaint();
			} catch (NumberFormatException nfe) {
				Utils.debug(nfe);
				JOptionPane.showMessageDialog(this, "Error: " + nfe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		} else if (e.getSource() == btnAddSwitch) {
			// {{{
			try {
				int[] ip = Utils.parseIP(txtIPAddress.getText());
				int[] subnet = Utils.parseIP(txtSubnet.getText());
				int[] gateway;
				
				if (txtGateway.getText().length() > 0) {
				    gateway = Utils.parseIP(txtGateway.getText());
			    } else {
				    gateway = null; // Optional
			    }
				
				addSwitch(ip, subnet, gateway);
				
				selectedIndex = switches.size() - 1;
				ringDiagram.repaint();
				populateSelectedSwitch();
			} catch (NumberFormatException nfe) {
				Utils.debug(nfe);
				JOptionPane.showMessageDialog(this, "Error: " + nfe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		} else if (e.getSource() == btnMoveClockwise) {
			// {{{
			if (selectedIndex == -1) return;
			
			int swapWith = selectedIndex + 1;
			swapWith %= switches.size();
			swapSwitches(selectedIndex, swapWith);
			selectedIndex = swapWith;
			ringDiagram.repaint();
			populateSelectedSwitch();
		} else if (e.getSource() == btnMoveCounter) {
			// {{{
			if (selectedIndex == -1) return;
			
			int swapWith = selectedIndex - 1;
			if (swapWith < 0) swapWith = switches.size() - 1;
			swapSwitches(selectedIndex, swapWith);
			selectedIndex = swapWith;
			ringDiagram.repaint();
			populateSelectedSwitch();
		} else if (e.getSource() == btnApply) {
			// {{{
			if (selectedIndex == -1) return;
			
			Project.Switch sw = switches.get(selectedIndex);
			
			// Need to parse them to make sure the user didn't enter garbage
			try {
				sw.setAddress(Utils.makeIP(Utils.parseIP(txtSwitchIP.getText())));
			} catch (NumberFormatException nfe) {
				Utils.debug(nfe);
				JOptionPane.showMessageDialog(this, "Error: " + nfe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			
			try {
				sw.setSubnet(Utils.makeIP(Utils.parseIP(txtSwitchSubnet.getText())));
			} catch (NumberFormatException nfe) {
				Utils.debug(nfe);
				JOptionPane.showMessageDialog(this, "Error: " + nfe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			
			if (txtSwitchGateway.getText().length() > 0) {
				try {
					sw.setGateway(Utils.makeIP(Utils.parseIP(txtSwitchGateway.getText())));
				} catch (NumberFormatException nfe) {
					Utils.debug(nfe);
					JOptionPane.showMessageDialog(this, "Error: " + nfe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				sw.setGateway("");
			}
			
			if (chkRootSwitch.isSelected()) rootIndex = selectedIndex;
			ringDiagram.repaint();
		} else if (e.getSource() == btnRemoveSwitch) {
			// {{{
			if (selectedIndex == -1) return;
			
			Project.Switch removing = switches.get(selectedIndex);
			removing.getParent().removeChild(removing);
			switches.removeElementAt(selectedIndex);
			
			selectedIndex = -1;
			switchSettings.setEnabled(false);
			ringDiagram.repaint();
			populateSelectedSwitch();
		} else if (e.getSource() == btnSave) {
			// {{{
			// Put the switches into a dependency line, starting with the root switch
			if (switches.size() > 1) {
				int swIndex = rootIndex;
				int nextIndex = (swIndex + 1) % switches.size();
				do {
					switches.get(swIndex).addChild(switches.get(nextIndex));
					
					swIndex = nextIndex;
					nextIndex = (swIndex + 1) % switches.size();
				} while (nextIndex != rootIndex);
			}
			
			JFileChooser chooser = new JFileChooser();
			chooser.setFileFilter(new FileNameExtensionFilter("Firmware Project Files (*.fpj)", "fpj"));
			
			boolean saved = false;
			while (!saved) {
				boolean readyToSave = false;
				
				while (!readyToSave) {
					// If there's a project file, set the default directory to the same place
					if (owner.getConfig().getFileName() != null) chooser.setCurrentDirectory((new File(owner.getConfig().getFileName())).getParentFile());
					
					int retval = chooser.showSaveDialog(this);
					if (retval == JFileChooser.APPROVE_OPTION) {
						if (!chooser.getSelectedFile().isFile() && chooser.getSelectedFile().getName().indexOf('.') == -1) {
							chooser.setSelectedFile(new File(chooser.getSelectedFile().getAbsolutePath() + ".fpj"));
						}
						
						// If the file exists, confirm the overwrite
						if (chooser.getSelectedFile().isFile()) {
							retval = JOptionPane.showConfirmDialog(this, "A file named '" + chooser.getSelectedFile().getName() + "' exists, overwrite?", "Save", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
							
							if (retval == JOptionPane.YES_OPTION) {
								readyToSave = true;
							} else {
								readyToSave = false;
							}
						} else if (chooser.getSelectedFile().exists()) {
							JOptionPane.showMessageDialog(this, "An object named '" + chooser.getSelectedFile().getName() + "' exists and is not a file, cannot overwrite.", "Save", JOptionPane.WARNING_MESSAGE);
							readyToSave = false;
						} else {
							// Doesn't exist
							readyToSave = true;
						}
					} else if (retval == JFileChooser.CANCEL_OPTION) {
						return;
					}
				}
				
				project.setFileName(chooser.getSelectedFile().getAbsolutePath());
				try {
					project.save();
					
					saved = true;
					
					owner.openProject(chooser.getSelectedFile().getAbsolutePath());
				} catch (IOException ioe) {
					Utils.debug(ioe);
					JOptionPane.showMessageDialog(this, "Unable to save: " + ioe.getMessage(), "Save", JOptionPane.ERROR_MESSAGE);
					saved = false;
				}
			}
	    } else {
			throw new IllegalArgumentException("Unexpected source of action event: " + e.getSource());
		}
	}
	
	// addSwitch
	private void addSwitch(int[] ip, int[] subnet, int[] gateway) {
		String pwd1 = new String(txtPassword.getPassword());
		String pwd2 = new String(txtConfirm.getPassword());
		
		// Verify
		if (!pwd1.equals(pwd2)) {
			txtPassword.requestFocusInWindow();
			JOptionPane.showMessageDialog(this, "Password confirmation failed, please re-enter your password.", "Error", JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		Project.Switch sw = project.getNewSwitch();
		
		sw.setName("");
		sw.setAddress(Utils.makeIP(ip));
		sw.setSubnet(Utils.makeIP(subnet));
		
		if (gateway == null) {
			sw.setGateway("");
		} else {
			sw.setGateway(Utils.makeIP(gateway));
		}
		
		sw.setUser(txtUser.getText());
		sw.setPassword(pwd1);
		
		switches.add(sw);
	}
	
	// swapSwitches
	private void swapSwitches(int a, int b) {
		if (rootIndex == a) {
			rootIndex = b;
		} else if (rootIndex == b) {
			rootIndex = a;
		}
		
		Project.Switch temp = switches.get(a);
		switches.set(a, switches.get(b));
		switches.set(b, temp);
	}
	
	// populateSelectedSwitch
	private void populateSelectedSwitch() {
		if (selectedIndex == -1) {
			txtSwitchIP.setText("");
			txtSwitchSubnet.setText("");
			txtSwitchGateway.setText("");
			chkRootSwitch.setSelected(false);
			
			switchSettings.setEnabled(false);
		} else {
			Project.Switch sw = switches.get(selectedIndex);
			
			txtSwitchIP.setText(sw.getAddress());
			txtSwitchSubnet.setText(sw.getSubnet());
			txtSwitchGateway.setText(sw.getGateway());
			chkRootSwitch.setSelected(selectedIndex == rootIndex);
			
			switchSettings.setEnabled(true);
		}
	}
	
	// ProjectUI functions
	public char[] askForPassword(String msg) {
		PasswordDialog pwd = new PasswordDialog(msg, "Password", false, false);
		PasswordDialog.UserPass userPass = pwd.showDialog();
		
		if (userPass == null) return null;
		
		char[] ans = new char[userPass.passwd.length];
		System.arraycopy(userPass.passwd, 0, ans, 0, ans.length);
		
		Arrays.fill(userPass.passwd, '\0');
		
		return ans;
	}
	
	public void showWarning(String msg) {
		JOptionPane.showMessageDialog(this, msg, "Warning", JOptionPane.WARNING_MESSAGE);
	}
}

/*
 * ProjectUI.java
 *
 * An object that provides UI functions for Project, so it can give warnings and ask for passwords...
 *
 * Jonathan Pearson
 * December 21, 2007
 *
 */

package com.sixnetio.Firmware;

public interface ProjectUI {
	public char[] askForPassword(String msg);
	public void showWarning(String msg);
}

/*
 * Project.java
 *
 * Represents all of the settings of a project, along with load/save methods to output to a file.
 *
 * Jonathan Pearson
 * December 10, 2007
 *
 */

package com.sixnetio.Firmware;

import java.io.*;
import java.util.*;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.jonp.xml.Content;
import org.jonp.xml.Tag;

import com.sixnetio.Switch.SwitchConfig;
import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.io.DelineatedInputStream;
import com.sixnetio.util.Utils;

public class Project {
	/**
	 * Represents a switch in the project.
	 *
	 * @author Jonathan Pearson
	 */
	public class Switch {
		// What to do when the user wants to load all
		/** Always include this switch in All operations. */
		public static final int L_ALWAYS = 0;
		
		/** Skip this switch in the next All operation. */
		public static final int L_SKIP_NEXT = 1;
		
		/** Never include this switch in All operations. */
		public static final int L_NEVER = 2;
		
		private String name = "";
		private String addr = "10.2.0.1";
		private String subnet = "255.0.0.0";
		private String gateway = "";
		private String user = "";
		private String password = "";
		
		private int iShouldLoad = L_ALWAYS;
		
		// List of saved configuration states
		private Vector<SwitchConfig> savedStates;
		
		private Switch parent;
		private Vector<Switch> children;
		
		/**
		 * Construct a new Switch.
		 * 
		 * @param name The switch's name.
		 * @param parent The parent of the switch in the dependency tree.
		 */
		// Don't want anyone else calling these, we need to control all of them
		private Switch(String name, Switch parent) {
			this.name = name;
			this.savedStates = new Vector<SwitchConfig>();
			this.user = Project.this.getUser();
			this.password = Project.this.getPassword();
			this.children = new Vector<Switch>();
			this.parent = parent;
		}
		
		/**
		 * Construct a new switch from XML data.
		 * 
		 * @param from The XML data.
		 * @param formatVersion The file format version number.
		 * @param parent The parent of the switch in the dependency tree.
		 */
		private Switch(Tag from, double formatVersion, Switch parent) {
			name = from.getStringAttribute("name");
			addr = from.getStringAttribute("address");
			subnet = from.getStringAttribute("subnet");
			gateway = from.getStringAttribute("gateway");
			user = Utils.rot13(from.getStringAttribute("user"));
			password = Utils.rot13(from.getStringAttribute("password"));
			iShouldLoad = from.getIntAttribute("shouldload");
			
			savedStates = new Vector<SwitchConfig>();
			Tag[] tStates = from.getEachTag("", "savedstate");
			for (int i = 0; i < tStates.length; i++) {
				try {
					savedStates.add(new SwitchConfig(tStates[i].getLongAttribute("timestamp"), Utils.decode64(tStates[i].getStringContents())));
				} catch (IOException ioe) {
					Utils.debug(ioe);
					
					// Not really anything we can do about this... It must be corrupt, so it will just disappear
					// FUTURE: Find a way to alert the user about this
				}
			}
			
			this.parent = parent;
			
			children = new Vector<Switch>();
			Tag[] tChildren = from.getEachTag("", "switch");
			for (int i = 0; i < tChildren.length; i++) {
				Switch sw = new Switch(tChildren[i], formatVersion, this);
				children.add(sw);
			}
			
			sortStates();
		}
		
		/**
		 * Generate XML data representing this switch.
		 * 
		 * @param into The switch's XML tag will be added as a child of this one.
		 * @throws IOException If there is a problem encoding part of the switch data.
		 */
		private void saveData(Tag into) throws IOException {
			if (parent != null) {
				into.setAttribute("name", name);
				into.setAttribute("address", addr);
				into.setAttribute("subnet", subnet);
				into.setAttribute("gateway", gateway);
				into.setAttribute("user", Utils.rot13(user));
				into.setAttribute("password", Utils.rot13(password));
				into.setAttribute("shouldload", iShouldLoad);
				
				for (SwitchConfig state : savedStates) {
					Tag tState = new Tag("savedstate");
					tState.setAttribute("timestamp", state.getTimestamp());
					tState.addContent(Utils.encode64(state.getData()));
					
					
					into.addContent(tState);
				}
			}
			
			for (Switch child : children) {
				Tag tChild = new Tag("switch");
				child.saveData(tChild);
				
				into.addContent(tChild);
			}
		}
		
		/**
		 * Reorders the 'savedStates' list to be sorted by timestamp, descending.
		 */
		private void sortStates() {
			SwitchConfig[] states = savedStates.toArray(new SwitchConfig[0]);
			
			Arrays.sort(states, new Comparator<SwitchConfig>() {
				public int compare(SwitchConfig a, SwitchConfig b) {
					// We want a descending list, so reverse the signs
					long diff = a.getTimestamp() - b.getTimestamp();
					if (diff < 0) {
						return 1;
					} else if (diff > 0) {
						return -1;
					} else {
						return 0;
					}
				}
				
				@Override
                public boolean equals(Object obj) {
					return (this == obj);
				}
			});
			
			savedStates.clear();
			for (int i = 0; i < states.length; i++) {
				savedStates.add(states[i]);
			}
		}
		
		/**
		 * Get the name of the switch.
		 */
		public String getName() {
			return name;
		}
		
		/**
		 * Get the address of the switch.
		 */
		public String getAddress() {
			return addr;
		}
		
		/**
		 * Get the subnet mask of the switch.
		 */
		public String getSubnet() {
			return subnet;
		}
		
		/**
		 * Get the gateway address of the switch.
		 */
		public String getGateway() {
			return gateway;
		}
		
		/**
		 * Get the Web UI user name of the switch.
		 */
		public String getUser() {
			return user;
		}
		
		/**
		 * Get the Web UI password of the switch.
		 */
		public String getPassword() {
			return password;
		}
		
		/**
		 * Set the name of the switch.
		 * 
		 * @param val The new name for the switch.
		 */
		public void setName(String val) {
			name = val;
		}
		
		/**
		 * Set the address of the switch.
		 * 
		 * @param val The new address for the switch.
		 */
		public void setAddress(String val) {
			addr = val;
		}
		
		/**
		 * Set the subnet mask of the switch.
		 * 
		 * @param val The new subnet mask for the switch.
		 */
		public void setSubnet(String val) {
			subnet = val;
		}
		
		/**
		 * Set the gateway address of the switch.
		 * 
		 * @param val The new gateway address for the switch.
		 */
		public void setGateway(String val) {
			gateway = val;
		}
		
		/**
		 * Set the Web UI user name of the switch.
		 * 
		 * @param val The new Web UI user name for the switch.
		 */
		public void setUser(String val) {
			user = val;
		}
		
		/**
		 * Set the Web UI password of the switch.
		 * 
		 * @param val The new Web UI password for the switch.
		 */
		public void setPassword(String val) {
			password = val;
		}
		
		/**
		 * Get a value indicating whether this switch should participate in
		 * the next All operation.
		 * 
		 * @return One of the L_* values above.
		 */
		public int getShouldLoad() {
			return iShouldLoad;
		}
		
		/**
		 * Set a value indicating whether this switch should participate in
		 * the next All operation.
		 * 
		 * @param val One of the L_* values above.
		 */
		public void setShouldLoad(int val) {
			iShouldLoad = val;
		}
		
		/**
		 * Get a count of the number of saved configuration checkpoints in
		 * this switch.
		 */
		public int getSavedStateCount() {
			return savedStates.size();
		}
		
		/**
		 * Get an iterator over the saved configuration checkpoints. You may
		 * call 'remove()' on this.
		 */
		public Iterator<SwitchConfig> getSavedStates() {
			return savedStates.iterator();
		}
		
		/**
		 * Add a saved configuration checkpoint to this switch.
		 */
		public void addSavedState(SwitchConfig state) {
			savedStates.add(state);
			sortStates();
		}
		
		/**
		 * Create a new Switch as a child of this one.
		 * 
		 * @return The new Switch.
		 */
		public Switch createChild() {
			Switch sw = new Switch("New Switch " + (newSwitchCounter++), this);
			addChild(sw);
			return sw;
		}
		
		/**
		 * Get a count of the number of children of this Switch.
		 */
		public int getChildCount() {
			return children.size();
		}
		
		/**
		 * Get the child Switch at the specified index.
		 * 
		 * @param index The index.
		 * @return The child Switch at that index.
		 */
		public Switch getChild(int index) {
			return children.get(index);
		}
		
		/**
		 * Modify this Switch and the given Switch so that this is the parent
		 * of that Switch.
		 * 
		 * @param sw The Switch to make a child of this Switch.
		 */
		public void addChild(Switch sw) {
			sw.setParent(this);
			children.add(sw);
		}
		
		/**
		 * Removes the given child Switch from this Switch, orphaning it.
		 * 
		 * @param child The child Switch to orphan.
		 */
		public void removeChild(Switch child) {
			children.remove(child);
		}
		
		/**
		 * Modifies the parent of this Switch, removing it as a child from its
		 * current parent if it has one.
		 * 
		 * @param newParent The new parent Switch (may be <tt>null</tt>).
		 */
		public void setParent(Switch newParent) {
			if (parent != null) parent.removeChild(this);
			parent = newParent;
		}
		
		/**
		 * Get the parent Switch of this Switch.
		 */
		public Switch getParent() {
			return parent;
		}
	}
	
	/** Project file format version. */
	public static final double FF_VERSION = 1.5;
	
	/** Default firmware file name (not project file name). */
	public static final String D_FIRMWARE_FILE = "";
	
	/** Default global Web UI user name. */
	public static final String D_USER = "";
	
	/** Default global Web UI password. */
	public static final String D_PASSWORD = "";
	
	/** Default serial port path. */
	public static final String D_SERIAL = "";

	/** Default baud rate. */
	public static final int D_BAUD_RATE = 9600;
	
	/** Default local IP address. */
	public static final String D_IPADDR = "";
	
	/** Default setting for whether to perform parallel loads. */
	public static final boolean D_PARALLEL = true;
	
	/** Default setting for whether to load bottom-to-top. */
	public static final boolean D_BOTTOM_TO_TOP = true;
	
	/** Default setting for whether to save/restore configuration checkpoints. */
	public static final boolean D_SAVE_CONFIG = true;
	
	/** Default setting for whether to probe after loading. */
	public static final boolean D_PROBE_AFTER = true;
	
	/** Default setting for whether to force ethernet-only loading. */
	public static final boolean D_FORCE_LOAD = false;
	
	/** Default setting for whether to password-protect this file. */
	public static final boolean D_PROTECTED_FILE = false;
	
	
	// Private data members
	// General stuff
	private char[] projectPassword = null;
	private ProjectUI owner;
	private Switch rootSwitch;
	
	// Current values, set to defaults
	private String sFirmwareFile = D_FIRMWARE_FILE,
	               sUser = D_USER,
	               sPassword = D_PASSWORD,
	               sSerial = D_SERIAL,
	               sIPAddr = D_IPADDR;
	private int sBaudRate = D_BAUD_RATE;
	private boolean bParallel = D_PARALLEL,
	                bBottomToTop = D_BOTTOM_TO_TOP,
	                bSaveConfig = D_SAVE_CONFIG,
	                bProbeAfter = D_PROBE_AFTER,
	                bForceLoad = D_FORCE_LOAD;
	private FirmwareBundle theBundle;
	
	private String fileName = null;
	private boolean protectedFile = D_PROTECTED_FILE; // Whether this project is password-protected
	
	private int newSwitchCounter = 1; // So we can name new switches properly ("New Switch 1", ...)
	
	/**
	 * Construct a new Project.
	 * 
	 * @param owner The owner of this project.
	 */
	public Project(ProjectUI owner) {
		this.owner = owner;
		rootSwitch = new Switch(null, null);
	}
	
	/**
	 * Construct a new Project, loading from a file.
	 * 
	 * @param owner The owner of this project.
	 * @param projectFile The path to the file to load from.
	 * @throws IOException If there was a problem loading the file.
	 */
	public Project(ProjectUI owner, String projectFile) throws IOException {
		this.owner = owner;
		fileName = projectFile;
		
		if (fileName == null) {
			rootSwitch = new Switch(null, null);
		} else {
			loadFile();
		}
	}
	
	/**
	 * Clean up during garbage collection.
	 */
	@Override
    protected void finalize() {
		clearProjectPassword();
	}
	
	/**
	 * Get the path of the project file.
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * Set the path of the project file.
	 * 
	 * @param val The new path to the project file.
	 */
	public void setFileName(String val) {
		fileName = val;
	}
	
	/**
	 * Get the path to the firmware file.
	 */
	public String getFirmwareFile() {
		return sFirmwareFile;
	}
	
	/**
	 * Set the path to the firmware file.
	 * 
	 * @param val The new path to the firmware file.
	 */
	public void setFirmwareFile(String val) {
		sFirmwareFile = val;
	}
	
	/**
	 * Get the Web UI user name.
	 */
	public String getUser() {
		return sUser;
	}
	
	/**
	 * Set the Web UI user name.
	 * 
	 * @param val The new Web UI user name.
	 */
	public void setUser(String val) {
		sUser = val;
	}
	
	/**
	 * Get the Web UI password.
	 */
	public String getPassword() {
		return sPassword;
	}
	
	/**
	 * Set the Web UI password.
	 * 
	 * @param val The new Web UI password.
	 */
	public void setPassword(String val) {
		sPassword = val;
	}
	
	/**
	 * Get the serial port path.
	 */
	public String getSerial() {
		return sSerial;
	}

	/**
	 * Get the baud rate
	 */
	public int getBaudRate() {
		return sBaudRate;
	}
	
	/**
	 * Set the serial port path.
	 * 
	 * @param val The new path to the serial port.
	 */
	public void setSerial(String val) {
		sSerial = val;
	}

	/**
	 * Set the baud rate.
	 *
	 * @param val The new baud rate.
	 */
	public void setBaudRate(int baud) {
		sBaudRate = baud;
	}
	
	/**
	 * Get the local IP address to use.
	 */
	public String getIPAddress() {
		return sIPAddr;
	}
	
	/**
	 * Set the local IP address to use.
	 * 
	 * @param val The new local IP address.
	 */
	public void setIPAddress(String val) {
		sIPAddr = val;
	}
	
	/**
	 * Check whether parallel loads are enabled.
	 */
	public boolean getParallel() {
		return bParallel;
	}
	
	/**
	 * Set whether parallel loads are enabled.
	 * 
	 * @param val True = parallel, false = sequential.
	 */
	public void setParallel(boolean val) {
		bParallel = val;
	}
	
	/**
	 * Check whether bottom-to-top loads are enabled.
	 */
	public boolean getBottomToTop() {
		return bBottomToTop;
	}
	
	/**
	 * Set whether bottom-to-top loads are enabled.
	 * 
	 * @param val True = bottom-to-top, false = top-to-bottom.
	 */
	public void setBottomToTop(boolean val) {
		bBottomToTop = val;
	}
	
	/**
	 * Check whether configuration checkpoints are saved/restored.
	 */
	public boolean getSaveConfig() {
		return bSaveConfig;
	}
	
	/**
	 * Set whether configuration checkpoints are saved/restored.
	 * 
	 * @param val True = save/restore when loading, false = don't save/restore.
	 */
	public void setSaveConfig(boolean val) {
		bSaveConfig = val;
	}
	
	/**
	 * Check whether we should probe after loading.
	 */
	public boolean getProbeAfter() {
		return bProbeAfter;
	}
	
	/**
	 * Set whether we should probe after loading.
	 * 
	 * @param val True = probe after load, false = don't.
	 */
	public void setProbeAfter(boolean val) {
		bProbeAfter = val;
	}
	
	/**
	 * Check whether forced ethernet-only loads are enabled.
	 */
	public boolean getForceLoad() {
		return bForceLoad;
	}
	
	/**
	 * Set whether forced ethernet-only loads are enabled.
	 * 
	 * @param val True = force ethernet-only loads, false = fail if disabled.
	 */
	public void setForceLoad(boolean val) {
		bForceLoad = val;
	}
	
	/**
	 * Get the firmware bundle.
	 */
	public FirmwareBundle getBundle() {
		return theBundle;
	}
	
	/**
	 * Set the firmware bundle.
	 * 
	 * @param val The new bundle.
	 */
	public void setBundle(FirmwareBundle val) {
		theBundle = val;
	}
	
	/**
	 * Check whether this project is protected.
	 */
	public boolean getProtectedFile() {
		return protectedFile;
	}
	
	/**
	 * Set whether this project is protected.
	 * 
	 * @param val True = password protected, false = not protected.
	 */
	public void setProtectedFile(boolean val) {
		protectedFile = val;
	}
	
	/**
	 * Clear the project password (carefully).
	 */
	public void clearProjectPassword() {
		if (projectPassword != null) {
			Arrays.fill(projectPassword, '\0');
			projectPassword = null;
		}
	}
	
	/**
	 * Set the project password. You should clear the array manually immediately
	 * after calling this.
	 * 
	 * @param passwd The array containing password characters.
	 */
	public void setProjectPassword(char[] passwd) {
		clearProjectPassword();
		
		projectPassword = new char[passwd.length];
		System.arraycopy(passwd, 0, projectPassword, 0, projectPassword.length);
	}
	
	/**
	 * Construct a new root-level Switch.
	 * 
	 * @return The new Switch.
	 */
	public Switch getNewSwitch() {
		return rootSwitch.createChild();
	}
	
	/**
	 * Get the absolute top-level switch. This does not represent a real switch,
	 * only the root of a convenient organizational tree structure.
	 */
	public Switch getRootSwitch() {
		return rootSwitch;
	}
	
	// Load
	// Used by openProjectFile
	private static final int FIELD_SEPARATOR = 0xff;
	
	/**
	 * Load the project file set in 'fileName'.
	 * 
	 * @throws IOException If there is a problem loading it.
	 */
	private void loadFile() throws IOException {
		FileInputStream fileIn = new FileInputStream(fileName);
		
		try {
    		DelineatedInputStream in = new DelineatedInputStream(fileIn, FIELD_SEPARATOR, 2);
    		Tag main;
    		
    		// The XML parser is stupid (or too smart for its own good), so we need to buffer the whole thing and
    		//   then pass it through as its own stream
    		// Somehow it was reading bytes beyond the returned 'EOF' character, and failing to parse the
    		//   weird byte values
    		{
    			Vector<Byte> vec = new Vector<Byte>();
    			int b;
    			while ((b = in.read()) != -1) vec.add((byte)(b & 0xff));
    			
    			byte[] data = new byte[vec.size()];
    			int pos = 0;
    			for (byte bt : vec) data[pos++] = bt;
    			
    			ByteArrayInputStream bais = new ByteArrayInputStream(data);
    			main = Utils.xmlParser.parseXML(bais, true, false);
    		}
    		
    		if (!main.getName().equals("firmproj")) throw new IOException("Not a firmware loader project");
    		
    		double formatVersion = main.getDoubleAttribute("version");
    		
    		// Check supported versions
    		if (!(formatVersion >= 1.0 &&
    			  formatVersion <= 1.5)) {
    			
    			throw new IOException("Unsupported file format version: " + formatVersion);
    		}
    		
    		// Versions >= 1.3 support password-protection
    		if (formatVersion >= 1.3 && main.getBooleanAttribute("pwprotect")) {
    			char[] passwd = owner.askForPassword("This file is password protected. Please enter the password");
    			
    			if (passwd == null) throw new IOException("Cannot load a password protected file without the password");
    			
    			setProjectPassword(passwd);
    			Arrays.fill(passwd, '\0');
    			
    			// Read the rest of the file through a cipher stream
    			in.newField();
    			CipherInputStream cin = getCipherInputStream(in, projectPassword);
    			
    			Tag realBody;
    			try {
    				realBody = Utils.xmlParser.parseXML(cin, false, false);
    			} catch (IOException ioe) {
    				Utils.debug(ioe);
    				throw new IOException("Incorrect password");
    			}
    			
    			// Transfer the contents of the tag over to main
    			main.removeContents();
    			
    			for (Content c : realBody) {
    				main.addContent(c);
    			}
    			
    			cin.close();
    			
    			protectedFile = true;
    		} else {
    			protectedFile = false;
    		}
    		
    		// Application state (introduced in 1.4)
    		if (formatVersion >= 1.4) {
    			Tag appState = main.getChild("appstate");
    			if (appState == null) throw new IOException("Application state info missing");
    			
    			// New switch counter
    			newSwitchCounter = appState.getIntContents("newswitchcounter");
    		} else {
    			newSwitchCounter = 1;
    		}
    		
    		// Configuration info
    		{
    			Tag config = main.getChild("config");
    			if (config == null) throw new IOException("Configuration info missing");
    			
    			// Firmware
    			{
    				Tag firmware = config.getChild("firmware");
    				if (firmware == null) throw new IOException("Firmware info missing");
    				
    				sFirmwareFile = firmware.getStringContents("");
    				
    				File f = new File(sFirmwareFile);
    				if (f.isFile()) {
    					InputStream fin = new FileInputStream(f);
    					FirmwareBundle bundle;
    					try {
    						bundle = new FirmwareBundle(fin);
    					}
    					finally {
    						fin.close();
    					}
    					
    					theBundle = bundle;
    				} else {
    					theBundle = null;
    				}
    			}
    			
    			// Login
    			{
    				Tag login = config.getChild("login");
    				if (login == null) throw new IOException("Login info missing");
    				
    				sUser = Utils.rot13(login.getStringAttribute("user"));
    				sPassword = Utils.rot13(login.getStringAttribute("password"));
    			}
    			
    			// Local
    			{
    				Tag local = config.getChild("local");
    				if (local == null) throw new IOException("Local info missing");
    				
    				// Specify IP (dropped since 1.2)
    				// Ignore it now, but warn the user
    				if (formatVersion < 1.2) {
    					boolean bSpecifyIP = local.getBooleanAttribute("enabled");
    					
    					if (!bSpecifyIP) {
    						owner.showWarning("Specifying the local address is no longer optional. Please visit the 'Local' tab on the 'Config' page and confirm that the correct address is selected.");
    					} else {
    						// It is specified
    						sIPAddr = local.getStringContents();
    					}
    				} else {
    					// Easiest way to get the index
    					sIPAddr = local.getStringContents();
    				}
    				
    				sSerial = local.getStringAttribute("serial");
    			}
    			
    			// Loading
    			{
    				Tag loading = config.getChild("loading");
    				if (loading == null) throw new IOException("Loading info missing");
    				
    				// Loading all
    				{
    					Tag loadingAll = loading.getChild("loadingAll");
    					if (loadingAll == null) throw new IOException("Loading all info missing");
    					
    					bParallel = loadingAll.getBooleanAttribute("parallel");
    				}
    				
    				// Save config
    				{
    					Tag saveConfig = loading.getChild("saveConfig");
    					if (saveConfig == null) throw new IOException("Save config info missing");
    					
    					bSaveConfig = saveConfig.getBooleanAttribute("saveBeforeLoad");
    				}
    				
    				// Ordering
    				{
    					Tag ordering = loading.getChild("ordering");
    					if (ordering == null) throw new IOException("Ordering info missing");
    					
    					bBottomToTop = ordering.getBooleanAttribute("bottomToTop");
    				}
    				
    				// Probe after load (supported since v1.1)
    				if (formatVersion >= 1.1) {
    					Tag probeAfter = loading.getChild("probeAfter");
    					if (probeAfter == null) throw new IOException("Probe After info missing");
    					
    					bProbeAfter = probeAfter.getBooleanAttribute("enabled");
    				} else {
    					bProbeAfter = true; // Default value
    				}
    				
    				// Forced (introduced in 1.5)
    				if (formatVersion >= 1.5) {
    					Tag forced = loading.getChild("forceLoad");
    					if (forced == null) throw new IOException("Forced loading info missing");
    					
    					bForceLoad = forced.getBooleanAttribute("enabled");
    				} else {
    					bForceLoad = false;
    				}
    			}
    		}
    		
    		// Switches
    		{
    			Tag switches = main.getChild("switches");
    			rootSwitch = new Switch(switches, formatVersion, null);
    		}
		} finally {
			fileIn.close();
		}
	}
	
	/**
	 * Save the project file to the stored file name.
	 * 
	 * @throws IOException If there is a problem saving the file.
	 */
	public void save() throws IOException {
		Tag main = new Tag("firmproj");
		main.setAttribute("version", FF_VERSION);
		main.setAttribute("pwprotect", protectedFile);
		
		// Application state info
		{
			Tag appState = new Tag("appstate");
			main.addContent(appState);
			
			// New switch counter
			{
				Tag counter = new Tag("newswitchcounter");
				appState.addContent(counter);
				
				counter.addContent("" + newSwitchCounter);
			}
		}
		
		// Configuration info
		{
			Tag config = new Tag("config");
			main.addContent(config);
			
			// Firmware
			{
				Tag fwFile = new Tag("firmware");
				config.addContent(fwFile);
				
				fwFile.addContent(sFirmwareFile);
			}
			
			// Login
			{
				Tag login = new Tag("login");
				config.addContent(login);
				
				login.setAttribute("user", Utils.rot13(sUser));
				login.setAttribute("password", Utils.rot13(sPassword));
			}
			
			// Local
			{
				Tag local = new Tag("local");
				config.addContent(local);
				
				local.addContent(sIPAddr);
				
				local.setAttribute("serial", sSerial);
			}
			
			// Loading
			{
				Tag loading = new Tag("loading");
				config.addContent(loading);
				
				// Loading all
				{
					Tag loadingAll = new Tag("loadingAll");
					loading.addContent(loadingAll);
					
					loadingAll.setAttribute("parallel", bParallel);
				}
				
				// Save config
				{
					Tag saveConfig = new Tag("saveConfig");
					loading.addContent(saveConfig);
					
					saveConfig.setAttribute("saveBeforeLoad", bSaveConfig);
				}
				
				// Ordering
				{
					Tag ordering = new Tag("ordering");
					loading.addContent(ordering);
					
					ordering.setAttribute("bottomToTop", bBottomToTop);
				}
				
				// Probe after load
				{
					Tag probeAfter = new Tag("probeAfter");
					loading.addContent(probeAfter);
					
					probeAfter.setAttribute("enabled", bProbeAfter);
				}
				
				// Forced loading
				{
					Tag forced = new Tag("forceLoad");
					loading.addContent(forced);
					
					forced.setAttribute("enabled", bForceLoad);
				}
			}
		}
		
		// Switches
		{
			Tag switches = new Tag("switches");
			main.addContent(switches);
			
			rootSwitch.saveData(switches);
		}
		
		if (protectedFile) {
			// Get a password from the user if we don't already have one
			if (projectPassword == null) {
				projectPassword = owner.askForPassword("Please enter a password to protect the project");
				
				if (projectPassword == null) throw new IOException("Cannot save a protected file without a password");
			}
			
			// Now we need to move all of main's data one level deeper and do some extra processing
			Tag protect = new Tag("protected");
			for (Content c : main) {
				protect.addContent(c);
			}
			
			main.removeContents();
			
			// Encrypt it
			OutputStream out = new FileOutputStream(fileName);
			
			try {
    			Utils.xmlParser.writeXMLStream(main, out, false);
    			out.write(FIELD_SEPARATOR); // This tells the DelineatedInputStream where the rest is
    			out.flush();
    			
    			CipherOutputStream cout = getCipherOutputStream(out, projectPassword);
    			Utils.xmlParser.writeXMLStream(protect, cout, false);
    			cout.flush();
			} finally {
				out.close();
			}
		} else {
			OutputStream out = new FileOutputStream(fileName);
			
			try {
				Utils.xmlParser.writeXMLStream(main, out, false);
			} finally {
				out.close();
			}
		}
	}
	
	/**
	 * Create a cipher input stream that will use the given input stream for input.
	 * 
	 * @param wrap The input stream to read from.
	 * @param password The password set on that stream.
	 * @return An input stream that will decrypt the input from the lower-level stream.
	 */
	private CipherInputStream getCipherInputStream(InputStream wrap, char[] password) {
		Cipher pbeCipher = getCipher(password, Cipher.DECRYPT_MODE);
		
		return new CipherInputStream(wrap, pbeCipher);
	}
	
	/**
	 * Create a cipher output stream that will use the given output stream for output.
	 * 
	 * @param wrap The output stream to write to.
	 * @param password The password to set for the stream.
	 * @return An output stream that will encrypt data going towards the lower-level stream.
	 */
	private CipherOutputStream getCipherOutputStream(OutputStream wrap, char[] password) {
		Cipher pbeCipher = getCipher(password, Cipher.ENCRYPT_MODE);
		
		return new CipherOutputStream(wrap, pbeCipher);
	}
	
	/**
	 * Construct a new Cipher for the given password and mode (Cipher.ENCRYPT_MODE or
	 * Cipher.DECRYPT_MODE).
	 * 
	 * @param password The password to use.
	 * @param cipherMode The cipher mode.
	 * @return A cipher that will encrypt/decrypt appropriately.
	 */
	private Cipher getCipher(char[] password, int cipherMode) {
		PBEKeySpec pbeKeySpec;
		PBEParameterSpec pbeParamSpec;
		SecretKeyFactory keyFac;
		
		// DO NOT CHANGE THESE, OLD FILES WILL NOT LOAD IF YOU DO!
		// This is just some random data (doesn't translate to ASCII, if you're curious)
		byte[] salt = {
			(byte)0xa4, (byte)0x73, (byte)0xb6, (byte)0xaa,
			(byte)0x7e, (byte)0xe2, (byte)0x24, (byte)0x99
		};
		int count = 20; // Number of saltings
		String EncryptionMethod = "PBEWithMD5AndDES";
		
		try {
			pbeParamSpec = new PBEParameterSpec(salt, count);
			pbeKeySpec = new PBEKeySpec(password);
			keyFac = SecretKeyFactory.getInstance(EncryptionMethod);
			SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);
			
			Cipher pbeCipher = Cipher.getInstance(EncryptionMethod);
			pbeCipher.init(cipherMode, pbeKey, pbeParamSpec);
			
			return pbeCipher;
		} catch (Exception e) {
			Utils.debug(e);
			throw new RuntimeException(e);
		}
	}
}

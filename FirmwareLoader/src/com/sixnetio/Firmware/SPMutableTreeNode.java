/*
 * SPMutableTreeNode.java
 *
 * An extension of DefaultMutableTreeNode specifically for SwitchPage objects
 *
 * Jonathan Pearson
 * June 27, 2007
 *
 */

package com.sixnetio.Firmware;

import javax.swing.tree.*;

public class SPMutableTreeNode extends DefaultMutableTreeNode {
	public SPMutableTreeNode(SwitchPage page) {
		super(page);
	}
	
	public boolean isLeaf() {
		return (getChildCount() == 0);
	}
	
	public boolean getAllowsChildren() {
		return true;
	}
	
	public void add(DefaultMutableTreeNode child) {
		super.add(child);
		
		SwitchPage childPage = (SwitchPage)child.getUserObject();
		SwitchPage newParent = (SwitchPage)getUserObject();
		
		newParent.addChild(childPage);
	}
	
	public void remove(DefaultMutableTreeNode child) {
		super.remove(child);
		
		SwitchPage childPage = (SwitchPage)child.getUserObject();
		SwitchPage parentPage = (SwitchPage)getUserObject();
		
		if (parentPage != null) parentPage.removeChild(childPage);
	}
	
	public String toString() {
		return "" + getUserObject();
	}
}
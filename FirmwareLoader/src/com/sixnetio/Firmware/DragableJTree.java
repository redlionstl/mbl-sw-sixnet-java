/*
 * DragableJTree.java
 *
 * Extends JTree to add drag&drop functionality. Dropping a node on another node
 * makes it a child.
 * Note: This is built specifically for working with SwitchPage objects
 *   (through SPMutableTreeNode), otherwise it would be in GUI.
 *
 * Jonathan Pearson
 * June 27, 2007
 *
 */

package com.sixnetio.Firmware;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

public class DragableJTree extends JTree implements DragGestureListener, DragSourceListener, DropTargetListener, TreeSelectionListener {
	private Frame parent = null;
	
	// Stores the selected node info
	protected TreePath selectedTreePath = null;
	protected SPMutableTreeNode selectedNode = null;
	
	// Variables needed for DnD
	private DragSource dragSource = null;
	private DragSourceContext dragSourceContext = null;
	
	public DragableJTree(TreeModel newModel, Frame parent) {
		super(newModel);
		
		this.parent = parent;
		
		addTreeSelectionListener(this);
		
		dragSource = DragSource.getDefaultDragSource();
		DragGestureRecognizer dgr = dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_MOVE, this);
		
		dgr.setSourceActions(dgr.getSourceActions() & ~InputEvent.BUTTON3_MASK); // Disable right-click
		
		new DropTarget(this, this);
	}
	
	public SPMutableTreeNode getSelectedNode() {
		return selectedNode;
	}
	
	public void dragGestureRecognized(DragGestureEvent e) {
		if (selectedNode != null) {
			SwitchPage dragPage = (SwitchPage)selectedNode.getUserObject();
			
			if (dragPage.isDisplayable()) {
				
				Transferable transferable = (Transferable)selectedNode.getUserObject();
				
				Cursor cursor = DragSource.DefaultMoveNoDrop;
				
				dragSource.startDrag(e, cursor, transferable, this);
			}
		}
	}
	
	public void dragDropEnd(DragSourceDropEvent dsde) { }
	
	public void dragEnter(DragSourceDragEvent dsde) { }
	public void dragExit(DragSourceEvent dse) { }
	
	public void dragOver(DragSourceDragEvent dsde) {
		dragSourceContext = dsde.getDragSourceContext();
	}
	
	public void dropActionChanged(DragSourceDragEvent dsde) { }
	
	public void dragEnter(DropTargetDragEvent dtde) { }
	public void dragExit(DropTargetEvent dte) { }
	public void dragOver(DropTargetDragEvent dtrde) {
		// Get cursor location
		Point cursorLocationBis = dtrde.getLocation();
		TreePath destinationPath = getPathForLocation(cursorLocationBis.x, cursorLocationBis.y);
		
		String msg = testDropTarget(destinationPath, selectedTreePath);
		
		// If destination is okay, accept drop
		if (msg == null) {
			dtrde.acceptDrag(DnDConstants.ACTION_MOVE);
			if (dragSourceContext != null) dragSourceContext.setCursor(DragSource.DefaultMoveDrop);
		} else {
			// otherwise reject
			dtrde.rejectDrag();
			if (dragSourceContext != null) dragSourceContext.setCursor(DragSource.DefaultMoveNoDrop);
		}
	}
	
	public void drop(DropTargetDropEvent dtde) {
		Transferable tr = dtde.getTransferable();
		
		// Flavor not supported, reject drop
		if (!tr.isDataFlavorSupported(SwitchPage.FLAVOR)) {
			dtde.rejectDrop();
			return;
		}
		
		SwitchPage page = (SwitchPage)selectedNode.getUserObject();
		if (!page.confirmMoveAway()) {
			dtde.rejectDrop();
			return;
		}
		
		Point loc = dtde.getLocation();
		TreePath destinationPath = getPathForLocation(loc.x, loc.y);
		
		final String msg = testDropTarget(destinationPath, selectedTreePath);
		if (msg != null) {
			dtde.rejectDrop();
			
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					JOptionPane.showMessageDialog(parent, msg, "Error", JOptionPane.ERROR_MESSAGE);
				}
			});
			return;
		}
		
		SPMutableTreeNode newParent = (SPMutableTreeNode)destinationPath.getLastPathComponent();
		SPMutableTreeNode oldParent = (SPMutableTreeNode)selectedNode.getParent();
		
		try {
			newParent.add(selectedNode);
			
			dtde.acceptDrop(DnDConstants.ACTION_MOVE);
		} catch (IllegalStateException ise) {
			dtde.rejectDrop();
			return;
		}
		
		// Expand nodes appropriately
		DefaultTreeModel model = (DefaultTreeModel)getModel();
		model.reload(oldParent);
		model.reload(newParent);
		model.reload(selectedNode);
		
		final SPMutableTreeNode fNewPar = newParent;
		final SPMutableTreeNode fOldPar = oldParent;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				TreePath np = new TreePath(fNewPar.getPath()); // Gets used a few times
				
				expandPath(np);
				expandPath(new TreePath(fOldPar.getPath()));
				
				// For some reason, the child path doesn't get set even by this point, so select its new parent
				setSelectionPath(np);
				//fireValueChanged(new TreeSelectionEvent(this, np, true, np, np));
				
				for (int i = 0; i < fNewPar.getChildCount(); i++) {
					expandPath(new TreePath(((SPMutableTreeNode)fNewPar.getChildAt(i)).getPath()));
				}
			}
		});
		
		dtde.getDropTargetContext().dropComplete(true);
	}
	
	public void dropActionChanged(DropTargetDragEvent dtde) { }
	
	public void valueChanged(TreeSelectionEvent e) {
		selectedTreePath = e.getNewLeadSelectionPath();
		if (selectedTreePath == null) {
			selectedNode = null;
		} else {
			if (selectedTreePath.getLastPathComponent() instanceof SPMutableTreeNode) {
				selectedNode = (SPMutableTreeNode)selectedTreePath.getLastPathComponent();
			} else {
				selectedNode = null;
				selectedTreePath = null;
			}
		}
	}
	
	private String testDropTarget(TreePath destination, TreePath dropper) {
		if (destination == null) {
			return "Invalid drop location";
		}
		
		if (!(destination.getLastPathComponent() instanceof SPMutableTreeNode)) {
			return "Can only drop on a switch";
		}
		
		SPMutableTreeNode node = (SPMutableTreeNode)destination.getLastPathComponent();
		if (!node.getAllowsChildren()) {
			return "This node does not allow children";
		}
		
		if (destination.equals(dropper)) {
			return "Destination cannot be same as source";
		}
		
		if (dropper.isDescendant(destination)) {
			return "Destination node cannot be a descendant";
		}
		
		if (dropper.getParentPath().equals(destination)) {
			return "Destination node cannot be a parent";
		}
		
		return null;
	}
}

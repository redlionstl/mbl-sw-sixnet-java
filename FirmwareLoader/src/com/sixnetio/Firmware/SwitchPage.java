/*
 * SwitchPage.java
 *
 * Represents the configuration page for a single switch, along with methods for
 * acting on that switch.
 *
 * Jonathan Pearson
 * June 28, 2007
 *
 */

package com.sixnetio.Firmware;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.sixnetio.GUI.ActionWindow;
import com.sixnetio.GUI.StatusUI;
import com.sixnetio.Switch.*;
import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.fs.vfs.VFile;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;
import com.sixnetio.util.DependencyGraph.DependencyGraph;

public class SwitchPage extends JPanel implements ActionListener, ChangeListener, FirmwareLoaderUI, Transferable {
	// Constants
	public static final DataFlavor FLAVOR = new DataFlavor(SwitchPage.class, "Switch Page");
	public static final DataFlavor[] FLAVORS = {FLAVOR};
	
	private static final String TABNAME_GENERAL = "General",
	                            TABNAME_SAVEDSTATES = "Saved States";
	
	// What we are doing
	private static final int A_NOTHING = 0,
	                         A_PROBING = 1,
	                         A_LOADING = 2,
	                         A_SERIAL = 3;
	
	// Private data members
	private int currentAction = A_NOTHING;
	
	private JTabbedPane tabMain;
	
	// General info
	private JTextField txtName,
	                   txtAddr,
	                   txtSubnet,
	                   txtGateway,
	                   txtUser;
	                   
	private JPasswordField txtPassword,
	                       txtConfirm;
	
	private JButton btnApply,
	                btnRevert,
	                btnUseDefault,
	                btnProbe,
	                btnLoadNow,
	                btnLoadSerial;
	
	private JLabel lblProduct,
	               lblFWCurrent,
	               lblVendor,
	               lblArch,
	               lblEthOnly;
	
	private ButtonGroup grpShouldLoad;
	private JRadioButton rdoAlwaysLoad,
	                     rdoSkipNext,
	                     rdoNeverLoad;
	
	// Saved switch configuration states
	private JComboBox cmbSavedStates;
	private JButton btnSaveState,
	                btnRestoreState,
	                btnDeleteState,
	                bnnSaveStateFile;
	
	private FirmwareLoader loader = null;
	private SerialLoader sLoader = null;
	private FirmwareLoaderGUI2 owner;
	private Project.Switch config;
	
	private SwitchPage parent;
	private HashSet<SwitchPage> children;
	
	private boolean displayable; // Set to true if we have controls, false otherwise ("Switches" node in the tree)
	private String name; // Only set when 'displayable' is false
	private StatusUI ui = null;
	private int lastState = -1; // The last state that the loader told us about
	private int selectedTab; // Index of the last selected tab in tabMain
	
	// Constructors
	public SwitchPage(FirmwareLoaderGUI2 owner, String name) {
		this.name = name;
		this.displayable = false;
		this.children = new HashSet<SwitchPage>();
		this.config = owner.getConfig().getRootSwitch();
	}
	
	public SwitchPage(FirmwareLoaderGUI2 owner, Project.Switch config) {
		this.owner = owner;
		this.config = config;
		this.displayable = true;
		this.children = new HashSet<SwitchPage>();
		
		FocusListener fl = new FocusAdapter() {
			@Override
            public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextField) {
					((JTextField)(e.getSource())).selectAll();
				}
			}
		};
		
		setLayout(new GridLayout(1, 1));
		
		tabMain = new JTabbedPane();
		tabMain.addChangeListener(this);
		add(tabMain);
		
		// General panel
		{
			JPanel general = new JPanel();
			general.setLayout(new BorderLayout());
			
			tabMain.addTab(TABNAME_GENERAL, null, general, "Connection information and immediate loading commands");
			
			{
				JPanel center = new JPanel();
				general.add(center, BorderLayout.CENTER);
				
				center.setLayout(new GridLayout(2, 2));
				
				// Top-left: Connection info panel
				{
					JPanel pnlConnectionInfo = new JPanel();
					pnlConnectionInfo.setBorder(BorderFactory.createTitledBorder("Connection Information"));
					center.add(pnlConnectionInfo);
					
					GridBagLayout gridbag = new GridBagLayout();
					pnlConnectionInfo.setLayout(gridbag);
					
					GridBagConstraints gb = new GridBagConstraints();
					
					JLabel lblName = new JLabel("Name (Optional): ");
					gridbag.setConstraints(lblName, gb);
					pnlConnectionInfo.add(lblName);
					
					txtName = new JTextField(15);
					txtName.setPreferredSize(new Dimension(txtName.getFontMetrics(txtName.getFont()).getMaxAdvance() * 15,
														   txtName.getFontMetrics(txtName.getFont()).getHeight() + 4));
					txtName.setText(config.getName());
					txtName.addFocusListener(fl);
					txtName.addActionListener(this);
					gb.gridwidth = GridBagConstraints.REMAINDER;
					gridbag.setConstraints(txtName, gb);
					pnlConnectionInfo.add(txtName);
					
					JLabel lblAddress = new JLabel("Address: ");
					gb.gridwidth = 1;
					gridbag.setConstraints(lblAddress, gb);
					pnlConnectionInfo.add(lblAddress);
					
					txtAddr = new JTextField(15);
					txtAddr.setPreferredSize(new Dimension(txtName.getFontMetrics(txtName.getFont()).getMaxAdvance() * 15,
														   txtName.getFontMetrics(txtName.getFont()).getHeight() + 4));
					txtAddr.setText(config.getAddress());
					txtAddr.addFocusListener(fl);
					txtAddr.addActionListener(this);
					gb.gridwidth = GridBagConstraints.REMAINDER;
					gridbag.setConstraints(txtAddr, gb);
					pnlConnectionInfo.add(txtAddr);
					
					JLabel lblSubnet = new JLabel("Subnet: ");
					gb.gridwidth = 1;
					gridbag.setConstraints(lblSubnet, gb);
					pnlConnectionInfo.add(lblSubnet);
					
					txtSubnet = new JTextField(15);
					txtSubnet.setPreferredSize(new Dimension(txtName.getFontMetrics(txtName.getFont()).getMaxAdvance() * 15,
															 txtName.getFontMetrics(txtName.getFont()).getHeight() + 4));
					txtSubnet.setText(config.getSubnet());
					txtSubnet.addFocusListener(fl);
					txtSubnet.addActionListener(this);
					gb.gridwidth = GridBagConstraints.REMAINDER;
					gridbag.setConstraints(txtSubnet, gb);
					pnlConnectionInfo.add(txtSubnet);
					
					JLabel lblGateway = new JLabel("Gateway IP: ");
					gb.gridwidth = 1;
					gridbag.setConstraints(lblGateway, gb);
					pnlConnectionInfo.add(lblGateway);
					
					txtGateway = new JTextField(15);
					txtGateway.setPreferredSize(new Dimension(txtName.getFontMetrics(txtName.getFont()).getMaxAdvance() * 15,
															  txtName.getFontMetrics(txtName.getFont()).getHeight() + 4));
					txtGateway.setText(config.getGateway());
					txtGateway.addFocusListener(fl);
					txtGateway.addActionListener(this);
					gb.gridwidth = GridBagConstraints.REMAINDER;
					gridbag.setConstraints(txtGateway, gb);
					pnlConnectionInfo.add(txtGateway, gb);
				}
				
				// Top-right: Switch Operations panel (formerly "Probe")
				{
					JPanel pnlProbe = new JPanel();
					pnlProbe.setBorder(BorderFactory.createTitledBorder("Switch Operations"));
					center.add(pnlProbe);
					
					pnlProbe.setLayout(new BorderLayout());
					
					// Top half
					{
						JPanel upper = new JPanel();
						pnlProbe.add(upper, BorderLayout.CENTER);
						
						upper.setLayout(new GridLayout(5, 1));
						
						// Note: All of these have leading spaces to push them away from the edge of the panel
						lblProduct = new JLabel("  Product ID: ???");
						lblFWCurrent = new JLabel("  Current FW: ???");
						lblVendor = new JLabel("  Vendor: ???");
						lblArch = new JLabel("  Architecture: ???");
						lblEthOnly = new JLabel("  Network Loading: ???");
						
						upper.add(lblProduct);
						upper.add(lblFWCurrent);
						upper.add(lblVendor);
						upper.add(lblArch);
						upper.add(lblEthOnly);
					}
					
					// Bottom half
					{
						JPanel south = new JPanel();
						pnlProbe.add(south, BorderLayout.SOUTH);
						
						south.setLayout(new FlowLayout());
						
						btnProbe = new JButton("Probe");
						btnProbe.addActionListener(this);
						south.add(btnProbe);
						
						btnLoadNow = new JButton("Load Now");
						btnLoadNow.addActionListener(this);
						south.add(btnLoadNow);
						
						btnLoadSerial = new JButton("Serial Load");
						btnLoadSerial.addActionListener(this);
						south.add(btnLoadSerial);
					}
				}
				
				// Bottom-left: Login info panel
				{
					JPanel pnlLoginInfo = new JPanel();
					pnlLoginInfo.setBorder(BorderFactory.createTitledBorder("Login Information"));
					center.add(pnlLoginInfo);
					
					GridBagLayout gridbag = new GridBagLayout();
					pnlLoginInfo.setLayout(gridbag);
					
					GridBagConstraints gb = new GridBagConstraints();
					
					JLabel lblUser = new JLabel("User Name: ");
					gridbag.setConstraints(lblUser, gb);
					pnlLoginInfo.add(lblUser);
					
					txtUser = new JTextField(15);
					txtUser.setPreferredSize(new Dimension(txtName.getFontMetrics(txtName.getFont()).getMaxAdvance() * 15,
														   txtName.getFontMetrics(txtName.getFont()).getHeight() + 4));
					txtUser.setText(config.getUser());
					txtUser.addFocusListener(fl);
					txtUser.addActionListener(this);
					gb.gridwidth = GridBagConstraints.REMAINDER;
					gridbag.setConstraints(txtUser, gb);
					pnlLoginInfo.add(txtUser);
					
					JLabel lblPassword = new JLabel("Password: ");
					gb.gridwidth = 1;
					gridbag.setConstraints(lblPassword, gb);
					pnlLoginInfo.add(lblPassword);
					
					txtPassword = new JPasswordField(15);
					txtPassword.setPreferredSize(new Dimension(txtName.getFontMetrics(txtName.getFont()).getMaxAdvance() * 15,
															   txtName.getFontMetrics(txtName.getFont()).getHeight() + 4));
					txtPassword.setText(config.getPassword());
					txtPassword.addFocusListener(fl);
					txtPassword.addActionListener(this);
					gb.gridwidth = GridBagConstraints.REMAINDER;
					gridbag.setConstraints(txtPassword, gb);
					pnlLoginInfo.add(txtPassword);
					
					JLabel lblConfirm = new JLabel("Confirm: ");
					gb.gridwidth = 1;
					gridbag.setConstraints(lblConfirm, gb);
					pnlLoginInfo.add(lblConfirm);
					
					txtConfirm = new JPasswordField(15);
					txtConfirm.setPreferredSize(new Dimension(txtName.getFontMetrics(txtName.getFont()).getMaxAdvance() * 15,
															  txtName.getFontMetrics(txtName.getFont()).getHeight() + 4));
					txtConfirm.setText(config.getPassword());
					txtConfirm.addFocusListener(fl);
					txtConfirm.addActionListener(this);
					gb.gridwidth = GridBagConstraints.REMAINDER;
					gridbag.setConstraints(txtConfirm, gb);
					pnlLoginInfo.add(txtConfirm);
					
					btnUseDefault = new JButton("Use Default");
					btnUseDefault.addActionListener(this);
					gridbag.setConstraints(btnUseDefault, gb);
					pnlLoginInfo.add(btnUseDefault);
				}
				
				// Bottom-right: When to load
				{
					JPanel pnlShouldLoad = new JPanel();
					pnlShouldLoad.setBorder(BorderFactory.createTitledBorder("Load All Action"));
					center.add(pnlShouldLoad);
					
					pnlShouldLoad.setLayout(new GridLayout(3, 1));
					
					grpShouldLoad = new ButtonGroup();
					
					grpShouldLoad.add(rdoAlwaysLoad = new JRadioButton("Always Load", config.getShouldLoad() == Project.Switch.L_ALWAYS));
					grpShouldLoad.add(rdoSkipNext = new JRadioButton("Skip Next", config.getShouldLoad() == Project.Switch.L_SKIP_NEXT));
					grpShouldLoad.add(rdoNeverLoad = new JRadioButton("Always Skip", config.getShouldLoad() == Project.Switch.L_NEVER));
					
					pnlShouldLoad.add(rdoAlwaysLoad);
					pnlShouldLoad.add(rdoSkipNext);
					pnlShouldLoad.add(rdoNeverLoad);
				}
			}
			
			// Underneath everything: Apply/revert buttons
			// Right side
			{
				JPanel p = new JPanel();
				general.add(p, BorderLayout.SOUTH);
				
				p.setLayout(new FlowLayout());
				
				btnApply = new JButton("Apply");
				btnApply.addActionListener(this);
				p.add(btnApply);
				
				btnRevert = new JButton("Revert");
				btnRevert.addActionListener(this);
				p.add(btnRevert);
			}
		}
		// Saved States panel
		{
			JPanel states = new JPanel();
			
			GridBagLayout gridbag = new GridBagLayout();
			GridBagConstraints gb = new GridBagConstraints();
			
			states.setLayout(gridbag);
			
			tabMain.addTab(TABNAME_SAVEDSTATES, null, states, "Saved switch configurations");
			
			JLabel lblStates = new JLabel("Saved States: ");
			gb.gridwidth = 1;
			gridbag.setConstraints(lblStates, gb);
			states.add(lblStates);
			
			cmbSavedStates = new JComboBox();
			cmbSavedStates.setPreferredSize(new Dimension(200, 20));
			gb.gridwidth = GridBagConstraints.REMAINDER;
			gridbag.setConstraints(cmbSavedStates, gb);
			states.add(cmbSavedStates);
			
			// Save and Restore buttons
			{
				JPanel p = new JPanel();
				p.setLayout(new FlowLayout());
				
				gb.gridwidth = GridBagConstraints.REMAINDER;
				gridbag.setConstraints(p, gb);
				states.add(p);
				
				
				btnSaveState = new JButton("Save Current State");
				btnSaveState.addActionListener(this);
				p.add(btnSaveState);
				
				btnRestoreState = new JButton("Restore State");
				btnRestoreState.addActionListener(this);
				p.add(btnRestoreState);
			}
			
			// Delete and Save to Disk buttons
			{
				JPanel p = new JPanel();
				p.setLayout(new FlowLayout());
				
				gb.gridwidth = GridBagConstraints.REMAINDER;
				gridbag.setConstraints(p, gb);
				states.add(p);
				
				btnDeleteState = new JButton("Delete State");
				btnDeleteState.addActionListener(this);
				p.add(btnDeleteState);
				
				bnnSaveStateFile = new JButton("Save State to File");
				bnnSaveStateFile.addActionListener(this);
				p.add(bnnSaveStateFile);
			}
			
			refreshStates();
		}
		
		initializeLoader();
    }
	
	// Give Focus
	public void giveFocus() {
		txtAddr.requestFocusInWindow();
	}
	
	// Config change functions
	public boolean confirmMoveAway() {
		if (detectConfigChange()) {
			int result = JOptionPane.showConfirmDialog(owner, "Settings have changed, keep the changes?", "Keep Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
			
			if (result == JOptionPane.YES_OPTION) {
				btnApply.doClick();
				
				return !detectConfigChange(); // If there was a problem, don't allow a move away from the page
			} else if (result == JOptionPane.NO_OPTION) {
				btnRevert.doClick();
				return true;
			} else { // Must be cancel
				return false;
			}
		} else {
			return true;
		}
	}
	
	private boolean detectConfigChange() {
		// For debugging
		if (Utils.DEBUG) {
			if (!config.getName().equals(txtName.getText())) {
				Utils.debug("Name");
			}
			
			if (!config.getAddress().equals(txtAddr.getText())) {
				Utils.debug("Address");
			}
			
			if (!config.getSubnet().equals(txtSubnet.getText())) {
				Utils.debug("Subnet");
			}
			
			if (!config.getGateway().equals(txtGateway.getText())) {
				Utils.debug("Gateway");
			}
			
			if (!config.getUser().equals(txtUser.getText())) {
				Utils.debug("User");
			}
			
			if (!config.getPassword().equals(new String(txtPassword.getPassword()))) {
				Utils.debug("Password");
			}
			
			if (!config.getPassword().equals(new String(txtConfirm.getPassword()))) {
				Utils.debug("Confirm");
			}
		}
		
		return (!config.getName().equals(txtName.getText()) ||
		        !config.getAddress().equals(txtAddr.getText()) ||
		        !config.getSubnet().equals(txtSubnet.getText()) ||
		        !config.getGateway().equals(txtGateway.getText()) ||
		        !config.getUser().equals(txtUser.getText()) ||
		        !config.getPassword().equals(new String(txtPassword.getPassword())) ||
		        !config.getPassword().equals(new String(txtConfirm.getPassword())));
	}
	
	private void saveOptions() {
		config.setName(txtName.getText());
		config.setAddress(txtAddr.getText());
		config.setSubnet(txtSubnet.getText());
		config.setGateway(txtGateway.getText());
		
		config.setUser(txtUser.getText());
		config.setPassword(new String(txtPassword.getPassword()));
		
		if (rdoAlwaysLoad.isSelected()) {
			config.setShouldLoad(Project.Switch.L_ALWAYS);
		} else if (rdoSkipNext.isSelected()) {
			config.setShouldLoad(Project.Switch.L_SKIP_NEXT);
		} else {
			config.setShouldLoad(Project.Switch.L_NEVER);
		}
		
		owner.changed = true;
		owner.resetTitle();
		
		owner.repaint();
		
		initializeLoader();
	}
	
	private void revertOptions() {
		txtName.setText(config.getName());
		txtAddr.setText(config.getAddress());
		txtSubnet.setText(config.getSubnet());
		txtGateway.setText(config.getGateway());
		
		txtUser.setText(config.getUser());
		txtPassword.setText(config.getPassword());
		txtConfirm.setText(config.getPassword());
		
		rdoAlwaysLoad.setSelected(false);
		rdoSkipNext.setSelected(false);
		rdoNeverLoad.setSelected(false);
		switch (config.getShouldLoad()) {
			case Project.Switch.L_ALWAYS:
				rdoAlwaysLoad.setSelected(true);
				break;
			case Project.Switch.L_SKIP_NEXT:
				rdoSkipNext.setSelected(true);
				break;
			case Project.Switch.L_NEVER:
				rdoNeverLoad.setSelected(true);
				break;
			default:
				throw new RuntimeException("Unrecognized value for ShouldLoad: " + config.getShouldLoad());
		}
		
		initializeLoader();
	}
	
	public void initializeLoader() {
		if (config.getAddress() != null && config.getAddress().length() > 0 &&
		    config.getSubnet() != null && config.getSubnet().length() > 0 &&
		    config.getUser() != null && config.getUser().length() > 0 &&
		    config.getPassword() != null && config.getPassword().length() > 0) {
			
			loader = new FirmwareLoader(this, config.getAddress(), config.getSubnet(), config.getGateway(), config.getUser(), config.getPassword(), FirmwareLoaderGUI2.getIgnoreVendor());
		} else {
			loader = null;
		}
	}
	
	// Should load
	public boolean shouldLoad() {
		switch (config.getShouldLoad()) {
			case Project.Switch.L_ALWAYS:
				return true;
			case Project.Switch.L_SKIP_NEXT:
				config.setShouldLoad(Project.Switch.L_ALWAYS);
				rdoAlwaysLoad.setSelected(true);
				rdoSkipNext.setSelected(false);
				rdoNeverLoad.setSelected(false);
				
				// Fall through
			case Project.Switch.L_NEVER:
				return false;
			default:
				throw new RuntimeException("Unrecognized value for ShouldLoad: " + config.getShouldLoad());
		}
	}
	
	public boolean peekShouldLoad() {
		// Same as shouldLoad, but doesn't change the value if it is set to 'SkipNext'
		switch (config.getShouldLoad()) {
			case Project.Switch.L_ALWAYS:
				return true;
			case Project.Switch.L_SKIP_NEXT:
				// Fall through
			case Project.Switch.L_NEVER:
				return false;
			default:
				throw new RuntimeException("Unrecognized value for ShouldLoad: " + config.getShouldLoad());
		}
	}
	
	// Accessors
	public Project.Switch getConfig() {
		return config;
	}
	
	@Override
    public boolean isDisplayable() {
		return displayable;
	}
	
	public FirmwareLoader getFirmwareLoader() {
		return loader;
	}
	
	// FirmwareLoaderUI Functions
	public void handleException(Exception e, String extraMessage, boolean mt) {
		Utils.debug(e);
		if (ui != null) {
			if (e instanceof InterruptedOperationException) {
				ui.updateState("Operation Canceled");
			} else {
				ui.updateState("Error: " + e.getMessage());
			}
			
			ui.updateMessage(extraMessage);
			ui.updateColor(FirmwareLoaderGUI2.C_ERROR);
		}
	}
	
	public void saveSwitchState(SwitchConfig state) {
		config.addSavedState(state);
		
		refreshStates();
		
		owner.changed = true;
		owner.resetTitle();
	}
	
	public void updateState(int state) {
		lastState = state;
		
		if (ui != null) {
			ui.updateColor(FirmwareLoaderGUI2.C_INPROGRESS); // Normal color during load
			
			switch(state) {
				case S_VERIFY_ETH_ONLY:
					ui.updateState("Verify");
					break;
				case S_SET_FORCE:
					ui.updateState("Force");
					break;
				case S_PROBE_SWITCH:
					ui.updateState("Probe");
					break;
				case S_EXTRACT_FILES:
					ui.updateState("Extract");
					break;
				case S_DOWNLOAD_CONFIG:
					ui.updateState("Download Config");
					break;
				case S_UPGRADE_REQUEST:
					ui.updateState("Request");
					break;
				case S_RESTORE_CONFIG:
					ui.updateState("Restore");
					break;
				case S_COMPLETE:
					ui.updateState("Complete");
					ui.updateColor(FirmwareLoaderGUI2.C_COMPLETE);
					break;
				case S_APPLY_POWER:
					ui.updateState("Please apply power to the switch");
					break;
				case S_ARCH:
					ui.updateState("Probe");
					break;
				case S_SETUP:
					ui.updateState("Environment Setup");
					break;
				case S_LOAD_IMAGE:
					ui.updateState("Upload Filesystem Image");
					break;
				case S_FLASH_IMAGE:
					ui.updateState("Writing Filesystem Image to NAND");
					break;
				case S_LOAD_BOOT_IMAGE:
					ui.updateState("Upload Boot Image");
					break;
				case S_FLASH_BOOT_IMAGE:
					ui.updateState("Writing Boot Image to NAND");
					break;
				case S_WAIT:
					ui.updateState("Wait for Response");
					break;
				case S_LOAD_USER_IMAGE:
					ui.updateState("Upload User Image");
					break;
				case S_FLASH_USER_IMAGE:
					ui.updateState("Writing User Image to NAND");
					break;
				default:
					ui.updateState("Unknown");
					ui.updateColor(FirmwareLoaderGUI2.C_ERROR);
			}
		}
	}
	
	public void tftpProgress(float percentage) {
		if (percentage == -100.0) return;
		
		switch (lastState) {
			case S_LOAD_IMAGE:
				ui.updateState(String.format("Upload Filesystem Image: %.1f%%", percentage));
				break;
			case S_LOAD_BOOT_IMAGE:
				ui.updateState(String.format("Upload Boot Image: %.1f%%", percentage));
				break;
			case S_LOAD_USER_IMAGE:
				ui.updateState(String.format("Upload User Image: %.1f%%", percentage));
				break;
		}
	}
	
	public String getTFTPAddr() {
		return owner.getConfig().getIPAddress();
	}
	
	public String getTFTPDir() {
		return owner.getTFTPDir();
	}
	
	// Action performed
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnApply || e.getSource() instanceof JTextField) {
			// Verify values
			if (txtAddr.getText().length() == 0) {
				JOptionPane.showMessageDialog(owner, "Switch address must be set", "Invalid", JOptionPane.WARNING_MESSAGE);
				return;
			} else if (txtSubnet.getText().length() == 0) {
				JOptionPane.showMessageDialog(owner, "Switch subnet must be set", "Invalid", JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			// If any is set, all must be set and the passwords must match
			if (txtUser.getText().length() != 0 || txtPassword.getPassword().length != 0 || txtConfirm.getPassword().length != 0) {
				String pwd1 = new String(txtPassword.getPassword());
				String pwd2 = new String(txtConfirm.getPassword());
				
				if (txtUser.getText().length() == 0) {
					JOptionPane.showMessageDialog(owner, "Switch user must be set", "Invalid", JOptionPane.WARNING_MESSAGE);
					return;
				} else if (pwd1.length() == 0) {
					JOptionPane.showMessageDialog(owner, "Switch password must be set", "Invalid", JOptionPane.WARNING_MESSAGE);
					return;
				} else if (!pwd1.equals(pwd2)) {
					JOptionPane.showMessageDialog(owner, "Passwords do not match", "Invalid", JOptionPane.WARNING_MESSAGE);
					return;
				}
			}
			
			saveOptions();
		} else if (e.getSource() == btnRevert) {
			revertOptions();
		} else if (e.getSource() == btnUseDefault) {
			txtUser.setText(owner.getConfig().getUser());
			txtPassword.setText(owner.getConfig().getPassword());
			txtConfirm.setText(owner.getConfig().getPassword());
		} else if (e.getSource() == btnProbe) {
			if (!confirmMoveAway()) return;
			
			if (loader == null) {
				JOptionPane.showMessageDialog(owner, "You must first provide connection and login information for the switch, and then click the Apply button.", "Warning", JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			DependencyGraph<SwitchPage> deps = new DependencyGraph<SwitchPage>();
			
			SwitchPage dummy = new SwitchPage(owner, "Dummy");
			
			deps.addDependency(dummy, this);
			
			ActionWindow<SwitchPage> aw = new ActionWindow<SwitchPage>(deps, dummy, "Probing", new ActionWindow.ActionTaker() {
				public void run(Object arg, ActionWindow<?>.ActionItem ui) {
					SwitchPage sp = (SwitchPage)arg;
					sp.probe(ui);
				}
				
				public void cancel(Object arg, ActionWindow<?>.ActionItem ui) {
					SwitchPage sp = (SwitchPage)arg;
					sp.cancel(ui);
				}
			});
			
			aw.showDialog();
		} else if (e.getSource() == btnLoadNow) {
			if (!confirmMoveAway()) return;
			
			if (loader == null) {
				JOptionPane.showMessageDialog(owner, "You must first provide connection and login information for the switch, and then click the Apply button.", "Error", JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			if (owner.getConfig().getBundle() == null) {
				JOptionPane.showMessageDialog(owner, "You must first choose firmware to load from the 'Firmware' tab on the 'Config' page.", "Error", JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			DependencyGraph<SwitchPage> deps = new DependencyGraph<SwitchPage>();
			SwitchPage dummy = new SwitchPage(owner, "Dummy");
			deps.addDependency(dummy, this);
			
			ActionWindow<SwitchPage> aw = new ActionWindow<SwitchPage>(deps, dummy, "Loading Firmware", new ActionWindow.ActionTaker() {
				public void run(Object arg, ActionWindow<?>.ActionItem ui) {
					SwitchPage sp = (SwitchPage)arg;
					sp.load(ui);
				}
				
				public void cancel(Object arg, ActionWindow<?>.ActionItem ui) {
					SwitchPage sp = (SwitchPage)arg;
					sp.cancel(ui);
				}
			});
			
			aw.showDialog();
		} else if (e.getSource() == btnLoadSerial) {
			if (!confirmMoveAway()) return;
			
			if (owner.getConfig().getBundle() == null) {
				JOptionPane.showMessageDialog(owner, "You must first choose firmware to load from the 'Firmware' tab on the 'Config' page.", "Error", JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			SerialLoader sl = sLoader = null;
			
			try {
				sLoader = sl =
                                    new SerialLoader(this,
                                        config.getAddress(),
                                        config.getSubnet(),
                                        config.getGateway(),
                                        owner.getConfig().getSerial(),
                                        owner.getConfig().getBaudRate(),
                                        FirmwareLoaderGUI2.getIgnoreVendor());
			} catch (IllegalArgumentException iae) {
				Utils.debug(iae);
				JOptionPane.showMessageDialog(owner, "You must first provide connection information, make sure the serial port has been set, and then click the Apply button.", "Error", JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			// So far so good, now warn the user about what he is doing
			int result = JOptionPane.showConfirmDialog(owner, "A serial load cannot back up the current switch settings, and should only be used if the switch has stopped responding. Proceed?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (result == JOptionPane.NO_OPTION) return;
			
			// Tell the user to make sure the cables are connected and to remove power from the switch
			JOptionPane.showMessageDialog(owner, "Please confirm that both an ethernet and a serial/USB cable are connected to the switch, and disconnect all power inputs from it.", "Setup", JOptionPane.WARNING_MESSAGE);
			
			DependencyGraph<SwitchPage> deps = new DependencyGraph<SwitchPage>();
			SwitchPage dummy = new SwitchPage(owner, "Dummy");
			deps.addDependency(dummy, this);
			
			final SerialLoader fsl = sl;
			ActionWindow<SwitchPage> aw = new ActionWindow<SwitchPage>(deps, dummy, "Loading Firmware", new ActionWindow.ActionTaker() {
				public void run(Object arg, ActionWindow<?>.ActionItem ui) {
					SwitchPage sp = (SwitchPage)arg;
					sp.loadSerial(ui, fsl);
				}
				
				public void cancel(Object arg, ActionWindow<?>.ActionItem ui) {
					SwitchPage sp = (SwitchPage)arg;
					sp.cancel(ui);
				}
			});
			
			aw.showDialog();
		} else if (e.getSource() == btnSaveState) {
			if (loader == null) {
				JOptionPane.showMessageDialog(owner, "You must first provide connection and login information for the switch, and then click the Apply button.", "Error", JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			DependencyGraph<SwitchPage> deps = new DependencyGraph<SwitchPage>();
			SwitchPage dummy = new SwitchPage(owner, "Dummy");
			deps.addDependency(dummy, this);
			
			ActionWindow<SwitchPage> aw = new ActionWindow<SwitchPage>(deps, dummy, "Saving Configuration", new ActionWindow.ActionTaker() {
				public void run(Object arg, ActionWindow<?>.ActionItem ui) {
					ui.updateState("Requesting configuration");
					ui.updateColor(FirmwareLoaderGUI2.C_INPROGRESS);
					
					try {
						SwitchConfig cfg = WebUI.readSwitchConfig(config.getAddress(), config.getUser(), config.getPassword());
						
						config.addSavedState(cfg);
						
						refreshStates();
						
						owner.changed = true;
						owner.resetTitle();
						
						ui.updateState("Complete");
						ui.updateColor(FirmwareLoaderGUI2.C_COMPLETE);
					} catch (Exception e) {
						Utils.debug(e);
						ui.updateState("Error: " + e.getMessage());
						ui.updateColor(FirmwareLoaderGUI2.C_ERROR);
					}
				}
				
				public void cancel(Object arg, ActionWindow<?>.ActionItem ui) { }
			});
			
			aw.showDialog();
		} else if (e.getSource() == btnRestoreState) {
			if (loader == null) {
				JOptionPane.showMessageDialog(owner, "You must first provide connection and login information for the switch, and then click the Apply button.", "Error", JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			DependencyGraph<SwitchPage> deps = new DependencyGraph<SwitchPage>();
			SwitchPage dummy = new SwitchPage(owner, "Dummy");
			deps.addDependency(dummy, this);
			
			ActionWindow<SwitchPage> aw = new ActionWindow<SwitchPage>(deps, dummy, "Restoring Configuration", new ActionWindow.ActionTaker() {
				public void run(Object arg, ActionWindow<?>.ActionItem ui) {
					ui.updateState("Restoring configuration");
					ui.updateColor(FirmwareLoaderGUI2.C_INPROGRESS);
					
					try {
						WebUI.writeSwitchConfig((SwitchConfig)cmbSavedStates.getSelectedItem(), config.getAddress(), config.getUser(), config.getPassword());
						
						ui.updateState("Complete");
						ui.updateColor(FirmwareLoaderGUI2.C_COMPLETE);
					} catch (Exception e) {
						Utils.debug(e);
						ui.updateState("Error: " + e.getMessage());
						ui.updateColor(FirmwareLoaderGUI2.C_ERROR);
					}
				}
				
				public void cancel(Object arg, ActionWindow<?>.ActionItem ui) { }
			});
			
			aw.showDialog();
		} else if (e.getSource() == btnDeleteState) {
			Iterator<SwitchConfig> savedStates = config.getSavedStates();
			while (savedStates.hasNext()) {
				if (cmbSavedStates.getSelectedItem() == savedStates.next()) {
					savedStates.remove();
					break;
				}
			}
			
			refreshStates();
		} else if (e.getSource() == bnnSaveStateFile) {
			JFileChooser chooser = new JFileChooser();
			chooser.setFileFilter(new FileNameExtensionFilter("Configuration Checkpoints", "tgz"));
			
			int retval = chooser.showOpenDialog(owner);
			if (retval == JFileChooser.APPROVE_OPTION) {
				if (chooser.getSelectedFile().isFile()) {
					retval = JOptionPane.showConfirmDialog(owner, "A file named '" + chooser.getSelectedFile().getName() + "' exists, overwrite?", "Save State", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
					
					if (retval == JOptionPane.NO_OPTION) {
						return;
					}
				} else if (chooser.getSelectedFile().exists()) {
					JOptionPane.showMessageDialog(owner, "An object named '" + chooser.getSelectedFile().getName() + "' exists and is not a file, cannot overwrite.", "Save State", JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				try {
					FileOutputStream out = new FileOutputStream(chooser.getSelectedFile());
					out.write(((SwitchConfig)(cmbSavedStates.getSelectedItem())).getData());
					out.close();
				} catch (IOException ioe) {
					Utils.debug(ioe);
					JOptionPane.showMessageDialog(owner, "Unable to save the specified configuration checkpoint:" + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else {
			throw new IllegalArgumentException("Unknown source of action event");
		}
	}
	
	// stateChanged
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() == tabMain) {
			if (tabMain.getSelectedIndex() != selectedTab) {
				if (tabMain.indexOfTab(TABNAME_GENERAL) == selectedTab) {
					// Moving away from the 'General' tab, make sure that the user has applied changes
					if (!confirmMoveAway()) {
						tabMain.setSelectedIndex(selectedTab);
						return;
					}
				} else if (tabMain.indexOfTab(TABNAME_SAVEDSTATES) == selectedTab) {
					// Moving away from the 'Saved States' tab
					// Nothing to check here
				} else {
					throw new IllegalArgumentException("Cannot determine which tab is being moved away from");
				}
			}
		} else {
			throw new IllegalArgumentException("Unexpected source of change event: " + e.getSource());
		}
	}
	
	// Refresh states
	private void refreshStates() {
		cmbSavedStates.removeAllItems();
		Iterator<SwitchConfig> savedStates = config.getSavedStates();
		while (savedStates.hasNext()) {
			cmbSavedStates.addItem(savedStates.next());
		}
		
		btnRestoreState.setEnabled(cmbSavedStates.getItemCount() > 0);
		btnDeleteState.setEnabled(cmbSavedStates.getItemCount() > 0);
		bnnSaveStateFile.setEnabled(cmbSavedStates.getItemCount() > 0);
	}
	
	// toString
	@Override
    public String toString() {
		if (displayable) {
			if (config.getName().length() > 0) {
				return config.getName() + " (" + config.getAddress() + ")";
			} else {
				return config.getAddress();
			}
		} else {
			return name;
		}
	}
	
	// Switch functions
	public boolean probe(StatusUI ui) {
		if (!displayable) return true; // Nothing to do
		
		this.ui = ui;
		
		try {
			currentAction = A_PROBING;
			
			if (ui != null) {
				ui.updateState("Probing");
				ui.updateColor(FirmwareLoaderGUI2.C_INPROGRESS);
			}
			
			SwitchConfig swConfig = WebUI.readSwitchConfig(config.getAddress(), config.getUser(), config.getPassword());
			SwitchInfo swInfo = WebUI.getSwitchInfo(config.getAddress(), config.getUser(), config.getPassword());
			boolean ethOnlyEnabled = loader.verifyEthOnlyEnabled(swConfig);
			boolean archSupported = true;
			
			lblProduct.setText("Product: " + swInfo.sxid);
			lblFWCurrent.setText("Current FW: " + swInfo.fwVersion);
			if (swInfo.vendor.length() > 0) {
				lblVendor.setText("Vendor: " + swInfo.vendor);
			} else {
				lblVendor.setText("Vendor: sixnet");
			}
			
			// Make sure the architecture is included in the bundle
			lblArch.setText("Architecture: " + owner.getConfig().getBundle().translateUserType(swInfo.getArchTag()));
			lblArch.setForeground(lblProduct.getForeground()); // Something we haven't changed, default color
			if (owner.getConfig().getBundle() != null) {
				VFile image = owner.getConfig().getBundle().find(swInfo.getArchTag(), FirmwareBundle.T_FILE_IMAGE, null);
				if (image == null) {
					archSupported = false;
					lblArch.setText(lblArch.getText() + " (Not in firmware)");
					lblArch.setForeground(FirmwareLoaderGUI2.C_ERROR);
				}
			}
			
			if (ethOnlyEnabled) {
				lblEthOnly.setText("Network Loading: Enabled");
				lblEthOnly.setForeground(lblProduct.getForeground()); // Something we haven't changed, default color
			} else {
				lblEthOnly.setText("Network Loading: Disabled");
				lblEthOnly.setForeground(FirmwareLoaderGUI2.C_ERROR); // Error, need to enable it to work properly
			}
			
			if (ui != null) {
				// Put these in order of importance
				if (!archSupported) {
					ui.updateState("Architecture not supported by chosen firmware");
					ui.updateColor(FirmwareLoaderGUI2.C_ERROR);
				} else if (!ethOnlyEnabled) {
					ui.updateState("Network loading disabled");
					ui.updateColor(FirmwareLoaderGUI2.C_WARNING);
				} else if (!FirmwareLoaderGUI2.getIgnoreVendor() && owner.getConfig().getBundle() != null && !owner.getConfig().getBundle().getFWVendor().equals(swInfo.vendor)) {
					ui.updateState("Vendor mismatch");
					ui.updateColor(FirmwareLoaderGUI2.C_WARNING);
				} else {
					ui.updateState("Complete");
					ui.updateColor(FirmwareLoaderGUI2.C_COMPLETE);
				}
			}
		} catch (Exception ex) {
			Utils.debug(ex);
			
			if (ui != null) {
				ui.updateState("Error: " + ex.getMessage());
				ui.updateColor(FirmwareLoaderGUI2.C_ERROR);
			}
			
			return false;
		} finally {
			currentAction = A_NOTHING;
		}
		
		return true;
	}
	
	public void load(StatusUI ui) {
		if (!displayable) return; // Nothing to do
		
		this.ui = ui;
		currentAction = A_LOADING;
		
		if (ui != null) {
			ui.updateState("Loading");
			ui.updateColor(FirmwareLoaderGUI2.C_INPROGRESS);
		}
		
		loader.loadFirmware(owner.getConfig().getBundle(), owner.getConfig().getSaveConfig(), owner.getConfig().getForceLoad());
		
		if (owner.getConfig().getProbeAfter() && lastState == S_COMPLETE) {
			probe(ui);
		}
		
		currentAction = A_NOTHING;
	}
	
	public void loadSerial(StatusUI ui, SerialLoader sl) {
		if (!displayable) return; // Nothing to do
		
		this.ui = ui;
		currentAction = A_SERIAL;
		
		if (ui != null) {
			ui.updateState("Loading");
			ui.updateColor(FirmwareLoaderGUI2.C_INPROGRESS);
		}
		
		sl.loadFirmware(owner.getConfig().getBundle());
		
		if (owner.getConfig().getProbeAfter() && lastState == S_COMPLETE) {
			if (loader != null) {
				probe(ui);
			}
		}
		
		currentAction = A_NOTHING;
	}
	
	public void cancel(StatusUI ui) {
		switch (currentAction) {
			case A_NOTHING:
				break;
			case A_LOADING:
			case A_PROBING:
				loader.cancel();
				break;
			case A_SERIAL:
				sLoader.cancel();
				break;
			default:
				throw new RuntimeException("Unrecognized currentAction value: " + currentAction);
		}
	}
	
	// Tree layout functions
	public void addChild(SwitchPage page) {
		page.setSwitchParent(this);
		
		synchronized (children) {
	        children.add(page);
        }
		
		page.getConfig().setParent(config);
		config.addChild(page.getConfig());
	}
	
	public void removeChild(SwitchPage page) {
		page.setSwitchParent(null);
		
		synchronized (children) {
	        children.remove(page);
        }
		
		page.getConfig().setParent(null);
		config.removeChild(page.getConfig());
	}
	
	public SwitchPage getSwitchParent() {
		return parent;
	}
	
	public void setSwitchParent(SwitchPage page) {
		if (!displayable) throw new IllegalStateException("Cannot set parent of an undisplayable switch page");
		
		parent = page;
	}
	
	public Set<SwitchPage> getChildren() {
		return new HashSet<SwitchPage>(children);
	}
	
	// Transferable functions
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return flavor.equals(FLAVOR);
	}
	
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
		if (flavor.equals(FLAVOR)) {
			return this;
		} else {
			throw new UnsupportedFlavorException(flavor);
		}
	}
		
	
	public DataFlavor[] getTransferDataFlavors() {
		return FLAVORS;
	}
}

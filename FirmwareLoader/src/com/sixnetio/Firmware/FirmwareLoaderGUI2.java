/*
 * FirmwareLoaderGUI2.java
 *
 * A different GUI for the firmware loader.
 *
 * Jonathan Pearson
 * June 26, 2007
 *
 * Version history (started at 0.4.0)
 * Version 0.4.0 (November 2, 2007)
 *   New Features:
 *     - Auto-load project on startup option
 *     - Each switch may be skipped on a Load All operation, either once or every time
 *     - The title bar now reflects the name of the loaded project, and whether it has
 *       been modified since it was last saved
 *     - "New Switch X" saves the counter in the project file
 *     - Canceling a load/probe will actually cancel operations in progress, now makes
 *       user wait for the operations to report that they have been canceled
 *     - Support for serial loading over USB
 *     - Support for using an external TFTP server, as long as we have write access to
 *       the shared directory
 *   Bug Fixes:
 *     - "Settings have changed" coming up when removing a switch after modifying it
 *     - Switch page remaining open after removing the corresponding switch
 *     - Recent projects in File menu not always being updated
 *
 * Version 0.4.1 (November 5, 2007)
 *   New Features:
 *     - Auto-load improved to allow for a specified file to always be loaded on startup
 *     - The firmware revision is specified in the title bar, so you can see what it is
 *       for auto-loaded projects
 *     - Some streamlining for quickly loading switches:
 *         - If a project is loaded, defaults to the "Switches" tab
 *         - If there are any switches in the project, selects the first one
 *         - The switch page defaults to focus on the switch address, instead of the
 *           name
 *   Bug Fixes:
 *     - Auto-loading a password-protected project and canceling the password dialog
 *       leaving the user with a running program with no windows
 *
 * Version 0.4.2 (November 6, 2007)
 *   New Features:
 *     - Auto-load improved to allow the auto-loaded file to be treated as if it were a
 *       new project
 *     - Password protection on files is now completely project-oriented; the
 *       application option only specifies the default for new projects
 *   Bug Fixes:
 *     - When quitting or selecting "File->New", canceling may not always keep the
 *       program open, resulting in a possible loss of project data
 * Version 0.4.3 (December 6, 2007)
 *   - Option to forcefully enable ethernet-only firmware loading while performing a load
 * Version 0.4.4 (December 7, 2007)
 *   - Improved the method used for forcefully enabling ethernet-only firmware loading so
 *     that it does not require a new version of the CLI that had not been packaged as of
 *     firmware release 4.1.999
 * Version 0.4.5 (December 12, 2007)
 *   New Features:
 *     - Loading and saving configuration checkpoints directly into a project
 *     - On failure during a load when the configuration was to be restored afterwards,
 *       the configuration checkpoint will be saved in the project instead of a file with
 *       a random name in the user's home directory
 *   Bug Fixes:
 *     - When switching tab pages, changing the 'Force Load' setting did not prompt for
 *       the user to save settings
 *     - If restoring the configuration checkpoint results in a response from the server
 *       containing an 'alert' call, an error message containing the alert message is
 *       displayed to the user (as opposed to appearing to succeed)
 *   Internal:
 *     - Moved project file/option handling outside of FirmwareLoaderGUI2 and into Project
 * Version 0.4.6 (December 21, 2007)
 *   New Features:
 *     - Added a new application icon (taken from kitchensync)
 *     - The user is notified in a helpful manner if the chosen firmware bundle does not
 *       support the architecture of the switch
 *   Bug Fixes:
 *     - When the switch responds with "Failed to load firmware", the loader
 *       would proceed as if it had loaded and succeeded
 *     - If the IP address in a saved project was not available on the local machine, no
 *       notification was given to the user that a new one was chosen arbitrarily
 *     - Probing a 3.5 switch with switchinfo.tgz present in the firmware bundle would
 *       break the web interface
 *     - A completely new project would believe that settings had changed because the IP
 *       address was not set in the project and was therefore different from whatever was
 *       chosen on the Local page
 * Version 0.4.7 (June 18, 2008)
 *   Note: This release is due to JNI library incompatibilities as a result of integrating
 *     the firmware loader code with the rest of the Java projects (different package means
 *     different function prototype for native calls)
 *   New Features:
 *     - Behavior on Load All (always, this time, never)
 *     - More reliable forced loading by updating the switch configuration directly,
 *        instead of parsing the web page (Note: the new method is a bit slower)
 *   Bug Fixes:
 *     - Can't remember, unfortunately, it's been so long since I made updates...
 *   Updates:
 *     - Code is in place to perform ring loads, but it needs tweaking
 *     - The firmware loader code is now integrated with the rest of the Java projects
 * Version 0.4.8 (Unreleased)
 *   Bug Fixes:
 *     - If the program was set to use a default project file, and that file did not exist,
 *       the program would display an error and exit instead of opening a blank project
 *     - File open dialogs would not remember the current directory
 *     - "Contact" info was confusing, changed to "connection"
 *     - Descriptions of the project preferences tabs were only available as tooltips
 *     - The program options tab had a confusing label
 *     - The serial load button doesn't always show up
 *       - Still not satisfied with this fix, which involved changing the preferred window size
 *       - Perhaps panels should have scroll bars?
 *     - Default file for the file chooser would sometimes be the wrong type (Bug #823)
 */

// TODO: Shut down the TFTP server after finishing a load, so we don't hog the port while idle
// TODO: If invoked with -ignoreVendor, add an extra panel to the Config Loading screen that gives the option
//   of setting the switch vendor to match the firmware (probably not saved in the project file), or maybe do it
//   automatically (only works over serial)
// TODO: Allow the user to load multiple switch configuration files at once (date cutoff?)
// TODO: Allow a firmware patch to be applied to one/all switches
// TODO: Create named switch groups which can be chosen when performing an 'all' action
// TODO: Create an environment variable editor (only works over serial)
// TODO: Configuration tools to help the user set up RSTP, VLANs, ...
/* TODO: Ring Mode:
 * Requirements:
 *   - At least one switch in the ring must be running >= 4.3
 *   - Must be loading firmware revision >= 4.3
 *   - All switches must be arranged in a dependency line with the last switch physically connected to the first
 *   - The first switch must be the one closest to the loading computer
 *   - The Restore Config After Load option must be selected
 * Procedure:
 *   - Make sure that the user has selected a firmware bundle with version >= 4.3
 *   - Determine the MAC address of the NIC that the user selected on the Local Settings page
 *   - Probe through the switches until we find one running at least 4.3, call that switch Z
 *   - While probing, make sure all switches have Ethernet-only loading enabled (or that Forced Load is selected)
 *   - While probing, make sure the bundle contains each switch's architecture
 *   - Ask Z for how many ports it has (portconf.cgi has a table with every port in it, as do a number
 *       of other pages)
 *   - Ask Z for msinfo?fdb&mac=<my mac address> to determine what port we are connected to
 *   - Tell Z to turn off every port except the one we are connected to (the port settings page)
 *   - Set up a dependency graph with the root as the top switch and two branches going in opposite directions along
 *       the tree; do not include Z in the dependency graph
 *   - Load firmware to the graph
 *   - When it finishes, turn on all ports on Z
 *   - Choose any other switch, call it Y
 *   - Ask Y for how many ports it has and for the FDB entry containing our MAC address
 *   - Turn off all ports on Y except the one we are connected to
 *   - Load firmware to Z
 *   - Turn on all ports on Y
 * Option 2: Tell the user to load one switch manually, outside of the ring, and then use it as a seed (switch Z)
 * Note: Most efficient if Z is the farthest point from where the user is connected (most loads in parallel)
 */

package com.sixnetio.Firmware;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.*;

import org.jonp.xml.*;

import com.sixnetio.GUI.ActionWindow;
import com.sixnetio.GUI.PasswordDialog;
import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.net.IfcUtil;
import com.sixnetio.util.Utils;
import com.sixnetio.util.DependencyGraph.DependencyGraph;

public class FirmwareLoaderGUI2 extends JFrame implements ActionListener, ChangeListener, ItemListener, ProjectUI, TreeSelectionListener {
	// Make sure you update this as necessary
	public static final String PROG_VERSION = "0.4.8x1";
	
	// Colors for ActionWindow messages
	public static final Color C_ERROR = Color.RED,
	                          C_WARNING = Color.ORANGE,
	                          C_COMPLETE = Color.GREEN,
	                          C_INPROGRESS = Color.BLUE,
	                          C_WAITING = Color.BLACK;
	
	// Static data members
	private static boolean ignoreVendor = false;
	
	public static boolean getIgnoreVendor() {
		return ignoreVendor;
	}
	
	private static class RecentProject {
		public JMenuItem menuItem;
		public File file;
		
		public RecentProject(JMenuItem item, File file) {
			menuItem = item;
			this.file = file;
		}
	}
	
	public static final double OPT_VERSION = 1.1; // Application options file format
	
	public static final String WINDOW_TITLE = "Batch Firmware Loader (FOR INTERNAL SIXNET USE ONLY)";
	public static final FileNameExtensionFilter FILTER_PROJECT = new FileNameExtensionFilter("Firmware Project Files (*.fpj)", "fpj");
	public static final FileNameExtensionFilter FILTER_BUNDLE = new FileNameExtensionFilter("Firmware Bundles", "fwb");
	
	public static final String APP_OPTIONS_FILE;
	static {
		APP_OPTIONS_FILE = System.getProperty("user.home") + File.separator + ".firmloadrc";
	}
	
	// Static functions
	private static void lockToSixnet() {
		// Make sure we can access svr1 and that the IP address is correct
		try {
			InetAddress svr1 = InetAddress.getByName("svr1");
			if (!svr1.getHostAddress().equals("10.128.0.1")) {
				throw new Exception();
			}
		} catch (Exception e) {
			Utils.debug(e);
			JOptionPane.showMessageDialog(null, "This program is locked for internal SIXNET use only.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}
	
	public static void main(String[] args) {
		// Lock to SIXNET (remove this or add an override option if necessary)
		lockToSixnet();
		
		String projectFile = null;
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-ignoreVendor")) {
				ignoreVendor = true;
			} else if (args[i].equals("-h") || args[i].equals("-?")) {
				usage(0);
			} else {
				// Open project file
				projectFile = args[i];
				
				File f = new File(projectFile);
				if (!f.isFile()) {
					System.err.println("File not found: " + projectFile);
					usage(1);
				}
			}
		}
		
		new FirmwareLoaderGUI2(projectFile, true);
	}
	
	public static void usage(int errno) {
		System.err.println("Usage: FirmwareLoaderGUI2 [<project file>] [{-h | -?}]");
		System.err.println("<project file>  Open the specified project file");
		System.err.println("-h/-?           Print this help message and quit");
		System.exit(errno);
	}
	
	// Private data members
	// General stuff
	private JPanel pnlMain;
	private JTree tree;
	private DefaultTreeModel treeModel;
	private SPMutableTreeNode nodeSwitches;
	private JTabbedPane tabMain;
	private FirmwareBundle notSavedBundle;
	private Project project;
	private XML xmlParser;
	public boolean changed = false; // Set to true if any config/switch changes are made
	
	private String appTabName = "Program Options",
	               switchTabName = "Switches",
	               configTabName = "Project Setup";
	
	// Used by stateChanged() and valueChanged(), updated by actionPerformed() when removing a node
	private DefaultMutableTreeNode lastSelected = null;
	private int selectedTabIndex = 0;
	
	
	// Application preferences panel
	private JPanel pnlApp;
	
	private JCheckBox chkProtectFiles;
	
	private ButtonGroup grpAutoload;
	private JRadioButton rdoNoAutoload,
	                     rdoAutoloadLast,
	                     rdoAutoloadSpecific;
	private JCheckBox chkTreatAsNew;
	private JTextField txtAutoload;
	private JButton btnAutoloadBrowse;
	
	private ButtonGroup grpTFTP;
	private JRadioButton rdoInternalTFTP,
	                     rdoExternalTFTP;
	private JTextField txtExternalTFTP;
	
	private JButton btnAppApply,
	                btnAppRevert;
	
	// Application preferences data
	// Values for autoloading
	public static final int AL_NONE = 0,
	                        AL_LAST = 1,
	                        AL_FILE = 2; // Autoload a specific file
	
	private int iAutoload = AL_NONE;
	private boolean bProtectFiles = false,
	                bExternalTFTP = false,
	                bTreatAsNew = false;
	private String sExternalTFTPDir = "",
	               sAutoload = "";
	
	// Config panel controls
	private JPanel pnlConfig;
	private JTabbedPane tabConfig;
	
	private String firmwareTabName = "Firmware",
	               loginTabName = "Login",
	               localTabName = "Local",
	               loadingTabName = "Loading";
	
	private JButton btnApply,
	                btnRevert;
	
	// Firmware panel
	private JTextField txtFirmwareFile;
	private JButton btnBrowse;
	private JLabel lblFWVersionValue,
	               lblFWVendorValue,
	               lblFWProductValue;
	
	// Login panel
	private JTextField txtUser;
	private JPasswordField txtPassword,
	                       txtConfirm;
	
	// Local panel
	private JComboBox cmbIPAddr;
	private JLabel lblSubnetValue;
	
	private JLabel lblSerial;
	private JTextField txtSerial;
	
	// Loading panel
	private ButtonGroup grpLoading;
	private JRadioButton rdoSequential,
	                     rdoParallel;
	
	private ButtonGroup grpSaveConfig;
	private JRadioButton rdoDoNotSave,
	                     rdoSaveConfig;
	
	private ButtonGroup grpOrder;
	private JRadioButton rdoTopToBottom,
	                     rdoBottomToTop;
	
	private ButtonGroup grpProbe;
	private JRadioButton rdoDoNotProbe,
	                     rdoProbeAfter;
	
	private ButtonGroup grpForceLoad;
	private JRadioButton rdoDoNotForceLoad,
	                     rdoForceLoad;
	
	// Menu items
	private JMenuItem mnuFileNew,
	                  mnuFileOpen,
	                  mnuFileSave,
	                  mnuFileSaveAs,
	                  mnuFileProtectThisProject,
	                  mnuFileChangePassword,
	                  mnuFileExit;
	
	private Vector<RecentProject> recentProjects;
	
	private JMenuItem mnuSwitchAdd,
	                  mnuSwitchRemove,
	                  mnuSwitchLoadAll,
	                  mnuSwitchProbeAll,
	                  mnuSwitchRingSetup,
	                  mnuSwitchRingLoad;
	
	private JMenuItem mnuHelpAbout;
	
	// Use one file chooser throughout so the current directory is remembered
	private JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
	
	// Constructor
	public FirmwareLoaderGUI2(String projectFile, boolean allowAutoLoad) {
		addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent e) {
				actionPerformed(new ActionEvent(mnuFileExit, 0, ""));
			}
		});
		
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		// JSplitPane with a JTree on the left and a JPanel in the center
		setLayout(new GridLayout(1, 1));
		
		tabMain = new JTabbedPane();
		// Wait on adding the change listener until later, we don't want weird exceptions
		
		add(tabMain);
		
		// Stuff like the local IP address, default user name/login, whether to backup switch configurations, ...
		//   goes into the config page
		setupConfigPanel();
		
		setupPreferencesPanel();
		
		// This is needed by all of the file loading functions
		xmlParser = new XML();
		
		// Needs to happen BEFORE setupMenus()
		loadRecentProjects();
		
		// Menubar has menus for doing everything (like adding switches, saving the current config, ...)
		// Needs to be done AFTER loadRecentProjects(), but should be done BEFORE addRecentProject()
		setupMenus();
		
		// Check if we're supposed to autoload something
		// Needs to happen AFTER setupMenus()
		readAppOpts();
		
		// If we aren't supposed to be loading a project, and we were called with allowAutoLoad,
		//   which probably means from main(), and the app-option for auto-loading is set, and
		//   (finally) there is a recent project to load, the load it
		boolean autoloading = false;
		if (projectFile == null && allowAutoLoad && iAutoload == AL_LAST && recentProjects.size() > 0) {
			// Load the most recent project (doesn't need a bump on the list, since it's the first already)
			projectFile = recentProjects.get(0).file.getAbsolutePath();
			autoloading = true;
		} else if (projectFile == null && allowAutoLoad && iAutoload == AL_FILE) {
			// Load a specific project, don't bump it on the list
			projectFile = sAutoload;
			autoloading = true;
		} else if (projectFile != null) {
			// Update recent projects to reflect opening a new file
			addRecentProject(new File(projectFile));
		}
		
		// Load the project file, if there is one
		if (projectFile != null) {
			try {
				project = new Project(this, projectFile);
				
				if (autoloading && bTreatAsNew) {
					// Treat the loaded project as a blank project, so get rid of the file name
					project.setFileName(null);
					
					// Default to the app default for protecting files
					project.setProtectedFile(bProtectFiles);
				}
				
				mnuFileProtectThisProject.setSelected(project.getProtectedFile());
				mnuFileChangePassword.setEnabled(project.getProtectedFile());
			} catch (IOException ioe) {
				// Failed to load, display a message and re-construct as an empty project
				Utils.debug(ioe);
				JOptionPane.showMessageDialog(this, "Failed to load project: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				
				projectFile = null; // Cause the no-project-to-load case to fire
			}
		}
		
		if (projectFile == null) {
			project = new Project(this);
			
			// Default to the app default for protecting files
			project.setProtectedFile(bProtectFiles);
			project.setIPAddress(cmbIPAddr.getSelectedItem().toString()); // Otherwise it is empty
			mnuFileProtectThisProject.setSelected(project.getProtectedFile());
			mnuFileChangePassword.setEnabled(project.getProtectedFile());
		}
		
		// Switches
		{
			JPanel pnlSwitches = new JPanel();
			pnlSwitches.setLayout(new GridLayout(1, 1));
			pnlSwitches.setPreferredSize(new Dimension(575, 500));
			
			tabMain.addTab(switchTabName, null, pnlSwitches, switchTabName);
			
			JPanel west = new JPanel();
			pnlMain = new JPanel();
			
			pnlSwitches.add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, west, pnlMain));
			
			west.setLayout(new GridLayout(1, 1));
			pnlMain.setLayout(new GridLayout(1, 1));
			
			// Create a root node
			nodeSwitches = new SPMutableTreeNode(new SwitchPage(this, "Switches"));
			
			// Set up the tree
			tree = new DragableJTree(treeModel = new DefaultTreeModel(nodeSwitches), this);
			//tree = new JTree(treeModel = new DefaultTreeModel(rootNode));
			tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
			tree.addTreeSelectionListener(this);
			tree.setPreferredSize(new Dimension(150, 0));
			
			DefaultTreeCellRenderer rend = new DefaultTreeCellRenderer();
			rend.setOpenIcon(new ImageIcon(this.getClass().getClassLoader().getResource("images/firmload/Parent.gif")));
			rend.setClosedIcon(new ImageIcon(this.getClass().getClassLoader().getResource("images/firmload/Parent.gif")));
			rend.setLeafIcon(new ImageIcon(this.getClass().getClassLoader().getResource("images/firmload/Leaf.gif")));
			
			tree.setCellRenderer(rend);
			
			west.add(tree);
			
			// Root needs to be visible, otherwise you cannot drag nodes around to make multiple independent trees
			//tree.setRootVisible(false);
		}
		
		// Create the tree
		{
			createSwitches(nodeSwitches, project.getRootSwitch());
			
			// If we loaded a project, there are a few user-interface smoothing things to do
			if (projectFile != null) {
				// Make the Switches tab active
				tabMain.setSelectedIndex(tabMain.indexOfTab(switchTabName));
				
				// Make the menu items enabled
				mnuSwitchAdd.setEnabled(true);
				mnuSwitchRemove.setEnabled(true);
				
				// If there are any switches in the project, select the first one
				if (nodeSwitches.getChildCount() > 0) {
					tree.setSelectionPath(new TreePath(((SPMutableTreeNode)nodeSwitches.getChildAt(0)).getPath()));
				}
				
				tree.expandPath(new TreePath(nodeSwitches.getPath()));
			}
		}
			
		revertOptions();
		changed = false;
		
		tabMain.addChangeListener(this);
		
		setIconImage((new ImageIcon(this.getClass().getClassLoader().getResource("images/firmload/appicon.gif"))).getImage());
		resetTitle();
		setPreferredSize(new Dimension(800, 650));
		pack();
		setVisible(true);
	}
	
	// Setup menus
	private void setupMenus() {
		JMenuBar menuBar = getJMenuBar();
		
		if (menuBar == null) {
			menuBar = new JMenuBar();
		} else {
			menuBar.removeAll();
		}
		
		// File menu
		{
			JMenu file = new JMenu("File");
			file.setMnemonic(KeyEvent.VK_F);
			menuBar.add(file);
			
			mnuFileNew = new JMenuItem("New");
			mnuFileNew.setMnemonic(KeyEvent.VK_N);
			mnuFileNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
			mnuFileNew.addActionListener(this);
			file.add(mnuFileNew);
			
			file.addSeparator();
			
			mnuFileOpen = new JMenuItem("Open...");
			mnuFileOpen.setMnemonic(KeyEvent.VK_O);
			mnuFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
			mnuFileOpen.addActionListener(this);
			file.add(mnuFileOpen);
			
			mnuFileSave = new JMenuItem("Save");
			mnuFileSave.setMnemonic(KeyEvent.VK_S);
			mnuFileSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
			mnuFileSave.addActionListener(this);
			file.add(mnuFileSave);
			
			mnuFileSaveAs = new JMenuItem("Save As...");
			mnuFileSaveAs.setMnemonic(KeyEvent.VK_A);
			mnuFileSaveAs.addActionListener(this);
			file.add(mnuFileSaveAs);
			
			file.addSeparator();
			
			mnuFileProtectThisProject = new JCheckBoxMenuItem("Protect This Project", Project.D_PROTECTED_FILE);
			mnuFileProtectThisProject.setMnemonic(KeyEvent.VK_T);
			mnuFileProtectThisProject.addActionListener(this);
			file.add(mnuFileProtectThisProject);
			
			mnuFileChangePassword = new JMenuItem("Change Password...");
			mnuFileChangePassword.setMnemonic(KeyEvent.VK_P);
			mnuFileChangePassword.addActionListener(this);
			if (project == null) {
				mnuFileChangePassword.setEnabled(Project.D_PROTECTED_FILE);
			} else {
				mnuFileChangePassword.setEnabled(project.getProtectedFile());
			}
			file.add(mnuFileChangePassword);
			
			file.addSeparator();
			
			if (recentProjects.size() > 0) {
				int count = 0;
				for (RecentProject prj : recentProjects) {
					count++;
					
					if (prj.menuItem == null) {
						prj.menuItem = new JMenuItem(count + ") " + prj.file.getName());
						prj.menuItem.setMnemonic(KeyEvent.VK_0 + count); // It's not exactly ideal, but it should work
						prj.menuItem.addActionListener(this);
					}
					
					file.add(prj.menuItem);
				}
				
				file.addSeparator();
			}
			
			mnuFileExit = new JMenuItem("Exit");
			mnuFileExit.setMnemonic(KeyEvent.VK_X);
			mnuFileExit.addActionListener(this);
			file.add(mnuFileExit);
		}
		
		// Switch Menu
		{
			JMenu sw = new JMenu("Switch");
			sw.setMnemonic(KeyEvent.VK_S);
			menuBar.add(sw);
			
			mnuSwitchAdd = new JMenuItem("Add Switch");
			mnuSwitchAdd.setMnemonic(KeyEvent.VK_D);
			mnuSwitchAdd.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.CTRL_MASK));
			mnuSwitchAdd.addActionListener(this);
			mnuSwitchAdd.setEnabled(false); // Start on the Config tab, so disable this
			sw.add(mnuSwitchAdd);
			
			mnuSwitchRemove = new JMenuItem("Remove Switch");
			mnuSwitchRemove.setMnemonic(KeyEvent.VK_R);
			mnuSwitchRemove.addActionListener(this);
			mnuSwitchRemove.setEnabled(false); // Start on the Config tab, so disable this
			sw.add(mnuSwitchRemove);
			
			sw.addSeparator();
			
			mnuSwitchLoadAll = new JMenuItem("Load All");
			mnuSwitchLoadAll.setMnemonic(KeyEvent.VK_L);
			mnuSwitchLoadAll.addActionListener(this);
			sw.add(mnuSwitchLoadAll);
			
			mnuSwitchProbeAll = new JMenuItem("Probe All");
			mnuSwitchProbeAll.setMnemonic(KeyEvent.VK_P);
			mnuSwitchProbeAll.addActionListener(this);
			sw.add(mnuSwitchProbeAll);
			
			/*
			sw.addSeparator();
			
			mnuSwitchRingSetup = new JMenuItem("Ring Setup...");
			mnuSwitchRingSetup.setMnemonic(KeyEvent.VK_S);
			mnuSwitchRingSetup.addActionListener(this);
			sw.add(mnuSwitchRingSetup);
			
			mnuSwitchRingLoad = new JMenuItem("Ring Load");
			mnuSwitchRingLoad.setMnemonic(KeyEvent.VK_R);
			mnuSwitchRingLoad.addActionListener(this);
			sw.add(mnuSwitchRingLoad);
			*/
		}
		
		// Help Menu
		{
			JMenu help = new JMenu("Help");
			help.setMnemonic(KeyEvent.VK_H);
			menuBar.add(help);
			
			mnuHelpAbout = new JMenuItem("About...");
			mnuHelpAbout.setMnemonic(KeyEvent.VK_A);
			mnuHelpAbout.addActionListener(this);
			help.add(mnuHelpAbout);
		}
		
		if (getJMenuBar() == null) {
			setJMenuBar(menuBar);
		}
		
		// This is a workaround to a Swing bug
		menuBar.updateUI();
	}
	
	// Setup config panel
	private void setupConfigPanel() {
		pnlConfig = new JPanel();
		pnlConfig.setLayout(new BorderLayout());
		
		tabConfig = new JTabbedPane();
		pnlConfig.add(tabConfig, BorderLayout.CENTER);
		
		tabMain.addTab(configTabName, null, pnlConfig, "Project-specific configuration options");
		
		FocusListener fl = new FocusAdapter() {
			@Override
            public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextField) {
					((JTextField)(e.getSource())).selectAll();
				}
			}
		};
		
		// Firmware panel
		{
			JPanel firmwarePanel = new JPanel();
			tabConfig.addTab(firmwareTabName, null, firmwarePanel, "Specify the firmware file to load");
			
			firmwarePanel.setLayout(new BorderLayout());
			
			// Top (description)
			{
				JPanel desc = new JPanel();
				desc.setLayout(new FlowLayout(FlowLayout.CENTER));
				firmwarePanel.add(desc, BorderLayout.NORTH);
				
				desc.add(new JLabel("Specify the firmware file to load onto all switches."));
			}
			
			// Bottom (fields)
			{
				GridBagLayout gridbag = new GridBagLayout();
				GridBagConstraints gb = new GridBagConstraints();
				
				JPanel fields = new JPanel();
				fields.setLayout(gridbag);
				firmwarePanel.add(fields, BorderLayout.CENTER);
				
    			JLabel lblTemp;
    			
    			lblTemp = new JLabel("File: ");
    			gb.gridwidth = 1;
    			gridbag.setConstraints(lblTemp, gb);
    			fields.add(lblTemp);
    			
    			txtFirmwareFile = new JTextField(25);
    			txtFirmwareFile.setText(Project.D_FIRMWARE_FILE);
    			txtFirmwareFile.setEditable(false);
    			gb.gridwidth = 3;
    			gridbag.setConstraints(txtFirmwareFile, gb);
    			fields.add(txtFirmwareFile);
    			
    			btnBrowse = new JButton("...");
    			btnBrowse.setPreferredSize(new Dimension(25, 20));
    			btnBrowse.addActionListener(this);
    			gb.gridwidth = GridBagConstraints.REMAINDER;
    			gridbag.setConstraints(btnBrowse, gb);
    			fields.add(btnBrowse);
    			
    			lblTemp = new JLabel("Version: ");
    			gb.gridwidth = 1;
    			gridbag.setConstraints(lblTemp, gb);
    			fields.add(lblTemp);
    			
    			lblFWVersionValue = new JLabel("?.?.?");
    			gb.gridwidth = GridBagConstraints.REMAINDER;
    			gridbag.setConstraints(lblFWVersionValue, gb);
    			fields.add(lblFWVersionValue);
    			
    			lblTemp = new JLabel("Vendor: ");
    			gb.gridwidth = 1;
    			gridbag.setConstraints(lblTemp, gb);
    			fields.add(lblTemp);
    			
    			lblFWVendorValue = new JLabel("???");
    			gb.gridwidth = GridBagConstraints.REMAINDER;
    			gridbag.setConstraints(lblFWVendorValue, gb);
    			fields.add(lblFWVendorValue);
    			
    			lblTemp = new JLabel("Product Family: ");
    			gb.gridwidth = 1;
    			gridbag.setConstraints(lblTemp, gb);
    			fields.add(lblTemp);
    			
    			lblFWProductValue = new JLabel("???");
    			gb.gridwidth = GridBagConstraints.REMAINDER;
    			gridbag.setConstraints(lblFWProductValue, gb);
    			fields.add(lblFWProductValue);
			}
		}
		
		// Login panel
		{
			JPanel loginPanel = new JPanel();
			tabConfig.addTab(loginTabName, null, loginPanel, "Specify the default login for new switches");
			
			loginPanel.setLayout(new BorderLayout());
			
			// Top (description)
			{
				JPanel desc = new JPanel();
				desc.setLayout(new FlowLayout(FlowLayout.CENTER));
				loginPanel.add(desc, BorderLayout.NORTH);
				
				desc.add(new JLabel("Specify the default login information for new switches."));
			}
			
			// Bottom (fields)
			{
				GridBagLayout gridbag = new GridBagLayout();
				GridBagConstraints labels = new GridBagConstraints();
				GridBagConstraints text = new GridBagConstraints();
				text.gridwidth = GridBagConstraints.REMAINDER;
				
				JPanel fields = new JPanel();
				fields.setLayout(gridbag);
				loginPanel.add(fields, BorderLayout.CENTER);
				
    			JLabel lblTemp;
    			
    			lblTemp = new JLabel("User Name: ");
    			gridbag.setConstraints(lblTemp, labels);
    			fields.add(lblTemp);
    			
    			txtUser = new JTextField(15);
    			txtUser.setText(Project.D_USER);
    			txtUser.addFocusListener(fl);
    			txtUser.addActionListener(this);
    			gridbag.setConstraints(txtUser, text);
    			fields.add(txtUser);
    			
    			lblTemp = new JLabel("Password: ");
    			gridbag.setConstraints(lblTemp, labels);
    			fields.add(lblTemp);
    			
    			txtPassword = new JPasswordField(15);
    			txtPassword.setText(Project.D_PASSWORD);
    			txtPassword.addFocusListener(fl);
    			txtPassword.addActionListener(this);
    			gridbag.setConstraints(txtPassword, text);
    			fields.add(txtPassword);
    			
    			lblTemp = new JLabel("Confirm: ");
    			gridbag.setConstraints(lblTemp, labels);
    			fields.add(lblTemp);
    			
    			txtConfirm = new JPasswordField(15);
    			txtConfirm.setText(Project.D_PASSWORD);
    			txtConfirm.addFocusListener(fl);
    			txtConfirm.addActionListener(this);
    			gridbag.setConstraints(txtConfirm, text);
    			fields.add(txtConfirm);
			}
		}
		
		// Local panel
		{
			JPanel localPanel = new JPanel();
			tabConfig.addTab(localTabName, null, localPanel, "Specify the address of the local machine");
			
			localPanel.setLayout(new BorderLayout());
			
			// Top (description)
			{
				JPanel desc = new JPanel();
				desc.setLayout(new FlowLayout(FlowLayout.CENTER));
				localPanel.add(desc, BorderLayout.NORTH);
				
				desc.add(new JLabel("Specify connection details for your local system."));
			}
			
			// Bottom (fields)
			{
				GridBagLayout gridbag = new GridBagLayout();
				GridBagConstraints labels = new GridBagConstraints();
				GridBagConstraints controls = new GridBagConstraints();
				controls.gridwidth = GridBagConstraints.REMAINDER;
				
				JPanel fields = new JPanel();
				fields.setLayout(gridbag);
				localPanel.add(fields, BorderLayout.CENTER);
			
    			JLabel lblTemp;
    			
    			lblTemp = new JLabel("IP Address: ");
    			gridbag.setConstraints(lblTemp, labels);
    			fields.add(lblTemp);
    			
    			java.util.List<String> localAddresses = null;
    			try {
        			localAddresses = new Vector<String>(IfcUtil.discoverLocalAddresses(false).keySet());
    			} catch (SocketException se) {
    				Utils.debug(se);
    				JOptionPane.showMessageDialog(null, "Unable to retrieve list of network interfaces: " + se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    				
    				System.exit(1);
       				return;
    			}
    			
    			if (localAddresses == null || localAddresses.size() == 0) {
    				JOptionPane.showMessageDialog(null, "No network interfaces found. You must have at least one network interface enabled.", "Error", JOptionPane.ERROR_MESSAGE);

    				System.exit(1);
       				return;
    			}
    			
    			cmbIPAddr = new JComboBox(new Vector<String>(localAddresses));
    			cmbIPAddr.addItemListener(this);
    			cmbIPAddr.setEditable(false);
    			gridbag.setConstraints(cmbIPAddr, controls);
    			fields.add(cmbIPAddr);
    			
    			lblTemp = new JLabel("Subnet Mask: ");
    			gridbag.setConstraints(lblTemp, labels);
    			fields.add(lblTemp);
    			
    			lblSubnetValue = new JLabel("?.?.?.?");
    			gridbag.setConstraints(lblSubnetValue, controls);
    			fields.add(lblSubnetValue);
    			
    			JPanel spacer = new JPanel();
    			spacer.setPreferredSize(new Dimension(10, 30));
    			gridbag.setConstraints(spacer, controls);
    			fields.add(spacer);
    			
    			lblSerial = new JLabel("Serial Port: ");
    			gridbag.setConstraints(lblSerial, labels);
    			fields.add(lblSerial);
    			
    			txtSerial = new JTextField(10);
    			txtSerial.setText(Project.D_SERIAL);
    			txtSerial.addFocusListener(fl);
    			txtSerial.addActionListener(this);
    			gridbag.setConstraints(txtSerial, controls);
    			fields.add(txtSerial);
    			
    			if (System.getProperty("os.name").toLowerCase().indexOf("windows") != -1) {
    				lblTemp = new JLabel("(Probably COM1)");
    			} else {
    				lblTemp = new JLabel("(Probably /dev/ttyS0)");
    			}
    			gridbag.setConstraints(lblTemp, controls);
    			fields.add(lblTemp);
    			
    			
    			// Select the one that would be used by FirmwareLoader by default
    			try {
    				InetAddress addr = InetAddress.getLocalHost();
    				cmbIPAddr.setSelectedItem(addr.getHostAddress().toString());
    			} catch (IOException ioe) {
    				// Ignore it for now, but it may come back to bite us later
    				// But we do want to make sure the fields are populated
    				Utils.debug(ioe);
    				itemStateChanged(new ItemEvent(cmbIPAddr, 0, cmbIPAddr.getSelectedItem(), ItemEvent.SELECTED));
    			}
			}
		}
		
		// Loading panel
		{
			JPanel loadingPanel = new JPanel();
			tabConfig.addTab(loadingTabName, null, loadingPanel, "Set options to control how firmware loading will take place");
			
			loadingPanel.setLayout(new BorderLayout());
			
			// Top (description)
			{
				JPanel desc = new JPanel();
				desc.setLayout(new FlowLayout(FlowLayout.CENTER));
				loadingPanel.add(desc, BorderLayout.NORTH);
				
				desc.add(new JLabel("Set options to control how firmware loading will take place."));
			}
			
			// Bottom (fields)
			{
				JPanel fields = new JPanel();
				fields.setLayout(new GridLayout(0, 2));
				loadingPanel.add(fields, BorderLayout.CENTER);
				
    			// Loading All
    			{
    				JPanel p = new JPanel();
    				p.setBorder(BorderFactory.createTitledBorder("Loading All"));
    				fields.add(p);
    				
    				p.setLayout(new GridLayout(2, 1));
    				
    				grpLoading = new ButtonGroup();
    				
    				rdoSequential = new JRadioButton("Load Switches Sequentially", !Project.D_PARALLEL);
    				grpLoading.add(rdoSequential);
    				
    				rdoParallel = new JRadioButton("Load Independent Switches in Parallel", Project.D_PARALLEL);
    				grpLoading.add(rdoParallel);
    				
    				p.add(rdoSequential);
    				p.add(rdoParallel);
    			}
    			
    			// Restore Config
    			{
    				JPanel p = new JPanel();
    				p.setBorder(BorderFactory.createTitledBorder("Restore Config"));
    				fields.add(p);
    				
    				p.setLayout(new GridLayout(2, 1));
    				
    				grpSaveConfig = new ButtonGroup();
    				
    				rdoDoNotSave = new JRadioButton("Do Not Restore", !Project.D_SAVE_CONFIG);
    				grpSaveConfig.add(rdoDoNotSave);
    				
    				rdoSaveConfig = new JRadioButton("Restore Config After Load", Project.D_SAVE_CONFIG);
    				grpSaveConfig.add(rdoSaveConfig);
    				
    				p.add(rdoDoNotSave);
    				p.add(rdoSaveConfig);
    			}
    			
    			// Ordering
    			{
    				JPanel p = new JPanel();
    				p.setBorder(BorderFactory.createTitledBorder("Ordering of Loads"));
    				fields.add(p);
    				
    				p.setLayout(new GridLayout(2, 1));
    				
    				grpOrder = new ButtonGroup();
    				
    				rdoTopToBottom = new JRadioButton("Root to Leaf Switches", !Project.D_BOTTOM_TO_TOP);
    				grpOrder.add(rdoTopToBottom);
    				
    				rdoBottomToTop = new JRadioButton("Leaf Switches to Root", Project.D_BOTTOM_TO_TOP);
    				grpOrder.add(rdoBottomToTop);
    				
    				p.add(rdoTopToBottom);
    				p.add(rdoBottomToTop);
    			}
    			
    			// Probe After Load
    			{
    				JPanel p = new JPanel();
    				p.setBorder(BorderFactory.createTitledBorder("Probe After Load"));
    				fields.add(p);
    				
    				p.setLayout(new GridLayout(2, 1));
    				
    				grpProbe = new ButtonGroup();
    				
    				rdoDoNotProbe = new JRadioButton("Do Not Probe", !Project.D_PROBE_AFTER);
    				grpProbe.add(rdoDoNotProbe);
    				
    				rdoProbeAfter = new JRadioButton("Probe After Load", Project.D_PROBE_AFTER);
    				grpProbe.add(rdoProbeAfter);
    				
    				p.add(rdoDoNotProbe);
    				p.add(rdoProbeAfter);
    			}
    			
    			// Force Load
    			{
    				JPanel p = new JPanel();
    				p.setBorder(BorderFactory.createTitledBorder("Force Ethernet-Only Load"));
    				fields.add(p);
    				
    				p.setLayout(new GridLayout(2, 1));
    				
    				grpForceLoad = new ButtonGroup();
    				
    				rdoDoNotForceLoad = new JRadioButton("Do Not Force", !Project.D_FORCE_LOAD);
    				grpForceLoad.add(rdoDoNotForceLoad);
    				
    				rdoForceLoad = new JRadioButton("Force Load", Project.D_FORCE_LOAD);
    				grpForceLoad.add(rdoForceLoad);
    				
    				p.add(rdoDoNotForceLoad);
    				p.add(rdoForceLoad);
    			}
			}
		}
		
		// Below the tabbed pane
		{
			JPanel p = new JPanel();
			pnlConfig.add(p, BorderLayout.SOUTH);
			
			p.setLayout(new FlowLayout());
			
			btnApply = new JButton("Apply");
			btnApply.addActionListener(this);
			p.add(btnApply);
			
			btnRevert = new JButton("Revert");
			btnRevert.addActionListener(this);
			p.add(btnRevert);
		}
	}
	
	// Setup preferences panel
	private void setupPreferencesPanel() {
		pnlApp = new JPanel();
		pnlApp.setLayout(new BorderLayout());
		
		tabMain.addTab(appTabName, null, pnlApp, "Application-wide preferences");
		
		FocusListener fl = new FocusAdapter() {
			@Override
            public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextField) {
					((JTextField)(e.getSource())).selectAll();
				}
			}
		};
		
		ChangeListener cl = new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (rdoNoAutoload.isSelected()) {
					chkTreatAsNew.setEnabled(false);
					txtAutoload.setEnabled(false);
					btnAutoloadBrowse.setEnabled(false);
				} else if (rdoAutoloadLast.isSelected()) {
					chkTreatAsNew.setEnabled(true);
					txtAutoload.setEnabled(false);
					btnAutoloadBrowse.setEnabled(false);
				} else {
					chkTreatAsNew.setEnabled(true);
					txtAutoload.setEnabled(true);
					btnAutoloadBrowse.setEnabled(true);
				}
				
				if (rdoInternalTFTP.isSelected()) {
					txtExternalTFTP.setEnabled(false);
				} else {
					txtExternalTFTP.setEnabled(true);
				}
			}
		};
		
		JPanel pnlMain = new JPanel();
		pnlApp.add(pnlMain, BorderLayout.CENTER);
		
		// Two columns to save space (could always convert to a JScrollPane, anyway)
		// By not telling it how many rows, we don't need to come back to change it if we add more options later
		pnlMain.setLayout(new GridLayout(0, 2));
		
		// Security
		{
			JPanel pnlSecurity = new JPanel();
			pnlSecurity.setBorder(BorderFactory.createTitledBorder("Security"));
			pnlSecurity.setLayout(new GridLayout(0, 1));
			pnlMain.add(pnlSecurity);
			
			chkProtectFiles = new JCheckBox("Password Protect New Projects");
			chkProtectFiles.setSelected(bProtectFiles);
			pnlSecurity.add(chkProtectFiles);
		}
		
		// Autoloading
		{
			grpAutoload = new ButtonGroup();
			
			JPanel pnlAutoload = new JPanel();
			pnlAutoload.setBorder(BorderFactory.createTitledBorder("Autoload Project on Startup"));
			pnlAutoload.setLayout(new GridLayout(0, 1));
			pnlMain.add(pnlAutoload);
			
			rdoNoAutoload = new JRadioButton("Blank Project", iAutoload == AL_NONE);
			rdoNoAutoload.addChangeListener(cl);
			grpAutoload.add(rdoNoAutoload);
			pnlAutoload.add(rdoNoAutoload);
			
			rdoAutoloadLast = new JRadioButton("Last Project", iAutoload == AL_LAST);
			rdoAutoloadLast.addChangeListener(cl);
			grpAutoload.add(rdoAutoloadLast);
			pnlAutoload.add(rdoAutoloadLast);
			
			rdoAutoloadSpecific = new JRadioButton("This Projct:", iAutoload == AL_FILE);
			rdoAutoloadSpecific.addChangeListener(cl);
			grpAutoload.add(rdoAutoloadSpecific);
			pnlAutoload.add(rdoAutoloadSpecific);
			
			// Text fields are special - they look stupid if you let them be grid-layouted
			{
				JPanel pnlTemp = new JPanel();
				pnlTemp.setLayout(new FlowLayout());
				pnlAutoload.add(pnlTemp);
				
				txtAutoload = new JTextField(sAutoload, 25);
				txtAutoload.addFocusListener(fl);
				txtAutoload.addActionListener(this);
				pnlTemp.add(txtAutoload);
				
				btnAutoloadBrowse = new JButton("...");
				btnAutoloadBrowse.setPreferredSize(new Dimension(25, 20));
				btnAutoloadBrowse.addActionListener(this);
				pnlTemp.add(btnAutoloadBrowse);
			}
			
			chkTreatAsNew = new JCheckBox("Treat as New Project");
			pnlAutoload.add(chkTreatAsNew);
		}
		
		// Internal/External TFTP server
		{
			grpTFTP = new ButtonGroup();
			
			JPanel pnlExternalTFTP = new JPanel();
			pnlExternalTFTP.setBorder(BorderFactory.createTitledBorder("TFTP Server"));
			pnlExternalTFTP.setLayout(new GridLayout(0, 1));
			pnlMain.add(pnlExternalTFTP);
			
			rdoInternalTFTP = new JRadioButton("Internal", !bExternalTFTP);
			rdoInternalTFTP.addChangeListener(cl);
			grpTFTP.add(rdoInternalTFTP);
			pnlExternalTFTP.add(rdoInternalTFTP);
			
			rdoExternalTFTP = new JRadioButton("External", bExternalTFTP);
			rdoExternalTFTP.addChangeListener(cl);
			grpTFTP.add(rdoExternalTFTP);
			pnlExternalTFTP.add(rdoExternalTFTP);
			
			JLabel lblTemp = new JLabel("External TFTP Shared Directory:");
			pnlExternalTFTP.add(lblTemp);
			
			// Text fields are special - they look stupid if you let them be grid-layouted
			JPanel pnlTemp = new JPanel();
			pnlTemp.setLayout(new FlowLayout());
			pnlExternalTFTP.add(pnlTemp);
			
			txtExternalTFTP = new JTextField(sExternalTFTPDir, 30);
			txtExternalTFTP.addFocusListener(fl);
			txtExternalTFTP.addActionListener(this);
			pnlTemp.add(txtExternalTFTP);
		}
		
		// Apply/Revert buttons
		JPanel pnlBottom = new JPanel();
		pnlBottom.setLayout(new FlowLayout());
		pnlApp.add(pnlBottom, BorderLayout.SOUTH);
		
		btnAppApply = new JButton("Apply");
		btnAppApply.addActionListener(this);
		pnlBottom.add(btnAppApply);
		
		btnAppRevert = new JButton("Revert");
		btnAppRevert.addActionListener(this);
		pnlBottom.add(btnAppRevert);
	}
	
	// actionPerformed
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnApply) {
			saveOptions();
		} else if (e.getSource() == btnAppApply) {
			saveAppOptions();
		} else if (e.getSource() instanceof JTextField) {
			// Since you shouldn't be able to get from one tab to another without applying or reverting options...
			// Save all of them
			saveOptions();
			saveAppOptions();
		} else if (e.getSource() == btnRevert) {
			revertOptions();
		} else if (e.getSource() == btnAppRevert) {
			revertAppOptions();
		} else if (e.getSource() == btnBrowse) {
			fileChooser.setFileFilter(FILTER_BUNDLE);
			
			// If firmware has been chosen, set the default directory to the same place
			if (project.getFirmwareFile().length() > 0) {
				fileChooser.setSelectedFile(new File(project.getFirmwareFile()));
			} else {
				fileChooser.setSelectedFile(null);
			}
			
			int retval = fileChooser.showOpenDialog(this);
			if (retval == JFileChooser.APPROVE_OPTION) {
				try {
					FirmwareBundle bundle;
					InputStream in = new FileInputStream(fileChooser.getSelectedFile());
					try {
						bundle = new FirmwareBundle(in);
					}
					finally {
						in.close();
					}
					
					notSavedBundle = bundle;
					
					txtFirmwareFile.setText(fileChooser.getSelectedFile().getAbsolutePath());
					txtFirmwareFile.setToolTipText(txtFirmwareFile.getText()); // In case it's too long to fit in the box
					
					lblFWVersionValue.setText(bundle.getFWVersion().replace('_', '.'));
					String vendor = bundle.getFWVendor();
					if (vendor.length() > 0) {
						lblFWVendorValue.setText(vendor);
					} else {
						lblFWVendorValue.setText("sixnet");
					}
					lblFWProductValue.setText(bundle.getFWTypeAsString());
					
					lblFWVersionValue.setForeground(Color.BLACK);
					lblFWVendorValue.setForeground(Color.BLACK);
					lblFWProductValue.setForeground(Color.BLACK);
				} catch (IOException ioe) {
					Utils.debug(ioe);
					JOptionPane.showMessageDialog(this, "Unable to open the specified firmware file: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					return;
				} catch (OutOfMemoryError oome) {
					Utils.debug(oome);
					JOptionPane.showMessageDialog(this,
					                              "Unable to open the specified firmware file: Out of memory\n" +
					                              "Try running Java with a larger maximum heap size, such as '-Xmx128M'", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
			}
		} else if (e.getSource() == mnuFileNew) {
			if (detectConfigChange()) {
				int result = JOptionPane.showConfirmDialog(this, "Settings have changed, keep the changes?", "Keep Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (result == JOptionPane.YES_OPTION) {
					if (!saveOptions()) {
						return;
					}
				} else if (result == JOptionPane.NO_OPTION) {
					revertOptions();
				} else { // Must be cancel
					return;
				}
			}
			
			if (changed) {
				int result = JOptionPane.showConfirmDialog(this, "Save project?", "Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				
				if (result == JOptionPane.YES_OPTION) {
					mnuFileSave.doClick();
					if (changed) {
						return; // The user must have canceled something, or there was an error
					}
				} else if (result == JOptionPane.CANCEL_OPTION) {
					return;
				}
			}
			
			new FirmwareLoaderGUI2(null, false);
			
			setVisible(false);
			dispose();
		} else if (e.getSource() == btnAutoloadBrowse) {
			fileChooser.setFileFilter(FILTER_PROJECT);
			
			// If there's a project file, set the default directory to the same place
			if (project.getFileName() != null) {
				fileChooser.setSelectedFile(new File(project.getFileName()));
			} else {
				fileChooser.setSelectedFile(null);
			}
			
			int retval = fileChooser.showOpenDialog(this);
			if (retval == JFileChooser.APPROVE_OPTION) {
				// If it isn't a file (or doesn't exist), and doesn't end with .fpj, add .fpj and try again
				if (!fileChooser.getSelectedFile().isFile() && fileChooser.getSelectedFile().getName().indexOf('.') == -1) {
					fileChooser.setSelectedFile(new File(fileChooser.getSelectedFile().getAbsolutePath() + ".fpj"));
				}
				
				if (!fileChooser.getSelectedFile().isFile()) {
					JOptionPane.showMessageDialog(this, "Chosen file does not exist or is not a file.", "Error", JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				txtAutoload.setText(fileChooser.getSelectedFile().getAbsolutePath());
			}
		} else if (e.getSource() == mnuFileOpen) {
			if (detectConfigChange()) {
				int result = JOptionPane.showConfirmDialog(this, "Settings have changed, keep the changes?", "Keep Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (result == JOptionPane.YES_OPTION) {
					if (!saveOptions()) {
						return;
					}
				} else if (result == JOptionPane.NO_OPTION) {
					revertOptions();
				} else { // Must be cancel
					return;
				}
			}
			
			if (changed) {
				int result = JOptionPane.showConfirmDialog(this, "Save project?", "Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				
				if (result == JOptionPane.YES_OPTION) {
					mnuFileSave.doClick();
					if (changed) {
						return; // Must have had an error
					}
				} else if (result == JOptionPane.CANCEL_OPTION) {
					return;
				}
			}
			
			fileChooser.setFileFilter(FILTER_PROJECT);
			
			// If there's a project file, set the default directory to the same place
			if (project.getFileName() != null) {
				fileChooser.setSelectedFile(new File(project.getFileName()));
			} else {
				fileChooser.setSelectedFile(null);
			}
			
			int retval = fileChooser.showOpenDialog(this);
			if (retval == JFileChooser.APPROVE_OPTION) {
				// If it isn't a file (or doesn't exist), and doesn't end with .fpj, add .fpj and try again
				if (!fileChooser.getSelectedFile().isFile() && fileChooser.getSelectedFile().getName().indexOf('.') == -1) {
					fileChooser.setSelectedFile(new File(fileChooser.getSelectedFile().getAbsolutePath() + ".fpj"));
				}
				
				if (!fileChooser.getSelectedFile().isFile()) {
					JOptionPane.showMessageDialog(this, "Chosen file does not exist or is not a file.", "Error", JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				String projFile = fileChooser.getSelectedFile().getAbsolutePath();
				
				new FirmwareLoaderGUI2(projFile, false);
				
				setVisible(false);
				dispose();
			}
		} else if (e.getSource() == mnuFileSave) {
			if (project.getFileName() == null) {
				mnuFileSaveAs.doClick();
			} else {
				try {
					project.save();
					changed = false;
					resetTitle();
				} catch (IOException ioe) {
					Utils.debug(ioe);
					JOptionPane.showMessageDialog(this, "Unable to save file: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if (e.getSource() == mnuFileSaveAs) {
			fileChooser.setFileFilter(FILTER_PROJECT);
			
			boolean saved = false;
			while (!saved) {
				boolean readyToSave = false;
				
				while (!readyToSave) {
					// If there's a project file, set the default directory to the same place
					if (project.getFileName() != null) {
						fileChooser.setSelectedFile(new File(project.getFileName()));
					} else {
						fileChooser.setSelectedFile(null);
					}
					
					int retval = fileChooser.showSaveDialog(this);
					if (retval == JFileChooser.APPROVE_OPTION) {
						if (!fileChooser.getSelectedFile().isFile() && fileChooser.getSelectedFile().getName().indexOf('.') == -1) {
							fileChooser.setSelectedFile(new File(fileChooser.getSelectedFile().getAbsolutePath() + ".fpj"));
						}
						
						// If the file exists, confirm the overwrite
						if (fileChooser.getSelectedFile().isFile()) {
							retval = JOptionPane.showConfirmDialog(this, "A file named '" + fileChooser.getSelectedFile().getName() + "' exists, overwrite?", "Save", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
							
							if (retval == JOptionPane.YES_OPTION) {
								readyToSave = true;
							} else {
								readyToSave = false;
							}
						} else if (fileChooser.getSelectedFile().exists()) {
							JOptionPane.showMessageDialog(this, "An object named '" + fileChooser.getSelectedFile().getName() + "' exists and is not a file, cannot overwrite.", "Save", JOptionPane.WARNING_MESSAGE);
							readyToSave = false;
						} else {
							// Doesn't exist
							readyToSave = true;
						}
					} else if (retval == JFileChooser.CANCEL_OPTION) {
						return;
					}
				}
				
				project.setFileName(fileChooser.getSelectedFile().getAbsolutePath());
				try {
					project.save();
					
					// Update recent projects to reflect the new one
					addRecentProject(fileChooser.getSelectedFile());
					
					saved = true;
					changed = false;
					resetTitle();
				} catch (IOException ioe) {
					Utils.debug(ioe);
					JOptionPane.showMessageDialog(this, "Unable to save: " + ioe.getMessage(), "Save", JOptionPane.ERROR_MESSAGE);
					saved = false;
				}
			}
		} else if (e.getSource() == mnuFileProtectThisProject) {
			// Toggle protection on this project
			project.setProtectedFile(mnuFileProtectThisProject.isSelected());
			mnuFileChangePassword.setEnabled(project.getProtectedFile());
			
			if (!project.getProtectedFile()) {
				project.clearProjectPassword();
			}
			
			changed = true; // The format of the save file will change, so we need to do this
			resetTitle();
		} else if (e.getSource() == mnuFileChangePassword) {
			PasswordDialog pwd = new PasswordDialog("Please enter a new password for this project", "Password", false, true);
			PasswordDialog.UserPass userPass = pwd.showDialog();
			
			if (userPass == null) {
				return;
			}
			
			project.setProjectPassword(userPass.passwd);
			Arrays.fill(userPass.passwd, '\0'); // A little protection
		} else if (e.getSource() == mnuFileExit) {
			// Make sure there isn't unsaved configuration data
			if (detectConfigChange()) {
				int result = JOptionPane.showConfirmDialog(this, "Settings have changed, keep the changes?", "Keep Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (result == JOptionPane.YES_OPTION) {
					if (!saveOptions()) {
						return;
					}
				} else if (result == JOptionPane.NO_OPTION) {
					revertOptions();
				} else { // Must be cancel
					return;
				}
			}
			
			// Make sure there isn't unsaved data about a switch
			if (tabMain.indexOfTab(switchTabName) == selectedTabIndex) {
				if (lastSelected != null) {
					SwitchPage page = (SwitchPage)((SPMutableTreeNode)lastSelected).getUserObject();
					if (page.isDisplayable()) {
						if (!page.confirmMoveAway()) {
							tabMain.setSelectedIndex(tabMain.indexOfTab(switchTabName));
							tree.setSelectionPath(new TreePath(lastSelected.getPath()));
							return;
						}
					}
				}
			}
			
			// Make sure there isn't unsaved application options data
			if (detectAppConfigChange()) {
				int result = JOptionPane.showConfirmDialog(this, "Application options have changed, keep the changes?", "Keep Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (result == JOptionPane.YES_OPTION) {
					saveAppOptions();
				} else if (result == JOptionPane.NO_OPTION) {
					revertOptions();
				} else { // Must be cancel
					return;
				}
			}
			
			if (changed) {
				int result = JOptionPane.showConfirmDialog(this, "Save project?", "Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				
				if (result == JOptionPane.YES_OPTION) {
					mnuFileSave.doClick();
					if (changed) {
						return; // Must have had an error
					}
				} else if (result == JOptionPane.CANCEL_OPTION) {
					return;
				}
			}
			
			System.exit(0);
		} else if (e.getSource() == mnuSwitchAdd) {
			if (detectConfigChange()) {
				int result = JOptionPane.showConfirmDialog(this, "Settings have changed, keep the changes?", "Keep Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (result == JOptionPane.YES_OPTION) {
					if (!saveOptions()) {
						return;
					}
				} else if (result == JOptionPane.NO_OPTION) {
					revertOptions();
				} else { // Must be cancel
					return;
				}
			}
			
			SPMutableTreeNode selected;
			
			if (tree.getLastSelectedPathComponent() == null) {
				selected = nodeSwitches;
			} else if (tree.getLastSelectedPathComponent() instanceof SPMutableTreeNode) {
				selected = (SPMutableTreeNode)tree.getLastSelectedPathComponent();
				
				// Try to make the new node a sibling of the current node, instead of a child
				if (selected.getParent() != null) {
					selected = (SPMutableTreeNode)selected.getParent();
				}
			} else {
				selected = nodeSwitches;
			}
			
			if (selected.getSharedAncestor(nodeSwitches) != nodeSwitches) {
				JOptionPane.showMessageDialog(this, "New switches may only be added beneath the \"Switches\" node.", "Error", JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			SPMutableTreeNode newNode = new SPMutableTreeNode(new SwitchPage(this, project.getNewSwitch()));
			
			treeModel.insertNodeInto(newNode, selected, selected.getChildCount());
			
			TreePath path = new TreePath(newNode.getPath());
			tree.setSelectionPath(path);
			
			changed = true;
			resetTitle();
		} else if (e.getSource() == mnuSwitchRemove) {
			SPMutableTreeNode selected = (SPMutableTreeNode)tree.getLastSelectedPathComponent();
			if (selected != null && (selected.getUserObject() instanceof SwitchPage)) {
				lastSelected = null; // Make sure 'Do you want to keep changes' box doesn't come up
				tree.setSelectionPath(new TreePath(nodeSwitches.getPath())); // Make sure the page doesn't stay up
				treeModel.removeNodeFromParent(selected); // Delete the node
				
				Project.Switch removing = ((SwitchPage)selected.getUserObject()).getConfig();
				removing.getParent().removeChild(removing);
				
				changed = true;
				resetTitle();
			}
		} else if (e.getSource() == mnuSwitchLoadAll) {
			loadAll();
		} else if (e.getSource() == mnuSwitchProbeAll) {
			probeAll();
		} else if (e.getSource() == mnuSwitchRingSetup) {
			ringSetup();
		} else if (e.getSource() == mnuSwitchRingLoad) {
			ringLoad();
		} else if (e.getSource() == mnuHelpAbout) {
			JOptionPane.showMessageDialog(this,
			                              "Firmware Loader v" + PROG_VERSION + "\n" +
		                                  "Written by Jonathan Pearson\n" +
		                                  "This program includes code originally distributed under the Apache Software License, Version 2.0",
		                                  "About",
		                                  JOptionPane.INFORMATION_MESSAGE);
		} else {
			if (e.getSource() instanceof JMenuItem) {
				for (RecentProject prj : recentProjects) {
					if (prj.menuItem == e.getSource()) {
						String projFile = prj.file.getAbsolutePath();
						
						new FirmwareLoaderGUI2(projFile, false);
						
						setVisible(false);
						dispose();
						return;
					}
				}
			}
			
			throw new IllegalArgumentException("Unexpected source of action event: " + e.getSource());
		}
	}
	
	// stateChanged
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() == tabMain) {
			// First-up, let the user know that his changes were not saved before switching away
			if (tabMain.getSelectedIndex() != selectedTabIndex) { // Moved away from a tab
				// Which are we moving away from?
				if (tabMain.indexOfTab(configTabName) == selectedTabIndex) { // Moved away from the config tab
					if (detectConfigChange()) {
						int result = JOptionPane.showConfirmDialog(this, "Settings have changed, keep the changes?", "Keep Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
						if (result == JOptionPane.YES_OPTION) {
							if (!saveOptions()) {
								return;
							}
						} else if (result == JOptionPane.NO_OPTION) {
							revertOptions();
						} else { // Must be cancel
							tabMain.setSelectedIndex(selectedTabIndex);
							return;
						}
					}
				} else if (tabMain.indexOfTab(switchTabName) == selectedTabIndex) { // Moved away from the switches tab
					if (lastSelected != null) {
						SwitchPage page = (SwitchPage)((SPMutableTreeNode)lastSelected).getUserObject();
						if (page.isDisplayable()) {
							if (!page.confirmMoveAway()) {
								tabMain.setSelectedIndex(tabMain.indexOfTab(switchTabName));
								tree.setSelectionPath(new TreePath(lastSelected.getPath()));
								return;
							}
						}
					}
				} else if (tabMain.indexOfTab(appTabName) == selectedTabIndex) { // Moved away from the preferences tab
					// If anything changed, let the user know
					if (detectAppConfigChange()) {
						int result = JOptionPane.showConfirmDialog(this, "Preferences have changed, keep the changes?", "Keep Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
						if (result == JOptionPane.YES_OPTION) {
							saveAppOptions();
						} else if (result == JOptionPane.NO_OPTION) {
							revertAppOptions();
						} else { // Must be cancel
							tabMain.setSelectedIndex(selectedTabIndex);
							return;
						}
					}
				} else {
					throw new IllegalStateException("Unexpected tab selected on tabMain: " + tabMain.getSelectedIndex());
				}
				
				selectedTabIndex = tabMain.getSelectedIndex();
				
				// Also, enable/disable mnuSwitchAdd and mnuSwitchRemove based on which tab we are on
				if (selectedTabIndex == tabMain.indexOfTab(switchTabName)) {
					mnuSwitchAdd.setEnabled(true);
					mnuSwitchRemove.setEnabled(true);
				} else {
					mnuSwitchAdd.setEnabled(false);
					mnuSwitchRemove.setEnabled(false);
				}
			}
		} else {
			throw new IllegalArgumentException("Unexpected source of change event: " + e.getSource());
		}
	}
	
	// itemStateChanged
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() == cmbIPAddr) {
			lblSubnetValue.setText("?.?.?.?");
			try {
				String strAddr = cmbIPAddr.getSelectedItem().toString();
				InetAddress localAddr = InetAddress.getByName(strAddr);
				NetworkInterface ifc = NetworkInterface.getByInetAddress(localAddr);
				
				Iterator<InterfaceAddress> itChoices = ifc.getInterfaceAddresses().iterator();
				while (itChoices.hasNext()) {
					InterfaceAddress ifaddr = itChoices.next();
					
					if (ifaddr.getAddress().equals(localAddr)) {
						lblSubnetValue.setText(Utils.subnetFromPrefix(ifaddr.getNetworkPrefixLength()));
						break;
					}
				}
			} catch (Exception ex) {
				Utils.debug(ex);
				JOptionPane.showMessageDialog(this, "Unable to retrieve information about " + cmbIPAddr.getSelectedItem().toString() + ": " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				ex.printStackTrace();
			}
			
			setPreferredSize(getSize());
			pack();
		} else {
			throw new IllegalArgumentException("Unexpected source of item event: " + e.getSource());
		}
	}
	
	// valueChanged
	public void valueChanged(TreeSelectionEvent e) {
		SPMutableTreeNode selected = (SPMutableTreeNode)tree.getLastSelectedPathComponent();
		
		// Make sure the user knows to save
		if (lastSelected != null && lastSelected instanceof SPMutableTreeNode && selected != lastSelected) {
			SwitchPage page = (SwitchPage)((SPMutableTreeNode)lastSelected).getUserObject();
			if (page.isDisplayable()) {
				if (!page.confirmMoveAway()) {
					tree.setSelectionPath(new TreePath(lastSelected.getPath()));
					return;
				}
			}
		}
		
		lastSelected = selected;
		
		if (selected == null) {
			return;
		}
		
		if (selected == nodeSwitches) {
			pnlMain.removeAll();
			// There is nothing to display for this
			
			setPreferredSize(getSize());
			pack();
			repaint();
		} else if (selected.getUserObject() instanceof SwitchPage) {
			pnlMain.removeAll();
			pnlMain.add((SwitchPage)selected.getUserObject());
			((SwitchPage)selected.getUserObject()).giveFocus();
			
			setPreferredSize(getSize());
			pack();
			repaint();
		} else {
			throw new IllegalArgumentException("Unexpected source of tree selection event");
		}
	}
	
	// Accessor functions
	public String getTFTPDir() {
		if (bExternalTFTP) {
			return sExternalTFTPDir;
		} else {
			return null;
		}
	}
	
	public XML getXMLParser() {
		return xmlParser;
	}
	
	public Project getConfig() {
		return project;
	}
	
	public int getRootSwitchCount() {
		return nodeSwitches.getChildCount();
	}
	
	public SPMutableTreeNode getRootSwitch(int index) {
		return (SPMutableTreeNode)nodeSwitches.getChildAt(index);
	}
	
	public boolean getProtectFiles() {
		return bProtectFiles;
	}
	
	// Save/Revert options
	private boolean saveOptions() {
		String pwd1 = new String(txtPassword.getPassword());
		String pwd2 = new String(txtConfirm.getPassword());
		
		// Verify
		if (txtUser.getText().length() != 0 || txtPassword.getPassword().length != 0 || txtConfirm.getPassword().length != 0) {
			if (!pwd1.equals(pwd2)) {
				tabConfig.setSelectedIndex(tabConfig.indexOfTab(loginTabName));
				txtPassword.requestFocusInWindow();
				JOptionPane.showMessageDialog(this, "Password confirmation failed, please re-enter your password.", "Error", JOptionPane.WARNING_MESSAGE);
				return false;
			}
		}
		
		if (notSavedBundle != null && notSavedBundle.getFWType() != FirmwareBundle.T_TYPE_MS) {
			tabConfig.setSelectedIndex(tabConfig.indexOfTab(firmwareTabName));
			txtFirmwareFile.requestFocusInWindow();
			JOptionPane.showMessageDialog(this, "You must choose firmware for the \"Managed Switch\" product family.", "Error", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		
		// Firmware panel
		project.setFirmwareFile(txtFirmwareFile.getText());
		project.setBundle(notSavedBundle);
		
		// Login panel
		project.setUser(txtUser.getText());
		project.setPassword(pwd1);
		
		// Local panel
		project.setIPAddress(cmbIPAddr.getSelectedItem().toString());
		project.setSerial(txtSerial.getText());
		
		// Loading panel
		project.setParallel(rdoParallel.isSelected());
		project.setSaveConfig(rdoSaveConfig.isSelected());
		project.setBottomToTop(rdoBottomToTop.isSelected());
		project.setProbeAfter(rdoProbeAfter.isSelected());
		project.setForceLoad(rdoForceLoad.isSelected());
		
		changed = true;
		resetTitle();
		
		return true;
	}
	
	private boolean ipWarning = false; // So we only display the IP warning dialog once
	private void revertOptions() {
		// Firmware panel
		txtFirmwareFile.setText(project.getFirmwareFile());
		txtFirmwareFile.setToolTipText(project.getFirmwareFile());
		notSavedBundle = project.getBundle();
		
		if (notSavedBundle != null) {
			lblFWVersionValue.setText(notSavedBundle.getFWVersion().replace('_', '.'));
			String vendor = notSavedBundle.getFWVendor();
			if (vendor.length() > 0) {
				lblFWVendorValue.setText(vendor);
			} else {
				lblFWVendorValue.setText("sixnet");
			}
			lblFWProductValue.setText(notSavedBundle.getFWTypeAsString());
			
			lblFWVersionValue.setForeground(Color.BLACK);
			lblFWVendorValue.setForeground(Color.BLACK);
			lblFWProductValue.setForeground(Color.BLACK);
		} else {
			lblFWVersionValue.setForeground(Color.RED);
			lblFWVendorValue.setForeground(Color.RED);
			lblFWProductValue.setForeground(Color.RED);
			
			if (project.getFirmwareFile().length() > 0) {
				lblFWVersionValue.setText("File not found");
				lblFWVendorValue.setText("File not found");
				lblFWProductValue.setText("File not found");
			} else {
				lblFWVersionValue.setText("?.?.?");
				lblFWVendorValue.setText("???");
				lblFWProductValue.setText("???");
			}
		}
		
		// Login panel
		txtUser.setText(project.getUser());
		txtPassword.setText(project.getPassword());
		txtConfirm.setText(project.getPassword());
		
		// Local panel
		cmbIPAddr.setSelectedItem(project.getIPAddress());
		itemStateChanged(new ItemEvent(cmbIPAddr, 0, cmbIPAddr.getSelectedItem(), ItemEvent.SELECTED));
		txtSerial.setText(project.getSerial());
		
		if (!ipWarning && !cmbIPAddr.getSelectedItem().equals(project.getIPAddress())) {
			ipWarning = true;
			JOptionPane.showMessageDialog(this,
			                              "The project references a local IP address (" + project.getIPAddress() + ") that was not detected.\n" +
			                              "If it should have been detected, make sure that all cables are connected and that the interface is\n" +
			                              "enabled, then restart the application. If the address should not be available, please confirm the\n" +
			                              "IP address setting on the Project Setup/Local tab.",
			                              "IP Address",
			                              JOptionPane.WARNING_MESSAGE);
		}
		
		
		// Loading panel
		rdoSequential.setSelected(!project.getParallel());
		rdoParallel.setSelected(project.getParallel());
		
		rdoDoNotSave.setSelected(!project.getSaveConfig());
		rdoSaveConfig.setSelected(project.getSaveConfig());
		
		rdoTopToBottom.setSelected(!project.getBottomToTop());
		rdoBottomToTop.setSelected(project.getBottomToTop());
		
		rdoDoNotProbe.setSelected(!project.getProbeAfter());
		rdoProbeAfter.setSelected(project.getProbeAfter());
		
		rdoDoNotForceLoad.setSelected(!project.getForceLoad());
		rdoForceLoad.setSelected(project.getForceLoad());
	}
	
	// Save/Revert app options
	private void saveAppOptions() {
		bProtectFiles = chkProtectFiles.isSelected();
		
		// If the project has not been saved yet, then keep the Protect This Project status linked to bProtectFiles
		if (project.getFileName() == null) {
			project.setProtectedFile(bProtectFiles);
			mnuFileProtectThisProject.setSelected(project.getProtectedFile());
			mnuFileChangePassword.setEnabled(project.getProtectedFile());
		}
		
		if (rdoNoAutoload.isSelected()) {
			iAutoload = AL_NONE;
		} else if (rdoAutoloadLast.isSelected()) {
			iAutoload = AL_LAST;
		} else if (rdoAutoloadSpecific.isSelected()) {
			iAutoload = AL_FILE;
		}
		sAutoload = txtAutoload.getText();
		bTreatAsNew = chkTreatAsNew.isSelected();
		
		bExternalTFTP = rdoExternalTFTP.isSelected();
		sExternalTFTPDir = txtExternalTFTP.getText();
		
		try {
			writeAppOpts();
		} catch (IOException ioe) {
			Utils.debug(ioe);
			JOptionPane.showMessageDialog(this, "Unable to save preferences: " + ioe.getMessage() + "; Preferences will apply to this project only, and will be lost when the program closes.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void revertAppOptions() {
		chkProtectFiles.setSelected(bProtectFiles);
		
		rdoNoAutoload.setSelected(iAutoload == AL_NONE);
		rdoAutoloadLast.setSelected(iAutoload == AL_LAST);
		rdoAutoloadSpecific.setSelected(iAutoload == AL_FILE);
		txtAutoload.setText(sAutoload);
		chkTreatAsNew.setSelected(bTreatAsNew);
		
		rdoInternalTFTP.setSelected(!bExternalTFTP);
		rdoExternalTFTP.setSelected(bExternalTFTP);
		txtExternalTFTP.setText(sExternalTFTPDir);
	}
	
	// Detect config change functions
	private boolean detectConfigChange() {
		String pwd = new String(txtPassword.getPassword());
		
		// For debugging
		if (Utils.DEBUG) {
			if (!project.getFirmwareFile().equals(txtFirmwareFile.getText())) {
				Utils.debug("Firmware");
			}
			if (!project.getUser().equals(txtUser.getText())) {
				Utils.debug("User");
			}
			if (!project.getPassword().equals(pwd)) {
				Utils.debug("Password");
			}
			if (!project.getIPAddress().equals(cmbIPAddr.getSelectedItem().toString())) {
				Utils.debug("Chosen IP");
			}
			if (!project.getSerial().equals(txtSerial.getText())) {
				Utils.debug("Serial");
			}
			if (project.getParallel() != rdoParallel.isSelected()) {
				Utils.debug("Parallel");
			}
			if (project.getSaveConfig() != rdoSaveConfig.isSelected()) {
				Utils.debug("SaveConfig");
			}
			if (project.getBottomToTop() != rdoBottomToTop.isSelected()) {
				Utils.debug("Bottom to Top");
			}
			if (project.getProbeAfter() != rdoProbeAfter.isSelected()) {
				Utils.debug("Probe after load");
			}
			if (project.getForceLoad() != rdoForceLoad.isSelected()) {
				Utils.debug("Force load");
			}
		}
		
		boolean result = (!project.getFirmwareFile().equals(txtFirmwareFile.getText()) ||
						  !project.getUser().equals(txtUser.getText()) ||
						  !project.getPassword().equals(pwd) ||
						  !project.getIPAddress().equals(cmbIPAddr.getSelectedItem().toString()) ||
						  !project.getSerial().equals(txtSerial.getText()) ||
						  project.getParallel() != rdoParallel.isSelected() ||
						  project.getSaveConfig() != rdoSaveConfig.isSelected() ||
						  project.getBottomToTop() != rdoBottomToTop.isSelected() ||
						  project.getProbeAfter() != rdoProbeAfter.isSelected() ||
						  project.getForceLoad() != rdoForceLoad.isSelected());
		
		return result;
	}
	
	private boolean detectAppConfigChange() {
		// For debugging
		if (Utils.DEBUG) {
			if (bProtectFiles != chkProtectFiles.isSelected()) {
				Utils.debug("Protect files");
			}
			
			if (iAutoload == AL_NONE && !rdoNoAutoload.isSelected()) {
				Utils.debug("Autoload None");
			}
			
			if (iAutoload == AL_LAST && !rdoAutoloadLast.isSelected()) {
				Utils.debug("Autoload Last");
			}
			
			if (iAutoload == AL_FILE && !rdoAutoloadSpecific.isSelected()) {
				Utils.debug("Autoload Specific");
			}
			
			if (bTreatAsNew != chkTreatAsNew.isSelected()) {
				Utils.debug("Treat as new");
			}
			
			if (!sAutoload.equals(txtAutoload.getText())) {
				Utils.debug("Autoload File");
			}
			
			if (bExternalTFTP != rdoExternalTFTP.isSelected()) {
				Utils.debug("External TFTP");
			}
			
			if (bExternalTFTP && !sExternalTFTPDir.equals(txtExternalTFTP.getText())) {
				Utils.debug("External TFTP dir");
			}
		}
		
		boolean result = (bProtectFiles != chkProtectFiles.isSelected() ||
		                  (iAutoload == AL_NONE && !rdoNoAutoload.isSelected()) ||
		                  (iAutoload == AL_LAST && !rdoAutoloadLast.isSelected()) ||
		                  (iAutoload == AL_FILE && !rdoAutoloadSpecific.isSelected()) ||
		                  !sAutoload.equals(txtAutoload.getText()) ||
		                  bTreatAsNew != chkTreatAsNew.isSelected() ||
		                  bExternalTFTP != rdoExternalTFTP.isSelected() ||
		                  !sExternalTFTPDir.equals(txtExternalTFTP.getText()));
		
		return result;
	}
	
	// Functions to act on all switches
	private void probeAll() {
		DependencyGraph<SwitchPage> deps = getDependencyGraph();
		
		ActionWindow<SwitchPage> aw = new ActionWindow<SwitchPage>(deps, (SwitchPage)nodeSwitches.getUserObject(), "Probing", new ActionWindow.ActionTaker() {
			public void run(Object arg, ActionWindow<?>.ActionItem ui) {
				SwitchPage sp = (SwitchPage)arg;
				sp.probe(ui);
			}
			
			public void cancel(Object arg, ActionWindow<?>.ActionItem ui) {
				SwitchPage sp = (SwitchPage)arg;
				sp.cancel(ui);
			}
		});
		
		deps.satisfy((SwitchPage)nodeSwitches.getUserObject());
		
		aw.showDialog();
	}
	
	private void loadAll() {
		DependencyGraph<SwitchPage> deps = getDependencyGraph();
		
		ActionWindow<SwitchPage> aw = new ActionWindow<SwitchPage>(deps, (SwitchPage)nodeSwitches.getUserObject(), "Loading Firmware", new ActionWindow.ActionTaker() {
			public void run(Object arg, ActionWindow<?>.ActionItem ui) {
				SwitchPage sp = (SwitchPage)arg;
				
				if (sp.shouldLoad()) {
					sp.load(ui);
				} else {
					ui.updateColor(C_WARNING);
					ui.updateState("Skipped");
				}
			}
			
			
			public void cancel(Object arg, ActionWindow<?>.ActionItem ui) {
				SwitchPage sp = (SwitchPage)arg;
				sp.cancel(ui);
			}
		});
		
		aw.showDialog();
	}
	
	private DependencyGraph<SwitchPage> getDependencyGraph() {
		// Grab the switch node, then go through all of its children to build the graph
		DependencyGraph<SwitchPage> graph = new DependencyGraph<SwitchPage>();
		
		if (project.getParallel()) {
			for (int i = 0; i < nodeSwitches.getChildCount(); i++) {
				SPMutableTreeNode node = (SPMutableTreeNode)nodeSwitches.getChildAt(i);
				
				if (project.getBottomToTop()) {
					graph.addDependency((SwitchPage)node.getUserObject(), (SwitchPage)nodeSwitches.getUserObject());
				} else {
					graph.addDependency((SwitchPage)nodeSwitches.getUserObject(), (SwitchPage)node.getUserObject());
				}
				graphDeps(graph, node);
			}
		} else {
			// Each node depends on the one before it vertically in the tree
			SPMutableTreeNode last = nodeSwitches;
			
			if (project.getBottomToTop()) {
				// Loop from the last to the first
				for (int i = nodeSwitches.getChildCount() - 1; i >= 0; i--) {
					SPMutableTreeNode current = (SPMutableTreeNode)nodeSwitches.getChildAt(i);
					
					last = graphLinear(graph, current, last);
				}
			} else {
				for (int i = 0; i < nodeSwitches.getChildCount(); i++) {
					SPMutableTreeNode current = (SPMutableTreeNode)nodeSwitches.getChildAt(i);
					
					last = graphLinear(graph, current, last);
				}
			}
		}
		
		return graph;
	}
	
	private SPMutableTreeNode graphLinear(DependencyGraph<SwitchPage> graph, SPMutableTreeNode node, SPMutableTreeNode dependsOn) {
		if (project.getBottomToTop()) {
			// Loop from the last to the first
			for (int i = node.getChildCount() - 1; i >= 0; i--) {
				SPMutableTreeNode current = (SPMutableTreeNode)node.getChildAt(i);
				
				dependsOn = graphLinear(graph, current, dependsOn);
			}
			
			graph.addDependency((SwitchPage)dependsOn.getUserObject(), (SwitchPage)node.getUserObject());
			
			return node;
		} else {
			graph.addDependency((SwitchPage)dependsOn.getUserObject(), (SwitchPage)node.getUserObject());
			dependsOn = node; // Next node down will depend on us
			
			for (int i = 0; i < node.getChildCount(); i++) {
				SPMutableTreeNode current = (SPMutableTreeNode)node.getChildAt(i);
				
				dependsOn = graphLinear(graph, current, dependsOn);
			}
			
			return dependsOn;
		}
	}
	
	private void graphDeps(DependencyGraph<SwitchPage> graph, SPMutableTreeNode node) {
		for (int i = 0; i < node.getChildCount(); i++) {
			SPMutableTreeNode child = (SPMutableTreeNode)node.getChildAt(i);
			
			if (project.getBottomToTop()) {
				graph.addDependency((SwitchPage)child.getUserObject(), (SwitchPage)node.getUserObject());
			} else {
				graph.addDependency((SwitchPage)node.getUserObject(), (SwitchPage)child.getUserObject());
			}
			
			graphDeps(graph, child);
		}
	}
	
	// Ring functions
	private void ringSetup() {
		new RingSetup(this);
	}
	
	private void ringLoad() {
		// TODO: Special case: 2 switches
		
		// Verify that everything is set up properly
		// Requirements: Restore config after load, one dependency line of switches going straight down
		if (!project.getSaveConfig()) {
			JOptionPane.showMessageDialog(this, "The option to restore switch configurations after load must be selected.", "Ring Load", JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		if (nodeSwitches.getChildCount() > 1) {
			JOptionPane.showMessageDialog(this, "Switches must each depend on one other switch, with the dependency between the root and the last switch implied.", "Ring Load", JOptionPane.WARNING_MESSAGE);
			return;
		} else if (nodeSwitches.getChildCount() < 1) {
			JOptionPane.showMessageDialog(this, "There must be at least one switch to load.", "Ring Load", JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		SPMutableTreeNode sw = (SPMutableTreeNode)nodeSwitches.getChildAt(0);
		int count = 1; // Might as well count them as we go
		while (sw != null) {
			SwitchPage sp = (SwitchPage)sw.getUserObject();
			// Make sure it's not going to be skipped
			if (!sp.peekShouldLoad()) {
				JOptionPane.showMessageDialog(this, "Ring loading cannot skip switches, please make sure all switches are set to load every time.", "Ring Load", JOptionPane.WARNING_MESSAGE);
				return;
			} else  if (sw.getChildCount() > 1) {
				JOptionPane.showMessageDialog(this, "Switches must each depend on one other switch, with the dependency between the root and the last switch implied.", "Ring Load", JOptionPane.WARNING_MESSAGE);
				return;
			} else if (sw.getChildCount() == 1) {
				count++;
				sw = (SPMutableTreeNode)sw.getChildAt(0);
			} else {
				sw = null;
			}
		}
		
		if (count == 1) {
			JOptionPane.showMessageDialog(this, "There must be more than one switch to perform a ring load. Please use one of the other load methods.", "Ring Load", JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		Utils.debug("Asking the user whether he is ready to perform a ring load");
		if (JOptionPane.showConfirmDialog(this,
			                              "Cancelling a ring load will likely require significant reconfiguration to repair your ring.\n" +
				                          "Are you sure you are ready to perform a ring load?",
					                      "Confirm Ring Load",
						                  JOptionPane.YES_NO_OPTION,
							              JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) {
			Utils.debug("User is not ready to perform a ring load.");
			return;
		}
		
		Utils.debug("Building the dependency graph");
		
		DependencyGraph<SwitchPage> graph = new DependencyGraph<SwitchPage>();
		SPMutableTreeNode rootNode = (SPMutableTreeNode)nodeSwitches.getChildAt(0);
		SwitchPage breakNode; // Break the connection just past this node
		if (project.getParallel()) {
			Utils.debug("  In parallel");
			
			// Split the switches halfway down so a parallel load will go at maximum speed
			SPMutableTreeNode middleSwitch = rootNode;
			for (int i = 0; i < count / 2; i++) {
				middleSwitch = (SPMutableTreeNode)middleSwitch.getChildAt(0);
			}
			
			breakNode = (SwitchPage)middleSwitch.getUserObject();
			
			Utils.debug("    Middle switch is " + breakNode.getConfig().getAddress());
			
			SPMutableTreeNode turnwise = (SPMutableTreeNode)middleSwitch.getChildAt(0);
			SPMutableTreeNode widdershins = (SPMutableTreeNode)middleSwitch.getParent();
			
			// The break node needs to be absolutely last, because we need to re-connect it and choose another break
			//   node before loading it
			if (project.getBottomToTop()) {
				// Break node will be after the root node
				graph.addDependency((SwitchPage)rootNode.getUserObject(), breakNode);
			} else {
				// Break node will be after the bottom two nodes (the ones that border it)
				graph.addDependency((SwitchPage)turnwise.getUserObject(), breakNode);
				graph.addDependency((SwitchPage)widdershins.getUserObject(), breakNode);
			}
			
			while (turnwise != rootNode || widdershins != rootNode) {
				SPMutableTreeNode turnwiseNext;
				SPMutableTreeNode widdershinsNext;
				
				if (turnwise != rootNode) {
					if (turnwise.getChildCount() == 0) {
						turnwiseNext = rootNode;
					} else {
						turnwiseNext = (SPMutableTreeNode)turnwise.getChildAt(0);
					}
				} else {
					turnwiseNext = null;
				}
				
				if (widdershins != rootNode) {
					widdershinsNext = (SPMutableTreeNode)widdershins.getParent();
				} else {
					widdershinsNext = null;
				}
				
				// Although this may produce a number of identical dependencies, DependencyGraph stores them in a Set, so it doesn't matter
				if (project.getBottomToTop()) {
					// Each step closer to the root depends on the switch further away
					if (turnwiseNext != null) {
						graph.addDependency((SwitchPage)turnwise.getUserObject(), (SwitchPage)turnwiseNext.getUserObject());
					}
					if (widdershinsNext != null) {
						graph.addDependency((SwitchPage)widdershins.getUserObject(), (SwitchPage)widdershinsNext.getUserObject());
					}
				} else {
					// Each step closer to the root ('next' node) guards the switch further away
					if (turnwiseNext != null) {
						graph.addDependency((SwitchPage)turnwiseNext.getUserObject(), (SwitchPage)turnwise.getUserObject());
					}
					if (widdershinsNext != null) {
						graph.addDependency((SwitchPage)widdershinsNext.getUserObject(), (SwitchPage)widdershins.getUserObject());
					}
				}
				
				if (turnwiseNext != null) {
					turnwise = turnwiseNext;
				}
				if (widdershinsNext != null) {
					widdershins = widdershinsNext;
				}
			}
		} else { // Sequential load
			Utils.debug("  Sequentially");
			
			// No point in splitting the list in half, since it won't go any faster
			
			if (project.getBottomToTop()) {
				// Each switch depends on the one below, but the bottom switch depends on nodeSwitches
				// The last switch is the break switch, though, so it needs to be absolutely last
				SPMutableTreeNode before = rootNode;
				SPMutableTreeNode after = (SPMutableTreeNode)before.getChildAt(0);
				
				while (after.getChildCount() > 0) { // Need to save the last one, as it depends on everything
					graph.addDependency((SwitchPage)after.getUserObject(), (SwitchPage)before.getUserObject());
					before = after;
					
					if (after.getChildCount() > 0) {
						after = (SPMutableTreeNode)after.getChildAt(0);
					} else {
						after = null;
					}
				}
				
				breakNode = (SwitchPage)after.getUserObject();
				graph.addDependency((SwitchPage)rootNode.getUserObject(), breakNode);
			} else { // Top to bottom
				// Each switch depends on the one above
				// Break the connection after the last switch (no special testing needed, as it will depend on everything else)
				SPMutableTreeNode before = (SPMutableTreeNode)nodeSwitches.getChildAt(0);
				SPMutableTreeNode after = (SPMutableTreeNode)before.getChildAt(0);
				
				while (after != null) {
					graph.addDependency((SwitchPage)before.getUserObject(), (SwitchPage)after.getUserObject());
					before = after;
					
					if (after.getChildCount() > 0) {
						after = (SPMutableTreeNode)after.getChildAt(0);
					} else {
						after = null;
					}
				}
				
				breakNode = (SwitchPage)before.getUserObject();
			}
		}
		
		if (Utils.DEBUG) {
			graph.print(Utils.DEBUG_TEXT_STREAM);
		}
		
		Utils.debug("Building the ActionWindow");
		final SwitchPage breakPage = breakNode;
		final SwitchPage rootPage = (SwitchPage)rootNode.getUserObject();
		ActionWindow<SwitchPage> aw = new ActionWindow<SwitchPage>(graph, null, "Loading Firmware", new ActionWindow.ActionTaker() {
			private boolean broken = false; // Set to true once we have broken the ring
			private Object l_broken = new Object();
			
			private int brokenPort;
			private boolean failed = false; // Set to true if something required for everything fails, so we can stop everything
			
			public void run(Object arg, ActionWindow<?>.ActionItem ui) {
				// Nice thing with this is that the first will do it, and any others will need to wait for it to be done
				//   before forging ahead (kind of like a loose barrier -- can only pass once it has happened)
				ui.updateColor(C_WAITING);
				ui.updateState("Waiting for the ring to break");
				
				synchronized (l_broken) {
					if (!broken) {
						Utils.debug("Breaking the ring at switch " + breakPage.getConfig().getAddress());
						
						// Need to break the ring
						ui.updateColor(C_INPROGRESS);
						ui.updateState("Breaking the ring");
						try {
							brokenPort = breakPage.getFirmwareLoader().breakRing();
							Utils.debug("  Broken on port " + brokenPort);
						} catch (Exception e) {
							Utils.debug(e);
							
							ui.updateColor(C_ERROR);
							ui.updateState("Error: " + e.getMessage());
							failed = true;
							return;
						}
						
						broken = true;
					}
				}
				
				if (failed) {
					ui.updateColor(C_ERROR);
					ui.updateState("An error occured");
					return;
				}
				
				if (arg == breakPage) {
					Utils.debug("We have reached the break switch, time to change the break point");
					
					ui.updateColor(C_INPROGRESS);
					ui.updateState("Reconnecting the ring");
					// Reconnect the ring, break it again at the root
					
					try {
						Utils.debug("  Reconnecting " + breakPage.getConfig().getAddress() + " port " + brokenPort);
						breakPage.getFirmwareLoader().connectRing(brokenPort);
						
						ui.updateState("Breaking the ring at the root");
						Utils.debug("  Breaking at " + rootPage.getConfig().getAddress());
						brokenPort = rootPage.getFirmwareLoader().breakRing();
						Utils.debug("    Broken at " + brokenPort);
					} catch (Exception e) {
						Utils.debug(e);
						
						ui.updateColor(C_ERROR);
						ui.updateState("Error: " + e.getMessage());
						failed = true;
						return;
					}
				}
				
				SwitchPage sp = (SwitchPage)arg;
				
				Utils.debug("Loading switch " + sp.getConfig().getAddress());
				sp.load(ui);
				
				if (arg == breakPage) {
					// Reconnect the ring at the root
					ui.updateColor(C_INPROGRESS);
					ui.updateState("Reconnecting the ring at the root");
					
					try {
						Utils.debug("Reconnecting the ring at " + rootPage.getConfig().getAddress() + " port " + brokenPort);
						
						rootPage.getFirmwareLoader().connectRing(brokenPort);
						
						ui.updateState("Complete");
						ui.updateColor(C_COMPLETE);
					} catch (Exception e) {
						Utils.debug(e);
						
						ui.updateColor(C_ERROR);
						ui.updateState("Failed to reconnect the root");
					}
				}
			}
			
			public void cancel(Object arg, ActionWindow<?>.ActionItem ui) {
				SwitchPage sp = (SwitchPage)arg;
				sp.cancel(ui);
			}
		});
		
		Utils.debug("Showing the ActionWindow");
		aw.showDialog();
	}
	
	public void openProject(String projFile) {
		// This is the code for mnuFileOpen, without the JFileChooser Stuff
		if (detectConfigChange()) {
			int result = JOptionPane.showConfirmDialog(this, "Settings have changed, keep the changes?", "Keep Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (result == JOptionPane.YES_OPTION) {
				if (!saveOptions()) {
					return;
				}
			} else if (result == JOptionPane.NO_OPTION) {
				revertOptions();
			} else { // Must be cancel
				return;
			}
		}
		
		if (changed) {
			int result = JOptionPane.showConfirmDialog(this, "Save project?", "Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
			
			if (result == JOptionPane.YES_OPTION) {
				mnuFileSave.doClick();
				if (changed) {
					return; // Must have had an error
				}
			} else if (result == JOptionPane.CANCEL_OPTION) {
				return;
			}
		}
		
		new FirmwareLoaderGUI2(projFile, false);
		
		setVisible(false);
		dispose();
	}
	
	// Deal with the recent projects list
	private void addRecentProject(File file) {
		RecentProject found = null;
		for (RecentProject prj : recentProjects) {
			if (prj.file.equals(file)) {
				found = prj;
				break;
			}
		}
		
		if (found != null) {
			// Already in the list, move to the front
			recentProjects.remove(found);
			recentProjects.add(0, found);
		} else {
			// Not in there, create a new one and put it in
			JMenuItem item = new JMenuItem("1) " + file.getName());
			item.setMnemonic(KeyEvent.VK_1);
			item.addActionListener(this);
			
			found = new RecentProject(item, file);
			
			recentProjects.add(0, found);
		}
		
		// Remove items beyond #9
		while (recentProjects.size() > 9) {
			recentProjects.remove(recentProjects.size() - 1);
		}
		
		// Go through the vector, updating the names and mnemonics
		int pos = 1;
		for (RecentProject prj : recentProjects) {
			prj.menuItem.setText(pos + ") " + prj.file.getName());
			prj.menuItem.setMnemonic(KeyEvent.VK_0 + pos);
			
			pos++;
		}
		
		// Save
		saveRecentProjects();
		
		// Re-create our menus
		setupMenus();
	}
	
	private void saveRecentProjects() {
		// Output the file
		Tag mainTag = new Tag("recentprojects");
		for (RecentProject prj : recentProjects) {
			// Make sure the file exists
			if (!prj.file.isFile()) {
				continue;
			}
			
			Tag projTag = new Tag("project");
			projTag.addContent(prj.file.getAbsolutePath());
			mainTag.addContent(projTag);
		}
		
		try {
			FileOutputStream out = new FileOutputStream(System.getProperty("user.home") + File.separator + "fpj.recent");
			mainTag.toStream(out);
			out.close();
		} catch (IOException ioe) {
			Utils.debug(ioe);
		}
	}
	
	private void loadRecentProjects() {
		// Try to load the list of recent projects; if we fail, don't add anything to the menu
		recentProjects = new Vector<RecentProject>();
		
		try {
			Vector<String> files = new Vector<String>();
			
			Tag recent = xmlParser.parseXML(System.getProperty("user.home") + File.separator + "fpj.recent", false, false);
			
			// The exception will be caught and discarded, and there will be no recent projects
			if (!recent.getName().equals("recentprojects")) {
				throw new IOException("Not the right type of file");
			}
			
			for (Content c : recent) {
				if (!c.isTag()) {
					continue;
				}
				
				Tag tag = c.getTag();
				if (!tag.getName().equals("project")) {
					continue;
				}
				
				files.add(tag.getStringContents());
			}
			
			int count = 0;
			for (String fileName : files) {
				File f = new File(fileName);
				if (f.isFile()) {
					count++;
					
					if (count > 9) {
						break;
					}

					recentProjects.add(new RecentProject(null, f));
				}
			}
		} catch (IOException ioe) {
			Utils.debug(ioe);
		}
	}
	
	// Open/Save the application options file
	private void readAppOpts() {
		File optFile = new File(APP_OPTIONS_FILE);
		if (!optFile.isFile()) {
			return; // Silently ignore, it will be created if the user applies options
		}
		
		try {
			Tag main = xmlParser.parseXML(APP_OPTIONS_FILE, true, false);
			
			if (!main.getName().equals("firmloadopts")) {
				throw new IOException("Not a firmware loader option file");
			}
			
			double formatVersion = main.getDoubleAttribute("version");
			
			// Check supported versions
			if (!(formatVersion >= 1.0 &&
				  formatVersion <= 1.1)) {
				
				throw new IOException("Unsupported file format version: " + formatVersion);
			}
			
			// Password-protect files?
			bProtectFiles = main.getBooleanContents("protectfiles");
			
			// Auto-load project file? Different between 1.0 and >=1.1
			if (formatVersion == 1.0) {
				// autoload is a boolean, differentiating between AL_None and AL_Last
				if (main.getBooleanContents("autoload")) {
					iAutoload = AL_LAST;
				} else {
					iAutoload = AL_NONE;
				}
				
				sAutoload = ""; // This didn't exist
				bTreatAsNew = false; // Neither did this
			} else {
				// autoload is an integer, one of AL_None, AL_Last, or AL_File
				iAutoload = main.getIntContents("autoload");
				sAutoload = main.getStringContents("autoproj");
				bTreatAsNew = main.getBooleanContents("treatasnew");
			}
			
			// Use external TFTP server?
			bExternalTFTP = main.getBooleanContents("externaltftp");
			
			sExternalTFTPDir = main.getStringContents("externaltftpdir", "");
			
			revertAppOptions(); // Set the fields
		} catch (IOException ioe) {
			Utils.debug(ioe);
			JOptionPane.showMessageDialog(this, "Application option file (" + APP_OPTIONS_FILE + ") is corrupt and cannot be loaded. Please check your application options.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void writeAppOpts() throws IOException {
		Tag main = new Tag("firmloadopts");
		main.setAttribute("version", OPT_VERSION);
		
		// Password-protect files?
		{
			Tag protect = new Tag("protectfiles");
			protect.addContent("" + bProtectFiles);
			
			main.addContent(protect);
		}
		
		// Auto-load project file?
		{
			Tag autoLoad = new Tag("autoload");
			autoLoad.addContent("" + iAutoload);
			main.addContent(autoLoad);
			
			Tag autoProj = new Tag("autoproj");
			autoProj.addContent(sAutoload);
			main.addContent(autoProj);
		}
		
		// Use external TFTP server?
		{
			Tag externalTFTP = new Tag("externaltftp");
			externalTFTP.addContent("" + bExternalTFTP);
			main.addContent(externalTFTP);
			
			Tag externalTFTPDir = new Tag("externaltftpdir");
			externalTFTPDir.addContent("" + sExternalTFTPDir);
			main.addContent(externalTFTPDir);
			
			Tag treatAsNew = new Tag("treatasnew");
			treatAsNew.addContent("" + bTreatAsNew);
			main.addContent(treatAsNew);
		}
		
		// Output
		FileOutputStream out = new FileOutputStream(APP_OPTIONS_FILE);
		main.toStream(out);
	}
	
	// General utility functions
	public void resetTitle() {
		String title;
		
		if (project.getFileName() == null) {
			title = "(Untitled)";
		} else {
			File temp = new File(project.getFileName());
			title = temp.getName();
		}
		
		if (changed) {
			title += "*";
		}
		
		if (project.getBundle() != null) {
			title += " - FW v. " + project.getBundle().getFWVersion();
		}
		
		setTitle(title + " - " + WINDOW_TITLE);
	}
	
	public char[] askForPassword(String msg) {
		PasswordDialog pwd = new PasswordDialog(msg, "Password", false, false);
		PasswordDialog.UserPass userPass = pwd.showDialog();
		
		if (userPass == null) {
			return null;
		}
		
		char[] ans = new char[userPass.passwd.length];
		System.arraycopy(userPass.passwd, 0, ans, 0, ans.length);
		
		Arrays.fill(userPass.passwd, '\0');
		
		return ans;
	}
	
	public void showWarning(String msg) {
		JOptionPane.showMessageDialog(this, msg, "Warning", JOptionPane.WARNING_MESSAGE);
	}
	
	private void createSwitches(SPMutableTreeNode parent, Project.Switch swParent) {
		for (int i = 0; i < swParent.getChildCount(); i++) {
			Project.Switch sw = swParent.getChild(i);
			SwitchPage newPage = new SwitchPage(this, sw);
			SPMutableTreeNode newNode = new SPMutableTreeNode(newPage);
			
			treeModel.insertNodeInto(newNode, parent, parent.getChildCount());
			tree.makeVisible(new TreePath(newNode.getPath()));
			
			createSwitches(newNode, sw);
		}
	}
}

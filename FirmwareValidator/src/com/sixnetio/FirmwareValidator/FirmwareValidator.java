/*
 * FirmwareValidator.java
 *
 * Main entry point for the FirmwareValidator program, which tests firmware
 * images/bundles for what is different between them. If there are unexpected
 * changes, those changes are displayed and the validation fails.
 *
 * Jonathan Pearson
 * December 2, 2008
 *
 */

package com.sixnetio.FirmwareValidator;

import java.io.*;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;

import com.sixnetio.fs.generic.*;
import com.sixnetio.fs.jffs2.JFFS2;
import com.sixnetio.fs.tar.GZippedTarball;
import com.sixnetio.fs.tar.Tarball;
import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.io.Magic;
import com.sixnetio.util.MultiIOException;
import com.sixnetio.util.Utils;

public class FirmwareValidator
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public static final String PROG_VERSION = "0.5.0x";
	
	// Sometimes it is useful to pair a couple of differences
	private static class DiffPair
	{
		public final Difference diff1;
		public final Difference diff2;
		
		public DiffPair(Difference diff1, Difference diff2)
		{
			this.diff1 = diff1;
			this.diff2 = diff2;
		}
	}
	
	// To simplify the Difference class below, these subclasses represent
	// specific types of differences
	
	/**
	 * Indicates that the name of the top-level file has changed.
	 */
	private static class FileNameDifference
		extends Difference
	{
		/** Name of the old image/bundle file (informational only). */
		public final String oldFileName;
		
		/** Name of the new image/bundle file. */
		public final String newFileName;
		
		/**
		 * Construct a new FileNameDifference during inspection.
		 * 
		 * @param oldName The old file name of the bundle.
		 * @param newName The new file name of the bundle.
		 */
		public FileNameDifference(String oldName, String newName)
		{
			super("", "(Changed File Name)");
			
			this.oldFileName = oldName;
			this.newFileName = newName;
		}
		
		/**
		 * Construct a new FileNameDifference from saved data.
		 * 
		 * @param tag The tag describing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public FileNameDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			this.oldFileName = tag.getStringContents("oldFileName", null);
			if (this.oldFileName == null) {
				throw new IOException("Missing oldFileName");
			}
			
			this.newFileName = tag.getStringContents("newFileName", null);
			if (this.newFileName == null) {
				throw new IOException("Missing newFileName");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldFileName = new Tag("oldFileName", oldFileName);
			tagDiff.addContent(tagOldFileName);
			
			Tag tagNewFileName = new Tag("newFileName", newFileName);
			tagDiff.addContent(tagNewFileName);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			FileNameDifference fnd = (FileNameDifference)d;
			
			// Old file name is informational only
			
			if ( ! this.newFileName.equals(fnd.newFileName)) {
				return false;
			}
			
			return true;
		}
	}
	
	/**
	 * Indicates that the type of the image has changed (for example, JFFS2 to
	 * Tarball).
	 * 
	 * @author Jonathan Pearson
	 */
	private static class ImageTypeDifference
		extends Difference
	{
		/** Simple name of the class used to parse the old image. */
		public final String oldParser;
		
		/** Simple name of the class used to parse the new image. */
		public final String newParser;
		
		/**
		 * Construct a new ImageTypeDifference during inspection.
		 * 
		 * @param oldParser The old parser class's simple name.
		 * @param newParser The new parser class's simple name.
		 */
		public ImageTypeDifference(String oldParser,
		                           String newParser)
		{
			
			super("", "(Changed Image Type)");
			
			this.oldParser = oldParser;
			this.newParser = newParser;
		}
		
		/**
		 * Construct a new ImageTypeDifference from saved data.
		 * 
		 * @param tag The tag containing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public ImageTypeDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			this.oldParser = tag.getStringContents("oldParser",
			                                       null);
			if (this.oldParser == null) {
				throw new IOException("Missing oldParser");
			}
			
			this.newParser = tag.getStringContents("newParser",
			                                       null);
			if (this.newParser == null) {
				throw new IOException("Missing newParser");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldParser = new Tag("oldParser",
			                           oldParser);
			tagDiff.addContent(tagOldParser);
			
			Tag tagNewParser = new Tag("newParser",
			                           newParser);
			tagDiff.addContent(tagNewParser);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			ImageTypeDifference itd = (ImageTypeDifference)d;
			
			// Old parser is informational only
			
			if ( ! this.newParser.equals(itd.newParser)) {
				return false;
			}
			
			return true;
		}
	}
	
	/**
	 * Indicates that the vendor string in a bundle has changed.
	 * 
	 * @author Jonathan Pearson
	 */
	private static class BundleVendorDifference
		extends Difference
	{
		/** Bundle vendor string in the old bundle (informational only). */
		public final String oldBundleVendor;
		
		/** Bundle vendor string in the new bundle. */
		public final String newBundleVendor;
		
		/**
		 * Construct a new BundleVendorDifference during inspection.
		 * 
		 * @param oldBundleVendor The old bundle vendor string.
		 * @param newBundleVendor The new bundle vendor string.
		 */
		public BundleVendorDifference(String oldBundleVendor,
		                              String newBundleVendor)
		{
			
			super("", "(Changed Vendor)");
			
			this.oldBundleVendor = oldBundleVendor;
			this.newBundleVendor = newBundleVendor;
		}
		
		/**
		 * Construct a new BundleVendorDifference from saved data.
		 * 
		 * @param tag The tag containing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public BundleVendorDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			this.oldBundleVendor = tag.getStringContents("oldBundleVendor",
			                                             null);
			if (this.oldBundleVendor == null) {
				throw new IOException("Missing oldBundleVendor");
			}
			
			this.newBundleVendor = tag.getStringContents("newBundleVendor",
			                                             null);
			if (this.newBundleVendor == null) {
				throw new IOException("Missing newBundleVendor");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldBundleVendor = new Tag("oldBundleVendor",
			                                 oldBundleVendor);
			tagDiff.addContent(tagOldBundleVendor);
			
			Tag tagNewBundleVendor = new Tag("newBundleVendor",
			                                 newBundleVendor);
			tagDiff.addContent(tagNewBundleVendor);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			BundleVendorDifference bvd = (BundleVendorDifference)d;
			
			// Old bundle vendor is informational only
			
			if ( ! this.newBundleVendor.equals(bvd.newBundleVendor)) {
				return false;
			}
			
			return true;
		}
	}
	
	/**
	 * Indicates that the version number of a bundle has changed.
	 * 
	 * @author Jonathan Pearson
	 */
	private static class BundleVersionDifference
		extends Difference
	{
		/**
		 * Firmware version in the old bundle (informational only).
		 */
		public final String oldBundleVersion;
		
		/**
		 * Firmware version in the new bundle.
		 */
		public final String newBundleVersion;
		
		/**
		 * Construct a new BundleVersionDifference during inspection.
		 * 
		 * @param oldBundleVersion The old bundle version string.
		 * @param newBundleVersion The new bundle version string.
		 */
		public BundleVersionDifference(String oldBundleVersion,
		                               String newBundleVersion)
		{
			
			super("", "(Changed Version)");
			
			this.oldBundleVersion = oldBundleVersion;
			this.newBundleVersion = newBundleVersion;
		}
		
		/**
		 * Construct a new BundleVersionDifference from loaded data.
		 * 
		 * @param tag The tag describing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public BundleVersionDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			this.oldBundleVersion = tag.getStringContents("oldBundleVersion",
			                                              null);
			if (this.oldBundleVersion == null) {
				throw new IOException("Missing oldBundleVersion");
			}
			
			this.newBundleVersion = tag.getStringContents("newBundleVersion",
			                                              null);
			if (this.newBundleVersion == null) {
				throw new IOException("Missing newBundleVersion");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldBundleVersion = new Tag("oldBundleVersion",
			                                  oldBundleVersion);
			tagDiff.addContent(tagOldBundleVersion);
			
			Tag tagNewBundleVersion = new Tag("newBundleVersion",
			                                  newBundleVersion);
			tagDiff.addContent(tagNewBundleVersion);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			BundleVersionDifference bvd = (BundleVersionDifference)d;
			
			// Old bundle version is informational only
			
			if ( ! this.newBundleVersion.equals(bvd.newBundleVersion)) {
				return false;
			}
			
			return true;
		}
	}
	
	/**
	 * Indicates that the firmware type of a bundle has changed.
	 * 
	 * @author Jonathan Pearson
	 */
	private static class BundleTypeDifference
		extends Difference
	{
		/**
		 * Firmware type in the old bundle (informational only).
		 */
		public final String oldBundleType;
		
		/**
		 * Firmware type in the new bundle.
		 */
		public final String newBundleType;
		
		/**
		 * Construct a new BundleTypeDifference during inspection.
		 * 
		 * @param oldBundleType The old bundle type string.
		 * @param newBundleType The new bundle type string.
		 */
		public BundleTypeDifference(String oldBundleType,
		                            String newBundleType)
		{
			
			super("", "(Changed Version)");
			
			this.oldBundleType = oldBundleType;
			this.newBundleType = newBundleType;
		}
		
		/**
		 * Construct a new BundleTypeDifference from loaded data.
		 * 
		 * @param tag The tag describing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public BundleTypeDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			this.oldBundleType = tag.getStringContents("oldBundleType",
			                                           null);
			if (this.oldBundleType == null) {
				throw new IOException("Missing oldBundleType");
			}
			
			this.newBundleType = tag.getStringContents("newBundleType",
			                                           null);
			if (this.newBundleType == null) {
				throw new IOException("Missing newBundleType");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldBundleType = new Tag("oldBundleType", oldBundleType);
			tagDiff.addContent(tagOldBundleType);
			
			Tag tagNewBundleType = new Tag("newBundleType", newBundleType);
			tagDiff.addContent(tagNewBundleType);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			BundleTypeDifference btd = (BundleTypeDifference)d;
			
			// Old bundle type is informational only
			
			if ( ! this.newBundleType.equals(btd.newBundleType)) {
				return false;
			}
			
			return true;
		}
	}
	
	/**
	 * Indicates that the endian-ness of a JFFS2 image has changed.
	 * 
	 * @author Jonathan Pearson
	 */
	private static class JFFS2EndianDifference
		extends Difference
	{
		/**
		 * True if the old image is little-endian.
		 */
		public final boolean oldLittleEndian;
		
		/**
		 * True if the new image is little-endian.
		 */
		public final boolean newLittleEndian;
		
		/**
		 * Construct a new EndianDifference during inspection.
		 * 
		 * @param oldLittleEndian True if the old image is little-endian.
		 * @param newLittleEndian True if the new image is little-endian.
		 */
		public JFFS2EndianDifference(boolean oldLittleEndian,
		                        boolean newLittleEndian)
		{
			
			super("", "(Changed Endianness)");
			
			this.oldLittleEndian = oldLittleEndian;
			this.newLittleEndian = newLittleEndian;
		}
		
		/**
		 * Construct a new EndianDifference from loaded data.
		 * 
		 * @param tag The tag describing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public JFFS2EndianDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			if (tag.getStringContents("oldLittleEndian", null) == null) {
				throw new IOException("Missing oldLittleEndian");
			}
			this.oldLittleEndian = tag.getBooleanContents("oldLittleEndian",
			                                              false);
			
			if (tag.getStringContents("newLittleEndian", null) == null) {
				throw new IOException("Missing newLittleEndian");
			}
			this.newLittleEndian= tag.getBooleanContents("newLittleEndian",
			                                             false);
			
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldLittleEndian = new Tag("oldLittleEndian",
			                                 "" + oldLittleEndian);
			tagDiff.addContent(tagOldLittleEndian);
			
			Tag tagNewLittleEndian = new Tag("newLittleEndian",
			                                 "" + newLittleEndian);
			tagDiff.addContent(tagNewLittleEndian);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			JFFS2EndianDifference ed = (JFFS2EndianDifference)d;
			
			// Old endian-ness is informational only
			
			if (this.newLittleEndian != ed.newLittleEndian) {
				return false;
			}
			
			return true;
		}
	}
	
	/**
	 * Indicates that an object exists on one side but not the other.
	 * 
	 * @author Jonathan Pearson
	 */
	private static class MissingObjectDifference
		extends Difference
	{
		/**
		 * This will be <tt>true</tt> if the file was added going into the new
		 * version. It will be <tt>false</tt> if the file was removed.
		 */
		public final boolean added;
		
		/**
		 * Construct a new MissingObjectDifference during inspection.
		 * 
		 * @param container The path to the container of this object.
		 * @param existingObject The object, as it appears in one of the images.
		 * @param added True if the object is present only in the new image,
		 *   false if present only in the old image.
		 */
		public MissingObjectDifference(String container,
		                               FSObject existingObject, boolean added)
		{
			
			super(container, existingObject.getPath());
			
			this.added = added;
		}
		
		/**
		 * Construct a new MissingObjectDifference from saved data.
		 * 
		 * @param tag The tag describing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public MissingObjectDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			if (tag.getChild("absent") != null) {
				this.added = false;
			}
			else if (tag.getChild("new") != null) {
				this.added = true;
			}
			else {
				throw new IOException("Missing object difference must be" +
				                      " either 'absent' or 'new'");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			if ( ! added) {
				// Removed from the previous version
				tagDiff.addContent(new Tag("absent"));
			}
			else {
				// Added since the previous version
				tagDiff.addContent(new Tag("new"));
			}
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			MissingObjectDifference mod = (MissingObjectDifference)d;
			if (this.added != mod.added) {
				return false;
			}
			
			return true;
		}
		
		@Override
		public boolean similarChange(Difference d)
		{
			// We actually do need to override this, otherwise, for example, a
			// file that should have been deleted but was not will show up as
			// a similar change instead of a mismatch
			return sameChange(d);
		}
	}
	
	
	/**
	 * Indicates that the type of the file (link, regular file, directory,
	 * device, ...) changed.
	 * 
	 * @author Jonathan Pearson
	 */
	private static class TypeMismatchDifference
		extends Difference
	{
		/** The old type of the object (informational only). */
		public final String oldObjectType;
		
		/** The new type of the object. */
		public final String newObjectType;
		
		/**
		 * Construct a new TypeMismatchDifference during inspection.
		 * 
		 * @param container The path to the container of this object.
		 * @param oldFSObj The object from the old image.
		 * @param newFSObj The object from the new image.
		 */
		public TypeMismatchDifference(String container, FSObject oldFSObj,
		                              FSObject newFSObj)
		{
			
			super(container, newFSObj.getPath());
			
			this.oldObjectType = oldFSObj.getTypeAsString();
			this.newObjectType = newFSObj.getTypeAsString();
		}
		
		/**
		 * Construct a new TypeMismatchDifference from saved data.
		 * 
		 * @param tag The tag describing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public TypeMismatchDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			this.oldObjectType = tag.getStringContents("oldObjectType", null);
			if (this.oldObjectType == null) {
				throw new IOException("Missing oldObjectType");
			}
			
			this.newObjectType = tag.getStringContents("newObjectType", null);
			if (this.newObjectType == null) {
				throw new IOException("Missing newObjectType");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldObjectType = new Tag("oldObjectType", oldObjectType);
			tagDiff.addContent(tagOldObjectType);
			
			Tag tagNewObjectType = new Tag("newObjectType", newObjectType);
			tagDiff.addContent(tagNewObjectType);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			TypeMismatchDifference tmd = (TypeMismatchDifference)d;
			
			// Old object type is informational only
			
			if ( ! this.newObjectType.equals(tmd.newObjectType)) {
				return false;
			}
			
			return true;
		}
	}
	
	
	/**
	 * Indicates that the file contents changed.
	 * 
	 * @author Jonathan Pearson
	 */
	private static class FileDifference
		extends Difference
	{
		/** The MD5 checksum of the file in the old version. */
		public final String oldMD5;
		
		/** The MD5 checksum of the file in the new version. */
		public final String newMD5;
		
		/**
		 * Construct a new FileDifference during inspection.
		 * 
		 * @param container The path to the container of this object.
		 * @param file The file, as it appears in either image.
		 * @param oldMD5 The MD5 checksum of the file in the old image.
		 * @param newMD5 The MD5 checksum of the file in the new image.
		 */
		public FileDifference(String container, FSObject file,
		                      String oldMD5, String newMD5)
		{
			
			super(container, file.getPath());
			
			this.oldMD5 = oldMD5;
			this.newMD5 = newMD5;
		}
		
		/**
		 * Construct a new FileDifference from saved data.
		 * 
		 * @param tag The tag describing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public FileDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			this.oldMD5 = tag.getStringContents("oldMD5", null);
			if (this.oldMD5 == null) {
				throw new IOException("Missing oldMD5");
			}
			
			this.newMD5 = tag.getStringContents("newMD5", null);
			if (this.newMD5 == null) {
				throw new IOException("Missing newMD5");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldMD5 = new Tag("oldMD5", oldMD5);
			tagDiff.addContent(tagOldMD5);
			
			Tag tagNewMD5 = new Tag("newMD5", newMD5);
			tagDiff.addContent(tagNewMD5);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			FileDifference fd = (FileDifference)d;
			
			// Old MD5 is informational only
			
			if ( ! this.newMD5.equals(fd.newMD5)) {
				return false;
			}
			
			return true;
		}
	}
	
	
	/**
	 * Indicates that the major and/or minor device numbers have changed.
	 * 
	 * @author Jonathan Pearson
	 */
	private static class DeviceDifference
		extends Difference
	{
		/** The major device number in the old version (informational only). */
		public final int oldDevMajor;
		
		/** The minor device number in the old version (informational only). */
		public final int oldDevMinor;
		
		/** The major device number in the new version. */
		public final int newDevMajor;
		
		/** The minor device number in the new version. */
		public final int newDevMinor;
		
		/**
		 * Construct a new DeviceDifference during inspection.
		 * 
		 * @param container The path to the container of this object.
		 * @param oldDevice The device node from the old image.
		 * @param newDevice The device node from the new image.
		 */
		public DeviceDifference(String container, FSObject oldDevice,
		                        FSObject newDevice)
		{
			
			super(container, newDevice.getPath());
			
			this.oldDevMajor = ((Device)oldDevice).getMajor();
			this.oldDevMinor = ((Device)oldDevice).getMinor();
			
			this.newDevMajor = ((Device)newDevice).getMajor();
			this.newDevMinor = ((Device)newDevice).getMinor();
		}
		
		/**
		 * Construct a new DeviceDifference from saved data.
		 * 
		 * @param tag The tag describing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public DeviceDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			this.oldDevMajor = tag.getIntContents("oldDevMajor", -1);
			if (this.oldDevMajor == -1) {
				throw new IOException("Missing oldDevMajor");
			}
			
			this.oldDevMinor = tag.getIntContents("oldDevMinor", -1);
			if (this.oldDevMinor == -1) {
				throw new IOException("Missing oldDevMinor");
			}
			
			this.newDevMajor = tag.getIntContents("newDevMajor", -1);
			if (this.newDevMajor == -1) {
				throw new IOException("Missing newDevMajor");
			}
			
			this.newDevMinor = tag.getIntContents("newDevMinor", -1);
			if (this.newDevMinor == -1) {
				throw new IOException("Missing newDevMinor");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldDevMajor = new Tag("oldDevMajor", "" + oldDevMajor);
			tagDiff.addContent(tagOldDevMajor);
			
			Tag tagOldDevMinor = new Tag("oldDevMinor", "" + oldDevMinor);
			tagDiff.addContent(tagOldDevMinor);
			
			Tag tagNewDevMajor = new Tag("newDevMajor", "" + newDevMajor);
			tagDiff.addContent(tagNewDevMajor);
			
			Tag tagNewDevMinor = new Tag("newDevMinor", "" + newDevMinor);
			tagDiff.addContent(tagNewDevMinor);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			DeviceDifference dd = (DeviceDifference)d;
			
			// Old device major/minor are informational only
			
			if (this.newDevMajor != dd.newDevMajor) {
				return false;
			}
			
			if (this.newDevMinor != dd.newDevMinor) {
				return false;
			}
			
			return true;
		}
	}
	
	
	/**
	 * Indicates that the target of a link has changed.
	 * 
	 * @author Jonathan Pearson
	 */
	private static class LinkDifference
		extends Difference
	{
		/** The target of the link in the old version. */
		public final String oldLinkTarget;
		
		/** The target of the link in the new version. */
		public final String newLinkTarget;
		
		/**
		 * Construct a new LinkDifference during inspection.
		 * 
		 * @param container The path to the container of this object.
		 * @param oldLink The link from the old image.
		 * @param newLink The link from the new image.
		 */
		public LinkDifference(String container, FSObject oldLink,
		                      FSObject newLink)
		{
			
			super(container, newLink.getPath());
			
			this.oldLinkTarget = ((Symlink)oldLink).getLinkName();
			this.newLinkTarget = ((Symlink)newLink).getLinkName();
		}
		
		/**
		 * Construct a new LinkDifference from saved data.
		 * 
		 * @param tag The tag describing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public LinkDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			this.oldLinkTarget = tag.getStringContents("oldLinkTarget", null);
			if (this.oldLinkTarget == null) {
				throw new IOException("Missing oldLinkTarget");
			}
			
			this.newLinkTarget = tag.getStringContents("newLinkTarget", null);
			if (this.newLinkTarget == null) {
				throw new IOException("Missing newLinkTarget");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldLinkTarget = new Tag("oldLinkTarget", oldLinkTarget);
			tagDiff.addContent(tagOldLinkTarget);
			
			Tag tagNewLinkTarget = new Tag("newLinkTarget", newLinkTarget);
			tagDiff.addContent(tagNewLinkTarget);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			LinkDifference ld = (LinkDifference)d;
			
			// Old link target is informational only
			
			if ( ! this.newLinkTarget.equals(ld.newLinkTarget)) {
				return false;
			}
			
			return true;
		}
	}
	
	
	/**
	 * Indicates that the ownership/permissions of an object have changed.
	 * 
	 * @author Jonathan Pearson
	 */
	private static class PermissionDifference
		extends Difference
	{
		/** User ID in the old version (informational only). */
		public final int oldUID;
		
		/** Group ID in the old version (informational only). */
		public final int oldGID;
		
		/** Permission mode in the old version (informational only). */
		public final int oldMode;
		
		/** User ID in the new version. */
		public final int newUID;
		
		/** Group ID in the new version. */
		public final int newGID;
		
		/** Permission mode in the new version. */
		public final int newMode;
		
		/**
		 * Construct a new PermissionDifference during inspection.
		 * 
		 * @param container The path to the container of this object.
		 * @param oldFSObj The object from the old image.
		 * @param newFSObj The object from the new image.
		 */
		public PermissionDifference(String container, FSObject oldFSObj,
		                            FSObject newFSObj)
		{
			
			super(container, newFSObj.getPath());
			
			this.oldUID = oldFSObj.getUID();
			this.oldGID = oldFSObj.getGID();
			this.oldMode = oldFSObj.getMode();
			
			this.newUID = newFSObj.getUID();
			this.newGID = newFSObj.getGID();
			this.newMode = newFSObj.getMode();
		}
		
		/**
		 * Construct a new PermissionDifference from saved data.
		 * 
		 * @param tag The tag describing the difference.
		 * @throws IOException If the tag was incomplete.
		 */
		public PermissionDifference(Tag tag)
			throws IOException
		{
			super(tag);
			
			this.oldUID = tag.getIntContents("oldUID", -1);
			if (this.oldUID == -1) {
				throw new IOException("Missing oldUID");
			}
			
			this.oldGID = tag.getIntContents("oldGID", -1);
			if (this.oldGID == -1) {
				throw new IOException("Missing oldGID");
			}
			
			// Parse specially because we save in octal (standard format for
			// Unix permissions)
			this.oldMode = Utils.parseInt(tag.getStringContents("oldMode", "-1"));
			if (this.oldMode == -1) {
				throw new IOException("Missing oldMode");
			}
			

			this.newUID = tag.getIntContents("newUID", -1);
			if (this.newUID == -1) {
				throw new IOException("Missing newUID");
			}
			
			this.newGID = tag.getIntContents("newGID", -1);
			if (this.newGID == -1) {
				throw new IOException("Missing newGID");
			}
			
			// Parse specially because we save in octal (standard format for
			// Unix permissions)
			this.newMode = Utils.parseInt(tag.getStringContents("newMode", "-1"));
			if (this.newMode == -1) {
				throw new IOException("Missing newMode");
			}
		}
		
		@Override
		public Tag asTag()
		{
			Tag tagDiff = super.asTag();
			
			Tag tagOldUID = new Tag("oldUID", "" + oldUID);
			tagDiff.addContent(tagOldUID);
			
			Tag tagOldGID = new Tag("oldGID", "" + oldGID);
			tagDiff.addContent(tagOldGID);
			
			Tag tagOldMode = new Tag("oldMode", String.format("0%o", oldMode));
			tagDiff.addContent(tagOldMode);
			
			Tag tagNewUID = new Tag("newUID", "" + newUID);
			tagDiff.addContent(tagNewUID);
			
			Tag tagNewGID = new Tag("newGID", "" + newGID);
			tagDiff.addContent(tagNewGID);
			
			Tag tagNewMode = new Tag("newMode", String.format("0%o", newMode));
			tagDiff.addContent(tagNewMode);
			
			return tagDiff;
		}
		
		@Override
		public boolean sameChange(Difference d)
		{
			if (this == d) {
				return true; // Simple and quick
			}
			
			if ( ! super.sameChange(d)) {
				// Basic properties are not acceptable
				return false;
			}
			
			PermissionDifference pd = (PermissionDifference)d;
			
			// Old uid/gid/mode are informational only
			
			if (this.newUID != pd.newUID) {
				System.err.println("  Failed on UID");
				return false;
			}
			
			if (this.newGID != pd.newGID) {
				System.err.println("  Failed on GID");
				return false;
			}
			
			if (this.newMode != pd.newMode) {
				System.err.println("  Failed on Mode");
				return false;
			}
			
			return true;
		}
	}
	
	
	/**
	 * Generic difference class, superclass to all the specific differences.
	 * 
	 * @author Jonathan Pearson
	 */
	private static abstract class Difference
	{
		// Properties common to many types of differences
		/**
		 * The path of the most relevant container in which the difference was
		 * found. This will be "" for differences found between top-level
		 * containers, and something like <code>/ARM9/image</code> for a
		 * difference found within the ARM9 'image' JFFS2 file system image.
		 */
		public final String container;
		
		/**
		 * The path of the file that changed. This will be a descriptive string
		 * enclosed within parenthesis for differences unrelated to files within
		 * a firmware image, or a relative path (no leading '/') for other
		 * differences.
		 */
		public final String path;
		
		/**
		 * Construct a new Difference object.
		 * 
		 * @param container The path to the container of this object referenced
		 *   by this difference. A container path will be the path from the root
		 *   of the top-level container passed in by the user, followed by the
		 *   paths through each lower-level container, separated by slashes. For
		 *   example: <code>ARM9/image</code>.
		 * @param path The path of the object referenced by this difference,
		 *   relative to the most immediate container. For example,
		 *   <code>/bin/sh</code>.
		 */
		protected Difference(String container, String path)
		{
			this.container = container;
			this.path = path;
		}
		
		/**
		 * Parse an XML tag into this difference.
		 * 
		 * @param tag The tag to parse.
		 * @throws IOException If the tag was incomplete.
		 */
		protected Difference(Tag tag)
			throws IOException
		{
			this.container = tag.getStringContents("container", "");
			
			this.path = tag.getStringAttribute("path", null);
			if (this.path == null) {
				throw new IOException("No path");
			}
		}
		
		/**
		 * Load the class (a subclass of Difference) named in the 'type' field
		 * of the given tag.
		 * 
		 * @param tag The tag describing the difference to load.
		 * @return The difference.
		 * @throws IOException If there was a problem loading the difference.
		 */
		public static Difference loadDifference(Tag tag)
			throws IOException
		{
			String className = tag.getStringContents("type");
			if (className == null) {
				throw new IOException("Missing type");
			}
			
			Class<?> clazz;
			try {
				clazz = Class.forName(className);
			}
			catch (ClassNotFoundException cnfe) {
				throw new IOException("Unable to find named class '" +
				                      className + "'", cnfe);
			}
			
			Constructor<?> constructor;
			try {
				constructor = clazz.getConstructor(Tag.class);
			}
			catch (NoSuchMethodException nsme) {
				throw new IOException("Named class '" + className +
				                      "' does not provide a constructor" +
				                      " taking only a Tag", nsme);
			}
			
			try {
				return (Difference)constructor.newInstance(tag);
			}
			catch (InstantiationException ie) {
				throw new IOException("Named class '" + className +
				                      "' is not instantiatable", ie);
			}
			catch (IllegalAccessException iae) {
				throw new IOException("Named class '" + className +
				                      "' has incorrect permissions on its" +
				                      " Tag constructor", iae);
			}
			catch (InvocationTargetException ite) {
				throw new IOException("Named class '" + className +
				                      "' threw an exception during" +
				                      " construction: " +
				                      ite.getTargetException().getMessage(),
				                      ite.getTargetException());
			}
		}
		
		/**
		 * Construct an XML tag that represents this difference. Subclasses
		 * should override this to add their own extra data.
		 */
		public Tag asTag()
		{
			Tag tagDiff = new Tag("difference");
			
			Tag tagType = new Tag("type", getClass().getName());
			tagDiff.addContent(tagType);
			
			Tag tagArch = new Tag("container", container);
			tagDiff.addContent(tagArch);
			
			tagDiff.setAttribute("path", path);
			
			return tagDiff;
		}
		
		/**
		 * If this difference is a known exception, test the given difference to
		 * see if it represents the same difference as this exception.
		 * Subclasses should override this to check their own special data.
		 * 
		 * @param d The difference to check.
		 * @return True if it is acceptable, false if it is not.
		 */
		public boolean sameChange(Difference d)
		{
			return changeHelper(d);
		}
		
		/** Used by sameChange() and similarChange(). */
		private boolean changeHelper(Difference d)
		{
			if ( ! (this.getClass().isInstance(d))) {
				// Not the right type of difference
				return false;
			}
			
			// Otherwise, we need to actually check everything
			
			// Must be in the same container
			if ( ! this.container.equals(d.container)) {
				return false;
			}
			
			// Paths must be equal
			if ( ! this.path.equals(d.path)) {
				return false;
			}
			
			return true;
		}
		
		/**
		 * If this difference is a known exception, test the given difference to
		 * see if it represents a similar difference, but perhaps with a
		 * different result (for example, we expect file contents to change, and
		 * they have, but the resulting checksum is not what we expected to
		 * see). Subclasses may override this, but it is not usually necessary.
		 * 
		 * @param d The difference to check.
		 * @return True if it represents a similar change, false if it
		 *   represents a completely different change.
		 */
		public boolean similarChange(Difference d)
		{
			return changeHelper(d);
		}
		
		/**
		 * Compare two strings for equality, even if one or both is
		 * <tt>null</tt>.
		 * 
		 * @return True if both strings are <tt>null</tt> or are equal, false if
		 *         only one is <tt>null</tt> or they are not equal.
		 */
		protected static boolean matchStrings(String a, String b)
		{
			return ((a == null && b == null) ||
			        (a != null && a.equals(b)));
		}
	}
	
	/**
	 * Output usage information.
	 * 
	 * @param out The stream to write to.
	 */
	private static void usage(PrintStream out)
	{
		out.println("FirmwareValidator v" + PROG_VERSION + " - Validate firmware releases");
		out.println("  Copyright 2010 SIXNET, written by Jonathan Pearson (Jon.Pearson@sixnet.com)");
		
		out.println("Usage: FirmwareValidator -f <previous container> -t <new container>");
		out.println("                         [-x <exception file> [-g]]");
		out.println("       FirmwareValidator -h");
		out.println("  -f  Provide the previous image/bundle");
		out.println("  -t  Provide the new image/bundle");
		out.println("  -x  Provide the file containing the expected changes between the images");
		out.println("        If not specified, all differences between images will be reported");
		out.println("  -g  If specified, <exceptions file> will be generated from the differences");
		out.println("        between the two images (this will overwrite any existing file)");
		out.println("  -h  Print this message and exit");
	}
	
	/** The name of the 'old' top-level image file. */
	private static String oldTop;
	
	/** The name of the 'new' top-level image file. */
	private static String newTop;
	
	public static void main(String[] args)
	{
		File fromImage = null;
		File toImage = null;
		File exceptionsFile = null;
		boolean generate = false;
		
		try {
			// XXX: The loop invariant is modified in the loop body
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-f")) {
					File f = new File(args[++i]);
					if ( ! f.isFile()) {
						throw new Exception("Unable to find file '" +
						                    f.getCanonicalPath() + "'");
					}
					else if ( ! f.canRead()) {
						throw new Exception("Unable to read file '" +
						                    f.getCanonicalPath() + "'");
					}
					
					fromImage = f;
				}
				else if (args[i].equals("-t")) {
					File f = new File(args[++i]);
					if ( ! f.isFile()) {
						throw new Exception("Unable to find file '" +
						                    f.getCanonicalPath() + "'");
					}
					else if ( ! f.canRead()) {
						throw new Exception("Unable to read file '" +
						                    f.getCanonicalPath() + "'");
					}
					
					toImage = f;
				}
				else if (args[i].equals("-x")) {
					// Can't test for existence here, in case we are supposed to
					// generate it
					exceptionsFile = new File(args[++i]);
				}
				else if (args[i].equals("-g")) {
					generate = true;
				}
				else if (args[i].equals("-h")) {
					usage(System.out);
					return;
				}
				else {
					throw new Exception("Unrecognized option: " + args[i]);
				}
			}
			
			// Make sure the fromImage, toImage, and exceptionsFile are provided
			if (fromImage == null) {
				throw new Exception("<previous image> is not optional");
			}
			if (toImage == null) {
				throw new Exception("<current image> is not optional");
			}
			
			if (generate) {
				if (exceptionsFile == null) {
					throw new Exception("<exceptions file> is not optional" +
					                    " when -g is specified");
				}
				
				// Make sure exceptionsFile is writable
				if (exceptionsFile.isFile()) {
					if ( ! exceptionsFile.canWrite()) {
						throw new Exception("Unable to write to '" +
						                    exceptionsFile.getCanonicalPath() +
						                    "'");
					}
				}
				else {
					if ( ! exceptionsFile.getCanonicalFile().getParentFile().isDirectory()) {
						throw new Exception("Parent directory to '" +
						                    exceptionsFile.getCanonicalPath() +
						                    "' does not exist");
					}
					else if ( ! exceptionsFile.getCanonicalFile().getParentFile().canWrite()) {
						throw new Exception("Cannot create a new file in '" +
						                    exceptionsFile.getParentFile().getCanonicalPath() +
						                    "'");
					}
				}
			}
			else if (exceptionsFile != null) {
				// Otherwise, make sure we can read it
				if ( ! exceptionsFile.isFile()) {
					throw new Exception("Unable to find file '" +
					                    exceptionsFile.getCanonicalPath() +
					                    "'");
				}
				if ( ! exceptionsFile.canRead()) {
					throw new Exception("Unable to read file '" +
					                    exceptionsFile.getCanonicalPath() +
					                    "'");
				}
			}
		}
		catch (Exception e) {
			logger.error("Error parsing command-line arguments", e);
			
			System.err.println(e.getMessage());
			usage(System.err);
			System.exit(1);
			return;
		}
		
		oldTop = fromImage.getName();
		newTop = toImage.getName();
		
		List<Difference> found;
		try {
			found = findDifferences(fromImage, toImage);
		}
		catch (IOException ioe) {
			logger.error("Error detecting differences", ioe);
			
			System.err.printf("Error detecting differences: %s\n",
			                  ioe.getMessage());
			System.exit(1);
			return;
		}
		
		if (generate) {
			try {
				saveExceptionsFile(exceptionsFile, found);
			}
			catch (IOException ioe) {
				logger.error("Unable to save exceptions file", ioe);
				
				System.err.printf("Unable to save exceptions file: %s\n",
				                   ioe.getMessage());
				System.exit(1);
				return;
			}
		}
		else {
			// Testing, not generating
			try {
				List<Difference> expected = loadExceptions(exceptionsFile);
				
				verifyExceptions(found, expected);
				
				System.out.println("Image passed verification");
			}
			catch (MultiIOException mioe) {
				// By catching this first, we can sort the error lines
				System.out.println("Image failed verification:");
				
				String[] errors = Utils.getLines(mioe.getMessage());
				Arrays.sort(errors);
				
				for (String err : errors) {
					System.out.println(err);
				}
				
				System.exit(1);
			}
			catch (Exception e) {
				logger.error("Unexpected exception", e);
				
				System.err.printf("Unexpected error: %s\n", e.getMessage());
				System.exit(1);
			}
		}
	}
	
	/**
	 * Examine a pair of top-level images for differences.
	 * 
	 * @param fromImageFile The 'old' image file.
	 * @param toImageFile The 'new' image file.
	 * @return The list of detected differences.
	 * @throws IOException If there was a problem reading either file.
	 */
	private static List<Difference> findDifferences(File fromImageFile,
	                                                File toImageFile)
		throws IOException
	{
		Object[] fromParser = new Object[1];
		Object[] toParser = new Object[1];
		
		// Turn each image into a directory tree
		Directory fromDir = openFile(fromImageFile, fromParser);
		Directory toDir = openFile(toImageFile, toParser);
		
		List<Difference> differences =
			findDifferences("", fromDir, fromParser[0], toDir, toParser[0],
			                false, false);
		
		// Check file names
		if ( ! fromImageFile.getName().equals(toImageFile.getName())) {
			differences.add(new FileNameDifference(fromImageFile.getName(),
			                                       toImageFile.getName()));
		}
		
		return differences;
	}
	
	/**
	 * Test whether the given file is a likely image containing more files.
	 * 
	 * @param f The file to test.
	 * @return True if it is a likely image, false if not.
	 */
	private static boolean isLikelyImage(com.sixnetio.fs.generic.File f)
	{
		Magic.FileType ft = Magic.detectType(f.getData());
		
		switch (ft) {
			case FirmwareBundle:
			case GZippedTarball:
			case JFFS2BigEndian:
			case JFFS2LittleEndian:
			case Tarball:
				return true;
				
			case GZip:
			case Unknown:
				return false;
		}
		
		throw new RuntimeException("Unknown file type: " + ft.name());
	}
	
	/**
	 * Opens the given file, detecting its type in the process, and parses it
	 * into a generic directory tree.
	 * 
	 * @param f The file to open.
	 * @param out_parser Receives the object used to parse the file in [0], if
	 *   not <code>null</code> and of length at least 1.
	 * @return The root of the directory tree contained within the file.
	 * @throws IOException If there was a problem reading the file, including it
	 *   being an unrecognized file type.
	 */
	private static Directory openFile(File f, Object[] out_parser)
		throws IOException
	{
		Magic.FileType ft = Magic.detectType(f);
		
		// Assume it succeeded; we can always close the stream
		InputStream in = new FileInputStream(f);
		
		try {
			if (out_parser == null || out_parser.length < 1) {
				out_parser = new Object[1];
			}
			
			switch (ft) {
				case FirmwareBundle: {
					FirmwareBundle fwb = new FirmwareBundle(in);
					out_parser[0] = fwb;
					return fwb.getRoot();
				}
					
				case GZippedTarball: {
					GZippedTarball gzt = new GZippedTarball(in);
					out_parser[0] = gzt;
					return gzt.getRoot();
				}
				
				case JFFS2BigEndian:
				case JFFS2LittleEndian: {
					JFFS2 jffs2 = new JFFS2(in);
					out_parser[0] = jffs2;
					return jffs2.getRoot();
				}
					
				case Tarball: {
					Tarball tb = new Tarball(in);
					out_parser[0] = tb;
					return tb.getRoot();
				}
					
				case GZip:
				case Unknown:
					throw new IOException("File '" + f.getAbsolutePath() +
					                      "' is of an unknown type, and" +
					                      " cannot be verified");
			}
		}
		finally {
			in.close();
		}
		
		throw new RuntimeException("Unimplemented file type: " + ft.name());
	}
	
	/**
	 * Opens the given file, detecting its type in the process, and parses it
	 * into a generic directory tree.
	 * 
	 * @param f The file to open.
	 * @param out_parser Receives the object used to parse the file in [0], if
	 *   not <code>null</code> and of length at least 1.
	 * @return The root of the directory tree contained within the file.
	 * @throws IOException If there was a problem reading the file, including it
	 *   being an unrecognized file type.
	 */
	private static Directory openFile(com.sixnetio.fs.generic.File f,
	                                  Object[] out_parser)
		throws IOException
	{
		byte[] data = f.getData();
		Magic.FileType ft = Magic.detectType(data);
		
		if (out_parser == null || out_parser.length < 1) {
			out_parser = new Object[1];
		}
		
		switch (ft) {
			case FirmwareBundle: {
				FirmwareBundle fwb = new FirmwareBundle(data);
				out_parser[0] = fwb;
				return fwb.getRoot();
			}
				
			case GZippedTarball: {
				GZippedTarball gzt = new GZippedTarball(data);
				out_parser[0] = gzt;
				return gzt.getRoot();
			}
				
			case JFFS2BigEndian:
			case JFFS2LittleEndian: {
				JFFS2 jffs2 = new JFFS2(data);
				out_parser[0] = jffs2;
				return jffs2.getRoot();
			}
				
			case Tarball: {
				Tarball tb = new Tarball(data);
				out_parser[0] = tb;
				return tb.getRoot();
			}
				
			case GZip:
			case Unknown:
				throw new IOException("File '" + f.getPath() +
				                      "' is of an unknown type, and" +
				                      " cannot be verified");
		}
		
		throw new RuntimeException("Unimplemented file type: " + ft.name());
	}
	
	/**
	 * Check for metadata in the parsers of two images for differences.
	 * 
	 * @param fromParser The parser that generated the 'old' image.
	 * @param toParser The parser that generated the 'new' image.
	 * @return Any differences found.
	 */
	private static List<Difference> checkMetadata(Object fromParser,
	                                              Object toParser)
	{
		List<Difference> differences = new LinkedList<Difference>();
		
		if ( ! fromParser.getClass().equals(toParser.getClass())) {
			differences.add(new ImageTypeDifference(fromParser.getClass().getSimpleName(),
			                                        toParser.getClass().getSimpleName()));
		}
		else {
			if (fromParser instanceof FirmwareBundle) {
				// Firmware bundles
				FirmwareBundle fb = (FirmwareBundle)fromParser;
				FirmwareBundle tb = (FirmwareBundle)toParser;
				
				if ( ! fb.getFWVendor().equals(tb.getFWVendor())) {
					differences.add(new BundleVendorDifference(fb.getFWVendor(),
					                                           tb.getFWVendor()));
				}
				
				if ( ! fb.getFWVersion().equals(tb.getFWVersion())) {
					differences.add(new BundleVersionDifference(fb.getFWVersion(),
					                                            tb.getFWVersion()));
				}
				
				if (fb.getFWType() != tb.getFWType()) {
					differences.add(new BundleTypeDifference(fb.getFWTypeAsString(),
					                                         tb.getFWTypeAsString()));
				}
			}
			else if (fromParser instanceof JFFS2) {
				// JFFS2 images
				JFFS2 fj = (JFFS2)fromParser;
				JFFS2 tj = (JFFS2)toParser;
				
				if (fj.isLittleEndian() != tj.isLittleEndian()) {
					differences.add(new JFFS2EndianDifference(fj.isLittleEndian(),
					                                     tj.isLittleEndian()));
				}
			}
			
			// Nothing special present in tarballs
		}
		
		return differences;
	}
	
	/**
	 * Flatten a directory tree into a map.
	 * 
	 * @param root The root of the directory tree to flatten.
	 * @return A map of object paths onto the filesystem objects themselves.
	 */
	private static Map<String, FSObject> generateFSMap(Directory root)
	{
		Map<String, FSObject> map = new LinkedHashMap<String, FSObject>();
		
		map.put(root.getPath(), root);
		
		for (FSObject obj : root) {
			if (obj instanceof Directory) {
				map.putAll(generateFSMap((Directory)obj));
			}
			else {
				map.put(obj.getPath(), obj);
			}
		}
		
		return map;
	}
	
	/**
	 * Examine a pair of directory trees, along with any metadata available from
	 * the images that contained those trees, for differences.
	 * 
	 * @param containerPath The path of the most immediate container of this
	 *   pair of directory trees. Top-level containers are not included, so if,
	 *   for example, these trees are from the initial bundle that the user
	 *   passed in, containerPath should be "".
	 * @param fromDir The 'old' directory tree.
	 * @param fromParser The parser that was used to read <code>fromDir</code>.
	 * @param toDir The 'new' directory tree.
	 * @param toParser The parser that was used to read <code>toDir</code>.
	 * @param missingOnly If false, all differences are searched for. If true,
	 *   only missing objects are searched for.
	 * @param reverse If false, 'from' items are for the 'old' image and 'to'
	 *   items are for the 'new' image. If true, they are reversed (so
	 *   differences will be saved appropriately).
	 * @return The list of detected differences.
	 */
	private static List<Difference> findDifferences(String containerPath,
	                                                Directory fromDir,
	                                                Object fromParser,
	                                                Directory toDir,
	                                                Object toParser,
	                                                boolean missingOnly,
	                                                boolean reverse)
		throws IOException
	{
		
		// List of differences that we are building
		List<Difference> differences = new LinkedList<Difference>();
		
		// Check metadata specific to the container
		differences.addAll(checkMetadata(fromParser, toParser));
		
		// Scan each directory tree for differences, keeping an eye out for
		// JFFS2 images and Tarballs to delve into
		Map<String, FSObject> fromMap = generateFSMap(fromDir);
		Map<String, FSObject> toMap = generateFSMap(toDir);
		
		// Walk through the files of each image, testing against the files of
		// the other image
		differences.addAll(search(fromDir, toMap, containerPath,
		                          missingOnly, reverse));
		differences.addAll(search(toDir, fromMap, containerPath,
		                          missingOnly, ! reverse));
		
		return differences;
	}
	
	/**
	 * Search for differences between a directory tree and a flattened directory
	 * tree. This will recurse into JFFS2 images and Tarballs detected in the
	 * directory trees.
	 * 
	 * @param fromDir The root of the 'old' directory tree.
	 * @param toMap The flattened 'new' directory tree.
	 * @param containerPath The path of the container of this set of images.
	 * @param missingOnly If true, only count missing objects on this pass.
	 * @param reverse If true, 'old' and 'new' apply as expected; if false,
	 *   they are reversed.
	 * @return A list of detected differences.
	 * @throws IOException If there was an error opening a sub-image.
	 */
	private static List<Difference> search(Directory fromDir,
	                                       Map<String, FSObject> toMap,
	                                       String containerPath,
	                                       boolean missingOnly,
	                                       boolean reverse)
		throws IOException
	{
		
		List<Difference> differences = new LinkedList<Difference>();
		
		for (FSObject fromFSObj : fromDir) {
		        // get the old object in the new map, if it exists
			FSObject toFSObj = toMap.get(fromFSObj.getPath());
			
			// Set when we delve into a sub-image; prevents less in-depth tests
			// for differences from detecting the same ones
			boolean delved = false;
			
			// old object is not in the new map (it was removed)
			if (toFSObj == null) {
				// Exists on only one side
				differences.addAll(generateMissingTree(containerPath,
				                                       fromFSObj,
				                                       reverse));
				
				// Skip the any other tests, since 'toFSObj' is null
				continue;
			}
			else if (fromFSObj instanceof Directory) {
				// Go through the contents
				differences.addAll(search((Directory)fromFSObj,
				                          toMap,
				                          containerPath,
				                          missingOnly,
				                          reverse));
			}
			else if (fromFSObj instanceof com.sixnetio.fs.generic.File &&
			         toFSObj instanceof com.sixnetio.fs.generic.File) {
				
				com.sixnetio.fs.generic.File ff =
					(com.sixnetio.fs.generic.File)fromFSObj;
				com.sixnetio.fs.generic.File tf =
					(com.sixnetio.fs.generic.File)toFSObj;
				
				Object[] fromParser = new Object[1];
				Object[] toParser = new Object[1];
				
				Directory fromSub = null;
				Directory toSub = null;
				
				if (isLikelyImage(ff)) {
					try {
						fromSub = openFile(ff, fromParser);
					}
					catch (IOException ioe) {
						logger.warn("Object " + containerPath + ff.getPath() +
						            " looks like an image, but will not open");
					}
				}
				
				if (isLikelyImage(tf)) {
					try {
						toSub = openFile(tf, toParser);
					}
					catch (IOException ioe) {
						logger.warn("Object " + containerPath + tf.getPath() +
						            " looks like an image, but will not open");
					}
				}
				
				if (fromSub != null && toSub != null) {
					differences.addAll(findDifferences(containerPath + "/" + ff.getPath(),
					                                   fromSub,
					                                   fromParser[0],
					                                   toSub,
					                                   toParser[0],
					                                   missingOnly,
					                                   reverse));
					delved = true;
				}
				else if (fromSub != null && toSub == null) {
					differences.addAll(generateMissingTree(containerPath,
					                                       fromFSObj,
					                                       ! reverse));
					
					delved = true;
				}
				
				// Don't worry about the other direction, they'll be added by a
				// separate call to search() in the other direction
			}
			
			// Skip further tests, if that was all the caller wanted
			if (missingOnly) {
				continue;
			}
			
			if (fromFSObj.getClass() != toFSObj.getClass()) {
				// Type mismatch
				differences.add(new TypeMismatchDifference(containerPath,
				                                           reverse ?
				                                               fromFSObj :
				                                               toFSObj,
				                                           reverse ?
				                                               toFSObj :
				                                               fromFSObj));
			}
			else if (fromFSObj instanceof com.sixnetio.fs.generic.File) {
				// Don't bother examining the checksums for differences if we've
				// already gone down into the files to search
				if ( ! delved) {
					// It's a big cast, shortcut it
					com.sixnetio.fs.generic.File fromFile =
						(com.sixnetio.fs.generic.File)fromFSObj;
					com.sixnetio.fs.generic.File toFile =
						(com.sixnetio.fs.generic.File)toFSObj;
					
					String fromMD5 = fromFile.md5Sum();
					String toMD5 = toFile.md5Sum();
					
					if ( ! fromMD5.equals(toMD5)) {
						// MD5 mismatch
						differences.add(new FileDifference(containerPath,
						                                   reverse ?
						                                       toFSObj :
						                                       fromFSObj,
						                                   reverse ?
						                                       fromMD5 :
						                                       toMD5,
						                                   reverse ?
						                                       toMD5 :
						                                       fromMD5));
					}
				}
			}
			else if (fromFSObj instanceof Symlink) {
				FSObject fromTarget = ((Symlink)fromFSObj).getLinkTarget();
				FSObject toTarget = ((Symlink)fromFSObj).getLinkTarget();
				boolean different = false;
				
				if (fromTarget == null ^ toTarget == null) {
					different = true;
				}
				else if (fromTarget != null &&
				         toTarget != null &&
				         ! fromTarget.getPath().equals(toTarget.getPath())) {
					
					different = true;
				}
				
				if (different) {
					// Link target mismatch
					differences.add(new LinkDifference(containerPath,
					                                   reverse ?
					                                       fromFSObj :
					                                       toFSObj,
					                                   reverse ?
					                                       toFSObj :
					                                       fromFSObj));
				}
			}
			else if (fromFSObj instanceof Device) {
				int fromMajor = ((Device)fromFSObj).getMajor();
				int fromMinor = ((Device)fromFSObj).getMinor();
				
				int toMajor = ((Device)toFSObj).getMajor();
				int toMinor = ((Device)toFSObj).getMinor();
				
				if (fromMajor != toMajor || fromMinor != toMinor) {
					// Device major/minor mismatch
					differences.add(new DeviceDifference(containerPath,
					                                     reverse ?
					                                         fromFSObj :
					                                         toFSObj,
					                                     reverse ?
					                                         toFSObj :
					                                         fromFSObj));
				}
			}
			
			// This is separate because it needs to be tested for all object
			// types
			if (fromFSObj.getUID() != toFSObj.getUID() ||
				fromFSObj.getGID() != toFSObj.getGID() ||
				fromFSObj.getMode() != toFSObj.getMode()) {
				
				differences.add(new PermissionDifference(containerPath,
				                                         reverse ?
				                                             fromFSObj :
				                                             toFSObj,
				                                         reverse ?
				                                             toFSObj :
				                                             fromFSObj));
			}
		}
		
		return differences;
	}
	
	/**
	 * Generate a list of {@link MissingObjectDifference}s, corresponding to
	 * a given object and all descendants of that object if it is a directory.
	 * 
	 * @param containerPath The path to the container of the object.
	 * @param fsObj The object.
	 * @param added Whether the objects have been added (appear on the 'new'
	 *   side), or removed (appear only on the 'old' side).
	 * @return A list of generated {@link MissingObjectDifference}s.
	 */
	private static List<Difference> generateMissingTree(String containerPath,
	                                                    FSObject fsObj,
	                                                    boolean added)
	{
		List<Difference> differences = new LinkedList<Difference>();
		
		differences.add(new MissingObjectDifference(containerPath,
		                                            fsObj,
		                                            added));
		if (fsObj instanceof Directory) {
			Directory dir = (Directory)fsObj;
			for (FSObject child : dir) {
				differences.addAll(generateMissingTree(containerPath,
				                                       child,
				                                       added));
			}
		}
		
		return differences;
	}
	
	/**
	 * Save a list of differences to an exceptions file.
	 * 
	 * @param exceptionsFile The exceptions file to create.
	 * @param differences The list of differences to save.
	 * @throws IOException If there was a problem writing the exceptions file.
	 */
	private static void saveExceptionsFile(File exceptionsFile,
	                                       List<Difference> differences)
		throws IOException
	{
		// Generate XML data based on everything we found
		Tag tagRoot = new Tag("firmData");
		for (Difference diff : differences) {
			tagRoot.addContent(diff.asTag());
		}
		
		// Write to the output file
		Utils.xmlParser.writeXMLFile(tagRoot, exceptionsFile);
		
		System.out.println("Generated file contains " + differences.size() +
		                   " differences");
	}
	
	/**
	 * Load an exceptions file.
	 * 
	 * @param exceptionsFile The exceptions file, or <code>null</code> to
	 *   respond with an empty list of expected differences.
	 * @return The list of expected differences, according to the exceptions
	 *   file.
	 * @throws IOException If there was a problem reading the file.
	 */
	private static List<Difference> loadExceptions(File exceptionsFile)
		throws IOException
	{
		List<Difference> differences = new LinkedList<Difference>();
		
		if (exceptionsFile == null) {
			return differences;
		}
		
		Tag tagRoot = Utils.xmlParser.parseXML(exceptionsFile, false, false);
		Tag[] exceptionTags = tagRoot.getEachTag("", "difference");
		
		for (Tag tag : exceptionTags) {
			// Load this difference
			Difference diff = Difference.loadDifference(tag);
			
			// And save it into the list
			differences.add(diff);
		}
		
		return differences;
	}
	
	/**
	 * Verify that all differences are fully described by a pre-built list of
	 * expected differences.
	 * 
	 * @param found The differences we found.
	 * @param expected The differences we expected.
	 * @throws IOException If there was a problem reading any of the files.
	 */
	private static void verifyExceptions(List<Difference> found,
	                                     List<Difference> expected)
		throws IOException
	{
		// Used to make the output more coherent
		List<DiffPair> similar = new LinkedList<DiffPair>();
		
		// Scan the lists of differences, placing them into maps of
		// Path --> Difference; the path will include the container path
		Map<String, Difference> foundDiff =
			new HashMap<String, Difference>();
		Map<String, Difference> expectedDiff =
			new HashMap<String, Difference>();
		
		for (Difference d : found) {
			foundDiff.put(d.container + d.path, d);
		}
		
		for (Difference d : expected) {
			expectedDiff.put(d.container + d.path, d);
		}
		
		
		// Scan through one map, removing the difference from both maps if
		// present in both and they are the same or similar; if similar, also
		// add to the list of similar differences
		// No need to scan through both maps, as we will not find anything new
		// on the second pass
		// After this, the two maps will contain all differences that were found
		// but not expected, and expected but not found
		for (Iterator<Map.Entry<String, Difference>> itFound =
		         foundDiff.entrySet().iterator();
		     itFound.hasNext(); ) {
			
			Map.Entry<String, Difference> enFound = itFound.next();
			String path = enFound.getKey();
			Difference diff = enFound.getValue();
			Difference exp = expectedDiff.get(path);
			
			if (exp != null) {
				if (diff.sameChange(exp)) {
					itFound.remove();
					expectedDiff.remove(path);
				}
				else if (diff.similarChange(exp)) {
					itFound.remove();
					expectedDiff.remove(path);
					similar.add(new DiffPair(diff, exp));
				}
			}
		}
		
		// Convert the results into a MultiIOException and throw it if it's not
		// empty
		MultiIOException mioe = new MultiIOException();
		
		// Describe the differences we found but did not expect
		for (Difference diff : foundDiff.values()) {
			// Try to put together a reasonable description for each
			String exMsg;
			
			// Shortcuts
			String container = newTop + diff.container;
			String path = container + "/" + diff.path;
			
			if (diff instanceof FileNameDifference) {
				// Only possible at the top level, since there is no simple way
				// to detect a rename anywhere else
				FileNameDifference d = (FileNameDifference)diff;
				exMsg = String.format("Top-level container renamed from" +
				                      " '%s' to '%s'",
				                      d.oldFileName, d.newFileName);
			}
			else if (diff instanceof ImageTypeDifference) {
				// Image type has changed (ex: JFFS2 to FWB or Tarball)
				ImageTypeDifference d = (ImageTypeDifference)diff;
				exMsg = String.format("Container '%s' type changed from" +
				                      " '%s' to '%s'",
				                      container,
				                      d.oldParser, d.newParser);
			}
			else if (diff instanceof BundleVendorDifference) {
				// Bundle vendor string changed
				BundleVendorDifference d = (BundleVendorDifference)diff;
				exMsg = String.format("Bundle '%s' vendor changed from" +
				                      " '%s' to '%s'",
				                      container,
				                      d.oldBundleVendor, d.newBundleVendor);
			}
			else if (diff instanceof BundleVersionDifference) {
				// Bundle version number changed
				BundleVersionDifference d = (BundleVersionDifference)diff;
				exMsg = String.format("Bundle '%s' version changed from " +
				                      " '%s' to '%s'",
				                      container,
				                      d.oldBundleVersion, d.newBundleVersion);
			}
			else if (diff instanceof BundleTypeDifference) {
				// Bundle type changed
				BundleTypeDifference d = (BundleTypeDifference)diff;
				exMsg = String.format("Bundle '%s' type changed from " +
				                      " '%s' to '%s'",
				                      container,
				                      d.oldBundleType, d.newBundleType);
			}
			else if (diff instanceof JFFS2EndianDifference) {
				// JFFS2 endian-ness changed
				JFFS2EndianDifference d = (JFFS2EndianDifference)diff;
				exMsg = String.format("Image '%s' endian changed from " +
				                      " '%s' to '%s'",
				                      container,
				                      d.oldLittleEndian ? "little" : "big",
				                      d.newLittleEndian ? "little" : "big");
			}
			else if (diff instanceof MissingObjectDifference) {
				// An object is missing
				MissingObjectDifference d = (MissingObjectDifference)diff;
				if (d.added) {
					exMsg = String.format("Object '%s' added", path);
				}
				else {
					exMsg = String.format("Object '%s%s/%s' removed",
					                      oldTop, d.container, d.path);
				}
			}
			else if (diff instanceof TypeMismatchDifference) {
				// An object changed types (ex: file to directory)
				TypeMismatchDifference d = (TypeMismatchDifference)diff;
				exMsg = String.format("Object '%s' type changed from" +
				                      " '%s' to '%s'",
				                      path,
				                      d.oldObjectType, d.newObjectType);
			}
			else if (diff instanceof FileDifference) {
				// File contents have changed
				FileDifference d = (FileDifference)diff;
				exMsg = String.format("File '%s' changed", path);
			}
			else if (diff instanceof DeviceDifference) {
				// A device node changed major/minor numbers
				DeviceDifference d = (DeviceDifference)diff;
				exMsg = String.format("Device node '%s' changed from" +
				                      " '%d,%d' to '%d,%d'",
				                      path,
				                      d.oldDevMajor, d.oldDevMinor,
				                      d.newDevMajor, d.newDevMinor);
			}
			else if (diff instanceof LinkDifference) {
				// A symlink's target path changed
				LinkDifference d = (LinkDifference)diff;
				exMsg = String.format("Symlink '%s' target changed from" +
				                      " '%s' to '%s'",
				                      path,
				                      d.oldLinkTarget, d.newLinkTarget);
			}
			else if (diff instanceof PermissionDifference) {
				// An object's ownership/permissions changed
				PermissionDifference d = (PermissionDifference)diff;
				exMsg = String.format("Object '%s' ownership/permissions" +
				                      " changed from '%d:%d:%o' to '%d:%d:%o'",
				                      path,
				                      d.oldUID, d.oldGID, d.oldMode,
				                      d.newUID, d.newGID, d.newMode);
			}
			else {
				exMsg = "Unknown difference type: " +
				diff.getClass().getName();
			}
			
			mioe.add(new IOException(exMsg));
		}
		
		// Describe the differences that we found and expected, but were still
		// different from what we expected
		for (DiffPair dp : similar) {
			Difference fd = dp.diff1; // Found difference
			Difference ed = dp.diff2; // Expected difference
			
			// Try to put together a reasonable description for each
			String exMsg;
			
			// Shortcuts
			String container = newTop + fd.container;
			String path = container + "/" + fd.path;
			
			if (fd instanceof FileNameDifference) {
				// Only possible at the top level, since there is no simple way
				// to detect a rename anywhere else
				FileNameDifference d = (FileNameDifference)fd;
				FileNameDifference e = (FileNameDifference)ed;
				exMsg = String.format("Top-level container renamed from" +
				                      " '%s' to '%s', should be '%s' to '%s'",
				                      d.oldFileName, d.newFileName,
				                      e.oldFileName, e.newFileName);
			}
			else if (fd instanceof ImageTypeDifference) {
				// Image type has changed (ex: JFFS2 to FWB or Tarball)
				ImageTypeDifference d = (ImageTypeDifference)fd;
				ImageTypeDifference e = (ImageTypeDifference)ed;
				exMsg = String.format("Container '%s' type changed from" +
				                      " '%s' to '%s', should be '%s' to '%s'",
				                      container,
				                      d.oldParser, d.newParser,
				                      e.oldParser, e.newParser);
			}
			else if (fd instanceof BundleVendorDifference) {
				// Bundle vendor string changed
				BundleVendorDifference d = (BundleVendorDifference)fd;
				BundleVendorDifference e = (BundleVendorDifference)ed;
				exMsg = String.format("Bundle '%s' vendor changed from" +
				                      " '%s' to '%s', should be '%s' to '%s'",
				                      container,
				                      d.oldBundleVendor, d.newBundleVendor,
				                      e.oldBundleVendor, e.newBundleVendor);
			}
			else if (fd instanceof BundleVersionDifference) {
				// Bundle version number changed
				BundleVersionDifference d = (BundleVersionDifference)fd;
				BundleVersionDifference e = (BundleVersionDifference)ed;
				exMsg = String.format("Bundle '%s' version changed from " +
				                      " '%s' to '%s', should be '%s' to '%s'",
				                      container,
				                      d.oldBundleVersion, d.newBundleVersion,
				                      e.oldBundleVersion, e.newBundleVersion);
			}
			else if (fd instanceof BundleTypeDifference) {
				// Bundle type changed
				BundleTypeDifference d = (BundleTypeDifference)fd;
				BundleTypeDifference e = (BundleTypeDifference)ed;
				exMsg = String.format("Bundle '%s' type changed from " +
				                      " '%s' to '%s', should be '%s' to '%s",
				                      container,
				                      d.oldBundleType, d.newBundleType,
				                      e.oldBundleType, e.newBundleType);
			}
			else if (fd instanceof JFFS2EndianDifference) {
				// JFFS2 endian-ness changed
				JFFS2EndianDifference d = (JFFS2EndianDifference)fd;
				JFFS2EndianDifference e = (JFFS2EndianDifference)ed;
				exMsg = String.format("Image '%s' endian changed from " +
				                      " '%s' to '%s', should be '%s' to '%s'",
				                      container,
				                      d.oldLittleEndian ? "little" : "big",
				                      d.newLittleEndian ? "little" : "big",
				                      e.oldLittleEndian ? "little" : "big",
				                      e.newLittleEndian ? "little" : "big");
			}
			else if (fd instanceof MissingObjectDifference) {
				// An object is missing
				MissingObjectDifference d = (MissingObjectDifference)fd;
				MissingObjectDifference e = (MissingObjectDifference)ed;
				exMsg = String.format("Object '%s' %s, should be %s", path,
				                      d.added ? "added" : "removed",
				                      e.added ? "added" : "removed");
				
				if (d.added) {
					if (e.added) {
						// Just being complete; this doesn't really make any
						// sense
						exMsg = String.format("Object '%s' added, should be" +
						                      " added differently", path);
					}
					else {
						exMsg = String.format("Object '%s' added, should be" +
						                      " removed", path);
					}
				}
				else {
					if (e.added) {
						exMsg = String.format("Object '%s%s/%s' removed," +
						                      " should be added",
						                      oldTop, d.container, d.path);
					}
					else {
						// This doesn't make any sense either
						exMsg = String.format("Object '%s%s/%s' removed," +
						                      " should be removed differencly",
						                      oldTop, d.container, d.path);
					}
				}
			}
			else if (fd instanceof TypeMismatchDifference) {
				// An object changed types (ex: file to directory)
				TypeMismatchDifference d = (TypeMismatchDifference)fd;
				TypeMismatchDifference e = (TypeMismatchDifference)ed;
				exMsg = String.format("Object '%s' type changed from" +
				                      " '%s' to '%s', should be '%s' to '%s'",
				                      path,
				                      d.oldObjectType, d.newObjectType,
				                      e.oldObjectType, e.newObjectType);
			}
			else if (fd instanceof FileDifference) {
				// File contents have changed
				FileDifference d = (FileDifference)fd;
				FileDifference e = (FileDifference)ed;
				exMsg = String.format("File '%s' changed, should have" +
				                      " changed differently", path);
			}
			else if (fd instanceof DeviceDifference) {
				// A device node changed major/minor numbers
				DeviceDifference d = (DeviceDifference)fd;
				DeviceDifference e = (DeviceDifference)ed;
				exMsg = String.format("Device node '%s' changed from" +
				                      " '%d,%d' to '%d,%d', should be '%d,%d'" +
				                      " to '%d,%d'",
				                      path,
				                      d.oldDevMajor, d.oldDevMinor,
				                      d.newDevMajor, d.newDevMinor,
				                      e.oldDevMajor, e.oldDevMinor,
				                      e.newDevMajor, e.newDevMinor);
			}
			else if (fd instanceof LinkDifference) {
				// A symlink's target path changed
				LinkDifference d = (LinkDifference)fd;
				LinkDifference e = (LinkDifference)ed;
				exMsg = String.format("Symlink '%s' target changed from" +
				                      " '%s' to '%s', should be '%s' to '%s'",
				                      path,
				                      d.oldLinkTarget, d.newLinkTarget,
				                      e.oldLinkTarget, e.newLinkTarget);
			}
			else if (fd instanceof PermissionDifference) {
				// An object's ownership/permissions changed
				PermissionDifference d = (PermissionDifference)fd;
				PermissionDifference e = (PermissionDifference)ed;
				exMsg = String.format("Object '%s' ownership/permissions" +
				                      " changed from '%d:%d:%o' to" +
				                      " '%d:%d:%o', should be '%d:%d:%o' to" +
				                      " '%d:%d:%o'",
				                      path,
				                      d.oldUID, d.oldGID, d.oldMode,
				                      d.newUID, d.newGID, d.newMode,
				                      e.oldUID, e.oldGID, e.oldMode,
				                      e.newUID, e.newGID, e.newMode);
			}
			else {
				exMsg = "Unknown difference type: " + fd.getClass().getName();
			}
			
			mioe.add(new IOException(exMsg));
		}
		
		// Describe the differences that we expected but did not find
		for (Difference diff : expectedDiff.values()) {
			// Try to put together a reasonable description for each
			String exMsg;
			
			// Shortcuts
			String container = newTop + diff.container;
			String path = container + "/" + diff.path;
			
			if (diff instanceof FileNameDifference) {
				// Only possible at the top level, since there is no simple way
				// to detect a rename anywhere else
				FileNameDifference d = (FileNameDifference)diff;
				exMsg = String.format("Top-level container NOT renamed from" +
				                      " '%s' to '%s'",
				                      d.oldFileName, d.newFileName);
			}
			else if (diff instanceof ImageTypeDifference) {
				// Image type has changed (ex: JFFS2 to FWB or Tarball)
				ImageTypeDifference d = (ImageTypeDifference)diff;
				exMsg = String.format("Container '%s' type NOT changed from" +
				                      " '%s' to '%s'",
				                      container,
				                      d.oldParser, d.newParser);
			}
			else if (diff instanceof BundleVendorDifference) {
				// Bundle vendor string changed
				BundleVendorDifference d = (BundleVendorDifference)diff;
				exMsg = String.format("Bundle '%s' vendor NOT changed from" +
				                      " '%s' to '%s'",
				                      container,
				                      d.oldBundleVendor, d.newBundleVendor);
			}
			else if (diff instanceof BundleVersionDifference) {
				// Bundle version number changed
				BundleVersionDifference d = (BundleVersionDifference)diff;
				exMsg = String.format("Bundle '%s' version NOT changed from " +
				                      " '%s' to '%s'",
				                      container,
				                      d.oldBundleVersion, d.newBundleVersion);
			}
			else if (diff instanceof BundleTypeDifference) {
				// Bundle type changed
				BundleTypeDifference d = (BundleTypeDifference)diff;
				exMsg = String.format("Bundle '%s' type NOT changed from " +
				                      " '%s' to '%s'",
				                      container,
				                      d.oldBundleType, d.newBundleType);
			}
			else if (diff instanceof JFFS2EndianDifference) {
				// JFFS2 endian-ness changed
				JFFS2EndianDifference d = (JFFS2EndianDifference)diff;
				exMsg = String.format("Image '%s' endian NOT changed from " +
				                      " '%s' to '%s'",
				                      container,
				                      d.oldLittleEndian ? "little" : "big",
				                      d.newLittleEndian ? "little" : "big");
			}
			else if (diff instanceof MissingObjectDifference) {
				// An object is missing
				MissingObjectDifference d = (MissingObjectDifference)diff;
				if (d.added) {
					exMsg = String.format("Object '%s' NOT added", path);
				}
				else {
					exMsg = String.format("Object '%s%s/%s' NOT removed",
					                      oldTop, d.container, d.path);
				}
			}
			else if (diff instanceof TypeMismatchDifference) {
				// An object changed types (ex: file to directory)
				TypeMismatchDifference d = (TypeMismatchDifference)diff;
				exMsg = String.format("Object '%s' type NOT changed from" +
				                      " '%s' to '%s'",
				                      path,
				                      d.oldObjectType, d.newObjectType);
			}
			else if (diff instanceof FileDifference) {
				// File contents have changed
				FileDifference d = (FileDifference)diff;
				exMsg = String.format("File '%s' NOT changed", path);
			}
			else if (diff instanceof DeviceDifference) {
				// A device node changed major/minor numbers
				DeviceDifference d = (DeviceDifference)diff;
				exMsg = String.format("Device node '%s' NOT changed from" +
				                      " '%d,%d' to '%d,%d'",
				                      path,
				                      d.oldDevMajor, d.oldDevMinor,
				                      d.newDevMajor, d.newDevMinor);
			}
			else if (diff instanceof LinkDifference) {
				// A symlink's target path changed
				LinkDifference d = (LinkDifference)diff;
				exMsg = String.format("Symlink '%s' target NOT changed from" +
				                      " '%s' to '%s'",
				                      path,
				                      d.oldLinkTarget, d.newLinkTarget);
			}
			else if (diff instanceof PermissionDifference) {
				// An object's ownership/permissions changed
				PermissionDifference d = (PermissionDifference)diff;
				exMsg = String.format("Object '%s' ownership/permissions NOT" +
				                      " changed from '%d:%d:%o' to '%d:%d:%o'",
				                      path,
				                      d.oldUID, d.oldGID, d.oldMode,
				                      d.newUID, d.newGID, d.newMode);
			}
			else {
				exMsg = "Unknown difference type: " +
				diff.getClass().getName();
			}
			
			mioe.add(new IOException(exMsg));
		}
		
		if ( ! mioe.isEmpty()) {
			throw mioe;
		}
	}
}

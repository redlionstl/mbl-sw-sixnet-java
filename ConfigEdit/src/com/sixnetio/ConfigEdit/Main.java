/*
 * Main.java
 *
 * The entry point for the Config Editor program.
 *
 * Jonathan Pearson
 * February 21, 2008
 *
 */

package com.sixnetio.ConfigEdit;

import com.sixnetio.Switch.*;

import java.io.*;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main {
	public static void main(String[] args) {
		int editorCount = 0;
		
		// Log4j is not actually used by this application, although some
		// libraries use it; this prevents warnings about it not being
		// configured properly
		Logger.getRootLogger().setLevel(Level.OFF);
		
		for (int i = 0; i < args.length; i++) {
			// No options yet, so assume the argument is a file name and load it
			
			try {
				SwitchConfig swcfg = new SwitchConfig(new FileInputStream(args[i]));
				ConfigEditor configEditor = new ConfigEditor();
				configEditor.setActiveConfig(swcfg);
				editorCount++;
			} catch (IOException ioe) {
				System.err.println("Unable to open " + args[i] + ": " + ioe.getMessage());
			}
		}
		
		if (editorCount == 0) new ConfigEditor();
	}
}

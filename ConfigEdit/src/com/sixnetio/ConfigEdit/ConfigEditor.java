/*
 * ConfigEditor.java
 *
 * Allows the importation (read from a switch or the filesystem),
 * viewing/editing, and exporting (write to a switch or the filesystem) of
 * switch configuration checkpoints.
 *
 * Jonathan Pearson
 * February 20, 2008
 *
 */

package com.sixnetio.ConfigEdit;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.*;
import java.io.*;
import java.util.Set;
import java.util.Vector;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.sixnetio.ConfigEdit.Views.EditView;
import com.sixnetio.GUI.LoginDialog;
import com.sixnetio.Switch.SwitchConfig;
import com.sixnetio.Switch.WebUI;

public class ConfigEditor extends JFrame {
	private static final String VIEW_DIR_NAME = "ConfigEditor.Views";
	
	// FileMenuHandler class
	private class FileMenuHandler implements ActionListener {
		// Public data members
		public JMenuItem mnuOpen,
		                 mnuImport,
		                 mnuSave,
		                 mnuExport,
		                 mnuExit;
		
		// public FileMenuHandler(JMenuBar bar)
		public FileMenuHandler() {
			JMenuBar bar = ConfigEditor.this.getJMenuBar();
			
			JMenu mnuFile = new JMenu("File");
			mnuFile.setMnemonic(KeyEvent.VK_F);
			bar.add(mnuFile);
			
			mnuOpen = new JMenuItem("Open...");
			mnuOpen.setMnemonic(KeyEvent.VK_O);
			mnuOpen.addActionListener(this);
			mnuFile.add(mnuOpen);
			
			mnuImport = new JMenuItem("Import...");
			mnuImport.setMnemonic(KeyEvent.VK_I);
			mnuImport.addActionListener(this);
			mnuFile.add(mnuImport);
			
			mnuFile.addSeparator();
			
			mnuSave = new JMenuItem("Save...");
			mnuSave.setMnemonic(KeyEvent.VK_S);
			mnuSave.addActionListener(this);
			mnuFile.add(mnuSave);
			
			mnuExport = new JMenuItem("Export...");
			mnuExport.setMnemonic(KeyEvent.VK_E);
			mnuExport.addActionListener(this);
			mnuFile.add(mnuExport);
			
			mnuFile.addSeparator();
			
			mnuExit = new JMenuItem("Exit");
			mnuExit.setMnemonic(KeyEvent.VK_X);
			mnuExit.addActionListener(this);
			mnuFile.add(mnuExit);
		}
		
		// public void actionPerformed(ActionEvent e)
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == mnuOpen) {
				if (!activeView.allowMoveAway()) return;
				
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Choose a Checkpoint");
				chooser.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
                    public boolean accept(File f) {
						return (f.isDirectory() || // So the user can navigate properly
						        f.getName().endsWith(".tar.gz") ||
						        f.getName().endsWith(".tgz"));
					}
					
					@Override
                    public String getDescription() {
						return "Configuration Checkpoints";
					}
				});
				
				int ret = chooser.showOpenDialog(ConfigEditor.this);
				
				if (ret == JFileChooser.APPROVE_OPTION) {
					Cursor cursor = ConfigEditor.this.getCursor();
					ConfigEditor.this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
					
					try {
						SwitchConfig swcfg = new SwitchConfig(new FileInputStream(chooser.getSelectedFile()));
						ConfigEditor.this.setActiveConfig(swcfg);
					} catch (IOException ioe) {
						JOptionPane.showMessageDialog(ConfigEditor.this, "Unable to open file: " + ioe.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
					}
					
					ConfigEditor.this.setCursor(cursor);
				}
			} else if (e.getSource() == mnuImport) {
				if (!activeView.allowMoveAway()) return;
				
				LoginDialog ld = new LoginDialog("Switch Login");
				ld.setUsername("admin");
				ld.setPassword("admin");
				ld.setServer("10.2.0.1");
				
				int ret = ld.showDialog();
				
				if (ret == LoginDialog.OKAY_OPTION) {
					Cursor cursor = ConfigEditor.this.getCursor();
					ConfigEditor.this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
					
					try {
						SwitchConfig swcfg = WebUI.readSwitchConfig(ld.getServer(), ld.getUsername(), ld.getPassword());
						ConfigEditor.this.setActiveConfig(swcfg);
					} catch (IOException ioe) {
						JOptionPane.showMessageDialog(ConfigEditor.this, "Unable to retrieve configuration checkpoint: " + ioe.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
					}
					
					ConfigEditor.this.setCursor(cursor);
				}
			} else if (e.getSource() == mnuSave) {
				activeView.saveChanges();
				
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Save a Checkpoint");
				chooser.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() {
					@Override
                    public boolean accept(File f) {
						return (f.isDirectory() || // So the user can navigate properly
						        f.getName().endsWith(".tar.gz") ||
						        f.getName().endsWith(".tgz"));
					}
					
					@Override
                    public String getDescription() {
						return "Configuration Checkpoints";
					}
				});
				
				int ret = chooser.showSaveDialog(ConfigEditor.this);
				
				if (ret == JFileChooser.APPROVE_OPTION) {
					Cursor cursor = ConfigEditor.this.getCursor();
					ConfigEditor.this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
					
					File f = chooser.getSelectedFile();
					if (!(f.getName().toLowerCase().endsWith(".tgz") ||
					      f.getName().toLowerCase().endsWith(".tar.gz"))) {
						f = new File(f.getAbsolutePath() + ".tgz");
					}
					
					try {
						SwitchConfig swcfg = ConfigEditor.this.getActiveConfig();
						OutputStream out = new FileOutputStream(f);
						swcfg.toStream(out);
						out.close();
					} catch (IOException ioe) {
						JOptionPane.showMessageDialog(ConfigEditor.this, "Unable to save file: " + ioe.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
					}
					
					ConfigEditor.this.setCursor(cursor);
				}
			} else if (e.getSource() == mnuExport) {
				activeView.saveChanges();
				
				LoginDialog ld = new LoginDialog("Switch Login");
				ld.setUsername("admin");
				ld.setPassword("admin");
				ld.setServer("10.2.0.1");
				
				int ret = ld.showDialog();
				
				if (ret == LoginDialog.OKAY_OPTION) {
					Cursor cursor = ConfigEditor.this.getCursor();
					ConfigEditor.this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
					
					try {
						WebUI.writeSwitchConfig(ConfigEditor.this.getActiveConfig(), ld.getServer(), ld.getUsername(), ld.getPassword());
					} catch (IOException ioe) {
						JOptionPane.showMessageDialog(ConfigEditor.this, "Unable to activate configuration checkpoint: " + ioe.getMessage(), "Error", JOptionPane.WARNING_MESSAGE);
					}
					
					ConfigEditor.this.setCursor(cursor);
				}
			} else if (e.getSource() == mnuExit) {
				if (!activeView.allowMoveAway()) return;
				
				ConfigEditor.this.setVisible(false);
				ConfigEditor.this.dispose();
			} else {
				throw new IllegalArgumentException("Unexpected source of action event: " + e.getSource());
			}
		}
	}
	
	// Private data members
	// In case we need to manually activate any menu items (or enable/disable...)
	private FileMenuHandler fileMenuHandler;
	private JTabbedPane tabs;
	private java.util.List<EditView> views; // Read-only after the constructor
	private SwitchConfig activeConfig;
	private EditView activeView;
	
	// public ConfigEditor()
	public ConfigEditor() {
		super("Configuration Editor");
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent e) {
				fileMenuHandler.mnuExit.doClick();
			}
		});
		
		setupMenus();
		setupInterface();
		
		pack();
		setVisible(true);
	}
	
	// private void setupMenus()
	private void setupMenus() {
		JMenuBar bar = new JMenuBar();
		setJMenuBar(bar);
		
		// File menu
		fileMenuHandler = new FileMenuHandler();
	}
	
	// {{{
	private void setupInterface() {
		setLayout(new BorderLayout());
		
		// Tabbed views of the data to allow multiple different ways to edit it
		tabs = new JTabbedPane();
		add(tabs, BorderLayout.CENTER);
		
		views = new Vector<EditView>();
		
		Set<EditView> availableViews = ViewLoader.loadViews(new File(".", VIEW_DIR_NAME));
		
		if (availableViews.size() == 0) {
			// Try the user's home dir
			availableViews = ViewLoader.loadViews(new File(System.getProperty("user.home"), VIEW_DIR_NAME));
		}
		
		if (availableViews.size() == 0) {
			JOptionPane.showMessageDialog(this, "No views were found. Please place view plugins in " + VIEW_DIR_NAME, "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
			return;
		}
		
		for (EditView view : availableViews) {
			tabs.add(view.getName(), view);
			views.add(view);
		}
		
		activeView = (EditView)tabs.getSelectedComponent();
		
		tabs.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (tabs.getSelectedComponent() != activeView) {
					if (!activeView.allowMoveAway()) {
						tabs.setSelectedComponent(activeView);
					} else {
						activeView = (EditView)tabs.getSelectedComponent();
						activeView.gainFocus();
					}
				}
			}
		});
	}
	
	public void setActiveConfig(SwitchConfig swcfg) {
		activeConfig = swcfg;
		
		for (EditView view : views) {
			view.setConfig(swcfg);
		}
	}
	
	public SwitchConfig getActiveConfig() {
		return activeConfig;
	}
}

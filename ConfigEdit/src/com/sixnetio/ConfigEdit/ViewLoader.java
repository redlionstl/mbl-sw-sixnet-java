/*
 * ViewLoader.java
 *
 * Scans a directory for JAR files that contain EditViews and loads them.
 *
 * Jonathan Pearson
 * February 21, 2008
 *
 */

package com.sixnetio.ConfigEdit;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.*;

import com.sixnetio.ConfigEdit.Views.EditView;
import com.sixnetio.util.JarClassLoader;
import com.sixnetio.util.Utils;

class ViewLoader {
	// 'from' should be a directory
	public static Set<EditView> loadViews(File from) {
		Set<EditView> views = new HashSet<EditView>();
		
		// Scan 'from' for jar files
		if (!from.exists()) {
			Utils.debug("No directory to load views from: " + from.getAbsolutePath());
			return views; // No directory means no views
		}
		
		if (!from.isDirectory()) {
			throw new IllegalArgumentException("Not a directory: " + from.getAbsolutePath()); // But if it's something else...
		}
		
		File[] jars = from.listFiles(new FileFilter() {
			public boolean accept(File f) {
				return (f.isFile() && f.getName().toLowerCase().endsWith(".jar"));
			}
		});
		
		// Scan each of those to see if it is labeled as an EditView
		Utils.debug("Found " + jars.length + " possible view plugins.");
		for (int i = 0; i < jars.length; i++) {
			Utils.debug("Examining possible view: " + jars[i].getAbsolutePath());
			
			if (isEditView(jars[i])) {
				Utils.debug("  It identifies itself as an edit view");
				Set<EditView> view = loadEditView(jars[i]);
				views.addAll(view);
			} else {
				Utils.debug("  It does not identify itself as an edit view");
			}
		}
		
		Utils.debug("Found a total of " + views.size() + " views");
		return views;
	}
	
	private static boolean isEditView(File f) {
		// Tests whether f's manifest labels it as an EditView
		JarInputStream in = null; // So we can be sure to close it before returning
		try {
			in = new JarInputStream(new FileInputStream(f));
			
			Manifest m = in.getManifest();
			if (m == null) return false;
			
			Attributes attr = m.getMainAttributes();
			for (Object name : attr.keySet()) {
				if (name.toString().equals("EditView") &&
				    attr.get(name).toString().equals("ConfigEditor")) {
					return true;
				}
			}
			
			return false;
		} catch (IOException ioe) {
			Utils.debug(ioe);
			return false;
		} finally {
			try {
				if (in != null) in.close();
			} catch (IOException ioe) { }
		}
	}
	
	private static Set<EditView> loadEditView(File f) {
		Set<EditView> views = new HashSet<EditView>();
		
		try {
			JarClassLoader loader;
			
			FileInputStream fin = new FileInputStream(f);
			try {
    			JarInputStream jin = new JarInputStream(fin);
    			try {
        			loader = new JarClassLoader(ViewLoader.class.getClassLoader(), jin);
    			} finally {
    				jin.close();
    			}
			} finally {
				fin.close();
			}
			
			// Search for .class files and load them
			// If we happen across an EditView subclass, hold onto it so we can return it
			for (String name : loader.getClassNames()) {
				try {
					Class<?> clazz = loader.loadClass(name);
					if (EditView.class.isAssignableFrom(clazz)) views.add((EditView)clazz.newInstance());
				} catch (Exception e) {
					Utils.debug(e);
				}
			}
			
			return views;
		} catch (IOException ioe) {
			Utils.debug(ioe);
			return views;
		}
	}
}

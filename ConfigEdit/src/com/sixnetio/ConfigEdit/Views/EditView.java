/*
 * EditView.java
 *
 * All editor types must subclass this.
 *
 * Jonathan Pearson
 * February 20, 2008
 *
 */

package com.sixnetio.ConfigEdit.Views;

import com.sixnetio.Switch.*;

import javax.swing.*;

public abstract class EditView extends JPanel {
	// There must be an empty constructor
	public EditView() {
	}
	
	// The name that should be displayed on the tab for this editor
	public abstract String getName();
	
	// 'config' will be decompressed by the time this is called
	public abstract void setConfig(SwitchConfig config);
	
	// Called whenever the tab is losing focus or the user is moving to a different config
	public abstract boolean allowMoveAway();
	
	// Called when the user saves/exports the current config, to make sure everything is up-to-date first
	// allowMoveAway() should probably call this when changes should be kept
	public abstract void saveChanges();
	
	// Called when the editor gains new focus
	// It should update any cached data, as another editor may have changed it
	// Not called after a load, so setConfig() should probably call this in case this is the active editor
	public abstract void gainFocus();
}

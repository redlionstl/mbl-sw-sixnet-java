/*
 * BasicConfigView.java
 *
 * A very basic config editor. Simply separates everything by file name, and it
 * is the user's job to navigate within each.
 *
 * Jonathan Pearson
 * February 20, 2008
 *
 */

package com.sixnetio.ConfigEdit.Views.Basic;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.sixnetio.ConfigEdit.Views.EditView;
import com.sixnetio.Switch.SwitchConfig;
import com.sixnetio.fs.generic.FSObject;
import com.sixnetio.util.Utils;

public class BasicConfigView extends EditView implements ListSelectionListener {
	private static class SimpleModel extends AbstractListModel {
		private FSObject[] values;
		
		public SimpleModel(FSObject[] vals) {
			this.values = vals;
		}
		
		public int getSize() {
			return values.length;
		}
		
		public String getElementAt(int index) {
			if (values[index] instanceof com.sixnetio.fs.generic.Directory) {
				return values[index].getPath() + "/";
			}
			else {
				return values[index].getPath();
			}
		}
		
		public FSObject getFSObjectAt(int index) {
			return values[index];
		}
	}
	
	private JList lstFiles;
	private JTextArea txtContents;
	
	JScrollPane scrFiles,
	            scrContents;
	
	private SwitchConfig config;
	private int selectedIndex = -1;
	
	// There must be an empty constructor
	public BasicConfigView() {
		setLayout(new BorderLayout());
		
		lstFiles = new JList();
		lstFiles.addListSelectionListener(this);
		
		txtContents = new JTextArea(40, 81);
		txtContents.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
		
		scrFiles = new JScrollPane(lstFiles);
		scrContents = new JScrollPane(txtContents);
		
		add(scrFiles, BorderLayout.WEST);
		add(scrContents, BorderLayout.CENTER);
	}
	
	// The name that should be displayed on the tab for this editor
	@Override
    public String getName() {
		return "Basic";
	}
	
	@Override
    public void setConfig(final SwitchConfig config) {
		this.config = config;
		
		FSObject[] files = config.getRoot().getAllObjects().toArray(new FSObject[0]);
		Arrays.sort(files, new Comparator<FSObject>() {
			@Override
			public int compare(FSObject o1, FSObject o2)
			{
				return o1.getPath().compareTo(o2.getPath());
			}
		});
		lstFiles.setModel(new SimpleModel(files));
		
		lstFiles.setSelectedIndex(selectedIndex = -1);
		txtContents.setText("");
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JScrollBar bar = scrFiles.getVerticalScrollBar();
				bar.setValue(bar.getMinimum());
			}
		});
	}
	
	// Called whenever the tab is losing focus or the user is moving to a different config
	@Override
    public boolean allowMoveAway() {
		if (config == null || selectedIndex == -1) return true;
		
		FSObject tf = ((SimpleModel)lstFiles.getModel()).getFSObjectAt(selectedIndex);
		
		if (tf instanceof com.sixnetio.fs.generic.File) {
			com.sixnetio.fs.generic.File file = (com.sixnetio.fs.generic.File)tf;
			
			if ( ! Arrays.equals(txtContents.getText().getBytes(), file.getData())) {
				int ret = JOptionPane.showConfirmDialog(this, "Keep changes?", "Data Changed", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (ret == JOptionPane.YES_OPTION) {
					file.setData(txtContents.getText().getBytes());
					return true;
				}
				else if (ret == JOptionPane.NO_OPTION) {
					return true;
				}
				else {
					return false;
				}
			} else {
				return true;
			}
		}
		else {
			// Non-files are not editable
			return true;
		}
	}
	
	// Called when the user saves/exports the current config, to make sure everything is up-to-date first
	// allowMoveAway() should probably call this when changes should be kept
	@Override
    public void saveChanges() {
		FSObject tf = config.getRoot().locate(lstFiles.getSelectedValue().toString());
		if (tf != null && tf instanceof com.sixnetio.fs.generic.File) {
			// Only files are editable
			((com.sixnetio.fs.generic.File)tf).setData(txtContents.getText().getBytes());
		}
	}
	
	// Called when the editor gains new focus
	// It should update any cached data, as another editor may have changed it
	// Not called after a load, so setConfig() should probably call this in case this is the active editor
	@Override
    public void gainFocus() {
		if (config != null && lstFiles.getSelectedIndex() != -1) {
			FSObject tf = config.getRoot().locate(lstFiles.getSelectedValue().toString());
			
			if (tf != null) {
				// It should never be null, but best not to crash just in case
				Utils.debug("Refreshing file " + tf.getPath());
				
				if (tf instanceof com.sixnetio.fs.generic.File) {
					byte[] data = ((com.sixnetio.fs.generic.File)tf).getData();
					txtContents.setText(new String(data));
					txtContents.setEditable(true);
				}
				else {
					txtContents.setText("");
					txtContents.setEditable(false);
				}
			}
		}
		else {
			txtContents.setText("");
			txtContents.setEditable(false);
		}
	}
	
	public void valueChanged(ListSelectionEvent e) {
		if (e.getValueIsAdjusting()) return;
		
		if (allowMoveAway()) {
			selectedIndex = lstFiles.getSelectedIndex();
			
			FSObject tf = config.getRoot().locate(lstFiles.getSelectedValue().toString());
			
			if (tf != null && tf instanceof com.sixnetio.fs.generic.File) {
				Utils.debug("User selected file " + tf);
				
				byte[] data = ((com.sixnetio.fs.generic.File)tf).getData();
				txtContents.setText(new String(data));
				txtContents.setEditable(true);
				
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						JScrollBar bar = scrContents.getVerticalScrollBar();
						bar.setValue(bar.getMinimum());
					}
				});
			}
			else if (tf != null && tf instanceof com.sixnetio.fs.generic.Symlink) {
				txtContents.setText("Link to " +
				                    ((com.sixnetio.fs.generic.Symlink)tf).getLinkName());
				txtContents.setEditable(false);
			}
			else {
				// Not a file, no contents
				txtContents.setText("");
				txtContents.setEditable(false);
			}
		}
	}
}

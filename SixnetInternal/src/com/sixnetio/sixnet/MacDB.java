/*
 * MacDB.java
 *
 * Handles MAC addresses/ranges stored in a sqlite database. This is basically a
 * translation of macdb.cpp.
 *
 * Jonathan Pearson
 * October 30, 2009
 *
 */

package com.sixnetio.sixnet;

import java.io.File;
import java.sql.*;

import org.apache.log4j.Logger;

import com.sixnetio.util.*;
import com.sixnetio.util.Ref;

/**
 * <p>
 * Deals with a Sixnet SQLite MAC database. This is public as a hack to allow
 * the database path to be set before a blind reflective instantiation.
 * </p>
 * 
 * <p>
 * To retrieve a new block of MAC addresses:
 * </p>
 * 
 * <p>
 * <code>
 * byte[] macBlock;<br />
 * MacDB db = new MacDB("macs.db");<br />
 * try {<br />
 * &nbsp;&nbsp;macBlock = db.getNewMACAddrBlock(8);<br />
 * }<br />
 * finally {<br />
 * &nbsp;&nbsp;db.close();<br />
 * }
 * </code>
 * </p>
 * 
 * <p>
 * To determine whether an address has been assigned, and the block it came
 * from:
 * </p>
 * 
 * <p>
 * <code>
 * int blockSize;<br />
 * boolean assigned;<br />
 * MacDB db = new MacDB("macs.db");<br />
 * try {<br />
 * &nbsp;&nbsp;int[] numUsed = new int[1];<br />
 * &nbsp;&nbsp;assigned = db.verifyMACAddress(address, numUsed);<br />
 * &nbsp;&nbsp;blockSize = numUsed[0];<br />
 * }<br />
 * finally {<br />
 * &nbsp;&nbsp;db.close();<br />
 * }
 * </code>
 * </p>
 * 
 * @author Jonathan Pearson
 */
public class MacDB
    extends MacProvider
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /**
     * Path to the MAC database. This MUST be set before attempting to construct
     * an instance of this object.
     */
    private static String dbPath;

    /**
     * Set the path to the database (MUST be called before any instantiation
     * will succeed).
     * 
     * @param path The path to the database.
     */
    public static void setDatabasePath(final String path)
    {
        dbPath = path;
    }

    /**
     * Get the path to the database.
     * 
     * @return The path to the database.
     */
    public static String getDatabasePath()
    {
        return dbPath;
    }

    public static void registerProvider()
    {
        // Higher priority than MacFile
        MacProvider.registerProvider(0, MacDB.class);
    }

    /** Schema version. */
    private static final int MACDB_SCHEMA_CODE = 20090817;

    /** The connection to the database. */
    private Connection conn;

    /**
     * Construct a new MacDB object for dealing with the MAC database.
     * 
     * @throws SQLException If there was a problem opening/reading the file.
     */
    public MacDB()
        throws SQLException
    {
        // Does the file exist?
        File f = new File(dbPath);
        if (!f.isFile()) {
            throw new SQLException("Database file '" + dbPath +
                                   "' does not exist");
        }

        // Load the database driver
        try {
            Class.forName("org.sqlite.JDBC");
        }
        catch (ClassNotFoundException cnfe) {
            throw new IllegalStateException("SQLITE JDBC library not present");
        }

        // Open a connection to the database
        Connection conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);

        boolean success = false;
        try {
            // Verify the schema version
            if (!checkVersion(conn)) {
                throw new SQLException("Incorrect schema version for database");
            }

            // Perform housekeeping tasks
            doHousekeeping(conn);

            success = true;
        }
        finally {
            // Failed? Close the connection before exiting with the exception
            if (!success) {
                conn.close();
            }
        }

        this.conn = conn;
    }

    /**
     * Check the schema version on the connected database.
     * 
     * @param conn The connection to the database.
     * @return <tt>true</tt> if the schema version is okay, <tt>false</tt> if it
     *         is not okay.
     * @throws SQLException If there is an error communicating with the
     *             database.
     */
    private static boolean checkVersion(Connection conn)
        throws SQLException
    {
        Statement s = conn.createStatement();

        try {
            s.setQueryTimeout(QUERY_TIMEOUT);

            ResultSet rs = s.executeQuery("PRAGMA user_version");

            try {
                while (rs.next()) {
                    int result = rs.getInt(1);

                    return (result == MACDB_SCHEMA_CODE);
                }

                return false;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }


    /**
     * Create a new MAC address block in the database of the given size. No
     * validity checking is done.
     * 
     * @param macAddr The starting MAC address for the block.
     * @param blockSize The size of the block, in MAC addresses.
     * @throws SQLException If there is an error creating the block.
     * @throws ArithmeticException If the ending address for the block (starting
     *             address plus block size minus one) would have a different OUI
     *             than the starting address.
     */
    private void createMACBlock(MacAddress macAddr, int blockSize)
        throws SQLException, ArithmeticException
    {
        if (conn == null) {
            throw new IllegalStateException("Database connection is not open");
        }

        MacAddress endAddr = macAddr.incrementBy(blockSize - 1);

        PreparedStatement s =
            conn.prepareStatement("INSERT INTO macdb"
                                  + " (startAddr, endAddr, blockSize)"
                                  + " VALUES (?, ?, ?)");

        try {
            s.setQueryTimeout(QUERY_TIMEOUT);

            s.setBytes(1, macAddr.getBytes());
            s.setBytes(2, endAddr.getBytes());
            s.setInt(3, blockSize);

            s.executeUpdate();
        }
        finally {
            s.close();
        }
    }

    /**
     * Split a MAC address block into pieces.
     * 
     * @param macAddr The beginning address for the block.
     * @param blockSize The current size of the block.
     * @param targetSize The desired block size.
     * @throws SQLException If there is a problem updating the database.
     */
    private void splitMACAddrBlock(MacAddress macAddr, int blockSize,
                                   int targetSize)
        throws SQLException
    {
        if (conn == null) {
            throw new IllegalStateException("Database is not open");
        }

        if (blockSize == targetSize) {
            return; // Nothing to do, current and desired sizes match
        }
        else if (blockSize < targetSize) {
            throw new IllegalArgumentException(String.format(
                "BUG: Block size %d < target size %d", blockSize, targetSize));
        }

        // TODO: (copied from macdb.cpp) Check the power of 2 constraint

        // We'll make the target size block first

        // Logic check paranoia - make sure we end up with a block of the right
        // size before doing anything
        int currentSize = targetSize;
        int accum;

        // We want a progression like 1, 1, 2, 4, 8, 16 ... each term is the sum
        // of all terms that came before. This can be looked at in terms of
        // binary addition, for example with a block size 32 you have
        // 0010 0000 = 0000 0001 +
        // 0000 0001 +
        // 0000 0010 +
        // 0000 0100 +
        // 0000 1000 +
        // 0001 0000
        // = 0001 1111 + 1
        for (accum = currentSize; accum < blockSize;) {
            accum += currentSize;
            currentSize *= 2;
        }

        if (accum != blockSize) {
            throw new ArithmeticException(String.format(
                "BUG! Block %s (%d addresses) would have %d addresses when"
                    + " split (target size: %d)", macAddr, blockSize, accum,
                targetSize));
        }

        // Start by deleting the current block
        PreparedStatement s =
            conn.prepareStatement("DELETE FROM macdb WHERE startAddr = ?"
                                  + " AND blockSize = ? AND blockUsed = 0");

        try {
            s.setQueryTimeout(QUERY_TIMEOUT);

            s.setBytes(1, macAddr.getBytes());
            s.setInt(2, blockSize);

            int updatedRows = s.executeUpdate();

            if (updatedRows != 1) {
                throw new IllegalStateException(String.format(
                    "BUG! %d rows found for block %s (%d addresses)",
                    updatedRows, macAddr, blockSize));
            }
        }
        finally {
            s.close();
        }

        // Create the first MAC block
        currentSize = targetSize;
        createMACBlock(macAddr, currentSize);

        // Create the remaining MAC blocks
        for (accum = currentSize; accum < blockSize;) {
            // Create a block of size currentSize at the current address
            MacAddress workingMAC = macAddr.incrementBy(accum);
            createMACBlock(workingMAC, currentSize);

            accum += currentSize;
            currentSize *= 2;
        }

        if (accum != blockSize) {
            throw new ArithmeticException(String.format(
                "BUG! Block %s (%d addresses) has %d addresses when split"
                    + " (target size: %d)", macAddr, blockSize, accum,
                targetSize));
        }
    }

    /**
     * Close the connection to the database.
     */
    @Override
    public synchronized void close()
    {
        try {
            conn.close();
        }
        catch (SQLException sqle) {
            logger.debug("Error closing database connection", sqle);
        }
        finally {
            conn = null;
        }
    }

    @Override
    public synchronized MacAddress[] getNewMACAddrBlock(int blockSize)
        throws SQLException
    {
        if (conn == null) {
            throw new IllegalStateException("Database connection is not open");
        }

        // Begin transaction. Immediate locks the database for writing, although
        // reads can continue. (This function should be the only entry point
        // that writes to the database under normal conditions.)
        Statement transaction = conn.createStatement();

        try {
            transaction.setQueryTimeout(QUERY_TIMEOUT);

            transaction.execute("BEGIN IMMEDIATE");
        }
        finally {
            transaction.close();
        }

        // Make sure to commit the transaction at the end
        boolean success = false;
        try {
            MacAddress macAddr = null;

            // Check for an appropriate, unused block. If it exists, great! Mark
            // it used and return it.
            boolean foundMatch = false;

            PreparedStatement s =
                conn
                    .prepareStatement("SELECT startAddr, blockSize FROM macdb"
                                      + " WHERE blockUsed = 0 AND blockSize = ?");

            try {
                s.setQueryTimeout(QUERY_TIMEOUT);

                s.setInt(1, blockSize);

                ResultSet rs = s.executeQuery();
                try {
                    // Save the result into macAddr to be used below.

                    // Need to retrieve the pointer first to force the result
                    // into the expected format. Hopefully everything in the
                    // database is already a blob, but...
                    while (rs.next()) {
                        byte[] macBytes = rs.getBytes(1);
                        if (macBytes.length != MacAddress.ETH_ALEN) {
                            throw new IllegalStateException(String.format(
                                "BUG! Found record with address length %d"
                                    + " (expected %d)", macBytes.length,
                                MacAddress.ETH_ALEN));
                        }

                        macAddr = new MacAddress(macBytes);
                        foundMatch = true;

                        break;
                    }
                }
                finally {
                    rs.close();
                }
            }
            finally {
                s.close();
            }

            if (!foundMatch) {
                // No matches. We need to se if a larger block can be split up
                // into
                // blocks of an appropriate size.
                int splitBlockSize = 0;

                s =
                    conn
                        .prepareStatement("SELECT startAddr, blockSize FROM macdb"
                                          + " WHERE blockUsed = 0 AND blockSize >= ?"
                                          + " ORDER BY blockSize ASC");

                try {
                    s.setQueryTimeout(QUERY_TIMEOUT);

                    s.setInt(1, blockSize);

                    ResultSet rs = s.executeQuery();
                    try {
                        while (rs.next()) {
                            byte[] macBytes = rs.getBytes(1);
                            splitBlockSize = rs.getInt(2);

                            if (macBytes.length != MacAddress.ETH_ALEN) {
                                throw new IllegalStateException(String.format(
                                    "BUG! Found record with address length %d"
                                        + " (expected %d)", macBytes.length,
                                    MacAddress.ETH_ALEN));
                            }

                            macAddr = new MacAddress(macBytes);
                            foundMatch = true;

                            break;
                        }
                    }
                    finally {
                        rs.close();
                    }
                }
                finally {
                    s.close();
                }

                if (!foundMatch) {
                    // The database is out of MACs we can use.
                    return null;
                }

                if (splitBlockSize < blockSize) {
                    // What do we do? This should never happen.
                    throw new IllegalStateException(String.format(
                        "BUG! Block to split smaller (%d)"
                            + " than needed block size (%d)", splitBlockSize,
                        blockSize));
                }

                splitMACAddrBlock(macAddr, splitBlockSize, blockSize);
            }

            // We have (whether immediately or after splitting up a block) an
            // appropriate row. Mark it as used, store the current timestamp,
            // and
            // return

            // The MAC address should be in macAddr by this point.

            // DATETIME() is in GMT by default
            s =
                conn
                    .prepareStatement("UPDATE macdb SET blockUsed = 1, ts = DATETIME()"
                                      + " WHERE startAddr = ? AND blockSize = ? AND blockUsed = 0");
            try {
                s.setQueryTimeout(QUERY_TIMEOUT);

                s.setBytes(1, macAddr.getBytes());
                s.setInt(2, blockSize);

                int updates = s.executeUpdate();
                if (updates != 1) {
                    throw new SQLException(String.format(
                        "BUG! %d rows changed for MAC  %s (%d addresses)",
                        updates, macAddr, blockSize));
                }
            }
            finally {
                s.close();
            }

            // Expand the one address into a full block
            MacAddress[] macAddresses = new MacAddress[blockSize];
            for (int i = 0; i < blockSize; i++) {
                macAddresses[i] = macAddr.incrementBy(i);
            }

            success = true;
            return macAddresses;
        }
        finally {
            if (success) {
                Statement closeTransaction = conn.createStatement();

                try {
                    closeTransaction.setQueryTimeout(QUERY_TIMEOUT);
                    closeTransaction.execute("COMMIT TRANSACTION");
                }
                finally {
                    closeTransaction.close();
                }
            }
            else {
                Statement rollback = conn.createStatement();

                try {
                    rollback.setQueryTimeout(QUERY_TIMEOUT);
                    rollback.execute("ROLLBACK");
                }
                finally {
                    rollback.close();
                }
            }
        }
    }

    @Override
    public synchronized boolean verifyMACAddress(Ref<MacAddress> macAddr,
                                                 Ref<Integer> numIssued)
        throws SQLException
    {
        if (conn == null) {
            throw new IllegalStateException("Database connection is not open");
        }

        // Make sure we don't have an error writing to numIssued
        if (numIssued == null) {
            numIssued = new Ref<Integer>();
        }

        PreparedStatement s =
            conn.prepareStatement("SELECT startAddr, blockSize" + " FROM macDB"
                                  + " WHERE startAddr <= ? AND endAddr >= ? "
                                  + "   AND blockUsed = 1");
        try {
            s.setQueryTimeout(QUERY_TIMEOUT);

            s.setBytes(1, macAddr.get().getBytes());
            s.setBytes(2, macAddr.get().getBytes());

            ResultSet rs = s.executeQuery();
            try {
                while (rs.next()) {
                    byte[] blockStartBytes = rs.getBytes(1);
                    if (blockStartBytes.length != MacAddress.ETH_ALEN) {
                        throw new IllegalStateException(String.format(
                            "Mismatched address length %d (expected %d)",
                            blockStartBytes.length, MacAddress.ETH_ALEN));
                    }
                    int blockSize = rs.getInt(2);

                    logger.debug("Located address " + macAddr + " in block " +
                                 Conversion.bytesToHex(blockStartBytes, ":"));

                    macAddr.set(new MacAddress(blockStartBytes));
                    numIssued.set(blockSize);

                    return true;
                }

                // If we get here, there were no matches
                logger.warn("Address " + macAddr +
                            " either not available or not yet allocated");
                return false;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }

    /**
     * <p>
     * Do some housekeeping tasks. Right now this means:
     * </p>
     * 
     * <p>
     * <code>
     * PRAGMA integrity_check;<br />
     * ANALYZE;
	 * </code>
     * </p>
     * 
     * @param conn The connection to work over.
     * @throws SQLException If there was an error performing the housekeeping
     *             tasks.
     */
    private static void doHousekeeping(Connection conn)
        throws SQLException
    {
        if (conn == null) {
            throw new IllegalStateException("Database connection is not open");
        }

        Statement s = conn.createStatement();
        try {
            s.setQueryTimeout(QUERY_TIMEOUT);

            ResultSet rs = s.executeQuery("PRAGMA integrity_check");
            try {
                while (rs.next()) {
                    String result = rs.getString(1);
                    if (!result.equalsIgnoreCase("ok")) {
                        throw new SQLException("Failed integrity check");
                    }
                }
            }
            finally {
                rs.close();
            }

            s.execute("ANALYZE");
        }
        finally {
            s.close();
        }
    }

    @Override
    public int estimateRemainingAddresses(int blockSize)
        throws SQLException
    {
        if (conn == null) {
            throw new IllegalStateException("Database connection is not open");
        }

        PreparedStatement s =
            conn.prepareStatement("SELECT SUM(blockSize) FROM macdb"
                                  + " WHERE blockUsed = 0"
                                  + "   AND blockSize >= ?");
        try {
            s.setQueryTimeout(QUERY_TIMEOUT);

            s.setInt(1, blockSize);

            ResultSet rs = s.executeQuery();
            try {
                while (rs.next()) {
                    return rs.getInt(1);
                }

                // No results? No blocks of size >= blockSize
                return 0;
            }
            finally {
                rs.close();
            }
        }
        finally {
            s.close();
        }
    }
}

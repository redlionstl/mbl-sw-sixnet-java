/*
 * MacFile.java
 *
 * Provides a simple interface for acquiring/verifying MAC addresses from the
 * SIXNET files.
 *
 * Jonathan Pearson
 * November 2, 2009
 *
 */

package com.sixnetio.sixnet;

import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.sixnetio.util.Ref;
import com.sixnetio.util.Utils;

class MacFile
    extends MacProvider
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    // Root file name for MAC address files
    public static final String ENCRYPTED_ROOT;

    // Postfixes for block sizes from each file
    // Postfix for getting single addresses (0-2097151 (0-1FFFFF) range)
    public static final String BLOCK_1 = "";

    // Postfix for getting blocks of 8 (2097152-4194303 (200000-3FFFFF) range)
    public static final String BLOCK_8 = "2";

    // Postfix for getting blocks of 16 (4194304-8388607 (400000-7FFFFF) range)
    public static final String BLOCK_16 = "4";

    // File extensions, depending on what we are doing
    public static final String EXT_DATA = ".dat";
    public static final String EXT_BACKUP = ".bak";
    public static final String EXT_LOCK = ".lck";
    public static final String EXT_TEMP = ".tmp";

    static {
        if (Utils.DEBUG) {
            if (Utils.osIsWindows()) {
                ENCRYPTED_ROOT = "c:\\temp\\ethr_num";
            }
            else {
                ENCRYPTED_ROOT = "/tmp/ethr_num";
            }
        }
        else {
            if (Utils.osIsWindows()) {
                ENCRYPTED_ROOT = "w:\\mfg\\test\\working\\ethr_num";
            }
            else {
                ENCRYPTED_ROOT = "/w/mfg/test/working/ethr_num";
            }
        }

        logger.debug("ENCRYPTED_ROOT = " + ENCRYPTED_ROOT);
    }

    public static void registerProvider()
    {
        // Lower priority than MacDB
        MacProvider.registerProvider(1, MacFile.class);
    }

    public MacFile()
        throws IOException
    {
        // Test that all of the necessary files exist
        File[] files =
            new File[] {
                new File(ENCRYPTED_ROOT + BLOCK_1 + EXT_DATA),
                new File(ENCRYPTED_ROOT + BLOCK_8 + EXT_DATA),
                new File(ENCRYPTED_ROOT + BLOCK_16 + EXT_DATA),
            };

        for (File f : files) {
            if (!f.isFile()) {
                throw new IOException("MAC address file '" +
                                      f.getAbsolutePath() + "' does not exist");
            }
        }
    }

    @Override
    public void close()
    {
        // Nothing to do
    }

    @Override
    public int estimateRemainingAddresses(int howMany)
        throws IOException
    {
        Ref<Integer> blockSize = new Ref<Integer>(howMany);
        String macRoot = getRootFileName(blockSize);

        int[] range = readRangeFile(macRoot + EXT_DATA);

        int end = range[1]; // Not interested in the beginning

        // Don't need to acquire the lock, we are only reading
        // Check that we don't go past the end of the range (pethnum.r##.doc)
        if (blockSize.get() == 1) {
            return (0x1cffff - end);
        }
        else if (blockSize.get() == 8) {
            return (0x3cffff - end);
        }
        else if (blockSize.get() == 16) {
            return (0x7cffff - end);
        }
        else {
            throw new IllegalArgumentException(
                "Block size must be 1, 8, or 16; not " + blockSize.get());
        }
    }

    @Override
    public MacAddress[] getNewMACAddrBlock(int howMany)
        throws IOException
    {
        Ref<Integer> blockSize = new Ref<Integer>(howMany);
        String macRoot = getRootFileName(blockSize);

        File lockFile = new File(macRoot + EXT_LOCK);
        Utils.acquireLock(lockFile, QUERY_TIMEOUT);

        try { // Finally clause deletes lock file
            int[] range = readRangeFile(macRoot + EXT_DATA);

            MacAddress[] macAddresses = new MacAddress[blockSize.get()];
            for (int i = 0; i < blockSize.get(); i++) {
                macAddresses[i] = getMACAddress(blockSize.get(), range);
            }

            writeDataFile(macRoot, range);

            return macAddresses;
        }
        finally {
            if (!lockFile.delete()) {
                logger.debug(String.format(
                    "Unable to delete lock file '%s',"
                        + " this is likely to cause problems...", lockFile
                        .getAbsolutePath()));
            }
        }
    }

    @Override
    public boolean verifyMACAddress(Ref<MacAddress> macAddr,
                                    Ref<Integer> out_blockSize)
        throws IOException
    {
        if (out_blockSize == null) {
            out_blockSize = new Ref<Integer>(0);
        }

        // Check the beginning of the address, to see if the OUI matches SIXNET
        // The file method is only used for addresses allocated locally by
        // SIXNET, not by contract manufacturers (CMs), so this is safe
        OUI oui = macAddr.get().getOUI();
        if (Utils.DEBUG) {
            OUI sixnet = new OUI("00:00:00");
            if (!oui.equals(sixnet)) {
                logger.debug("Not a debugging address");
                return false;
            }
        }
        else {
            OUI sixnet = new OUI("00:a0:1d");
            if (!oui.equals(sixnet)) {
                logger.debug("Not a SIXNET address");
                return false;
            }
        }

        // Passed the prefix test, decrypt and check whether it has been
        // assigned

        // Decrypt the lower part of the address to figure out which range it
        // came from
        byte[] bytes = macAddr.get().getBytes();
        int number = 0;
        number |= (bytes[3] & 0xff) << 16;
        number |= (bytes[4] & 0xff) << 8;
        number |= (bytes[5] & 0xff) << 0;

        try {
            number = decryptMACAddress(number);
        }
        catch (IOException ioe) {
            logger.debug("Decrypting MAC address failed", ioe);
            return false; // Not a valid address
        }

        int blockSize;
        String macRoot;

        // Too bad we can't use getRootFileName() here...
        if (0 <= number && number <= 0x1fffff) {
            blockSize = 1;
            macRoot = ENCRYPTED_ROOT + BLOCK_1;
        }
        else if (0x200000 <= number && number <= 0x3fffff) {
            blockSize = 8;
            macRoot = ENCRYPTED_ROOT + BLOCK_8;
        }
        else if (0x400000 <= number && number <= 0x7fffff) {
            blockSize = 16;
            macRoot = ENCRYPTED_ROOT + BLOCK_16;
        }
        else {
            logger.debug("Not in a valid range");
            return false; // Not in a valid range
        }

        if (addressIsAssigned(macRoot, number)) {
            // Mask to 0 the lowest bits in the address, to make it into the
            // base address for its block
            switch (blockSize) {
                case 1:
                    // No change (block of 1 does not need to have a low 0)
                    break;
                case 8:
                    // f8 = 1111 1000
                    bytes[5] = (byte)(bytes[5] & 0xf8);
                    break;
                case 16:
                    // f0 = 1111 0000
                    bytes[5] = (byte)(bytes[5] & 0xf0);
                    break;
                default:
                    logger.debug("Not in a valid range");
                    return false;
            }

            macAddr.set(new MacAddress(bytes));
            out_blockSize.set(blockSize);
            return true;
        }
        else {
            logger.debug(String.format("Address %d (0x%x) from block %d has"
                                       + " not been assigned", number, number,
                blockSize));
            return false;
        }
    }

    /**
     * Figure out the root name of the MAC address file.
     * 
     * @param blockSize [IN, OUT] The block size desired. Will be set to the
     *            block size of the file chosen.
     * @return The root name (no extension) of the file to use for the chosen
     *         block size.
     * @throws IOException If there is no file available for the given block
     *             size.
     */
    private static String getRootFileName(Ref<Integer> blockSize)
        throws IOException
    {
        if (blockSize.get() < 1) {
            throw new IOException("Cannot request fewer than 1 address");
        }
        else if (blockSize.get() == 1) {
            return ENCRYPTED_ROOT + BLOCK_1;
        }
        else if (blockSize.get() <= 8) {
            blockSize.set(8);
            return ENCRYPTED_ROOT + BLOCK_8;
        }
        else if (blockSize.get() <= 16) {
            blockSize.set(16);
            return ENCRYPTED_ROOT + BLOCK_16;
        }
        else {
            throw new IOException("Cannot request more than 16 addresses");
        }
    }

    /**
     * Read a MAC address range file. This does not perform any locking.
     * 
     * @param file The file (including extension).
     * @return [0] = the beginning of the range in the file, [1] = the end of
     *         the range in the file. The range specifies the addresses that
     *         have been allocated already.
     * @throws IOException If there is a problem reading the file.
     */
    private static int[] readRangeFile(String file)
        throws IOException
    {
        // Read the data file
        String lineIn;

        FileReader fin = new FileReader(file);
        try {
            BufferedReader in = new BufferedReader(fin);
            lineIn = in.readLine();
        }
        finally {
            fin.close();
        }

        if (lineIn == null) {
            throw new IOException("Empty MAC address file");
        }

        // Split the line we read
        StringTokenizer tok = new StringTokenizer(lineIn);
        if (tok.countTokens() != 2) {
            throw new IOException("MAC address data file '" + file +
                                  "' has the wrong number of values");
        }

        int[] range = new int[2];
        for (int i = 0; i < range.length; i++) {
            try {
                range[i] = Integer.parseInt(tok.nextToken());
            }
            catch (NumberFormatException nfe) {
                throw new IOException("MAC address data file '" + file +
                                      "' contains a non-integer value");
            }
        }

        return range;
    }

    /**
     * Get a single MAC address from the file for the given block size.
     * 
     * @param blockSize The block size.
     * @param range [IN, OUT] The beginning [0] and end [1] of the range of
     *            previously-assigned addresses. This will be updated to reflect
     *            the new range of assigned addresses.
     * @return A MAC address allocated from the end of the provided range.
     * @throws IOException If there was a problem allocating an address.
     */
    private static MacAddress getMACAddress(int blockSize, int[] range)
        throws IOException
    {
        int begin = range[0];
        int end = range[1];

        // If the number has either bit 16 or 17 set, skip past until they are 0
        // again (pethnum.r##.doc)
        if (blockSize == 1) {
            if (((end & 0x00030000) != 0) || ((end & 0x0000ffff) == 0x0000ffff)) {

                end = (end & 0xfffc0000) + 0x3ffff;
            }
        }
        else if (blockSize == 8) {
            if (((end + blockSize) & 0x00030000) != 0) {
                end = (end & 0xfffc0000) + 0x3ffff;
            }
        }
        else if (blockSize == 16) {
            if (((end + blockSize) & 0x00030000) != 0) {
                end = (end & 0xfffc0000) + 0x3ffff;
            }
        }
        else {
            throw new IllegalArgumentException(
                "Block size must be 1, 8, or 16;" + " not " + blockSize);
        }

        if (begin == 0 && end == 0) {
            throw new IOException("Begin and end are both 0");
        }

        // Check that we don't go past the end of the range (pethnum.r##.doc)
        if (blockSize == 1) {
            if (end >= 0x1cffff) {
                throw new IOException("No more MAC addresses available in"
                                      + " allotment group 0-0x1FFFFF");
            }
        }
        else if (blockSize == 8) {
            if (end >= 0x3cffff) {
                throw new IOException("No more MAC addresses available in"
                                      + " allotment group 0x200000-0x3FFFFF");
            }
        }
        else if (blockSize == 16) {
            if (end >= 0x7cffff) {
                throw new IOException("No more MAC addresses available in"
                                      + " allotment group 0x400000-0x7FFFFF");
            }
        }
        else {
            throw new IllegalArgumentException(
                "Block size must be 1, 8, or 16;" + " not " + blockSize);
        }

        // Grab the next address
        end++;

        int addr = encryptMACAddress(end);

        byte[] macAddr = new byte[6];

        if (Utils.DEBUG) {
            macAddr[0] = (byte)0x00;
            macAddr[1] = (byte)0x00;
            macAddr[2] = (byte)0x00;
        }
        else {
            macAddr[0] = (byte)0x00;
            macAddr[1] = (byte)0xA0;
            macAddr[2] = (byte)0x1D;
        }
        macAddr[3] = (byte)((addr >> 16) & 0xFF);
        macAddr[4] = (byte)((addr >> 8) & 0xFF);
        macAddr[5] = (byte)(addr & 0xFF);

        range[0] = begin; // This never actually gets changed
        range[1] = end;

        return new MacAddress(macAddr);
    }

    /**
     * Write the new begin/end to the data file.
     * 
     * @param macRoot The root (no extension) of the file to write.
     * @param range The beginning [0] and end [1] of the range, written to the
     *            file.
     * @throws IOException If there was a problem writing to the file.
     */
    private static void writeDataFile(String macRoot, int[] range)
        throws IOException
    {
        File backup = new File(macRoot + EXT_BACKUP);
        File data = new File(macRoot + EXT_DATA);
        File temp = new File(macRoot + EXT_TEMP);

        // Write a temp file which will become a new data file
        try {
            FileOutputStream fout = new FileOutputStream(temp);

            try {
                PrintStream out = new PrintStream(fout);
                out.print(range[0] + " " + range[1]);
            }
            finally {
                fout.close();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Failed to create the temp file '" +
                                  temp.getAbsolutePath() + "': " +
                                  ioe.getMessage(), ioe);
        }

        // Verify the new file
        try {
            int[] verifyRange = readRangeFile(temp.getAbsolutePath());
            if (!Arrays.equals(range, verifyRange)) {
                throw new IOException(
                    "File does not match what was just written");
            }
        }
        catch (IOException ioe) {
            throw new IOException("Failed to verify the temp file '" +
                                  temp.getAbsolutePath() + "': " +
                                  ioe.getMessage(), ioe);
        }

        // Backup the old data file and move the temp file into its place
        if (!backup.delete()) {
            throw new IOException("Failed to delete the old backup file '" +
                                  backup.getAbsolutePath() + "'");
        }

        if (!data.renameTo(backup)) {
            throw new IOException("Failed to create the backup file '" +
                                  backup.getAbsolutePath() + "'");
        }

        if (!temp.renameTo(data)) {
            throw new IOException("Failed to create the new data file '" +
                                  data.getAbsolutePath() + "'");
        }
    }

    /**
     * Check whether the given address (before being turned into a MAC) has been
     * assigned yet.
     * 
     * @param macRoot The root path of the file that the address may have been
     *            assigned from.
     * @param address The address to check.
     * @return <tt>true</tt> if it has been assigned from the file,
     *         <tt>false</tt> if not.
     * @throws IOException If there was a problem reading the file.
     */
    private static boolean addressIsAssigned(String macRoot, int address)
        throws IOException
    {
        // Check whether 'address' is in the range of addresses specified by the
        // data file
        // Don't need to acquire a lock, we are just reading
        int[] range = readRangeFile(macRoot + EXT_DATA);
        return (range[0] <= address && address <= range[1]);
    }

    private static final int BITS = 3;
    private static final int PATTERN = 0x0000AA1D;
    private static final int PATTERN2 = 0x00362C98;
    private static final int PATTERN4 = 0x00362E90;
    private static final int SCRAMBLEMASK = 0x00000007;

    /**
     * Encrypt a MAC address as read from a file containing a range.
     * 
     * @param address The address to encrypt.
     * @return The encrypted address.
     * @throws IOException If there was
     */
    private static int encryptMACAddress(int address)
        throws IOException
    {
        byte[] crypt_array = new byte[8];
        int x, index;
        int encrypted_address, temp;
        int p0, p1, p2, p3, p4, p5, p6, p7;

        logger.debug(String.format("Encrypting MAC address %,d (0x%06x)",
            address, address));

        encrypted_address = -1;

        if (address < 0x200000) {
            logger.debug("  Block 1");

            address ^= PATTERN;

            for (x = 0; x < 8; x++) {
                index = (((x * 3) + 1) % 8);
                crypt_array[index] = (byte)(address & SCRAMBLEMASK);
                address >>= BITS;
            }

            for (x = 0; x < 8; x++) {
                encrypted_address =
                    (encrypted_address << BITS) |
                        (crypt_array[x] & SCRAMBLEMASK);
            }

            encrypted_address ^= PATTERN;
        }
        else if (address < 0x400000) {
            logger.debug("  Block 8");

            temp = address;
            // orig. position new position
            p0 = ((temp >> (BITS * 0)) & SCRAMBLEMASK) << (BITS * 0);
            p1 = ((temp >> (BITS * 7)) & SCRAMBLEMASK) << (BITS * 1);
            p2 = ((temp >> (BITS * 4)) & SCRAMBLEMASK) << (BITS * 2);
            p3 = ((temp >> (BITS * 1)) & SCRAMBLEMASK) << (BITS * 3);
            p4 = ((temp >> (BITS * 6)) & SCRAMBLEMASK) << (BITS * 4);
            p5 = ((temp >> (BITS * 3)) & SCRAMBLEMASK) << (BITS * 5);
            p6 = ((temp >> (BITS * 2)) & SCRAMBLEMASK) << (BITS * 6);
            p7 = ((temp >> (BITS * 5)) & SCRAMBLEMASK) << (BITS * 7);
            temp = p7 | p6 | p5 | p4 | p3 | p2 | p1 | p0;

            encrypted_address = temp ^ PATTERN2;
        }
        else if (address < 0x800000) {
            logger.debug("  Block 16");

            // This range swaps data bits 3 & 21
            int d3, d21;

            d3 = address & 0x00000008; // Get databit 3
            d21 = address & 0x00200000; // Get databit 21
            temp = (d21 >> 18) | (d3 << 18); // Swap them
            temp |= (address & 0x00DFFFF7); // Put the swapped bits back in

            // orig. position new position
            p0 = (((temp >> (BITS * 0)) & SCRAMBLEMASK) << (BITS * 0));
            p1 = (((temp >> (BITS * 7)) & SCRAMBLEMASK) << (BITS * 1));
            p2 = (((temp >> (BITS * 4)) & SCRAMBLEMASK) << (BITS * 2));
            p3 = (((temp >> (BITS * 1)) & SCRAMBLEMASK) << (BITS * 3));
            p4 = (((temp >> (BITS * 6)) & SCRAMBLEMASK) << (BITS * 4));
            p5 = (((temp >> (BITS * 3)) & SCRAMBLEMASK) << (BITS * 5));
            p6 = (((temp >> (BITS * 2)) & SCRAMBLEMASK) << (BITS * 6));
            p7 = (((temp >> (BITS * 5)) & SCRAMBLEMASK) << (BITS * 7));
            temp = p7 | p6 | p5 | p4 | p3 | p2 | p1 | p0;

            encrypted_address = temp ^ PATTERN4;
        }
        else { // Can't use 0x800000 and up yet
            throw new IOException("Number given to encrypt " + address + " (" +
                                  String.format("%06x", address) +
                                  ") is outside allowed range");
        }

        logger.debug(String.format("  %,d (0x%06x)", encrypted_address,
            encrypted_address));

        return encrypted_address;
    }

    /**
     * Decrypt a MAC address to a value that would appear in the range in a
     * file.
     * 
     * @param address The address (lowest 24 bits) to decrypt.
     * @return The value that would appear in the file.
     * @throws IOException If there was a problem decrypting the address.
     */
    private static int decryptMACAddress(int address)
        throws IOException
    {
        logger.debug(String.format("Decrypting address %,d (0x%06x)", address,
            address));

        int rangeIndicator = address & 0x38;
        switch (rangeIndicator) {
            case 0x18:
                logger.debug("  Block 1");
                rangeIndicator = 1; // File containing singles
                break;
            case 0x10:
                logger.debug("  Block 8");
                rangeIndicator = 8; // File containing blocks of 8
                break;
            case 0x08:
            case 0x00:
                logger.debug("  Block 16");
                rangeIndicator = 16; // File containing blocks of 16
                break;
            default:
                throw new IOException("Not a valid address");
        }

        int ret = -1;
        int p0, p1, p2, p3, p4, p5, p6, p7;
        int d3, d21, temp;

        switch (rangeIndicator) {
            case 1:
                address ^= PATTERN;

                p0 = (((address >> (BITS * 6)) & SCRAMBLEMASK) << (BITS * 0));
                p1 = (((address >> (BITS * 3)) & SCRAMBLEMASK) << (BITS * 1));
                p2 = (((address >> (BITS * 0)) & SCRAMBLEMASK) << (BITS * 2));
                p3 = (((address >> (BITS * 5)) & SCRAMBLEMASK) << (BITS * 3));
                p4 = (((address >> (BITS * 2)) & SCRAMBLEMASK) << (BITS * 4));
                p5 = (((address >> (BITS * 7)) & SCRAMBLEMASK) << (BITS * 5));
                p6 = (((address >> (BITS * 4)) & SCRAMBLEMASK) << (BITS * 6));
                p7 = (((address >> (BITS * 1)) & SCRAMBLEMASK) << (BITS * 7));
                ret = p7 | p6 | p5 | p4 | p3 | p2 | p1 | p0;

                ret ^= PATTERN;
                break;
            case 8:
            case 16:
                if (rangeIndicator == 8) {
                    address ^= PATTERN2;
                }
                else {
                    address ^= PATTERN4;
                }

                p0 = ((address >> (BITS * 0)) & SCRAMBLEMASK) << (BITS * 0);
                p1 = ((address >> (BITS * 3)) & SCRAMBLEMASK) << (BITS * 1);
                p2 = ((address >> (BITS * 6)) & SCRAMBLEMASK) << (BITS * 2);
                p3 = ((address >> (BITS * 5)) & SCRAMBLEMASK) << (BITS * 3);
                p4 = ((address >> (BITS * 2)) & SCRAMBLEMASK) << (BITS * 4);
                p5 = ((address >> (BITS * 7)) & SCRAMBLEMASK) << (BITS * 5);
                p6 = ((address >> (BITS * 4)) & SCRAMBLEMASK) << (BITS * 6);
                p7 = ((address >> (BITS * 1)) & SCRAMBLEMASK) << (BITS * 7);
                address = p7 | p6 | p5 | p4 | p3 | p2 | p1 | p0;

                if (rangeIndicator == 16) {
                    // needs d21 swapped with d3
                    temp = 0;
                    d3 = address & 0x00000008; // Get databit 3
                    d21 = address & 0x00200000; // Get databit 21
                    temp = (d21 >> 18) | (d3 << 18); // Swap them

                    // Put the swapped bits back in
                    ret = (address & 0x00DFFFF7) | temp;
                }
                else {
                    ret = address;
                }

                break;
            default:
                throw new IOException("Not a valid address");
        }

        logger.debug(String.format("  %,d (0x%06x)", address, address));

        return ret;
    }
}

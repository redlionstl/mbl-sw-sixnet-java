package com.sixnetio.sixnet;

import com.sixnetio.util.Conversion;

/**
 * Represents a single serial number.
 */
public class SerialNumber
    implements Comparable<SerialNumber>
{
    private final int num;

    /**
     * Construct a new SerialNumber.
     * 
     * @param _num The integer form of the number.
     */
    public SerialNumber(final int _num)
    {
        num = _num;
    }

    /**
     * Construct a new SerialNumber.
     * 
     * @param bytes The bytes of the number, big endian. Only the first 4 are
     *            read. This will not be modified.
     */
    public SerialNumber(final byte[] bytes)
    {
        num = Conversion.bytesToInt(bytes, 0);
    }

    /**
     * Construct a new SerialNumber.
     * 
     * @param s The string representation of the number.
     */
    public SerialNumber(String s)
    {
        num = Integer.parseInt(s);
    }

    /**
     * Get the integer form of this serial number.
     * 
     * @return The integer form of this number.
     */
    public int getSerialNumber()
    {
        return num;
    }

    /**
     * Get the bytes of this number, big endian.
     * 
     * @return The bytes of this number.
     */
    public byte[] getBytes()
    {
        byte[] data = new byte[4];
        Conversion.intToBytes(data, 0, num);
        return data;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(SerialNumber sn)
    {
        if (getSerialNumber() < sn.getSerialNumber()) {
            return -1;
        }
        else if (getSerialNumber() > sn.getSerialNumber()) {
            return 1;
        }
        else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null) {
            return false;
        }
        else if (o instanceof SerialNumber) {
            return (getSerialNumber() == ((SerialNumber)o).getSerialNumber());
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode()
    {
        return getSerialNumber();
    }

    @Override
    public String toString()
    {
        return String.format("%d", getSerialNumber());
    }
}

package com.sixnetio.sixnet;

import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.io.INIParser;
import com.sixnetio.util.Utils;

/**
 * Represents the set of ranges of serial numbers allocated to contract
 * manufacturers. Immutable, except to classes in the same package (for
 * testing).
 */
public class SerialNumberRanges
    implements Iterable<SerialNumberRange>
{
    private static final Logger log = Logger.getLogger(Utils.thisClassName());

    /** Name of the section containing serial number ranges. */
    public static final String SECTION = "Sernum";

    private final Set<SerialNumberRange> ranges =
        new HashSet<SerialNumberRange>();

    /** Construct a new, empty SerialNumberRanges object (for testing). */
    SerialNumberRanges()
    {
        // Nothing to do
    }

    /**
     * Construct a new SerialNumberRanges from a file.
     * 
     * @param path The path to the file.
     * @throws IOException If there was a problem reading the file.
     */
    public SerialNumberRanges(String path)
        throws IOException
    {
        LineNumberReader in = new LineNumberReader(new FileReader(path));
        INIParser parser;
        try {
            parser = new INIParser(in);
        }
        finally {
            try {
                in.close();
            }
            catch (IOException ioe) {
                log.warn("Failed to close reader, ignoring", ioe);
            }
        }

        if (parser.sectionExists(SECTION)) {
            for (String key : parser.getKeys(SECTION)) {
                String value = parser.getValue(SECTION, key);
                String[] lowhighname = value.split(",");
                SerialNumber low = new SerialNumber(lowhighname[0]);
                SerialNumber high = new SerialNumber(lowhighname[1]);
                String name = lowhighname[2];

                SerialNumberRange range =
                    new SerialNumberRange(name, low, high);
                ranges.add(range);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Iterable#iterator()
     */
    public Iterator<SerialNumberRange> iterator()
    {
        return Utils.readOnlyIterator(ranges.iterator());
    }

    /**
     * Search for the SerialNumberRange that contains the given serial number.
     * 
     * @param sn The serial number for whose range to search.
     * @return The matching range, or <code>null</code> if none is found.
     */
    public SerialNumberRange find(SerialNumber sn)
    {
        for (SerialNumberRange range : ranges) {
            if (range.contains(sn)) {
                return range;
            }
        }

        return null;
    }

    /**
     * Add a new SerialNumberRange (for testing).
     * 
     * @param range The range to add.
     */
    void add(SerialNumberRange range)
    {
        ranges.add(range);
    }
}

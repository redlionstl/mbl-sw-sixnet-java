package com.sixnetio.sixnet;

import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.io.INIParser;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;

/**
 * Represents the set of ranges of MAC addresses allocated to contract
 * manufacturers. Immutable, except to classes in the same package (for
 * testing).
 */
public class MacAddressRanges
    implements Iterable<OUI>
{
    private static final Logger log = Logger.getLogger(Utils.thisClassName());

    private final Map<OUI, Set<MacAddressRange>> ranges =
        new HashMap<OUI, Set<MacAddressRange>>();

    /** Construct a new, empty MacAddressRanges object (for testing). */
    MacAddressRanges()
    {
        // Nothing to do
    }

    /**
     * Construct a new MacAddressRanges from a file.
     * 
     * @param path The path to the file.
     * @throws IOException If there was a problem reading the file.
     */
    public MacAddressRanges(String path)
        throws IOException
    {
        LineNumberReader in = new LineNumberReader(new FileReader(path));
        INIParser parser;
        try {
            parser = new INIParser(in);
        }
        finally {
            try {
                in.close();
            }
            catch (IOException ioe) {
                log.warn("Failed to close reader, ignoring", ioe);
            }
        }

        for (String section : parser) {
            if (section.startsWith("OUI_")) {
                // OUI in section title has no delimiters
                String sOUI = section.substring("OUI_".length());
                OUI oui = new OUI(Conversion.hexToBytes(sOUI));
                Set<MacAddressRange> rangeset = new HashSet<MacAddressRange>();

                ranges.put(oui, rangeset);

                for (String key : parser.getKeys(section)) {
                    String value = parser.getValue(section, key);
                    String[] lowhigh = value.split(",");
                    MacAddress low =
                        new MacAddress(oui, Conversion.hexToBytes(lowhigh[0]));
                    MacAddress high =
                        new MacAddress(oui, Conversion.hexToBytes(lowhigh[1]));

                    MacAddressRange range = new MacAddressRange(low, high);
                    rangeset.add(range);
                }
            }

            // Ignore unknown sections
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Iterable#iterator()
     */
    public Iterator<OUI> iterator()
    {
        return Utils.readOnlyIterator(ranges.keySet().iterator());
    }

    /**
     * Get an iterable over the MAC address ranges known for the given OUI.
     * 
     * @param oui The OUI.
     * @return A read-only iterable over the known ranges for that OUI.
     */
    public Iterable<MacAddressRange> iterable(OUI oui)
    {
        return Collections.unmodifiableCollection(ranges.get(oui));
    }

    /**
     * Search for the MacAddressRange that contains the given address.
     * 
     * @param addr The address for whose range to search.
     * @return The matching range, or <code>null</code> if none is found.
     */
    public MacAddressRange find(MacAddress addr)
    {
        for (Set<MacAddressRange> rangeset : ranges.values()) {
            for (MacAddressRange range : rangeset) {
                if (range.contains(addr)) {
                    return range;
                }
            }
        }

        return null;
    }

    /**
     * Add a MacAddressRange to the given OUI (for testing).
     * 
     * @param oui The OUI.
     * @param range The range to add.
     */
    void add(OUI oui, MacAddressRange range)
    {
        Set<MacAddressRange> rangeset = ranges.get(oui);
        if (rangeset == null) {
            rangeset = new HashSet<MacAddressRange>();
            ranges.put(oui, rangeset);
        }

        rangeset.add(range);
    }
}

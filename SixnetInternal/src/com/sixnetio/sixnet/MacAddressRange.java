package com.sixnetio.sixnet;

/**
 * Represents a range of MAC addresses within a single OUI.
 */
public class MacAddressRange
{
    private final OUI oui;
    private final MacAddress low;
    private final MacAddress high;

    /**
     * Construct a new MAC address range.
     * 
     * @param _low The low end of the range, inclusive.
     * @param _high The high end of the range, inclusive.
     * @throws IllegalArgumentException If the low and high addresses have
     *             different OUIs, or if the low address is larger than the high
     *             address.
     */
    public MacAddressRange(final MacAddress _low, final MacAddress _high)
        throws IllegalArgumentException
    {
        low = _low;
        high = _high;

        oui = low.getOUI();
        if (!oui.equals(high.getOUI())) {
            throw new IllegalArgumentException(
                "Low and high MAC addresses must have the same OUI.");
        }
        else if (low.compareTo(high) > 0) {
            throw new IllegalArgumentException(
                "Low address is larger than high address.");
        }
    }

    /**
     * Get the OUI of the addresses in this range.
     * 
     * @return The OUI of the addresses.
     */
    public OUI getOUI()
    {
        return oui;
    }

    /**
     * Get the low address of this range.
     * 
     * @return The low address, inclusive.
     */
    public MacAddress getLowAddress()
    {
        return low;
    }

    /**
     * Get the high address of this range.
     * 
     * @return The high address, inclusive.
     */
    public MacAddress getHighAddress()
    {
        return high;
    }

    /**
     * Test whether a specific address is within this range.
     * 
     * @param mac The address to test.
     * @return True if the address is within this range. False otherwise.
     */
    public boolean contains(MacAddress mac)
    {
        return (getLowAddress().compareTo(mac) <= 0 && mac
            .compareTo(getHighAddress()) <= 0);
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null) {
            return false;
        }
        else if (o instanceof MacAddressRange) {
            MacAddressRange r = (MacAddressRange)o;
            return (getLowAddress().equals(r.getLowAddress()) && getHighAddress()
                .equals(r.getHighAddress()));
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode()
    {
        return (getLowAddress().hashCode() ^ getHighAddress().hashCode());
    }

    @Override
    public String toString()
    {
        return String.format("%s - %s", getLowAddress(), getHighAddress());
    }
}

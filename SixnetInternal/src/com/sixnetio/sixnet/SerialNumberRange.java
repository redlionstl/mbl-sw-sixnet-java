package com.sixnetio.sixnet;

/**
 * Represents a range of serial numbers.
 */
public class SerialNumberRange
{
    private final String name;
    private final SerialNumber low;
    private final SerialNumber high;

    /**
     * Construct a new Serial Number Range.
     * 
     * @param _name The name of the range.
     * @param _low The low end of the range, inclusive.
     * @param _high The high end of the range, inclusive.
     */
    public SerialNumberRange(final String _name, final SerialNumber _low,
                             final SerialNumber _high)
    {
        name = _name;
        low = _low;
        high = _high;
    }

    /**
     * Get the name of this range.
     * 
     * @return The name of this range.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Get the low end of this range.
     * 
     * @return The low end, inclusive, of this range.
     */
    public SerialNumber getLowSerial()
    {
        return low;
    }

    /**
     * Get the high end of this range.
     * 
     * @return The high end, inclusive, of this range.
     */
    public SerialNumber getHighSerial()
    {
        return high;
    }

    /**
     * Test whether a serial number is in this range.
     * 
     * @param sn The number to test.
     * @return True if the number is in this range. False otherwise.
     */
    public boolean contains(SerialNumber sn)
    {
        return (getLowSerial().compareTo(sn) <= 0 && sn
            .compareTo(getHighSerial()) <= 0);
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null) {
            return false;
        }
        else if (o instanceof SerialNumberRange) {
            SerialNumberRange snr = (SerialNumberRange)o;
            return (getLowSerial().equals(snr.getLowSerial()) && getHighSerial()
                .equals(snr.getHighSerial()));
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode()
    {
        return (getLowSerial().hashCode() ^ getHighSerial().hashCode());
    }

    @Override
    public String toString()
    {
        return String.format("%s - %s (%s)", getLowSerial(), getHighSerial(),
            getName());
    }
}

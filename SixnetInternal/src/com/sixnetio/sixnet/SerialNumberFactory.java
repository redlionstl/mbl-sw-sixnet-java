/*
 * SerNum.java
 *
 * This class provides a function for acquiring new serial numbers in accordance
 * with w:\tq_doc\eng\psernum.r03.doc
 *
 * Jonathan Pearson
 * January 9, 2008
 *
 */

package com.sixnetio.sixnet;

import java.io.*;
import java.util.StringTokenizer;

import com.sixnetio.util.UserInterface;
import com.sixnetio.util.Utils;

/**
 * Provides a static function, @link{getSerialNumber()}, which will acquire a
 * single unique serial number. Should work in Windows or Linux, as long as the
 * W drive is mapped properly (w: in Windows, /w in Linux).
 * 
 * @author Jonathan Pearson
 */
public class SerialNumberFactory
{
    // Constants
    private static final String SER_ROOT;
    private static final String SER_HIGH_ROOT;

    private static final int LOCK_TIMEOUT = 15; // Number of seconds before
    // giving up on trying to
    // acquire the lock

    private static final String EXT_DATA = ".dat";
    private static final String EXT_BACKUP = ".bak";
    private static final String EXT_LOCK = ".lck";
    private static final String EXT_TEMP = ".tmp";

    // static initialization
    static {
        if (Utils.DEBUG) {
            if (Utils.osIsWindows()) {
                SER_ROOT = "c:\\temp\\ser_num";
                SER_HIGH_ROOT = "c:\\temp\\UDR_high_sernum";
            }
            else {
                SER_ROOT = "/tmp/ser_num";
                SER_HIGH_ROOT = "/tmp/UDR_high_sernum";
            }
        }
        else {
            if (Utils.osIsWindows()) {
                SER_ROOT = "w:\\mfg\\test\\working\\ser_num";
                SER_HIGH_ROOT = "w:\\mfg\\test\\working\\UDR_high_sernum";
            }
            else {
                SER_ROOT = "/w/mfg/test/working/ser_num";
                SER_HIGH_ROOT = "/w/mfg/test/working/UDR_high_sernum";
            }
        }
    }

    protected static String getSerHighDataFile()
    {
        String ret;
        ret = SER_HIGH_ROOT + EXT_DATA;
        return ret;
    }

    protected static String getSerLowDataFile()
    {
        String ret;
        ret = SER_ROOT + EXT_DATA;
        return ret;
    }

    // getSerialNumber()
    /**
     * Get a single unique serial number.
     * 
     * @return A serial number.
     * @throws IOException When an error occurs, before any change is made to
     *             the data file containing the serial numbers.
     */
    public static SerialNumber getSerialNumber(UserInterface ui)
        throws IOException
    {
        Utils.debug("Acquiring new low serial number");

        File lockFile = new File(SER_ROOT + EXT_LOCK);

        Utils.acquireLock(lockFile, LOCK_TIMEOUT);

        try { // Finally clause deletes lock file
            // Read the data file
            BufferedReader in =
                new BufferedReader(new FileReader(SER_ROOT + EXT_DATA));
            String lineIn = in.readLine();
            in.close();

            if (lineIn == null) {
                throw new IOException("Empty serial number file");
            }

            // Split the line we read
            StringTokenizer tok = new StringTokenizer(lineIn);
            if (tok.countTokens() != 2) {
                throw new IOException("Serial number data file  '" + SER_ROOT +
                                      EXT_DATA + "' has bad format");
            }

            int[] vals = new int[2];
            for (int i = 0; i < vals.length; i++) {
                vals[i] = Integer.parseInt(tok.nextToken());
            }

            SerialNumber serial = calculateSerialNumber(vals);

            writeDataFile(SER_ROOT, vals);

            return serial;
        }
        finally {
            if (!lockFile.delete()) {
                Utils
                    .debug(
                        "Unable to delete lock file '%s', this is likely to cause problems...\n",
                        lockFile.getAbsolutePath());
            }
        }
    }

    // getHighSerialNumber()
    /**
     * Get a single unique serial number.
     * 
     * @return A serial number.
     * @throws IOException When an error occurs, before any change is made to
     *             the data file containing the serial numbers.
     */
    public static SerialNumber getHighSerialNumber(UserInterface ui)
        throws IOException
    {
        Utils.debug("Acquiring new high serial number");

        File lockFile = new File(SER_HIGH_ROOT + EXT_LOCK);

        Utils.acquireLock(lockFile, LOCK_TIMEOUT);

        try { // Finally clause deletes lock file
            // Read the data file
            BufferedReader in =
                new BufferedReader(new FileReader(SER_HIGH_ROOT + EXT_DATA));
            String lineIn = in.readLine();
            in.close();

            if (lineIn == null) {
                throw new IOException("Empty serial number file");
            }

            // Split the line we read
            StringTokenizer tok = new StringTokenizer(lineIn);
            if (tok.countTokens() != 3) {
                throw new IOException("Serial number data file '" +
                                      SER_HIGH_ROOT + EXT_DATA +
                                      "' has bad format");
            }

            int[] vals = new int[3];
            for (int i = 0; i < vals.length; i++) {
                vals[i] = Integer.parseInt(tok.nextToken());
            }

            Utils.debug("vals[0] = " + vals[0] + "\n" + "vals[1] = " + vals[1] +
                        "\n" + "vals[2] = " + vals[2]);

            int serial = vals[0] + vals[2];
            vals[2]++;

            if (serial >= vals[1]) {
                throw new IOException("There are no serial numbers left in '" +
                                      SER_HIGH_ROOT + EXT_DATA + "'");
            }
            else if (serial + 1000 >= vals[1]) {
                if (ui != null) {
                    ui.displayMessage(null, UserInterface.M_WARNING,
                        "There are only " + (vals[1] - serial) +
                            " serial numbers remaining in '" + SER_HIGH_ROOT +
                            EXT_DATA + "'");
                }
            }

            writeDataFile(SER_HIGH_ROOT, vals);

            return new SerialNumber(serial);
        }
        finally {
            if (!lockFile.delete()) {
                Utils
                    .debug(
                        "Unable to delete lock file '%s', this is likely to cause problems...\n",
                        lockFile.getAbsolutePath());
            }
        }
    }

    // isLegalSerial()
    /**
     * Checks whether a single serial number is in the legal range.
     * 
     * @param serial The number to check.
     * @return True if it has been assigned, false if not legal/not assigned
     */
    public static boolean isLegalSerial(SerialNumber serial)
        throws IOException
    {
        if (serial.getSerialNumber() == 0 ||
            serial.getSerialNumber() == 0xffffffff) {
            return false;
        }

        int val = decrypt(serial);
        return numberAssigned(SER_ROOT, val);
    }

    // isLegalHighSerial()
    /**
     * Checks whether a single serial number is in the legal range.
     * 
     * @param serial The number to check.
     * @param cmRanges The Contract Manufacturer (CM) allocated ranges. The
     *            serial is checked against this if it cannot be found in the
     *            database of allocated numbers.
     * @return True if it has been assigned, false if not legal/not assigned
     */
    public static boolean isLegalHighSerial(SerialNumber serial,
                                            SerialNumberRanges cmRanges)
        throws IOException
    {
        if (serial.getSerialNumber() == 0 ||
            serial.getSerialNumber() == 0xffffffff) {
            return false;
        }

        if (highNumberAssigned(SER_HIGH_ROOT, serial)) {
            return true;
        }

        // Test whether it may have been assigned by a different contract
        // manufacturer (CM)
        return (cmRanges.find(serial) != null);
    }

    // calculateSerialNumber()
    private static SerialNumber calculateSerialNumber(int[] vals)
        throws IOException
    {
        int begin = vals[0];
        int end = vals[1];

        SerialNumber serial = encrypt(end);

        if (end != decrypt(serial)) {
            throw new IOException(
                "Calculation of new serial number failed: verification failed");
        }

        end++;

        vals[0] = begin;
        vals[1] = end;

        return serial;
    }

    // encrypt()
    private static final int NIBBLEMASK = 0x0000000F;

    public static SerialNumber encrypt(int sernum)
    {
        byte[] crypt_array = new byte[8];
        int x, index;
        int encrypted_serial_number;

        Utils.debug(String.format("Encrypting serial number %,d (0x%08x)",
            sernum, sernum));

        encrypted_serial_number = 0;

        for (x = 0; x < 8; x++) {
            index = (((x * 3) + 1) % 8);
            crypt_array[index] = (byte)(sernum & NIBBLEMASK);
            sernum >>= 4;
        }

        for (x = 0; x < 8; x++) {
            encrypted_serial_number =
                (encrypted_serial_number << 4) | (crypt_array[x] & NIBBLEMASK);
        }

        Utils.debug(String.format("  %,d (0x%08x)", encrypted_serial_number,
            encrypted_serial_number));

        return new SerialNumber(encrypted_serial_number);
    }

    // decrypt()
    // For verification
    private static int decrypt(SerialNumber sn)
    {
        int candidate = sn.getSerialNumber();
        int ret;
        int nib0, nib1, nib2, nib3, nib4, nib5, nib6, nib7;

        Utils.debug(String.format("Decrypting serial number %,d (0x%08x)",
            candidate, candidate));

        nib0 = (((candidate >> 24) & NIBBLEMASK) << 0);
        nib1 = (((candidate >> 12) & NIBBLEMASK) << 4);
        nib2 = (((candidate >> 0) & NIBBLEMASK) << 8);
        nib3 = (((candidate >> 20) & NIBBLEMASK) << 12);
        nib4 = (((candidate >> 8) & NIBBLEMASK) << 16);
        nib5 = (((candidate >> 28) & NIBBLEMASK) << 20);
        nib6 = (((candidate >> 16) & NIBBLEMASK) << 24);
        nib7 = (((candidate >> 4) & NIBBLEMASK) << 28);

        ret = nib7 | nib6 | nib5 | nib4 | nib3 | nib2 | nib1 | nib0;

        Utils.debug(String.format("  %,d (0x%08x)", ret, ret));

        return ret;
    }

    // writeDataFile()
    private static void writeDataFile(String serRoot, int[] vals)
        throws IOException
    {
        File backup = new File(serRoot + EXT_BACKUP);
        File data = new File(serRoot + EXT_DATA);
        File temp = new File(serRoot + EXT_TEMP);

        // Write a temp file which will become a new data file
        try {
            PrintStream out = new PrintStream(new FileOutputStream(temp));

            for (int i = 0; i < vals.length; i++) {
                out.print(vals[i]);
                if (i < vals.length - 1) {
                    out.print(" ");
                }
            }

            out.close();
        }
        catch (IOException ioe) {
            throw new IOException("Failed to create the temp file '" +
                                  temp.getAbsolutePath() + "': " +
                                  ioe.getMessage());
        }

        // Verify the new file
        String lineIn;
        try {
            BufferedReader in = new BufferedReader(new FileReader(temp));
            lineIn = in.readLine();
            in.close();
        }
        catch (IOException ioe) {
            throw new IOException("Failed to verify the temp file '" +
                                  temp.getAbsolutePath() + "': " +
                                  ioe.getMessage());
        }

        if (lineIn == null) {
            throw new IOException("Empty serial number file");
        }

        StringTokenizer tok = new StringTokenizer(lineIn);

        if (tok.countTokens() != vals.length) {
            throw new IOException("Failed to verify the temp file '" +
                                  temp.getAbsolutePath() +
                                  "': wrong number of tokens");
        }

        while (tok.hasMoreTokens()) {
            try {
                Integer.parseInt(tok.nextToken());
            }
            catch (NumberFormatException nfe) {
                throw new IOException("Failed to verify the temp file '" +
                                      temp.getAbsolutePath() +
                                      "': tokens did not parse into integers");
            }
        }

        // Backup the old data file and move the temp file into its place
        if (backup.exists()) {
            if (!backup.delete()) {
                throw new IOException("Failed to delete the old backup file '" +
                                      backup.getAbsolutePath() + "'");
            }
        }
        if (!data.renameTo(backup)) {
            throw new IOException("Failed to create the backup file '" +
                                  backup.getAbsolutePath() + "'");
        }
        if (!temp.renameTo(data)) {
            throw new IOException("Failed to create the new data file '" +
                                  data.getAbsolutePath() + "'");
        }
    }

    // numberAssigned()
    private static boolean numberAssigned(String serRoot, int serial)
        throws IOException
    {
        // Check whether 'serial' is in the range of numbers specified by the
        // data file
        // Don't need to acquire a lock, we are just reading
        BufferedReader in =
            new BufferedReader(new FileReader(serRoot + EXT_DATA));
        String lineIn = in.readLine();
        in.close();

        if (lineIn == null) {
            throw new IOException("Empty serial number file");
        }

        // Split the line we read
        StringTokenizer tok = new StringTokenizer(lineIn);
        if (tok.countTokens() != 2) {
            throw new IOException("Serial number data file '" + serRoot +
                                  EXT_DATA + "' has bad format");
        }

        int[] vals = new int[2];
        for (int i = 0; i < vals.length; i++) {
            vals[i] = Integer.parseInt(tok.nextToken());
        }

        return (vals[0] <= serial && serial <= vals[1]);
    }

    // highNumberAssigned()
    private static boolean highNumberAssigned(String serRoot,
                                              SerialNumber serial)
        throws IOException
    {
        // Check whether 'serial' is in the range of numbers specified by the
        // data file
        // Don't need to acquire a lock, we are just reading
        BufferedReader in =
            new BufferedReader(new FileReader(serRoot + EXT_DATA));
        String lineIn = in.readLine();
        in.close();

        if (lineIn == null) {
            throw new IOException("Empty serial number file");
        }

        // Split the line we read
        StringTokenizer tok = new StringTokenizer(lineIn);
        if (tok.countTokens() != 3) {
            throw new IOException("Serial number data file '" + serRoot +
                                  EXT_DATA + "' has bad format");
        }

        int[] vals = new int[3];
        for (int i = 0; i < vals.length; i++) {
            vals[i] = Integer.parseInt(tok.nextToken());
        }

        return (vals[0] <= serial.getSerialNumber() && serial.getSerialNumber() <= vals[0] +
                                                                                   vals[2]);
    }
}

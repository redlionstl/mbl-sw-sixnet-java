/*
 * MacProvider.java
 *
 * The superclass for all MAC address providers.
 *
 * Jonathan Pearson
 * November 2, 2009
 *
 */

package com.sixnetio.sixnet;

import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.util.Ref;
import com.sixnetio.util.Utils;


/**
 * The superclass for all MAC address providers. Each individual provider should
 * have a no-argument constructor which will throw an exception if it is unable
 * to provide addresses (for example, a database-backed provider that cannot
 * locate the database).
 * 
 * @author Jonathan Pearson
 */
abstract class MacProvider
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /** The timeout before failing a query (seconds). */
    public static final int QUERY_TIMEOUT = 15;

    /** The registered MAC providers. */
    private static final Map<Integer, Class<? extends MacProvider>> registeredProviders =
        new HashMap<Integer, Class<? extends MacProvider>>();

    static {
        // Load the known providers
        // If there was a void() method or something which just allows you to
        // be certain that the class has been loaded, that would be preferred,
        // but this works
        MacDB.registerProvider();
        MacFile.registerProvider();
    }

    /**
     * Register a new MAC provider.
     * 
     * @param priority The priority of the new provider.
     * @param clazz The class that implements MacProvider.
     */
    public static void registerProvider(int priority,
                                        Class<? extends MacProvider> clazz)
    {
        synchronized (registeredProviders) {
            if (registeredProviders.containsKey(priority)) {
                throw new IllegalStateException(String.format(
                    "MAC provider '%s' already registered with"
                        + " priority %d, unable to register provider '%s'",
                    registeredProviders.get(priority).getName(), priority,
                    clazz.getName()));
            }

            registeredProviders.put(priority, clazz);
        }
    }

    /**
     * Retrieve the registered providers in a map of priority onto the class
     * that implements MacProvider.
     */
    public static Map<Integer, Class<? extends MacProvider>> getRegisteredProviders()
    {
        synchronized (registeredProviders) {
            Map<Integer, Class<? extends MacProvider>> copy =
                new HashMap<Integer, Class<? extends MacProvider>>();
            copy.putAll(registeredProviders);

            return copy;
        }
    }

    /**
     * Retrieve a new instance of the registered MacProvider implementation with
     * the best (numerically lowest) priority that is able to serve MAC
     * addresses.
     * 
     * @return A new MacProvider, or <tt>null</tt> if there are no registered
     *         providers which are able to server MAC addresses.
     */
    public static MacProvider getMacProvider()
    {
        synchronized (registeredProviders) {
            // Sort the priorities
            Integer[] priorities =
                registeredProviders.keySet().toArray(
                    new Integer[registeredProviders.size()]);
            Arrays.sort(priorities);

            // Loop through the priorities, trying to construct each provider
            // The first that succeeds without error is returned
            for (int priority : priorities) {
                Class<? extends MacProvider> clazz =
                    registeredProviders.get(priority);

                try {
                    return clazz.newInstance();
                }
                catch (Exception e) {
                    logger.debug("Unable to instantiate " + clazz.getName(), e);
                }
            }

            // Failed to find one that works
            return null;
        }
    }

    /**
     * Tests a MAC address against all providers to see if it has been given
     * out.
     * 
     * @param macAddr [IN, OUT] On input, the MAC address to validate. If valid
     *            according to any provider, and that provider is able, this
     *            will be set to the first address in the block. If the provider
     *            is unable to look up the beginning of the block, this will be
     *            set to <code>null</code>.
     * @param out_blockSize [OUT] If the address is valid according to any
     *            provider, and lookup is supported by the provider, will be set
     *            to the size of the allocated block. If lookup is not
     *            supported, will be set to <code>null</code>.
     * @return <tt>true</tt> if the block is valid (has been allocated)
     *         according to any provider, <tt>false</tt> if no provider is able
     *         to verify the address.
     */
    public static boolean verifyMACAddressAcrossProviders(
                                                          Ref<MacAddress> macAddr,
                                                          Ref<Integer> out_blockSize)
    {
        // Pull out all providers, to avoid locking the map for a long time
        List<Class<? extends MacProvider>> providers;
        synchronized (registeredProviders) {
            providers =
                new ArrayList<Class<? extends MacProvider>>(registeredProviders
                    .size());

            for (Class<? extends MacProvider> providerClass : registeredProviders
                .values()) {

                providers.add(providerClass);
            }
        }

        // Loop through each provider, testing its verifyMACAddress function
        for (Class<? extends MacProvider> providerClass : providers) {
            // Instantiate this provider
            MacProvider provider;
            try {
                provider = providerClass.newInstance();
            }
            catch (Exception e) {
                logger.debug("Unable to instantiate provider " +
                             providerClass.getName(), e);

                continue;
            }

            // Make sure we close the provider
            try {
                // Copy the address, we do not want to overwrite it
                try {
                    Ref<MacAddress> ma = macAddr.clone();
                    if (provider.verifyMACAddress(ma, out_blockSize)) {
                        macAddr.set(ma.get());
                        return true;
                    }
                }
                catch (Exception e) {
                    logger.debug("Unable to verify the address through " +
                                 provider.getClass().getName(), e);
                }
            }
            finally {
                provider.close();
            }
        }

        return false;
    }

    /**
     * Allocate a new MAC address block, if one of the proper size is available.
     * The block will be aligned according to the block size.
     * 
     * @param blockSize The block size to allocate; must be a power of 2.
     * @return The MAC addresses that have been allocated, or <tt>null</tt> if
     *         none could be allocated of the specified block size.
     * @throws Exception If there was a problem allocating the block.
     */
    public abstract MacAddress[] getNewMACAddrBlock(int blockSize)
        throws Exception;

    /**
     * Given a MAC address, looks up the block the MAC address belongs to.
     * 
     * @param macAddr [IN, OUT] On input, the MAC address to validate. If valid,
     *            and lookup is supported by the provider, this will be set to
     *            the first address in the block on output. If lookup is not
     *            supported, it will be set to <code>null</code>.
     * @param out_blockSize [OUT] If the address is valid and lookup is
     *            supported by the provider, will be set to the size of the
     *            allocated block. If lookup is not supported, will be set to
     *            <code>null</code>.
     * @return <tt>true</tt> if the block is valid, <tt>false</tt> otherwise.
     * @throws Exception If there was a problem verifying the block.
     */
    protected abstract boolean verifyMACAddress(Ref<MacAddress> macAddr,
                                                Ref<Integer> out_blockSize)
        throws Exception;

    /**
     * Estimate the number of addresses remaining.
     * 
     * @param blockSize The block size to estimate for.
     * @return The approximate number of addresses remaining for the given block
     *         size.
     * @throws Exception If there was a problem calculating the estimate.
     */
    public abstract int estimateRemainingAddresses(int blockSize)
        throws Exception;

    /**
     * This should be called when the provider is no longer needed.
     */
    public abstract void close();
}

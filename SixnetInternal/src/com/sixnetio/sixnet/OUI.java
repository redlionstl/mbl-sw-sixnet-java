package com.sixnetio.sixnet;

import java.util.Arrays;

import com.sixnetio.util.Comparer;
import com.sixnetio.util.Conversion;

/**
 * Represents an OUI (Organizationally Unique Identifier). This is the first
 * three bytes of a MAC address.
 */
public class OUI
    implements Comparable<OUI>
{
    private final byte[] data;

    /**
     * Construct a new OUI.
     * 
     * @param _data The bytes of the OUI. Must be at least three bytes long.
     *            Only the first three bytes will be used. This value will not
     *            be modified.
     */
    public OUI(final byte[] _data)
    {
        data = new byte[3];
        System.arraycopy(_data, 0, data, 0, 3);
    }

    /**
     * Construct a new OUI from its string representation.
     * 
     * @param s A string in the format "03:57:9b". Case-insensitive.
     */
    public OUI(final String s)
    {
        data = Conversion.hexToBytes(s, ":");
    }

    /**
     * Get the bytes of this OUI.
     * 
     * @return The bytes of this OUI.
     */
    public byte[] getBytes()
    {
        return Arrays.copyOf(data, 3);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(final OUI oui)
    {
        return Comparer.compareUnsigned(getBytes(), oui.getBytes());
    }

    @Override
    public boolean equals(final Object o)
    {
        if (o == null) {
            return false;
        }
        else if (o instanceof OUI) {
            return (compareTo((OUI)o) == 0);
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode()
    {
        final byte[] local = getBytes();
        return (((local[0] & 0xff) << 16) | ((local[1] & 0xff) << 8) | ((local[2] & 0xff) << 0));
    }

    @Override
    public String toString()
    {
        return Conversion.bytesToHex(getBytes(), ":");
    }
}

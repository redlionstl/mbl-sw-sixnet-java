package com.sixnetio.sixnet;

import java.util.Arrays;

import com.sixnetio.util.Comparer;
import com.sixnetio.util.Conversion;

/**
 * Represents a MAC (Media Access Control) address.
 */
public class MacAddress
    implements Comparable<MacAddress>
{
    /** Length of an Ethernet MAC address, in bytes. */
    public static final int ETH_ALEN = 6;

    /** A MAC address consisting of all 0-bits. */
    public static final MacAddress ALL_ZEROS =
        new MacAddress("00:00:00:00:00:00");

    /** A MAC address consisting of all 1-bits (0xff bytes). */
    public static final MacAddress ALL_ONES =
        new MacAddress("ff:ff:ff:ff:ff:ff");

    private final byte[] data;

    /**
     * Construct a new MAC address from a byte array.
     * 
     * @param _data The bytes of the MAC address. Must be at least 6 bytes long.
     *            Only the first 6 bytes will be used.
     */
    public MacAddress(final byte[] _data)
    {
        data = new byte[ETH_ALEN];
        System.arraycopy(_data, 0, data, 0, data.length);
    }

    /**
     * Construct a new MAC address from its OUI and lower bytes.
     * 
     * @param oui The OUI.
     * @param low The lower bytes. Must be at least 3 bytes long. Only the first
     *            3 bytes will be used.
     */
    public MacAddress(final OUI oui, final byte[] low)
    {
        data = new byte[ETH_ALEN];
        System.arraycopy(oui.getBytes(), 0, data, 0, 3);
        System.arraycopy(low, 0, data, 3, ETH_ALEN - 3);
    }

    /**
     * Construct a new MAC address from its string representation.
     * 
     * @param s The string representation, in the format "01:23:45:67:89:ab".
     *            Case-insensitive.
     */
    public MacAddress(final String s)
    {
        data = Conversion.hexToBytes(s, ":");
    }

    /**
     * Get the bytes of this MAC address.
     * 
     * @return The bytes of this MAC address.
     */
    public byte[] getBytes()
    {
        return Arrays.copyOf(data, data.length);
    }

    /**
     * Get the OUI of this MAC address.
     * 
     * @return The OUI of this MAC address.
     */
    public OUI getOUI()
    {
        return new OUI(data);
    }

    /**
     * Increment this address by the given offset.
     * 
     * @param offset The number of positions to increment the MAC by.
     * @return The updated MAC address.
     * @throws IllegalArgumentException If the increment pushes this address out
     *             of the current OUI.
     */
    public MacAddress incrementBy(int offset)
        throws IllegalArgumentException
    {
        // Grab the low three bytes (we do not allow incrementing the OUI)
        byte[] bytes = getBytes();
        int tempMAC = Conversion.bytesToInt(bytes, 2) & 0xffffff;

        // Modify it as an integer
        tempMAC += offset;

        if ((tempMAC & 0xff000000) != 0) {
            throw new IllegalArgumentException(
                "Cannot arithmatically modify a MAC's OUI");
        }

        // Put back into byte form
        byte tempByte = bytes[2]; // About to overwrite it, but we need it
        Conversion.intToBytes(bytes, 2, tempMAC);
        bytes[2] = tempByte;

        return new MacAddress(bytes);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(final MacAddress mac)
    {
        return Comparer.compareUnsigned(getBytes(), mac.getBytes());
    }

    @Override
    public boolean equals(final Object o)
    {
        if (o == null) {
            return false;
        }
        else if (o instanceof MacAddress) {
            return (compareTo((MacAddress)o) == 0);
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode()
    {
        // Lower bytes (upper ones are less likely to be different)
        return Conversion.bytesToInt(getBytes(), 2);
    }

    @Override
    public String toString()
    {
        return Conversion.bytesToHex(getBytes(), ":");
    }
}

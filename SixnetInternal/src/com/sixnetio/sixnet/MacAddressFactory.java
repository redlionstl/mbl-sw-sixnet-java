/*
 * MacAddr.java
 *
 * This class provides a function for acquiring new MAC addresses
 *
 * Jonathan Pearson
 * January 9, 2008
 *
 */

package com.sixnetio.sixnet;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.*;

/**
 * Provides a static function, @link{getMACAddresses(int)}, which will acquire a
 * number of unique addresses. Should work in Windows or Linux, as long as the W
 * drive is mapped properly (w: in Windows, /w in Linux). Will get contiguous
 * address ranges of size 1, 8, or 16. May allow larger ranges (if backed by a
 * database). If a request for a large block fails, try again with multiple
 * smaller block sizes (if that will work for the product), or fail.
 * 
 * @author Jonathan Pearson
 */
public class MacAddressFactory
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /** The number of addresses beneath which we will start warning the user. */
    private static final int REMAINING_THRESHOLD = 16384;

    /**
     * Get the specified number of MAC addresses.
     * 
     * @param howMany The number of MAC addresses to acquire. This may be
     *            rounded up to a larger block size, if necessary.
     * @return An array of MAC addresses.
     * @throws IOException If there is a problem acquiring addresses.
     */
    public static MacAddress[] getMACAddresses(UserInterface ui, int howMany)
        throws IOException
    {
        if (howMany == 0) {
            throw new IOException("Cannot allocate 0 addresses");
        }

        MacProvider provider = MacProvider.getMacProvider();
        if (provider == null) {
            throw new IOException(
                "The system does not have the ability to provide MAC addresses");
        }

        // Make sure we close the provider
        try {
            // Make sure that 'howMany' is a power of 2, rounding up as
            // necessary
            // Round up to the nearest power of 2
            // Algorithm thanks to
            // http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
            // Basically, it sets all of the bits below the highest bit, and
            // then adds one
            // It avoids rounding up when unnecessary by subtracting one at
            // the beginning
            howMany--;
            howMany |= howMany >>> 1;
            howMany |= howMany >>> 2;
            howMany |= howMany >>> 4;
            howMany |= howMany >>> 8;
            howMany |= howMany >>> 16;
            howMany++;

            MacAddress[] macAddresses;
            try {
                macAddresses = provider.getNewMACAddrBlock(howMany);
            }
            catch (Exception e) {
                throw new IOException("Unable to acquire " + howMany +
                                      " MAC addresses: " + e.getMessage(), e);
            }

            if (macAddresses == null) {
                throw new IOException(
                    "Not enough MAC addresses left to satisfy the request for " +
                        howMany);
            }

            // Verify that we are not running short on addresses
            int remaining;
            try {
                remaining =
                    provider.estimateRemainingAddresses(macAddresses.length);

                if (remaining < REMAINING_THRESHOLD) {
                    ui
                        .displayMessage(
                            null,
                            UserInterface.M_WARNING,
                            String
                                .format(
                                    "Warning: Only around %d MAC addresses left in allotment group",
                                    remaining));
                }
            }
            catch (Exception e) {
                logger.warn(
                    "Unable to determine the number of remaining addresses", e);

                ui
                    .displayMessage(
                        null,
                        UserInterface.M_WARNING,
                        "Warning: Unable to determine the number of remaining addresses: " +
                            e.getMessage());
            }

            return macAddresses;
        }
        finally {
            provider.close();
        }
    }

    /**
     * Checks whether a single MAC address is in one of the legal ranges.
     * 
     * @param address The address to check.
     * @param blockSize [OUT] If the address is valid (has been allocated), and
     *            the block size can be determined, this will be set to the
     *            block size. Otherwise, will be set to <code>null</code>.
     * @param cmRanges The ranges of addresses allocated to Contract
     *            Manufacturers (CMs). These will be checked if the address
     *            cannot be found in the local database.
     * @return True if the address has been allocated. It counts as having been
     *         allocated if it resides in a range assigned to a CM.
     * @throws IOException If there is a problem verifying the address.
     */
    public static boolean isLegalAddress(MacAddress address,
                                         Ref<Integer> blockSize,
                                         MacAddressRanges cmRanges)
        throws IOException
    {
        Ref<MacAddress> mac = new Ref<MacAddress>(address);
        blockSize.set(null);

        logger.debug("Testing address " + address + " through providers");
        if (MacProvider.verifyMACAddressAcrossProviders(mac, blockSize)) {
            return true;
        }

        logger.debug("Not found in any provider, testing CM ranges");
        boolean found = (cmRanges.find(address) != null);

        if (found) {
            logger.debug("Found address " + address + " in a CM range");
        }
        else {
            logger.debug("Unable to locate " + address + " anywhere");
        }

        return found;
    }
}

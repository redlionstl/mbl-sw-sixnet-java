package com.sixnetio.sixnet;

import java.io.*;

import junit.framework.TestCase;

import org.junit.*;

import com.sixnetio.util.Utils;

/**
 * SerNumTest
 * 
 * The main focus here is the 'getSerialNumber' and 'getHighSerialNumber'
 * functions. Temporary/known data is provided and the tests ensure that the
 * returned data is correct.
 * 
 * 'IsLegalSerial()' and 'IsLegalHighSerial()' are also tested using the
 * known/temporary data.
 * 
 */
public class SerialNumberFactoryTest
    extends TestCase
{
    public static String dataFileLow, dataFileHigh;

    private String dataLow = "268435439 268560719";
    private SerialNumber expectedSerNumLow = new SerialNumber(0x0fe04119);
    private String dataHigh = "5000000 9999999 11632";
    private SerialNumber expectedSerNumHigh = new SerialNumber(0x004c78b0);

    /**
     * If Utils.DEBUG is not set to true, we will be using the live data on the
     * network instead of a temporary folder locally. Utils.DEBUG is set when a
     * file "DEBUG" exists. It is deleted when done with the test.
     */
    static {
        try {
            File dbg = new File("DEBUG");
            dbg.createNewFile();
        }
        catch (IOException e) {
            fail("Unable to create debug file.");
        }
    }

    @Override
    @Before
    public void setUp()
        throws Exception
    {
        if (!Utils.DEBUG) {
            throw new Exception(
                "Utils.DEBUG must be 'true'. Cannot use live data for test.");
        }
        else {
            dataFileLow = SerialNumberFactory.getSerLowDataFile();
            dataFileHigh = SerialNumberFactory.getSerHighDataFile();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @Override
    @After
    public void tearDown()
        throws Exception
    {
        File dbg = new File("DEBUG");
        if (dbg.exists()) {
            dbg.delete();
        }
    }

    // /////////////////////////////////////////////////////////////////////
    // restoreDataFile(String fileName)
    //
    // Brings .dat.tmp files back to .dat files
    //
    // fileName: Full path to .dat file to restore
    // **(not the path to the .tmp file)
    // /////////////////////////////////////////////////////////////////////
    private void restoreDataFile(String fileName)
        throws IOException
    {
        File origDataFile = new File(fileName + ".tmp");
        File destDataFile = new File(fileName);
        if (destDataFile.exists()) {
            destDataFile.delete();
        }
        origDataFile.renameTo(destDataFile);
    }

    // /////////////////////////////////////////////////////////////////////////////////////
    // prepareDataFile(String fileName, String fileData)
    //
    // Renames .dat files to .dat.tmp, and creates new
    // .dat files with the given data.
    //			
    // fileName: Full path to .dat file to replace
    // fileData: Test data to fill in the .dat files with
    // /////////////////////////////////////////////////////////////////////////////////////
    private void prepareDataFile(String fileName, String fileData)
        throws Exception
    {
        // just in case we are pointing to data files that
        // really matter, rename them to .tmp. they will
        // be restored after completing the test.
        File origDataFile = new File(fileName);
        File destDataFile = new File(fileName + ".tmp");
        origDataFile.renameTo(destDataFile);
        // write known data to the data files:
        File outputFile = new File(fileName);
        Writer output = null;
        try {
            output = new BufferedWriter(new FileWriter(outputFile));
        }
        catch (IOException e) {
            fail("Unable to create FileWriter for file: " + fileName);
        }
        if (output != null) {
            try {
                // FileWriter always assumes default encoding is OK!
                output.write(fileData);
            }
            catch (IOException e) {
                fail("Unable to write test data to file: " + fileName);
            }
        }
        if (output != null) {
            try {
                output.close();
            }
            catch (IOException e) {
                fail("Unable to close file: " + fileName);
            }
        }
    }


    /**
     * Test method for
     * {@link com.sixnetio.SerialNumberFactory.SerNum#getSerialNumber(com.sixnetio.util.UserInterface)}
     * .
     */
    @Test
    public void testGetSerialNumber()
    {
        // Set known values into the 'low' data file:
        try {
            prepareDataFile(SerialNumberFactory.getSerLowDataFile(), dataLow);
        }
        catch (Exception e) {
            fail("Unable to prepare low serial number file.");
        }
        try {
            // get a serial number from the known data
            SerialNumber newSerNum = SerialNumberFactory.getSerialNumber(null);
            if (newSerNum.getSerialNumber() < 1) {
                fail("Invalid serial number found: " + newSerNum);
            }
            else {
                // Make sure the returned value matches
                // what is expected:
                if (!newSerNum.equals(expectedSerNumLow)) {
                    fail("Incorrect serial number returned: " + newSerNum +
                         " expected " + expectedSerNumLow);
                }
            }
        }
        catch (IOException ioe) {
            fail("IOException found during getSerialNumber(null).");
        }
        try {
            // put whatever original data was in the file
            // back:
            restoreDataFile(SerialNumberFactory.getSerLowDataFile());
        }
        catch (IOException ioe) {
            // can't call 'fail' here - it's not
            // something the class under test
            // is supposed to do - it's
            // the responsibility of the test.
        }
    }

    /**
     * Test method for
     * {@link com.sixnetio.SerialNumberFactory.SerNum#getHighSerialNumber(com.sixnetio.util.UserInterface)}
     * .
     */
    @Test
    public void testGetHighSerialNumber()
    {
        // place known data into the file:
        try {
            prepareDataFile(SerialNumberFactory.getSerHighDataFile(), dataHigh);
        }
        catch (Exception e) {
            fail("Unable to prepare high serial number file.");
        }
        try {
            // try grabbbing one serial number:
            SerialNumber newSerNum =
                SerialNumberFactory.getHighSerialNumber(null);
            if (newSerNum.getSerialNumber() < 1) {
                fail("Invalid high serial number found: " + newSerNum);
            }
            else {
                // make sure it matches the expected value:
                if (!newSerNum.equals(expectedSerNumHigh)) {
                    fail("Incorrect high serial number returned: " + newSerNum +
                         " expected " + expectedSerNumHigh);
                }
            }
        }
        catch (IOException ioe) {
            fail("IOException found during getHighSerialNumber(null).");
        }
        try {
            restoreDataFile(SerialNumberFactory.getSerHighDataFile());
        }
        catch (IOException ioe) {
            // no fail here - the method isn't responsible for
            // restoring the data
        }
    }

    /**
     * Test method for
     * {@link com.sixnetio.SerialNumberFactory.SerNum#isLegalSerial(int)}.
     */
    @Test
    public void testIsLegalSerial()
    {
        try {
            prepareDataFile(SerialNumberFactory.getSerLowDataFile(), dataLow);
        }
        catch (Exception e) {
            fail("Unable to prepare low serial number file.");
        }
        try {
            SerialNumber newSerNum = expectedSerNumLow;
            if (!SerialNumberFactory.isLegalSerial(newSerNum)) {
                fail("Expected: " + newSerNum + " to be valid.");
            }
            else {
                // make sure 0 fails:
                newSerNum = new SerialNumber(0);
                if (SerialNumberFactory.isLegalSerial(newSerNum)) {
                    fail("Expected: " + newSerNum + " to be invalid.");
                }
                else {
                    // make sure -1 fails:
                    newSerNum = new SerialNumber(0xffffffff);
                    if (SerialNumberFactory.isLegalSerial(newSerNum)) {
                        fail("Expected: " + newSerNum + " to be invalid.");
                    }
                    else {
                        // try a value > current serial number in data file
                        newSerNum =
                            new SerialNumber(expectedSerNumLow
                                .getSerialNumber() + 1);
                        if (SerialNumberFactory.isLegalSerial(newSerNum)) {
                            fail("Expected: " + newSerNum + " to be invalid.");
                        }
                        else {
                            newSerNum = new SerialNumber(0xfffffef);
                            if (SerialNumberFactory.isLegalSerial(newSerNum)) {
                                fail("Expected: " + newSerNum +
                                     " to be invalid.");
                            }
                        }
                    }
                }
            }
        }
        catch (IOException ioe) {
            fail("IOException found during getSerialNumber(null).");
        }
        try {
            restoreDataFile(SerialNumberFactory.getSerLowDataFile());
        }
        catch (IOException ioe) {
            fail("IOException found during restoreDataFile: " +
                 SerialNumberFactory.getSerLowDataFile());
        }
    }

    /**
     * Test method for
     * {@link com.sixnetio.SerialNumberFactory.SerNum#isLegalHighSerial(int)}.
     */
    @Test
    public void testIsLegalHighSerial()
    {
        SerialNumberRanges emptyRanges = new SerialNumberRanges();

        try {
            prepareDataFile(SerialNumberFactory.getSerHighDataFile(), dataHigh);
        }
        catch (Exception e) {
            fail("Unable to prepare high serial number file.");
        }
        try {
            SerialNumber newSerNum = expectedSerNumHigh;
            if (!SerialNumberFactory.isLegalHighSerial(newSerNum, emptyRanges)) {
                fail("Expected: " + newSerNum + " to be valid.");
            }
            else {
                // make sure 0 fails:
                newSerNum = new SerialNumber(0);
                if (SerialNumberFactory.isLegalHighSerial(newSerNum,
                    emptyRanges)) {
                    fail("Expected: " + newSerNum + " to be invalid.");
                }
                else {
                    // make sure -1 fails:
                    newSerNum = new SerialNumber(0xffffffff);
                    if (SerialNumberFactory.isLegalHighSerial(newSerNum,
                        emptyRanges)) {
                        fail("Expected: " + newSerNum + " to be invalid.");
                    }
                    else {
                        // try a value > current serial number in data file
                        newSerNum =
                            new SerialNumber(expectedSerNumHigh
                                .getSerialNumber() + 1);
                        if (SerialNumberFactory.isLegalHighSerial(newSerNum,
                            emptyRanges)) {
                            fail("Expected: " + newSerNum + " to be invalid.");
                        }
                        else {
                            // try a value < current serial number in data file
                            newSerNum = new SerialNumber(0x4f403c0b);
                            if (SerialNumberFactory.isLegalHighSerial(
                                newSerNum, emptyRanges)) {
                                fail("Expected: " + newSerNum +
                                     " to be invalid.");
                            }
                        }
                    }
                }
            }
        }
        catch (IOException ioe) {
            fail("IOException found during testIsLegalHighSerial().");
        }
        try {
            restoreDataFile(SerialNumberFactory.getSerHighDataFile());
        }
        catch (IOException ioe) {
            // Ignore it
        }
    }

    @Test
    public void testSerialOutsideRange()
    {
        SerialNumberRanges snRanges = new SerialNumberRanges();
        snRanges.add(new SerialNumberRange("Internal", new SerialNumber(
            expectedSerNumHigh.getSerialNumber() - 1), new SerialNumber(
            expectedSerNumHigh.getSerialNumber() + 1)));
        snRanges.add(new SerialNumberRange("External", new SerialNumber(0),
            new SerialNumber("2")));

        try {
            prepareDataFile(SerialNumberFactory.getSerHighDataFile(), dataHigh);
        }
        catch (Exception e) {
            fail("Unable to prepare high serial number file.");
        }

        try {
            // Make sure the expected value works
            SerialNumber newSerNum = expectedSerNumHigh;
            if (!SerialNumberFactory.isLegalHighSerial(newSerNum, snRanges)) {
                fail("Expected: " + newSerNum + " to be valid (internal).");
            }

            // Make sure 1 works
            newSerNum = new SerialNumber(1);
            if (!SerialNumberFactory.isLegalHighSerial(newSerNum, snRanges)) {
                fail("Expected: " + newSerNum + " to be valid (external).");
            }

            // Make sure -1 fails:
            newSerNum = new SerialNumber(0xffffffff);
            if (SerialNumberFactory.isLegalHighSerial(newSerNum, snRanges)) {
                fail("Expected: " + newSerNum + " to be invalid.");
            }
        }
        catch (IOException ioe) {
            fail("IOException found during testIsLegalHighSerial().");
        }

        try {
            restoreDataFile(SerialNumberFactory.getSerHighDataFile());
        }
        catch (IOException ioe) {
            // Ignore it
        }
    }
}

/*
 * MacFileTest.java
 *
 * Tests the MacFile class.
 *
 * Jonathan Pearson
 * November 3, 2009
 *
 */

package com.sixnetio.sixnet;

import static org.junit.Assert.fail;

import java.io.*;
import java.util.Arrays;

import org.apache.log4j.*;
import org.junit.*;

import com.sixnetio.util.Ref;
import com.sixnetio.util.Utils;

public class MacFileTest
{
    // XXX: Test ordering matters

    private static final String BACKUP_EXT = ".backup";

    private static final MacAddress singleAddressStart =
        new MacAddress("00:00:00:2c:30:1d");

    private static final MacAddress eightAddressStart =
        new MacAddress("00:00:00:34:35:50");

    private static final MacAddress sixteenAddressStart =
        new MacAddress("00:00:00:2b:06:80");

    private static boolean createdDebugFile = false;
    private static MacFile macFile;

    @BeforeClass
    public static void initialize()
        throws IOException
    {
        // Make sure we are in DEBUG mode, we do not want to be using live data
        try {
            File dbg = new File("DEBUG");
            if (!dbg.isFile()) {
                dbg.createNewFile();
                dbg.deleteOnExit();

                createdDebugFile = true;
            }
        }
        catch (IOException ioe) {
            fail("Unable to create debug file: " + ioe.getMessage());
        }

        Assert.assertTrue("DEBUG mode set", Utils.DEBUG);

        Logger.getRootLogger().setLevel(Level.OFF);
        PropertyConfigurator.configure("log4j.properties");

        // Create local MAC address files, backing up any existing files
        prepareDataFile(MacFile.ENCRYPTED_ROOT + MacFile.BLOCK_1, "1990 272437");
        prepareDataFile(MacFile.ENCRYPTED_ROOT + MacFile.BLOCK_8,
            "2097656 2390047");
        prepareDataFile(MacFile.ENCRYPTED_ROOT + MacFile.BLOCK_16,
            "4194560 4720095");

        macFile = new MacFile();
    }

    @AfterClass
    public static void cleanUp()
    {
        if (macFile != null) {
            macFile.close();
        }

        // Delete our files and restore the backups
        restoreDataFile(MacFile.ENCRYPTED_ROOT + MacFile.BLOCK_1);
        restoreDataFile(MacFile.ENCRYPTED_ROOT + MacFile.BLOCK_8);
        restoreDataFile(MacFile.ENCRYPTED_ROOT + MacFile.BLOCK_16);
    }

    @Test
    public void allocateOneAddress()
        throws IOException
    {
        MacAddress[] expectedAddresses =
            buildExpectedAddresses(singleAddressStart, 1);
        MacAddress[] macAddresses = macFile.getNewMACAddrBlock(1);

        Assert.assertTrue("getNewMACAddrBlock(1) retrieved expected address",
            Arrays.equals(expectedAddresses, macAddresses));
    }

    @Test
    public void verifyOneAddress()
        throws IOException
    {
        // Needs to come after allocateOneAddress()

        // Make sure that the address which was just allocated comes back as
        // valid
        Ref<MacAddress> ma = new Ref<MacAddress>(singleAddressStart);
        Ref<Integer> bs = new Ref<Integer>();

        boolean valid = macFile.verifyMACAddress(ma, bs);

        Assert.assertTrue("Address is valid", valid);
        Assert.assertEquals("Block size is 1", bs.get().intValue(), 1);
        Assert.assertEquals("First address in the block is correct",
            singleAddressStart, ma.get());
    }

    @Test
    public void allocateEightAddresses()
        throws IOException
    {
        MacAddress[] expectedAddresses =
            buildExpectedAddresses(eightAddressStart, 8);

        // Requesting 2-8 addresses should retrieve 8
        MacAddress[] macAddresses = macFile.getNewMACAddrBlock(2);

        Assert.assertEquals("getNewMACAddrBlock(2) retrieved 8 addresses",
            macAddresses.length, 8);
        Assert.assertTrue("getNewMACAddrBlock(2) retrieved expected addresses",
            Arrays.equals(expectedAddresses, macAddresses));
    }

    @Test
    public void verifyEightAddresses()
        throws IOException
    {
        // Needs to come after allocateEightAddresses()

        // Make sure that an address which was just allocated comes back as
        // valid
        // Increment it so it is somewhere in the middle of the block
        Ref<MacAddress> ma =
            new Ref<MacAddress>(eightAddressStart.incrementBy(5));
        Ref<Integer> bs = new Ref<Integer>();

        boolean valid = macFile.verifyMACAddress(ma, bs);

        Assert.assertTrue("Address is valid", valid);
        Assert.assertEquals("Block size is 8", bs.get().intValue(), 8);
        Assert.assertEquals("First address in the block is correct",
            eightAddressStart, ma.get());
    }

    @Test
    public void allocateSixteenAddresses()
        throws IOException
    {
        MacAddress[] expectedAddresses =
            buildExpectedAddresses(sixteenAddressStart, 16);

        // Requesting 9-16 addresses should retrieve 16
        MacAddress[] macAddresses = macFile.getNewMACAddrBlock(9);

        Assert.assertEquals("getNewMACAddrBlock(9) retrieved 16 addresses",
            macAddresses.length, 16);
        Assert.assertTrue("getNewMACAddrBlock(9) retrieved expected addresses",
            Arrays.equals(expectedAddresses, macAddresses));
    }

    @Test
    public void verifySixteenAddresses()
        throws IOException
    {
        // Needs to come after allocateSixteenAddresses()

        // Make sure that an address which was just allocated comes back as
        // valid
        // Increment it so it is somewhere in the middle of the block
        Ref<MacAddress> ma =
            new Ref<MacAddress>(sixteenAddressStart.incrementBy(13));
        Ref<Integer> bs = new Ref<Integer>();

        boolean valid = macFile.verifyMACAddress(ma, bs);

        Assert.assertTrue("Address is valid", valid);
        Assert.assertEquals("Block size is 16", bs.get().intValue(), 16);
        Assert.assertEquals("First address in the block is correct",
            sixteenAddressStart, ma.get());
    }

    @Test(expected = IOException.class)
    public void unableToAllocateMoreThanSixteenAddresses()
        throws IOException
    {
        macFile.getNewMACAddrBlock(17);
        fail("getNewMACAddrBlock(17) should have thrown an IOException");
    }

    @Test
    public void expectedNumberOfSingleAddresses()
        throws IOException
    {
        // Needs to come after allocateOneAddress()
        Assert.assertEquals("Expected number of single addresses matches",
            macFile.estimateRemainingAddresses(1), 0x1cffff - 272438);
    }

    @Test
    public void expectedNumberOfEightAddresses()
        throws IOException
    {
        // Needs to come after allocateEightAddresses()
        Assert.assertEquals("Expected number of block 8 addresses matches",
            macFile.estimateRemainingAddresses(8), 0x3cffff - 2390055);
    }

    @Test
    public void expectedNumberOfSixteenAddresses()
        throws IOException
    {
        // Needs to come after allocateSixteenAddresses()
        Assert.assertEquals("Expected number of block 16 addresses matches",
            macFile.estimateRemainingAddresses(16), 0x7cffff - 4720111);
    }


    /**
     * Create a new data file containing the provided data, backing up an
     * existing file if one exists.
     * 
     * @param rootName The root name (no extension) of the data file.
     * @param data The data to write to the file.
     */
    private static void prepareDataFile(String rootName, String data)
    {
        File f = new File(rootName + MacFile.EXT_DATA);
        File b = new File(rootName + MacFile.EXT_BACKUP);

        if (f.exists()) {
            Assert.assertTrue("Data file is a file", f.isFile());

            // Back up the file
            File backup = new File(f.getName() + BACKUP_EXT);
            Assert.assertFalse("Backup destination does not exist", backup
                .exists());

            Assert.assertTrue("Renamed existing data file '" +
                              f.getAbsolutePath() + "' to '" +
                              backup.getAbsolutePath() + "'", f
                .renameTo(backup));
        }

        if (b.exists()) {
            Assert.assertTrue("Backup file is a file", b.isFile());

            // Back up the file
            File backup = new File(b.getName() + BACKUP_EXT);
            Assert.assertFalse("Backup destination does not exist", backup
                .exists());

            Assert.assertTrue("Renamed existing data file '" +
                              b.getAbsolutePath() + "' to '" +
                              backup.getAbsolutePath() + "'", f
                .renameTo(backup));
        }

        // Write the data line to the file and the backup (MacFile requires one
        // to exist)
        try {
            FileOutputStream out = new FileOutputStream(f);

            try {
                out.write(data.getBytes("US-ASCII"));
            }
            finally {
                out.close();
            }

            out = new FileOutputStream(b);
            try {
                out.write(data.getBytes("US-ASCII"));
            }
            finally {
                out.close();
            }
        }
        catch (IOException ioe) {
            fail("Unable to write new data files '" + f.getAbsolutePath() +
                 "' and '" + b.getAbsolutePath() + "': " + ioe.getMessage());
        }
    }

    /**
     * Delete a data file, and if a backup exists, restore the backup.
     * 
     * @param rootName The root name (no extension) of the data file.
     */
    private static void restoreDataFile(String rootName)
    {
        File f = new File(rootName + MacFile.EXT_DATA);
        File b = new File(rootName + MacFile.EXT_BACKUP);

        if (f.exists()) {
            Assert.assertTrue("Data file is a file", f.isFile());
            Assert.assertTrue("Deleted test data file '" + f.getAbsolutePath() +
                              "'", f.delete());
        }

        if (b.exists()) {
            Assert.assertTrue("Backup file is a file", b.isFile());
            Assert.assertTrue("Deleted test backup file '" +
                              b.getAbsolutePath() + "'", b.delete());
        }

        File backup = new File(f.getName() + BACKUP_EXT);
        if (backup.exists()) {
            Assert.assertTrue("Backup file is a file", backup.isFile());
            Assert.assertTrue("Restored backup file '" +
                              backup.getAbsolutePath() + "'", backup
                .renameTo(f));
        }

        backup = new File(b.getName() + BACKUP_EXT);
        if (backup.exists()) {
            Assert.assertTrue("Backup file is a file", backup.isFile());
            Assert.assertTrue("Restored backup file '" +
                              backup.getAbsolutePath() + "'", backup
                .renameTo(b));
        }
    }

    /**
     * Given a start address, copy and increment it to build a set of addresses
     * based off of the start.
     * 
     * @param start The starting address.
     * @param number The number of addresses to return.
     * @return <tt>number</tt> addresses, with the first equal to <tt>start</tt>
     *         and the rest singly incremented beyond that.
     */
    private MacAddress[] buildExpectedAddresses(MacAddress start, int number)
    {
        MacAddress[] addresses = new MacAddress[number];
        for (int i = 0; i < number; i++) {
            addresses[i] = start.incrementBy(i);
        }

        return addresses;
    }
}

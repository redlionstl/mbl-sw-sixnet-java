/*
 * MacDBTest.java
 *
 * Tests the MacDB class.
 *
 * Jonathan Pearson
 * November 3, 2009
 *
 */

package com.sixnetio.sixnet;

import java.io.File;
import java.sql.*;
import java.util.Arrays;

import org.apache.log4j.*;
import org.junit.*;

import com.sixnetio.util.Ref;
import com.sixnetio.util.Utils;

public class MacDBTest
{
    private static final Logger log = Logger.getLogger(Utils.thisClassName());

    // XXX: Test ordering matters

    private static final MacAddress firstAddress =
        new MacAddress("00:00:00:aa:e0:00");

    private static final int BLOCK_SIZE = 8192;

    private static boolean createdDebugFile = false;
    private static MacDB macDB;

    @BeforeClass
    public static void initialize()
        throws Exception
    {
        Logger.getRootLogger().setLevel(Level.OFF);
        PropertyConfigurator.configure("log4j.properties");

        // Create a temporary MAC database
        File tempfile = File.createTempFile("macdb", ".testdb");
        tempfile.deleteOnExit();

        log.info("Using temporary database path: " + tempfile);

        MacDB.setDatabasePath(tempfile.getAbsolutePath());
        prepareDatabase();
        addBlockToDatabase(firstAddress, BLOCK_SIZE);

        macDB = new MacDB();
    }

    @AfterClass
    public static void cleanUp()
    {
        if (macDB != null) {
            macDB.close();
        }
    }

    @Test
    public void allocateOneAddress()
        throws SQLException
    {
        MacAddress[] expectedAddresses =
            buildExpectedAddresses(firstAddress, 0, 1);
        MacAddress[] macAddresses = macDB.getNewMACAddrBlock(1);

        Assert.assertTrue("getNewMACAddrBlock(1) retrieved expected address",
            Arrays.equals(expectedAddresses, macAddresses));
    }

    @Test
    public void verifyOneAddress()
        throws SQLException
    {
        // Needs to come after allocateOneAddress()

        // Make sure that the address which was just allocated comes back as
        // valid
        Ref<MacAddress> ma = new Ref<MacAddress>(firstAddress);
        Ref<Integer> bs = new Ref<Integer>();

        boolean valid = macDB.verifyMACAddress(ma, bs);

        Assert.assertTrue("Address is valid", valid);
        Assert.assertEquals("Block size is 1", bs.get().intValue(), 1);
        Assert.assertEquals("First address in the block is correct",
            firstAddress, ma.get());
    }

    @Test
    public void expectedNumberOfSingleAddresses()
        throws SQLException
    {
        Assert.assertEquals("Expected number of single addresses matches",
            macDB.estimateRemainingAddresses(1), BLOCK_SIZE - 1);
    }

    @Test
    public void allocateTwoAddresses()
        throws SQLException
    {
        MacAddress[] expectedAddresses =
            buildExpectedAddresses(firstAddress, 2, 2);
        MacAddress[] macAddresses = macDB.getNewMACAddrBlock(2);

        Assert.assertTrue("getNewMACAddrBlock(2) retrieved expected address",
            Arrays.equals(expectedAddresses, macAddresses));
    }

    @Test
    public void verifyTwoAddresses()
        throws SQLException
    {
        // Needs to come after allocateTwoAddresses()

        // Make sure that an address which was just allocated comes back as
        // valid
        // Increment it so it is at the beginning of the block
        MacAddress blockStart = firstAddress.incrementBy(2);

        // Choose a position in the middle of the block for the query
        Ref<MacAddress> middle = new Ref<MacAddress>(blockStart.incrementBy(1));
        Ref<Integer> bs = new Ref<Integer>();

        boolean valid = macDB.verifyMACAddress(middle, bs);

        Assert.assertTrue("Address is valid", valid);
        Assert.assertEquals("Block size is 2", bs.get().intValue(), 2);
        Assert.assertEquals("First address in the block is correct",
            blockStart, middle.get());
    }

    @Test
    public void expectedNumberOfTwoAddresses()
        throws SQLException
    {
        Assert.assertEquals("Expected number of block 2 addresses matches",
            macDB.estimateRemainingAddresses(2), BLOCK_SIZE - 4);
    }

    @Test
    public void allocateFourAddresses()
        throws SQLException
    {
        MacAddress[] expectedAddresses =
            buildExpectedAddresses(firstAddress, 4, 4);
        MacAddress[] macAddresses = macDB.getNewMACAddrBlock(4);

        Assert.assertTrue("getNewMACAddrBlock(4) retrieved expected address",
            Arrays.equals(expectedAddresses, macAddresses));
    }

    @Test
    public void verifyFourAddresses()
        throws SQLException
    {
        // Needs to come after allocateFourAddresses()

        // Make sure that an address which was just allocated comes back as
        // valid
        // Increment it so it is at the beginning of the block
        MacAddress blockStart = firstAddress.incrementBy(4);

        // Choose a position in the middle of the block for the query
        Ref<MacAddress> middle = new Ref<MacAddress>(blockStart.incrementBy(3));
        Ref<Integer> bs = new Ref<Integer>();

        boolean valid = macDB.verifyMACAddress(middle, bs);

        Assert.assertTrue("Address is valid", valid);
        Assert.assertEquals("Block size is 4", bs.get().intValue(), 4);
        Assert.assertEquals("First address in the block is correct",
            blockStart, middle.get());
    }

    @Test
    public void expectedNumberOfFourAddresses()
        throws SQLException
    {
        Assert.assertEquals("Expected number of block 4 addresses matches",
            macDB.estimateRemainingAddresses(4), BLOCK_SIZE - 8);
    }

    @Test
    public void allocateEightAddresses()
        throws SQLException
    {
        MacAddress[] expectedAddresses =
            buildExpectedAddresses(firstAddress, 8, 8);
        MacAddress[] macAddresses = macDB.getNewMACAddrBlock(8);

        Assert.assertTrue("getNewMACAddrBlock(8) retrieved expected address",
            Arrays.equals(expectedAddresses, macAddresses));
    }

    @Test
    public void verifyEightAddresses()
        throws SQLException
    {
        // Needs to come after allocateEightAddresses()

        // Make sure that an address which was just allocated comes back as
        // valid
        // Increment it so it is at the beginning of the block
        MacAddress blockStart = firstAddress.incrementBy(8);

        // Choose a position in the middle of the block for the query
        Ref<MacAddress> middle = new Ref<MacAddress>(blockStart.incrementBy(5));
        Ref<Integer> bs = new Ref<Integer>();

        boolean valid = macDB.verifyMACAddress(middle, bs);

        Assert.assertTrue("Address is valid", valid);
        Assert.assertEquals("Block size is 8", bs.get().intValue(), 8);
        Assert.assertEquals("First address in the block is correct",
            blockStart, middle.get());
    }

    @Test
    public void expectedNumberOfEightAddresses()
        throws SQLException
    {
        Assert.assertEquals("Expected number of block 8 addresses matches",
            macDB.estimateRemainingAddresses(8), BLOCK_SIZE - 16);
    }

    @Test
    public void allocateSixteenAddresses()
        throws SQLException
    {
        MacAddress[] expectedAddresses =
            buildExpectedAddresses(firstAddress, 16, 16);
        MacAddress[] macAddresses = macDB.getNewMACAddrBlock(16);

        Assert.assertTrue("getNewMACAddrBlock(16) retrieved expected address",
            Arrays.deepEquals(expectedAddresses, macAddresses));
    }

    @Test
    public void verifySixteenAddresses()
        throws SQLException
    {
        // Needs to come after allocateSixteenAddresses()

        // Make sure that an address which was just allocated comes back as
        // valid
        // Increment it so it is at the beginning of the block
        MacAddress blockStart = firstAddress.incrementBy(16);

        // Choose a position in the middle of the block for the query
        Ref<MacAddress> middle =
            new Ref<MacAddress>(blockStart.incrementBy(13));
        Ref<Integer> bs = new Ref<Integer>();

        boolean valid = macDB.verifyMACAddress(middle, bs);

        Assert.assertTrue("Address is valid", valid);
        Assert.assertEquals("Block size is 16", bs.get().intValue(), 16);
        Assert.assertEquals("First address in the block is correct",
            blockStart, middle.get());
    }

    @Test
    public void expectedNumberOfSixteenAddresses()
        throws SQLException
    {
        Assert.assertEquals("Expected number of block 16 addresses matches",
            macDB.estimateRemainingAddresses(16), BLOCK_SIZE - 32);
    }


    private static void prepareDatabase()
        throws Exception
    {
        // Create the database and the table within it
        Class.forName("org.sqlite.JDBC");
        Connection conn =
            DriverManager.getConnection("jdbc:sqlite:" +
                                        MacDB.getDatabasePath());

        try {
            Statement s = conn.createStatement();

            try {
                s.executeUpdate("CREATE TABLE macdb (" //
                                + "  startAddr BLOB,"
                                + "  endAddr BLOB," //
                                + "  blockSize INTEGER,"
                                + "  blockUsed INTEGER DEFAULT (0),"
                                + "  ts TEXT," //
                                + "  note TEXT,"
                                + "  PRIMARY KEY (startAddr, blockSize))");
                s.executeUpdate("CREATE INDEX idx_blocks"
                                + " ON macdb(blockUsed, blockSize)");
                s.executeUpdate("CREATE INDEX idx_startAddr"
                                + " ON macdb(startAddr)");
                s.executeUpdate("PRAGMA user_version = '20090817'");
            }
            finally {
                s.close();
            }
        }
        finally {
            conn.close();
        }
    }

    private static void addBlockToDatabase(MacAddress start, int count)
        throws Exception
    {
        Class.forName("org.sqlite.JDBC");
        Connection conn =
            DriverManager.getConnection("jdbc:sqlite:" +
                                        MacDB.getDatabasePath());

        try {
            PreparedStatement s =
                conn.prepareStatement("INSERT INTO macdb"
                                      + " (startAddr, endAddr, blockSize)"
                                      + " VALUES (?, ?, ?)");

            try {
                s.setBytes(1, start.getBytes());

                MacAddress end = start.incrementBy(count);
                s.setBytes(2, end.getBytes());

                s.setInt(3, count);

                s.executeUpdate();
            }
            finally {
                s.close();
            }
        }
        finally {
            conn.close();
        }
    }

    /**
     * Given a start address, copy and increment it to build a set of addresses
     * based off of the start. Allows for starting at an incremented position
     * from the start address.
     * 
     * @param start The starting address.
     * @param offset How far from the <tt>start</tt> address to increment before
     *            beginning to include in the result.
     * @param number The number of addresses to return.
     * @return <tt>number</tt> addresses, with the first equal to <tt>start</tt>
     *         incremented by <tt>offset</tt>, and the rest singly incremented
     *         beyond that.
     */
    private MacAddress[] buildExpectedAddresses(MacAddress start, int offset,
                                                int number)
    {
        MacAddress[] addresses = new MacAddress[number];
        for (int i = 0; i < number; i++) {
            addresses[i] = start.incrementBy(offset + i);
        }

        return addresses;
    }
}

package com.sixnetio.TFTPServer;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.net.tftp.TFTPClient;
import org.apache.commons.net.tftp.TFTP;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import com.sixnetio.util.Utils;

/**
 * TFTPTest sets up a series of binary and
 * ASCII tftp requests from the server.
 * 
 * The main focus of this test is to
 * ensure that tftp transfers from
 * switches/ipm's will succeed. This test
 * does not currently exercise each tftp
 * option. It does test that files of
 * troublesome (0, 512, 513, 3M...) sizes
 * are handled properly and that the
 * files transfered are exactly the same. 
 * 
 */
public class TFTPTest {
	/** The size of the buffer used
	during the addFileStringByteArray() method. */
	static final int ADDFILE_MAX_BUFFER = 9;
	
	/** The default server port number (usually 69 for
	 * TFTP but cannot open 69 on Linux without root). 
	 * 1658 is a SIXNET port that is currently used
	 * for client transfers (Sixlog) only.*/
	static final int TEST_TFTP_SVR_PORT = 1658;
	
	/** The TFTP server obj we will be testing. */
	static com.sixnetio.server.tftp.TFTP tftpServer;
	
	/** Folder for server's outbound path. */
	static File serverDirectory = new File(System.getProperty("java.io.tmpdir"));
	
    static String filePrefix = "tftp-";
    
    /** The number of test files to transfer. */
    static final int MAX_FILES = 7;
    
    /** Array that holds File objects for files to
     * be transferred. */
    static File[] files = new File[MAX_FILES];
	
    /** This is here to avoid the warnings from log4j
     * something about not initialized properly
     * if this is not present. */
	static {
		String systemRoot = "";
		
		if (Utils.osIsWindows()) {
			systemRoot = System.getenv("SystemRoot");
			
			if (systemRoot == null) {
				// Choose the current directory
				systemRoot = System.getProperty("user.dir");
			}
		} 
		else
			systemRoot = "/etc";
		
		String props =
		        Utils.getFirstExistingFile(new String[] {
		                "log4j.properties",
		                System.getProperty("user.home") + File.separator + ".cfg" + File.separator +
		                        "tftpservertest.log4j.properties",
		                systemRoot + File.separator + "TFTPServerTest" + File.separator +
		                        "tftpservertest.log4j.properties",
		        });
		
		if (props == null) {
			// Disable logging
			Logger.getRootLogger().setLevel(Level.OFF);
		}
		else
			PropertyConfigurator.configureAndWatch(props, 5000);
	}
    
    static{
        try{
            files[0] = createFile(new File(serverDirectory, filePrefix + "empty.txt"), 0);
            files[1] = createFile(new File(serverDirectory, filePrefix + "small.txt"), 1);
            files[2] = createFile(new File(serverDirectory, filePrefix + "511.txt"), 511);
            files[3] = createFile(new File(serverDirectory, filePrefix + "512.txt"), 512);
            files[4] = createFile(new File(serverDirectory, filePrefix + "513.txt"), 513);
            files[5] = createFile(new File(serverDirectory, filePrefix + "1M.txt"), 100 * 1024);
            files[6] = createFile(new File(serverDirectory, filePrefix + "10M.txt"), 1000 * 1024);
            try {
            	if (com.sixnetio.server.tftp.TFTP.isRunning())
            		fail("TFTP server already running.");
            	
            	//Need to use non-std port num in Linux if
            	//not running as root. Using 1658 since it is a SIXNET
            	//port that is very rarely used (sixlog - client xfers).
            	//start the server:
            	tftpServer = com.sixnetio.server.tftp.TFTP.getTFTP(TEST_TFTP_SVR_PORT);
            }
            catch (IOException e){
            	fail("Unable to start TFTP server.");            	
            }
            
            try{
	            tftpServer.addFile(files[0].getAbsolutePath(), files[0]);
	            tftpServer.addFile(files[1].getAbsolutePath(), files[1]);
	            tftpServer.addFile(files[2].getAbsolutePath(), files[2]);
	            tftpServer.addFile(files[3].getAbsolutePath(), files[3]);
	            tftpServer.addFile(files[4].getAbsolutePath(), files[4]);
	            tftpServer.addFile(files[5].getAbsolutePath(), files[5]);
	            tftpServer.addFile(files[6].getAbsolutePath(), files[6]);
            }
            catch (IllegalArgumentException e){
            	fail("Unable to add test files to TFTP server.");
            }
            if (!com.sixnetio.server.tftp.TFTP.isRunning())
            	fail("TFTP server not running.");
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
    
    /**
     * createFile(File file, int size)
     * 
     * Create a file, size specified in bytes
     */
    private static File createFile(File file, int size) throws IOException
    {
        OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
        byte[] temp = "0".getBytes();
        for (int i = 0; i < size; i++){
        	if (temp[0] == (byte)0xFF)
        		temp[0] = (byte)0x01;
        	else
        		temp[0]++;        	
            os.write(temp);
        }
        os.close();
        return file;
    }

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
        for (int i = 0; i < files.length; i++){
            files[i].delete();
        }
	}
	
	/**
	 * 	filesIdentical(File a, File b)
	 *	
	 *	Compares contents of a and b. 
	 */
	private boolean filesIdentical(File a, File b) throws IOException
    {
        if (!a.exists() || !b.exists()){
            return false;
        }

        if (a.length() != b.length()){
            return false;
        }

        InputStream fisA = new BufferedInputStream(new FileInputStream(a));
        InputStream fisB = new BufferedInputStream(new FileInputStream(b));

        int aBit = fisA.read();
        int bBit = fisB.read();

        while (aBit != -1){
            if (aBit != bBit){
                fisA.close();
                fisB.close();
                return false;
            }
            aBit = fisA.read();
            bBit = fisB.read();
        }

        fisA.close();
        fisB.close();
        return true;
    }
	
	public void testTFTPBinaryDownloads() throws Exception
    {
		System.out.print("Testing TFTP xfers in binary mode----------------\n");
		for (File f : files) 
            downloadFile(TFTP.BINARY_MODE, f);
    }

    public void testASCIIDownloads() throws Exception
    {
    	System.out.print("Testing TFTP xfers in ASCII mode----------------\n");
    	for (File f: files)
            downloadFile(TFTP.ASCII_MODE, f);        
    }
	
    /**
     * This function creates a TFTP client
     * and attempts to read a file from the server.
     * If the xfer is successful, it then compares
     * the contents of the file read vs. the file
     * that is on the server side.
     * 
     * @param[in] mode upload or download
     * @param[in] file to transfer
     * @throws IOException ...
     */
	private void downloadFile(int mode, File file) throws IOException
    {
        // Create our TFTP instance to handle the file transfer.
        TFTPClient tftp = new TFTPClient();
        tftp.open();
        tftp.setSoTimeout(2000);
        File out = new File(serverDirectory, filePrefix + "download");

        // cleanup old failed runs
        out.delete();
        assertTrue("Couldn't clear output location", !out.exists());

        FileOutputStream output = new FileOutputStream(out);
        
        System.out.print("Attempting to download " + file.getAbsolutePath() + "...\n");

        tftp.receiveFile(file.getAbsolutePath(), mode, output, "localhost", 
        		TEST_TFTP_SVR_PORT);
        output.close();
        
        System.out.print("Download complete.\n");
        
        assertTrue("file not created", out.exists());
        assertTrue("files not identical on file " + file, filesIdentical(out, file));

        // delete the downloaded file
        out.delete();
    }

	/**
	 * testRun()
	 * This function attempts to
	 * read several files of varying
	 * sizes in binary and ASCII
	 * xfer modes. This is the heart
	 * of the TFTP server testing.
	 */
	@Test
	public void testRun() {
		try{
			testTFTPBinaryDownloads();
		}
		catch (Exception e){
			fail("Failed during testTFTPBinaryDownloads() " + e.getMessage());
		}
		
		try{
			testASCIIDownloads();
		}
		catch (Exception e){
			fail("Failed during testTFTPASCIIDownloads() " + e.getMessage());
		}
	}
}

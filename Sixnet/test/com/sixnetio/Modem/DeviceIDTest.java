package com.sixnetio.Modem;

import static org.junit.Assert.*;

import org.apache.log4j.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test the {@link DeviceID} class.
 */
public class DeviceIDTest
{
    @Test
    public void testESN()
    {
        final String ids[] = new String[] {
            // General examples
            "0106C01B", "e5ab0134", "23911037459", "24110201020",

            // These are for testing boundary conditions
            "00000000000", "00000000", "25516777215", "ffffffff",

            // And this is for testing dash placement
            "024-13579246",
        };

        // Expected result of toString() on each of those
        final String primary[] = new String[] {
            // General examples
            "0106C01B", "E5AB0134", "EFA86B13", "F19BA7BC",

            // Low and high values
            "00000000", "00000000", "FFFFFFFF", "FFFFFFFF",

            // Dash placement
            "18CF33EE",
        };

        // Expected result of toAlternateString() on each of those
        final String alternate[] = new String[] {
            // General examples
            "001-00442395", "229-11206964", "239-11037459", "241-10201020",

            // Boundaries
            "000-00000000", "000-00000000", "255-16777215", "255-16777215",

            // Dash placement
            "024-13579246",
        };

        testIDs(ids, primary, alternate);
    }

    @BeforeClass
    public static void init()
    {
        Logger.getRootLogger().setLevel(Level.ALL);
        Logger.getRootLogger().addAppender(
            new ConsoleAppender(new PatternLayout(
                "%r [%t] %-5p %c{2} %x - %m%n"), "System.out"));
    }

    @Test
    public void testIMEI()
    {
        final String[] ids =
            new String[] {
                // General examples
                "352974021192539", "123456786543211", "1234567890123456",

                // Low and high values
                "000000000000000", "0000000000000000", "999999999999994",
                "9999999999999998",

                // Dash placement
                "02-468135-790246-8", "13-579024-681357-90",
            };

        final String[] primary =
            new String[] {
                // General examples
                "35-297402-119253-9", "12-345678-654321-1",
                "12-345678-901234-56",

                // Low and high values
                "00-000000-000000-0", "00-000000-000000-00",
                "99-999999-999999-4", "99-999999-999999-98",

                // Dash placement
                "02-468135-790246-8", "13-579024-681357-90",
            };

        // No alternate format for IMEIs

        testIDs(ids, primary, primary);
    }

    @Test
    public void testMEID()
    {
        final String[] ids =
            new String[] {
                // General examples
                "AF0123450ABCDE",
                "A10000009296F2",
                "A00000003FF642",
                "ff000000b2c63a",
                "268435456010201020",

                // Same general examples, but with check digits
                "AF0123450ABCDEC",
                "A10000009296F2F",
                "A00000003FF6421",
                "ff000000b2c63a3",
                "2684354560102010208",

                // Low and high values
                "a0000000000000", "a00000000000006", "ffffffffffffff",
                "ffffffffffffffe", "268435456000000000", "2684354560000000004",
                "429496729516777215",
                "4294967295167772153",

                // Dash placement
                "26843-54560-1020-1020", "26843-54560-0000-0000",
                "26843-54560-0000-0000-4", "42949-67295-1677-7215",
                "42949-67295-1677-7215-3",
            };

        final String[] primary =
            new String[] {
                // General values
                "29360-87365-0070-3710-0",
                "27011-31776-0960-6898-4",
                "26843-54560-0419-1810-7",
                "42781-90080-1171-6154-7",
                "26843-54560-1020-1020-8",

                // Same general examples, but with check digits
                "29360-87365-0070-3710-0",
                "27011-31776-0960-6898-4",
                "26843-54560-0419-1810-7",
                "42781-90080-1171-6154-7",
                "26843-54560-1020-1020-8",

                // Low and high values
                "26843-54560-0000-0000-4", "26843-54560-0000-0000-4",
                "42949-67295-1677-7215-3", "42949-67295-1677-7215-3",
                "26843-54560-0000-0000-4", "26843-54560-0000-0000-4",
                "42949-67295-1677-7215-3",
                "42949-67295-1677-7215-3",

                // Dash placement
                "26843-54560-1020-1020-8", "26843-54560-0000-0000-4",
                "26843-54560-0000-0000-4", "42949-67295-1677-7215-3",
                "42949-67295-1677-7215-3",
            };

        final String[] alternate =
            new String[] {
                // General values
                "AF0123450ABCDEC",
                "A10000009296F2F",
                "A00000003FF6421",
                "FF000000B2C63A3",
                "A00000009BA7BCA",

                // Same general examples, but with check digits
                "AF0123450ABCDEC", "A10000009296F2F",
                "A00000003FF6421",
                "FF000000B2C63A3",
                "A00000009BA7BCA",

                // Low and high values
                "A00000000000006", "A00000000000006", "FFFFFFFFFFFFFFE",
                "FFFFFFFFFFFFFFE", "A00000000000006", "A00000000000006",
                "FFFFFFFFFFFFFFE", "FFFFFFFFFFFFFFE",

                // Dash placement
                "A00000009BA7BCA", "A00000000000006", "A00000000000006",
                "FFFFFFFFFFFFFFE", "FFFFFFFFFFFFFFE",
            };

        testIDs(ids, primary, alternate);
    }

    @Test
    public void testIllegal()
    {
        final String[] ids =
            new String[] {
                // Out of range ESNS
                "25600000000", // Manufacturer ID of 256 cannot fit in 8 bits
                "00016777216", // SN of 16777216 cannot fit in 24 bits

                // Bad dash placement ESNs (exhaustive)
                "-12345678901",
                "1-2345678901",
                "12-345678901",
                "1234-5678901",
                "12345-678901",
                "123456-78901",
                "1234567-8901",
                "12345678-901",
                "123456789-01",
                "1234567890-1",
                "12345678901-",

                // Out of range IMEIs
                "0000000000000099", // Bad SV (cannot be 99)
                "000000000000001", // Bad check digit

                // Bad dash placement IMEIs (obviously not exhaustive, but it
                // covers many cases)
                "-02468135-790246-8",
                "0-2468135-790246-8",
                "02-4-68135790246-8",
                "02-46-8135790246-8",
                "02-468-135790246-8",
                "02-4681-35790246-8",
                "02-46813-5790246-8",
                "02-468135-7-902468",
                "02-468135-79-02468",
                "02-468135-790-2468",
                "02-468135-7902-468",
                "02-468135-79024-68",
                "02-468135-7902468-",

                // Out of range MEIDs
                "00000000000000", // Regional code must be A0-FF
                "16843-54560-0000-0000", // Same
                "AF0123450ABCDE8", // Bad check digit
                "42949-67295-1677-7215-7", // Same

                // Bad dash placement MEIDs (not exhaustive)
                "-3000000000-0000-0000-7", "3-000000000-0000-0000-7",
                "30-00000000-0000-0000-7", "300-0000000-0000-0000-7",
                "3000-000000-0000-0000-7", "300000-0000-0000-0000-7",
                "3000000-000-0000-0000-7", "30000000-00-0000-0000-7",
                "300000000-0-0000-0000-7", "30000-0-00000000-0000-7",
                "30000-00-0000000-0000-7", "30000-000-000000-0000-7",
                "30000-0000-00000-0000-7", "30000-00000-0-0000000-7",
                "30000-00000-00-000000-7", "30000-00000-000-00000-7",
                "30000-00000-0000-0-0007", "30000-00000-0000-00-007",
                "30000-00000-0000-000-07", "30000-00000-0000-00007-",
            };

        for (String s : ids) {
            try {
                DeviceID devid = DeviceID.parseDeviceID(s);
                assertTrue("String '" + s +
                           "' parsed properly, should have failed", false);
            }
            catch (NumberFormatException nfe) {
                // This was expected and correct
            }
        }
    }

    /**
     * Verify that each of the given <code>ids</code> is properly parsed without
     * errors, and matches the given {@link DeviceID#toString()} and
     * {@link DeviceID#toAlternateString()} output.
     * 
     * @param ids The device IDs to test.
     * @param primary The expected output of {@link DeviceID#toString()} for
     *            each ID.
     * @param alternate The expected output of
     *            {@link DeviceID#toAlternateString()} for each ID.
     */
    private void testIDs(String[] ids, String[] primary, String[] alternate)
    {
        for (int i = 0; i < ids.length; i++) {
            String id = ids[i];
            DeviceID devid;

            try {
                devid = DeviceID.parseDeviceID(id);
            }
            catch (NumberFormatException nfe) {
                nfe.printStackTrace(System.err);
                assertTrue(nfe.getMessage(), false);
                continue;
            }

            String s1 = devid.toString();
            String s2 = devid.toAlternateString();

            assertEquals("Primary match", primary[i], s1);
            assertEquals("Alternate match", alternate[i], s2);
        }
    }
}

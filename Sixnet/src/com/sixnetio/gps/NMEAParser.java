/*
 * NMEAParser.java
 *
 * Parses NMEA 0183 data into a series of messages, and allows extraction of
 * known data from those messages.
 *
 * Jonathan Pearson
 * December 22, 2008
 *
 */

package com.sixnetio.gps;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

public class NMEAParser implements Iterable<NMEAParser.NMEAMessage> {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Represents a generic NMEA message from a stream.
	 *
	 * @author Jonathan Pearson
	 */
	public static class NMEAMessage {
		public final String text;
		public final String type;
		
		/**
		 * Construct a new NMEA 0183 message from a given line in a stream.
		 * 
		 * @param text The line representing this message.
		 */
		protected NMEAMessage(String text) {
			this.text = text;
			this.type = text.substring(3, 6);
		}
		
		/**
		 * Get a new instance of an NMEA 0183 message for the given line of text.
		 * 
		 * @param text The line of text in the NMEA stream.
		 * @return A NMEAMessage or subclass representing that message.
		 * @throws IOException If there was a problem parsing the message.
		 */
		public static NMEAMessage makeNMEAMessage(String text) throws IOException {
			// It had better start with a '$'
			if (!text.startsWith("$")) throw new IOException("Not a NMEA 0183 message");
			
			// Check for a CRC32 at the end; if found, verify it
			if (text.indexOf('*') != -1) {
				byte crc = Byte.parseByte(text.substring(text.indexOf('*') + 1), 16);
				
				byte[] data = text.getBytes();
				byte check = 0;
				int index = 1;
				do {
					check ^= data[index++];
				} while (data[index] != (byte)'*');
				
				if (crc != check) throw new IOException("Bad CRC");
				
				// Remove the CRC so the message constructors don't need to deal with it
				text = text.substring(0, text.indexOf('*'));
			}
			
			// Check for types of messages that we know
			String msgType = text.substring(3, 6).toUpperCase();
			
			try {
    			if (msgType.equals("GLL")) {
    				return new GLLMessage(text);
    			} else if (msgType.equals("GGA")) {
    				return new GGAMessage(text);
    			} else if (msgType.equals("GSA")) {
    				return new GSAMessage(text);
    			} else if (msgType.equals("GSV")) {
    				return new GSVMessage(text);
    			} else if (msgType.equals("RMC")) {
    				return new RMCMessage(text);
    			} else if (msgType.equals("VTG")) {
    				return new VTGMessage(text);
    			} else if (msgType.equals("ZDA")) {
    				return new ZDAMessage(text);
    			} else {
    				return new NMEAMessage(text);
    			}
			} catch (RuntimeException re) {
				throw new IOException("Unable to construct NMEA message of type '" + msgType + "': " + re.getMessage(), re);
			}
		}
	}
	
	/**
	 * Represents a GLL message containing:
	 *   <ul>
	 *     <li>Latitude</li>
	 *     <li>Longitude</li>
	 *     <li>Timestamp</li>
	 *     <li>Validity</li>
	 *   </ul>
	 *
	 * @author Jonathan Pearson
	 */
	public static class GLLMessage extends NMEAMessage {
		private final Float lat, lon; // Latitude/longitude, in degrees
		private final Long ts; // Milliseconds since midnight, this morning
		private final Boolean valid; // Whether the data is valid or bad
		
		/**
		 * Construct a new GLLMessage.
		 * 
		 * @param text The NMEA line.
		 */
		protected GLLMessage(String text) {
			super(text);
			
			// Parse by commas
			String[] tokens = padArray(Utils.tokenize(text, ",", false), 7);
			
			// Latitude
			Float lat;
			try {
				lat = parseDegrees(tokens[1]);
				
				// North/South
				if (tokens[2].equals("S")) {
					lat *= -1;
				}
			} catch (Exception e) {
				logger.debug("Bad latitude: " + tokens[1] + "," + tokens[2], e);
				
				lat = null;
			}
			this.lat = lat;
			
			// Longitude
			Float lon;
			try {
				lon = parseDegrees(tokens[3]);
				
				// East/West
				if (tokens[4].equals("W")) {
					lon *= -1;
				}
			} catch (Exception e) {
				logger.debug("Bad longitude: " + tokens[3] + "," + tokens[4], e);
				
				lon = null;
			}
			this.lon = lon;
			
			// Timestamp
			Long ts;
			try {
				ts = parseUTC(tokens[5]);
			} catch (Exception e) {
				logger.debug("Bad timestamp: " + tokens[5], e);
				
				ts = null;
			}
			this.ts = ts;
			
			// Valid?
			Boolean valid;
			try {
				if (tokens[6].equals("A")) {
					valid = true;
				} else {
					valid = false;
				}
			} catch (Exception e) {
				logger.debug("Bad validity: " + tokens[6], e);
				
				valid = null;
			}
			this.valid = valid;
		}
		
		/**
		 * Get the latitude of the transmitter, in degrees north.
		 * 
		 * @return The latitude, or <tt>null</tt> if not present.
		 */
		public Float getLatitude() {
        	return lat;
        }
		
		/**
		 * Get the longitude of the transmitter, in degrees east.
		 * 
		 * @return The longitude, or <tt>null</tt> if not present.
		 */
		public Float getLongitude() {
        	return lon;
        }
		
		/**
		 * Get the timestamp of the message, in milliseconds since
		 * midnight this morning.
		 * 
		 * @return The timestamp, or <tt>null</tt> if not present.
		 */
		public Long getTimestamp() {
        	return ts;
        }
		
		/**
		 * Check if the data is valid, as it may have been read while
		 * the device did not have valid data to transmit.
		 * 
		 * @return The validity, or <tt>null</tt> if not present.
		 */
		public Boolean isValid() {
        	return valid;
        }
	}
	
	/**
	 * Represents a GGA message containing:
	 *   <ul>
	 *     <li>Latitude</li>
	 *     <li>Longitude</li>
	 *     <li>Timestamp</li>
	 *     <li>Quality indicator</li>
	 *     <li>Number of satellites in view</li>
	 *     <li>Horizontal dilution</li>
	 *     <li>Antenna altitude</li>
	 *     <li>Geoidal separation</li>
	 *     <li>Differential reference station ID</li>
	 *   </ul>
	 *
	 * @author Jonathan Pearson
	 */
	public static class GGAMessage extends NMEAMessage {
		/**
		 * Indicates the quality of a message.
		 *
		 * @author Jonathan Pearson
		 */
		public static enum QualityIndicator {
			/** The quality indicator value was not recognized. */
			UnknownType("Unknown"),
			
			/** A fix was not available. */
			FixNotAvailable("Fix Not Available"),
			
			/** A GPS fix. */
			GPSFix("GPS Fix"),
			
			/** A differential GPS fix. */
			DifferentialGPSFix("Differential GPS Fix"),
			
			/** A PPS fix. */
			PPSFix("PPS Fix"),
			
			/** Real-time kinematic. */
			RealTimeKinematic("Real-time Kinematic"),
			
			/** Float RTK. */
			FloatRTK("Float RTK"),
			
			/** Estimated (dead reckoning). */
			Estimated("Estimated"),
			
			/** Manual input mode. */
			ManualInput("Manual"),
			
			/** Simulation mode. */
			Simulation("Simulation"),
			;
			
			private final String description;
			
			private QualityIndicator(String description)
			{
				this.description = description;
			}
			
			@Override
			public String toString()
			{
				return description;
			}
		}
		
		private final Long ts; // Milliseconds since midnight, this morning
		private final Float lat, lon; // Latitude/longitude, in degrees
		private final QualityIndicator quality;
		private final Integer satellites; // Satellites in view
		private final Float horizontalDilution; // Horizontal dilution of precision in meters
		private final Float antennaAltitude; // Antenna altitude above/below mean sea level in meters
		private final Float geoidalSeparation; // Difference between WGS-84 earth ellipsoid and mean sea level, in meters
		private final Integer differentialReferenceStationID;
		
		/**
		 * Construct a new GGAMessage.
		 * 
		 * @param text The NMEA line.
		 * @throws IOException If there is a problem parsing the message.
		 */
		protected GGAMessage(String text) {
			super(text);
			
			// Parse by commas
			String[] tokens = padArray(Utils.tokenize(text, ",", false), 14);
			
			// Timestamp
			Long ts;
			try {
				ts = parseUTC(tokens[1]);
			} catch (Exception e) {
				logger.debug("Bad timestamp: " + tokens[1], e);
				
				ts = null;
			}
			this.ts = ts;
			
			// Latitude
			Float lat;
			try {
				lat = parseDegrees(tokens[2]);
				
				// North/South
				if (tokens[3].equals("S")) {
					lat *= -1;
				}
			} catch (Exception e) {
				logger.debug("Bad latitude: " + tokens[2] + "," + tokens[3], e);
				
				lat = null;
			}
			this.lat = lat;
			
			// Longitude
			Float lon;
			try {
				lon = parseDegrees(tokens[4]);
				
				// East/West
				if (tokens[5].equals("W")) {
					lon *= -1;
				}
			} catch (Exception e) {
				logger.debug("Bad longitude: " + tokens[4] + "," + tokens[5], e);
				
				lon = null;
			}
			this.lon = lon;
			
			// Quality indicator
			QualityIndicator quality;
			try {
				int val = Integer.parseInt(tokens[6]);
				
				switch (val) {
					case 0:
						quality = QualityIndicator.FixNotAvailable;
						break;
					case 1:
						quality = QualityIndicator.GPSFix;
						break;
					case 2:
						quality = QualityIndicator.DifferentialGPSFix;
						break;
					case 3:
						quality = QualityIndicator.PPSFix;
						break;
					case 4:
						quality = QualityIndicator.RealTimeKinematic;
						break;
					case 5:
						quality = QualityIndicator.FloatRTK;
						break;
					case 6:
						quality = QualityIndicator.Estimated;
						break;
					case 7:
						quality = QualityIndicator.ManualInput;
						break;
					case 8:
						quality = QualityIndicator.Simulation;
						break;
					default:
						quality = QualityIndicator.UnknownType;
				}
			} catch (Exception e) {
				logger.debug("Bad quality indicator: " + tokens[6], e);
				
				quality = null;
			}
			this.quality = quality;
			
			// Number of satellites in view
			Integer satellites;
			try {
				satellites = Integer.parseInt(tokens[7]);
			} catch (Exception e) {
				logger.debug("Bad satellite count: " + tokens[7], e);
				
				satellites = null;
			}
			this.satellites = satellites;
			
			// Horizontal dilution of precision
			Float horizontalDilution;
			try {
				horizontalDilution = Float.parseFloat(tokens[8]);
			} catch (Exception e) {
				logger.debug("Bad horizontal dilution: " + tokens[8], e);
				
				horizontalDilution = null;
			}
			this.horizontalDilution = horizontalDilution;
			
			// Antenna altitude
			Float antennaAltitude;
			try {
				antennaAltitude = Float.parseFloat(tokens[9]);
				
				String unit = tokens[10];
				if (!unit.equals("M")) throw new IOException("Unrecognized unit: " + unit);
			} catch (Exception e) {
				logger.debug("Bad antenna altitude: " + tokens[9] + "," + tokens[10], e);
				
				antennaAltitude = null;
			}
			this.antennaAltitude = antennaAltitude;
			
			// Geoidal separation
			Float geoidalSeparation;
			try {
				geoidalSeparation = Float.parseFloat(tokens[11]);
				
				String unit = tokens[12];
				if (!unit.equals("M")) throw new IOException("Unrecognized unit: " + unit);
			} catch (Exception e) {
				logger.debug("Bad geoidal separation: " + tokens[11] + "," + tokens[12], e);
				
				geoidalSeparation = null;
			}
			this.geoidalSeparation = geoidalSeparation;
			
			// Differential reference station ID
			Integer differentialReferenceStationID;
			try {
				differentialReferenceStationID = Integer.parseInt(tokens[13]);
			} catch (Exception e) {
				logger.debug("Bad differential reference station ID: " + tokens[13], e);
				
				differentialReferenceStationID = null;
			}
			this.differentialReferenceStationID = differentialReferenceStationID;
		}
		
		/**
		 * Get the timestamp of this message in milliseconds
		 * since midnight, this morning.
		 * 
		 * @return The timestamp, or <tt>null</tt> if not present.
		 */
		public Long getTimestamp() {
        	return ts;
        }
		
		/**
		 * Get the latitude of the transmitter in degrees north.
		 * 
		 * @return The latitude, or <tt>null</tt> if not present.
		 */
		public Float getLatitude() {
        	return lat;
        }
		
		/**
		 * Get the longitude of the transmitter in degrees east.
		 * 
		 * @return The longitude, or <tt>null</tt> if not present.
		 */
		public Float getLongitude() {
        	return lon;
        }
		
		/**
		 * Get the quality indicator of the message.
		 * 
		 * @return The quality indicator, or <tt>null</tt> if not present.
		 */
		public QualityIndicator getQuality() {
        	return quality;
        }
		
		/**
		 * Get the number of visible satellites.
		 * 
		 * @return The number of satellites, or <tt>null</tt> if not present.
		 */
		public Integer getSatellites() {
        	return satellites;
        }
		
		/**
		 * Get the horizontal dilution of precision, in meters.
		 * 
		 * @return The horizontal dilution, or <tt>null</tt> if not present.
		 */
		public Float getHorizontalDilution() {
        	return horizontalDilution;
        }
		
		/**
		 * Get the antenna altitude above mean sea level, in meters.
		 * 
		 * @return The antenna altitude, or <tt>null</tt> if not present.
		 */
		public Float getAntennaAltitude() {
        	return antennaAltitude;
        }
		
		/**
		 * Get the geoidal separation, in meters. This is the difference
		 * between the WGS-84 earth ellipsoid and mean sea level. Negative
		 * values mean that mean sea level is below the ellipsoid.
		 * 
		 * @return The geoidal separation, or <tt>null</tt> if not present.
		 */
		public Float getGeoidalSeparation() {
        	return geoidalSeparation;
        }
		
		/**
		 * Get the differential reference station ID.
		 * 
		 * @return The differential reference station ID, or <tt>null</tt> if not present.
		 */
		public Integer getDifferentialReferenceStationID() {
        	return differentialReferenceStationID;
        }
	}
	
	/**
	 * Represents a GSA message, containing:
	 *   <ul>
	 *     <li>Manual/automatic mode</li>
	 *     <li>Fix mode</li>
	 *     <li>IDs of satellites used for this fix</li>
	 *     <li>DOP values for P, H, and V</li>
	 *   </ul>
	 * DOP values can be compared between past/future messages to see how
	 * accurate the fix is. A DOP of 4, for example, means twice the likelihood
	 * of an error in the given position than a DOP of 2. There are no units on
	 * the DOP, so you can't compare with readings from other sources.
	 *
	 * @author Jonathan Pearson
	 */
	public static class GSAMessage extends NMEAMessage {
		/**
		 * Represents a fix mode.
		 *
		 * @author Jonathan Pearson
		 */
		public static enum FixMode {
			/** The value in the message was not recognized. */
			Unknown("Unknown"),
			
			/** No fix, currently. */
			NoFix("No Fix"),
			
			/** Two-dimensional fix. */
			TwoDimFix("2D Fix"),
			
			/** Three-dimensional fix. */
			ThreeDimFix("3D Fix"),
			;
			
			private final String description;
			
			private FixMode(String description)
			{
				this.description = description;
			}
			
			@Override
			public String toString()
			{
				return description;
			}
		}
		
		private final Boolean manualMode; // True if manual mode, false if automatic mode
		private final FixMode fixMode;
		private final Integer[] satelliteIDs = new Integer[12]; // IDs of satellites used for fix (always 12), -1 if empty
		private final Integer pdop;
		private final Integer hdop;
		private final Integer vdop;
		
		/**
		 * Construct a new GSA message.
		 * 
		 * @param text The NMEA line.
		 */
		protected GSAMessage(String text) {
			super(text);
			
			// Parse by commas
			String[] tokens = padArray(Utils.tokenize(text, ",", false), 3 + satelliteIDs.length + 3);
			
			// Selection mode
			Boolean manualMode;
			try {
				if (tokens[1].equals("M")) {
					manualMode = true;
				} else {
					manualMode = false;
				}
			} catch (Exception e) {
				logger.debug("Bad manual/automatic mode: " + tokens[1], e);
				
				manualMode = null;
			}
			this.manualMode = manualMode;
			
			// Fix mode
			FixMode fixMode;
			try {
				int fm = Integer.parseInt(tokens[2]);
				
				switch (fm) {
					case 1:
						fixMode = FixMode.NoFix;
						break;
					case 2:
						fixMode = FixMode.TwoDimFix;
						break;
					case 3:
						fixMode = FixMode.ThreeDimFix;
						break;
					default:
						fixMode = FixMode.Unknown;
				}
			} catch (Exception e) {
				logger.debug("Bad fix mode: " + tokens[2], e);
				
				fixMode = null;
			}
			this.fixMode = fixMode;
			
			// Satellite IDs
			{
				for (int i = 0; i < satelliteIDs.length; i++) {
					try {
    					String satID = tokens[3 + i];
    					
    					if (satID.length() > 0) {
    						satelliteIDs[i] = Integer.parseInt(satID);
    					} else {
    						satelliteIDs[i] = -1;
    					}
					} catch (Exception e) {
						logger.debug("Bad satellite ID: " + tokens[3 + i], e);
						
						satelliteIDs[i] = null;
					}
				}
			}
			
			int idx = 3 + satelliteIDs.length;
			
			// DOP values
			Integer pdop;
			try {
				pdop = Integer.parseInt(tokens[idx + 0]);
			} catch (Exception e) {
				logger.debug("Bad PDOP", e);
				
				pdop = null;
			}
			this.pdop = pdop;
			
			Integer hdop;
			try {
				hdop = Integer.parseInt(tokens[idx + 1]);
			} catch (Exception e) {
				logger.debug("Bad HDOP", e);
				
				hdop = null;
			}
			this.hdop = hdop;
			
			Integer vdop;
			try {
				vdop = Integer.parseInt(tokens[idx + 2]);
			} catch (Exception e) {
				logger.debug("Bad VDOP", e);
				
				vdop = null;
			}
			this.vdop = vdop;
		}
		
		/**
		 * Manual or automatic mode.
		 * 
		 * @return True = manual mode, false = automatic mode, or <tt>null</tt> if not present.
		 */
		public Boolean isManualMode() {
        	return manualMode;
        }
		
		/**
		 * Fix mode.
		 * 
		 * @return The fix mode, or <tt>null</tt> if not present.
		 */
		public FixMode getFixMode() {
        	return fixMode;
        }
		
		/**
		 * Number of satellite IDs we expected to see.
		 * 
		 * @return The number of expected satellites (for {@link #getSatelliteID(int)}).
		 */
		public int getSatelliteIDCount() {
			return satelliteIDs.length;
		}
		
		/**
		 * Get a satellite ID used for the last fix.
		 * 
		 * @param index The index of the satellite ID.
		 * @return The satellite ID, or <tt>null</tt> if not present.
		 */
		public Integer getSatelliteID(int index) {
        	return satelliteIDs[index];
        }
		
		/**
		 * Get the PDOP value.
		 * 
		 * @return The PDOP value, or <tt>null</tt> if not present.
		 */
		public Integer getPDOP() {
        	return pdop;
        }
		
		/**
		 * Get the HDOP value.
		 * 
		 * @return The HDOP value, or <tt>null</tt> if not present.
		 */
		public Integer getHDOP() {
        	return hdop;
        }
		
		/**
		 * Get the VDOP value.
		 * 
		 * @return The VDOP value, or <tt>null</tt> if not present.
		 */
		public Integer getVDOP() {
        	return vdop;
        }
	}
	
	/**
	 * Represents a GSV message containing:
	 *   <ul>
	 *     <li>Total number of GSV messages in this sequence</li>
	 *     <li>Index of this GSV message within the sequence</li>
	 *     <li>Number of visible satellites</li>
	 *     <li>List of some of the visible satellites</li>
	 *   </ul>
	 * These generally come in a sequence of 4-6, which report on the visible
	 * satellites. Generally 2-3 satellites will be reported per message, for a
	 * total of 12 satellites. Some devices may report a different number (for
	 * example, invisible satellites with a -1 SNR, or WAAS satellites).
	 *
	 * @author Jonathan Pearson
	 */
	public static class GSVMessage extends NMEAMessage {
		/**
		 * Represents a satellite in a GSV message.
		 *
		 * @author Jonathan Pearson
		 */
		public static class Satellite {
			/** PRN of the satellite, or <tt>null</tt> if not present. */
			public final Integer prn;
			
			/** Elevation of the satellite, in degrees (0-90), or <tt>null</tt> if not present. */
			public final Integer elevation;
			
			/** Azimuth of the satellite, in degrees to true north (0-359), or <tt>null</tt> if not present. */
			public final Integer azimuth;
			
			/** SNR of the satellite, in dB (0-99), or <tt>null</tt> if not present. */
			public final Integer snr;
			
			/**
			 * Construct a new Satellite.
			 * 
			 * @param prn The PRN.
			 * @param elevation The elevation, in degrees.
			 * @param azimuth The azimuth, in degrees to true north.
			 * @param snr The SNR, or -1 if blank.
			 */
			protected Satellite(Integer prn, Integer elevation, Integer azimuth, Integer snr) {
				this.prn = prn;
				this.elevation = elevation;
				this.azimuth = azimuth;
				this.snr = snr;
			}
		}
		
		private final Integer messageCount; // Total number of GSV messages in this group
		private final Integer messageIndex; // Number of this GSV message in the group, 1-based
		private final Integer satelliteCount; // Number of visible satellites
		private final List<Satellite> satellites; // Read-only after construction
		
		/**
		 * Construct a new GSV message.
		 * 
		 * @param text The NMEA line.
		 */
		protected GSVMessage(String text) {
			super(text);
			
			// Parse by commas
			String[] tokens = padArray(Utils.tokenize(text, ",", false), 4);
			
			// Message count
			Integer messageCount;
			try {
				messageCount = Integer.parseInt(tokens[1]);
			} catch (Exception e) {
				logger.debug("Bad message count: " + tokens[1], e);
				
				messageCount = null;
			}
			this.messageCount = messageCount;
			
			// Message index
			Integer messageIndex;
			try {
				messageIndex = Integer.parseInt(tokens[2]);
			} catch (Exception e) {
				logger.debug("Bad message index: " + tokens[2], e);
				
				messageIndex = null;
			}
			this.messageIndex = messageIndex;
			
			// Satellite count
			Integer satelliteCount;
			try {
				satelliteCount = Integer.parseInt(tokens[3]);
			} catch (Exception e) {
				logger.debug("Bad satellite count: " + tokens[3], e);
				
				satelliteCount = null;
			}
			this.satelliteCount = satelliteCount;
			
			this.satellites = new Vector<Satellite>();
			
			// Parse satellites (may not be all of satelliteCount in this message)
			int idx = 4;
			while (idx < tokens.length) {
				Integer prn;
				Integer elevation;
				Integer azimuth;
				Integer snr;
				
				try {
					prn = Integer.parseInt(tokens[idx + 0]);
				} catch (Exception e) {
					prn = null;
				}
				
				try {
					elevation = Integer.parseInt(tokens[idx + 1]);
				} catch (Exception e) {
					elevation = null;
				}
				
				try {
					azimuth = Integer.parseInt(tokens[idx + 2]);
				} catch (Exception e) {
					azimuth = null;
				}
				
				try {
					snr = Integer.parseInt(tokens[idx + 3]);
				} catch (Exception e) {
					snr = null;
				}
				
				idx += 4;
				
				if (prn == null) {
					// Empty entry, skip it
					continue;
				}
				
				this.satellites.add(new Satellite(prn, elevation, azimuth, snr));
			}
		}
		
		/**
		 * Get the number of messages in this group.
		 * 
		 * @return The number of messages in this group, or <tt>null</tt> if not present.
		 */
		public Integer getMessageCount() {
        	return messageCount;
        }
		
		/**
		 * Get the index within the message group of this message, 1-based.
		 * 
		 * @return The index of this message within the group, or <tt>null</tt> if not present.
		 */
		public Integer getMessageIndex() {
        	return messageIndex;
        }
		
		/**
		 * Get the number of satellites that are being reported IN THE GROUP.
		 * 
		 * @return The number of satellites being reported, or <tt>null</tt> if not present.
		 */
		public Integer getSatelliteCount() {
        	return satelliteCount;
        }
		
		/** Get a read-only iterator over the satellites reported IN THIS MESSAGE. */
		public Iterator<Satellite> getSatellites() {
        	return Utils.readOnlyIterator(satellites.iterator());
        }
	}
	
	/**
	 * Represents an RMC message containing:
	 *   <ul>
	 *     <li>Latitude</li>
	 *     <li>Longitude</li>
	 *     <li>Timestamp</li>
	 *     <li>Date</li>
	 *     <li>Confidence warning</li>
	 *     <li>Speed in knots</li>
	 *     <li>Track (heading)</li>
	 *     <li>Magnetic variation</li>
	 *   </ul>
	 *
	 * @author Jonathan Pearson
	 */
	public static class RMCMessage extends NMEAMessage {
		private final Float lat, lon; // Latitude/longitude, in degrees
		private final Long ts; // Milliseconds since midnight, this morning
		private final Long date; // Milliseconds since the epoch at midnight, this morning
		private final Boolean warning; // Whether the receiving threshold was too low to have confidence
		private final Float speed; // Ground speed, in knots
		private final Float track; // Tracking direction, in degrees to true north
		private final Float magVar; // Magnetic variation, in degrees
		
		/**
		 * Construct a new RMCMessage.
		 * 
		 * @param text The NMEA line.
		 */
		protected RMCMessage(String text) {
			super(text);
			
			// Parse by commas
			String[] tokens = padArray(Utils.tokenize(text, ",", false), 12);
			
			// Timestamp
			Long ts;
			try {
				ts = parseUTC(tokens[1]);
			} catch (Exception e) {
				logger.debug("Bad timestamp: " + tokens[1], e);
				
				ts = null;
			}
			this.ts = ts;
			
			// Warning/Valid
			Boolean warning;
			try {
				if (tokens[2].equals("A")) {
					warning = false;
				} else {
					warning = true;
				}
			} catch (Exception e) {
				logger.debug("Bad warning value: " + tokens[2], e);
				
				warning = null;
			}
			this.warning = warning;
			
			// Latitude
			Float lat;
			try {
				lat = parseDegrees(tokens[3]);
				
				// North/South
				if (tokens[4].equals("S")) {
					lat *= -1;
				}
			} catch (Exception e) {
				logger.debug("Bad latitude: " + tokens[3] + "," + tokens[4], e);
				
				lat = null;
			}
			this.lat = lat;
			
			
			// Longitude
			Float lon;
			try {
				lon = parseDegrees(tokens[5]);
				
				// East/West
				if (tokens[6].equals("W")) {
					lon *= -1;
				}
			} catch (Exception e) {
				logger.debug("Bad longitude: " + tokens[5] + "," + tokens[6], e);
				
				lon = null;
			}
			this.lon = lon;
			
			// Ground speed (knots)
			Float speed;
			try {
				speed = Float.parseFloat(tokens[7]);
			} catch (Exception e) {
				logger.debug("Bad speed: " + tokens[7], e);
				
				speed = null;
			}
			this.speed = speed;
			
			// Track
			Float track;
			try {
				track = Float.parseFloat(tokens[8]);
			} catch (Exception e) {
				logger.debug("Bad track: " + tokens[8], e);
				
				track = null;
			}
			this.track = track;
			
			// Date
			Long date;
			try {
				date = parseDate(tokens[9]);
			} catch (Exception e) {
				logger.debug("Bad date: " + tokens[9], e);
				
				date = null;
			}
			this.date = date;
			
			// Magnetic variation
			Float magVar;
			try {
				magVar = Float.parseFloat(tokens[10]);
				
				// East/West
				if (tokens[11].equals("W")) {
					magVar *= -1;
				}
			} catch (Exception e) {
				logger.debug("Bad magnetic variation: " + tokens[10] + "," + tokens[11], e);
				
				magVar = null;
			}
			this.magVar = magVar;
		}
		
		/**
		 * Get the latitude of the transmitter, in degrees North.
		 * 
		 * @return The latitude, or <tt>null</tt> if not present.
		 */
		public Float getLatitude() {
        	return lat;
        }
		
		/**
		 * Get the longitude of the transmitter, in degrees East.
		 * 
		 * @return The longitude, or <tt>null</tt> if not present.
		 */
		public Float getLongitude() {
        	return lon;
        }
		
		/**
		 * Get the timestamp of the message, in milliseconds since
		 * midnight, this morning.
		 * 
		 * @return The timestamp, or <tt>null</tt> if not present.
		 */
		public Long getTimestamp() {
        	return ts;
        }
		
		/**
		 * Get the date of the message, in milliseconds since the
		 * epoch of midnight, this morning (i.e. it does not include
		 * the day-time of the message, only the date).
		 * 
		 * @return The date, or <tt>null</tt> if not present.
		 */
		public Long getDate() {
        	return date;
        }
		
		/**
		 * Check whether the signal strength was too low to have confidence
		 * in this message.
		 * 
		 * @return The warning state, or <tt>null</tt> if not present.
		 */
		public Boolean getWarning() {
        	return warning;
        }
		
		/**
		 * Get the speed of the transmitter, in knots.
		 * 
		 * @return The speed, or <tt>null</tt> if not present.
		 */
		public Float getKnotSpeed() {
        	return speed;
        }
		
		/**
		 * Get the heading of the transmitter, in degrees to true north.
		 * 
		 * @return The heading, or <tt>null</tt> if not present.
		 */
		public Float getHeading() {
        	return track;
        }
		
		/**
		 * Get the magnetic variation from true north at the
		 * site of the transmitter, in degrees.
		 * 
		 * @return The magnetic variation, or <tt>null</tt> if not present.
		 */
		public Float getMagneticVariation() {
        	return magVar;
        }
	}
	
	/**
	 * Represents a VTG message containing:
	 *   <ul>
	 *     <li>Track (heading)</li>
	 *     <li>Magnetic track</li>
	 *     <li>Speed in knots</li>
	 *     <li>Speed in KPH</li>
	 *   </ul>
	 *
	 * @author Jonathan Pearson
	 */
	public static class VTGMessage extends NMEAMessage {
		private final Float trueTrack; // Heading by true north, in degrees
		private final Float magTrack; // Heading by magnetic north, in degrees
		private final Float knotSpeed; // Speed in knots
		private final Float kphSpeed; // Speed in KPH
		
		/**
		 * Parse a VTG message.
		 * 
		 * @param text The NMEA line.
		 */
		protected VTGMessage(String text) {
			super(text);
			
			// Parse by commas
			String[] tokens = padArray(Utils.tokenize(text, ",", false), 8);
			
			int idx = 1;
			
			// Track from true north
			Float trueTrack;
			try {
				String val = tokens[idx];
				trueTrack = parseDegrees(val);
			} catch (Exception e) {
				logger.debug("Bad true track: " + tokens[idx], e);
				
				trueTrack = null;
			}
			this.trueTrack = trueTrack;
			idx++;
			
			// Track from magnetic north
			Float magTrack;
			try {
				// New format has a 'T' here, old format skips that
				String val = tokens[idx];
				
				if (val.equals("T")) {
					idx++;
					val = tokens[idx];
				}
				
				if (val.length() == 0) {
					// Not provided
					magTrack = null;
				} else {
					magTrack = parseDegrees(val);
				}
			} catch (Exception e) {
				logger.debug("Bad magnetic track: " + tokens[idx], e);
				
				magTrack = null;
			}
			this.magTrack = magTrack;
			idx++;
			
			// Ground speed in knots
			Float knotSpeed;
			try {
				// New format has a 'M' here, old format skips that; new format may leave it blank, also
				String val = tokens[idx];
				
				if (val.equals("M") || val.length() == 0) {
					idx++;
					val = tokens[idx];
				}
				
				knotSpeed = Float.parseFloat(val);
			} catch (Exception e) {
				logger.debug("Bad ground speed: " + tokens[idx], e);
				
				knotSpeed = null;
			}
			this.knotSpeed = knotSpeed;
			idx++;
			
			// Ground speed in kph
			Float kphSpeed;
			try {
				// New format has a 'N' here, old format skips that
				String val = tokens[idx];
				
				if (val.equals("N")) {
					idx++;
					val = tokens[idx];
				}
				
				kphSpeed = Float.parseFloat(val);
			} catch (Exception e) {
				kphSpeed = null;
			}
			this.kphSpeed = kphSpeed;
			idx++;
		}
		
		/**
		 * Get the heading of the transmitter in relation to true north, in degrees.
		 * 
		 * @return The true heading, or <tt>null</tt> if not present.
		 */
		public Float getTrueHeading() {
        	return trueTrack;
        }
		
		/**
		 * Get the heading of the transmitter in relation to magnetic north, in degrees.
		 * 
		 * @return The magnetic heading, or <tt>null</tt> if not present.
		 */
		public Float getMagneticHeading() {
        	return magTrack;
        }
		
		/**
		 * Get the speed of the transmitter, in knots.
		 * 
		 * @return The speed in knots, or <tt>null</tt> if not present.
		 */
		public Float getKnotSpeed() {
        	return knotSpeed;
        }
		
		/**
		 * Get the speed of the transmitter, in kilometers per hour.
		 * 
		 * @return The speed in KPH, or <tt>null</tt> if not present.
		 */
		public Float getKphSpeed() {
        	return kphSpeed;
        }
	}
	
	/**
	 * Represents a ZDA message containing:
	 *   <ul>
	 *     <li>Timestamp</li>
	 *     <li>Date</li>
	 *     <li>Timezone hours correction</li>
	 *     <li>Timezone minutes correction</li>
	 *   </ul>
	 *
	 * @author Jonathan Pearson
	 */
	public static class ZDAMessage extends NMEAMessage {
		private final Long ts; // Milliseconds since midnight, this morning
		private final Long date; // Milliseconds since the epoch of midnight, this morning, or -1 if unable to parse
		private final Integer tzHours; // Time zone hours correction as +/- 0-13 hours
		private final Integer tzMins; // Time zone minutes correction
		
		/**
		 * Construct a new ZDA message.
		 * 
		 * @param text The NMEA line.
		 */
		protected ZDAMessage(String text) {
			super(text);
			
			// Parse by commas
			String[] tokens = padArray(Utils.tokenize(text, ",", false), 7);
			
			// Timestamp
			Long ts;
			try {
				ts = parseUTC(tokens[1]);
			} catch (Exception e) {
				logger.debug("Bad timestamp: " + tokens[1]);
				
				ts = null;
			}
			this.ts = ts;
			
			// Date
			Long date;
			try {
				int day = Integer.parseInt(tokens[2]);
				int month = Integer.parseInt(tokens[3]);
				int year = Integer.parseInt(tokens[4]);
				
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				
			    Date dt = df.parse(String.format("%04d-%02d-%02d", year, month, day));
                date = dt.getTime();
			} catch (Exception e) {
				logger.debug("Bad date: " + tokens[2] + "," + tokens[3] + "," + tokens[4]);
				
                date = null;
			}
			this.date = date;
			
			// Time zone
			Integer tzHours;
			try {
				tzHours = Integer.parseInt(tokens[5]);
			} catch (Exception e) {
				logger.debug("Bad timezone hour correction: " + tokens[5]);
				
				tzHours = null;
			}
			this.tzHours = tzHours;
			
			Integer tzMins;
			try {
				tzMins = Integer.parseInt(tokens[6]);
			} catch (Exception e) {
				logger.debug("Bad timezone minute correction: " + tokens[6]);
				
				tzMins = null;
			}
			this.tzMins = tzMins;
		}
		
		/**
		 * Get the timestamp of the message in milliseconds since midnight, this morning.
		 * 
		 * @return The timestamp, or <tt>null</tt> if not present.
		 */
		public Long getTimestamp() {
        	return ts;
        }
		
		/**
		 * Get the date of the message as milliseconds since the epoch of midnight, this morning.
		 * 
		 * @return The date, or <tt>null</tt> if not present.
		 */
		public Long getDate() {
        	return date;
        }
		
		/**
		 * Get the timezone hour correction.
		 * 
		 * @return The timezone hour correction, or <tt>null</tt> if not present.
		 */
		public Integer getTzHours() {
        	return tzHours;
        }
		
		/**
		 * Get the timezone minute correction.
		 * 
		 * @return The timezone minute correction, or <tt>null</tt> if not present.
		 */
		public Integer getTzMins() {
        	return tzMins;
        }
	}
	
	/**
	 * Parse a degrees value in the format "dddmm.mm". There may be up
	 * to 3 'd' digits indicating degrees, then exactly two 'm' digits
	 * representing minutes, then a decimal place, then any number of
	 * 'm' digits representing fractional minutes.
	 * 
	 * @param value The string representation.
	 * @return The value in degrees.
	 */
	public static float parseDegrees(String value) {
		// Format is dddmm.mm, where there are up to 3 'd' digits indicating degrees,
		//   then exactly two 'm' digits before the decimal indicating minutes, then
		//   any number of 'm' digits after the decimal indicating fractional minutes
		String minutes = value.substring(value.indexOf('.') - 2);
		String degrees = value.substring(0, value.length() - minutes.length());
		
		return (Float.parseFloat(degrees) + Float.parseFloat(minutes) / 60.0f);
	}
	
	/**
	 * Given a UTC time in the format "hhmmss.sss", return a value representing
	 * milliseconds since midnight, this morning.
	 * 
	 * @param value The string representation of the value.
	 * @return The time as the number of milliseconds since midnight, this morning.
	 */
	public static long parseUTC(String value) {
		int hr = Integer.parseInt(value.substring(0, 2));
		int mn = Integer.parseInt(value.substring(2, 4));
		float sec = Float.parseFloat(value.substring(4));
		
		return ((hr * 60 * 60 * 1000) + (mn * 60 * 1000) + (long)(sec * 1000));
	}
	
	/**
	 * Parse a date in the format "ddmmyy" into a value representing milliseconds
	 * since the epoch of midnight on the morning of that day.
	 * 
	 * @param value The string representation of the date.
	 * @return The date as the number of milliseconds since the epoch on the morning
	 *   of that date. The two-digit year starts as 70 = 1970, progressing through
	 *   00 = 2000, up to 69 = 2069.
	 */
	public static long parseDate(String value) {
		int dy = Integer.parseInt(value.substring(0, 2));
		int mo = Integer.parseInt(value.substring(2, 4));
		int yr = Integer.parseInt(value.substring(4, 6));
		
		if (yr < 70) yr += 100;
		yr += 1900;
		
		Calendar cal = Calendar.getInstance();
		cal.set(yr, mo - 1, dy);
		
		return cal.getTimeInMillis();
	}
	
	/**
	 * Combine a date and timestamp into a Date object.
	 * 
	 * @param date A date as milliseconds since the epoch of midnight, this morning.
	 * @param timestamp A timestamp as milliseconds since midnight, this morning.
	 * @return The date/time combined into a Date object.
	 */
	public static Date makeDate(long date, long timestamp) {
		return new Date(date + timestamp);
	}
	
	/**
	 * Guarantee that the given String array has enough populated entries to avoid
	 * ArrayIndexOutOfBounds exceptions. Populates missing entries with empty strings
	 * to help avoid NullPointerExceptions.
	 * 
	 * @param array The existing array that may not be long enough.
	 * @param minSize The minimum size necessary.
	 * @return A new array that is at least <tt>minSize</tt> elements long. The beginning
	 *   will be populated from <tt>array</tt>, and any remaining elements will be
	 *   populated with empty strings (not <tt>null</tt>s).
	 */
	private static String[] padArray(String[] array, int minSize) {
		if (minSize < 0) throw new RuntimeException("Minumum array size is negative: " + minSize);
		
		if (array.length < minSize) {
			// Not long enough? Make a new one
			String[] newArray = new String[minSize];
			
			// Initialize with empty strings
			Arrays.fill(newArray, "");
			
			// Copy what's available over
			System.arraycopy(array, 0, newArray, 0, array.length);
			
			// Replace the old one
			array = newArray;
		}
		
		return array;
	}
	
	// Read-only after construction
	private List<NMEAMessage> messages = new LinkedList<NMEAMessage>();
	
	/**
	 * Parse a block of NMEA messages.
	 * 
	 * @param data The block of data to parse.
	 */
	public NMEAParser(byte[] data) {
		this(new String(data));
	}
	
	/**
	 * Parse a block of NMEA messages.
	 * 
	 * @param data The block of data to parse.
	 */
	public NMEAParser(String data) {
		// Convert to an array of lines
		String[] lines = Utils.getLines(data);
		
		// Scan each line for valid messages
		for (String line : lines) {
			try {
				messages.add(NMEAMessage.makeNMEAMessage(line));
			} catch (Exception e) {
				logger.debug("Error parsing NMEA message '" + line + "'", e);
			}
		}
	}
	
	/**
	 * Get a read-only iterator through the parsed messages.
	 */
	@Override
	public Iterator<NMEAMessage> iterator() {
		return Utils.readOnlyIterator(messages.iterator());
	}
}

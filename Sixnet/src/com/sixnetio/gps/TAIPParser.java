/*
 * TAIPParser.java
 *
 * Parses TAIP data into a series of messages, and allows extraction of known
 * data from those messages.
 *
 * Jonathan Pearson
 * December 22, 2008
 *
 */

package com.sixnetio.gps;

import java.io.IOException;
import java.util.*;

import com.sixnetio.util.Utils;

public class TAIPParser implements Iterable<TAIPParser.TAIPMessage> {
	/**
	 * Represents a generic TAIP message from a stream.
	 *
	 * @author Jonathan Pearson
	 */
	public static class TAIPMessage {
		private final String text;
		private final String vid; // If present
		
		/**
		 * Construct a new TAIPMessage from a given line in a stream.
		 * 
		 * @param text The line representing this message.
		 */
		protected TAIPMessage(String text) {
			this.text = text;
			
			if (text.indexOf(";ID=") != -1) {
				String idString = text.substring(text.indexOf(";ID="));
				
				// Cut off the leading chars
				idString = idString.substring(";ID=".length());
				
				// Cut off the trailing chars
				if (idString.indexOf(';') != -1) {
					idString = idString.substring(0, idString.indexOf(';'));
				}
				
				vid = idString;
			} else {
				vid = null;
			}
		}
		
		/**
		 * Get a new instance of a TAIPMessage for the given line of text.
		 * 
		 * @param text The line of text in the TAIP data stream.
		 * @return A TAIPMessage or subclass representing that message.
		 * @throws IOException If there was a problem parsing the message.
		 */
		public static TAIPMessage makeTAIPMessage(String text) throws IOException {
			// It had better start with '>' and end with '<'
			if (!text.startsWith(">") || !text.endsWith("<")) throw new IOException("Not a TAIP message");
			
			// Check for a CRC32 at the end; if found, verify it
			if (text.indexOf('*') != -1) {
				byte crc = Byte.parseByte(text.substring(text.indexOf('*') + 1, text.indexOf('<')), 16);
				
				byte[] data = text.getBytes();
				byte check = data[0];
				int index = 1;
				do {
					check ^= data[index++];
				} while (data[index] != (byte)'*');
				
				if (crc != check) throw new IOException("Bad CRC");
			}
			
			// Check for types of messages that we know
			String msgType = text.substring(1, 4).toUpperCase();
			if (msgType.equals("RPV")) {
				return new RPVMessage(text);
			} else {
				return new TAIPMessage(text);
			}
		}
	}
	
	/**
	 * Represents an RPV message, which is a specific type of TAIP message carrying GPS data.
	 *
	 * @author Jonathan Pearson
	 */
	public static class RPVMessage extends TAIPMessage {
		private final long ts; // Milliseconds since midnight, this morning
		private final float lat, lon; // Latitude/longitude in degrees, + North/East
		private final float mphSpeed; // Speed in MPH
		private final float heading; // Heading in degrees
		
		/**
		 * Construct a new RPVMessage.
		 * 
		 * @param text The TAIP line.
		 */
		protected RPVMessage(String text) {
			super(text);
			
			// 4 chars of message header, ">RPV"
			// Timestamp is 5 chars
			ts = Integer.parseInt(text.substring(4, 9)) * 1000L;
			
			// Latitude is 8 chars
			lat = Float.parseFloat(text.substring(9, 17)) / 100000f;
			
			// Longitude is 9 chars
			lon = Float.parseFloat(text.substring(17, 26)) / 100000f;
			
			// Speed is 3 chars
			mphSpeed = Float.parseFloat(text.substring(26, 29));
			
			// Heading is 3 chars
			heading = Float.parseFloat(text.substring(29, 32));
			
			// Source (not stored) is 1 char
			// Age (not stored) is 1 char
		}
		
		/**
		 * Get the timestamp of this message, in milliseconds since
		 * midnight this morning.
		 */
		public long getTimestamp() {
        	return ts;
        }
		
		/**
		 * Get the latitude of the transmitter of this message in
		 * degrees, with North being positive.
		 */
		public float getLatitude() {
        	return lat;
        }
		
		/**
		 * Get the longitude of the transmitter of this message in
		 * degrees, with East being positive.
		 */
		public float getLongitude() {
        	return lon;
        }
		
		/**
		 * Get the speed of the transmitter in MPH.
		 */
		public float getMphSpeed() {
        	return mphSpeed;
        }
		
		/**
		 * Get the directional heading of the transmitter in degrees.
		 */
		public float getHeading() {
        	return heading;
        }
	}
	
	// Read-only after construction
	private List<TAIPMessage> messages = new LinkedList<TAIPMessage>();
	
	/**
	 * Parse a chunk of TAIP data.
	 * 
	 * @param data The TAIP data to parse.
	 */
	public TAIPParser(byte[] data) {
		this(new String(data));
	}
	
	/**
	 * Parse a chunk of TAIP data.
	 * 
	 * @param data The TAIP data to parse.
	 */
	public TAIPParser(String data) {
		// Split such that each message is framed by '>' and '<'
		// Remove all newlines and carriage returns
		String text = data.replaceAll("\r|\n", "");
		String[] lines = Utils.tokenize(text, "<>", false);
		
		// The tokenization removed the <> substrings, make sure to re-frame each message 
		for (String line : lines) {
			if (!line.startsWith(">")) line = ">" + line;
			if (!line.endsWith("<")) line = line + "<";
			
			try {
				messages.add(TAIPMessage.makeTAIPMessage(line));
			} catch (Exception e) {
				Utils.debug(e);
			}
		}
	}
	
	/**
	 * Get a read-only iterator over the parsed messages.
	 */
	@Override
	public Iterator<TAIPMessage> iterator() {
		return Utils.readOnlyIterator(messages.iterator());
	}
}

/*
 * GPSLib.java
 *
 * Provides functions for pulling useful data out of GPS messages.
 *
 * Jonathan Pearson
 * October 1, 2009
 *
 */

package com.sixnetio.gps;

import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;

/**
 * A library for parsing GPS data into a map of properties.
 *
 * @author Jonathan Pearson
 */
public class GPSLib {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Parse a GPS message. Currently this supports NMEA, with TAIP planned for
	 * the near future.
	 * 
	 * @param message The message string.
	 * @param props Output parameter to catch the properties that are found. See
	 *   each of the parse*_* functions for what will be populated. You should
	 *   probably pass in an empty map; if it is not populated with information,
	 *   then the data could not be parsed or simply did not actually contain
	 *   anything useful.
	 */
	public static void parseGPSData(String message, Map<String, String> props)
	{
		// Is it NMEA or TAIP?
		// NMEA starts with '$', TAIP starts with '>'
		if (message.startsWith("$")) {
			NMEAParser parser = new NMEAParser(message);
			
			parseGPSData(parser, props);
		}
		else if (message.startsWith(">")) {
			// TAIP
			// Not currently supported (don't trust the TAIP parser)
			logger.info("Ignoring TAIP data");
		}
	}
	
	/**
	 * Pull data out of an NMEA message and into a more useful container. NMEA,
	 * unfortunately, is made up of a number of three-letter messages, none of
	 * which seem to have any meaning. For example, GGA messages contain
	 * latitude, longitude, fix quality, satellite count, and some other random
	 * pieces of information, but the message name "GGA" does not indicate this
	 * at all.
	 * 
	 * @param parser The NMEAParser that contains the data.
	 * @param props Output parameter to catch the properties that are found. See
	 *   each of the parseNMEA_* functions for what will be populated.
	 */
	public static void parseGPSData(NMEAParser parser,
	                                Map<String, String> props)
	{
		for (NMEAParser.NMEAMessage message : parser) {
			if (message instanceof NMEAParser.GGAMessage) {
				logger.debug("GGA message: " + message.text);
				
				NMEAParser.GGAMessage msg = (NMEAParser.GGAMessage)message;
				parseNMEA_GGA(msg, props);
			}
			else if (message instanceof NMEAParser.GLLMessage) {
				logger.debug("GLL message: " + message.text);
				
				NMEAParser.GLLMessage msg = (NMEAParser.GLLMessage)message;
				parseNMEA_GLL(msg, props);
			}
			else if (message instanceof NMEAParser.GSAMessage) {
				logger.debug("GSA message: " + message.text);
				
				NMEAParser.GSAMessage msg = (NMEAParser.GSAMessage)message;
				parseNMEA_GSA(msg, props);
			}
			else if (message instanceof NMEAParser.GSVMessage) {
				logger.debug("GSV message: " + message.text);
				
				NMEAParser.GSVMessage msg = (NMEAParser.GSVMessage)message;
				parseNMEA_GSV(msg, props);
			}
			else if (message instanceof NMEAParser.RMCMessage) {
				logger.debug("RMC message: " + message.text);
				
				NMEAParser.RMCMessage msg = (NMEAParser.RMCMessage)message;
				parseNMEA_RMC(msg, props);
			}
			else if (message instanceof NMEAParser.VTGMessage) {
				logger.debug("VTG message: " + message.text);
				
				NMEAParser.VTGMessage msg = (NMEAParser.VTGMessage)message;
				parseNMEA_VTG(msg, props);
			}
			else if (message instanceof NMEAParser.ZDAMessage) {
				logger.debug("ZDA message: " + message.text);
				
				NMEAParser.ZDAMessage msg = (NMEAParser.ZDAMessage)message;
				parseNMEA_ZDA(msg, props);
			}
			else {
				logger.debug("Non-implemented NMEA message type: " +
				             message.type);
			}
		}
	}
	
	/**
	 * Parse an NMEA GGA message.
	 * 
	 * @param msg The message.
	 * @param props Output parameter. After this call, it may contain these
	 *   properties:
	 *   <ul>
	 *     <li><b>Latitude</b> (float) - Latitude in degrees +N/-S</li>
	 *     <li><b>Longitude</b> (float) - Longitude in degrees +E/-W</li>
	 *     <li><b>GPS Time</b> (long) - Milliseconds since midnight, this
	 *       morning</li>
	 *     <li><b>GPS Fix Quality</b> (String) - GPS fix quality</li>
	 *     <li><b>GPS Satellite Count</b> (int) - Number of satellites in
	 *       view</li>
	 *     <li><b>GPS HDOP</b> (float) - Horizontal dilution of precision</li>
	 *     <li><b>Altitude</b> (float) - Altitude of the antenna above/below
	 *       mean sea level, in meters</li>
	 *     <li><b>GPS Geoidal Separation</b> (float) - Difference between WGS-84
	 *       Earth ellipsoid and mean sea level, in meters</li>
	 *     <li><b>GPS Differential Reference Station ID</b> (int) - Differential
	 *       reference station ID</li>
	 *   </ul>
	 */
	public static void parseNMEA_GGA(NMEAParser.GGAMessage msg,
	                                 Map<String, String> props)
	{
		putProperty(props, "Latitude", msg.getLatitude(), "GGA message missing latitude");
		putProperty(props, "Longitude", msg.getLongitude(), "GGA message missing longitude");
		putProperty(props, "GPS Time", msg.getTimestamp(), "GGA message missing timestamp");
		putProperty(props, "GPS Fix Quality", msg.getQuality(), "GGA message missing fix quality");
		putProperty(props, "GPS Satellite Count", msg.getSatellites(), "GGA message missing satellite count");
		putProperty(props, "GPS HDOP", msg.getHorizontalDilution(), "GGA message missing HDOP");
		putProperty(props, "Altitude", msg.getAntennaAltitude(), "GGA message missing altitude");
		putProperty(props, "GPS Geoidal Separation", msg.getGeoidalSeparation(), "GGA message missing geoidal separation");
		putProperty(props, "GPS Differential Reference Station ID", msg.getDifferentialReferenceStationID(),
		            "GGA message missing differential reference station ID");
	}
	
	/**
	 * Parse an NMEA GLL message.
	 * 
	 * @param msg The message.
	 * @param props Output parameter. After this call, it may contain these
	 *   properties:
	 *   <ul>
	 *     <li><b>Latitude</b> (float) - Latitude in degrees, +N/-S</li>
	 *     <li><b>Longitude</b> (float) - Longitude in degrees, +E/-W</li>
	 *     <li><b>GPS Time</b> (long) - Milliseconds since midnight, this
	 *       morning</li>
	 *   </ul>
	 *   If the message reports itself as invalid, no properties will be
	 *   recorded.
	 */
	public static void parseNMEA_GLL(NMEAParser.GLLMessage msg,
	                                 Map<String, String> props)
	{
		if ( ! msg.isValid()) {
			return;
		}
		
		putProperty(props, "Latitude", msg.getLatitude(), "GLL message missing latitude");
		putProperty(props, "Longitude", msg.getLongitude(), "GLL message missing longitude");
		putProperty(props, "GPS Time", msg.getTimestamp(), "GLL message missing timestamp");
	}
	
	/**
	 * Parse an NMEA GSA message.
	 * 
	 * @param msg The message.
	 * @param props Output parameter. After this call, it may contain these
	 *   properties:
	 *   <ul>
	 *     <li><b>GPS Manual Mode</b> (boolean) - True if manual mode, false
	 *       otherwise</li>
	 *     <li><b>GPS Fix Mode</b> (String) - Fix mode</li>
	 *     <li><b>GPS Sat X ID</b> (int) - Satellite ID of satellite X used
	 *       for the fix (one for each satellite used)</li>
	 *     <li><b>GPS PDOP</b> (int) - Positional dilution of precision</li>
	 *     <li><b>GPS HDOP</b> (int) - Horizontal dilution of precision</li>
	 *     <li><b>GPS VDOP</b> (int) - Vertical dilution of precision</li>
	 *   </ul>
	 */
	public static void parseNMEA_GSA(NMEAParser.GSAMessage msg,
	                                 Map<String, String> props)
	{
		putProperty(props, "GPS Manual Mode", msg.isManualMode(), "GSA message missing manual mode indicator");
		putProperty(props, "GPS Fix Mode", msg.getFixMode(), "GSA message missing fix mode");
		
		// Can't use putProperty here, we need to test for null and -1
		for (int satIndex = 0;
		     satIndex < msg.getSatelliteIDCount();
		     satIndex++) {
		
			if (msg.getSatelliteID(satIndex) != null &&
			    msg.getSatelliteID(satIndex) != -1) {
				
				props.put("GPS Sat " + (satIndex + 1), " ID" +
				          msg.getSatelliteID(satIndex));
			}
			else {
				logger.debug("GSA message missing satellite ID #" + (satIndex + 1));
			}
		}
		
		putProperty(props, "GPS PDOP", msg.getPDOP(), "GSA message missing PDOP");
		putProperty(props, "GPS HDOP", msg.getHDOP(), "GSA message missing HDOP");
		putProperty(props, "GPS VDOP", msg.getVDOP(), "GSA message missing VDOP");
	}
	
	/**
	 * Parse an NMEA GSV message.
	 * 
	 * @param msg The message.
	 * @param props Output parameter. After this call, it may contain these
	 *   properties:
	 *   <ul>
	 *     <li><b>GPS Sat X.Y PRN</b> (int) - PRN of satellite Y reported in the
	 *       X message of this GSV sequence</li>
	 *     <li><b>GPS Sat X.Y Elevation</b> (int) - Elevation of that satellite
	 *       in degrees</li>
	 *     <li><b>GPS Sat X.Y Azimuth</b> (int) - Azimuth of that satellite in
	 *       degrees to true north (0-359)</li>
	 *     <li><b>GPS Sat X.Y SNR</b> (int) - SNR of that satellite in dB
	 *       (0-99)</li>
	 *   </ul>
	 */
	public static void parseNMEA_GSV(NMEAParser.GSVMessage msg,
	                                 Map<String, String> props)
	{
		int messageIndex = msg.getMessageIndex();
		int satelliteIndex = 0;
		
		for (Iterator<NMEAParser.GSVMessage.Satellite> itSatellites = msg.getSatellites();
		     itSatellites.hasNext(); ) {
			
			NMEAParser.GSVMessage.Satellite satellite = itSatellites.next();
			satelliteIndex++;
			
			putProperty(props, String.format("GPS Sat %d.%d PRN", messageIndex, satelliteIndex), satellite.prn,
			            String.format("GSV message %d of %d missing PRN of satellite %d", messageIndex,
			                          msg.getMessageCount(), satelliteIndex));
			putProperty(props, String.format("GPS Sat %d.%d Elevation", messageIndex, satelliteIndex), satellite.elevation,
			            String.format("GSV message %d of %d missing Elevation of satellite %d",
			                          messageIndex, msg.getMessageCount(), satelliteIndex));
			putProperty(props, String.format("GPS Sat %d.%d Azimuth", messageIndex, satelliteIndex), satellite.azimuth,
			            String.format("GSV message %d of %d missing Azimuth of satellite %d",
			                          messageIndex, msg.getMessageCount(), satelliteIndex));
			putProperty(props, String.format("GPS Sat %d.%d SNR", messageIndex, satelliteIndex), satellite.snr,
			            String.format("GSV message %d of %d missing SNR of satellite %d",
			                          messageIndex, msg.getMessageCount(), satelliteIndex));
		}
	}
	
	/**
	 * Parse an NMEA RMC message.
	 * 
	 * @param msg The message.
	 * @param props Output parameter. After this call, it may contain these
	 *   properties:
	 *   <ul>
	 *     <li><b>Latitude</b> (float) - Latitude in degrees +N/-S</li>
	 *     <li><b>Longitude</b> (float) - Longitude in degrees +E/-W</li>
	 *     <li><b>GPS Time</b> (long) - Milliseconds since midnight, this
	 *       morning</li>
	 *     <li><b>GPS Date</b> (long) - Milliseconds since the epoch, at
	 *       midnight, this morning</li>
	 *     <li><b>GPS Low Signal</b> (boolean) - A warning indicator that the
	 *       GPS signal is too low to have confidence in the position</li>
	 *     <li><b>GPS Speed km/h</b> (float) - Ground speed, in kilometers per
	 *       hour</li>
	 *     <li><b>GPS True Heading</b> (float) - Tracking direction, in degrees
	 *       to true north</li>
	 *     <li><b>GPS Magnetic Variation</b> (float) - Magnetic variation, in
	 *       degrees</li>
	 *   </ul>
	 */
	public static void parseNMEA_RMC(NMEAParser.RMCMessage msg,
	                                 Map<String, String> props)
	{
		putProperty(props, "Latitude", msg.getLatitude(), "RMC message missing latitude");
		putProperty(props, "Longitude", msg.getLongitude(), "RMC message missing longitude");
		putProperty(props, "GPS Time", msg.getTimestamp(), "RMC message missing timestamp");
		putProperty(props, "GPS Date", msg.getDate(), "RMC message missing datestamp");
		putProperty(props, "GPS Low Signal", msg.getWarning(), "RMC message missing confidence warning");
		
		// Can't use putProperty, we need to convert this to KPH
		if (msg.getKnotSpeed() != null) {
			props.put("GPS Speed km/h",
			          "" + Conversion.knotsToKph(msg.getKnotSpeed()));
		}
		else {
			logger.debug("RMC message missing ground speed");
		}
		
		putProperty(props, "GPS True Heading", msg.getHeading(), "RMC message missing heading");
		putProperty(props, "GPS Magnetic Variation", msg.getMagneticVariation(), "RMC message missing magnetic variation");
	}
	
	/**
	 * Parse an NMEA VTG message.
	 * 
	 * @param msg The message.
	 * @param props Output parameter. After this call, it may contain these
	 *   properties:
	 *   <ul>
	 *     <li><b>GPS True Heading</b> (float) - Tracking direction, in degrees
	 *       to true north</li>
	 *     <li><b>GPS Magnetic Heading</b> (float) - Tracking direction, in
	 *       degrees to magnetic north</li>
	 *     <li><b>GPS Speed km/h</b> (float) - Ground speed, in kilometers per
	 *       hour</li>
	 *   </ul>
	 */
	public static void parseNMEA_VTG(NMEAParser.VTGMessage msg,
	                                 Map<String, String> props)
	{
		putProperty(props, "GPS True Heading", msg.getTrueHeading(), "VTG message missing true heading");
		putProperty(props, "GPS Magnetic Heading", msg.getMagneticHeading(), "VTG message missing magnetic heading");
		
		// Need to make a conversion if the KPH version is unavailable
		if (msg.getKphSpeed() != null) {
			// If available, use the KPH speed from the transmitter
			props.put("GPS Speed km/h", "" + msg.getKphSpeed());
		}
		else if (msg.getKnotSpeed() != null) {
			// But if not, try to convert the speed in knots
			props.put("GPS Speed km/h",
			          "" + Conversion.knotsToKph(msg.getKnotSpeed()));
		}
		else {
			logger.debug("VTG message missing speed (kph or knots)");
		}
	}
	
	/**
	 * Parse an NMEA ZDA message.
	 * 
	 * @param msg The message.
	 * @param props Output parameter. After this call, it may contain these
	 *   properties:
	 *   <ul>
	 *     <li><b>GPS Time</b> (long) - Milliseconds since midnight, this
	 *       morning</li>
	 *     <li><b>GPS Date</b> (long) - Milliseconds since the epoch, at
	 *       midnight, this morning</li>
	 *     <li><b>GPS Timezone Hour Correction</b> (int) - Time zone correction
	 *       hours</li>
	 *     <li><b>GPS Timezone Minute Correction</b> (int) - Time zone
	 *       correction minutes</li>
	 *   </ul>
	 */
	public static void parseNMEA_ZDA(NMEAParser.ZDAMessage msg,
	                                  Map<String, String> props)
	{
		putProperty(props, "GPS Time", msg.getTimestamp(), "ZDA message missing timestamp");
		putProperty(props, "GPS Date", msg.getDate(), "ZDA message missing datestamp");
		putProperty(props, "GPS Timezone Hour Correction", msg.getTzHours(), "ZDA message missing timezone hour correction");
		putProperty(props, "GPS Timezone Minute Correction", msg.getTzMins(), "ZDA message missing timezone minute correction");
	}
	
	/**
	 * A helper function for adding properties to a map.
	 * 
	 * @param props The map to add to.
	 * @param key The key for the value in the map.
	 * @param value If non-<tt>null</tt>, will go into the map with the given
	 *   <tt>key</tt>.
	 * @param errMsg If <tt>value == null</tt> then this will be sent out on the
	 *   logger's debug stream. If <tt>null</tt>, no logging message will be
	 *   sent.
	 */
	private static void putProperty(Map<String, String> props, String key,
	                                Object value, String errMsg)
	{
		if (value != null) {
			props.put(key, value.toString());
		}
		else if (errMsg != null) {
			logger.debug(errMsg);
		}
	}
}

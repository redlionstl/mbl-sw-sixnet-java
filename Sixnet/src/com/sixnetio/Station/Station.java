/*
 * Station.java
 *
 * Mimics a station; it is a user's job to extend this to compute values
 * for the input registers when they are read.
 *
 * Jonathan Pearson
 * March 23, 2007
 *
 */

package com.sixnetio.Station;

import java.util.LinkedList;
import java.util.Queue;

import com.sixnetio.UDR.*;

public abstract class Station implements MessageListener {
    private static class TimeoutMessage {
        public MessageDispatcher.Message msg;
        public long sentAt;
        public int retries;
        
        public TimeoutMessage(MessageDispatcher.Message msg) {
            this.msg = msg;
            sentAt = System.currentTimeMillis();
            retries = 0;
        }
        
        public boolean matches(MessageDispatcher.Message received) {
            return (msg.msg.getSession() == received.msg.getSession() &&
                    msg.msg.getSequence() == received.msg.getSequence());
        }
        
        public boolean checkTimeout() {
            return (sentAt + timeout < System.currentTimeMillis());
        }
    }
    
    
    public static final int S_UP = 1, // Station is up and responding to messages
                            S_DOWN = 2, // Station is down and not responding, but with intent to be up again
                            S_HALT = 3; // Station is down and will not come back
    
    // Default version information
    public static final short V_PROD_CODE = (short)254; // Non-SIXNET supporting non-byte boundaries
    public static final byte V_MAJOR = (byte)1;
    public static final byte V_MINOR = (byte)0;
    
    private static int timeout = 10000; // Messages that were sent will time-out after this many milliseconds
    private static int retries = 10; // Messages will be resent this many times before simply being dropped
                                     //   Set retries to -1 for infinite retries (not recommended)
    
    private int status; // Running status, see S_* values
    
    private short id;
    
    // These are our registers
    private byte[] discretesIn;
    private byte[] discretesOut;
    
    private short[] analogsIn;
    private short[] analogsOut;
    
    // If SCLOCK is received, this holds the time difference in seconds between our clock and theirs
    // Equal to (our time) - (their time)
    private int clockOffset = 0;
    
    private Queue<MessageDispatcher.Message> incoming;
    private MessageDispatcher dispatcher;
    
    private Queue<TimeoutMessage> inFlight;
    
    public Station(MessageDispatcher dispatcher, short id,
                   short discreteRegs, short analogRegs) {
        
        this(dispatcher, id, discreteRegs, discreteRegs, analogRegs, analogRegs);
    }
    
    public Station(MessageDispatcher dispatcher, short id,
                   short discreteIns, short discreteOuts,
                   short analogIns, short analogOuts) {
        
        inFlight = new LinkedList<TimeoutMessage>();
        
        this.dispatcher = dispatcher;
        this.id = id;
        
        dispatcher.registerListener(this);
        
        discretesIn = new byte[discreteIns];
        discretesOut = new byte[discreteOuts];
        
        analogsIn = new short[analogIns];
        analogsOut = new short[analogOuts];
        
        incoming = new LinkedList<MessageDispatcher.Message>();
        
        setStatus(S_UP);
        
        // This is in an anonymous class because otherwise a subclass's run() method
        //   would override this one
        Thread th = new Thread() {
            @Override
            public void run() {
                while (status != S_HALT) {
                    while (status == S_UP) {
                        MessageDispatcher.Message msg;
                        
                        synchronized(incoming) {
                            while (incoming.isEmpty()) {
                                try {
                                    incoming.wait();
                                } catch (InterruptedException ie) { }
                            }
                            
                            msg = incoming.remove();
                        }
                        
                        if (msg != null) {
                            if (msg.msg.getDestination() == UDRMessage.STA_ANY || msg.msg.getDestination() == getID()) {
                                process(msg);
                            } // otherwise ignore it, it's not for us
                        } else {
                            Thread.yield();
                        }
                    }
                    
                    Thread.yield();
                }
            }
        };
        th.setDaemon(true);
        th.start();
        
        th = new Thread(new TimeoutChecker());
        th.setDaemon(true);
        th.start();
    }
    
    public final short getID() {
        return id;
    }
    
    public static void setTimeout(int millis) {
        timeout = millis;
    }
    
    public static int getTimeout() {
        return timeout;
    }
    
    public static void setRetries(int retries) {
        Station.retries = retries;
    }
    
    public static int getRetries() {
        return retries;
    }
    
    public final void handleMessage(MessageDispatcher.Message msg) {
        synchronized(incoming) {
            incoming.add(msg);
            
            incoming.notify();
        }
    }
    
    public final void send(MessageDispatcher.Message message) {
        UDRMessage msg = message.msg;
        
        if (msg.getCommand() != UDRMessage.C_ACK &&
            msg.getCommand() != UDRMessage.C_NAK) {
            
            // We do not add ACK and NAK messages to the inflight list, since
            //   we do not wait for ACK/NAK responses to them
            synchronized(inFlight) {
                inFlight.add(new TimeoutMessage(message));
            }
        }
        
        dispatcher.send(message);
    }
    
    public final void send(UDRMessage msg) {
        MessageDispatcher.Message message = new MessageDispatcher.Message(msg);
        send(message);
    }
    
    private void resend(TimeoutMessage msg) {
        if (msg.retries < retries) {
            msg.retries++;
            msg.sentAt = System.currentTimeMillis();
            
            synchronized(inFlight) {
                inFlight.add(msg);
            }
            
            dispatcher.send(msg.msg);
        }
    }
    
    private void respond(MessageDispatcher.Message message, UDRMessage msg) {
        MessageDispatcher.Message sending = new MessageDispatcher.Message(msg,
                                                                          message.handler,
                                                                          message.addr,
                                                                          message.port);
        
        send(sending);
    }
    
    public final void setStatus(int newStatus) {
        status = newStatus;
    }
    
    public final int getStatus() {
        return status;
    }
    
    private void process(MessageDispatcher.Message message) {
        UDRMessage msg = message.msg;
        
        try {
            switch (msg.getCommand()) {
                case UDRMessage.C_NOP: {
                    UDR_NoOp m = (UDR_NoOp)msg;
                    respond(message, m.acknowledge());
                    
                    break;
                }
                case UDRMessage.C_ACK:
                case UDRMessage.C_NAK: {
                    // Clear out the message it is responding to, and handle it while we're here
                    synchronized(inFlight) {
                        Queue<TimeoutMessage> temp = new LinkedList<TimeoutMessage>();
                        
                        while (!inFlight.isEmpty()) {
                            TimeoutMessage tom = inFlight.remove();
                            
                            if (tom.matches(message)) {
                                if (msg instanceof UDR_Acknowledge) {
                                    receivedPositiveAck(tom.msg.msg, (UDR_Acknowledge)msg);
                                } else {
                                    receivedNegativeAck(tom.msg.msg, (UDR_NAcknowledge)msg);
                                }
                            } else {
                                temp.add(tom);
                            }
                        }
                        
                        inFlight = temp;
                    }
                    
                    break;
                }
                case UDRMessage.C_VERS: {
                    UDR_Version m = (UDR_Version)msg;
                    
                    respond(message, receivedVers(m, m.acknowledge(V_PROD_CODE, V_MAJOR, V_MINOR)));

                    break;
                }
                case UDRMessage.C_NIO: {
                    UDR_NumberIO m = (UDR_NumberIO)msg;
    
                    switch (m.getType()) {
                        case UDRMessage.T_D_AIN:
                        case UDRMessage.T_P_AIN:
                        case UDRMessage.T_C_AIN: {
                            respond(message, m.acknowledge((short)discretesIn.length, (short)analogsIn.length));
                            
                            break;
                        }
                        
                        case UDRMessage.T_D_AOUT:
                        case UDRMessage.T_P_AOUT:
                        case UDRMessage.T_C_AOUT: {
                            respond(message, m.acknowledge((short)discretesOut.length, (short)analogsOut.length));
                            
                            break;
                        }
                        
                        case UDRMessage.T_D_DIN:
                        case UDRMessage.T_C_DIN:
                        case UDRMessage.T_P_DIN: {
                            respond(message, m.acknowledge((short)discretesIn.length, (short)discretesIn.length));
                            
                            break;
                        }
                        
                        case UDRMessage.T_D_DOUT:
                        case UDRMessage.T_C_DOUT:
                        case UDRMessage.T_P_DOUT: {
                            respond(message, m.acknowledge((short)discretesOut.length, (short)discretesOut.length));
                            
                            break;
                        }
                        
                        case UDRMessage.T_LONGINT_IN:
                        case UDRMessage.T_LONGINT_OUT: {
                            respond(message, m.acknowledge((short)0, (short)0)); // Longs are not supported
                            
                            break;
                        }
                        case UDRMessage.T_FLOAT_IN:
                        case UDRMessage.T_FLOAT_OUT: {
                            respond(message, m.acknowledge((short)0, (short)0)); // Floats are not supported
                            
                            break;
                        }
                        default: {
                            if (UDRMessage.T_USER <= m.getType() && m.getType() <= UDRMessage.T_USERMAX) {
                                // Not currently supported
                                respond(message, m.acknowledge((short)0, (short)0));
                            } else {
                                respond(message, m.nAcknowledge());
                            }
                        }
                    }
                    
                    break;
                }
                case UDRMessage.C_GETD: {
                    UDR_GetD m = (UDR_GetD)msg;
                    
                    byte type = m.getType();
                    
                    byte[] readFrom = null;
                    
                    if (type == UDRMessage.T_D_AIN ||
                        type == UDRMessage.T_C_AIN ||
                        type == UDRMessage.T_P_AIN ||
                        type == UDRMessage.T_D_DIN ||
                        type == UDRMessage.T_C_DIN ||
                        type == UDRMessage.T_P_DIN) {
                        
                        readFrom = discretesIn;
                    } else if (type == UDRMessage.T_D_AOUT ||
                               type == UDRMessage.T_C_AOUT ||
                               type == UDRMessage.T_P_AOUT ||
                               type == UDRMessage.T_D_DOUT ||
                               type == UDRMessage.T_C_DOUT ||
                               type == UDRMessage.T_P_DOUT) {
                        
                        readFrom = discretesOut;
                    }
                    
                    if (readFrom != null) {
                        int num = m.getNum() / 8;
                        if (m.getNum() % 8 > 0) num++;
                        
                        byte[] retData = new byte[num];
                        
                        for (int i = 0; i < m.getNum(); i++) {
                            int idx = i / 8;
                            int bit = i % 8;
                            
                            if (getD(readFrom, m.getStart() + i)) {
                                retData[idx] |= (byte)(1 << bit);
                            }
                        }
                        
                        respond(message, receivedGetD(m, m.acknowledge(retData)));
                    } else {
                        // Unsupported
                        respond(message, m.nAcknowledge());
                    }
                    
                    break;
                }
                case UDRMessage.C_GETB: {
                    UDR_GetB m = (UDR_GetB)msg;
                    byte type = m.getType();
                    
                    byte[] readFrom = null;
                    
                    if (type == UDRMessage.T_D_AIN ||
                        type == UDRMessage.T_C_AIN ||
                        type == UDRMessage.T_P_AIN ||
                        type == UDRMessage.T_D_DIN ||
                        type == UDRMessage.T_C_DIN ||
                        type == UDRMessage.T_P_DIN) {
                        
                        readFrom = discretesIn;
                    } else if (type == UDRMessage.T_D_AOUT ||
                               type == UDRMessage.T_C_AOUT ||
                               type == UDRMessage.T_P_AOUT ||
                               type == UDRMessage.T_D_DOUT ||
                               type == UDRMessage.T_C_DOUT ||
                               type == UDRMessage.T_P_DOUT) {
                        
                        readFrom = discretesOut;
                    }
                    
                    if (readFrom != null) {
                        byte[] retData = new byte[m.getNum()];
                        
                        for (int i = 0; i < m.getNum(); i++) {
                            retData[i] = getB(readFrom, m.getStart() + i);
                        }
                        
                        respond(message, receivedGetB(m, m.acknowledge(retData)));
                    } else {
                        // Unsupported
                        respond(message, m.nAcknowledge());
                    }
                    
                    break;
                }
                case UDRMessage.C_GETA: {
                    UDR_GetA m = (UDR_GetA)msg;
                    byte type = m.getType();
                    
                    short[] readFrom = null;
                    
                    if (type == UDRMessage.T_D_AIN ||
                        type == UDRMessage.T_C_AIN ||
                        type == UDRMessage.T_P_AIN ||
                        type == UDRMessage.T_D_DIN ||
                        type == UDRMessage.T_C_DIN ||
                        type == UDRMessage.T_P_DIN) {
                        
                        readFrom = analogsIn;
                    } else if (type == UDRMessage.T_D_AOUT ||
                               type == UDRMessage.T_C_AOUT ||
                               type == UDRMessage.T_P_AOUT ||
                               type == UDRMessage.T_D_DOUT ||
                               type == UDRMessage.T_C_DOUT ||
                               type == UDRMessage.T_P_DOUT) {
                        
                        readFrom = analogsOut;
                    }
                    
                    if (readFrom != null) {
                        short[] retData = new short[m.getNum()];
                        
                        for (int i = 0; i < m.getNum(); i++) {
                            retData[i] = getA(readFrom, m.getStart() + i);
                        }
                        
                        respond(message, receivedGetA(m, m.acknowledge(retData)));
                    } else {
                        // Unsupported
                        respond(message, m.nAcknowledge());
                    }
                    
                    break;
                }
                case UDRMessage.C_GETS: {
                    UDR_GetS m = (UDR_GetS)msg;
                    byte type = m.getType();
                    
                    byte[] readFrom = null;
                    
                    if (type == UDRMessage.T_D_AIN ||
                        type == UDRMessage.T_C_AIN ||
                        type == UDRMessage.T_P_AIN ||
                        type == UDRMessage.T_D_DIN ||
                        type == UDRMessage.T_C_DIN ||
                        type == UDRMessage.T_P_DIN) {
                        
                        readFrom = discretesIn;
                    } else if (type == UDRMessage.T_D_AOUT ||
                               type == UDRMessage.T_C_AOUT ||
                               type == UDRMessage.T_P_AOUT ||
                               type == UDRMessage.T_D_DOUT ||
                               type == UDRMessage.T_C_DOUT ||
                               type == UDRMessage.T_P_DOUT) {
                        
                        readFrom = discretesOut;
                    }
                    
                    if (readFrom != null) {
                        String ans = "";
                        
                        char ch;
                        int i = m.getStart();
                        while ((ch = (char)getB(readFrom, m.getStart() + i)) != '\0') {
                            ans += ch;
                            i++;
                        }
                        
                        respond(message, receivedGetS(m, m.acknowledge(ans)));
                    } else {
                        // Unsupported
                        respond(message, m.nAcknowledge());
                    }
                    
                    break;
                }
                case UDRMessage.C_PUTD: {
                    UDR_PutD m = (UDR_PutD)msg;
                    
                    byte type = m.getType();
                    
                    boolean output;
                    
                    if (type == UDRMessage.T_D_AIN ||
                        type == UDRMessage.T_C_AIN ||
                        type == UDRMessage.T_P_AIN ||
                        type == UDRMessage.T_D_DIN ||
                        type == UDRMessage.T_C_DIN ||
                        type == UDRMessage.T_P_DIN) {
                        
                        output = false;
                    } else if (type == UDRMessage.T_D_AOUT ||
                               type == UDRMessage.T_C_AOUT ||
                               type == UDRMessage.T_P_AOUT ||
                               type == UDRMessage.T_D_DOUT ||
                               type == UDRMessage.T_C_DOUT ||
                               type == UDRMessage.T_P_DOUT) {
                        
                        output = true;
                    } else {
                        // Unsupported
                        respond(message, m.nAcknowledge());
                        break;
                    }
                    
                    int num = m.getNum() / 8;
                    if (m.getNum() % 8 > 0) num++;
                    
                    if (output) {
                        for (int i = 0; i < m.getNum(); i++) {
                            putDO(m.getStart() + i, m.getVal(i));
                        }
                    } else {
                        for (int i = 0; i < m.getNum(); i++) {
                            putDI(m.getStart() + i, m.getVal(i));
                        }
                    }

                    respond(message, receivedPutD(m, m.acknowledge()));

                    break;
                }
                case UDRMessage.C_PUTB: {
                    UDR_PutB m = (UDR_PutB)msg;
                    
                    byte type = m.getType();
                    
                    boolean output;
                    
                    if (type == UDRMessage.T_D_AIN ||
                        type == UDRMessage.T_C_AIN ||
                        type == UDRMessage.T_P_AIN ||
                        type == UDRMessage.T_D_DIN ||
                        type == UDRMessage.T_C_DIN ||
                        type == UDRMessage.T_P_DIN) {
                        
                        output = false;
                    } else if (type == UDRMessage.T_D_AOUT ||
                               type == UDRMessage.T_C_AOUT ||
                               type == UDRMessage.T_P_AOUT ||
                               type == UDRMessage.T_D_DOUT ||
                               type == UDRMessage.T_C_DOUT ||
                               type == UDRMessage.T_P_DOUT) {
                        
                        output = true;
                    } else {
                        // Unsupported
                        respond(message, m.nAcknowledge());
                        break;
                    }
                    
                    if (output) {
                        for (int i = 0; i < m.getNum(); i++) {
                            putBO(m.getStart() + i, m.getVal(i));
                        }
                    } else {
                        for (int i = 0; i < m.getNum(); i++) {
                            putBI(m.getStart() + i, m.getVal(i));
                        }
                    }
                        
                    respond(message, receivedPutB(m, m.acknowledge()));
                    
                    break;
                }
                case UDRMessage.C_PUTA: {
                    UDR_PutA m = (UDR_PutA)msg;
                    
                    byte type = m.getType();
                    
                    boolean output;
                    
                    // We do not support PutA messages writing into discretes
                    if (type == UDRMessage.T_D_AIN ||
                        type == UDRMessage.T_C_AIN ||
                        type == UDRMessage.T_P_AIN) {
                        
                        output = false;
                    } else if (type == UDRMessage.T_D_AOUT ||
                               type == UDRMessage.T_C_AOUT ||
                               type == UDRMessage.T_P_AOUT) {
                        
                        output = true;
                    } else {
                        // Unsupported
                        respond(message, m.nAcknowledge());
                        break;
                    }
                    
                    if (output) {
                        for (int i = 0; i < m.getNum(); i++) {
                            putAO(m.getStart() + i, m.getVal(i));
                        }
                    } else {
                        for (int i = 0; i < m.getNum(); i++) {
                            putAI(m.getStart() + i, m.getVal(i));
                        }
                    }

                    respond(message, receivedPutA(m, m.acknowledge()));
                    
                    break;
                }
                case UDRMessage.C_SETD: {
                    UDR_SetD m = (UDR_SetD)msg;
                    
                    byte type = m.getType();
                    
                    boolean output;
                    
                    if (type == UDRMessage.T_D_AIN ||
                        type == UDRMessage.T_C_AIN ||
                        type == UDRMessage.T_P_AIN ||
                        type == UDRMessage.T_D_DIN ||
                        type == UDRMessage.T_C_DIN ||
                        type == UDRMessage.T_P_DIN) {
                        
                        output = false;
                    } else if (type == UDRMessage.T_D_AOUT ||
                               type == UDRMessage.T_C_AOUT ||
                               type == UDRMessage.T_P_AOUT ||
                               type == UDRMessage.T_D_DOUT ||
                               type == UDRMessage.T_C_DOUT ||
                               type == UDRMessage.T_P_DOUT) {
                        
                        output = true;
                    } else {
                        // Unsupported
                        respond(message, m.nAcknowledge());
                        break;
                    }
                    
                    int num = m.getNum() / 8;
                    if (m.getNum() % 8 > 0) num++;
                        
                    if (output) {
                        for (int i = 0; i < m.getNum(); i++) {
                            if (m.getVal(i)) {
                                putDO(m.getStart() + i, true);
                            }
                        }
                    } else {
                        for (int i = 0; i < m.getNum(); i++) {
                            if (m.getVal(i)) {
                                putDI(m.getStart() + i, true);
                            }
                        }
                    }

                    respond(message, receivedSetD(m, m.acknowledge()));
                    
                    break;
                }
                case UDRMessage.C_CLRD: {
                    UDR_ClearD m = (UDR_ClearD)msg;
                    
                    byte type = m.getType();
                    
                    boolean output;
                    
                    if (type == UDRMessage.T_D_AIN ||
                        type == UDRMessage.T_C_AIN ||
                        type == UDRMessage.T_P_AIN ||
                        type == UDRMessage.T_D_DIN ||
                        type == UDRMessage.T_C_DIN ||
                        type == UDRMessage.T_P_DIN) {
                        
                        output = false;
                    } else if (type == UDRMessage.T_D_AOUT ||
                               type == UDRMessage.T_C_AOUT ||
                               type == UDRMessage.T_P_AOUT ||
                               type == UDRMessage.T_D_DOUT ||
                               type == UDRMessage.T_C_DOUT ||
                               type == UDRMessage.T_P_DOUT) {
                        
                        output = true;
                    } else {
                        // Unsupported
                        respond(message, m.nAcknowledge());
                        break;
                    }
                    
                    int num = m.getNum() / 8;
                    if (m.getNum() % 8 > 0) num++;
                        
                    if (output) {
                        for (int i = 0; i < m.getNum(); i++) {
                            if (m.getVal(i)) {
                                putDO(m.getStart() + i, false);
                            }
                        }
                    } else {
                        for (int i = 0; i < m.getNum(); i++) {
                            if (m.getVal(i)) {
                                putDI(m.getStart() + i, false);
                            }
                        }
                    }

                    respond(message, receivedClrD(m, m.acknowledge()));
                    
                    break;
                }
                case UDRMessage.C_SCLOCK: {
                    UDR_SetClock m = (UDR_SetClock)msg;
                    
                    clockOffset = (int)(System.currentTimeMillis() / 1000) - m.getTime();
                    
                    respond(message, receivedSClock(m, m.acknowledge(UDR_SetClock.S_ST_CLOCKSET)));
                    
                    break;
                }
                case UDRMessage.C_GCLOCK: {
                    UDR_GetClock m = (UDR_GetClock)msg;
                    
                    respond(message, receivedGClock(m, m.acknowledge((int)(System.currentTimeMillis() / 1000) - clockOffset)));
                    
                    break;
                }
                case UDRMessage.C_IOXCHG: {
                    //Utils.debug("Received:\n" + msg);
                    
                    UDR_IOExchange m = (UDR_IOExchange)msg;
                    
                    byte[] dIns = null;
                    byte[] dOuts = null;
                    short[] aIns = null;
                    short[] aOuts = null;
                    
                    if (m.writeDIn()) {
                        //Utils.debug("Write " + m.getDInCount() + " discrete ins");
                        
                        // Writing discrete ins (like putb)
                        for (int i = 0; i < m.getDInCount(); i++) {
                            //Utils.debug("  " + i + ": " + m.getDIn(i));
                            putBI(i, m.getDIn(i));
                        }
                    } else {
                        //Utils.debug("Read " + m.getDInCount() + " discrete ins");
                        
                        // Reading discrete ins (like getb)
                        dIns = new byte[m.getDInCount()];
                        for (int i = 0; i < dIns.length; i++) {
                            dIns[i] = getBI(i);
                        }
                    }
                    
                    if (m.writeDOut()) {
                        //Utils.debug("Write " + m.getDOutCount() + " discrete outs");
                        
                        // Writing discrete outs
                        for (int i = 0; i < m.getDOutCount(); i++) {
                            //Utils.debug("  " + i + ": " + m.getDOut(i));
                            putBO(i, m.getDOut(i));
                        }
                    } else {
                        //Utils.debug("Read " + m.getDOutCount() + " discrete outs");
                        
                        // Reading discrete outs
                        dOuts = new byte[m.getDOutCount()];
                        for (int i = 0; i < dOuts.length; i++) {
                            dOuts[i] = getBO(i);
                        }
                    }
                    
                    if (m.writeAIn()) {
                        //Utils.debug("Write " + m.getAInCount() + " analog ins");
                        
                        // Writing analog ins
                        for (int i = 0; i < m.getAInCount(); i++) {
                            //Utils.debug("  " + i + ": " + m.getAIn(i));
                            putAI(i, m.getAIn(i));
                        }
                    } else {
                        //Utils.debug("Read " + m.getAInCount() + " analog ins");
                        
                        // Reading analog ins
                        aIns = new short[m.getAInCount()];
                        for (int i = 0; i < aIns.length; i++) {
                            aIns[i] = getAI(i);
                        }
                    }
                    
                    if (m.writeAOut()) {
                        //Utils.debug("Write " + m.getAOutCount() + " analog outs");
                        
                        // Writing analog outs
                        for (int i = 0; i < m.getAOutCount(); i++) {
                            //Utils.debug("  " + i + ": " + m.getAOut(i));
                            putAO(i, m.getAOut(i));
                        }
                    } else {
                        //Utils.debug("Read " + m.getAOutCount() + " analog outs");
                        
                        // Reading analog outs
                        aOuts = new short[m.getAOutCount()];
                        for (int i = 0; i < aOuts.length; i++) {
                            aOuts[i] = getAO(i);
                        }
                    }
                    
                    respond(message, receivedIOXCHG(m, m.acknowledge(dIns, dOuts, aIns, aOuts)));
                    
                    break;
                }
                default: {
                    respond(message, msg.nAcknowledge()); // Anything else is not supported
                }
            } // switch (msg.getCommand())
        } catch (Exception e) {
            // Something went wrong, do a negative-acknowledge
            // It was probably an array-out-of-bounds thing, but we're just going to do a catch-all
            e.printStackTrace();
            respond(message, msg.nAcknowledge());
        }
    } // process()
    
    // Subclasses may override these
    // The "received*" functions are the flexible way to be notified about changes
    public UDRMessage receivedVers(UDR_Version msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedGetD(UDR_GetD msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedGetB(UDR_GetB msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedGetA(UDR_GetA msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedGetS(UDR_GetS msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public void receivedPositiveAck(UDRMessage about, UDR_Acknowledge ack) { }
    public void receivedNegativeAck(UDRMessage about, UDR_NAcknowledge nak) { }
    
    public UDRMessage receivedPutD(UDR_PutD msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedPutB(UDR_PutB msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedPutA(UDR_PutA msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedSetD(UDR_SetD msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedClrD(UDR_ClearD msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedSClock(UDR_SetClock msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedGClock(UDR_GetClock msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    public UDRMessage receivedIOXCHG(UDR_IOExchange msg, UDR_Acknowledge ack) {
        return ack;
    }
    
    // The "changed*" functions are a less flexible, but easier to use way
    //   to be notified about changes
    public void changedDI(int index, boolean val) { }
    public void changedDO(int index, boolean val) { }
    public void changedBI(int index, byte val) { }
    public void changedBO(int index, byte val) { }
    public void changedAI(int index, short val) { }
    public void changedAO(int index, short val) { }
    public void changedTime(int time) { }
    
    
    // These are for easy access by the get*I and get*O functions
    private static boolean getD(byte[] data, int index) {
        int idx = index / 8;
        int bit = index % 8;
        
        return ((data[idx] & (1 << bit)) != 0);
    }
    
    private static byte getB(byte[] data, int index) {
        return data[index];
    }
    
    private static short getA(short[] data, int index) {
        return data[index];
    }
    
    // These will get data from the class
    public final boolean getDO(int index) {
        return BussedConnection.getDiscreteValue(this, BussedConnection.T_DO, (short)index);
    }
    
    public final boolean getStraightDO(int index) {
        return getD(discretesOut, index);
    }
    
    public final boolean getDI(int index) {
        return BussedConnection.getDiscreteValue(this, BussedConnection.T_DI, (short)index);
    }
    
    public final boolean getStraightDI(int index) {
        return getD(discretesIn, index);
    }
    
    public final byte getBO(int index) {
        byte val = 0;
        
        // BussedConnection does not deal with bytes, only bits and shorts, so we need to put together a result
        for (int bit = 0; bit < 8; bit++) {
            boolean b = BussedConnection.getDiscreteValue(this, BussedConnection.T_DO, (short)(index * 8 + bit));
            if (b) {
                val |= (byte)(1 << bit);
            }
        }
        
        return val;
    }
    
    public final byte getStraightBO(int index) {
        return getB(discretesOut, index);
    }
    
    public final byte getBI(int index) {
        byte val = 0;
        
        for (int bit = 0; bit < 8; bit++) {
            boolean b = BussedConnection.getDiscreteValue(this, BussedConnection.T_DI, (short)(index * 8 + bit));
            if (b) {
                val |= (byte)(1 << bit);
            }
        }
        
        return val;
    }
    
    public final byte getStraightBI(int index) {
        return getB(discretesIn, index);
    }
    
    public final short getAO(int index) {
        return BussedConnection.getAnalogValue(this, BussedConnection.T_AO, (short)index);
    }
    
    public final short getStraightAO(int index) {
        return getA(analogsOut, index);
    }
    
    public final short getAI(int index) {
        return BussedConnection.getAnalogValue(this, BussedConnection.T_AI, (short)index);
    }
    
    public final short getStraightAI(int index) {
        return getA(analogsIn, index);
    }
    
    public final int getTime() {
        return ((int)(System.currentTimeMillis() / 1000) - clockOffset);
    }
    
    // You SHOULD NOT change the values that you receive from these
    // If you do, there will be no notification to subclasses about the change
    // Use these functions ONLY if you want a fast way to read through the arrays
    public final byte[] getDiscretesOut() {
        return discretesOut;
    }
    
    public final byte[] getDiscretesIn() {
        return discretesIn;
    }
    
    public final short[] getAnalogsOut() {
        return analogsOut;
    }
    
    public final short[] getAnalogsIn() {
        return analogsIn;
    }
    
    public final byte[] getDiscretesOutCopy() {
        byte[] ans = new byte[discretesOut.length];
        System.arraycopy(discretesOut, 0, ans, 0, ans.length);
        return ans;
    }
    
    public final byte[] getDiscretesInCopy() {
        byte[] ans = new byte[discretesIn.length];
        System.arraycopy(discretesIn, 0, ans, 0, ans.length);
        return ans;
    }
    
    public final short[] getAnalogsOutCopy() {
        short[] ans = new short[analogsOut.length];
        System.arraycopy(analogsOut, 0, ans, 0, ans.length);
        return ans;
    }
    
    public final short[] getAnalogsInCopy() {
        short[] ans = new short[analogsIn.length];
        System.arraycopy(analogsIn, 0, ans, 0, ans.length);
        return ans;
    }
    
    public final short getDiscreteOutCount() {
        return (short)discretesOut.length;
    }
    
    public final short getDiscreteInCount() {
        return (short)discretesIn.length;
    }
    
    public final short getAnalogOutCount() {
        return (short)analogsOut.length;
    }
    
    public final short getAnalogInCount() {
        return (short)analogsIn.length;
    }
    
    // These are for easy access for the put*O and put*I functions
    private static void putD(byte[] data, int index, boolean val) {
        int idx = index / 8;
        int bit = index % 8;
        
        if (val) {
            data[idx] |= (byte)(1 << bit);
        } else {
            data[idx] &= (byte)(~(1 << bit));
        }
    }
    
    private static void putB(byte[] data, int index, byte val) {
        data[index] = val;
    }
    
    private static void putA(short[] data, int index, short val) {
        data[index] = val;
    }
    
    // These will set data in the class
    public final void putDO(int index, boolean val) {
        if (getDO(index) != val) {
            putD(discretesOut, index, val);
            
            BussedConnection.compute();
            
            changedDO(index, getDO(index));
        }
    }
    
    public final void putDI(int index, boolean val) {
        if (getDI(index) != val) {
            putD(discretesIn, index, val);
            
            BussedConnection.compute();
            
            changedDI(index, getDI(index));
        }
    }
    
    public final void putBO(int index, byte val) {
        if (getBO(index) != val) {
            putB(discretesOut, index, val);
            
            BussedConnection.compute();
    
            changedBO(index, getBO(index));
        }
    }
    
    public final void putBI(int index, byte val) {
        if (getBI(index) != val) {
            putB(discretesIn, index, val);
            
            BussedConnection.compute();
            
            changedBI(index, getBI(index));
        }
    }
    
    public final void putAO(int index, short val) {
        if (getAO(index) != val) {
            putA(analogsOut, index, val);
            
            BussedConnection.compute();
        
            changedAO(index, getAO(index));
        }
    }
    
    public final void putAI(int index, short val) {
        if (getAI(index) != val) {
            putA(analogsIn, index, val);
            
            BussedConnection.compute();
            
            changedAI(index, getAI(index));
        }
    }
    
    public final void setTime(int time) {
        clockOffset = (int)(System.currentTimeMillis() / 1000) - time;
        
        changedTime(time);
    }
    
    private class TimeoutChecker implements Runnable {
        private static final int DELAY = 1000; // Delay for a second after doing nothing
        public TimeoutChecker() { }
        
        public void run() {
            while (true) {
                boolean didNothing = true;
                
                synchronized(inFlight) {
                    TimeoutMessage msg = inFlight.poll();
                    
                    if (msg != null) {
                        if (msg.checkTimeout()) {
                            // Send it again (this checks retries and resets necessary fields)
                            resend(msg);
                            
                            didNothing = false;
                        }
                    }
                }
                
                if (didNothing) {
                    try {
                        Thread.sleep(DELAY);
                    } catch (InterruptedException ie) { }
                }
            }
        }
    }
}

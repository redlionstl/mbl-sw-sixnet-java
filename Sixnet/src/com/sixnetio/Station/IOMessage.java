/*
 * IOMessage.java
 *
 * Represents a message in an I/O protocol, such as UDR or Modbus.
 *
 * Jonathan Pearson
 * May 10, 2010
 *
 */

package com.sixnetio.Station;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Represents a message in an I/O protocol, such as UDR or Modbus.
 *
 * @author Jonathan Pearson
 */
public interface IOMessage
{
	/**
	 * Get a byte array representation of this message.
	 * 
	 * @throws IllegalStateException If the message is not valid.
	 */
	public byte[] getBytes()
		throws IllegalStateException;
	
	/**
	 * Dump the bytes of this message to the given output stream.
	 * 
	 * @param out The output stream to dump to. This may be flushed by the
	 *   function call.
	 * @throws IOException If there was a problem writing to the stream.
	 * @throws IllegalStateException If the message is not valid.
	 */
	public void toStream(OutputStream out)
		throws IOException, IllegalStateException;
}

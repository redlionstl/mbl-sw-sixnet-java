/*
 * TransportMethod.java
 *
 * Represents the various communications links supported by protocols
 * (especially UDR, and to a lesser extend Modbus).
 *
 * Jonathan Pearson
 * May 7, 2010
 *
 */

package com.sixnetio.Station;

/**
 * Represents the communications link over which a given protocol is working.
 *
 * @author Jonathan Pearson
 */
public enum TransportMethod
{
	/** A serial device. */
	Serial,
	
	/**
	 * A TCP link sending to a specified port, but listening on a random port.
	 */
	TCP,
	
	/**
	 * UDP datagrams sending to a specified port, but listening on a random
	 * port.
	 */
	UDP,
	
	/**
	 * UDP datagrams being broadcast on the network to a specified port, but
	 * listening on a random port. This is useful if you are sending to the
	 * broadcast station number and really want all stations to respond.
	 */
	UDPBroadcast,
	
	/**
	 * UDP datagrams, sending to the loopback interface, both sending and
	 * listening on the specified port. This is useful if you are simulating one
	 * or more stations that listen for messages from the world.
	 */
	UDPLoop,
	;
}

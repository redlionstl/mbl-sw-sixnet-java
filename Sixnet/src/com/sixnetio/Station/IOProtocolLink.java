/*
 * IOProtocolLink.java
 *
 * Represents a link between this program and a device using an I/O protocol
 * such as UDR or Modbus.
 *
 * Jonathan Pearson
 * May 10, 2010
 *
 */

package com.sixnetio.Station;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sixnetio.COM.*;
import com.sixnetio.io.UDPInputStream;
import com.sixnetio.io.UDPOutputStream;
import com.sixnetio.util.Utils;

/**
 * Represents a link between this program and a device using an I/O protocol
 * such as UDR or Modbus.
 *
 * @author Jonathan Pearson
 */
public abstract class IOProtocolLink
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private static int linkCount = 0;
	private static final Object l_linkCount = new Object();
	
	protected final TransportMethod transport;
	protected final String address;
	protected final Map<TransportMethod, String> defaultParams;
	
	protected DatagramSocket udpSock;
	protected Socket tcpSock;
	protected InputStream input;
	protected OutputStream output;
	protected boolean closed = true;
	protected String description;
	protected final int linkIndex;
	
	/**
	 * Construct a new IOProtocolLink.
	 * 
	 * @param transport The transport method.
	 * @param address The address of the device with which to communicate:
	 *   <p>
	 *     For TCP/UDP methods, this is specified as the host name or IP address
	 *     ("localhost" or equivalent for UDPLoop, the broadcast address for
	 *     UDPBroadcast)), optionally followed by a single space and then the
	 *     port number.
	 *   </p>
	 *   <p>
	 *     For Serial, it is the path to the serial device, optionally followed
	 *     by a single space and then the communications parameters in the
	 *     format required by {@link ComPort#openComPort(String, String)}.
	 *   </p>
	 * @param defaultParams A map of transport method onto the default
	 *   parameters to use for that transport method, in case the specified
	 *   address does not contain them. If the entry corresponding to the
	 *   specified transport method is <code>null</code>, it is assumed that
	 *   the protocol does not work over that method and an IOException will
	 *   be thrown.
	 * @throws IOException If there was a problem opening the connection.
	 */
	public IOProtocolLink(TransportMethod transport, String address,
	                      Map<TransportMethod, String> defaultParams)
		throws IOException
	{
		this.transport = transport;
		this.address = address;
		this.defaultParams = new HashMap<TransportMethod, String>(defaultParams);
		
		synchronized (l_linkCount) {
			linkIndex = linkCount++;
		}
		
		if ( ! defaultParams.containsKey(transport)) {
			// Not a supported method
			throw new IOException("Protocol implemented by '" +
			                      this.getClass().getName() +
			                      "' does not support transport method " +
			                      transport);
		}
		
		connect();
		
		logger.info("New IOProtocolLink: " + description);
	}
	
	/**
	 * Close any open connection.
	 */
	@Override
	protected void finalize()
	{
		close();
	}
	
	/**
	 * Connect to the device, if disconnected (closed)
	 * 
	 * @throws IOException If the connection could not be established.
	 */
	public void connect()
		throws IOException
	{
		if ( ! closed) {
			// Nothing to do
			logger.warn("Already connected");
			return;
		}
		
		String addr;
		String params;
		
		if (address.indexOf(' ') == -1) {
			addr = address;
			params = defaultParams.get(transport);
		}
		else {
			addr = address.substring(0, address.indexOf(' '));
			params = address.substring(address.indexOf(' ') + 1);
		}
		
		switch (transport) {
			case Serial: {
				input = new COMInputStream(addr, params);
				output = new COMOutputStream(addr);
				
				description = "Serial: " + addr;
				break;
			}
			case TCP:
			case UDP:
			case UDPBroadcast:
			case UDPLoop: {
				InetAddress inaddr = InetAddress.getByName(addr);
				
				int port = Integer.parseInt(params);
				
				if (transport == TransportMethod.TCP) {
					logger.info(String.format("Connecting to %s:%d...",
					                          inaddr.getHostAddress(), port));
					tcpSock = new Socket(inaddr, port);
					
					try {
						input = tcpSock.getInputStream();
						output = tcpSock.getOutputStream();
					}
					catch (IOException ioe) {
						// If there was a problem, close the socket and clean up
						logger.error("Unable to acquire socket streams", ioe);
						
						try {
							tcpSock.close();
						}
						catch (IOException ioe2) {
							logger.warn("Unable to close socket", ioe2);
						}
						
						input = null;
						output = null;
						
						throw ioe;
					}
					
					description = "TCP: " + inaddr.getHostAddress();
				}
				else if (transport == TransportMethod.UDP ||
				         transport == TransportMethod.UDPBroadcast) {
					udpSock = new DatagramSocket();
					
					try {
						udpSock.setBroadcast(transport == TransportMethod.UDPBroadcast);
					}
					catch (IOException ioe) {
						// If there was a problem, close the socket and clean up
						logger.error("Unable to set broadcast", ioe);
						
						udpSock.close();
						
						input = null;
						output = null;
						
						throw ioe;
					}
					
					input = new UDPInputStream(udpSock);
					output = new UDPOutputStream(udpSock, inaddr, port);
					
					if (transport == TransportMethod.UDPBroadcast) {
						description = "UDP BC: " + inaddr.getHostAddress();
					}
					else {
						description = "UDP: " + inaddr.getHostAddress();
					}
				}
				else if (transport == TransportMethod.UDPLoop) {
					udpSock = new MulticastSocket(port);
					
					input = new UDPInputStream(udpSock);
					output = new UDPOutputStream(udpSock,
					                             InetAddress.getByName(addr),
					                             port);
					
					description = "UDP Loopback: " + addr;
				}
				else {
					throw new RuntimeException("Entry in switch/case missing in if block");
				}
				
				break;
			}
			default:
				throw new IOException("Unrecognized transport: " + transport);
		}
		
		description = linkCount + "(" + description + ")";
		
		closed = false;
	}
	
	/**
	 * Close all streams/devices open by this handler.
	 */
	public void close()
	{
		if (closed) {
			// Nothing to do
			logger.warn("Already closed");
			return;
		}
		
		if (logger.isDebugEnabled()) {
			Throwable th = new Throwable();
			th.fillInStackTrace();
			logger.debug("Link " + getDescription() + " closed by", th);
		}
		
		try {
			input.close();
		}
		catch (IOException ioe) {
			logger.warn("IOException while closing a stream", ioe);
		}
		
		try {
			output.close();
		}
		catch (IOException ioe) {
			logger.warn("IOException while closing a stream", ioe);
		}
		
		if (tcpSock != null) {
			try {
				tcpSock.close();
			}
			catch (IOException ioe) {
				logger.warn("IOException while closing a socket", ioe);
			}
			
			tcpSock = null;
		}
		
		if (udpSock != null) {
			udpSock.close();
			udpSock = null;
		}
		
		closed = true;
	}
	
	/**
	 * Test whether this handler has been closed.
	 * 
	 * @return True if close() has been called, false otherwise.
	 */
	public boolean isClosed()
	{
		return closed;
	}
	
	/** Get the transport method being used by this link. */
	public TransportMethod getTransportMethod()
	{
		return transport;
	}
	
	/**
	 * Send a message to the connected device.
	 * 
	 * @param msg The message to send.
	 * @throws IOException If there was a problem sending the message.
	 * @throws IllegalStateException If the message was invalid.
	 */
	public void send(IOMessage msg)
		throws IOException, IllegalStateException
	{
		if (closed) {
			throw new IOException("Cannot send on a closed link");
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("Sending message via %s:\n%s",
			                           description, msg.toString()));
		}
		
		msg.toStream(output);
		output.flush();
	}
	
	/**
	 * Send a message via UDP to a specific device.
	 * 
	 * @param msg The message to send.
	 * @param addr The address to send to.
	 * @param port The port to send to.
	 * @throws IOException If there was a problem sending the message.
	 * @throws IllegalStateException If the message was invalid.
	 */
	public void sendTo(IOMessage msg, InetAddress addr, int port)
		throws IOException, IllegalStateException
	{
		if (closed) {
			throw new IOException("Cannot send on a closed link");
		}
		else if ( ! (output instanceof UDPOutputStream)) {
			throw new IllegalStateException("Cannot use sendTo on a non-UDP link");
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("Sending message to %s:%d:\n%s",
			                           addr.getHostAddress(), port,
			                           msg.toString()));
		}
		
		byte[] buf = msg.getBytes();
		((UDPOutputStream)output).writeTo(buf, 0, buf.length, addr, port);
	}
	
	/**
	 * Receive a message from the connected device.
	 * 
	 * @param expectAck Whether to expect this message to be an acknowledgment.
	 *   Not necessarily relevant to all protocols.
	 * @return The received message.
	 * @throws IOException If there was a problem receiving the message.
	 */
	public abstract IOMessage receive(boolean expectAck)
		throws IOException;
	
	/**
	 * Receive a message from any UDP device.
	 * 
	 * @param expectAck Whether to expect this message to be an acknowledgment.
	 * @param out_addr Returns the address the message was received from in [0].
	 * @param out_port Returns the port the message was received from in [0].
	 * @return The message that was received.
	 * @throws IOException If there was a problem receiving the message.
	 */
	public abstract IOMessage receive(boolean expectAck, InetAddress[] out_addr,
	                                  int[] out_port)
		throws IOException;
	
	/**
	 * Receive a message from the connected device. This is a wrapper around
	 * {@link #receive(boolean)} which passes <code>true</code> for
	 * <code>expectAck</code>.
	 * 
	 * @return The message received.
	 * @throws IOException If there was a problem receiving the message.
	 */
	public IOMessage receive()
		throws IOException
	{
		return receive(true);
	}
	
	/**
	 * Receive a message from the connected device. This is a wrapper around
	 * {@link #receive(boolean, InetAddress[], int[])} which passes
	 * <code>true</code> for <code>expectAck</code>.
	 * 
	 * @param out_addr Returns the address the message was received from in [0].
	 * @param out_port Returns the port the message was received from in [0].
	 * @return The message that was received.
	 * @throws IOException If there was a problem receiving the message.
	 */
	public IOMessage receive(InetAddress[] out_addr, int[] out_port)
		throws IOException
	{
		return receive(true, out_addr, out_port);
	}
	
	/**
	 * Is this ModbusHandler working over UDP?
	 * 
	 * @return True if this handler is working over UDP, false otherwise.
	 */
	public boolean overUDP()
	{
		if (input instanceof UDPInputStream &&
		    output instanceof UDPOutputStream) {
			
			return true;
		}
		else if (output instanceof UDPOutputStream ||
		         input instanceof UDPInputStream) {
			throw new RuntimeException("Input/output streams are differe types: " +
			                           input.getClass().getName() + "/" +
			                           output.getClass().getName());
		}
		else {
			return false;
		}
	}
	
	/**
	 * Gets a description of this handler. Contains data such as how and where
	 * it is connected.
	 */
	public String getDescription()
	{
		return description;
	}
}

/*
 * IOStation.java
 *
 * Mimics an I/O station attached to a user interface. Outputs are
 * set via UDR, inputs are set via the UI. Writing an output over
 * the network will signal the UI what happened; reading an input
 * over the network will query the UI for a value.
 *
 * Jonathan Pearson
 * April 16, 2007
 *
 */

package com.sixnetio.Station;

import com.sixnetio.UDR.*;

public class IOStation extends Station {
    private IOStationUI ui;
    
    // Some default values that the UI can set
    // VERS info
    // Non-SIXNET supporting non-byte boundaries
    // Firmware version 1.0
    public short V_ProdCode = (short)254;
    public byte V_Major = (byte)1;
    public byte V_Minor = (byte)0;
    
    
    public IOStation(MessageDispatcher dispatch, short myID, short discretes, short analogs, IOStationUI ui) {
        this(dispatch, myID, discretes, discretes, analogs, analogs, ui);
    }
    
    public IOStation(MessageDispatcher dispatch, short myID, 
                     short discretesIn, short discretesOut,
                     short analogsIn, short analogsOut,
                     IOStationUI ui) {
        
        super(dispatch, myID, discretesIn, discretesOut, analogsIn, analogsOut);

        this.ui = ui;
        
        // This is a reactive station, so we don't need a new thread
    }
    
    // Overridden functions from Station
    public UDRMessage receivedVers(UDR_Version msg, UDR_Acknowledge ack) {
        return msg.acknowledge(V_ProdCode, V_Major, V_Minor);
    }
    
    public void changedDI(int index, boolean val) {
        ui.putDI(this, index, val);
    }
    
    public void changedDO(int index, boolean val) {
        ui.putDO(this, index, val);
    }
    
    public void changedBI(int index, byte val) {
        for (int bit = 0; bit < 8; bit++) {
            ui.putDI(this, index * 8 + bit, (val & (1 << bit)) != 0);
        }
    }
    
    public void changedBO(int index, byte val) {
        for (int bit = 0; bit < 8; bit++) {
            ui.putDO(this, index * 8 + bit, (val & (1 << bit)) != 0);
        }
    }
    
    public void changedAI(int index, short val) {
        ui.putAI(this, index, val);
    }
    
    public void changedAO(int index, short val) {
        ui.putAO(this, index, val);
    }
}

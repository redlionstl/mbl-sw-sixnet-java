/*
 * ModbusLib.java
 *
 * A library of Modbus function wrappers to make normal coding easier. Base on
 * UDRLib. Note that Modbus does not work with the MessageDispatcher.
 *
 * Jonathan Pearson
 * May 10, 2010
 *
 */

package com.sixnetio.Station;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.sixnetio.Modbus.*;
import com.sixnetio.util.Utils;

/**
 * A library of Modbus-backed functions.
 *
 * @author Jonathan Pearson
 */
public class ModbusLib
	extends MessageLib
{
	static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private byte station;
	private short sequence = 0;
	private final Object l_sequence = new Object();
	
	/**
	 * Construct a new ModbusLib. This starts a new daemon thread which will die
	 * when the link is closed (no other way to interrupt the receiver's read
	 * call).
	 * 
	 * @param link The link over which to communicate.
	 * @param station The station number to send messages to.
	 */
	public ModbusLib(ModbusLink link, byte station)
	{
		super(link);
		
		this.station = station;
	}
	
	/** Get the station number that this library communicates with. */
	public byte getStation()
	{
		return station;
	}
	
	/** Set the station number that this library communicates with. */
	public void setStation(byte station)
	{
		this.station = station;
	}
	
	@Override
	public void putAO(short addr, short val)
		throws IOException, TimeoutException
	{
		send(new MB_AOWrite(false, addr, val));
	}
	
	@Override
	public void putAO(short start, short[] vals)
		throws IOException, TimeoutException
	{
		send(new MB_MultiAOWrite(start, vals));
	}
	
	@Override
	public void putDO(short addr, boolean val)
		throws IOException, TimeoutException
	{
		send(new MB_DOWrite(false, addr, val));
	}
	
	@Override
	public void putDO(short start, boolean[] vals)
		throws IOException, TimeoutException
	{
		send(new MB_MultiDOWrite(start, vals));
	}
	
	@Override
	public short[] getAI(short start, short count)
		throws IOException, TimeoutException
	{
		return readAnalogs(new MB_AIRead(start, count));
	}
	
	@Override
	public short[] getAO(short start, short count)
		throws IOException, TimeoutException
	{
		return readAnalogs(new MB_AORead(start, count));
	}
	
	@Override
	public boolean[] getDI(short start, short count)
		throws IOException, TimeoutException
	{
		return readDiscretes(new MB_DIRead(start, count));
	}
	
	@Override
	public boolean[] getDO(short start, short count)
		throws IOException, TimeoutException
	{
		return readDiscretes(new MB_DORead(start, count));
	}
	
	@Override
	public byte[] getBI(short start, short count)
		throws IOException, TimeoutException
	{
		return readBytes(new MB_DORead((short)(start * 8), (short)(count * 8)));
	}
	
	@Override
	public byte[] getBO(short start, short count)
		throws IOException, TimeoutException
	{
		return readBytes(new MB_DIRead((short)(start * 8), (short)(count * 8)));
	}
	
	@Override
	public void clearDO(short start, boolean[] vals)
		throws IOException, TimeoutException
	{
		boolean[] current = getDO(start, (short)vals.length);
		
		for (int i = 0; i < current.length && i < vals.length; i++) {
			current[i] &= ! vals[i];
		}
		
		putDO(start, current);
	}
	
	@Override
	public void setDO(short start, boolean[] vals)
		throws IOException, TimeoutException
	{
		boolean[] current = getDO(start, (short)vals.length);
		
		for (int i = 0; i < current.length && i < vals.length; i++) {
			current[i] |= vals[i];
		}
		
		putDO(start, current);
	}
	
	/**
	 * Read the exception byte from the device.
	 * 
	 * @return The exception byte. The meaning of this byte is device-specific.
	 * @throws IOException If there was a problem communicating with the device.
	 * @throws TimeoutException If no response was received after the tries and
	 *   timeouts.
	 */
	public byte readException()
		throws IOException, TimeoutException
	{
		ModbusMessage response = send(new MB_ReadException());
		
		checkResponse(response, MB_ReadException.class);
		
		return ((MB_ReadException)response).getCode();
	}
	
	/**
	 * Get the slave ID from the device.
	 * 
	 * @return The slave ID. The meaning of this field is device-specific.
	 * @throws IOException If there was a problem communicating with the device.
	 * @throws TimeoutException If no response was received after the tries and
	 *   timeouts.
	 */
	public byte[] getSlaveID()
		throws IOException, TimeoutException
	{
		ModbusMessage response = send(new MB_ReportSlaveID());
		
		checkResponse(response, MB_ReportSlaveID.class);
		
		return ((MB_ReportSlaveID)response).getSlaveID();
	}
	
	/**
	 * Read a range of analog registers.
	 * 
	 * @param msg The message to use to read the registers.
	 * @return The values of the registers that were read.
	 * @throws IOException If there was a problem sending the message, or if the
	 *   response was invalid.
	 * @throws TimeoutException If no response was received after the tries and
	 *   timeouts.
	 */
	private short[] readAnalogs(MB_AnalogRead msg)
		throws IOException, TimeoutException
	{
		ModbusMessage response = send(msg);
		
		checkResponse(response, msg.getClass());
		
		return ((MB_AnalogRead)response).getReturnedRegisters();
	}
	
	/**
	 * Read a range of discrete registers.
	 * 
	 * @param msg The message to use to read the registers.
	 * @return The values of the registers that were read.
	 * @throws IOException If there was a problem sending the message, or if the
	 *   response was invalid.
	 * @throws TimeoutException If no response was received after the tries and
	 *   timeouts.
	 */
	private boolean[] readDiscretes(MB_DiscreteRead msg)
		throws IOException, TimeoutException
	{
		ModbusMessage response = send(msg);
		
		checkResponse(response, msg.getClass());
		
		return ((MB_DiscreteRead)response).getReturnedRegisters(msg.getCount());
	}
	
	private byte[] readBytes(MB_DiscreteRead msg)
		throws IOException, TimeoutException
	{
		ModbusMessage response = send(msg);
		
		checkResponse(response, msg.getClass());
		
		return ((MB_DiscreteRead)response).getReturnedBytes();
	}
	
	/**
	 * Check whether a response matches a request message.
	 * 
	 * @param response The response message
	 * @param clazz The class of the request message (expected to be the class
	 *   of the response message).
	 * @throws IOException If the verification failed, meaning the response does
	 *   not match the request.
	 */
	private void checkResponse(ModbusMessage response,
	                           Class<? extends ModbusMessage> clazz)
		throws IOException
	{
		if ( ! (response.getClass().isAssignableFrom(clazz))) {
			logger.info(String.format("Response to %1$s is not a %1$s: %2$s",
			            clazz.getSimpleName(),
			            response.getClass().getName()));
			throw new IOException(String.format("Bad response to %s: %s",
			                                    clazz.getSimpleName(),
			                                    response.getClass().getSimpleName()));
		}
		else if (response.isAcknowledgment() == null ||
		         response.isAcknowledgment() == false) {
			if (logger.isInfoEnabled()) {
				logger.info(String.format("Response to %s is not an acknowledgment:\n%s",
				                          clazz.getSimpleName(),
				                          response.toString()));
			}
			throw new IOException(String.format("Response to %s is not an acknowledgment",
			                                    clazz.getClass().getSimpleName()));
		}
	}
	
	@Override
	public IOMessage send(IOMessage msg)
		throws IOException, TimeoutException, IllegalArgumentException
	{
		if (msg instanceof ModbusMessage) {
			return send((ModbusMessage)msg);
		}
		else {
			throw new IllegalArgumentException(String.format("%s is not an instance of ModbusMessage",
			                                                 msg.getClass().getName()));
		}
	}
	
	/**
	 * Send a message and wait for a response.
	 * 
	 * @param msg The message to send.
	 * @return The response that was returned, if it was successful.
	 * @throws IOException If there was an IO error sending the message, or if
	 *   the response was an error message.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeouts.
	 */
	public synchronized ModbusMessage send(ModbusMessage msg)
		throws IOException, TimeoutException
	{
		ModbusMessage response = null;
		
		synchronized (l_sequence) {
			msg.setup(sequence++, station);
		}
		
		for (int tries = 0; tries < getTries() && response == null; tries++) {
			if (tries > 0) {
				logger.info("Retry " + tries);
			}
			
			link.send(msg);
			
			long stopAt = System.currentTimeMillis() + timeout;
			
			synchronized (inQueue) {
				while ((response = checkQueue(msg)) == null &&
				       (timeout <= 0 || System.currentTimeMillis() < stopAt)) {
					
					try {
						inQueue.wait(stopAt - System.currentTimeMillis());
					}
					catch (InterruptedException ie) {
						logger.warn("Interrupted waiting for a response",
						            ie);
						stopAt = System.currentTimeMillis(); // Stop now
					}
				}
			}
		}
		
		logger.debug("End of tries loop for " + link.getDescription() +
		             "; response is " +
		             (response == null ? "null" : "not null"));
		
		if (response == null) {
			logger.error("Timed out");
			throw new TimeoutException("Timed out waiting for a response");
		}
		else if (response.isError() != null && response.isError() == true) {
			if (logger.isEnabledFor(Level.ERROR)) {
				logger.error("Error response from device:\n" +
				             response.toString());
			}
			
			throw new IOException("Error response: " +
			                      response.getErrorMessage());
		}
		
		logger.info("Received a response");
		return response;
	}
	
	/**
	 * Search the input queue for a message whose header matches the given
	 * message. You <b>must</b> hold the monitor on the queue when calling this.
	 * 
	 * @param msg The message whose header to search for.
	 * @return The first matching message, or <code>null</code> if none could be
	 *   found. The matching message will be removed from the queue.
	 */
	private ModbusMessage checkQueue(ModbusMessage msg)
	{
		assert(Thread.holdsLock(inQueue));
		
		// Loop through all received messages, checking for the
		// response
		for (Iterator<IOMessage> itInQueue = inQueue.iterator();
		     itInQueue.hasNext(); ) {
			
			IOMessage recv = itInQueue.next();
			itInQueue.remove();
			
			if (recv instanceof ModbusMessage) {
				ModbusMessage r = (ModbusMessage)recv;
				
				if (headersEqual(r, msg)) {
					return r;
				}
				else if (logger.isDebugEnabled()) {
					logger.debug("Received an unexpected message:\n" +
					             "Sent:\n" + msg.toString(2) +
					             "Received:\n" + r.toString(2));
					logger.debug(String.format("Sequence: %d == %d (%b)",
					                           r.getSequence(),
					                           msg.getSequence(),
					                           r.getSequence().equals(msg.getSequence())));
					logger.debug(String.format("Protocol: %d == %d (%b)",
					                           r.getProtocol(),
					                           msg.getProtocol(),
					                           r.getProtocol().equals(msg.getProtocol())));
					logger.debug(String.format("Station: %d == %d (%b)",
					                           r.getStation(),
					                           msg.getStation(),
					                           r.getStation().equals(msg.getStation())));
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Compare the headers of two Modbus messages to see if they are likely a
	 * request/response pair. Compares the sequence, protocol, and station
	 * fields. Obviously does not compare length fields.
	 * 
	 * @param a The first message.
	 * @param b The second message.
	 * @return True if the messages have equal header fields, false if not.
	 */
	private boolean headersEqual(ModbusMessage a, ModbusMessage b)
	{
		return (a.getSequence().equals(b.getSequence()) &&
		        a.getProtocol().equals(b.getProtocol()) &&
		        a.getStation().equals(b.getStation()));
	}
}

/*
 * SimpleUDRLib.java
 *
 * A library of UDR function wrappers to make normal coding easier. Based on
 * ModbusLib, in that it does not work with the MessageDispatcher.
 *
 * Jonathan Pearson
 * May 26, 2010
 *
 */

package com.sixnetio.Station;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.sixnetio.GUI.ProgressListener;
import com.sixnetio.UDR.*;
import com.sixnetio.UDR.Filesys.*;
import com.sixnetio.lang.IntRef;
import com.sixnetio.util.Utils;

/**
 * A library of UDR-backed functions.
 * 
 * @author Jonathan Pearson
 */
public class SimpleUDRLib
    extends MessageLib
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /** The maximum number of bytes for a file data transfer message. */
    public static final int BLOCKSIZE = 237;

    /**
     * Specify this as the creation size for a directory to format an ET-2
     * partition.
     */
    public static final int FORMAT_MAGIC = 0x12345678;

    private short station;
    private byte session;
    private byte sequence = 0;
    private final Object l_sequence = new Object();

    /**
     * Construct a new SimpleUDRLib. This starts a new daemon thread which will
     * die when the link is closed (no other way to interrupt the receiver's
     * read call).
     * 
     * @param link The link over which to communicate.
     * @param station The station number to send messages to. The local station
     *            number will be the broadcast number.
     * @param session The session number to use.
     */
    public SimpleUDRLib(UDRLink link, short station, byte session)
    {
        super(link);

        this.station = station;
    }

    /** Get the station number that this library communicates with. */
    public short getStation()
    {
        return station;
    }

    /** Set the station number that this library communicates with. */
    public void setStation(short station)
    {
        this.station = station;
    }

    /** Get the session number. */
    public byte getSession()
    {
        return session;
    }

    /** Set the session number. */
    public void setSession(byte session)
    {
        this.session = session;
    }

    @Override
    public void putAO(short addr, short val)
        throws IOException, TimeoutException
    {
        send(new UDR_PutA(UDRMessage.T_D_AOUT, addr, (short)1, new short[] {
            val
        }));
    }

    @Override
    public void putAO(short start, short[] vals)
        throws IOException, TimeoutException
    {
        send(new UDR_PutA(UDRMessage.T_D_AOUT, start, (short)vals.length, vals));
    }

    @Override
    public void putDO(short addr, boolean val)
        throws IOException, TimeoutException
    {
        send(new UDR_PutD(UDRMessage.T_D_DOUT, addr, (short)1, new boolean[] {
            val
        }));
    }

    @Override
    public void putDO(short start, boolean[] vals)
        throws IOException, TimeoutException
    {
        send(new UDR_PutD(UDRMessage.T_D_DOUT, start, (short)vals.length, vals));
    }

    @Override
    public short[] getAI(short start, short count)
        throws IOException, TimeoutException
    {
        return readAnalogs(new UDR_GetA(UDRMessage.T_D_AIN, start, count));
    }

    @Override
    public short[] getAO(short start, short count)
        throws IOException, TimeoutException
    {
        return readAnalogs(new UDR_GetA(UDRMessage.T_D_AOUT, start, count));
    }

    @Override
    public boolean[] getDI(short start, short count)
        throws IOException, TimeoutException
    {
        return readDiscretes(new UDR_GetD(UDRMessage.T_D_DIN, start, count));
    }

    @Override
    public boolean[] getDO(short start, short count)
        throws IOException, TimeoutException
    {
        return readDiscretes(new UDR_GetD(UDRMessage.T_D_DOUT, start, count));
    }

    @Override
    public byte[] getBI(short start, short count)
        throws IOException, TimeoutException
    {
        return readBytes(new UDR_GetB(UDRMessage.T_D_DIN, start, count));
    }

    @Override
    public byte[] getBO(short start, short count)
        throws IOException, TimeoutException
    {
        return readBytes(new UDR_GetB(UDRMessage.T_D_DOUT, start, count));
    }

    @Override
    public void clearDO(short start, boolean[] vals)
        throws IOException, TimeoutException
    {
        send(new UDR_ClearD(UDRMessage.T_D_DOUT, start, (short)vals.length,
            vals));
    }

    @Override
    public void setDO(short start, boolean[] vals)
        throws IOException, TimeoutException
    {
        send(new UDR_SetD(UDRMessage.T_D_DOUT, start, (short)vals.length, vals));
    }

    /**
     * Get the number of I/O points of the given type that exist in the station.
     * 
     * @param type The data type (one of the UDRMessage.T_* values).
     * @return The number of I/O points of that type.
     */
    public short nio(byte type)
        throws IOException, TimeoutException
    {
        UDR_Acknowledge ack = send(new UDR_NumberIO(type));

        switch (type) {
            // Analog types need to use getAnalogs()
            case UDRMessage.T_C_AIN:
            case UDRMessage.T_C_AOUT:
            case UDRMessage.T_D_AIN:
            case UDRMessage.T_D_AOUT:
            case UDRMessage.T_P_AIN:
            case UDRMessage.T_P_AOUT:
                return UDR_NumberIO.getAnalogs(ack);

                // Everything else (discretes especially) can use getDiscretes()
            default:
                return UDR_NumberIO.getDiscretes(ack);
        }
    }

    /**
     * Create a new file on a remote device.
     * 
     * @param path The path of the file to create.
     * @param size The size of the file to create. Specify {@link #FORMAT_MAGIC}
     *            and a path to a directory to format an ET-2 partition.
     * @param alias If non-<code>null</code>, returns a read-write handle to the
     *            created file. Otherwise, the file is closed.
     * @throws IOException If there was a problem communicating with the device.
     *             Up to {@link #getTries()} attempts are made, each with the
     *             station possibly responding with an error to each, before
     *             giving up. A timeout is not given extra attempts.
     * @throws TimeoutException If any attempt timed out.
     */
    public void createFile(String path, int size, IntRef alias)
        throws IOException, TimeoutException
    {
        boolean closeAtEnd = false;
        if (alias == null) {
            alias = new IntRef(-1);
            closeAtEnd = true;
        }

        try {
            UDR_Acknowledge ack = null;
            int tries = 0;
            boolean done = false;
            while (!done && tries < getTries()) {
                ack = send(new FS_Create(FS_Create.O_RDWR, size, path));

                if (FS_Create.getError(ack) == FS_Create.E_NO_ERROR) {
                    logger.info("Created file '" + path + "'");
                    alias.set(FS_Create.getAlias(ack));
                    done = true;
                }
                else {
                    // Keep trying
                    logger.warn("Failed to create file '" + path + "': " +
                                FS_Create.translateError(ack));
                    tries++;
                    Utils.sleep(100);
                }
            }

            if (!done) {
                logger.error("Failed to create file  '" + path +
                             "', no tries left: " +
                             FS_Create.translateError(ack));
                throw new IOException("Failed to create file '" + path + "': " +
                                      FS_Create.translateError(ack));
            }
        }
        finally {
            if (closeAtEnd) {
                try {
                    closeFile(alias);
                }
                catch (Exception e) {
                    logger.warn("Error closing alias", e);
                }
            }
        }
    }

    /**
     * Open an existing file on a device.
     * 
     * @param path The path to the file.
     * @param alias If non-<code>null</code>, returns a read-write handle to the
     *            opened file. Otherwise, the file is closed.
     * @throws IOException If there was a problem communicating with the device.
     *             Up to {@link #getTries()} attempts are made, each with the
     *             station possibly responding with an error to each, before
     *             giving up. A timeout is not given extra attempts.
     * @throws TimeoutException If any attempt timed out.
     */
    public void openFile(String path, IntRef alias)
        throws IOException, TimeoutException
    {
        boolean closeAtEnd = false;
        if (alias == null) {
            alias = new IntRef(-1);
            closeAtEnd = true;
        }

        try {
            UDR_Acknowledge ack = null;
            int tries = 0;
            boolean done = false;
            while (!done && tries < getTries()) {
                ack = send(new FS_GetAlias(FS_GetAlias.O_RDWR, path));

                if (FS_GetAlias.getError(ack) == FS_GetAlias.E_NO_ERROR) {
                    logger.info("Opened file '" + path + "'");
                    alias.set(FS_GetAlias.getAlias(ack));
                    done = true;
                }
                else {
                    // Keep trying
                    logger.warn("Failed to open file '" + path + "': " +
                                FS_GetAlias.translateError(ack));
                    tries++;
                    Utils.sleep(100);
                }
            }

            if (!done) {
                logger.error("Failed to open file '" + path +
                             "', no tries left: " +
                             FS_GetAlias.translateError(ack));
                throw new IOException("Failed to open file '" + path + "': " +
                                      FS_GetAlias.translateError(ack));
            }
        }
        finally {
            if (closeAtEnd) {
                try {
                    closeFile(alias);
                }
                catch (Exception e) {
                    logger.warn("Error closing alias", e);
                }
            }
        }
    }

    /**
     * Close an open file handle.
     * 
     * @param alias The open file handle. Will be set to -1.
     * @throws IOException If there is a problem communicating with the device.
     *             No extra attempts are made for this message.
     * @throws TimeoutException If no response is received after all tries and
     *             timeouts.
     */
    public void closeFile(IntRef alias)
        throws IOException, TimeoutException
    {
        if (alias == null) {
            // Nothing to do
            return;
        }

        if (alias.intValue() == -1) {
            // Nothing to do
            logger.debug("Skipping close operation, alias is -1");
            return;
        }

        send(new FS_CloseAlias(alias.intValue()));
        logger.info("Closed alias " + alias.intValue());
        alias.set(-1);
    }

    /**
     * Stat a file on a remote device.
     * 
     * @param path The path to the file.
     * @param alias If non-<code>null</code>, receives a read-write handle to
     *            the file. You may pass in an existing handle if you have one,
     *            otherwise set this to -1.
     * @return The stat information.
     * @throws IOException If there was a problem communicating with the device.
     *             Up to {@link #getTries()} attempts are made, each with the
     *             station possibly responding with an error to each, before
     *             giving up. A timeout is not given extra attempts.
     * @throws TimeoutException If any attempt timed out.
     */
    public FS_Stat.Stat statFile(String path, IntRef alias)
        throws IOException, TimeoutException
    {
        boolean closeAtEnd = false;
        if (alias == null) {
            alias = new IntRef(-1);
            closeAtEnd = true;
        }

        try {
            UDR_Acknowledge ack = null;
            FS_Stat.Stat response = null;
            int tries = 0;
            while (response == null && tries < getTries()) {
                if (alias.intValue() == -1) {
                    openFile(path, alias);
                }

                ack = send(new FS_Stat(FS_Stat.O_NONE, alias.intValue()));

                if (FS_Stat.getError(ack) == FS_Stat.E_NO_ERROR) {
                    // We got the information we needed
                    logger.info("Stat succeeded on '" + path + "'");
                    response = FS_Stat.getStat(ack);
                }
                else if (FS_Stat.getError(ack) == FS_Stat.E_INVALID_ALIAS) {
                    // Don't count this as a try
                    logger.warn("Alias expired");
                    alias.set(-1); // Re-open on next loop
                }
                else {
                    // Some other error, keep trying
                    logger.warn("Stat failed on '" + path + "': " +
                                FS_Stat.translateError(ack));
                    tries++;
                }
            }

            if (response == null) {
                // Throw an exception describing the last error we received
                logger.error("Stat failed on '" + path + "', no tries left: " +
                             FS_Stat.translateError(ack));
                throw new IOException("Stat failed on  '" + path + "': " +
                                      FS_Stat.translateError(ack));
            }

            return response;
        }
        finally {
            if (closeAtEnd) {
                try {
                    closeFile(alias);
                }
                catch (Exception e) {
                    // Ignore it, just log the error
                    logger.warn("Error closing alias", e);
                }
            }
        }
    }

    /**
     * Delete a file on a remote device.
     * 
     * @param path The path to the file to delete.
     * @throws IOException If there was a problem communicating with the device.
     *             Up to {@link #getTries()} attempts are made, each with the
     *             station possibly responding with an error to each, before
     *             giving up. A timeout is not given extra attempts.
     * @throws TimeoutException If any attempt timed out.
     */
    public void deleteFile(String path)
        throws IOException, TimeoutException
    {
        UDR_Acknowledge ack = null;
        int tries = 0;
        boolean done = false;
        while (!done && tries < getTries()) {
            ack = send(new FS_Delete(FS_Delete.O_NONE, path));
            if (FS_Delete.getError(ack) == FS_Delete.E_NO_ERROR) {
                logger.info("Deleted '" + path + "'");
                done = true;
            }
            else {
                logger.warn("Failed to delete '" + path + "': " +
                            FS_Delete.translateError(ack));
                tries++;
            }
        }

        if (!done) {
            logger.error("Failed to delete '" + path + "', no tries left: " +
                         FS_Delete.translateError(ack));
            throw new IOException("Failed to delete '" + path + "': " +
                                  FS_Delete.translateError(ack));
        }
    }

    /**
     * Rename a file on a remote device.
     * 
     * @param oldPath The old path to the file.
     * @param newPath The new path for the file.
     * @throws IOException If there was a problem communicating with the device.
     *             Up to {@link #getTries()} attempts are made, each with the
     *             station possibly responding with an error to each, before
     *             giving up. A timeout is not given extra attempts.
     * @throws TimeoutException If any attempt timed out.
     */
    public void renameFile(String oldPath, String newPath)
        throws IOException, TimeoutException
    {
        UDR_Acknowledge ack = null;
        int tries = 0;
        boolean done = false;
        while (!done && tries < getTries()) {
            ack = send(new FS_Rename(FS_Rename.O_NONE, oldPath, newPath));
            if (FS_Rename.getError(ack) == FS_Rename.E_NO_ERROR) {
                logger.info("Renamed '" + oldPath + "' to '" + newPath + "'");
                done = true;
            }
            else {
                logger.warn("Failed to rename '" + oldPath + "' to '" +
                            newPath + "': " + FS_Rename.translateError(ack));
                tries++;
            }
        }

        if (!done) {
            logger.error("Failed to rename '" + oldPath + "' to '" + newPath +
                         "', no tries left: " + FS_Rename.translateError(ack));
            throw new IOException("Failed to rename '" + oldPath + "' to '" +
                                  newPath + "': " +
                                  FS_Rename.translateError(ack));
        }
    }

    /**
     * Read a portion of a file from a remote device.
     * 
     * @param path The path to the file to read.
     * @param start The offset within the file where the read should begin.
     * @param length The number of bytes to read from the file. If -1, the file
     *            size will be queried and all bytes from <code>start</code> to
     *            the end will be read.
     * @param alias If non-<code>null</code>, is used as an open file handle for
     *            reading, and will receive a read-write handle to the file on
     *            return. Set to -1 if you just want the returned handle.
     * @param pl If non-<code>null</code>, read progress will be sent to this
     *            object.
     * @return The bytes read from the file.
     * @throws IOException If there was a problem communicating with the device.
     *             Up to {@link #getTries()} attempts are made, each with the
     *             station possibly responding with an error to each, before
     *             giving up. A timeout is not given extra attempts.
     * @throws TimeoutException If any attempt timed out.
     */
    public byte[] readFile(String path, int start, int length, IntRef alias,
                           ProgressListener pl)
        throws IOException, TimeoutException
    {
        boolean closeAtEnd = false;
        if (alias == null) {
            alias = new IntRef(-1);
            closeAtEnd = true;
        }

        if (length == -1) {
            FS_Stat.Stat stat = statFile(path, alias);
            length = stat.size - start;
        }

        try {
            byte[] data = new byte[length];
            int offset = 0;

            UDR_Acknowledge ack = null;
            FS_Stat.Stat response = null;
            int tries = 0;

            while (offset < length && tries < getTries()) {
                if (alias.intValue() == -1) {
                    openFile(path, alias);
                }

                int blockSize = Math.min(BLOCKSIZE, length - offset);
                ack =
                    send(new FS_Read(alias.intValue(), start + offset,
                        (short)blockSize));

                if (FS_Read.getError(ack, 0) == FS_Read.E_NO_ERROR) {
                    logger.info(String.format(
                        "Read [%d, %d] succeeded on '%s'", start + offset,
                        start + offset + blockSize - 1, path));

                    byte[] block = FS_Read.getData(ack, 0);
                    short blockLen = FS_Read.getNumber(ack, 0);
                    System.arraycopy(block, 0, data, offset, blockLen);

                    offset += blockLen;

                    if (pl != null) {
                        pl.updateProgress((float)offset / (float)length);
                    }
                }
                else if (FS_Read.getError(ack, 0) == FS_Read.E_INVALID_ALIAS) {
                    // Don't count this as a try
                    logger.warn("Alias expired");
                    alias.set(-1); // Re-open on next loop
                }
                else {
                    logger
                        .warn("Read error: " + FS_Read.translateError(ack, 0));
                    tries++;
                }
            }

            return data;
        }
        finally {
            if (closeAtEnd) {
                try {
                    closeFile(alias);
                }
                catch (Exception e) {
                    // Ignore it, just log the error
                    logger.warn("Error closing alias", e);
                }
            }
        }
    }

    /**
     * Write to a file on a remote device.
     * 
     * @param data Contains the data to write to the file.
     * @param path The path to the file.
     * @param start The offset within the file where writing should start.
     * @param off The offset within <code>data</code> to start reading.
     * @param len The number of bytes to write to the file. The file will be
     *            queried for its size, and if it is not large enough to receive
     *            this many bytes at the starting offset, an {@link IOException}
     *            will be thrown.
     * @param alias If non-<code>null</code>, is used as an open file handle for
     *            writing, and will receive a read-write handle to the file on
     *            return. Set to -1 if you just want the returned handle.
     * @param pl If non-<code>null</code>, write progress will be sent to this
     *            object.
     * @throws IOException If there was a problem communicating with the device.
     *             Up to {@link #getTries()} attempts are made, each with the
     *             station possibly responding with an error to each, before
     *             giving up. A timeout is not given extra attempts.
     * @throws TimeoutException If any attempt timed out.
     */
    public void writeFile(byte[] data, String path, int start, int off,
                          int len, IntRef alias, ProgressListener pl)
        throws IOException, TimeoutException
    {
        boolean closeAtEnd = false;
        if (alias == null) {
            alias = new IntRef(-1);
            closeAtEnd = true;
        }

        try {
            // Make sure the file is large enough
            FS_Stat.Stat stat = statFile(path, alias);
            if (stat.size < start + len) {
                logger.error(String.format("File '%s' is %d bytes long, must"
                                           + " be at least %d for write",
                    stat.size, start + len));
                throw new IOException("File '" + path + "' is not large enough");
            }

            int offset = 0; // Offset from 0-len, add to 'off' to get data index

            UDR_Acknowledge ack = null;
            FS_Stat.Stat response = null;
            int tries = 0;

            while (offset < len && tries < getTries()) {
                if (alias.intValue() == -1) {
                    openFile(path, alias);
                }

                int blockSize = Math.min(BLOCKSIZE, len - offset);
                byte[] block = new byte[blockSize];
                System.arraycopy(data, off + offset, block, 0, blockSize);
                ack =
                    send(new FS_Write(alias.intValue(), start + offset,
                        (short)blockSize, block));

                if (FS_Write.getError(ack, 0) == FS_Read.E_NO_ERROR) {
                    logger.info(String.format(
                        "Write [%d, %d] succeeded on '%s'", start + offset,
                        start + offset + blockSize - 1, path));

                    short blockLen = FS_Write.getNumber(ack, 0);
                    offset += blockLen;

                    if (pl != null) {
                        pl.updateProgress((float)offset / (float)len);
                    }
                }
                else if (FS_Write.getError(ack, 0) == FS_Read.E_INVALID_ALIAS) {
                    // Don't count this as a try
                    logger.warn("Alias expired");
                    alias.set(-1); // Re-open on next loop
                }
                else {
                    logger.warn("Writeerror: " +
                                FS_Write.translateError(ack, 0));
                    tries++;
                }
            }
        }
        finally {
            if (closeAtEnd) {
                try {
                    closeFile(alias);
                }
                catch (Exception e) {
                    // Ignore it, just log the error
                    logger.warn("Error closing alias", e);
                }
            }
        }
    }

    /**
     * Create and write a file on a remote device.
     * 
     * @param data The data which will become the file's data.
     * @param path The path to the file.
     * @param allowDelete If true, and the file exists, the file will be deleted
     *            and re-created before writing. If false and the file exists,
     *            but the file is not large enough for the data being written,
     *            an {@link IOException} will be thrown. If false and the file
     *            exists and is large enough, <code>data</code> will be written
     *            to the beginning of the file and the remainder will be left
     *            alone.
     * @param pl If non-<code>null</code>, write progress will be sent to this
     *            object.
     * @throws IOException If there is a problem communicating with the device.
     * @throws TimeoutException If no response is received after the tries and
     *             timeouts.
     */
    public void uploadFile(byte[] data, String path, boolean allowDelete,
                           ProgressListener pl)
        throws IOException, TimeoutException
    {
        IntRef alias = new IntRef(-1);
        FS_Stat.Stat stat = null;

        // Check if the file exists
        try {
            stat = statFile(path, alias);
        }
        catch (IOException ioe) {
            // File doesn't exist, create it
            logger.info("Error (expected) stat-ing file '" + path, ioe);
        }

        if (stat != null && allowDelete) {
            logger.debug("File '" + path + "' exists, deleting");

            closeFile(alias);
            deleteFile(path);
            stat = null;
        }

        if (stat == null) {
            createFile(path, data.length, alias);
        }
        else {
            // Make sure it is large enough
            if (stat.size < data.length) {
                throw new IOException("Too much data for the existing file '" +
                                      path + "'");
            }
        }

        writeFile(data, path, 0, 0, data.length, alias, pl);
    }

    /**
     * Read a range of analog registers.
     * 
     * @param msg The message to use to read the registers.
     * @return The values of the registers that were read.
     * @throws IOException If there was a problem sending the message, or if the
     *             response was invalid.
     * @throws TimeoutException If no response was received after the tries and
     *             timeouts.
     */
    private short[] readAnalogs(UDR_GetA msg)
        throws IOException, TimeoutException
    {
        UDR_Acknowledge response = send(msg);

        return UDR_GetA.getVals(response);
    }

    /**
     * Read a range of discrete registers.
     * 
     * @param msg The message to use to read the registers.
     * @return The values of the registers that were read.
     * @throws IOException If there was a problem sending the message, or if the
     *             response was invalid.
     * @throws TimeoutException If no response was received after the tries and
     *             timeouts.
     */
    private boolean[] readDiscretes(UDR_GetD msg)
        throws IOException, TimeoutException
    {
        UDR_Acknowledge response = send(msg);

        return UDR_GetD.getVals(response);
    }

    /**
     * Read a range of discrete registers as bytes.
     * 
     * @param msg The message to use to read the registers.
     * @return The values of the registers that were read.
     * @throws IOException If there was a problem sending the message, or if the
     *             response was invalid.
     * @throws TimeoutException If no response was received after the tries and
     *             timeouts.
     */
    private byte[] readBytes(UDR_GetB msg)
        throws IOException, TimeoutException
    {
        UDR_Acknowledge response = send(msg);

        return UDR_GetB.getVals(response);
    }

    @Override
    public IOMessage send(IOMessage msg)
        throws IOException, TimeoutException, IllegalArgumentException
    {
        if (msg instanceof UDRMessage) {
            return send((UDRMessage)msg);
        }
        else {
            throw new IllegalArgumentException(String
                .format("%s is not an instance of UDRMessage", msg.getClass()
                    .getName()));
        }
    }

    /**
     * Send a message and wait for a response.
     * 
     * @param msg The message to send.
     * @return The response that was returned, if it was successful.
     * @throws IOException If there was an IO error sending the message, or if
     *             the response was an error message.
     * @throws TimeoutException If no response was received within the tries and
     *             timeouts.
     */
    public synchronized UDR_Acknowledge send(UDRMessage msg)
        throws IOException, TimeoutException
    {
        UDRMessage response = null;

        byte format = UDRMessage.F_FIXEDCRC;

        if (link.getTransportMethod() == TransportMethod.Serial) {
            // This transport method does not inherently checksum data
            format = UDRMessage.F_BINARY;
        }

        synchronized (l_sequence) {
            msg.setup(format, UDRMessage.STA_ANY, getStation(), session,
                sequence++);
        }

        for (int tries = 0; tries < getTries() && response == null; tries++) {
            if (tries > 0) {
                logger.info("Retry " + tries);
            }

            link.send(msg);

            long stopAt = System.currentTimeMillis() + timeout;

            synchronized (inQueue) {
                while ((response = checkQueue(msg)) == null &&
                       (timeout <= 0 || System.currentTimeMillis() < stopAt)) {

                    try {
                        inQueue.wait(stopAt - System.currentTimeMillis());
                    }
                    catch (InterruptedException ie) {
                        logger.warn("Interrupted waiting for a response", ie);
                        stopAt = System.currentTimeMillis(); // Stop now
                    }
                }
            }
        }

        if (response == null) {
            logger.error("Timed out");
            throw new TimeoutException("Timed out waiting for a response");
        }
        else if (!(response instanceof UDR_Acknowledge)) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger.error("Response is not a positive acknowledgment:\n" +
                             response.toString());
            }

            throw new IOException("Response is not a positive acknowledgment");
        }
        else if (!msg.verifyAck((UDR_Acknowledge)response)) {
            if (logger.isEnabledFor(Level.ERROR)) {
                logger
                    .error("Response acknowledgment does not match sent message:\n" +
                           response.toString());
            }
            throw new IOException(
                "Response acknowledgment does not match sent message");
        }

        UDR_Acknowledge ack = (UDR_Acknowledge)response;
        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Received a message %s\n", msg
                .toString(ack)));
        }

        return ack;
    }

    /**
     * Search the input queue for a message that looks like the response to the
     * given message. You <b>must</b> hold the monitor on the queue when calling
     * this.
     * 
     * @param msg The message whose response to search for.
     * @return The first matching message, or <code>null</code> if none could be
     *         found. The matching message will be removed from the queue.
     */
    private UDRMessage checkQueue(UDRMessage msg)
    {
        assert (Thread.holdsLock(inQueue));

        // Loop through all received messages, checking for the response
        for (Iterator<IOMessage> itInQueue = inQueue.iterator(); itInQueue
            .hasNext();) {

            IOMessage recv = itInQueue.next();
            itInQueue.remove();

            if (recv instanceof UDRMessage) {
                if (isResponse((UDRMessage)recv, msg)) {
                    return (UDRMessage)recv;
                }
            }
        }

        return null;
    }

    /**
     * Check whether a message looks like a response to another message.
     * 
     * @param resp The message that may be a response.
     * @param msg The message that <code>resp</code> is responding to.
     * @return True if the messages have equal header fields, false if not.
     */
    private boolean isResponse(UDRMessage resp, UDRMessage msg)
    {
        return (resp.getSequence() == msg.getSequence() &&
                resp.getSession() == msg.getSession() &&
                resp.getDestination() == msg.getSource() && resp.getSource() == msg
            .getDestination());
    }
}

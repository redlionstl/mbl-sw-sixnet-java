/*
 * MessageLib.java
 *
 * A superclass for the common functions in the messaging libraries.
 *
 * Jonathan Pearson
 * May 27, 2010
 *
 */

package com.sixnetio.Station;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * A superclass for the common functions in the messaging libraries.
 *
 * @author Jonathan Pearson
 */
public abstract class MessageLib
{
	static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private class Receiver
		extends Thread
	{
		public volatile boolean running;
		
		public Receiver()
		{
			super("MessageLib Receiver");
		}
		
		@Override
		public void run()
		{
			running = true;
			
			try {
				while (running) {
					try {
						IOMessage msg = link.receive(true);
						
						synchronized (inQueue) {
							inQueue.add(msg);
							inQueue.notify();
							
							logger.debug(String.format("Queue now contains %d items",
							                           inQueue.size()));
						}
					}
					catch (IOException ioe) {
						logger.warn("IOException waiting for a message through " +
						            link.getDescription() + ", killing receiver",
						            ioe);
						
						// Kill the receiver to prevent a runaway series of errors
						running = false;
					}
				}
			}
			finally {
				running = false;
			}
		}
		
		public void terminate()
		{
			running = false;
		}
	}
	
	protected final IOProtocolLink link;
	
	/**
	 * Receives messages read from the link, and allows for simple signaling to
	 * the procedure waiting for a message. It would be best to only allow one
	 * thread to send/receive messages at a time, and to clear this list out
	 * regularly.
	 */ 
	protected final List<IOMessage> inQueue;
	
	private Receiver receiver;
	
	protected long timeout = 3000;
	protected int tries = 3;
	
	/**
	 * Construct a new MessageLib.
	 * 
	 * @param link The link over which to communicate.
	 */
	protected MessageLib(IOProtocolLink link)
	{
		this.link = link;
		
		inQueue = new LinkedList<IOMessage>();
		
		receiver = new Receiver();
		receiver.setDaemon(true);
		receiver.start();
	}
	
	/**
	 * If the receiver is not running, start it. This will join on the receiver
	 * thread for one second, in case it was about to finish stopping. Note that
	 * this is still a race condition, although an unlikely one. It amounts to
	 * the halting problem to actually determine if the receiver was about to
	 * stop.
	 */
	public void restartReceiver()
	{
		try {
			receiver.join(1000);
		}
		catch (InterruptedException ie) {
			logger.warn("Interrupted waiting for receiver to stop running", ie);
		}
		
		if (receiver.isAlive()) {
			logger.warn("Receiver is running, cannot restart it");
		}
		else {
			logger.info("Restarting receiver thread");
			
			receiver = new Receiver();
			receiver.setDaemon(true);
			receiver.start();
		}
	}
	
	/** Get the link used for communications. */
	public IOProtocolLink getLink()
	{
		return link;
	}
	
	/** Get the timeout (in milliseconds) to wait for responses. */
	public long getTimeout()
	{
		return timeout;
	}
	
	/** Set the timeout (in milliseconds) to wait for responses. */
	public void setTimeout(long timeout)
	{
		this.timeout = timeout;
	}
	
	/** Get the number of tries before giving up on a communication. */
	public int getTries()
	{
		return tries;
	}
	
	/** Set the number of tries before giving up on a communication. */
	public void setTries(int tries)
	{
		this.tries = tries;
	}
	
	/**
	 * Write a single analog output register.
	 * 
	 * @param addr The register address.
	 * @param val The value to write.
	 * @throws IOException If there was a problem writing the register.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract void putAO(short addr, short val)
		throws IOException, TimeoutException;
	
	/**
	 * Write a range of analog output registers.
	 * 
	 * @param start The first register to write.
	 * @param vals The values to write.
	 * @throws IOException If there was a problem writing the registers.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract void putAO(short start, short[] vals)
		throws IOException, TimeoutException;
	
	/**
	 * Write a single discrete output register.
	 * 
	 * @param addr The register address.
	 * @param val The value to write.
	 * @throws IOException If there was a problem writing the register.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract void putDO(short addr, boolean val)
		throws IOException, TimeoutException;
	
	/**
	 * Write a range of discrete output registers..
	 * 
	 * @param start The first register to write.
	 * @param vals The values to write.
	 * @throws IOException If there was a problem writing the registers.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract void putDO(short start, boolean[] vals)
		throws IOException, TimeoutException;
	
	/**
	 * Read a range of analog input registers.
	 * 
	 * @param start The first register to read.
	 * @param count The number of registers to read.
	 * @return The values of the registers that were read (may not be the same
	 *   number as was requested).
	 * @throws IOException If there was a problem sending the message, or if the
	 *   response was invalid.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract short[] getAI(short start, short count)
		throws IOException, TimeoutException;
	
	/**
	 * Read a range of analog output registers.
	 * 
	 * @param start The first register to read.
	 * @param count The number of registers to read.
	 * @return The values of the registers that were read (may not be the same
	 *   number as was requested).
	 * @throws IOException If there was a problem sending the message, or if the
	 *   response was invalid.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract short[] getAO(short start, short count)
		throws IOException, TimeoutException;
	
	/**
	 * Read a range of discrete input registers.
	 * 
	 * @param start The first register to read.
	 * @param count The number of registers to read.
	 * @return The values of the registers that were read (may not be the same
	 *   number as was requested).
	 * @throws IOException If there was a problem sending the message, or if the
	 *   response was invalid.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract boolean[] getDI(short start, short count)
		throws IOException, TimeoutException;
	
	/**
	 * Read a range of discrete output registers.
	 * 
	 * @param start The first register to read.
	 * @param count The number of registers to read.
	 * @return The values of the registers that were read (may not be the same
	 *   number as was requested).
	 * @throws IOException If there was a problem sending the message, or if the
	 *   response was invalid.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract boolean[] getDO(short start, short count)
		throws IOException, TimeoutException;
	
	/**
	 * Read a range of discrete input registers as bytes.
	 * 
	 * @param start The first byte to read.
	 * @param count The number of bytes to read.
	 * @return The values of the bytes that were read.
	 * @throws IOException If there was a problem sending the message, or if the
	 *   response was invalid.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract byte[] getBI(short start, short count)
		throws IOException, TimeoutException;
	
	/**
	 * Read a range of discrete input registers as bytes.
	 * 
	 * @param start The first byte to read.
	 * @param count The number of bytes to read.
	 * @return The values of the bytes that were read.
	 * @throws IOException If there was a problem sending the message, or if the
	 *   response was invalid.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract byte[] getBO(short start, short count)
		throws IOException, TimeoutException;
	
	/**
	 * Turn off a set of discrete output registers (basically, perform a bitwise
	 * "AND NOT" operation). Depending on the underlying protocol, this
	 * operation may be implemented by multiple message exchanges.
	 * 
	 * @param start The starting register number.
	 * @param vals The values to clear. True elements will correspond to
	 *   registers being turned off, false elements will have no effect on the
	 *   corresponding registers.
	 * @throws IOException If there was a problem sending the message, or if the
	 *   response was invalid.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract void clearDO(short start, boolean[] vals)
		throws IOException, TimeoutException;
	
	/**
	 * Turn on a set of discrete output registers (basically, perform a bitwise
	 * OR operation). Depending on the underlying protocol, this operation may
	 * be implemented by multiple message exchanges.
	 * 
	 * @param start The starting register number.
	 * @param vals The values to clear. True elements will correspond to
	 *   registers being turned on, false elements will have no effect on the
	 *   corresponding registers.
	 * @throws IOException If there was a problem sending the message, or if the
	 *   response was invalid.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeout periods.
	 */
	public abstract void setDO(short start, boolean[] vals)
		throws IOException, TimeoutException;
	
	/**
	 * Send a message and wait for a response.
	 * 
	 * @param msg The message to send.
	 * @return The response that was returned, if it was successful.
	 * @throws IOException If there was an IO error sending the message, or if
	 *   the response was an error message.
	 * @throws TimeoutException If no response was received within the tries and
	 *   timeouts.
	 * @throws IllegalArgumentException If <code>msg</code> is not supported by
	 *   the link in use by this library.
	 */
	public abstract IOMessage send(IOMessage msg)
		throws IOException, TimeoutException, IllegalArgumentException;
}

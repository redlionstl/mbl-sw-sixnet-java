/*
 * BussedConnection.java
 *
 * Basically a bi-directional graph of wires connecting I/O points on stations.
 * An intelligent station will, instead of reading its own input/output value,
 * retrieve the value from this class. Methods are static to make it easy to
 * get values.
 *
 * Jonathan Pearson
 * April 19, 2007
 *
 */

package com.sixnetio.Station;

import java.util.Enumeration;
import java.util.Hashtable;

import org.jonp.util.Matrix;

public class BussedConnection {
	public static final int T_DI = 1,
							T_DO = 2,
							T_AI = 3,
							T_AO = 4;
	
	private static class IOPoint {
		public Station station;
		public int type;
		public short index;
		public short lastKnownValue;
		public boolean marked1 = false; // Used while traversing for outputs
		public boolean marked2 = false; // Used while traversing for inputs
		
		private static int sCounter = 0;
		public int counter = sCounter++;
		
		public IOPoint(Station station, int type, short index) {
			this.station = station;
			this.type = type;
			this.index = index;
			
			this.lastKnownValue = (short)(getValue() + 1); // To be certain that it counts as changed
		}
		
		@Override
        public boolean equals(Object obj) {
			if (obj instanceof IOPoint) {
				IOPoint l = (IOPoint)obj;
				
				return (l.station == station &&
						l.type == type &&
						l.index == index);
			} else {
				return false;
			}
		}
		
		@Override
        public int hashCode() {
			return toString().hashCode();
		}
		
		public boolean isOutput() {
			return (type == T_DO || type == T_AO);
		}
		
		public boolean isAnalog() {
			return (type == T_AI || type == T_AO);
		}
		
		public boolean hasChanged() {
			short value = getValue();
			return (value != lastKnownValue);
		}
		
		public void updateLastKnown() {
			lastKnownValue = getValue();
		}
		
		public short getValue() {
			if (type == T_DI) {
				if (station.getStraightDI(index)) {
					return -1;
				} else {
					return 0;
				}
			} else if (type == T_DO) {
				if (station.getStraightDO(index)) {
					return -1;
				} else {
					return 0;
				}
			} else if (type == T_AI) {
				return station.getStraightAI(index);
			} else if (type == T_AO) {
				return station.getStraightAO(index);
			} else {
				throw new IllegalStateException("IOPoint type is not an expected value: " + type);
			}
		}
		
		public void alertStation(short val) {
			if (type == T_DI) {
				station.changedDI(index, val != 0);
			} else if (type == T_DO) {
				station.changedDO(index, val != 0);
			} else if (type == T_AI) {
				station.changedAI(index, val);
			} else if (type == T_AO) {
				station.changedAO(index, val);
			} else {
				throw new IllegalStateException("IOPoint type is not an expected value: " + type);
			}
		}
		
		@Override
        public String toString() {
			return station.getID() + ":" + index + ":" + type;
		}
	}
	
	// Note:
	//  Technically, multiple outputs connected together in any way at all (including through
	//    an input) is a big no-no, as it can burn out the outputs (there would be a voltage
	//    applied to the output from the other output), but this class allows for it by
	//    choosing the maximum output and applying it to connected inputs
	
	
	// If there is a wire between the two IOPoints, the Boolean will be true
	// If there is no IOPoint, it will be null or false (probably null)
	// We only store one Boolean for every pair of IOPoints to save memory;
	//   the IOPoint with the lower hashCode goes first
	private static Matrix<IOPoint, IOPoint, Boolean> connections;
	
	// This carries the current values for each output (use IOPoint.getValue() for input)
	// Discrete will be -1 for true, 0 for false
	// If analog being interpreted as discrete, 0 is false and != 0 is true
	// That way, discretes act as an OR operation
	private static Hashtable<IOPoint, Short> values;
	
	static {
		connections = new Matrix<IOPoint, IOPoint, Boolean>();
		values = new Hashtable<IOPoint, Short>();
	}
	
	public static void addConnection(Station a, int typeA, short indexA, 
									 Station b, int typeB, short indexB) {
		
		IOPoint la = new IOPoint(a, typeA, indexA);
		IOPoint lb = new IOPoint(b, typeB, indexB);
		
		// Make sure we don't insert duplicates
		la = find(la);
		lb = find(lb);
		
		if (la.hashCode() < lb.hashCode()) {
			connections.put(la, lb, true); // Only store in one direction to save space
		} else {
			connections.put(lb, la, true);
		}
		
		values.put(la, la.getValue());
		values.put(lb, lb.getValue());
		
		compute(true); // Force a recompute
	}
	
	private static IOPoint find(IOPoint l) {
		Enumeration<IOPoint> level1 = connections.keys1();
		while (level1.hasMoreElements()) {
			IOPoint a = level1.nextElement();
			
			if (a.equals(l)) return a;
			
			Enumeration<IOPoint> level2 = connections.keys2(a);
			while (level2.hasMoreElements()) {
				IOPoint b = level2.nextElement();
				
				if (b.equals(l)) return b;
			}
		}
		
		return l;
	}
	
	public static boolean isConnected(Station a, int typeA, short indexA,
									  Station b, int typeB, short indexB) {
		
		IOPoint la = new IOPoint(a, typeA, indexA);
		IOPoint lb = new IOPoint(b, typeB, indexB);
		
		return isConnected(la, lb);
	}
	
	private static boolean isConnected(IOPoint a, IOPoint b) {
		// Since we only store in one direction to save space, we need to test both directions
		Boolean bln;
		if (a.hashCode() < b.hashCode()) {
			bln = connections.get(a, b);
		} else {
			bln = connections.get(b, a);
		}
		
		if (bln != null && bln.booleanValue()) return true;
		
		return false;
	}
	
	public static void removeConnection(Station a, int typeA, short indexA, 
										Station b, int typeB, short indexB) {
		
		IOPoint la = new IOPoint(a, typeA, indexA);
		IOPoint lb = new IOPoint(b, typeB, indexB);
		
		removeConnection(la, lb);
	}
	
	private static void removeConnection(IOPoint a, IOPoint b) {
		if (a.hashCode() < b.hashCode()) {
			connections.remove(a, b);
		} else {
			connections.remove(b, a);
		}
		
		values.remove(a);
		values.remove(b);
	}
	
	public static boolean valueExists(Station s, int type, short index) {
		IOPoint l = new IOPoint(s, type, index);
		
		return valueExists(l);
	}
	
	public static short getAnalogValue(Station s, int type, short index) {
		IOPoint l = new IOPoint(s, type, index);
		
		return getAnalogValue(l);
	}
	
	public static boolean getDiscreteValue(Station s, int type, short index) {
		IOPoint l = new IOPoint(s, type, index);
		
		return getDiscreteValue(l);
	}
	
	private static boolean valueExists(IOPoint l) {
		return values.containsKey(l);
	}
	
	private static short getAnalogValue(IOPoint l) {
		compute();
		
		if (valueExists(l)) {
			return values.get(l).shortValue();
		} else {
			return l.getValue();
		}
	}
	
	private static boolean getDiscreteValue(IOPoint l) {
		compute();
		
		if (valueExists(l)) {
			return (values.get(l).shortValue() != 0);
		} else {
			return (l.getValue() != 0);
		}
	}
	
	public static void compute() {
		compute(false);
	}
	
	public static void compute(boolean force) {
		// First, check whether we actually *need* to compute, because this can take a while
		boolean changed = force;
		
		if (!force) { // Not forced, figure out whether it's necessary
			Enumeration<IOPoint> level1 = connections.keys1();
			while (level1.hasMoreElements() && !changed) {
				IOPoint la = level1.nextElement();
				
				changed |= la.hasChanged();
				
				Enumeration<IOPoint> level2 = connections.keys2(la);
				while (level2.hasMoreElements() && !changed) {
					IOPoint lb = level2.nextElement();
					
					changed |= lb.hasChanged();
				}
			}
		}
		
		if (changed) {
			// We need to re-compute, an output has changed (outputs provide values to be bussed to inputs)
			// This is depth-first a graph traversal, marking as we go
			IOPoint start;
			while ((start = getUnmarked()) != null) {
				int value = trace(start); // Marks outputs
				
				apply(start, (short)(value & 0xFFFF)); // Marks inputs
			}
			
			clearAll();
		}
	}
	
	private static IOPoint getUnmarked() {
		// Search through the graph for a IOPoint without marked1 set
		Enumeration<IOPoint> level1 = connections.keys1();
		while (level1.hasMoreElements()) {
			IOPoint a = level1.nextElement();
			
			if (!a.marked1) return a;
			
			Enumeration<IOPoint> level2 = connections.keys2(a);
			while (level2.hasMoreElements()) {
				IOPoint b = level2.nextElement();
				
				if (!b.marked1) return b;
			}
		}
		
		return null;
	}
	
	private static void clearAll() {
		// Loop through the graph clearing all marked1 and marked2 values
		Enumeration<IOPoint> level1 = connections.keys1();
		while (level1.hasMoreElements()) {
			IOPoint a = level1.nextElement();
			
			a.marked1 = a.marked2 = false;
			a.updateLastKnown();
			
			Enumeration<IOPoint> level2 = connections.keys2(a);
			while (level2.hasMoreElements()) {
				IOPoint b = level2.nextElement();
				
				b.marked1 = b.marked2 = false;
				b.updateLastKnown();
			}
		}
	}
	
	private static int trace(IOPoint start) {
		if (start.marked1) return 0; // Already counted marked IOPoints
		start.marked1 = true;
		
		int value = 0;
		
		value = start.getValue() & 0xFFFF;
		
		// Grab all IOPoints where 'start' is the first of the pair
		Enumeration<IOPoint> level2 = connections.keys2(start);
		while (level2.hasMoreElements()) {
			IOPoint other = level2.nextElement();
			
			if (other.marked1) continue; // Quick skip so we don't have to enter the function again
			
			value = Math.max(trace(other), value);
		}
		
		// Now do it the other way around ('start' is the second of the pair)
		Enumeration<IOPoint> level1 = connections.keys1(start);
		while (level1.hasMoreElements()) {
			IOPoint other = level1.nextElement();
			
			if (other.marked1) continue; // Quick skip
			
			value = Math.max(trace(other), value);
		}
		
		return value;
	}
	
	// Set all unmarked inputs to value and store unmarked output values
	private static void apply(IOPoint start, short value) {
		if (start.marked2) return; // Already processed
		start.marked2 = true;
		
		values.put(start, value);
		start.alertStation(value);
		
		// Grab all IOPoints where 'start' is the first of the pair
		Enumeration<IOPoint> level2 = connections.keys2(start);
		while (level2.hasMoreElements()) {
			IOPoint other = level2.nextElement();
			
			if (other.marked2) continue; // Quick skip
			
			apply(other, value);
		}
		
		// Now do it the other way around ('start' is the second of the pair)
		Enumeration<IOPoint> level1 = connections.keys1(start);
		while (level1.hasMoreElements()) {
			IOPoint other = level1.nextElement();
			
			if (other.marked2) continue; // Quick skip
			
			apply(other, value);
		}
	}
}

/*
 * MessageListener.java
 *
 * An interface that must be implemented by any class which wants to
 * receive messages from a MessageDispatcher.
 *
 * Jonathan Pearson
 * March 23, 2007
 *
 */

package com.sixnetio.Station;

public interface MessageListener {
    // Get the ID for this listener
    public short getID();
    
    // A message was received, deal with it
    // It would be best if this simply added the message to a queue and handled
    //   it in another thread later, since as long as this blocks, no more
    //   messages will be received by the entire system
    public void handleMessage(MessageDispatcher.Message msg);
}

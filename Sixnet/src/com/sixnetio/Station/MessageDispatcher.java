/*
 * MessageDispatcher.java
 *
 * This is a multi-threaded class which will listen for new messages from
 * multiple sources, and dispatch them to multiple registered clients based on
 * station ID. It will also route messages, skipping over communications media
 * if the message is going from one registered client to another.
 *
 * Jonathan Pearson
 * March 23, 2007
 *
 */

package com.sixnetio.Station;

import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.UDR.UDRLink;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.util.Utils;

public class MessageDispatcher
	extends Thread
{
	static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private static MessageDispatcher theDispatcher;
	private static Object l_theDispatcher = new Object();
	
	/**
	 * Get the message dispatcher for the system.
	 * 
	 * @return The dispatcher, constructing it if necessary.
	 */
	public static MessageDispatcher getDispatcher()
	{
		synchronized (l_theDispatcher) {
			if (theDispatcher == null) {
				theDispatcher = new MessageDispatcher();
			}
			
			return theDispatcher;
		}
	}
	
	// Message class
	// This way we can see where a message came from, and we can send to a
	// specific location if necessary
	public static class Message
	{
		public UDRMessage msg;
		
		// In case something bad happens involving this message, so we can
		// provide an accurate exception
		public Throwable throwable;
		
		public UDRLink handler;
		public InetAddress addr;
		public int port;
		
		public Message(UDRMessage msg)
		{
			this.msg = msg;
			this.throwable = null;
			
			this.handler = null;
			this.addr = null;
			this.port = -1;
		}
		
		public Message(UDRMessage msg, UDRLink handler)
		{
			this.msg = msg;
			this.throwable = null;
			
			this.handler = handler;
			this.addr = null;
			this.port = -1;
		}
		
		public Message(UDRMessage msg, UDRLink handler, InetAddress addr,
		               int port)
		{
			this.msg = msg;
			this.throwable = null;
			
			this.handler = handler;
			this.addr = addr;
			this.port = port;
		}
	}
	
	Queue<Message> incoming;
	Queue<Message> outgoing;
	
	Hashtable<Short, Vector<MessageListener>> listeners;
	Vector<UDRLink> handlers;
	
	/**
	 * Construct a new MessageDispatcher.
	 * 
	 * @throws IOException If there was a problem constructing the local
	 *   handler.
	 */
	private MessageDispatcher()
	{
		super("MessageDispatcher");
		
		incoming = new LinkedList<Message>();
		outgoing = new LinkedList<Message>();
		listeners = new Hashtable<Short, Vector<MessageListener>>();
		handlers = new Vector<UDRLink>();
		
		new Sender();
		
		setDaemon(true);
		start();
	}
	
	@Override
	public void run()
	{
		while (true) {
			Message msg;
			
			synchronized (incoming) {
				while (incoming.isEmpty()) {
					try {
						incoming.wait();
					}
					catch (InterruptedException ie) {
						logger.warn("Interrupted while waiting", ie);
					}
				}
				
				msg = incoming.remove();
			}
			
			if (msg != null) {
				dispatch(msg);
			}
		}
	}
	
	// Handler functions
	public int handlerCount()
	{
		return handlers.size();
	}
	
	public void registerHandler(UDRLink handler)
	{
		new Receiver(handler);
		
		synchronized (handlers) {
			handlers.add(handler);
		}
	}
	
	public void unregisterHandler(UDRLink handler)
	{
		synchronized (handlers) {
			handlers.remove(handler);
		}
		
		handler.close();
	}
	
	// Listener functions
	public void registerListener(MessageListener listener)
	{
		synchronized (listeners) {
			Vector<MessageListener> vec = listeners.get(listener.getID());
			
			if (vec == null) {
				vec = new Vector<MessageListener>();
				listeners.put(listener.getID(), vec);
			}
			
			vec.add(listener);
		}
	}
	
	public void unregisterListener(MessageListener listener)
	{
		synchronized (listeners) {
			Vector<MessageListener> vec = listeners.get(listener.getID());
			
			if (vec == null) {
				return;
			}
			
			vec.remove(listener);
		}
	}
	
	public void send(Message msg)
	{
		synchronized (outgoing) {
			msg.throwable = new Throwable();
			outgoing.add(msg);
			
			outgoing.notify();
		}
	}
	
	public void send(UDRMessage msg)
	{
		synchronized (outgoing) {
			Message message = new Message(msg);
			message.throwable = new Throwable();
			
			outgoing.add(message);
			
			outgoing.notify();
		}
	}
	
	private void dispatch(Message msg)
	{
		short dest = msg.msg.getDestination();
		
		if (dest == UDRMessage.STA_ANY) {
			// Dispatch to all listeners
			synchronized (listeners) {
				Enumeration<Short> keys = listeners.keys();
				while (keys.hasMoreElements()) {
					Vector<MessageListener> vec = listeners.get(keys.nextElement());
					
					if (vec == null) {
						continue;
					}
					
					for (MessageListener listener : vec) {
						// Keep this thread from dying due to a misbehaving
						// listener
						try {
							listener.handleMessage(msg);
						}
						catch (Throwable th) {
							logger.debug(th);
						}
					}
				}
			}
		}
		else {
			synchronized (listeners) {
				Vector<MessageListener> vec = listeners.get(dest);
				
				if (vec == null) {
					return;
				}
				
				for (MessageListener listener : vec) {
					// Keep this thread from dying due to a misbehaving listener
					try {
						listener.handleMessage(msg);
					}
					catch (Throwable th) {
						logger.debug(th);
					}
				}
			}
		}
	}
	
	// Sender class (works with all registered handlers)
	private class Sender
		extends Thread
	{
		public Sender()
		{
			super("MessageDispatcherSender");
			
			setDaemon(true);
			start();
		}
		
		@Override
		public void run()
		{
			while (true) {
				Message msg;
				
				synchronized (outgoing) {
					while (outgoing.isEmpty()) {
						try {
							outgoing.wait();
						}
						catch (InterruptedException ie) {
							logger.warn("MessageDispatcher sender interrupted",
							            ie);
						}
					}
					
					msg = outgoing.remove();
				}
				
				if (msg != null) {
					short dest = msg.msg.getDestination();
					// Not being broadcast and matches a local station?
					if (dest != UDRMessage.STA_ANY &&
						listeners.containsKey(dest)) {
						
						synchronized (incoming) {
							// Just send it to the local station
							incoming.add(msg);
							incoming.notify();
						}
					}
					else {
						synchronized (handlers) {
							try {
								if (msg.handler != null) {
									if (msg.addr != null && msg.port != -1) {
										msg.handler.sendTo(msg.msg, msg.addr,
										                   msg.port);
									}
									else {
										msg.handler.send(msg.msg);
									}
								}
								else {
									for (UDRLink handler : handlers) {
										if (msg.addr != null &&
											msg.port != -1) {
											
											handler.sendTo(msg.msg, msg.addr,
											               msg.port);
										}
										else {
											handler.send(msg.msg);
										}
									}
								}
							}
							catch (Exception e) {
								e.initCause(msg.throwable);
								
								logger.warn("Error sending message", e);
							}
						}
					}
				}
			}
		}
	}
	
	// Receiver class (works with an individual handler)
	private class Receiver
		extends Thread
	{
		private UDRLink myHandler;
		
		public Receiver(UDRLink handler)
		{
			super("MessageDispatcherReceiver");
			
			myHandler = handler;
			
			setDaemon(true);
			start();
		}
		
		@Override
		public void run()
		{
			while ( ! myHandler.isClosed()) {
				try {
					Message msg;
					
					if (myHandler.overUDP()) {
						InetAddress[] from = new InetAddress[1];
						int[] port = new int[1];
						
						UDRMessage umsg = myHandler.receive(from, port);
						
						msg = new Message(umsg, myHandler, from[0], port[0]);
					}
					else {
						UDRMessage umsg = myHandler.receive();
						
						msg = new Message(umsg, myHandler);
					}
					
					synchronized (incoming) {
						incoming.add(msg);
						
						incoming.notify();
					}
				}
				catch (Exception e) {
					logger.warn("Error receiving message", e);
				}
			}
			
			logger.info("Handler closed, receiver exiting");
		}
	}
}

/*
 * UDRLib.java
 *
 * A library of UDR function wrappers to make normal coding easier. Note that
 *   this is not the usual static library, since that would not be thread-safe.
 *
 * Jonathan Pearson
 * April 7, 2008
 *
 */

package com.sixnetio.Station;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeoutException;

import com.sixnetio.GUI.ProgressListener;
import com.sixnetio.UDR.*;
import com.sixnetio.UDR.Filesys.*;
import com.sixnetio.UDR.Gateway.*;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

public class UDRLib implements MessageListener {
	/**
	 * Pass this to {@link #createFile(short, String, int)} as the size and a
	 * directory as the target to format a partition on an EtherTRAK2.
	 */
	public static final int SZ_FORMAT = 0x12345678;
	
	/**
	 * The maximum number of bytes we can fit into a file write message.
	 */
	public static final int SZ_BLOCK = 237;
	
	// Private data members
	private long messageTimeout = 3000; // Milliseconds before re-sending a message
	private int tries = 3; // Number of times to re-send a message before giving up
	private int fileDelay = 50; // Milliseconds to delay after a file operation fails
	private byte format = UDRMessage.F_BINARY;
	
	private Map<Byte, UDRMessage> inFlight; // Sequence Number -> Message
	private Vector<MessageDispatcher.Message> received;
	private short stationID;
	private byte session;
	private byte sequence = 0; // Increment with each message sent
	
	// Constructor/destructor
	/**
	 * Construct a new UDRLib library.
	 * @param stationID The station ID that this library will listen on.
	 * @param session The session number that this library will send/listen on.
	 * @throws IOException If there is an error registering this class as a listener with the MessageDispatcher.
	 */
	public UDRLib(short stationID, byte session) throws IOException {
		this.stationID = stationID;
		this.session = session;
		
		received = new Vector<MessageDispatcher.Message>();
		inFlight = new Hashtable<Byte, UDRMessage>();
		
		MessageDispatcher.getDispatcher().registerListener(this);
	}
	
	/**
	 * Unregisters this message listener from the message dispatcher.
	 */
	@Override
    protected void finalize() {
		MessageDispatcher.getDispatcher().unregisterListener(this);
	}
	
	/**
	 * Get the station ID used by this message listener.
	 */
	@Override
	public short getID() {
		return stationID;
	}
	
	/**
	 * Handle a UDR message from the message dispatcher. This is public only as
	 * an implementation detail.
	 */
	public void handleMessage(MessageDispatcher.Message msg) {
		// Only interested in messages from the same session that we are using
		// Also only interested in ACKs and NAKs
		// Since it's possible that the user is sending to the broadcast station ID,
		//   sent messages could come back through here
		if (msg.msg.getSession() == session) {
			synchronized (received) {
				synchronized (inFlight) {
    				if (inFlight.containsKey(msg.msg.getSequence())) {
    					UDRMessage original = inFlight.remove(msg.msg.getSequence());
    					
    					if (msg.msg instanceof UDR_Acknowledge) {
    						Utils.debug("Received:\n" + original.toString((UDR_Acknowledge)msg.msg, 2));
    					} else {
    						Utils.debug("Received response to " + original.getCommandString() + ":\n" + msg.msg.toString(2));
    					}
    					
    					received.add(msg);
    					received.notifyAll();
    				} else {
    					Utils.debug("Received unsolicited message, discarding:\n" + msg.msg.toString(2));
    				}
				}
			}
		} else {
			// Different session number? Completely unrelated, discard it silently
		}
	}
	
	
	// ***** Accessors *****
	/**
	 * Set the timeout (milliseconds) before resending a message.
	 */
	public void setMessageTimeout(long val) {
		messageTimeout = val;
	}
	
	/** Get the message timeout. */
	public long getMessageTimeout() {
		return messageTimeout;
	}
	
	/**
	 * Set the number of times a message will be re-sent before giving up.
	 */
	public void setTries(int val) {
		tries = val;
	}
	
	/** Get the number of tries. */
	public int getTries() {
		return tries;
	}
	
	/**
	 * Set the file operation delay (in milliseconds). This is used when a
	 * station fails a file operation, to add an extra delay before retrying.
	 * The problem is that some stations buffer file write requests, and then
	 * start refusing to fulfill new ones when the buffer fills up. This allows
	 * the station to clear its buffers before continuing.
	 */
	public void setFileDelay(int val) {
		fileDelay = val;
	}
	
	/** Get the file operation delay. */
	public int getFileDelay() {
		return fileDelay;
	}
	
	/**
	 * Set the message format byte (see {@link UDRMessage#F_BINARY} and
	 * similar). This will be used for all future outgoing messages.
	 */
	public void setFormat(byte val) {
		format = val;
	}
	
	/** Get the message format byte used for outgoing messages. */
	public byte getFormat() {
		return format;
	}
	
	
	// ***** Standard I/O functions *****
	// NIO
	/**
	 * Get the number of I/O points of the given type that exist in the station.
	 * 
	 * @param to The station to send to.
	 * @param type The data type (one of the UDRMessage.T_* values).
	 * @return The number of I/O points of that type.
	 */
	public short nio(short to, byte type) throws IOException, TimeoutException {
		UDR_Acknowledge ack = guaranteedSend(to, new UDR_NumberIO(type));
		
		switch (type) {
			// Analog types need to use getAnalogs()
			case UDRMessage.T_C_AIN:
			case UDRMessage.T_C_AOUT:
			case UDRMessage.T_D_AIN:
			case UDRMessage.T_D_AOUT:
			case UDRMessage.T_P_AIN:
			case UDRMessage.T_P_AOUT:
				return UDR_NumberIO.getAnalogs(ack);
				
			// Everything else (discretes especially) can use getDiscretes()
			default:
				return UDR_NumberIO.getDiscretes(ack);
		}
	}
	
	// Put messages
	// PutA
	/**
	 * Set analog output values.
	 * @param to The station to send to.
	 * @param start The offset of the first analog value to set.
	 * @param num The number of analog values to set.
	 * @param data The analog values to send to the station. There must be at least num values,
	 *   otherwise you will get an ArrayIndexOutOfBoundsException.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void putA(short to, short start, short num, short[] data) throws IOException, TimeoutException {
		putA(to, UDRMessage.T_D_AOUT, start, num, data);
	}
	
	/**
	 * Set analog values.
	 * @param to The station to send to.
	 * @param type The type of registers to set.
	 * @param start The offset of the first analog value to set.
	 * @param num The number of analog values to set.
	 * @param data The analog values to send to the station. There must be at least num values,
	 *   otherwise you will get an ArrayIndexOutOfBoundsException.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void putA(short to, byte type, short start, short num, short[] data) throws IOException, TimeoutException {
		guaranteedSend(to, new UDR_PutA(type, start, num, data));
	}
	
	// PutB
	/**
	 * Set discrete output values as bytes.
	 * @param to The station to send to.
	 * @param start The offset of the first byte to set.
	 * @param num The number of bytes to set.
	 * @param data The bytes to send to the station. There must be at least num values,
	 *   otherwise you will get an ArrayIndexOutOfBoundsException.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void putB(short to, short start, short num, byte[] data) throws IOException, TimeoutException {
		putB(to, UDRMessage.T_D_DOUT, start, num, data);
	}
	
	/**
	 * Set discrete values as bytes.
	 * @param to The station to send to.
	 * @param type The type of registers to set.
	 * @param start The offset of the first byte to set.
	 * @param num The number of bytes to set.
	 * @param data The bytes to send to the station. There must be at least num values,
	 *   otherwise you will get an ArrayIndexOutOfBoundsException.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void putB(short to, byte type, short start, short num, byte[] data) throws IOException, TimeoutException {
		guaranteedSend(to, new UDR_PutB(type, start, num, data));
	}
	
	// PutD
	/**
	 * Set discrete output values as bits.
	 * @param to The station to send to.
	 * @param start The offset of the first bit to set.
	 * @param num The number of bits to set.
	 * @param data The bits to send to the station. There must be at least num values,
	 *   otherwise you will get an ArrayIndexOutOfBoundsException.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void putD(short to, short start, short num, boolean[] data) throws IOException, TimeoutException {
		putD(to, UDRMessage.T_D_DOUT, start, num, data);
	}
	
	/**
	 * Set discrete values as bits.
	 * @param to The station to send to.
	 * @param type The type of registers to set.
	 * @param start The offset of the first bit to set.
	 * @param num The number of bits to set.
	 * @param data The bits to send to the station. There must be at least num values,
	 *   otherwise you will get an ArrayIndexOutOfBoundsException.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void putD(short to, byte type, short start, short num, boolean[] data) throws IOException, TimeoutException {
		guaranteedSend(to, new UDR_PutD(type, start, num, data));
	}
	
	// SetD
	/**
	 * Turn on discrete output values as bits.
	 * @param to The station to send to.
	 * @param start The offset of the first bit to set.
	 * @param num The number of bits to set.
	 * @param data The bits to send to the station. There must be at least num values,
	 *   otherwise you will get an ArrayIndexOutOfBoundsException.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void setD(short to, short start, short num, boolean[] data) throws IOException, TimeoutException {
		setD(to, UDRMessage.T_D_DOUT, start, num, data);
	}
	
	/**
	 * Turn on discrete values as bits.
	 * @param to The station to send to.
	 * @param type The type of registers to turn on.
	 * @param start The offset of the first bit to set.
	 * @param num The number of bits to set.
	 * @param data The bits to send to the station. There must be at least num values,
	 *   otherwise you will get an ArrayIndexOutOfBoundsException.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void setD(short to, byte type, short start, short num, boolean[] data) throws IOException, TimeoutException {
		guaranteedSend(to, new UDR_SetD(type, start, num, data));
	}
	
	// ClrD
	/**
	 * Clear discrete output values as bits.
	 * @param to The station to send to.
	 * @param start The offset of the first bit to clear.
	 * @param num The number of bits to clear.
	 * @param data The bits to send to the station. There must be at least num values,
	 *   otherwise you will get an ArrayIndexOutOfBoundsException.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void clrD(short to, short start, short num, boolean[] data) throws IOException, TimeoutException {
		clrD(to, UDRMessage.T_D_DOUT, start, num, data);
	}
	
	/**
	 * Clear discrete values as bits.
	 * @param to The station to send to.
	 * @param type The type of registers to clear.
	 * @param start The offset of the first bit to clear.
	 * @param num The number of bits to clear.
	 * @param data The bits to send to the station. There must be at least num values,
	 *   otherwise you will get an ArrayIndexOutOfBoundsException.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void clrD(short to, byte type, short start, short num, boolean[] data) throws IOException, TimeoutException {
		guaranteedSend(to, new UDR_ClearD(type, start, num, data));
	}
	
	// Get messages
	// GetA
	/**
	 * Read analog input values.
	 * @param to The station to send to.
	 * @param start The offset of the first analog value to read.
	 * @param num The number of analog values to read.
	 * @return The analog values read from the station.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public short[] getA(short to, short start, short num) throws IOException, TimeoutException {
		return getA(to, UDRMessage.T_D_AIN, start, num);
	}
	
	/**
	 * Read analog values.
	 * @param to The station to send to.
	 * @param type The type of registers to read.
	 * @param start The offset of the first analog value to read.
	 * @param num The number of analog values to read.
	 * @return The analog values read from the station.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public short[] getA(short to, byte type, short start, short num) throws IOException, TimeoutException {
		// Sometimes the station will respond improperly (wrong number of actual
		//   registers in the message, even though the 'number' field is correct)
		// Keep trying until it sends the right one
		ArrayIndexOutOfBoundsException theException = null;
		
		for (int thisTry = 0; thisTry < getTries(); thisTry++) {
			UDR_Acknowledge ack = guaranteedSend(to, new UDR_GetA(type, start, num));
			short[] data = new short[UDR_GetA.getNum(ack)];
			
			try {
				for (int i = 0; i < data.length; i++) {
					data[i] = UDR_GetA.getVal(ack, i);
				}
				
				return data;
			} catch (ArrayIndexOutOfBoundsException ex) {
				Utils.debug(ex);
				theException = ex;
			}
		}
		
		throw theException;
	}
	
	// GetB
	/**
	 * Read discrete input values as bytes.
	 * @param to The station to send to.
	 * @param start The offset of the first byte to read.
	 * @param num The number of bytes to read.
	 * @return The bytes read from the station.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public byte[] getB(short to, short start, short num) throws IOException, TimeoutException {
		return getB(to, UDRMessage.T_D_DIN, start, num);
	}
	
	/**
	 * Read discrete values as bytes.
	 * @param to The station to send to.
	 * @param type The type of registers to read.
	 * @param start The offset of the first byte to read.
	 * @param num The number of bytes to read.
	 * @return The bytes read from the station.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public byte[] getB(short to, byte type, short start, short num) throws IOException, TimeoutException {
		// Sometimes the station will respond improperly (wrong number of actual
		//   registers in the message, even though the 'number' field is correct)
		// Keep trying until it sends the right one
		ArrayIndexOutOfBoundsException theException = null;
		
		for (int thisTry = 0; thisTry < getTries(); thisTry++) {
			UDR_Acknowledge ack = guaranteedSend(to, new UDR_GetB(type, start, num));
			byte[] data = new byte[UDR_GetB.getNum(ack)];
			
			try {
				for (int i = 0; i < data.length; i++) {
					data[i] = UDR_GetB.getVal(ack, i);
				}
				
				return data;
			} catch (ArrayIndexOutOfBoundsException ex) {
				Utils.debug(ex);
				theException = ex;
			}
		}
		
		throw theException;
	}
	
	// GetD
	/**
	 * Read discrete input values as bits.
	 * @param to The station to send to.
	 * @param start The offset of the first discrete value to read.
	 * @param num The number of discrete values to read.
	 * @return The discrete values read from the station.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public boolean[] getD(short to, short start, short num) throws IOException, TimeoutException {
		return getD(to, UDRMessage.T_D_DIN, start, num);
	}
	
	/**
	 * Read discrete values as bits.
	 * @param to The station to send to.
	 * @param type The type of registers to read.
	 * @param start The offset of the first discrete value to read.
	 * @param num The number of discrete values to read.
	 * @return The discrete values read from the station.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public boolean[] getD(short to, byte type, short start, short num) throws IOException, TimeoutException {
		// Sometimes the station will respond improperly (wrong number of actual
		//   registers in the message, even though the 'number' field is correct)
		// Keep trying until it sends the right one
		ArrayIndexOutOfBoundsException theException = null;
		
		for (int thisTry = 0; thisTry < getTries(); thisTry++) {
			UDR_Acknowledge ack = guaranteedSend(to, new UDR_GetD(type, start, num));
			boolean[] data = new boolean[UDR_GetD.getNum(ack)];
			
			try {
				for (int i = 0; i < data.length; i++) {
					data[i] = UDR_GetD.getVal(ack, i);
				}
				
				return data;
			} catch (ArrayIndexOutOfBoundsException ex) {
				Utils.debug(ex);
				theException = ex;
			}
		}
		
		throw theException;
	}
	
	// Other messages and helper functions
	/**
	 * Send a no-op message, which will cause no changes but can be used to verify
	 *   connectivity with a station.
	 * @param to The station to send to.
	 * @throws IOException If the station responds with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void nop(short to) throws IOException, TimeoutException {
		guaranteedSend(to, new UDR_NoOp());
	}
	
	/**
	 * Block until the given station starts responding or 60 seconds pass. Uses NOP messages.
	 * @param stationID The station to wait for.
	 * @throws IOException If the station responds badly.
	 * @throws TimeoutException If the station does not respond within 60 seconds.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void waitForResponse(short stationID) throws IOException, TimeoutException {
		waitForResponse(stationID, 60000);
	}
	
	/**
	 * Block until the given station starts responding or the given amount of time passes. Uses NOP messages.
	 * @param stationID The station to wait for.
	 * @param timeout The amount of time to wait, in milliseconds.
	 * @throws IOException If the station responds badly.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void waitForResponse(short stationID, long timeout) throws IOException, TimeoutException {
		long startTime = System.currentTimeMillis();
		
		while (startTime + timeout > System.currentTimeMillis()) {
			try {
				nop(stationID);
				
				return;
			} catch (TimeoutException e) { }
		}
		
		// If we get here, the loop timed out
		throw new TimeoutException("Unable to contact the device after " + (timeout / 1000) + " seconds");
	}
	
	/**
	 * Set the clock on a station.
	 * @param to The station whose clock should be set.
	 * @param time The time (in seconds since the epoch) that the clock should be set to.
	 * @throws IOException If there is an error communicating with the station.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void setClock(short to, int time) throws IOException, TimeoutException {
		UDR_Acknowledge ack = guaranteedSend(to, new UDR_SetClock(time));
		if (UDR_SetClock.getStatus(ack) == UDR_SetClock.S_NOCLOCK) {
			throw new IOException("Station did not set the clock");
		}
	}
	
	/**
	 * Get the time read from a station clock.
	 * @param to The station whose clock to read.
	 * @return The time (in seconds since the epoch) read from the clock.
	 * @throws IOException If there is an error communicating with the station.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public int getClock(short to) throws IOException, TimeoutException {
		UDR_Acknowledge ack = guaranteedSend(to, new UDR_GetClock());
		return UDR_GetClock.getTime(ack);
	}
	
	
	// ***** Filesys functions *****
	/**
	 * Create a file with read/write permissions. Tries getTries() times.
	 * @param to The station ID to send to.
	 * @param path The path to the file.
	 * @param size The size of the file.
	 * @return The alias of the file.
	 * @throws IOException If there is an IO error.
	 * @throws TimeoutException If no response is received from the station.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public int createFile(short to, String path, int size) throws IOException, TimeoutException {
		int failureCounter = 0;
		
		while (true) {
			UDRMessage response = send(to, new FS_Create(FS_Create.O_RDWR, size, path));
			
			if (response instanceof UDR_Acknowledge) {
				if (FS_Create.getError((UDR_Acknowledge)response) == FS_Create.E_NO_ERROR) {
					return FS_Create.getAlias((UDR_Acknowledge)response);
				} else {
					failureCounter++;
					
					if (failureCounter > getTries()) throw new IOException("Bad response from station: " + FS_Create.translateError((UDR_Acknowledge)response));
				}
			} else {
				failureCounter++;
				
				if (failureCounter > getTries()) throw new IOException("Bad response from station: " + response.getClass());
			}
			
			Utils.sleep(100); // Only gets here if it failed
		}
	}
	
	/**
	 * Get a file alias. This will always return a read/write alias.
	 * @param to The station ID to send to.
	 * @param path The path to the file.
	 * @return The alias for the file.
	 * @throws IOException If there is an IO error communicating with the station.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public int getAlias(short to, String path) throws IOException, TimeoutException {
		int failureCounter = 0;
		
		while (true) {
			UDRMessage response = send(to, new FS_GetAlias(FS_GetAlias.O_RDWR, path));
			
			if (response instanceof UDR_Acknowledge) {
				if (FS_GetAlias.getError((UDR_Acknowledge)response) == FS_GetAlias.E_NO_ERROR) {
					return FS_GetAlias.getAlias((UDR_Acknowledge)response);
				} else {
					failureCounter++;
					
					if (failureCounter > getTries()) throw new IOException("Bad response from station when trying to open " + path + ": " + FS_GetAlias.translateError((UDR_Acknowledge)response));
					
					Utils.debug("Station responded badly to GetAlias, giving it a chance to relax");
					Utils.sleep(100);
				}
			} else {
				failureCounter++;
				
				if (failureCounter > getTries()) throw new IOException("Bad response from station when trying to open " + path + ": " + response.getClass());
				
				Utils.debug("Station responded badly to GetAlias, giving it a chance to relax");
				Utils.sleep(100);
			}
		}
	}
	
	/**
	 * Close a file alias.
	 * @param to The station ID to send to.
	 * @param alias The alias to close.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void closeAlias(short to, int alias) throws IOException, TimeoutException {
		send(to, new FS_CloseAlias(alias));
	}
	
	/**
	 * Read a file.
	 * @param to The station ID to send to.
	 * @param path The path to the file, in case we need to refresh an expired alias.
	 * @param start The starting offset within the file to read.
	 * @param length The number of bytes to read. If -1, will determine the file size
	 *   with a Stat message and read the whole thing from <b>start</b> to the end.
	 * @param alias If you pass an int array whose size is at least 1, the value in
	 *   index 0 will be used as the alias for the file. If it expires and a new alias
	 *   is obtained, it will be stored in index 0 so you will have it upon completion.
	 * @return The bytes read from the file.
	 * @throws IOException If there is an IO error while reading.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public byte[] readFile(short to, String path, int start, int length, int[] alias) throws IOException, TimeoutException {
		boolean closeAtEnd = false;
		
		if (alias == null || alias.length < 1) {
			alias = new int[1];
			alias[0] = -1;
			closeAtEnd = true;
		}
		
		if (length == -1) {
			int[] len = new int[1];
			statFile(to, path, len, null, null, alias);
			length = len[0] - start;
		}
		
		int failureCounter = 0;
		int offset = 0;
		byte[] data = new byte[length];
		
		Utils.debug("Trying to read " + length + " bytes from " + path + ", starting at " + start);
		
		while (offset < length) {
			if (alias[0] == -1) alias[0] = getAlias(to, path);
			
			int blockSize = Math.min(SZ_BLOCK, length - offset);
			UDRMessage response = send(to, new FS_Read(alias[0], start + offset, (short)blockSize));
			
			if (response instanceof UDR_Acknowledge) {
				if (FS_Read.getError((UDR_Acknowledge)response, 0) == FS_Read.E_INVALID_ALIAS) {
					// The alias expired, refresh it and try again
					alias[0] = -1;
					continue;
				}
				
				if (FS_Read.getError((UDR_Acknowledge)response, 0) != FS_Read.E_NO_ERROR) {
					failureCounter++;
					
					if (failureCounter > getTries()) throw new IOException("Bad response from station: " + FS_Read.translateError(FS_Read.getError((UDR_Acknowledge)response, 0)));
					continue;
				}
			} else {
				failureCounter++;
				
				if (failureCounter > getTries()) throw new IOException("Bad response from station: " + response.getClass());
				
				// Give it a moment, maybe it's busy
				Utils.debug("Station NAKed a Read, giving it a moment to relax");
				Utils.sleep(fileDelay);
				
				continue;
			}
			
			byte[] block = FS_Read.getData((UDR_Acknowledge)response, 0);
			short number = FS_Read.getNumber((UDR_Acknowledge)response, 0);
			System.arraycopy(block, 0, data, offset, number);
			
			offset += number;
		}
		
		if (closeAtEnd) closeAlias(to, alias[0]);
		
		return data;
	}
	
	/**
	 * Write data to a file on the given station.
	 * @param to The station ID to send to.
	 * @param path The path to the file. This will be used to re-open the file if its alias expires.
	 * @param data The data to write to the file. All of it will be written.
	 * @param start The offset within the file at which to start writing.
	 * @param alias If you pass an int array whose size is at least 1, the value in
	 *   index 0 will be used as the alias for the file. If it expires and a new alias
	 *   is obtained, it will be stored in index 0 so you will have it upon completion.
	 * @throws IOException If a communications error occurs.
	 * @throws TimeoutException If no response is received from the station within getMessageTimeout() milliseconds.
	 * @throws InterruptedOperationException If the thread is interrupted while working.
	 */
	public void writeFile(short to, String path, byte[] data, int start, int[] alias) throws IOException, TimeoutException {
		writeFile(to, path, data, 0, data.length, start, alias);
	}
	
	/**
	 * Write data to a file on the given station.
	 * @param to The station ID to send to.
	 * @param path The path to the file. This will be used to re-open the file if its alias expires.
	 * @param data The data to write to the file. All of it will be written.
	 * @param start The offset within the file at which to start writing.
	 * @param alias If you pass an int array whose size is at least 1, the value in
	 *   index 0 will be used as the alias for the file. If it expires and a new alias
	 *   is obtained, it will be stored in index 0 so you will have it upon completion.
	 * @param progressListener If not null, will be notified of the progress writing the file.
	 * @throws IOException If a communications error occurs.
	 * @throws TimeoutException If no response is received from the station within getMessageTimeout() milliseconds.
	 * @throws InterruptedOperationException If the thread is interrupted while working.
	 */
	public void writeFile(short to, String path, byte[] data, int start, int[] alias, ProgressListener progressListener) throws IOException, TimeoutException {
		writeFile(to, path, data, 0, data.length, start, alias, progressListener);
	}
	
	/**
	 * Write data to a file on the given station.
	 * @param to The station ID to send to.
	 * @param path The path to the file. This will be used to re-open the file if its alias expires.
	 * @param data The data to write to the file. All of it will be written.
	 * @param off The offset within 'data' to start at.
	 * @param len The number of bytes from 'data' to write.
	 * @param start The offset within the file at which to start writing.
	 * @param alias If you pass an int array whose size is at least 1, the value in
	 *   index 0 will be used as the alias for the file. If it expires and a new alias
	 *   is obtained, it will be stored in index 0 so you will have it upon completion.
	 * @throws IOException If a communications error occurs.
	 * @throws TimeoutException If no response is received from the station within getMessageTimeout() milliseconds.
	 * @throws InterruptedOperationException If the thread is interrupted while working.
	 */
	public void writeFile(short to, String path, byte[] data, int off, int len, int start, int[] alias) throws IOException, TimeoutException {
		writeFile(to, path, data, off, len, start, alias, null);
	}
	
	/**
	 * Write data to a file on the given station.
	 * @param to The station ID to send to.
	 * @param path The path to the file. This will be used to re-open the file if its alias expires.
	 * @param data The data to write to the file. All of it will be written.
	 * @param off The offset within 'data' to start at.
	 * @param len The number of bytes from 'data' to write.
	 * @param start The offset within the file at which to start writing.
	 * @param alias If you pass an int array whose size is at least 1, the value in
	 *   index 0 will be used as the alias for the file. If it expires and a new alias
	 *   is obtained, it will be stored in index 0 so you will have it upon completion.
	 * @param progressListener If not null, will be notified of the progress writing the file.
	 * @throws IOException If a communications error occurs.
	 * @throws TimeoutException If no response is received from the station within getMessageTimeout() milliseconds.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting working.
	 */
	public void writeFile(short to, String path, byte[] data, int off, int len, int start, int[] alias, ProgressListener progressListener) throws IOException, TimeoutException {
		boolean closeAtEnd = false;
		
		if (alias == null || alias.length < 1) {
			alias = new int[1];
			alias[0] = -1;
			closeAtEnd = true;
		}
		
		int failureCounter = 0;
		int position = 0;
		
		Utils.debug("Writing " + len + " bytes to '" + path + "'");
		
		while (position < len) {
			if (alias[0] == -1) alias[0] = getAlias(to, path);
			
			byte[] block;
			
			if (len - position > SZ_BLOCK) {
				block = new byte[SZ_BLOCK];
			} else {
				block = new byte[len - position];
			}
			
			System.arraycopy(data, off + position, block, 0, block.length);
			
			Utils.debug("Writing to station:");
			Utils.debug("  ", block);
			
			UDRMessage response = send(to, new FS_Write(alias[0], start + position, (short)block.length, block));
			
			if (response instanceof UDR_Acknowledge) {
				if (FS_Write.getError((UDR_Acknowledge)response, 0) == FS_Write.E_INVALID_ALIAS) {
					// The alias somehow expired already, so refresh it and try again
					alias[0] = -1;
					continue;
				}
				
				if (FS_Write.getError((UDR_Acknowledge)response, 0) != FS_Write.E_NO_ERROR &&
					FS_Write.getError((UDR_Acknowledge)response, 0) != FS_Write.E_FLASH_BUSY) { // Ignore flash busy errors too
					failureCounter++;
					
					if (failureCounter > getTries()) throw new IOException("Bad response from station: " + FS_Write.translateError((UDR_Acknowledge)response, 0));
					continue;
				}
			} else {
				failureCounter++;
				
				if (failureCounter > getTries()) throw new IOException("Bad response from station: " + response.getClass());
				continue;
			}
			
			short bytesWritten = FS_Write.getNumber((UDR_Acknowledge)response, 0);
			if (bytesWritten < block.length) {
				failureCounter++;
				if (failureCounter > getTries()) throw new IOException("Too many delay requests in response to writing data");
				
				Utils.debug("Station wrote fewer bytes than expected, delaying to allow it to flush its buffer");
				Utils.sleep(fileDelay * failureCounter);
			} else {
				failureCounter = 0;
			}
			
			position += bytesWritten;
			
			if (progressListener != null) progressListener.updateProgress((float)position / (float)len);
		}
		
		if (closeAtEnd && alias[0] != -1) closeAlias(to, alias[0]);
	}
	
	/**
	 * Delete a file in a station.
	 * @param to The station ID to send to.
	 * @param path The path to the file to delete.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void deleteFile(short to, String path) throws IOException, TimeoutException {
		UDRMessage response = send(to, new FS_Delete(FS_Delete.O_NONE, path));
		
		if (response instanceof UDR_Acknowledge) {
			if (FS_Delete.getError((UDR_Acknowledge)response) != FS_Delete.E_NO_ERROR) {
				throw new IOException("Bad response from station: " + FS_Delete.translateError((UDR_Acknowledge)response));
			}
		} else {
			throw new IOException("Bad response from station: " + response.getClass());
		}
	}
	
	/**
	 * Stat a file in a station.
	 * @param to The station containing the file.
	 * @param path The path within the station of the file to stat.
	 * @param out_size If provided and long enough, [0] will have the size of the file upon return.
	 * @param out_timestamp If provided and long enough, [0] will have the timestamp of the file upon return.
	 * @param out_mode If provided and long enough, [0] will have the mode of the file upon return.
	 * @param alias If provided and long enough, [0] will be used first as the alias of the file,
	 *   and [0] will have the alias used to stat the file upon return.
	 * @throws IOException If there is a problem communicating with the station.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void statFile(short to, String path, int[] out_size, int[] out_timestamp, byte[] out_mode, int[] alias) throws IOException, TimeoutException {
		boolean closeAtEnd = false;
		
		if (alias == null || alias.length < 1) {
			alias = new int[1];
			alias[0] = -1;
			closeAtEnd = true;
		}
		
		for (int failureCounter = 0; failureCounter < getTries(); failureCounter++) {
    		if (alias[0] == -1) alias[0] = getAlias(to, path);
    		
    		UDRMessage response = send(to, new FS_Stat(FS_Stat.O_NONE, alias[0]));
    		
    		if (response instanceof UDR_Acknowledge) {
    			if (FS_Stat.getError((UDR_Acknowledge)response) == FS_Stat.E_INVALID_ALIAS) {
    				// The alias somehow expired already, so refresh it and try again
    				// Don't count this as a failure
    				failureCounter--;
    				alias[0] = -1;
    				continue;
    			}
    			
    			if (FS_Stat.getError((UDR_Acknowledge)response) != FS_Stat.E_NO_ERROR) {
    				if (failureCounter == getTries() - 1) throw new IOException("Bad response from station: " + FS_Write.translateError((UDR_Acknowledge)response, 0));
    				continue;
    			}
    		} else {
    			if (failureCounter == getTries() - 1) throw new IOException("Bad response from station: " + response.getClass());
    			continue;
    		}
    		
    		UDR_Acknowledge ack = (UDR_Acknowledge)response;
			if (out_size != null && out_size.length >= 1) out_size[0] = FS_Stat.getSize(ack);
			if (out_timestamp != null && out_timestamp.length >= 1) out_timestamp[0] = FS_Stat.getTimestamp(ack);
			if (out_mode != null && out_mode.length >= 1) out_mode[0] = FS_Stat.getAccess(ack);
			break;
		}
		
		if (closeAtEnd) closeAlias(to, alias[0]);
	}
	
	/**
	 * Rename a remote file.
	 * @param to The station to send to.
	 * @param oldPath The current name of the file.
	 * @param newPath The new name of the file.
	 * @throws IOException If there is a problem communicating with the station.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void renameFile(short to, String oldPath, String newPath) throws IOException, TimeoutException {
		guaranteedSend(to, new FS_Rename(FS_Rename.O_NONE, oldPath, newPath));
	}
	
	/**
	 * Copy a file from the local machine to a remote station.
	 * This function will simply create and copy the file, it will not deal with any other
	 *   files in the same directory, which may be necessary for less intelligent devices.
	 * @param to The station to copy the file to.
	 * @param localFile The file on the local machine to copy.
	 * @param remotePath The name that the file should take on the remote device.
	 * @throws IOException If there is an error reading 'localFile' or communicating with the station.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while working.
	 */
	public void copyFile(short to, File localFile, String remotePath) throws IOException, TimeoutException {
		copyFile(to, localFile, remotePath, null);
	}
	
	/**
	 * Copy a file from the local machine to a remote station.
	 * This function will simply create and copy the file, it will not deal with any other
	 *   files in the same directory, which may be necessary for less intelligent devices.
	 * @param to The station to copy the file to.
	 * @param localFile The file on the local machine to copy.
	 * @param remotePath The name that the file should take on the remote device.
	 * @param progressListener If provided, will receive notifications of the progress of the file copy operation.
	 * @throws IOException If there is an error reading 'localFile' or communicating with the station.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while working.
	 */
	public void copyFile(short to, File localFile, String remotePath, ProgressListener progressListener) throws IOException, TimeoutException {
		if (!localFile.isFile()) {
	    	throw new IOException(localFile.getAbsolutePath() + " does not exist or is not a file");
	    }
	    
		Utils.debug("Copying '" + localFile.getAbsolutePath() + "' to '" + remotePath + "'");
		
	    byte[] data;
		FileInputStream fin = new FileInputStream(localFile);
	    try {
    	    data = Utils.bytesToEOF(fin);
	    } finally {
	    	fin.close();
	    }
	    
	    Utils.debug("  Read " + data.length + " bytes from '" + localFile.getAbsolutePath() + "'");
	    
	    // If the file doesn't exist, create it
	    try {
	    	statFile(to, remotePath, null, null, null, null);
	    } catch (IOException ioe) {
	    	int alias = createFile(to, remotePath, data.length);
	    	closeAlias(to, alias); // Not used here
	    }
	    
	    writeFile(to, remotePath, data, 0, null, progressListener);
    }
	
	/**
	 * Get a directory listing of a remote directory.
	 * @param to The station to send to.
	 * @param path The path to the remote directory.
	 * @param extended Whether to get extended data about the directory entries.
	 * @param alias If provided and long enough, alias[0] will be used to access the directory,
	 *   and alias[0] will have the alias used to access the directory upon return.
	 * @return An array of directory entries describing the contents of the directory.
	 * @throws IOException If there is a problem communicating with the device.
	 * @throws TimeoutException If the device does not respond.
	 */
	public FS_Dir.DirEntry[] dir(short to, String path, boolean extended, int[] alias) throws IOException, TimeoutException {
		List<FS_Dir.DirEntry> entries = new LinkedList<FS_Dir.DirEntry>();
		
		boolean closeAtEnd = false;
		
		if (alias == null || alias.length < 1) {
			alias = new int[1];
			alias[0] = -1;
			closeAtEnd = true;
		}
		
		int failureCounter = 0;
		final byte options = extended ? FS_Dir.O_EXTENDED : FS_Dir.O_STANDARD;
		int position = 0;
		
		do {
			if (alias[0] == -1) alias[0] = getAlias(to, path);
			
			UDRMessage response = send(to, new FS_Dir(options, alias[0], position));
			
			if (response instanceof UDR_Acknowledge) {
				if (FS_Dir.getError((UDR_Acknowledge)response) == FS_Dir.E_INVALID_ALIAS) {
					alias[0] = -1;
					continue;
				}
				
				if (FS_Dir.getError((UDR_Acknowledge)response) != FS_Dir.E_NO_ERROR) {
					failureCounter++;
					
					if (failureCounter > getTries()) throw new IOException("Bad response from station: " + FS_Dir.translateError((UDR_Acknowledge)response));
					continue;
				}
			} else {
				failureCounter++;
				
				if (failureCounter > getTries()) throw new IOException("Bad response from station: " + response.getClass());
				
				// Give it a moment, maybe it's busy
				Utils.debug("Station NAKed a Dir, giving it a moment to relax");
				Utils.sleep(fileDelay);
				
				continue;
			}
			
			FS_Dir.DirEntry[] messageEntries = FS_Dir.getEntries((UDR_Acknowledge)response, options);
			if (messageEntries.length == 0) break; // Nothing left
			
			entries.addAll(Utils.makeLinkedList(messageEntries));
			
			position = FS_Dir.getPosition((UDR_Acknowledge)response);
		} while (position != -1);
		
		if (closeAtEnd) closeAlias(to, alias[0]);
		
		return entries.toArray(new FS_Dir.DirEntry[0]);
	}
	
	// ***** Gateway Messages *****
	/**
	 * Reset a gateway. Returns immediately, you must wait yourself for the station to come back up.
	 * This waits for 250ms before sending the reset message, in case the station needs to flush some
	 *   file buffers or something.
	 * @param to The station to send to.
	 */
	public void resetGateway(short to) {
		try {
			// Delay for 1/4 second, in case someone was writing file data to the station
			Utils.sleep(250);
			send(to, new GWY_ResetGateway(), false);
		} catch (TimeoutException te) {
			// Not actually possible, this is just here as an implementation detail
		} catch (IOException ioe) {
			// Same with this one, since there's no acknowledgment
		}
	}
	
	/**
	 * Reset an IPm station. Returns after receiving an acknowledgment. Does not wait for the station
	 *   to come back up.
	 * @param to The station to send to.
	 * @param serial The serial number of the base that the station is plugged into.
	 * @param hard Whether to perform a hard reset.
	 * @throws IOException If an I/O error occurs.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void resetIPm(short to, int serial, boolean hard) throws IOException, TimeoutException {
		guaranteedSend(to, new GWY_ResetIPm(serial, hard ? GWY_ResetIPm.O_HARD_RESET : GWY_ResetIPm.O_SOFT_RESET));
	}
	
	/**
	 * Execute a command on a station, returning the output from it (up to one message's worth).
	 * You may be better off piping output to a file and then reading that file, rather than relying
	 * on the response from the station. This will only try to run the command once.
	 * Note: You may need to set a long timeout before calling this, or use
	 * executeCommand(short, String, long).
	 * @param to The station to send to.
	 * @return The response from the station.
	 * @param command The command to execute.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public String executeCommand(short to, String command) throws IOException, TimeoutException {
		return executeCommand(to, command, messageTimeout);
	}
	
	/**
	 * Execute a command on a station, returning the output from it (up to one message's worth).
	 * You may be better off piping output to a file and then reading that file, rather than relying
	 * on the response from the station. This will only try to run the command once.
	 * @param to The station to send to.
	 * @param command The command to execute.
	 * @param timeout How long to wait before declaring a timeout.
	 * @return The response from the station.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public String executeCommand(short to, String command, long timeout) throws IOException, TimeoutException {
		int oldTries = getTries();
		setTries(1);
		
		long oldTimeout = getMessageTimeout();
		setMessageTimeout(timeout);
		
		try {
    		UDRMessage response = send(to, new GWY_Execute(GWY_Execute.V_1, GWY_Execute.O_NONE, command));
    		
    		if (response instanceof UDR_Acknowledge) {
    			if (GWY_Execute.getError((UDR_Acknowledge)response) != GWY_Execute.E_NO_ERROR) {
    				throw new IOException("Bad response from station: " + GWY_Execute.translateError((UDR_Acknowledge)response));
    			}
    			
    			return GWY_Execute.getResponse((UDR_Acknowledge)response);
    		} else {
    			throw new IOException("Bad response from station: " + response.getClass());
    		}
		} finally {
			setTries(oldTries);
			setMessageTimeout(oldTimeout);
		}
	}
	
	/**
	 * Read basic settings out of an IPm station, regardless of its serial number.
	 * @param to The station to send to, may be UDRMessage.STA_ANY.
	 * @param out_station The station number of the station that responded.
	 * @param out_serial The serial number of the station that responded.
	 * @param out_ipAddr The IP address of the station that responded.
	 * @param out_netMask The netmask of the station that responded.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void readIPmSettings(short to, short[] out_station, int[] out_serial, int[] out_ipAddr, int[] out_netMask) throws IOException, TimeoutException {
		readIPmSettings(to, GWY_IPmReadSettings.ANY_SERIAL, out_station, out_serial, out_ipAddr, out_netMask);
	}
	
	/**
	 * Read basic settings out of an IPm station.
	 * @param to The station to send to, may be UDRMessage.STA_ANY.
	 * @param serial The serial number of the station to read settings out of.
	 * @param out_station The station number of the station that responded.
	 * @param out_serial The serial number of the station that responded.
	 * @param out_ipAddr The IP address of the station that responded.
	 * @param out_netMask The netmask of the station that responded.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	public void readIPmSettings(short to, int serial, short[] out_station, int[] out_serial, int[] out_ipAddr, int[] out_netMask) throws IOException, TimeoutException {
		if (out_station == null || out_station.length < 1) out_station = new short[1];
		if (out_serial == null || out_serial.length < 1) out_serial = new int[1];
		if (out_ipAddr == null || out_ipAddr.length < 1) out_ipAddr = new int[1];
		if (out_netMask == null || out_netMask.length < 1) out_netMask = new int[1];
		
		UDR_Acknowledge ack = guaranteedSend(to, new GWY_IPmReadSettings(serial, GWY_IPmReadSettings.PS_SERIAL_AND_IP));
		
		out_station[0] = GWY_IPmReadSettings.getStationNumber(ack);
		out_serial[0] = GWY_IPmReadSettings.getSerial(ack);
		out_ipAddr[0] = GWY_IPmReadSettings.getIPAddr(ack);
		out_netMask[0] = GWY_IPmReadSettings.getNetMask(ack);
	}
	
	// ***** Private helper functions *****
	/**
	 * Send a message, guaranteeing that we get an acknowledgment back.
	 * @param to The station to send to.
	 * @param msg The message to send.
	 * @return The acknowledgment sent back by the station.
	 * @throws IOException If the station replies with something other than an ACK.
	 * @throws TimeoutException If the station does not respond.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	private UDR_Acknowledge guaranteedSend(short to, UDRMessage msg) throws IOException, TimeoutException {
		UDRMessage response = send(to, msg);
		
		if (!(response instanceof UDR_Acknowledge)) {
			throw new IOException("Station sent " + response.getClass() + " in response to " + msg.getClass());
		}
		
		return ((UDR_Acknowledge)response);
	}
	
	/**
	 * Send a message.
	 * @param to The station ID to send to.
	 * @param msg The message to send.
	 * @return The response to the message.
	 * @throws TimeoutException If no response is received after getTries() times,
	 *   waiting for getTimeout() milliseconds between each try.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	private UDRMessage send(short to, UDRMessage msg) throws IOException, TimeoutException {
		return send(to, msg, true);
	}
	
	/**
	 * Send a message.
	 * @param to The station ID to send to.
	 * @param msg The message to send.
	 * @param expectResponse True = wait for a response and throw a TimeoutException if one is not received.
	 *   False = do not wait for a response, return null immediately after sending.
	 * @return The response to the message.
	 * @throws TimeoutException If no response is received after getTries() times,
	 *   waiting for getTimeout() milliseconds between each try. This will never happen if
	 *   expectResponse is false.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	private UDRMessage send(short to, UDRMessage msg, boolean expectResponse) throws IOException, TimeoutException {
		msg.setup(getFormat(), getID(), to, session, sequence++);
		
		Utils.debug("Sending\n" + msg.toString(2));
		
		if (expectResponse) {
    		synchronized (inFlight) {
    	        inFlight.put(msg.getSequence(), msg);
            }
		}
		
		for (int tryCount = 0; tryCount < getTries(); tryCount++) {
			try {
				MessageDispatcher.getDispatcher().send(msg);
				
				if (!expectResponse) return null;
				
				UDRMessage response = waitForMessage(msg.getSequence());
				
				if (response instanceof UDR_Acknowledge) {
					if (msg.verifyAck((UDR_Acknowledge)response)) {
						return response;
					} else {
						throw new IOException("Badly formatted reply from station");
					}
				} else {
					return response;
				}
			} catch (TimeoutException te) {
				if (tryCount < getTries() - 1) {
					Utils.debug(te);
					
					Utils.debug("Trying again...");
				} else {
					synchronized (inFlight) {
	                    // Make sure the message gets removed from the inFlight map
	                    inFlight.remove(msg.getSequence());
                    }
					
					throw te;
				}
			} catch (IOException ioe) {
				if (tryCount < getTries() - 1) {
					Utils.debug(ioe);
					
					Utils.debug("Trying again...");
				} else {
					synchronized (inFlight) {
	                    // Make sure the message gets removed from the inFlight map
	                    inFlight.remove(msg.getSequence());
                    }
					
					throw ioe;
				}
			} catch (InterruptedOperationException ioe) {
				synchronized (inFlight) {
	                // Make sure the message gets removed from the inFlight map
	                inFlight.remove(msg.getSequence());
                }
				
				throw ioe;
			}
		}
		
		// Can't actually reach this point
		throw new RuntimeException("Unreachable statement");
	}
	
	/**
	 * Wait for a response from the station.
	 * @param seq The sequence number to wait for.
	 * @return The response.
	 * @throws TimeoutException If no response is received after getMessageTimeout() milliseconds.
	 * @throws InterruptedOperationException If the thread is interrupted while waiting for a response.
	 */
	private UDRMessage waitForMessage(byte seq) throws TimeoutException {
		Utils.debug("Waiting for response to message with sequence number " + seq);
		
		synchronized (received) {
			UDRMessage ans = null;
			boolean found = false;
			long endTime = System.currentTimeMillis() + getMessageTimeout();
			
			while (!found && (getMessageTimeout() == 0 || endTime > System.currentTimeMillis())) {
				Iterator<MessageDispatcher.Message> itMessages = received.iterator();
				while (itMessages.hasNext()) {
					MessageDispatcher.Message msg = itMessages.next();
					
					if (msg.msg.getSequence() == seq) {
						Utils.debug("Received message with sequence number " + seq);
						itMessages.remove();
						
						ans = msg.msg;
						found = true;
						
						break;
					}
				}
				
				if (!found) {
					try {
						long waitFor = endTime - System.currentTimeMillis();
						if (waitFor <= 0) waitFor = 1; // Wait for 1ms so we can just loop around and throw a timeout exception
						if (getMessageTimeout() == 0) waitFor = 0; // Wait forever, since the caller set timeout to 0
						
						received.wait(waitFor);
					} catch (InterruptedException ie) {
						throw new InterruptedOperationException(ie);
					}
				}
			}
			
			if (ans == null) {
				Utils.debug("Timed out while waiting for " + seq);
				throw new TimeoutException("Timed out waiting for response");
			} else {
				Utils.debug("Done waiting for message " + seq);
				return ans;
			}
		}
	}
}

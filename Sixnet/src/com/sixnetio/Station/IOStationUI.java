/*
 * IOStationUI.java
 *
 * An interface that must be implemented by any class which wants to
 * perform the actions of an IOStation user interface. For a GUI,
 * I would recommend a diagram similar to EtherTRAK, etc. with
 * lights that turn on/off depending on what is going on.
 *
 * Jonathan Pearson
 * April 16, 2007
 *
 */

package com.sixnetio.Station;

public interface IOStationUI {
    public void putDI(IOStation from, int index, boolean value);
    public void putDO(IOStation from, int index, boolean value);
    public void putAI(IOStation from, int index, short value);
    public void putAO(IOStation from, int index, short value);
}

/*
 * UDPInputStream.java
 *
 * Yes, it is basically a contradiction in terms, but this acts like an
 * InputStream which will read data from an open DatagramSocket. It does nothing
 * special to the data, just pulls it out of a DatagramSocket and returns it.
 * This will buffer data until it has been read, as the user may not be reading
 * a full packet at a time.
 *
 * Jonathan Pearson
 * March 20, 2007
 *
 */

package com.sixnetio.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Treat a DatagramSocket as an input stream.
 *
 * @author Jonathan Pearson
 */
public class UDPInputStream
	extends InputStream
{
	private DatagramSocket sock;
	private byte[] data;
	private int pos; // index of next byte to read
	
	private DatagramPacket lastPacket;
	
	/**
	 * Construct a new UDPInputStream.
	 * 
	 * @param sock The socket to read from.
	 */
	public UDPInputStream(DatagramSocket sock)
	{
		this.sock = sock;
		
		data = new byte[0]; // cause an immediate receive
	}
	
	@Override
	public void close()
		throws IOException
	{
		sock.close();
	}
	
	@Override
	public int available()
		throws IOException
	{
		return (data.length - pos);
	}
	
	@Override
	public synchronized int read()
		throws IOException
	{
		while (pos >= data.length) {
			getPacket();
		}
		
		return (data[pos++] & 0xFF);
	}
	
	@Override
	public int read(byte[] b)
		throws IOException
	{
		return read(b, 0, b.length);
	}
	
	@Override
	public synchronized int read(byte[] b, int offset, int len)
		throws IOException
	{
		// Make sure we have data
		while (pos >= data.length) {
			getPacket();
		}
		
		// Minimum of data available and data requested
		if (data.length - pos < len) {
			len = data.length - pos;
		}
		
		System.arraycopy(data, pos, b, offset, len);
		pos += len;
		
		return len;
	}
	
	/**
	 * Get the last received packet.
	 */
	public DatagramPacket getLastPacket()
	{
		return lastPacket;
	}
	
	/**
	 * Discard any remaining data in the buffer, forcing the next read to go to
	 * the socket.
	 */
	public synchronized void flush()
	{
		pos = data.length;
	}
	
	/**
	 * Retrieve the next packet from the socket.
	 * 
	 * @throws IOException If there was a problem reading from the socket.
	 */
	private void getPacket()
		throws IOException
	{
		DatagramPacket pkt =
			new DatagramPacket(new byte[sock.getReceiveBufferSize()],
			                   sock.getReceiveBufferSize());
		sock.receive(pkt);
		data = pkt.getData();
		pos = pkt.getOffset();
		
		lastPacket = pkt;
	}
}

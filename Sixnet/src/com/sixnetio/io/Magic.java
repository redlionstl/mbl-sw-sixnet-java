/*
 * Magic.java
 *
 * Determines file types based on magic numbers, or similar heuristic methods.
 *
 * Jonathan Pearson
 * July 2, 2010
 *
 */

package com.sixnetio.io;

import java.io.*;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;

import com.sixnetio.util.Conversion;

/**
 * Determines file types base on magic numbers, or similar heuristic methods.
 *
 * @author Jonathan Pearson
 */
public class Magic
{
	public static enum FileType
	{
		/** The file is a Sixnet Firmware Bundle. */
		FirmwareBundle(),
		
		/** The file is a GZipped GNU Tarball. */
		// Needs to be before GZip, because that would always find this
		GZippedTarball(),
		
		/** The file is GZip-compressed. */
		GZip(new byte[] { 0x1f, (byte)0x8b, 0x08, 0x00 }, 0),
		
		/** The file is a big-endian JFFS2 image. */
		JFFS2BigEndian(new byte[] { 0x19, (byte)0x85 }, 0),
		
		/** The file is a little-endian JFFS2 image. */
		JFFS2LittleEndian(new byte[] { (byte)0x85, 0x19 }, 0),
		
		/** The file is a GNU Tarball. */
		// Look for "ustar" in the first file entry
		// getBytes() should return UTF-8, and "ustar" is all between 0-127, so
		// the result will be standard ASCII (we can't pass a charset name
		// because there is no way to handle the UnsupportedEncodingException)
		Tarball("ustar".getBytes(), 257),
		
		/** The file type is unknown. */
		Unknown(),
		;
		
		final boolean standard;
		private final byte[] magicNumber;
		private final int magicOffset;
		
		private FileType()
		{
			standard = false;
			magicNumber = null;
			magicOffset = -1;
		}
		
		private FileType(byte[] magicNumber, int magicOffset)
		{
			standard = true;
			this.magicNumber = magicNumber;
			this.magicOffset = magicOffset;
		}
		
		/**
		 * Test whether the given file data matches this file type.
		 * 
		 * @param data The data to check.
		 * @return True if the data is likely this file type, false if not.
		 */
		boolean isMatch(byte[] data)
			throws IllegalStateException
		{
			if (standard) {
				if (data.length < magicOffset + magicNumber.length) {
					// Not enough data to be this type of file
					return false;
				}
				
				byte[] magic = new byte[magicNumber.length];
				System.arraycopy(data, magicOffset, magic, 0, magic.length);
				
				return Arrays.equals(magicNumber, magic);
			}
			else {
				// The test is more involved for the non-standard types
				switch (this) {
					// Skip the standard types, we did them above
					case JFFS2BigEndian:
					case JFFS2LittleEndian:
					case GZip:
					case Tarball:
						assert(false); // Should not have gotten here
						
					case Unknown: // Skip 'Unknown'; nothing matches this one
						return false;
					
					case FirmwareBundle:
						return isFirmwareBundle(data);
						
					case GZippedTarball:
						return isGZippedTarball(data);
				}
				
				// Should not get here
				throw new RuntimeException("Unknown file type: " +
				                           this.name());
			}
		}
		
		/**
		 * Test whether the given open file matches this file type.
		 * 
		 * @param raf The file to check.
		 * @return True if the file is likely this file type, false if not.
		 */
		boolean isMatch(RandomAccessFile raf)
			throws IOException
		{
			if (standard) {
				if (raf.length() < magicOffset + magicNumber.length) {
					// Not enough data to be this type of file
					return false;
				}
				
				byte[] magic = new byte[magicNumber.length];
				raf.seek(magicOffset);
				raf.read(magic);
				
				return Arrays.equals(magicNumber, magic);
			}
			else {
				// The test is more involved for the non-standard types
				switch (this) {
					// Skip the standard types, we did them above
					case JFFS2BigEndian:
					case JFFS2LittleEndian:
					case GZip:
					case Tarball:
						assert(false); // Should not have gotten here
						
					case Unknown: // Skip 'Unknown'; nothing matches this one
						return false;
					
					case FirmwareBundle:
						return isFirmwareBundle(raf);
						
					case GZippedTarball:
						return isGZippedTarball(raf);
				}
				
				// Should not get here
				throw new RuntimeException("Unknown file type: " +
				                           this.name());
			}
		}
	}
	
	/**
	 * Attempt to detect the likely file type of the given data.
	 * 
	 * @param data The data to test.
	 * @return The file type of the data, or {@link FileType#Unknown} if it
	 *   cannot be determined.
	 */
	public static FileType detectType(byte[] data)
	{
		// Loop through the file types, searching for the right one
		for (FileType ft : FileType.values()) {
			if (ft.isMatch(data)) {
				return ft;
			}
		}
		
		return FileType.Unknown;
	}
	
	/**
	 * Attempt to detect the likely file type of the given file, based on
	 * contents.
	 * 
	 * @param f The file to test.
	 * @return The file type of the file, or {@link FileType#Unknown} if it
	 *   cannot be determined.
	 * @throws IOException If there is a problem reading the file.
	 */
	public static FileType detectType(File f)
		throws IOException
	{
		// Open the file and loop through the file types on it
		RandomAccessFile raf = new RandomAccessFile(f, "r");
		try {
			for (FileType ft : FileType.values()) {
				if (ft.isMatch(raf)) {
					return ft;
				}
			}
		}
		finally {
			raf.close();
		}
		
		return FileType.Unknown;
	}
	
	
	/**
	 * Detect whether the given data is a Sixnet Firmware Bundle.
	 * 
	 * @param data The data to check.
	 * @return True if it is likely a firmware bundle, false otherwise.
	 */
	static boolean isFirmwareBundle(byte[] data)
	{
		byte[] expect;
		int offset = 0;
		
		// First four bytes should be 00000001 for a v1 bundle
		expect = new byte[] { 0x00, 0x00, 0x00, 0x01 };
		if ( ! match(data, offset, expect)) {
			return false;
		}
		offset += 4;
		
		// Skip 'firmware type' field
		offset += 4;
		
		// Skip the 'firmware version' length-encoded string field
		if (data.length < offset + 4) {
			// Not enough data for the length of the string
			return false;
		}
		
		int len = Conversion.bytesToInt(data, offset);
		offset += 4;
		offset += len;
		
		// Offset will be checked by the next 'match' call, no need to test it
		// again ourselves
		
		// Next entry is the root directory
		// We need the User Type field from it, looking for "FWB" 0x01
		// Skip the 'type' field
		offset += 4;
		
		// User type field is here
		expect = new byte[] { 0x46, 0x57, 0x42, 0x01 }; // F, W, B, 0x01
		if ( ! match(data, offset, expect)) {
			return false;
		}
		
		// We can be confident that this is a firmware bundle now
		return true;
	}
	
	/**
	 * Detect whether the given open file is a Sixnet Firmware Bundle.
	 * 
	 * @param raf The file to check.
	 * @return True if it is likely a firmware bundle, false otherwise.
	 * @throws IOException If there is a problem reading from the file.
	 */
	static boolean isFirmwareBundle(RandomAccessFile raf)
		throws IOException
	{
		byte[] expect;
		int offset = 0;
		
		// First four bytes should be 00000001 for a v1 bundle
		expect = new byte[] { 0x00, 0x00, 0x00, 0x01 };
		if ( ! match(raf, offset, expect)) {
			return false;
		}
		offset += 4;
		
		// Skip 'firmware type' field
		offset += 4;
		
		// Skip the 'firmware version' length-encoded string field
		if (raf.length() < offset + 4) {
			// Not enough data for the length of the string
			return false;
		}
		
		raf.seek(offset);
		int len = raf.readInt();
		offset += 4;
		offset += len;
		
		// Offset will be checked by the next 'match' call, no need to test it
		// again ourselves
		
		// Next entry is the root directory
		// We need the User Type field from it, looking for "FWB" 0x01
		// Skip the 'type' field
		offset += 4;
		
		// User type field is here
		expect = new byte[] { 0x46, 0x57, 0x42, 0x01 }; // F, W, B, 0x01
		if ( ! match(raf, offset, expect)) {
			return false;
		}
		
		// We can be confident that this is a firmware bundle now
		return true;
	}
	
	/**
	 * Test whether the given data is a GZipped GNU Tarball.
	 * 
	 * @param data The data to check.
	 * @return True if it is a GZipped GNU Tarball, false otherwise.
	 */
	static boolean isGZippedTarball(byte[] data)
	{
		// Make sure it's a GZip first (faster test)
		if ( ! FileType.GZip.isMatch(data)) {
			return false;
		}
		
		try {
			GZIPInputStream in =
				new GZIPInputStream(new ByteArrayInputStream(data));
			
			// Skip the first 257 bytes, then read for "ustar"
			in.skip(257);
			
			byte[] expect = "ustar".getBytes();
			byte[] found = new byte[expect.length];
			in.read(found);
			
			if ( ! match(found, 0, expect)) {
				return false;
			}
		}
		catch (IOException ioe) {
			// Unable to open the GZip stream? Not a GZippedTarball
			return false;
		}
		
		return true;
	}
	
	/**
	 * Test whether the given file is a GZipped GNU Tarball.
	 * 
	 * @param raf The file to check.
	 * @return True if it is a GZipped GNU Tarball, false otherwise.
	 */
	static boolean isGZippedTarball(final RandomAccessFile raf)
		throws IOException
	{
		if ( ! FileType.GZip.isMatch(raf)) {
			return false;
		}
		
		// GZipInputStream needs a stream to read from, so construct a quick one
		InputStream rafIn = new InputStream() {
			@Override
			public int read()
				throws IOException
			{
				return raf.read();
			}
			
			@Override
			public long skip(long count)
				throws IOException
			{
				if (count > Integer.MAX_VALUE) {
					throw new IllegalArgumentException("Skip count too large");
				}
				
				return raf.skipBytes((int)count);
			}
		};

                GZIPInputStream in;
                try {
                    in = new GZIPInputStream(rafIn);
                }
                catch (IOException ioe) {
                    throw new IOException(
                        "\nThis utility can't currently handle " +
                        "gzipped tarballs. Try gunzipping the\n" +
                        "file and feeding in the resulting tarball.");
                }
		
		// Skip the first 257 bytes, then read for "ustar"
		in.skip(257);
		
		byte[] expect = "ustar".getBytes();
		byte[] found = new byte[expect.length];
		in.read(found);
		
		if ( ! match(found, 0, expect)) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Check whether the given array slice matches what we expect to see.
	 * 
	 * @param data The data containing the array slice to test.
	 * @param offset The offset of the beginning of the slice within
	 *   <code>data</code>.
	 * @param expect The bytes we expect to see.
	 * @return True if the slice of <code>data</code> matched
	 *   <code>expect</code>, false otherwise.
	 */
	private static boolean match(byte[] data, int offset, byte[] expect)
	{
		if (data.length < offset + expect.length) {
			// Not enough data
			return false;
		}
		
		for (int i = 0; i < expect.length; i++) {
			if (data[offset + i] != expect[i]) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Check whether the open file matches what we expect to see in the given
	 * position.
	 * 
	 * @param raf The file.
	 * @param offset The offset of the beginning of the slice within
	 *   <code>data</code>.
	 * @param expect The bytes we expect to see.
	 * @return True if the slice of <code>data</code> matched
	 *   <code>expect</code>, false otherwise.
	 * @throws IOException If there is a problem reading from the file.
	 */
	private static boolean match(RandomAccessFile raf, int offset,
	                             byte[] expect)
		throws IOException
	{
		if (raf.length() < offset + expect.length) {
			// Not enough data
			return false;
		}
		
		raf.seek(offset);
		for (int i = 0; i < expect.length; i++) {
			if (raf.read() != expect[i]) {
				return false;
			}
		}
		
		return true;
	}
}

/*
 * UDPOutputStream.java
 *
 * Yes, it is basically a contradiction in terms, but this acts like an
 * OutputStream which will send data over an open DatagramSocket. It does
 * nothing special to the data, just packages it up into a datagram and sends
 * it. This will buffer data until flushed or the buffer runs out.
 *
 * Jonathan Pearson
 * March 20, 2007
 *
 */

package com.sixnetio.io;

import java.io.IOException;
import java.io.OutputStream;
import java.net.*;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

public class UDPOutputStream
	extends OutputStream
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	// Default amount of data before flushing
	private static final int D_BUFFER_SIZE = 1024;
	
	private DatagramSocket sock;
	private byte[] data;
	private int pos = 0;
	
	private InetAddress to;
	private int port;
	
	/**
	 * Construct a new UDPOutputStream.
	 * 
	 * @param sock The socket to write to.
	 * @param to When using standard output stream functions, the address to
	 *   send to when the buffer is full.
	 * @param port The default port to send to when the buffer is full.
	 */
	public UDPOutputStream(DatagramSocket sock, InetAddress to, int port)
	{
		this(sock, to, port, D_BUFFER_SIZE);
	}
	
	/**
	 * Construct a new UDPOutputStream.
	 * 
	 * @param sock The socket to write to.
	 * @param to When using standard output stream functions, the address to
	 *   send to when the buffer is full.
	 * @param port The default port to send to when the buffer is full.
	 * @param bufferSize The size of the buffer..
	 */
	public UDPOutputStream(DatagramSocket sock, InetAddress to, int port,
	                       int bufferSize)
	{
		this.sock = sock;
		data = new byte[bufferSize];
		
		this.to = to;
		this.port = port;
	}
	
	@Override
	public void close()
		throws IOException
	{
		sock.close();
	}
	
	@Override
	public synchronized void write(int b)
		throws IOException
	{
		logger.debug("Buffering 1 byte");
		
		data[pos++] = (byte)b;
		
		if (pos == data.length) {
			// No point in using >=, since > would fail with an
			// ArrayIndexOutOfBoundsException
			flush();
		}
	}
	
	@Override
	public synchronized void write(byte[] data)
		throws IOException
	{
		write(data, 0, data.length);
	}
	
	@Override
	public synchronized void write(byte[] data, int off, int len)
		throws IOException
	{
		while (len > 0) {
			if (pos + len >= this.data.length) {
				logger.debug(String.format("Buffering %d bytes",
				                           this.data.length - pos));
				
				System.arraycopy(data, off, this.data, pos,
				                 this.data.length - pos);
				
				pos = this.data.length;
				off += this.data.length - pos;
				len -= this.data.length - pos;
				flush();
			}
			else {
				logger.debug(String.format("Buffering %d bytes", len));
				System.arraycopy(data, off, this.data, pos, len);
				
				pos += len;
				off += len;
				len = 0;
			}
		}
	}
	
	/**
	 * Write a data buffer to a specific address, bypassing the buffer.
	 * 
	 * @param data The data to send.
	 * @param start The starting index in the data being sent.
	 * @param len The number of bytes to send.
	 * @param to The address to send the data to.
	 * @param port The port to send to.
	 * @throws IOException If there was a problem sending the data.
	 */
	public void writeTo(byte[] data, int start, int len, InetAddress to,
	                    int port)
		throws IOException
	{
		logger.debug(String.format("Writing %d bytes to %s:%d",
		                           len, to.getHostAddress(), port));
		DatagramPacket pkt = new DatagramPacket(data, start, len, to, port);
		sock.send(pkt);
	}
	
	@Override
	public synchronized void flush()
		throws IOException
	{
		if (pos == 0) {
			// Nothing to do
			logger.debug("Skipping flush, no buffered data");
			return;
		}
		
		logger.debug(String.format("Flushing %d bytes to %s:%d",
		                           pos, to.getHostAddress(), port));
		writeTo(data, 0, pos, to, port);
		pos = 0;
	}
}

/*
 * CSVParser.java
 *
 * Used to parse data from a CSV file.
 * 
 * Jonathan Pearson
 * February 5, 2009
 *
 */

package com.sixnetio.io;

import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Parses CSV data. This class is NOT THREAD-SAFE.
 *
 * @author Jonathan Pearson
 */
public class CSVParser {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private LineNumberReader in;
	
	private List<String> columns;
	private List<String> warnings;
	
	private Map<String, String> current;
	
	/**
	 * Construct a new CSV parser.
	 * 
	 * @param in The stream to read CSV data from.
	 */
	public CSVParser(InputStream in) {
		this(new LineNumberReader(new InputStreamReader(in)));
	}
	
	/**
	 * Construct a new CSV parser.
	 * 
	 * @param in The BufferedReader to get lines from.
	 */
	public CSVParser(LineNumberReader in) {
		this.in = in;
		
		this.columns = new Vector<String>();
		this.warnings = new LinkedList<String>();
	}
	
	/**
	 * Create new columns to expect while reading data. Call this for each
	 * column you expect, in the order that the columns appear in the CSV file.
	 * Even if you are not interested in the data from a column, if you want a
	 * column after it then you must name the column to skip. You may give blank
	 * names to columns you are not interested in; no warnings will be generated
	 * for duplicate column names with different values if the column names are
	 * blank. The CSV data may contain more columns than you specify, but if any
	 * line contains fewer then it is skipped as erroneous and a warning is
	 * generated.
	 * 
	 * @param columns Names of columns to add. If <tt>null</tt>, no action is
	 *            taken.
	 */
	public void addColumns(String... columnNames) {
		if (columnNames == null) return;
		
		synchronized (columns) {
    		for (String s : columnNames) {
    			columns.add(s);
    		}
		}
	}
	
	/**
	 * Delete the named columns. You should only need to call this if the format
	 * of the CSV data changes in the middle.
	 * 
	 * @param columnNames Names of columns to delete. Columns after each named
	 *            column will all be shifted to the left. If <tt>null</tt>, no
	 *            action is taken.
	 */
	public void remColumns(String... columnNames) {
		if (columnNames == null) return;
		
		synchronized (columns) {
    		for (String s : columnNames) {
    			columns.remove(s);
    		}
		}
	}
	
	/**
	 * Delete all columns. You should only need to call this if the format of
	 * the CSV data changes in the middle.
	 */
	public void clearColumns() {
		synchronized (columns) {
			columns.clear();
		}
	}
	
	/**
	 * Get a list of the column names. Modifying this list will not affect the
	 * internal workings of this object.
	 */
	public List<String> getColumns() {
		synchronized (columns) {
			return new LinkedList<String>(columns);
		}
	}
	
	/**
	 * Move on to the next column in the CSV file. You must call either this or
	 * {@link #next()} prior to any attempt to retrieve data. This will catch
	 * IOExceptions and format them as warnings, returning <tt>false</tt> if
	 * that happens.
	 * 
	 * @return <tt>true</tt> = there was a column to move to; <tt>false</tt> =
	 *         there were no more columns, or an IOException was thrown by the
	 *         underlying stream.
	 */
	public boolean safeNext() {
		try {
			return next();
		} catch (IOException ioe) {
			logger.error("Error reading inpum", ioe);
			
			warnings.add(String.format("Error reading input: %s", ioe.getMessage()));
		}
		
		current = null;
		return false;
	}
	
	/**
	 * Move on to the next column in the CSV file. You must call either this or
	 * {@link #safeNext()} prior to any attempt to retrieve data.
	 * 
	 * @return <tt>true</tt> = there was a column to move to; <tt>false</tt> =
	 *         there were no more columns, or an IOException was thrown by the
	 *         underlying stream.
	 * @throws IOException If the underlying stream throws one. These will not
	 *             be added to the list of warnings.
	 */
	public boolean next() throws IOException {
		// Read from the stream until we hit a CSV line
		// If we hit EOF, return false
		// Keep track of warnings as we go
		String lineIn;
		
		while ((lineIn = in.readLine()) != null) {
			if (lineIn.length() == 0) continue; // Blank lines are fine
			
			String[] tokens = Utils.tokenize(lineIn, ",", false);
			
			synchronized (columns) {
    			if (tokens.length < columns.size()) {
    				warnings.add(String.format("Line %,d contains %,d columns, but we require %,d",
    				                           in.getLineNumber(), tokens.length, columns.size()));
    				continue;
    			}
    			
    			// Parse
    			current = new HashMap<String, String>();
    			for (int i = 0; i < tokens.length; i++) {
    				String key = columns.get(i);
    				String value = tokens[i];
    				String oldVal = current.get(key);
    				
    				// If the column actually has a name, check if it's a duplicate
    				// Unnamed columns are allowed as place holders; their data
    				// should not be relied upon
    				if (key.length() > 0 && oldVal != null && !oldVal.equals(value)) {
    					warnings
    					        .add(String
    					                   .format(
    					                           "Line %,d contains duplicate column with different values; using the first",
    					                           in.getLineNumber()));
    				} else {
    					current.put(key, value);
    				}
    			}
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Get the value of the named column in the current line of the CSV data.
	 * 
	 * @param name The name of the column to retrieve.
	 * @return The value of the named column, or <tt>null</tt> if that column is
	 *         unknown.
	 */
	public String getColumn(String name) {
		if (current == null) throw new IllegalStateException(
		                                                     "A call to next() must return true before you may read data");
		
		return current.get(name);
	}
	
	/**
	 * Get the current line number in the CSV data. The first line is 1.
	 */
	public int getLineNumber() {
		return in.getLineNumber();
	}
	
	/**
	 * Get a list of warnings. This list is not thread-safe, so DO NOT use it in
	 * a separate thread from the one calling {@link #next()} without extra
	 * synchronization.
	 * 
	 * @return A list of warnings that have occurred. You may remove items from
	 *         this list (or add them if you really want), allowing you to check
	 *         for warnings and clear them after each line. The list is
	 *         preserved between calls to {@link #next()}.
	 */
	public List<String> getWarnings() {
		return warnings;
	}
	
	/**
	 * Close the underlying stream.
	 * 
	 * @throws IOException If the 'close()' call throws one.
	 */
	public void close() throws IOException {
		in.close();
	}
	
	/**
	 * Dump all warnings to the provided stream, clearing each warning as it is
	 * printed. This is not thread-safe, so DO NOT call it from a separate thread
	 * as {@link #next()} without extra synchronization.
	 * 
	 * @param out The stream to print warnings to (one per line).
	 */
	public void dumpWarnings(PrintStream out) {
		for (Iterator<String> itWarnings = getWarnings().iterator(); itWarnings.hasNext();) {
			String warning = itWarnings.next();
			itWarnings.remove();
			
			out.println(warning);
		}
	}
}

/*
 * INIParser.java
 *
 * Used to parse data from an INI-type file.
 *
 * Jonathan Pearson
 * April 14, 2009
 *
 */

package com.sixnetio.io;

import java.io.*;
import java.util.*;

/**
 * Parses INI-format files. This class is NOT THREAD-SAFE. As an
 * {@link Iterable}, iterates over section names.
 */
public class INIParser
    implements Iterable<String>
{
    private Map<String, Map<String, String>> contents;

    /**
     * Construct a new INI parser with an initially empty table of values.
     */
    public INIParser()
    {
        this.contents = new LinkedHashMap<String, Map<String, String>>();
    }

    /**
     * Construct a new INI parser.
     * 
     * @param in The stream to read INI data from.
     * @throws IOException If the underlying stream throws one, or if the file
     *             format is incorrect.
     */
    public INIParser(InputStream in)
        throws IOException
    {
        this(new LineNumberReader(new InputStreamReader(in)));
    }

    /**
     * Construct a new INI parser.
     * 
     * @param in The BufferedReader to get lines from.
     * @throws IOException If the underlying stream throws one, or if the file
     *             format is incorrect.
     */
    public INIParser(LineNumberReader in)
        throws IOException
    {
        this.contents = new LinkedHashMap<String, Map<String, String>>();
        parse(in);
    }

    /**
     * Parse the stream until EOF is reached.
     * 
     * @param in The stream to read from.
     * @throws IOException If there is an underlying stream error or a file
     *             formatting problem.
     */
    private void parse(LineNumberReader in)
        throws IOException
    {
        String lineIn;

        try {
            String section = null;

            while ((lineIn = in.readLine()) != null) {
                int comment = lineIn.indexOf(';');
                if (comment > -1) {
                    lineIn = lineIn.substring(0, comment);
                }

                comment = lineIn.indexOf('#');
                if (comment > -1) {
                    lineIn = lineIn.substring(0, comment);
                }

                lineIn = lineIn.trim();

                if (lineIn.length() == 0) {
                    continue;
                }

                if (lineIn.startsWith("[")) {
                    if (!lineIn.endsWith("]")) {
                        throw new Exception("Missing ']' character");
                    }

                    section = lineIn.substring(1, lineIn.length() - 1);
                    if (contents.containsKey(section)) {
                        throw new Exception(String.format(
                            "Duplicate section '%s'", section));
                    }

                    contents.put(section, new LinkedHashMap<String, String>());
                }
                else {
                    if (lineIn.indexOf('=') == -1) {
                        throw new Exception("Missing '=' character");
                    }

                    String key =
                        lineIn.substring(0, lineIn.indexOf('=')).trim();
                    String value =
                        lineIn.substring(lineIn.indexOf('=') + 1).trim();

                    if (section == null) {
                        throw new Exception("Data found outside of a section");
                    }

                    contents.get(section).put(key, value);
                }
            }
        }
        catch (Exception e) {
            if (e instanceof IOException) {
                throw (IOException)e;
            }
            else {
                throw new IOException(String.format(
                    "Bad file format (line %d): %s", in.getLineNumber(), e
                        .getMessage()), e);
            }
        }
    }

    /**
     * Get a set containing the names of all sections in the file.
     */
    public Set<String> getSections()
    {
        return contents.keySet();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Iterable#iterator()
     */
    public Iterator<String> iterator()
    {
        return getSections().iterator();
    }

    /**
     * Check whether a given section name exists.
     * 
     * @param section The section name.
     * @return True = the section exists, false = the section does not exist.
     */
    public boolean sectionExists(String section)
    {
        return contents.containsKey(section);
    }

    /**
     * Get the set of keys in the given section.
     * 
     * @param section The section name.
     * @return The keys in that section, or <tt>null</tt> if not found.
     */
    public Set<String> getKeys(String section)
    {
        Map<String, String> submap = contents.get(section);

        if (submap == null) {
            return null;
        }

        return submap.keySet();
    }

    /**
     * Get the value for the given section/key.
     * 
     * @param section The section name.
     * @param key The key name.
     * @return The value for the section/key combination, or <tt>null</tt> if
     *         not found.
     */
    public String getValue(String section, String key)
    {
        Map<String, String> submap = contents.get(section);

        if (submap == null) {
            return null;
        }

        return submap.get(key);
    }

    /**
     * Get the value for the given section/key as an int.
     * 
     * @param section The section name.
     * @param key The key name.
     * @param dflt The value to return if the named value is missing or not an
     *            integer.
     * @return The named value, or <tt>dflt</tt> if that is not possible.
     */
    public Integer getIntValue(String section, String key, Integer dflt)
    {
        String value = getValue(section, key);

        if (value == null) {
            return dflt;
        }

        try {
            return Integer.parseInt(value);
        }
        catch (NumberFormatException nfe) {
            return dflt;
        }
    }

    /**
     * Get the value for the given section/key as a long.
     * 
     * @param section The section name.
     * @param key The key name.
     * @param dflt The value to return if the named value is missing or not a
     *            long.
     * @return The named value, or <tt>dflt</tt> if that is not possible.
     */
    public Long getLongValue(String section, String key, Long dflt)
    {
        String value = getValue(section, key);

        if (value == null) {
            return dflt;
        }

        try {
            return Long.parseLong(value);
        }
        catch (NumberFormatException nfe) {
            return dflt;
        }
    }

    /**
     * Get the value for the given section/key as a float.
     * 
     * @param section The section name.
     * @param key The key name.
     * @param dflt The value to return if the named value is missing or not a
     *            float.
     * @return The named value, or <tt>dflt</tt> if that is not possible.
     */
    public Float getFloatValue(String section, String key, Float dflt)
    {
        String value = getValue(section, key);

        if (value == null) {
            return dflt;
        }

        try {
            return Float.parseFloat(value);
        }
        catch (NumberFormatException nfe) {
            return dflt;
        }
    }

    /**
     * Get the value for the given section/key as a double.
     * 
     * @param section The section name.
     * @param key The key name.
     * @param dflt The value to return if the named value is missing or not a
     *            double.
     * @return The named value, or <tt>dflt</tt> if that is not possible.
     */
    public Double getDoubleValue(String section, String key, Double dflt)
    {
        String value = getValue(section, key);

        if (value == null) {
            return dflt;
        }

        try {
            return Double.parseDouble(value);
        }
        catch (NumberFormatException nfe) {
            return dflt;
        }
    }

    /**
     * Get the value for the given section/key as a boolean.
     * 
     * @param section The section name.
     * @param key The key name.
     * @param dflt The value to return if the named value is missing or not a
     *            boolean.
     * @return The named value, or <tt>dflt</tt> if that is not possible.
     */
    public Boolean getBooleanValue(String section, String key, Boolean dflt)
    {
        String value = getValue(section, key);

        if (value == null) {
            return dflt;
        }

        // Unfortunately, Boolean.parseBoolean does not throw a
        // NumberFormatException
        // when its input does not match 'true' or 'false', so we need to do
        // this manually
        if (value.equalsIgnoreCase("true")) {
            return true;
        }
        if (value.equalsIgnoreCase("false")) {
            return false;
        }

        return dflt;
    }

    /**
     * Set a value for the given section/key.
     * 
     * @param section The section name.
     * @param key The key name.
     * @param value The value for the section/key combination.
     */
    public void setValue(String section, String key, String value)
    {
        Map<String, String> submap = contents.get(section);

        if (submap == null) {
            submap = new LinkedHashMap<String, String>();
            contents.put(section, submap);
        }

        submap.put(key, value);
    }

    /**
     * Remove a section.
     * 
     * @param section The section name.
     */
    public void removeSection(String section)
    {
        contents.remove(section);
    }

    /**
     * Remove a key/value pair from a section.
     * 
     * @param section The section name.
     * @param key The key name.
     */
    public void removeValue(String section, String key)
    {
        Map<String, String> submap = contents.get(section);

        if (submap == null) {
            return;
        }

        submap.remove(key);
    }

    /**
     * Write the data to an output stream.
     * 
     * @param to The stream to write to.
     * @throws IOException If the stream throws one.
     */
    public void write(OutputStream to)
        throws IOException
    {
        PrintStream out = new PrintStream(to);

        for (Map.Entry<String, Map<String, String>> sectionEntry : contents
            .entrySet()) {
            out.printf("[%s]\n", sectionEntry.getKey());

            for (Map.Entry<String, String> valueEntry : sectionEntry.getValue()
                .entrySet()) {
                out.printf("%s=%s\n", valueEntry.getKey(), valueEntry
                    .getValue());
            }

            // Extra newline between sections
            out.println();
        }

        out.flush();
    }

    /**
     * Get a byte array containing the output of a call to
     * {@link #write(OutputStream)}.
     */
    public byte[] getBytes()
    {
        // This should never actually throw an exception
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            write(out);
            return out.toByteArray();
        }
        catch (IOException ioe) {
            throw new RuntimeException("Unexpected IOException", ioe);
        }
    }
}

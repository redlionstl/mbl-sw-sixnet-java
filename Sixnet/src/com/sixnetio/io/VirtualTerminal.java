/*
 * VirtualTerminal.java
 *
 * Provides functions for interfacing with a virtual terminal, mostly for formatting.
 *
 * Jonathan Pearson
 * October 28, 2008
 *
 */

package com.sixnetio.io;

import java.io.PrintStream;

public class VirtualTerminal {
	// Display attributes for text
	public static enum DisplayAttribute {
		ResetAll(0),
		Bright(1),
		Dim(2),
		Underscore(4),
		Blink(5),
		Reverse(7),
		Hidden(8),
		
		FGBlack(30),
		FGRed(31),
		FGGreen(32),
		FGYellow(33),
		FGBlue(34),
		FGMagenta(35),
		FGCyan(36),
		FGWhite(37),
		
		BGBlack(40),
		BGRed(41),
		BGGreen(42),
		BGYellow(43),
		BGBlue(44),
		BGMagenta(45),
		BGCyan(46),
		BGWhite(47);
		
		protected final int number;
		
		private DisplayAttribute(int number) {
			this.number = number;
		}
	}
	
	public static void setAttributes(PrintStream term, DisplayAttribute... attrs) {
		// Put together an output string
		String output = "";
		output += (char)27;
		output += '[';
		
		for (int i = 0; i < attrs.length; i++) {
			if (i > 0) output += ';';
			output += attrs[i].number;
		}
		
		output += 'm';
		
		term.print(output);
	}
}

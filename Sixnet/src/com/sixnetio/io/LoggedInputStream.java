package com.sixnetio.io;

import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * An input stream that logs the data that flows through it. Intended mainly for
 * debugging.
 */
public class LoggedInputStream
    extends FilterInputStream
{
    private static final Logger log = Logger.getLogger(Utils.thisClassName());

    private byte[] buffer = new byte[4096];
    private final List<byte[]> backlog = new LinkedList<byte[]>();

    private int pos;
    private int recorded;

    /**
     * Construct a new LoggedInputStream.
     * 
     * @param in The input stream whose data to log.
     */
    public LoggedInputStream(InputStream in)
    {
        super(in);

        pos = recorded = 0;
    }

    @Override
    public synchronized int read()
        throws IOException
    {
        int val = super.read();
        if (val != -1) {
            buffer[pos++] = (byte)val;
            recorded++;

            if (recorded < 0) {
                // Too much data
                // Clear it all out and log a warning
                pos = recorded = 0;
                backlog.clear();
                log
                    .warn("LoggedInputStream at capacity, discarding saved data");
            }
            else if (pos >= buffer.length) {
                backlog.add(buffer);
                buffer = new byte[buffer.length];
                pos = 0;
            }
        }

        return val;
    }

    @Override
    public synchronized int read(byte[] buf, int off, int len)
        throws IOException
    {
        int count = super.read(buf, off, len);
        if (count > 0) {
            int myoff = 0;
            while (myoff < count) {
                final int amount = Math.min(count - myoff, buffer.length - pos);
                System.arraycopy(buf, myoff, buffer, pos, amount);
                pos += amount;
                recorded += amount;
                myoff += amount;

                if (recorded < 0) {
                    // Clear it all out and log a warning
                    pos = recorded = 0;
                    backlog.clear();
                    log
                        .warn("LoggedInputStream at capacity, discarding saved data");
                }
                else if (pos >= buffer.length) {
                    backlog.add(buffer);
                    buffer = new byte[buffer.length];
                    pos = 0;
                }
            }
        }

        return count;
    }

    /** Clear all logged data. */
    public synchronized void clear()
    {
        pos = recorded = 0;
        backlog.clear();
    }

    /**
     * Get the number of recorded bytes of data.
     * 
     * @return The number of recorded bytes of data.
     */
    public int size()
    {
        return recorded;
    }

    /**
     * Get the recorded bytes of data.
     * 
     * @return All recorded bytes of data.
     */
    public synchronized byte[] getBytes()
    {
        byte[] result = new byte[recorded];
        int off = 0;
        for (byte[] block : backlog) {
            System.arraycopy(block, 0, result, off, block.length);
            off += block.length;
        }

        System.arraycopy(buffer, 0, result, off, pos);

        return result;
    }

    /**
     * Get a slice of the recorded bytes.
     * 
     * @param buf The array to store the bytes to. Bytes will be copied starting
     *            at position 0. Must be large enough to store the requested
     *            number of bytes.
     * @param off The starting byte in the record to copy.
     * @param len The number of bytes of the record to copy.
     * @return The actual number of bytes copied (may be less than requested if
     *         there were not enough bytes available).
     */
    public synchronized int getBytes(byte[] buf, int off, int len)
    {
        if (len > recorded) {
            len = recorded;
        }

        Iterator<byte[]> it = backlog.iterator();
        byte[] block = null;
        int total = 0;

        // Skip forward until we hit the proper starting byte
        while (off > 0 && it.hasNext()) {
            block = it.next();
            off -= block.length;
        }

        // Copy the desired portion of the last skipped block
        if (block != null) {
            int count = Math.min(-off, len);
            System.arraycopy(block, block.length + off, buf, 0, count);
            off = 0;
            total += count;
        }

        // Copy each desired subsequent block
        while (total < len && it.hasNext()) {
            block = it.next();
            int count = Math.min(block.length, len - total);
            System.arraycopy(block, 0, buf, total, count);
            total += count;
        }

        // Copy any remaining bytes from the current buffer
        if (total < len) {
            int count = Math.min(pos, len - total);
            System.arraycopy(buffer, 0, buf, total, count);
            total += count;
        }

        return total;
    }
}

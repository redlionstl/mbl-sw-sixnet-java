/*
 * PasswordReader.java
 *
 * Reads a password from System.in, replacing every
 * character read with a user-defined character.
 *
 * Jonathan Pearson
 * February 13, 2009
 *
 */

package com.sixnetio.io;

import java.io.IOError;
import java.io.IOException;

import com.sixnetio.util.InterruptedOperationException;

public class PasswordReader {
	private char replacement = '*';
	private boolean running = false;
	
	/**
	 * Construct a new password reader.
	 */
	public PasswordReader() {
		
	}
	
	/**
	 * Change the replacement character for reading a password. The default is '*'.
	 * @param val The new replacement character.
	 */
	public void setReplacementChar(char val) {
		replacement = val;
	}
	
	/**
	 * Get the current replacement character.
	 */
	public char getReplacementChar() {
		return replacement;
	}
	
	/**
	 * Read a password without printing a prompt.
	 * @return The password read.
	 */
	public String readPassword() {
		return readPassword("");
	}
	
	/**
	 * Read a password immediately after printing the given prompt.
	 * @param fmt A format string.
	 * @param args Arguments to the format string.
	 * @return The password read.
	 */
	public String readPassword(String fmt, Object... args) {
		System.out.printf(fmt, args);
		
		Thread th = new Thread() {
			public void run() {
				setName("Password Reader");
				while (running) {
					System.out.printf("%c*", (char)8);
				}
			}
		};
		
		running = true;
		th.start();
		
		String passwd;
		
		try {
    		UnbufferedInputStream uis = new UnbufferedInputStream(System.in);
    		passwd = uis.readLine();
		} catch (IOException ioe) {
			// This should not happen
			throw new IOError(ioe);
		}
		
		running = false;
		
		try {
			th.join();
		} catch (InterruptedException ie) {
			throw new InterruptedOperationException(ie);
		}
		
		return passwd;
	}
}

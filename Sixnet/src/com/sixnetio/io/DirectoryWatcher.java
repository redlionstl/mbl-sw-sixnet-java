/*
 * DirectoryWatcher.java
 *
 * Watches a given directory for new/deleted/changed files matching a given pattern.
 * Alerts using an interface callback.
 * Allows for a simple one-time scan without starting a new thread.
 *
 * Jonathan Pearson
 * April 7, 2009
 *
 */

package com.sixnetio.io;

import java.io.File;
import java.io.FileFilter;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Watch a given directory for new or deleted files. Alerts are passed back
 * through an interface. If using this in threaded mode (as opposed to single
 * scan mode), I suggest starting it as a daemon thread.
 * 
 * To use this class in single-scan mode, call {@link #scanForMatches()} to get
 * a set of files found in the directory. The call {@link #findNewFiles(Set)}
 * and {@link #findDeletedFiles(Set)} to determine what changed from the last
 * scan. Finally, call {@link #updateFileCache(Set)} to update the cache so the
 * correct results will be returned next time you perform a scan.
 * 
 * To use this class in threaded mode, call
 * {@link #addDirectoryChangeListener(DirectoryChangeListener)} to register a
 * listener for directory change events BEFORE calling start(). Otherwise, you
 * will miss the events fired for the files that existed in the directory before
 * starting up. You will probably want to run the thread as a daemon, so you
 * should call setDaemon(true) before calling start(). Use
 * {@link #setScanInterval(long)} to change the scan interval away from
 * {@link #SCAN_INTERVAL} at any time, and {@link #stopThread()} to signal the
 * thread to stop (a non-blocking operation; use join() afterwards to block until
 * the thread dies).
 * 
 * @author Jonathan Pearson
 */
public class DirectoryWatcher extends Thread {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Objects interested in directory contents changes should implement this
	 * interface and add themselves as listeners of a DirectoryWatcher.
	 * 
	 * @author Jonathan Pearson
	 */
	public static interface DirectoryChangeListener {
		/**
		 * Called when the directory being scanned changed somehow.
		 * 
		 * @param e The event representing the change.
		 */
		public void directoryChanged(DirectoryChangeEvent e);
		
		/**
		 * Called when a scan just finished for the directory being watched.
		 * 
		 * @param watcher The DirectoryWatcher that performed the scan.
		 */
		public void directoryScanned(DirectoryWatcher watcher);
	}
	
	/**
	 * Types of directory change events.
	 *
	 * @author Jonathan Pearson
	 */
	public static enum DirectoryChangeType {
		/** A file was created. */
		FileCreated,
		
		/** A file was changed. */
		FileChanged,
		
		/** A file was deleted. */
		FileDeleted;
	}
	
	/**
	 * The event passed to DirectoryChangeListeners when a change is detected.
	 *
	 * @author Jonathan Pearson
	 */
	public static class DirectoryChangeEvent {
		private DirectoryWatcher source;
		private File changedFile;
		private DirectoryChangeType changeType;
		
		/**
		 * Construct a new directory change event.
		 * 
		 * @param source The DirectoryWatcher that generated the event.
		 * @param changedFile The file that changed.
		 * @param changeType The type of change.
		 */
		protected DirectoryChangeEvent(DirectoryWatcher source,
				File changedFile, DirectoryChangeType changeType) {
			
			this.source = source;
			this.changedFile = changedFile;
			this.changeType = changeType;
		}
		
		/**
		 * Get the DirectoryWatcher that generated this event.
		 */
		public DirectoryWatcher getSource() {
        	return source;
        }
		
		/**
		 * Get the file whose change caused this event.
		 */
		public File getChangedFile() {
        	return changedFile;
        }
		
		/**
		 * Get the type of change.
		 */
		public DirectoryChangeType getChangeType() {
        	return changeType;
        }
	}
	
	/**
	 * Represents a file in the directory being watched.
	 *
	 * @author Jonathan Pearson
	 */
	public static class FileEntry {
		public final File file;
		public final Date modifiedTime;
		
		/**
		 * Construct a new FileEntry object.
		 * 
		 * @param file The file to represent.
		 */
		protected FileEntry(File file) {
			this.file = file;
			this.modifiedTime = new Date(file.lastModified());
		}
		
		/**
		 * Compare two FileEntry objects for equality. If the object is a
		 *   FileEntry, this just compares the two file objects for
		 *   equality; otherwise, it returns false.
		 * 
		 * @param obj The object to check for equality.
		 * @return True = The object is a FileEntry and points to the same
		 *   file as this FileEntry. False otherwise.
		 */
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof FileEntry) {
				FileEntry entry = (FileEntry)obj;
				
				if (this.file.equals(entry.file)) return true;
			}
			
			return false;
		}
		
		/**
		 * Get a hash code for this object.
		 * 
		 * @return The hash code of the file object.
		 */
		@Override
		public int hashCode() {
			return file.hashCode();
		}
	}
	
	/** Milliseconds between directory scans (default). */
	public static final long SCAN_INTERVAL = 5000;
	
	private File directory;
	private String pattern;
	
	private Set<FileEntry> fileCache;
	private FileFilter filter;
	
	private long scanInterval = SCAN_INTERVAL;
	
	private List<DirectoryChangeListener> directoryChangeListeners =
	        new LinkedList<DirectoryChangeListener>();
	
	private boolean running = false;
	
	/**
	 * Set up the directory watcher to watch for files matching the given
	 * pattern in the given directory. This only watches files, not
	 * subdirectories or files in subdirectories.
	 * 
	 * @param directory The directory to watch.
	 * @param pattern A regular expression representing the file name
	 *   pattern to match on.
	 */
	public DirectoryWatcher(File directory, String pattern) {
		super("Directory Watcher");
		
		this.directory = directory;
		this.pattern = pattern;
		
		// Empty for now because we haven't scanned it yet, so everything is new
		this.fileCache = new HashSet<FileEntry>();
		
		this.filter = new FileFilter() {
			@Override
			public boolean accept(File file) {
				if (file.isFile()) {
					if (file.getName().matches(DirectoryWatcher.this.pattern)) {
						return true;
					}
				}
				
				return false;
			}
		};
	}
	
	/**
	 * Update the file cache.
	 * 
	 * @param newCache The new file cache. Make sure that you have
	 *   exclusive use of this object.
	 */
	public void updateFileCache(Set<FileEntry> newCache) {
		synchronized (fileCache) {
			fileCache.clear();
			fileCache.addAll(newCache);
		}
	}
	
	/**
	 * Scan the directory for all files matching the pattern.
	 * 
	 * @return A set of matching files.
	 */
	public Set<FileEntry> scanForMatches() {
		File[] files = directory.listFiles(filter);
		Set<FileEntry> matches = new HashSet<FileEntry>();
		
		for (File file : files) {
			matches.add(new FileEntry(file));
		}
		
		return matches;
	}
	
	/**
	 * Given a directory scan, compare to the cache and return files created
	 * since the last update.
	 * 
	 * @param foundFiles The result of a directory scan. Make sure that you
	 *   have exclusive use of this object.
	 * @return A set of files that exist in the scan but not in the cache.
	 */
	public Set<FileEntry> findNewFiles(Set<FileEntry> foundFiles) {
		// Copy it so we can modify without side effects
		Set<FileEntry> addedFiles = new HashSet<FileEntry>(foundFiles);
		
		synchronized (fileCache) {
	        // Subtract the files we expect from the files found in the directory
	        // This is the set of new files
	        addedFiles.removeAll(fileCache);
        }
		
		return addedFiles;
	}
	
	/**
	 * Given a directory scan, compare to the cache and return files deleted
	 * since the last update.
	 * 
	 * @param foundFiles The result of a directory scan. Make sure that you
	 *   have exclusive use of this object.
	 * @return A set of files that exist in the cache but not in the scan.
	 */
	public Set<FileEntry> findDeletedFiles(Set<FileEntry> foundFiles) {
		// Copy the set of expected files so we can modify without side effects
		Set<FileEntry> removedFiles;
		synchronized (fileCache) {
	        removedFiles = new HashSet<FileEntry>(fileCache);
        }
		
		// Subtract files currently in the directory from what we expect
		// This is the set of deleted files
		removedFiles.removeAll(foundFiles);
		
		return removedFiles;
	}
	
	/**
	 * Given a directory scan, compare to the cache and return files that
	 * have changed since the last update.
	 * 
	 * @param foundFiles The result of a directory scan. Make sure that you
	 *   have exclusive use of this object.
	 * @return A set of files that have different modification times between
	 *   the cache and the directory scan.
	 */
	public Set<FileEntry> findChangedFiles(Set<FileEntry> foundFiles) {
		Set<FileEntry> changedFiles = new HashSet<FileEntry>();
		
		// Convert the cache into a map of FileEntry --> Modified time
		Map<FileEntry, Date> modifiedTimes = new Hashtable<FileEntry, Date>();
		synchronized (fileCache) {
	        for (FileEntry cacheFile : fileCache) {
		        modifiedTimes.put(cacheFile, cacheFile.modifiedTime);
	        }
        }
		
		// Scan through the found files, checking the map to see if the
		//   modified times are different
		for (FileEntry scanFile : foundFiles) {
			Date modifiedTime = modifiedTimes.get(scanFile);
			
			if (modifiedTime != null && !modifiedTime.equals(scanFile.modifiedTime)) {
				changedFiles.add(scanFile);
			}
		}
		
		return changedFiles;
	}
	
	/**
	 * Get the scan interval (in milliseconds) used when in threaded mode.
	 * 
	 * @return The interval at which directory scans are performed and compared.
	 */
	public long getScanInterval() {
		return scanInterval;
	}
	
	/**
	 * Set the scan interval (in milliseconds) used when in threaded mode.
	 * 
	 * @param val The new scan interval, in milliseconds. Must be greater than
	 *   zero (not checked).
	 */
	public void setScanInterval(long val) {
		scanInterval = val;
	}
	
	/**
	 * Signals the running thread to stop. Does not wait for the thread to die,
	 * you need to call join() for that.
	 */
	public void stopThread() {
		running = false;
	}
	
	/**
	 * Add a listener of directory change events.
	 * 
	 * @param listener The listener to add. No object may be added more than
	 *            once; subsequent additions are ignored.
	 */
	public void addDirectoryChangeListener(DirectoryChangeListener listener) {
		synchronized (directoryChangeListeners) {
    		if (!directoryChangeListeners.contains(listener)) {
    			directoryChangeListeners.add(listener);
    		}
		}
	}
	
	/**
	 * Remove a listener of directory change events.
	 * 
	 * @param listener The listener to remove.
	 */
	public void removeDirectoryChangeListener(DirectoryChangeListener listener) {
		synchronized (directoryChangeListeners) {
			directoryChangeListeners.remove(listener);
		}
	}
	
	/**
	 * Used internally to notify listeners that the directory changed somehow.
	 * 
	 * @param f The file that caused the change.
	 * @param eventType The type of event.
	 */
	protected void fireDirectoryChanged(File f, DirectoryChangeType eventType) {
		List<DirectoryChangeListener> listeners;
		synchronized (directoryChangeListeners) {
			listeners = new LinkedList<DirectoryChangeListener>(directoryChangeListeners);
		}
		
		for (DirectoryChangeListener listener : listeners) {
			try {
				listener.directoryChanged(new DirectoryChangeEvent(this, f, eventType));
			} catch (Throwable th) {
				logger.error("Error calling DirectoryChangeListener.directoryChanged", th);
			}
		}
	}
	
	/**
	 * Used internally to notify listeners that a directory scan just completed.
	 */
	protected void fireDirectoryScanned() {
		List<DirectoryChangeListener> listeners;
		synchronized (directoryChangeListeners) {
			listeners = new LinkedList<DirectoryChangeListener>(directoryChangeListeners);
		}
		
		for (DirectoryChangeListener listener : listeners) {
			try {
				listener.directoryScanned(this);
			} catch (Throwable th) {
				logger.error(th);
			}
		}
	}
	
	/**
	 * Public only as an implementation detail, you should not call this.
	 */
	@Override
	public void run() {
		running = true;
		
		try {
			while (running) {
				Set<FileEntry> scan = scanForMatches();
				
				// Scan for created files and fire off events for them
				for (FileEntry entry : findNewFiles(scan)) {
					fireDirectoryChanged(entry.file, DirectoryChangeType.FileCreated);
				}
				
				// Scan for deleted files and fire off events for them
				for (FileEntry entry : findDeletedFiles(scan)) {
					fireDirectoryChanged(entry.file, DirectoryChangeType.FileDeleted);
				}
				
				// Scan for changed files and fire off events for them
				for (FileEntry entry : findChangedFiles(scan)) {
					fireDirectoryChanged(entry.file, DirectoryChangeType.FileChanged);
				}
				
				updateFileCache(scan);
				
				fireDirectoryScanned();
				
				Utils.sleep(getScanInterval());
			}
		} finally {
			running = false;
		}
	}
}

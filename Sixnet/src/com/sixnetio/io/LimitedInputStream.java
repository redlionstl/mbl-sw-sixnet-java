/*
 * LimitedInputStream.java
 *
 * An InputStream that allows the user to place a limit on the number of
 * bytes that may be read.
 *
 * Jonathan Pearson
 * April 13, 2010
 *
 */

package com.sixnetio.io;

import java.io.*;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * A DataInputStream that allows the user to place a limit on the number of
 * bytes that may be read.
 * 
 * @author Jonathan Pearson
 */
public class LimitedInputStream
    extends FilterInputStream
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    private long bytesRead = 0;
    private long limit = -1;

    /**
     * Construct a new LimitedDataInputStream with no limit. It will immediately
     * start counting bytes.
     * 
     * @param in The InputStream to read from.
     */
    public LimitedInputStream(InputStream in)
    {
        super(in);
    }

    /**
     * Set the number of bytes that may be read before we start return EOF.
     * 
     * @param val The number of bytes (from construction or the last call to
     *            {@link #resetCount()}) that may be read before we start to
     *            return EOF. If -1, there is no limit placed on the stream.
     */
    public void setLimit(long val)
    {
        limit = val;
    }

    /**
     * Get the number of bytes that may be read before we start return EOF.
     * 
     * @return The number of bytes (from construction or the last call to
     *         {@link #resetCount()}) that may be read before we start to return
     *         EOF. If -1, there is no limit placed on the stream.
     */
    public long getLimit()
    {
        return limit;
    }

    /**
     * Reset the count of the number of bytes read.
     */
    public void resetCount()
    {
        bytesRead = 0;
    }

    /**
     * Get the number of bytes read since construction or the last call to
     * {@link #resetCount()}.
     */
    public long getByteCount()
    {
        return bytesRead;
    }

    /**
     * Read and discard data from the stream until the byte counter reaches the
     * limit. If the limit is not set (-1), this will do nothing.
     */
    public void discardToCompletion()
        throws IOException
    {
        if (limit == -1) {
            // Nothing to do
            logger.warn("Tried to discard end of an unlimited stream");

            return;
        }

        byte[] block = new byte[4096];
        while (bytesRead < limit) {
            if (read(block) == -1) {
                // EOF is close enough
                break;
            }
        }
    }

    @Override
    public int read()
        throws IOException
    {
        if (limit != -1 && bytesRead >= limit) {
            // No data left, it would put us over the limit
            return -1;
        }

        int b = super.read();
        bytesRead++; // The read() may throw an exception, skipping this

        return b;
    }

    @Override
    public int read(byte[] into)
        throws IOException
    {
        return read(into, 0, into.length);
    }

    @Override
    public int read(byte[] into, int off, int len)
        throws IOException
    {
        if (limit != -1) {
            if (len > limit - bytesRead) {
                // Do not try to read more than is available
                // Cast is safe because the value being casted is definitely
                // smaller than the value in an int variable
                len = (int)(limit - bytesRead);
            }
        }

        int l = super.read(into, off, len);
        if (l > -1) {
            bytesRead += l;
        }

        return l;
    }
}

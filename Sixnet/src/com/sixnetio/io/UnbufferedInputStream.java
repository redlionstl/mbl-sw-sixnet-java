/*
 * UnbufferedInputStream.java
 *
 * Extends the standard InputStream class with a readLine function, but does not buffer input.
 * \r characters are completely ignored, and \n characters designate new lines, but are not
 * returned in the output.
 *
 * Jonathan Pearson
 * April 9, 2007
 *
 */

package com.sixnetio.io;

import java.io.*;

public class UnbufferedInputStream extends FilterInputStream {
	private boolean lineEndsWithData; // Whether to return from readLine() when the stream reports nothing more available
	
	public UnbufferedInputStream(InputStream from) {
		super(from);
	}
	
	public int read() throws IOException {
		return super.read();
	}
	
	public void setLineEndsWithData(boolean val) {
		lineEndsWithData = val;
	}
	
	public boolean getLineEndsWithData() {
		return lineEndsWithData;
	}
	
	public String readLine() throws IOException {
		String ans = "";
		int ch;
		
		while ((lineEndsWithData ? super.available() > 0 : true) &&
			   (ch = super.read()) != -1) {
			
			if (ch == '\r') {
				continue;
			} else if (ch == '\n') {
				return ans;
			} else {
				ans += (char)ch;
			}
		}
		
		return ans;
	}
}

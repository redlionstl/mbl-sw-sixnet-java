/*
 * DelineatedInputStream.java
 *
 * Part of a hack to enable password-protected project files stored in XML. You specify how many fields
 * there are, and the byte that separates them, and this will return EOF until you reset it. Once it
 * begins reading the last field, it no longer looks for the byte, and simply acts like a normal stream.
 *
 * Jonathan Pearson
 * September 24, 2007
 *
 */

package com.sixnetio.io;

import java.io.*;

/**
 * Filters an input stream so that when a specific character is read, this acts
 * like EOF was hit until {@link #newField()} is called. Only allows for a
 * specific number of fields (separated by the separator char).
 *
 * @author Jonathan Pearson
 */
public class DelineatedInputStream extends FilterInputStream {
	private int sep;
	private int count;
	private boolean eof = false;
	
	/**
	 * Construct a new DelineatedInputStream.
	 * 
	 * @param wrap The input stream to read from.
	 * @param separator The separator character (0xff is usually good).
	 * @param fieldCount The number of fields to expect.
	 */
	public DelineatedInputStream(InputStream wrap, int separator, int fieldCount) {
		super(wrap);
		
		sep = separator;
		count = fieldCount - 1;
	}
	
	/**
	 * Read a byte from the stream.
	 * 
	 * @return The byte read, or -1 on EOF (or end of field, until {@link #newField()}
	 * is called.
	 * @throws IOException If there was a problem reading from the underlying stream.
	 */
	public int read() throws IOException {
		if (eof) {
			return -1;
		}
		
		if (count > 0) {
			int b = super.read();
			
			if (b == sep) {
				eof = true;
				count--;
				return -1;
			} else {
				return b;
			}
		} else {
			int b = super.read();
			return b;
		}
	}
	
	/**
	 * Move on to the next field after hitting the end of the previous one.
	 */
	public void newField() {
		eof = false;
	}
}

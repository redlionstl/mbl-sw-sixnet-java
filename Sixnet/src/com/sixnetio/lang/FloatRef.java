/*
 * FloatRef.java
 *
 * Provides a mutable reference to a float, much like java.lang.Float
 * provides an immutable reference.
 *
 * Jonathan Pearson
 * June 8, 2010
 *
 */

package com.sixnetio.lang;


/**
 * Provides a mutable Float-like object.
 *
 * @author Jonathan Pearson
 */
public class FloatRef
	extends Number
	implements Comparable<Number>
{
	/** Default tolerance to use for equality comparisons. */
	public static final float TOLERANCE = (float)0.00001;
	
	protected float value;
	
	/**
	 * Construct a new FloatRef initialized to 0.
	 */
	public FloatRef()
	{
		this.value = 0;
	}
	
	/**
	 * Construct a new FloatRef.
	 * 
	 * @param value The initial value for the object.
	 */
	public FloatRef(float value)
	{
		this.value = value;
	}
	
	/** Set the value of this object. */
	public void set(float value)
	{
		this.value = value;
	}
	
	@Override
	public double doubleValue()
	{
		return value;
	}
	
	@Override
	public float floatValue()
	{
		return value;
	}
	
	@Override
	public int intValue()
	{
		return (int)value;
	}
	
	@Override
	public long longValue()
	{
		return (long)value;
	}
	
	@Override
	public int compareTo(Number o)
	{
		float c = floatValue() - o.floatValue();
		
		if (c > 0) {
			return 1;
		}
		else if (c < 0) {
			return -1;
		}
		else {
			return 0;
		}
	}
	
	/**
	 * This function should not be used. Instead, use
	 * {@link #equals(Number, double)}.
	 */
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Number) {
			return equals((Number)o, 0);
		}
		else {
			return false;
		}
	}
	
	/**
	 * Compare this object with another number, testing for near-equality.
	 * 
	 * @param n The number compare to.
	 * @param tolerance How close the numbers must be to be considered equal. A
	 *   good start would be {@link #TOLERANCE}.
	 * @return True if the two numbers are no further from each other than
	 *   <code>tolerance</code>, false otherwise.
	 */
	public boolean equals(Number n, float tolerance)
	{
		float f1 = floatValue();
		float f2 = n.floatValue();
		return (Math.abs(f1 - f2) <= tolerance);
	}
	
	/**
	 * Do not use this object as a key; this function is only provided for
	 * completeness.
	 */
	@Override
	public int hashCode()
	{
		return (int)Math.floor(doubleValue());
	}
}

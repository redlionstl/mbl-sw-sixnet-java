/*
 * DoubleRef.java
 *
 * Provides a mutable reference to a double, much like java.lang.Double
 * provides an immutable reference.
 *
 * Jonathan Pearson
 * June 8, 2010
 *
 */

package com.sixnetio.lang;


/**
 * Provides a mutable Double-like object.
 *
 * @author Jonathan Pearson
 */
public class DoubleRef
	extends Number
	implements Comparable<Number>
{
	/** Default tolerance to use for equality comparisons. */
	public static final double TOLERANCE = 0.00001;
	
	protected double value;
	
	/**
	 * Construct a new DoubleRef initialized to 0.
	 */
	public DoubleRef()
	{
		this.value = 0;
	}
	
	/**
	 * Construct a new DoubleRef.
	 * 
	 * @param value The initial value for the object.
	 */
	public DoubleRef(double value)
	{
		this.value = value;
	}
	
	/** Set the value of this object. */
	public void set(double value)
	{
		this.value = value;
	}
	
	@Override
	public double doubleValue()
	{
		return value;
	}
	
	@Override
	public float floatValue()
	{
		return (float)value;
	}
	
	@Override
	public int intValue()
	{
		return (int)value;
	}
	
	@Override
	public long longValue()
	{
		return (long)value;
	}
	
	@Override
	public int compareTo(Number o)
	{
		double c = doubleValue() - o.doubleValue();
		
		if (c > 0) {
			return 1;
		}
		else if (c < 0) {
			return -1;
		}
		else {
			return 0;
		}
	}
	
	/**
	 * This function should not be used. Instead, use
	 * {@link #equals(Number, double)}.
	 */
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Number) {
			return equals((Number)o, 0);
		}
		else {
			return false;
		}
	}
	
	/**
	 * Compare this object with another number, testing for near-equality.
	 * 
	 * @param n The number compare to.
	 * @param tolerance How close the numbers must be to be considered equal. A
	 *   good start would be {@link #TOLERANCE}.
	 * @return True if the two numbers are no further from each other than
	 *   <code>tolerance</code>, false otherwise.
	 */
	public boolean equals(Number n, double tolerance)
	{
		double f1 = doubleValue();
		double f2 = n.doubleValue();
		return (Math.abs(f1 - f2) <= tolerance);
	}
	
	/**
	 * Do not use this object as a key; this function is only provided for
	 * completeness.
	 */
	@Override
	public int hashCode()
	{
		return (int)Math.floor(doubleValue());
	}
}

/*
 * ShortRef.java
 *
 * Provides a mutable reference to a short, much like java.lang.Short provides
 * an immutable reference.
 *
 * Jonathan Pearson
 * June 8, 2010
 *
 */

package com.sixnetio.lang;

/**
 * Provides a mutable Short-like object.
 *
 * @author Jonathan Pearson
 */
public class ShortRef
	extends Number
	implements Comparable<Number>
{
	protected short value;
	
	/**
	 * Construct a new ShortRef initialized to 0.
	 */
	public ShortRef()
	{
		this.value = 0;
	}
	
	/**
	 * Construct a new ShortRef.
	 * 
	 * @param value The initial value for the object.
	 */
	public ShortRef(short value)
	{
		this.value = value;
	}
	
	/** Set the value of this object. */
	public void set(short value)
	{
		this.value = value;
	}
	
	@Override
	public double doubleValue()
	{
		return value;
	}
	
	@Override
	public float floatValue()
	{
		return value;
	}
	
	@Override
	public int intValue()
	{
		return (value & 0xffff); // Prevent sign extension
	}
	
	@Override
	public long longValue()
	{
		return (value & 0xffff);
	}
	
	@Override
	public int compareTo(Number o)
	{
		return (intValue() - o.intValue());
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Number) {
			return (intValue() == ((Number)o).intValue());
		}
		else {
			return false;
		}
	}
	
	/**
	 * Do not use this object as a key; this function is only provided for
	 * completeness.
	 */
	@Override
	public int hashCode()
	{
		return intValue();
	}
}

/*
 * ByteRef.java
 *
 * Provides a mutable reference to a byte, much like java.lang.Byte provides an
 * immutable reference.
 *
 * Jonathan Pearson
 * June 8, 2010
 *
 */

package com.sixnetio.lang;

/**
 * Provides a mutable Byte-like object.
 *
 * @author Jonathan Pearson
 */
public class ByteRef
	extends Number
	implements Comparable<Number>
{
	protected byte value;
	
	/**
	 * Construct a new ByteRef initialized to 0.
	 */
	public ByteRef()
	{
		this.value = 0;
	}
	
	/**
	 * Construct a new ByteRef.
	 * 
	 * @param value The initial value for the object.
	 */
	public ByteRef(byte value)
	{
		this.value = value;
	}
	
	/** Set the value of this object. */
	public void set(byte value)
	{
		this.value = value;
	}
	
	@Override
	public double doubleValue()
	{
		return value;
	}
	
	@Override
	public float floatValue()
	{
		return value;
	}
	
	@Override
	public int intValue()
	{
		return (value & 0xff); // Prevent sign extension
	}
	
	@Override
	public long longValue()
	{
		return (value & 0xff);
	}
	
	@Override
	public int compareTo(Number o)
	{
		return (intValue() - o.intValue());
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Number) {
			return (intValue() == ((Number)o).intValue());
		}
		else {
			return false;
		}
	}
	
	/**
	 * Do not use this object as a key; this function is only provided for
	 * completeness.
	 */
	@Override
	public int hashCode()
	{
		return intValue();
	}
}

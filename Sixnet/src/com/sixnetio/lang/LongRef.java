/*
 * LongRef.java
 *
 * Provides a mutable reference to a long, much like java.lang.Long provides an
 * immutable reference.
 *
 * Jonathan Pearson
 * June 8, 2010
 *
 */

package com.sixnetio.lang;

/**
 * Provides a mutable Long-like object.
 *
 * @author Jonathan Pearson
 */
public class LongRef
	extends Number
	implements Comparable<Number>
{
	protected long value;
	
	/**
	 * Construct a new LongRef initialized to 0.
	 */
	public LongRef()
	{
		this.value = 0;
	}
	
	/**
	 * Construct a new LongRef.
	 * 
	 * @param value The initial value for the object.
	 */
	public LongRef(long value)
	{
		this.value = value;
	}
	
	/** Set the value of this object. */
	public void set(long value)
	{
		this.value = value;
	}
	
	@Override
	public double doubleValue()
	{
		return value;
	}
	
	@Override
	public float floatValue()
	{
		return value;
	}
	
	@Override
	public int intValue()
	{
		return (int)(value & 0xffffffff);
	}
	
	@Override
	public long longValue()
	{
		return value;
	}
	
	@Override
	public int compareTo(Number o)
	{
		long c = longValue() - o.longValue();
		
		if (c > 0) {
			return 1;
		}
		else if (c < 0) {
			return -1;
		}
		else {
			return 0;
		}
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Number) {
			return (longValue() == ((Number)o).longValue());
		}
		else {
			return false;
		}
	}
	
	/**
	 * Do not use this object as a key; this function is only provided for
	 * completeness.
	 */
	@Override
	public int hashCode()
	{
		return intValue();
	}
}

/*
 * IntRef.java
 *
 * Provides a mutable reference to an integer, much like java.lang.Integer
 * provides an immutable reference.
 *
 * Jonathan Pearson
 * June 8, 2010
 *
 */

package com.sixnetio.lang;

/**
 * Provides a mutable Integer-like object.
 *
 * @author Jonathan Pearson
 */
public class IntRef
	extends Number
	implements Comparable<Number>
{
	protected int value;
	
	/**
	 * Construct a new IntRef initialized to 0.
	 */
	public IntRef()
	{
		this.value = 0;
	}
	
	/**
	 * Construct a new IntRef.
	 * 
	 * @param value The initial value for the object.
	 */
	public IntRef(int value)
	{
		this.value = value;
	}
	
	/** Set the value of this object. */
	public void set(int value)
	{
		this.value = value;
	}
	
	@Override
	public double doubleValue()
	{
		return value;
	}
	
	@Override
	public float floatValue()
	{
		return value;
	}
	
	@Override
	public int intValue()
	{
		return value;
	}
	
	@Override
	public long longValue()
	{
		return (value & 0xffffffff); // Prevent sign extension
	}
	
	@Override
	public int compareTo(Number o)
	{
		return (intValue() - o.intValue());
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Number) {
			return (intValue() == ((Number)o).intValue());
		}
		else {
			return false;
		}
	}
	
	/**
	 * Do not use this object as a key; this function is only provided for
	 * completeness.
	 */
	@Override
	public int hashCode()
	{
		return intValue();
	}
}

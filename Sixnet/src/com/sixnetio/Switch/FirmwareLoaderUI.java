
/*
 * FirmwareLoaderUI.java
 *
 * An interface that any UI must implement to use the firmware loader.
 *
 * Jonathan Pearson
 * June 26, 2007
 *
 */

package com.sixnetio.Switch;

public interface FirmwareLoaderUI {
    // Ethernet-only load
    public static final int S_VERIFY_ETH_ONLY = 10,     // Make sure ethernet-only loading is enabled
        S_SET_FORCE = 20,       // Turn on ethernet-only loading, if forcing the load
        S_PROBE_SWITCH = 30,    // Probe the switch to determine architecture
        S_EXTRACT_FILES = 40,   // Extract files from the bundle
        S_DOWNLOAD_CONFIG = 50, // Download the current configuration
        S_UPGRADE_REQUEST = 60, // Tell the switch to reboot and download
        S_RESTORE_CONFIG = 80,  // Restore the saved configuration settings
        S_COMPLETE = 90;        // Load is complete

    // Ethernet + Serial load
    public static final int S_APPLY_POWER = 1010,       // Ask the user to apply power to the switch
        S_ARCH = 1020,          // Detecting the switch architecture
    //S_EXTRACT_FILES = 1030, // This is used, but it is already defined above
        S_SETUP = 1040,         // Setting up the switch to perform the upgrade
        S_LOAD_IMAGE = 1050,    // Loading the filesystem image
        S_FLASH_IMAGE = 1055,   // Move to NAND
        S_LOAD_BOOT_IMAGE = 1060,       // Loading the boot image
        S_FLASH_BOOT_IMAGE = 1065,      // Move to NAND
        S_WAIT = 1070,          // Waiting for the switch to come online
        S_LOAD_USER_IMAGE = 1085,       // Loading the boot image
        S_FLASH_USER_IMAGE = 1090,       // Loading the boot image
        S_CONFIG_IMAGE = 1095;
    //S_COMPLETE = 1080 // This is used, but it is already defined above

    /**
     * Called when there was an exception thrown, during either single- or multi-threaded execution.
     * @param e The exception that was thrown.
     * @param extraMessage Any extra information beyond what is contained in the exception.
     * @param mt True if the exception occurred during multi-threaded execution, false during single-threaded.
     */
    public void handleException(Exception e, String extraMessage,
                                boolean mt);

    /**
     * Called when the state of the load changes. Use the S_* values above to determine what is happening.
     * @param state The new state of the firmware load.
     */
    public void updateState(int state);

    /**
     * If using the internal server, this will be updated with TFTP progress as files are transferred.
     * 
     * @param percentage The percent complete (0.0-100.0). -100.0 will be sent when the transfer ends.
     */
    public void tftpProgress(float percentage);

    /**
     * What is the IP address of the TFTP server? It is okay to return null, that means auto-detect a local addr.
     * @return The IP address of the TFTP server, if specified, or null otherwise.
     */
    public String getTFTPAddr();

    /**
     * What is the directory to put files into if using an external TFTP server? If using the internal server,
     * return null.
     * @return The directory to put TFTP-shared files, or null to use the internal server.
     */
    public String getTFTPDir();

    /**
     * If the load fails, this will pass the SwitchConfig object containing the saved state; if no state
     * was saved, it will not be called.
     * @param state The saved switch configuration.
     */
    public void saveSwitchState(SwitchConfig state);
}

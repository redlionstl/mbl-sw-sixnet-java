
/*
 * FirmwareLoader.java
 *
 * The back-end for an ethernet-only firmware loader for Sixnet managed switches.
 *
 * Jonathan Pearson
 * June 21, 2007
 *
 */

package com.sixnetio.Switch;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import com.sixnetio.fs.generic.FSObject;
import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.fs.vfs.VFile;
import com.sixnetio.io.INIParser;
import com.sixnetio.server.TransferObserver;
import com.sixnetio.server.tftp.TFTP;
import com.sixnetio.util.*;

public class FirmwareLoader extends Thread {
    private static final Logger logger =
    Logger.getLogger(Utils.thisClassName());

    // Private data members
    private String swAddr;
    private String swSubnet;
    private String swGateway;
    private String userName;
    private String password;

    private FirmwareLoaderUI ui;
    private boolean ignoreVendor;
    private long responseTimeout = 0;

    private Thread loadThread;  // The thread that performs the firmware load
    private Exception interruptCause = null;    // If we interrupt the load thread internally, set this

    /**
     * Construct a new FirmwareLoader. This will enforce a vendor match between the firmware bundle
     * and the switch.
     * 
     * @param ui The UI that will be used to notify the user of status/errors.
     * @param swAddr The address of the switch.
     * @param swSubnet The subnet of the switch.
     * @param swGateway The gateway address of the switch.
     * @param userName The user name for logging into the Web UI.
     * @param passwd The password for logging into the Web UI.
     */
    public FirmwareLoader(FirmwareLoaderUI ui, String swAddr,
                          String swSubnet, String swGateway,
                          String userName, String passwd) {
        this(ui, swAddr, swSubnet, swGateway, userName, passwd, false);
    }

    /**
     * Construct a new FirmwareLoader.
     * 
     * @param ui The UI that will be used to notify the user of status/errors.
     * @param swAddr The address of the switch.
     * @param swSubnet The subnet of the switch.
     * @param swGateway The gateway address of the switch.
     * @param userName The user name for logging into the Web UI.
     * @param passwd The password for logging into the Web UI.
     * @param ignoreVendor Whether to ignore vendor mismatches between firmware and switch.
     */
    public FirmwareLoader(FirmwareLoaderUI ui, String swAddr,
                          String swSubnet,
                          String swGateway,
                          String userName, String passwd,
                          boolean ignoreVendor) {
        super("FirmwareLoader");

        if (ui == null)
            throw new
            IllegalArgumentException
            ("The FirmwareLoaderUI must be provided");

        this.ui = ui;

        setSwitchAddress(swAddr);
        setSwitchSubnet(swSubnet);
        setSwitchGateway(swGateway);

        setLoginInfo(userName, passwd);

        this.ignoreVendor = ignoreVendor;
    }

    /**
     * Set the user name/password for the Web UI.
     * 
     * @param user The user name.
     * @param pass The password.
     */
    public void setLoginInfo(String user, String pass) {
        if (user == null || user.length() == 0)
            throw new
            IllegalArgumentException
            ("Switch user name must have a value");
        if (pass == null || pass.length() == 0)
            throw new
            IllegalArgumentException
            ("Switch password must have a value");

        userName = user;
        password = pass;
    }

    /**
     * Get the user name for the Web UI.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Get the password for the Web UI.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Get the switch's address.
     */
    public String getSwitchAddress() {
        return swAddr;
    }

    /**
     * Get the switch's subnet mask.
     */
    public String getSwitchSubnet() {
        return swSubnet;
    }

    /**
     * Get the switch's gateway address.
     */
    public String getSwitchGateway() {
        return swGateway;
    }

    /**
     * Set the switch's address.
     * 
     * @param val The address of the switch.
     */
    public void setSwitchAddress(String val) {
        if (val == null || val.length() == 0)
            throw new
            IllegalArgumentException
            ("Switch address must have a value");

        swAddr = val;
    }

    /**
     * Set the switch's subnet mask.
     * 
     * @param val The subnet mask for the switch.
     */
    public void setSwitchSubnet(String val) {
        if (val == null || val.length() == 0)
            throw new
            IllegalArgumentException
            ("Switch subnet must have a value");

        swSubnet = val;
    }

    /**
     * Set the switch's gateway address.
     * 
     * @param val The gateway address for the switch.
     */
    public void setSwitchGateway(String val) {
        swGateway = val;
    }

    /**
     * Get the response timeout. This is how long to wait after finishing a load
     * before deciding that the switch is unresponsive.
     * 
     * @return The response timeout, in ms. 0 means wait forever.
     */
    public long getResponseTimeout() {
        return responseTimeout;
    }

    /**
     * Set the response timeout.
     * 
     * @param val The response timeout, in ms. 0 means wait forever, a negative
     * value will not wait at all.
     */
    public void setResponseTimeout(long val) {
        responseTimeout = val;
    }

    /**
     * Load firmware onto the switch. Block until finished. This
     * will restore configuration settings, but will not force the load.
     * 
     * @param bundle The firmware bundle to load onto the switch.
     */
    public void loadFirmware(FirmwareBundle bundle) {
        loadFirmware(bundle, true, false);
    }

    /**
     * Load firmware onto the switch. Block until finished.
     * 
     * @param bundle The firmware bundle to load onto the switch.
     * @param backupSettings Whether to back up/restore settings.
     */
    public void loadFirmware(FirmwareBundle bundle,
                             boolean backupSettings) {
        loadFirmware(bundle, backupSettings, false);
    }

    /**
     * Load firmware onto the switch. Block until finished.
     * 
     * @param bundle The firmware bundle to load onto the switch.
     * @param backupSettings Whether to back up/restore settings.
     * @param forceLoad Whether to force the load, if necessary.
     */
    public void loadFirmware(FirmwareBundle bundle,
                             boolean backupSettings,
                             boolean forceLoad) {
        if (bundle == null)
            throw new
            IllegalArgumentException
            ("The firmware bundle must be provided");

        this.firmware = bundle;
        this.backupSettings = backupSettings;
        this.forceLoad = forceLoad;
        this.multiThread = false;

        this.loadThread = Thread.currentThread();
        run();
    }

    /**
     * Spawn a new thread to load firmware onto the switch. This
     * will restore configuration settings, but will not force the load.
     * 
     * @param bundle The firmware bundle to load onto the switch.
     */
    public void loadFirmwareAsync(FirmwareBundle bundle) {
        loadFirmwareAsync(bundle, true, false);
    }

    /**
     * Spawn a new thread to load firmware onto the switch. This
     * will restore configuration settings, but will not force the load.
     * 
     * @param bundle The firmware bundle to load onto the switch.
     * @param backupSettings Whether to back up/restore settings.
     */
    public void loadFirmwareAsync(FirmwareBundle bundle,
                                  boolean backupSettings) {
        loadFirmwareAsync(bundle, backupSettings, false);
    }

    /**
     * Spawn a new thread to load firmware onto the switch. This
     * will restore configuration settings, but will not force the load.
     * 
     * @param bundle The firmware bundle to load onto the switch.
     * @param backupSettings Whether to back up/restore settings.
     * @param forceLoad Whether to force the load, if necessary.
     */
    public void loadFirmwareAsync(FirmwareBundle bundle,
                                  boolean backupSettings,
                                  boolean forceLoad) {
        if (bundle == null)
            throw new
            IllegalArgumentException
            ("The firmware bundle must be provided");

        this.firmware = bundle;
        this.backupSettings = backupSettings;
        this.forceLoad = forceLoad;
        this.multiThread = true;

        this.loadThread = new Thread(this);
        loadThread.start();
    }

    /**
     * Cancel the load by interrupting the thread.
     */
    public void cancel() {
        loadThread.interrupt();
    }

    // Some more private data members, set by the load functions
    private FirmwareBundle firmware;
    private boolean backupSettings;
    private boolean forceLoad;
    private boolean multiThread;

    // Access is synchronized on the FirmwareLoader object
    private List < File > filesToDelete;

    // These are set as the TFTP transfers finish
    private boolean imageDone;
    private boolean bootImageDone;
    private Object l_images = new Object();     // Signals when images finish transferring

    private Object l_loading = new Object();    // Only allow one load to take place at a time on a given switch

    /**
     * This is public only as an implementation detail. Please do not
     * call this manually.
     */
    @Override public void run() {
        synchronized(l_loading) {
            // Clear the list of files to delete at the end
            filesToDelete = new LinkedList < File > ();

            String bootImageName = null;
            TransferObserver bootObserver = null;

            String rootImageName = null;
            TransferObserver rootObserver = null;

            SwitchConfig checkpoint = null;
            SwitchInfo info = null;

            imageDone = false;
            bootImageDone = false;

            try {
                // Read info from the switch
                checkpoint = backupConfig();
                info = probeSwitch(checkpoint);

                // Verify that everything should work, and tweak if necessary
                verifyEthOnly(checkpoint, info);
                forceIfNecessary(checkpoint, info);

                // Extract the images and feed them to the TFTP server
                VFile[]images = extractImages(checkpoint, info);

                bootImageName =
                getImageBName(firmware, info.getArchTag());
                bootObserver =
                addToTFTP(images[0], bootImageName, checkpoint,
                          info);

                rootImageName =
                getImageName(firmware, info.getArchTag());
                rootObserver =
                addToTFTP(images[1], rootImageName, checkpoint,
                          info);

                // Tell the switch to do the load
                performLoad(checkpoint, info, bootImageName,
                            rootImageName);

                // Wait for a response
                waitForResponse(checkpoint, info);

                // Give it just a little bit longer
                Utils.sleep(5000);

                // Restore the configuration, if necessary
                restoreIfNecessary(checkpoint, info);

                ui.updateState(FirmwareLoaderUI.S_COMPLETE);
            } catch(Exception e) {
                Utils.debug(e);

                String extraMessage = "";
                if (checkpoint != null) {
                    ui.saveSwitchState(checkpoint);
                    extraMessage += "Config saved in switch data";
                }

                // If interrupted, and interruptCause is set, then the interruption was
                //   internal; use that exception instead
                if (e instanceof InterruptedException ||
                    e instanceof InterruptedOperationException) {

                    if (interruptCause != null) {
                        e = interruptCause;
                    }
                }

                ui.handleException(e, extraMessage, multiThread);
            }
            finally {
                Thread.interrupted();   // Clear the interrupted flag, just in case

                // Remove the observers from the TFTP server
                if (bootObserver != null) {
                    try {
                        TFTP.getTFTP().removeObserver(bootImageName,
                                                      bootObserver);
                    }
                    catch(IOException ioe) {
                        throw new
                        RuntimeException
                        ("TFTP server is running, shouldn't throw IOException",
                         ioe);
                    }
                }

                if (rootObserver != null) {
                    try {
                        TFTP.getTFTP().removeObserver(rootImageName,
                                                      rootObserver);
                    }
                    catch(IOException ioe) {
                        throw new
                        RuntimeException
                        ("TFTP server is running, shouldn't throw IOException",
                         ioe);
                    }
                }
            }
        }
    }

    private SwitchConfig backupConfig() throws IOException {
        /* We need to look at the configuration, so grab it now. */
        ui.updateState(FirmwareLoaderUI.S_DOWNLOAD_CONFIG);

        // Grab a configuration checkpoint from the switch
        SwitchConfig checkpoint =
        WebUI.readSwitchConfig(getSwitchAddress(),
                               getUserName(),
                               getPassword());

        if  (Thread.interrupted())
            throw new InterruptedOperationException();

        return checkpoint;
    } private SwitchInfo probeSwitch(SwitchConfig checkpoint) throws
        IOException {
        /* We also need to know other information about the switch. */
        ui.updateState(FirmwareLoaderUI.S_PROBE_SWITCH);

        // Grab other info from the switch
        SwitchInfo swInfo =
        WebUI.getSwitchInfo(getSwitchAddress(), getUserName(),
                            getPassword());

        // Confirm the vendor strings
        if  (!ignoreVendor)
            confirmVendor(swInfo, firmware.getFWVendor());

        // Make sure the bundle includes the proper architecture for the switch
        VFile image =
        firmware.find(swInfo.getArchTag(),
                      FirmwareBundle.T_FILE_IMAGE, null);
        if  (image == null) {
            throw new
            IOException
            ("The firmware file does not support this device.");
        }
        // Make sure the switch is running at least 3.7
        confirmVersion(swInfo, "3.7");

        if (Thread.interrupted())
            throw new InterruptedOperationException();

        return swInfo;
    }

    private void verifyEthOnly(SwitchConfig checkpoint,
                               SwitchInfo info) throws IOException {
        /* Make sure that we are able to load firmware using this method. */
        ui.updateState(FirmwareLoaderUI.S_VERIFY_ETH_ONLY);

        // Check if Ethernet-Only firmware loading is enabled
        boolean ethOnlyEnabled = verifyEthOnlyEnabled(checkpoint);
        boolean allowedToLoad = ethOnlyEnabled || forceLoad;

        if  (!allowedToLoad) {
            throw new IOException("Network loading disabled");
        } if (Thread.interrupted())
              throw new InterruptedOperationException();
    }

    private void forceIfNecessary(SwitchConfig checkpoint,
                                  SwitchInfo info) throws IOException {
        // If ethernet-only loading is disabled and forced loading is allowed,
        //   enable ethernet-only loading
        if (!verifyEthOnlyEnabled(checkpoint) && forceLoad) {
            ui.updateState(FirmwareLoaderUI.S_SET_FORCE);

            enableEthOnly(checkpoint);
        } if (Thread.interrupted())
              throw new InterruptedOperationException();
    }

    // [0] = boot image
    // [1] = root image
    private VFile[] extractImages(SwitchConfig checkpoint,
                                  SwitchInfo info) throws IOException {
        /* Extract the firmware files from the bundle. */
        ui.updateState(FirmwareLoaderUI.S_EXTRACT_FILES);

        VFile rootImage = extractImage(checkpoint, info);
        VFile bootImage = extractBootImage(checkpoint, info);

        return new VFile[] {
            bootImage, rootImage};
    }

    private TransferObserver addToTFTP(final VFile image, String name,
                                       SwitchConfig checkpoint,
                                       SwitchInfo info) throws
                                           IOException {
        String sharedDir = ui.getTFTPDir();
        byte[] buffer = image.getData();

        if  (sharedDir == null) {
            // Make a new observer to watch the transfers
            TransferObserver observer = new TransferObserver(){
                    // Make sure to compare to the switch address
                    // Wouldn't want to update progress based on someone else's transfer
                    InetAddress switchAddress =
                    InetAddress.getByName(getSwitchAddress());

                    @Override
                        public boolean transferEnded(String path,
                                                     InetAddress with,
                                                     boolean success) {
                        // If not my transfer, ignore it
                        if (!with.equals(switchAddress))
                            return true;

                        // Contract says pass -100.0f when complete
                        ui.tftpProgress(-100.0f);

                        if (!success) {
                            // Transfer failed, interrupt the thread
                            interruptCause =
                            new Exception("Failed to transfer file '" +
                                          path + "'");
                            loadThread.interrupt();
                        } if (image.getVFSAttributes().getUserType() ==
                              FirmwareBundle.T_FILE_IMAGE) {
                            imageDone = true;
                        }
                        else if (image.getVFSAttributes().getUserType() ==
                                 FirmwareBundle.T_FILE_IMAGEB) {
                            bootImageDone = true;
                        }

                        // Let the listener know that something finished
                        synchronized(l_images) {
                            l_images.notifyAll();
                        }

                        return false;
                    }

                    @Override
                        public void transferProgress(String path,
                                                     InetAddress with,
                                                     int block, int total) {
                        if (!with.equals(switchAddress))
                            return;

                        // Update the UI progress
                        float progress = (float)block / total * 100;
                        if (progress > 100)
                            progress = 100;

                        ui.tftpProgress(progress);
                    }

                    @Override
                        public void transferStarted(String path,
                                                    InetAddress with) {
                        if (!with.equals(switchAddress))
                            return;

                        // Update the UI state according to which file is being transferred
                        if (image.getVFSAttributes().getUserType() ==
                            FirmwareBundle.T_FILE_IMAGE) {
                            ui.updateState(FirmwareLoaderUI.S_LOAD_IMAGE);
                        }
                        else if (image.getVFSAttributes().getUserType() ==
                                 FirmwareBundle.T_FILE_IMAGEB) {
                            ui.updateState(FirmwareLoaderUI.
                                           S_LOAD_BOOT_IMAGE);
                        }
                    }
                };

            if (!TFTP.getTFTP().fileExists(name)) {
                TFTP.getTFTP().addFile(name, buffer);
            }

            // Add the observer to the file
            TFTP.getTFTP().addObserver(name, observer);

            return observer;
        }
        else {
            // Write the file to the shared dir, if it isn't already there
            File imageFile =
            new File(sharedDir + File.separator + name);

            if (imageFile.exists() && !imageFile.isFile()) {
                throw new IOException("Please remove the object " +
                                      name + " from the directory " +
                                      sharedDir + " and try again.");
            }

            if (!imageFile.exists()) {
                FileOutputStream fout = new FileOutputStream(imageFile);
                try {
                    filesToDelete.add(imageFile);
                    fout.write(buffer);
                }
                finally {
                    fout.close();
                }
            }

            return null;
        }
    }

    private void performLoad(SwitchConfig checkpoint, SwitchInfo info,
                             String imageBName,
                             String imageName) throws IOException {
        ui.updateState(FirmwareLoaderUI.S_UPGRADE_REQUEST);

        URI loadURI;
        try {
            loadURI =
            new URI("http://" + swAddr +
                    "/cgi-bin/perform_fwinstall.cgi?" + "ipaddr=" +
                    swAddr + "&" + "netmask=" + swSubnet + "&" +
                    "gatewayip=" + swGateway + "&" + "serverip=" +
                    getLocalIP() + "&" + "bootimage=" + imageBName +
                    "&" + "rootimage=" + imageName);
        } catch(URISyntaxException urise) {
            throw new IOException("Bad switch address (" + swAddr +
                                  "): " + urise.getMessage());
        }

        URL server = loadURI.toURL();

        HttpURLConnection connection =
        (HttpURLConnection) server.openConnection();

        connection.setConnectTimeout(30000);
        connection.setReadTimeout(30000);

        setupAuthorization(connection);
        connection.connect();

        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
            throw new
            IOException
            ("Cannot access web server on switch with address " +
             swAddr + ", response was \"" +
             connection.getResponseMessage() + "\"");

        // Make sure the server gets to send its half
        String response = "";
        try {
            response = Utils.returnToEOF(connection.getInputStream());
        }
        catch(SocketTimeoutException ste) {
            // Perhaps it reset before it finished sending the response?
            Utils.debug(ste);
        }
        connection.disconnect();

        // Check if the switch failed to perform the load
        if (response.indexOf("Failed to load firmware") != -1) {
            throw new
            IOException
            ("Switch responded with \"Failed to load firmware\"");
        }

        // Wait a little while for the switch to restart, because then we want to wait for the web server to come back up
        Utils.sleep(10000);     // 10 seconds

        if (Thread.interrupted())
            throw new InterruptedOperationException();
    }

    private void waitForResponse(SwitchConfig checkpoint,
                                 SwitchInfo info) throws IOException {
        // Wait until it is back online and the HTTP server is running
        // Before we actually start pinging, wait until both files have been uploaded

        // If using an external TFTP server, sleep for a bit and then just start pinging
        if (ui.getTFTPDir() != null) {
            Utils.sleep(10000);
        }
        else {
            synchronized(l_images) {
                while (!imageDone || !bootImageDone) {
                    try {
                        l_images.wait();
                    }
                    catch(InterruptedException ie) {
                        throw new InterruptedOperationException(ie);
                    }
                }
            }
        }

        // No point waiting at all if we're not supposed to
        if (getResponseTimeout() < 0) {
            // Also, no way to know whether we can call deleteFiles(), so don't
            return;
        }

        ui.updateState(FirmwareLoaderUI.S_WAIT);

        try {
            waitForHTTP();

            // If that succeeded, then we can delete the external TFTP files
            deleteFiles();
        }
        catch(TimeoutException te) {
            throw new IOException("Timed out waiting for response", te);
        }

        if (Thread.interrupted())
            throw new InterruptedOperationException();
    }

    private void deleteFiles() {
        for (File f:filesToDelete) {
            if (!f.delete()) {
                logger.warn("Unable to delete file '" +
                            f.getAbsolutePath() + "'");
            }
        }

        filesToDelete.clear();
    }

    private void restoreIfNecessary(SwitchConfig checkpoint,
                                    SwitchInfo info) throws IOException
    {
        // If we're supposed to, restore saved settings
        if (backupSettings) {
            ui.updateState(FirmwareLoaderUI.S_RESTORE_CONFIG);
            WebUI.writeSwitchConfig(checkpoint, getSwitchAddress(),
                                    getUserName(), getPassword());
        }}

    /**
     * Check whether the switch has ethernet-only loading enabled.
     * 
     * @return True = ethernet-only loading is enabled, false = disabled.
     * @throws IOException If there was a problem communicating with the switch.
     */
    public boolean verifyEthOnlyEnabled(SwitchConfig
                                        checkpoint)
        throws IOException {
        // Decompress it and check the relevant setting
        FSObject configFile =
        checkpoint.getRoot().locate("etc/switch/main.config");
        if  (configFile != null &&
             configFile instanceof com.sixnetio.fs.generic.File) {
            com.sixnetio.fs.generic.File file =
            (com.sixnetio.fs.generic.File) configFile;
            INIParser parser =
            new INIParser(new ByteArrayInputStream(file.getData()));
            return parser.getValue("remote configuration",
                                   "firmware_load").
            equals("network");
        }
        else {
            throw new
            IOException
            ("main.config missing from switch configuration, or not a file");
        }
    }

    /**
     * Force-enable ethernet-only loading.
     * 
     * @throws IOException If there was a problem communicating with the switch.
     */
    private void enableEthOnly(SwitchConfig checkpoint) throws
        IOException {
        // Copy the checkpoint, since we need to modify it
        SwitchConfig config = new SwitchConfig(checkpoint);

        // Change the relevant setting
        FSObject configFile =
        config.getRoot().locate("etc/switch/main.config");
        if  (configFile != null &&
             configFile instanceof com.sixnetio.fs.generic.File) {
            com.sixnetio.fs.generic.File file =
            (com.sixnetio.fs.generic.File) configFile;
            INIParser parser =
            new INIParser(new ByteArrayInputStream(file.getData()));
            parser.setValue("remote configuration", "firmware_load",
                            "network");

            // Save it back into the tarball
            file.setData(parser.getBytes());

            // Send it back to the switch
            WebUI.writeSwitchConfig(config, getSwitchAddress(),
                                    getUserName(), getPassword());
        }
        else {
            throw new
            IOException
            ("main.config missing from switch configuration, or not a file");
        }
    }

    /**
     * Retrieve the local IP address from the UI. Default to the IP address
     * of the local host if unavailable.
     * 
     * @return The local IP address.
     * @throws IOException If there was a problem retrieving the IP address
     * of the local host.
     */
    private String getLocalIP() throws IOException {
        String localAddr = ui.getTFTPAddr();

        if  (localAddr != null) {
            return localAddr;
        }
        else {
            // What is our primary IP address?
            InetAddress addr = InetAddress.getLocalHost();

            return addr.getHostAddress();
        }
    }

    /**
     * Make sure that the vendor in the switch matches the required vendor name.
     * 
     * @param info The info from the switch (containing the vendor name).
     * @param requiredVendor The required vendor name.
     * @throws IOException If the vendor in the switch does not match the required vendor name.
     */
    private static void confirmVendor(SwitchInfo info,
                                      String requiredVendor) throws
                                          IOException {
        if (!info.vendor.equals(requiredVendor)) {
            throw new IOException(String.
                                  format
                                  ("Vendor mismatch: expected '%s', found '%s'",
                                   requiredVendor, info.vendor));
        }}

    /**
     * Make sure that the version number in the switch is at least the required version number.
     * 
     * @param info The info from the switch (containing the firmware version running in it).
     * @param requiredVersion The minimum version number to require.
     * @throws IOException If the version in the switch is less than the required version.
     */
    private static void confirmVersion(SwitchInfo info,
                                       String
                                       requiredVersion)
        throws IOException {
        Version test = new Version(info.fwVersion);
        Version require = new Version(requiredVersion);

        if  (test.compareTo(require) < 0) {
            throw new
            IOException
            ("Unable to perform operation, the device must be running firmware revision "
             + requiredVersion + " or later");
        }}

    /**
     * Get unique name for the root image file.
     * 
     * @param firmware The firmware bundle containing the file.
     * @param arch The architecture of the file.
     * @return A unique name for the file.
     */
    private String getImageName(FirmwareBundle firmware, int arch) {
        String name = String.format("%x.image.%s.%s.%s",
                                    Utils.srand.nextLong(),     // Guarantees that we will not step on someone else's image
                                    firmware.getFWVersion(),
                                    firmware.translateUserType(arch),
                                    (firmware.getFWVendor().length() >
                                     0 ? firmware.
                                     getFWVendor() : "sx"));

        if (name.length() > 29) {
            name = name.substring(0, 29);
        }

        return name;
    }

    /**
     * Get a unique name for the boot image file.
     * 
     * @param firmware The firmware bundle containing the file.
     * @param arch The architecture of the file.
     * @return A unique name for the file.
     */
    private String getImageBName(FirmwareBundle firmware, int arch) {
        String name = String.format("%x.imageb.%s.%s.%s",
                                    Utils.srand.nextLong(),
                                    firmware.getFWVersion(),
                                    firmware.translateUserType(arch),
                                    (firmware.getFWVendor().length() > 0 ? firmware.getFWVendor() : "sx"));     // Guarantees that we will not step on someone else's image

        if (name.length() > 29) {
            name = name.substring(0, 29);
        }

        return name;
    }

    /**
     * Extract the image for the proper architecture from the bundle, or throw an exception.
     * 
     * @param checkpoint A configuration checkpoint for the switch.
     * @param info The information about the switch (including architecture).
     * @return The node with the image data.
     * @throws IOException If a matching image could not be found.
     */
    private VFile extractImage(SwitchConfig checkpoint,
                               SwitchInfo info) throws IOException {
        VFile image = getImage(info.getArchTag(), firmware);

        if  (image == null)
            throw new
            IOException
            ("The firmware file does not support this device.");

        return image;
    }

    /**
     * Extract the boot image for the proper architecture from the bundle, or throw an exception.
     * 
     * @param checkpoint A configuration checkpoint for the switch.
     * @param info The information about the switch (including architecture).
     * @return The node with the boot image data.
     * @throws IOException If a matching boot image could not be found.
     */
    private VFile extractBootImage(SwitchConfig checkpoint,
                                   SwitchInfo info)
        throws IOException {
        VFile bimage = getBImage(info.getArchTag(), firmware);

        if  (bimage == null)
            throw new
            IOException
            ("The firmware file does not support this device.");

        return bimage;
    }

    /**
     * Get the virtual FS node representing the root image file for the architecture.
     * 
     * @param arch The architecture (one of FirmwareBundle.T_Arch.*).
     * @param bundle The bundle to extract from.
     * @return The node for the root image, or <tt>null</tt> if not found.
     */
    private static VFile getImage(int arch, FirmwareBundle bundle) {
        return bundle.find(arch, FirmwareBundle.T_FILE_IMAGE, null);
    }

    /**
     * Get the virtual FS node representing the boot image file for the architecture.
     * 
     * @param arch The architecture (one of FirmwareBundle.T_Arch.*).
     * @param bundle The bundle to extract from.
     * @return The node for the boot image, or <tt>null</tt> if not found.
     */
    private static VFile getBImage(int arch, FirmwareBundle bundle) {
        return bundle.find(arch, FirmwareBundle.T_FILE_IMAGEB, null);
    }

    /**
     * Wait for the web server on the switch to start listening on port 80.
     * 
     * @throws IOException If there was a problem pinging the switch prior to
     * waiting for the connection to open.
     * @throws TimeoutException If the request timeout is reached before the
     * switch starts responding.
     */
    private void waitForHTTP() throws IOException, TimeoutException {
        FWLoadUtil.waitForHTTP(getLocalIP(), swAddr,
                               getResponseTimeout());
    }

    /**
     * Set up an HttpURLConnection to perform basic password authorization.
     * 
     * @param connection The connection to set up.
     */
    private void setupAuthorization(HttpURLConnection connection) {
        connection.setRequestProperty("Authorization",
                                      "Basic " +
                                      Utils.
                                      encode64((userName + ":" +
                                                password).getBytes()));
    }

    /**
     * Tell the switch to turn off a ring port to break the ring.
     * 
     * @return The port that was turned off, so it can be turned back on later.
     * @throws IOException If there was a problem communicating with the switch.
     */
    public int breakRing() throws IOException {
        int portCount =
        WebUI.getPortCount(getSwitchAddress(), getUserName(),
                           getPassword());

        // Figure out which ports are participating in RSTP
        URI info;
        try {
            info =
            new URI("http://" + swAddr +
                    "/cgi-bin/stpstat.cgi?getdata");
        } catch(URISyntaxException urise) {
            throw new IOException("Bad switch address (" + swAddr +
                                  "): " + urise.getMessage());
        }

        URL server = info.toURL();

        HttpURLConnection connection =
        (HttpURLConnection) server.openConnection();

        setupAuthorization(connection);
        connection.connect();

        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
            throw new
            IOException
            ("Cannot access web server on switch with address " +
             swAddr + ", response was \"" +
             connection.getResponseMessage() + "\"");

        InputStream in = connection.getInputStream();

        Vector < Integer > includedPorts = new Vector < Integer > ();
        for (int i = 1; i <= portCount; i++) {
            // Look for "<td>port_[i]</td>, then check whether it is Included
            // Forwarding is not important
            Utils.readUntil(in, "<td>port_" + i + "</td><td>");
            String included = Utils.returnUntil(in, "</td>");

            if (included.equals("Included"))
                includedPorts.add(i);
        }

        Utils.readToEOF(in);

        connection.disconnect();

        if (includedPorts.size() != 2)
            throw new
            IOException("Wrong number of RSTP-included ports (" +
                        includedPorts.size() +
                        "), refusing to break ring");

        // Get the current switch config
        SwitchConfig config =
        WebUI.readSwitchConfig(getSwitchAddress(), getUserName(),
                               getPassword());

        // Change the relevant setting (turn off includedPorts[0])
        FSObject configFile =
        config.getRoot().locate("etc/switch/port" +
                                includedPorts.get(0) + ".config");
        if (configFile != null &&
            configFile instanceof com.sixnetio.fs.generic.File) {
            com.sixnetio.fs.generic.File file =
            (com.sixnetio.fs.generic.File) configFile;
            INIParser parser =
            new INIParser(new ByteArrayInputStream(file.getData()));
            parser.setValue("port configuration", "port_enable",
                            "disabled");

            // Send it back to the switch
            file.setData(parser.getBytes());
            WebUI.writeSwitchConfig(config, getSwitchAddress(),
                                    getUserName(), getPassword());

            return includedPorts.get(0);
        }
        else {
            throw new IOException("etc/switch/port" +
                                  includedPorts.get(0) +
                                  ".config does not exist or is not a file");
        }
    }

    /**
     * Turn on the given port.
     * 
     * @param port The port to turn on.
     * @throws IOException If there was a problem communicating with the switch.
     */
    public void connectRing(int port) throws IOException {
        // Get the current switch config
        SwitchConfig config =
        WebUI.readSwitchConfig(getSwitchAddress(), getUserName(),
                               getPassword());

        // Change the relevant setting (turn on 'port')
        FSObject configFile =
        config.getRoot().locate("etc/switch/port" + port +
                                ".config");
        if  (configFile != null &&
             configFile instanceof com.sixnetio.fs.generic.File) {
            com.sixnetio.fs.generic.File file =
            (com.sixnetio.fs.generic.File) configFile;
            INIParser parser =
            new INIParser(new ByteArrayInputStream(file.getData()));
            parser.setValue("port configuration", "port_enable",
                            "enabled");

            // Send it back to the switch
            file.setData(parser.getBytes());
            WebUI.writeSwitchConfig(config, getSwitchAddress(),
                                    getUserName(), getPassword());
        }
        else {
            throw new IOException("etc/switch/port" + port +
                                  ".config does not exist or is not a file");
        }
    }
}

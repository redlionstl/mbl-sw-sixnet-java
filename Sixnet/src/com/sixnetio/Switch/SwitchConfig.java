
/*
 * SwitchConfig.java
 *
 * Represents a backed-up configuration from a switch.
 * Basically a file held in memory.
 *
 * Jonathan Pearson
 * June 22, 2007
 *
 */

package com.sixnetio.Switch;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.Date;

import com.sixnetio.fs.tar.GZippedTarball;

public class SwitchConfig extends GZippedTarball {
    private long timestamp;

    // In case it was loaded from a file
    public SwitchConfig(long timestamp, byte[]data) throws IOException {
        super(data);
        this.timestamp = timestamp;
    } public SwitchConfig(InputStream in) throws IOException {
        super(in);
        timestamp = System.currentTimeMillis();
    } public SwitchConfig(SwitchConfig checkpoint) throws IOException {
        super(checkpoint.getData());
        timestamp = checkpoint.timestamp;
    } public long getTimestamp() {
        return timestamp;
    } public String toString() {
        // Convert the timestamp to a date string
        return DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
                                              DateFormat.SHORT).
        format(new Date(timestamp));
    }
}

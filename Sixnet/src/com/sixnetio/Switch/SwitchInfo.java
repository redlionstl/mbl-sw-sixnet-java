
/*
 * SwitchInfo.java
 *
 * Represents information about a switch.
 *
 * Jonathan Pearson
 * Apr 21, 2009
 *
 */

package com.sixnetio.Switch;

import com.sixnetio.fs.vfs.FirmwareBundle;

public class SwitchInfo {

    /** The sxid of the switch (minus the vendor). */
    public final String sxid;

    /** The firmware version number in the switch. */
    public final String fwVersion;

    /** The vendor portion of the sxid, or "" for Sixnet. */
    public final String vendor;

    /** The architecture of the switch (long names, like 'sxni9260'). */
    public final String architecture;

    /**
     * Construct a new SwitchInfo with values.
     * 
     * @param sxid The product portion of the SXID.
     * @param fwVersion The firmware version number.
     * @param vendor The OEM portion of the SXID.
     * @param architecture The CPU architecture.
     */
    public SwitchInfo(String sxid, String fwVersion, String vendor,
                      String architecture) {
        this.sxid = sxid;
        this.fwVersion = fwVersion;
        this.vendor = vendor;
        this.architecture = architecture;
    }

    /**
     * Get one of the FirmwareBundle.T_Arch_* values based on 'architecture'.
     */ public int getArchTag() {
         if (architecture.indexOf("sxni9260") != -1) {
             return FirmwareBundle.T_ARCH_ARM9;
         }
         else if (architecture.indexOf("sxni855t") != -1) {
             return FirmwareBundle.T_ARCH_PPC;
         }
         else if (architecture.indexOf("sxni79524") != -1) {
             return FirmwareBundle.T_ARCH_ARM;
         }
         else if (architecture.indexOf("sxni8313") != -1) {
             return FirmwareBundle.T_ARCH_PPC_8313;
         }
         else {
             throw new
             IllegalStateException("Unrecognized architecture: " +
                                   architecture);
         }
     }
}


/*
 * WebUI.java
 *
 * Provides functions for interfacing with the switch's Web UI.
 *
 * Jonathan Pearson
 * February 20, 2008
 *
 */

package com.sixnetio.Switch;

import java.io.*;
import java.net.*;
import java.util.StringTokenizer;

import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

public class WebUI {

    /**
     * Read a configuration checkpoint from a switch.
     * @param swAddr The address of the switch. Can be either an IP or a host name.
     * @param user The user name for accessing the switch's web UI.
     * @param password The password for accessing the switch's web UI.
     * @return The switch's current configuration.
     * @throws IOException If there is a communication error.
     * @throws URISyntaxException If there is a problem with the switch's address.
     */
    public static SwitchConfig readSwitchConfig(String swAddr,
                                                String user,
                                                String password) throws
                                                    IOException {
        URI saveChkpt;
        try {
            saveChkpt =
            new URI("http://" + swAddr + "/cgi-bin/cfgcommit.cgi");
        } catch(URISyntaxException urise) {
            // We know that the URI is good when the switch address is good, so it must
            //   be the switch address's fault if the URI is bad
            throw new IOException("Bad switch address '" + swAddr +
                                  "': " + urise.getMessage(), urise);
        } URL server = saveChkpt.toURL();

        HttpURLConnection connection =
        (HttpURLConnection) server.openConnection();

        String postData =
        "savenw=1&cfgtype=1&cfgtime=2000-1-1+12%3A0%3A0";

        connection.setRequestMethod("POST");

        connection.setDoOutput(true);
        connection.setFixedLengthStreamingMode(postData.length());

        setupAuthorization(connection, user, password);
        connection.connect();

        OutputStream out = connection.getOutputStream();
        out.write(postData.getBytes());

        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
            throw new
            IOException
            ("Cannot access web server on switch with address " +
             swAddr + ", response was \"" +
             connection.getResponseMessage() + "\"");

        // Not actually interested in the result, so just discard the data and close the connection
        Utils.readToEOF(connection.getInputStream());
        connection.disconnect();

        // Now do it again to download that checkpoint
        URI dlChkpt;
        try {
            dlChkpt =
            new URI("http://" + swAddr + "/cgi-bin/cfgdl.cgi");
        }
        catch(URISyntaxException urise) {
            throw new IOException("Bad switch address '" + swAddr +
                                  "': " + urise.getMessage(), urise);
        }

        server = dlChkpt.toURL();

        connection = (HttpURLConnection) server.openConnection();

        setupAuthorization(connection, user, password);
        connection.connect();

        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
            throw new
            IOException
            ("Cannot access web server on switch with address " +
             swAddr + ", response was \"" +
             connection.getResponseMessage() + "\"");

        InputStream in = connection.getInputStream();

        SwitchConfig ans = new SwitchConfig(in);

        Utils.readToEOF(in);    // In case there's anything else (shouldn't be)
        connection.disconnect();

        return ans;
    }

    /**
     * Write a configuration checkpoint to a switch and activate it.
     * @param swCfg The checkpoint to write to the switch.
     * @param swAddr The address of the switch. Can be either an IP or a host name.
     * @param user The user name for accessing the switch's web UI.
     * @param password The password for accessing the switch's web UI.
     * @throws IOException If there is a communication error.
     * @throws URISyntaxException If there is a problem with the switch's address.
     */
    public static void writeSwitchConfig(SwitchConfig swCfg,
                                         String swAddr, String user,
                                         String password) throws
                                             IOException {
        URI ulChkpt;
        try {
            ulChkpt =
            new URI("http://" + swAddr + "/cgi-bin/cfgul.cgi");
        } catch(URISyntaxException urise) {
            throw new IOException("Bad switch address '" + swAddr +
                                  "': " + urise.getMessage(), urise);
        }

        URL server = ulChkpt.toURL();

        // Upload the switch config data
        HttpURLConnection connection =
        (HttpURLConnection) server.openConnection();

        connection.setRequestMethod("POST");
        String boundary =
        "----------------" + System.currentTimeMillis();

        connection.setRequestProperty("Content-Type",
                                      "multipart/form-data; boundary=" +
                                      boundary);

        String header = boundary + "\r\n" +
        "Content-Disposition: form-data; name=\"upload\"; filename=\"switchcfg.tgz\"\r\n"
        + "Content-Type: application/octet-stream\r\n\r\n";

        String nextBlock = boundary + "\r\n" +
        "Content-Disposition: form-data; name=\"cfgtype\"\r\n\r\n" +
        "SubmitType\r\n";

        String lastBlock = boundary + "--";

        byte[]configData = swCfg.getData();

        connection.setFixedLengthStreamingMode(header.length() +
                                               configData.length +
                                               nextBlock.length() +
                                               lastBlock.length());
        connection.setDoOutput(true);

        setupAuthorization(connection, user, password);
        connection.connect();

        OutputStream out = connection.getOutputStream();

        out.write(header.getBytes());
        out.write(configData);
        out.write(nextBlock.getBytes());
        out.write(lastBlock.getBytes());

        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
            throw new
            IOException
            ("Cannot access web server on switch with address " +
             swAddr + ", response was \"" +
             connection.getResponseMessage() + "\"");

        // Make sure the server gets to send its half
        Utils.readToEOF(connection.getInputStream());
        connection.disconnect();

        // Now connect to tell the switch to restore the checkpoint
        URI restore;
        try {
            restore =
            new URI("http://" + swAddr + "/cgi-bin/cfgcommit.cgi");
        }
        catch(URISyntaxException urise) {
            throw new IOException("Bad switch address '" + swAddr +
                                  "': " + urise.getMessage(), urise);
        }

        server = restore.toURL();

        int timeout = 60000;    // First timeout is 60 seconds, try again at 90 if we time out, then fail

        while (true) {
            try {
                connection =
                (HttpURLConnection) server.openConnection();

                connection.setReadTimeout(timeout);

                String postData =
                "savenw=1&cfgtype=2&cfgtime=2000-1-1+12%3A0%3A0";

                connection.setRequestMethod("POST");

                connection.setDoOutput(true);
                connection.setFixedLengthStreamingMode(postData.
                                                       length());

                setupAuthorization(connection, user, password);
                connection.connect();

                out = connection.getOutputStream();
                out.write(postData.getBytes());

                if (connection.getResponseCode() !=
                    HttpURLConnection.HTTP_OK)
                    throw new
                    IOException
                    ("Cannot access web server on switch with address "
                     + swAddr + ", response was \"" +
                     connection.getResponseMessage() + "\"");

                // All done
                String page =
                Utils.returnToEOF(connection.getInputStream());
                connection.disconnect();

                // Check for 'alert's, which could signal an error
                int index = page.toLowerCase().indexOf("alert");
                if (index > -1) {
                    // Pull out the message
                    page = page.substring(index + "alert".length());

                    int quote = page.indexOf('"');
                    int half = page.indexOf('\'');

                    char lookFor;
                    if (quote == -1) {
                        index = half;
                        lookFor = '\'';
                    }
                    else if (half == -1) {
                        index = quote;
                        lookFor = '"';
                    }
                    else if (quote < half) {
                        index = quote;
                        lookFor = '"';
                    }
                    else {
                        index = half;
                        lookFor = '\'';
                    }

                    String message = page.substring(index + 1, page.indexOf(lookFor, index + 1));       // FIXME: This does not allow for \" in the message

                    throw new IOException("Bad response from server: " +
                                          message);
                }

                break;
            }
            catch(SocketTimeoutException ste) {
                connection.disconnect();

                if (timeout < 90000) {
                    timeout += 30000;   // Add 30 seconds
                }
                else {
                    throw new IOException("No HTTP response");
                }
            }
        }
    }

    /**
     * Get the number of ports on the switch.
     * 
     * @param swAddr The address of the switch.
     * @param user The Web UI user name.
     * @param password The Web UI password.
     * @return The number of ports on the switch.
     * @throws IOException If there was a problem communicating with the switch.
     */
    public static int getPortCount(String swAddr, String user,
                                   String password) throws IOException {
        // Get portstat.cgi and search successively for each port number; return the highest we find before EOF
        URI info;
        try {
            info =
            new URI("http://" + swAddr + "/cgi-bin/portstat.cgi");
        } catch(URISyntaxException urise) {
            throw new IOException("Bad switch address '" + swAddr +
                                  "': " + urise.getMessage(), urise);
        }

        URL server = info.toURL();

        HttpURLConnection connection =
        (HttpURLConnection) server.openConnection();

        setupAuthorization(connection, user, password);
        connection.connect();

        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
            throw new
            IOException
            ("Cannot access web server on switch with address " +
             swAddr + ", response was \"" +
             connection.getResponseMessage() + "\"");

        InputStream in = connection.getInputStream();

        int portCounter = 1;
        while (true) {
            try {
                Utils.readUntil(in, "port_" + portCounter);

                // Found it, try the next
                portCounter++;
            }
            catch(IOException ioe) {
                // That one didn't exist
                portCounter--;
                break;
            }
        }

        Utils.readToEOF(in);

        connection.disconnect();

        return portCounter;
    }

    /**
     * Get the firmware version running in the switch.
     * 
     * @param swAddr The address of the switch.
     * @param user The Web UI user name.
     * @param password The Web UI password.
     * @return The firmware version running in the switch.
     * @throws IOException If there was a problem communicating with the switch.
     */
    public static String getFirmwareVersion(String swAddr, String user,
                                            String password) throws
                                                IOException {
        URI info;
        try {
            info = new URI("http://" + swAddr + "/cgi-bin/sysinfo.cgi");
        } catch(URISyntaxException urise) {
            throw new IOException("Bad switch address '" + swAddr +
                                  "': " + urise.getMessage(), urise);
        }

        URL server = info.toURL();

        HttpURLConnection connection =
        (HttpURLConnection) server.openConnection();

        connection.setReadTimeout(30000);       // 30 seconds

        setupAuthorization(connection, user, password);

        try {
            connection.connect();

            if (connection.getResponseCode() !=
                HttpURLConnection.HTTP_OK)
                throw new
                IOException
                ("Cannot access web server on switch with address "
                 + swAddr + ", response was \"" +
                 connection.getResponseMessage() + "\"");
        }
        catch(SocketTimeoutException ste) {
            throw new
            IOException
            ("The switch did not respond after 30 seconds", ste);
        }

        InputStream in = connection.getInputStream();

        // Read to "Firmware revision"
        Utils.readUntil(in, "Firmware revision");

        // Read to "<td>"
        Utils.readUntil(in, "<td>");

        // Read the number
        String revision = Utils.returnUntil(in, "</td>");

        // Finish off the stream
        Utils.readToEOF(in);
        connection.disconnect();

        return revision;
    }

    /**
     * Retrieve switch info from the switch using the /cgi-bin/getinfo.cgi.
     * 
     * @param swAddr The address of the switch.
     * @param user The Web UI user name.
     * @param password The Web UI password.
     * @return Information about the switch.
     * @throws IOException If there was a problem communicating with the switch.
     */
    public static SwitchInfo getSwitchInfo(String swAddr, String user,
                                           String password) throws
                                               IOException {
        URI info;
        try {
            info = new URI("http://" + swAddr + "/cgi-bin/getinfo.cgi");
        } catch(URISyntaxException urise) {
            throw new IOException("Bad switch address '" + swAddr +
                                  "': " + urise.getMessage(), urise);
        }

        URL server = info.toURL();

        HttpURLConnection connection =
        (HttpURLConnection) server.openConnection();

        connection.setReadTimeout(30000);       // 30 seconds

        setupAuthorization(connection, user, password);

        try {
            connection.connect();

            if (connection.getResponseCode() !=
                HttpURLConnection.HTTP_OK)
                throw new
                IOException
                ("Cannot access web server on switch with address "
                 + swAddr + ", response was \"" +
                 connection.getResponseMessage() + "\"");
        }
        catch(SocketTimeoutException ste) {
            throw new
            IOException
            ("The switch did not respond after 30 seconds");
        }

        // Read everything
        InputStream in = connection.getInputStream();
        String strInfo = Utils.returnToEOF(in);

        if (Thread.interrupted())
            throw new InterruptedOperationException();

        // Split on &
        StringTokenizer tok = new StringTokenizer(strInfo, "&");

        String sxid = null;
        String fwVersion = null;
        String vendor = null;
        String architecture = null;

        while (tok.hasMoreTokens()) {
            if (Thread.interrupted())
                throw new InterruptedOperationException();

            // Split each token on =
            StringTokenizer subtok =
            new StringTokenizer(tok.nextToken(), "=");

            String key = subtok.nextToken();
            String value = null;

            if (subtok.hasMoreTokens()) {
                value = subtok.nextToken();
            }
            else {
                continue;
            }

            if (key.equalsIgnoreCase("sxid")) {
                // For some reason, some switches will send "sxid=sxid=..." instead of "sxid=..."
                if (subtok.hasMoreTokens())
                    value = subtok.nextToken();

                if (value.equalsIgnoreCase("Unknown")) {
                    sxid = "Unknown";
                }
                else {
                    // Cut value just after the underscore to get the product name
                    // If there's a dot, cut before that and pull off the rest to get the vendor
                    sxid = value.substring(value.indexOf("_") + 1);
                    if (sxid.indexOf(".") != -1) {
                        sxid = sxid.substring(0, sxid.indexOf("."));
                        vendor =
                        value.substring(value.indexOf(".") + 1);
                    }
                    else {
                        vendor = "";
                    }
                }
            }
            else if (key.equalsIgnoreCase("boardId")) {
                // Cut value just before "sxni" and convert to lower-case to get the architecture
                value = value.toLowerCase();
                architecture = value.substring(value.indexOf("sxni"));
            }
            else if (key.equalsIgnoreCase("fwRev")) {
                // Take the whole thing as the firmware version
                fwVersion = value;
            }
        }

        SwitchInfo swInfo =
        new SwitchInfo(sxid, fwVersion, vendor, architecture);

        connection.disconnect();

        if (swInfo.architecture != null) {
            // Make sure it's recognized
            swInfo.getArchTag();
        }
        else {
            throw new
            IOException
            ("Unable to determine architecture of switch");
        }

        return swInfo;
    }

    public static void applyTFTPPatch(String swAddr, String tftpAddr,
                                      String fileName, String user,
                                      String password) throws
                                          IOException {
        // Tell the switch to TFTP and install that file
        URI fwUpdate;
        try {
            fwUpdate =
            new URI("http://" + swAddr + "/cgi-bin/updcommit.cgi");
        } catch(URISyntaxException urise) {
            throw new IOException("Bad switch address '" + swAddr +
                                  "': " + urise.getMessage(), urise);
        }

        URL server = fwUpdate.toURL();

        HttpURLConnection connection =
        (HttpURLConnection) server.openConnection();

        String postData =
        String.format("tftp=%s&file=%s", tftpAddr, fileName);

        connection.setRequestMethod("POST");

        connection.setDoOutput(true);
        connection.setFixedLengthStreamingMode(postData.length());

        connection.setReadTimeout(60000);       // One minute

        setupAuthorization(connection, user, password);

        try {
            connection.connect();

            OutputStream out = connection.getOutputStream();
            out.write(postData.getBytes());

            if (connection.getResponseCode() !=
                HttpURLConnection.HTTP_OK)
                throw new
                IOException
                ("Cannot access web server on switch with address "
                 + swAddr + ", response was \"" +
                 connection.getResponseMessage() + "\"");
        }
        catch(SocketTimeoutException ste) {
            throw new
            IOException
            ("The switch did not respond after one minute.");
        }

        // Make sure the server gets to send its half
        Utils.readToEOF(connection.getInputStream());
        connection.disconnect();
    }

    private static void setupAuthorization(HttpURLConnection connection,
                                           String user,
                                           String password) {
        connection.setRequestProperty("Authorization",
                                      "Basic " +
                                      Utils.
                                      encode64((user + ":" +
                                                password).getBytes()));
    }
}


/*
 * SerialLoader.java
 *
 * A firmware loader that runs over ethernet & serial.
 *
 * Jonathan Pearson
 * July 6, 2007
 *
 */

package com.sixnetio.Switch;

import java.io.*;
import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import com.sixnetio.COM.ComPort;
import com.sixnetio.COM.UBootCommunicator;
import com.sixnetio.GUI.ProgressListener;
import com.sixnetio.fs.vfs.FirmwareBundle;
import com.sixnetio.fs.vfs.VFile;
import com.sixnetio.server.tftp.TFTP;
import com.sixnetio.uboot.MTDPartsParser;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

public class SerialLoader extends Thread {
    private static final Logger logger =
    Logger.getLogger(Utils.thisClassName());

    private static final long UBOOT_TIMEOUT = 60000;    // One minute to catch U-Boot

    private static Object l_loading = new Object();     // Only allow one load to take place at a time for serial

    private String swAddr, swSubnet, swGateway, comPath;

    private int baudRate;
    private FirmwareLoaderUI ui;
    private boolean ignoreVendor;
    private long responseTimeout = 0;
    private boolean loadBothSides = false;
    private boolean applyConfigImage = false;
    private Thread loadThread;  // The thread that performs the firmware load
    private String sxid;
    private boolean showScript = false;

    /**
     * Construct a new SerialLoader. This will enforce a match between the switch and
     * firmware vendors.
     * 
     * @param ui The UI for communication with the user.
     * @param swAddr The switch's address.
     * @param swSubnet The switch's subnet.
     * @param swGateway The switch's gateway.
     * @param comPath The path to the serial port.
     */
    public SerialLoader(FirmwareLoaderUI ui, String swAddr,
                        String swSubnet, String swGateway,
                        String comPath, int baud) {
        this(ui, swAddr, swSubnet, swGateway, comPath, baud, false);
    }
    
    /**
     * Construct a new SerialLoader.
     * 
     * @param ui The UI for communication with the user.
     * @param swAddr The switch's address.
     * @param swSubnet The switch's subnet.
     * @param swGateway The switch's gateway.
     * @param comPath The path to the serial port.
     * @param ignoreVendor Whether to ignore a mismatch between the switch and firmware
     * vendors.
     */
    public SerialLoader(FirmwareLoaderUI ui, String swAddr,
                        String swSubnet,
                        String swGateway, String comPath, int baud,
                        boolean ignoreVendor) {
        super("SerialLoader");
        
        if (ui == null) {
            throw new
            IllegalArgumentException
            ("The FirmwareLoaderUI must be provided");
        }

        this.ui = ui;

        setSwitchAddress(swAddr);
        setSwitchSubnet(swSubnet);
        setSwitchGateway(swGateway);

        setComPath(comPath);
        setBaudRate(baud);

        this.ignoreVendor = ignoreVendor;
    }

    /**
     * Cancel the load by interrupting the thread.
     */
    public void cancel() {
        loadThread.interrupt();
    }

    /**
     * Get the switch's address.
     */
    public String getSwitchAddress() {
        return swAddr;
    }

    /**
     * Get the switch's subnet mask.
     */
    public String getSwitchSubnet() {
        return swSubnet;
    }

    /**
     * Get the switch's gateway address.
     */
    public String getSwitchGateway() {
        return swGateway;
    }

    /**
     * Get the path to the serial port.
     */
    public String getComPath() {
        return comPath;
    }

    /**
     * Set the switch's address.
     * 
     * @param val The new address.
     */
    public void setSwitchAddress(String val) {
        if (val == null || val.length() == 0) {
            throw new
            IllegalArgumentException
            ("Switch address must have a value");
        }

        swAddr = val;
    }

    /**
     * Set the switch's subnet mask.
     * 
     * @param val The new subnet mask.
     */
    public void setSwitchSubnet(String val) {
        if (val == null || val.length() == 0) {
            throw new
            IllegalArgumentException
            ("Switch subnet must have a value");
        }

        swSubnet = val;
    }

    /**
     * Set the switch's gateway address.
     * 
     * @param val The new gateway address.
     */
    public void setSwitchGateway(String val) {
        swGateway = val;
    }

    /**
     * Set the path to the serial port.
     * 
     * @param val The new path.
     */
    public void setComPath(String val) {
        if (val == null || val.length() == 0) {
            throw new
            IllegalArgumentException("Com path must have a value");
        }

        comPath = val;
    }

    /**
     * Set the baud rate.
     *
     * @param baud The new baud rate.
     */
    public void setBaudRate(int baud) {
        baudRate = baud;
    }

    /**
     * Get the response timeout. This is how long to wait after finishing a load
     * before deciding that the switch is unresponsive.
     * 
     * @return The response timeout, in ms. 0 means wait forever.
     */
    public long getResponseTimeout() {
        return responseTimeout;
    }

    /**
     * Set the response timeout.
     * 
     * @param val The response timeout, in ms. 0 means wait forever, a negative
     * value will not wait at all.
     */
    public void setResponseTimeout(long val) {
        responseTimeout = val;
    }

    /**
     * Check whether we will load both sides of a new layout switch.
     * 
     * @return <tt>true</tt> = load both sides of a new layout switch (ignored
     *   on an old layout switch); <tt>false</tt> = load the inactive side.
     */
    public boolean getLoadBothSides() {
        return loadBothSides;
    }

    /**
     * Set whether to load both sides of a new layout switch.
     * 
     * @param loadBothSides <tt>true</tt> = load both sides of a new layout
     *   switch (ignored on an old layout switch); <tt>false</tt> = load the
     *   inactive side.
     */
    public void setLoadBothSides(boolean loadBothSides) {
        this.loadBothSides = loadBothSides;
    }

    /**
     * Check whether we will apply the config image, if one exists in the
     * bundle.
     */
    public boolean getApplyConfigImage() {
        return applyConfigImage;
    }

    /**
     * Set whether to apply the config image, if one exists in the bundle.
     */
    public void setApplyConfigImage(boolean applyConfigImage) {
        this.applyConfigImage = applyConfigImage;
    }

    /**
     * Load firmware onto the switch. Block until finished. This
     * will restore configuration settings, but will not force the load.
     *
     * @param bundle The firmware bundle to load onto the switch.
     */
    public void loadFirmware(FirmwareBundle bundle)
    {
        loadFirmware(bundle, false);
    }

    /**
     * Load firmware onto the switch. Block until finished. This
     * will restore configuration settings, but will not force the load.
     * 
     * @param bundle The firmware bundle to load onto the switch.
     * @param showScript print out the u-boot commands being sent to the unit
     */
    public void loadFirmware(FirmwareBundle bundle, boolean showScript) {
        if (bundle == null) {
            throw new
            IllegalArgumentException
            ("The firmware bundle must be provided");
        }

        this.firmware = bundle;
        this.multiThread = false;
        this.showScript = showScript;

        this.loadThread = Thread.currentThread();
        run();
    }

    /**
     * Spawn a new thread to load firmware onto the switch. This
     * will restore configuration settings, but will not force the load.
     * 
     * @param bundle The firmware bundle to load onto the switch.
     */
    public void loadFirmwareAsync(FirmwareBundle bundle) {
        if (bundle == null) {
            throw new
            IllegalArgumentException
            ("The firmware bundle must be provided");
        }

        this.firmware = bundle;
        this.multiThread = true;

        this.loadThread = new Thread(this);
        loadThread.start();
    }

    private FirmwareBundle firmware;

    // Access is synchronized on the SerialLoader object
    private List < File > filesToDelete;
    private boolean multiThread;

    /**
     * This is public only as an implementation detail. Please do not
     * call this manually.
     */
    @Override public void run() {
        synchronized(l_loading) {
            filesToDelete = new LinkedList < File > ();

            UBootCommunicator uboot =
            new UBootCommunicator(comPath, baudRate, 8,
                                  ComPort.PARITY_NONE,
                                  ComPort.STOPBITS_1);
            // Display commands sent to u-boot, if requested.
            uboot.scriptPrint = this.showScript;

            try {
                catchUBoot(uboot);

                // set our class instance of sxid as early as possible
                sxid = uboot.getVariable("sxid");

                if (!ignoreVendor) {
                    String vendor = getSwitchVendor(uboot);
                    String compareTo =
                    firmware.getFWVendor().toLowerCase();

                    if (!vendor.equals(compareTo)) {
                        uboot.writeln("reset");

                        if (vendor.length() == 0) {
                            throw new
                            IOException
                            ("Vendor mismatch, switch is sixnet");
                        }
                        else {
                            throw new
                            IOException
                            ("Vendor mismatch, switch is " +
                             vendor);
                        }
                    }
                }

                int arch = requestArchitecture(uboot);

                String layoutVersion = getLayoutVersion(uboot, arch);

                // Extract the images and feed them to the TFTP server
                VFile[] images = extractImages(arch, layoutVersion);

                String bootImageName = null;
                // only v 1, 2, and 3 have boot images:
                if (layoutVersion.equals("3") ||
                    layoutVersion.equals("2") ||
                    layoutVersion.equals("1")) {
                    bootImageName = getImageBName(firmware, arch);
                    addToTFTP(images[0], bootImageName, arch);
                }

                String rootImageName = getImageName(firmware, arch);
                addToTFTP(images[1], rootImageName, arch);

                String configImageName = null;
                if (images[2] != null) {
                    configImageName = getImageCName(firmware, arch);
                    addToTFTP(images[2], configImageName, arch);
                }

                // Setup the switch network settings
                setupSwitch(uboot);

                // Load the images
		performLoad(uboot, arch, layoutVersion, images, bootImageName, 
                            rootImageName, configImageName);

                // Mostly for in-house debugging, to make the serial port accessible
                //   from the computer while waiting for a response
                uboot.close();

                waitForResponse();

                ui.updateState(FirmwareLoaderUI.S_COMPLETE);
            }
            catch(Exception e) {
                ui.handleException(e, "", multiThread);
            }
            finally {
                Thread.interrupted();   // Clear the interrupted flag, just in case

                // Make sure it's closed, even if an exception occurred
                uboot.close();

                deleteFiles();
            }
        }
    }

    /**
     * Get the vendor string from the connected switch.
     * 
     * @param uboot The UBoot communicator to use to communicate with the
     *   switch.
     * @return The vendor string (may be "").
     * @throws IOException If there is a problem communicating with the switch.
     */
    public String getSwitchVendor(UBootCommunicator uboot)
        throws IOException {
        String vendor;

        if (sxid == null) {
            sxid = uboot.getVariable("sxid");
        }

        if (sxid.indexOf('.') != -1) {
            vendor = sxid.substring(sxid.indexOf('.') + 1);
        }
        else {
            // Sixnet
            vendor = "";
        }

        return vendor;
    }

    private void catchUBoot(UBootCommunicator uboot) throws IOException {
        uboot.startCatch();

        ui.updateState(FirmwareLoaderUI.S_APPLY_POWER);

        try {
            uboot.waitForCatch(UBOOT_TIMEOUT);
        } catch(TimeoutException e) {
            throw new IOException("Timed out trying to catch UBoot");
        } finally {
            uboot.stopCatch();
        }
    }

    private int requestArchitecture(UBootCommunicator uboot) throws
        IOException {
        // Get the architecture
        ui.updateState(FirmwareLoaderUI.S_ARCH);
        int arch = getArch(uboot);

        if  (Thread.interrupted()) {
            throw new InterruptedOperationException();
        } return arch;
    }

    // [0] = boot image
    // [1] = root image
    // [2] = config image
    private VFile[] extractImages(int arch, String layoutVersion) throws IOException {
        /* Extract the firmware files from the bundle. */
        ui.updateState(FirmwareLoaderUI.S_EXTRACT_FILES);
	VFile[] images;

        VFile bootImage = null;
        // only v 1, 2, and 3 have boot images:
        if (layoutVersion.equals("3") ||
            layoutVersion.equals("2") ||
            layoutVersion.equals("1")) {
            bootImage = extractBootImage(arch);
        }
        
        VFile rootImage = extractImage(arch);
        VFile configImage = extractConfigImage(arch);
        images = new VFile[]{bootImage, rootImage, configImage};

	return images;
    }

    private void addToTFTP(final VFile image, String name,
                           int arch) throws IOException {
        String sharedDir = ui.getTFTPDir();
        byte[] buffer = image.getData();

        if  (sharedDir == null) {
            if (!TFTP.getTFTP().fileExists(name)) {
                TFTP.getTFTP().addFile(name, buffer);
            }
        }
        else {
            // Write the file to the shared dir, if it isn't already there
            File imageFile =
            new File(sharedDir + File.separator + name);

            if (imageFile.exists() && !imageFile.isFile()) {
                throw new IOException("Please remove the object " +
                                      name + " from the directory " +
                                      sharedDir + " and try again.");
            }

            if (!imageFile.exists()) {
                FileOutputStream fout = new FileOutputStream(imageFile);
                try {
                    filesToDelete.add(imageFile);
                    fout.write(buffer);
                }
                finally {
                    fout.close();
                }
            }
        }
    }

    // images[0] = boot image
    // images[1] = root image
    // images[2] = config image (or null)
    private void performLoad(UBootCommunicator uboot,
                             int arch, String layoutVersion, VFile[]images,
                             String bootImageName,
                             String rootImageName, String imageCName)
        throws IOException {
        // This handles its own UI interaction
        loadImages(arch, layoutVersion, uboot, new int[] {
                images[0] == null ? 0 : images[0].getSize(),
                images[1].getSize(),
                images[2] == null ? 0 : images[2].getSize(),},
            bootImageName, rootImageName, imageCName);

        uboot.writeln("reset");

        // Try to delete the files now, since they are no longer needed
        deleteFiles();
    }

    private void waitForResponse() throws IOException {
        if (getResponseTimeout() < 0) {
            return;             // No waiting
        } ui.updateState(FirmwareLoaderUI.S_WAIT);

        try {
            waitForHTTP();
        }
        catch(TimeoutException te) {
            throw new IOException("Timed out waiting for response", te);
        }
    }

    /**
     * Extract the image for the proper architecture from the bundle, or throw an exception.
     * 
     * @param arch One of FirmwareBundle.T_Arch_*
     * @return The file with the image data.
     * @throws IOException If a matching image could not be found.
     */
    private VFile extractImage(int arch)throws IOException {
        VFile image = getImage(arch, firmware);

        if  (image == null) {
            // the 6xxx series IPms use a bundle called image that
            // uses a type of T_FILE_IMAGE6, so we wouldn't find it
            // above. If that is the case, try again.

            if (sxid.equals("sxid=6350") || sxid.equals("sxid=6410")) {
                image = getImage6(arch, firmware);
            }
            if (image == null) {
                throw new
                IOException
                ("The firmware file does not support this device.");
            }
        }
        return image;
    }

    /**
     * Get the virtual FS file representing the root image file for the
     * architecture.
     * 
     * @param arch The architecture (one of FirmwareBundle.T_Arch.*).
     * @param bundle The bundle to extract from.
     * @return The file for the root image, or <tt>null</tt> if not found.
     */
    private static VFile getImage(int arch, FirmwareBundle bundle) {
        return bundle.find(arch, FirmwareBundle.T_FILE_IMAGE, null);
    }

    /**
     * Get the virtual FS file representing the root image file for the
     * architecture. This uses the IMAGE6 type.
     * 
     * @param arch The architecture (one of FirmwareBundle.T_Arch.*).
     * @param bundle The bundle to extract from.
     * @return The file for the root image, or <tt>null</tt> if not found.
     */
    private static VFile getImage6(int arch, FirmwareBundle bundle) {
        return bundle.find(arch, FirmwareBundle.T_FILE_IMAGE6, null);
    }

    /**
     * Get the file representing the boot image file for the architecture.
     * 
     * @param arch The architecture (one of FirmwareBundle.T_Arch.*).
     * @param bundle The bundle to extract from.
     * @return The file for the boot image, or <tt>null</tt> if not found.
     */
    private static VFile getBImage(int arch, FirmwareBundle bundle) {
        return bundle.find(arch, FirmwareBundle.T_FILE_IMAGEB, null);
    }

    /**
     * Extract the boot image for the proper architecture from the bundle, or throw an exception.
     * 
     * @param arch One of FirmwareBundle.T_Arch_*
     * @return The file with the boot image data.
     * @throws IOException If a matching boot image could not be found.
     */
    private VFile extractBootImage(int arch)throws IOException {
        VFile bimage = getBImage(arch, firmware);

        if  (bimage == null) {
            throw new
            IOException
            ("The firmware file does not support this device.");
        } return bimage;
    }

    /**
     * Get the file representing the config image file for the architecture.
     * 
     * @param arch The architecture (one of FirmwareBundle.T_ARCH_*).
     * @param bundle The bundle to extract from.
     * @return The file for the config image, or <tt>null</tt> if not found.
     */
    private static VFile getCImage(int arch, FirmwareBundle bundle) {
        return bundle.find(arch, FirmwareBundle.T_FILE_IMAGEC, null);
    }

    /**
     * Extract the config image for the proper architecture from the bundle.
     * 
     * @param arch One of FirmwareBundle.T_ARCH_*.
     * @return The file with the config image data, or <tt>null</tt> if there
     *   was no config image in the bundle.
     */
    private VFile extractConfigImage(int arch) {
        VFile cimage = getCImage(arch, firmware);

        // This one is allowed to be null
        return cimage;
    }

    /**
     * Get the architecture of the switch.
     * 
     * @param uboot The UBoot communicator connected to the switch.
     * @return The architecture, as one of the FirmwareBundle.T_Arch_* values.
     * @throws IOException If there was a problem communicating with the switch.
     */
    public int getArch(UBootCommunicator uboot) throws IOException {
        String board;
        int arch;

        try {
            board = uboot.getVariable("board");
            // Match the model to known values
            if (board.indexOf("sxni9260") != -1) {
                arch = FirmwareBundle.T_ARCH_ARM9;
            }
            else if (board.indexOf("nglsw") != -1) {
                arch = FirmwareBundle.T_ARCH_ARM9;
            }
            else if (board.indexOf("SXNI855T") != -1) {
                // We could also check for "stdin=" in the entire printenv output to
                // match PPC, but since PPC is not supported any more and that is
                // unlikely to show up anyway, don't bother
                arch = FirmwareBundle.T_ARCH_PPC;
            }
            else if (board.indexOf("sxni79524") != -1) {
                arch = FirmwareBundle.T_ARCH_ARM;
            }
            else if (board.indexOf("sxni8313") != -1) {
                arch = FirmwareBundle.T_ARCH_PPC_8313;
            }
            else {
                throw new IOException("Unrecognized board: " + board);
            }
        }
        catch(IOException e) {
            // If the exception message contains 'Error:
            // "board" not defined', then we're can infer that
            // we're an IPm (because they don't have board
            // variable set) and therefore a PPC CPU. If not,
            // then it's some other error, and we should //
            // rethrow it.
            if (e.getMessage().
                indexOf("Error: \"board\" not defined") != -1) {
                arch = FirmwareBundle.T_ARCH_PPC;
            }
            else {
                throw e;
            }
        }

        return arch;
    }

    /**
     * Get the layout version of the switch
     *
     * @param uboot The UBoot communicator connected to the switch.
     * @param arch The architecture of the switch (so we can set a
     *             sane default if nothing is defined)
     * @return The layout version, defaulting to 1 if not defined
     * @throws IOException If there was a problem communicating with the switch.
     */
    private String getLayoutVersion(UBootCommunicator uboot, int arch) 
        throws IOException {
        String layoutVersion = "1";
        
        try {
            layoutVersion = uboot.getVariable("layoutVersion");
        }
        catch(IOException ioe) {
            // Ignore it, this means it was not defined
            // For the new IPm, if the layoutVersion wasn't set (or
            // was explictly 1), set it to 4, which is the default for
            // this guy (it was never shipped with a layout of 1)
            if (arch == FirmwareBundle.T_ARCH_PPC_8313) {
                layoutVersion = "4";
            }
        }
        return layoutVersion;
    }

    /**
     * Set up the necessary UBoot environment variable to perform a load.
     * 
     * @param uboot The UBoot communicator connected to the switch.
     * @throws IOException If there was a problem communicating with the switch.
     */
    private void setupSwitch(UBootCommunicator uboot) throws IOException {
        ui.updateState(FirmwareLoaderUI.S_SETUP);

        String localIP = getLocalIP();

        uboot.setVariable("serverip", localIP);
        uboot.setVariable("gatewayip", getSwitchGateway());
        uboot.setVariable("netmask", getSwitchSubnet());
        uboot.setVariable("ipaddr", getSwitchAddress());
        uboot.setVariable("use_dhcp", "0");
        uboot.setVariable("use_net_settings", "1");
        uboot.setVariable("cfgPHYdelay", "200");

        uboot.flushVariables();

        if  (Thread.interrupted()) {
            throw new InterruptedOperationException();
        }}

    /**
     * Generate a unique name for a file.
     * 
     * @param firmware The firmware bundle containing the file.
     * @param arch The architecture of the file.
     * @param basename The base name of the file (ex: 'image' or 'imageb').
     * @return A unique name for the file.
     */
    private static String generateName(FirmwareBundle
                                       firmware, int arch,
                                       String basename) {
        return String.format("%x.%s.%s.%s.%s", Utils.srand.nextLong(),  // Guarantees that we will not step on someone else's image
                             basename,
                             firmware.getFWVersion(),
                             firmware.translateUserType(arch),
                             (firmware.getFWVendor().length() >
                              0 ? firmware.getFWVendor() : "sx"));
    }

    /**
     * Get unique name for the root image file.
     * 
     * @param firmware The firmware bundle containing the file.
     * @param arch The architecture of the file.
     * @return A unique name for the file.
     */
    private String getImageName(FirmwareBundle firmware, int arch) {
        return generateName(firmware, arch, "image");
    }

    /**
     * Get a unique name for the boot image file.
     * 
     * @param firmware The firmware bundle containing the file.
     * @param arch The architecture of the file.
     * @return A unique name for the file.
     */
    private String getImageBName(FirmwareBundle firmware, int arch) {
        return generateName(firmware, arch, "imageb");
    }

    /**
     * Get a unique name for the config image file.
     * 
     * @param firmware The firmware bundle containing the file.
     * @param arch The architecture of the file.
     * @return A unique name for the file.
     */
    private String getImageCName(FirmwareBundle firmware, int arch) {
        return generateName(firmware, arch, "imagec");
    }

    /**
     * Load images into the switch.
     * 
     * @param arch The architecture of the switch.
     * @param layoutVersion The layout version the switch expects
     * @param uboot The UBoot communicator connected to the switch.
     * @param sizes [0] = boot or user image size, [1] = root image size (bytes), [2] =
     *   config image size (if present)..
     * @param bootImageName The file name to pass to the switch as the boot image
     *   name.
     * @param rootImageName The file name to pass to the switch as the root image
     *   name.
     * @param imageCName The file name to pass to the switch as the config image
     *   name (or <tt>null</tt> if there was none in the bundle).
     * @throws IOException If there was a problem communicating with the switch.
     */
    private void loadImages(int arch, String layoutVersion, UBootCommunicator uboot,
                            int[]sizes, String bootImageName,
                            String rootImageName, String imageCName)
        throws IOException {
        long tempRAMAddr;
        long fsSize = 0;
        long bootTarget = 0xffffffff;
        long rootTarget = 0xffffffff;
        long configTarget = 0x80000;    // May not be used

        // These are only used if loading both sides of a new layout switch
        long bootTarget2 = 0;
        long rootTarget2 = 0;

        int side = -1;

        if  (arch == FirmwareBundle.T_ARCH_ARM9) {
            // NOTE: There is one ARM9 product with 16M of flash instead of 32M
            //   Loading still works on it, but the erase step prints an error
            //   that is ignored; if it becomes an issue, we need to detect that
            //   product and use 'fsSize = 0xf80000'

            tempRAMAddr = 0x20100000;   // 513 M

            if (layoutVersion.equals("1")) {
                fsSize = 0x1f80000;     // 31.5 M

                bootTarget = 0x80000;   // 512 K
                rootTarget = 0x280000;  // 2.5 M
            }
            else if (layoutVersion.equals("2") ||
                     layoutVersion.equals("99")) {
                // Get the partition layout and parse it (it may not be what we
                // expect, at least for a development unit)
                String mtdparts = uboot.getVariable("mtdparts.linux");
                List < MTDPartsParser.Partition > parts =
                MTDPartsParser.parseMTDParts(mtdparts);
                Map < String, MTDPartsParser.Partition > partMap =
                MTDPartsParser.makePartMap(parts, "sxni9260.0");

                // We do require that each boot/root pair is of equal size, so
                // find the first pair and figure out its combined size
                fsSize =
                partMap.get("boot1").length +
                partMap.get("root1").length;

                if (loadBothSides) {
                    // Loading both sides, initialize the variables so Java is
                    // happy (they will not be used)
                    bootTarget = partMap.get("boot1").offset;
                    rootTarget = partMap.get("root1").offset;

                    // a 99 version doesn't have these, so trying to get them would
                    // cause an issue.
                    if (!layoutVersion.equals("99")) {
                        bootTarget2 = partMap.get("boot2").offset;
                        rootTarget2 = partMap.get("root2").offset;
                    }
                }
                else {
                    // Are we loading to side 1 or side 2?
                    String activeInstall =
                    uboot.getVariable("activeInstall");

                    if (activeInstall.equals("1")) {
                        // Currently on side 1, load side 2
                        bootTarget = partMap.get("boot2").offset;
                        rootTarget = partMap.get("root2").offset;

                        side = 2;
                    }
                    else if (activeInstall.equals("2")) {
                        // Currently on side 2, load onto side 1
                        bootTarget = partMap.get("boot1").offset;
                        rootTarget = partMap.get("root1").offset;

                        side = 1;
                    }
                    else {
                        // Unknown which side we are on, fail
                        throw new
                        IOException
                        ("Unable to determine active partition: " +
                         activeInstall);
                    }
                }

                if (applyConfigImage && imageCName != null) {
                    // Going to apply a config image, erase that partition now
                    uboot.eraseRange(partMap.get("config").offset, partMap.get("config").length);
                }
            }
            else if (layoutVersion.equals("3")) {
                // Get the partition layout and parse it (it may not be what we
                // expect, at least for a development unit)
                String mtdparts = uboot.getVariable("mtdparts");
                List < MTDPartsParser.Partition > parts =
                MTDPartsParser.parseMTDParts(mtdparts);
                Map < String, MTDPartsParser.Partition > partMap =
                MTDPartsParser.makePartMap(parts, "sxni9260.0");

                // We do require that each boot/root pair is of equal size, so
                // find the first pair and figure out its combined size
                fsSize =
                    partMap.get("boot1").length +
                    partMap.get("root1").length;

                bootTarget = partMap.get("boot1").offset;
                rootTarget = partMap.get("root1").offset;

                if (applyConfigImage && imageCName != null) {
                    // Going to apply a config image, erase that partition now
                    uboot.eraseRange(partMap.get("config").offset, partMap.get("config").length);
                }
            }
            else if (layoutVersion.equals("5")) {
                // this layout is 100% ubi, and requires no explicit "erase"
                // step.  The UBI subsystem takes care of that.
                tempRAMAddr =
                    Long.parseLong(uboot.getVariable("loadaddrram")
                                        .replace("0x",""), 16);
            }
            else {
                // Unknown layout version
                throw new IOException
                    ("Unrecognized filesystem layout version: " +
                     layoutVersion);
            }
        }
        else if (arch == FirmwareBundle.T_ARCH_PPC) {
            tempRAMAddr = 0x100000;     // 1 M
            // there are several different flavors of IPm, with
            // different amounts of flash. The most common one has 16M.
            // See trac.sixnetio.com/trac/wiki/IPmDifferences
            // Also of note - the actual sxid line is "sxid=6350", so
            // the whole U-Boot reply is "sxid=sxid=6350".
            fsSize = 0x1000000; // 16 M
            // 6350 and 6410 have 128MB
            if (sxid.equals("sxid=6350") || sxid.equals("sxid=6410")) {
                fsSize = 0x8000000; // 128 M
            }
            // 5410, 5410_135u and 5410_245u have 64M
            else if (sxid.indexOf("5410") != -1) {
                fsSize = 0x4000000; // 64 M
            }
            // 3410, 3410_113 and 3410_213 have 32M
            else if (sxid.indexOf("3410") != -1) {
                fsSize = 0x2000000; // 32 M
            }

            bootTarget = 0x0;   // 0 M
            rootTarget = 0x200000;      // 2 M
        }
        else if (arch == FirmwareBundle.T_ARCH_ARM) {
            tempRAMAddr = 0x100000;     // 1 M
            fsSize = 0xf80000;  // 15.5 M

            bootTarget = 0x80000;       // 512 K
            rootTarget = 0x280000;      // 2.5 M
        }
        else if (arch == FirmwareBundle.T_ARCH_PPC_8313) {
            tempRAMAddr = 0x100000;
            fsSize = 0;
            rootTarget = 0;
            configTarget = 0;
            bootTarget = 0;
            setApplyConfigImage(true);
        }
        else {
            throw new
            IllegalArgumentException("Unrecognized architecture: " +
                                     arch);
        }

        uboot.setRamAddress(tempRAMAddr);

        // Do the erase manually, so if only one file loads the switch
        // will not boot with something wrong
        // This is skipped for layout version 4, as it uses the UBI
        // load commands.
        if (layoutVersion.equals("3") ||
            layoutVersion.equals("2") ||
            layoutVersion.equals("1")) {
            if (layoutVersion.equals("2") && loadBothSides) {
                // Get the partition layout and parse it (it may not be what we
                // expect, at least for a development unit)
                String mtdparts = uboot.getVariable("mtdparts.linux");
                List < MTDPartsParser.Partition > parts =
                MTDPartsParser.parseMTDParts(mtdparts);
                Map < String, MTDPartsParser.Partition > partMap =
                MTDPartsParser.makePartMap(parts, "sxni9260.0");
                // Erase the whole thing from boot1 through root2
                // boot1 is first, so we can use that offset.
                uboot.eraseRange(partMap.get("boot1").offset, 2 * fsSize);

            }
            else {
                // Erase one side, from bootX through rootX
                uboot.eraseRange(bootTarget, fsSize);
            }
        }

        ProgressListener progressListener = new ProgressListener(){
                boolean end = false;

                @Override public void updateProgress(float percent) {
                    if (end && percent < 1.0) {
                        // Dropped below 100% after being above? New transfer
                        end = false;
                    }

                    // Signal the end of the transfer when we hit 100%
                    if (!end) {
                        ui.tftpProgress(percent * 100);

                        if (percent >= 1.0) {
                            end = true;
                            ui.tftpProgress(-100.0f);
                        }
                    }
                }
            };

        ui.updateState(FirmwareLoaderUI.S_CONFIG_IMAGE);
        if (applyConfigImage && imageCName != null) {
            if (layoutVersion.equals("2") || layoutVersion.equals("3") ||
                layoutVersion.equals("99")) {
                // This is tiny, don't pass a progress listener
                uboot.loadFile(imageCName, false, sizes[2], configTarget, 0,
                               null);
            }
            else if (layoutVersion.equals("4")) {
                // UBI
                // This is tiny, don't pass a progress listener
                uboot.loadFile(imageCName, false, sizes[2], "nand0,3", "cfgfs", 
                               null);
            }
            else if (layoutVersion.equals("5")) {
                // UBI
                uboot.loadTFTP(imageCName, false, sizes[2], progressListener);
                uboot.execCommand(
                    "run erasepersist && " +
                    "run load_config_common");
            }
            // else, this layout doesn't support this image, we
            // should never have gotten here.
        }

        // Now load the root filesystem first, so no booting can happen
        ui.updateState(FirmwareLoaderUI.S_LOAD_IMAGE);
        if (layoutVersion.equals("4")) {
            // UBI
            uboot.loadFile(rootImageName, false, sizes[1], "nand0,1", "rootfs",
                           progressListener);
        }
        else if (layoutVersion.equals("5")) {
            // UBI with u-boot scripts, always loads both sides.
            uboot.loadTFTP(rootImageName, false, sizes[1], progressListener);
            uboot.execCommand(
                "run eraseroot && " +
                "setenv side 2 && " +
                "run set_side && " +
                "run load_linux_common && " +
                "setenv side 1 && " +
                "run set_side && " +
                "run load_linux_common");
        }
        else {
            uboot.loadFile(rootImageName, false, sizes[1], rootTarget, 0,
                           progressListener);
        }

        if (loadBothSides) {
            if (layoutVersion.equals("2")) {
                // UBootCommunicator says that it leaves the file at
                // the temporary RAM address in case we need to do
                // more with it, so let's copy it
                // to the second root location now
                uboot.execCommand(String.
                                  format("nand write.e %x %x $(filesize)",
                                         uboot.getRamAddress(),
                                         rootTarget2));
            }
            else if (layoutVersion.equals("4")) {
                // UBI
                uboot.loadFile(rootImageName, false, sizes[1], "nand0,2", 
                               "rootfs", progressListener);
            }
            // else, this layout doesn't support this image, we
            // should never have gotten here.
        }

        // Layout version 4 lacks a boot image, and does not yet
        // switch between A and B firmware.
        if (layoutVersion.equals("3") ||
            layoutVersion.equals("2") ||
            layoutVersion.equals("1")) {
            ui.updateState(FirmwareLoaderUI.S_LOAD_BOOT_IMAGE);
            uboot.loadFile(bootImageName, false, sizes[0], bootTarget, 0,
                           progressListener);

            if (layoutVersion.equals("2") && loadBothSides) {
                // UBootCommunicator says that it leaves the file at
                // the temporary RAM address in case we need to do
                // more with it, so let's copy it to the second boot
                // location now
                uboot.execCommand(String.
                                  format("nand write.e %x %x $(filesize)",
                                         uboot.getRamAddress(),
                                         bootTarget2));
            }
            else if (side != -1) {
                // Only flip to the other side ('side' is the target
                // side) if we did not load both sides
                uboot.setVariable("mtdparts", "$(mtdparts." + side + ")");
                uboot.setVariable("mrootargs", "$(mrootargs." + side + ")");
                uboot.setVariable("activeInstall", "" + side);
                uboot.flushVariables();
            }
        }
    }

    /**
     * Retrieve the local IP address from the UI. Default to the IP address
     * of the local host if unavailable.
     * 
     * @return The local IP address.
     * @throws IOException If there was a problem retrieving the IP address
     * of the local host.
     */
    private String getLocalIP() throws IOException {
        String localAddr = ui.getTFTPAddr();

        if  (localAddr != null) {
            return localAddr;
        }
        else {
            // What is our primary IP address?
            InetAddress addr = InetAddress.getLocalHost();

            return addr.getHostAddress();
        }
    }

    /**
     * Wait for the web server on the switch to start listening on port 80.
     * 
     * @throws IOException If there was a problem pinging the switch prior to
     *   waiting for the connection to open.
     * @throws TimeoutException If the request timeout is reached before the
     *   switch starts responding.
     */
    private void waitForHTTP()
        throws IOException, TimeoutException {
        FWLoadUtil.waitForHTTP(getLocalIP(), swAddr,
                               getResponseTimeout());
    } private void deleteFiles() {
        for (Iterator < File > itFiles = filesToDelete.iterator();
             itFiles.hasNext();) {
            File f = itFiles.next();

            if (!f.delete()) {
                logger.warn("Unable to delete file '" +
                            f.getAbsolutePath() + "'");
            }
            else {
                itFiles.remove();       // Don't try again later
            }
        }
    }
}

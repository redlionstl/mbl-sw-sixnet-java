
/*
 * FWLoadUtil.java
 *
 * Functions common to both firmware loading schemes. This is incomplete, and
 * should probably be made into a superclass of the two firmware loader classes
 * at some point.
 *
 * Jonathan Pearson
 * June 30, 2010
 *
 */

package com.sixnetio.Switch;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import com.sixnetio.util.*;

public class FWLoadUtil {
    private static final Logger logger =
    Logger.getLogger(Utils.thisClassName());

    /**
     * Wait for the web server on a remote machine to start listening on port
     * 80.
     * 
     * @param localIP The IP address for the local interface which is connected
     *   to the remote machine, or <code>null</code> if any interface may be
     *   used.
     * @param remoteIP The IP address of the remote machine.
     * @param timeout Milliseconds before timing out. Pass -1 to skip the whole
     *   test, 0 to never time out.
     * @throws IOException If there was a problem pinging the switch prior to
     *   waiting for the connection to open.
     * @throws TimeoutException If the request timeout is reached before the
     *   switch starts responding.
     */
    public static void waitForHTTP(String localIP, String remoteIP,
                                   long timeout)
        throws IOException, TimeoutException {
        NetworkInterface localIfc = null;
        SocketAddress localSocketAddr = null;

        InetAddress swInetAddr;
        SocketAddress swSocketAddr;

        boolean timeoutEnabled = (timeout != 0);
        long endTime;
        boolean pingSuccess = false;
        boolean httpSuccess = false;

        boolean doBind = true;  // Whether to bind the socket before connecting

        if  (timeout < 0) {
            // Do not even bother testing
            return;
        } if (timeoutEnabled) {
            logger.info(String.
                        format("Giving the switch %,dms to respond",
                               timeout));
        }
        else {
            logger.
            info("Waiting as long as necessary for the switch to" +
                 " respond");
        }

        if (localIP != null) {
            InetAddress localInetAddr = InetAddress.getByName(localIP);

            localIfc = NetworkInterface.getByInetAddress(localInetAddr);
            localSocketAddr = new InetSocketAddress(localInetAddr, 0);

            logger.debug(String.format("Pinging from %s[%s] locally",
                                       localIfc.getDisplayName(),
                                       localInetAddr.getHostAddress()));
        }
        else {
            logger.debug("Pinging from any interface/address locally");
        }

        swInetAddr = InetAddress.getByName(remoteIP);
        swSocketAddr = new InetSocketAddress(swInetAddr, 80);

        logger.debug(String.format("Pinging to %s remotely",
                                   swInetAddr.getHostAddress()));

        endTime = System.currentTimeMillis() + timeout;

        do {
            // Try for 2 seconds
            pingSuccess = swInetAddr.isReachable(localIfc, 0, 2000);

            if (!pingSuccess) {
                logger.
                debug
                ("ICMP Ping failed, trying again in 4 seconds");
                Utils.sleep(4000);
            }
        } while (!pingSuccess &&
                 (endTime > System.currentTimeMillis() ||
                  !timeoutEnabled));

        if (!pingSuccess) {
            logger.error("ICMP Ping failed, timed out");
            throw new TimeoutException("No response to ICMP ping");
        }

        logger.info("ICMP Ping succeeded, switching to port 80 scans");

        // Now try to connect to the HTTP server

        // Test whether to bind the socket before connecting (depends on whether
        // we have an address to bind to, and also on OS and version)
        if (localSocketAddr == null) {
            doBind = false;
        }
        else if (Utils.osIsWindows()) {
            Version maxVer = new Version("5.1");        // Windows XP
            Version osVer = Utils.osVersion();
            if (osVer.compareTo(maxVer) > 0) {
                // Doesn't work on versions of Windows past XP; some weird error
                // about the connection address/port being invalid
                // This *should* be safe, because we just succeeded in pinging
                // the address through the specific interface, so the address
                // should be in the ARP table and will be routed correctly
                logger.debug("Windows v" + osVer.toString() +
                             " does not" +
                             " support bind before connect");
                doBind = false;
            }
        }

        do {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            try {
                Socket sock = new Socket();

                if (doBind) {
                    sock.bind(localSocketAddr);
                }

                sock.connect(swSocketAddr, 2000);       // 2 second timeout
                sock.close();

                httpSuccess = true;
            }
            catch(IOException ioe) {
                logger.
                debug("HTTP Ping failed, trying again in 4 seconds",
                      ioe);
                Utils.sleep(4000);
            }
        } while (!httpSuccess &&
                 (endTime > System.currentTimeMillis() ||
                  !timeoutEnabled));

        if (!httpSuccess) {
            logger.error("HTTP Ping failed, timed out");
            throw new TimeoutException("No response to HTTP ping");
        }

        logger.info("HTTP Ping succeeded");
    }
}

/*
 * DeviceID.java
 *
 * Represents an ESN or IMEI.
 *
 * Jonathan Pearson
 * August 25, 2009
 *
 */

// FIXME: Does this properly handle ESNs with 14-bit manufacturer IDs? I could
// find no reference for how those should be displayed

package com.sixnetio.Modem;

import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;


/**
 * Represents an ESN, IMEI, or MEID in a shared numeric format.
 * 
 * @author Jonathan Pearson
 */
public class DeviceID
{
    private static final Logger log = Logger.getLogger(Utils.thisClassName());

    // See Notes.txt in this same directory for details on how the three formats
    // work

    /**
     * When you want to represent a device ID that is unknown but <tt>null</tt>
     * is unavailable, use this value.
     */
    public static final DeviceID DEVID_UNKNOWN = new DeviceID(-1);

    /**
     * Mask the raw value with this, shift right {@link #SHIFT} bits, and cast
     * to <code>int</code> to test for what type it is. We use only the top
     * three bits to indicate the type, allowing for 8 types including legacy
     * and unknown. The largest device ID type is MEID, which is 54 bits, so
     * this also leaves room for a bit of expansion in both directions (up to
     * {@link #SHIFT} bits of ID, or a number of other formats). Outside callers
     * should use {@link #getTypeIndicator(long)}.
     */
    private static final long MASK_TYPE = 0xe000000000000000L;

    /**
     * Number of bits to shift by when using {@link #MASK_TYPE} and the TYPE_*
     * constants.
     */
    private static final int SHIFT = 61;

    /**
     * The ID is in the old shared format, if the masked raw value is equal to
     * this.
     */
    public static final int TYPE_OLD = 0x00;

    /** The ID is an ESN, if the masked raw value is equal to this. */
    public static final int TYPE_ESN = 0x01;

    /** The ID is an IMEI, if the masked raw value is equal to this. */
    public static final int TYPE_IMEI = 0x02;

    /** The ID is an IMEISV, if the masked raw value is equal to this. */
    public static final int TYPE_IMEISV = 0x03;

    /** The ID is an MEID, if the masked raw value is equal to this. */
    public static final int TYPE_MEID = 0x04;

    /** The ID type is unknown, if the masked raw value is equal to this. */
    public static final int TYPE_UNKNOWN = 0x07;

    /** Matches strings containing only the digits 0-9. */
    private static final Pattern BASE10_NOPUNCT = Pattern.compile("[0-9]+");

    /**
     * Matches strings containing only the digits 0-9 and the letters A-F,
     * ignoring case.
     */
    private static final Pattern BASE16_NOPUNCT =
        Pattern.compile("[a-fA-F0-9]+");

    /**
     * The shared format of the ESN, IMEI, or MEID. The largest of these is only
     * 56 bits, allowing us to mask off a few of the highest bits to specify the
     * type of ID.
     */
    private final long deviceID;

    /**
     * Construct a new DeviceID using the shared format.
     * 
     * @param deviceID The shared format DeviceID. If it does not specify a
     *            type, one will be guessed.
     */
    public DeviceID(long deviceID)
    {
        if (getTypeIndicator(deviceID) == TYPE_OLD) {
            // IMEIs and MEIDs will always have bits set above bit 32
            // IMEIs and MEIDs should always have byte 7 set, ESNs never
            // MEIDs will always have byte 7 between A0 and FF, IMEIs less than
            // that
            // Use an int so we don't need to worry about signs
            int b7 = (int)((deviceID >> 48) & 0xff);
            if ((deviceID & 0xffffffff00000000L) == 0) {
                // ESN
                this.deviceID = stringToDevID(Long.toString(deviceID, 16));
            }
            else if (b7 < 0xa0) {
                // IMEI
                // FUTURE: Figure out how to determine IMEI vs. IMEISV
                // Need to shift a little to keep binary compatibility with
                // IMEISV, which saves the software version in the low 7 bits
                this.deviceID = stringToDevID(Long.toString(deviceID, 10));
            }
            else {
                // MEID
                this.deviceID = stringToDevID(Long.toString(deviceID, 16));
            }
        }
        else {
            this.deviceID = deviceID;
        }
    }

    /**
     * Get a BEP-formatted long value representing this device ID. This format
     * is discouraged, as it relies on the bit patterns to determine the type of
     * a device ID. Only use this format when necessary (i.e. in a BEP message).
     * 
     * @return A raw device ID as a long. Not a recommended format.
     */
    public long asBEPLong()
    {
        int type = getTypeIndicator(deviceID);
        switch (type) {
            case TYPE_UNKNOWN:
            case TYPE_OLD:
                // Should not happen, but let it pass
                return deviceID;

            case TYPE_ESN:
                // Very simple format, without even a Luhn check digit
                return (deviceID & ~MASK_TYPE);

            case TYPE_IMEI: {
                long a = (deviceID >> 47) & 0x7f;
                long b = (deviceID >> 27) & 0x0fffff;
                long c = (deviceID >> 7) & 0x0fffff;

                String s = String.format("%02d%06d%06d", a, b, c);
                long d = luhnCalculate(s, 10);
                s += String.format("%01d", d);

                return Long.parseLong(s, 10);
            }

            case TYPE_IMEISV:
                throw new UnsupportedOperationException(
                    "BEP device ID format for IMEISV is not supported");

            case TYPE_MEID:
                throw new UnsupportedOperationException(
                    "BEP device ID format for MEID is not supported");

            default:
                throw new RuntimeException("Unrecognized deviceID type: " +
                                           getTypeIndicator(deviceID) +
                                           " (deviceID = " + deviceID + ")");
        }
    }

    /**
     * Parse a BEP-formatted long value into a device ID.
     * 
     * @param deviceID The BEP-formatted long.
     * @return A device ID.
     */
    public static DeviceID fromBEPLong(long deviceID)
    {
        return new DeviceID(deviceID);
    }

    /**
     * Parse an ESN or IMEI string into a DeviceID.
     * 
     * @param deviceID The string representation of an ESN or IMEI.
     * @return A DeviceID.
     * @throws NumberFormatException If the value cannot be parsed.
     */
    public static DeviceID parseDeviceID(String deviceID)
        throws NumberFormatException
    {
        return new DeviceID(stringToDevID(deviceID));
    }

    /**
     * Get a string representation of this device ID. This will return the
     * hexadecimal style for ESNs, and the 15-digit style of IMEIs. Includes
     * separator characters.
     */
    @Override
    public String toString()
    {
        return devIDToString(deviceID);
    }

    /**
     * Same as {@link #toString()}, but gives control over whether to include
     * separator characters.
     * 
     * @param withSeparators True to include separator characters. False to not
     *            include them.
     * @return This device ID in string format.
     */
    public String toString(boolean withSeparators)
    {
        return devIDToString(deviceID, withSeparators);
    }

    /**
     * Get a string representation of this device ID. This will return the 3+8
     * style for ESNs, and the 15-digit style of IMEIs. Includes separator
     * characters.
     */
    public String toAlternateString()
    {
        return devIDToStringAlternate(deviceID);
    }

    /**
     * Same as {@link #toAlternateString()}, but gives control over whether to
     * include separator characters.
     * 
     * @param withSeparators True to include separator characters. False to not
     *            include them.
     * @return This device ID in string format.
     */
    public String toAlternateString(boolean withSeparators)
    {
        return devIDToStringAlternate(deviceID, withSeparators);
    }

    /**
     * Get this device ID as a long in the shared format. This format includes
     * type information, making it easy to distinguish between device IDs of
     * various types. When you need a numeric device ID, this is the preferred
     * format. When possible, though, you should use {@link #toString()}.
     * 
     * @return A device ID in the preferred long format.
     */
    public long asSharedLong()
    {
        return deviceID;
    }

    /**
     * Test for equality with another object. This compares the shared long
     * format of this object with the value returned by
     * {@link Number#longValue()} if the other object is a Number. Otherwise,
     * returns <tt>false</tt>.
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj != null && obj instanceof DeviceID) {
            return (obj.toString().equals(toString()));
        }
        else {
            return false;
        }
    }

    /**
     * Create a hash code for this object. Returns the XOR of the first half
     * with the second half of the shared long representation.
     */
    @Override
    public int hashCode()
    {
        int a = (int)(deviceID & 0xffffffff);
        int b = (int)((deviceID >> 32) & 0xffffffff);

        return (a ^ b);
    }

    /**
     * Given a string containing a modem device ID (ESN or IMEI), make it into a
     * long.
     * 
     * @param s The device ID string.
     * @return The device ID as a long.
     * @throws NumberFormatException If there is a problem parsing the string.
     */
    public static long stringToDevID(String s)
    {
        // Note: In formats, # means decimal and X means hex
        // All punctuation is optional, but must be in the
        // correct place if is it present

        try {
            switch (s.length()) {
                case 8: {
                    // ESN: XXXXXXXX
                    return parseESN(s, 16, false);
                }

                case 9: {
                    // ESN: #########
                    return parseESN(s, 10, false);
                }

                case 11: {
                    // ESN: ###-######## (without punctuation)
                    // First three digits are the high byte, rest are the lower
                    // three
                    return parseESN(s, 10, true);
                }

                case 12: {
                    // ESN: ###-######## (with punctuation)
                    // First three digits are the high byte, rest are the lower
                    // three
                    return parseESN(strip(s, "3-8", 10), 10, true);
                }

                case 14: {
                    // MEID: XXXXXXXXXXXXXX
                    // First digit must be in the range A-F (checked by parse)
                    return parseMEID(s, 16, false);
                }

                case 15: {
                    char first = Character.toLowerCase(s.charAt(0));
                    if ('a' <= first && first <= 'f') {
                        // MEID: XXXXXXXXXXXXXXX
                        // First digit must be in the range A-F
                        // Last digit is a base-16 Luhn check digit
                        return parseMEID(s, 16, true);
                    }
                    else {
                        // IMEI: ##-######-######-# (without punctuation)
                        // Last digit is a base-10 Luhn check digit
                        return parseIMEI(s);
                    }
                }

                case 16: {
                    // IMEISV: ##-######-######-## (without punctuation)
                    return parseIMEISV(s);
                }

                case 18: {
                    if (BASE10_NOPUNCT.matcher(s).matches()) {
                        // MEID: #####-#####-####-#### (without punctuation)
                        return parseMEID(s, 10, false);
                    }
                    else {
                        // IMEI: ##-######-######-# (with punctuation)
                        // Last digit is a base-10 Luhn check digit
                        return parseIMEI(strip(s, "2-6-6-1", 10));
                    }
                }

                case 19: {
                    if (BASE10_NOPUNCT.matcher(s).matches()) {
                        // MEID: #####-#####-####-####-# (without punctuation)
                        // Last digit is a base-10 Luhn check digit
                        return parseMEID(s, 10, true);
                    }
                    else {
                        // IMEISV: ##-######-######-## (with punctuation)
                        // Last two digits are software version, discard
                        return parseIMEISV(strip(s, "2-6-6-2", 10));
                    }
                }

                case 21: {
                    // MEID: #####-#####-####-#### (with punctuation)
                    return parseMEID(strip(s, "5-5-4-4", 10), 10, false);
                }

                case 23: {
                    // MEID: #####-#####-####-####-# (with punctuation)
                    // Last digit is a base-10 Luhn check digit
                    return parseMEID(strip(s, "5-5-4-4-1", 10), 10, true);
                }

                default:
                    // Fall through
            }
        }
        catch (NumberFormatException nfe) {
            log.debug("Error parsing " + s, nfe);

            throw new NumberFormatException(
                "Bad device ID, not an ESN, IMEI, or MEID: " + s + ": " +
                    nfe.getMessage());
        }

        // If we get here, the parse failed for some other reason
        throw new NumberFormatException(
            "Bad device ID, not an ESN, IMEI, or MEID: " + s);
    }

    /**
     * Returns <code>devIDToString(devID, true)</code>.
     * 
     * @param devID The device ID in the shared long format.
     * @return The device ID as a string with separator characters.
     */
    public static String devIDToString(long devID)
    {
        return devIDToString(devID, true);
    }

    /**
     * Given a modem device ID (ESN, IMEI(SV), or MEID), convert it to a
     * properly formatted string in the most common format. If the number is an
     * ESN, this will return the hexadecimal representation. If it is an IMEI,
     * this will return the 15-digit style with dashes. If it is an IMEISV, this
     * will return the 16-digit style with dashes. If it is an MEID, this will
     * return the 19-digit style with dashes.
     * 
     * @param devID The device ID in the shared long format.
     * @param withSeparators True to include separator characters. False not to.
     * @return A properly formatted textual representation of the device ID.
     */
    public static String devIDToString(long devID, boolean withSeparators)
    {
        int mask = getTypeIndicator(devID);
        switch (mask) {
            case TYPE_OLD: {
                // We cannot be absolutely certain what type of ID this is, so
                // guess based on the rules that we used to use
                // ESNs always have 00 in byte 6, IMEIs always have something
                // there
                if ((devID & 0xff0000000000L) == 0) {
                    // ESN
                    return String.format("%08X", devID & 0xffffffffL);
                }
                else {
                    // IMEI
                    String s = String.format("%015d", devID);
                    if (withSeparators) {
                        return String.format("%s-%s-%s-%s", s.substring(0, 2),
                            s.substring(2, 8), s.substring(8, 14), s
                                .substring(14));
                    }
                    else {
                        return s;
                    }
                }
            }

            case TYPE_ESN: {
                // XXXXXXXX
                return String.format("%08X", devID & 0xffffffffL);
            }

            case TYPE_IMEI: {
                // ##-######-######-#
                int a = (int)(devID >> 47) & 0x7f; // 7 bits
                int b = (int)(devID >> 27) & 0x0fffff; // 20 bits
                int c = (int)(devID >> 7) & 0x0fffff; // 20 bits

                String temp = String.format("%02d%06d%06d", a, b, c);
                int d = luhnCalculate(temp, 10);

                if (withSeparators) {
                    return String.format("%02d-%06d-%06d-%01d", a, b, c, d);
                }
                else {
                    return String.format("%02d%06d%06d%01d", a, b, c, d);
                }
            }

            case TYPE_IMEISV: {
                // ##-######-######-##
                int a = (int)(devID >> 47) & 0x7f; // 7 bits
                int b = (int)(devID >> 27) & 0x0fffff; // 20 bits
                int c = (int)(devID >> 7) & 0x0fffff; // 20 bits
                int e = (int)(devID & 0xffffffff) & 0x7f; // 7 bits

                if (withSeparators) {
                    return String.format("%02d-%06d-%06d-%02d", a, b, c, e);
                }
                else {
                    return String.format("%02d%06d%06d%02d", a, b, c, e);
                }
            }

            case TYPE_MEID: {
                // XXXXXXXXXXXXXXX, check digit last
                String temp = String.format("%014X", devID & (~MASK_TYPE));
                int cd = luhnCalculate(temp, 16);

                return String.format("%s%X", temp, cd);
            }

            case TYPE_UNKNOWN: {
                return String.format("(Unknown: %d)", devID & (~MASK_TYPE));
            }

            default: {
                return String.format("(Unknown: %d)", devID);
            }
        }
    }

    /**
     * Returns <code>devIDToStringAlternate(devID, true)</code>.
     * 
     * @param devID The device ID to format, in the shared long format.
     * @return The device ID, as a string, with separator characters.
     */
    public static String devIDToStringAlternate(long devID)
    {
        return devIDToStringAlternate(devID, true);
    }

    /**
     * Given a modem device ID (ESN or IMEI), convert it to a properly formatted
     * string in the less common format. If the number is an ESN, this will
     * return the 3+8 style representation. If it is an MEID, this will return
     * the full hex form plus check digit with no separators. If it is an IMEI
     * or IMEISV, the primary form will be used as there is no alternate form.
     * 
     * @param devID The device ID.
     * @param withSeparators True to include separator characters. False to not
     *            include them.
     * @return A properly formatted textual representation of the device ID.
     */
    public static String devIDToStringAlternate(long devID,
                                                boolean withSeparators)
    {
        int mask = getTypeIndicator(devID);
        switch (mask) {
            case TYPE_OLD: {
                // We cannot be absolutely certain what type of ID this is, so
                // guess based on the rules that we used to use
                // ESNs always have 00 in byte 6, IMEIs always have something
                // there
                if ((devID & 0xff0000000000L) == 0) {
                    // ESN
                    // 3+8 style
                    return String.format("%03d-%08d",
                        (devID & 0xff000000L) >> 24, devID & 0xffffffL);
                }
                else {
                    // IMEI (there is no alternate style)
                    return devIDToString(devID);
                }
            }

            case TYPE_ESN: {
                // ###-######## (3+8 style)
                if (withSeparators) {
                    return String.format("%03d-%08d",
                        (devID & 0xff000000L) >> 24, devID & 0xffffffL);
                }
                else {
                    return String.format("%03d%08d",
                        (devID & 0xff000000L) >> 24, devID & 0xffffffL);
                }
            }

            case TYPE_MEID: {
                // #####-#####-####-####-#
                String rm =
                    String.format("%010d", (devID & 0x00ffffffff000000L) >> 24);
                String serial = String.format("%08d", devID & 0x00ffffffL);
                int c = luhnCalculate(rm + serial, 10);

                if (withSeparators) {
                    return String.format("%s-%s-%s-%s-%01d",
                        rm.substring(0, 5), rm.substring(5, 10), serial
                            .substring(0, 4), serial.substring(4, 8), c);
                }
                else {
                    return String.format("%s%01d", rm, c);
                }
            }

            default: {
                // Everything else goes to the primary method
                return devIDToString(devID, withSeparators);
            }
        }
    }

    /**
     * Get the type indicator of the given device ID in the shared long format.
     * 
     * @param devID The device ID in the shared long format.
     * @return The type indicator, which should equal one of the TYPE_* values.
     */
    public static int getTypeIndicator(long devID)
    {
        return (int)((devID & MASK_TYPE) >>> SHIFT);
    }

    /**
     * Parse an ESN from a string.
     * 
     * @param s The string to parse.
     * @param base The base of the digits in the string (10 or 16).
     * @param separate Whether to parse the string as separate manufacturer ID
     *            and serial number pieces. The base should be 10, and the
     *            string should be exactly 11 characters long.
     * @return The parsed ESN in the shared long format, with the masked type
     *         set.
     */
    private static long parseESN(String s, int base, boolean separate)
    {
        long result;
        if (separate) {
            long mfid = Long.parseLong(s.substring(0, 3), base);
            long sn = Long.parseLong(s.substring(3), base);

            if (mfid > 255) {
                throw new NumberFormatException("ESN manufacturer ID (" + mfid +
                                                ") is too large: " + s);
            }

            if (sn > 16777215) {
                throw new NumberFormatException("ESN serial number (" + sn +
                                                ") is too large: " + s);
            }

            result = (((long)TYPE_ESN << SHIFT) | (mfid << 24) | sn);
        }
        else {
            result = (((long)TYPE_ESN << SHIFT) | Long.parseLong(s, base));
        }

        // Verify it fits into 32 bits
        if (((result & (~MASK_TYPE)) & 0xffffffff00000000L) != 0) {
            throw new NumberFormatException("ESN is too large: " + s);
        }

        return result;
    }

    /**
     * Parse the given string as an IMEI.
     * 
     * @param s The string to parse.
     * @return The parsed IMEI in the shared long format, with the masked type
     *         set.
     */
    private static long parseIMEI(String s)
    {
        luhnCheck(s, 10);
        s = s.substring(0, s.length() - 1);

        long a = Long.parseLong(s.substring(0, 2)); // 7 bits
        long b = Long.parseLong(s.substring(2, 8)); // 20 bits
        long c = Long.parseLong(s.substring(8)); // 20 bits

        // We do not store the check digit, but we give it 7 bits to be
        // consistent with the IMEISV format, which stores the software version
        // there

        if (a > 99) {
            throw new NumberFormatException("IMEI allocator code (" + a +
                                            ") is too large: " + s);
        }

        if (b > 0xfffff) {
            throw new NumberFormatException("IMEI manufacturer/model (" + b +
                                            ") code is too large: " + s);
        }

        if (c > 0xfffff) {
            throw new NumberFormatException("IMEI serial number (" + c +
                                            ") is too large: " + s);
        }

        long result =
            (((long)TYPE_IMEI << SHIFT) | (a << 47) | (b << 27) | (c << 7));

        return result;
    }

    /**
     * Parse the given string as an IMEISV.
     * 
     * @param s The string to parse.
     * @return The parsed IMEISV in the shared long format, with the masked type
     *         set.
     */
    private static long parseIMEISV(String s)
    {
        long a = Long.parseLong(s.substring(0, 2)); // 7 bits
        long b = Long.parseLong(s.substring(2, 8)); // 20 bits
        long c = Long.parseLong(s.substring(8, 14)); // 20 bits
        long e = Long.parseLong(s.substring(14)); // 7 bits

        if (a > 99) {
            throw new NumberFormatException("IMEISV allocator code (" + a +
                                            ") is too large: " + s);
        }

        if (b > 0xfffff) {
            throw new NumberFormatException("IMEISV manufacturer/model (" + b +
                                            ") code is too large: " + s);
        }

        if (c > 0xfffff) {
            throw new NumberFormatException("IMEISV serial number (" + c +
                                            ") is too large: " + s);
        }

        if (e == 99) {
            throw new NumberFormatException("IMEISV software version (" + e +
                                            ") is illegal: " + s);
        }
        else if (e > 99) {
            throw new NumberFormatException("IMEISV software version (" + e +
                                            ") is too large: " + s);
        }

        long result =
            (((long)TYPE_IMEISV << SHIFT) | (a << 47) | (b << 27) | (c << 7) | e);

        return result;
    }

    /**
     * Parse the given string as a MEID.
     * 
     * @param s The string to parse.
     * @param base The base of the digits in the string (10 or 16).
     * @param checkDigit Whether the string includes a Luhn check digit.
     * @return The parsed MEID in the shared long format, with the masked type
     *         set.
     */
    private static long parseMEID(String s, int base, boolean checkDigit)
    {
        if (base == 16) {
            // First digit of a hex MEID must be in the range A-F
            char first = Character.toLowerCase(s.charAt(0));
            if (first < 'a' || first > 'f') {
                throw new NumberFormatException(
                    "Not a valid MEID, first digit is not in the range A-F: " +
                        s);
            }
        }

        if (checkDigit) {
            luhnCheck(s, base);
            s = s.substring(0, s.length() - 1);
        }

        long mfid;
        long sn;

        if (base == 10) {
            // #####-#####-####-####, first two groups form one 10-digit
            // grouping representing 32 bits; last two groups form one 8-digit
            // grouping representing 24 bits
            mfid = Long.parseLong(s.substring(0, 10), base);
            sn = Long.parseLong(s.substring(10), base);
        }
        else {
            mfid = Long.parseLong(s.substring(0, 8), base);
            sn = Long.parseLong(s.substring(8, 14), base);
        }

        if (mfid < 0xa0000000L) {
            throw new NumberFormatException(
                "MEID regional/manufacturer code (" + mfid +
                    ") is too small: " + s);
        }
        else if (mfid > 0xffffffffL) {
            throw new NumberFormatException(
                "MEID regional/manufacturer code (" + mfid +
                    ") is too large: " + s);
        }

        if (sn > 0xffffffL) {
            throw new NumberFormatException("MEID serial number (" + sn +
                                            ") is too large: " + s);
        }

        long result = (((long)TYPE_MEID << SHIFT) | (mfid << 24) | sn);

        return result;
    }

    /**
     * Remove all characters from the given string that are not digits of the
     * given base.
     * 
     * @param s The string to strip.
     * @param pattern The expected spacer pattern. If <code>null</code>, does
     *            not verify the placement of spacers. The pattern should be
     *            formatted as "#-#-#", where each "#" is the number of digits,
     *            and each "-" is a spacer. You must use the "-" character to
     *            represent spacers in the pattern string.
     * @param base The base whose digits to accept. All characters that are not
     *            digits in this base will be dropped.
     * @return The stripped string.
     * @throws NumberFormatException If the spacers are not in the correct
     *             places, or if the pattern is badly formatted (contains
     *             characters other than base-10 digits and dashes).
     */
    private static String strip(String s, String pattern, int base)
        throws NumberFormatException
    {
        StringBuilder sb = new StringBuilder();

        int next = -1; // Position of the next spacer
        int expect = -1; // Number of digits left before the next legal spacer
        if (pattern != null) {
            next = pattern.indexOf('-');
            if (next == -1) {
                next = pattern.length();
            }

            expect = Integer.parseInt(pattern.substring(0, next));
        }

        for (char c : s.toCharArray()) {
            boolean isDigit = false;
            char min = Character.forDigit(0, base);
            char max;
            if (base <= 10) {
                // Highest possible digit in this base
                max = Character.forDigit(base - 1, base);
            }
            else {
                // Highest numeric digit in this base will be 9
                max = Character.forDigit(9, base);
            }

            if (min <= c && c <= max) {
                isDigit = true;
            }
            else if (base > 10) {
                // Not in the numeric range, but this base includes letters
                char lc = Character.toLowerCase(c);
                min = Character.forDigit(10, base);
                max = Character.forDigit(base - 1, base);

                if (min <= lc && lc <= max) {
                    isDigit = true;
                }
            }

            if (isDigit) {
                if (expect == 0) {
                    // This character was supposed to be a spacer
                    throw new NumberFormatException(
                        "Badly formatted number: Separator placement is incorrect");
                }

                sb.append(c);

                if (expect != -1) {
                    expect--;
                }
            }
            else {
                if (expect > 0) {
                    // This character was supposed to be a digit
                    throw new NumberFormatException(
                        "Badly formatted number: Separator placement is incorrect");
                }
                else if (expect == 0) {
                    int pos = next + 1; // 'next' is pointing at a -
                    next = pattern.indexOf('-', pos);
                    if (next == -1) {
                        next = pattern.length();
                    }

                    expect = Integer.parseInt(pattern.substring(pos, next));
                }
            }
        }

        if (expect > 0) {
            throw new NumberFormatException(
                "Badly formatted number: Not enough digits after last separator");
        }

        return sb.toString();
    }

    /**
     * Verify the Luhn check digit on a string of digits. The last digit is
     * assumed to be the check digit.
     * 
     * @param s The string. The last digit must be the Luhn check digit. There
     *            must be no values in this string other than legal digits for
     *            the given base.
     * @param base The base of the digits in the string.
     * @throws NumberFormatException If the Luhn check digit is incorrect.
     */
    private static void luhnCheck(String s, int base)
        throws NumberFormatException
    {
        // Normally, this would be implemented by performing the Luhn check
        // digit calculation and then verifying that the real check digit added
        // to the calculated value is equal to zero (mod the base)
        // But, to avoid code duplication, we just calculate the digit and
        // do a simple comparison

        int expect = luhnCalculate(s.substring(0, s.length() - 1), base);
        int actual =
            Integer
                .parseInt(Character.toString(s.charAt(s.length() - 1)), base);

        if (expect != actual) {
            throw new NumberFormatException("Luhn check digit failed for " + s);
        }
    }

    /**
     * Calculate the Luhn check digit for a string of digits.
     * 
     * @param s The string of digits.
     * @param base The base of the digits in the string, as well as the check
     *            digit that will be returned.
     * @return The check digit.
     */
    private static int luhnCalculate(String s, int base)
    {
        // Sum the values of the digits using the Luhn method (every second
        // digit counts as the sum of the digits of double the value of that
        // digit)
        int sum = 0;
        int pos = 0;
        for (char c : s.toCharArray()) {
            pos++;
            int value = luhnDigitHelper(pos, c, base);
            sum += value;
        }

        sum %= base;
        return ((base - sum) % base); // The extra mod prevents returning base
    }

    /**
     * Helper function for determining the value of a digit in a string being
     * Luhn-checked.
     * 
     * @param pos The position of this digit, base 1.
     * @param c The digit.
     * @param base The base of the digit.
     * @return The value of the digit. This will be the integer value of the
     *         digit on odd positions, and the sum of the digits of double the
     *         integer value of the digit on even positions.
     */
    private static int luhnDigitHelper(int pos, char c, int base)
    {
        // Determine the integer value of the digit
        int value = Integer.parseInt(Character.toString(c), base);
        if (pos % 2 == 0) {
            value *= 2;
        }

        // Convert back to a string of digits and add their individual values
        // together
        String s = Integer.toString(value, base);
        int sum = 0;
        for (char ch : s.toCharArray()) {
            sum += Integer.parseInt(String.valueOf(ch), base);
        }

        return sum;
    }
}

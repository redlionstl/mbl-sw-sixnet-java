/*
 * PipedEthernetSocketImpl.java
 *
 * An EthernetSocketImpl that works with piped input/output streams
 * instead of dealing with real interfaces.
 *
 * Jonathan Pearson
 * September 18, 2008
 *
 */

package com.sixnetio.net;

import java.io.*;
import java.net.NetworkInterface;

import com.sixnetio.util.Conversion;

public class PipedEthernetSocketImpl extends EthernetSocketImpl {
	protected PipedInputStream in;
	protected PipedOutputStream out;
	protected EthernetAddress address;
	
	protected void close() throws IOException {
		in.close();
		out.close();
		
		in = null;
		out = null;
	}
	
	protected void open(NetworkInterface ifc) throws IOException {
		throw new RuntimeException("Use open(EthernetAddress, PipedInputStream, PipedOutputStream) instead");
	}
	
	protected void open(EthernetAddress address, PipedInputStream in, PipedOutputStream out) throws IOException {
		if (this.in != null || this.out != null) throw new IOException("Socket already open");
		if (in == null || out == null) throw new NullPointerException("Streams must not be null");
		
		this.address = address;
		this.in = in;
		this.out = out;
	}
	
	protected EthernetPacket receive() throws IOException {
		if (in == null) throw new IOException("Cannot read from closed socket");
		
		// Try to read an ethernet packet from 'in'
		// Since there is no physical device to separate individual packets, we need to do it ourselves
		// Luckily, we can guarantee that there will be another PipedEthernetSocketImpl on the other end
		
		// 2-byte frame size
		byte[] pktSize = new byte[2];
		int readBytes = in.read(pktSize);
		if (readBytes == -1) return null;
		
		short size = Conversion.bytesToShort(pktSize, 0);
		byte[] buf = new byte[size];
		readBytes = in.read(buf);
		
		if (readBytes == -1) return null;
		
		return new EthernetPacket(buf);
	}
	
	protected EthernetPacket receiveNonBlocking() throws IOException {
		// If there is no data available, simply return
		if (in.available() == 0) return null;
		
		// There must be a packet available
		return receive();
	}
	
	protected void send(EthernetPacket packet) throws IOException {
		byte[] bytes = packet.getBytes();
		
		short size = (short)bytes.length;
		byte[] pktSize = new byte[2];
		Conversion.shortToBytes(pktSize, 0, size);
		
		// Send the size
		out.write(pktSize);
		
		// Send the data
		out.write(bytes);
		
		// No need for a CRC, since it's all just internal pipes
	}
}

/*
 * IfcUtil.java
 *
 * Provides a library of functions for utility operations on network interfaces.
 *
 * Jonathan Pearson
 * April 17, 2009
 *
 */

package com.sixnetio.net;

import java.net.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

public class IfcUtil
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Get a mapping of IP addresses exported by this host onto the
	 * InterfaceAddress objects representing those addresses.
	 * 
	 * @param includeIPv6 Whether to include IPv6 addresses; IPv4 addresses will
	 *   always be included.
	 * @return Non-loopback IP addresses exported by this host mapped onto the
	 *   corresponding InterfaceAddress objects which provide more detail.
	 * @throws SocketException If there is a problem enumerating the addresses.
	 */
	public static Map<String, InterfaceAddress> discoverLocalAddresses(boolean includeIPv6)
		throws SocketException
	{
		// Discover interface address
		Map<String, InterfaceAddress> addrs =
			new HashMap<String, InterfaceAddress>();
		
		logger.debug("Enumerating network interfaces");
		Enumeration<NetworkInterface> enIfcs =
			NetworkInterface.getNetworkInterfaces();
		
		while (enIfcs.hasMoreElements()) {
			NetworkInterface ifc = enIfcs.nextElement();
			
			logger.debug("  Enumerating addresses for interface " +
			             ifc.getDisplayName());
			for (InterfaceAddress ifcAddr :
			     ifc.getInterfaceAddresses()) {
				
				InetAddress addr = ifcAddr.getAddress();
				
				logger.debug("    Examining address " + addr.getHostAddress() +
				             "/" + ifcAddr.getNetworkPrefixLength());
				
				if ( ! addr.isLoopbackAddress()) {
					String ipaddr = addr.getHostAddress();
					
					// If it's an IPv6 address
					if (addr instanceof Inet6Address) {
						// And we're allowing IPv6 addresses
						if (includeIPv6) {
							// Format it so it looks nice by removing the scope
							// id
							if (ipaddr.indexOf('%') != -1) {
								ipaddr = ipaddr.substring(0, ipaddr.indexOf('%'));
							}
						}
						else {
							// Otherwise, throw it out
							ipaddr = null;
						}
					}
					
					if (ipaddr != null) {
						logger.debug("      Including address " +
						             addr.getHostAddress());
						
						addrs.put(ipaddr, ifcAddr);
					}
				}
			}
		}
		
		// Sort them, so they always show up in the same order
		String[] addresses = addrs.keySet().toArray(new String[addrs.size()]);
		Arrays.sort(addresses);
		
		Map<String, InterfaceAddress> realAddrs =
			new LinkedHashMap<String, InterfaceAddress>();
		
		for (String address : addresses) {
			realAddrs.put(address, addrs.get(address));
		}
		
		return realAddrs;
	}
	
	/**
	 * Compare a pair of Internet addresses up to a given prefix length to see
	 * if they are on the same subnet.
	 * 
	 * @param address1 The first address.
	 * @param address2 The second address.
	 * @param networkPrefixLength The number of set bits in the subnet mask. For
	 *   example, 255.128.0.0 would be 15 (255(8 bits) + 128(7 bits) = 15 bits).
	 * @return True if the two addresses are on the same subnet, false
	 *   otherwise.
	 */
	public static boolean sameSubnet(InetAddress address1, InetAddress address2,
	                                 short networkPrefixLength)
	{
		if ( ! address1.getClass().equals(address2.getClass())) {
			throw new IllegalArgumentException("Cannot compare " +
			                                   address1.getClass().getName() +
			                                   " with " +
			                                   address2.getClass().getName());
		}
		
		logger.debug(String.format("Checking addresses '%s' and '%s' out to" +
		                           " subnet length %d",
		                           address1.getHostAddress(),
		                           address2.getHostAddress(),
		                           networkPrefixLength));
		
		// These are what we are really comparing
		byte[] b1 = address1.getAddress();
		byte[] b2 = address2.getAddress();
		
		// Position in bits through the addresses
		short position = 0;
		
		// Byte position of the current comparison in the arrays
		int index = 0;
		
		// Loop until we've compared the prefixes
		while (position < networkPrefixLength) {
			// Figure out how many bits (up to 8) we are comparing on this
			// iteration
			int bits = networkPrefixLength - position;
			if (bits > 8) {
				bits = 8;
			}
			
			// Construct a mask to show us only the important bits (on the most
			// significant end)
			int mask = (0xff << (8 - bits)) & 0xff;
			
			// Compare the masked bits
			if ((b1[index] & mask) != (b2[index] & mask)) {
				// Not a match? No communication
				logger.debug("  Addresses differ around bits " + position +
				             " through " + (position + bits));
				return false;
			}
			
			// Increment
			position += 8;
			index++;
		}
		
		// All relevant bits matched
		logger.debug("  Addresses are on the same subnet");
		return true;
	}
}

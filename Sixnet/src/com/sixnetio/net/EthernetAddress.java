/*
 * EthernetAddress.java
 *
 * Represents an ethernet address.
 *
 * Jonathan Pearson
 * September 15, 2008
 *
 */

package com.sixnetio.net;

import java.net.SocketAddress;
import java.util.Arrays;
import java.util.StringTokenizer;

public class EthernetAddress extends SocketAddress {
	private final byte[] mac = new byte[6];
	
	public EthernetAddress(byte[] mac) {
		this(mac, 0);
	}
	
	public EthernetAddress(byte[] mac, int off) {
		System.arraycopy(mac, off, this.mac, 0, 6);
	}
	
	public EthernetAddress(String mac) {
		StringTokenizer tok = new StringTokenizer(mac, ":");
		int index = 0;
		while (tok.hasMoreTokens()) {
			this.mac[index++] = (byte)(Integer.parseInt(tok.nextToken(), 16) & 0xff);
		}
	}
	
	@Override
    public boolean equals(Object obj) {
		if (!(obj instanceof EthernetAddress)) return false;
		
		EthernetAddress rhs = (EthernetAddress)obj;
		return Arrays.equals(mac, rhs.getBytes());
	}
	
	public byte[] getBytes() {
		byte[] mac = new byte[this.mac.length];
		System.arraycopy(this.mac, 0, mac, 0, 6);
		return mac;
	}
	
	@Override
    public int hashCode() {
		int hc = 0x12345678;
		for (int i = 0; i < 6; i++) {
			hc ^= (mac[i] << (i * 5));
		}
		return hc;
	}
	
	@Override
    public String toString() {
		return String.format("%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	}
}

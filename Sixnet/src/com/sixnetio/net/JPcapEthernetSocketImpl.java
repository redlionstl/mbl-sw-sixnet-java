/*
 * JPcapEthernetSocketImpl.java
 *
 * Uses JPcap to implement the functions defined in EthernetSocketImpl.
 *
 * Jonathan Pearson
 * September 16, 2008
 *
 */

package com.sixnetio.net;

import java.io.IOException;
import java.net.NetworkInterface;
import java.util.Arrays;

import jpcap.*;
import jpcap.packet.*;

public class JPcapEthernetSocketImpl extends EthernetSocketImpl {
	private JpcapCaptor captor;
	private JpcapSender sender;
	
	protected void close() throws IOException {
		if (captor == null) return;
		
		captor.close();
		sender.close();
		
		captor = null;
		sender = null;
	}
	
	protected void open(NetworkInterface ifc) throws IOException {
		// Find the jpcap network interface that corresponds to the one passed in
		jpcap.NetworkInterface[] ifcs = JpcapCaptor.getDeviceList();
		jpcap.NetworkInterface jifc = null;
		for (int i = 0; i < ifcs.length; i++) {
			if (Arrays.equals(ifcs[i].mac_address, ifc.getHardwareAddress())) {
				jifc = ifcs[i];
				break;
			}
		}
		
		if (jifc == null) throw new IOException("Unable to locate the chosen interface");
		
		open(jifc);
	}
	
	protected void open(jpcap.NetworkInterface ifc) throws IOException {
		captor = JpcapCaptor.openDevice(ifc, 65535, true, 0);
		sender = captor.getJpcapSenderInstance();
	}
	
	protected EthernetPacket receive() throws IOException {
		captor.setNonBlockingMode(false);
		captor.setPacketReadTimeout(0);
		return receivePacket();
	}
	
	protected EthernetPacket receiveNonBlocking() throws IOException {
		captor.setNonBlockingMode(true);
		captor.setPacketReadTimeout(10);
		return receivePacket();
	}
	
	private EthernetPacket receivePacket() throws IOException {
		if (captor == null) throw new IOException("Cannot read from a closed socket");
		
		Packet pkt = captor.getPacket();
		if (pkt == null) return null;
		
		if (!(pkt.datalink instanceof jpcap.packet.EthernetPacket)) return null; // Don't recognize this one
		jpcap.packet.EthernetPacket epkt = (jpcap.packet.EthernetPacket)pkt.datalink;
		
		EthernetAddress dstAddr = new EthernetAddress(epkt.dst_mac);
		EthernetAddress srcAddr = new EthernetAddress(epkt.src_mac);
		short type = epkt.frametype;
		
		EthernetPacket ethernetPacket = new EthernetPacket(dstAddr, srcAddr, type);
		
		// Not sure what the 'header' of Packet is for, so let's throw it together with the data
		byte[] data = new byte[pkt.header.length + pkt.data.length];
		System.arraycopy(pkt.header, 0, data, 0, pkt.header.length);
		System.arraycopy(pkt.data, 0, data, pkt.header.length, pkt.data.length);
		
		ethernetPacket.setPayload(data);
		
		return ethernetPacket;
	}
	
	protected void send(EthernetPacket packet) throws IOException {
		if (sender == null) throw new IOException("Cannot send from closed socket");
		
		jpcap.packet.EthernetPacket epkt = new jpcap.packet.EthernetPacket();
		
		epkt.src_mac = packet.getSrcAddr().getBytes();
		epkt.dst_mac = packet.getDstAddr().getBytes();
		epkt.frametype = packet.getType();
		
		Packet pkt = new Packet();
		pkt.datalink = epkt;
		pkt.header = new byte[0];
		pkt.data = packet.getPayload();
		
		sender.sendPacket(pkt);
	}
}

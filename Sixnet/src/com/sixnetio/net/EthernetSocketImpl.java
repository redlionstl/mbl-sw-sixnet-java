/*
 * EthernetSocketImpl.java
 *
 * The abstract back-end for EthernetSocket. This cannot implement SocketImpl,
 * as that is specifically for IP-based sockets, so it follows what it can from
 * that model.
 *
 * Jonathan Pearson
 * September 15, 2008
 *
 */

package com.sixnetio.net;

import java.io.IOException;
import java.net.NetworkInterface;
import java.net.SocketException;

public abstract class EthernetSocketImpl {
	protected NetworkInterface ifc; // The interface that this socket communicates through
	
	@Override
    protected void finalize() {
		try {
			close();
		} catch (IOException ioe) { }
	}
	
	/**
	 * Open a raw ethernet socket on the provided interface.
	 * @param ifc The interface to open for raw ethernet I/O.
	 * @throws IOException If there is an error opening the interface.
	 */
	protected abstract void open(NetworkInterface ifc) throws IOException;
	
	/**
	 * Close this socket. Does nothing if this socket is not open.
	 * @throws IOException If there is an error closing the socket.
	 */
	protected abstract void close() throws IOException;
	
	/**
	 * Read a single ethernet frame from this socket, blocking until one is received.
	 * @return The frame.
	 * @throws IOException If there is an error reading the frame.
	 */
	protected abstract EthernetPacket receive() throws IOException;
	
	/**
	 * Read a single ethernet frame from this socket, returning within 10ms if none is received.
	 * @return The frame, or null if none is received before timing out
	 * @throws IOException If there is an error reading the frame.
	 */
	protected abstract EthernetPacket receiveNonBlocking() throws IOException;
	
	/**
	 * Send a single ethernet frame out of this socket.
	 * @param packet The frame to send.
	 * @throws IOException If there is an error sending the frame.
	 */
	protected abstract void send(EthernetPacket packet) throws IOException;
	
	/**
	 * Get the ethernet address (MAC address) associated with this socket.
	 * @throws SocketException If there is an error.
	 */
	protected EthernetAddress getEthernetAddress() throws SocketException {
		if (ifc == null) return null;
		
		return new EthernetAddress(ifc.getHardwareAddress());
	}
}

/*
 * EthernetSocketFactory.java
 *
 * Creates ethernet sockets of various types.
 *
 * Jonathan Pearson
 * September 18, 2008
 *
 */

package com.sixnetio.net;

import java.io.*;
import java.net.NetworkInterface;

public class EthernetSocketFactory {
	public static EthernetSocket createPhysicalSocket(NetworkInterface ifc) throws IOException {
		JPcapEthernetSocketImpl impl = new JPcapEthernetSocketImpl();
		impl.open(ifc);
		return new EthernetSocket(impl);
	}
	
	public static EthernetSocket[] createPipedSockets(EthernetAddress address1, EthernetAddress address2) throws IOException {
		PipedEthernetSocketImpl impl1 = new PipedEthernetSocketImpl();
		PipedEthernetSocketImpl impl2 = new PipedEthernetSocketImpl();
		
		PipedInputStream in1 = new PipedInputStream(65536);
		PipedOutputStream out2 = new PipedOutputStream(in1);
		
		PipedInputStream in2 = new PipedInputStream(65536);
		PipedOutputStream out1 = new PipedOutputStream(in2);
		
		impl1.open(address1, in1, out1);
		impl2.open(address2, in2, out2);
		
		EthernetSocket[] sockets = {
			new EthernetSocket(impl1),
			new EthernetSocket(impl2)
		};
		
		return sockets;
	}
	
	
}

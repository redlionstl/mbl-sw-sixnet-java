/*
 * EthernetSocket.java
 *
 * A socket for communication over raw Ethernet links.
 *
 * Jonathan Pearson
 * September 15, 2008
 *
 */

package com.sixnetio.net;

import java.net.*;
import java.io.*;

public class EthernetSocket {
	private EthernetSocketImpl impl;
	
	public EthernetSocket() {
		impl = new JPcapEthernetSocketImpl();
	}
	
	protected EthernetSocket(EthernetSocketImpl impl) {
		this.impl = impl;
	}
	
	public void open(NetworkInterface ifc) throws IOException {
		impl.open(ifc);
	}
	
	public void close() throws IOException {
		impl.close();
	}
	
	public EthernetPacket receive() throws IOException {
		return impl.receive();
	}
	
	public EthernetPacket receiveNonBlocking() throws IOException {
		return impl.receiveNonBlocking();
	}
	
	public void send(EthernetPacket packet) throws IOException {
		impl.send(packet);
	}
	
	public EthernetAddress getEthernetAddress() throws IOException {
		return impl.getEthernetAddress();
	}
}

/*
 * EthernetPacket.java
 *
 * Represents a single packet of Ethernet data. This is modeled after DatagramPacket.
 *
 * Jonathan Pearson
 * September 15, 2008
 *
 */

package com.sixnetio.net;

import com.sixnetio.util.Conversion;

public class EthernetPacket {
	private EthernetAddress srcAddr, dstAddr;
	private short type = 0;
	private boolean forceType = false; // If set, does not auto-detect Ethernet 1/Ethernet 2 by type
	protected byte[] payload; // Can never be null (all constructors set it, possibly to new byte[0])
	
	public EthernetPacket() {
		srcAddr = dstAddr = null;
		payload = new byte[0];
	}
	
	public EthernetPacket(byte[] data) {
		this(data, 0, data.length);
	}
	
	public EthernetPacket(byte[] data, int off, int len) {
		payload = new byte[len];
		System.arraycopy(data, off, payload, 0, len);
		
		srcAddr = dstAddr = null;
	}
	
	public EthernetPacket(byte[] data, int off, int len, short type) {
		this(data, off, len);
		this.type = type;
	}
	
	// If srcAddr is null at the time of sending, the address of the output interface is used
	public EthernetPacket(EthernetAddress dstAddr) {
		this.dstAddr = dstAddr;
		
		srcAddr = null;
		payload = new byte[0];
	}
	
	public EthernetPacket(EthernetAddress srcAddr, EthernetAddress dstAddr) {
		this.srcAddr = srcAddr;
		this.dstAddr = dstAddr;
		
		payload = new byte[0];
	}
	
	public EthernetPacket(EthernetAddress srcAddr, EthernetAddress dstAddr, short type) {
		this(srcAddr, dstAddr);
		
		this.type = type;
	}
	
	public EthernetPacket(EthernetAddress dstAddr, byte[] data) {
		this(data);
		
		this.dstAddr = dstAddr;
		srcAddr = null;
	}
	
	public EthernetPacket(EthernetAddress dstAddr, byte[] data, int off, int len) {
		this(data, off, len);
		
		this.dstAddr = dstAddr;
		srcAddr = null;
	}
	
	public EthernetPacket(EthernetAddress srcAddr, EthernetAddress dstAddr, byte[] data) {
		this(dstAddr, data);
		
		this.srcAddr = srcAddr;
	}
	
	public EthernetPacket(EthernetAddress srcAddr, EthernetAddress dstAddr, byte[] data, int off, int len) {
		this(dstAddr, data, off, len);
		
		this.srcAddr = srcAddr;
	}
	
	public EthernetPacket(EthernetAddress srcAddr, EthernetAddress dstAddr, byte[] data, int off, int len, short type) {
		this(srcAddr, dstAddr, data, off, len);
		
		this.type = type;
	}
	
	protected EthernetPacket(byte[] packet, int off) {
		dstAddr = new EthernetAddress(packet, off);
		srcAddr = new EthernetAddress(packet, off + 6);
		type = Conversion.bytesToShort(packet, off + 12);
		
		payload = new byte[packet.length - 18]; // Skip the CRC on the end
		System.arraycopy(packet, off + 14, payload, 0, payload.length);
		
		// FUTURE: Check the CRC to make sure it did not fail
	}
	
	public EthernetAddress getSrcAddr() {
    	return srcAddr;
    }
	
	public void setSrcAddr(EthernetAddress srcAddr) {
    	this.srcAddr = srcAddr;
    }
	
	public EthernetAddress getDstAddr() {
    	return dstAddr;
    }
	
	public void setDstAddr(EthernetAddress dstAddr) {
    	this.dstAddr = dstAddr;
    }
	
	public short getType() {
		return type;
	}
	
	public void setType(short type) {
		this.type = type;
	}
	
	public boolean getForceType() {
		return forceType;
	}
	
	public void setForceType(boolean forceType) {
		this.forceType = forceType;
	}
	
	public byte[] getPayload() {
		byte[] payload = new byte[this.payload.length];
		System.arraycopy(this.payload, 0, payload, 0, payload.length);
    	return payload;
    }
	
	public void setPayload(byte[] payload) {
		setPayload(payload, 0, payload.length);
	}
	
	public void setPayload(byte[] payload, int off, int len) {
		if (this.payload.length != len) {
			this.payload = new byte[len];
		}
		
		System.arraycopy(payload, off, this.payload, 0, len);
    }
	
	// This skips the 8-byte preamble (7 bytes of 10101010, then one of 10101011) and the 4-byte CRC
	// This does make sure the data is padded out to 60 bytes as necessary (min size is 64 bytes, but
	//   we're skipping the CRC so we do 60)
	public byte[] getBytes() {
		if (payload.length > 1500) throw new IllegalStateException("Cannot generate an Ethernet packet with a payload size larger than 1500");
		
		int len = 14 + payload.length;
		
		if (len < 60) {
			len = 60;
		}
		
		byte[] buf = new byte[len];
		
		System.arraycopy(getDstAddr().getBytes(), 0, buf, 0, 6);
		System.arraycopy(getSrcAddr().getBytes(), 0, buf, 6, 6);
		
		if (forceType || (type >= 0x0600)) {
			// Trust the 'type' field
			Conversion.shortToBytes(buf, 12, getType());
		} else {
			// Ethernet 1 -- write the payload length in
			Conversion.shortToBytes(buf, 12, (short)payload.length);
		}
		
		System.arraycopy(payload, 0, buf, 14, payload.length);
		
		return buf;
	}
}

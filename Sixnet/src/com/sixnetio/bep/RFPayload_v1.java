/*
 * RFInfo_v1.java
 *
 * Represents version 1 of an RF info payload node.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.DataInputStream;
import java.io.IOException;

import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Utils;


public class RFPayload_v1
    extends PayloadNode
{
    /** Indicates the existence and version of the RF info payload. */
    private static final int MASK_RF = 0x30;

    /** Version 1, third half-nibble. */
    private static final int MASKED_VERSION = 1 << 4;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_RF, MASKED_VERSION);
    }

    private float rssi;

    public RFPayload_v1()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream in)
        throws IOException
    {
        try {
            int len = in.readByte() & 0xff;

            LimitedInputStream lin = new LimitedInputStream(in);
            lin.setLimit(len);

            try {
                DataInputStream din = new DataInputStream(lin);

                byte rssi = din.readByte();

                if (rssi >= 0) {
                    // Reported in the range [0, 31] or 99; convert to real RSSI
                    if (rssi == 0) {
                        this.rssi = -113;
                    }
                    else if (rssi == 1) {
                        this.rssi = -111;
                    }
                    else if (2 <= rssi && rssi <= 30) {
                        this.rssi = -109.0f + 2.0f * (rssi - 2);
                    }
                    else if (rssi == 31) {
                        this.rssi = -51;
                    }
                    else {
                        this.rssi = 0;
                    }
                }
                else {
                    this.rssi = rssi;
                }
            }
            finally {
                lin.discardToCompletion();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted RF1 data", ioe);
        }
    }

    public RFPayload_v1(float rssi)
    {
        this.rssi = rssi;
    }

    public float getRSSI()
    {
        return rssi;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        data[0] = 1;

        if (rssi < -112) {
            data[1] = 0;
        }
        else if (rssi < -110) {
            data[1] = 1;
        }
        else if (rssi < -51) {
            data[1] = (byte)(2 + (rssi + 109) * 0.5f);
        }
        else if (rssi < -50) {
            data[1] = 31;
        }
        else {
            data[1] = 32;
        }

        return data;
    }

    @Override
    public int getLength()
    {
        return 2;
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_RF;
    }

    @Override
    public int getPosition()
    {
        return 2;
    }

    @Override
    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%sRSSI: %f\n", indentString, rssi));

        return builder.toString();
    }
}

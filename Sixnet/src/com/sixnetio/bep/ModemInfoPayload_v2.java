/*
 * ModemInfo_v2.java
 *
 * Represents version 2 of a modem info payload node.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.DataInputStream;
import java.io.IOException;

import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;


public class ModemInfoPayload_v2
    extends PayloadNode
{
    /** Indicates the existence and version of the modem info payload. */
    private static final int MASK_MODEM = 0xc0;

    /** Version 2, fourth half-nibble. */
    private static final int MASKED_VERSION = 2 << 6;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_MODEM, MASKED_VERSION);
    }

    private int modelID;
    private String fwVersion;
    private String cfgVersion;
    private String prlVersion;

    public ModemInfoPayload_v2()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream in)
        throws IOException
    {
        try {
            int len = Conversion.reverse(in.readShort()) & 0xffff;

            LimitedInputStream lin = new LimitedInputStream(in);
            lin.setLimit(len);

            try {
                DataInputStream din = new DataInputStream(lin);

                modelID = Conversion.reverse(din.readInt());

                fwVersion = readNullTerminatedString(din, 32);
                cfgVersion = readNullTerminatedString(din, 32);
                prlVersion = readNullTerminatedString(din, 32);
            }
            finally {
                lin.discardToCompletion();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted Modem (MI2) data", ioe);
        }
    }

    public ModemInfoPayload_v2(int modelID, String fwVersion,
                               String cfgVersion, String prlVersion)
    {

        this.modelID = modelID;
        this.fwVersion = fwVersion;
        this.cfgVersion = cfgVersion;
        this.prlVersion = prlVersion;
    }

    /**
     * Get the model ID contained within this message.
     */
    public int getModelID()
    {
        return modelID;
    }

    /**
     * Get the firmware version contained within this message.
     */
    public String getFWVersion()
    {
        return fwVersion;
    }

    /**
     * Get the configuration version contained within this message.
     */
    public String getCFGVersion()
    {
        return cfgVersion;
    }

    /**
     * Get the PRL version contained within this message.
     * 
     * @return
     */
    public String getPRLVersion()
    {
        return prlVersion;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        Conversion.shortToBytesLE(data, 0, (short)(data.length - 2));
        Conversion.intToBytesLE(data, 2, modelID);

        int offset = 6;
        offset = writeNullTerminatedString(fwVersion, data, offset, 32);
        offset = writeNullTerminatedString(cfgVersion, data, offset, 32);
        offset = writeNullTerminatedString(prlVersion, data, offset, 32);

        return data;
    }

    @Override
    public int getLength()
    {
        // Length (2) + Model ID (4) + fwVersion + \0 + cfgVersion + \0 +
        // prlVersion + \0
        return (2 + 4 + Math.min(32, fwVersion.getBytes().length) + 1 +
                Math.min(32, cfgVersion.getBytes().length) + 1 +
                Math.min(32, prlVersion.getBytes().length) + 1);
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_MODEM;
    }

    @Override
    public int getPosition()
    {
        return 1;
    }

    @Override
    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);
        StringBuilder builder = new StringBuilder();

        builder
            .append(String.format("%sModel ID: %d\n", indentString, modelID));
        builder.append(String.format("%sFW Version: %s\n", indentString,
            fwVersion));
        builder.append(String.format("%sCFG Version: %s\n", indentString,
            cfgVersion));
        builder.append(String.format("%sPRL Version : %s\n", indentString,
            prlVersion));

        return builder.toString();
    }
}

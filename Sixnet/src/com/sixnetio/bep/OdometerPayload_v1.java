/*
 * OdometerInfo_v1.java
 *
 * Represents a version 1 odometer info payload node.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.DataInputStream;
import java.io.IOException;

import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;


public class OdometerPayload_v1
    extends PayloadNode
{
    /** Indicates the existence and version of the odometer info payload. */
    private static final int MASK_ODOMETER = 0xc000;

    /** Version 1, eighth half-nibble. */
    private static final int MASKED_VERSION = 1 << 14;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_ODOMETER, MASKED_VERSION);
    }

    private int odometer;

    public OdometerPayload_v1()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream in)
        throws IOException
    {
        try {
            int len = in.readByte() & 0xff;

            LimitedInputStream lin = new LimitedInputStream(in);
            lin.setLimit(len);

            try {
                DataInputStream din = new DataInputStream(lin);
                odometer = Conversion.reverse(din.readInt());
            }
            finally {
                lin.discardToCompletion();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted Odometer (OD1) data", ioe);
        }
    }

    public OdometerPayload_v1(int odometer)
    {
        this.odometer = odometer;
    }

    public int getOdometer()
    {
        return odometer;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        data[0] = (byte)(data.length - 1);

        Conversion.intToBytesLE(data, 1, odometer);

        return data;
    }

    @Override
    public int getLength()
    {
        return 5;
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_ODOMETER;
    }

    @Override
    public int getPosition()
    {
        return 5;
    }

    @Override
    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%sOdometer Meters: %,d\n", indentString,
            odometer));

        return builder.toString();
    }
}

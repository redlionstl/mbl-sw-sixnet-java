/*
 * GPSInfo_v1.java
 *
 * Represents version 1 of a GPS info payload node.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.DataInputStream;
import java.io.IOException;

import com.sixnetio.gps.NMEAParser;
import com.sixnetio.gps.TAIPParser;
import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Conversion;

public class GPSPayload_v1
    extends PayloadNode
{
    /** Indicates the existence and version of the GPS info payload. */
    private static final int MASK_GPS = 0x03;

    /** Version 1, first half-nibble. */
    private static final int MASKED_VERSION = 1 << 0;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_GPS, MASKED_VERSION);
    }

    private String gpsData;

    public GPSPayload_v1()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream in)
        throws IOException
    {
        try {
            // Read the length (short)
            // The bitwise AND prevents sign extension
            int len = Conversion.reverse(in.readShort()) & 0xffff;

            LimitedInputStream lin = new LimitedInputStream(in);
            lin.setLimit(len);

            try {
                DataInputStream din = new DataInputStream(lin);

                byte[] gpsBytes = new byte[Math.min(1024, len)];
                din.readFully(gpsBytes);
                gpsData = new String(gpsBytes);
            }
            finally {
                lin.discardToCompletion();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted GPS (GP1) data", ioe);
        }
    }

    public GPSPayload_v1(String gpsData)
    {
        this.gpsData = gpsData;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        Conversion.shortToBytesLE(data, 0, (short)((data.length - 2) & 0xffff));

        System.arraycopy(gpsData.getBytes(), 0, data, 2, data.length - 2);

        return data;
    }

    /**
     * Get the raw GPS data contained in this payload node.
     */
    public String getGPSData()
    {
        return gpsData;
    }

    /**
     * Get the latitude in + North degrees, or NaN if no latitude was found.
     */
    public float getLatitude()
    {
        // Try to parse it as NMEA
        NMEAParser nmea = new NMEAParser(gpsData);

        for (NMEAParser.NMEAMessage msg : nmea) {
            if (msg instanceof NMEAParser.GLLMessage) {
                return (((NMEAParser.GLLMessage)msg).getLatitude());
            }
            else if (msg instanceof NMEAParser.GGAMessage) {
                return (((NMEAParser.GGAMessage)msg).getLatitude());
            }
            else if (msg instanceof NMEAParser.RMCMessage) {
                return (((NMEAParser.RMCMessage)msg).getLatitude());
            }
        }

        // Didn't find the data? Try parsing as TAIP
        TAIPParser taip = new TAIPParser(gpsData);

        for (TAIPParser.TAIPMessage msg : taip) {
            if (msg instanceof TAIPParser.RPVMessage) {
                return (((TAIPParser.RPVMessage)msg).getLatitude());
            }
        }

        // Not found
        return Float.NaN;
    }

    /**
     * Get the longitude in + East degrees, or NaN if no longitude was found.
     */
    public float getLongitude()
    {
        // Try to parse it as NMEA
        NMEAParser nmea = new NMEAParser(gpsData);

        for (NMEAParser.NMEAMessage msg : nmea) {
            if (msg instanceof NMEAParser.GLLMessage) {
                return (((NMEAParser.GLLMessage)msg).getLongitude());
            }
            else if (msg instanceof NMEAParser.GGAMessage) {
                return (((NMEAParser.GGAMessage)msg).getLongitude());
            }
            else if (msg instanceof NMEAParser.RMCMessage) {
                return (((NMEAParser.RMCMessage)msg).getLongitude());
            }
        }

        // Didn't find the data? Try parsing as TAIP
        TAIPParser taip = new TAIPParser(gpsData);

        for (TAIPParser.TAIPMessage msg : taip) {
            if (msg instanceof TAIPParser.RPVMessage) {
                return (((TAIPParser.RPVMessage)msg).getLongitude());
            }
        }

        // Not found
        return Float.NaN;
    }

    /**
     * Get the altitude in meters, or NaN if no altitude was found.
     */
    public float getAltitude()
    {
        // Try to parse it as NMEA
        NMEAParser nmea = new NMEAParser(gpsData);

        for (NMEAParser.NMEAMessage msg : nmea) {
            if (msg instanceof NMEAParser.GGAMessage) {
                return (((NMEAParser.GGAMessage)msg).getAntennaAltitude());
            }
        }

        // Didn't find the data? We could parse as TAIP, but at the moment, none
        // of the supported TAIP messages contain altitude, so there's no point

        // Not found
        return Float.NaN;
    }

    public float getHeading()
    {
        // Try to parse it as NMEA
        NMEAParser nmea = new NMEAParser(gpsData);

        for (NMEAParser.NMEAMessage msg : nmea) {
            if (msg instanceof NMEAParser.RMCMessage) {
                return (((NMEAParser.RMCMessage)msg).getHeading());
            }
            else if (msg instanceof NMEAParser.VTGMessage) {
                return (((NMEAParser.VTGMessage)msg).getTrueHeading());
            }
        }

        // Didn't find the data? Try parsing as TAIP
        TAIPParser taip = new TAIPParser(gpsData);

        for (TAIPParser.TAIPMessage msg : taip) {
            if (msg instanceof TAIPParser.RPVMessage) {
                return (((TAIPParser.RPVMessage)msg).getHeading());
            }
        }

        // Not found
        return Float.NaN;
    }

    public float getKphSpeed()
    {
        // Try to parse it as NMEA
        NMEAParser nmea = new NMEAParser(gpsData);

        for (NMEAParser.NMEAMessage msg : nmea) {
            if (msg instanceof NMEAParser.RMCMessage) {
                return (((NMEAParser.RMCMessage)msg).getKnotSpeed() * 1.852f);
            }
            else if (msg instanceof NMEAParser.VTGMessage) {
                return (((NMEAParser.VTGMessage)msg).getKphSpeed());
            }
        }

        // Didn't find the data? Try parsing as TAIP
        TAIPParser taip = new TAIPParser(gpsData);

        for (TAIPParser.TAIPMessage msg : taip) {
            if (msg instanceof TAIPParser.RPVMessage) {
                return (((TAIPParser.RPVMessage)msg).getMphSpeed() * 1.609344f);
            }
        }

        // Not found
        return Float.NaN;
    }

    @Override
    public int getLength()
    {
        // Length (2) + GPS data (max 1024)
        return (2 + Math.min(1024, gpsData.getBytes().length));
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_GPS;
    }

    @Override
    public int getPosition()
    {
        return 4;
    }

    @Override
    public String toString(int indent)
    {
        // No point in dumping a bunch of raw GPS data, and parsing it would
        // take a while
        return "";
    }
}

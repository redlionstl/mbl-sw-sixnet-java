/*
 * ModemInfo_v1.java
 *
 * Represents version 1 of a modem info payload node.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.DataInputStream;
import java.io.IOException;

import com.sixnetio.util.Utils;

public class ModemInfoPayload_v1
    extends PayloadNode
{
    /** Indicates the existence and version of the modem info payload. */
    private static final int MASK_MODEM = 0xc0;

    /** Version 1, fourth half-nibble. */
    private static final int MASKED_VERSION = 1 << 6;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_MODEM, MASKED_VERSION);
    }

    private String deviceName;

    public ModemInfoPayload_v1()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream din)
        throws IOException
    {
        try {
            // Length byte is the length byte for the string
            // Don't bother with the LimitedInputStream, this should be fine
            deviceName = readLengthEncodedString(din, 32);
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted Modem (MI1) data", ioe);
        }
    }

    public ModemInfoPayload_v1(String deviceName)
    {
        this.deviceName = deviceName;
    }

    /**
     * Get the device name contained in this message.
     */
    public String getDeviceName()
    {
        return deviceName;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        // The payload length byte is the string length byte
        writeLengthEncodedString(deviceName, data, 0, 32);

        return data;
    }

    @Override
    public int getLength()
    {
        return (Math.min(32, deviceName.getBytes().length) + 1);
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_MODEM;
    }

    @Override
    public int getPosition()
    {
        return 1;
    }

    @Override
    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%sDevice Name: %s\n", indentString,
            deviceName));

        return builder.toString();
    }
}

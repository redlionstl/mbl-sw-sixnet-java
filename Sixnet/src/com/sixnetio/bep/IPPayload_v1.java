/*
 * IPInfo_v1.java
 *
 * Represent a version 1 IP info payload node.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;

import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;


public class IPPayload_v1
    extends PayloadNode
{
    /** Indicates the existence and version of the IP info payload. */
    private static final int MASK_IP = 0x3000;

    /** Version 1, seventh half-nibble. */
    private static final int MASKED_VERSION = 1 << 12;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_IP, MASKED_VERSION);
    }

    private InetAddress address;

    public IPPayload_v1()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream in)
        throws IOException
    {
        try {
            int len = in.readByte() & 0xff;

            LimitedInputStream lin = new LimitedInputStream(in);
            lin.setLimit(len);

            try {
                DataInputStream din = new DataInputStream(lin);

                // Read the IP address
                int ip = Conversion.reverse(din.readInt());

                // Convert to bytes in network byte order
                byte[] ipBytes = new byte[4];
                Conversion.intToBytes(ipBytes, 0, ip);

                // Convert to an InetAddress
                address = InetAddress.getByAddress(ipBytes);
            }
            finally {
                lin.discardToCompletion();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted IP1 data", ioe);
        }
    }

    public IPPayload_v1(InetAddress address)
    {
        this.address = address;
    }

    public InetAddress getAddress()
    {
        return address;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        data[0] = (byte)(data.length - 1);

        byte[] address = this.address.getAddress();
        data[1] = address[3];
        data[2] = address[2];
        data[3] = address[1];
        data[4] = address[0];

        return data;
    }

    @Override
    public int getLength()
    {
        return 5;
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_IP;
    }

    @Override
    public int getPosition()
    {
        return 6;
    }

    @Override
    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%sIP Address: %s\n", indentString,
            address.getHostAddress()));

        return builder.toString();
    }
}

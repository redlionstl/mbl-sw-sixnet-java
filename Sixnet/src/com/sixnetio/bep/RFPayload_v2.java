/*
 * RFInfo_v2.java
 *
 * Represents version 2 of an RF info payload node.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.DataInputStream;
import java.io.IOException;

import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Utils;


public class RFPayload_v2
    extends PayloadNode
{
    /** Indicates the existence and version of the RF info payload. */
    private static final int MASK_RF = 0x30;

    /** Version 2, third half-nibble. */
    private static final int MASKED_VERSION = 2 << 4;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_RF, MASKED_VERSION);
    }

    // Masks for the values in the reserved/roaming byte
    private static final int FLAG_ROAMING = 0x01;

    private float rssi;
    private boolean roaming;
    private String serviceType;
    private String carrier;

    public RFPayload_v2()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream in)
        throws IOException
    {
        try {
            long len = in.readByte() & 0xff;

            LimitedInputStream lin = new LimitedInputStream(in);
            lin.setLimit(len);

            try {
                DataInputStream din = new DataInputStream(lin);
                this.rssi = -din.readByte();

                byte flags = din.readByte();
                this.roaming = ((flags & FLAG_ROAMING) != 0);

                this.serviceType = readNullTerminatedString(din, 32);
                this.carrier = readNullTerminatedString(din, 32);
            }
            finally {
                lin.discardToCompletion();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted RF2 data", ioe);
        }
    }

    public RFPayload_v2(float rssi, boolean roaming, String serviceType,
                        String carrier)
    {

        this.rssi = rssi;
        this.roaming = roaming;
        this.serviceType = serviceType;
        this.carrier = carrier;
    }

    public float getRSSI()
    {
        return rssi;
    }

    public boolean getRoaming()
    {
        return roaming;
    }

    public String getServiceType()
    {
        return serviceType;
    }

    public String getCarrier()
    {
        return carrier;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        data[0] = (byte)(data.length - 1);

        data[1] = (byte)(-rssi);
        data[2] = buildFlags(roaming);

        int offset = 3;
        offset = writeNullTerminatedString(serviceType, data, offset, 32);
        offset = writeNullTerminatedString(carrier, data, offset, 32);

        return data;
    }

    @Override
    public int getLength()
    {
        // Length (1) + RSSI (1) + Roaming (1) + service + \0 + carrier + \0
        return (1 + 1 + 1 + Math.min(32, serviceType.getBytes().length) + 1 +
                Math.min(32, carrier.getBytes().length) + 1);
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_RF;
    }

    @Override
    public int getPosition()
    {
        return 2;
    }

    @Override
    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%sRSSI: %f\n", indentString, rssi));
        builder.append(String.format("%sRoaming: %b\n", indentString, roaming));
        builder.append(String.format("%sService Type: %s\n", indentString,
            serviceType));
        builder.append(String.format("%sCarrier: %s\n", indentString, carrier));

        return builder.toString();
    }
}

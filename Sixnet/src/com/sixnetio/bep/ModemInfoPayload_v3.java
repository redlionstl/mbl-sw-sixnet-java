/*
 * ModemInfo_v3.java
 *
 * Represents version 3 of a modem info payload node.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.DataInputStream;
import java.io.IOException;

import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;


public class ModemInfoPayload_v3
    extends PayloadNode
{
    /** Indicates the existence and version of the modem info payload. */
    private static final int MASK_MODEM = 0xc0;

    /** Version 3, fourth half-nibble. */
    private static final int MASKED_VERSION = 3 << 6;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_MODEM, MASKED_VERSION);
    }

    private int modelID;
    private String model;
    private String phoneNumber;
    private String serialNumber;
    private String deviceName;
    private String fwVersion;
    private String cfgVersion;
    private String prlVersion;
    private String gpsVersion;
    private String rfVersion;
    private String hwVersion;
    private String osVersion;
    private String bootVersion;
    private String simCardNumber;

    public ModemInfoPayload_v3()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream in)
        throws IOException
    {
        try {
            int len = Conversion.reverse(in.readShort()) & 0xffff;

            LimitedInputStream lin = new LimitedInputStream(in);
            lin.setLimit(len);

            try {
                DataInputStream din = new DataInputStream(lin);

                modelID = Conversion.reverse(din.readInt());

                model = readNullTerminatedString(din, 32);
                phoneNumber = readNullTerminatedString(din, 32);
                serialNumber = readNullTerminatedString(din, 32);
                deviceName = readNullTerminatedString(din, 32);
                fwVersion = readNullTerminatedString(din, 32);
                cfgVersion = readNullTerminatedString(din, 32);
                prlVersion = readNullTerminatedString(din, 32);
                gpsVersion = readNullTerminatedString(din, 64);
                rfVersion = readNullTerminatedString(din, 32);
                hwVersion = readNullTerminatedString(din, 32);
                osVersion = readNullTerminatedString(din, 32);
                bootVersion = readNullTerminatedString(din, 32);
                simCardNumber = readNullTerminatedString(din, 32);
            }
            finally {
                lin.discardToCompletion();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted Modem (MI3) data", ioe);
        }
    }

    public ModemInfoPayload_v3(int modelID, String model, String phoneNumber,
                               String serialNumber, String deviceName,
                               String fwVersion, String cfgVersion,
                               String prlVersion, String gpsVersion,
                               String rfVersion, String hwVersion,
                               String osVersion, String bootVersion,
                               String simCardNumber)
    {

        this.modelID = modelID;
        this.model = model;
        this.serialNumber = serialNumber;
        this.deviceName = deviceName;
        this.fwVersion = fwVersion;
        this.cfgVersion = cfgVersion;
        this.prlVersion = prlVersion;
        this.gpsVersion = gpsVersion;
        this.rfVersion = rfVersion;
        this.hwVersion = hwVersion;
        this.osVersion = osVersion;
        this.bootVersion = bootVersion;
        this.simCardNumber = simCardNumber;
    }

    /**
     * Get the model ID contained within this message.
     */
    public int getModelID()
    {
        return modelID;
    }

    public String getModel()
    {
        return model;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public String getSerialNumber()
    {
        return serialNumber;
    }

    public String getDeviceName()
    {
        return deviceName;
    }

    public String getFwVersion()
    {
        return fwVersion;
    }

    public String getCfgVersion()
    {
        return cfgVersion;
    }

    public String getPrlVersion()
    {
        return prlVersion;
    }

    public String getGpsVersion()
    {
        return gpsVersion;
    }

    public String getRfVersion()
    {
        return rfVersion;
    }

    public String getHwVersion()
    {
        return hwVersion;
    }

    public String getOsVersion()
    {
        return osVersion;
    }

    public String getBootVersion()
    {
        return bootVersion;
    }

    public String getSimCardNumber()
    {
        return simCardNumber;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        Conversion.shortToBytesLE(data, 0, (short)(data.length - 2));
        Conversion.intToBytesLE(data, 2, modelID);

        int offset = 6;
        offset = writeNullTerminatedString(model, data, offset, 32);
        offset = writeNullTerminatedString(phoneNumber, data, offset, 32);
        offset = writeNullTerminatedString(serialNumber, data, offset, 32);
        offset = writeNullTerminatedString(deviceName, data, offset, 32);
        offset = writeNullTerminatedString(fwVersion, data, offset, 32);
        offset = writeNullTerminatedString(cfgVersion, data, offset, 32);
        offset = writeNullTerminatedString(prlVersion, data, offset, 32);
        offset = writeNullTerminatedString(gpsVersion, data, offset, 64);
        offset = writeNullTerminatedString(rfVersion, data, offset, 32);
        offset = writeNullTerminatedString(hwVersion, data, offset, 32);
        offset = writeNullTerminatedString(osVersion, data, offset, 32);
        offset = writeNullTerminatedString(bootVersion, data, offset, 32);
        offset = writeNullTerminatedString(simCardNumber, data, offset, 32);

        return data;
    }

    @Override
    public int getLength()
    {
        // Length (2) + Model ID (4) + each string + \0 for each string
        return (2 + 4 + Math.min(32, model.getBytes().length) + 1 +
                Math.min(32, phoneNumber.getBytes().length) + 1 +
                Math.min(32, serialNumber.getBytes().length) + 1 +
                Math.min(32, deviceName.getBytes().length) + 1 +
                Math.min(32, fwVersion.getBytes().length) + 1 +
                Math.min(32, cfgVersion.getBytes().length) + 1 +
                Math.min(32, prlVersion.getBytes().length) + 1 +
                Math.min(64, gpsVersion.getBytes().length) + 1 +
                Math.min(32, rfVersion.getBytes().length) + 1 +
                Math.min(32, hwVersion.getBytes().length) + 1 +
                Math.min(32, osVersion.getBytes().length) + 1 +
                Math.min(32, bootVersion.getBytes().length) + 1 +
                Math.min(32, simCardNumber.getBytes().length) + 1);
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_MODEM;
    }

    @Override
    public int getPosition()
    {
        return 1;
    }

    @Override
    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);
        StringBuilder builder = new StringBuilder();

        builder
            .append(String.format("%sModel ID: %d\n", indentString, modelID));
        builder.append(String.format("%sModel: %s\n", indentString, model));
        builder.append(String.format("%sPhone Number: %s\n", indentString,
            phoneNumber));
        builder.append(String.format("%sSerial Number: %s\n", indentString,
            serialNumber));
        builder.append(String.format("%sDevice Name: %s\n", indentString,
            deviceName));
        builder.append(String.format("%sFW Version: %s\n", indentString,
            fwVersion));
        builder.append(String.format("%sCFG Version: %s\n", indentString,
            cfgVersion));
        builder.append(String.format("%sPRL Version: %s\n", indentString,
            prlVersion));
        builder.append(String.format("%sGPS Version: %s\n", indentString,
            gpsVersion));
        builder.append(String.format("%sRF Version: %s\n", indentString,
            rfVersion));
        builder.append(String.format("%sHW Version: %s\n", indentString,
            hwVersion));
        builder.append(String.format("%sOS Version: %s\n", indentString,
            osVersion));
        builder.append(String.format("%sBoot Version: %s\n", indentString,
            bootVersion));
        builder.append(String.format("%sSim Card Number: %s\n", indentString,
            simCardNumber));

        return builder.toString();
    }
}

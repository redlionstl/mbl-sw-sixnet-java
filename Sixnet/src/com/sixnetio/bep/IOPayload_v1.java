/*
 * IOInfo_v1.java
 *
 * Represents version 1 of an IO info payload node.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.DataInputStream;
import java.io.IOException;

import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;

public class IOPayload_v1
    extends PayloadNode
{
    /** Indicates the existence and version of the IO info payload. */
    private static final int MASK_IO = 0x0c;

    /** Version 1, second half-nibble. */
    private static final int MASKED_VERSION = 1 << 2;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_IO, MASKED_VERSION);
    }

    // Masks for the Digital I/O State field
    private static final int FLAG_DI1 = 0x01;
    private static final int FLAG_DI2 = 0x02;
    private static final int FLAG_DI3 = 0x04;
    private static final int FLAG_DI4 = 0x08;
    private static final int FLAG_IGN = 0x10;
    private static final int FLAG_DO1 = 0x010000;
    private static final int FLAG_DO2 = 0x020000;
    private static final int FLAG_DO3 = 0x040000;

    private boolean di1, di2, di3, di4;
    private boolean ign;
    private boolean do1, do2, do3;

    private float pwr;
    private float ai1, ai2, ai3;

    public IOPayload_v1()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream in)
        throws IOException
    {
        try {
            int len = in.readByte() & 0xff;

            LimitedInputStream lin = new LimitedInputStream(in);
            lin.setLimit(len);

            try {
                DataInputStream din = new DataInputStream(lin);

                int ioStates = Conversion.reverse(din.readInt());
                di1 = ((ioStates & FLAG_DI1) != 0);
                di2 = ((ioStates & FLAG_DI2) != 0);
                di3 = ((ioStates & FLAG_DI3) != 0);
                di4 = ((ioStates & FLAG_DI4) != 0);
                ign = ((ioStates & FLAG_IGN) != 0);
                do1 = ((ioStates & FLAG_DO1) != 0);
                do2 = ((ioStates & FLAG_DO2) != 0);
                do3 = ((ioStates & FLAG_DO3) != 0);

                pwr = Float.intBitsToFloat(Conversion.reverse(din.readInt()));
                ai1 = Float.intBitsToFloat(Conversion.reverse(din.readInt()));
                ai2 = Float.intBitsToFloat(Conversion.reverse(din.readInt()));
                ai3 = Float.intBitsToFloat(Conversion.reverse(din.readInt()));
            }
            finally {
                lin.discardToCompletion();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted IO1 data", ioe);
        }
    }

    public IOPayload_v1(boolean di1, boolean di2, boolean di3, boolean di4,
                        boolean ign, boolean do1, boolean do2, boolean do3,
                        float pwr, float ai1, float ai2, float ai3)
    {

        this.di1 = di1;
        this.di2 = di2;
        this.di3 = di3;
        this.di4 = di4;
        this.ign = ign;
        this.do1 = do1;
        this.do1 = do2;
        this.do1 = do3;

        this.pwr = pwr;
        this.ai1 = ai1;
        this.ai1 = ai2;
        this.ai1 = ai3;
    }

    public boolean getDI1()
    {
        return di1;
    }

    public boolean getDI2()
    {
        return di2;
    }

    public boolean getDI3()
    {
        return di3;
    }

    public boolean getDI4()
    {
        return di4;
    }

    public boolean getIGN()
    {
        return ign;
    }

    public boolean getDO1()
    {
        return do1;
    }

    public boolean getDO2()
    {
        return do2;
    }

    public boolean getDO3()
    {
        return do3;
    }

    public float getPower()
    {
        return pwr;
    }

    public float getAI1()
    {
        return ai1;
    }

    public float getAI2()
    {
        return ai2;
    }

    public float getAI3()
    {
        return ai3;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        data[0] = (byte)(data.length - 1);

        // The Digital I/O States field is encoded as a 4-byte int in
        // little-endian byte order
        data[1] = buildFlags(di1, di2, di3, di4, ign);
        data[2] = 0;
        data[3] = buildFlags(do1, do2, do3);
        data[4] = 0;

        Conversion.intToBytesLE(data, 5, Float.floatToIntBits(pwr));
        Conversion.intToBytesLE(data, 9, Float.floatToIntBits(ai1));
        Conversion.intToBytesLE(data, 13, Float.floatToIntBits(ai2));
        Conversion.intToBytesLE(data, 17, Float.floatToIntBits(ai3));

        return data;
    }

    @Override
    public int getLength()
    {
        return 21;
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_IO;
    }

    @Override
    public int getPosition()
    {
        return 3;
    }

    @Override
    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%sDI1: %b\n", indentString, di1));
        builder.append(String.format("%sDI2: %b\n", indentString, di2));
        builder.append(String.format("%sDI3: %b\n", indentString, di3));
        builder.append(String.format("%sDI4: %b\n", indentString, di4));
        builder.append(String.format("%sDO1: %b\n", indentString, do1));
        builder.append(String.format("%sDO2: %b\n", indentString, do2));
        builder.append(String.format("%sDO3: %b\n", indentString, do3));
        builder.append(String.format("%sIGN: %b\n", indentString, ign));

        builder.append(String.format("%sAI1: %f\n", indentString, ai1));
        builder.append(String.format("%sAI2: %f\n", indentString, ai2));
        builder.append(String.format("%sAI3: %f\n", indentString, ai3));
        builder.append(String.format("%sPWR: %f\n", indentString, pwr));

        return builder.toString();
    }
}

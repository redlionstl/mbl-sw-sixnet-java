/*
 * InfoNode.java
 *
 * The superclass of the various nodes that may be part of a BEP payload.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.*;
import java.util.ArrayList;

/**
 * Represents a BEP payload node. Implements Comparable to compare by position.
 * Every subclass must have a public no-argument constructor and a no-argument
 * public static method named <code>init</code>, which registers the node with
 * {@link BEPMessage}.
 * 
 * @author Jonathan Pearson
 */
public abstract class PayloadNode
    implements Comparable<PayloadNode>
{
    protected PayloadNode()
    {
        // Nothing to do, just keep it from being public
    }

    /**
     * Read the data from a stream into the node.
     * 
     * @param din The stream to read from.
     * @throws IOException If there was a problem reading from the stream, or
     *             the data was not properly formatted.
     */
    protected abstract void readData(DataInputStream din)
        throws IOException;

    /**
     * Get the byte-by-byte representation of this node.
     */
    public abstract byte[] getBytes();

    /**
     * Get the length of this node, in bytes.
     */
    public abstract int getLength();

    /**
     * Get the payload mask value (used to determine which payload field this
     * node puts its indicator in).
     */
    public abstract long getMask();

    /**
     * Return the value which would indicate that this node was present, if it
     * existed in the payload mask field.
     */
    public abstract int getPayloadIndicator();

    /**
     * Get the absolute position of this payload node in the sequence that
     * follows the BEP header.
     */
    public abstract int getPosition();

    @Override
    public int compareTo(PayloadNode node)
    {
        return (getPosition() - node.getPosition());
    }

    @Override
    public String toString()
    {
        return toString(0);
    }

    /**
     * Return a description of this payload node (newlines are fine), prefixing
     * each line with the specified number of spaces.
     * 
     * @param indent The number of spaces to prefix each line with.
     * @return A description of this payload node.
     */
    public abstract String toString(int indent);

    /**
     * Read a null-terminated string from a block of (ASCII) bytes.
     * 
     * @param din The data input stream to read from.
     * @param max The maximum number of bytes to read (including the terminating
     *            \0) before stopping. Pass -1 if there is no maximum.
     * @return The string read.
     * @throws IOException If there is a problem reading from the stream.
     */
    public static String readNullTerminatedString(DataInputStream din, int max)
        throws IOException
    {
        // Use an ArrayList to keep track of the bytes as we read them
        ArrayList<Byte> bytes;
        if (max == -1) {
            // Pick a large enough number that we will not likely need to resize
            bytes = new ArrayList<Byte>(256);
        }
        else {
            bytes = new ArrayList<Byte>(max);
        }

        // Read until we hit the max or a \0
        int count = 0;
        byte readByte;
        while ((max == -1 || count < max) &&
               (readByte = din.readByte()) != (byte)0) {

            bytes.add(readByte);
            count++;
        }

        // Transfer the bytes from the ArrayList to a plain byte array
        byte[] realBytes = new byte[bytes.size()];
        int pos = 0;
        for (Byte b : bytes) {
            realBytes[pos++] = b;
        }

        // Turn it into a String
        try {
            return new String(realBytes, "US-ASCII");
        }
        catch (UnsupportedEncodingException uee) {
            throw new RuntimeException(
                "Standard charset US-ASCII not implemented", uee);
        }
    }

    /**
     * Read a length-encoded string from a block of bytes. The length is encoded
     * as the leading byte, with an obvious maximum of 255. The string is ASCII.
     * 
     * @param din The stream to read from.
     * @param max The maximum value of the leading 'length' byte. Pass -1 to
     *            allow any leading length byte.
     * @return The string read.
     * @throws IOException If there is a problem reading from the stream.
     */
    public static String readLengthEncodedString(DataInputStream din, int max)
        throws IOException
    {
        // Read the length byte
        // The bitwise AND prevents sign extension
        int length = din.readByte() & 0xff;

        // Enforce the maximum
        if (max != -1 && length > max) {
            length = max;
        }

        // Read that many bytes
        byte[] bytes = new byte[length];
        din.readFully(bytes);

        // Turn it into a String
        try {
            return new String(bytes, "US-ASCII");
        }
        catch (UnsupportedEncodingException uee) {
            throw new RuntimeException(
                "Standard charset US-ASCII not implemented", uee);
        }
    }

    /**
     * Write a null-terminated string into a block of bytes.
     * 
     * @param str The string to write.
     * @param data The block to write to.
     * @param offset The offset to start writing at.
     * @param max The maximum number of characters to include, not including the
     *            terminating \0. If -1 then no limit is applied.
     * @return The offset of the next byte beyond the terminating null.
     * @throws ArrayIndexOutOfBoundsException If there is not enough space in
     *             the byte array.
     */
    public static int writeNullTerminatedString(String str, byte[] data,
                                                int offset, int max)
    {

        byte[] bytes;
        try {
            bytes = str.getBytes("US-ASCII");
        }
        catch (UnsupportedEncodingException uee) {
            throw new RuntimeException(
                "Standard charset US-ASCII not implemented", uee);
        }

        int length;
        if (max == -1) {
            length = bytes.length;
        }
        else {
            length = Math.min(max, bytes.length);
        }

        System.arraycopy(bytes, 0, data, offset, length);
        data[offset + length] = (byte)0;

        return (offset + length + 1);
    }

    /**
     * Write a length-encoded string into a block of bytes. The length is
     * encoded as a single unsigned byte, so the maximum length is 255. The
     * string is encoded as ASCII.
     * 
     * @param str The string to write.
     * @param data The block to write to.
     * @param offset The offset to start writing at.
     * @param max The maximum number of characters to include, not including the
     *            leading length byte. If -1 then no limit is applied.
     * @return The offset of the next byte beyond the end of the string.
     * @throws ArrayIndexOutOfBoundsException If there is not enough space in
     *             the byte array.
     */
    public static int writeLengthEncodedString(String str, byte[] data,
                                               int offset, int max)
    {

        byte[] bytes;
        try {
            bytes = str.getBytes("US-ASCII");
        }
        catch (UnsupportedEncodingException uee) {
            throw new RuntimeException(
                "Standard charset US-ASCII not implemented", uee);
        }

        int length;
        if (max == -1) {
            length = bytes.length;
        }
        else {
            length = Math.min(max, bytes.length);
        }

        if (length > 255) {
            throw new IllegalArgumentException(
                "String too long for length-encoding");
        }

        data[offset] = (byte)(length & 0xff);
        System.arraycopy(bytes, 0, data, offset + 1, length);

        return (offset + length + 1);
    }

    /**
     * Build a byte containing boolean flags.
     * 
     * @param flags The flags. You may pass up to eight at a time. These will be
     *            placed in the byte from the least significant bit out of
     *            <tt>flags[0]</tt> upwards.
     * @return A byte containing bit flags based on the booleans passed in.
     * @throws IllegalArgumentException If more than eight flags are passed in.
     */
    public static byte buildFlags(Boolean... flags)
    {
        if (flags.length > 8) {
            throw new IllegalArgumentException("Too many flags");
        }

        byte data = 0;

        for (int i = 0; i < flags.length; i++) {
            if (flags[i] != null && flags[i]) {
                data |= (1 << i);
            }
        }

        return data;
    }
}

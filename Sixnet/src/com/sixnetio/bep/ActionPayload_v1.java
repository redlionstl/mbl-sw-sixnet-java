/*
 * ActionInfo_v1.java
 *
 * Represents a version 1 action payload node.
 *
 * Jonathan Pearson
 * August 27, 2009
 *
 */

package com.sixnetio.bep;

import java.io.DataInputStream;
import java.io.IOException;

import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;


public class ActionPayload_v1
    extends PayloadNode
{
    /**
     * Indicates the existence and version of the authentication info payload.
     */
    private static final long MASK_ACTION = 0x3000000000L;

    /** Version 1, third half-nibble of second payload field. */
    private static final int MASKED_VERSION = 1 << 4;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_ACTION, MASKED_VERSION);
    }

    private short requestID;
    private String action;

    public ActionPayload_v1()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream in)
        throws IOException
    {
        try {
            // Read the length (short), prevent sign extension
            int length = Conversion.reverse(in.readShort()) & 0xffff;

            LimitedInputStream lin = new LimitedInputStream(in);
            lin.setLimit(length);

            try {
                DataInputStream din = new DataInputStream(lin);

                requestID = Conversion.reverse(din.readShort());
                action =
                    readNullTerminatedString(din, Math.min(length - 2, 8192));
            }
            finally {
                lin.discardToCompletion();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted Action (AC1) data", ioe);
        }
    }

    public ActionPayload_v1(short requestID, String action)
    {
        this.requestID = requestID;
        this.action = action;
    }

    public short getRequestID()
    {
        return requestID;
    }

    public String getAction()
    {
        return action;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        Conversion.shortToBytesLE(data, 0, (short)(data.length - 2));
        Conversion.shortToBytesLE(data, 2, requestID);
        writeNullTerminatedString(action, data, 4, 8192);

        return data;
    }

    @Override
    public int getLength()
    {
        // 2 (length) + 2 (request ID) + message + \0 (1)
        return (2 + 2 + action.getBytes().length + 1);
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_ACTION;
    }

    @Override
    public int getPosition()
    {
        return 8;
    }

    @Override
    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%sRequest ID: %d\n", indentString,
            requestID));
        builder.append(String.format("%sAction: %s\n", indentString, action));

        return builder.toString();
    }
}

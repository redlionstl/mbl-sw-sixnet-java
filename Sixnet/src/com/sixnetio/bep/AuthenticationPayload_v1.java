/*
 * AuthInfo_v1.java
 *
 * Represents a version 1 authentication payload node.
 *
 * Jonathan Pearson
 * August 26, 2009
 *
 */

package com.sixnetio.bep;

import java.io.*;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;


public class AuthenticationPayload_v1
    extends PayloadNode
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /**
     * Indicates the existence and version of the authentication info payload.
     */
    private static final long MASK_AUTH = 0xc000000000L;

    /** Version 1, fourth half-nibble of second payload field. */
    private static final int MASKED_VERSION = 1 << 6;

    /** Register this payload with BEPMessage. */
    public static void init()
    {
        BEPMessage.registerNodeType(MASK_AUTH, MASKED_VERSION);
    }

    /**
     * Sign a BEP message with an AU1 payload node.
     * 
     * <p>
     * If no AU1 node exists in the message, a new one will be created with the
     * provided nonce.
     * </p>
     * 
     * <p>
     * If no AU1 exists and the provided <tt>nonce</tt> is <tt>null</tt>, a
     * {@link NullPointerException} will be thrown.
     * </p>
     * 
     * <p>
     * If both exist, the AU1 node's nonce will be set to the value provided.
     * </p>
     * 
     * @param msg The message to sign.
     * @param nonce If non-<tt>null</tt>, the nonce to use.
     * @param modemPasswd The password of the modem that this message will be
     *            sent to.
     */
    public static void signMessage(BEPMessage msg, String nonce,
                                   String modemPasswd)
    {
        // Find the Authentication payload in the message, we'll need to update
        // it
        AuthenticationPayload_v1 au1 = null;
        for (PayloadNode node : msg) {
            if (node instanceof AuthenticationPayload_v1) {
                au1 = (AuthenticationPayload_v1)node;
                break;
            }
        }

        if (au1 == null) {
            // No authentication payload? Make a new one, if we can
            if (nonce == null) {
                throw new NullPointerException("No nonce provided");
            }

            au1 = new AuthenticationPayload_v1(nonce);
            msg.addInfoNode(au1);
        }
        else {
            // Make sure its MD5 sum is set to all 1s
            byte[] allOnes = new byte[16];
            Arrays.fill(allOnes, (byte)0xff);
            au1.setMD5Bytes(allOnes);

            // Make sure the nonce is the same, if one was provided
            if (nonce != null) {
                au1.setNonce(nonce);
            }
            else {
                nonce = au1.getNonce();
            }
        }

        // Compute the MD5 sum over the message, nonce, and password
        MessageDigest digester = Utils.getMD5Digester();
        byte[] md5;

        digester.update(msg.getBytes());
        try {
            digester.update(nonce.getBytes("US-ASCII"));
            md5 = digester.digest(modemPasswd.getBytes("US-ASCII"));
        }
        catch (UnsupportedEncodingException uee) {
            throw new RuntimeException(
                "Standard encoding US-ASCII not implemented: " +
                    uee.getMessage(), uee);
        }

        // Put the new MD5 sum into the message
        au1.setMD5Bytes(md5);
    }

    /**
     * Check a BEP message for authenticity.
     * 
     * @param msg The message to check.
     * @param modemPasswd The password for the modem that sent the message.
     * @return <tt>true</tt> if the BEP message contained no
     *         AuthenticationPayload1 payload node, or if it did and it passes
     *         the authentication check; <tt>false</tt> if the BEP message
     *         authentication payload did not pass the check.
     */
    public static boolean authenticateMessage(BEPMessage msg, String modemPasswd)
    {
        // Treat a blank password as an empty password
        if (modemPasswd == null) {
            modemPasswd = "";
        }

        // Does it have an Authentication payload?
        AuthenticationPayload_v1 payload = null;
        for (PayloadNode node : msg) {
            if (node instanceof AuthenticationPayload_v1) {
                payload = (AuthenticationPayload_v1)node;
                break;
            }
        }

        if (payload == null) {
            // No authentication payload? No way to prove that it is not
            // authentic.
            return true;
        }

        // Build an alternate copy of the BEP message, we need to twiddle it and
        // we don't want to alter the copy that came in
        BEPMessage msg2;
        try {
            byte[] bytes = msg.getBytes();

            if (logger.isDebugEnabled()) {
                logger.debug("BEP message back to bytes: " +
                             Conversion.bytesToHex(bytes));
            }

            msg2 = new BEPMessage(bytes, msg.isAcknowledgment());
        }
        catch (IOException ioe) {
            if (logger.isDebugEnabled()) {
                logger
                    .debug("BEP message generated an unparseable copy of itself");
                logger.debug(msg.toString());
                logger.debug(Conversion.bytesToHex(msg.getBytes()));
            }

            throw new RuntimeException(
                "BEP Message generated an unparseable copy of itself", ioe);
        }

        // Alter its authentication payload to report an MD5 of all FF bytes
        {
            byte[] newMD5 = new byte[16];
            Arrays.fill(newMD5, (byte)0xff);

            AuthenticationPayload_v1 replacement =
                new AuthenticationPayload_v1(payload.getNonce());

            for (Iterator<PayloadNode> itNodes = msg2.iterator(); itNodes
                .hasNext();) {
                PayloadNode node = itNodes.next();
                if (node instanceof AuthenticationPayload_v1) {
                    itNodes.remove();
                    break;
                }
            }

            msg2.addInfoNode(replacement);
        }

        // Convert to bytes and MD5 it
        MessageDigest digester = Utils.getMD5Digester();
        digester.update(msg2.getBytes());
        digester.update(payload.getNonce().getBytes());

        byte[] md5 = digester.digest(modemPasswd.getBytes());

        // Compare MD5 checksums
        return Arrays.equals(payload.getMD5Bytes(), md5);
    }

    private byte[] md5;
    private String nonce;

    public AuthenticationPayload_v1()
    {
        // Nothing to do
    }

    @Override
    protected void readData(DataInputStream in)
        throws IOException
    {
        try {
            int len = Conversion.reverse(in.readShort()) & 0xffff;

            LimitedInputStream lin = new LimitedInputStream(in);
            lin.setLimit(len);

            try {
                DataInputStream din = new DataInputStream(lin);

                md5 = new byte[16];
                din.readFully(md5);
                nonce = readNullTerminatedString(din, 128);
            }
            finally {
                lin.discardToCompletion();
            }
        }
        catch (IOException ioe) {
            throw new IOException("Badly formatted Authentication (AU1) data",
                ioe);
        }
    }

    public AuthenticationPayload_v1(String nonce)
    {
        this.md5 = new byte[16];
        Arrays.fill(md5, (byte)0xff);

        this.nonce = nonce;
    }

    public AuthenticationPayload_v1(String md5, String nonce)
    {
        this.md5 = Conversion.hexToBytes(md5);
        this.nonce = nonce;
    }

    public byte[] getMD5Bytes()
    {
        byte[] bytes = new byte[md5.length];
        System.arraycopy(md5, 0, bytes, 0, bytes.length);
        return bytes;
    }

    /**
     * Set the MD5 bytes for this authentication payload.
     * 
     * @param newMD5 The new MD5 bytes. Must be at least 16 bytes long.
     */
    void setMD5Bytes(byte[] newMD5)
    {
        System.arraycopy(newMD5, 0, this.md5, 0, this.md5.length);
    }

    public String getMD5String()
    {
        return Conversion.bytesToHex(md5);
    }

    public String getNonce()
    {
        return nonce;
    }

    void setNonce(String nonce)
    {
        this.nonce = nonce;
    }

    @Override
    public byte[] getBytes()
    {
        byte[] data = new byte[getLength()];

        Conversion.shortToBytesLE(data, 0, (short)(data.length - 2));

        System.arraycopy(md5, 0, data, 2, md5.length);
        writeNullTerminatedString(nonce, data, 18, 128);

        return data;
    }

    @Override
    public int getLength()
    {
        // Length (2) + MD5 (16) + nonce + \0 (1)
        return (2 + 16 + nonce.getBytes().length + 1);
    }

    @Override
    public int getPayloadIndicator()
    {
        return MASKED_VERSION;
    }

    @Override
    public long getMask()
    {
        return MASK_AUTH;
    }

    @Override
    public int getPosition()
    {
        return 7;
    }

    @Override
    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%sMD5: %s\n", indentString, Conversion
            .bytesToHex(md5)));
        builder.append(String.format("%sNonce: %s\n", indentString, nonce));

        return builder.toString();
    }
}

/*
 * BEPMessage.java
 *
 * Represents a BEP (BlueTree Event Protocol) message and allows construction of
 * new messages.
 *
 * Jonathan Pearson
 * December 22, 2008
 * 
 * August 27, 2009 (Jonathan Pearson):
 * Overhauled to make this more modular and to add the newest BEP functionality.
 *
 */

package com.sixnetio.bep;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.Modem.DeviceID;
import com.sixnetio.io.LimitedInputStream;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;

/**
 * Provides parsing and building functionality for BEP (BlueTree Event Protocol)
 * messages. This class is <b>not thread-safe</b>, so if you will be using it in
 * a multithreaded environment, you are responsible for performing the necessary
 * synchronization.
 * 
 * @author Jonathan Pearson
 */
public class BEPMessage
    implements Iterable<PayloadNode>
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    private static final Object l_sequence = new Object();
    private static int sequence = 1;

    /** Holds classes of registered BEP node types. */
    private static Map<Long, Map<Integer, Class<? extends PayloadNode>>> registeredNodes =
        new HashMap<Long, Map<Integer, Class<? extends PayloadNode>>>();

    private static boolean initialized = false;

    private static synchronized void init()
    {
        if (!initialized) {
            initialized = true;
            // Initialize the node types that we know about; others will need to
            // be loaded some other way (likely through some plugin-type method)
            ActionPayload_v1.init();
            AuthenticationPayload_v1.init();
            GPSPayload_v1.init();
            IOPayload_v1.init();
            IPPayload_v1.init();
            ModemInfoPayload_v1.init();
            ModemInfoPayload_v2.init();
            ModemInfoPayload_v3.init();
            OdometerPayload_v1.init();
            RFPayload_v1.init();
            RFPayload_v2.init();
        }
    }

    /**
     * Get a new sequence number, unique to this JVM instance.
     */
    public static int getNewSequence()
    {
        synchronized (l_sequence) {
            return sequence++;
        }
    }

    /**
     * Register a new BEP node type.
     * 
     * @param mask The mask that may be applied to the 'Payload Mask' field of a
     *            message to determine whether this node type is present in the
     *            payload. If this has bits beyond the low-order 32 set, the
     *            fifth byte of the mask will be applied to the second payload
     *            mask field rather than the first, and will be compared with an
     *            un-altered version (so you should use the low-order byte of
     *            version to match).
     * @param version The value that the 'Payload Mask' field will have after
     *            being bitwise ANDed with <tt>mask</tt> if this node type is
     *            present. Note that this is not likely to be a number like 1 or
     *            2, but rather a value which would be non-zero after ANDing
     *            with the <tt>mask</tt>.
     * @param clazz The class of this node type.
     * @throws IllegalStateException If a class is already registered for that
     *             combination of mask and version.
     */
    public static void registerNodeType(long mask, int version,
                                        Class<? extends PayloadNode> clazz)
        throws IllegalStateException
    {

        // Make sure the mask matches the version
        if ((mask & 0xffffffff00000000L) == 0) {
            // Normal payload field, simple bitwise &
            int shortMask = (int)(mask & 0xffffffff);

            if ((version & ~shortMask) != 0) {
                throw new IllegalArgumentException(String.format(
                    "Version 0x%x does not match mask 0x%x", version, mask));
            }
        }
        else {
            // Extra payload field, shift before checking
            int shortMask = (int)((mask >> 32) & 0xffffffff);

            if ((version & ~shortMask) != 0) {
                throw new IllegalArgumentException(
                    String
                        .format(
                            "Version 0x%x does not match mask 0x%x for secondary payload field",
                            version, mask));
            }
        }

        synchronized (registeredNodes) {
            if (registeredNodes.containsKey(mask) &&
                registeredNodes.get(mask).containsKey(version)) {

                throw new IllegalStateException(String.format(
                    "Duplicate node class: mask 0x%x, version %d", mask,
                    version));
            }

            Map<Integer, Class<? extends PayloadNode>> submap =
                registeredNodes.get(mask);

            if (submap == null) {
                submap = new HashMap<Integer, Class<? extends PayloadNode>>();
                registeredNodes.put(mask, submap);
            }

            submap.put(version, clazz);
        }
    }

    /**
     * When called from an InfoNode subclass, this will find that class on the
     * call stack and register it. Use this to avoid cut/paste errors. This will
     * NOT WORK if you call it from a different classloader.
     * 
     * @param mask See {@link #registerNodeType(long, int, Class)}.
     * @param version See {@link #registerNodeType(long, int, Class)}.
     * @throws IllegalStateException See
     *             {@link #registerNodeType(long, int, Class)}.
     * @throws RuntimeException If an instance of InfoNode could not be found on
     *             the call stack.
     */
    @SuppressWarnings("unchecked")
    public static void registerNodeType(long mask, int version)
        throws IllegalStateException, RuntimeException
    {

        // Get a stack trace
        StackTraceElement[] trace;
        try {
            throw new Throwable();
        }
        catch (Throwable th) {
            trace = th.getStackTrace();
        }

        // Search for a subclass of InfoNode
        for (StackTraceElement elem : trace) {
            Class<?> clazz;
            try {
                clazz = Class.forName(elem.getClassName());
            }
            catch (ClassNotFoundException cnfe) {
                throw new RuntimeException(
                    "Cannot locate class in call stack: " + cnfe.getMessage(),
                    cnfe);
            }

            if (PayloadNode.class.isAssignableFrom(clazz)) {
                // Found it

                // The cast to Class<? extends InfoNode> requires the
                // "unchecked" warning suppression above
                registerNodeType(mask, version,
                    (Class<? extends PayloadNode>)clazz);
                return;
            }
        }

        // Not found, throw an exception
        throw new RuntimeException("Call stack did not include an instance "
                                   + "of InfoNode");
    }

    /**
     * Retrieve a Map of masks onto the set of available versions for node
     * classes registered for each mask.
     */
    public static Map<Long, Set<Integer>> getRegisteredNodeTypes()
    {
        Map<Long, Set<Integer>> types = new HashMap<Long, Set<Integer>>();

        synchronized (registeredNodes) {
            for (Map.Entry<Long, Map<Integer, Class<? extends PayloadNode>>> entry : registeredNodes
                .entrySet()) {

                types.put(entry.getKey(), new HashSet<Integer>(entry.getValue()
                    .keySet()));
            }
        }

        return types;
    }

    /**
     * Instantiate a new node.
     * 
     * @param mask The payload mask which was applied to determine that this
     *            node was present.
     * @param version The result of ANDing the mask with the 'Payload Mask'
     *            field of the message.
     * @return An InfoNode which can represent the specified type of payload.
     *         Use its {@link PayloadNode#readData(DataInputStream)} function to
     *         read the data out of a stream.
     * @throws InvocationTargetException If the node's constructor threw an
     *             exception. This should not happen.
     * @throws IllegalArgumentException If the mask or mask/version combination
     *             are not recognized.
     * @throws IllegalStateException If the node class cannot be instantiated
     *             due to some structural problem within that class, such as not
     *             having a public no-arguments constructor.
     */
    public static PayloadNode instantiateNode(long mask, int version)
        throws InvocationTargetException, IllegalArgumentException,
        IllegalStateException
    {

        synchronized (registeredNodes) {
            // Grab the submap containing the class
            Map<Integer, Class<? extends PayloadNode>> submap =
                registeredNodes.get(mask);

            // If null, that mask has not been registered
            if (submap == null) {
                throw new IllegalArgumentException(String.format(
                    "Unrecognized mask: 0x%x", mask));
            }

            // Grab the class from the submap
            Class<? extends PayloadNode> clazz = submap.get(version);

            // If null, that version has not been registered
            if (clazz == null) {
                throw new IllegalArgumentException(String.format(
                    "Unrecognized mask/version combo: 0x%x/%d", mask, version));
            }

            // Grab the no-arguments constructor for that class
            // This will throw a NoSuchMethodException rather than returning
            // null if the constructor cannot be found
            Constructor<? extends PayloadNode> constructor;
            try {
                constructor = clazz.getConstructor();
            }
            catch (NoSuchMethodException nsme) {
                throw new IllegalStateException(String.format(
                    "Class '%s' does not have a no-argument constructor", clazz
                        .getName()), nsme);
            }

            // Construct a new instance of the node
            try {
                return constructor.newInstance();
            }
            catch (InstantiationException ie) {
                throw new IllegalStateException(String.format(
                    "Class '%s' cannot be instantiated: %s", clazz.getName(),
                    ie.getMessage()), ie);
            }
            catch (IllegalAccessException iae) {
                throw new IllegalStateException(String.format(
                    "Class '%s' has insufficient permissions on its"
                        + " no-argument constructor to allow reflective"
                        + " construction: %s", clazz.getName(), iae
                        .getMessage()), iae);
            }
        }
    }

    // General Options masks
    /** Message is in big-endian byte order (not supported). */
    private static final int OPT_BIGENDIAN = 0x08;

    /** Message is compressed (not supported). */
    private static final int OPT_COMPRESSED = 0x04;

    /** Message is encrypted (not supported). */
    private static final int OPT_ENCRYPTED = 0x02;

    /** Sender expects an acknowledgment. */
    private static final int OPT_REQUIRE_ACK = 0x01;

    /** Mask to retrieve the protocol version. */
    private static final int MASK_PROTOCOL_VERSION = 0x0f;

    // Application ID values
    /** Application ID for AVL. */
    public static final int APPID_AVL = 0x1;

    /** Reserved application ID. */
    public static final int APPID_RESERVED = 0x2;

    /** Application ID for MOM (Mobile Originated Management). */
    public static final int APPID_MOM = 0x3;

    /** Application ID for initial configuration. */
    public static final int APPID_INITCFG = 0x4;

    // Version values
    /**
     * If the general options masked with {@link #MASK_PROTOCOL_VERSION} is
     * equal to this, then this is a version 1 message.
     */
    private static final int VERSION_1 = 0x01;


    /** Size of a BEP header. */
    private static final int BEP_HEADER_SIZE = 24;

    private DeviceID devID;

    private boolean requireACK;
    private boolean encrypted;
    private boolean compressed;
    private boolean bigEndian;

    private int applicationID;

    private int protocolVersion;

    private int seq;

    private int eventID;

    private boolean acknowledgment;
    private int ackSeq;


    // Outside of the constructor, this is read-only
    private List<PayloadNode> nodes;

    /**
     * Parse a BEP message from a byte array.
     * 
     * @param data The byte array containing the BEP message.
     * @param isACK Whether to parse the message as a normal BEP message or as
     *            an acknowledgment.
     * @throws IOException If there was a parsing error.
     */
    public BEPMessage(byte[] data, boolean isACK)
        throws IOException
    {
        this(new ByteArrayInputStream(data), isACK);
    }

    /**
     * Parse a BEP message from an input stream.
     * 
     * @param in The input stream to read from.
     * @param isACK Whether to parse the message as a normal BEP message or as
     *            an acknowledgment.
     * @throws IOException If there was a parsing error.
     */
    public BEPMessage(InputStream in, boolean isACK)
        throws IOException
    {
        init();

        LimitedInputStream lin = new LimitedInputStream(in);
        DataInputStream din = new DataInputStream(lin);

        // Read out the data
        try {
            devID = DeviceID.fromBEPLong(Conversion.reverse(din.readLong()));
        }
        catch (NumberFormatException nfe) {
            throw new IOException("Bad device ID: " + nfe.getMessage(), nfe);
        }

        byte plMask2 = din.readByte();

        byte options = din.readByte();

        bigEndian = ((options & OPT_BIGENDIAN) != 0);
        compressed = ((options & OPT_COMPRESSED) != 0);
        encrypted = ((options & OPT_ENCRYPTED) != 0);
        requireACK = ((options & OPT_REQUIRE_ACK) != 0);

        // Use bit mask to convert to int without sign extension
        applicationID = din.readByte() & 0xff;

        protocolVersion = din.readByte() & MASK_PROTOCOL_VERSION;

        if (protocolVersion != VERSION_1) {
            if (logger.isDebugEnabled()) {
                logger.debug(String.format(
                    "Received partial message (bad protocol version) from %s: \n"
                        + "  Payload mask: 0x%02x\n" + "  Big Endian:   %b\n"
                        + "  Compressed:   %b\n" + "  Encrypted:    %b\n"
                        + "  Require ACK:  %b\n" + "  App ID:       %d\n"
                        + "  Proto Vers:   %d", devID.toString(), plMask2,
                    bigEndian, compressed, encrypted, requireACK,
                    applicationID, protocolVersion));

                logger.debug(String.format(
                    "Bad data read from stream: %08x%02x%02x%02x%02x",
                    Conversion.reverse(devID.asBEPLong()), plMask2, options,
                    applicationID, protocolVersion));
            }

            throw new IOException("Unknown BEP protocol version: " +
                                  protocolVersion);
        }

        seq = Conversion.reverse(din.readInt());

        // Prevent sign extension
        int len = Conversion.reverse(din.readShort()) & 0xffff;
        logger.debug(String.format("Reading a %d byte message", len));
        lin.setLimit(len);

        try {
            eventID = Conversion.reverse(din.readShort());

            int plMask;
            if (isACK) {
                acknowledgment = true;
                ackSeq = Conversion.reverse(din.readInt());

                // Prevent any attempts to parse payloads from this field
                plMask = 0;
            }
            else {
                plMask = din.readInt();
            }

            // Search for nodes
            // If there are any unrecognized nodes, we cannot parse the message
            // because we have no way to determine the node length

            // These masks have bits dropped from them as we search through the
            // payloads; if either is non-zero afterwards, then there is a
            // payload that we do not recognize and therefore cannot parse
            // (because the size of the payload length field cannot be
            // determined, so we don't know how many bytes to skip to get to the
            // next payload)
            int zeroingPLMask = plMask;
            int zeroingPLMask2 = plMask2;

            Map<Long, Set<Integer>> knownNodes = getRegisteredNodeTypes();
            Map<Long, Integer> foundPayloads = new HashMap<Long, Integer>();
            for (Map.Entry<Long, Set<Integer>> entry : knownNodes.entrySet()) {
                // Test for this entry
                long mask = entry.getKey();

                int version;

                // Test whether this payload is marked by the first or second
                // payload mask field
                if ((mask & 0xffffffff00000000L) == 0) {
                    // First payload mask field
                    version = plMask & (int)(mask & 0xffffffff);

                    // Drop the bits from the first zeroing payload mask that
                    // corresponds to this payload being present
                    zeroingPLMask &= ~(int)(mask & 0xffffffff);
                }
                else {
                    // Applied to the second payload field (high byte of genOpt)
                    version = plMask2 & (int)((mask >> 32) & 0xffffffff);

                    // Drop the bits from the second zeroing payload mask
                    zeroingPLMask2 &= ~((mask >> 32) & 0xffffffff);
                }

                // Is this payload present in the message?
                if (version != 0) {
                    // Yes, it is present
                    // Do we know how to parse this specific version of the
                    // payload?
                    if (entry.getValue().contains(version)) {
                        // Yes, we recognize this version
                        foundPayloads.put(mask, version);
                    }
                    else {
                        // No, we do not recognize this version
                        throw new IOException(String.format(
                            "Unknown payload mask/version: 0x%x/%d", mask,
                            version));
                    }
                }
            }

            if (zeroingPLMask != 0) {
                throw new IOException(String.format("Message contains unknown "
                                                    + "payloads: 0x%x",
                    zeroingPLMask));
            }
            else if (zeroingPLMask2 != 0) {
                throw new IOException(String.format(
                    "Message contains unknown " + "secondary payloads: 0x%x",
                    zeroingPLMask2));
            }

            // Create the payload objects
            nodes = new LinkedList<PayloadNode>();
            for (Map.Entry<Long, Integer> entry : foundPayloads.entrySet()) {
                try {
                    nodes
                        .add(instantiateNode(entry.getKey(), entry.getValue()));
                }
                catch (IllegalArgumentException iae) {
                    throw new IOException("Bad BEP message: " +
                                          iae.getMessage(), iae);
                }
                catch (IllegalStateException ise) {
                    throw new RuntimeException("Bad Payload class: " +
                                               ise.getMessage(), ise);
                }
                catch (InvocationTargetException ite) {
                    throw new RuntimeException(
                        "Payload class failed to initialize: " +
                            ite.getMessage(), ite);
                }
            }

            // Sort by packet order
            {
                PayloadNode[] ordered =
                    nodes.toArray(new PayloadNode[nodes.size()]);
                Arrays.sort(ordered);
                nodes = Utils.makeLinkedList(ordered);
            }

            // Initialize the payloads
            for (PayloadNode node : nodes) {
                node.readData(din);
            }
        }
        finally {
            // Make sure everything was read
            lin.discardToCompletion();
        }
    }

    public BEPMessage(DeviceID devID, boolean requireACK, boolean encrypted,
                      boolean compressed, boolean bigEndian, int applicationID,
                      int protocolVersion, int seq, int eventID)
    {
        init();

        this.devID = devID;
        this.requireACK = requireACK;
        this.encrypted = encrypted;
        this.compressed = compressed;
        this.bigEndian = bigEndian;
        this.applicationID = applicationID;
        this.protocolVersion = protocolVersion;
        this.seq = seq;
        this.eventID = eventID;

        this.nodes = new LinkedList<PayloadNode>();
    }

    public Iterator<PayloadNode> iterator()
    {
        return nodes.iterator();
    }

    public void addInfoNode(PayloadNode node)
    {
        nodes.add(node);
    }

    public DeviceID getDeviceID()
    {
        return devID;
    }

    public boolean getRequireACK()
    {
        return requireACK;
    }

    public boolean isEncrypted()
    {
        return encrypted;
    }

    public boolean isCompressed()
    {
        return compressed;
    }

    public boolean isBigEndian()
    {
        return bigEndian;
    }

    public int getSequence()
    {
        return seq;
    }

    public int getApplicationID()
    {
        return applicationID;
    }

    public int getEventID()
    {
        return eventID;
    }

    public void setAcknowledgment(boolean ack)
    {
        this.acknowledgment = ack;
    }

    public boolean isAcknowledgment()
    {
        return acknowledgment;
    }

    public void setAckSeq(int seq)
    {
        this.ackSeq = seq;
    }

    public int getAckSeq()
    {
        return ackSeq;
    }

    public byte[] getBytes()
    {
        List<byte[]> pieces = new LinkedList<byte[]>();
        int byteCount = 0;

        // Header
        {
            byte[] header = new byte[BEP_HEADER_SIZE];

            Conversion.longToBytes(header, 0, Conversion.reverse(devID
                .asBEPLong()));

            header[8] = 0; // Extra payload mask
            header[9] =
                PayloadNode.buildFlags(requireACK, encrypted, compressed,
                    bigEndian);
            header[10] = (byte)(applicationID & 0xff);
            header[11] = (byte)(protocolVersion & 0x0f);

            Conversion.intToBytesLE(header, 12, seq);
            Conversion
                .shortToBytesLE(header, 16, (short)(getLength() & 0xffff));
            Conversion.shortToBytesLE(header, 18, (short)(eventID & 0xffff));

            if (acknowledgment) {
                Conversion.intToBytesLE(header, 20, ackSeq);

                // Build the payload mask
                int plMask = 0;
                for (PayloadNode node : this.nodes) {
                    long mask = node.getMask();

                    if ((mask & 0xffffffff00000000L) == 0) {
                        throw new IllegalStateException(
                            "Non-ACK compatible payload found in ACK " +
                                "message: " + node.getClass().getName());
                    }
                    else {
                        header[8] |= (byte)(node.getPayloadIndicator() & 0xff);
                    }
                }
            }
            else {
                // Build the payload mask
                int plMask = 0;
                for (PayloadNode node : this.nodes) {
                    long mask = node.getMask();

                    if ((mask & 0xffffffff00000000L) == 0) {
                        plMask |= node.getPayloadIndicator();
                    }
                    else {
                        header[8] |= (byte)(node.getPayloadIndicator() & 0xff);
                    }
                }

                Conversion.intToBytes(header, 20, plMask);
            }

            pieces.add(header);
            byteCount += header.length;
        }

        // Pull out the nodes
        PayloadNode[] nodes =
            this.nodes.toArray(new PayloadNode[this.nodes.size()]);

        // Sort by position
        Arrays.sort(nodes);

        // Apply them in that order
        for (PayloadNode node : nodes) {
            byte[] data = node.getBytes();
            pieces.add(data);
            byteCount += data.length;
        }

        // Construct the final message
        byte[] msg = new byte[byteCount];
        int offset = 0;
        for (byte[] block : pieces) {
            System.arraycopy(block, 0, msg, offset, block.length);
            offset += block.length;
        }

        return msg;
    }

    public int getLength()
    {
        int len = BEP_HEADER_SIZE;

        for (PayloadNode node : nodes) {
            len += node.getLength();
        }

        return len;
    }

    /**
     * Construct an acknowledgment to this message.
     * 
     * @param requireACK Whether the ACK should be ACKed (if you are going to
     *            include an AC1 payload, then it should; otherwise it should
     *            not).
     * @return A new BEP message acknowledging this message.
     */
    public BEPMessage buildACK(boolean requireACK)
    {
        BEPMessage msg =
            new BEPMessage(devID, requireACK, encrypted, compressed, bigEndian,
                applicationID, protocolVersion, getNewSequence(), eventID);

        msg.setAcknowledgment(true);
        msg.setAckSeq(getSequence());

        return msg;
    }

    @Override
    public String toString()
    {
        return toString(0);
    }

    public String toString(int indent)
    {
        String indentString = Utils.multiplyChar(' ', indent);

        // Sort the payload nodes by position, to help the reader
        PayloadNode[] sortedNodes =
            nodes.toArray(new PayloadNode[nodes.size()]);
        Arrays.sort(sortedNodes);


        StringBuilder builder = new StringBuilder();

        if (acknowledgment) {
            builder.append(String.format("%sBEP Acknowledgement\n",
                indentString));
        }
        else {
            builder.append(String.format("%sBEP Message:\n", indentString));
        }

        builder.append(String.format("%s  Modem ID: %s\n", indentString, devID
            .toString()));
        builder.append(String.format("%s  Payload Mask 1:\n", indentString));

        // Determine which payload nodes are present in this payload mask field
        for (PayloadNode node : sortedNodes) {
            if ((node.getMask() & 0xffffffff00000000L) != 0) {
                builder.append(String.format("%s    %s\n", indentString, node
                    .getClass().getSimpleName()));

                builder.append(node.toString(indent + 6));
            }
        }

        builder.append(String.format("%s  Options:\n", indentString));

        builder.append(String.format("%s    Byte Order: %s\n", indentString,
            bigEndian ? "Big Endian" : "Little Endian"));
        builder.append(String.format("%s    Compressed: %b\n", indentString,
            compressed));
        builder.append(String.format("%s    Encrypted: %b\n", indentString,
            encrypted));
        builder.append(String.format("%s    ACK required: %b\n", indentString,
            requireACK));
        builder.append(String.format("%s    Application ID: %d\n",
            indentString, applicationID));
        builder.append(String.format("%s    Protocol Version: %d\n",
            indentString, protocolVersion));
        builder.append(String.format("%s  Sequence: %d\n", indentString, seq));
        builder.append(String.format("%s  Event ID: %d\n", indentString,
            eventID));

        if (acknowledgment) {
            builder.append(String.format("%s  ACK Seq: %d\n", indentString,
                ackSeq));
        }
        else {
            builder
                .append(String.format("%s  Payload Mask 2:\n", indentString));

            // Determine which payload nodes are present in this payload mask
            // field
            for (PayloadNode node : sortedNodes) {
                if ((node.getMask() & 0xffffffff00000000L) == 0) {
                    builder.append(String.format("%s    %s\n", indentString,
                        node.getClass().getSimpleName()));

                    builder.append(node.toString(indent + 6));
                }
            }
        }

        return builder.toString();
    }
}

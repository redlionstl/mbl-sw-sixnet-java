/*
 * HTTP.java
 *
 * A simple HTTP server.
 *
 * Jonathan Pearson
 * October 6, 2009
 *
 */

package com.sixnetio.server.http;

import java.io.*;
import java.net.*;
import java.nio.channels.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.zip.GZIPOutputStream;

import org.apache.log4j.Logger;

import com.sixnetio.server.*;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

public class HTTP
    extends Thread
{
    static Logger logger = Logger.getLogger(Utils.thisClassName());

    /** The default minimum number of threads to keep in the thread pool. */
    public static final int D_MIN_POOL_SIZE = 3;

    /** The default maximum number of threads to keep in the thread pool. */
    public static final int D_MAX_POOL_SIZE = 25;

    /** The default thread keepalive time, in milliseconds. */
    public static final long D_KEEPALIVE = 30000; // 30 seconds

    /**
     * The amount of time (in milliseconds) to wait for new connections before
     * checking whether the thread has been shut down.
     */
    private static final long CYCLE_TIME = 3000; // 3 seconds

    /**
     * The block size to use for transfer observers, in bytes.
     */
    private static final int BLOCK_SIZE = 1024;

    /**
     * Handles a single HTTP GET request.
     * 
     * @author Jonathan Pearson
     */
    private class HTTPConnection
        implements Runnable
    {
        /** The channel over which to communicate. */
        private SocketChannel channel;

        /**
         * Shortcut to {@link #channel}.{@link SocketChannel#socket() socket()}.
         * {@link Socket#getInputStream() getInputStream()}.
         */
        private InputStream sockIn;

        /**
         * Shortcut to {@link #channel}.{@link SocketChannel#socket() socket()}.
         * {@link Socket#getOutputStream() getOutputStream()}.
         */
        private OutputStream sockOut;

        /** The HTTP version from the request (we use the same in the response). */
        private String version;

        /** The Data Source that we are sending. */
        private DataSource file;

        /** The input stream used to read the data source. */
        private InputStream fileIn;

        /** The list of headers that were on the request. */
        private List<HTTPHeader> requestHeaders = new LinkedList<HTTPHeader>();

        /**
         * Construct a new HTTPConnection object.
         * 
         * @param channel The channel to communicate over.
         * @throws IOException If there is a problem getting the communication
         *             streams out of the channel.
         */
        public HTTPConnection(SocketChannel channel)
            throws IOException
        {
            this.channel = channel;
            this.sockIn = channel.socket().getInputStream();
            this.sockOut = channel.socket().getOutputStream();

            logger.debug("New HTTP connection from " +
                         channel.socket().getRemoteSocketAddress().toString());
        }

        /**
         * Send the HTTP header block to the client.
         * 
         * @param version The HTTP version to send (e.g. "HTTP/1.0" or
         *            "HTTP/1.1").
         * @param code The HTTP status code to send.
         * @param headers The headers to send.
         * @throws IOException If there was a problem sending data over the
         *             socket.
         */
        private void sendHeader(String version, StatusCode code,
                                List<HTTPHeader> headers)
            throws IOException
        {
            StringBuilder output = new StringBuilder();
            output.append(String.format("%s %s\r\n", version, code
                .getResponseString()));

            for (HTTPHeader header : headers) {
                output.append(header.toString());
                output.append("\r\n");
            }

            output.append("\r\n");

            logger.debug("Sending HTTP headers:\n" + output.toString());

            byte[] buffer = output.toString().getBytes();
            sockOut.write(buffer);
            sockOut.flush();
        }

        /**
         * Send an error code to the client and throw an IOException.
         * 
         * @param code The code to send.
         * @throws IOException <b>Always</b>, an exception with a similar
         *             message to the code being sent to the client.
         */
        private void error(StatusCode code)
            throws IOException
        {
            logger.error("HTTP error: " + code.getResponseString());

            if (sockOut != null) {
                List<HTTPHeader> responseHeaders = new LinkedList<HTTPHeader>();
                responseHeaders.add(new HTTPHeader("Content-Length", "0"));

                sendHeader("HTTP/1.0", code, responseHeaders);

                channel.close();
            }

            throw new IOException("Connection error: " + code.toString());
        }

        /**
         * Read the request from the client.
         * 
         * @throws IOException If there is an I/O error, or if the request
         *             cannot be fulfilled for some reason.
         */
        private void readRequest()
            throws IOException
        {
            // Read the first line
            String line = Utils.returnUntil(sockIn, "\n").trim();

            logger.debug("HTTP request line: '" + line + "'");

            // Split into pieces
            // Format: "Method URI Version\r\n"
            // Method: GET (there are others, but we do not support them)
            // URI: Full URI, we need to parse down to a file name
            // Version: HTTP/1.0 or HTTP/1.1
            StringTokenizer tok = new StringTokenizer(line, " ");

            String method;
            String uriString;
            String version;

            try {
                method = tok.nextToken();
                uriString = tok.nextToken();
                version = tok.nextToken();
            }
            catch (NoSuchElementException nsee) {
                error(StatusCode.BadRequest);

                // Keep the compiler happy
                return;
            }

            // We support GET
            if (!method.equalsIgnoreCase("GET")) {
                error(StatusCode.MethodNotAllowed);
            }

            // We support HTTP 1.0 and 1.1
            if (!(version.equalsIgnoreCase("HTTP/1.0") || version
                .equalsIgnoreCase("HTTP/1.1"))) {
                error(StatusCode.HTTPVersionNotSupported);
            }

            this.version = version;

            // Parse the URI
            URI uri;
            try {
                uri = new URI(uriString);
            }
            catch (URISyntaxException urise) {
                logger.warn("Bad URI from client", urise);

                error(StatusCode.BadRequest);

                // error() threw an IOException, this keeps the compiler happy
                return;
            }

            String fileName = uri.getPath();
            if (fileName == null) {
                logger.debug("No file name found in URI");

                error(StatusCode.BadRequest);
            }

            if (fileName.startsWith("/")) {
                fileName = fileName.substring(1);
            }

            this.file = files.get(fileName);
            if (this.file == null) {
                logger.debug("Unable to locate file '" + fileName + "'");

                error(StatusCode.NotFound);
            }

            try {
                this.fileIn = this.file.getInputStream();
            }
            catch (IOException ioe) {
                logger.error("Unable to read from data source", ioe);
                error(StatusCode.InternalServerError);
            }

            // Read the headers (until a blank line)
            do {
                line = Utils.returnUntil(sockIn, "\n").trim();

                if (line.length() > 0) {
                    try {
                        logger.debug("HTTP header: '" + line + "'");

                        requestHeaders.add(new HTTPHeader(line));
                    }
                    catch (IllegalArgumentException iae) {
                        // Bad header, ignore it
                    }
                }
            } while (line.length() > 0);

            this.file.fireTransferStarted(channel.socket().getInetAddress());
        }

        /**
         * Test whether the client accepts the GZip encoding.
         * 
         * @return <tt>true</tt> if the client accepts GZip encoding.
         *         <tt>false</tt> if the client does not accept GZip encoding.
         * @throws IOException If the client does not accept either of the two
         *             supported encodings, GZip and identity.
         */
        private boolean acceptsGZip()
            throws IOException
        {
            // Is there an "Accept-Encoding" header?
            HTTPHeader acceptEncoding = null;
            for (HTTPHeader header : requestHeaders) {
                if (header.equals("Accept-Encoding")) {
                    acceptEncoding = header;
                    break;
                }
            }

            // Parse that field
            List<HTTPEncoding> encodings;
            if (acceptEncoding != null) {
                encodings =
                    HTTPEncoding.parseEncodings(acceptEncoding.getValue());
            }
            else {
                encodings = HTTPEncoding.parseEncodings("");
            }

            // Check for "gzip", "identity", and "*" with Q-values above 0
            boolean supportsIdentity = false;
            for (HTTPEncoding encoding : encodings) {
                if (encoding.getName().equals("gzip") &&
                    encoding.getQValue() > 0.0f) {

                    return true;
                }
                else if (encoding.getName().equals("*") &&
                         encoding.getQValue() > 0.0f) {

                    return true;
                }
                else if (encoding.getName().equals("identity") &&
                         encoding.getQValue() > 0.0f) {

                    supportsIdentity = true;
                }
            }

            // If we get here, GZip is not supported but identity may be
            if (supportsIdentity) {
                // GZip is not supported, but at least we can send straight
                return false;
            }
            else {
                // Nothing that we support is supported
                logger
                    .debug("The client does not support gzip or identity transfer encodings");
                error(StatusCode.PreconditionFailed);
                return false; // Keep the compiler happy
            }
        }

        /**
         * Send our response to the client.
         * 
         * @throws IOException If there was an I/O error sending the response.
         */
        private void sendResponse()
            throws IOException
        {
            // Send the response line and headers
            List<HTTPHeader> responseHeaders = new LinkedList<HTTPHeader>();

            // Does the client support GZipped content? That is good for saving
            // transfer time and bandwidth
            boolean gzip = acceptsGZip();
            if (gzip) {
                logger.debug("Using 'gzip' transfer encoding");
                responseHeaders.add(new HTTPHeader("Content-Encoding", "gzip"));
            }
            else {
                logger.debug("Using 'identity' transfer encoding");
                responseHeaders.add(new HTTPHeader("Content-Length",
                    "" + file.getSize()));
                responseHeaders.add(new HTTPHeader("Content-Encoding",
                    "identity"));
            }

            sendHeader(version, StatusCode.OK, responseHeaders);

            // Send the body of the transfer, GZipping as necessary
            OutputStream writeTo = sockOut;
            if (gzip) {
                writeTo = new GZIPOutputStream(sockOut);
            }

            // Shortcuts so we aren't calling these functions all the time
            int totalBlocks =
                file.getSize() / BLOCK_SIZE +
                    (file.getSize() % BLOCK_SIZE == 0 ? 0 : 1);
            InetAddress clientAddress = channel.socket().getInetAddress();


            byte[] block = new byte[BLOCK_SIZE];
            int bytesRead;
            int total = 0;
            while ((bytesRead = fileIn.read(block)) != -1) {
                writeTo.write(block, 0, bytesRead);
                total += bytesRead;

                file.fireTransferProgress(clientAddress, total / BLOCK_SIZE,
                    totalBlocks);
            }

            if (gzip) {
                ((GZIPOutputStream)writeTo).finish();
            }

            writeTo.flush();

            // Fire one more time to make sure we hit 100%
            file.fireTransferProgress(clientAddress, totalBlocks, totalBlocks);

            logger.debug("Finished sending data");
        }

        @Override
        public void run()
        {
            try {
                try {
                    readRequest();
                }
                catch (IOException ioe) {
                    throw new IOException("HTTP request failed: " +
                                          ioe.getMessage(), ioe);
                }

                boolean success = false; // For the file transfer observer
                try {
                    sendResponse();
                    success = true;
                }
                catch (IOException ioe) {
                    throw new IOException("HTTP response failed: " +
                                          ioe.getMessage(), ioe);
                }
                finally {
                    this.file.fireTransferEnded(channel.socket()
                        .getInetAddress(), success);
                }
            }
            catch (IOException ioe) {
                logger.error("HTTP transfer failed", ioe);
            }
            finally {
                try {
                    channel.close();
                }
                catch (IOException ioe) {
                    logger.warn("Unable to close the channel", ioe);
                }
            }
        }
    }

    /** The mapping of paths onto shared files. */
    Hashtable<String, DataSource> files;

    /** Whether the server is running. */
    private boolean running = true;

    /** The server socket channel used to accept new connections. */
    private ServerSocketChannel serverChannel;

    /** The selector used to wait asynchronously on the channel. */
    private Selector selector;

    /** The thread pool executor that spins off new connection handlers. */
    private ThreadPoolExecutor executor;

    /**
     * Constructs a new HTTP server listening on the specified port number.
     * 
     * @param localAddress The local address to listen on.
     * @param backlog The number of connections to allow the server socket to
     *            keep in its backlog before new connection attempts are
     *            dropped.
     * @param capacity The maximum number of connection handler threads to
     *            allow.
     * @throws IOException If it was not possible to start.
     */
    public HTTP(InetSocketAddress localAddress, int backlog, int capacity)
        throws IOException
    {
        super("HTTP Server on " + localAddress.toString());
        setDaemon(true);

        files = new Hashtable<String, DataSource>();

        // Start up the thread pool
        BlockingQueue<Runnable> threadQueue =
            new LinkedBlockingQueue<Runnable>(capacity);

        executor =
            new ThreadPoolExecutor(D_MIN_POOL_SIZE, D_MAX_POOL_SIZE,
                D_KEEPALIVE, TimeUnit.MILLISECONDS, threadQueue);
        executor.prestartAllCoreThreads();

        // Open the server socket
        serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        serverChannel.socket().bind(localAddress, backlog);

        // Set up the selector
        selector = Selector.open();
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);
    }

    /** Get the minimum thread pool size. */
    public int getMinPoolSize()
    {
        return executor.getCorePoolSize();
    }

    /**
     * Set the minimum thread pool size.
     * 
     * @param minPoolSize The new minimum thread pool size.
     */
    public void setMinPoolSize(int minPoolSize)
    {
        executor.setCorePoolSize(minPoolSize);
    }

    /** Get the maximum thread pool size. */
    public int getMaxPoolSize()
    {
        return executor.getMaximumPoolSize();
    }

    /**
     * Set the maximum thread pool size.
     * 
     * @param maxPoolSize The new maximum thread pool size.
     */
    public void setMaxPoolSize(int maxPoolSize)
    {
        executor.setMaximumPoolSize(maxPoolSize);
    }

    /** Get the thread keepalive time in milliseconds. */
    public long getThreadKeepalive()
    {
        return executor.getKeepAliveTime(TimeUnit.MILLISECONDS);
    }

    /**
     * Set the thread keepalive time.
     * 
     * @param ms The new keepalive time in milliseconds.
     */
    public void setThreadKeepalive(long ms)
    {
        executor.setKeepAliveTime(ms, TimeUnit.MILLISECONDS);
    }

    /**
     * Add a file to be shared, backed by a real file on the filesystem.
     * 
     * @param name The name that will retrieve the file in requests. This does
     *            not need to be the real name of the file.
     * @param data The actual file on the disk to share.
     */
    public void addFile(String name, File data)
    {
        if (!data.isFile()) {
            throw new IllegalArgumentException("File '" +
                                               data.getAbsolutePath() +
                                               "' does not exist or is not a file");
        }

        addFile(name, new FileDataSource(name, data));
    }

    /**
     * Add a file to be shared, backed by an array of bytes.
     * 
     * @param name The name that will retrieve the file in requests.
     * @param data The actual data to share.
     */
    public void addFile(String name, byte[] data)
    {
        addFile(name, new BufferDataSource(name, data));
    }

    /**
     * Add a file to be shared, backed by an arbitrary data source.
     * 
     * @param name The name that will retrieve the file in requests.
     * @param ds The data source that will provide the file content.
     */
    public void addFile(String name, DataSource ds)
    {
        files.put(name, ds);
        logger.info(String.format("Now sharing '%s' (%,d bytes)", name, ds
            .getSize()));
    }

    /**
     * Remove a file that is currently being shared. Does nothing if the file
     * was not being shared.
     * 
     * @param name The name of the file to stop sharing.
     */
    public void removeFile(String name)
    {
        files.remove(name);
        logger.info(String.format("No longer sharing '%s'", name));
    }

    /**
     * Stop sharing all files.
     */
    public void removeAll()
    {
        files.clear();
        logger.info("No longer sharing any files");
    }

    /**
     * Check whether a request for a given file will succeed.
     * 
     * @param name The name of the file to check.
     * @return True = the file is being shared, false = not shared.
     */
    public boolean fileExists(String name)
    {
        return (files.get(name) != null);
    }

    /**
     * Get the size of the file, in bytes.
     * 
     * @param name The name of the file.
     * @return The size of the file in bytes.
     * @throws NullPointerException If the file is not being shared.
     */
    public int getFileSize(String name)
    {
        return (files.get(name).getSize());
    }

    /**
     * Add an observer to the given file to be notified of all transfer events.
     * 
     * @param name The name of the file to observe.
     * @param observer The observer to receive transfer event notifications.
     * @throws NullPointerException If the file is not being shared.
     */
    public void addObserver(String name, TransferObserver observer)
    {
        files.get(name).addObserver(observer);
    }

    /**
     * Remove an observer from receiving transfer events.
     * 
     * @param name The name of the file to stop observing. If not found, nothing
     *            happens.
     * @param observer The observer to remove.
     */
    public void removeObserver(String name, TransferObserver observer)
    {
        DataSource file = files.get(name);
        if (file == null) {
            return;
        }

        file.removeObserver(observer);
    }

    /**
     * Public only as an implementation detail, DO NOT CALL THIS!
     */
    @Override
    public void run()
    {
        running = true;

        while (running) {
            // Listen for new connections/messages
            int keys;
            try {
                logger.debug("Waiting for a connection...");
                keys = selector.select(CYCLE_TIME);
            }
            catch (IOException ioe) {
                logger.error("Failure in select", ioe);
                continue;
            }

            logger.debug("Connections received on " + keys + " channels");

            // Shortcut to avoid creating a new iterator
            if (keys == 0) {
                continue;
            }

            // Pass off a new connection to the executor
            for (Iterator<SelectionKey> itKeys =
                selector.selectedKeys().iterator(); itKeys.hasNext();) {

                SelectionKey key = itKeys.next();

                // Remove the key from the set
                // Even if we are unable to spin off a new thread to handle
                // the connection, we don't want to leave it in the set for next
                // time
                itKeys.remove();

                HTTPConnection connection;
                try {
                    if (key.channel() == serverChannel) {
                        // Spin off a new handler
                        connection = new HTTPConnection(serverChannel.accept());
                    }
                    else {
                        logger.error("Unexpected source of selector exit: " +
                                     key.channel().getClass().getName());
                        continue;
                    }
                }
                catch (IOException ioe) {
                    logger.error("Unable to accept connection", ioe);
                    continue;
                }

                try {
                    executor.execute(connection);
                }
                catch (RejectedExecutionException ree) {
                    logger.warn("Unable to schedule to handle HTTP connection",
                        ree);
                }
            }
        }

        try {
            serverChannel.close();
        }
        catch (IOException ioe) {
            logger.warn("Unable to close the server channel", ioe);
        }
    }

    /**
     * Shut down the HTTP server.
     * 
     * @throws InterruptedOperationException If the thread is interrupted while
     *             waiting for the server to die.
     */
    public void close()
    {
        logger.info("Shutting down...");

        // Tell the server to stop
        running = false;

        // Wait for the server thread to die
        try {
            this.join();
        }
        catch (InterruptedException ie) {
            throw new InterruptedOperationException(ie);
        }

        logger.info("HTTP server shut down");
    }
}

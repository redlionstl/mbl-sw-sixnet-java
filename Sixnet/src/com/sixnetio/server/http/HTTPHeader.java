/*
 * HTTPHeader.java
 *
 * Represents an HTTP header field.
 *
 * Jonathan Pearson
 * October 7, 2009
 *
 */

package com.sixnetio.server.http;

/**
 * Represents an HTTP header field.
 *
 * @author Jonathan Pearson
 */
public class HTTPHeader
{
	/** The name of the field. */
	private final String name;
	
	/** The value of the field. */
	private final String value;
	
	/**
	 * Construct a new HTTP header field.
	 * 
	 * @param name The name of the field.
	 * @param value The value of the field.
	 */
	public HTTPHeader(String name, String value)
	{
		this.name = name;
		this.value = value;
	}
	
	/**
	 * Construct a new HTTP header field.
	 * 
	 * @param line A header field in proper format.
	 * @throws IllegalArgumentException If the field is not in proper format.
	 */
	public HTTPHeader(String line)
	{
		// Make sure there is a colon, that the line continues past it, and that
		// a space follows the first colon
		if (line.indexOf(':') == -1 ||
		    line.length() - 1 == line.indexOf(':') ||
		    line.charAt(line.indexOf(':') + 1) != ' ') {
			
			throw new IllegalArgumentException("Badly formatted HTTP header line");
		}
		
		// Actually parse the line
		this.name = line.substring(0, line.indexOf(':'));
		
		// Skip the space after the ':'
		this.value = line.substring(line.indexOf(':') + 2);
	}
	
	/** Get the name of the field. */
	public String getName()
	{
		return name;
	}
	
	/** Get the value of the field. */
	public String getValue()
	{
		return value;
	}
	
	@Override
	public int hashCode()
	{
		return name.hashCode();
	}
	
	/**
	 * Check whether this header field equals some other object.
	 * 
	 * @return <tt>true</tt> if:
	 *   <ul>
	 *     <li><tt>rhs</tt> is an HTTPHeader and has the same name and value as
	 *       this HTTPHeader.</li>
	 *     <li><tt>rhs</tt> is a String equal to the name of this
	 *       HTTPHeader.</tt>
	 *   </ul>
	 *   Otherwise, <tt>false</tt>.
	 */
	@Override
	public boolean equals(Object rhs)
	{
		if (rhs instanceof HTTPHeader) {
			return (name.equals(((HTTPHeader)rhs).getName()) &&
					value.equals(((HTTPHeader)rhs).getValue()));
		}
		else if (rhs instanceof String) {
			return name.equals(rhs);
		}
		else {
			return false;
		}
	}
	
	@Override
	public String toString()
	{
		return String.format("%s: %s", name, value);
	}
}

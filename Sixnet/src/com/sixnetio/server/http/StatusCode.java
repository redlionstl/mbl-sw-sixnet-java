/*
 * StatusCode.java
 * 
 * An enumeration of the HTTP 1.1 status codes.
 * 
 * Jonathan Pearson
 * October 6, 2009
 * 
 */

package com.sixnetio.server.http;

/**
 * The various response codes for HTTP 1.1. The descriptions were taken from
 * Wikipedia (http://en.wikipedia.org/wiki/List_of_HTTP_status_codes).
 *
 * @author Jonathan Pearson
 */
public enum StatusCode
{
	// Informational
	/**
	 * The server has received the headers and the client should proceed to
	 * send the body of the request.
	 */
	Continue(100, "Continue"),
	
	/**
	 * Requester has asked the server to switch protocols and the server has
	 * acknowledged that it will do so.
	 */
	SwitchingProtocols(101, "Switching Protocols"),
	
	// Success
	/**
	 * The request was successful and a normal response is coming.
	 */
	OK(200, "OK"),
	
	/**
	 * The request has been fulfilled and resulted in a new resource being
	 * created.
	 */
	Created(201, "Created"),
	
	/**
	 * The request has been accepted for processing, but the processing has
	 * not been completed.
	 */
	Accepted(202, "Accepted"),
	
	/**
	 * The server successfully processed the request, but is returning
	 * information that may be from another source (since HTTP 1.1).
	 */
	NonAuthoritativeInformation(203, "Non-authoritative Information"),
	
	/**
	 * The server successfully processed the request, but is not returning
	 * any content.
	 */
	NoContent(204, "No Content"),
	
	/**
	 * The server successfully processed the request, but is not returning
	 * any content. Unlike 204, this requires that the requester reset the
	 * document view.
	 */
	ResetContent(205, "Reset Content"),
	
	/**
	 * The server is delivering only part of the resource due to a range
	 * header sent by the client. This is used by tools like wget to enable
	 * resuming of interrupted downloads, or split a download into multiple
	 * simultaneous streams.
	 */
	PartialContent(206, "Partial Content"),
	
	// Redirection
	/**
	 * Indicates multiple options for the resource that the client may
	 * follow.
	 */
	MultipleChoices(300, "Multiple Choices"),
	
	/**
	 * This and all future requests should be directed to the given URI.
	 */
	MovedPermanently(301, "Moved Permanently"),
	
	/**
	 * Temporary redirect (HTTP 1.0), split into 303 and 307 for HTTP 1.1.
	 */
	Found(302, "Found"),
	
	/**
	 * The response to the request can be found under another URI using a
	 * GET method. When received in response to a PUT, it should be assumed
	 * that the server has received the data and the redirect should be
	 * issued with a separate GET message (since HTTP 1.1).
	 */
	SeeOther(303, "See Other"),
	
	/**
	 * The resource has not been modified since last requested. Typically
	 * used in conjunction with the "if-modified-since" header sent by the
	 * client.
	 */
	NotModified(304, "Not Modified"),
	
	/**
	 * Not handled properly by Mozilla and IE, shouldn't be used (since
	 * HTTP 1.1).
	 */
	UseProxy(305, "Use Proxy"),
	
	/**
	 * The request should be repeated with another URI, but future requests
	 * can continue to use the original URI (since HTTP 1.1).
	 */
	TemporaryRedirect(307, "Temporary Redirect"),
	
	// Client error
	/**
	 * The request contains bad syntax or cannot be fulfilled.
	 */
	BadRequest(400, "Bad Request"),
	
	/**
	 * Similar to 403, but specifically for use when authentication is
	 * possible but has failed or has not yet been provided.
	 */
	Unauthorized(401, "Unauthorized"),
	
	/**
	 * The original intention was to use this code as part of some form of
	 * digital cash or micropayment scheme, but that has not happened and
	 * this code has never been used.
	 */
	PaymentRequired(402, "Payment Required"),
	
	/**
	 * The request was legal, but the server is refusing to respond to it.
	 */
	Forbidden(403, "Forbidden"),
	
	/**
	 * The requested resource could not be found but may be available in the
	 * future.
	 */
	NotFound(404, "Not Found"),
	
	/**
	 * A request was made on a resource using a method not supported by that
	 * resource (GET, POST, or PUT).
	 */
	MethodNotAllowed(405, "Method Not Allowed"),
	
	/**
	 * The requested resource is only capable of generating content not
	 * acceptable according to the Accept headers sent in the request.
	 */
	NotAcceptable(406, "Not Acceptable"),
	
	/**
	 * Not used.
	 */
	ProxyAuthenticationRequired(407, "Proxy Authentication Required"),
	
	/**
	 * The server timed out waiting for the request.
	 */
	RequestTimeout(408, "Request Time-out"),
	
	/**
	 * The request could not be processed because of a conflict in the
	 * request (such as an edit conflict).
	 */
	Conflict(409, "Conflict"),
	
	/**
	 * The resource requested is no longer available an will not be
	 * available again.
	 */
	Gone(410, "Gone"),
	
	/**
	 * The request did not specify the length of its content, which is
	 * required by the requested resource.
	 */
	LengthRequired(411, "Length Required"),
	
	/**
	 * The server does not meet one of the preconditions that the requester
	 * put on the request.
	 */
	PreconditionFailed(412, "Precondition Failed"),
	
	/**
	 * The request is larger than the server is willing or able to process.
	 */
	RequestEntityTooLarge(413, "Request Entity Too Large"),
	
	/**
	 * The URI provided was too long for the server to process.
	 */
	RequestURITooLong(414, "Request-URI Too Large"),
	
	/**
	 * The request did not specify any media types that the server or
	 * resource supports.
	 */
	UnsupportedMediaType(415, "Unsupported Media Type"),
	
	/**
	 * The client has asked for a portion of the file, but the server cannot
	 * supply that portion (perhaps it is beyond the end of the file).
	 */
	RequestedRangeNotSatisfiable(416, "Requested range not satisfiable"),
	
	/**
	 * The server cannot meet the requirements of the Expect request header
	 * field.
	 */
	ExpectationFailed(417, "Expectation Failed"),
	
	// Server error
	/**
	 * A generic error message, given when no more specific message is
	 * suitable.
	 */
	InternalServerError(500, "Internal Server Error"),
	
	/**
	 * The server either does not recognize the request method, or it lacks
	 * the ability to fulfill the request.
	 */
	NotImplemented(501, "Not Implemented"),
	
	/**
	 * The server was acting as a gateway or proxy and received an invalid
	 * response from the downstream server.
	 */
	BadGateway(502, "Bad Gateway"),
	
	/**
	 * The server is currently unavailable. Generally this is a temporary
	 * state.
	 */
	ServiceUnavailable(503, "Service Unavailable"),
	
	/**
	 * The server was acting as a gateway or proxy and did not receive a
	 * timely response from the downstream server.
	 */
	GatewayTimeout(504, "Gateway Time-out"),
	
	/**
	 * The server does not support the HTTP protocol version used in the
	 * request.
	 */
	HTTPVersionNotSupported(505, "HTTP Version not supported"),
	;
	
	private final int code;
	private final String description;
	
	private StatusCode(int code, String description)
	{
		this.code = code;
		this.description = description;
	}
	
	/**
	 * Get the response code number for this response.
	 */
	public int getCode()
	{
		return code;
	}
	
	/**
	 * Get the portion of the header line in HTTP that represents this response
	 * code. Example: "404 Not Found".
	 */
	public String getResponseString()
	{
		return String.format("%d %s", code, description);
	}
	
	/**
	 * Get the descriptive response message for this response.
	 */
	@Override
	public String toString()
	{
		return description;
	}
}

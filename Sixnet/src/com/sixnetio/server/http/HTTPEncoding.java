/*
 * HTTPEncoding.java
 *
 * Represents an HTTP encoding, as specified by the Accept-Encoding header
 * field.
 *
 * Jonathan Pearson
 * October 7, 2009
 *
 */

package com.sixnetio.server.http;

import java.util.LinkedList;
import java.util.List;

/**
 * Represents a possible HTTP encoding scheme, from the Accept-Encoding field.
 * 
 * @author Jonathan Pearson
 */
public class HTTPEncoding
    implements Comparable<HTTPEncoding>
{
    /** The name of the encoding. */
    private final String name;

    /** The Q-Value of the encoding. */
    private final float qvalue;

    /**
     * Parse the value of the Accept-Encoding header field.
     * 
     * @param line The value of the field. If blank, the default legal encodings
     *            will be returned.
     * @return A list of HTTPEncoding objects representing the values in the
     *         field.
     */
    public static List<HTTPEncoding> parseEncodings(String line)
    {
        List<HTTPEncoding> encodings = new LinkedList<HTTPEncoding>();

        StringBuilder encodingName = new StringBuilder();
        StringBuilder encodingQValue = new StringBuilder();

        boolean onQValue = false;

        char[] chars = line.toCharArray();
        for (char ch : chars) {
            if (ch == ';') {
                // Beginning of a qvalue
                onQValue = true;
            }
            else if (ch == ',') {
                // Signals the next encoding type
                // No Q-value means use a value of 1
                if (encodingQValue.length() == 0) {
                    encodingQValue.append("1.0");
                }

                try {
                    encodings.add(new HTTPEncoding(encodingName.toString(),
                        Float.parseFloat(encodingQValue.toString())));
                }
                catch (NumberFormatException nfe) {
                    // Ignore this one
                }

                encodingName.setLength(0);
                encodingQValue.setLength(0);

                onQValue = false;
            }
            else if (ch == ' ') {
                // Ignore it, usually comes after a ',' or ';' and means nothing
                // Technically it could come in the middle of a name or qvalue,
                // which would create an illegal string, but the result would be
                // throwing an exception instead of parsing what the client
                // *probably* meant
            }
            else {
                if (onQValue) {
                    encodingQValue.append(ch);
                }
                else {
                    encodingName.append(ch);
                }
            }
        }

        // If no "*" or "identity" is specified, "identity" is available
        if (!(encodings.contains("*") || encodings.contains("identity"))) {
            encodings.add(new HTTPEncoding("identity", 1.0f));
        }

        return encodings;
    }

    /**
     * Construct a new HTTP encoding.
     * 
     * @param name The name of the encoding.
     * @param qvalue The Q-Value of the encoding.
     */
    public HTTPEncoding(String name, float qvalue)
    {
        this.name = name;
        this.qvalue = qvalue;
    }

    /** Get the name of this encoding. */
    public String getName()
    {
        return name;
    }

    /** Get the Q-Value of this encoding. */
    public double getQValue()
    {
        return qvalue;
    }

    /**
     * Get the string representation of this encoding, as it would appear in the
     * Accept-Encoding header field. If there are multiple encodings going into
     * the field, they should be separated by commas.
     */
    @Override
    public String toString()
    {
        return String.format("%s;q=%.1f", name, qvalue);
    }

    /**
     * Check whether this encoding equals some other object.
     * 
     * @return <tt>true</tt> if:
     *         <ul>
     *         <li><tt>rhs</tt> is an HTTPEncoding and has the same name and
     *         qvalue as this HTTPEncoding.</li>
     *         <li><tt>rhs</tt> is a String equal to the name of this
     *         HTTPEncoding.</tt>
     *         </ul>
     *         Otherwise, <tt>false</tt>.
     */
    @Override
    public boolean equals(Object o)
    {
        if (o instanceof HTTPEncoding) {
            return ((((HTTPEncoding)o).getName().equals(name)) && (((HTTPEncoding)o)
                .getQValue() == qvalue));
        }
        else if (o instanceof String) {
            return name.equals(o);
        }
        else {
            return false;
        }
    }

    @Override
    public int compareTo(HTTPEncoding o)
    {
        if (qvalue < o.getQValue()) {
            return -1;
        }
        else if (qvalue > o.getQValue()) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return name.hashCode();
    }
}

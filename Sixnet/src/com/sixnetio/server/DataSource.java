/*
 * DataSource.java
 *
 * Represents a source of data for a server.
 *
 * Jonathan Pearson
 * October 6, 2009
 *
 */

package com.sixnetio.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.*;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Represents a data source for servers, abstracting out the actual source of
 * the data.
 *
 * @author Jonathan Pearson
 */
public abstract class DataSource
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private final String path;
	private final List<TransferObserver> observers = new LinkedList<TransferObserver>();
	
	/**
	 * Construct a new data source.
	 * 
	 * @param path The virtual path (server file name) to share the data at.
	 */
	protected DataSource(String path)
	{
		this.path = path;
	}
	
	/**
	 * Get an input stream that will read the data of this data source.
	 * 
	 * @return An input stream over the bytes of this data source.
	 * @throws IOException If there is a problem constructing the input stream.
	 */
	public abstract InputStream getInputStream()
		throws IOException;
	
	/**
	 * Get the size of the data from this data source.
	 * 
	 * @return The size of the data of this data source.
	 */
	public abstract int getSize();
	
	/**
	 * Add a transfer observer that monitors for transmission of this data
	 * source.
	 * 
	 * @param observer The observer to add. If it is already present, a new one
	 *   will <b>not</b> be added.
	 */
	public void addObserver(TransferObserver observer)
	{
		synchronized (observers) {
			if ( ! observers.contains(observer)) {
				observers.add(observer);
			}
		}
	}
	
	/**
	 * Remove a transfer observer from this data source.
	 * 
	 * @param observer The observer to remove. If the observer is not already
	 *   monitoring this data source, nothing will happen.
	 */
	public void removeObserver(TransferObserver observer) {
		synchronized (observers) {
			observers.remove(observer);
		}
	}
	
	/**
	 * Servers should call this each time a client begins downloading this data
	 * source. Non-servers should not call this function.
	 * 
	 * @param client The address of the client that has begin downloading this
	 *   file.
	 */
	public synchronized void fireTransferStarted(InetAddress client)
	{
		List<TransferObserver> observers;
		synchronized (this.observers) {
			observers = new ArrayList<TransferObserver>(this.observers);
		}
		
		for (Iterator<TransferObserver> itObservers = observers.iterator();
		     itObservers.hasNext(); ) {
			
			TransferObserver observer = itObservers.next();
			
			try {
				observer.transferStarted(path, client);
			}
			catch (Throwable th) {
				itObservers.remove();
				
				logger.error("Observer threw an exception, removing", th);
			}
		}
	}
	
	/**
	 * Servers should call this each time a block has been transferred to the
	 * client. They may call this multiple times if the same block is sent to
	 * the client repetitively.
	 * 
	 * @param client The address of the client that is downloading this file.
	 * @param progress The index of the block that has just been transferred.
	 * @param totalBlocks The total number of blocks (blocks are server-defined)
	 *   in this file.
	 */
	public synchronized void fireTransferProgress(InetAddress client,
	                                              int progress, int totalBlocks)
	{
		List<TransferObserver> observers;
		synchronized (this.observers) {
			observers = new ArrayList<TransferObserver>(this.observers);
		}
		
		for (Iterator<TransferObserver> itObservers = observers.iterator();
		     itObservers.hasNext(); ) {
			
			TransferObserver observer = itObservers.next();
			
			try {
				observer.transferProgress(path, client, progress, totalBlocks);
			}
			catch (Throwable th) {
				itObservers.remove();
				
				logger.error("Observer threw an exception, removing", th);
			}
		}
	}
	
	/**
	 * Servers should call this when a transfer completes.
	 * 
	 * @param client The address of the client with whom the transfer has
	 *   completed.
	 * @param success Indicates whether the transfer was a success
	 *   (<tt>true</tt>) or a failure (<tt>false</tt>).
	 */
	public synchronized void fireTransferEnded(InetAddress client,
	                                           boolean success)
	{
		List<TransferObserver> observers;
		synchronized (this.observers) {
			observers = new ArrayList<TransferObserver>(this.observers);
		}
		
		for (Iterator<TransferObserver> itObservers = observers.iterator();
		     itObservers.hasNext(); ) {
			
			TransferObserver observer = itObservers.next();
			
			boolean remove = false;
			
			try {
				remove = !observer.transferEnded(path, client, success);
			}
			catch (Throwable th) {
				itObservers.remove();
				
				logger.error("Observer threw an exception, removing", th);
			}
			
			if (remove) {
				itObservers.remove();
			}
		}
	}
}

/*
 * FileDataSource.java
 *
 * A data source that reads from a file.
 *
 * Jonathan Pearson
 * October 7, 2009
 *
 */

package com.sixnetio.server;

import java.io.*;

public class FileDataSource
	extends DataSource
{
	private final File file;
	
	/**
	 * Construct a new file-backed data source. Files are limited to signed 32
	 * bit sizes when the {@link #getSize()} function is used.
	 * 
	 * @param path The virtual path (server file name) to share the data at.
	 * @param source The file that provides the data for this data source.
	 */
	public FileDataSource(String path, File source)
	{
		super(path);
		
		this.file = source;
	}
	
	@Override
	public InputStream getInputStream()
		throws IOException
	{
		return new FileInputStream(file);
	}
	
	@Override
	public int getSize()
	{
		return (int)file.length();
	}
}

/*
 * TransferObserver.java
 * 
 * Objects which want access to the progress of a file transfer should implement
 * this class and register for notifications.
 */

package com.sixnetio.server;

import java.net.InetAddress;

/**
 * Implement this interface and register yourself to observe transfer progress
 * on a file.
 *
 * @author Jonathan Pearson
 */
public interface TransferObserver
{
	/**
	 * Called when a new transfer of the named file starts.
	 * 
	 * @param path The path of the file that was requested.
	 * @param with The address of the client.
	 */
	public void transferStarted(String path, InetAddress with);
	
	/**
	 * Called each time a new block is sent.
	 * 
	 * @param path The path of the file that is being transferred.
	 * @param with The address of the client.
	 * @param block The block number that was just sent.
	 * @param total The total number of blocks making up the file. A block may
	 *   be a single byte, or it may be a number of bytes.
	 */
	public void transferProgress(String path, InetAddress with, int block, int total);
	
	/**
	 * Called when a transfer ends.
	 * 
	 * @param path The path of the file that is being transferred.
	 * @param with The address of the client.
	 * @param success True = the transfer ended successfully, false = it ended
	 * due to some error.
	 * @return True = continue observing this file, false = stop observing this file.
	 */
	public boolean transferEnded(String path, InetAddress with, boolean success);
}

/*
 * BufferDataSource.java
 *
 * A data source that reads from a memory buffer.
 *
 * Jonathan Pearson
 * October 7, 2009
 *
 */

package com.sixnetio.server;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class BufferDataSource
	extends DataSource
{
	private final byte[] buffer;
	
	/**
	 * Construct a new Buffer-backed data source.
	 * 
	 * @param path The virtual path (server file name) to share the data at.
	 * @param source The bytes that will be shared.
	 */
	public BufferDataSource(String path, byte[] source)
	{
		super(path);
		
		buffer = source;
	}
	
	@Override
	public InputStream getInputStream()
	{
		return new ByteArrayInputStream(buffer);
	}
	
	@Override
	public int getSize()
	{
		return buffer.length;
	}
}

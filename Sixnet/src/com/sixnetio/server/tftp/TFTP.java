/*
 * TFTP.java
 *
 * A TFTP server that can serve files from the filesystem or from memory.
 *
 * Jonathan Pearson
 * June 25, 2007
 *
 */

package com.sixnetio.server.tftp;

import java.io.*;
import java.net.*;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.sixnetio.server.*;
import com.sixnetio.util.*;

public class TFTP extends Thread {
	private static Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Bytes per block transferred. */
	public static final int BLOCK_SIZE = 512;
	
	/** The default port number to listen on.*/
	public static final int DEF_SERVER_PORT_NUM = 69;
	
	/** Timeout before retransmitting a packet (in milliseconds). */
	public static final int SOCKET_TIMEOUT = 2000; // 2 seconds between re-transmits
	
	private static final short OP_READ = 1,
	                           OP_WRITE = 2,
	                           OP_DATA = 3,
	                           OP_ACK = 4,
	                           OP_ERROR = 5;

	private static final short E_NOT_DEFINED = 0,
	                           E_FILE_NOT_FOUND = 1,
	                           E_ACCESS_VIOLATION = 2,
	                           E_DISK_FULL = 3,
	                           E_ILLEGAL_OPERATION = 4,
	                           E_UNKNOWN_TID = 5,
	                           E_FILE_EXISTS = 6,
	                           E_NO_SUCH_USER = 7;
	
	private static TFTP theServer;
	private static final Object l_theServer = new Object();
	
	/**
	 * Get the running TFTP server, constructing a new one if necessary.
	 * 
	 * @return The running TFTP server.
	 * @throws IOException If it was not running and needed to be started,
	 * but failed to start.
	 */
	public static TFTP getTFTP() throws IOException {
		return getTFTP(DEF_SERVER_PORT_NUM);
	}
	
	/**
	 * Get the running TFTP server, constructing a new one if necessary
	 * using the port number specified.
	 * 
	 * @param portNum The port number to listen on.
	 * @return The running TFTP server.
	 * @throws IOException If it was not running and needed to be started,
	 * but failed to start.
	 */
	public static TFTP getTFTP(int portNum) throws IOException {
		synchronized (l_theServer) {
    		if (theServer == null) {
    			try {
    				theServer = new TFTP(portNum);
    				theServer.start();
    			} catch (IOException ioe) {
    				throw new IOException("TFTP Server failed to start: " + ioe.getMessage(), ioe);
    			}
    		}
    		
    		return theServer;
		}
	}
	
	/**
	 * Check whether the TFTP server is running.
	 * 
	 * @return True = the TFTP server is running, false = not running.
	 */
	public static boolean isRunning() {
		return (theServer != null);
	}
	
	private class TFTPConnection extends Thread {
		private InetAddress host;
		private String hostAddress;
		private int port;
		private DataSource source;
		private InputStream input;
		private DatagramSocket sock;
		private String filename;
		private final int fileSizeInBlocks;
		
		private DatagramPacket lastPacketSent;
		
		public TFTPConnection(DatagramPacket pkt, int tid) throws IOException {
			super("TFTPConnection:" + pkt.getAddress().toString());
			setDaemon(true);
			
			this.host = pkt.getAddress();
			this.hostAddress = this.host.getHostAddress(); // Cache for speed
			this.port = pkt.getPort();
			this.sock = new DatagramSocket(tid);
			
			sock.setSoTimeout(SOCKET_TIMEOUT);
			
			// Type of transfer?
			short opcode = Conversion.bytesToShort(pkt.getData(), 0);
			
			if (opcode != OP_READ) {
				error(E_ILLEGAL_OPERATION, "TFTP operation not supported: " + opcode);
				
				fileSizeInBlocks = 0;
			} else {
				filename = Conversion.bytesToString(pkt.getData(), 2);
				logger.debug(String.format("TFTP request for '%s' from '%s'", filename, hostAddress));
				
				source = files.get(filename);
				
				if (source == null) {
					error(E_FILE_NOT_FOUND, String.format("TFTP File '%s' not found on server", filename));
					fileSizeInBlocks = 0;
				} else {
					fileSizeInBlocks = source.getSize() / TFTP.BLOCK_SIZE +
						(source.getSize() % TFTP.BLOCK_SIZE == 0 ? 0 : 1);
					
					source.fireTransferStarted(this.host);
					
					input = source.getInputStream();
					
					// Send our response, the first block of data
					sendBlock();
				}
			}
			
			start();
		}
		
		private void error(short errCode, String msg) throws IOException {
			if (port != -1) {
	    		byte[] buffer = new byte[2 + 2 + msg.length() + 1];
    			Conversion.shortToBytes(buffer, 0, (short)5);
    			Conversion.shortToBytes(buffer, 2, (short)(errCode & 0xffff));
    			Conversion.stringToBytes(buffer, 4, msg);
    			
				DatagramPacket pkt = new DatagramPacket(buffer, buffer.length, host, port);
    			
    			sock.send(pkt);
			}
			
			throw new IOException("Connection error: " + msg);
		}
		
		private short lastBlockIndex = 0;
		private boolean lastPacket = false;
		private void sendBlock() throws IOException {
			lastBlockIndex++;
			
			byte[] data = new byte[BLOCK_SIZE];
			int bytesToSend = 0;
			
			int readBytes = -1; // After the loop, -1 indicates EOF
			while (bytesToSend < data.length) {
				readBytes = input.read(data, bytesToSend, data.length - bytesToSend);
				if (readBytes == -1) break;
				
				bytesToSend += readBytes;
			}
			
			lastPacket = (readBytes == -1);
			
			// Put together a packet
			byte[] payload = new byte[2 + 2 + bytesToSend];
			Conversion.shortToBytes(payload, 0, OP_DATA);
			Conversion.shortToBytes(payload, 2, lastBlockIndex);
			System.arraycopy(data, 0, payload, 4, bytesToSend);
			
			lastPacketSent = new DatagramPacket(payload, payload.length, host, port);
			
			sock.send(lastPacketSent);
			
			source.fireTransferProgress(host, lastBlockIndex, fileSizeInBlocks);
		}
		
		@Override
        public void run() {
			boolean running = true;
			
			try {
				while (running) {
					// Listen for a packet, or wait for a timeout
					try {
						byte[] buffer = new byte[1024];
						DatagramPacket pkt = new DatagramPacket(buffer, buffer.length);
						
						sock.receive(pkt);
						
						short opcode = Conversion.bytesToShort(pkt.getData(), 0);
						
						if (opcode == OP_ACK) {
							short block = Conversion.bytesToShort(buffer, 2);
							
							if (block != lastBlockIndex) {
								// Doesn't match what we expect, re-send
								logger.debug(String.format("TFTP block " + lastBlockIndex + " of '%s' for '%s' dropped, resending", filename, hostAddress));
								sock.send(lastPacketSent);
							} else {
								if (lastPacket) {
									logger.info(String.format("TFTP done sending '" + filename + "' to '%s'", hostAddress));
									sock.close();
									source.fireTransferEnded(host, true);
									running = false;
								} else {
									sendBlock();
								}
							}
						} else if (opcode == OP_ERROR) {
							String msg = Conversion.bytesToString(pkt.getData(), 4);
							logger.error(String.format("TFTP transfer error sending '%s' to '%s': %s", filename, hostAddress, msg));
							source.fireTransferEnded(host, false);
							throw new IOException("Error during transfer: " + msg);
						} else {
							source.fireTransferEnded(host, false);
							error(E_ILLEGAL_OPERATION, "Unexpected opcode: " + opcode);
						}
					} catch (SocketTimeoutException ste) {
						logger.warn(String.format("TFTP timeout on ACK for block %d of '%s' to '%s', resending", lastBlockIndex, filename, hostAddress));
						sock.send(lastPacketSent);
					}
				}
			} catch (IOException ioe) {
				logger.error("TFTPConnection failed", ioe);
				source.fireTransferEnded(host, false);
			}
		}
	}
	
	private DatagramSocket sock;
	private Hashtable<String, DataSource> files;
	private boolean running = true;
	private Thread serverThread;
	
	/**
	 * Constructs a new TFTP server using the
	 * specified port number.
	 * 
	 * @param portNumber Port number to listen on.
	 * @throws IOException If it was not possible to start.
	 */
	private TFTP(int portNumber) throws IOException {
		super("TFTPServer");
		setDaemon(true);
		files = new Hashtable<String, DataSource>();
		sock = new DatagramSocket(portNumber);
		sock.setSoTimeout(SOCKET_TIMEOUT);
	}
	/**
	 * Construct a new TFTP server.
	 * 
	 * @throws IOException If it was not possible to start.
	 */
	private TFTP() throws IOException {
		this(DEF_SERVER_PORT_NUM);
	}
	
	/**
	 * Add a file to be shared, backed by a real file on the filesystem.
	 * 
	 * @param name The name that will retrieve the file in requests.
	 * This does not need to be the real name of the file.
	 * @param data The actual file on the disk to share.
	 */
	public void addFile(String name, File data) {
		if (!data.isFile()) throw new IllegalArgumentException("File '" + data.getAbsolutePath() + "' does not exist or is not a file");
		
		files.put(name, new FileDataSource(name, data));
		logger.info(String.format("TFTP: Now sharing '%s' (%,d bytes)", name, data.length()));
	}
	
	/**
	 * Add a file to be shared, backed by an array of bytes.
	 * 
	 * @param name The name that will retrieve the file in requests.
	 * @param data The actual data to share.
	 */
	public void addFile(String name, byte[] data) {
		files.put(name, new BufferDataSource(name, data));
		logger.info(String.format("TFTP: Now sharing '%s' (%,d bytes)", name, data.length));
	}
	
	/**
	 * Add a file to be shared, backed by an arbitrary data source.
	 * 
	 * @param name The name that will retrieve the file in requests.
	 * @param ds The data source that provides the file data.
	 */
	public void addFile(String name, DataSource ds)
	{
		files.put(name, ds);
		logger.info(String.format("TFTP: Now sharing '%s' (%,d bytes)", name, ds.getSize()));
	}
	
	/**
	 * Remove a file that is currently being shared. Does nothing if the file
	 * was not being shared.
	 * 
	 * @param name The name of the file to stop sharing.
	 */
	public void removeFile(String name) {
		files.remove(name);
		logger.info(String.format("TFTP: No longer sharing '%s'", name));
	}
	
	/**
	 * Stop sharing all files.
	 */
	public void removeAll() {
		files.clear();
		logger.info("TFTP: No longer sharing any files");
	}
	
	/**
	 * Check whether a request for a given file will succeed.
	 * 
	 * @param name The name of the file to check.
	 * @return True = the file is being shared, false = not shared.
	 */
	public boolean fileExists(String name) {
		return (files.get(name) != null);
	}
	
	/**
	 * Get the size of the file, in bytes.
	 * 
	 * @param name The name of the file.
	 * @return The size of the file in bytes.
	 * @throws NullPointerException If the file is not being shared.
	 */
	public int getFileSize(String name) {
		return (files.get(name).getSize());
	}
	
	/**
	 * Add an observer to the given file to be notified of all transfer events.
	 * 
	 * @param name The name of the file to observe.
	 * @param observer The observer to receive transfer event notifications.
	 * @throws NullPointerException If the file is not being shared.
	 */
	public void addObserver(String name, TransferObserver observer) {
		files.get(name).addObserver(observer);
	}
	
	/**
	 * Remove an observer from receiving transfer events.
	 * 
	 * @param name The name of the file to stop observing. If not found,
	 * nothing happens.
	 * @param observer The observer to remove.
	 */
	public void removeObserver(String name, TransferObserver observer) {
		DataSource file = files.get(name);
		if (file == null) return;
		
		file.removeObserver(observer);
	}
	
	/**
	 * Public only as an implementation detail, DO NOT CALL THIS!
	 */
	@Override
    public void run() {
		byte[] buffer = new byte[1024];
		DatagramPacket pkt = new DatagramPacket(buffer, buffer.length);
		
		while (running) {
			try {
				sock.receive(pkt);
			} catch (SocketTimeoutException se) {
				// Timeout, try again
				continue;
			} catch (IOException ioe) {
				// Less expected...
				logger.error(ioe);
				continue;
			}
			
			try {
				int tid = Utils.rand.nextInt(65535 - 1024) + 1024;
				
				new TFTPConnection(pkt, tid);
			} catch (IOException ioe) {
				logger.error("Error constructing new TFTP connection", ioe);
			}
		}
		
		sock.close();
		
		// This prevents a race condition in close() that could throw a null-pointer exception
		synchronized (l_theServer) {
			theServer = null;
			
			l_theServer.notifyAll();
		}
	}
	
	/**
	 * Shut down the TFTP server.
	 * @throws InterruptedOperationException If the thread is interrupted
	 *   while waiting for the server to die.
	 */
	public void close() {
		logger.info("Shutting down the TFTP server...");
		
		// Tell the server to stop
		running = false;
		
		// If we simply did a "while (theServer != null) theServer.join()", a race condition
		//   exists when theServer is not null on the while-loop test, but it is null on the
		//   call to join() (saw this once)
		// By synchronizing and notifying when the server is down, we avoid the race condition
		
		// Wait for the server thread to die
		synchronized (l_theServer) {
			// It will die just after setting 'theServer' to null, so that's a safe test
			while (theServer != null) {
				try {
    				l_theServer.wait();
    			} catch (InterruptedException ie) {
    				throw new InterruptedOperationException();
    			}
			}
		}
		
		logger.info("  TFTP server shut down");
	}
}

/*
 * UBootLoader.java
 *
 * Loads UBoot into a device from a given piece of binary data.
 *
 * Jonathan Pearson
 * June 26, 2009
 *
 */

package com.sixnetio.uboot;

import java.io.*;
import java.net.*;

import com.sixnetio.COM.ComPort;
import com.sixnetio.COM.UBootCommunicator;
import com.sixnetio.GUI.ProgressListener;
import com.sixnetio.net.IfcUtil;
import com.sixnetio.server.tftp.TFTP;
import com.sixnetio.util.Utils;

/**
 * Provides a simple interface for loading UBoot into a device.
 *
 * @author Jonathan Pearson
 */
public class UBootLoader {
	/**
	 * Provides a UBoot image upon request.
	 *
	 * @author Jonathan Pearson
	 */
	public static class UBootSource {
		private final InputStream dataStream;
		private boolean closed = false;
		
		/**
		 * Provide the UBoot image in the form of a byte array.
		 * 
		 * @param data The UBoot image bytes.
		 */
		public UBootSource(byte[] data) {
			this.dataStream = new ByteArrayInputStream(data);
		}
		
		/**
		 * Provide the UBoot image in the form of a URL.
		 * 
		 * @param url The URL pointing at the UBoot image.
		 * @throws IOException If there is a problem opening a stream to read
		 *   the data.
		 */
		public UBootSource(URL url) throws IOException {
			this.dataStream = url.openStream();
		}
		
		/**
		 * Provide the UBoot image in the form of a URI.
		 * 
		 * @param uri The URI pointing at the UBoot image.
		 * @throws IOException If there is a problem converting the URI to a
		 *   URL, or there is a problem opening a stream to read the data.
		 */
		public UBootSource(URI uri) throws IOException {
			this(uri.toURL());
		}
		
		/**
		 * Provide the UBoot image in the form of a file.
		 * 
		 * @param file The file containing the UBoot data.
		 * @throws IOException If there is a problem opening the file for
		 *   reading.
		 */
		public UBootSource(File file) throws IOException {
			this.dataStream = new FileInputStream(file);
		}
		
		/**
		 * Get the input stream used to read this data source.
		 */
		public InputStream getInputStream() {
			return dataStream;
		}
		
		/**
		 * Close the data stream from this source.
		 * 
		 * @throws IOException If it was thrown trying to close the stream.
		 */
		public void close() throws IOException {
			if (closed) {
				return;
			}
			
			dataStream.close();
			closed = true;
		}
		
		/**
		 * Finalize by closing the data source. Exceptions are ignored.
		 */
		@Override
        protected void finalize() {
			try {
	            close();
            } catch (IOException ioe) {
	            // Do nothing
            }
		}
	}
	
	/**
	 * As some of the loading procedure is interactive, an implementor of this
	 * class must be passed into the
	 * {@link UBootLoader#loadUBoot(UBootLoaderUI)} method to be notified when
	 * these actions must be taken.
	 *
	 * @author Jonathan Pearson
	 */
	public static interface UBootLoaderUI extends ProgressListener {
		/**
		 * Turn off power to the device. Block until power has been turned off.
		 */
		public void powerOff();
		
		/**
		 * Turn on power to the device. Return either immediately, or
		 * immediately after powering on the device.
		 */
		public void powerOn();
		
		/**
		 * Called when an exception occurs during the process.
		 * 
		 * @param e The exception that was caught.
		 */
		public void handleException(Exception e);
		
		/**
		 * Called after opening communications to the device, but before
		 * performing any updates. Allows the caller to verify that the device
		 * is compatible with the update being applied to it.
		 * 
		 * @param uc The open UBootCommunicator object with the device.
		 * @return True = the device passed whatever verification was necessary,
		 *   and the load should proceed. False = the device failed the
		 *   verification; the device will be reset, with no updates performed.
		 * @throws IOException So that if there is a communications problem, the
		 *   implementor does not need to worry about it. This will cause the
		 *   test to act as if the exception was thrown internally, notifying
		 *   the UI appropriately and aborting.
		 */
		public boolean verifyDevice(UBootCommunicator uc) throws IOException;
	}
	
	// Serial communication settings
	private String serialPort; // Default set by constructor
	private int baud = 9600;
	private int bits = 8;
	private int parity = ComPort.PARITY_NONE;
	private int stopBits = ComPort.STOPBITS_1;
	
	// Address of the local machine performing the load
	private String localAddress;
	
	// If non-null, the IP settings in the device will be updated to these
	private String devAddress;
	private String devSubnet;
	private String devGateway;
	
	// Where UBoot comes from/where it goes
	private UBootSource dataSource;
	private long ubootAddress = -1;
	private int ubootLength = -1;
	
	/**
	 * Construct a new UBootLoader. Sets up default settings for serial
	 * communications, but you should probably update at least the serial port
	 * path.
	 */
	public UBootLoader() {
		// Choose a sensible default for the serial port
		if (Utils.osIsWindows()) {
			serialPort = "COM1";
		} else {
			serialPort = "/dev/ttyS0";
		}
		
		// Choose a sensible default for the local address
		// Basically, whatever comes up first in the discovery map
		try {
	        this.localAddress = IfcUtil.discoverLocalAddresses(false).entrySet()
	        	.iterator().next().getKey();
        } catch (SocketException se) {
	        // Ignore it
        }
	}
	
	/**
	 * Set the data source for reading a UBoot image.
	 * 
	 * @param source The data source.
	 */
	public void setUBootSource(UBootSource source) {
		this.dataSource = source;
	}
	
	/**
	 * Get the serial port that will be used for communications.
	 */
	public String getSerialPort() {
    	return serialPort;
    }
	
	/**
	 * Set the serial port to use for communications.
	 */
	public void setSerialPort(String serialPort) {
    	this.serialPort = serialPort;
    }
	
	/**
	 * Set the memory range in the device where UBoot should be erased from and
	 * then written to.
	 * 
	 * @param offset The memory offset (beginning) of the range.
	 * @param length The length of the range.
	 */
	public void setUBootRange(long offset, int length) {
		this.ubootAddress = offset;
		this.ubootLength = length;
	}
	
	/** Get the address where UBoot will be written. */
	public long getUBootAddress() {
		return ubootAddress;
	}
	
	/** Get the length of the range where UBoot will be written. */
	public int getUBootLength() {
		return ubootLength;
	}
	
	/** Get the baud rate that will be used. */
	public int getBaud() {
    	return baud;
    }
	
	/** Get the number of data bits (byte size) that will be used. */
	public int getByteSize() {
    	return bits;
    }
	
	/** Get the parity setting that will be used. */
	public int getParity() {
    	return parity;
    }
	
	/** Get the number of stop bits that will be used. */
	public int getStopBits() {
    	return stopBits;
    }
	
	/**
	 * Set the communication parameters for the serial port.
	 * 
	 * @param baud The baud rate.
	 * @param bits The number of data bits.
	 * @param parity The parity setting.
	 * @param stopBits The number of stop bits.
	 */
	public void setCommunicationParameters(int baud, int bits, int parity,
			int stopBits) {
		
		this.baud = baud;
		this.bits = bits;
		this.parity = parity;
		this.stopBits = stopBits;
	}
	
	/**
	 * Get the local address that was set for this machine.
	 */
	public String getLocalAddress() {
    	return localAddress;
    }
	
	/**
	 * Set the local address that will be used by this machine.
	 * 
	 * @param localAddress An IPv4 address to be used by this machine.
	 */
	public void setLocalAddress(String localAddress) {
    	this.localAddress = localAddress;
    }
	
	/**
	 * Get the address that the device will use for communications.
	 * 
	 * @return The address that the device will use, or <tt>null</tt> to allow
	 *   the device to keep its current address.
	 */
	public String getDevAddress() {
    	return devAddress;
    }
	
	/**
	 * Get the subnet mask that the device will use for communications.
	 * 
	 * @return The subnet mask that the device will use, or <tt>null</tt> to
	 *   allow the device to keep its current subnet mask.
	 */
	public String getDevSubnet() {
    	return devSubnet;
    }
	
	/**
	 * Get the gateway address that the device will use for communications.
	 * 
	 * @return The gateway address that the device will use, or <tt>null</tt> to
	 *   allow the device to keep its current gateway address.
	 */
	public String getDevGateway() {
    	return devGateway;
    }
	
	/**
	 * Set the IP parameters that will be given to the device. If these are not
	 * provided, the device will use whatever its current settings are. If they
	 * are provided, the device's IP settings will be updated and will remain
	 * even after the load is complete.
	 * 
	 * @param address The IPv4 address for the device to use, or <tt>null</tt>
	 *   to allow the device to keep its current address.
	 * @param subnet The subnet mask for the device to use, or <tt>null</tt> to
	 *   allow the device to keep its current subnet mask.
	 * @param gateway The gateway address for the device to use, or
	 *   <tt>null</tt> to allow the device to keep its current gateway address.
	 */
	public void setDeviceParameters(String address, String subnet, String gateway) {
		this.devAddress = address;
		this.devSubnet = subnet;
		this.devGateway = gateway;
	}
	
	/**
	 * Load UBoot into the device.
	 * 
	 * @param ui The UBootLoaderUI to handle interactive pieces of the load.
	 */
	public void loadUBoot(UBootLoaderUI ui) {
		// Make sure all of the necessary pieces have been provided
		if (ui == null) {
			throw new NullPointerException("ui is null");
		}
		if (serialPort == null) {
			throw new IllegalStateException("No serial port provided");
		}
		if (localAddress == null) {
			throw new IllegalStateException("No local address provided");
		}
		if (dataSource == null) {
			throw new IllegalStateException("No data source provided");
		}
		if (ubootAddress == -1) {
			throw new IllegalStateException("No UBoot offset provided");
		}
		if (ubootLength == -1) {
			throw new IllegalStateException("No UBoot size provided");
		}
		
		// Read the entire UBoot image into memory so that there will be no
		//   IOException thrown reading it after UBoot has been erased
		byte[] data;
        try {
	        data = Utils.bytesToEOF(dataSource.getInputStream());
        } catch (IOException ioe) {
	        ui.handleException(ioe);
	        return;
        }
		
		// Start up a TFTP server to serve the UBoot image
        final String imageName = "ubootImage";
		try {
	        TFTP.getTFTP().addFile(imageName, data);
        } catch (IOException ioe) {
	        ui.handleException(ioe);
	        return;
        }
		
		// Open a UBoot communicator
		UBootCommunicator uc = new UBootCommunicator(serialPort, baud, bits,
		                                             parity, stopBits);
		
		// Catch UBoot
		ui.powerOff();
		uc.startCatch();
		ui.powerOn();
		
		try {
	        uc.waitForCatch(60000);
        } catch (Exception e) {
        	uc.stopCatch();
	        ui.handleException(e);
	        return;
        }
        
        // Verify that this is a compatible device
        try {
        	if (!ui.verifyDevice(uc)) {
        		uc.close();
        		closeTFTP();
        		return;
        	}
        } catch (IOException ioe) {
        	uc.close();
        	closeTFTP();
        	
        	ui.handleException(ioe);
        	return;
        }
        
        // Update environment variables
        try {
	        uc.setVariable("serverip", localAddress);
	        
	        if (devAddress != null) {
				uc.setVariable("ipaddr", devAddress);
			}
	        if (devSubnet != null) {
				uc.setVariable("netmask", devSubnet);
			}
	        if (devGateway != null) {
				uc.setVariable("gateway", devGateway);
			}
	        
	        uc.flushVariables();
        } catch (IOException ioe) {
        	uc.close();
        	closeTFTP();
        	
	        ui.handleException(ioe);
	        return;
        }
        
        // Perform the load
        try {
	    uc.loadFile(imageName, false, -1, ubootAddress, ubootLength, ui);
        } catch (IOException ioe) {
	        uc.close();
	        closeTFTP();
	        
	        ui.handleException(ioe);
	        return;
        }
        
        // Reset the device
        try {
	        uc.writeln("reset");
	        Utils.sleep(1000);
        } catch (IOException ioe) {
	        IOException wrap = new IOException("Unable to reset the device, but the update was successful", ioe);
	        wrap.fillInStackTrace();
	        
	        ui.handleException(wrap);
	        
	        // Allow the normal cleanup
        }
        
        uc.close();
        closeTFTP();
	}
	
	/**
	 * A helper method that closes the TFTP object, ignoring any thrown
	 * exceptions.
	 */
	private void closeTFTP() {
		try {
            TFTP.getTFTP().close();
        } catch (IOException ex) {
        	// Ignore it
        	// This shouldn't happen since the TFTP server was already
        	//   started
        }
	}
}

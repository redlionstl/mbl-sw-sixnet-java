/*
 * MTDPartsParser.java
 *
 * Parses an MTDParts string used by both U-Boot and the Linux kernel to set up
 * partitions in a flash device.
 *
 * Jonathan Pearson
 * January 22, 2010
 *
 */

package com.sixnetio.uboot;

import java.util.*;

/**
 * Parses an MTDParts string used by both U-Boot and the Linux kernel to set up
 * partitions in a flash device.
 *
 * @author Jonathan Pearson
 */
public class MTDPartsParser
{
	public static class Partition
	{
		/** Partition offset, in bytes. */
		public final long offset;
		
		/** Partition length, in bytes. -1 if it uses all remaining space. */
		public final long length;
		
		/** Partition name, or <tt>null</tt>. */
		public final String partName;
		
		/** Device name. */
		public final String devName;
		
		/**
		 * Create a new Partition.
		 * 
		 * @param offset The offset, in bytes.
		 * @param length The length, in bytes. Use -1 to use all remaining
		 *   space on the device.
		 * @param partName The partition name, or <tt>null</tt>.
		 * @param devName The name of the device that contains the partition.
		 */
		public Partition(long offset, long length, String partName, String devName)
		{
			this.offset = offset;
			this.length = length;
			this.partName = partName;
			this.devName = devName;
		}
	}
	
	/**
	 * Parse an mtdparts string into a list of partitions.
	 * 
	 * @param mtdparts The mtdparts string.
	 * @return The list of partitions.
	 */
	public static List<Partition> parseMTDParts(String mtdparts)
	{
		// The specification may start with "mtdparts=", cut that off
		if (mtdparts.startsWith("mtdparts=")) {
			mtdparts = mtdparts.substring("mtdparts=".length());
		}
		
		// Make sure it ends with a ';' so the last entry gets counted
		if ( ! mtdparts.endsWith(";")) {
			mtdparts += ";";
		}
		
		// Example: mem.0:2M@4M(boot);sxni9260.0:2M@512K(boot),-(root)
		List<Partition> parts = new LinkedList<Partition>();
		
		StringBuilder piece = new StringBuilder();
		String device = null;
		String partition = null;
		long offset = -1;
		long length = -1;
		long endOfPrevious = 0;
		
		int state = 0; // 0 = device name, 1 = length, 2 = start, 3 = part name
		
		for (char c : mtdparts.toCharArray()) {
			if (c == ':') {
				// End of device name, beginning of partition length
				device = piece.toString();
				piece.setLength(0);
				state = 1;
			}
			else if (c == '@') {
				// End of partition length, beginning of partition offset
				length = parseSize(piece.toString());
				piece.setLength(0);
				state = 2;
			}
			else if (c == '(') {
				// Beginning of partition name
				if (state == 1) {
					length = parseSize(piece.toString());
				}
				else if (state == 2) {
					offset = parseSize(piece.toString());
				}
				piece.setLength(0);
				state = 3;
			}
			else if (c == ')') {
				// End of partition name, but we'll ignore it here and take care
				// of it with ',' and ';'
			}
			else if (c == ',' || c == ';') {
				// Beginning of partition length or device name
				if (state == 1) {
					length = parseSize(piece.toString());
				}
				else if (state == 2) {
					offset = parseSize(piece.toString());
				}
				else if (state == 3) {
					partition = piece.toString();
				}
				
				piece.setLength(0);
				
				if (device == null) {
					throw new IllegalArgumentException("Partitions must be within a device");
				}
				
				if (length == -1 && c == ',') {
					throw new IllegalArgumentException("Cannot have a partition follow a partition with size '-'");
				}
				
				if (offset == -1) {
					offset = endOfPrevious;
				}
				
				parts.add(new Partition(offset, length, partition, device));
				endOfPrevious = offset + length;
				
				partition = null;
				offset = -1;
				length = -1;
				
				if (c == ';') {
					state = 0;
					device = null;
					endOfPrevious = 0;
				}
				else {
					state = 1;
				}
			}
			else if (c == '-' && state != 3) {
				// Device size is whatever's left
				length = -1;
				piece.setLength(0);
			}
			else {
				piece.append(c);
			}
		}
		
		return parts;
	}
	
	public static Map<String, Partition> makePartMap(List<Partition> parts,
	                                                 String devName)
	{
		Map<String, Partition> partMap = new HashMap<String, Partition>();
		for (Partition part : parts) {
			if (part.devName.equals(devName)) {
				partMap.put(part.partName, part);
			}
		}
		
		return partMap;
	}
	
	private static long parseSize(String s)
	{
		char unit = s.charAt(s.length() - 1);
		int unitSize = 1;
		
		if (unit == 'k' || unit == 'K') {
			unitSize = 1024;
		}
		else if (unit == 'm' || unit == 'M') {
			unitSize = 1024 * 1024;
		}
		else if (unit == 'g' || unit == 'G') {
			unitSize = 1024 * 1024 * 1024;
		}
		else if (unit == 't' || unit == 'T') {
			unitSize = 1024 * 1024 * 1024 * 1024;
		}
		else if (unit == 'p' || unit == 'P') {
			unitSize = 1024 * 1024 * 1024 * 1024 * 1024;
		}
		else {
			// Really don't expect to have a larger unit in any of our products
		}
		
		if (unitSize > 1) {
			// Cut off the unit specifier char
			s = s.substring(0, s.length() - 1);
		}
		
		return (Long.parseLong(s) * unitSize);
	}
}

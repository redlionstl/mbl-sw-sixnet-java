/*
 * HexEdit.java
 *
 * A text area for editing hexadecimal bytes.
 *
 * Jonathan Pearson
 * November 4, 2008
 *
 */

package com.sixnetio.GUI;

import com.sixnetio.util.*;

import java.util.*;

import javax.swing.JTextArea;
import javax.swing.text.*;
import javax.swing.undo.UndoableEdit;

public class HexEdit extends JTextArea {
	private static class HexPosition implements Position {
		public int offset;
		public Bias bias = Bias.Forward;
		
		public HexPosition(int offset) {
			this.offset = offset;
		}
		
		public int getOffset() {
			return offset;
		}
	}
	
	private static class HexContent implements AbstractDocument.Content {
		private static final int LINELEN = 8 + 2 + 16 * 3 - 1 + 2 + 16;
		
		private byte[] data;
		private List<HexPosition> positions = new LinkedList<HexPosition>();
		
		public HexContent(byte[] data) {
			this.data = new byte[data.length];
			System.arraycopy(data, 0, this.data, 0, data.length);
		}
		
		public Position createPosition(int offset) throws BadLocationException {
			HexPosition pos = new HexPosition(offset);
	        positions.add(pos);
	        
	        return pos;
        }
		
		public void getChars(int where, int len, Segment txt) throws BadLocationException {
			// Each line is LINELEN chars long, so figure out which line we're on
			int line = where / LINELEN;
			
			// Now multiply that by 16 to figure out what offset in the byte array it corresponds to
			int offset = line * 16;
			
			if (offset + len > data.length) throw new BadLocationException("Selection past end of data", where + len - 1);
			
			// Generate that line
			Segment lineSeg = new Segment();
			makeLine(offset, lineSeg);
			
			// Figure out the starting char in that line based on 'where'
			int startChar = where - line * LINELEN;
			
			// Update lineSeg to only have the chars from 'startChar' to the end of the line
			lineSeg.offset += startChar;
			lineSeg.count -= startChar;
			
			// If the end of the selection is before the end of the line, just copy directly into txt
			//   and update the count
			if (lineSeg.count >= len) {
				txt.offset = lineSeg.offset;
				txt.count = len;
				txt.array = lineSeg.array;
				
				return;
			}
			
			// Otherwise, we need to build a list of segments that covers the text range
			List<Segment> segments = new LinkedList<Segment>();
			segments.add(lineSeg);
			
			int totalLen = txt.count; // Number of chars so far
			
			while (totalLen < len) {
				line++;
				offset = line * 16;
				
				// Already tested for offset + len > data.length, no need to do it again
				lineSeg = makeLine(offset, new Segment());
				segments.add(lineSeg);
				
				totalLen += lineSeg.count;
				
				if (totalLen > len) {
					// Cut that last segment down to size
					lineSeg.count -= totalLen - len;
				}
			}
			
			// Set up the output segment
			txt.offset = 0;
			txt.count = len;
			txt.array = new char[len];
			
			// Reusing offset for a different purpose here
			offset = 0;
			for (Segment seg : segments) {
				System.arraycopy(seg.array, seg.offset, txt.array, offset, seg.count);
				offset += seg.count;
			}
        }
		
		public String getString(int where, int len) throws BadLocationException {
	        // Use getChars to build the data, then return it as a String
			Segment seg = new Segment();
			getChars(where, len, seg);
			
			return seg.toString();
        }
		
		public UndoableEdit insertString(int where, String txt) throws BadLocationException {
			// Convert to an offset
			int line = where / LINELEN;
			int offset = line * 16;
			
			// Get the line
			Segment lineSeg = makeLine(offset, new Segment());
			
			// Figure out exactly where in that line 'where' points to
			int startPos = where - line * LINELEN;
			
			// Test the location
			if (startPos < 10 || // No writing in the 'address' section or the following two spaces
				startPos  > 48) { // No writing in the ASCII rendition section
				throw new BadLocationException("Cannot write in that section", where);
			}
			
			// TODO: Convert the text to bytes (currently don't know what it looks like)
			Utils.debug("insertString(" + where + ", '" + txt + "')");
			
			// TODO: Update all positions after the insertion point
			
	        return null;
        }
		
		public int length() {
	        // TODO: length of the text version of the byte array
	        return 0;
        }
		
		public UndoableEdit remove(int arg0, int arg1) throws BadLocationException {
	        // TODO: delete the range, update all positions that occurred in/after it
	        return null;
        }
		
		// Returns lineOut
		public Segment makeLine(int offset, Segment lineOut) {
			offset = offset & 0xfffffff0;
			
			lineOut.offset = 0;
			
			// 8-digit address, 2 spaces, 16 bytes with 2 chars + 1 space per byte, but no
			//   space for the 16th byte, then two spaces and 16 ASCII-rendered chars
			lineOut.count = LINELEN;
			lineOut.array = new char[lineOut.count];
			
			StringBuilder buf = new StringBuilder();
			buf.append(String.format("%08x  ", offset));
			
			for (int i = offset; i < offset + 16 && i < data.length; i++) {
				buf.append(String.format("%02x ", data[i]));
			}
			
			int remaining = (16 - offset) * 3;
			buf.append(String.format("%" + remaining + "s", ""));
			
			for (int i = offset; i < offset + 16 && i < data.length; i++) {
				if (32 <= data[i] && data[i] <= 126) {
					buf.append((char)data[i]);
				} else {
					buf.append(".");
				}
			}
			
			buf.getChars(0, lineOut.count, lineOut.array, 0);
			
			return lineOut;
		}
	}
	
	// This describes a document made up of lines, each line containing elements for:
	//   - The offset (one element)
	//   - 16 bytes (one element each)
	//   - ASCII rendition of those bytes (one element)
	private static class HexDocument extends AbstractDocument {
		protected HexDocument(byte[] data) {
	        super(createContent(data));
        }
		
		private static Content createContent(byte[] data) {
			// TODO: Implement this
			return null;
		}

		public Element getDefaultRootElement() {
	        // TODO Auto-generated method stub
	        return null;
        }

        public Element getParagraphElement(int pos) {
	        // TODO Auto-generated method stub
	        return null;
        }
	}
	
	public HexEdit() {
		super();
	}
	
	public HexEdit(String text) {
		super(text);
	}
	
	public HexEdit(Document doc) {
		super(doc);
	}
	
	public HexEdit(int rows, int columns) {
		super(rows, columns);
	}
	
	public HexEdit(String text, int rows, int colunms) {
		super(text, rows, colunms);
	}
	
	public HexEdit(Document doc, String text, int rows, int columns) {
		super(doc, text, rows, columns);
	}
}

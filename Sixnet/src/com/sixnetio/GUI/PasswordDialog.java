/*
 * PasswordDialog.java
 *
 * A simple dialog box which asks for a username and password and returns the text entered,
 * or null if the user canceled. Your option whether the username or confirm password fields
 * are shown.
 *
 * Jonathan Pearson
 * April 9, 2007
 * June 26, 2007: Added user name field
 * September 6, 2007: Made both user name and confirm password fields optional
 * September 24, 2007: Made this more secure by using a char array for passwords
 *
 */

package com.sixnetio.GUI;

import java.awt.Dialog;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;

import javax.swing.*;

public class PasswordDialog extends JDialog implements ActionListener, PropertyChangeListener {
	public static class UserPass {
		public String userName; // If not asked for, this will be null
		public char[] passwd;
		
		public UserPass(String u, char[] p) {
			userName = u;
			passwd = new char[p.length];
			System.arraycopy(p, 0, passwd, 0, passwd.length);
		}
	}
	
	boolean canceled;
	private String user = null;
	private char[] passwd = null;

	private JTextField txtUser;
	private JPasswordField txtPasswd;
	private JPasswordField txtConfirm;
	private JOptionPane optionPane;
	
	private String okButton = "OK";
	private String cancelButton = "Cancel";
	
	public PasswordDialog(String msg, String title, boolean showUser, boolean showConfirm) {
		super((Dialog)null, title, true);
		
		int components = 3;
		
		// Create an array of the components to be displayed
		JLabel lblMsg = new JLabel(msg);
		
		FocusListener fl = new FocusAdapter() {
			@Override
            public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextField) {
					((JTextField)(e.getSource())).selectAll();
				}
			}
		};
		
		JLabel lblUser = new JLabel("User Name: ");
		txtUser = null;
		
		if (showUser) {
			txtUser = new JTextField(10);
			txtUser.addFocusListener(fl);
			components += 2;
		}
		
		JLabel lblPwd1 = new JLabel("Password: ");
		txtPasswd = new JPasswordField(10);
		txtPasswd.addFocusListener(fl);
		
		JLabel lblPwd2 = new JLabel("Confirm: ");
		txtConfirm = null;
		
		
		if (showConfirm) {
			txtConfirm = new JPasswordField(10);
			txtConfirm.addFocusListener(fl);
			components += 2;
		}
		
		Object[] array = new Object[components];
		int i = 0;
		
		array[i++] = lblMsg;
		
		if (showUser) {
			array[i++] = lblUser;
			array[i++] = txtUser;
		}
		
		array[i++] = lblPwd1;
		array[i++] = txtPasswd;
		
		if (showConfirm) {
			array[i++] = lblPwd2;
			array[i++] = txtConfirm;
		}
		
		Object[] options = {okButton, cancelButton};
		
		optionPane = new JOptionPane(array,
									 JOptionPane.QUESTION_MESSAGE,
									 JOptionPane.OK_CANCEL_OPTION,
									 null,
									 options,
									 options[0]);
		
		setContentPane(optionPane);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent we) {
				optionPane.setValue(Integer.valueOf(JOptionPane.CLOSED_OPTION));
			}
		});
		
		addComponentListener(new ComponentAdapter() {
			@Override
            public void componentShown(ComponentEvent ce) {
				if (txtUser != null) {
					txtUser.requestFocusInWindow();
				} else {
					txtPasswd.requestFocusInWindow();
				}
			}
		});
		
		if (showUser) txtUser.addActionListener(this);
		txtPasswd.addActionListener(this);
		if (showConfirm) txtConfirm.addActionListener(this);
		
		optionPane.addPropertyChangeListener(this);
		
		pack();
	}
	
	public UserPass showDialog() {
		setVisible(true);
		
		if (canceled) {
			return null;
		} else {
			return new UserPass(getUser(), getPassword());
		}
	}
	
	public String getUser() {
		return user;
	}
	
	public char[] getPassword() {
		char[] passwd = new char[this.passwd.length];
		System.arraycopy(this.passwd, 0, passwd, 0, passwd.length);
		return passwd;
	}
	
	public void actionPerformed(ActionEvent e) {
		optionPane.setValue(okButton);
	}
	
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();
		
		if (isVisible() &&
			(e.getSource() == optionPane) &&
			(JOptionPane.VALUE_PROPERTY.equals(prop) ||
			 JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
			
			Object value = optionPane.getValue();
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
				return;
			}
			
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);
			
			if (okButton.equals(value)) {
				canceled = false;
				
				if (txtUser != null) {
					user = txtUser.getText();
					
					if (user.length() == 0) {
						JOptionPane.showMessageDialog(PasswordDialog.this,
						                              "Please specify a user name",
						                              "Error",
						                              JOptionPane.ERROR_MESSAGE);
						
						txtUser.selectAll();
						txtUser.requestFocusInWindow();
						return;
					}
				} else {
					user = null;
				}
				
				if (txtConfirm != null) {
					char[] pwd1 = txtPasswd.getPassword();
					char[] pwd2 = txtConfirm.getPassword();
					
					if (pwd1.length == 0) {
						JOptionPane.showMessageDialog(PasswordDialog.this,
						                              "Please specify a password",
						                              "Error",
						                              JOptionPane.ERROR_MESSAGE);
						
						txtPasswd.selectAll();
						txtPasswd.requestFocusInWindow();
						return;
					}
					
					if (Arrays.equals(pwd1, pwd2)) {
						passwd = pwd1;
						clearAndHide();
					} else {
						JOptionPane.showMessageDialog(PasswordDialog.this,
													  "Password does not match confirm",
													  "Error",
													  JOptionPane.ERROR_MESSAGE);
						
						txtPasswd.selectAll();
						txtPasswd.requestFocusInWindow();
					}
				} else {
					passwd = txtPasswd.getPassword();
					clearAndHide();
				}
			} else {
				canceled = true;
				clearAndHide();
			}
		}
	}
	
	public void clearAndHide() {
		setVisible(false);
	}
}

/*
 * LoginDialog.java
 *
 * Ask the user for a login name, password, and server. Provides an interface to
 * optionally show an options dialog (ex: connection options).
 *
 * Jonathan Pearson
 * February 18, 2008
 *
 */

package com.sixnetio.GUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LoginDialog extends JDialog implements ActionListener {
	public static final int CANCEL_OPTION = 0,
	                        OKAY_OPTION = 1;
	
	public static interface OptionsDialog {
		public Object showDialog(Object currentOptions);
	}
	
	private JTextField txtUsername,
	                   txtPassword,
	                   txtServer;
	
	private JButton btnOkay,
	                btnOptions,
	                btnCancel;
	
	private int status;
	
	private String username;
	private String password;
	private String server;
	private OptionsDialog optionsDialog;
	private Object options;
	
	public LoginDialog(String title) {
		this(title, null);
	}
	
	public LoginDialog(String title, OptionsDialog optionsDialog) {
		super((Dialog)null, title, true);
		
		this.optionsDialog = optionsDialog;
		options = null;
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				btnCancel.doClick();
			}
		});
		
		setLayout(new BorderLayout());
		
		// Center: Name, Password, Server
		{
			JPanel center = new JPanel();
			center.setLayout(new GridLayout(0, 1));
			add(center, BorderLayout.CENTER);
			
			ActionListener al = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnOkay.doClick();
				}
			};
			
			FocusListener fl = new FocusAdapter() {
				public void focusGained(FocusEvent e) {
					JTextField field = (JTextField)e.getSource();
					field.selectAll();
				}
			};
			
			// Top: Name
			{
				JPanel p = new JPanel();
				p.setLayout(new FlowLayout(FlowLayout.LEFT));
				center.add(p);
				
				p.add(new JLabel("Name: "));
				
				txtUsername = new JTextField(10);
				txtUsername.addActionListener(al);
				txtUsername.addFocusListener(fl);
				p.add(txtUsername);
			}
			
			// Middle: Password
			{
				JPanel p = new JPanel();
				p.setLayout(new FlowLayout(FlowLayout.LEFT));
				center.add(p);
				
				p.add(new JLabel("Password: "));
				
				txtPassword = new JPasswordField(10);
				txtPassword.addActionListener(al);
				txtPassword.addFocusListener(fl);
				p.add(txtPassword);
			}
			
			// Bottom: Server
			{
				JPanel p = new JPanel();
				p.setLayout(new FlowLayout(FlowLayout.LEFT));
				center.add(p);
				
				p.add(new JLabel("Server: "));
				
				txtServer = new JTextField(10);
				txtServer.addActionListener(al);
				txtServer.addFocusListener(fl);
				p.add(txtServer);
			}
		}
		
		// Bottom: Buttons
		{
			JPanel p = new JPanel();
			p.setLayout(new FlowLayout());
			add(p, BorderLayout.SOUTH);
			
			btnOkay = new JButton("Okay");
			btnOkay.addActionListener(this);
			p.add(btnOkay);
			
			btnOptions = new JButton("Options");
			if (optionsDialog != null) {
				btnOptions.addActionListener(this);
				p.add(btnOptions);
			}
			
			btnCancel = new JButton("Cancel");
			btnCancel.addActionListener(this);
			p.add(btnCancel);
		}
		
		pack();
	}
	
	public int showDialog() {
		setVisible(true);
		
		return status;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String val) {
		txtUsername.setText(val);
		username = val;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String val) {
		txtPassword.setText(val);
		password = val;
	}
	
	public String getServer() {
		return server;
	}
	
	public void setServer(String val) {
		txtServer.setText(val);
		server = val;
	}
	
	public Object getOptions() {
		return options;
	}
	
	public void setOptions(Object val) {
		options = val;
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnOkay) {
			username = txtUsername.getText();
			password = txtPassword.getText();
			server = txtServer.getText();
			
			status = OKAY_OPTION;
			setVisible(false);
			dispose();
		} else if (e.getSource() == btnOptions) {
			options = optionsDialog.showDialog(options);
		} else if (e.getSource() == btnCancel) {
			status = CANCEL_OPTION;
			setVisible(false);
			dispose();
		} else {
			throw new IllegalArgumentException("Unexpected source of action event: " + e.getSource());
		}
	}
}

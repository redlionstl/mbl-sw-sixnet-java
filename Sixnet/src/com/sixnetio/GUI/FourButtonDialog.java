/*
 * FourButtonDialog.java
 *
 * Gives the user four choices, as opposed to the maximum of three from JOptionPane.
 *
 * Jonathan Pearson
 * April 29, 2008
 *
 */

package com.sixnetio.GUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FourButtonDialog extends JDialog implements ActionListener {
	public static final int OPTION1 = 1,
	                        OPTION2 = 2,
	                        OPTION3 = 3,
	                        CANCELED = 4;
	
	private JButton btn1,
	                btn2,
	                btn3,
	                btnCancel;
	
	private int status;
	
	public FourButtonDialog(String title, String caption, String cap1, String cap2, String cap3, String capCancel) {
		super((Dialog)null, title, true);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				btnCancel.doClick();
			}
		});
		
		setLayout(new BorderLayout(5, 5));
		
		// Top: Caption
		JLabel lblCap = new JLabel(caption);
		add(lblCap, BorderLayout.NORTH);
		
		// Bottom: Buttons
		{
			JPanel p = new JPanel();
			p.setLayout(new FlowLayout());
			add(p, BorderLayout.SOUTH);
			
			btn1 = new JButton(cap1);
			btn1.addActionListener(this);
			p.add(btn1);
			
			btn2 = new JButton(cap2);
			btn2.addActionListener(this);
			p.add(btn2);
			
			btn3 = new JButton(cap3);
			btn3.addActionListener(this);
			p.add(btn3);
			
			btnCancel = new JButton(capCancel);
			btnCancel.addActionListener(this);
			p.add(btnCancel);
		}
		
		pack();
	}
	
	public int showDialog() {
		setVisible(true);
		
		return status;
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn1) {
			status = OPTION1;
		} else if (e.getSource() == btn2) {
			status = OPTION2;
		} else if (e.getSource() == btn3) {
			status = OPTION3;
		} else if (e.getSource() == btnCancel) {
			status = CANCELED;
		} else {
			throw new IllegalArgumentException("Unexpected source of action event: " + e.getSource());
		}
		
		setVisible(false);
		dispose();
	}
}

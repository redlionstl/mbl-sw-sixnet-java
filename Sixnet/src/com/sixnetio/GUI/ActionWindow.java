/*
 * ActionWindow.java
 *
 * A window that displays a series of actions, based on a dependency graph and
 * an action that will be taken on each as it is satisfied.
 *
 * Jonathan Pearson
 * June 28, 2007
 *
 */

package com.sixnetio.GUI;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import com.sixnetio.util.DependencyGraph.*;

public class ActionWindow<E> extends JDialog implements ActionListener, DependencySatisfactionListener<E> {
	public class ActionItem extends JPanel implements Runnable, StatusUI {
		private E item;
		
		private JLabel lblName,
		               lblMessage,
		               lblStatus;
		
		private boolean started = false;
		
		public ActionItem(E item) {
			this.item = item;
			
			GridBagLayout gridbag = new GridBagLayout();
			GridBagConstraints gb = new GridBagConstraints();
			
			setLayout(gridbag);
			
			gb.ipady = 4;
			
			lblName = new JLabel(item.toString());
			gb.ipadx = 5;
			gridbag.setConstraints(lblName, gb);
			add(lblName);
			
			gb.gridwidth = GridBagConstraints.REMAINDER;
			lblStatus = new JLabel("");
			updateWaitingStatus();
			gridbag.setConstraints(lblStatus, gb);
			add(lblStatus);
			
			lblMessage = new JLabel("");
			gb.ipadx = 0;
			gridbag.setConstraints(lblMessage, gb);
			add(lblMessage);
			
			setPreferredSize(new Dimension(450, 34));
		}
		
		public void start() {
			if (!started) {
				started = true;
				
				Thread th = new Thread(this, "ActionItem:" + item.toString());
				th.start();
			}
		}
		
		public void run() {
			lblStatus.setText("Status: Running");
			//moveToTop(ActionItem.this);
			action.run(item, ActionItem.this);
			
			int runningActions;
			synchronized (running) {
	            running.remove(item);
	            deps.satisfy(item);
	            runningActions = running.size();
            }
			
			if (runningActions == 0) {
				btnOkay.setEnabled(true);
				btnCancel.setEnabled(false);
				
				makeModeless();
			} else {
				// Update relevant waiting status items
				Enumeration<E> keys = actionItems.keys();
				while (keys.hasMoreElements()) {
					E key = keys.nextElement();
					
					if (!deps.isSatisfied(key)) {
						actionItems.get(key).updateWaitingStatus();
					}
				}
			}
		}
		
		public void updateWaitingStatus() {
			if (!started && !canceled) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						lblStatus.setText("Waiting for " + deps.getFirstUnsatisfiedDependency(item));
					}
				});
			}
		}
		
		public void updateCanceledStatus() {
			if (!started) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						lblStatus.setText("Canceled");
						lblStatus.setForeground(Color.RED);
					}
				});
			} else {
				action.cancel(item, ActionItem.this);
			}
		}
		
		public void updateState(String msg) {
			final String fmsg = msg;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					lblStatus.setText("Status: " + fmsg);
					ActionWindow.this.setPreferredSize(ActionWindow.this.getSize());
					ActionWindow.this.pack();
					repaint();
				}
			});
		}
		
		public void updateMessage(String msg) {
			final String fmsg = msg;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					lblMessage.setText(fmsg);
					ActionWindow.this.setPreferredSize(ActionWindow.this.getSize());
					ActionWindow.this.pack();
					repaint();
				}
			});
		}
		
		public void updateColor(final Color color) {
			final Color fcolor = color;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					lblStatus.setForeground(fcolor);
					repaint();
				}
			});
		}
	}
	
	public static interface ActionTaker {
		public void run(Object arg, ActionWindow<?>.ActionItem ui);
		
		// Doesn't matter whether it succeeds, since we need to wait for the thread to complete either way
		// If it succeeds, that time will simply be shorter
		public void cancel(Object arg, ActionWindow<?>.ActionItem ui);
	}
	
	private DependencyGraph<E> deps;
	private ActionTaker action;
	
	private JPanel pnlMain;
	private JButton btnOkay,
	                btnCancel;
	
	private Hashtable<E, ActionItem> actionItems;
	private Set<E> running;
	
	private boolean canceled = false;
	
	public ActionWindow(DependencyGraph<E> deps, E noDisplay, String title, ActionTaker action) {
		super((Dialog)null, title, true);
		
		addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent e) {
				actionPerformed(new ActionEvent(btnCancel, 0, ""));
			}
		});
		
		this.deps = deps;
		this.action = action;
		
		setLayout(new BorderLayout());
		
		// Main panel
		{
			pnlMain = new JPanel();
			
			pnlMain.setLayout(new GridLayout(deps.size(), 1));
			
			actionItems = new Hashtable<E, ActionItem>();
			for (E item : deps.getAllItems()) {
				if (item != noDisplay) {
					ActionItem ai = new ActionItem(item);
					
					actionItems.put(item, ai);
					pnlMain.add(ai);
				}
			}
		}
		
		JScrollPane scrMain = new JScrollPane(pnlMain, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrMain, BorderLayout.CENTER);
		
		
		// Buttons
		{
			JPanel p = new JPanel();
			add(p, BorderLayout.SOUTH);
			
			p.setLayout(new FlowLayout());
			
			btnOkay = new JButton("OK");
			btnOkay.setEnabled(false);
			btnOkay.addActionListener(this);
			
			btnCancel = new JButton("Cancel");
			btnCancel.addActionListener(this);
			
			p.add(btnOkay);
			p.add(btnCancel);
		}
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		deps.addDependencySatisfactionListener(this);
		
		running = deps.getStartingItems();
		
		if (noDisplay != null) {
			deps.satisfy(noDisplay);
		}
		
		pack();
	}
	
	public void showDialog() {
		synchronized (running) {
	        Iterator<E> items = running.iterator();
	        while (items.hasNext()) {
		        E item = items.next();
		        
		        if (!deps.isSatisfied(item)) {
			        ActionItem ai = actionItems.get(item);
			        //moveToTop(ai);
			        ai.start();
		        } else {
			        items.remove();
		        }
	        }
        }
		
		setVisible(true);
	}
	
	private Object l_pnlMain = new Object();
	private void moveToTop(ActionItem ai) {
		synchronized(l_pnlMain) {
			pnlMain.remove(ai);
			pnlMain.add(ai, 0);
			pack();
			repaint();
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnOkay) {
			clearAndHide();
		} else if (e.getSource() == btnCancel) {
			if (canceled) {
				JOptionPane.showMessageDialog(this, "Please wait until all actions report that they have been canceled.", "Wait", JOptionPane.WARNING_MESSAGE);
				
				/*
				int result = JOptionPane.showConfirmDialog(this, "Running actions will continue unless you quit the program, however you will not be notified of the progress. Are you sure you want to close this window?", "Close", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
				
				if (result == JOptionPane.OK_OPTION) {
					clearAndHide();
				} else {
					return;
				}
				*/
			} else {
				canceled = true;
				btnCancel.setEnabled(false);
				
				Enumeration<E> keys = actionItems.keys();
				while (keys.hasMoreElements()) {
					E key = keys.nextElement();
					actionItems.get(key).updateCanceledStatus();
				}
				
				makeModeless();
			}
		}
	}
	
	public void makeModeless() {
		// Switch to modeless so the user can reference this window while doing other things
		setModalityType(Dialog.ModalityType.MODELESS);
		
		// We need to do this to cause the modality change to occur
		setVisible(false);
		setVisible(true);
		toFront(); // X has a tendency to put it in the background after that sequence
	}
	
	public void dependencySatisfied(DependencySatisfactionEvent<E> e) {
		if (e.getSource() == deps) {
			if (!canceled) {
				if (!deps.isSatisfied(e.getItem())) {
					synchronized (running) {
	                    running.add(e.getItem());
                    }
					
					ActionItem ai = actionItems.get(e.getItem());
					ai.start();
				}
			}
		} else {
			throw new IllegalArgumentException("Unexpected source of dependency satisfaction event");
		}
	}
	
	public void clearAndHide() {
		deps.removeDependencySatisfactionListener(this);
		setVisible(false);
	}
}

/*
 * Suicide.java
 *
 * Automatically kills the application after a few seconds, if it does not die
 * first. Useful for GUI and database programs, where you may lose track of an
 * automatically created thread.
 *
 * Jonathan Pearson
 * April 20, 2007
 *
 */

package com.sixnetio.GUI;

import com.sixnetio.util.Utils;

/**
 * Automatically kills the application with System.exit(0) after 3 seconds. This
 * is useful for GUI applications where you may not be certain that you have
 * shut down all threads, but you are certain that you are finished cleaning up.
 * If you have truly finished cleaning up, your program will halt before this
 * calls System.exit(), since it works by constructing a new <b>daemon</b>
 * thread that simply sleeps for 3 seconds.
 *
 * @author Jonathan Pearson
 */
public class Suicide implements Runnable {
	public static final int DELAY = 3000; // 3 seconds
	
	/**
	 * Construct a new Suicide, which will kill the application in approximately
	 * {@link #DELAY} milliseconds. This is approximate because there is no
	 * guarantee that the scheduler will immediately start executing the thread
	 * that this creates.
	 */
	public Suicide() {
		Thread th = new Thread(this);
		th.setDaemon(true);
		th.start();
	}
	
	public void run() {
		Utils.sleep(DELAY);
		Utils.debug("Kamakazi!");
		System.exit(0);
	}
}

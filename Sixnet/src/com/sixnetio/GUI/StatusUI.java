/*
 * StatusUI.java
 *
 * An interface that a status UI must implement to receive messages from a Switch.
 *
 * Jonathan Pearson
 * June 29, 2007
 *
 */

package com.sixnetio.GUI;

import java.awt.*;

public interface StatusUI {
	// Top-level message describing what is happening
	public void updateState(String msg);
	
	// Optional descriptive message going into more detail, always in black
	public void updateMessage(String msg);
	
	// Update the color of the message set by updateState(String)
	public void updateColor(Color color);
}

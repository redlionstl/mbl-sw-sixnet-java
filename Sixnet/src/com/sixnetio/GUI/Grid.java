/*
 * Grid.java
 *
 * A Swing component that represents a grid, similar to a spreadsheet view. The
 * user gets control over whether lines are displayed, what text goes into each
 * cell, and whether/which cells are editable.
 *
 * Jonathan Pearson
 * February 18, 2008
 *
 */

package com.sixnetio.GUI;

import com.sixnetio.util.*;

import java.awt.*;
import java.util.*;
import javax.swing.*;

public class Grid extends JPanel {
	// Column class
	private class Column extends Vector<Cell> {
		private int width, height; // in pixels
		
		public Column(Class<? extends Cell> cellType, int cellCount, int cellWidth, int cellHeight) throws InstantiationException, IllegalAccessException {
			super(cellCount);
			width = cellWidth;
			height = cellCount * cellHeight;
			
			for (int i = 0; i < cellCount; i++) {
				Cell cell = cellType.newInstance();
				cell.setWidth(cellWidth);
				cell.setHeight(cellHeight);
				
				add(cell);
			}
		}
		
		public Column(java.util.List<? extends Cell> cells) {
			super(cells);
			computeDimensions();
		}
		
		public int getWidth() {
			return width;
		}
		
		public void setWidth(int val) {
			width = val;
			
			for (Cell cell : this) {
				cell.setWidth(width);
			}
			
			for (Row row : rows) {
				row.computeDimensions();
			}
		}
		
		public int getHeight() {
			return height;
		}
		
		public void computeDimensions() {
			width = height = 0;
			
			for (Cell cell : this) {
				if (cell.getWidth() > width) width = cell.getWidth();
				height += cell.getHeight();
			}
		}
	}
	
	// Row class
	private class Row extends Vector<Cell> {
		public int width, height; // in pixels
		
		public Row(Class<? extends Cell> cellType, int cellCount, int cellWidth, int cellHeight) {
			super(cellCount);
			width = cellCount * cellWidth;
			height = cellHeight;
			
			for (int i = 0; i < cellCount; i++) {
				try {
					Cell cell = cellType.newInstance();
					cell.setWidth(cellWidth);
					cell.setHeight(cellHeight);
					
					add(cell);
				} catch (Exception e) {
					Utils.debug(e);
					throw new RuntimeException(e);
				}
			}
		}
		
		public Row(java.util.List<? extends Cell> cells) {
			super(cells);
			computeDimensions();
		}
		
		public int getWidth() {
			return width;
		}
		
		public int getHeight() {
			return height;
		}
		
		public void setHeight(int val) {
			height = val;
			
			for (Cell cell : this) {
				cell.setHeight(height);
			}
			
			for (Column col : columns) {
				col.computeDimensions();
			}
		}
		
		public void computeDimensions() {
			width = height = 0;
			
			for (Cell cell : this) {
				width += cell.getWidth();
				if (height < cell.getHeight()) height = cell.getHeight();
			}
		}
	}
	
	// Cell class
	public static abstract class Cell {
		// Private data members
		private int width, height; // in pixels
		
		// Width of each of the grid lines around this control
		private int topW, leftW, bottomW, rightW;
		
		// Background color
		private Color background;
		
		// Constructor
		/**
		 * Construct a new empty cell. All subclasses need to have an
		 * empty-argument-list constructor.
		 */
		public Cell() {
			width = height = 1;
			topW = leftW = bottomW = rightW = 1;
			background = Color.WHITE;
		}
		
		// getControl
		/**
		 * Get a component that displays the contents of this cell.
		 * @param lines Whether to display grid lines.
		 */
		public final JComponent getControl(boolean lines) {
			JComponent control = getSubControl();
			
			if (lines) {
				control.setBorder(BorderFactory.createMatteBorder(topW, leftW, bottomW, rightW, Color.BLACK));
			} else {
				control.setBorder(BorderFactory.createEmptyBorder());
			}
			
			control.setSize(new Dimension(getWidth(), getHeight()));
			control.setBackground(background);
			
			return control;
		}
		
		// getSubControl
		public abstract JComponent getSubControl();
		
		// Width
		public int getWidth() {
			return width;
		}
		
		// Make sure to tell the grid to redisplay after calling this
		public void setWidth(int val) {
			width = val;
		}
		
		// Background
		public Color getBackground() {
			return background;
		}
		
		public void setBackground(Color val) {
			background = val;
		}
		
		// Height
		public int getHeight() {
			return height;
		}
		
		// Make sure to tell the grid to redisplay after calling this
		public void setHeight(int val) {
			height = val;
		}
		
		// Borders
		public int getBorderTop() {
			return topW;
		}
		
		public int getBorderLeft() {
			return leftW;
		}
		
		public int getBorderBottom() {
			return bottomW;
		}
		
		public int getBorderRight() {
			return rightW;
		}
		
		// Make sure to tell the grid to redisplay after calling this
		public void setBorders(int top, int left, int bottom, int right) {
			topW = top;
			leftW = left;
			bottomW = bottom;
			rightW = right;
		}
	}
	
	// TextCell class
	public static class TextCell extends Cell {
		// Private data members
		private JTextField control;
		private boolean autosize;
		
		// Constructor
		/**
		 * Construct a new empty cell.
		 */
		public TextCell() {
			control = new JTextField("");
		}
		
		// getSubControl
		/**
		 * Get a component that displays the contents of this cell.
		 */
		public JComponent getSubControl() {
			control.setSize(new Dimension(getWidth(), getHeight()));
			return control;
		}
		
		// Accessors
		// Width
		public int getWidth() {
			if (autosize) {
				FontMetrics fm = control.getFontMetrics(control.getFont());
				
				char[] chars = control.getText().toCharArray();
				int width = fm.charsWidth(chars, 0, chars.length);
				
				// Leave some margins
				return (width + 8);
			} else {
				return super.getWidth();
			}
		}
		
		// Height
		public int getHeight() {
			if (autosize) {
				FontMetrics fm = control.getFontMetrics(control.getFont());
				
				int height = fm.getHeight();
				
				return (height + 4);
			} else {
				return super.getWidth();
			}
		}
		
		// Contents
		public String getText() {
			return control.getText();
		}
		
		// Make sure to redisplay() the grid some time after this
		public void setText(String val) {
			control.setText(val);
		}
		
		// Editable
		public boolean getEditable() {
			return control.isEditable();
		}
		
		public void setEditable(boolean val) {
			control.setEditable(val);
		}
		
		// Alignment
		public int getAlignment() {
			return control.getHorizontalAlignment();
		}
		
		public void setAlignment(int alignment) {
			control.setHorizontalAlignment(alignment);
		}
		
		// Autosize
		public boolean getAutosize() {
			return autosize;
		}
		
		// To see the result, make sure to redisplay() the grid after this if passing 'true'
		public void setAutosize(boolean val) {
			autosize = val;
		}
	}
	
	// Private data members
	private int width, height; // in pixels
	private Vector<Column> columns;
	private Vector<Row> rows;
	private boolean lines;
	private boolean editable;
	
	// Constructors
	public Grid() {
		this(1, 1);
	}
	
	public Grid(int initialWidth, int initialHeight) {
		this(initialWidth, initialHeight, 10, 10);
	}
	
	public Grid(int initialWidth, int initialHeight, int cellWidth, int cellHeight) {
		this(initialWidth, initialHeight, cellWidth, cellHeight, true);
	}
	
	public Grid(int initialWidth, int initialHeight, int cellWidth, int cellHeight, boolean lines) {
		this(initialWidth, initialHeight, cellWidth, cellHeight, lines, false);
	}
	
	public Grid(int initialWidth, int initialHeight, int cellWidth, int cellHeight, boolean lines, boolean editable) {
		try {
			init(TextCell.class, initialWidth, initialHeight, cellWidth, cellHeight, lines, editable);
		} catch (Exception e) {
			Utils.debug(e);
			throw new RuntimeException(e);
		}
	}
	
	public Grid(Class<? extends Cell> cellType, int initialWidth, int initialHeight, int cellWidth, int cellHeight, boolean lines, boolean editable) throws InstantiationException, IllegalAccessException {
		init(cellType, initialWidth, initialHeight, cellWidth, cellHeight, lines, editable);
	}
	
	private void init(Class<? extends Cell> cellType, int initialWidth, int initialHeight, int cellWidth, int cellHeight, boolean lines, boolean editable) throws InstantiationException, IllegalAccessException {
		columns = new Vector<Column>(initialWidth);
		rows = new Vector<Row>(initialHeight);
		
		for (int i = 0; i < initialWidth; i++) {
			columns.add(new Column(cellType, initialHeight, cellWidth, cellHeight));
		}
		
		for (int i = 0; i < initialHeight; i++) {
			java.util.List<Cell> thisRow = new Vector<Cell>(initialWidth);
			
			for (int j = 0; j < initialWidth; j++) {
				thisRow.add(columns.get(j).get(i));
			}
			
			rows.add(new Row(thisRow));
		}
		
		this.lines = lines;
		this.editable = editable;
		
		// Now place controls for all of that stuff
		// We don't want a real layout, we need to manually control everything
		setLayout(null);
		setBackground(Color.WHITE);
		
		redisplay();
	}
	
	// placeControls
	private void placeControls() {
		removeAll();
		setPreferredSize(new Dimension(width, height));
		
		int posX = 0;
		for (int i = 0; i < columns.size(); i++) {
			Column col = columns.get(i);
			
			int cellWidth = col.getWidth();
			
			int posY = 0;
			for (int j = 0; j < rows.size(); j++) {
				Row row = rows.get(j);
				Cell cell = col.get(j);
				
				int cellHeight = row.getHeight();
				
				JComponent control = cell.getControl(lines);
				control.setLocation(posX, posY);
				control.setSize(new Dimension(cellWidth, cellHeight));
				add(control);
				
				posY += rows.get(j).getHeight();
			}
			
			posX += col.getWidth();
		}
	}
	
	// redisplay
	public void redisplay() {
		width = height = 0;
		
		for (Column col : columns) {
			col.computeDimensions();
			
			width += col.getWidth();
		}
		
		for (Row row : rows) {
			row.computeDimensions();
			
			height += row.getHeight();
		}
		
		setPreferredSize(new Dimension(width, height));
		placeControls();
	}
	
	// insertRow
	public void insertRow(int where, String contents, int cellWidth, int cellHeight) {
		try {
			insertRow(TextCell.class, where, contents, cellWidth, cellHeight);
		} catch (Exception e) {
			Utils.debug(e);
			throw new RuntimeException(e);
		}
	}
	
	public void insertRow(Class<? extends Cell> cellType, int where, String contents, int cellWidth, int cellHeight) throws InstantiationException, IllegalAccessException {
		Row row = new Row(cellType, columns.size(), cellWidth, cellHeight);
		
		if (rows.size() <= where) rows.setSize(where + 1);
		
		for (int i = 0; i < row.size(); i++) {
			Column col = columns.get(i);
			
			if (col.size() <= where) col.setSize(where + 1);
			
			col.insertElementAt(row.get(i), where);
		}
		
		rows.insertElementAt(row, where);
		
		redisplay();
	}
	
	// insertCol
	public void insertCol(int where, String contents, int cellWidth, int cellHeight) {
		try {
			insertCol(TextCell.class, where, contents, cellWidth, cellHeight);
		} catch (Exception e) {
			Utils.debug(e);
			throw new RuntimeException(e);
		}
	}
	
	public void insertCol(Class<? extends Cell> cellType, int where, String contents, int cellWidth, int cellHeight) throws InstantiationException, IllegalAccessException {
		Column col = new Column(cellType, rows.size(), cellWidth, cellHeight);
		
		if (columns.size() <= where) columns.setSize(where + 1);
		
		for (int i = 0; i < col.size(); i++) {
			Row row = rows.get(i);
			
			if (row.size() <= where) row.setSize(where + 1);
			
			row.insertElementAt(col.get(i), where);
		}
		
		columns.insertElementAt(col, where);
		
		redisplay();
	}
	
	// Delete rows/cols
	public void deleteRow(int which) {
		rows.remove(which);
		
		redisplay();
	}
	
	public void deleteCol(int which) {
		columns.remove(which);
		
		redisplay();
	}
	
	// setCell
	public void setCell(int row, int col, Cell cell) {
		columns.get(col).set(row, cell);
	}
	
	// getCell
	public Cell getCell(int row, int col) {
		return columns.get(col).get(row);
	}
}

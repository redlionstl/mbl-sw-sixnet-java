/*
 * ProgressListener.java
 *
 * Is notified of progress updates.
 *
 * Jonathan Pearson
 * October 9, 2008
 *
 */

package com.sixnetio.GUI;

// TODO: Find a better place for this; it doesn't necessarily belong to GUIs alone

public interface ProgressListener {
	/**
	 * Notify the listener that progress has changed.
	 * 
	 * @param percent The percent complete. A number in the range [0.0, 1.0].
	 */
	public void updateProgress(float percent);
}

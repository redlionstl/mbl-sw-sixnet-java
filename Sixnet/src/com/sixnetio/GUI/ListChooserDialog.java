/*
 * ListChooserDialog.java
 *
 * Gives the user a list of options from which to choose.
 *
 * Jonathan Pearson
 * February 6, 2008
 *
 */

package com.sixnetio.GUI;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class ListChooserDialog extends JDialog implements ActionListener {
	public static final int CANCEL_OPTION = 0,
	                        OKAY_OPTION = 1;
	
	private JList lstChoices;
	private JButton btnOkay,
	                btnCancel;
	
	private Object choice;
	private int status;
	
	public ListChooserDialog(java.util.List<? extends Object> items, String title) {
		super((Dialog)null, title, true);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent e) {
				btnCancel.doClick();
			}
		});
		
		setLayout(new BorderLayout());
		
		// Center: List box of choices
		{
			Object[] choices;
			synchronized (items) {
				choices = items.toArray(new Object[items.size()]);
			}
			
			lstChoices = new JList(choices);
			if (choices.length > 0) lstChoices.setSelectedIndex(0);
			
			lstChoices.addMouseListener(new MouseAdapter() {
				@Override
                public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2) {
						btnOkay.doClick();
					}
				}
			});
			
			// A key listener to automatically react to some standard buttons
			lstChoices.addKeyListener(new KeyListener() {
	            public void keyPressed(KeyEvent e) {
	            	if (e.getKeyCode() == KeyEvent.VK_ENTER) {
	            		btnOkay.doClick();
	            	} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
	            		btnCancel.doClick();
	            	}
	            }
	            
	            public void keyReleased(KeyEvent e) { }
	            
	            public void keyTyped(KeyEvent e) { }
			});
			
			JScrollPane scroller = new JScrollPane(lstChoices);
			add(scroller, BorderLayout.CENTER);
		}
		
		// Left: Buttons
		{
			JPanel p = new JPanel();
			p.setLayout(new GridLayout(0, 1));
			add(p, BorderLayout.EAST);
			
			// Top: Okay
			{
				JPanel p2 = new JPanel();
				p2.setLayout(new FlowLayout());
				p.add(p2);
				
				btnOkay = new JButton("Okay");
				btnOkay.addActionListener(this);
				p2.add(btnOkay);
			}
			
			// Bottom: Cancel
			{
				JPanel p2 = new JPanel();
				p2.setLayout(new FlowLayout());
				p.add(p2);
				
				btnCancel = new JButton("Cancel");
				btnCancel.addActionListener(this);
				p2.add(btnCancel);
			}
		}
		
		pack();
	}
	
	public int showDialog() {
		setVisible(true);
		
		return status;
	}
	
	public Object getChoice() {
		return choice;
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnOkay) {
			choice = lstChoices.getSelectedValue();
			status = OKAY_OPTION;
			setVisible(false);
			dispose();
		} else if (e.getSource() == btnCancel) {
			status = CANCEL_OPTION;
			setVisible(false);
			dispose();
		} else {
			throw new IllegalArgumentException("Unexpected source of action event: " + e.getSource());
		}
	}
}

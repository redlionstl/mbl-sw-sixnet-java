/*
 * P9500.java
 *
 * This class controlls a P9500 printer by delegating all calls to a JNI library.
 *
 * Jonathan Pearson
 * January 24, 2008
 *
 */

package com.sixnetio.P9500;

import com.sixnetio.util.*;

public class P9500 {
	static {
		Utils.debug("Loading the P9500 library...");
		System.loadLibrary("P9500_P9500");
		Utils.debug("  Loaded");
	}
	
	public static boolean printLabel(String address) {
		return print3ByteMACLabel(address.toUpperCase());
	}
	
	public static boolean printLabel(String serial, String address) {
		Utils.debug("Printing label with " + serial + " and " + address.toUpperCase());
		
		return printSerNumMACLabel(serial, address.toUpperCase());
	}
	
	public static boolean printBarcode(String serial, String address) {
		return printBarcode128SN_MACLabel(serial, address.toUpperCase());
	}
	
	public static boolean printForcecomLabel(String partNumber, String address) {
		return printForceComLabel(partNumber.toUpperCase(), address.toUpperCase());
	}
	
	public static boolean printText(String text, boolean cut) {
		return printTextLabel(text, cut);
	}
	
	public static boolean printBarcode(String text, boolean cut) {
		return printBarcode39Label(text, cut);
	}
	
	private static native boolean print3ByteMACLabel(String address);
	private static native boolean printSerNumMACLabel(String serial, String address);
	private static native boolean printBarcode128SN_MACLabel(String serial, String address);
	private static native boolean printForceComLabel(String partNumber, String address);
	private static native boolean printTextLabel(String text, boolean cut);
	private static native boolean printBarcode39Label(String text, boolean cut);
}

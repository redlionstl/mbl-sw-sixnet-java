/*
 * MB_DiscreteRead.java
 *
 * A superclass for Modbus messages that read discrete registers.
 *
 * Jonathan Pearson
 * May 11, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * A superclass for Modbus messages that read discrete registers.
 *
 * @author Jonathan Pearson
 */
public abstract class MB_DiscreteRead
	extends MB_StandardRead
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Initialize the common back-end for discrete register reading. This
	 * verifies the payload size if it is a request or an error, but not if it
	 * is an acknowledgment (as the sizes in an acknowledgment are different
	 * depending on data type).
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param ack Whether this is an acknowledgment.
	 * @param sequence The sequence number of the message.
	 * @param protocol The protocol of the message.
	 * @param station The station number of the message.
	 * @param function The function code of the message.
	 * @param payload The remaining payload of the message.
	 * @throws IOException If there is a problem parsing the message.
	 */
	protected MB_DiscreteRead(byte realFC, boolean ack, short sequence,
	                          short protocol, byte station, byte function,
	                          byte[] payload)
		throws IOException
	{
		super(realFC, ack, sequence, protocol, station, function, payload);
		
		// Subclass handles non-ack payload check
	}
	
	/**
	 * Construct a message from its base pieces.
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param ack Whether this is an acknowledgment.
	 * @param err Whether this is an error (it should also be an acknowledgment
	 *   in this case).
	 * @param args A list of bytes, shorts, ints, longs, or arrays of those
	 *   types. These will be packed into the payload in the order they were
	 *   passed, in network byte order.
	 */
	protected MB_DiscreteRead(byte realFC, boolean ack, boolean err,
	                          Object... args)
	{
		super(realFC, ack, err, args);
	}
	
	/**
	 * Get the starting address (unsigned). Non-acknowledgments only.
	 */
	public short getStart()
	{
		return getShort(0, false, false, "Start");
	}
	
	/**
	 * Get the register count (unsigned). Non-acknowledgments only.
	 */
	public short getCount()
	{
		return getShort(2, false, false, "Count");
	}
	
	/**
	 * Get the number of returned bytes (unsigned). Acknowledgments only.
	 */
	public byte getByteCount()
	{
		return getByte(0, true, false, "Byte Count");
	}
	
	/**
	 * Get a copy of the returned bytes. Acknowledgments only.
	 */
	public byte[] getReturnedBytes()
	{
		return getByteArray(1, getByteCount() & 0xff, true, false, "Bytes");
	}
	
	/**
	 * Get the state of a specific register. Acknowledgments only.
	 * 
	 * @param idx The index within this message of the register to get.
	 */
	public boolean getRegister(int idx)
	{
		return getBit(8 + idx, true, false, "Register");
	}
	
	/**
	 * Get a copy of the returned registers. Acknowledgments only.
	 * 
	 * @param count The number of registers to get (as this parameter is not
	 *   available in acknowledgments).
	 */
	public boolean[] getReturnedRegisters(int count)
	{
		return getBitArray(8, count, true, false, "Register");
	}
	
	@Override
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		byte[] payload = getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (ack) {
				if (err) {
					if (i == 0) {
						ans.append(String.format(" Error Code = %s",
						                         getErrorString(getErrorCode())));
					}
				}
				else {
					if (i == 0) {
						ans.append(String.format(" Byte Count = %d",
						                         getByteCount() & 0xff));
					}
					else if (i == 1) {
						ans.append(String.format(" Returned registers"));
					}
				}
			}
			else {
				if (i == 0) {
					ans.append(String.format(" Start = %d",
					                         getStart() & 0xffff));
				}
				else if (i == 2) {
					ans.append(String.format(" Count = %d",
					                         getCount() & 0xffff));
				}
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(ModbusMessage ackMsg, String indent)
	{
		if (this.ack) {
			throw new IllegalStateException("Cannot ACK an ACK, nor describe such a construct");
		}
		else if ( ! (ackMsg instanceof MB_DiscreteRead)) {
			throw new IllegalArgumentException("ACK not an MB_DiscreteRead message");
		}
		
		MB_DiscreteRead ack = (MB_DiscreteRead)ackMsg;
		
		StringBuilder ans = new StringBuilder();
		byte[] payload = ack.getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (err) {
				if (i == 0) {
					ans.append(String.format(" Error Code = %s",
					                         ack.getErrorMessage()));
				}
			}
			else {
				if (i == 0) {
					ans.append(String.format(" Byte Count = %d",
					                         ack.getByteCount() & 0xff));
				}
				else if (i == 1) {
					ans.append(String.format(" Returned registers"));
				}
				else if (i == payload.length - 1) {
					// ACK doesn't know how many bits, but we do (since ACK is
					// in response to this message)
					int bits = getCount() & 7;
					if (bits > 0) {
						ans.append(String.format(" (Only first %d bits)",
						                         bits));
					}
				}
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
}

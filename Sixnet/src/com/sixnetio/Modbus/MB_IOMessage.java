/*
 * MB_IOMessage.java
 *
 * A layer above the standard read and standard write classes, for shared data
 * and code.
 *
 * Jonathan Pearson
 * May 7, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;

/**
 * Represents a generic I/O read/write Modbus message.
 *
 * @author Jonathan Pearson
 */
public abstract class MB_IOMessage
	extends ModbusMessage
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Indicates whether this is an ACK or an original message. */
	protected boolean ack;
	
	/** Indicates whether this is an error response. */
	protected boolean err;
	
	/** The basic function code of the message. */
	protected byte fc;
	
	/**
	 * Initialize the common back-end for I/O register access. This verifies
	 * the payload size if it is an error, but not in the other cases.
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param ack Whether this is an acknowledgment.
	 * @param sequence The sequence number of the message.
	 * @param protocol The protocol of the message.
	 * @param station The station number of the message.
	 * @param function The function code of the message.
	 * @param payload The remaining payload of the message.
	 * @throws IOException If there is a problem parsing the message.
	 */
	protected MB_IOMessage(byte realFC, boolean ack, short sequence,
	                       short protocol, byte station, byte function,
	                       byte[] payload)
		throws IOException
	{
		super(sequence, protocol, station, function, payload);
		
		this.ack = ack;
		this.fc = realFC;
		
		if ((function & ERR_MASK) == ERR_MASK) {
			function &= ~ERR_MASK;
			super.setFunction(function);
			err = true;
		}
		
		if (function != fc) {
			logger.error(String.format("Wrong function code for %s: 0x%02x",
			                           getClass().getName(), function));
			throw new IOException("Wrong function code");
		}
		
		// Verify the payload
		if (ack) {
			if (err) {
				if (payload.length != 1) {
					logger.error(String.format("Payload size is %d bytes, must be 1",
					                           payload.length));
					throw new IOException("Payload is not the right size");
				}
			}
			
			// Subclasses handle successful ACKs
		}
		else {
			if (payload.length != 4) {
				logger.error(String.format("Payload size is %d bytes, must be 4",
				                           payload.length));
				throw new IOException("Payload is not the right size");
			}
		}
	}
	
	/**
	 * Construct a message from its base pieces.
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param ack Whether this is an acknowledgment.
	 * @param err Whether this is an error (it should also be an acknowledgment
	 *   in this case).
	 * @param args A list of bytes, shorts, ints, longs, or arrays of those
	 *   types or booleans. These will be packed into the payload in the order
	 *   they were passed, in network byte order. An array of booleans will be
	 *   encoded as a range of bytes, zero-padded at the end if necessary.
	 */
	protected MB_IOMessage(byte realFC, boolean ack, boolean err,
	                       Object... args)
	{
		super((byte)(realFC | (err ? ERR_MASK : 0)));
		
		this.ack = ack;
		this.err = err;
		this.fc = realFC;
		
		// Compute payload size
		int size = 0;
		for (Object arg : args) {
			if (arg instanceof Byte) {
				size += 1;
			}
			else if (arg instanceof byte[]) {
				size += ((byte[])arg).length * 1;
			}
			else if (arg instanceof Short) {
				size += 2;
			}
			else if (arg instanceof short[]) {
				size += ((short[])arg).length * 2;
			}
			else if (arg instanceof Integer) {
				size += 4;
			}
			else if (arg instanceof int[]) {
				size += ((int[])arg).length * 4;
			}
			else if (arg instanceof Long) {
				size += 8;
			}
			else if (arg instanceof long[]) {
				size += ((long[])arg).length * 8;
			}
			else if (arg instanceof boolean[]) {
				// Round up to the nearest byte
				size += (((boolean[])arg).length + 7) >> 3;
			}
			else {
				throw new IllegalArgumentException("Not a recognized type: " +
				                                   arg.getClass().getSimpleName());
			}
		}
		
		byte[] payload = new byte[size];
		int offset = 0;
		for (Object arg : args) {
			if (arg instanceof Byte) {
				payload[offset] = ((Byte)arg).byteValue();
				offset += 1;
			}
			else if (arg instanceof byte[]) {
				byte[] array = (byte[])arg;
				for (int i = 0; i < array.length; i++) {
					payload[offset] = array[i];
					offset += 1;
				}
			}
			else if (arg instanceof Short) {
				Conversion.shortToBytes(payload, offset, ((Short)arg).shortValue());
				offset += 2;
			}
			else if (arg instanceof short[]) {
				short[] array = (short[])arg;
				for (int i = 0; i < array.length; i++) {
					Conversion.shortToBytes(payload, offset, array[i]);
					offset += 2;
				}
			}
			else if (arg instanceof Integer) {
				Conversion.intToBytes(payload, offset, ((Integer)arg).intValue());
				offset += 4;
			}
			else if (arg instanceof int[]) {
				int[] array = (int[])arg;
				for (int i = 0; i < array.length; i++) {
					Conversion.intToBytes(payload, offset, array[i]);
					offset += 4;
				}
			}
			else if (arg instanceof Long) {
				Conversion.longToBytes(payload, offset, ((Long)arg).longValue());
				offset += 8;
			}
			else if (arg instanceof long[]) {
				long[] array = (long[])arg;
				for (int i = 0; i < array.length; i++) {
					Conversion.longToBytes(payload, offset, array[i]);
					offset += 8;
				}
			}
			else if (arg instanceof boolean[]) {
				boolean[] array = (boolean[]) arg;
				int bit = 0;
				for (int i = 0; i < array.length; i++) {
					if (array[i]) {
						payload[offset] |= 1 << bit;
					}
					
					bit++;
					
					if ((bit & 7) == 0) {
						bit = 0;
						offset++;
					}
				}
			}
			else {
				throw new AssertionError("Recognized list of types different between two loops; second does not include " +
				                         arg.getClass().getSimpleName());
			}
		}
		
		assert(size == offset);
		
		super.setPayloadDirect(payload);
	}
	
	@Override
	public void setFunction(Byte fc)
	{
		if (fc != this.fc) {
			throw new IllegalArgumentException("Cannot change the function code of a specific message");
		}
	}
	
	@Override
	public void setPayload(byte[] payload)
	{
		throw new IllegalStateException("Cannot directly modify the payload of a specific message");
	}
	
	/**
	 * Verify that this message matches the specified criteria, throwing an
	 * exception if it does not.
	 * 
	 * @param ackOnly If non-<code>null</code>, verifies that the message's
	 *   acknowledgment status matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the message's
	 *   error status matches this value.
	 * @param fieldName If an error is detected, the name of the field to report
	 *   in the exception. Exception messages will look something like
	 *   "Acknowledgments do not have a '<code>fieldName</code>' field".
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	private void verifyMessageType(Boolean ackOnly, Boolean errOnly,
	                               String fieldName)
		throws IllegalStateException
	{
		if (ackOnly != null) {
			if (ackOnly && ! ack) {
				throw new IllegalStateException("Acknowledgments do not have a '" +
				                                fieldName + "' field");
			}
			else if ( ! ackOnly && ack) {
				throw new IllegalStateException("Non-acknowledgments do not have a '" +
				                                fieldName + "' field");
			}
		}
		
		if (errOnly != null) {
			if (errOnly && ! err) {
				throw new IllegalStateException("Errors do not have a '" +
				                                fieldName + "' field");
			}
			else if ( ! errOnly && err) {
				throw new IllegalStateException("Non-errors do not have a '" +
				                                fieldName + "' field");
			}
		}
	}
	
	/**
	 * Get a bit from the payload, verifying first that this specific type of
	 * message has that field.
	 * 
	 * @param offset The offset within the payload where this field resides.
	 *   This is the position of the bit from the very beginning of the payload.
	 * @param ackOnly If non-<code>null</code>, verifies that the acknowledgment
	 *   status of this message matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the error status
	 *   of this message matches this value.
	 * @param fieldName The name of the field, in case the criteria do not
	 *   match and an exception must be thrown.
	 * @return The value of the field.
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	protected boolean getBit(int offset, Boolean ackOnly, Boolean errOnly,
	                         String fieldName)
		throws IllegalStateException
	{
		verifyMessageType(ackOnly, errOnly, fieldName);
		
		int bite = offset >> 3;
		int bit = 1 << (offset & 7);
		
		return ((getPayloadDirect()[bite] & bit) != 0);
	}
	
	/**
	 * Get a bit array from the payload, verifying first that this specific
	 * type of message has that field.
	 * 
	 * @param offset The offset within the payload where this field resides.
	 *   This is the position of the bit from the very beginning of the payload
	 *   where we should start copying.
	 * @param len The number of elements to retrieve.
	 * @param ackOnly If non-<code>null</code>, verifies that the acknowledgment
	 *   status of this message matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the error status
	 *   of this message matches this value.
	 * @param fieldName The name of the field, in case the criteria do not
	 *   match and an exception must be thrown.
	 * @return The value of the field.
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	protected boolean[] getBitArray(int offset, int len, Boolean ackOnly,
	                                Boolean errOnly, String fieldName)
		throws IllegalStateException
	{
		verifyMessageType(ackOnly, errOnly, fieldName);
		
		boolean[] range = new boolean[len];
		byte[] bytes = getPayloadDirect();
		
		for (int i = 0; i < len; i++) {
			range[i] = (bytes[(offset + i) >> 3] & (1 << ((offset + i) & 7))) != 0;
		}
		
		return range;
	}
	
	/**
	 * Get a byte field from the payload, verifying first that this specific
	 * type of message has that field.
	 * 
	 * @param offset The offset within the payload where this field resides.
	 * @param ackOnly If non-<code>null</code>, verifies that the acknowledgment
	 *   status of this message matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the error status
	 *   of this message matches this value.
	 * @param fieldName The name of the field, in case the criteria do not
	 *   match and an exception must be thrown.
	 * @return The value of the field.
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	protected byte getByte(int offset, Boolean ackOnly, Boolean errOnly,
	                       String fieldName)
		throws IllegalStateException
	{
		verifyMessageType(ackOnly, errOnly, fieldName);
		return getPayloadDirect()[offset];
	}
	
	/**
	 * Get a byte array from the payload, verifying first that this specific
	 * type of message has that field.
	 * 
	 * @param offset The offset within the payload where this field resides.
	 * @param len The number of elements to retrieve.
	 * @param ackOnly If non-<code>null</code>, verifies that the acknowledgment
	 *   status of this message matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the error status
	 *   of this message matches this value.
	 * @param fieldName The name of the field, in case the criteria do not
	 *   match and an exception must be thrown.
	 * @return The value of the field.
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	protected byte[] getByteArray(int offset, int len, Boolean ackOnly,
	                            Boolean errOnly, String fieldName)
		throws IllegalStateException
	{
		verifyMessageType(ackOnly, errOnly, fieldName);
		
		byte[] range = new byte[len];
		System.arraycopy(getPayloadDirect(), offset, range, 0, len);
		return range;
	}
	
	/**
	 * Get a short field from the payload, verifying first that this specific
	 * type of message has that field.
	 * 
	 * @param offset The offset within the payload where this field resides.
	 * @param ackOnly If non-<code>null</code>, verifies that the acknowledgment
	 *   status of this message matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the error status
	 *   of this message matches this value.
	 * @param fieldName The name of the field, in case the criteria do not
	 *   match and an exception must be thrown.
	 * @return The value of the field.
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	protected short getShort(int offset, Boolean ackOnly, Boolean errOnly,
	                         String fieldName)
		throws IllegalStateException
	{
		verifyMessageType(ackOnly, errOnly, fieldName);
		return Conversion.bytesToShort(getPayloadDirect(), offset);
	}
	
	/**
	 * Get a short array from the payload, verifying first that this specific
	 * type of message has that field.
	 * 
	 * @param offset The offset within the payload where this field resides.
	 * @param len The number of elements to retrieve.
	 * @param ackOnly If non-<code>null</code>, verifies that the acknowledgment
	 *   status of this message matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the error status
	 *   of this message matches this value.
	 * @param fieldName The name of the field, in case the criteria do not
	 *   match and an exception must be thrown.
	 * @return The value of the field.
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	protected short[] getShortArray(int offset, int len, Boolean ackOnly,
	                                Boolean errOnly, String fieldName)
		throws IllegalStateException
	{
		verifyMessageType(ackOnly, errOnly, fieldName);
		
		short[] range = new short[len];
		for (int i = 0; i < len; i++) {
			range[i] = Conversion.bytesToShort(getPayloadDirect(),
			                                   offset + i * 2);
		}
		return range;
	}
	
	/**
	 * Get an integer field from the payload, verifying first that this specific
	 * type of message has that field.
	 * 
	 * @param offset The offset within the payload where this field resides.
	 * @param ackOnly If non-<code>null</code>, verifies that the acknowledgment
	 *   status of this message matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the error status
	 *   of this message matches this value.
	 * @param fieldName The name of the field, in case the criteria do not
	 *   match and an exception must be thrown.
	 * @return The value of the field.
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	protected int getInt(int offset, Boolean ackOnly, Boolean errOnly,
	                     String fieldName)
		throws IllegalStateException
	{
		verifyMessageType(ackOnly, errOnly, fieldName);
		return Conversion.bytesToInt(getPayloadDirect(), offset);
	}
	
	/**
	 * Get ant integer array from the payload, verifying first that this
	 * specific type of message has that field.
	 * 
	 * @param offset The offset within the payload where this field resides.
	 * @param len The number of elements to retrieve.
	 * @param ackOnly If non-<code>null</code>, verifies that the acknowledgment
	 *   status of this message matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the error status
	 *   of this message matches this value.
	 * @param fieldName The name of the field, in case the criteria do not
	 *   match and an exception must be thrown.
	 * @return The value of the field.
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	protected int[] getIntArray(int offset, int len, Boolean ackOnly,
	                            Boolean errOnly, String fieldName)
		throws IllegalStateException
	{
		verifyMessageType(ackOnly, errOnly, fieldName);
		
		int[] range = new int[len];
		for (int i = 0; i < len; i++) {
			range[i] = Conversion.bytesToInt(getPayloadDirect(),
			                                 offset + i * 4);
		}
		return range;
	}
	
	/**
	 * Get a long field from the payload, verifying first that this specific
	 * type of message has that field.
	 * 
	 * @param offset The offset within the payload where this field resides.
	 * @param ackOnly If non-<code>null</code>, verifies that the acknowledgment
	 *   status of this message matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the error status
	 *   of this message matches this value.
	 * @param fieldName The name of the field, in case the criteria do not
	 *   match and an exception must be thrown.
	 * @return The value of the field.
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	protected long getLong(int offset, Boolean ackOnly, Boolean errOnly,
	                       String fieldName)
		throws IllegalStateException
	{
		verifyMessageType(ackOnly, errOnly, fieldName);
		return Conversion.bytesToLong(getPayloadDirect(), offset);
	}
	
	/**
	 * Get a long array from the payload, verifying first that this specific
	 * type of message has that field.
	 * 
	 * @param offset The offset within the payload where this field resides.
	 * @param len The number of elements to retrieve.
	 * @param ackOnly If non-<code>null</code>, verifies that the acknowledgment
	 *   status of this message matches this value.
	 * @param errOnly If non-<code>null</code>, verifies that the error status
	 *   of this message matches this value.
	 * @param fieldName The name of the field, in case the criteria do not
	 *   match and an exception must be thrown.
	 * @return The value of the field.
	 * @throws IllegalStateException If any of the criteria do not match.
	 */
	protected long[] getLongArray(int offset, int len, Boolean ackOnly,
	                              Boolean errOnly, String fieldName)
		throws IllegalStateException
	{
		verifyMessageType(ackOnly, errOnly, fieldName);
		
		long[] range = new long[len];
		for (int i = 0; i < len; i++) {
			range[i] = Conversion.bytesToLong(getPayloadDirect(),
			                                  offset + i * 2);
		}
		return range;
	}
	
	/**
	 * Get the error code for an error response. Error acknowledgments only.
	 */
	public byte getErrorCode()
	{
		return getByte(0, true, true, "Error Code");
	}
	
	@Override
	public Boolean isAcknowledgment()
	{
		return ack;
	}
	
	@Override
	public Boolean isError()
	{
		return err;
	}
}

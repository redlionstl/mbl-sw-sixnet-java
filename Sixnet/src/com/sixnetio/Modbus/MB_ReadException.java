/*
 * MB_ReadException.java
 *
 * Description
 *
 * Jonathan Pearson
 * May 14, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Description
 *
 * @author Jonathan Pearson
 */
public class MB_ReadException
	extends MB_IOMessage
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Function code for 'Read Exception Status' (this message). */
	public static final byte FC = 0x07;
	
	/** Error meaning function code not supported. */
	public static final byte ERR_FUNCTION = 1;
	
	/** Error indicating the action of accessing the requested values failed. */
	public static final byte ERR_IO = 4;
	
	
	/** Get the name of the function that this class implements. */
	public static String getFunctionName()
	{
		return "Read Exception Status";
	}
	
	/**
	 * Construct a new MB_ReadException message.
	 * 
	 * @param ack Whether this is an acknowledgment.
	 * @param sequence The sequence number.
	 * @param protocol The protocol.
	 * @param station The station number.
	 * @param function The function code (must be {@link #FC}).
	 * @param payload The payload of the message.
	 * @throws IOException If there was a problem parsing the payload.
	 */
	public MB_ReadException(boolean ack, short sequence, short protocol,
	                        byte station, byte function, byte[] payload)
		throws IOException
	{
		super(FC, ack, sequence, protocol, station, function, payload);
		
		if ( ! ack) {
			if (payload.length != 0) {
				logger.error(String.format("Payload size is %d bytes, must be 0",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
		}
		else if ( ! err) {
			if (payload.length != 1) {
				logger.error(String.format("Payload size is %d bytes, must be 1",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
		}
	}
	
	/**
	 * Construct a new MB_ReadException message.
	 */
	public MB_ReadException()
	{
		super(FC, false, false);
	}
	
	/**
	 * Construct an acknowledgment to an MB_ReadException message.
	 * 
	 * @param err Whether this is an error response.
	 * @param code The error code (if <code>err</code>), or the exception code
	 *   otherwise.
	 */
	public MB_ReadException(boolean err, byte code)
	{
		super(FC, true, err, code);
	}
	
	/** Get the exception code from this message. */
	public byte getCode()
	{
		return getByte(0, true, false, "Exception Code");
	}
	
	/**
	 * Build an acknowledgment to this message. Non-acknowledgments only.
	 * 
	 * @param code The exception code to return.
	 * @return An acknowledgment to this message, ready to be sent.
	 */
	public MB_ReadException acknowledge(byte code)
	{
		MB_ReadException msg = new MB_ReadException(false, code);
		msg.setup(getSequence(), getStation());
		msg.setProtocol(getProtocol());
		
		return msg;
	}
	
	@Override
	public String getErrorMessage()
	{
		if ( ! isError()) {
			return null;
		}
		
		return getErrorString(getErrorCode());
	}
	
	/**
	 * Translate an error code into a descriptive string.
	 * 
	 * @param code The error code.
	 * @return The descriptive string.
	 */
	public static String getErrorString(byte code)
	{
		switch (code) {
			case ERR_FUNCTION:
				return "Unsupported function code";
			case ERR_IO:
				return "Error accessing registers";
			default:
				return "(Unknown)";
		}
	}
	
	@Override
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		byte[] payload = getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (ack) {
				if (err) {
					if (i == 0) {
						ans.append(String.format(" Error Code = %s",
						                         getErrorString(payload[0])));
					}
				}
				else {
					if (i == 0) {
						ans.append(String.format(" Exception Code = %d",
						                         getCode() & 0xff));
					}
				}
			}
			else {
				// No payload
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(ModbusMessage ackMsg, String indent)
	{
		if (this.ack) {
			throw new IllegalStateException("Cannot ACK an ACK, nor describe such a construct");
		}
		else if ( ! (ackMsg instanceof MB_ReadException)) {
			throw new IllegalArgumentException("ACK not an MB_ReadException message");
		}
		
		// Description is the same whether we know the request or not
		return ackMsg.describePayload(indent);
	}
}

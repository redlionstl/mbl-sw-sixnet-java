/*
 * MB_AnalogRead.java
 *
 * A superclass for Modbus messages that read analog registers.
 *
 * Jonathan Pearson
 * May 11, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

/**
 * A superclass for Modbus messages that read analog registers.
 *
 * @author Jonathan Pearson
 */
public abstract class MB_AnalogRead
	extends MB_StandardRead
{
	/**
	 * Initialize the common back-end for analog register reading. This verifies
	 * the payload size if it is a request or an error, but not if it is an
	 * acknowledgment (as the sizes in an acknowledgment are different depending
	 * on data type).
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param ack Whether this is an acknowledgment.
	 * @param sequence The sequence number of the message.
	 * @param protocol The protocol of the message.
	 * @param station The station number of the message.
	 * @param function The function code of the message.
	 * @param payload The remaining payload of the message.
	 * @throws IOException If there is a problem parsing the message.
	 */
	protected MB_AnalogRead(byte realFC, boolean ack, short sequence,
	                        short protocol, byte station, byte function,
	                        byte[] payload)
		throws IOException
	{
		super(realFC, ack, sequence, protocol, station, function, payload);
		
		// Subclass needs to verify successful ACK payload size
	}
	
	/**
	 * Construct a message from its base pieces.
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param ack Whether this is an acknowledgment.
	 * @param err Whether this is an error (it should also be an acknowledgment
	 *   in this case).
	 * @param args A list of bytes, shorts, ints, longs, or arrays of those
	 *   types. These will be packed into the payload in the order they were
	 *   passed, in network byte order.
	 */
	protected MB_AnalogRead(byte realFC, boolean ack, boolean err,
	                          Object... args)
	{
		super(realFC, ack, err, args);
	}
	
	/**
	 * Get the starting address (unsigned). Non-acknowledgments only.
	 */
	public short getStart()
	{
		return getShort(0, false, false, "Start");
	}
	
	/**
	 * Get the register count (unsigned). Non-acknowledgments only.
	 */
	public short getCount()
	{
		return getShort(2, false, false, "Count");
	}
	
	/**
	 * Get the number of returned bytes (unsigned). Acknowledgments only.
	 */
	public byte getByteCount()
	{
		return getByte(0, true, false, "Byte Count");
	}
	
	/**
	 * Get the number of returned registers. Acknowledgments only.
	 */
	public int getRegisterCount()
	{
		return ((getByteCount() & 0xff) / 2);
	}
	
	/**
	 * Get a copy of the returned bytes. Acknowledgments only.
	 */
	public byte[] getReturnedBytes()
	{
		return getByteArray(1, getByteCount() & 0xff, true, false,
		                    "Register Values");
	}
	
	/**
	 * Get the actual returned register values. Acknowledgments only.
	 */
	public short[] getReturnedRegisters()
	{
		return getShortArray(1, getRegisterCount(), true, false,
		                     "Register Values");
	}
	
	/**
	 * Get a single register from the returned values. Acknowledgments only.
	 * 
	 * @param idx The register index.
	 * @return The value of the register at position <code>idx</code> in this
	 *   reply.
	 */
	public short getRegister(int idx)
	{
		return getShort(1 + idx * 2, true, false, "Register Values");
	}
	
	@Override
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		byte[] payload = getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (ack) {
				if (err) {
					if (i == 0) {
						ans.append(String.format(" Error Code = %s",
						                         getErrorString(getErrorCode())));
					}
				}
				else {
					if (i == 0) {
						ans.append(String.format(" Byte Count = %d",
						                         getByteCount() & 0xff));
					}
					else if (((i - 1) & 1) == 0) {
						ans.append(String.format(" Register Value = %d",
						                         getRegister((i - 1) / 2) & 0xffff));
					}
				}
			}
			else {
				if (i == 0) {
					ans.append(String.format(" Start = %d",
					                         getStart() & 0xffff));
				}
				else if (i == 2) {
					ans.append(String.format(" Count = %d",
					                         getCount() & 0xffff));
				}
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(ModbusMessage ackMsg, String indent)
	{
		if (this.ack) {
			throw new IllegalStateException("Cannot ACK an ACK, nor describe such a construct");
		}
		else if ( ! (ackMsg instanceof MB_AnalogRead)) {
			throw new IllegalArgumentException("ACK not an MB_AnalogRead message");
		}
		
		// Description is the same whether we know the request or not
		return ackMsg.describePayload(indent);
	}
}

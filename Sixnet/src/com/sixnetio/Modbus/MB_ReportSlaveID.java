/*
 * MB_ReportSlaveID.java
 *
 * Represents a Modbus Report Slave ID message.
 *
 * Jonathan Pearson
 * May 18, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Represents a Modbus Report Slave ID message.
 *
 * @author Jonathan Pearson
 */
public class MB_ReportSlaveID
	extends MB_IOMessage
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Function code for 'Report Slave ID' (this message). */
	public static final byte FC = 0x11;
	
	/** Error meaning function code not supported. */
	public static final byte ERR_FUNCTION = 1;
	
	/** Error indicating the counter could not be retrieved. */
	public static final byte ERR_IO = 4;
	
	
	/** Get the name of the function that this class implements. */
	public static String getFunctionName()
	{
		return "Report Slave ID";
	}
	
	/**
	 * Construct a new MB_ReportSlaveID message.
	 * 
	 * @param ack Whether this is an acknowledgment.
	 * @param sequence The sequence number.
	 * @param protocol The protocol.
	 * @param station The station number.
	 * @param function the function code (must be {@link #FC}.
	 * @param payload The payload of the message.
	 * @throws IOException If there was a problem parsing the payload.
	 */
	public MB_ReportSlaveID(boolean ack, short sequence, short protocol,
	                        byte station, byte function, byte[] payload)
		throws IOException
	{
		super(FC, ack, sequence, protocol, station, function, payload);
		
		if ( ! ack) {
			if (payload.length != 0) {
				logger.error(String.format("Payload size is %d bytes, must be 0",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
		}
		else if ( ! err) {
			if (payload.length < 1) {
				logger.error(String.format("Payload size is %d bytes, must be at least 1",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
		}
	}
	
	/**
	 * Construct a new MB_ReportSlaveID message.
	 */
	public MB_ReportSlaveID()
	{
		super(FC, false, false);
	}
	
	/**
	 * Construct a new MB_ReportSlaveID acknowledgment.
	 * 
	 * @param slaveID The slave ID data to return.
	 */
	public MB_ReportSlaveID(byte[] slaveID)
	{
		super(FC, true, false, (byte)(slaveID.length & 0xff), slaveID);
	}
	
	/**
	 * Construct a new MB_ReportSlaveID error acknowledgment.
	 * 
	 * @param code The error code to return.
	 */
	public MB_ReportSlaveID(byte code)
	{
		super(FC, true, true, code);
	}
	
	/** Get the number of bytes in the slave ID field. Acknowledgments only. */
	public int getSlaveIDCount()
	{
		return (getByte(0, true, false, "Byte Count") & 0xff);
	}
	
	/** Get the returned slave ID data. Acknowledgments only. */
	public byte[] getSlaveID()
	{
		return getByteArray(1, getSlaveIDCount(), true, false, "Slave ID");
	}
	
	/**
	 * Build an acknowledgment to this message. Non-acknowledgments only.
	 * 
	 * @param slaveID The slave ID data to return.
	 */
	public MB_ReportSlaveID acknowledge(byte[] slaveID)
	{
		MB_ReportSlaveID msg = new MB_ReportSlaveID(slaveID);
		msg.setup(getSequence(), getStation());
		msg.setProtocol(getProtocol());
		
		return msg;
	}
	
	@Override
	public String getErrorMessage()
	{
		if ( ! isError()) {
			return null;
		}
		
		return getErrorString(getErrorCode());
	}
	
	/**
	 * Translate an error code into a descriptive string.
	 * 
	 * @param code The error code.
	 * @return The descriptive string.
	 */
	public static String getErrorString(byte code)
	{
		switch (code) {
			case ERR_FUNCTION:
				return "Unsupported function code";
			case ERR_IO:
				return "Error retrieving slave ID";
			default:
				return "(Unknown)";
		}
	}
	
	@Override
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		byte[] payload = getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (ack) {
				if (err) {
					if (i == 0) {
						ans.append(String.format(" Error Code = %s",
						                         getErrorString(payload[0])));
					}
				}
				else {
					if (i == 0) {
						ans.append(String.format(" Byte Count"));
					}
					else if (i == 1) {
						ans.append(String.format(" Slave ID Data"));
					}
				}
			}
			else {
				// No data in the request
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(ModbusMessage ackMsg, String indent)
	{
		if (this.ack) {
			throw new IllegalStateException("Cannot ACK an ACK, nor describe such a construct");
		}
		else if ( ! (ackMsg instanceof MB_ReportSlaveID)) {
			throw new IllegalArgumentException("ACK not an MB_ReportSlaveID message");
		}
		
		// Description is the same whether we know the request or not
		return ackMsg.describePayload(indent);
	}
}

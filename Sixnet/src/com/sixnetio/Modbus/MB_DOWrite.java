/*
 * MB_DOWrite.java
 *
 * Implements the Modbus 'Write Single Coil' message.
 *
 * Jonathan Pearson
 * May 7, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Implements the Modbus 'Write Single Coil' message, which writes a single
 * discrete output register.
 *
 * @author Jonathan Pearson
 */
public class MB_DOWrite
	extends MB_StandardWrite
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** The value to write to a register meaning 'true' or 'on'. */
	public static final short VAL_ON = (short)0xff00;
	
	/** The value to write to a register meaning 'false' or 'off'. */
	public static final short VAL_OFF = (short)0x0000;
	
	/** Function code for 'Write Single Coil' (this message). */
	public static final byte FC = 0x05;
	
	/**
	 * Get the name of the function that this class implements.
	 */
	public static String getFunctionName()
	{
		return "Write Single Coil";
	}
	
	/**
	 * Construct a new MB_DOWrite message. Note: This does not verify that the
	 * register value is one of the two legal values.
	 * 
	 * @param ack Whether this message is an acknowledgment.
	 * @param sequence The sequence number.
	 * @param protocol The protocol.
	 * @param station The station number.
	 * @param function The function code (must be {@link #FC}).
	 * @param payload The payload of the message.
	 * @throws IOException If there was a problem parsing the payload.
	 */
	public MB_DOWrite(boolean ack, short sequence, short protocol,
	                  byte station, byte function, byte[] payload)
		throws IOException
	{
		super(FC, ack, sequence, protocol, station, function, payload);
	}
	
	/**
	 * Construct a new MB_DOWrite message, or acknowledgment (they look the
	 * same).
	 * 
	 * @param ack Whether this should be an acknowledgment.
	 * @param addr The address to write to (unsigned).
	 * @param value The value to write to that address.
	 */
	public MB_DOWrite(boolean ack, short addr, boolean value)
	{
		super(FC, ack, addr, value ? VAL_ON : VAL_OFF);
	}
	
	/**
	 * Construct a new MB_DOWrite message, or acknowledgment (they look the
	 * same). Allows an illegal value to be specified for the register being
	 * written.
	 * 
	 * @param ack Whether this should be an acknowledgment.
	 * @param addr The address to write to (unsigned).
	 * @param value The value to write to that address (unsigned).
	 */
	public MB_DOWrite(boolean ack, short addr, short value)
	{
		super(FC, ack, addr, value);
	}
	
	/**
	 * Construct an error response to an MB_DOWrite message.
	 * 
	 * @param code The error code to return.
	 */
	public MB_DOWrite(byte code)
	{
		super(FC, code);
	}
	
	/**
	 * Get the value of the register that was written. Non-errors only.
	 * 
	 * @return The value of the register. This treats any non-{@link #VAL_OFF}
	 *   response as 'on', however the Modbus protocol specifies that the only
	 *   legal values are {@link #VAL_OFF} and {@link #VAL_ON}. Use
	 *   {@link #getRegisterValue()} to see what was actually written.
	 */
	public boolean getRegister()
	{
		if (getRegisterValue() == VAL_OFF) {
			return false;
		}
		else {
			return true;
		}
	}
	
	/**
	 * Build an acknowledgment to this message. Non-acknowledgments only. Note:
	 * This allows you to return a written value other than the value specified
	 * in the request.
	 * 
	 * @param value The value written to the register.
	 * @return An acknowledgment to this message, ready to be sent.
	 */
	public MB_DOWrite acknowledge(boolean value)
	{
		MB_DOWrite msg = new MB_DOWrite(true, getAddress(), value);
		msg.setup(getSequence(), getStation());
		msg.setProtocol(getProtocol());
		
		return msg;
	}
	
	/**
	 * Build an acknowledgment to this message. Non-acknowledgments only. Note:
	 * This allows you to return an illegal value as the value written to the
	 * register.
	 * 
	 * @param value The value written to the register.
	 * @return An acknowledgment to this message, ready to be sent.
	 */
	public MB_DOWrite acknowledge(short value)
	{
		MB_DOWrite msg = new MB_DOWrite(true, getAddress(), value);
		setup(getSequence(), getStation());
		msg.setProtocol(getProtocol());
		
		return msg;
	}
	
	@Override
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		byte[] payload = getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (ack) {
				if (err) {
					if (i == 0) {
						ans.append(String.format(" Error Code = %s",
						                         getErrorString(payload[0])));
					}
				}
				else {
					if (i == 0) {
						ans.append(String.format(" Address = %d",
						                         getAddress() & 0xffff));
					}
					else if (i == 2) {
						ans.append(String.format(" Register Value = %b",
						                         getRegister()));
					}
				}
			}
			else {
				if (i == 0) {
					ans.append(String.format(" Address = %d",
					                         getAddress() & 0xffff));
				}
				else if (i == 2) {
					ans.append(String.format(" Register Value = %b",
					                         getRegister()));
				}
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(ModbusMessage ackMsg, String indent)
	{
		if (this.ack) {
			throw new IllegalStateException("Cannot ACK an ACK, nor describe such a construct");
		}
		else if ( ! (ackMsg instanceof MB_DOWrite)) {
			throw new IllegalArgumentException("ACK not an MB_DOWrite message");
		}
		
		// Description is the same whether we know the request or not
		return ackMsg.describePayload(indent);
	}
}

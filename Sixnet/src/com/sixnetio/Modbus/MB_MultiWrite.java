/*
 * MB_MultiWrite.java
 *
 * Groups common code for a number of the Modbus multiple register writing
 * messages.
 *
 * Jonathan Pearson
 * May 18, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * A standard back-end for Modbus multiple register writing messages.
 *
 * @author Jonathan Pearson
 */
public abstract class MB_MultiWrite
	extends MB_IOMessage
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Error meaning function code not supported. */
	public static final byte ERR_FUNCTION = 1;
	
	/** Error meaning either start address or range is bad. */
	public static final byte ERR_RANGE = 2;
	
	/** Error meaning the number of values being written is bad. */
	public static final byte ERR_COUNT = 3;
	
	/** Error indicating the action of accessing the requested values failed. */
	public static final byte ERR_IO = 4;
	
	/**
	 * Initialize the common back-end for multiple I/O register writing. This
	 * verifies the payload size, but not its contents.
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param ack Whether this is an acknowledgment.
	 * @param sequence The sequence number of the message.
	 * @param protocol The protocol of the message.
	 * @param station The station number of the message.
	 * @param function The function code of the message.
	 * @param payload The remaining payload of the message.
	 * @throws IOException If there is a problem parsing the message.
	 */
	protected MB_MultiWrite(byte realFC, boolean ack, short sequence,
	                        short protocol, byte station, byte function,
	                        byte[] payload)
		throws IOException
	{
		super(realFC, ack, sequence, protocol, station, function, payload);
		
		if ( ! ack) {
			if (payload.length < 5) {
				logger.error(String.format("Payload size is %d bytes, must be at least 5",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
			
			int bytes = payload[0] & 0xff;
			if (payload.length - 5 != bytes) {
				logger.error(String.format("Payload size is %d bytes, must be %d",
				                           payload.length, bytes + 5));
				throw new IOException("Payload is the wrong size");
			}
		}
		else if ( ! err) {
			if (payload.length != 4) {
				logger.error(String.format("Payload size is %d bytes, must be 4",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
		}
	}
	
	/**
	 * Construct a message from its base pieces.
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param ack Whether this is an acknowledgment.
	 * @param err Whether this is an error (it should also be an acknowledgment
	 *   in this case).
	 * @param args A list of bytes, shorts, ints, longs, or arrays of those
	 *   types. These will be packed into the payload in the order they were
	 *   passed, in network byte order.
	 */
	protected MB_MultiWrite(byte realFC, boolean ack, boolean err,
	                       Object... args)
	{
		super(realFC, ack, err, args);
	}
	
	/**
	 * Construct a new error response to a register write command.
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param code The error code to return.
	 */
	protected MB_MultiWrite(byte realFC, byte code)
	{
		super(realFC, true, true, code);
	}
	
	/**
	 * Get the starting address (unsigned). Non-errors only.
	 */
	public short getStart()
	{
		return getShort(0, null, false, "Starting Address");
	}
	
	/**
	 * Get the number of outputs being written. Non-errors only.
	 */
	public short getCount()
	{
		return getShort(2, null, false, "Quantity of Outputs");
	}
	
	/**
	 * Get the number of bytes worth of register values are contained in this
	 * message. Non-acknowledgments only.
	 */
	public int getByteCount()
	{
		return (getByte(3, false, false, "Byte Count") & 0xff);
	}
	
	@Override
	public String getErrorMessage()
	{
		if ( ! isError()) {
			return null;
		}
		
		return getErrorString(getErrorCode());
	}
	
	/**
	 * Translate an error code into a descriptive string.
	 * 
	 * @param code The error code.
	 * @return The descriptive string.
	 */
	public static String getErrorString(byte code)
	{
		switch (code) {
			case ERR_FUNCTION:
				return "Unsupported function code";
			case ERR_RANGE:
				return "Illegal range";
			case ERR_COUNT:
				return "Invalid count";
			case ERR_IO:
				return "Error accessing registers";
			default:
				return "(Unknown)";
		}
	}
}

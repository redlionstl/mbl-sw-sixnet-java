/*
 * MB_GetCommEventLog.java
 *
 * Represents a Modbus Get Comm Event Log message.
 *
 * Jonathan Pearson
 * May 18, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Represents a Modbus Get Comm Event Log message.
 *
 * @author Jonathan Pearson
 */
public class MB_GetCommEventLog
	extends MB_IOMessage
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Function code for 'Get Comm Event Log' (this message). */
	public static final byte FC = 0x0c;
	
	/** Error meaning function code not supported. */
	public static final byte ERR_FUNCTION = 1;
	
	/** Error indicating the data could not be retrieved. */
	public static final byte ERR_IO = 4;
	
	
	/** The status value indicating that the device is not busy. */
	public static final short VAL_NOTBSY = 0;
	
	/** The status value indicating that the device is busy. */
	public static final short VAL_BSY = (short)0xffff;
	
	
	/** Receive event mask indicating communication error. */
	public static final byte MASK_RECV_COMMERR = 0x02;
	
	/** Receive event mask indicating character overrun. */
	public static final byte MASK_RECV_CHAROVERRUN = 0x10;
	
	/** Receive event mask indicating the device is in listen-only mode. */
	public static final byte MASK_RECV_LISTENONLY = 0x20;
	
	/** Receive event mask indicating that a broadcast was received. */
	public static final byte MASK_RECV_BCAST = 0x40;
	
	/**
	 * Mask with this and match to {@link #MATCH_RECV} to indicate a receive
	 * event description.
	 */
	public static final byte MASK_RECV = (byte)0x80;
	
	/**
	 * Mask with {@link #MASK_RECV} and match to this to indicate a receive
	 * event description.
	 */
	public static final byte MATCH_RECV = (byte)0x80;
	
	/** Send event mask indicating that a read exception was sent. */
	public static final byte MASK_SEND_READEXC = 0x01;
	
	/** Send event mask indicating that a slave abort exception was sent. */
	public static final byte MASK_SEND_SLABRT = 0x02;
	
	/** Send event mask indicating that a slave busy exception was sent. */
	public static final byte MASK_SEND_SLBSY = 0x04;
	
	/**
	 * Send event mask indicating that a slave program NAK exception was sent.
	 */
	public static final byte MASK_SEND_SLNAK = 0x08;
	
	/** Send event mask indicating that a write timeout error occurred. */
	public static final byte MASK_SEND_WRTO = 0x10;
	
	/** Send event mask indicating that the device is in listen-only mode. */
	public static final byte MASK_SEND_LISTENONLY = 0x20;
	
	/**
	 * Mask with this and match to {@link #MATCH_SEND} to indicate a send event
	 * description.
	 */
	public static final byte MASK_SEND = (byte)0xc0;
	
	/**
	 * Mask with {@link #MASK_SEND} and match to this to indicate a send event
	 * description.
	 */
	public static final byte MATCH_SEND = 0x40;
	
	
	/** Get the name of the function that this class implements. */
	public static String getFunctionName()
	{
		return "Get Comm Event Log";
	}
	
	/**
	 * Construct a new MB_GetCommEventLog message.
	 * 
	 * @param ack Whether this is an acknowledgment.
	 * @param sequence The sequence number.
	 * @param protocol The protocol.
	 * @param station The station number.
	 * @param function the function code (must be {@link #FC}.
	 * @param payload The payload of the message.
	 * @throws IOException If there was a problem parsing the payload.
	 */
	public MB_GetCommEventLog(boolean ack, short sequence, short protocol,
	                          byte station, byte function, byte[] payload)
		throws IOException
	{
		super(FC, ack, sequence, protocol, station, function, payload);
		
		if ( ! ack) {
			if (payload.length != 0) {
				logger.error(String.format("Payload size is %d bytes, must be 0",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
		}
		else if ( ! err) {
			if (payload.length < 7) {
				logger.error(String.format("Payload size is %d bytes, must be at least 7",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
			
			int bytes = payload[0] & 0xff;
			if (payload.length - 1 != bytes) {
				logger.error(String.format("Payload size is %d bytes, must be %d",
				                           payload.length, bytes + 1));
				throw new IOException("Payload is the wrong size");
			}
		}
	}
	
	/**
	 * Construct a new MB_GetCommEventLog message.
	 */
	public MB_GetCommEventLog()
	{
		super(FC, false, false);
	}
	
	/**
	 * Construct a new MB_GetCommEventLog acknowledgment.
	 * 
	 * @param status The status value.
	 * @param evtcount The event count.
	 * @param msgcount The message count.
	 * @param events The event descriptions.
	 */
	public MB_GetCommEventLog(short status, short evtcount, short msgcount,
	                          byte[] events)
	{
		super(FC, true, false,
		      (byte)((events.length + 6) & 0xff), status, evtcount, msgcount,
		      events);
	}
	
	/**
	 * Construct a new MB_GetCommEventLog error acknowledgment.
	 * 
	 * @param code The error code to return.
	 */
	public MB_GetCommEventLog(byte code)
	{
		super(FC, true, true, code);
	}
	
	/** Get the number of event description bytes. Acknowledgments only. */
	public int getEventDescriptionCount()
	{
		return ((getByte(0, true, false, "Byte Count") - 6) & 0xff);
	}
	
	/** Get the status field. Acknowledgments only. */
	public short getStatus()
	{
		return getShort(1, true, false, "Status");
	}
	
	/** Get the event count field. Acknowledgments only. */
	public short getEventCount()
	{
		return getShort(3, true, false, "Event Count");
	}
	
	/** Get the message count field. Acknowledgments only. */
	public short getMessageCount()
	{
		return getShort(5, true, false, "Message Count");
	}
	
	/**
	 * Get an event description byte. Acknowledgments only.
	 * 
	 * @param index The byte index, in the range
	 *   [0, {@link #getEventDescriptionCount()}).
	 */
	public byte getEventDescription(int index)
	{
		return getByte(6 + index, true, false, "Events");
	}
	
	/** Get all event description bytes. Acknowledgments only. */
	public byte[] getEvents()
	{
		return getByteArray(6, getEventDescriptionCount(), true, false,
		                    "Events");
	}
	
	/**
	 * Build an acknowledgment to this message. Non-acknowledgments only.
	 * 
	 * @param status The status value to return.
	 * @param count The event count to return.
	 */
	public MB_GetCommEventCounter acknowledge(short status, short count)
	{
		MB_GetCommEventCounter msg = new MB_GetCommEventCounter(status, count);
		msg.setup(getSequence(), getStation());
		msg.setProtocol(getProtocol());
		
		return msg;
	}
	
	@Override
	public String getErrorMessage()
	{
		if ( ! isError()) {
			return null;
		}
		
		return getErrorString(getErrorCode());
	}
	
	/**
	 * Translate an error code into a descriptive string.
	 * 
	 * @param code The error code.
	 * @return The descriptive string.
	 */
	public static String getErrorString(byte code)
	{
		switch (code) {
			case ERR_FUNCTION:
				return "Unsupported function code";
			case ERR_IO:
				return "Error retrieving event counter";
			default:
				return "(Unknown)";
		}
	}
	
	/**
	 * Get a descriptive string describing this acknowledgment's status value.
	 * 
	 * @return The status value.
	 * @throws IllegalStateException If this is not an acknowledgment.
	 */
	public String getStatusString()
	{
		return getStatusString(getStatus());
	}
	
	/**
	 * Translate a status value into a descriptive string.
	 * 
	 * @param status The status value code.
	 * @return The descriptive string.
	 */
	public static String getStatusString(short status)
	{
		switch (status) {
			case VAL_NOTBSY:
				return "Not Busy";
			case VAL_BSY:
				return "Busy";
			default:
				return "(Unknown)";
		}
	}
	
	/**
	 * Translate an event description byte into a descriptive string.
	 * 
	 * @param event The event description byte.
	 * @return The descriptive string.
	 */
	public static String describeEvent(byte event)
	{
		StringBuilder ans = new StringBuilder();
		
		if ((event & MASK_RECV) == MATCH_RECV) {
			// Receive event
			ans.append("Recv");
			
			if ((event & MASK_RECV_COMMERR) == MASK_RECV_COMMERR) {
				ans.append(", CommError");
			}
			
			if ((event & MASK_RECV_CHAROVERRUN) == MASK_RECV_CHAROVERRUN) {
				ans.append(", CharOverrun");
			}
			
			if ((event & MASK_RECV_LISTENONLY) == MASK_RECV_LISTENONLY) {
				ans.append(", ListenOnly");
			}
			
			if ((event & MASK_RECV_BCAST) == MASK_RECV_BCAST) {
				ans.append(", BroadcastReceived");
			}
		}
		else if ((event & MASK_SEND) == MATCH_SEND) {
			// Send event
			ans.append("Send");
			
			if ((event & MASK_SEND_READEXC) == MASK_SEND_READEXC) {
				ans.append(", ReadException");
			}
			
			if ((event & MASK_SEND_SLABRT) == MASK_SEND_SLABRT) {
				ans.append(", SlaveAbort");
			}
			
			if ((event & MASK_SEND_SLBSY) == MASK_SEND_SLBSY) {
				ans.append(", SlaveBusy");
			}
			
			if ((event & MASK_SEND_SLNAK) == MASK_SEND_SLNAK) {
				ans.append(", SlaveNAK");
			}
			
			if ((event & MASK_SEND_WRTO) == MASK_SEND_WRTO) {
				ans.append(", WriteTimeout");
			}
			
			if ((event & MASK_SEND_LISTENONLY) == MASK_SEND_LISTENONLY) {
				ans.append(", ListenOnly");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		byte[] payload = getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (ack) {
				if (err) {
					if (i == 0) {
						ans.append(String.format(" Error Code = %s",
						                         getErrorString(payload[0])));
					}
				}
				else {
					if (i == 0) {
						ans.append(String.format(" Byte count"));
					}
					else if (i == 1) {
						ans.append(String.format(" Status = 0x%04x (%s)",
						                         getStatus() & 0xffff,
						                         getStatusString()));
					}
					else if (i == 3) {
						ans.append(String.format(" Event Count = %d",
						                         getEventCount() & 0xffff));
					}
					else if (i == 5) {
						ans.append(String.format(" Message Count = %d",
						                         getMessageCount() & 0xffff));
					}
					else if (i >= 7) {
						ans.append(String.format(" Event: %s",
						                         describeEvent(getEventDescription(i - 7))));
					}
				}
			}
			else {
				// No data in the request
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(ModbusMessage ackMsg, String indent)
	{
		if (this.ack) {
			throw new IllegalStateException("Cannot ACK an ACK, nor describe such a construct");
		}
		else if ( ! (ackMsg instanceof MB_GetCommEventLog)) {
			throw new IllegalArgumentException("ACK not an MB_GetCommEventLog message");
		}
		
		// Description is the same whether we know the request or not
		return ackMsg.describePayload(indent);
	}
}

/*
 * MB_MultiDOWrite.java
 *
 * A Modbus message for writing multiple DOs.
 *
 * Jonathan Pearson
 * May 18, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * A Modbus message for writing multiple DOs.
 *
 * @author Jonathan Pearson
 */
public class MB_MultiDOWrite
	extends MB_MultiWrite
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Function code for 'Write Multiple Coils' (this message). */
	public static final byte FC = 0x0f;
	
	/**
	 * Get the name of the function that this class implements.
	 */
	public static String getFunctionName()
	{
		return "Write Multiple Coils";
	}
	
	/**
	 * Construct a new MB_MultiDOWrite message.
	 * 
	 * @param ack Whether this message is an acknowledgment.
	 * @param sequence The sequence number.
	 * @param protocol The protocol.
	 * @param station The station number.
	 * @param function The function code (must be {@link #FC}).
	 * @param payload The payload of the message.
	 * @throws IOException If there was a problem parsing the payload.
	 */
	public MB_MultiDOWrite(boolean ack, short sequence, short protocol,
	                       byte station, byte function, byte[] payload)
		throws IOException
	{
		super(FC, ack, sequence, protocol, station, function, payload);
	}
	
	/**
	 * Construct a new MB_MultiDOWrite request from its pieces.
	 * 
	 * @param start The starting address to write (unsigned).
	 * @param data The values to write to those registers.
	 */
	public MB_MultiDOWrite(short start, boolean[] data)
	{
		super(FC, false, false,
		      start, (short)(data.length & 0xffff),
		      (byte)(((data.length + 7) >> 3) & 0xff), data);
		
		if (data.length < 1 || data.length > 1968) {
			logger.error("Register count must be in the range [1, 1968]: " +
			             data.length);
			throw new IllegalArgumentException("Bad register count");
		}
	}
	
	/**
	 * Construct a new MB_MultiDOWrite acknowledgment.
	 * 
	 * @param start The starting address that was written (unsigned).
	 * @param count The number of registers that were written (unsigned).
	 */
	public MB_MultiDOWrite(short start, short count)
	{
		super(FC, true, false, start, count);
	}
	/**
	 * Construct an error response to an MB_MultiDOWrite message.
	 * 
	 * @param code The error code to return.
	 */
	public MB_MultiDOWrite(byte code)
	{
		super(FC, code);
	}
	
	/** Get a single register value. Non-acknowledgments only. */
	public boolean getRegister(int index)
	{
		return getBit(40 + index, false, false, "Output Values");
	}
	
	/** Get all registers being written. Non-acknowledgments only. */
	public boolean[] getRegisters()
	{
		return getBitArray(40, getCount() & 0xffff, false, false,
		                   "Output Values");
	}
	
	/**
	 * Build an acknowledgment to this message. Non-acknowledgments only. Note:
	 * this will allow you to return a different range of registers than was
	 * requested without any warning.
	 * 
	 * @param start The starting register that was written (unsigned).
	 * @param count The number of registers that were written (unsigned).
	 * @return An acknowledgment to this message, ready to be sent.
	 */
	public MB_MultiDOWrite acknowledge(short start, short count)
	{
		MB_MultiDOWrite msg = new MB_MultiDOWrite(start, count);
		msg.setup(getSequence(), getStation());
		msg.setProtocol(getProtocol());
		
		return msg;
	}
	
	/**
	 * Build an acknowledgment to this message with the default values.
	 * 
	 * @return An acknowledgment to this message, ready to be sent.
	 */
	public MB_MultiDOWrite acknowledge()
	{
		return acknowledge(getStart(), getCount());
	}
	
	@Override
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		byte[] payload = getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (ack) {
				if (err) {
					if (i == 0) {
						ans.append(String.format(" Error Code = %s",
						                         getErrorString(payload[0])));
					}
				}
				else {
					if (i == 0) {
						ans.append(String.format(" Start = %d",
						                         getStart() & 0xffff));
					}
					else if (i == 2) {
						ans.append(String.format(" Count = %d",
						                         getCount() & 0xffff));
					}
				}
			}
			else {
				if (i == 0) {
					ans.append(String.format(" Start = %d",
					                         getStart() & 0xffff));
				}
				else if (i == 2) {
					ans.append(String.format(" Count = %d",
					                         getCount() & 0xffff));
				}
				else if (i == 4) {
					ans.append(String.format(" Byte Count"));
				}
				else if (i == 5) {
					ans.append(String.format(" Register Values"));
				}
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(ModbusMessage ackMsg, String indent)
	{
		if (this.ack) {
			throw new IllegalStateException("Cannot ACK an ACK, nor describe such a construct");
		}
		else if ( ! (ackMsg instanceof MB_MultiDOWrite)) {
			throw new IllegalArgumentException("ACK not an MB_MultiDOWrite message");
		}
		
		// Description is the same whether we know the request or not
		return ackMsg.describePayload(indent);
	}
}

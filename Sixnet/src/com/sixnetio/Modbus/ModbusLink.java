/*
 * ModbusLink.java
 *
 * Implements functions for reading/writing Modbus messages over UDP and TCP.
 * 
 * FUTURE: Implement Modbus over Serial
 *
 * Jonathan Pearson
 * March 20, 2007
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sixnetio.Station.IOProtocolLink;
import com.sixnetio.Station.TransportMethod;
import com.sixnetio.io.UDPInputStream;
import com.sixnetio.util.Utils;

/**
 * Handles communication of Modbus messages over a single TCP or UDP link.
 * 
 * @author Jonathan Pearson
 */
public class ModbusLink
	extends IOProtocolLink
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Standard port for Modbus messages over Ethernet. */
	public static final int MODBUS_PORT = 502;
	private static final Map<TransportMethod, String> defaultParams;
	
	static {
		defaultParams = new HashMap<TransportMethod, String>();
		
		// FUTURE: Implement Modbus over Serial
		// defaultParams.put(TransportMethod.Serial, "9600-8-N-1");
		defaultParams.put(TransportMethod.TCP, "" + MODBUS_PORT);
		defaultParams.put(TransportMethod.UDP, "" + MODBUS_PORT);
		defaultParams.put(TransportMethod.UDPBroadcast, "" + MODBUS_PORT);
		defaultParams.put(TransportMethod.UDPLoop, "" + MODBUS_PORT);
	}
	
	/**
	 * Construct a new ModbusHandler that works over the specified link and
	 * communicates with the device at the specified address.
	 * 
	 * @param transport The link over which to communicate. Supports TCP and the
	 *   various UDP methods.
	 * @param address The address with which to communicate. See
	 *   {@link IOProtocolLink#IOProtocolLink(TransportMethod, String, Map)} for
	 *   details.
	 * @throws IOException If there was a problem opening the transport medium.
	 */
	public ModbusLink(TransportMethod transport, String address)
		throws IOException
	{
		super(transport, address, defaultParams);
	}
	
	@Override
	public ModbusMessage receive(boolean ack)
		throws IOException
	{
		if (closed) {
			throw new IOException("Cannot receive from a closed link");
		}
		
		if (overUDP()) {
			UDPInputStream inp = (UDPInputStream)input;
			inp.flush();
		}
		
		ModbusMessage msg = ModbusMessage.parse(ack, input);
		
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("Received a message via %s:\n%s",
			                           description, msg.toString()));
		}
		
		return msg;
	}
	
	@Override
	public ModbusMessage receive(boolean ack, InetAddress[] addr, int[] port)
		throws IOException
	{
		if (closed) {
			throw new IOException("Cannot receive from a closed link");
		}
		
		if (addr == null || addr.length < 1) {
			addr = new InetAddress[1];
		}
		
		if (port == null || port.length < 1) {
			port = new int[1];
		}
		
		UDPInputStream in = (UDPInputStream)input;
		ModbusMessage msg = ModbusMessage.parse(ack, in);
		
		DatagramPacket pkt = in.getLastPacket();
		addr[0] = pkt.getAddress();
		port[0] = pkt.getPort();
		
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("Received a message from %s:%d:\n",
			                           addr[0].getHostAddress(), port[0],
			                           msg.toString()));
		}
		
		return msg;
	}
	
	@Override
	public ModbusMessage receive()
		throws IOException
	{
		return receive(true);
	}
	
	@Override
	public ModbusMessage receive(InetAddress[] out_addr, int[] out_port)
		throws IOException
	{
		return receive(true, out_addr, out_port);
	}
}

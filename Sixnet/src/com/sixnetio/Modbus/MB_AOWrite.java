/*
 * MB_AOWrite.java
 *
 * Implements the Modbus 'Write Single Register' message.
 *
 * Jonathan Pearson
 * May 7, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Implements the Modbus 'Write Single Register' message, which writes a single
 * analog output register.
 *
 * @author Jonathan Pearson
 */
public class MB_AOWrite
	extends MB_StandardWrite
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Function code for 'Write Single Register' (this message). */
	public static final byte FC = 0x06;
	
	/**
	 * Get the name of the function that this class implements.
	 */
	public static String getFunctionName()
	{
		return "Write Single Register";
	}
	
	/**
	 * Construct a new MB_AOWrite message.
	 * 
	 * @param ack Whether this message is an acknowledgment.
	 * @param sequence The sequence number.
	 * @param protocol The protocol.
	 * @param station The station number.
	 * @param function The function code (must be {@link #FC}).
	 * @param payload The payload of the message.
	 * @throws IOException If there was a problem parsing the payload.
	 */
	public MB_AOWrite(boolean ack, short sequence, short protocol,
	                  byte station, byte function, byte[] payload)
		throws IOException
	{
		super(FC, ack, sequence, protocol, station, function, payload);
	}
	
	/**
	 * Construct a new MB_AOWrite message, or acknowledgment (they look the
	 * same).
	 * 
	 * @param ack Whether this should be an acknowledgment.
	 * @param addr The address to write to (unsigned).
	 * @param value The value to write to that address.
	 */
	public MB_AOWrite(boolean ack, short addr, short value)
	{
		super(FC, ack, addr, value);
	}
	
	/**
	 * Construct an error response to an MB_AOWrite message.
	 * 
	 * @param code The error code to return.
	 */
	public MB_AOWrite(byte code)
	{
		super(FC, code);
	}
	
	/**
	 * Build an acknowledgment to this message. Non-acknowledgments only. Note:
	 * This allows you to return a written value other than the value specified
	 * in the request.
	 * 
	 * @param value The value written to the register.
	 * @return An acknowledgment to this message, ready to be sent.
	 */
	public MB_AOWrite acknowledge(short value)
	{
		MB_AOWrite msg = new MB_AOWrite(true, getAddress(), value);
		msg.setup(getSequence(), getStation());
		msg.setProtocol(getProtocol());
		
		return msg;
	}
	
	@Override
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		byte[] payload = getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (ack) {
				if (err) {
					if (i == 0) {
						ans.append(String.format(" Error Code = %s",
						                         getErrorString(payload[0])));
					}
				}
				else {
					if (i == 0) {
						ans.append(String.format(" Address = %d",
						                         getAddress() & 0xffff));
					}
					else if (i == 2) {
						ans.append(String.format(" Register Value = %d",
						                         getRegisterValue() & 0xffff));
					}
				}
			}
			else {
				if (i == 0) {
					ans.append(String.format(" Address = %d",
					                         getAddress() & 0xffff));
				}
				else if (i == 2) {
					ans.append(String.format(" Register Value = %b",
					                         getRegisterValue() & 0xffff));
				}
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(ModbusMessage ackMsg, String indent)
	{
		if (this.ack) {
			throw new IllegalStateException("Cannot ACK an ACK, nor describe such a construct");
		}
		else if ( ! (ackMsg instanceof MB_AOWrite)) {
			throw new IllegalArgumentException("ACK not an MB_AOWrite message");
		}
		
		// Description is the same whether we know the request or not
		return ackMsg.describePayload(indent);
	}
}

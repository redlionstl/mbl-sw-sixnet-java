/*
 * MB_StandardWrite.java
 *
 * Groups common code for a number of the Modbus register writing messages.
 *
 * Jonathan Pearson
 * May 7, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * A standard back-end for Modbus register writing messages.
 *
 * @author Jonathan Pearson
 */
public abstract class MB_StandardWrite
	extends MB_IOMessage
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Error meaning function code not supported. */
	public static final byte ERR_FUNCTION = 1;
	
	/** Error meaning either start address or range is bad. */
	public static final byte ERR_RANGE = 2;
	
	/** Error meaning the value being written is not legal. */
	public static final byte ERR_VALUE = 3;
	
	/** Error indicating the action of accessing the requested values failed. */
	public static final byte ERR_IO = 4;
	
	/**
	 * Initialize the common back-end for I/O register writing. This verifies
	 * the payload size, but not its contents.
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param ack Whether this is an acknowledgment.
	 * @param sequence The sequence number of the message.
	 * @param protocol The protocol of the message.
	 * @param station The station number of the message.
	 * @param function The function code of the message.
	 * @param payload The remaining payload of the message.
	 * @throws IOException If there is a problem parsing the message.
	 */
	protected MB_StandardWrite(byte realFC, boolean ack, short sequence,
	                           short protocol, byte station, byte function,
	                           byte[] payload)
		throws IOException
	{
		super(realFC, ack, sequence, protocol, station, function, payload);
		
		if ( ! err) {
			// Same payload size check for request and response
			if (payload.length != 4) {
				logger.error(String.format("Payload size if %d bytes, must be 4",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
		}
	}
	
	/**
	 * Construct a message from its base pieces.
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param ack Whether this is an acknowledgment.
	 * @param addr The address of the register to write.
	 * @param val The value to write to that register.
	 */
	protected MB_StandardWrite(byte realFC, boolean ack, short addr, short val)
	{
		super(realFC, ack, false, addr, val);
	}
	
	/**
	 * Construct a new error response to a register write command.
	 * 
	 * @param realFC The base function code of the implementing subclass.
	 * @param code The error code to return.
	 */
	protected MB_StandardWrite(byte realFC, byte code)
	{
		super(realFC, true, true, code);
	}
	
	/**
	 * Get the address (unsigned). Non-acknowledgments only.
	 */
	public short getAddress()
	{
		return getShort(0, null, false, "Register Address");
	}
	
	/**
	 * Get the value written to the register. Non-errors only.
	 */
	public short getRegisterValue()
	{
		return getShort(2, null, false, "Register Value");
	}
	
	@Override
	public String getErrorMessage()
	{
		if ( ! isError()) {
			return null;
		}
		
		return getErrorString(getErrorCode());
	}
	
	/**
	 * Translate an error code into a descriptive string.
	 * 
	 * @param code The error code.
	 * @return The descriptive string.
	 */
	public static String getErrorString(byte code)
	{
		switch (code) {
			case ERR_FUNCTION:
				return "Unsupported function code";
			case ERR_RANGE:
				return "Illegal range";
			case ERR_VALUE:
				return "Invalid register value";
			case ERR_IO:
				return "Error accessing registers";
			default:
				return "(Unknown)";
		}
	}
}

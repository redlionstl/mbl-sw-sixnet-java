/*
 * MB_Diagnostics.java
 *
 * Represents a Modbus Diagnostics message.
 *
 * Jonathan Pearson
 * May 17, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Description
 *
 * @author Jonathan Pearson
 */
public class MB_Diagnostics
	extends MB_IOMessage
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Function code for 'Diagnostics' (this message). */
	public static final byte FC = 0x08;
	
	/** Error meaning function code not supported. */
	public static final byte ERR_FUNCTION = 1;
	
	/** Error meaning the data value was not understood. */
	public static final byte ERR_DATA = 3;
	
	/** Error indicating the diagnostic test failed. */
	public static final byte ERR_DIAGNOSTIC = 4;
	
	
	/** Sub-function indicating return query data. */
	public static final short DIAG_ECHO = 0x00;
	
	/** Sub-function indicating restart communications. */
	public static final short DIAG_RESTART = 0x01;
	
	/** Sub-function indicating return diagnostic register. */
	public static final short DIAG_RETREG = 0x02;
	
	/** Sub-function indicating change ASCII input delimiter. */
	public static final short DIAG_CHGDEL = 0x03;
	
	/** Sub-function indicating force listen only mode. */
	public static final short DIAG_LISTEN = 0x04;
	
	/** Sub-function indicating clear counters and diagnostic register. */
	public static final short DIAG_CLEAR = 0x0a;
	
	/** Sub-function indicating return bus message count. */
	public static final short DIAG_MSGCOUNT = 0x0b;
	
	/** Sub-function indicating return bus communication error count. */
	public static final short DIAG_ERRCOUNT = 0x0c;
	
	/** Sub-function indicating return bus exception error count. */
	public static final short DIAG_EXCCOUNT = 0x0d;
	
	/** Sub-function indicating return slave message count. */
	public static final short DIAG_SLAVE_MSGCOUNT = 0x0e;
	
	/** Sub-function indicating return slave no response count. */
	public static final short DIAG_SLAVE_NORESP = 0x0f;
	
	/** Sub-function indicating return slave NAK count. */
	public static final short DIAG_SLAVE_NAKCOUNT = 0x10;
	
	/** Sub-function indicating return slave busy count. */
	public static final short DIAG_SLAVE_BSYCOUNT = 0x11;
	
	/** Sub-function indicating return bus character overrun count. */
	public static final short DIAG_OVRCOUNT = 0x12;
	
	/** Sub-function indicating clear overrun counter and flag. */
	public static final short DIAG_CLROVR = 0x14;
	
	
	/** Get the name of the function that this class implements. */
	public static String getFunctionName()
	{
		return "Diagnostics";
	}
	
	/**
	 * Construct a new MB_Diagnostics message.
	 * 
	 * @param ack Whether this is an acknowledgment.
	 * @param sequence The sequence number.
	 * @param protocol The protocol.
	 * @param station The station number.
	 * @param function the function code (must be {@link #FC}.
	 * @param payload The payload of the message.
	 * @throws IOException If there was a problem parsing the payload.
	 */
	public MB_Diagnostics(boolean ack, short sequence, short protocol,
	                      byte station, byte function, byte[] payload)
		throws IOException
	{
		super(FC, ack, sequence, protocol, station, function, payload);
		
		if ( ! err) {
			// Same payload size check for request and response
			if (payload.length < 2) {
				logger.error(String.format("Payload size is %d bytes, must be at least 2",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
			
			// Number of bytes after the first two is arbitrary
		}
	}
	
	/**
	 * Construct a new MB_Diagnostics message.
	 * 
	 * @param ack Whether this is an acknowledgment.
	 * @param subFC The sub-function code.
	 * @param dataValues The data to send.
	 */
	public MB_Diagnostics(boolean ack, short subFC, short[] dataValues)
	{
		super(FC, ack, false, subFC, dataValues);
	}
	
	/**
	 * Construct a new MB_Diagnostics error acknowledgment.
	 * 
	 * @param code The error code to return.
	 */
	public MB_Diagnostics(byte code)
	{
		super(FC, true, true, code);
	}
	
	/** Get the sub-function code. */
	public short getSubFunction()
	{
		return getShort(0, null, false, "Sub Function Code");
	}
	
	/** Get the number of data values present in this message. */
	public int getDataValueCount()
	{
		return ((getPayloadDirect().length - 2) / 2);
	}
	
	/** Get the data values. */
	public short[] getDataValues()
	{
		return getShortArray(2, getDataValueCount(), null, false, "Data Values");
	}
	
	/**
	 * Build an acknowledgment to this message. Non-acknowledgments only.
	 * 
	 * @param data The data values with which to respond.
	 */
	public MB_Diagnostics acknowledge(short[] dataValues)
	{
		MB_Diagnostics msg = new MB_Diagnostics(true, getSubFunction(),
		                                        dataValues);
		msg.setup(getSequence(), getStation());
		msg.setProtocol(getProtocol());
		
		return msg;
	}
	
	@Override
	public String getErrorMessage()
	{
		if ( ! isError()) {
			return null;
		}
		
		return getErrorString(getErrorCode());
	}
	
	/**
	 * Translate an error code into a descriptive string.
	 * 
	 * @param code The error code.
	 * @return The descriptive string.
	 */
	public static String getErrorString(byte code)
	{
		switch (code) {
			case ERR_FUNCTION:
				return "Unsupported function code";
			case ERR_DATA:
				return "Bad data values";
			case ERR_DIAGNOSTIC:
				return "Error running diagnostic";
			default:
				return "(Unknown)";
		}
	}
	
	/**
	 * Get a descriptive string describing this message's sub-function.
	 * 
	 * @return The sub-function.
	 * @throws IllegalStateException If this is an error message.
	 */
	public String getSubFunctionString()
	{
		return getSubFunctionString(getSubFunction());
	}
	
	/**
	 * Translate a sub-function code into a descriptive string.
	 * 
	 * @param sf The sub-function code.
	 * @return The descriptive string.
	 */
	public static String getSubFunctionString(short sf)
	{
		switch (sf) {
			case DIAG_ECHO:
				return "Return Query Data";
			case DIAG_RESTART:
				return "Restart Communications Option";
			case DIAG_RETREG:
				return "Return Diagnostic Register";
			case DIAG_CHGDEL:
				return "Change ASCII Input Delimiter";
			case DIAG_LISTEN:
				return "Force Listen Only Mode";
			case DIAG_CLEAR:
				return "Clear Counters and Diagnostic Register";
			case DIAG_MSGCOUNT:
				return "Return Bus Message Count";
			case DIAG_ERRCOUNT:
				return "Return Bus Communication Error Count";
			case DIAG_EXCCOUNT:
				return "Return Bus Exception Error Count";
			case DIAG_SLAVE_MSGCOUNT:
				return "Return Slave Message Count";
			case DIAG_SLAVE_NORESP:
				return "Return Slave No Response Count";
			case DIAG_SLAVE_NAKCOUNT:
				return "Return Slave NAK Count";
			case DIAG_SLAVE_BSYCOUNT:
				return "Return Slave Busy Count";
			case DIAG_OVRCOUNT:
				return "Return Bus Character Overrun Count";
			case DIAG_CLROVR:
				return "Clear Overrun Counter and Flag";
			default:
				return "(Unknown)";
		}
	}
	
	@Override
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		byte[] payload = getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (err) {
				if (i == 0) {
					ans.append(String.format(" Error Code = %s",
					                         getErrorString(payload[0])));
				}
			}
			else {
				// ACK and non-ACK have the same format
				if (i == 0) {
					ans.append(String.format(" Diagnostic Function = %d",
					                         getSubFunction() & 0xffff));
				}
				else if (i == 2) {
					ans.append(String.format(" Diagnostic Function Data Values"));
				}
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(ModbusMessage ackMsg, String indent)
	{
		if (this.ack) {
			throw new IllegalStateException("Cannot ACK an ACK, nor describe such a construct");
		}
		else if ( ! (ackMsg instanceof MB_Diagnostics)) {
			throw new IllegalArgumentException("ACK not an MB_Diagnostics message");
		}
		
		// Description is the same whether we know the request or not
		return ackMsg.describePayload(indent);
	}
}

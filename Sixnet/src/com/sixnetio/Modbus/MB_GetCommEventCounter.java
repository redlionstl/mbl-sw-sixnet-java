/*
 * MB_GetCommEventCounter.java
 *
 * Represents a Modbus Get Comm Event Counter message.
 *
 * Jonathan Pearson
 * May 18, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Represents a Modbus Get Comm Event Counter message.
 *
 * @author Jonathan Pearson
 */
public class MB_GetCommEventCounter
	extends MB_IOMessage
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Function code for 'Get Comm Event Counter' (this message). */
	public static final byte FC = 0x0b;
	
	/** Error meaning function code not supported. */
	public static final byte ERR_FUNCTION = 1;
	
	/** Error indicating the counter could not be retrieved. */
	public static final byte ERR_IO = 4;
	
	
	/** The status value indicating that the device is not busy. */
	public static final short VAL_NOTBSY = 0;
	
	/** The status value indicating that the device is busy. */
	public static final short VAL_BSY = (short)0xffff;
	
	
	/** Get the name of the function that this class implements. */
	public static String getFunctionName()
	{
		return "Get Comm Event Counter";
	}
	
	/**
	 * Construct a new MB_GetCommEventCounter message.
	 * 
	 * @param ack Whether this is an acknowledgment.
	 * @param sequence The sequence number.
	 * @param protocol The protocol.
	 * @param station The station number.
	 * @param function the function code (must be {@link #FC}.
	 * @param payload The payload of the message.
	 * @throws IOException If there was a problem parsing the payload.
	 */
	public MB_GetCommEventCounter(boolean ack, short sequence, short protocol,
	                              byte station, byte function, byte[] payload)
		throws IOException
	{
		super(FC, ack, sequence, protocol, station, function, payload);
		
		if ( ! ack) {
			if (payload.length != 0) {
				logger.error(String.format("Payload size is %d bytes, must be 0",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
		}
		else if ( ! err) {
			if (payload.length != 4) {
				logger.error(String.format("Payload size is %d bytes, must be 4",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
		}
	}
	
	/**
	 * Construct a new MB_GetCommEventCounter message.
	 */
	public MB_GetCommEventCounter()
	{
		super(FC, false, false);
	}
	
	/**
	 * Construct a new MB_GetCommEventCounter acknowledgment.
	 * 
	 * @param status The status value.
	 * @param count The event count.
	 */
	public MB_GetCommEventCounter(short status, short count)
	{
		super(FC, true, false, status, count);
	}
	
	/**
	 * Construct a new MB_GetCommEventCounter error acknowledgment.
	 * 
	 * @param code The error code to return.
	 */
	public MB_GetCommEventCounter(byte code)
	{
		super(FC, true, true, code);
	}
	
	/** Get the status value. Acknowledgments only. */
	public short getStatus()
	{
		return getShort(0, true, false, "Status");
	}
	
	/** Get the event count. Acknowledgments only. */
	public short getEventCount()
	{
		return getShort(2, true, false, "Event Count");
	}
	
	/**
	 * Build an acknowledgment to this message. Non-acknowledgments only.
	 * 
	 * @param status The status value to return.
	 * @param count The event count to return.
	 */
	public MB_GetCommEventCounter acknowledge(short status, short count)
	{
		MB_GetCommEventCounter msg = new MB_GetCommEventCounter(status, count);
		msg.setup(getSequence(), getStation());
		msg.setProtocol(getProtocol());
		
		return msg;
	}
	
	@Override
	public String getErrorMessage()
	{
		if ( ! isError()) {
			return null;
		}
		
		return getErrorString(getErrorCode());
	}
	
	/**
	 * Translate an error code into a descriptive string.
	 * 
	 * @param code The error code.
	 * @return The descriptive string.
	 */
	public static String getErrorString(byte code)
	{
		switch (code) {
			case ERR_FUNCTION:
				return "Unsupported function code";
			case ERR_IO:
				return "Error retrieving event counter";
			default:
				return "(Unknown)";
		}
	}
	
	/**
	 * Get a descriptive string describing this acknowledgment's status value.
	 * 
	 * @return The status value.
	 * @throws IllegalStateException If this is not an acknowledgment.
	 */
	public String getStatusString()
	{
		return getStatusString(getStatus());
	}
	
	/**
	 * Translate a status value into a descriptive string.
	 * 
	 * @param status The status value code.
	 * @return The descriptive string.
	 */
	public static String getStatusString(short status)
	{
		switch (status) {
			case VAL_NOTBSY:
				return "Not Busy";
			case VAL_BSY:
				return "Busy";
			default:
				return "(Unknown)";
		}
	}
	
	@Override
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		byte[] payload = getPayloadDirect();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (ack) {
				if (err) {
					if (i == 0) {
						ans.append(String.format(" Error Code = %s",
						                         getErrorString(payload[0])));
					}
				}
				else {
					if (i == 0) {
						ans.append(String.format(" Status = 0x%04x (%s)",
						                         getStatus() & 0xffff,
						                         getStatusString()));
					}
					else if (i == 2) {
						ans.append(String.format(" Event Count = %d",
						                         getEventCount() & 0xffff));
					}
				}
			}
			else {
				// No data in the request
			}
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	@Override
	public String describePayload(ModbusMessage ackMsg, String indent)
	{
		if (this.ack) {
			throw new IllegalStateException("Cannot ACK an ACK, nor describe such a construct");
		}
		else if ( ! (ackMsg instanceof MB_GetCommEventCounter)) {
			throw new IllegalArgumentException("ACK not an MB_GetCommEventCounter message");
		}
		
		// Description is the same whether we know the request or not
		return ackMsg.describePayload(indent);
	}
}

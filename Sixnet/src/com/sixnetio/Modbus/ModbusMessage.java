/*
 * ModbusMessage.java
 *
 * The superclass for all Modbus messages.
 *
 * Jonathan Pearson
 * May 6, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.*;
import java.lang.reflect.*;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sixnetio.Station.IOMessage;
import com.sixnetio.util.Utils;

/**
 * The superclass for all Modbus messages. Instances of this class are
 * <b>not</b> thread-safe.
 *
 * @author Jonathan Pearson
 */
public class ModbusMessage
	implements IOMessage
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** The Modbus broadcast address (station number). */
	public static final byte ADDR_BROADCAST = 0x00;
	
	/** The maximum (inclusive) valid station number. */
	public static final byte ADDR_MAX = (byte)0xf7;
	
	/** The protocol field value that means 'Modbus'. */
	public static final short PROT_MODBUS = 0;
	
	/** If this is ORed with the function code, the response is an error. */
	public static final byte ERR_MASK = (byte)0x80;
	
	
	static {
		// Guarantee that this exists before the subsequent calls try to modify
		// it
		msgTypes = new HashMap<Byte, Class<? extends ModbusMessage>>();
		
		// Register the functions that are included in this package
		registerMessageType(MB_DIRead.FC, MB_DIRead.class);
		registerMessageType(MB_DORead.FC, MB_DORead.class);
		registerMessageType(MB_DOWrite.FC, MB_DOWrite.class);
		registerMessageType(MB_MultiDOWrite.FC, MB_MultiDOWrite.class);
		
		registerMessageType(MB_AIRead.FC, MB_AIRead.class);
		registerMessageType(MB_AORead.FC, MB_AORead.class);
		registerMessageType(MB_AOWrite.FC, MB_AOWrite.class);
		registerMessageType(MB_MultiAOWrite.FC, MB_MultiAOWrite.class);
		
		registerMessageType(MB_ReadException.FC, MB_ReadException.class);
		registerMessageType(MB_Diagnostics.FC, MB_Diagnostics.class);
		registerMessageType(MB_GetCommEventCounter.FC,
		                    MB_GetCommEventCounter.class);
		registerMessageType(MB_GetCommEventLog.FC, MB_GetCommEventLog.class);
		registerMessageType(MB_ReportSlaveID.FC, MB_ReportSlaveID.class);
	}
	
	/** The map of known function codes. */
	private static final Map<Byte, Class<? extends ModbusMessage>> msgTypes;
	
	/**
	 * Get the standard ModbusMessage constructor for the given subclass.
	 * 
	 * @param clazz The subclass's Class.
	 * @return The constructor (it is required for all registered classes).
	 * @throws NoSuchMethodException If the constructor cannot be found.
	 */
	private static Constructor<? extends ModbusMessage> findSubclassConstructor(Class<? extends ModbusMessage> clazz)
		throws NoSuchMethodException
	{
		return clazz.getConstructor(boolean.class, short.class, short.class,
		                            byte.class, byte.class, byte[].class);
	}
	
	/**
	 * Register a message type handler class. The class <i>must</i> have a
	 * constructor that looks like this:
	 * <code>public MessageType(boolean ack, short sequence, short protocol,
	 * byte station, byte function, byte[] payload)</code>. The constructor may
	 * throw an {@link IOException}. The class <i>may</i> define a static
	 * accessor that looks like this: <code>public static String
	 * getFunctionName()</code>, which returns the name of the Modbus function
	 * implemented by the class. If not defined, the class name will be used in
	 * {@link #toString()} and related methods.
	 * 
	 * @param fc The function code to register.
	 * @param msgType The message handler class to associate with that function
	 *   code.
	 */
	public static void registerMessageType(byte fc,
	                                       Class<? extends ModbusMessage> msgType)
	{
		synchronized (msgTypes) {
			if (msgTypes.containsKey(fc)) {
				throw new IllegalArgumentException(String.format("Function code 0x%02x already taken by class %s",
				                                                 fc,
				                                                 msgTypes.get(fc).getClass().getName()));
			}
			
			// Make sure it has an appropriate constructor
			Constructor<? extends ModbusMessage> constructor;
			try {
				constructor = findSubclassConstructor(msgType);
			}
			catch (NoSuchMethodException nsme) {
				throw new IllegalArgumentException(String.format("Class %s does not provide a proper ModbusMessage constructor",
				                                                 msgType.getName()),
				                                   nsme);
			}
			
			msgTypes.put(fc, msgType);
		}
	}
	
	/**
	 * Get the message handler class associated with a given function code.
	 * 
	 * @param fc The function code.
	 * @return The message handler class registered for that function code, or
	 *   <code>null</code> if that function code is not associated with a
	 *   handler class.
	 */
	public static Class<? extends ModbusMessage> getMessageType(byte fc)
	{
		synchronized (msgTypes) {
			return msgTypes.get((byte)(fc & ~ERR_MASK));
		}
	}
	
	/**
	 * Get a map containing all registered message handler classes.
	 * 
	 * @return A map of the function code onto the message handler class with
	 *   which it is associated.
	 */
	public static Map<Byte, Class<? extends ModbusMessage>> getMessageTypes()
	{
		synchronized (msgTypes) {
			return new HashMap<Byte, Class<? extends ModbusMessage>>(msgTypes);
		}
	}
	
	/**
	 * Remove a message handler class from the set of registered handlers,
	 * perhaps in preparation for registering a different handler.
	 * 
	 * @param fc The function code to unregister.
	 */
	public static void unregisterMessageType(byte fc)
	{
		synchronized (msgTypes) {
			msgTypes.remove(fc);
		}
	}
	
	/**
	 * Constructs a new instance of a registered message handler subclass.
	 * 
	 * @param ack Whether to treat the message as an acknowledgment.
	 * @param sequence The sequence number of the message.
	 * @param protocol The protocol identifier of the message.
	 * @param station The station number of the message.
	 * @param function The function code of the message.
	 * @param payload The remaining bytes in the message, after the function
	 *   code.
	 * @return A new ModbusMessage subclass representing the message. If no
	 *   registered subclass exists, a generic ModbusMessage will be returned.
	 * @throws IOException If there is a problem parsing the message.
	 */
	private static ModbusMessage buildSubclass(boolean ack,
	                                           Short sequence,
	                                           Short protocol,
	                                           Byte station,
	                                           Byte function,
	                                           byte[] payload)
		throws IOException
	{
		Class<? extends ModbusMessage> clazz = getMessageType(function);
		
		if (clazz == null) {
			return new ModbusMessage(sequence, protocol, station, function,
			                         payload);
		}
		
		// Find the constructor
		Constructor<? extends ModbusMessage> constructor;
		try {
			constructor = findSubclassConstructor(clazz);
		}
		catch (NoSuchMethodException nsme) {
			// This should not happen, we checked for it when the subclass was
			// registered
			throw new IllegalStateException(String.format("Class %s does not have a proper ModbusMessage constructor",
			                                              clazz.getName()),
			                                nsme);
		}
		
		try {
			return constructor.newInstance(ack, sequence, protocol, station,
			                               function, payload);
		}
		catch (IllegalAccessException iae) {
			// Also tested above
			throw new IllegalStateException(String.format("Class %s does not have a proper ModbusMessage constructor",
			                                              clazz.getName()),
			                                iae);
		}
		catch (InstantiationException ie) {
			// Was not tested above; means the class is likely abstract or
			// something
			throw new IllegalStateException(String.format("Class %s does not have a proper ModbusMessage constructor",
			                                              clazz.getName()),
			                                ie);
		}
		catch (InvocationTargetException ite) {
			if (ite.getCause() instanceof IOException) {
				throw (IOException)ite.getCause();
			}
			else {
				throw new IOException("Unexpected error parsing message: " +
				                      ite.getCause().getMessage(),
				                      ite.getCause());
			}
		}
	}
	
	/**
	 * The 'Transaction Identifier' field, unsigned; default to
	 * <code>null</code>.
	 */
	private Short sequence;
	
	/**
	 * The 'Protocol Identifier' field, unsigned; defaults to
	 * {@link #PROT_MODBUS}.
	 */
	private Short protocol = PROT_MODBUS;
	
	/** The 'Unit Identifier' field, unsigned; default to <code>null</code>. */
	private Byte station;
	
	/** The function code, unsigned; default to <code>null</code>. */
	private Byte function;
	
	/**
	 * The contents of the message beyond the header. Subclasses determine how
	 * this data is parsed. Default to <code>null</code>.
	 */
	private byte[] payload;
	
	/**
	 * Construct an (almost) uninitialized ModbusMessage object. Most fields
	 * will default to <code>null</code>. The {@link #protocol} field will
	 * default to {@link #PROT_MODBUS}.
	 * 
	 * @param fc The value for the {@link #function} field.
	 */
	protected ModbusMessage(Byte fc)
	{
		function = fc;
		
		// Leave everything else at its default
	}
	
	/**
	 * Construct a new ModbusMessage.
	 * 
	 * @param sequence The sequence number (transaction identifier) of the
	 *   message.
	 * @param protocol The protocol identifier of the message.
	 * @param station The station number (unit identifier) of the message.
	 * @param function The function code of the message.
	 * @param payload The payload of the message (bytes following the function
	 *   code).
	 */
	protected ModbusMessage(Short sequence, Short protocol, Byte station,
	                        Byte function, byte[] payload)
	{
		this.sequence = sequence;
		this.protocol = protocol;
		this.station = station;
		this.function = function;
		
		if (payload != null) {
			this.payload = new byte[payload.length];
			System.arraycopy(payload, 0, this.payload, 0, payload.length);
		}
	}
	
	/**
	 * Construct a copy of this message, but as the ModbusMessage superclass.
	 * This allows for more intricate tweaking of the message contents, but at
	 * the cost of not having helpful functions for doing the standard
	 * operations.
	 */
	public ModbusMessage getGeneric()
	{
		return new ModbusMessage(getSequence(), getProtocol(), getStation(),
		                         getFunction(), getPayloadDirect());
	}
	
	/**
	 * A quick setter for the main properties.
	 * 
	 * @param sequence The sequence number.
	 * @param station The station number.
	 */
	public void setup(Short sequence, Byte station)
	{
		this.sequence = sequence;
		this.station = station;
	}
	
	/**
	 * Construct a Modbus message from a byte array.
	 * 
	 * @param ack Whether to treat the received message as an acknowledgment.
	 * @param data The data to parse.
	 * @return A ModbusMessage representing the data from the array.
	 * @throws IOException If there is an error parsing the message.
	 */
	public static ModbusMessage parse(boolean ack, byte[] data)
		throws IOException
	{
		if (data.length < 8) {
			logger.error(String.format("Message length %d, needs to be at least %d",
			                           data.length, 8));
			throw new IOException("Modbus message too short");
		}
		
		return parse(ack, new ByteArrayInputStream(data));
	}
	
	/**
	 * Construct a Modbus message from an input stream.
	 * 
	 * @param ack Whether to treat the received message as an acknowledgment.
	 * @param in The stream to read from.
	 * @return A ModbusMessage representing the data read from the stream.
	 * @throws IOException If there is an error reading from the stream or
	 *   parsing its data. There is a good chance that the stream will not be
	 *   synchronized in this case, and should be closed to prevent further
	 *   errors.
	 */
	public static ModbusMessage parse(boolean ack, InputStream in)
		throws IOException
	{
		DataInputStream din;
		if (in instanceof DataInputStream) {
			din = (DataInputStream)in;
		}
		else {
			din = new DataInputStream(in);
		}
		
		short sequence = din.readShort();
		short protocol = din.readShort();
		if (protocol != 0) {
			logger.error(String.format("Protocol should be 0: %d", protocol));
			throw new IOException("Modbus message protocol not recognized");
		}
		
		short length = din.readShort();
		byte station = din.readByte();
		byte function = din.readByte();
		
		int len = length & 0xffff; // Length is unsigned, but 'short' isn't
		byte[] payload = new byte[len - 2]; // Length includes station/function bytes
		din.readFully(payload);
		
		return buildSubclass(ack, sequence, protocol, station, function,
		                     payload);
	}
	
	/**
	 * Get the sequence number (transaction identifier) of the message.
	 */
	public Short getSequence()
	{
		return sequence;
	}
	
	/**
	 * Set the sequence number (transaction identifier) of the message.
	 */
	public void setSequence(Short sequence)
	{
		this.sequence = sequence;
	}
	
	/**
	 * Get the protocol identifier of the message. Should always be 0 for
	 * Modbus.
	 */
	public Short getProtocol()
	{
		return protocol;
	}
	
	/**
	 * Set the protocol identifier of the message. Note that Modbus always
	 * expects to see a 0 here.
	 */
	public void setProtocol(Short protocol)
	{
		this.protocol = protocol;
	}
	
	/**
	 * Get the value of the Length field in the header. See
	 * {@link #getMessageLength()} to get the length of the actual message.
	 */
	public short getLengthField()
	{
		return (short)((payload.length + 2) & 0xffff);
	}
	
	/**
	 * Get the station number (unit identifier) of the message.
	 */
	public Byte getStation()
	{
		return station;
	}
	
	/**
	 * Set the station number (unit identifier) of the message. Note that Modbus
	 * expects this to be in the range [1, 247].
	 */
	public void setStation(Byte station)
	{
		this.station = station;
	}
	
	/**
	 * Get the function code of the message.
	 */
	public Byte getFunction()
	{
		return function;
	}
	
	/**
	 * Set the function code of the message. Subclasses should refuse to allow
	 * the change; instead, call {@link #getGeneric()} to get a generic copy,
	 * and then set the function code on that. 
	 */
	public void setFunction(Byte function)
	{
		this.function = function;
	}
	
	/**
	 * Check whether this message is an acknowledgment. Intended to be
	 * overridden by subclasses.
	 * 
	 * @return True if this is an acknowledgment, false if not,
	 *   <code>null</code> if unknown or not applicable.
	 */
	public Boolean isAcknowledgment()
	{
		return null;
	}
	
	/**
	 * Check whether this acknowledgment message is an error response. Intended
	 * to be overridden by subclasses.
	 * 
	 * @return True if this is an error response, false if not,
	 *   <code>null</code> if unknown or not applicable.
	 */
	public Boolean isError()
	{
		return null;
	}
	
	/**
	 * Get a message describing the error in this message, if it is an error
	 * message.
	 * 
	 * @return A descriptive error, or <code>null</code> if this is not an
	 *   error message.
	 */
	public String getErrorMessage()
	{
		return null;
	}
	
	/**
	 * Get the payload bytes of the message. This returns a copy.
	 */
	public byte[] getPayload()
	{
		byte[] payload = new byte[this.payload.length];
		System.arraycopy(this.payload, 0, payload, 0, payload.length);
		
		return payload;
	}
	
	/**
	 * Set the payload bytes of the message. This copies the parameter.
	 * Subclasses should refuse to allow the change; instead, either use the
	 * accessors of the subclass or call {@link #getGeneric()} to get a generic
	 * copy, and then set the payload bytes on that.
	 */
	public void setPayload(byte[] payload)
	{
		if (payload == null) {
			this.payload = null;
		}
		else {
			if (this.payload.length != payload.length) {
				this.payload = new byte[payload.length];
			}
			
			System.arraycopy(payload, 0, this.payload, 0, payload.length);
		}
	}
	
	/**
	 * Get the actual payload field, allowing reads/writes without any array
	 * copying.
	 */
	protected byte[] getPayloadDirect()
	{
		return payload;
	}
	
	/**
	 * Set the actual payload field, avoiding an array copy.
	 */
	protected void setPayloadDirect(byte[] payload)
	{
		this.payload = payload;
	}
	
	/**
	 * Get the full length of this message. See {@link #getLengthField()} to get
	 * the length field in the message header.
	 */
	public int getMessageLength()
	{
		return (payload.length + 8);
	}
	
	@Override
	public byte[] getBytes()
	{
		ByteArrayOutputStream bout = new ByteArrayOutputStream(getMessageLength());
		
		try {
			toStream(bout);
		}
		catch (IOException ioe) {
			logger.error("Unexpected IOException writing to a byte array", ioe);
			throw new RuntimeException("Unexpected IOException writing to a byte array: " +
			                           ioe.getMessage(), ioe);
		}
		
		return bout.toByteArray();
	}
	
	@Override
	public void toStream(OutputStream out)
		throws IOException
	{
		Short sequence = getSequence();
		Short protocol = getProtocol();
		Byte station = getStation();
		Byte function = getFunction();
		byte[] payload = getPayload();
		
		if (sequence == null) {
			throw new IllegalStateException("Sequence not set");
		}
		
		if (protocol == null) {
			throw new IllegalStateException("Protocol not set");
		}
		
		if (station == null) {
			throw new IllegalStateException("Station not set");
		}
		
		if (function == null) {
			throw new IllegalStateException("Function not set");
		}
		
		if (payload == null) {
			throw new IllegalStateException("Payload not set");
		}
		
		DataOutputStream dout;
		if (out instanceof DataOutputStream) {
			dout = (DataOutputStream)out;
		}
		else {
			dout = new DataOutputStream(out);
		}
		
		dout.writeShort(sequence);
		dout.writeShort(protocol);
		dout.writeShort(getLengthField());
		dout.writeByte(station);
		dout.writeByte(function);
		dout.write(payload);
		dout.flush();
	}
	
	/**
	 * Describe the data payload of this message with no indentation.
	 */
	public String describePayload()
	{
		return describePayload(0);
	}
	
	/**
	 * Describe the data payload of this message with some spaces for
	 * indentation. Calls {@link #describePayload(String)}, passing 'indent'
	 * spaces.
	 * 
	 * @param indent The number of spaces to use to indent.
	 */
	public String describePayload(int indent)
	{
		String indentString = Utils.multiplyChar(' ', indent);
		return describePayload(indentString);
	}
	
	/**
	 * Describe the data payload of the given acknowledgment with no
	 * indentation.
	 * 
	 * @param ack The acknowledgment to describe.
	 * @return A description of the data payload of the acknowledgment.
	 */
	public String describePayload(ModbusMessage ack)
	{
		return describePayload(ack, 0);
	}
	
	/**
	 * Describe the data payload of the given acknowledgment with some
	 * indentation spaces.
	 * 
	 * @param ack The acknowledgment to describe.
	 * @param indent The number of spaces to use for indentation.
	 * @return A description of the payload of the acknowledgment with
	 *   indentation.
	 */
	public String describePayload(ModbusMessage ack, int indent)
	{
		String indentString = Utils.multiplyChar(' ', indent);
		return describePayload(ack, indentString);
	}
	
	/**
	 * Describe the data payload of this message with the given indentation
	 * string.
	 * 
	 * @param indent The indentation string.
	 * @return A description of the payload of this message.
	 */
	public String describePayload(String indent)
	{
		StringBuilder ans = new StringBuilder();
		
		for (int i = 0; i < payload.length; i++) {
			ans.append(String.format("%sData(%3d):  %s",
			                         indent, i, describeByte(payload[i])));
			
			if (i < payload.length - 1) {
				ans.append("\n");
			}
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe the data payload of the given acknowledgment using the given
	 * indentation string.
	 * 
	 * @param ack The acknowledgment to describe.
	 * @param indent The indentation string.
	 * @return An indented description of the payload of the acknowledgment.
	 */
	public String describePayload(ModbusMessage ack, String indent)
	{
		return ack.toString(indent);
	}
	
	// Public static helper functions
	// These are some helper functions, mainly for subclasses
	/**
	 * Describe a given byte.
	 * 
	 * @param b The byte to describe.
	 * @return A description of the byte in the format
	 *   '0xhh   (  ddd) [bbbb bbbb]' where 'h' is a hex digit, 'd' is a decimal
	 *    digit, and 'b' is a 0 or 1.
	 */
	public static String describeByte(byte b)
	{
		StringBuilder ans = new StringBuilder();
		
		ans.append(String.format("0x%1$02x   (%1$5d) [", b & 0xFF));
		
		for (int bit = 7; bit >= 0; bit--) {
			if (bit == 3) ans.append(' ');
			
			if ((b & (1 << bit)) == 0) {
				ans.append('0');
			} else {
				ans.append('1');
			}
		}
		
		ans.append(']');
		
		return ans.toString();
	}
	
	/**
	 * Produce a string representation of this message with no indentation.
	 */
	@Override
	public String toString()
	{
		return toString(0);
	}
	
	/**
	 * Produce a string representation of this message with the given number of
	 *   indentation spaces.
	 * 
	 * @param indent The number of spaces to prepend to every line of the
	 *   output.
	 * @return An indented string representation of this message.
	 */
	public String toString(int indent)
	{
		String indentString = Utils.multiplyChar(' ', indent);
		return toString(indentString);
	}
	
	/**
	 * Produce a string representation of this message with the given string
	 *   indentation.
	 * 
	 * @param indent A string to prepend to every line of the output.
	 * @return An indented string representation of this message.
	 */
	public String toString(String indent)
	{
		Short sequence = getSequence();
		Short protocol = getProtocol();
		Byte station = getStation();
		Byte function = getFunction();
		byte[] payload = getPayload();
		
		StringBuilder ans = new StringBuilder();
		
		if (sequence != null) {
			ans.append(String.format("%1$sSequence: 0x%2$04x   (%2$5d)\n",
			                         indent, sequence & 0xffff));
		}
		else {
			ans.append(String.format("%1$sSequence:          (Unset)\n",
			                         indent));
		}
		
		if (protocol != null) {
			ans.append(String.format("%1$sProtocol: 0x%2$04x   (%2$5d) %3$s\n",
			                         indent, protocol & 0xffff,
			                         getProtocolString(protocol)));
		}
		else {
			ans.append(String.format("%1$sProtocol:          (Unset)\n",
			                         indent));
		}
		
		ans.append(String.format("%1$sLength:   0x%2$04x   (%2$5d)\n",
		                         indent, getLengthField() & 0xffff));
		
		if (station != null) {
			ans.append(String.format("%1$sStation:    0x%2$02x   (  %2$3d)\n",
			                         indent, station & 0xff));
		}
		else {
			ans.append(String.format("%1$sStation:         (Unset)\n", indent));
		}
		
		if (function != null) {
			ans.append(String.format("%1$sFunction:   0x%2$02x   (  %2$3d) %3$s\n",
			                         indent, function & 0xff,
			                         getFunctionString(function)));
		}
		else {
			ans.append(String.format("%1$sFunction:        (Unset)\n", indent));
		}
		
		if (payload == null) {
			ans.append(String.format("%1$sData:               (Unset)\n",
			                         indent));
		}
		else if (payload.length == 0) {
			ans.append(String.format("%1$sData:               ( None)\n",
			                         indent));
		}
		else {
			ans.append(describePayload(indent));
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe an acknowledgment to this message.
	 * 
	 * @param ack The acknowledgment to describe.
	 * @return The description of the acknowledgment with no indentation.
	 */
	public String toString(ModbusMessage ack)
	{
		return toString(ack, 0);
	}
	
	/**
	 * Describe an acknowledgment to this message with the given amount of
	 * indentation.
	 * 
	 * @param ack The acknowledgment to describe.
	 * @param indent The number of spaces to prepend to each line of the output.
	 * @return The description of the acknowledgment.
	 */
	public String toString(ModbusMessage ack, int indent)
	{
		String indentString = Utils.multiplyChar(' ', indent);
		return toString(ack, indentString);
	}
	
	/**
	 * Describe an acknowledgment to this message with a given indentation
	 * string.
	 * 
	 * @param ack The acknowledgment to describe.
	 * @param indent The indentation string to prepend to each line of the
	 *   output.
	 * @return The description of the acknowledgment.
	 */
	public String toString(ModbusMessage ack, String indent)
	{
		Short sequence = ack.getSequence();
		Short protocol = ack.getProtocol();
		Byte station = ack.getStation();
		Byte function = ack.getFunction();
		byte[] payload = ack.getPayload();
		
		Byte myFunction = getFunction();
		
		StringBuilder ans = new StringBuilder();
		
		if (myFunction != null) {
			ans.append(String.format("%sAcknowledgment to %s\n",
			                         indent, getFunctionString(myFunction)));
		}
		else {
			ans.append(String.format("%sAcklowledgment to (Unset)\n", indent));
		}
		
		if (sequence != null) {
			ans.append(String.format("%1$sSequence: 0x%2$04x   (%2$5d)\n",
			                         indent, sequence & 0xffff));
		}
		else {
			ans.append(String.format("%1$sSequence:          (Unset)\n",
			                         indent));
		}
		
		if (protocol != null) {
			ans.append(String.format("%1$sProtocol: 0x%2$04x   (%2$5d) %3$s\n",
			                         indent, protocol & 0xffff,
			                         getProtocolString(protocol)));
		}
		else {
			ans.append(String.format("%1$sProtocol:          (Unset)\n",
			                         indent));
		}
		
		ans.append(String.format("%1$sLength:   0x%2$04x   (%2$5d)\n",
		                         indent, getLengthField() & 0xffff));
		
		if (station != null) {
			ans.append(String.format("%1$sStation:    0x%2$02x   (  %2$3d)\n",
			                         indent, station & 0xff));
		}
		else {
			ans.append(String.format("%1$sStation:         (Unset)\n", indent));
		}
		
		if (function != null) {
			ans.append(String.format("%1$sFunction:   0x%2$02x   (  %2$3d) %3$s\n",
			                         indent, function & 0xff,
			                         getFunctionString(function)));
		}
		else {
			ans.append(String.format("%1$sFunction:        (Unset)\n", indent));
		}
		
		if (payload == null) {
			ans.append(String.format("%1$sData:               (Unset)\n",
			                         indent));
		}
		else if (payload.length == 0) {
			ans.append(String.format("%1$sData:               ( None)\n",
			                         indent));
		}
		else {
			ans.append(describePayload(indent));
		}
		
		if (payload == null) {
			ans.append(String.format("%1$sData:               (Unset)\n",
			                         indent));
		}
		else if (payload.length == 0) {
			ans.append(String.format("%1$sData:               ( None)\n",
			                         indent));
		}
		else {
			ans.append(describePayload(indent));
		}
		
		return ans.toString();
	}
	
	/**
	 * Translate the 'protocol' field into a descriptive string.
	 * 
	 * @param protocol The protocol field value.
	 * @return The name of the protocol.
	 */
	public static String getProtocolString(short protocol)
	{
		switch (protocol) {
			case 0:
				return "Modbus";
			default:
				return "(Unknown)";
		}
	}
	
	/**
	 * Translate a function code into a descriptive string.
	 * 
	 * @param function The function code.
	 * @return If a handler is registered that provides the necessary accessor
	 *   function, the result of that function call; if the handler does not
	 *   provide the necessary accessor function, the name of the handler class;
	 *   if no handler is registered, the string '(Unknown)'.
	 */
	public static String getFunctionString(byte function)
	{
		
		Class<? extends ModbusMessage> clazz = getMessageType(function);
		
		if (clazz == null) {
			logger.debug(String.format("No class registered for function code 0x%02x",
			                           function));
			return "(Unknown)";
		}
		
		String name = clazz.getSimpleName();
		
		Method m = null;
		try {
			m = clazz.getDeclaredMethod("getFunctionName");
		}
		catch (NoSuchMethodException nsme) {
			logger.info(String.format("Class '%s' does not provide getFunctionName()",
			                          clazz.getName()), nsme);
		}
		
		if (m != null) {
			try {
				name = m.invoke(null).toString();
			}
			catch (InvocationTargetException ite) {
				logger.warn(String.format("%s.getFunctionName() threw an exception",
				                          clazz.getName()),
				                          ite.getCause());
			}
			catch (IllegalArgumentException iae) {
				logger.warn(String.format("%s.getFunctionName() is not static, or something weird happened",
				                          clazz.getName()),
				                          iae);
			}
			catch (IllegalAccessException iae) {
				logger.warn(String.format("%s.getFunctionName() has incorrect permissions",
				                          clazz.getName()),
				                          iae);
			}
		}
		
		return name;
	}
}

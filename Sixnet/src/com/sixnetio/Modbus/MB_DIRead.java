/*
 * MB_DIRead.java
 *
 * Implements the Modbus 'Read Discrete Inputs' message.
 *
 * Jonathan Pearson
 * May 7, 2010
 *
 */

package com.sixnetio.Modbus;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

/**
 * Implements the Modbus 'Read Discrete Inputs' message, which reads discrete
 * input registers.
 *
 * @author Jonathan Pearson
 */
public class MB_DIRead
	extends MB_DiscreteRead
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Function code for 'Read Discrete Inputs' (this message). */
	public static final byte FC = 0x02;
	
	/**
	 * Get the name of the function that this class implements.
	 */
	public static String getFunctionName()
	{
		return "Read Discrete Inputs";
	}
	
	/**
	 * Construct a new MB_DIRead message.
	 * 
	 * @param ack Whether this message is an acknowledgment.
	 * @param sequence The sequence number.
	 * @param protocol The protocol.
	 * @param station The station number.
	 * @param function The function code (must be {@link #FC}).
	 * @param payload The payload of the message.
	 * @throws IOException If there was a problem parsing the payload.
	 */
	public MB_DIRead(boolean ack, short sequence, short protocol,
	                 byte station, byte function, byte[] payload)
		throws IOException
	{
		super(FC, ack, sequence, protocol, station, function, payload);
		
		if (ack && ! err) {
			if (payload.length < 1) {
				logger.error(String.format("Payload size is %d bytes, must be at least 1",
				                           payload.length));
				throw new IOException("Payload is the wrong size");
			}
			
			// Number of bytes after this field
			int bytes = payload[0] & 0xff;
			if (payload.length != bytes + 1) {
				logger.error(String.format("Payload size is %d bytes, must be %d",
				                           payload.length, bytes + 1));
				throw new IOException("Payload is the wrong size");
			}
		}
	}
	
	/**
	 * Construct a new MB_DIRead message.
	 * 
	 * @param start The starting register number to read (unsigned).
	 * @param count The number of registers to read [1, 2000].
	 */
	public MB_DIRead(short start, short count)
	{
		super(FC, false, false, start, count);
		
		if (count < 1 || count > 2000) {
			logger.error("Register count must be in the range [1, 2000]: " +
			             count);
			throw new IllegalArgumentException("Bad register count");
		}
	}
	
	/**
	 * Construct an acknowledgment to an MB_DIRead message.
	 * 
	 * @param values The register values to return.
	 */
	public MB_DIRead(byte[] values)
	{
		super(FC, true, false, (byte)(values.length & 0xff), values);
		
		if (values.length > 250) {
			logger.error("Cannot respond with more than 250 bytes: " +
			             values.length);
			throw new IllegalArgumentException("Trying to send too many bytes");
		}
	}
	
	/**
	 * Build an acknowledgment to this message. Non-acknowledgments only. Note:
	 * this will allow you to return a different number of registers than was
	 * requested without any warning.
	 * 
	 * @param registers The register values to return. The first requested
	 *   register should be in the lowest-order bit of element 0.
	 * @return An acknowledgment to this message, ready to be sent.
	 */
	public MB_DIRead acknowledge(byte[] registers)
	{
		MB_DIRead msg = new MB_DIRead(registers);
		msg.setup(getSequence(), getStation());
		msg.setProtocol(getProtocol());
		
		return msg;
	}
}

/*
 * IOCmdLine.java
 *
 * Implements a simple command line for accessing and modifying an IORelay
 *
 * Jonathan Pearson
 * March 30, 2007
 *
 */

package com.sixnetio.Example;

import java.io.*;
import java.util.*;

public class IOCmdLine implements Runnable {
    public static void main(String[] args) throws IOException {
        new IOCmdLine(args);
    }
    
    private IORelay relay;
    
    public IOCmdLine(String[] args) throws IOException {
        IORelay.main(args);
        relay = IORelay.getLastRelay();
        
        Thread th = new Thread(this);
        th.setDaemon(false);
        th.start();
    }
    
    public void run() {
        BufferedReader in;
        PrintStream out;
        
        in = new BufferedReader(new InputStreamReader(System.in));
        out = System.out;

        while (true) {
            try {
                out.print("> ");
                String cmdLine = in.readLine();
                
                if (cmdLine == null) break; // EOF = end of program
                
                // Test against commands available
                StringTokenizer tok = new StringTokenizer(cmdLine);
                String cmd = tok.nextToken();
                if (cmd.equals("setd")) {
                    int index = Integer.parseInt(tok.nextToken()) - 1; // 0-based
                    boolean val = false;
                    String str = tok.nextToken();
                    
                    if (str.equalsIgnoreCase("on") ||
                        str.equalsIgnoreCase("true") ||
                        str.equalsIgnoreCase("t") ||
                        str.equalsIgnoreCase("1")) {
                        
                        val = true;
                    }
                    
                    relay.putDO(index, val);
                } else if (cmd.equals("setb")) {
                    int index = 0;
                    while (tok.hasMoreTokens()) {
                        relay.putBO(index++, (byte)(Integer.parseInt(tok.nextToken()) & 0xFF));
                    }
                } else if (cmd.equals("seta")) {
                    int index = 0;
                    while (tok.hasMoreTokens()) {
                        relay.putAO(index++, (short)(Integer.parseInt((tok.nextToken())) & 0xFFFF));
                    }
                } else if (cmd.equals("getd")) {
                    int index = Integer.parseInt(tok.nextToken()) - 1; // 0-based
                    boolean val = relay.getDO(index);
                    
                    if (val) {
                        out.println("On");
                    } else {
                        out.println("Off");
                    }
                } else if (cmd.equals("getb")) {
                    byte[] data = relay.getDiscretesOutCopy();
                    for (int i = 0; i < data.length; i++) {
                        out.print("" + (data[i] & 0xFF) + " ");
                    }
                    out.println();
                } else if (cmd.equals("geta")) {
                    short[] data = relay.getAnalogsOutCopy();
                    for (int i = 0; i < data.length; i++) {
                        out.print("" + (data[i] & 0xFFFF) + " ");
                    }
                    out.println();
                } else if (cmd.equals("exit") ||
                           cmd.equals("quit")) {
                    
                    System.exit(0);
                    return;
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }
}

                
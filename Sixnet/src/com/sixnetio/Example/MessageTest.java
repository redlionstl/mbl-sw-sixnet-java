/*
 * MessageTest.java
 *
 * Tests a few UDR messages by turning on/off I/O points on a module.
 * NOTE: This only tests UDP, it does not test serial communications or TCP.
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.Example;

import java.io.IOException;
import java.net.*;
import java.util.List;

import com.sixnetio.Station.*;
import com.sixnetio.UDR.*;

public class MessageTest implements MessageListener {
    public static final short MY_ID = 18;
    
    public static void main(String[] args) throws IOException {
        new MessageTest(args);
    }
    
    public MessageTest(String[] args) throws IOException {
        UDRLink handler;
        
        if (args[0].equalsIgnoreCase("com")) {
            String comPort = args[1];
            handler = new UDRLink(TransportMethod.Serial, comPort);
        } else if (args[0].equalsIgnoreCase("eth")) {
            if (args[1].equalsIgnoreCase("localhost")) {
                handler = null;
            } else {
                handler = new UDRLink(TransportMethod.UDP, args[1]);
            }
        } else if (args[0].equalsIgnoreCase("bc")) {
            InetAddress me = InetAddress.getByName(args[1]); // Address of interface to use
            NetworkInterface iface = NetworkInterface.getByInetAddress(me);
            
            // Use the first address of this interface for broadcasting
            List<InterfaceAddress> addresses = iface.getInterfaceAddresses();
            InterfaceAddress ifaddr = addresses.get(0);
            
            InetAddress bc = ifaddr.getBroadcast();
            
            handler = new UDRLink(TransportMethod.UDPBroadcast, bc.getHostAddress());
        } else {
            System.err.println("Usage: MessageTest {com|eth|bc} <address> <station> <value>");
            System.err.println("com:  <address> is the path to the com port");
            System.err.println("eth:  <address> is the address of the receiving station");
            System.err.println("<station> is the ID of the receiving station");
            System.err.println("<value> is the 2-byte (short) value to write to address 0");
            System.exit(1);
            return;
        }
        
        MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();
        if (handler != null) {
            dispatcher.registerHandler(handler);
        } else {
            dispatcher.registerHandler(new UDRLink(TransportMethod.UDPLoop, "localhost"));
        }
        
        dispatcher.registerListener(this);
        
        short id = Short.parseShort(args[2]);
        int val = Integer.parseInt(args[3]);
        
        byte[] data = new byte[1];
        data[0] = (byte)(val & 0xFF);
        
        // Build a new message to turn on I/O points 1, 3, and 5, and turn off 2, 4, and 6
        UDRMessage msg = new UDR_PutB(UDRMessage.T_D_DOUT, (short)0, (short)1, data);
        msg.setup(UDRMessage.F_FIXEDCRC, MY_ID, id, (byte)0, (byte)5);
        
        System.out.println("Sending:\n" + msg);
        dispatcher.send(msg);
        
        // This is just to keep the program from exiting due to only daemon threads running
        (new Thread() {
            @Override
            public void run() {
                while (true) {
                    Thread.yield();
                }
            }
        }).start();
    }
    
    public short getID() {
        return MY_ID;
    }
    
    public void handleMessage(MessageDispatcher.Message msg) {
        if (msg.msg instanceof UDR_Acknowledge) {
            System.out.println("Message acknowledged:\n" + msg.msg);
        } else if (msg.msg instanceof UDR_NAcknowledge) {
            System.out.println("Message acknowledge negatively:\n" + msg.msg);
        } else {
            System.out.println("Unexpected reply:\n" + msg.msg);
        }
        
        System.exit(0);
    }
}

            
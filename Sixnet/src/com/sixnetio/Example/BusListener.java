/*
 * BusListener.java
 *
 * An interface to allow a class to be alerted to GUI events which may change
 * bussing status between IOStationGUI objects.
 *
 * Jonathan Pearson
 * April 19, 2007
 *
 */

package com.sixnetio.Example;

public interface BusListener {
    public void busChanged(IOStationGUI station);
}

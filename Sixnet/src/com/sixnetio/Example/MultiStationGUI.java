/*
 * MultiStationGUI.java
 *
 * Similar to IOStationGUI, and uses that class, but displays
 * an arbitrary number of IOStations in one window, along with
 * labels for each one.
 *
 * Jonathan Pearson
 * April 17, 2007
 *
 */

package com.sixnetio.Example;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Vector;

import com.sixnetio.GUI.Suicide;
import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;

public class MultiStationGUI extends Frame implements BusListener {
    private static class LabeledStation {
        public Label lblName;
        public IOStationGUI station;
        
        public LabeledStation(String name, IOStationGUI station) {
            this.lblName = new Label(name + " (" + station.getID() + ")");
            this.station = station;
        }
    }

    public static void usage() {
        System.err.println("Usage: MultiStationGUI {com|eth} [<address>] [-station <name> <id> <discretesIn> <discretesOut> <analogsIn> <analogsOut>]...");
        System.err.println("com:  <address> is the path to the com port (required)");
        System.err.println("eth:  do not pass <address>");
        System.err.println("<name>         is the name of the station");
        System.err.println("<id>           is the ID of the station");
        System.err.println("<discretesIn>  is the number of discrete input registers");
        System.err.println("<discretesOut> is the number of discrete output registers");
        System.err.println("<analogsIn>    is the number of analog input registers");
        System.err.println("<analogsOut>   is the number of analog output registers");
    }
    
    public static void main(String[] args) throws IOException {
        // First, set up communication
        UDRLink handler = null;
        
        int argPos = 0;
        
        if (args[0].equalsIgnoreCase("com")) {
            String comPort = args[1];
            handler = new UDRLink(TransportMethod.Serial, comPort);
            
            argPos = 2;
        } else if (args[0].equalsIgnoreCase("eth")) {
            argPos = 1;
        } else {
            usage();
            System.exit(1);
            return;
        }
        
        MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();
        
        if (handler != null) {
            dispatcher.registerHandler(handler);
        } else {
            dispatcher.registerHandler(new UDRLink(TransportMethod.UDPLoop, "localhost"));
        }
        
        // Now loop through setting up stations
        int columns = 0;
        int rows = 0;
        Vector<LabeledStation> ioStations = new Vector<LabeledStation>();
        
        for (int i = argPos; i < args.length; i++) {
            if (args[i].equalsIgnoreCase("-rows")) {
                i++;
                rows = Integer.parseInt(args[i]);
            } else if (args[i].equalsIgnoreCase("-cols")) {
                i++;
                columns = Integer.parseInt(args[i]);
            } else if (args[i].equalsIgnoreCase("-station")) {
                i++;
                
                String name = args[i++];
                short myID = Short.parseShort(args[i++]);
                short discretesIn = Short.parseShort(args[i++]);
                short discretesOut = Short.parseShort(args[i++]);
                short analogsIn = Short.parseShort(args[i++]);
                short analogsOut = Short.parseShort(args[i]);
                
                ioStations.add(new LabeledStation(name, new IOStationGUI(false, dispatcher, myID, discretesIn, discretesOut, analogsIn, analogsOut)));
            } else {
                System.err.println("Unrecognized option: " + args[i]);
                usage();
                System.exit(1);
                return;
            }
        }
        
        new MultiStationGUI(ioStations, rows, columns);
    }
    
    public MultiStationGUI(Vector<LabeledStation> ioStations) {
        this(ioStations, 0, 0);
    }
    
    public MultiStationGUI(Vector<LabeledStation> ioStations, int rows, int columns) {
        if (columns == 0 && rows == 0) {
            columns = (int)Math.sqrt(ioStations.size() / 3.0);
            rows = ioStations.size() / columns;
            if (ioStations.size() % columns > 0) rows++;
        } else if (columns == 0) {
            columns = ioStations.size() / rows;
            if (ioStations.size() % rows > 0) columns++;
        } else if (rows == 0) {
            rows = ioStations.size() / columns;
            if (ioStations.size() % columns > 0) rows++;
        }
        
        setLayout(new GridLayout(rows, columns));
        
        Color separator = new Color(192, 192, 192);
        
        for (LabeledStation station : ioStations) {
            Panel p = new Panel();
            p.setLayout(new BorderLayout());
            
            {
                Panel a = new Panel();
                a.setLayout(new FlowLayout());
                a.setBackground(separator);
                a.add(station.lblName);
                station.lblName.setBackground(separator);
                
                p.add(a, BorderLayout.NORTH);
            }
            
            p.add(station.station.getMainPanel(), BorderLayout.CENTER);
            
            add(p);
            
            station.station.addBusListener(this);
        }
        
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                setVisible(false);
                new Suicide();
                dispose();
            }
        });
        
        setTitle("Multiple Stations");
        pack();
        setVisible(true);
    }
    
    private IOStationGUI lastBus;
    private short busIndex;
    private int busType;
    
    public void busChanged(IOStationGUI where) {
        if (lastBus == null) {
            lastBus = where;
            busIndex = where.busIndex;
            busType = where.busType;
        } else {
        	BussedConnection.addConnection(lastBus.getStation(), busType, busIndex,
                                           where.getStation(), where.busType, where.busIndex);
            
            lastBus = null;
            busType = 0;
            busIndex = 0;
        }
    }
}

/*
 * IORelay.java
 *
 * An implementation of Station which periodically relays our own output
 * registers to the specified station(s).
 *
 * Jonathan Pearson
 * March 30, 2007
 *
 */

package com.sixnetio.Example;

import java.io.IOException;
import java.net.*;
import java.util.List;

import com.sixnetio.Station.*;
import com.sixnetio.UDR.*;

public class IORelay extends Station implements Runnable {
    public static final int DELAY = 2000; // Send registers every 2 seconds
    
    private static IORelay lastRelay;
    
    public static IORelay getLastRelay() {
        return lastRelay;
    }
    
    public static void usage() {
        System.err.println("Usage: IORelay {com|eth|bc} <address> <myID> <otherID> <discretes> <analogs>");
        System.err.println("com:  <address> is the path to the com port");
        System.err.println("eth:  <address> is the address of the other station");
        System.err.println("<myID>      is the ID of this station");
        System.err.println("<otherID>   is the ID of the station to receive the relay");
        System.err.println("<discretes> is the number of discrete registers to forward");
        System.err.println("<analogs>   is the number of analog registers to forward");
    }
    
    public static void main(String[] args) throws IOException {
        UDRLink handler;
        boolean usingCom = false;
        
        if (args.length != 6) {
            usage();
            System.exit(1);
            return;
        }
        
        if (args[0].equalsIgnoreCase("com")) {
            String comPort = args[1];
            handler = new UDRLink(TransportMethod.Serial, comPort);
            usingCom = true;
        } else if (args[0].equalsIgnoreCase("eth")) {
            handler = new UDRLink(TransportMethod.UDP, args[1]);
        } else if (args[0].equalsIgnoreCase("bc")) {
            InetAddress me = InetAddress.getByName(args[1]); // Address of interface to use
            NetworkInterface iface = NetworkInterface.getByInetAddress(me);
            
            // Use the first address of this interface for broadcasting
            List<InterfaceAddress> addresses = iface.getInterfaceAddresses();
            InterfaceAddress ifaddr = addresses.get(0);
            
            InetAddress bc = ifaddr.getBroadcast();
            
            handler = new UDRLink(TransportMethod.UDPBroadcast, bc.getHostAddress());
        } else {
            usage();
            System.exit(1);
            return;
        }
        
        short myID = Short.parseShort(args[2]);
        short otherID = Short.parseShort(args[3]);
        short discretes = Short.parseShort(args[4]);
        short analogs = Short.parseShort(args[5]);
        
        // Don't listen over ethernet if we're only doing COM port communications
        MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();
        if ( ! usingCom) {
        	dispatcher.registerHandler(new UDRLink(TransportMethod.UDPLoop, "localhost"));
        }
        
        dispatcher.registerHandler(handler);
        
        lastRelay = new IORelay(dispatcher, myID, discretes, analogs, otherID);
    }
    
    private short targetID;
    
    public IORelay(MessageDispatcher dispatcher, short myID, short discretes, short analogs, short otherID) {
        super(dispatcher, myID, discretes, analogs);
        
        targetID = otherID;
        
        Thread th = new Thread(this);
        th.setDaemon(false);
        th.start();
    }
    
    public void run() {
        byte sequence = 1;
        
        while (getStatus() != S_HALT) {
            while (getStatus() == S_UP) {
                if (getDiscreteOutCount() > 0) {
                    UDR_PutB putb = new UDR_PutB(UDRMessage.T_D_DOUT, (short)0, getDiscreteOutCount(), getDiscretesOutCopy());
                    putb.setup(UDRMessage.F_FIXEDCRC, getID(), targetID, (byte)0, sequence++);
                    send(putb);
                }
                
                if (getAnalogOutCount() > 0) {
                    UDR_PutA puta = new UDR_PutA(UDRMessage.T_D_AOUT, (short)0, getAnalogOutCount(), getAnalogsOutCopy());
                    puta.setup(UDRMessage.F_FIXEDCRC, getID(), targetID, (byte)0, sequence++);
                    send(puta);
                }
                
                try {
                    Thread.sleep(DELAY);
                } catch (InterruptedException ie) {
                    Thread.yield(); // Just in case the sleep failed for some reason
                }
            }
        }
    }
}

/*
 * IOStationGUI.java
 *
 * An executable class that creates and runs a graphical version of an IO station.
 * Provide the numbers for discrete and analog inputs/outputs on the command line.
 *
 * Jonathan Pearson
 * April 16, 2007
 *
 */

package com.sixnetio.Example;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Vector;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.sixnetio.Station.*;
import com.sixnetio.UDR.UDRLink;

public class IOStationGUI extends Frame implements IOStationUI, MouseListener, ChangeListener {
    // Feel free to set these to anything except null, they're used for drawing discretes
    public Color Discrete_Background = Color.BLACK;
    public Color Discrete_Off = new Color(128, 0, 0);
    public Color Discrete_On = new Color(255, 0, 0);
    
    private static class ValueLabel extends Label {
        public Object value;
        
        public ValueLabel(String msg, Object val) {
            super(msg);
            
            value = val;
        }
    }
    
    private class DiscreteValue extends Canvas {
        // Default to off
        private boolean value = false;
        
        // These are for identifying which array we're in
        private int index;
        private boolean output;
        
        public DiscreteValue(int index, boolean output) {
            this.index = index;
            this.output = output;
        }
        
        @Override
        public void paint(Graphics g) {
        	if (getBufferStrategy() == null) {
            	createBufferStrategy(2);
            }
            
        	g = getBufferStrategy().getDrawGraphics();
        	
            // Black square with a red circle in the middle; dark for off, bright for on
            g.setColor(Discrete_Background);
            g.fillRect(0, 0, getWidth(), getHeight());
            
            if (value) {
                g.setColor(Discrete_On);
            } else {
                g.setColor(Discrete_Off);
            }
            g.fillOval(0, 0, Math.min(getWidth(), getHeight()), Math.min(getWidth(), getHeight()));
            
            getBufferStrategy().show();
        }
        
        @Override
        public void update(Graphics g) {
        	paint(g);
        }
        
        public boolean getValue() {
            return value;
        }
        
        public void setValue(boolean val) {
            value = val;
            repaint();
        }
        
        public int getIndex() {
            return index;
        }
        
        public boolean isOutput() {
            return output;
        }
    }
    
    private class AnalogValue extends JSpinner {
        private class NoChangeModel extends AbstractSpinnerModel {
            public Object getNextValue() {
                return getValue();
            }
            
            public Object getPreviousValue() {
                return getValue();
            }
            
            public Object getValue() {
                int val = (value & 0xFFFF);
                return Integer.valueOf(val);
            }
            
            public void setValue(Object val) {
                /*
                int v = Integer.parseInt(val.toString());
                if (value != (short)(v & 0xFFFF)) throw new IllegalArgumentException("Cannot change value");
                */
                value = (short)((Integer.parseInt(val.toString())) & 0xFFFF);
                fireStateChanged();
            }
        }
        
        private class SpecialNumberModel extends SpinnerNumberModel {
            SpecialNumberModel() {
                super(value, 0, 65535, 1);
            }
            
            @Override
            public Object getPreviousValue() {
                int v;
                
                if (output) {
                    v = (myStation.getStraightAO(index) & 0xFFFF) - 1;
                } else {
                    v = (myStation.getStraightAI(index) & 0xFFFF) - 1;
                }
                
                if (v <= -1) return Integer.valueOf(65535);
                else return Integer.valueOf(v);
            }
            
            @Override
            public Object getNextValue() {
                int v;
                
                if (output) {
                    v = (myStation.getStraightAO(index) & 0xFFFF) + 1;
                } else {
                    v = (myStation.getStraightAI(index) & 0xFFFF) + 1;
                }
                
                if (v >= 65536) return Integer.valueOf(0);
                else return Integer.valueOf(v);
            }
        }
        
        // Default to 0
        private short value = 0;
        
        // These are for identifying which array we're in
        private int index;
        private boolean output;
        
        public AnalogValue(int index, boolean output) {
            super();
            
            this.index = index;
            this.output = output;
            
            if (output) {
                setModel(new NoChangeModel());
            } else {
                SpecialNumberModel model = new SpecialNumberModel();
                setModel(model);
            }
        }
        
        public int getIndex() {
            return index;
        }
        
        public boolean isOutput() {
            return output;
        }
    }
    
    // If the user right-clicks, these are set to identify what was clicked on (for bussing)
    public short busIndex;
    public int busType;
    
    private Vector<BusListener> busListeners;
    
    private IOStation myStation;
    
    // Shortcut variables, so we don't need to go through IOStation to get the values
    // Use booleans for discretes so we don't need to fiddle with bits and bytes
    private DiscreteValue[] dIn;
    private DiscreteValue[] dOut;
    private AnalogValue[] aIn;
    private AnalogValue[] aOut;
    
    private Panel pnlMain;
    
    public static void main(String[] args) throws IOException {
        // First, set up communication
        UDRLink handler = null;
        
        int argPos = 0;
        
        if (args[0].equalsIgnoreCase("com")) {
            String comPort = args[1];
            handler = new UDRLink(TransportMethod.Serial, comPort);
            
            argPos = 2;
        } else if (args[0].equalsIgnoreCase("eth")) {
            argPos = 1;
        } else {
            usage();
            System.exit(1);
            return;
        }
        
        short myID = Short.parseShort(args[argPos++]);
        short discretesIn = Short.parseShort(args[argPos++]);
        short discretesOut = Short.parseShort(args[argPos++]);
        short analogsIn = Short.parseShort(args[argPos++]);
        short analogsOut = Short.parseShort(args[argPos++]);
        
        MessageDispatcher dispatcher = MessageDispatcher.getDispatcher();
        dispatcher.registerHandler(new UDRLink(TransportMethod.UDPLoop, "localhost"));
        
        if (handler != null) {
            dispatcher.registerHandler(handler);
        }
        
        new IOStationGUI(true, dispatcher, myID, discretesIn, discretesOut, analogsIn, analogsOut);
    }
    
    public static void usage() {
        System.err.println("Usage: IOStationGUI {com|eth} [<address>] <myID> <discretesIn> <discretesOut> <analogsIn> <analogsOut>");
        System.err.println("com:  <address> is the path to the com port (required)");
        System.err.println("eth:  do not pass <address>");
        System.err.println("<myID>         is the ID of this station");
        System.err.println("<discretesIn>  is the number of discrete input registers");
        System.err.println("<discretesOut> is the number of discrete output registers");
        System.err.println("<analogsIn>    is the number of analog input registers");
        System.err.println("<analogsOut>   is the number of analog output registers");
    }
    
    public IOStationGUI(boolean ownWindow, MessageDispatcher dispatcher, short myID,
                        short discretesIn, short discretesOut,
                        short analogsIn, short analogsOut) {
        
        busListeners = new Vector<BusListener>();
        
        dIn = new DiscreteValue[discretesIn];
        dOut = new DiscreteValue[discretesOut];
        aIn = new AnalogValue[analogsIn];
        aOut = new AnalogValue[analogsOut];
        
        // Now let's build the GUI
        // This goes into a panel
        // If ownWindow, then we add to this frame and make visible
        // Else, the panel can be retrieved and added someplace else
        pnlMain = new Panel();
        
        // Draw as a line of input lights (if there are any), a line of output lights (if there are any)
        //   (both labeled)
        // Then a line of input shorts in spinners and a line of output shorts in spinners
        
        // How many rows?
        int rowCount = 0;
        rowCount += discretesIn / 8;
        rowCount += discretesOut / 8;
        rowCount += analogsIn / 8;
        rowCount += analogsOut / 8;
        
        if (discretesIn % 8 > 0) rowCount++;
        if (discretesOut % 8 > 0) rowCount++;
        if (analogsIn % 8 > 0) rowCount++;
        if (analogsOut % 8 > 0) rowCount++;
        
        rowCount *= 2; // Rows of indices
        
        // How many columns? We're going for a grid layout
        int colCount = 0;
        if (discretesIn >= 8) colCount = 8;
        if (discretesOut >= 8) colCount = 8;
        if (analogsIn >= 8) colCount = 8;
        if (analogsOut >= 8) colCount = 8;
        
        if (colCount < discretesIn % 8) colCount = discretesIn % 8;
        if (colCount < discretesOut % 8) colCount = discretesOut % 8;
        if (colCount < analogsIn % 8) colCount = analogsIn % 8;
        if (colCount < analogsOut % 8) colCount = analogsOut % 8;
        
        // If colCount is greater than zero, we'll need a column of labels indicating input/output
        if (colCount > 0) colCount++;
        
        if (rowCount == 0 || colCount == 0) return; // Nothing to do, we don't have anything to display
        
        // Now let's start adding stuff
        pnlMain.setLayout(new GridLayout(rowCount, colCount));
        
        // Discretes in
        if (discretesIn > 0) {
        	pnlMain.add(new Label("Input"));
            
            // Rows of labels followed by values
            int counter = 0;
            while (discretesIn > 0) {
                // Labels
                for (int i = 0; i < 8 && i < discretesIn; i++) {
                    dIn[i + counter] = new DiscreteValue(i + counter, false);
                    ValueLabel lbl = new ValueLabel("" + (i + counter + 1), dIn[i + counter]);
                    lbl.addMouseListener(this);
                    pnlMain.add(lbl);
                }
                
                // Spacers
                for (int i = discretesIn; i < colCount - 1; i++) {
                    pnlMain.add(new Panel());
                }
                
                pnlMain.add(new Panel()); // Spacer where the "Input" or "Output" label goes
                
                for (int i = 0; i < 8 && i < discretesIn; i++) {
                    dIn[i + counter].addMouseListener(this);
                    pnlMain.add(dIn[i + counter]);
                }
                
                // Spacers
                for (int i = discretesIn; i < colCount - 1; i++) {
                    pnlMain.add(new Panel());
                }
                
                counter += 8;
                discretesIn -= 8;
                
                if (discretesIn > 0) pnlMain.add(new Panel());
            }
        }
        
        // Discretes out
        if (discretesOut > 0) {
            pnlMain.add(new Label("Output"));
            
            // Rows of labels followed by values
            int counter = 0;
            while (discretesOut > 0) {
                // Labels
                for (int i = 0; i < 8 && i < discretesOut; i++) {
                    dOut[i + counter] = new DiscreteValue(i + counter, true);
                    ValueLabel lbl = new ValueLabel("" + (i + counter + 1), dOut[i + counter]);
                    lbl.addMouseListener(this);
                    pnlMain.add(lbl);
                }
                
                // Spacers
                for (int i = discretesOut; i < colCount - 1; i++) {
                    pnlMain.add(new Panel());
                }
                
                pnlMain.add(new Panel()); // Spacer where the "Input" or "Output" label goes
                
                for (int i = 0; i < 8 && i < discretesOut; i++) {
                    //dOut[i + counter].addMouseListener(this);
                    pnlMain.add(dOut[i + counter]);
                }
                
                // Spacers
                for (int i = discretesOut; i < colCount - 1; i++) {
                    pnlMain.add(new Panel());
                }
                
                counter += 8;
                discretesOut -= 8;
                
                if (discretesOut > 0) pnlMain.add(new Panel());
            }
        }
        
        // Analogs in
        if (analogsIn > 0) {
            pnlMain.add(new Label("Input"));
            
            // Rows of labels followed by values
            int counter = 0;
            while (analogsIn > 0) {
                // Labels
                for (int i = 0; i < 8 && i < analogsIn; i++) {
                    aIn[i + counter] = new AnalogValue(i + counter, false);
                    ValueLabel lbl = new ValueLabel("" + (i + counter + 1), aIn[i + counter]);
                    lbl.addMouseListener(this);
                    pnlMain.add(lbl);
                }
                
                // Spacers
                for (int i = analogsIn; i < colCount - 1; i++) {
                    pnlMain.add(new Panel());
                }
                
                pnlMain.add(new Panel()); // Spacer where the "Input" or "Output" label goes
                
                for (int i = 0; i < 8 && i < analogsIn; i++) {
                    aIn[i + counter].addChangeListener(this);
                    pnlMain.add(aIn[i + counter]);
                }
                
                // Spacers
                for (int i = analogsIn; i < colCount - 1; i++) {
                    pnlMain.add(new Panel());
                }
                
                counter += 8;
                analogsIn -= 8;
                
                if (analogsIn > 0) {
                    pnlMain.add(new Panel());
                }
            }
        }
        
        // Analogs out
        if (analogsOut > 0) {
            pnlMain.add(new Label("Output"));
            
            // Rows of labels followed by values
            int counter = 0;
            while (analogsOut > 0) {
                // Labels
                for (int i = 0; i < 8 && i < analogsOut; i++) {
                    aOut[i + counter] = new AnalogValue(i + counter, true);
                    ValueLabel lbl = new ValueLabel("" + (i + counter + 1), aOut[i + counter]);
                    lbl.addMouseListener(this);
                    pnlMain.add(lbl);
                }
                
                // Spacers
                for (int i = analogsOut; i < colCount - 1; i++) {
                    pnlMain.add(new Panel());
                }
                
                pnlMain.add(new Panel()); // Spacer where the "Input" or "Output" label goes
                
                for (int i = 0; i < 8 && i < analogsOut; i++) {
                    pnlMain.add(aOut[i + counter]);
                }
                
                // Spacers
                for (int i = analogsOut; i < colCount - 1; i++) {
                    pnlMain.add(new Panel());
                }
                
                counter += 8;
                analogsOut -= 8;
                
                if (analogsOut > 0) pnlMain.add(new Panel());
            }
        }
        
        pack();
        
        myStation = new IOStation(dispatcher, myID,
                                  (short)dIn.length, (short)dOut.length,
                                  (short)aIn.length, (short)aOut.length,
                                  this);
        
        if (ownWindow) {
            addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    setVisible(false);
                    dispose();
                }
            });
            
            setLayout(new GridLayout(1, 1));
            add(pnlMain);
            setTitle("IOStation");
            setVisible(true);
        }
    }
    
    public void addBusListener(BusListener listener) {
        busListeners.add(listener);
    }
    
    public void removeBusListener(BusListener listener) {
        busListeners.remove(listener);
    }
    
    public void mousePressed(MouseEvent e) {
        if (e.getSource() instanceof DiscreteValue) {
            DiscreteValue dv = (DiscreteValue)e.getSource();
            //dv.setValue(!dv.getValue());
            
            if (dv.isOutput()) {
                // We do not support user-changed outputs
                //myStation.putDO(dv.getIndex(), dv.getValue());
            } else {
                myStation.putDI(dv.getIndex(), !dv.getValue());
            }
            
            BussedConnection.compute();
            
            repaint();
        } else if (e.getSource() instanceof ValueLabel) {
            ValueLabel vl = (ValueLabel)e.getSource();
            if (vl.value instanceof DiscreteValue) {
                DiscreteValue dv = (DiscreteValue)vl.value;
                
                if (dv.isOutput()) {
                    busType = BussedConnection.T_DO;
                } else {
                    busType = BussedConnection.T_DI;
                }
                
                busIndex = (short)dv.getIndex();
                
                fireBusChanged();
            } else if (vl.value instanceof AnalogValue) {
                AnalogValue av = (AnalogValue)vl.value;
                
                if (av.isOutput()) {
                    busType = BussedConnection.T_AO;
                } else {
                    busType = BussedConnection.T_AI;
                }
                
                busIndex = (short)av.getIndex();
                
                fireBusChanged();
            }
        }
    }
    
    public void mouseEntered(MouseEvent e) { }
    public void mouseExited(MouseEvent e) { }
    public void mouseClicked(MouseEvent e) { }
    public void mouseReleased(MouseEvent e) { }
    
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() instanceof AnalogValue) {
            AnalogValue av = (AnalogValue)e.getSource();
            
            int val = ((Integer)av.getValue()).intValue();
            if (av.isOutput()) {
                myStation.putAO(av.getIndex(), (short)(val & 0xFFFF));
            } else {
                myStation.putAI(av.getIndex(), (short)(val & 0xFFFF));
            }
            
            BussedConnection.compute();
            
            pack();
        }
    }
    
    public short getID() {
        return myStation.getID();
    }
    
    public IOStation getStation() {
        return myStation;
    }
    
    public void putDI(IOStation from, int index, boolean value) {
        if (index < 0 || index >= dIn.length) return;
        dIn[index].setValue(value);
    }
    
    public void putDO(IOStation from, int index, boolean value) {
        if (index < 0 || index >= dOut.length) return;
        dOut[index].setValue(value);
    }
    
    public void putAI(IOStation from, int index, short value) {
        if (index < 0 || index >= aIn.length) return;
        aIn[index].setValue(Integer.valueOf(value & 0xFFFF));
    }
    
    public void putAO(IOStation from, int index, short value) {
        if (index < 0 || index >= aOut.length) return;
        aOut[index].setValue(Integer.valueOf(value & 0xFFFF));
    }
    
    public Panel getMainPanel() {
        return pnlMain;
    }
    
    protected void fireBusChanged() {
        for (BusListener listener : busListeners) {
            listener.busChanged(this);
        }
    }
}

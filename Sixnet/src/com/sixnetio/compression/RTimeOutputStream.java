/*
 * RTimeOutputStream.java
 *
 * Run-time compressed output stream for binary data.
 * Note: This is not the same kind of RLE as is used by JFFS2.
 *
 * Jonathan Pearson
 * September 8, 2008
 *
 */

package com.sixnetio.compression;

import java.io.*;

public class RTimeOutputStream
	extends FilterOutputStream
{
	// Note: This buffer MUST ALWAYS have an even length
	private byte[] buffer;
	private int pos;
	
	public RTimeOutputStream(OutputStream out)
	{
		super(out);
		
		buffer = new byte[8192];
		pos = 0;
	}
	
	@Override
	public void write(byte[] b, int off, int len)
		throws IOException
	{
		for (int i = 0; i < len; i++) {
			if (pos >= buffer.length) {
				flush();
			}
			
			if (pos > 0) {
				// There is some data in the buffer
				// Check to see if this byte is a repeat of what is there; if
				// so, increment the counter
				// Need to make sure the counter can be incremented one more
				// time, though
				if (buffer[pos - 2] == b[off + i] &&
				    buffer[pos - 1] != (byte)0xff) {
					
					buffer[pos - 1]++;
				}
				else {
					buffer[pos++] = b[off + i];
					buffer[pos++] = 0;
				}
			}
		}
	}
	
	@Override
	public void write(byte[] b)
		throws IOException
	{
		write(b, 0, b.length);
	}
	
	public void write(byte b)
		throws IOException
	{
		byte[] buf = {b};
		write(buf, 0, 1);
	}
	
	@Override
	public void flush()
		throws IOException
	{
		super.write(buffer, 0, pos);
	}
}

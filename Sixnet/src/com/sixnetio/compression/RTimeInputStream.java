/*
 * RTimeInputStream.java
 *
 * Run-time compressed input stream for binary data.
 * Note: This is not the same kind of RLE as is used by JFFS2.
 *
 * Jonathan Pearson
 * August 4, 2008
 *
 */

package com.sixnetio.compression;

import java.io.*;

public class RTimeInputStream
	extends FilterInputStream
{
	private byte[] buffer;
	private int pos, size;
	
	public RTimeInputStream(InputStream in)
	{
		super(in);
		
		buffer = new byte[8192];
		pos = 0;
		size = 0;
	}
	
	@Override
	public synchronized int read()
		throws IOException
	{
		if (pos >= size) {
			fillBuffer();
		}
		
		if (pos == -1) {
			return -1;
		}
		else {
			return buffer[pos++];
		}
	}
	
	@Override
	public int read(byte[] b)
		throws IOException
	{
		return read(b, 0, b.length);
	}
	
	@Override
	public synchronized int read(byte[] b, int off, int len)
		throws IOException
	{
		if (pos >= size) {
			fillBuffer();
		}
		
		if (pos == -1) return -1;
		
		if (size - pos >= len) {
			System.arraycopy(buffer, pos, b, off, len);
			pos += len;
			
			return len;
		}
		else {
			int bytes = size - pos;
			System.arraycopy(buffer, pos, b, off, bytes);
			pos += bytes;
			
			return bytes;
		}
	}
	
	private void fillBuffer()
		throws IOException
	{
		int inpos = 0;
		
		// Leave some space in case the last byte gets lots of repeats
		while (inpos < buffer.length - 256) {
			int value = super.read();
			if (value == -1) {
				break;
			}
			
			buffer[inpos++] = (byte)(value & 0xff);
			
			int repeat = super.read();
			if (repeat == -1) {
				throw new IOException("Unexpected EOF in underlying input stream");
			}
			
			for (int i = 0; i < repeat; i++) {
				buffer[inpos++] = (byte)(value & 0xff);
			}
		}
		
		if (inpos == 0) {
			// Reached EOF immediately
			buffer = new byte[0]; // Free the memory
			pos = -1;
			size = 0;
		}
		else {
			pos = 0;
			size = inpos;
		}
	}
}

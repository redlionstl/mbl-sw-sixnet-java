/*
 * VFSObjectFactory.java
 *
 * Provides factory methods for creating VFS objects.
 *
 * Jonathan Pearson
 * November 11, 2009
 *
 */

package com.sixnetio.fs.vfs;

import org.apache.log4j.Logger;

import com.sixnetio.fs.generic.*;
import com.sixnetio.util.Utils;

public class VFSObjectFactory
	implements FSObjectFactory
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	@Override
	public BlockDevice createBlockDevice(String name, byte major, byte minor,
	                                     Directory parent)
	{
		throw new UnsupportedOperationException("VFS does not support block special devices");
	}
	
	@Override
	public CharDevice createCharDevice(String name, byte major, byte minor, Directory parent)
	{
		throw new UnsupportedOperationException("VFS does not support character special devices");
	}
	
	/**
	 * Create a VFS directory.
	 * 
	 * @param name The name of the directory.
	 * @param userType The user type for the directory (for firmware bundles,
	 *   see FirmwareBundle.T_ARCH_*).
	 * @param fileType The file type for the directory. Will be set to "" if
	 *   <tt>null</tt>.
	 * @param parent The parent of this directory, or <tt>null</tt> if this is
	 *   the root directory. This object will be added to the parent's contents
	 *   if not <tt>null</tt>.
	 * @return A new VFS directory.
	 */
	public VDirectory createDirectory(String name, int userType,
	                                  String fileType, VDirectory parent)
	{
		if (fileType == null) {
			fileType = "";
		}
		
		VDirectory obj = new VDirectory(name, userType, fileType, parent);
		if (parent != null) {
			parent.addChild(obj);
		}
		return obj;
	}
	
	@Override
	public VDirectory createDirectory(String name, Directory parent)
	{
		if ( ! (parent instanceof VDirectory)) {
			throw new IllegalArgumentException("Parent of a VFS object must be a VFS directory");
		}
		
		return createDirectory(name, 0, "", (VDirectory)parent);
	}
	
	@Override
	public FIFO createFIFO(String name, Directory parent)
	{
		throw new UnsupportedOperationException("VFS does not support FIFOs");
	}
	
	/**
	 * Create a VFS file.
	 * 
	 * @param name The name of the file.
	 * @param data The contents of the file.
	 * @param userType The user type for the file (for firmware bundles, see
	 *   FirmwareBundle.T_FILE_*).
	 * @param fileType The file type for the file. Will be set to "" if
	 *   <tt>null</tt>.
	 * @param parent The parent directory of this file.
	 * @return A new VFS file.
	 */
	public VFile createFile(String name, byte[] data, int userType,
	                        String fileType, VDirectory parent)
	{
		if (fileType == null) {
			fileType = "";
		}
		
		VFile obj = new VFile(name, data, userType, fileType, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public VFile createFile(String name, byte[] data, Directory parent)
	{
		if ( ! (parent instanceof VDirectory)) {
			throw new IllegalArgumentException("Parent of a VFS object must be a VFS directory");
		}
		
		return createFile(name, data, 0, "", (VDirectory)parent);
	}
	
	@Override
	public Socket createSocket(String name, Directory parent)
	{
		throw new UnsupportedOperationException("VFS does not support sockets");
	}
	
	/**
	 * Create a VFS symlink.
	 * 
	 * @param name The name of the symlink.
	 * @param linkName The target of the symlink. Note that all VSymlinks MUST
	 *   point at VFile objects, but this is not enforced here to avoid weird
	 *   errors due to ordering.
	 * @param userType The user type for the symlink (for firmware bundles, see
	 *   FirmwareBundle.T_FILE_*). VFS symlinks cannot point at directories.
	 * @param fileType The file type for the symlink. Will be set to "" if
	 *   <tt>null</tt>.
	 * @param parent The parent directory of this symlink.
	 * @return A new VFS symlink.
	 */
	public VSymlink createSymlink(String name, String linkName, int userType,
	                              String fileType, VDirectory parent)
	{
		if (fileType == null) {
			fileType = "";
		}
		
		VSymlink lnk = new VSymlink(name, linkName, userType, fileType, parent);
		parent.addChild(lnk);
		return lnk;
	}
	
	@Override
	public VSymlink createSymlink(String name, String linkName, Directory parent)
	{
		if ( ! (parent instanceof VDirectory)) {
			throw new IllegalArgumentException("Parent of a VFS object must be a VFS directory");
		}
		
		return createSymlink(name, linkName, 0, "", (VDirectory)parent);
	}
	
	@Override
	public Whiteout createWhiteout(String name, Directory parent)
	{
		throw new UnsupportedOperationException("VFS does not support whiteouts");
	}
	
	@Override
	public FSObject convertObject(FSObject obj, Directory parent)
	{
		if ( ! (parent instanceof VDirectory)) {
			throw new IllegalArgumentException("Parent of a VFS FSObject must be a VDirectory");
		}
		
		VDirectory vparent = (VDirectory)parent;
		
		if (obj instanceof VDirectory) {
			VDirectory o = (VDirectory)obj;
			
			return createDirectory(o.getName(),
			                       o.getVFSAttributes().getUserType(),
			                       o.getVFSAttributes().getFileType(),
			                       vparent);
			
		}
		else if (obj instanceof Directory) {
			Directory o = (Directory)obj;
			
			int userType = FirmwareBundle.stringToArch(o.getName());
			
			return createDirectory(o.getName(), userType, "", vparent);
		}
		else if (obj instanceof VFile) {
			VFile o = (VFile)obj;
			
			return createFile(o.getName(), o.getData(),
			                  o.getVFSAttributes().getUserType(),
			                  o.getVFSAttributes().getFileType(),
			                  vparent);
		}
		else if (obj instanceof File) {
			File o = (File)obj;
			
			int userType = FirmwareBundle.stringToType(o.getName());
			String fileType = "";
			
			if (o.getName().equalsIgnoreCase("switchinfo.tgz")) {
				fileType = "switchinfo.tgz";
			}
			
			return createFile(o.getName(), o.getData(), userType, fileType, vparent);
		}
		else if (obj instanceof VSymlink) {
			VSymlink o = (VSymlink)obj;
			
			return createSymlink(o.getName(), o.getLinkName(),
			                     o.getVFSAttributes().getUserType(),
			                     o.getVFSAttributes().getFileType(),
			                     vparent);
		}
		else if (obj instanceof Symlink) {
			Symlink o = (Symlink)obj;
			
			int userType = FirmwareBundle.stringToType(o.getName());
			String fileType = "";
			
			if (o.getName().equalsIgnoreCase("switchinfo.tgz")) {
				fileType = "switchinfo.tgz";
			}
			
			return createSymlink(o.getName(), o.getLinkName(), userType, fileType, vparent);
		}
		else {
			throw new IllegalArgumentException("VFS does not support '" +
			                                   obj.getClass().getName() + "'");
		}
	}
	
	/**
	 * Convert the filesystem rooted at <tt>dir</tt> to the object types
	 * constructed by this factory. This will attempt to match user types based
	 * on file/directory names to match FirmwareBundle types.
	 * 
	 * @param dir The directory to convert.
	 * @return A converted filesystem.
	 */
	@Override
	public VDirectory convertFilesystem(Directory dir)
	{
		VDirectory root = createDirectory(dir.getName(), 0, "", null);
		
		for (FSObject child : dir) {
			convertFilesystem(child, root);
		}
		
		return root;
	}
	
	private void convertFilesystem(FSObject obj, VDirectory root)
	{
		try {
			FSObject conv = convertObject(obj, root);
			
			if (obj instanceof Directory) {
				Directory o = (Directory)obj;
				for (FSObject child : o) {
					convertFilesystem(child, (VDirectory)conv);
				}
			}
		}
		catch (IllegalArgumentException iae) {
			logger.warn(iae.getMessage(), iae);
		}
	}
}

/*
 * VFSAttributes.java
 *
 * Provides the extra attributes that VFS files/directories possess.
 *
 * Jonathan Pearson
 * November 12, 2009
 *
 */

package com.sixnetio.fs.vfs;

public class VFSAttributes
{
	private int type;
	private int userType;
	private String fileType;
	
	/** Used as a marker while writing VFS data to a stream. */
	private int offset;
	
	public VFSAttributes(int type, int userType, String fileType)
	{
		this.type = type;
		this.userType = userType;
		this.fileType = fileType;
	}
	
	/** Get the VFS type of this object. */
	public int getVFSType()
	{
		return type;
	}
	
	/** Get the VFS user type of this object. */
	public int getUserType()
	{
		return userType;
	}
	
	/** Set the VFS user type of this object. */
	public void setUserType(int userType)
	{
		this.userType = userType;
	}
	
	/** Get the VFS file type of this object. */
	public String getFileType()
	{
		return fileType;
	}
	
	/** Set the VFS file type of this object. */
	public void setFileType(String fileType)
	{
		this.fileType = fileType;
	}
	
	/**
	 * Get the offset from the last time this object was written. The offset is
	 * used when writing VFS data to a stream.
	 */
	int getOffset()
	{
		return offset;
	}
	
	/** Set the offset. The offset is used when writing VFS data to a stream. */
	void setOffset(int offset)
	{
		this.offset = offset;
	}
}

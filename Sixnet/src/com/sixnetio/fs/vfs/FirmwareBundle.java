/*
 * FirmwareBundle.java
 *
 * Represents a firmware bundle, built on top of the VFS.
 *
 * Jonathan Pearson
 * June 21, 2007
 *
 */

package com.sixnetio.fs.vfs;

import java.io.*;

import com.sixnetio.fs.generic.FSObject;

public class FirmwareBundle
	extends VirtualFS
{
	// If you update these, make sure to update "getArchitectures()"
	public static final int T_ARCH_NONE = '0', // Extra, non-arch-related files
                                T_ARCH_PPC = 'A', // PPC
                                T_ARCH_ARM = 'B', // ARM
                                T_ARCH_ARM9 = 'C', // ARM9
                                T_ARCH_PPC_8313 = 'D'; // PPC 8313
	
	public static final int T_FILE_NONE = 99, // Not an image file of any sort
	                        T_FILE_IMAGEB = 1,
	                        T_FILE_IMAGE = 2,
	                        T_FILE_IMAGE6 = 3,
	                        T_FILE_IMAGEC = 4;
	
	public static final int T_TYPE_VT_IPM = 1001,
	                        T_TYPE_VT_UIPM = 1002,
	                        T_TYPE_VT6_IPM = 1003,
	                        T_TYPE_8xxx_IPM = 1004,
	                        
	                        T_TYPE_ST_IPM = 1101,
	                        T_TYPE_ST6_IPM = 1102,
	                        
	                        T_TYPE_ET_GT_ST_3 = 1201,
	                        
	                        T_TYPE_ST_GT_1210 = 1301,
	                        
                                T_TYPE_MS = 2000,

	                        T_TYPE_IPM = 3000;
	
	private static final int V_1 = 1;
	
	// "FWBx" where x is the version (V_* above)
	private static final int FWB_MAGIC = 0x46574200;
	
	private int fileVersion; // V_*
	private int fwType; // T_Type_*
	private String fwVersion;
	
	/**
	 * Construct a new FirmwareBundle by reading from a stream.
	 * 
	 * @param in The stream to read from.
	 * @throws IOException If there was a problem reading from the stream.
	 */
	public FirmwareBundle(InputStream in)
		throws IOException
	{
		DataInputStream din = new DataInputStream(in);
		int[] offset = new int[1];
		
		fileVersion = din.readInt();
		offset[0] += 4;
		
		switch (fileVersion) {
			case V_1:
				readV1Bundle(din, offset);
				break;
			default:
				throw new IOException("Unknown file version: " + fileVersion);
		}
	}
	
	/**
	 * Construct a new FirmwareBundle by reading from a byte array. This is a
	 * wrapper around {@link #FirmwareBundle(InputStream)}.
	 * 
	 * @param data The byte array to read from.
	 * @throws IOException If there is a problem parsing the data.
	 */
	public FirmwareBundle(byte[] data)
		throws IOException
	{
		this(new ByteArrayInputStream(data));
	}
	
	/**
	 * Construct a new empty firmware bundle.
	 * 
	 * @param fileVersion The bundle version number (probably {@link #V_1}).
	 * @param fwType The bundle type (one of T_TYPE_* above).
	 * @param fwVersion The firmware version contained in this bundle.
	 */
	public FirmwareBundle(int fileVersion, int fwType, String fwVersion)
	{
		super();
		
		this.fileVersion = fileVersion;
		this.fwType = fwType;
		this.fwVersion = fwVersion;
		
		VFSObjectFactory factory = new VFSObjectFactory();
		setRoot(factory.createDirectory("", FWB_MAGIC | fileVersion, "", null));
	}
	
	/**
	 * Find a file given some criteria.
	 * @param arch The architecture that the file belongs to (these are
	 *   implemented as directories), one of T_Arch_* above.
	 * @param file The type of file, one of T_File_* above.
	 * @param type The fileType of the file, or <tt>null</tt> for any.
	 * @return The first matching file that is found, or <tt>null</tt> if none
	 *   is found.
	 */
	public VFile find(int arch, int file, String type)
	{
		// Find the architecture directory
		FSObject archDir = getRoot().find(VirtualFS.T_DIR, arch,
		                                  null, null);
		
		if (archDir != null && archDir instanceof VDirectory) {
			FSObject archFile = ((VDirectory)archDir).
			    find(VirtualFS.T_FILE, file, type, null);
			
			if (archFile != null) {
				if (archFile instanceof VFile) {
					return (VFile)archFile;
				}
				else if (archFile instanceof VSymlink) {
					return (VFile)(((VSymlink)archFile).getLinkTarget());
				}
			}
		}
		
		// Unable to find the target
		return null;
	}
	
	/**
	 * Write this firmware bundle to a stream.
	 * 
	 * @param out The stream to write to.
	 * @throws IOException If there was a problem writing to the stream.
	 * @throws IllegalArgumentException If the firmware bundle's underlying VFS
	 *   is not structured properly (for example, if any VSymlink does not point
	 *   at a VFile).
	 */
	public void toStream(OutputStream out)
		throws IOException, IllegalArgumentException
	{
		DataOutputStream dout = new DataOutputStream(out);
		
		int offset = 0;
		
		// Header
		dout.writeInt(fileVersion);
		offset += 4;
		
		if (fileVersion == 1) {
			writeV1Bundle(dout, offset);
		}
		else {
			throw new IOException("Unknown file version: " + fileVersion);
		}
		
		dout.flush();
	}
	
	/**
	 * Write this firmware bundle to a byte array. This is a wrapper around
	 * {@link #toStream(OutputStream)}.
	 * 
	 * @return A byte array containing the data of this bundle.
	 */
	public byte[] getData()
	{
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			DataOutputStream dout = new DataOutputStream(out);
			
			toStream(dout);
			
			return out.toByteArray();
		}
		catch (IOException ioe) {
			throw new RuntimeException("Unexpected IOException: " +
			                           ioe.getMessage(), ioe);
		}
	}
	
	public void dump(PrintStream out)
	{
		out.println("Firmware Bundle version " + fileVersion);
		out.println("  For " + getFWTypeAsString());
		out.println("  Containing firmware version " + fwVersion);
		
		getRoot().dump(out);
	}
	
	public int getFileVersion()
	{
		return fileVersion;
	}
	
	public int getFWType()
	{
		return fwType;
	}
	
	public String getFWTypeAsString()
	{
		switch(fwType) {
			case T_TYPE_VT_IPM:
				return "VT-IPm";
			case T_TYPE_VT_UIPM:
				return "VT-uIPm";
			case T_TYPE_VT6_IPM:
				return "VT-IPm 6350";
			case T_TYPE_8xxx_IPM:
				return "8xxx IPm";
			case T_TYPE_ST_IPM:
				return "ST-IPm";
			case T_TYPE_ST6_IPM:
				return "ST-IPm 6350";
			case T_TYPE_ET_GT_ST_3:
				return "ET-GT-ST-3";
			case T_TYPE_ST_GT_1210:
				return "ST-GT 1210";
			case T_TYPE_MS:
				return "Managed Switch";
			case T_TYPE_IPM:
				return "IPm";
			default:
				return "Unknown";
		}
	}
	
	public String getFWVersion()
	{
		return fwVersion;
	}
	
	public String getFWVendor()
	{
		return getRoot().getName();
	}
	
	private void readV1Bundle(DataInputStream in, int[] offset)
		throws IOException
	{
		fwType = in.readInt();
		offset[0] += 4;
		
		fwVersion = readString(in, offset);
		readTree(in, offset);
		
		if (getRoot().getVFSAttributes().getUserType() != (FWB_MAGIC | fileVersion)) {
			throw new IOException("Bundle magic number does not match");
		}
	}
	
	/**
	 * Write a Version 1 Firmware Bundle to a stream.
	 * 
	 * @param out The stream to write to.
	 * @param offset The offset within the stream. We need to keep track of this
	 *   because symlink targets are by file offset, which is impossible to
	 *   compute from a stream.
	 * @throws IOException If there is a problem writing to the stream.
	 */
	private void writeV1Bundle(DataOutputStream out, int offset)
		throws IOException
	{
		out.writeInt(fwType);
		offset += 4;
		
		offset += writeString(fwVersion, out);
		
		calculateOffsets(getRoot(), offset);
		writeTree(getRoot(), out);
	}
	
	@Override
	public String translateUserType(int userType)
	{
		String ans;
		
		switch (userType) {
			case T_ARCH_NONE:
				ans = "None";
				break;
			case T_ARCH_PPC:
				ans = "PPC";
				break;
			case T_ARCH_PPC_8313:
				ans = "PPC_8313";
				break;
			case T_ARCH_ARM:
				ans = "ARM";
				break;
			case T_ARCH_ARM9:
				ans = "ARM9";
				break;
			case T_FILE_NONE:
				ans = "None";
				break;
			case T_FILE_IMAGEB:
				ans = "ImageB";
				break;
			case T_FILE_IMAGE:
				ans = "Image";
				break;
			case T_FILE_IMAGE6:
				ans = "Image6";
				break;
			case T_FILE_IMAGEC:
				ans = "ImageC";
				break;
			case (FWB_MAGIC | V_1):
				ans = "FWB1";
				break;
			default:
				ans = "Unknown (" + userType + ")";
		}
		
		return ans;
	}
	
	public static int stringToArch(String string)
	{
		string = string.toLowerCase();
		
		if (string.equals("arm") || string.equals("arm7")) {
			return T_ARCH_ARM;
		}
		else if (string.equals("arm9")) {
			return T_ARCH_ARM9;
		}
		else if (string.equals("ppc")) {
			return T_ARCH_PPC;
		}
		else if (string.equals("ppc_8313")) {
                        return T_ARCH_PPC_8313;
		}
		else {
			return T_ARCH_NONE;
		}
	}
	
	public static int stringToType(String string)
	{
		string = string.toLowerCase();
		
		if (string.equals("none")) {
			return T_FILE_NONE;
		}
		else if (string.equals("imageb")) {
			return T_FILE_IMAGEB;
		}
		else if (string.equals("image")) {
			return T_FILE_IMAGE;
		}
		else if (string.equals("image6")) {
			return T_FILE_IMAGE6;
		}
		else if (string.equals("imagec")) {
			return T_FILE_IMAGEC;
		}
		else {
			return T_FILE_NONE;
		}
	}
	
	public static int[] getArchitectures()
	{
	    int[] archs = { T_ARCH_ARM, T_ARCH_ARM9,
                            T_ARCH_PPC, T_ARCH_PPC_8313 };
	    return archs;
	}
}

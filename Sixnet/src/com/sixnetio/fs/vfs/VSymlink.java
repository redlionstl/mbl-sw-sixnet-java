/*
 * VSymlink.java
 *
 * Represents a symlink in Sixnet's Virtual file system (used for firmware
 * bundles).
 *
 * Jonathan Pearson
 * November 11, 2009
 *
 */

package com.sixnetio.fs.vfs;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Date;

import com.sixnetio.fs.generic.*;
import com.sixnetio.util.Utils;

public class VSymlink
	extends Symlink
	implements VFSObject
{
	private final VFSAttributes vfsAttrs;
	
	public VSymlink(String name, String linkName, int userType, String fileType,
	                VDirectory parent)
	{
		super(name, linkName, (short)0, (short)0, 0777 | FSObject.MODE_LNK,
		      now(), parent);
		
		vfsAttrs = new VFSAttributes(VirtualFS.T_LINK, userType, fileType);
	}
	
	@Override
	public FSObject find(Integer type, Integer userType, String fileType,
	                     String name)
	{
		FSObject found = null;
		
		if ((type == null || type == vfsAttrs.getVFSType()) &&
		    (userType == null || userType == vfsAttrs.getUserType()) &&
		    (fileType == null || fileType.equals(vfsAttrs.getFileType())) &&
		    (name == null || name.equals(getName()))) {
			
			found = this;
		}
		
		// This is not a directory, so we have nothing further to search
		
		return found;
	}
	
	@Override
	public void setParent(Directory parent)
	{
		if ( ! (parent instanceof VDirectory)) {
			throw new IllegalArgumentException("VFS object parent must be a VFS directory");
		}
		
		super.setParent(parent);
	}
	
	/**
	 * Set the (possibly relative) path to the target of this link. If this
	 * file system is going to be written to a stream, the targets of all
	 * VSymlinks MUST be VFiles. This check is not made here, as it would be
	 * difficult to work around the ordering issue of updating a link and its
	 * target.
	 */
	@Override
	public void setLinkName(String linkName)
	{
		// This is only overridden to provide extra Javadoc
		super.setLinkName(linkName);
	}
	
	@Override
	public VFSAttributes getVFSAttributes()
	{
		return vfsAttrs;
	}
	
	@Override
	public char getTypeAsChar()
	{
		return 'f';
	}
	
	@Override
	public void dump(int indentLevel, PrintStream out)
	{
		String indent = Utils.multiplyChar(' ', indentLevel * 2);
		
		String modTime =
			DateFormat.getDateInstance().format(new Date(getModTime() * 1000));
		
		String typeName;
		String userTypeName;
		
		VirtualFS translator = null;
		FSObject root = getRoot();
		if (root instanceof VDirectory) {
			translator = ((VDirectory)root).getTranslator();
		}
		
		if (translator == null) {
			typeName = "Link (" + vfsAttrs.getVFSType() + ")";
			userTypeName = "Unknown (" + vfsAttrs.getUserType() + ")";
		}
		else {
			typeName = translator.translateObjectType(vfsAttrs.getVFSType());
			userTypeName = translator.translateUserType(vfsAttrs.getUserType());
		}
		
		out.println(String.format("%c  %20s  %15s  %,12d  %s%s -> %s",
		                          getTypeAsChar(), userTypeName,
		                          "'" + vfsAttrs.getFileType() + "'",
		                          0, indent, getName(), getLinkName()));
	}
}

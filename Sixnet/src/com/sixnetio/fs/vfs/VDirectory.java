/*
 * VDirectory.java
 *
 * Represents a directory in Sixnet's Virtual file system (used for firmware
 * bundles).
 *
 * Jonathan Pearson
 * November 11, 2009
 *
 */

package com.sixnetio.fs.vfs;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.*;

import com.sixnetio.fs.generic.Directory;
import com.sixnetio.fs.generic.FSObject;
import com.sixnetio.util.Utils;

public class VDirectory
	extends Directory
	implements VFSObject
{
	private final VFSAttributes vfsAttrs;
	
	// If present (set it on the root of the filesystem), this may be used to
	// translate type names for dumping
	private VirtualFS translator;
	
	public VDirectory(String name, int userType, String fileType,
	                  VDirectory parent)
	{
		super(name, (short)0, (short)0, 0755 | FSObject.MODE_DIR, now(), parent);
		
		vfsAttrs = new VFSAttributes(VirtualFS.T_DIR, userType, fileType);
	}
	
	@Override
	public void setParent(Directory parent)
	{
		if ( ! (parent instanceof VDirectory)) {
			throw new IllegalArgumentException("VFS object parent must be a VFS directory");
		}
		
		super.setParent(parent);
	}
	
	@Override
	public VFSAttributes getVFSAttributes()
	{
		return vfsAttrs;
	}
	
	/**
	 * Get the VirtualFS object used to get descriptive names for type values.
	 */
	public VirtualFS getTranslator()
	{
		return translator;
	}
	
	/**
	 * Set the VirtualFS object used to get descriptive names for type values.
	 */
	public void setTranslator(VirtualFS translator)
	{
		this.translator = translator;
	}
	
	@Override
	public FSObject find(Integer type, Integer userType, String fileType,
	                     String name)
	{
		FSObject found = null;
		
		if ((type == null || type == vfsAttrs.getVFSType()) &&
		    (userType == null || userType == vfsAttrs.getUserType()) &&
		    (fileType == null || fileType.equals(vfsAttrs.getFileType())) &&
		    (name == null || name.equals(getName()))) {
			
			found = this;
		}
		else {
			for (FSObject obj : this) {
				if (found != null) {
					break;
				}
				
				if (obj instanceof VFile) {
					found = ((VFile)obj).find(type, userType,
					                          fileType, name);
				}
				else if (obj instanceof VDirectory) {
					found = ((VDirectory)obj).find(type, userType,
					                               fileType, name);
				}
				else if (obj instanceof VSymlink) {
					found = ((VSymlink)obj).find(type, userType,
					                             fileType, name);
				}
				else {
					// No idea what else there could be...
					throw new IllegalStateException("Unrecognized object as child of VDirectory: " +
					                                obj.getClass().getName());
				}
			}
		}
		
		return found;
	}
	
	@Override
	public void dump(int indentLevel, PrintStream out)
	{
		String indent = Utils.multiplyChar(' ', indentLevel * 2);
		
		String modTime =
			DateFormat.getDateInstance().format(new Date(getModTime() * 1000));
		
		String typeName;
		String userTypeName;
		
		VirtualFS translator = null;
		FSObject root = getRoot();
		if (root instanceof VDirectory) {
			translator = ((VDirectory)root).getTranslator();
		}
		
		if (translator == null) {
			typeName = "Directory (" + vfsAttrs.getVFSType() + ")";
			userTypeName = "Unknown (" + vfsAttrs.getUserType() + ")";
		}
		else {
			typeName = translator.translateObjectType(vfsAttrs.getVFSType());
			userTypeName = translator.translateUserType(vfsAttrs.getUserType());
		}
		
		out.println(String.format("%c  %20s  %15s  %,12d  %s%s/",
		                          getTypeAsChar(), userTypeName,
		                          "'" + vfsAttrs.getFileType() + "'",
		                          0, indent, getName()));
		
		List<FSObject> contents = getContents();
		FSObject[] children = contents.toArray(new FSObject[contents.size()]);
		
		// The default sort for FSObjects is by path, which is much slower than
		// sorting by name
		Arrays.sort(children, new Comparator<FSObject>() {
			public int compare(FSObject o1, FSObject o2)
			{
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		for (FSObject child : children) {
			child.dump(indentLevel + 1, out);
		}
	}
}

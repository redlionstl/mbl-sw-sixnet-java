/*
 * VFSObject.java
 *
 * Outlines the required methods for VFS objects.
 *
 * Jonathan Pearson
 * November 12, 2009
 *
 */

package com.sixnetio.fs.vfs;

import com.sixnetio.fs.generic.FSObject;

public interface VFSObject
{
	/**
	 * Get the VFS attributes applied to this object. Updates to the returned
	 * object affect this object.
	 */
	public VFSAttributes getVFSAttributes();
	
	/**
	 * Search the filesystem (starting at this point) for an object that matches
	 * the given criteria.
	 * 
	 * @param type The object type to match (see VirtualFS.T_*), or
	 *   <tt>null</tt> to match any object type.
	 * @param userType The user type to match, <tt>null</tt> to match any user
	 *   type.
	 * @param fileType The file type to match, or <tt>null</tt> to match any
	 *   file type.
	 * @param name The file name to match, or <tt>null</tt> to match any name.
	 * @return The first node in a me-first depth-first search that matches, or
	 *   <tt>null</tt> if nothing is found.
	 */
	public FSObject find(Integer type, Integer userType, String fileType,
	                     String name);
	
}

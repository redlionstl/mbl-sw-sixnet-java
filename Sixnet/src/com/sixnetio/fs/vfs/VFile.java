/*
 * VFile.java
 *
 * Represents a file in Sixnet's Virtual File System (used for firmware
 * bundles).
 *
 * Jonathan Pearson
 * November 11, 2009
 *
 */

package com.sixnetio.fs.vfs;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Date;

import com.sixnetio.fs.generic.*;
import com.sixnetio.util.Utils;

public class VFile
	extends File
	implements VFSObject
{
	private final VFSAttributes vfsAttrs;
	
	protected VFile(String name, byte[] data, int userType, String fileType,
	                VDirectory parent)
	{
		super(name, data, (short)0, (short)0, 0644 | FSObject.MODE_REG, now(),
		      parent);
		
		vfsAttrs = new VFSAttributes(VirtualFS.T_FILE, userType, fileType);
	}
	
	@Override
	public void setParent(Directory parent)
	{
		if ( ! (parent instanceof VDirectory)) {
			throw new IllegalArgumentException("VFS object parent must be a VFS directory");
		}
		
		super.setParent(parent);
	}
	
	@Override
	public VFSAttributes getVFSAttributes()
	{
		return vfsAttrs;
	}
	
	@Override
	public FSObject find(Integer type, Integer userType, String fileType,
	                     String name)
	{
		FSObject found = null;
		
		if ((type == null || type == vfsAttrs.getVFSType()) &&
		    (userType == null || userType == vfsAttrs.getUserType()) &&
		    (fileType == null || fileType.equals(vfsAttrs.getFileType())) &&
		    (name == null || name.equals(getName()))) {
			
			found = this;
		}
		
		// This is not a directory, so we have nothing further to search
		
		return found;
	}
	
	@Override
	public void dump(int indentLevel, PrintStream out)
	{
		String indent = Utils.multiplyChar(' ', indentLevel * 2);
		
		String modTime =
			DateFormat.getDateInstance().format(new Date(getModTime() * 1000));
		
		String typeName;
		String userTypeName;
		
		VirtualFS translator = null;
		FSObject root = getRoot();
		if (root instanceof VDirectory) {
			translator = ((VDirectory)root).getTranslator();
		}
		
		if (translator == null) {
			typeName = "File (" + vfsAttrs.getVFSType() + ")";
			userTypeName = "Unknown (" + vfsAttrs.getUserType() + ")";
		}
		else {
			typeName = translator.translateObjectType(vfsAttrs.getVFSType());
			userTypeName = translator.translateUserType(vfsAttrs.getUserType());
		}
		
		out.println(String.format("%c  %20s  %15s  %,12d  %s%s",
		                          getTypeAsChar(), userTypeName,
		                          "'" + vfsAttrs.getFileType() + "'",
		                          getSize(), indent, getName()));
	}
}

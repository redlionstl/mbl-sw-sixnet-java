/*
 * VirtualFS.java
 *
 * A Java implementation of the Sixnet VFS.
 *
 * Jonathan Pearson
 * June 21, 2007
 *
 */

package com.sixnetio.fs.vfs;

import java.io.*;

import org.apache.log4j.Logger;

import com.sixnetio.fs.generic.FSObject;
import com.sixnetio.util.Utils;

public class VirtualFS
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	public static final int T_FILE = 1,
	                        T_DIR = 2,
	                        T_LINK = 3;
	
	private VDirectory root;
	
	/**
	 * Construct a new, empty virtual FS. You should either call
	 * {@link #readTree(DataInputStream, int[])} or {@link #setRoot(VDirectory)}
	 * after using this.
	 */
	protected VirtualFS()
	{
		// Nothing to do
	}
	
	/**
	 * Initialize this VirtualFS by reading the filesystem tree from the given
	 * stream.
	 * 
	 * @param in The stream to read.
	 * @param offset Used for resolving symlinks. The number of bytes read will
	 *   be added to [0], which should contain the number of bytes which have
	 *   been read from the stream already.
	 * @throws IOException If there was a problem reading the data.
	 */
	protected void readTree(DataInputStream in, int[] offset)
		throws IOException
	{
		VFSObjectFactory factory = new VFSObjectFactory();
		
		VFSObject obj = readTree(null, factory, in, offset);
		if (obj == null ||
		    ! (obj instanceof VDirectory)) {
			
			throw new IOException("VFS has a non-directory as the root");
		}
		
		fixSymlinks((VDirectory)obj);
		
		root = (VDirectory)obj;
		root.setTranslator(this);
	}
	
	private VFSObject readTree(VDirectory parent, VFSObjectFactory factory,
	                           DataInputStream in, int[] offset)
		throws IOException
	{
		int dataOffset = offset[0];
		
		int type = in.readInt();
		offset[0] += 4;
		
		int userType = in.readInt();
		offset[0] += 4;
		
		String name = readString(in, offset);
		String fileType = readString(in, offset);
		
		int size = in.readInt();
		offset[0] += 4;
		
		VFSObject obj;
		
		switch(type) {
			case T_FILE:
				byte[] data = new byte[size];
				in.readFully(data);
				offset[0] += data.length;
				
				obj = factory.createFile(name, data, userType, fileType, parent);
				obj.getVFSAttributes().setOffset(dataOffset);
				
				break;
			case T_DIR:
				obj = factory.createDirectory(name, userType, fileType, parent);
				obj.getVFSAttributes().setOffset(dataOffset);
				
				for (int i = 0; i < size; i++) {
					readTree((VDirectory)obj, factory, in, offset);
				}
				
				break;
			case T_LINK:
				// Set the link name to the target's offset for now; we'll scan
				// through later to find the actual path
				obj = factory.createSymlink(name, "" + dataOffset, userType,
				                            fileType, parent);
				
				break;
			default:
				throw new IOException("Invalid VFS node type: " + type);
		}
		
		return obj;
	}
	
	/**
	 * Calculates object offsets. This must be performed on the tree before
	 * calling {@link #writeTree(VFSObject, DataOutputStream)}. This will also
	 * verify that all {@link VSymlink} targets are {@link VFile}s, throwing an
	 * {@link IllegalArgumentException} if that is not the case.
	 * 
	 * @param obj The current object. This should be the root object when
	 *   calling from outside.
	 * @param offset The current offset.
	 * @return The offset after the tree rooted at <tt>obj</tt> has been
	 *   processed.
	 */
	protected int calculateOffsets(VFSObject obj, int offset)
	{
		// Store the offset
		obj.getVFSAttributes().setOffset(offset);
		
		// int: VFS type
		offset += 4;
		
		// int: user type
		offset += 4;
		
		try {
			// string: object name
			offset += writeString(((FSObject)obj).getName(), null);
			
			// string: file type
			offset += writeString(obj.getVFSAttributes().getFileType(), null);
		}
		catch (IOException ioe) {
			// This should not happen
			throw new RuntimeException("Unexpected IOException: " +
			                           ioe.getMessage(), ioe);
		}
		
		// int: Length
		offset += 4;
		
		// Data
		if (obj instanceof VFile) {
			offset += ((VFile)obj).getSize();
		}
		else if (obj instanceof VDirectory) {
			for (FSObject child : (VDirectory)obj) {
				offset += calculateOffsets((VFSObject)child, offset);
			}
		}
		else if (obj instanceof VSymlink) {
			// No data to calculate, but make sure that the target is a VFile
			VSymlink lnk = (VSymlink)obj;
			FSObject target = lnk.getLinkTarget();
			if (target == null) {
				throw new IllegalArgumentException("Target of VSymlink " +
				                                   lnk.getPath() +
				                                   " -> " + lnk.getLinkName() +
				                                   "does not exist");
			}
			else if ( ! (target instanceof VFile)) {
				throw new IllegalArgumentException("VSymlink " + lnk.getPath() +
				                                   " -> " + lnk.getLinkName() +
				                                   " must point at a VFile, but instead points at a " +
				                                   target.getClass().getName());
			}
		}
		else {
			throw new RuntimeException("Unexpected child of VDirectory: " +
			                           obj.getClass().getName());
		}
		
		return offset;
	}
	
	/**
	 * Write the filesystem tree to a stream.
	 * 
	 * @param obj The current object being written. If it is a directory, this
	 *   will recurse through its children, writing each of them as well.
	 * @param out The stream to write to.
	 * @throws IOException If there was a problem writing to the stream.
	 */
	protected void writeTree(VFSObject obj, DataOutputStream out)
		throws IOException
	{
		FSObject fsobj = (FSObject)obj;
		
		out.writeInt(obj.getVFSAttributes().getVFSType());
		out.writeInt(obj.getVFSAttributes().getUserType());
		writeString(fsobj.getName(), out);
		writeString(obj.getVFSAttributes().getFileType(), out);
		
		if (obj instanceof VFile) {
			// Size is the file size
			out.writeInt(((VFile)obj).getSize());
			
			// Next comes the file data
			out.write(((VFile)obj).getData());
		}
		else if (obj instanceof VDirectory) {
			// Size is the number of children
			out.writeInt(((VDirectory)obj).getChildCount());
			
			// Next come the children
			for (FSObject child : (VDirectory)obj) {
				writeTree((VFSObject)child, out);
			}
		}
		else if (obj instanceof VSymlink) {
			// Size is the offset of the file that this symlink points to
			// The type of the target was tested by calculateOffsets, so it is
			// safe to perform a blind cast here
			VFile target = (VFile)((VSymlink)obj).getLinkTarget();
			out.writeInt(target.getVFSAttributes().getOffset());
		}
	}
	
	/**
	 * A filesystem which has just been read has offsets instead of target paths
	 * for symlinks, this function fixes that.
	 * 
	 * @param root The root of the tree to search for symlinks to fix.
	 */
	private void fixSymlinks(VDirectory root)
	{
		// Search each child of root to see if it is a symlink, and rectify its
		// link name if it is
		for (FSObject child : root) {
			if (child instanceof VFile) {
				// Do nothing
			}
			else if (child instanceof VDirectory) {
				// Search its children
				fixSymlinks((VDirectory)child);
			}
			else if (child instanceof VSymlink) {
				// Find its target and set the path
				int offset = Integer.parseInt(((VSymlink)child).getLinkName());
				VFSObject target = findTarget((VFSObject)root.getRoot(), offset);
				
				if (target == null) {
					// Remove the symlink, since its target makes no sense
					child.setParent(null);
					
					logger.warn("Target of link " + child.getPath() +
					            " (" + offset + ") not found, symlink removed");
				}
				else {
					((VSymlink)child).setLinkName(((FSObject)target).getPath());
				}
			}
		}
	}
	
	/**
	 * Search for an object with the give offset. The intended use is to search
	 * for Symlink targets, which gives this function its name.
	 * 
	 * @param obj The object to check. If it is a VDirectory, its descendants
	 *   will also be checked.
	 * @param offset The offset to look for.
	 * @return The object with the specified offset, or <tt>null</tt> if not
	 *   found.
	 */
	private VFSObject findTarget(VFSObject obj, int offset)
	{
		// Search for an object beneath root with the specified offset
		if (obj.getVFSAttributes().getOffset() == offset) {
			// Match!
			return obj;
		}
		else if (obj instanceof VDirectory) {
			// Search its children
			for (FSObject child : (VDirectory)obj) {
				VFSObject found = findTarget((VFSObject)child, offset);
				if (found != null) {
					return found;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Get the root of the filesystem.
	 */
	public VDirectory getRoot()
	{
		return root;
	}
	
	/**
	 * Set the root of the filesystem.
	 */
	public void setRoot(VDirectory root)
	{
		this.root = root;
	}
	
	/**
	 * Read a length-encoded (4-byte length) string.
	 * 
	 * @param in The input stream to read from.
	 * @param offset The number of bytes read will be added to [0].
	 * @return The string read.
	 * @throws IOException If there was a problem reading from the stream.
	 */
	protected static String readString(DataInputStream in, int[] offset)
		throws IOException
	{
		if (offset == null || offset.length < 1) {
			offset = new int[1];
		}
		
		int len = in.readInt();
		offset[0] += 4;
		
		byte[] data = new byte[len];
		in.readFully(data);
		offset[0] += data.length;
		
		return new String(data);
	}
	
	/**
	 * Write a length-encoded (4-byte length) string.
	 * 
	 * @param s The string to write.
	 * @param out The stream to write to, or <tt>null</tt> to only calculate the
	 *   number of bytes which would have been written to the stream.
	 * @return The number of bytes written to the stream.
	 * @throws IOException If there was a problem writing to the stream.
	 */
	protected static int writeString(String s, DataOutputStream out)
		throws IOException
	{
		int length = 0;
		
		if (out != null) {
			out.writeInt(s.length());
		}
		length += 4;
		
		byte[] bytes = s.getBytes();
		if (out != null) {
			out.write(bytes);
		}
		length += bytes.length;
		
		return length;
	}
	
	/**
	 * Translate a user type value into a string. Subclasses should override
	 * this.
	 * 
	 * @param userType The user type to translate.
	 * @return A descriptive name for that user type.
	 */
	public String translateUserType(int userType)
	{
		return "Unknown (" + userType + ")";
	}
	
	/**
	 * Translate an object type value into a string. Subclasses may override
	 * this, but it would not make much sense to do so.
	 * 
	 * @param objectType The object type to translate.
	 * @return A descriptive name for that object type.
	 */
	public String translateObjectType(int objectType)
	{
		String ans;
		switch (objectType) {
			case T_FILE:
				ans = "File";
				break;
			case T_DIR:
				ans = "Directory";
				break;
			case T_LINK:
				ans = "Link";
				break;
			default:
				ans = "Unknown";
		}
		
		return ans;
	}
}

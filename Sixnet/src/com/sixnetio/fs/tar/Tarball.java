/*
 * Tarball.java
 *
 * Represents and provides operations for tarballs
 *
 * Jonathan Pearson
 * July 30, 2008
 *
 */

package com.sixnetio.fs.tar;

import static org.apache.tools.tar.TarConstants.*;

import java.io.*;
import java.security.MessageDigest;
import java.util.*;

import org.apache.tools.tar.*;

import com.sixnetio.fs.generic.*;
import com.sixnetio.fs.generic.File;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;

/**
 * Represents a tarball, and provides operations for acting on its contents.
 * 
 * @author Jonathan Pearson
 *
 */
public class Tarball
{
	/**
	 * Since the TarEntry class is so dysfunctional, this performs the same task
	 * but does it better.
	 *
	 * @author Jonathan Pearson
	 */
	private static class TarEntryX
	{
		public String name;
		public int mode;
		public int userID;
		public int groupID;
		public long size;
		public long modtime;
		public int iModtime;
		
		public byte linkFlag;
		public String linkName;
		public String magic;
		public String userName;
		public String groupName;
		public int devMajor;
		public int devMinor;
		
		public byte[] data;
		
		/**
		 * Build a TarEntryX from a TarEntry.
		 * 
		 * @param entry The entry.
		 * @param in The stream providing the data for the entry.
		 * @throws IOException If there was a problem reading from the stream.
		 */
		public TarEntryX(TarEntry entry, InputStream in)
			throws IOException
		{
			byte[] buffer = new byte[TarConstants.NAMELEN    +
			                         TarConstants.MODELEN    +
			                         TarConstants.UIDLEN     +
			                         TarConstants.GIDLEN     +
			                         TarConstants.SIZELEN    +
			                         TarConstants.MODTIMELEN +
			                         TarConstants.CHKSUMLEN  +
			                         1                       + // Link flag
			                         TarConstants.NAMELEN    +
			                         TarConstants.MAGICLEN   +
			                         TarConstants.UNAMELEN   +
			                         TarConstants.GNAMELEN   +
			                         TarConstants.DEVLEN     +
			                         TarConstants.DEVLEN];
			
			// Read out the current data
			entry.writeEntryHeader(buffer);
			
			// Pull out the data into our own fields
			int offset = 0;
			
			this.name = TarUtils.parseName(buffer, offset, NAMELEN).toString();
			offset += NAMELEN;
			
			this.mode = (int) TarUtils.parseOctal(buffer, offset, MODELEN);
			offset += MODELEN;
			
			this.userID = (int) TarUtils.parseOctal(buffer, offset, UIDLEN);
			offset += UIDLEN;
			
			this.groupID = (int) TarUtils.parseOctal(buffer, offset, GIDLEN);
			offset += GIDLEN;
			
			this.size = TarUtils.parseOctal(buffer, offset, SIZELEN);
			offset += SIZELEN;
			
			this.modtime = TarUtils.parseOctal(buffer, offset, MODTIMELEN);
			this.iModtime = (int)((this.modtime / 1000) & 0xffffffff);
			offset += MODTIMELEN;
			
			offset += CHKSUMLEN;
			
			this.linkFlag = buffer[offset++];
			
			this.linkName = TarUtils.parseName(buffer, offset, NAMELEN).toString();
			offset += NAMELEN;
			
			this.magic = TarUtils.parseName(buffer, offset, MAGICLEN).toString();
			offset += MAGICLEN;
			
			this.userName = TarUtils.parseName(buffer, offset, UNAMELEN).toString();
			offset += UNAMELEN;
			
			this.groupName = TarUtils.parseName(buffer, offset, GNAMELEN).toString();
			offset += GNAMELEN;
			
			this.devMajor = (int) TarUtils.parseOctal(buffer, offset, DEVLEN);
			offset += DEVLEN;
			
			this.devMinor = (int) TarUtils.parseOctal(buffer, offset, DEVLEN);
			
			if (size > Integer.MAX_VALUE) {
				throw new IOException("Tar file data too large for " + name);
			}
			
			data = new byte[(int)size];
			int off = 0;
			while (off < data.length) {
				int read = in.read(data, off, data.length - off);
				
				if (read == -1) {
					throw new IOException("Unable to read data: Unexpected end of stream");
				}
				
				off += read;
			}
		}
		
		/**
		 * Build a TarEntryX from a filesystem object. The object must implement
		 * {@link TarObject}.
		 * 
		 * @param obj The object to build from.
		 */
		public TarEntryX(FSObject obj)
		{
			// Another view of the object
			TarObject tObj = (TarObject)obj;
			
			this.name = obj.getPath();
			this.mode = obj.getMode();
			this.userID = obj.getUID() & 0xffff;
			this.groupID = obj.getGID() & 0xffff;
			this.modtime = obj.getModTime() * 1000L;
			this.iModtime = obj.getModTime();
			this.magic = TarballFSObjectFactory.MAGIC;
			this.userName = tObj.getTarAttributes().getUserName();
			this.groupName = tObj.getTarAttributes().getGroupName();
			
			this.size = 0;
			this.data = new byte[0];
			this.linkName = "";
			this.devMajor = 0;
			this.devMinor = 0;
			
			if (obj instanceof Directory) {
				this.name += "/";
				this.linkFlag = LF_DIR;
			}
			else if (obj instanceof File) {
				this.size = ((File)obj).getSize() & 0xffffffff;
				this.data = ((File)obj).getData();
				this.linkFlag = LF_NORMAL;
			}
			else if (obj instanceof Symlink) {
				this.linkName = ((Symlink)obj).getLinkName();
				this.linkFlag = LF_SYMLINK;
			}
			else if (obj instanceof CharDevice) {
				this.devMajor = ((Device)obj).getMajor() & 0xff;
				this.devMinor = ((Device)obj).getMinor() & 0xff;
				this.linkFlag = LF_CHR;
			}
			else if (obj instanceof BlockDevice) {
				this.devMajor = ((Device)obj).getMajor() & 0xff;
				this.devMinor = ((Device)obj).getMinor() & 0xff;
				this.linkFlag = LF_BLK;
			}
			else if (obj instanceof FIFO) {
				this.linkFlag = LF_FIFO;
			}
			
			// Everything else gets ignored, since we can't handle it
		}
		
		/**
		 * Write an entry to a tar output stream.
		 * 
		 * @param out The stream to write to.
		 * @throws IOException If the stream has a problem.
		 */
		public void write(TarOutputStream out)
			throws IOException
		{
			byte[] buffer = new byte[TarConstants.NAMELEN    +
			                         TarConstants.MODELEN    +
			                         TarConstants.UIDLEN     +
			                         TarConstants.GIDLEN     +
			                         TarConstants.SIZELEN    +
			                         TarConstants.MODTIMELEN +
			                         TarConstants.CHKSUMLEN  +
			                         1                       + // Link flag
			                         TarConstants.NAMELEN    +
			                         TarConstants.MAGICLEN   +
			                         TarConstants.UNAMELEN   +
			                         TarConstants.GNAMELEN   +
			                         TarConstants.DEVLEN     +
			                         TarConstants.DEVLEN];
			
			int offset = 0;
			
			offset = TarUtils.getNameBytes(new StringBuffer(this.name), buffer, offset, NAMELEN);
			offset = TarUtils.getOctalBytes(this.mode, buffer, offset, MODELEN);
			offset = TarUtils.getOctalBytes(this.userID & 0xffff, buffer, offset, UIDLEN);
			offset = TarUtils.getOctalBytes(this.groupID & 0xffff, buffer, offset, GIDLEN);
			offset = TarUtils.getLongOctalBytes(this.size, buffer, offset, SIZELEN);
			offset = TarUtils.getLongOctalBytes(this.modtime, buffer, offset, MODTIMELEN);
			
			int csOffset = offset;
			
			for (int c = 0; c < CHKSUMLEN; ++c) {
				buffer[offset++] = (byte) ' ';
			}
			
			buffer[offset++] = this.linkFlag;
			offset = TarUtils.getNameBytes(new StringBuffer(this.linkName), buffer, offset, NAMELEN);
			offset = TarUtils.getNameBytes(new StringBuffer(this.magic), buffer, offset, MAGICLEN);
			offset = TarUtils.getNameBytes(new StringBuffer(this.userName), buffer, offset, UNAMELEN);
			offset = TarUtils.getNameBytes(new StringBuffer(this.groupName), buffer, offset, GNAMELEN);
			offset = TarUtils.getOctalBytes(this.devMajor & 0xff, buffer, offset, DEVLEN);
			offset = TarUtils.getOctalBytes(this.devMinor & 0xff, buffer, offset, DEVLEN);
			
			while (offset < buffer.length) {
				buffer[offset++] = 0;
			}
			
			long chk = TarUtils.computeCheckSum(buffer);
			TarUtils.getCheckSumOctalBytes(chk, buffer, csOffset, CHKSUMLEN);
			
			TarEntry entry = new TarEntry(buffer);
			
			out.putNextEntry(entry);
			if ((linkFlag & LF_NORMAL) != 0 ||
			    (linkFlag & LF_OLDNORM) != 0 ||
			    (linkFlag & LF_CONTIG) != 0) {
				
				out.write(data);
			}
			out.closeEntry();
		}
	}
	
	private final TarballFSObjectFactory factory = new TarballFSObjectFactory();
	private TDirectory root;
	
	/**
	 * Construct a new, empty tarball.
	 */
	public Tarball()
	{
		root = factory.createDirectory("", null);
	}
	
	/**
	 * Construct a new tarball from a byte array.
	 * 
	 * @param data The contents of the tarball.
	 * @throws IOException If there is a problem parsing the data as a tarball.
	 */
	public Tarball(byte[] data)
		throws IOException
	{
		root = untar(data, factory);
	}
	
	/**
	 * Construct a new tarball from an input stream.
	 * 
	 * @param in An input stream used to read the tarball data.
	 * @throws IOException If there is a problem reading or parsing the data as
	 *   a tarball.
	 */
	public Tarball(InputStream in)
		throws IOException
	{
		root = untar(Utils.bytesToEOF(in), factory);
	}
	
	/**
	 * Translate a byte array of tarball data into a filesystem.
	 * 
	 * @param data The tarball data.
	 * @return The root directory of the filesystem.
	 * @throws IOException If there is a problem parsing the data as a tarball.
	 */
	public static TDirectory untar(byte[] data, TarballFSObjectFactory factory)
		throws IOException
	{
		TDirectory root = null;
		
		// Make a table of the tar file contents
		TarInputStream in = new TarInputStream(new ByteArrayInputStream(data));
		Set<TarEntryX> entries = new LinkedHashSet<TarEntryX>();
		
		{
			TarEntry entry;
			while ((entry = in.getNextEntry()) != null) {
				entries.add(new TarEntryX(entry, in));
			}
		}
		
		in.close();
		
		root = factory.createDirectory("", null);
		
		// Sift through the files, generating a filesystem
		boolean changed = true;
		boolean makeParents = false; // Set to true if we cannot complete
		while (changed) {
			changed = false;
			for (Iterator<TarEntryX> itEntries = entries.iterator(); itEntries.hasNext(); ) {
				TarEntryX entry = itEntries.next();
				
				// Parse the path of this entry, to see if the parent is
				// available
				String path = entry.name;
				
				// Cut off the trailing '/' for directories
				if (path.endsWith("/")) {
					path = path.substring(0, path.length() - 1);
				}
				
				String name;
				TDirectory parent;
				if ( ! makeParents && path.indexOf('/') != -1) {
					// If there is a '/', then we need to traverse
					// subdirectories to get there
					name = path.substring(path.lastIndexOf('/') + 1);
					path = path.substring(0, path.lastIndexOf('/'));
					parent = (TDirectory)root.locate(path);
				}
				else if (makeParents && path.indexOf('/') != -1) {
					// Create the necessary parent directories
					name = path.substring(path.lastIndexOf('/') + 1);
					path = path.substring(0, path.lastIndexOf('/'));
					
					String remainingPath = path;
					parent = root;
					while (remainingPath.length() > 0) {
						if (remainingPath.startsWith("/")) {
							remainingPath = remainingPath.substring(1);
						}
						else {
							String piece;
							if (remainingPath.indexOf('/') != -1) {
								piece = remainingPath.substring(0, remainingPath.indexOf('/'));
								remainingPath = remainingPath.substring(remainingPath.indexOf('/') + 1);
							}
							else {
								piece = remainingPath;
								remainingPath = "";
							}
							
							// Create this piece if necessary, otherwise just
							// step onto it
							FSObject child = parent.locate(piece);
							if (child != null) {
								if (child instanceof TDirectory) {
									parent = (TDirectory)child;
								}
								else {
									throw new IOException(String.format("Parent of '%s/%s' is not a directory",
									                                    path, name));
								}
							}
							else {
								parent = factory.createDirectory(piece,
								                                 (short)-1, "",
								                                 (short)-1, "",
								                                 0777, 0,
								                                 parent.getTarAttributes().getMagic(),
								                                 parent.getTarAttributes().getLinkFlag(),
								                                 parent);
							}
						}
					}
				}
				else {
					// No '/' means that the target is in the root directory
					name = path;
					parent = root;
				}
				
				// If we have not yet created the parent, don't bother trying to
				// create the child yet
				if (parent != null) {
					if ((entry.linkFlag & LF_DIR) == LF_DIR) {
						// Directory
						factory.createDirectory(name,
						                        (short)entry.userID, entry.userName,
						                        (short)entry.groupID, entry.groupName,
						                        entry.mode, entry.iModtime,
						                        entry.magic, entry.linkFlag,
						                        parent);
						
						changed = true;
					}
					else if ((entry.linkFlag & LF_NORMAL) == LF_NORMAL ||
					         (entry.linkFlag & LF_OLDNORM) == LF_OLDNORM ||
					         (entry.linkFlag & LF_CONTIG) == LF_CONTIG) {
						// File
						factory.createFile(name, entry.data,
						                   (short)entry.userID, entry.userName,
						                   (short)entry.groupID, entry.groupName,
						                   entry.mode, entry.iModtime,
						                   entry.magic, entry.linkFlag,
						                   parent);
						
						changed = true;
					}
					else if ((entry.linkFlag & LF_SYMLINK) == LF_SYMLINK) {
						// Symlink
						factory.createSymlink(name, entry.linkName,
						                      (short)entry.userID, entry.userName,
						                      (short)entry.groupID, entry.groupName,
						                      entry.mode, entry.iModtime,
						                      entry.magic, entry.linkFlag,
						                      parent);
						
						changed = true;
					}
					else if ((entry.linkFlag & LF_CHR) == LF_CHR) {
						// Character special
						factory.createCharDevice(name,
						                         (byte)entry.devMajor,
						                         (byte)entry.devMinor,
						                         (short)entry.userID, entry.userName,
						                         (short)entry.groupID, entry.groupName,
						                         entry.mode, entry.iModtime,
						                         entry.magic, entry.linkFlag,
						                         parent);
						
						changed = true;
					}
					else if ((entry.linkFlag & LF_BLK) == LF_BLK) {
						// Block special
						factory.createBlockDevice(name,
						                          (byte)entry.devMajor,
						                          (byte)entry.devMinor,
						                          (short)entry.userID, entry.userName,
						                          (short)entry.groupID, entry.groupName,
						                          entry.mode, entry.iModtime,
						                          entry.magic, entry.linkFlag,
						                          parent);
						
						changed = true;
					}
					else if ((entry.linkFlag & LF_FIFO) == LF_FIFO) {
						// FIFO
						factory.createFIFO(name,
						                   (short)entry.userID, entry.userName,
						                   (short)entry.groupID, entry.groupName,
						                   entry.mode, entry.iModtime,
						                   entry.magic, entry.linkFlag,
						                   parent);
						
						changed = true;
					}
					
					// Sockets and whitespace files are not available in this
					// format
					
					if (changed) {
						itEntries.remove();
					}
				}
			}
			
			if ( ! changed && ! makeParents && entries.size() > 0) {
				// This tarball does not contain the parents of some of its
				// files/directories, so create those parents
				makeParents = true;
				changed = true;
			}
		}
		
		return root;
	}
	
	/**
	 * Convert a filesystem into a tarball. Any objects in this filesystem
	 * with a UID or GID of -1 will not be included in the tarball.
	 * 
	 * @param root The root of the filesystem to tar up. The root directory does
	 *   not get its own entry in the tarball.
	 * @return The tarball data containing the filesystem.
	 * @throws IOException If there is a problem tarring up the data.
	 */
	public static byte[] tar(TDirectory root)
		throws IOException
	{
		// Write the tar file into a byte array
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		TarOutputStream out = new TarOutputStream(byteOut);
		
		tar(root, out);
		
		out.finish();
		out.close();
		
		return byteOut.toByteArray();
	}
	
	private static void tar(Directory root, TarOutputStream out)
		throws IOException
	{
		for (FSObject f : root) {
			if (f.getUID() != (short)-1 && f.getGID() != (short)-1) {
				// Tarballs are not required to contain all parents of a given
				// object. If the UID/GID are -1, this indicates that the
				// parents of this object were likely injected while reading a
				// tarball so that the filesystem would be coherent, but should
				// not be written back out
				// If the user wanted these objects in the final tarball, he
				// would have given them a UID/GID
				TarEntryX entry = new TarEntryX(f);
				entry.write(out);
			}
			
			if (f instanceof Directory) {
				tar((Directory)f, out);
			}
		}
	}
	
	/**
	 * Write this tarball out to a stream.
	 * 
	 * @param out The stream to write on. It will be flushed after writing.
	 * @throws IOException If there is an error writing to the stream.
	 */
	public void toStream(OutputStream out)
		throws IOException
	{
		TarOutputStream tout = new TarOutputStream(out);
		tar(getRoot(), tout);
		
		tout.finish();
		tout.flush();
	}
	
	/**
	 * Get the data of this tarball as a byte array. This is a convenience
	 * method for tar(getRoot()).
	 * 
	 * @return The data of this tarball as a byte array.
	 * @throws IOException If there is a problem tarring up the data.
	 */
	public byte[] getData()
		throws IOException
	{
		return tar(getRoot());
	}
	
	/**
	 * Get the root of the filesystem laid out by this tarball.
	 */
	public TDirectory getRoot()
	{
		return root;
	}
	
	/**
	 * Set the root of the filesystem laid out by this tarball.
	 */
	public void setRoot(TDirectory root)
	{
		this.root = root;
	}
	
	public String generateMD5()
	{
		// Get an instance of the MD5 digest calculator
		MessageDigest digest = Utils.getMD5Digester();
		
		// Run through the files, adding pieces to the digest
		byte[] tempBuf = new byte[4];
		
		Set<FSObject> objects = getRoot().getAllObjects();
		FSObject[] fileArray = objects.toArray(new FSObject[objects.size()]);
		Arrays.sort(fileArray);
		
		for (int i = 0; i < fileArray.length; i++) {
			// Two different views of the same object
			FSObject fsObj = fileArray[i];
			TarObject tObj = (TarObject)fileArray[i];
			
			digest.update(fsObj.getPath().getBytes());
			if (fsObj instanceof Symlink) {
				digest.update(((Symlink)fsObj).getLinkName().getBytes());
			}
			
			if (tObj.getTarAttributes().getUserName() != null) {
				digest.update(tObj.getTarAttributes().getUserName().getBytes());
			}
			
			if (tObj.getTarAttributes().getGroupName() != null) {
				digest.update(tObj.getTarAttributes().getGroupName().getBytes());
			}
			
			Conversion.intToBytes(tempBuf, 0, fsObj.getMode());
			digest.update(tempBuf);
			
			if (fsObj instanceof Device) {
				Conversion.intToBytes(tempBuf, 0, ((Device)fsObj).getMajor() & 0xffff);
				digest.update(tempBuf);
				
				Conversion.intToBytes(tempBuf, 0, ((Device)fsObj).getMinor() & 0xffff);
				digest.update(tempBuf);
			}
			
			if (fsObj instanceof File) {
				digest.update(((File)fsObj).getData());
			}
		}
		
		String md5 = Conversion.bytesToHex(digest.digest());
		return md5;
	}
}

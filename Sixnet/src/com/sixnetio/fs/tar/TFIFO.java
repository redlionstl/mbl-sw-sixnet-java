/*
 * TFIFO.java
 *
 * Provides FIFO functionality for tarballs.
 *
 * Jonathan Pearson
 * November 12, 2009
 *
 */

package com.sixnetio.fs.tar;

import com.sixnetio.fs.generic.Directory;
import com.sixnetio.fs.generic.FIFO;

public class TFIFO
	extends FIFO
	implements TarObject
{
	private final TarAttributes tarAttrs;
	
	public TFIFO(String name, short uid, String userName,
	             short gid, String groupName, int mode, int modtime,
	             String magic, byte linkFlag, TDirectory parent)
	{
		super(name, uid, gid, mode, modtime, parent);
		
		tarAttrs = new TarAttributes(userName, groupName, magic, linkFlag);
	}
	
	@Override
	public void setParent(Directory parent)
	{
		if ( ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Tarball object parent must be a Tarball directory");
		}
		
		super.setParent(parent);
	}
	
	@Override
	public TarAttributes getTarAttributes()
	{
		return tarAttrs;
	}
}

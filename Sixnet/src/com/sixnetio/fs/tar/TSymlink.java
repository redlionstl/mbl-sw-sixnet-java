/*
 * TSymlink.java
 *
 * Provides symlink functionality for tarball symlinks.
 *
 * Jonathan Pearson
 * November 12, 2009
 *
 */

package com.sixnetio.fs.tar;

import com.sixnetio.fs.generic.Directory;
import com.sixnetio.fs.generic.Symlink;

public class TSymlink
	extends Symlink
	implements TarObject
{
	private final TarAttributes tarAttrs;
	
	protected TSymlink(String name, String linkName, short uid, String userName,
	                   short gid, String groupName, int mode, int modtime,
	                   String magic, byte linkFlag, TDirectory parent)
	{
		super(name, linkName, uid, gid, mode, modtime, parent);
		
		tarAttrs = new TarAttributes(userName, groupName, magic, linkFlag);
	}
	
	@Override
	public void setParent(Directory parent)
	{
		if ( ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Tarball object parent must be a Tarball directory");
		}
		
		super.setParent(parent);
	}
	
	@Override
	public TarAttributes getTarAttributes()
	{
		return tarAttrs;
	}
}

/*
 * TarballFSObjectFactory.java
 *
 * Builds Tarball-specific FSObject subclasses.
 *
 * Jonathan Pearson
 * November 11, 2009
 *
 */

package com.sixnetio.fs.tar;

import org.apache.log4j.Logger;
import org.apache.tools.tar.TarConstants;

import com.sixnetio.fs.generic.*;
import com.sixnetio.util.Utils;

public class TarballFSObjectFactory
	implements FSObjectFactory
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Standard 'magic' string for objects in tarballs. */
	public static final String MAGIC = "ustar";
	
	/**
	 * Create a block special device.
	 * 
	 * @param name The name of the device.
	 * @param major The major number.
	 * @param minor The minor number.
	 * @param uid The owner's UID.
	 * @param userName The name of the owner.
	 * @param gid The group's GID.
	 * @param groupName The name of the group.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param magic The magic string.
	 * @param linkFlag The link flag.
	 * @param parent The directory containing this device. This object will be
	 *   added to the parent's contents.
	 * @return A new block special device.
	 */
	public TBlockDevice createBlockDevice(String name, byte major, byte minor,
	                                      short uid, String userName,
	                                      short gid, String groupName, int mode,
	                                      int modtime, String magic,
	                                      byte linkFlag, TDirectory parent)
	{
		TBlockDevice obj = new TBlockDevice(name, major, minor, uid, userName,
		                                    gid, groupName, mode, modtime,
		                                    magic, linkFlag, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public TBlockDevice createBlockDevice(String name, byte major, byte minor,
	                                      Directory parent)
	{
		if ( ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Parent of a Tarball object must be a Tarball directory: " +
			                                   parent.getClass().getName());
		}
		
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_BLK; // Turn on the block type
		
		int modtime = FSObject.now();
		
		TarAttributes attrs = ((TDirectory)parent).getTarAttributes();
		return createBlockDevice(name, major, minor, parent.getUID(),
		                         attrs.getUserName(), parent.getGID(),
		                         attrs.getGroupName(), mode, modtime,
		                         attrs.getMagic(), TarConstants.LF_BLK,
		                         (TDirectory)parent);
	}
	
	/**
	 * Create a character special device.
	 * 
	 * @param name The name of the device.
	 * @param major The major number.
	 * @param minor The minor number.
	 * @param uid The owner's UID.
	 * @param userName The name of the owner.
	 * @param gid The group's GID.
	 * @param groupName The name of the group.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param magic The magic string.
	 * @param linkFlag The link flag.
	 * @param parent The directory containing this device. This object will be
	 *   added to the parent's contents.
	 * @return A new character special device.
	 */
	public TCharDevice createCharDevice(String name, byte major, byte minor,
	                                   short uid, String userName,
	                                   short gid, String groupName, int mode,
	                                   int modtime, String magic, byte linkFlag,
	                                   TDirectory parent)
	{
		TCharDevice obj = new TCharDevice(name, major, minor, uid, userName,
		                                  gid, groupName, mode, modtime,
		                                  magic, linkFlag, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public TCharDevice createCharDevice(String name, byte major, byte minor,
	                                    Directory parent)
	{
		if ( ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Parent of a Tarball object must be a Tarball directory: " +
			                                   parent.getClass().getName());
		}
		
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_CHR; // Turn on the character type
		
		int modtime = FSObject.now();
		
		TarAttributes attrs = ((TDirectory)parent).getTarAttributes();
		return createCharDevice(name, major, minor, parent.getUID(),
		                        attrs.getUserName(), parent.getGID(),
		                        attrs.getGroupName(), mode, modtime,
		                        attrs.getMagic(), TarConstants.LF_CHR,
		                        (TDirectory)parent);
	}
	
	/**
	 * Create a new directory.
	 * 
	 * @param name The name of the directory.
	 * @param uid The owner's UID.
	 * @param userName The name of the owner.
	 * @param gid The group's GID.
	 * @param groupName The name of the group.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param magic The magic string.
	 * @param linkFlag The link flag.
	 * @param parent The directory containing this directory, or <tt>null</tt>
	 *   if this is the root directory. This object will be added to the
	 *   parent's contents if not <tt>null</tt>.
	 * @return A new directory.
	 */
	public TDirectory createDirectory(String name, short uid, String userName,
	                                  short gid, String groupName, int mode,
	                                  int modtime, String magic, byte linkFlag,
	                                  TDirectory parent)
	{
		TDirectory obj = new TDirectory(name, uid, userName, gid, groupName,
		                                mode, modtime, magic, linkFlag, parent);
		if (parent != null) {
			parent.addChild(obj);
		}
		return obj;
	}
	
	@Override
	public TDirectory createDirectory(String name, Directory parent)
	{
		if (parent != null && ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Parent of a Tarball object must be a Tarball directory: " +
			                                   parent.getClass().getName());
		}
		
		int mode = 0775;
		if (parent != null) {
			mode = parent.getMode();
		}
		
		// Don't need to make any changes to the mode
		
		int modtime = FSObject.now();
		
		if (parent == null) {
			return createDirectory(name, (short)0, "root", (short)0, "root",
			                       mode, modtime, MAGIC, TarConstants.LF_DIR,
			                       null);
		}
		else {
			TarAttributes attrs = ((TDirectory)parent).getTarAttributes();
			return createDirectory(name, parent.getUID(), attrs.getUserName(),
			                       parent.getGID(), attrs.getGroupName(), mode,
			                       modtime, attrs.getMagic(),
			                       TarConstants.LF_DIR, (TDirectory)parent);
		}
	}
	
	/**
	 * Create a new FIFO.
	 * 
	 * @param name The name of the FIFO.
	 * @param uid The owner's UID.
	 * @param userName The name of the owner.
	 * @param gid The group's GID.
	 * @param groupName The name of the group.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param magic The magic string.
	 * @param linkFlag The link flag.
	 * @param parent The directory containing this FIFO. This object will be
	 *   added to the parent's contents.
	 * @return A new FIFO.
	 */
	public TFIFO createFIFO(String name, short uid, String userName,
	                        short gid, String groupName, int mode, int modtime,
	                        String magic, byte linkFlag, TDirectory parent)
	{
		TFIFO obj = new TFIFO(name, uid, userName, gid, groupName, mode,
		                      modtime, magic, linkFlag, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public TFIFO createFIFO(String name, Directory parent)
	{
		if ( ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Parent of a Tarball object must be a Tarball directory: " +
			                                   parent.getClass().getName());
		}
		
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_FIFO; // Turn on the FIFO type
		
		int modtime = FSObject.now();
		
		TarAttributes attrs = ((TDirectory)parent).getTarAttributes();
		return createFIFO(name, parent.getUID(), attrs.getUserName(),
		                  parent.getGID(), attrs.getGroupName(), mode, modtime,
		                  attrs.getMagic(), TarConstants.LF_FIFO,
		                  (TDirectory)parent);
	}
	
	/**
	 * Create a new file.
	 * 
	 * @param name The name of the file.
	 * @param data The data contained in the file.
	 * @param uid The owner's UID.
	 * @param userName The name of the owner.
	 * @param gid The group's GID.
	 * @param groupName The name of the group.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param magic The magic string.
	 * @param linkFlag The link flag.
	 * @param parent The directory containing this file. This object will be
	 *   added to the parent's contents.
	 * @return A new file.
	 */
	public TFile createFile(String name, byte[] data, short uid,
	                        String userName, short gid, String groupName,
	                        int mode, int modtime, String magic, byte linkFlag,
	                        TDirectory parent)
	{
		TFile obj = new TFile(name, data, uid, userName, gid, groupName, mode,
		                      modtime, magic, linkFlag, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public TFile createFile(String name, byte[] data, Directory parent)
	{
		if ( ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Parent of a Tarball object must be a Tarball directory: " +
			                                   parent.getClass().getName());
		}
		
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_REG; // Turn on the file type
		
		int modtime = FSObject.now();
		
		TarAttributes attrs = ((TDirectory)parent).getTarAttributes();
		return createFile(name, data, parent.getUID(), attrs.getUserName(),
		                  parent.getGID(), attrs.getGroupName(), mode, modtime,
		                  attrs.getMagic(), TarConstants.LF_NORMAL,
		                  (TDirectory)parent);
	}
	
	@Override
	public Socket createSocket(String name, Directory parent)
	{
		throw new UnsupportedOperationException("Tarballs do not support sockets.");
	}
	
	/**
	 * Create a new symlink.
	 * 
	 * @param name The name of the symlink.
	 * @param linkName The path (possibly relative) to the target.
	 * @param uid The owner's UID.
	 * @param userName The name of the owner.
	 * @param gid The group's GID.
	 * @param groupName The name of the group.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param magic The magic string.
	 * @param linkFlag The link flag.
	 * @param parent The directory containing this symlink. This object will be
	 *   added to the parent's contents.
	 * @return A new symlink.
	 */
	public TSymlink createSymlink(String name, String linkName, short uid,
	                              String userName, short gid, String groupName,
	                              int mode, int modtime, String magic,
	                              byte linkFlag, TDirectory parent)
	{
		TSymlink obj = new TSymlink(name, linkName, uid, userName, gid,
		                            groupName, mode, modtime, magic, linkFlag,
		                            parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public TSymlink createSymlink(String name, String linkName, Directory parent)
	{
		if ( ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Parent of a Tarball object must be a Tarball directory: " +
			                                   parent.getClass().getName());
		}
		
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_LNK; // Turn on the symlink type
		
		int modtime = FSObject.now();
		
		TarAttributes attrs = ((TDirectory)parent).getTarAttributes();
		return createSymlink(name, linkName, parent.getUID(),
		                     attrs.getUserName(), parent.getGID(),
		                     attrs.getGroupName(), mode, modtime,
		                     attrs.getMagic(), TarConstants.LF_SYMLINK,
		                     (TDirectory)parent);
	}
	
	@Override
	public Whiteout createWhiteout(String name, Directory parent)
	{
		throw new UnsupportedOperationException("Tarballs do not support whiteouts");
	}
	
	@Override
	public FSObject convertObject(FSObject obj, Directory parent)
	{
		if ( ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Parent of a Tarball object must be a Tarball directory: " +
			                                   parent.getClass().getName());
		}
		
		TDirectory tparent = (TDirectory)parent;
		
		if (obj instanceof TBlockDevice) {
			TBlockDevice o = (TBlockDevice)obj;
			return createBlockDevice(o.getName(), o.getMajor(), o.getMinor(),
			                         o.getUID(),
			                         o.getTarAttributes().getUserName(),
			                         o.getGID(),
			                         o.getTarAttributes().getGroupName(),
			                         o.getMode(),
			                         o.getModTime(),
			                         o.getTarAttributes().getMagic(),
			                         o.getTarAttributes().getLinkFlag(),
			                         tparent);
		}
		else if (obj instanceof BlockDevice) {
			BlockDevice o = (BlockDevice)obj;
			return createBlockDevice(o.getName(), o.getMajor(), o.getMinor(),
			                         o.getUID(), "", o.getGID(), "",
			                         o.getMode(), o.getModTime(), MAGIC,
			                         TarConstants.LF_BLK, tparent);
		}
		else if (obj instanceof TCharDevice) {
			TCharDevice o = (TCharDevice)obj;
			return createCharDevice(o.getName(), o.getMajor(), o.getMinor(),
			                        o.getUID(),
			                        o.getTarAttributes().getUserName(),
			                        o.getGID(),
			                        o.getTarAttributes().getGroupName(),
			                        o.getMode(),
			                        o.getModTime(),
			                        o.getTarAttributes().getMagic(),
			                        o.getTarAttributes().getLinkFlag(),
			                        tparent);
		}
		else if (obj instanceof CharDevice) {
			CharDevice o = (CharDevice)obj;
			return createCharDevice(o.getName(), o.getMajor(), o.getMinor(),
			                        o.getUID(), "", o.getGID(), "", o.getMode(),
			                        o.getModTime(), MAGIC, TarConstants.LF_CHR,
			                        tparent);
		}
		else if (obj instanceof TDirectory) {
			TDirectory o = (TDirectory)obj;
			return createDirectory(o.getName(),
			                       o.getUID(),
			                       o.getTarAttributes().getUserName(),
			                       o.getGID(),
			                       o.getTarAttributes().getGroupName(),
			                       o.getMode(),
			                       o.getModTime(),
			                       o.getTarAttributes().getMagic(),
			                       o.getTarAttributes().getLinkFlag(),
			                       tparent);
		}
		else if (obj instanceof Directory) {
			Directory o = (Directory)obj;
			return createDirectory(o.getName(), o.getUID(), "", o.getGID(), "",
			                       o.getMode(), o.getModTime(), MAGIC,
			                       TarConstants.LF_DIR, tparent);
		}
		else if (obj instanceof TFIFO) {
			TFIFO o = (TFIFO)obj;
			return createFIFO(o.getName(),
			                  o.getUID(), o.getTarAttributes().getUserName(),
			                  o.getGID(), o.getTarAttributes().getGroupName(),
			                  o.getMode(), o.getModTime(),
			                  o.getTarAttributes().getMagic(),
			                  o.getTarAttributes().getLinkFlag(),
			                  tparent);
		}
		else if (obj instanceof FIFO) {
			FIFO o = (FIFO)obj;
			return createFIFO(o.getName(), o.getUID(), "", o.getGID(), "",
			                  o.getMode(), o.getModTime(), MAGIC,
			                  TarConstants.LF_FIFO, tparent);
		}
		else if (obj instanceof TFile) {
			TFile o = (TFile)obj;
			return createFile(o.getName(), o.getData(),
			                  o.getUID(), o.getTarAttributes().getUserName(),
			                  o.getGID(), o.getTarAttributes().getGroupName(),
			                  o.getMode(), o.getModTime(),
			                  o.getTarAttributes().getMagic(),
			                  o.getTarAttributes().getLinkFlag(),
			                  tparent);
		}
		else if (obj instanceof File) {
			File o = (File)obj;
			return createFile(o.getName(), o.getData(), o.getUID(), "",
			                  o.getGID(), "", o.getMode(), o.getModTime(),
			                  MAGIC, TarConstants.LF_NORMAL, tparent);
		}
		else if (obj instanceof TSymlink) {
			TSymlink o = (TSymlink)obj;
			return createSymlink(o.getName(), o.getLinkName(),
			                     o.getUID(),
			                     o.getTarAttributes().getUserName(),
			                     o.getGID(),
			                     o.getTarAttributes().getGroupName(),
			                     o.getMode(),
			                     o.getModTime(),
			                     o.getTarAttributes().getMagic(),
			                     o.getTarAttributes().getLinkFlag(),
			                     tparent);
		}
		else if (obj instanceof Symlink) {
			Symlink o = (Symlink)obj;
			return createSymlink(o.getName(), o.getLinkName(), o.getUID(), "",
			                     o.getGID(), "", o.getMode(), o.getModTime(),
			                     MAGIC, TarConstants.LF_NORMAL, tparent);
		}
		else {
			throw new IllegalArgumentException("Tarballs do not support '" +
			                                   obj.getClass().getName() + "'");
		}
	}
	
	@Override
	public TDirectory convertFilesystem(Directory dir)
	{
		TDirectory root = createDirectory(dir.getName(), dir.getUID(), "",
		                                  dir.getGID(), "", dir.getMode(),
		                                  dir.getModTime(), MAGIC,
		                                  TarConstants.LF_DIR, null);
		
		for (FSObject child : dir) {
			convertFilesystem(child, root);
		}
		
		return root;
	}
	
	private void convertFilesystem(FSObject obj, TDirectory root)
	{
		try {
			FSObject conv = convertObject(obj, root);
			
			if (obj instanceof Directory) {
				Directory o = (Directory)obj;
				for (FSObject child : o) {
					convertFilesystem(child, (TDirectory)conv);
				}
			}
		}
		catch (IllegalArgumentException iae) {
			logger.warn(iae.getMessage(), iae);
		}
	}
}

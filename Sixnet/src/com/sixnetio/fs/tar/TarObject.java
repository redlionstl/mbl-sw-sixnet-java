/*
 * TarObject.java
 *
 * Outlines the methods that all tarball FSObjects must implement.
 *
 * Jonathan Pearson
 * November 12, 2009
 *
 */

package com.sixnetio.fs.tar;

public interface TarObject
{
	/**
	 * Get the TAR-specific attributes of this object. Changes to the returned
	 * object affect this object.
	 */
	public TarAttributes getTarAttributes();
}

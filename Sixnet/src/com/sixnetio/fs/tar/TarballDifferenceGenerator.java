/*
 * TarballDifferenceGenerator.java
 *
 * Finds all differences between two tarballs.
 *
 * Jonathan Pearson
 * December 5, 2008
 *
 */

package com.sixnetio.fs.tar;

import java.util.*;

import com.sixnetio.fs.generic.*;
import com.sixnetio.util.Utils;

public class TarballDifferenceGenerator
{
	public static class Difference
	{
		public static enum Type
		{
			/**
			 * The object exists only in the first tarball. The lineDiff field
			 * will be <tt>null</tt>.
			 */
			Deleted,
			
			/**
			 * The object exists only in the second tarball. The lineDiff field
			 * will be <tt>null</tt>.
			 */
			Added,
			
			/**
			 * <p>
			 * The type of the object changed (ex: Symlink -> File). The
			 * lineDiff field will contain a pair of lines like this:
			 * </p>
			 * 
			 * <p>
			 * <code>
			 * Old file type<br />
			 * New file type
			 * </code>
			 * </p>
			 */
			TypeChanged,
			
			/**
			 * <p>
			 * The target of a symlink changed. The lineDiff field will contain
			 * a pair of lines like this:
			 * </p>
			 * 
			 * <p>
			 * <code>
			 * path/to/old/target<br />
			 * path/to/new/target
			 * </code>
			 * </p>
			 */
			TargetChanged,
			
			/**
			 * <p>
			 * The major/minor number(s) of a device changed. The lineDiff
			 * field will contain lines like this:
			 * </p>
			 * 
			 * <p>
			 * <code>
			 * old Major, old Minor<br />
			 * new Major, new Minor
			 * </code>
			 * </p>
			 */
			DeviceChanged,
			
			/**
			 * The contents of a file changed. The lineDiff field will contain
			 * a line-by-line difference of the files if they are not binary; it
			 * will be <tt>null</tt> if either file is detected as being binary.
			 */
			ContentsChanged,
			;
		}
		public final Tarball tb1;
		public final Tarball tb2;
		
		public final String path;
		
		public final Type type;
		
		public final String[] lineDiff;
		
		// If a file is missing from the second tarball, use this constructor
		// Make two of these if the type of a file has changed
		public Difference(Tarball tb1, Tarball tb2, String path,
		                  Type type, String[] lineDiff)
		{
			this.tb1 = tb1;
			this.tb2 = tb2;
			
			this.path = path;
			
			this.type = type;
			if (lineDiff == null) {
				this.lineDiff = null;
			}
			else {
				this.lineDiff = new String[lineDiff.length];
				System.arraycopy(lineDiff, 0, this.lineDiff, 0, lineDiff.length);
			}
		}
	}
	
	public static List<Difference> diff(List<Tarball> tarballs)
	{
		List<Difference> differences = new LinkedList<Difference>();
		
		// Loop through each tarball, comparing its files with the others
		for (Tarball tb1 : tarballs) {
			boolean pastCurrent = false;
			for (Tarball tb2 : tarballs) {
				if (tb1 == tb2) {
					pastCurrent = true;
					continue;
				}
				
				if (!pastCurrent) {
					continue;
				}
				
				differences.addAll(compareTarballs(tb1, tb2));
			}
		}
		
		return differences;
	}
	
	public static List<Difference> compareTarballs(Tarball tb1, Tarball tb2)
	{
		// Build the differences here
		List<Difference> differences = new LinkedList<Difference>();
		
		// Path to object in tarball --> FSObject representing that object
		Map<String, FSObject> fileMap1 =
			new LinkedHashMap<String, FSObject>();
		Map<String, FSObject> fileMap2 =
			new LinkedHashMap<String, FSObject>();
		
		// Build the maps
		for (FSObject obj : tb1.getRoot().getAllObjects()) {
			fileMap1.put(obj.getPath(), obj);
		}
		
		for (FSObject obj : tb2.getRoot().getAllObjects()) {
			fileMap2.put(obj.getPath(), obj);
		}
		
		// Loop through tb1's files
		for (Map.Entry<String, FSObject> entry : fileMap1.entrySet()) {
			String path = entry.getKey();
			
			// Search each other tarball for this file
			FSObject obj2 = fileMap2.get(path);
			
			// If not in the second tarball, add as a missing file
			if (obj2 == null) {
				differences.add(new Difference(tb1, tb2, path, Difference.Type.Deleted, null));
				
				continue;
			}
			
			// Exists in both, compare
			FSObject obj1 = entry.getValue();
			
			if ( ! obj1.getClass().equals(obj2.getClass())) {
				// Different types of objects
				String[] lineDiff = new String[] {
					obj1.getTypeAsString(),
					obj2.getTypeAsString(),
				};
				differences.add(new Difference(tb1, tb2, path, Difference.Type.TypeChanged, lineDiff));
				
				continue;
			}
			
			// File?
			if (obj1 instanceof File) {
				// Binary?
				byte[] tf1Data = ((File)obj1).getData();
				byte[] tf2Data = ((File)obj2).getData();
				
				if (Utils.isBinaryData(tf1Data) || Utils.isBinaryData(tf2Data)) {
					// Compare bytes
					if ( ! Arrays.equals(tf1Data, tf2Data)) {
						differences.add(new Difference(tb1, tb2, path, Difference.Type.ContentsChanged, null));
					}
				}
				else {
					// Compare lines
					String[] lines1 = Utils.getLines(new String(tf1Data));
					String[] lines2 = Utils.getLines(new String(tf2Data));
					
					List<String> lineDiff = new LinkedList<String>();
					boolean differenceFound = false; // Whether lineDiff is worth saving
					
					for (int off1 = 0, off2 = 0;
					     off1 < lines1.length || off2 < lines2.length;
					     /* No increment here, depends on contents of the lines */ ) {
						
						if (off1 >= lines1.length) {
							// Past the end of lines1, all the remaining lines
							// in lines2 are additions
							lineDiff.add(String.format("+%d,%d> %s", off1, off2,
							                           lines2[off2]));
							differenceFound = true;
							
							off2++;
						}
						else if (off2 >= lines2.length) {
							// Past the end of lines2, all the remaining lines
							// in lines1 are deleted
							lineDiff.add(String.format("-%d,%d> %s", off1, off2,
							                           lines1[off1]));
							differenceFound = true;
							
							off1++;
						}
						else if (lines1[off1].equals(lines2[off2])) {
							// Lines are equal, skip them
							lineDiff.add(String.format("=%d,%d> %s", off1, off2,
							                           lines1[off1]));
							
							off1++;
							off2++;
						}
						else {
							differenceFound = true;
							
							// Lines are different
							// Search lines2 for an equal line
							int found2 = -1;
							for (int i = off2 + 1;
							     i < lines2.length && found2 == -1;
							     i++) {
								
								if (lines1[off1].equals(lines2[i])) {
									found2 = i;
								}
							}
							
							// Search for a better match in the other direction
							// (a better match is closer to the difference)
							int found1 = -1;
							for (int i = off1 + 1;
							     i < lines1.length && found1 == -1;
							     i++) {
								
								if (lines2[off2].equals(lines1[i])) {
									found1 = i;
								}
							}
							
							if (found1 == -1 && found2 == -1) {
								// Not found either way
								// Line was changed between versions
								lineDiff.add(String.format("-%d,%d> %s", off1, off2,
								                           lines1[off1]));
								lineDiff.add(String.format("+%d,%d> %s", off1, off2,
								                           lines2[off2]));
								
								off1++;
								off2++;
							}
							else if (found1 == -1 || found2 < found1) {
								// Found a match further down lines2
								// Lines from off2 to found2 were added
								for (int i = off2; i < found2; i++) {
									lineDiff.add(String.format("+%d,%d> %s",
									                           off1, i,
									                           lines2[i]));
								}
								
								off2 = found2;
							}
							else {
								// Found a match further down lines1
								// Lines from off1 to found1 were removed
								for (int i = off1; i < found1; i++) {
									lineDiff.add(String.format("-%d,%d> %s",
									                           i, off2,
									                           lines1[i]));
								}
								
								off1 = found1;
							}
						} // Lines are different
					} // Loop through each line in each file
					
					if (differenceFound) {
						differences.add(new Difference(tb1, tb2, path,
						                               Difference.Type.ContentsChanged,
						                               lineDiff.toArray(new String[lineDiff.size()])));
					}
				} // Files are not binary
			}
			else if (obj1 instanceof Symlink) {
				Symlink s1 = (Symlink)obj1;
				Symlink s2 = (Symlink)obj2;
				
				if ( ! s1.getLinkName().equals(s2.getLinkName())) {
					String[] lineDiff = new String[] {
						s1.getLinkName(),
						s2.getLinkName(),
					};
					differences.add(new Difference(tb1, tb2, path,
					                               Difference.Type.TargetChanged,
					                               lineDiff));
				}
			}
			else if (obj1 instanceof Device) {
				Device d1 = (Device)obj1;
				Device d2 = (Device)obj2;
				
				ArrayList<String> lineDiff = new ArrayList<String>(4);
				boolean differenceFound = false;
				
				if (d1.getMajor() != d2.getMajor() ||
				    d1.getMinor() != d2.getMinor()) {
					lineDiff.add(d1.getMajor() + ", " + d1.getMinor());
					lineDiff.add(d2.getMajor() + ", " + d2.getMinor());
					
					differences.add(new Difference(tb1, tb2, path,
					                               Difference.Type.DeviceChanged,
					                               lineDiff.toArray(new String[lineDiff.size()])));
				}
			}
		}
		
		// Loop through tb2's files, searching for files missing from tb1
		for (String path : fileMap2.keySet()) {
			if ( ! fileMap1.containsKey(path)) {
				differences.add(new Difference(tb1, tb2, path, Difference.Type.Added, null));
			}
		}
		
		return differences;
	}
}

/*
 * TDirectory.java
 *
 * Provides directory functionality for tarball directories.
 *
 * Jonathan Pearson
 * November 12, 2009
 *
 */

package com.sixnetio.fs.tar;

import com.sixnetio.fs.generic.Directory;

public class TDirectory
	extends Directory
	implements TarObject
{
	private final TarAttributes tarAttrs;
	
	protected TDirectory(String name, short uid, String userName,
	                     short gid, String groupName, int mode, int modtime,
	                     String magic, byte linkFlag, TDirectory parent)
	{
		super(name, uid, gid, mode, modtime, parent);
		
		tarAttrs = new TarAttributes(userName, groupName, magic, linkFlag);
	}
	
	@Override
	public void setParent(Directory parent)
	{
		if ( ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Tarball object parent must be a Tarball directory");
		}
		
		super.setParent(parent);
	}
	
	@Override
	public TarAttributes getTarAttributes()
	{
		return tarAttrs;
	}
}

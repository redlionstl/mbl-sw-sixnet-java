/*
 * TCharDevice.java
 *
 * Provides character special device functionality for tarballs.
 *
 * Jonathan Pearson
 * November 12, 2009
 *
 */

package com.sixnetio.fs.tar;

import com.sixnetio.fs.generic.CharDevice;
import com.sixnetio.fs.generic.Directory;

public class TCharDevice
	extends CharDevice
	implements TarObject
{
	private final TarAttributes tarAttrs;
	
	public TCharDevice(String name, byte major, byte minor,
	                   short uid, String userName, short gid, String groupName,
	                   int mode, int modtime, String magic, byte linkFlag,
	                   TDirectory parent)
	{
		super(name, major, minor, uid, gid, mode, modtime, parent);
		
		tarAttrs = new TarAttributes(userName, groupName, magic, linkFlag);
	}
	
	@Override
	public void setParent(Directory parent)
	{
		if ( ! (parent instanceof TDirectory)) {
			throw new IllegalArgumentException("Tarball object parent must be a Tarball directory");
		}
		
		super.setParent(parent);
	}
	
	@Override
	public TarAttributes getTarAttributes()
	{
		return tarAttrs;
	}
}

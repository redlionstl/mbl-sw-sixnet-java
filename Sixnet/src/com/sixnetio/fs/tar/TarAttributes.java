/*
 * TARAttributes.java
 *
 * Provides the extra attributes required by objects in tarballs.
 *
 * Jonathan Pearson
 * November 12, 2009
 *
 */

package com.sixnetio.fs.tar;

public class TarAttributes
{
	private String userName;
	private String groupName;
	private String magic;
	private byte linkFlag;
	
	public TarAttributes(String userName, String groupName, String magic,
	                     byte linkFlag)
	{
		this.userName = userName;
		this.groupName = groupName;
		this.magic = magic;
		this.linkFlag = linkFlag;
	}
	
	/** Get the name of the owner. */
	public String getUserName()
	{
		return userName;
	}
	
	/** Set the name of the owner. */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
	/** Get the name of the group. */
	public String getGroupName()
	{
		return groupName;
	}
	
	/** Set the name of the group. */
	public void setGroupName(String groupName)
	{
		this.groupName = groupName;
	}
	
	/** Get the magic string. */
	public String getMagic()
	{
		return magic;
	}
	
	/** Set the magic string. */
	public void setMagic(String magic)
	{
		this.magic = magic;
	}
	
	/** Get the link flag. */
	public byte getLinkFlag()
	{
		return linkFlag;
	}
	
	/** Set the link flag. */
	public void setLinkFlag(byte linkFlag)
	{
		this.linkFlag = linkFlag;
	}
}

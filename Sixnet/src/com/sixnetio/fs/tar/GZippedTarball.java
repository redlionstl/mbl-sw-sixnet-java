/*
 * GZippedTarball.java
 *
 * Description
 *
 * Jonathan Pearson
 * Jul 30, 2008
 *
 */

package com.sixnetio.fs.tar;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.sixnetio.util.Utils;

/**
 * A convenience class used to represent a GZipped tarball. Wraps the necessary
 * Tarball methods with gzip/gunzip operations.
 * 
 * @author Jonathan Pearson
 *
 */
public class GZippedTarball
	extends Tarball
{
	/**
	 * Construct a new, empty GZippedTarball.
	 */
	public GZippedTarball()
	{
		super();
	}
	/**
	 * Construct a new GZippedTarball from a byte array.
	 * 
	 * @param data The gzipped tarball data.
	 * @throws IOException If there is an error unzipping or untarring the data.
	 */
	public GZippedTarball(byte[] data)
		throws IOException
	{
		super(gunzip(data));
	}
	
	/**
	 * Construct a new GzippedTarball from an input stream.
	 * 
	 * @param in The input stream from which to read the gzipped tarball data.
	 * @throws IOException If there is an error reading, unzipping, or untarring
	 *   the data.
	 */
	public GZippedTarball(InputStream in)
		throws IOException
	{
		super(new GZIPInputStream(in));
	}
	
	/**
	 * Unzip the given byte array.
	 * 
	 * @param data The data to unzip.
	 * @return Unzipped data.
	 * @throws IOException If there is a problem parsing the gzipped data.
	 */
	public static byte[] gunzip(byte[] data)
		throws IOException
	{
		byte[] decompressed;
		
		// Decompress the GZip file into a tar file
		GZIPInputStream in = new GZIPInputStream(new ByteArrayInputStream(data));
		decompressed = Utils.bytesToEOF(in);
		in.close();
		
		return decompressed;
	}
	
	/**
	 * Zip up a byte array.
	 * 
	 * @param data The data to zip.
	 * @return GZipped data.
	 * @throws IOException If there is a problem zipping up the data.
	 */
	public static byte[] gzip(byte[] data)
		throws IOException
	{
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		GZIPOutputStream out = new GZIPOutputStream(byteOut);
		
		out.write(data);
		out.finish();
		out.close();
		
		return byteOut.toByteArray();
	}
	
	/**
	 * Get the gzipped tarball data out of this object.
	 */
	@Override
	public byte[] getData()
		throws IOException
	{
		return gzip(super.getData());
	}
}

/*
 * Whiteout.java
 *
 * Represents a Whiteout file in a filesystem. Whiteout files are used by
 * upper-level file systems on top of read-only file systems to give the
 * appearance of a deleted file.
 *
 * Jonathan Pearson
 * November 10, 2009
 *
 */

package com.sixnetio.fs.generic;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Date;

import com.sixnetio.util.Utils;

/**
 * Represents a Whiteout file in a filesystem. Whiteout files are used by
 * upper-level file systems on top of read-only file systems to give the
 * appearance of a deleted file. This is provided only for completeness, as it
 * contains no additional functionality.
 *
 * @author Jonathan Pearson
 */
public class Whiteout
	extends FSObject
{
	/**
	 * Construct a new whiteout.
	 * 
	 * @param name The name of the whiteout.
	 * @param uid The owning user ID of the whiteout.
	 * @param gid The owning group ID of the whiteout.
	 * @param mode The permissions of the whiteout.
	 * @param modtime The modification time of the whiteout.
	 * @param parent The parent directory of this whiteout.
	 */
	protected Whiteout(String name, short uid, short gid, int mode, int modtime,
	                Directory parent)
	{
		super(name, DT_WHT, uid, gid, mode, modtime, parent);
	}
	
	@Override
	public String getTypeAsString()
	{
		return "Whiteout";
	}
	
	@Override
	public char getTypeAsChar()
	{
		return 'w';
	}
	
	@Override
	public void dump(int indentLevel, PrintStream out)
	{
		out.println(getDumpLine(indentLevel));
	}
	
	@Override
	public String getDumpLine(int indentLevel)
	{
		String indent = Utils.multiplyChar(' ', indentLevel * 2);
		
		String modTime =
			DateFormat.getDateInstance().format(new Date(getModTime() * 1000));
		
		return String.format("%10s  %3d  %3d  %,12d  %12s  %s%s",
		                     getModeAsString(), getUID(), getGID(), 0,
		                     modTime, indent, getName());
	}
}

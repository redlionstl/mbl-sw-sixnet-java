/*
 * Socket.java
 *
 * Represents a Socket file.
 *
 * Jonathan Pearson
 * November 10, 2009
 *
 */

package com.sixnetio.fs.generic;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Date;

import com.sixnetio.util.Utils;

/**
 * Represents a socket file in a filesystem. This is provided only for
 * completeness, as it contains no special functionality of its own.
 *
 * @author Jonathan Pearson
 */
public class Socket
	extends FSObject
{
	/**
	 * Construct a new socket.
	 * 
	 * @param name The name of the socket.
	 * @param uid The owning user ID of the socket.
	 * @param gid The owning group ID of the socket.
	 * @param mode The permissions of the socket.
	 * @param modtime The modification time of the socket.
	 * @param parent The parent directory of this socket.
	 */
	protected Socket(String name, short uid, short gid, int mode, int modtime,
	                 Directory parent)
	{
		super(name, DT_SOCK, uid, gid, mode, modtime, parent);
	}
	
	@Override
	public String getTypeAsString()
	{
		return "Socket";
	}
	
	@Override
	public char getTypeAsChar()
	{
		return 's';
	}
	
	@Override
	public void dump(int indentLevel, PrintStream out)
	{
		out.println(getDumpLine(indentLevel));
	}
	
	@Override
	public String getDumpLine(int indentLevel)
	{
		String indent = Utils.multiplyChar(' ', indentLevel * 2);
		
		String modTime =
			DateFormat.getDateInstance().format(new Date(getModTime() * 1000));
		
		return String.format("%10s  %3d  %3d  %,12d  %12s  %s%s",
		                     getModeAsString(), getUID(), getGID(), 0,
		                     modTime, indent, getName());
	}
}

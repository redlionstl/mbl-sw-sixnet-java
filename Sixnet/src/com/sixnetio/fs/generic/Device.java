/*
 * Device.java
 *
 * Represents either of the device file types.
 *
 * Jonathan Pearson
 * November 10, 2009
 *
 */

package com.sixnetio.fs.generic;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Date;

import com.sixnetio.util.Utils;

/**
 * A superclass for the device file types.
 *
 * @author Jonathan Pearson
 */
public abstract class Device
	extends FSObject
{
	private byte major, minor;
	
	/**
	 * Construct a new block device.
	 * 
	 * @param name The name of the device.
	 * @param type The type of the file (see FSObject.DT_*).
	 * @param major The major device number.
	 * @param minor The minor device number.
	 * @param uid The owning user ID of the device.
	 * @param gid The owning group ID of the device.
	 * @param mode The permissions of the device.
	 * @param modtime The modification time of the device.
	 * @param parent The parent directory containing the device.
	 */
	protected Device(String name, byte type, byte major, byte minor, short uid,
	                 short gid, int mode, int modtime, Directory parent)
	{
		super(name, type, uid, gid, mode, modtime, parent);
		
		this.major = major;
		this.minor = minor;
	}
	
	/** Get the major version number of the device. */
	public byte getMajor()
	{
		return major;
	}
	
	/** Get the minor version number of the device. */
	public byte getMinor()
	{
		return minor;
	}
	
	
	@Override
	public void dump(int indentLevel, PrintStream out)
	{
		out.println(getDumpLine(indentLevel));
	}
	
	@Override
	public String getDumpLine(int indentLevel)
	{
		String indent = Utils.multiplyChar(' ', indentLevel * 2);
		
		String modTime =
			DateFormat.getDateInstance().format(new Date(getModTime() * 1000));
		
		return String.format("%10s  %3d  %3d  %6d,%5d  %12s  %s%s",
		                     getModeAsString(), getUID(), getGID(),
		                     getMajor() & 0xff, getMinor() & 0xff, modTime,
		                     indent, getName());
	}
}

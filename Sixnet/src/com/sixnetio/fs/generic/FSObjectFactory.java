/*
 * FSObjectFactory.java
 *
 * All FSObject factories must implement this interface.
 *
 * Jonathan Pearson
 * November 11, 2009
 *
 */

package com.sixnetio.fs.generic;

public interface FSObjectFactory
{
	/**
	 * Create a block special device.
	 * 
	 * @param name The name of the device.
	 * @param major The major number.
	 * @param minor The minor number.
	 * @param parent The directory containing this device. This object will be
	 *   added to the parent's contents.
	 * @return A new block special device.
	 */
	public BlockDevice createBlockDevice(String name, byte major, byte minor,
	                                     Directory parent);
	
	/**
	 * Create a character special device.
	 * 
	 * @param name The name of the device.
	 * @param major The major number.
	 * @param minor The minor number.
	 * @param parent The directory containing this device.
	 * @return A new character special device. This object will be
	 *   added to the parent's contents.
	 */
	public CharDevice createCharDevice(String name, byte major, byte minor,
	                                   Directory parent);
	
	/**
	 * Create a new directory.
	 * 
	 * @param name The name of the directory.
	 * @param parent The directory containing this directory, or <tt>null</tt>
	 *   if this is the root directory. This object will be added to the
	 *   parent's contents if not <tt>null</tt>.
	 * @return A new directory.
	 */
	public Directory createDirectory(String name, Directory parent);
	
	/**
	 * Create a new FIFO.
	 * 
	 * @param name The name of the FIFO.
	 * @param parent The directory containing this FIFO. This object will be
	 *   added to the parent's contents.
	 * @return A new FIFO.
	 */
	public FIFO createFIFO(String name, Directory parent);
	
	/**
	 * Create a new file.
	 * 
	 * @param name The name of the file.
	 * @param data The data contained in the file.
	 * @param parent The directory containing this file. This object will be
	 *   added to the parent's contents.
	 * @return A new file.
	 */
	public File createFile(String name, byte[] data, Directory parent);
	
	/**
	 * Create a new socket.
	 * 
	 * @param name The name of the socket.
	 * @param parent The directory containing this socket. This object will be
	 *   added to the parent's contents.
	 * @return A new socket.
	 */
	public Socket createSocket(String name, Directory parent);
	
	/**
	 * Create a new symlink.
	 * 
	 * @param name The name of the symlink.
	 * @param linkName The path (possibly relative) to the target.
	 * @param parent The directory containing this symlink. This object will be
	 *   added to the parent's contents.
	 * @return A new symlink.
	 */
	public Symlink createSymlink(String name, String linkName,
	                             Directory parent);
	
	/**
	 * Create a new whiteout.
	 * 
	 * @param name The name of the whiteout.
	 * @param parent The directory containing this whiteout. This object will be
	 *   added to the parent's contents.
	 * @return A new whiteout.
	 */
	public Whiteout createWhiteout(String name, Directory parent);
	
	/**
	 * Convert an individual object to an object type constructed by this
	 * factory. This may return a simple copy of the object, but it must not
	 * return the passed-in object itself, even if no changes are necessary,
	 * because that object is mutable. This will not convert children of a
	 * directory, only the directory entry itself. A directory will be left
	 * empty by this function.
	 * 
	 * @param obj The object to convert.
	 * @param parent The parent directory that will receive the converted object
	 *   as its child.
	 * @return  The converted object.
	 */
	public FSObject convertObject(FSObject obj, Directory parent);
	
	/**
	 * Convert the filesystem rooted at <tt>dir</tt> to the object types
	 * constructed by this factory. This may return a simple copy of the
	 * filesystem (with the passed-in directory as the root), but it must not
	 * return the passed-in object itself, even if no changes are necessary,
	 * because that object is mutable.
	 * 
	 * @param dir The directory to convert.
	 * @return A converted filesystem.
	 */
	public Directory convertFilesystem(Directory dir);
}

/*
 * Directory.java
 * 
 * Represents a Directory in a generic file system.
 * 
 * Jonathan Pearson
 * November 10, 2009
 * 
 */

package com.sixnetio.fs.generic;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.*;

import com.sixnetio.util.Utils;

/**
 * Represents a directory.
 *
 * @author Jonathan Pearson
 */
public class Directory
	extends FSObject
	implements Iterable<FSObject>
{
	private List<FSObject> contents;
	
	/**
	 * Construct a new directory.
	 * 
	 * @param name The name of the directory.
	 * @param uid The owning user ID of the directory.
	 * @param gid The owning group ID of the directory.
	 * @param mode The permissions of the directory.
	 * @param modtime The modification time of the directory.
	 * @param parent The parent directory of this directory.
	 */
	protected Directory(String name, short uid, short gid, int mode, int modtime,
	                    Directory parent)
	{
		super(name, DT_DIR, uid, gid, mode, modtime, parent);
		
		this.contents = new LinkedList<FSObject>();
	}
	
	/** Get the number of immediate children of this directory. */
	public int getChildCount()
	{
		return contents.size();
	}
	
	/**
	 * Get a list of the children of this directory. Modifying this list will
	 * not affect the contents of the directory, although modifying the objects
	 * within it will be reflected in the filesystem tree.
	 */
	public List<FSObject> getChildren()
	{
		return new ArrayList<FSObject>(contents);
	}
	
	/**
	 * Add a child to this directory. This will not tell the child that its
	 * parent has changed.
	 * 
	 * @param obj The child to add.
	 */
	public void addChild(FSObject obj)
	{
		this.contents.add(obj);
	}
	
	/**
	 * Remove a child from this directory. This will not tell the child that its
	 * parent has changed.
	 * 
	 * @param obj The child to remove.
	 */
	public void removeChild(FSObject obj)
	{
		this.contents.remove(obj);
	}
	
	/** Get a copy of the contents of this directory. */
	public List<FSObject> getContents()
	{
		return new ArrayList<FSObject>(contents);
	}
	
	/**
	 * Get an iterator over the contents of this directory. This may be used to
	 * remove items.
	 */
	public Iterator<FSObject> iterator()
	{
		return contents.iterator();
	}
	
	@Override
	public String getTypeAsString()
	{
		return "Directory";
	}
	
	@Override
	public char getTypeAsChar()
	{
		return 'd';
	}
	
	@Override
	public void dump(int indentLevel, PrintStream out)
	{
		out.println(getDumpLine(indentLevel));
		
		FSObject[] children = contents.toArray(new FSObject[contents.size()]);
		
		// The default compare for FSObjects is by Path, which is much slower
		// than sorting by name
		Arrays.sort(children, new Comparator<FSObject>() {
			public int compare(FSObject o1, FSObject o2)
			{
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		for (FSObject child : children) {
			child.dump(indentLevel + 1, out);
		}
	}
	
	@Override
	public String getDumpLine(int indentLevel)
	{
		String indent = Utils.multiplyChar(' ', indentLevel * 2);
		
		String modTime =
			DateFormat.getDateInstance().format(new Date(getModTime() * 1000));
		
		return String.format("%10s  %3d  %3d  %,12d  %12s  %s%s/",
		                     getModeAsString(), getUID(), getGID(), 0,
		                     modTime, indent, getName());
	}
	
	/**
	 * Locate an object given its path. The path may be relative to this
	 * directory, or absolute (starting with a '/').
	 * 
	 * @param path The path.
	 * @return The FSObject that resides at the end of the given path.
	 */
	public FSObject locate(String path)
	{
		if (path.startsWith("/")) {
			return getRoot().locate(path.substring(1));
		}
		else {
			// Pull off the next section and parse it
			String piece;
			if (path.indexOf('/') != -1) {
				piece = path.substring(0, path.indexOf('/'));
				path = path.substring(path.indexOf('/') + 1);
			}
			else {
				piece = path;
				path = "";
			}
			
			// Remove any double-/ artifacts
			while (path.startsWith("/")) {
				path = path.substring(1);
			}
			
			if (piece.equals(".")) {
				// Points to current directory
				return locate(path);
			}
			else if (piece.equals("..")) {
				// Points to parent directory
				return getParent().locate(path);
			}
			else {
				// Points to an object within the current directory
				FSObject obj = null;
				for (FSObject child : this) {
					if (piece.equals(child.getName())) {
						// Found the proper element
						obj = child;
						break;
					}
				}
				
				if (obj == null) {
					// Not found
					return null;
				}
				else if (path.length() == 0) {
					// Found the end of the path
					return obj;
				}
				else {
					// There is more searching to be done
					if (obj instanceof Directory) {
						return ((Directory)obj).locate(path);
					}
					else if (obj instanceof Symlink &&
							 ((Symlink)obj).getLinkTarget() instanceof Directory) {
						
						return ((Directory)((Symlink)obj).getLinkTarget()).locate(path);
					}
					else {
						// Unable to continue searching
						return null;
					}
				}
			}
		}
	}
	
	/**
	 * Get set of all of the objects beneath this directory (not including this
	 * directory).
	 */
	public Set<FSObject> getAllObjects()
	{
		Set<FSObject> objects = new HashSet<FSObject>();
		
		for (FSObject child : this) {
			objects.add(child);
			
			if (child instanceof Directory) {
				objects.addAll(((Directory)child).getAllObjects());
			}
		}
		
		return objects;
	}
}

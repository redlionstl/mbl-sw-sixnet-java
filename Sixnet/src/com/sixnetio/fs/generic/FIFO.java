/*
 * FIFO.java
 *
 * Represents a FIFO file.
 *
 * Jonathan Pearson
 * November 10, 2009
 *
 */

package com.sixnetio.fs.generic;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Date;

import com.sixnetio.util.Utils;

/**
 * Represents a FIFO object in a filesystem. This is provided only for
 * completeness, as it contains no additional functionality.
 *
 * @author Jonathan Pearson
 */
public class FIFO
	extends FSObject
{
	/**
	 * Construct a new FIFO.
	 * 
	 * @param name The name of the FIFO.
	 * @param uid The owning user ID of the FIFO.
	 * @param gid The owning group ID of the FIFO.
	 * @param mode The permissions of the FIFO.
	 * @param modtime The modification time of the FIFO.
	 * @param parent The parent directory of this FIFO.
	 */
	protected FIFO(String name, short uid, short gid, int mode, int modtime,
	               Directory parent)
	{
		super(name, DT_FIFO, uid, gid, mode, modtime, parent);
	}
	
	@Override
	public String getTypeAsString()
	{
		return "FIFO";
	}
	
	@Override
	public char getTypeAsChar()
	{
		return 'p';
	}
	
	@Override
	public void dump(int indentLevel, PrintStream out)
	{
		out.println(getDumpLine(indentLevel));
	}
	
	@Override
	public String getDumpLine(int indentLevel)
	{
		String indent = Utils.multiplyChar(' ', indentLevel * 2);
		
		String modTime =
			DateFormat.getDateInstance().format(new Date(getModTime() * 1000));
		
		return String.format("%10s  %3d  %3d  %,12d  %12s  %s%s",
		                     getModeAsString(), getUID(), getGID(), 0,
		                     modTime, indent, getName());
	}
}

/*
 * Symlink.java
 * 
 * Represents a symlink in any file system that supports it.
 * 
 * Jonathan Pearson
 * November 10, 2009
 * 
 */

package com.sixnetio.fs.generic;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Date;

import com.sixnetio.util.Utils;


/**
 * Represents a symbolic link.
 *
 * @author Jonathan Pearson
 */
public class Symlink
	extends FSObject
{
	private String linkName;
	
	/**
	 * Construct a new symbolic link.
	 * 
	 * @param name The name of the link.
	 * @param linkName The path to the target of the link.
	 * @param uid The owning user ID of the link.
	 * @param gid The owning group ID of the link.
	 * @param mode The permissions of the link.
	 * @param modtime The modification time of the link.
	 * @param parent The parent directory containing the link.
	 */
	protected Symlink(String name, String linkName, short uid, short gid,
	                  int mode, int modtime, Directory parent)
	{
		super(name, DT_LNK, uid, gid, mode, modtime, parent);
		
		this.linkName = linkName;
	}
	
	/** Get the (possibly relative) path to the target of this link. */
	public String getLinkName()
	{
		return linkName;
	}
	
	/** Set the (possibly relative) path to the target of this link. */
	public void setLinkName(String linkName)
	{
		this.linkName = linkName;
	}
	
	/** Get the object which is the target of this link. */
	public FSObject getLinkTarget()
	{
		return getParent().locate(linkName);
	}
	
	@Override
	public String getTypeAsString()
	{
		return "Link";
	}
	
	@Override
	public char getTypeAsChar()
	{
		return 'l';
	}
	
	@Override
	public void dump(int indentLevel, PrintStream out)
	{
		out.println(getDumpLine(indentLevel));
	}
	
	@Override
	public String getDumpLine(int indentLevel)
	{
		String indent = Utils.multiplyChar(' ', indentLevel * 2);
		
		String modTime =
			DateFormat.getDateInstance().format(new Date(getModTime() * 1000));
		
		return String.format("%10s  %3d  %3d  %,12d  %12s  %s%s -> %s",
		                     getModeAsString(), getUID(), getGID(),
		                     linkName.length(), modTime, indent,
		                     getName(), getLinkName());
	}
}

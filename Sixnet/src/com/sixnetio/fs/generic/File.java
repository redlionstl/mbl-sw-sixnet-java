/*
 * File.java
 * 
 * Represents a File in a generic file system.
 * 
 * Jonathan Pearson
 * November 10, 2009
 * 
 */

package com.sixnetio.fs.generic;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Date;

import com.sixnetio.util.Utils;

/**
 * Represents a regular file.
 *
 * @author Jonathan Pearson
 */
public class File
	extends FSObject
{
	private byte[] data;
	
	/**
	 * Construct a new File.
	 * 
	 * @param name The name of the file.
	 * @param data The data contained within this file.
	 * @param uid The owning user ID of this file.
	 * @param gid The owning group ID of this file.
	 * @param mode The permissions on this file.
	 * @param modtime The modification time of this file.
	 * @param parent The parent directory containing this file.
	 */
	protected File(String name, byte[] data, short uid, short gid, int mode,
	               int modtime, Directory parent)
	{
		super(name, DT_REG, uid, gid, mode, modtime, parent);
		
		this.data = data;
	}
	
	/** Get a copy of the data contained within this file. */
	public byte[] getData()
	{
		// Don't let the user modify it
		byte[] ans = new byte[data.length];
		System.arraycopy(data, 0, ans, 0, ans.length);
		return ans;
	}
	
	/**
	 * Update the data in this file.
	 * 
	 * @param data The new data to store in this file.
	 */
	public void setData(byte[] data)
	{
		this.data = new byte[data.length];
		System.arraycopy(data, 0, this.data, 0, data.length);
	}
	
	/** Get the size of this file, in bytes. */
	public int getSize()
	{
		return data.length;
	}
	
	/** Get an MD5 checksum of the data contained within this file. */
	public String md5Sum()
	{
		return Utils.md5sum(getData());
	}
	
	@Override
	public String getTypeAsString()
	{
		return "File";
	}
	
	@Override
	public char getTypeAsChar()
	{
		return '-';
	}
	
	@Override
	public void dump(int indentLevel, PrintStream out)
	{
		out.println(getDumpLine(indentLevel));
	}
	
	@Override
	public String getDumpLine(int indentLevel)
	{
		String indent = Utils.multiplyChar(' ', indentLevel * 2);
		
		String modTime =
			DateFormat.getDateInstance().format(new Date(getModTime() * 1000));
		
		return String.format("%10s  %3d  %3d  %,12d  %12s  %s%s",
		                     getModeAsString(), getUID(), getGID(),
		                     data.length, modTime, indent, getName());
	}
}

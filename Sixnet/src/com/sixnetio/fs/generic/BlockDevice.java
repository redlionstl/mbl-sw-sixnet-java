/*
 * BlockDevice.java
 * 
 * Represents a block device node in a generic file system.
 * 
 * Jonathan Pearson
 * November 10, 2009
 * 
 */

package com.sixnetio.fs.generic;



/**
 * Represents a block special device file.
 *
 * @author Jonathan Pearson
 */
public class BlockDevice
	extends Device
{
	/**
	 * Construct a new block device.
	 * 
	 * @param name The name of the device.
	 * @param major The major device number.
	 * @param minor The minor device number.
	 * @param uid The owning user ID of the device.
	 * @param gid The owning group ID of the device.
	 * @param mode The permissions of the device.
	 * @param modtime The modification time of the device.
	 * @param parent The parent directory containing the device.
	 */
	protected BlockDevice(String name, byte major, byte minor, short uid,
	                      short gid, int mode, int modtime, Directory parent)
	{
		super(name, DT_BLK, major, minor, uid, gid, mode, modtime, parent);
	}
	
	@Override
	public String getTypeAsString()
	{
		return "Block Special";
	}
	
	@Override
	public char getTypeAsChar()
	{
		return 'b';
	}
}

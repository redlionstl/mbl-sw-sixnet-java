/*
 * CharDevice.java
 * 
 * Represents a character device node in a generic file system.
 * 
 * Jonathan Pearson
 * November 10, 2009
 * 
 */

package com.sixnetio.fs.generic;



/**
 * Represents a character special device file.
 *
 * @author Jonathan Pearson
 */
public class CharDevice
	extends Device
{
	/**
	 * Construct a new character device.
	 * 
	 * @param name The name of the device.
	 * @param major The major device number.
	 * @param minor The minor device number.
	 * @param uid The owning user ID of the device.
	 * @param gid The owning group ID of the device.
	 * @param mode The permissions of the device.
	 * @param modtime The modification time of the device.
	 * @param parent The parent directory containing the device.
	 */
	protected CharDevice(String name, byte major, byte minor, short uid,
	                     short gid, int mode, int modtime, Directory parent)
	{
		super(name, DT_CHR, major, minor, uid, gid, mode, modtime, parent);
	}
	
	@Override
	public String getTypeAsString()
	{
		return "Character Special";
	}
	
	@Override
	public char getTypeAsChar()
	{
		return 'c';
	}
}

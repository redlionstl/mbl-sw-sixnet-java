/*
 * GenericFSObjectFactory.java
 *
 * Builds generic FSObject subclasses.
 *
 * Jonathan Pearson
 * November 11, 2009
 *
 */

package com.sixnetio.fs.generic;

import org.apache.log4j.Logger;

import com.sixnetio.util.Utils;

public class GenericFSObjectFactory
	implements FSObjectFactory
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Create a block special device.
	 * 
	 * @param name The name of the device.
	 * @param major The major number.
	 * @param minor The minor number.
	 * @param uid The owner's UID.
	 * @param gid The group's GID.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param parent The directory containing this device. This object will be
	 *   added to the parent's contents.
	 * @return A new block special device.
	 */
	public BlockDevice createBlockDevice(String name, byte major, byte minor,
	                                     short uid, short gid, int mode,
	                                     int modtime, Directory parent)
	{
		BlockDevice obj = new BlockDevice(name, major, minor, uid, gid, mode,
		                                  modtime, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public BlockDevice createBlockDevice(String name, byte major, byte minor,
	                                     Directory parent)
	{
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_BLK; // Turn on the block type
		
		int modtime = FSObject.now();
		
		return createBlockDevice(name, major, minor, (short)0, (short)0, mode,
		                         modtime, parent);
	}
	
	/**
	 * Create a character special device.
	 * 
	 * @param name The name of the device.
	 * @param major The major number.
	 * @param minor The minor number.
	 * @param uid The owner's UID.
	 * @param gid The group's GID.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param parent The directory containing this device. This object will be
	 *   added to the parent's contents.
	 * @return A new character special device.
	 */
	public CharDevice createCharDevice(String name, byte major, byte minor,
	                                   short uid, short gid, int mode,
	                                   int modtime, Directory parent)
	{
		CharDevice obj = new CharDevice(name, major, minor, uid, gid, mode,
		                                modtime, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public CharDevice createCharDevice(String name, byte major, byte minor,
	                                   Directory parent)
	{
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_CHR; // Turn on the character type
		
		int modtime = FSObject.now();
		
		return createCharDevice(name, major, minor, (short)0, (short)0, mode,
		                        modtime, parent);
	}
	
	/**
	 * Create a new directory.
	 * 
	 * @param name The name of the directory.
	 * @param uid The owner's UID.
	 * @param gid The group's GID.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param parent The directory containing this directory, or <tt>null</tt>
	 *   if this is the root directory. This object will be added to the
	 *   parent's contents if not <tt>null</tt>.
	 * @return A new directory.
	 */
	public Directory createDirectory(String name, short uid, short gid,
	                                 int mode, int modtime, Directory parent)
	{
		Directory obj = new Directory(name, uid, gid, mode, modtime, parent);
		if (parent != null) {
			parent.addChild(obj);
		}
		return obj;
	}
	
	@Override
	public Directory createDirectory(String name, Directory parent)
	{
		int mode = 0775;
		if (parent != null) {
			mode = parent.getMode();
		}
		
		// Don't need to make any changes to the mode
		
		int modtime = FSObject.now();
		
		return createDirectory(name, (short)0, (short)0, mode, modtime, parent);
	}
	
	/**
	 * Create a new FIFO.
	 * 
	 * @param name The name of the FIFO.
	 * @param uid The owner's UID.
	 * @param gid The group's GID.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param parent The directory containing this FIFO. This object will be
	 *   added to the parent's contents.
	 * @return A new FIFO.
	 */
	public FIFO createFIFO(String name, short uid, short gid, int mode,
	                       int modtime, Directory parent)
	{
		FIFO obj = new FIFO(name, uid, gid, mode, modtime, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public FIFO createFIFO(String name, Directory parent)
	{
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_FIFO; // Turn on the FIFO type
		
		int modtime = FSObject.now();
		
		return createFIFO(name, (short)0, (short)0, mode, modtime, parent);
	}
	
	/**
	 * Create a new file.
	 * 
	 * @param name The name of the file.
	 * @param data The data contained in the file.
	 * @param uid The owner's UID.
	 * @param gid The group's GID.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param parent The directory containing this file. This object will be
	 *   added to the parent's contents.
	 * @return A new file.
	 */
	public File createFile(String name, byte[] data, short uid, short gid,
	                       int mode, int modtime, Directory parent)
	{
		File obj = new File(name, data, uid, gid, mode, modtime, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public File createFile(String name, byte[] data, Directory parent)
	{
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_REG; // Turn on the file type
		
		int modtime = FSObject.now();
		
		return createFile(name, data, (short)0, (short)0, mode, modtime,
		                  parent);
	}
	
	/**
	 * Create a new socket.
	 * 
	 * @param name The name of the socket.
	 * @param uid The owner's UID.
	 * @param gid The group's GID.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param parent The directory containing this socket. This object will be
	 *   added to the parent's contents.
	 * @return A new socket.
	 */
	public Socket createSocket(String name, short uid, short gid, int mode,
	                           int modtime, Directory parent)
	{
		Socket obj = new Socket(name, uid, gid, mode, modtime, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public Socket createSocket(String name, Directory parent)
	{
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_SOCK; // Turn on the socket type
		
		int modtime = FSObject.now();
		
		return createSocket(name, (short)0, (short)0, mode, modtime, parent);
	}
	
	/**
	 * Create a new symlink.
	 * 
	 * @param name The name of the symlink.
	 * @param linkName The path (possibly relative) to the target.
	 * @param uid The owner's UID.
	 * @param gid The group's GID.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param parent The directory containing this symlink. This object will be
	 *   added to the parent's contents.
	 * @return A new symlink.
	 */
	public Symlink createSymlink(String name, String linkName, short uid,
	                             short gid, int mode, int modtime,
	                             Directory parent)
	{
		Symlink obj = new Symlink(name, linkName, uid, gid, mode, modtime,
		                          parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public Symlink createSymlink(String name, String linkName, Directory parent)
	{
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_LNK; // Turn on the symlink type
		
		int modtime = FSObject.now();
		
		return createSymlink(name, linkName, (short)0, (short)0, mode, modtime,
		                     parent);
	}
	
	/**
	 * Create a new whiteout.
	 * 
	 * @param name The name of the whiteout.
	 * @param uid The owner's UID.
	 * @param gid The group's GID.
	 * @param mode The mode (permissions and type).
	 * @param modtime The modification time, in seconds since the epoch.
	 * @param parent The directory containing this whiteout. This object will be
	 *   added to the parent's contents.
	 * @return A new whiteout.
	 */
	public Whiteout createWhiteout(String name, short uid, short gid, int mode,
	                               int modtime, Directory parent)
	{
		Whiteout obj = new Whiteout(name, uid, gid, mode, modtime, parent);
		parent.addChild(obj);
		return obj;
	}
	
	@Override
	public Whiteout createWhiteout(String name, Directory parent)
	{
		int mode = parent.getMode();
		mode &= ~0111; // Turn off execute bits
		mode &= ~FSObject.MODE_DIR; // Turn off the directory type
		mode |= FSObject.MODE_WHT; // Turn on the whiteout type
		
		int modtime = FSObject.now();
		
		return createWhiteout(name, (short)0, (short)0, mode, modtime, parent);
	}
	
	@Override
	public FSObject convertObject(FSObject obj, Directory parent)
	{
		if (obj instanceof BlockDevice) {
			BlockDevice o = (BlockDevice)obj;
			return createBlockDevice(o.getName(), o.getMajor(), o.getMinor(),
			                         o.getUID(), o.getGID(), o.getMode(),
			                         o.getModTime(), parent);
		}
		else if (obj instanceof CharDevice) {
			CharDevice o = (CharDevice)obj;
			return createCharDevice(o.getName(), o.getMajor(), o.getMinor(),
			                        o.getUID(), o.getGID(), o.getMode(),
			                        o.getModTime(), parent);
		}
		else if (obj instanceof Directory) {
			Directory o = (Directory)obj;
			return createDirectory(o.getName(), o.getUID(),
			                       o.getGID(), o.getMode(),
			                       o.getModTime(), parent);
		}
		else if (obj instanceof FIFO) {
			FIFO o = (FIFO)obj;
			return createFIFO(o.getName(), o.getUID(), o.getGID(), o.getMode(),
			                  o.getModTime(), parent);
		}
		else if (obj instanceof File) {
			File o = (File)obj;
			return createFile(o.getName(), o.getData(), o.getUID(), o.getGID(),
			                  o.getMode(), o.getModTime(), parent);
		}
		else if (obj instanceof Socket) {
			Socket o = (Socket)obj;
			return createSocket(o.getName(), o.getUID(), o.getGID(),
			                    o.getMode(), o.getModTime(), parent);
		}
		else if (obj instanceof Symlink) {
			Symlink o = (Symlink)obj;
			return createSymlink(o.getName(), o.getLinkName(), o.getUID(),
			                     o.getGID(), o.getMode(), o.getModTime(),
			                     parent);
		}
		else if (obj instanceof Whiteout) {
			Whiteout o = (Whiteout)obj;
			return createWhiteout(o.getName(), o.getUID(), o.getGID(), o.getMode(),
			                      o.getModTime(), parent);
		}
		else {
			throw new IllegalArgumentException("The generic filesystem does not support '" +
			                                   obj.getClass().getName() + "'");
		}
	}
	
	@Override
	public Directory convertFilesystem(Directory dir)
	{
		// Straight copy
		Directory root = createDirectory(dir.getName(), dir.getUID(),
		                                 dir.getGID(), dir.getMode(),
		                                 dir.getModTime(), null);
		
		for (FSObject child : dir) {
			convertFilesystem(child, root);
		}
		
		return root;
	}
	
	private void convertFilesystem(FSObject obj, Directory root)
	{
		try {
			FSObject conv = convertObject(obj, root);
			
			if (obj instanceof Directory) {
				Directory o = (Directory)obj;
				for (FSObject child : o) {
					convertFilesystem(child, (Directory)conv);
				}
			}
		}
		catch (IllegalArgumentException iae) {
			logger.warn(iae.getMessage(), iae);
		}
	}
}

/*
 * FSObject.java
 * 
 * The superclass for all filesystem objects.
 * 
 * Jonathan Pearson
 * November 10, 2009
 * 
 */

package com.sixnetio.fs.generic;

import java.io.PrintStream;
import java.util.StringTokenizer;


/**
 * Superclass for all filesystem objects.
 * 
 * @author Jonathan Pearson
 */
public abstract class FSObject
	implements Comparable<FSObject>
{
	// These are taken from dirent.h
	public static final byte DT_UNKNOWN = (byte)0x00, // Unknown
	                         DT_FIFO = (byte)0x01, // FIFO
	                         DT_CHR = (byte)0x02, // Character special
	                         DT_DIR = (byte)0x04, // Directory
	                         DT_BLK = (byte)0x06, // Block special
	                         DT_REG = (byte)0x08, // Regular file
	                         DT_LNK = (byte)0x0a, // Symlink
	                         DT_SOCK = (byte)0x0c, // Socket
	                         DT_WHT = (byte)0x0e; // Whiteout
	
	// These are the bit masks for the mode which also determine the file type
	public static final int MODE_FIFO = 0x1000,
	                        MODE_CHR = 0x2000,
	                        MODE_DIR = 0x4000,
	                        MODE_BLK = 0x6000,
	                        MODE_REG = 0x8000,
	                        MODE_LNK = 0xa000,
	                        MODE_SOCK = 0xc000,
	                        MODE_WHT = 0xe000; // This is an untested estimate
	
	/**
	 * Get the current time as it would be stored in the modtime time stamp.
	 * 
	 * @return The current time, as seconds since the epoch.
	 */
	public static int now()
	{
		return (int)((System.currentTimeMillis() / 1000) & 0xffffffff);
	}
	
	private String name;
	private byte type;
	private short uid, gid;
	private int mode, modtime;
	
	private Directory parent;
	
	/**
	 * Construct a new FSObject.
	 * 
	 * @param name The name of the object (not the full path, just the
	 *   name).
	 * @param type The type of the object (see the DT_* entries above).
	 * @param uid The owning user ID of the object.
	 * @param gid The owning group ID of the object.
	 * @param mode The permissions on the object.
	 * @param modtime The modification time of the object, in seconds since the
	 *   epoch (January 1, 1970, 00:00:00 UTC).
	 * @param parent The parent of the object, or null if this is the object
	 *   representing the root directory.
	 */
	protected FSObject(String name, byte type, short uid, short gid, int mode,
	                   int modtime, Directory parent)
	{
		this.name = name;
		this.uid = uid;
		this.gid = gid;
		this.mode = mode;
		this.modtime = modtime;
		
		if (parent == null) {
			this.parent = (Directory)this;
		}
		else {
			this.parent = parent;
		}
	}
	
	/** Get the owning user ID of this object. */
	public short getUID()
	{
		return uid;
	}
	
	/** Set the user ID of this object. */
	public void setUID(short uid)
	{
		this.uid = uid;
	}
	
	/** Get the owning group ID of this object. */
	public short getGID()
	{
		return gid;
	}
	
	/** Set the group ID of this object. */
	public void setGID(short gid)
	{
		this.gid = gid;
	}
	
	/** Get the permissions of this object. */
	public int getMode()
	{
		return mode;
	}
	
	/** Set the permissions on this object. */
	public void setMode(int mode)
	{
		this.mode = mode;
	}
	
	/** Get the modification time of this object, in seconds since the epoch. */
	public int getModTime()
	{
		return modtime;
	}
	
	/** Set the modification time of this object, in seconds since the epoch. */
	public void setModTime(int modtime)
	{
		this.modtime = modtime;
	}
	
	/** Update the modification time of this object to the current time. */
	public void updateModTime()
	{
		this.modtime = now();
	}
	
	/** Get the name of this object. */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Set the name of this object. Note: This will not update symlinks pointing
	 * at this object or using this object in the target path.
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	/**
	 * Get the type of this object. See the DT_* values above.
	 */
	public byte getType()
	{
		return type;
	}
	
	/**
	 * Get the parent of this object.
	 * 
	 * @return The parent of this object. Will be this object if this is the
	 *   root directory.
	 */
	public Directory getParent()
	{
		return parent;
	}
	
	/**
	 * Set the parent of this object. This will tell the current parent, if it
	 * is not <tt>null</tt> to remove this object as its child. This will tell
	 * the new parent, if it is not <tt>null</tt> to add this object as its
	 * child. Specialty type subclasses should override this with a test for
	 * the proper parent directory type.
	 */
	public void setParent(Directory parent)
	{
		if (this.parent != null) {
			this.parent.removeChild(this);
		}
		
		this.parent = parent;
		
		if (parent != null) {
			parent.addChild(this);
		}
	}
	
	/**
	 * Get the root of the file system.
	 * 
	 * @return The root of the file system. Will be this object if this is the
	 *   root directory.
	 */
	public Directory getRoot()
	{
		if (parent == this) {
			return (Directory)this;
		}
		else {
			return parent.getRoot();
		}
	}
	
	/**
	 * Get the absolute path to this object, separated by '/' characters. This
	 * will not include a leading '/'.
	 */
	public String getPath()
	{
		if (parent == this) {
			return "";
		}
		else {
			String parentPath = parent.getPath();
			if (parentPath.length() == 0) {
				return getName();
			}
			else {
				return parentPath + "/" + getName();
			}
		}
	}
	
	@Override
	public int compareTo(FSObject obj)
	{
		return getPath().compareTo(obj.getPath());
	}
	
	/** Returns "Name: Path" for this object. */
	@Override
	public String toString()
	{
		return getClass().getName() + ": " + getPath();
	}
	
	/** Translate the type byte to a descriptive string. */
	public abstract String getTypeAsString();
	
	/**
	 * Translate the type byte to a descriptive character, as would appear in
	 * the output of "ls -l".
	 */
	public abstract char getTypeAsChar();
	
	/**
	 * Get the mode of the object as it would appear in an "ls -l" listing,
	 * including the object type character.
	 */
	public String getModeAsString()
	{
		StringBuilder permBuilder = new StringBuilder();
		
		for (int block = 2; block >= 0; block--) {
			int userPerms = (mode >> (block * 3)) & 07;
			
			if ((userPerms & 4) == 4) {
				permBuilder.append('r');
			}
			else {
				permBuilder.append('-');
			}
			
			if ((userPerms & 2) == 2) {
				permBuilder.append('w');
			}
			else {
				permBuilder.append('-');
			}
			
			if ((userPerms & 1) == 1) {
				permBuilder.append('x');
			}
			else {
				permBuilder.append('-');
			}
		}
		
		int special = (mode >> 9) & 07;
		
		if ((special & 4) == 4) {
			if (permBuilder.charAt(2) == 'x') {
				permBuilder.setCharAt(2, 's');
			}
			else {
				permBuilder.setCharAt(2, 'S');
			}
		}
		
		if ((special & 2) == 2) {
			if (permBuilder.charAt(5) == 'x') {
				permBuilder.setCharAt(5, 's');
			}
			else {
				permBuilder.setCharAt(5, 'S');
			}
		}
		
		if ((special & 1) == 1) {
			if (permBuilder.charAt(8) == 'x') {
				permBuilder.setCharAt(8, 't');
			}
			else {
				permBuilder.setCharAt(8, 'T');
			}
		}
		
		return (getTypeAsChar() + permBuilder.toString());
	}
	
	/**
	 * Change the permissions on this file as the Unix 'chmod' command does.
	 * 
	 * @param commands A comma-separated list of strings that match this regex:
	 *   "[ugoa]+[+-][rwxXst]+"
	 */
	public void chmod(String commands)
	{
		// chmod supports these permission letters: rwxXst
		// r = read
		// w = write
		// x = execute/traverse
		// X = set x on directories, ignore files
		// s = suid/sgid
		// t = sticky
		
		// Convert into a number, with the goal of detecting errors BEFORE
		// making changes
		int perms = getMode();
		StringTokenizer tok = new StringTokenizer(commands, ",");
		while (tok.hasMoreTokens()) {
			String command = tok.nextToken();
			
			char how;
			if (command.indexOf('-') != -1) {
				how = '-';
			}
			else if (command.indexOf('+') != -1) {
				how = '+';
			}
			else {
				throw new IllegalArgumentException("Not a legal permission specification: " +
				                                   command);
			}
			
			String who = command.substring(0, command.indexOf(how)).toLowerCase();
			String change = command.substring(command.indexOf(how) + 1);
			
			// Easier to work with users one at a time
			if (who.indexOf('a') != -1) {
				who = "ugo";
			}
			
			for (int i = 0; i < who.length(); i++) {
				char one = who.charAt(i);
				
				for (int j = 0; j < change.length(); j++) {
					char perm = change.charAt(j);
					int bit = 0;
					
					switch (perm) {
						case 'r':
							bit = 4;
							break;
						case 'w':
							bit = 2;
							break;
						case 'x':
							bit = 1;
							break;
						case 'X':
							if (this instanceof Directory) {
								bit = 1;
							}
							break;
						case 's':
							if (one == 'u') {
								bit = 2048;
							}
							else if (one == 'g') {
								bit = 1024;
							}
							break;
						case 't':
							bit = 512;
							break;
						default:
							throw new IllegalArgumentException("Unrecognized permission: " +
							                                   perm);
					}
					
					if (bit <= 4) {
						if (one == 'u') {
							bit <<= 6;
						}
						else if (one == 'g') {
							bit <<= 3;
						}
					}
					
					if (how == '+') {
						perms |= bit;
					}
					else {
						perms &= ~bit;
					}
				}
			}
		}
		
		setMode(perms);
	}
	
	/**
	 * Set the permissions of this file using a format similar to 'ls -l'.
	 * 
	 * @param perms The new permissions of the file. Leave off the leading type
	 *   specifier. Example: "rwxr-xr-x".
	 */
	public void setMode(String perms)
	{
		int newPerms = 0;
		
		for (int i = 0; i < perms.length(); i++) {
			if (perms.charAt(i) == 'r') {
				if (i == 0) {
					newPerms |= 256;
				}
				else if (i == 3) {
					newPerms |= 32;
				}
				else if (i == 6) {
					newPerms |= 4;
				}
				else {
					throw new IllegalArgumentException("Illegal syntax, unexpected 'r': " +
					                                   perms);
				}
			}
			else if (perms.charAt(i) == 'w') {
				if (i == 1) {
					newPerms |= 128;
				}
				else if (i == 4) {
					newPerms |= 16;
				}
				else if (i == 7) {
					newPerms |= 2;
				}
				else {
					throw new IllegalArgumentException("Illegal syntax, unexpected 'w': " +
					                                   perms);
				}
			}
			else if (perms.charAt(i) == 'x') {
				if (i == 2) {
					newPerms |= 64;
				}
				else if (i == 5) {
					newPerms |= 8;
				}
				else if (i == 8) {
					newPerms |= 1;
				}
				else {
					throw new IllegalArgumentException("Illegal syntax, unexpected 'x': " +
					                                   perms);
				}
			}
			else if (perms.charAt(i) == 's') {
				if (i == 2) {
					newPerms |= (2048 | 64);
				}
				else if (i == 5) {
					newPerms |= (1024 | 8);
				}
				else {
					throw new IllegalArgumentException("Illegal syntax, unexpected 's': " +
					                                   perms);
				}
			}
			else if (perms.charAt(i) == 't') {
				if (i == 8) {
					newPerms |= (512 | 1);
				}
				else {
					throw new IllegalArgumentException("Illegal syntax, unexpected 't': " +
					                                   perms);
				}
			}
			else if (perms.charAt(i) == 'S') {
				if (i == 2) {
					newPerms |= 2048;
				}
				else if (i == 5) {
					newPerms |= 1024;
				}
				else {
					throw new IllegalArgumentException("Illegal syntax, unexpected 'S': " +
					                                   perms);
				}
			}
			else if (perms.charAt(i) == 'T') {
				if (i == 8) {
					newPerms |= 512;
				}
				else {
					throw new IllegalArgumentException("Illegal syntax, unexpected 'T': " +
					                                   perms);
				}
			}
		}
		
		// Replace the lowest 12 bits of the mode with the new permissions
		setMode((getMode() & 0xfffff000) | newPerms);
	}
	
	public void move(String newPath)
	{
		// Cut off any trailing '/'s
		while (newPath.endsWith("/")) {
			newPath = newPath.substring(0, newPath.length() - 1);
		}
		
		// Convert any '//' to '/'
		while (newPath.contains("//")) {
			newPath.replaceAll("//", "/");
		}
		
		// Split into parent path and name
		String parentDir;
		String name;
		
		// Cut off the final portion of the path, if there is one
		if (newPath.indexOf('/') != -1) {
			name = newPath.substring(newPath.lastIndexOf('/') + 1);
			parentDir = newPath.substring(0, newPath.lastIndexOf('/'));
		}
		else {
			name = newPath;
			parentDir = "";
		}
		
		// Update the parent as necessary
		if ( ! parentDir.equals(getParent().getPath())) {
			Directory newParent = (Directory)parent.locate(parentDir);
			
			if (newParent == null) {
				throw new IllegalArgumentException("Unable to find new parent directory '" +
				                                   parentDir + "'");
			}
			
			// Move this object to its new location
			setParent(newParent);
		}
		
		// Set the new name for the object
		setName(name);
	}
	
	/**
	 * Dump the file system layout (starting at this directory) to the given
	 * stream. The format will be similar to "ls -l", but will include
	 * indentations in file names to indicate directory structure.
	 * 
	 * @param out The stream to write to.
	 */
	public final void dump(PrintStream out)
	{
		dump(0, out);
	}
	
	/**
	 * Dump this object using the specified indentation level for the file name.
	 * 
	 * @param indentLevel The indentation level to use. Each level is equivalent
	 *   to two spaces at the beginning of the file name (not at the beginning
	 *   of the line).
	 * @param out The stream to write to.
	 */
	public abstract void dump(int indentLevel, PrintStream out);
	
	/**
	 * Get the line that would be printed if just this object were to be dumped,
	 * and not any of its children.
	 * 
	 * @param indentLevel The indentation level.
	 * @return The string that {@link #dump(int, PrintStream)} would print for
	 *   just this object, and none of its children.
	 */
	public abstract String getDumpLine(int indentLevel);
	
	/**
	 * Check whether this object is equal to another object.
	 * 
	 * @return <tt>true</tt> if the other object is as FSObject with the same
	 *   root object (by reference) as this one, and the other object has the
	 *   same path. <tt>false</tt> otherwise.
	 */
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof FSObject) {
			FSObject obj = (FSObject)o;
			
			return (getRoot() == obj.getRoot() &&
					getPath().equals(obj.getPath()));
		}
		else {
			return false;
		}
	}
	
	@Override
	public int hashCode()
	{
		return getPath().hashCode();
	}
}

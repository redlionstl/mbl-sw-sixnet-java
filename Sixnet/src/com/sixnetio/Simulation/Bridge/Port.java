/*
 * Port.java
 *
 * Represents a single port on a simulated bridge.
 *
 * Jonathan Pearson
 * September 16, 2008
 *
 */

package com.sixnetio.Simulation.Bridge;

import com.sixnetio.net.*;
import com.sixnetio.util.*;
import com.sixnetio.Simulation.Bridge.rstp.*;

import java.io.*;

public class Port extends Thread {
	private Bridge bridge;
	private RSTPPort rstpPort;
	
	private int portNumber;
	private EthernetSocket sock; // For communication
	
	private boolean learning = true;
	private boolean forwarding = true;
	
	public Port(Bridge bridge, int portNumber, EthernetSocket sock) {
		super(bridge.toString() + ":" + portNumber);
		
		this.bridge = bridge;
		this.portNumber = portNumber;
		this.sock = sock;
		
		this.rstpPort = new RSTPPort(this);
		
		setDaemon(true);
	}
	
	public boolean isLearning() {
		return learning;
	}
	
	public void setLearning(boolean learning) {
		boolean fireEvent = (this.learning != learning);
		
		this.learning = learning;
		
		if (fireEvent) bridge.firePortLearning(getPortNumber(), learning);
	}
	
	public boolean isForwarding() {
		return forwarding;
	}
	
	public void setForwarding(boolean forwarding) {
		boolean fireEvent = (this.forwarding != forwarding);
		
		this.forwarding = forwarding;
		
		if (fireEvent) bridge.firePortForwarding(getPortNumber(), forwarding);
	}
	
	public int getPortNumber() {
		return portNumber;
	}
	
	public Bridge getBridge() {
		return bridge;
	}
	
	public RSTPPort getRSTPPort() {
		return rstpPort;
	}
	
	public void run() {
		while (sock != null && bridge.isRunning()) {
			try {
				EthernetPacket packet = sock.receiveNonBlocking();
				if (packet == null) continue;
				
				bridge.route(packet, portNumber);
			} catch (IOException ioe) {
				Utils.debug(ioe);
			}
		}
	}
	
	protected void setSock(EthernetSocket sock) {
		if (this.sock == null) {
			this.sock = sock;
			start();
		} else {
			this.sock = sock;
		}
	}
	
	protected void send(EthernetPacket pkt) throws IOException {
		sock.send(pkt);
	}
	
	public void sendBPDU(BPDU bpdu) {
		EthernetPacket pkt = new EthernetPacket(bridge.getAddress(), Bridge.STP_ADDRESS, bpdu.getBytes());
		
		try {
			send(pkt);
		} catch (IOException ioe) {
			Utils.debug(ioe);
		}
	}
	
	protected void clearSock() {
		// It is possible that the port thread is about to start but the socket has been running for some time
		if (sock == null) return; // No socket? No data
		
		EthernetPacket pkt = null;
		do {
			try {
				pkt = sock.receiveNonBlocking();
			} catch (IOException ioe) {
				// Assume that there is no more data
				Utils.debug(ioe);
				pkt = null;
			}
		} while (pkt != null);
	}
}

/*
 * Bridge.java
 *
 * Performs the functions of a Layer-3 bridge, using both physical and virtual ports.
 *
 * Jonathan Pearson
 * September 16, 2008
 *
 */

package com.sixnetio.Simulation.Bridge;

import java.io.IOException;
import java.util.*;

import com.sixnetio.Simulation.Bridge.rstp.*;
import com.sixnetio.net.*;
import com.sixnetio.util.Utils;

public class Bridge {
	public static final EthernetAddress STP_ADDRESS = new EthernetAddress("01:80:C2:00:00:00");
	public static final int FILTERING_DATABASES = 256;
	
	private final EthernetAddress address;
	
	private boolean running;
	private Port[] ports;
	
	private RSTPBridge rstpBridge;
	
	// Read-only after the constructor
	private List<FilteringDatabase> filteringDBs = new ArrayList<FilteringDatabase>(FILTERING_DATABASES);
	
	// Aging time for filtering database entries in seconds; it has an extra 'e' because it is in the spec
	private int AgeingTime = 300; // 10-1,000,000
	
	private List<BridgeListener> bridgeListeners = new LinkedList<BridgeListener>();
	
	public Bridge(EthernetAddress address, int portCount) {
		this.address = address;
		
		// Create the array of ports here because the RSTPBridge needs to know how many there are
		ports = new Port[portCount];
		
		// Create the filtering databases
		for (int i = 0; i < FILTERING_DATABASES; i++) {
			filteringDBs.add(new FilteringDatabase(this));
		}
		
		rstpBridge = new RSTPBridge(this);
		
		// Create the ports; these need to access the rstpBridge
		for (int i = 0; i < ports.length; i++) {
			ports[i] = new Port(this, i, null);
		}
		
		rstpBridge.start();
		
		running = true;
	}
	
	public void addBridgeListener(BridgeListener listener) {
		synchronized (bridgeListeners) {
	        if (!bridgeListeners.contains(listener)) {
		        bridgeListeners.add(listener);
	        }
        }
	}
	
	public void removeBridgeListener(BridgeListener listener) {
		synchronized (bridgeListeners) {
	        bridgeListeners.remove(listener);
        }
	}
	
	protected void firePortLearning(int portNum, boolean learning) {
		List<BridgeListener> listeners;
		synchronized (bridgeListeners) {
			listeners = new ArrayList<BridgeListener>(bridgeListeners);
		}
		
		for (BridgeListener listener : listeners) {
			listener.learningChanged(portNum, learning);
		}
	}
	
	protected void firePortForwarding(int portNum, boolean forwarding) {
		List<BridgeListener> listeners;
		synchronized (bridgeListeners) {
			listeners = new ArrayList<BridgeListener>(bridgeListeners);
		}
		
		for (BridgeListener listener : listeners) {
			listener.forwardingChanged(portNum, forwarding);
		}
	}
	
	/**
	 * Do not call this, it is public only as an implementation detail.
	 */
	public void fireTopologyChange() {
		List<BridgeListener> listeners;
		synchronized (bridgeListeners) {
			listeners = new ArrayList<BridgeListener>(bridgeListeners);
		}
		
		for (BridgeListener listener : listeners) {
			listener.topologyChange();
		}
	}
	
	protected void fireTrafficFlow(int fromPort, int toPort) {
		List<BridgeListener> listeners;
		synchronized (bridgeListeners) {
			listeners = new ArrayList<BridgeListener>(bridgeListeners);
		}
		
		for (BridgeListener listener : listeners) {
			listener.trafficFlow(fromPort, toPort);
		}
	}
	
	/**
	 * Do not call this, it is public only as an implementation detail.
	 */
	public void fireRootChanged(boolean thisBridge, BridgeID rootID) {
		List<BridgeListener> listeners;
		synchronized (bridgeListeners) {
			listeners = new ArrayList<BridgeListener>(bridgeListeners);
		}
		
		for (BridgeListener listener : listeners) {
			listener.rootChanged(thisBridge, rootID);
		}
	}
	
	public EthernetAddress getAddress() {
    	return address;
    }
	
	public int getPortCount() {
		return ports.length;
	}
	
	public Port getPort(int index) {
		return ports[index];
	}
	
	public RSTPBridge getRSTPBridge() {
		return rstpBridge;
	}
	
	public void setPortSock(int portNum, EthernetSocket sock) {
		ports[portNum].setSock(sock);
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public void setRunning(boolean running) {
		if (!this.running && running) {
			rstpBridge.reset();
			
			// Going from false to true means we need to start up the port threads
			this.running = running;
			
			for (int i = 0; i < ports.length; i++) {
				ports[i].clearSock();
				ports[i].start();
			}
		} else {
			this.running = running;
		}
	}
	
	public int getAgeingTime() {
    	return AgeingTime;
    }
	
	public void setAgeingTime(int ageingTime) {
    	AgeingTime = ageingTime;
    }
	
	public Iterator<FilteringDatabase> getFilteringDatabases() {
		return Utils.readOnlyIterator(filteringDBs.iterator());
	}
	
	protected void route(EthernetPacket pkt, int recvPort) {
		// FUTURE: Use more than just FDB 0, based on VLANs and independent learning
		FilteringDatabase fdb = filteringDBs.get(0);
		
		// Look up static entries for the destination address in the filtering database
		EthernetAddress dstAddr = pkt.getDstAddr();
		
		// Check for bridge filtered addresses and handle them specially
		{
			byte[] addressBytes = dstAddr.getBytes();
			if (addressBytes[0] == (byte)0x01 &&
				addressBytes[1] == (byte)0x80 &&
				addressBytes[2] == (byte)0xC2 &&
				addressBytes[3] == (byte)0x00 &&
				addressBytes[4] == (byte)0x00 &&
				(addressBytes[5] & (byte)0xf0) == 0) {
				
				// Bridge filtered (Non-forwardable) address
				// Check if it is the spanning tree address
				if (dstAddr.equals(STP_ADDRESS)) {
					handleBPDU(pkt, recvPort);
					return;
				} else {
					// Discard it
					return;
				}
			}
		}
		
		// Check the RSTP state for this port; if not learning, discard
		if (!ports[recvPort].isLearning()) return;
		
		// Learn the source address
		fdb.learn(pkt.getSrcAddr(), recvPort);
		
		// Check the RSTP state for this port; if not forwarding, discard
		if (!ports[recvPort].isForwarding()) return;
		
		int outPort = -1;
		boolean forward = true;
		if (fdb.hasStaticEntry(dstAddr)) {
			if (fdb.getStaticForwarding(dstAddr)) {
				outPort = fdb.getStaticPort(dstAddr);
			} else {
				forward = false;
			}
		}
		// No static entries? Look up dynamic entries
		else if (fdb.hasDynamicEntry(dstAddr)) {
			if (fdb.getDynamicForwarding(dstAddr)) {
				outPort = fdb.getDynamicPort(dstAddr);
			} else {
				forward = false;
			}
		}
		
		if (forward) {
			// Packet is being forwarded... through which ports?
			if (outPort == -1) {
				// All other ports, since we don't know where it came from
				for (int i = 0; i < ports.length; i++) {
					if (i != recvPort && ports[i].isForwarding()) {
    					try {
    						ports[i].send(pkt);
    					} catch (IOException ioe) {
    						// Unable to send the packet out this port? Just report the exception for now
    						Utils.debug(ioe);
    					}
					}
				}
				
				fireTrafficFlow(recvPort, -1);
			} else {
				// We know where to send it
				if (outPort != recvPort) {
    				try {
    					ports[outPort].send(pkt);
    				} catch (IOException ioe) {
    					Utils.debug(ioe);
    				}
    				
    				fireTrafficFlow(recvPort, outPort);
				}
			}
		}
	}

	private void handleBPDU(EthernetPacket pkt, int recvPort) {
	    BPDU bpdu = BPDU.parseBPDU(pkt.getPayload());
	    if (bpdu == null) return;
	    
	    // Make sure that there is no loopback condition -- this BPDU should not have come from this port
	    if (bpdu.getBpduType() == BPDU.T_CONFIG ||
    		bpdu.getBpduType() == BPDU.T_RST) {
	    	
	    	// For Configuration BPDUs, compare to this bridge's ID and this port's ID; if equal, discard
	    	if (bpdu.getBpduType() == BPDU.T_CONFIG) {
	    		BridgeID bridgeID = bpdu.getBridgeID();
		    	PortID portID = bpdu.getPortID();
		    	
		    	if (bridgeID.equals(rstpBridge.getBridgeID()) &&
		    		portID.equals(ports[recvPort].getRSTPPort().getPortID())) return;
	    	}
	    	
	    	// Pass the BPDU on to the port's RSTP instance and run the state machines
	    	ports[recvPort].getRSTPPort().handleBPDU(bpdu);
	    	rstpBridge.stateStep();
	    }
    }
}

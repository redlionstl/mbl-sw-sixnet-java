/*
 * PortRoleTransitions.java
 *
 * The Port Role Transitions state machine. This is built out of
 * four mini state machines: AlternateBackupPort, DesignatedPort,
 * DisabledPortStates, and RootPort.
 *
 * Jonathan Pearson
 * December 15, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class PortRoleTransitionSM extends PortStateMachine {
	public enum State {
		None,
		AlternateBackup_Enter,
		AlternateBackup_Steady,
		Designated_Enter,
		Designated_Steady,
		Disabled_Enter,
		Disabled_Steady,
		Root_Enter,
		Root_Steady
	}
	
	private State state = State.None;
	
	private AlternateBackupPortStatesSubSM alternateBackupSM;
	private DesignatedPortStatesSubSM designatedSM;
	private DisabledPortStatesSubSM disabledSM;
	private RootPortStatesSubSM rootSM;
	
	public PortRoleTransitionSM(RSTPPort port) {
		super(port);
		
		this.alternateBackupSM = new AlternateBackupPortStatesSubSM(port);
		this.designatedSM = new DesignatedPortStatesSubSM(port);
		this.disabledSM = new DisabledPortStatesSubSM(port);
		this.rootSM = new RootPortStatesSubSM(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case AlternateBackup_Enter:
				state = State.AlternateBackup_Steady;
			case AlternateBackup_Steady:
				alternateBackupSM.stateStep();
				break;
				
			case Designated_Enter:
				state = State.Designated_Steady;
			case Designated_Steady:
				designatedSM.stateStep();
				break;
				
			case Disabled_Enter:
				state = State.Disabled_Steady;
			case Disabled_Steady:
				disabledSM.stateStep();
				break;
				
			case Root_Enter:
				state = State.Root_Steady;
			case Root_Steady:
				rootSM.stateStep();
				break;
		}
		
		if (port.selected && !port.updtInfo) {
			// We update port.bridge.stateChanged manually in here because changes in the
			//   state of a sub-machine may not correspond to a state change in this
			//   machine, which is the only test being performed below
			
			if (((port.selectedRole == Role.Alternate) ||
				 (port.selectedRole == Role.Backup)) &&
				(port.role != port.selectedRole)) {
				
				state = State.AlternateBackup_Enter;
				alternateBackupSM.state = AlternateBackupPortStatesSubSM.State.BlockPort_Enter;
				
				port.bridge.stateChanged = true;
			} else if ((port.selectedRole == Role.Designated) &&
					   (port.role != port.selectedRole)) {
				
				state = State.Designated_Enter;
				designatedSM.state = DesignatedPortStatesSubSM.State.DesignatedPort_Enter;
				
				port.bridge.stateChanged = true;
			} else if (port.bridge.begin) {
				state = State.Disabled_Enter;
				disabledSM.state = DisabledPortStatesSubSM.State.InitPort_Enter;
				
				port.bridge.stateChanged = true;
			} else if ((port.selectedRole == Role.Disabled) &&
			           (port.role != port.selectedRole)) {
				
				state = State.Disabled_Enter;
				disabledSM.state = DisabledPortStatesSubSM.State.DisablePort_Enter;
				
				port.bridge.stateChanged = true;
			} else if ((port.selectedRole == Role.Root) &&
			           (port.role != port.selectedRole)) {
				
				state = State.Root_Enter;
				rootSM.state = RootPortStatesSubSM.State.RootPort_Enter;
				
				port.bridge.stateChanged = true;
			}
		}
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
}

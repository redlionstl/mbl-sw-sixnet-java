/*
 * PortTransmit.java
 *
 * The Port Transmit state machine.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class PortTransmitSM extends PortStateMachine {
	public enum State {
		None,
		TransmitInit_Enter,
		TransmitInit_Steady,
		TransmitConfig_Enter,
		TransmitConfig_Steady,
		TransmitPeriodic_Enter,
		TransmitPeriodic_Steady,
		TransmitTCN_Enter,
		TransmitTCN_Steady,
		TransmitRstp_Enter,
		TransmitRstp_Steady,
		Idle_Enter,
		Idle_Steady;
	}
	
	private State state = State.None;
	
	public PortTransmitSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case TransmitInit_Enter:
				port.newInfo = true;
				port.txCount = 0;
				state = State.TransmitInit_Steady;
			case TransmitInit_Steady:
				if (true) state = State.Idle_Enter;
				break;
				
			case TransmitConfig_Enter:
				port.newInfo = false;
				port.txConfig();
				port.txCount++;
				port.tcAck = false;
				state = State.TransmitConfig_Steady;
			case TransmitConfig_Steady:
				if (true) state = State.Idle_Enter;
				break;
				
			case TransmitPeriodic_Enter:
				port.newInfo = port.newInfo || (port.role == Role.Designated || (port.role == Role.Root && (port.tcWhile != 0)));
				state = State.TransmitPeriodic_Steady;
			case TransmitPeriodic_Steady:
				if (true) state = State.Idle_Enter;
				break;
				
			case TransmitTCN_Enter:
				port.newInfo = false;
				port.txTcn();
				port.txCount++;
				state = State.TransmitTCN_Steady;
			case TransmitTCN_Steady:
				if (true) state = State.Idle_Enter;
				break;
				
			case TransmitRstp_Enter:
				port.newInfo = false;
				port.txRstp();
				port.txCount++;
				port.tcAck = false;
				state = State.TransmitRstp_Steady;
			case TransmitRstp_Steady:
				if (true) state = State.Idle_Enter;
				break;
				
			case Idle_Enter:
				port.helloWhen = port.getHelloTime();
				state = State.Idle_Steady;
			case Idle_Steady:
				if (port.selected && !port.updtInfo) {
					if (port.sendRSTP && port.newInfo && (port.txCount < port.getTxHoldCount()) && (port.helloWhen != 0)) state = State.TransmitRstp_Enter;
					if (!port.sendRSTP && port.newInfo && port.role == Role.Root && (port.txCount < port.getTxHoldCount()) && (port.helloWhen != 0)) state = State.TransmitTCN_Enter;
					if (!port.sendRSTP && port.newInfo && port.role == Role.Designated && (port.txCount < port.getTxHoldCount()) && (port.helloWhen != 0)) state = State.TransmitConfig_Enter;
					if (port.helloWhen == 0) state = State.TransmitPeriodic_Enter;
				}
				break;
		}
		
		if (port.bridge.begin) state = State.TransmitInit_Enter;
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

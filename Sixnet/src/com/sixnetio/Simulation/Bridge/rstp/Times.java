/*
 * Times.java
 *
 * This set of variables comes up regularly, this is a simple encapsulation for it.
 *
 * Jonathan Pearson
 * September 12, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

class Times {
	// These should all be kept in increments of 1/256th of a second
	public short messageAge;
	public short maxAge;
	public short forwardDelay;
	public short helloTime;
	
	public Times() { }
	
	public Times(short messageAge, short maxAge, short forwardDelay, short helloTime) {
		this.messageAge = messageAge;
		this.maxAge = maxAge;
		this.forwardDelay = forwardDelay;
		this.helloTime = helloTime;
	}
	
	public void copy(Times times) {
		this.messageAge = times.messageAge;
		this.maxAge = times.maxAge;
		this.forwardDelay = times.forwardDelay;
		this.helloTime = times.helloTime;
	}
	
	@Override
    public boolean equals(Object obj) {
		if (!(obj instanceof Times)) return false;
		
		Times rhs = (Times)obj;
		
		return (this.messageAge == rhs.messageAge &&
				this.maxAge == rhs.maxAge &&
				this.forwardDelay == rhs.forwardDelay &&
				this.helloTime == rhs.helloTime);
	}
	
	@Override
    public int hashCode() {
		return (messageAge + maxAge + forwardDelay + helloTime);
	}
}

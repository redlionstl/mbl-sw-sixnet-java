/*
 * BridgeDetection.java
 *
 * The Bridge Detection state machine.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class BridgeDetectionSM extends PortStateMachine {
	public enum State {
		None,
		Edge_Enter,
		Edge_Steady,
		NotEdge_Enter,
		NotEdge_Steady
	}
	
	private State state = State.None;
	
	public BridgeDetectionSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case Edge_Enter:
				port.operEdge = true;
				state = State.Edge_Steady;
			case Edge_Steady:
				if ((!port.portEnabled && !port.getAdminEdge()) || !port.operEdge) state = State.NotEdge_Enter;
				break;
				
			case NotEdge_Enter:
				port.operEdge = false;
				state = State.NotEdge_Steady;
			case NotEdge_Steady:
				if ((!port.portEnabled && port.getAdminEdge()) ||
					((port.edgeDelayWhile == 0) && port.getAutoEdge() && port.sendRSTP && port.proposing)) state = State.Edge_Enter;
				break;
		}
		
		if (port.bridge.begin && port.getAdminEdge()) state = State.Edge_Enter;
		if (port.bridge.begin && !port.getAdminEdge()) state = State.NotEdge_Enter;
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

/*
 * BridgeStateMachine.java
 *
 * The bridge-specific state machine(s) extend this
 *
 * Jonathan Pearson
 * December 15, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public abstract class BridgeStateMachine extends StateMachine {
	protected RSTPBridge bridge;
	
	public BridgeStateMachine(RSTPBridge bridge) {
		this.bridge = bridge;
	}
}

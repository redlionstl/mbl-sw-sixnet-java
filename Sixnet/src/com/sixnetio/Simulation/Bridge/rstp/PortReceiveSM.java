/*
 * PortReceive.java
 *
 * The Port Receive state machine.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class PortReceiveSM extends PortStateMachine {
	public enum State {
		None,
		Discard_Enter,
		Discard_Steady,
		Receive_Enter,
		Receive_Steady;
	}
	
	private State state = State.None;
	
	public PortReceiveSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case Discard_Enter:
				port.rcvdBPDU = port.rcvdRSTP = port.rcvdSTP = port.rcvdMsg = false;
				port.edgeDelayWhile = port.bridge.getMigrateTime();
				state = State.Discard_Steady;
			case Discard_Steady:
				if (port.rcvdBPDU && port.portEnabled) state = State.Receive_Enter;
				break;
				
			case Receive_Enter:
				port.updtBPDUVersion();
				port.operEdge = port.rcvdBPDU = false;
				port.rcvdMsg = true;
				port.edgeDelayWhile = port.bridge.getMigrateTime();
				state = State.Receive_Steady;
			case Receive_Steady:
				if (port.rcvdBPDU && port.portEnabled && !port.rcvdMsg) state = State.Receive_Enter;
				break;
		}
		
		if (port.bridge.begin || ((port.rcvdBPDU || (port.edgeDelayWhile != port.bridge.getMigrateTime())) && !port.portEnabled)) state = State.Discard_Enter;
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

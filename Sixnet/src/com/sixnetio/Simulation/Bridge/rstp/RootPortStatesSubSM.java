/*
 * RootPort.java
 *
 * The Root Port state machine. This is a mini state machine,
 * part of the PortRoleTransitions state machine.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class RootPortStatesSubSM extends PortStateMachine {
	public enum State {
		None,
		RootProposed_Enter,
		RootProposed_Steady,
		RootAgreed_Enter,
		RootAgreed_Steady,
		Reroot_Enter,
		Reroot_Steady,
		RootForward_Enter,
		RootForward_Steady,
		RootLearn_Enter,
		RootLearn_Steady,
		Rerooted_Enter,
		Rerooted_Steady,
		RootPort_Enter,
		RootPort_Steady;
	}
	
	// This can be controlled by PortRoleTransitions, the super-state machine of this one
	protected State state = State.None;
	
	public RootPortStatesSubSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case RootProposed_Enter:
				port.bridge.setSyncTree();
				port.proposed = false;
				state = State.RootProposed_Steady;
			case RootProposed_Steady:
				if (true) state = State.RootPort_Enter;
				break;
				
			case RootAgreed_Enter:
				port.proposed = port.sync = false;
				port.agree = port.newInfo = true;
				state = State.RootAgreed_Steady;
			case RootAgreed_Steady:
				if (true) state = State.RootPort_Enter;
				break;
				
			case Reroot_Enter:
				port.bridge.setReRootTree();
				state = State.Reroot_Steady;
			case Reroot_Steady:
				if (true) state = State.RootPort_Enter;
				break;
				
			case RootForward_Enter:
				port.fdWhile = 0;
				port.forward = true;
				state = State.RootForward_Steady;
			case RootForward_Steady:
				if (true) state = State.RootPort_Enter;
				break;
				
			case RootLearn_Enter:
				port.fdWhile = port.getForwardDelay();
				port.learn = true;
				state = State.RootLearn_Steady;
			case RootLearn_Steady:
				if (true) state = State.RootPort_Enter;
				break;
				
			case Rerooted_Enter:
				port.reRoot = false;
				state = State.Rerooted_Steady;
			case Rerooted_Steady:
				if (true) state = State.RootPort_Enter;
				break;
				
			case RootPort_Enter:
				port.role = Role.Root;
				port.rrWhile = port.getFwdDelay();
				state = State.RootPort_Steady;
			case RootPort_Steady:
				if (port.selected && !port.updtInfo) {
					if (port.rrWhile != port.getFwdDelay()) state = State.RootPort_Enter;
					if (port.reRoot && port.forward) state = State.Rerooted_Enter;
					if (((port.fdWhile == 0) || (port.getReRooted() && (port.rbWhile == 0)) && port.bridge.getRSTPVersion()) && !port.learn) state = State.RootLearn_Enter;
					if (((port.fdWhile == 0) || (port.getReRooted() && (port.rbWhile == 0)) && port.bridge.getRSTPVersion()) && port.learn && !port.forward) state = State.RootForward_Enter;
					if (!port.forward && !port.reRoot) state = State.Reroot_Enter;
					if ((port.bridge.getAllSynced() && !port.agree) || (port.proposed && port.agree)) state = State.RootAgreed_Enter;
					if (port.proposed && !port.agree) state = State.RootProposed_Enter;
				}
		}
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

/*
 * PortPriorityVector.java
 *
 * Represents a Port Priority Vector (see section 17.6 of the 802.1D-2004 spec)
 *
 * Jonathan Pearson
 * September 15, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

import com.sixnetio.util.Conversion;

class PortPriorityVector extends RSTPVector {
	protected BridgeID rootBridgeID;
	protected int rootPathCost;
	
	protected BridgeID designatedBridgeID;
	protected PortID designatedPortID;
	
	protected PortID bridgePortID;
	
	public PortPriorityVector(BridgeID rootBridgeID, int rootPathCost, BridgeID designatedBridgeID, PortID designatedPortID, PortID bridgePortID) {
		this.rootBridgeID = rootBridgeID;
		this.rootPathCost = rootPathCost;
		this.designatedBridgeID = designatedBridgeID;
		this.designatedPortID = designatedPortID;
		this.bridgePortID = bridgePortID;
	}
	
	public PortPriorityVector(byte[] ppv) {
		this(ppv, 0);
	}
	
	public PortPriorityVector(byte[] ppv, int off) {
		rootBridgeID = new BridgeID(ppv, off + 0);
		rootPathCost = Conversion.bytesToInt(ppv, off + 8);
		
		designatedBridgeID = new BridgeID(ppv, off + 12);
		designatedPortID = new PortID(ppv, off + 20);
		
		bridgePortID = new PortID(ppv, off + 22);
	}
	
	public byte[] getBytes() {
		byte[] ppv = new byte[24];
		
		System.arraycopy(rootBridgeID.getBytes(), 0, ppv, 0, 8);
		Conversion.intToBytes(ppv, 8, rootPathCost);
		
		System.arraycopy(designatedBridgeID.getBytes(), 0, ppv, 12, 8);
		System.arraycopy(designatedPortID.getBytes(), 0, ppv, 20, 2);
		
		System.arraycopy(bridgePortID.getBytes(), 0, ppv, 22, 2);
		
		return ppv;
	}
	
	public BridgeID getRootBridgeID() {
    	return rootBridgeID;
    }
	
	public void setRootBridgeID(BridgeID rootBridgeID) {
    	this.rootBridgeID = rootBridgeID;
    }
	
	public int getRootPathCost() {
    	return rootPathCost;
    }
	
	public void setRootPathCost(int rootPathCost) {
    	this.rootPathCost = rootPathCost;
    }
	
	public BridgeID getDesignatedBridgeID() {
    	return designatedBridgeID;
    }
	
	public void setDesignatedBridgeID(BridgeID designatedBridgeID) {
    	this.designatedBridgeID = designatedBridgeID;
    }
	
	public PortID getDesignatedPortID() {
    	return designatedPortID;
    }
	
	public void setDesignatedPortID(PortID designatedPortID) {
    	this.designatedPortID = designatedPortID;
    }
	
	public PortID getBridgePortID() {
    	return bridgePortID;
    }
	
	public void setBridgePortID(PortID bridgePortID) {
    	this.bridgePortID = bridgePortID;
    }
}

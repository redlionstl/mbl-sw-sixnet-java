/*
 * PortTimers.java
 *
 * The Port Timers state machine.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class PortTimersSM extends PortStateMachine {
	public enum State {
		None,
		OneSecond_Enter,
		OneSecond_Steady,
		Tick_Enter,
		Tick_Steady;
	}
	
	private State state = State.None;
	
	public PortTimersSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case OneSecond_Enter:
				port.bridge.tick = false;
				state = State.OneSecond_Steady;
			case OneSecond_Steady:
				if (port.bridge.tick) state = State.Tick_Enter;
				break;
				
			case Tick_Enter:
				if (port.helloWhen < 256) {
					port.helloWhen = 0;
				} else {
					port.helloWhen -= 256;
				}
				
				if (port.tcWhile < 256) {
					port.tcWhile = 0;
				} else {
					port.tcWhile -= 256;
				}
				
				if (port.fdWhile < 256) {
					port.fdWhile = 0;
				} else {
					port.fdWhile -= 256;
				}
				
				if (port.rcvdInfoWhile < 256) {
					port.rcvdInfoWhile = 0;
				} else {
					port.rcvdInfoWhile -= 256;
				}
				
				if (port.rrWhile < 256) {
					port.rrWhile = 0;
				} else {
					port.rrWhile -= 256;
				}
				
				if (port.rbWhile < 256) {
					port.rbWhile = 0;
				} else {
					port.rbWhile -= 256;
				}
				
				if (port.mdelayWhile < 256) {
					port.mdelayWhile = 0;
				} else {
					port.mdelayWhile -= 256;
				}
				
				if (port.edgeDelayWhile < 256) {
					port.edgeDelayWhile = 0;
				} else {
					port.edgeDelayWhile -= 256;
				}
				
				if (port.txCount > 0) {
					port.txCount--;
				}
				
				state = State.Tick_Steady;
			case Tick_Steady:
				if (true) state = State.OneSecond_Enter;
				break;
		}
		
		if (port.bridge.begin) state = State.OneSecond_Enter;
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

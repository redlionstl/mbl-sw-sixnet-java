/*
 * PortStateMachine.java
 *
 * The port-specific state machines extend this
 *
 * Jonathan Pearson
 * December 15, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public abstract class PortStateMachine extends StateMachine {
	protected RSTPPort port;
	
	public PortStateMachine(RSTPPort port) {
		this.port = port;
	}
}

/*
 * PortTimers.java
 *
 * The Disabled Port States state machine. This is a mini state machine,
 * part of the PortRoleTransitions state machine.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class DisabledPortStatesSubSM extends PortStateMachine {
	public enum State {
		None,
		InitPort_Enter,
		InitPort_Steady,
		DisablePort_Enter,
		DisablePort_Steady,
		DisabledPort_Enter,
		DisabledPort_Steady;
	}
	
	// This can be controlled by PortRoleTransitions, the super-state machine of this one
	protected State state = State.None;
	
	public DisabledPortStatesSubSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case InitPort_Enter:
				port.role = Role.Disabled;
				port.learn = port.forward = port.synced = false;
				port.sync = port.reRoot = true;
				port.rrWhile = port.getFwdDelay();
				port.fdWhile = port.getMaxAge();
				port.rbWhile = 0;
				state = State.InitPort_Steady;
			case InitPort_Steady:
				if (true) state = State.DisablePort_Enter;
				break;
				
			case DisablePort_Enter:
				port.role = port.selectedRole;
				port.learn = port.forward = false;
				state = State.DisablePort_Steady;
			case DisablePort_Steady:
				if (port.selected && !port.updtInfo) {
					if (!port.learning && !port.forwarding) state = State.DisabledPort_Enter;
				}
				break;
				
			case DisabledPort_Enter:
				port.fdWhile = port.getMaxAge();
				port.synced = true;
				port.rrWhile = 0;
				port.sync = port.reRoot = false;
				state = State.DisabledPort_Steady;
			case DisabledPort_Steady:
				if (port.selected && !port.updtInfo) {
					if ((port.fdWhile != port.getMaxAge()) || port.sync || port.reRoot || !port.synced) state = State.DisabledPort_Enter;
				}
				break;
		}
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

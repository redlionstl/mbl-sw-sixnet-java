/*
 * PortID.java
 *
 * Represents a Port Identifier (priority and number)
 *
 * Jonathan Pearson
 * September 15, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

import com.sixnetio.util.Conversion;

public class PortID extends RSTPVector {
	protected byte priority; // Only most significant 4 bits may be set
	protected final short portNumber; // Only least significant 12 bits may be set
	
	public PortID(byte priority, short portNumber) {
		if ((priority & 0x0f) != 0) throw new IllegalArgumentException("May not use lower 4 bits of priority");
		if ((portNumber & 0xf000) != 0) throw new IllegalArgumentException("May not use upper 4 bits of port number");
		
		this.priority = priority;
		this.portNumber = portNumber;
	}
	
	public PortID(byte[] pid) {
		this(pid, 0);
	}
	
	public PortID(byte[] pid, int off) {
		priority = (byte)(pid[off] & 0xf0);
		portNumber = (short)(Conversion.bytesToShort(pid, off) & 0x0fff);
	}
	
	public byte[] getBytes() {
		byte[] pid = new byte[2];
		
		Conversion.shortToBytes(pid, 0, portNumber);
		pid[0] |= priority;
		
		return pid;
	}
	
	public byte getPriority() {
    	return priority;
    }
	
	public void setPriority(byte priority) {
		if ((priority & 0x0f) != 0) throw new IllegalArgumentException("May not use lower 4 bits of priority");
		
		this.priority = priority;
    }
	
	public short getPortNumber() {
    	return portNumber;
    }
}

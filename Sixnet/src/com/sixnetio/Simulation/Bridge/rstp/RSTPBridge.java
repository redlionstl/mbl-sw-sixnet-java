/*
 * RSTPBridge.java
 *
 * Holds all of the variables for an RSTP bridge.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

import java.util.LinkedList;
import java.util.List;

import com.sixnetio.Simulation.Bridge.Bridge;
import com.sixnetio.util.Timer;


public class RSTPBridge implements Timer.TickListener {
	protected final RSTPPort[] ports;
	
	// Variables
	protected boolean begin = true; // Set to true when state machines should go to their BEGIN state
	protected boolean tick; // Each second this is set to true and the state machines are run until stable
	
	protected final BridgeID bridgeID;
	protected PortPriorityVector bridgePriorityVector;
	protected final Times bridgeTimes = new Times();
	protected PortPriorityVector rootPriorityVector;
	protected final Times rootTimes = new Times();
	
	// RSTP Performance parameters
	// Note: All times are in increments of 1/256th of a second
	private int ForceProtocolVersion; // 0 or 2
	private short ForwardDelay = (short)(15 * 256); // 4-30
	private short HelloTime = (short)(2 * 256); // 1-2
	private short IdentifierPriority = (short)(32768 & 0xffff); // 0-61,440 in steps of 4096
	private short MaxAge = (short)(20 * 256); // 6-40
	private final short MigrateTime = (short)(3 * 256);
	
	protected boolean stateChanged; // Used to detect when the state machines stabilize
	
	private Timer timer;
	
	// Read-only after the constructor
	private List<BridgeStateMachine> stateMachines = new LinkedList<BridgeStateMachine>();
	
	private Bridge bridge;
	
	public RSTPBridge(Bridge bridge) {
		this.bridge = bridge;
		
		bridgeID = new BridgeID(IdentifierPriority, bridge.getAddress().getBytes());
		ports = new RSTPPort[bridge.getPortCount()]; // As Bridge builds its port list it will fill this in
		
		bridgePriorityVector = new PortPriorityVector(bridgeID, 0, bridgeID, new PortID((byte)0, (short)0), new PortID((byte)0, (short)0));
		rootPriorityVector = bridgePriorityVector;
		
		stateMachines.add(new PortRoleSelectionSM(this));
	}
	
	// Bridge should call this once everything (ports included) are set up properly
	public void start() {
		timer = new Timer(1000);
		timer.addTickListener(this);
		timer.start();
	}
	
	public BridgeID getBridgeID() {
    	return bridgeID;
    }
	
	public void doTick(long elapsedMS) {
		tick = true;
		stateStep();
	}
	
	public void reset() {
		begin = true;
		
		stateStep();
	}
	
	public synchronized void stateStep() {
		// Step through all bridge state machines and port state machines until stable
		do {
			stateChanged = false;
			
    		for (StateMachine sm : stateMachines) {
    			sm.stateStep();
    		}
    		
    		for (int i = 0; i < ports.length; i++) {
    			ports[i].stateStep();
    		}
		} while (stateChanged);
	}
	
	protected void setRSTPPort(int index, RSTPPort port) {
		ports[index] = port;
	}
	
	protected boolean getAllSynced() {
		for (int i = 0; i < ports.length; i++) {
			if (ports[i].role == ports[i].selectedRole && (ports[i].synced || ports[i].role == Role.Root)) {
				// Good, keep going
			} else {
				return false;
			}
		}
		
		return true;
	}
	
	protected boolean getRSTPVersion() {
		return (getForceProtocolVersion() >= 2);
	}
	
	protected boolean getSTPVersion() {
		return (getForceProtocolVersion() < 2);
	}
	
	protected void clearReselectTree() {
	    for (int i = 0; i < ports.length; i++) {
	    	ports[i].reselect = false;
	    }
    }
	
	protected void setSyncTree() {
		for (int i = 0; i < ports.length; i++) {
			ports[i].sync = true;
		}
	}
	
	protected void setReRootTree() {
		for (int i = 0; i < ports.length; i++) {
			ports[i].reRoot = true;
		}
	}
	
	protected void setSelectedTree() {
		for (int i = 0; i < ports.length; i++) {
			if (ports[i].reselect) return;
		}
		
		for (int i = 0; i < ports.length; i++) {
			ports[i].selected = true;
		}
	}
	
	protected void updtRoleDisabledTree() {
		for (int i = 0; i < ports.length; i++) {
			ports[i].selectedRole = Role.Disabled;
		}
	}
	
	protected void updtRolesTree() {
		BridgeID oldRootBridgeID = rootPriorityVector.rootBridgeID;
		
		rootPriorityVector = bridgePriorityVector;
		RSTPPort chosenPort = null;
		
		for (int i = 0; i < ports.length; i++) {
			if (ports[i].messagePriorityVector != null && ports[i].infoIs == RSTPPort.InfoIs.Received) {
				PortPriorityVector rootPathPriorityVector;
				rootPathPriorityVector = new PortPriorityVector(ports[i].messagePriorityVector.rootBridgeID,
				                                                ports[i].messagePriorityVector.rootPathCost + ports[i].portPriorityVector.rootPathCost,
				                                                ports[i].messagePriorityVector.designatedBridgeID,
				                                                ports[i].messagePriorityVector.designatedPortID,
				                                                ports[i].messagePriorityVector.bridgePortID);
				
				if (!bridgePriorityVector.designatedBridgeID.equals(rootPathPriorityVector.designatedBridgeID) &&
					rootPathPriorityVector.compareTo(rootPriorityVector) < 0) {
					
					rootPriorityVector = rootPathPriorityVector;
					chosenPort = ports[i];
				}
			}
		}
		
		if (rootPriorityVector.equals(bridgePriorityVector)) {
			rootTimes.copy(bridgeTimes);
		} else {
			rootTimes.copy(chosenPort.portTimes);
			rootTimes.messageAge++;
		}
		
		for (int i = 0; i < ports.length; i++) {
			ports[i].designatedPriorityVector = new PortPriorityVector(rootPriorityVector.rootBridgeID,
			                                                           rootPriorityVector.rootPathCost,
			                                                           bridgeID,
			                                                           ports[i].portID,
			                                                           ports[i].portID);
			
			ports[i].designatedTimes.copy(rootTimes);
			ports[i].designatedTimes.helloTime = bridgeTimes.helloTime;
		}
		
		for (int i = 0; i < ports.length; i++) {
			if (ports[i].infoIs == RSTPPort.InfoIs.Disabled) { // f
				ports[i].selectedRole = Role.Disabled;
			} else if (ports[i].infoIs == RSTPPort.InfoIs.Aged){ // g
				ports[i].updtInfo = true;
				ports[i].selectedRole = Role.Designated;
			} else if (ports[i].infoIs == RSTPPort.InfoIs.Mine) { // h
				ports[i].selectedRole = Role.Designated;
				
				if (!ports[i].portPriorityVector.equals(ports[i].designatedPriorityVector) || !ports[i].portTimes.equals(rootTimes)) {
					ports[i].updtInfo = true;
				}
			} else if (ports[i].infoIs == RSTPPort.InfoIs.Received) { // i, j, k, l
				if (chosenPort == ports[i]) { // i
    				ports[i].selectedRole = Role.Root;
    				ports[i].updtInfo = false;
				} else { // j, k, l
					if (ports[i].designatedPriorityVector.compareTo(ports[i].portPriorityVector) <= 0) { // j, k
						if (!ports[i].portPriorityVector.designatedBridgeID.equals(bridgeID)) { // j
							ports[i].selectedRole = Role.Alternate;
							ports[i].updtInfo = false;
						} else { // k
							ports[i].selectedRole = Role.Backup;
							ports[i].updtInfo = false;
						}
					} else { // l
						ports[i].selectedRole = Role.Designated;
						ports[i].updtInfo = true;
					}
				}
			}
		}
		
		// If the root bridge changed
		if (!oldRootBridgeID.equals(rootPriorityVector.rootBridgeID)) {
			// Fire an event to say whether it became this bridge
		    if (rootPriorityVector.rootBridgeID.equals(bridgeID)) {
		    	bridge.fireRootChanged(true, rootPriorityVector.rootBridgeID);
    		} else {
    			bridge.fireRootChanged(false, rootPriorityVector.rootBridgeID);
    		}
		}
	}
	
	public int getForceProtocolVersion() {
    	return ForceProtocolVersion;
    }

	public void setForceProtocolVersion(int forceProtocolVersion) {
    	ForceProtocolVersion = forceProtocolVersion;
    }
	
	public short getForwardDelay() {
    	return ForwardDelay;
    }

	public void setForwardDelay(short forwardDelay) {
    	ForwardDelay = forwardDelay;
    }

	public short getHelloTime() {
    	return HelloTime;
    }

	public void setHelloTime(short helloTime) {
    	HelloTime = helloTime;
    }

	public short getIdentifierPriority() {
    	return IdentifierPriority;
    }

	public void setIdentifierPriority(short identifierPriority) {
    	IdentifierPriority = identifierPriority;
    }

	public short getMaxAge() {
    	return MaxAge;
    }

	public void setMaxAge(short maxAge) {
    	MaxAge = maxAge;
    }

	public short getMigrateTime() {
    	return MigrateTime;
    }
}

/*
 * BridgeID.java
 *
 * Represents a bridge identifier (priority and MAC)
 *
 * Jonathan Pearson
 * September 15, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

import com.sixnetio.util.Conversion;

public class BridgeID extends RSTPVector {
	protected short priority;
	protected final byte[] mac;
	
	public BridgeID(short priority, byte[] mac) {
		this.priority = priority;
		this.mac = new byte[6];
		
		System.arraycopy(mac, 0, this.mac, 0, 6);
	}
	
	public BridgeID(byte[] bid) {
		this(bid, 0);
	}
	
	public BridgeID(byte[] bid, int off) {
		this.priority = Conversion.bytesToShort(bid, off);
		this.mac = new byte[6];
		System.arraycopy(bid, off + 2, mac, 0, 6);
	}
	
	public byte[] getBytes() {
		byte[] bid = new byte[8];
		Conversion.shortToBytes(bid, 0, priority);
		System.arraycopy(mac, 0, bid, 2, 6);
		
		return bid;
	}
	
	public short getPriority() {
		return priority;
	}
	
	public void setPriority(short val) {
		priority = val;
	}
	
	public byte[] getMAC() {
		byte[] mac = new byte[6];
		System.arraycopy(this.mac, 0, mac, 0, 6);
		
		return mac;
	}
}

/*
 * RSTPPort.java
 *
 * Holds all of the variables for a single RSTP port.
 *
 * Jonathan Pearson
 * September 12, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

import java.util.LinkedList;
import java.util.List;

import com.sixnetio.Simulation.Bridge.Port;

public class RSTPPort {
	protected final PortID portID;
	protected final Port port;
	protected final RSTPBridge bridge;
	
	// Port data used by but not set by RSTP (may belong elsewhere)
	protected boolean operPointToPointMAC;
	
	// Timers, all in increments of 1/256th of a second
	protected short edgeDelayWhile; // Without receiving a BPDU, time before this port becomes an operEdgePort
	protected short fdWhile; // Delays port state transitions until other bridges receive spanning tree info
	protected short helloWhen; // Ensures at least one BPDU is sent by each Designated Port in each HelloTime period
	protected short mdelayWhile; // Allows time for another RSTP bridge to synchronize its migration state
	protected short rbWhile; // Maintained at 2 * HelloTime while the port is a Backup
	protected short rcvdInfoWhile; // Time before aging out ST info (portPriority and portTimes) if not refreshed
	protected short rrWhile; // Recent root timer
	protected short tcWhile; // TC timer; TCN messages are sent while this timer is running
	
	// Variables
	protected int ageingTime; // FDB entries for this port are aged out after this
	protected boolean agree;
	protected boolean agreed;
	
	protected PortPriorityVector designatedPriorityVector;
	
	protected Times designatedTimes;
	
	protected boolean disputed;
	protected boolean fdbFlush;
	protected boolean forward;
	protected boolean forwarding;
	
	public static enum InfoIs {
		Received,
		Mine,
		Aged,
		Disabled;
	}
	protected InfoIs infoIs;
	
	protected boolean learn;
	protected boolean learning;
	protected boolean mcheck;
	protected PortPriorityVector messagePriorityVector;
	
	protected Times msgTimes = new Times();
	
	protected boolean newInfo;
	protected boolean operEdge;
	protected boolean portEnabled;
	protected PortPriorityVector portPriorityVector;
	
	protected final Times portTimes = new Times();;
	
	protected boolean proposed;
	protected boolean proposing;
	protected boolean rcvdBPDU;
	
	public static enum Info {
		SuperiorDesignatedInfo,
		RepeatedDesignatedInfo,
		InferiorDesignatedInfo,
		InferiorRootAlternateInfo,
		OtherInfo;
	}
	protected Info rcvdInfo;
	
	protected boolean rcvdMsg;
	protected boolean rcvdRSTP;
	protected boolean rcvdSTP;
	protected boolean rcvdTc;
	protected boolean rcvdTcAck;
	protected boolean rcvdTcn;
	protected boolean reRoot;
	protected boolean reselect;
	
	protected Role role; // Section 17.7
	
	protected boolean selected;
	protected Role selectedRole;
	
	protected boolean sendRSTP;
	protected boolean sync;
	protected boolean synced;
	protected boolean tcAck;
	protected boolean tcProp;
	
	protected int txCount;
	protected boolean updtInfo;
	
	// State machine conditions and parameters
	private boolean AdminEdge; // Identified as an edge port
	private boolean AutoEdge; // Auto-detect edge port
	
	/* Default values for PortPathCost (recommend range); allowed range is 1-200,000,000
	 * <= 100 Kb/s: 200,000,000 (20,000,000 - 200,000,000)
	 *      1 Mb/s:  20,000,000 ( 2,000,000 - 200,000,000)
	 *     10 Mb/s:   2,000,000 (   200,000 -  20,000,000)
	 *    100 Mb/s:     200,000 (    20,000 -   2,000,000)
	 *      1 Gb/s:      20,000 (     2,000 -     200,000)
	 *     10 Gb/s:       2,000 (       200 -      20,000)
	 *    100 Gb/s:         200 (        20 -       2,000)
	 *      1 Tb/s:          20 (         2 -         200)
	 *     10 Tb/s:           2 (         1 -          20)
	 */
	protected int PortPathCost = 200000; // Default for 100 MB/s, which is the most common link in 2008
	private int TxHoldCount = 6; // 1-10
	
	// Read-only after the constructor
	private List<PortStateMachine> stateMachines = new LinkedList<PortStateMachine>();
	private BPDU lastReceivedBPDU;
	
	public RSTPPort(Port port) {
		this.port = port;
		this.bridge = port.getBridge().getRSTPBridge();
		this.portID = new PortID((byte)16, (short)port.getPortNumber());
		
		bridge.setRSTPPort(port.getPortNumber(), this);
		
		// Construct all of the state machines
		stateMachines.add(new BridgeDetectionSM(this));
		stateMachines.add(new PortInformationSM(this));
		stateMachines.add(new PortProtocolMigrationSM(this));
		stateMachines.add(new PortReceiveSM(this));
		stateMachines.add(new PortRoleTransitionSM(this));
		stateMachines.add(new PortStateTransitionSM(this));
		stateMachines.add(new PortTimersSM(this));
		stateMachines.add(new PortTransmitSM(this));
		stateMachines.add(new TopologyChangeSM(this));
	}
	
	public void stateStep() {
		for (StateMachine sm : stateMachines) {
			sm.stateStep();
		}
	}
	
	public PortID getPortID() {
    	return portID;
    }
	
	protected short getEdgeDelay() {
		if (operPointToPointMAC) {
			return bridge.getMigrateTime();
		} else {
			return getMaxAge();
		}
	}
	
	protected short getForwardDelay() {
		if (sendRSTP) {
			return getHelloTime();
		} else {
			return getFwdDelay();
		}
	}
	
	protected short getFwdDelay() {
		return designatedTimes.forwardDelay;
	}
	
	protected short getHelloTime() {
		return designatedTimes.helloTime;
	}
	
	protected short getMaxAge() {
		return designatedTimes.maxAge;
	}
	
	protected boolean getReRooted() {
		for (int i = 0; i < bridge.ports.length; i++) {
			if (i != portID.portNumber && bridge.ports[i].rrWhile != 0) return false;
		}
		
		return true;
	}
	
	protected boolean betterOrSameInfo(InfoIs newInfoIs) {
		if (newInfoIs == InfoIs.Received && infoIs == InfoIs.Received && messagePriorityVector.compareTo(portPriorityVector) <= 0) {
			return true;
		} else if (newInfoIs == InfoIs.Mine && infoIs == InfoIs.Mine && designatedPriorityVector.compareTo(portPriorityVector) <= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	protected void disableForwarding() {
		port.setForwarding(false);
	}
	
	protected void disableLearning() {
		port.setLearning(false);
	}
	
	protected void enableForwarding() {
		port.setForwarding(true);
	}
	
	protected void enableLearning() {
		port.setLearning(true);
	}
	
	protected void newTcWhile() {
		if (tcWhile == 0 && sendRSTP) {
			tcWhile = (short)(getHelloTime() + 256);
			newInfo = true;
		} else if (tcWhile == 0 && !sendRSTP) {
			tcWhile = (short)(bridge.rootTimes.maxAge + bridge.rootTimes.forwardDelay);
		}
	}
	
	protected Info rcvInfo() {
		messagePriorityVector = new PortPriorityVector(lastReceivedBPDU.getRootID(), lastReceivedBPDU.getRootPathCost(), lastReceivedBPDU.getBridgeID(), lastReceivedBPDU.getPortID(), portID);
		msgTimes = new Times(lastReceivedBPDU.getMessageAge(), lastReceivedBPDU.getMaxAge(), lastReceivedBPDU.getForwardDelay(), lastReceivedBPDU.getHelloTime());
		
		if (lastReceivedBPDU.getPortRole() == Role.Designated &&
			(messagePriorityVector.compareTo(portPriorityVector) < 0 ||
			 (messagePriorityVector.equals(portPriorityVector) &&
			  !msgTimes.equals(portTimes)))) {
			return Info.SuperiorDesignatedInfo;
		} else if (lastReceivedBPDU.getPortRole() == Role.Designated &&
				   messagePriorityVector.equals(portPriorityVector) &&
				   msgTimes.equals(portTimes)) {
			return Info.RepeatedDesignatedInfo;
		} else if (lastReceivedBPDU.getPortRole() == Role.Designated &&
				   messagePriorityVector.compareTo(portPriorityVector) > 0) {
			return Info.InferiorDesignatedInfo;
		} else {
			return Info.OtherInfo;
		}
	}
	
	protected void recordAgreement() {
		if (bridge.getRSTPVersion() &&
			operPointToPointMAC &&
			lastReceivedBPDU.getAgreementFlag()) {

			agreed = true;
			proposing = false;
		} else {
			agreed = false;
		}
	}
	
	protected void recordDispute() {
		if (lastReceivedBPDU.getLearningFlag()) {
    		agreed = true;
    		proposing = false;
		}
	}
	
	protected void recordProposal() {
		if (lastReceivedBPDU.getPortRole() == Role.Designated &&
			lastReceivedBPDU.getProposalFlag()) {
			
			proposed = true;
		}
	}
	
	protected void recordPriority() {
		portPriorityVector.rootBridgeID = messagePriorityVector.rootBridgeID;
		portPriorityVector.rootPathCost = messagePriorityVector.rootPathCost;
		portPriorityVector.designatedBridgeID = messagePriorityVector.designatedBridgeID;
		portPriorityVector.designatedPortID = messagePriorityVector.designatedPortID;
	}
	
	protected void recordTimes() {
		portTimes.copy(msgTimes);
		
		// This is the minimum from the Compatibility Range column of table 17-1 (PDF page 153 of the 802.1D-2004 spec)
		if (portTimes.helloTime < 1) portTimes.helloTime = 1;
	}
	
	protected void setTcFlags() {
		if (lastReceivedBPDU.getTCFlag()) {
			rcvdTc = true;
		}
		
		if (lastReceivedBPDU.getTCAckFlag()) {
			rcvdTcAck = true;
		}
		
		if (lastReceivedBPDU.getBpduType() == BPDU.T_TOP_CHANGE) {
			rcvdTcn = true;
		}
	}
	
	protected void setTcPropTree() {
		for (int i = 0; i < bridge.ports.length; i++) {
			if (i != portID.portNumber) bridge.ports[i].tcProp = true;
		}
		
		// This happens when a topology change is detected, so let everybody know
		port.getBridge().fireTopologyChange();
	}
	
	protected void txConfig() {
		BPDU bpdu = new BPDU(BPDU.PROTOCOL_ID, BPDU.PV_STP, BPDU.T_CONFIG);
		
		bpdu.setRootID(designatedPriorityVector.rootBridgeID);
		bpdu.setRootPathCost(designatedPriorityVector.rootPathCost);
		bpdu.setBridgeID(designatedPriorityVector.designatedBridgeID);
		bpdu.setPortID(designatedPriorityVector.designatedPortID);
		
		bpdu.setTCFlag(tcWhile != 0);
		bpdu.setTCAckFlag(tcAck);
		
		bpdu.setMessageAge(designatedTimes.messageAge);
		bpdu.setMaxAge(designatedTimes.maxAge);
		bpdu.setHelloTime(designatedTimes.helloTime);
		bpdu.setForwardDelay(designatedTimes.forwardDelay);
		
		port.sendBPDU(bpdu);
	}
	
	protected void txRstp() {
		BPDU bpdu = new BPDU(BPDU.PROTOCOL_ID, BPDU.PV_RSTP, BPDU.T_RST);
		
		bpdu.setRootID(designatedPriorityVector.rootBridgeID);
		bpdu.setRootPathCost(designatedPriorityVector.rootPathCost);
		bpdu.setBridgeID(designatedPriorityVector.designatedBridgeID);
		bpdu.setPortID(designatedPriorityVector.designatedPortID);
		
		bpdu.setPortRole(role);
		
		bpdu.setAgreementFlag(agree);
		bpdu.setProposalFlag(proposing);
		
		bpdu.setTCFlag(tcWhile != 0);
		bpdu.setTCAckFlag(false);
		
		bpdu.setLearningFlag(learning);
		bpdu.setForwardingFlag(forwarding);
		
		bpdu.setMessageAge(designatedTimes.messageAge);
		bpdu.setMaxAge(designatedTimes.maxAge);
		bpdu.setHelloTime(designatedTimes.helloTime);
		bpdu.setForwardDelay(designatedTimes.forwardDelay);
		
		port.sendBPDU(bpdu);
	}
	
	protected void txTcn() {
		BPDU bpdu = new BPDU(BPDU.PROTOCOL_ID, BPDU.PV_STP, BPDU.T_TOP_CHANGE);
		
		port.sendBPDU(bpdu);
	}
	
	protected void updtBPDUVersion() {
		if (lastReceivedBPDU.getProtocolVersion() < BPDU.PV_RSTP) {
			rcvdSTP = true;
		} else {
			rcvdRSTP = true;
		}
	}
	
	protected void updtRcvdInfoWhile() {
		if (portTimes.messageAge + 1 <= portTimes.maxAge) {
			rcvdInfoWhile = (short)(3 * portTimes.helloTime);
		} else {
			rcvdInfoWhile = 0;
		}
	}

	public boolean getAdminEdge() {
    	return AdminEdge;
    }

	public void setAdminEdge(boolean adminEdge) {
    	AdminEdge = adminEdge;
    }

	public boolean getAutoEdge() {
    	return AutoEdge;
    }

	public void setAutoEdge(boolean autoEdge) {
    	AutoEdge = autoEdge;
    }

	public int getTxHoldCount() {
    	return TxHoldCount;
    }

	public void setTxHoldCount(int txHoldCount) {
    	TxHoldCount = txHoldCount;
    }

	public void handleBPDU(BPDU bpdu) {
	    lastReceivedBPDU = bpdu;
		rcvdBPDU = true;
		
		bridge.stateStep();
    }
}

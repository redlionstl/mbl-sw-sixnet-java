/*
 * Role.java
 *
 * Port roles.
 *
 * Jonathan Pearson
 * September 12, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

enum Role {
	Unknown, // Not legal for sending, but may be received
	Root,
	Designated,
	Alternate,
	Backup,
	Disabled;
}

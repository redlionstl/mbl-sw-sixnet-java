/*
 * BPDU.java
 *
 * Represents a BPDU (Bridge Protocol Data Unit).
 *
 * Jonathan Pearson
 * September 18, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

import com.sixnetio.util.*;

public class BPDU {
	// Note: Timer values (ages) are represented in 1/256ths of a second
	//   This class does not translate these, it is up to the caller to do that
	
	// Protocol IDs
	public static final short PROTOCOL_ID = (short)0x0000;
	
	// Protocol Version Identifiers
	public static final byte PV_STP = (byte)0x00,
	                         PV_RSTP = (byte)0x02;
	
	// BPDU Types
	public static final byte T_CONFIG = (byte)0x00,
	                         T_TOP_CHANGE = (byte)0x80,
	                         T_RST = (byte)0x02;
	
	// Values for the 'flags' field
	public static final byte FLAG_TC = (byte)0x01, // Topology Change Flag
	                         FLAG_PROPOSAL = (byte)0x02, // Proposal flag
	                         ROLE_MASK = (byte)0x0c, // Mask before testing roles
	                         ROLE_UNKNOWN = (byte)0x00, // Unknown role
	                         ROLE_ALTERNATE_BACKUP = (byte)0x04, // Alternate or Backup
	                         ROLE_ROOT = (byte)0x08, // Root
	                         ROLE_DESIGNATED = (byte)0x0c, // Designated
	                         FLAG_LEARNING = (byte)0x10, // Learning flag
	                         FLAG_FORWARDING = (byte)0x20, // Forwarding flag
	                         FLAG_AGREEMENT = (byte)0x40, // Agreement flag
	                         FLAG_TC_ACK = (byte)0x80; // Topology Change Acknowledgment (as 0)
	
	// Present in all BPDUs
	protected short protocolID;
	protected byte protocolVersion;
	protected byte bpduType;
	
	// Present in Configuration and RST BPDUs
	protected byte flags;
	protected BridgeID rootID;
	protected int rootPathCost;
	protected BridgeID bridgeID;
	protected PortID portID;
	protected short messageAge;
	protected short maxAge;
	protected short helloTime;
	protected short forwardDelay;
	
	// Present in all RST BPDUs
	protected byte v1length;
	
	protected BPDU(short protocolID, byte protocolVersion, byte bpduType) {
		this.protocolID = protocolID;
		this.protocolVersion = protocolVersion;
		this.bpduType = bpduType;
	}
	
	// NOTE: It is the caller's responsibility to make sure the BPDU could not have come from this device
	public static BPDU parseBPDU(byte[] data) {
		try {
    		short protocolID = Conversion.bytesToShort(data, 0);
    		if (protocolID != PROTOCOL_ID) return null;
    		
    		byte protocolVersion = data[2];
    		
    		byte bpduType = data[3];
    		
    		BPDU bpdu = new BPDU(protocolID, protocolVersion, bpduType);
    		
    		if ((bpduType == T_CONFIG && protocolVersion >= PV_STP) ||
    			(bpduType == T_RST && protocolVersion >= PV_RSTP)) {
    			
    			bpdu.setFlags(data[4]);
    			bpdu.setRootID(new BridgeID(data, 5));
    			bpdu.setRootPathCost(Conversion.bytesToInt(data, 13));
    			bpdu.setBridgeID(new BridgeID(data, 17));
    			bpdu.setPortID(new PortID(data, 25));
    			bpdu.setMessageAge(Conversion.bytesToShort(data, 27));
    			bpdu.setMaxAge(Conversion.bytesToShort(data, 29));
    			bpdu.setHelloTime(Conversion.bytesToShort(data, 31));
    			bpdu.setForwardDelay(Conversion.bytesToShort(data, 33));
    			
    			if (bpduType == T_RST) {
    				bpdu.setV1length(data[35]);
    			}
    		} else if (bpduType == T_TOP_CHANGE && protocolVersion >= PV_STP) {
    			// No extra data to parse
    		} else {
    			// Unknown type or bad version
    			return null;
    		}
    		
    		return bpdu;
		} catch (Exception e) {
			// When there is a parse error, return null (ignore the BPDU)
			Utils.debug(e);
			return null;
		}
	}
	
	public byte[] getBytes() {
		byte[] data;
		
		if (getBpduType() == T_CONFIG) {
			data = new byte[35];
		} else if (getBpduType() == T_RST) {
			data = new byte[36];
		} else if (getBpduType() == T_TOP_CHANGE) {
			data = new byte[4];
		} else {
			throw new IllegalStateException("Unrecognized BPDU type: " + getBpduType());
		}
		
		Conversion.shortToBytes(data, 0, getProtocolID());
		data[2] = getProtocolVersion();
		data[3] = getBpduType();
		
		if (getBpduType() == T_CONFIG || getBpduType() == T_RST) {
			data[4] = getFlags();
			System.arraycopy(getRootID().getBytes(), 0, data, 5, 8);
			Conversion.intToBytes(data, 13, getRootPathCost());
			System.arraycopy(getBridgeID().getBytes(), 0, data, 17, 8);
			System.arraycopy(getPortID().getBytes(), 0, data, 25, 2);
			Conversion.shortToBytes(data, 27, getMessageAge());
			Conversion.shortToBytes(data, 29, getMaxAge());
			Conversion.shortToBytes(data, 31, getHelloTime());
			Conversion.shortToBytes(data, 33, getForwardDelay());
			
			if (getBpduType() == T_RST) {
				data[35] = getV1length();
			}
		}
		
		return data;
	}
	
	public short getProtocolID() {
    	return protocolID;
    }
	
	public byte getProtocolVersion() {
    	return protocolVersion;
    }
	
	public byte getBpduType() {
    	return bpduType;
    }
	
	public boolean getTCFlag() {
		return ((flags & FLAG_TC) == FLAG_TC);
	}
	
	public void setTCFlag(boolean set) {
		if (getBpduType() != T_CONFIG && getBpduType() != T_RST) throw new IllegalStateException("No TC flag for this type of BPDU");
		
		if (set) {
			flags |= FLAG_TC;
		} else {
			flags &= ~FLAG_TC;
		}
	}
	
	public boolean getProposalFlag() {
		return ((flags & FLAG_PROPOSAL) == FLAG_PROPOSAL);
	}
	
	public void setProposalFlag(boolean set) {
		if (getBpduType() != T_RST) throw new IllegalStateException("No Proposal flag for this type of BPDU");
		
		if (set) {
			flags |= FLAG_PROPOSAL;
		} else {
			flags &= ~FLAG_PROPOSAL;
		}
	}
	
	public boolean getLearningFlag() {
		return ((flags & FLAG_LEARNING) == FLAG_LEARNING);
	}
	
	public void setLearningFlag(boolean set) {
		if (getBpduType() != T_RST) throw new IllegalStateException("No Learning flag for this type of BPDU");
		
		if (set) {
			flags |= FLAG_LEARNING;
		} else {
			flags &= ~FLAG_LEARNING;
		}
	}
	
	public boolean getForwardingFlag() {
		return ((flags & FLAG_FORWARDING) == FLAG_FORWARDING);
	}
	
	public void setForwardingFlag(boolean set) {
		if (getBpduType() != T_RST) throw new IllegalStateException("No Forwarding flag for this type of BPDU");
		
		if (set) {
			flags |= FLAG_FORWARDING;
		} else {
			flags &= ~FLAG_FORWARDING;
		}
	}
	
	public boolean getAgreementFlag() {
		return ((flags & FLAG_AGREEMENT) == FLAG_AGREEMENT);
	}
	
	public void setAgreementFlag(boolean set) {
		if (getBpduType() != T_RST) throw new IllegalStateException("No TC flag for this type of BPDU");
		
		if (set) {
			flags |= FLAG_AGREEMENT;
		} else {
			flags &= ~FLAG_AGREEMENT;
		}
	}
	
	public boolean getTCAckFlag() {
		return ((flags & FLAG_TC_ACK) == FLAG_TC_ACK);
	}
	
	public void setTCAckFlag(boolean set) {
		if (getBpduType() != T_CONFIG && getBpduType() != T_RST) throw new IllegalStateException("No TC flag for this type of BPDU");
		
		if (set) {
			flags |= FLAG_TC_ACK;
		} else {
			flags &= ~FLAG_TC_ACK;
		}
	}
	
	public Role getPortRole() {
		switch (flags & ROLE_MASK) {
			case ROLE_UNKNOWN:
				return Role.Unknown;
			case ROLE_ALTERNATE_BACKUP:
				return Role.Alternate; // Had to choose one
			case ROLE_ROOT:
				return Role.Root;
			case ROLE_DESIGNATED:
				return Role.Designated;
			default:
				return Role.Unknown;
		}
	}
	
	public void setPortRole(Role role) {
		flags &= ~ROLE_MASK;
		
		switch (role) {
			case Alternate:
			case Backup:
				flags |= ROLE_ALTERNATE_BACKUP;
				break;
			case Designated:
				flags |= ROLE_DESIGNATED;
				break;
			case Disabled:
				flags |= ROLE_UNKNOWN;
				break;
			case Root:
				flags |= ROLE_ROOT;
				break;
			case Unknown:
				flags |= ROLE_UNKNOWN;
				break;
		}
	}
	
	protected byte getFlags() {
    	return flags;
    }
	
	protected void setFlags(byte flags) {
    	this.flags = flags;
    }
	
	public BridgeID getRootID() {
    	return rootID;
    }
	
	public void setRootID(BridgeID rootID) {
    	this.rootID = rootID;
    }
	
	public int getRootPathCost() {
    	return rootPathCost;
    }
	
	public void setRootPathCost(int rootPathCost) {
    	this.rootPathCost = rootPathCost;
    }
	
	public BridgeID getBridgeID() {
    	return bridgeID;
    }
	
	public void setBridgeID(BridgeID bridgeID) {
    	this.bridgeID = bridgeID;
    }
	
	public PortID getPortID() {
    	return portID;
    }
	
	public void setPortID(PortID portID) {
    	this.portID = portID;
    }
	
	public short getMessageAge() {
    	return messageAge;
    }
	
	public void setMessageAge(short messageAge) {
    	this.messageAge = messageAge;
    }
	
	public short getMaxAge() {
    	return maxAge;
    }
	
	public void setMaxAge(short maxAge) {
    	this.maxAge = maxAge;
    }
	
	public short getHelloTime() {
    	return helloTime;
    }
	
	public void setHelloTime(short helloTime) {
    	this.helloTime = helloTime;
    }
	
	public short getForwardDelay() {
    	return forwardDelay;
    }
	
	public void setForwardDelay(short forwardDelay) {
    	this.forwardDelay = forwardDelay;
    }
	
	public byte getV1length() {
    	return v1length;
    }
	
	protected void setV1length(byte v1length) {
		this.v1length = v1length;
	}
}

/*
 * TopologyChange.java
 *
 * The Topology Change state machine.
 *
 * Jonathan Pearson
 * September 12, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class TopologyChangeSM extends PortStateMachine {
	public enum State {
		None,
		Inactive_Enter,
		Inactive_Steady,
		Learning_Enter,
		Learning_Steady,
		Detected_Enter,
		Detected_Steady,
		NotifiedTCN_Enter,
		NotifiedTCN_Steady,
		NotifiedTC_Enter,
		NotifiedTC_Steady,
		Propagating_Enter,
		Propagating_Steady,
		Acknowledged_Enter,
		Acknowledged_Steady,
		Active_Enter,
		Active_Steady;
	}
	
	private State state = State.None;
	
	public TopologyChangeSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case Inactive_Enter:
				port.fdbFlush = true;
				port.tcWhile = 0;
				port.tcAck = false;
				state = State.Inactive_Steady;
			case Inactive_Steady:
				if (port.learn && !port.fdbFlush) state = State.Learning_Enter;
				break;
				
			case Learning_Enter:
				port.rcvdTc = port.rcvdTcn = port.rcvdTcAck = port.tcProp = false;
				state = State.Learning_Steady;
			case Learning_Steady:
				if (((port.role == Role.Root) || (port.role == Role.Designated)) && port.forward && !port.operEdge) state = State.Detected_Enter;
				if ((port.role != Role.Root) && (port.role != Role.Designated) && !(port.learn || port.learning) && !(port.rcvdTc || port.rcvdTcn || port.rcvdTcAck || port.tcProp)) state = State.Inactive_Enter;
				if (port.rcvdTc || port.rcvdTcn || port.rcvdTcAck || port.tcProp) state = State.Learning_Enter;
				break;
				
			case Detected_Enter:
				port.newTcWhile();
				port.setTcPropTree();
				port.newInfo = true;
				state = State.Detected_Steady;
			case Detected_Steady:
				if (true) state = State.Active_Enter;
				break;
				
			case NotifiedTCN_Enter:
				port.newTcWhile();
				state = State.NotifiedTCN_Steady;
			case NotifiedTCN_Steady:
				if (true) state = State.NotifiedTC_Enter;
				break;
				
			case NotifiedTC_Enter:
				port.rcvdTcn = port.rcvdTc = false;
				if (port.role == Role.Designated) port.tcAck = true;
				port.setTcPropTree(); // Spec says "setTcPropBridge()", but that function is never defined
				state = State.NotifiedTC_Steady;
			case NotifiedTC_Steady:
				if (true) state = State.Active_Enter;
				break;
				
			case Propagating_Enter:
				port.newTcWhile();
				port.fdbFlush = true;
				port.tcProp = false;
				state = State.Propagating_Steady;
			case Propagating_Steady:
				if (true) state = State.Active_Enter;
				break;
				
			case Acknowledged_Enter:
				port.tcWhile = 0;
				port.rcvdTcAck = false;
				state = State.Acknowledged_Steady;
			case Acknowledged_Steady:
				if (true) state = State.Active_Enter;
				break;
				
			case Active_Enter:
				state = State.Active_Steady;
			case Active_Steady:
				if (((port.role != Role.Root) && (port.role != Role.Designated)) || port.operEdge) state = State.Learning_Enter;
				if (port.rcvdTcn) state = State.NotifiedTCN_Enter;
				if (port.rcvdTc) state = State.NotifiedTC_Enter;
				if (port.tcProp && !port.operEdge) state = State.Propagating_Enter;
				if (port.rcvdTcAck) state = State.Acknowledged_Enter;
				break;
		}
		
		if (port.bridge.begin) state = State.Inactive_Enter;
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

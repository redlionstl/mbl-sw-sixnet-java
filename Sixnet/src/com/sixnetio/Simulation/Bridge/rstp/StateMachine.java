/*
 * StateMachine.java
 *
 * All of the RSTP state machines extend this.
 *
 * Jonathan Pearson
 * September 18, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

abstract class StateMachine {
	public abstract void stateStep();
}

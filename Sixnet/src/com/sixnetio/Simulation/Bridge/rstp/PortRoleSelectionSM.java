/*
 * PortRoleSelection.java
 *
 * The Port Role Selection state machine.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class PortRoleSelectionSM extends BridgeStateMachine {
	public enum State {
		None,
		InitBridge_Enter,
		InitBridge_Steady,
		RoleSelection_Enter,
		RoleSelection_Steady;
	}
	
	private State state = State.None;
	
	public PortRoleSelectionSM(RSTPBridge bridge) {
		super(bridge);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case InitBridge_Enter:
				bridge.updtRoleDisabledTree();
				state = State.InitBridge_Steady;
			case InitBridge_Steady:
				if (true) state = State.RoleSelection_Enter;
				break;
				
			case RoleSelection_Enter:
				bridge.clearReselectTree();
				bridge.updtRolesTree();
				bridge.setSelectedTree();
				state = State.RoleSelection_Steady;
			case RoleSelection_Steady:
				for (int i = 0; i < bridge.ports.length; i++) {
					if (bridge.ports[i].reselect) {
						state = State.RoleSelection_Enter;
						break;
					}
				}
				break;
		}
		
		if (bridge.begin) state = State.InitBridge_Enter;
		
		if (oldState != state) bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

/*
 * DesignatedPort.java
 *
 * The Designated Port state machine. This is a mini state machine,
 * part of the PortRoleTransitions state machine.
 *
 * Jonathan Pearson
 * September 12, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class DesignatedPortStatesSubSM extends PortStateMachine {
	public enum State {
		None,
		DesignatedPropose_Enter,
		DesignatedPropose_Steady,
		DesignatedSynced_Enter,
		DesignatedSynced_Steady,
		DesignatedRetired_Enter,
		DesignatedRetired_Steady,
		DesignatedForward_Enter,
		DesignatedForward_Steady,
		DesignatedLearn_Enter,
		DesignatedLearn_Steady,
		DesignatedDiscard_Enter,
		DesignatedDiscard_Steady,
		DesignatedPort_Enter,
		DesignatedPort_Steady;
	}
	
	// This can be controlled by PortRoleTransitions, the super-state machine of this one
	protected State state = State.None;
	
	public DesignatedPortStatesSubSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case DesignatedPropose_Enter:
				port.proposing = true;
				port.edgeDelayWhile = port.getEdgeDelay();
				port.newInfo = true;
				state = State.DesignatedPropose_Steady;
			case DesignatedPropose_Steady:
				if (true) state = State.DesignatedPort_Enter;
				break;
				
			case DesignatedSynced_Enter:
				port.rrWhile = 0;
				port.synced = true;
				port.sync = false;
				state = State.DesignatedSynced_Steady;
			case DesignatedSynced_Steady:
				if (true) state = State.DesignatedPort_Enter;
				break;
				
			case DesignatedRetired_Enter:
				port.reRoot = false;
				state = State.DesignatedRetired_Steady;
			case DesignatedRetired_Steady:
				if (true) state = State.DesignatedPort_Enter;
				break;
				
			case DesignatedForward_Enter:
				port.forward = true;
				port.fdWhile = 0;
				port.agreed = port.sendRSTP;
				state = State.DesignatedForward_Steady;
			case DesignatedForward_Steady:
				if (true) state = State.DesignatedPort_Enter;
				break;
				
			case DesignatedLearn_Enter:
				port.learn = true;
				port.fdWhile = port.getForwardDelay();
				state = State.DesignatedLearn_Steady;
			case DesignatedLearn_Steady:
				if (true) state = State.DesignatedPort_Enter;
				break;
				
			case DesignatedDiscard_Enter:
				port.learn = port.forward = port.disputed = false;
				port.fdWhile = port.getForwardDelay();
				state = State.DesignatedDiscard_Steady;
			case DesignatedDiscard_Steady:
				if (true) state = State.DesignatedPort_Enter;
				break;
				
			case DesignatedPort_Enter:
				port.role = Role.Designated;
				state = State.DesignatedPort_Steady;
			case DesignatedPort_Steady:
				if (port.selected && !port.updtInfo) {
					if (((port.sync && !port.synced) || (port.reRoot && (port.rrWhile != 0)) || port.disputed) && !port.operEdge && (port.learn || port.forward)) state = State.DesignatedDiscard_Enter;
					if (((port.fdWhile == 0) || port.agreed || port.operEdge) && ((port.rrWhile == 0) || !port.reRoot) && !port.sync && !port.learn) state = State.DesignatedLearn_Enter;
					if (((port.fdWhile == 0) || port.agreed || port.operEdge) && ((port.rrWhile == 0) || !port.reRoot) && !port.sync && (port.learn && !port.forward)) state = State.DesignatedForward_Enter;
					if ((port.rrWhile == 0) && port.reRoot) state = State.DesignatedRetired_Enter;
					if ((!port.learning && !port.forwarding && !port.synced) || (port.agreed && !port.synced) || (port.operEdge && !port.synced) || (port.sync && port.synced)) state = State.DesignatedSynced_Enter;
					if (!port.forward && !port.agreed && !port.proposing && !port.operEdge) state = State.DesignatedPropose_Enter;
				}
		}
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

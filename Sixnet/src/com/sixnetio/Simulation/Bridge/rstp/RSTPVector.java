/*
 * RSTPVector.java
 *
 * A generic RSTP byte vector.
 *
 * Jonathan Pearson
 * September 15, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

abstract class RSTPVector implements Comparable<RSTPVector> {
	public abstract byte[] getBytes();
	
	@Override
    public boolean equals(Object obj) {
		if (!(obj instanceof RSTPVector)) return false;
		
		RSTPVector rhs = (RSTPVector)obj;
		return (compareTo(rhs) == 0);
	}
	
	@Override
	public int compareTo(RSTPVector rhs) {
		byte[] myBytes = getBytes();
		byte[] hisBytes = rhs.getBytes();
		
		for (int i = 0; i < myBytes.length && i < hisBytes.length; i++) {
			if (myBytes[i] != hisBytes[i]) {
				return ((myBytes[i] & 0xff) - (hisBytes[i] & 0xff));
			}
		}
		
		if (myBytes.length > hisBytes.length) {
			return 1;
		} else if (hisBytes.length > myBytes.length) {
			return -1;
		} else {
			return 0;
		}
	}
	
	@Override
    public int hashCode() {
		byte[] bytes = getBytes();
		int hc = 0;
		for (byte b : bytes) {
			hc += b;
		}
		
		return hc;
	}
}

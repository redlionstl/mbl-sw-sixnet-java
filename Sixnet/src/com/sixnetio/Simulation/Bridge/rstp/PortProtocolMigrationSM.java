/*
 * PortProtocolMigration.java
 *
 * The Port Protocol Migration state machine.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class PortProtocolMigrationSM extends PortStateMachine {
	public enum State {
		None,
		CheckingRstp_Enter,
		CheckingRstp_Steady,
		SelectingStp_Enter,
		SelectingStp_Steady,
		Sensing_Enter,
		Sensing_Steady;
	}
	
	private State state = State.None;
	
	public PortProtocolMigrationSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case CheckingRstp_Enter:
				port.mcheck = false;
				port.sendRSTP = port.bridge.getRSTPVersion();
				port.mdelayWhile = port.bridge.getMigrateTime();
				state = State.CheckingRstp_Steady;
			case CheckingRstp_Steady:
				if (port.mdelayWhile != port.bridge.getMigrateTime() && !port.portEnabled) state = State.Sensing_Enter;
				if (port.mdelayWhile == 0) state = State.Sensing_Enter;
				break;
				
			case SelectingStp_Enter:
				port.sendRSTP = false;
				port.mdelayWhile = port.bridge.getMigrateTime();
				state = State.SelectingStp_Steady;
			case SelectingStp_Steady:
				if (port.mdelayWhile == 0 || !port.portEnabled || port.mcheck) state = State.Sensing_Enter;
				break;
				
			case Sensing_Enter:
				port.rcvdRSTP = port.rcvdSTP = false;
				state = State.Sensing_Steady;
			case Sensing_Steady:
				if (port.sendRSTP && port.rcvdSTP) state = State.SelectingStp_Enter;
				if (!port.portEnabled || port.mcheck || (port.bridge.getRSTPVersion() && !port.sendRSTP && port.rcvdRSTP)) state = State.CheckingRstp_Enter;
				break;
		}
		
		if (port.bridge.begin) state = State.CheckingRstp_Enter;
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

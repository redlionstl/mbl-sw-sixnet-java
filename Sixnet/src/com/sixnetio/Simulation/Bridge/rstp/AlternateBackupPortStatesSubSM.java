/*
 * DesignatedPort.java
 *
 * The Alternate and Backup Port state machine. This is a mini state machine,
 * part of the PortRoleTransitions state machine.
 *
 * Jonathan Pearson
 * September 12, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class AlternateBackupPortStatesSubSM extends PortStateMachine {
	public enum State {
		None,
		AlternateProposed_Enter,
		AlternateProposed_Steady,
		AlternateAgreed_Enter,
		AlternateAgreed_Steady,
		BlockPort_Enter,
		BlockPort_Steady,
		BackupPort_Enter,
		BackupPort_Steady,
		AlternatePort_Enter,
		AlternatePort_Steady;
	}
	
	// This can be controlled by PortRoleTransitions, the super-state machine of this one
	protected State state = State.None;
	
	public AlternateBackupPortStatesSubSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case AlternateProposed_Enter:
				port.bridge.setSyncTree();
				port.proposed = false;
				state = State.AlternateProposed_Steady;
			case AlternateProposed_Steady:
				if (true) state = State.AlternatePort_Enter;
				break;
				
			case AlternateAgreed_Enter:
				port.proposed = false;
				port.agree = port.newInfo = true;
				state = State.AlternatePort_Enter;
			case AlternateAgreed_Steady:
				if (true) state = State.AlternatePort_Enter;
				break;
				
			case BlockPort_Enter:
				port.role = port.selectedRole;
				port.learn = port.forward = false;
				state = State.BlockPort_Steady;
			case BlockPort_Steady:
				if (port.selected && !port.updtInfo) {
					if (!port.learning && !port.forwarding) state = State.AlternatePort_Enter;
				}
				break;
				
			case BackupPort_Enter:
				port.rbWhile = (short)(2 * port.getHelloTime());
				state = State.BackupPort_Steady;
			case BackupPort_Steady:
				if (true) state = State.AlternatePort_Enter;
				break;
				
			case AlternatePort_Enter:
				port.fdWhile = port.getFwdDelay();
				port.synced = true;
				port.rrWhile = 0;
				port.sync = port.reRoot = false;
				state = State.AlternatePort_Steady;
			case AlternatePort_Steady:
				if (port.selected && !port.updtInfo) {
					if ((port.fdWhile != port.getForwardDelay()) || port.sync || port.reRoot || !port.synced) state = State.AlternatePort_Enter;
					if ((port.rbWhile != 2 * port.getHelloTime()) && (port.role == Role.Backup)) state = State.BackupPort_Enter;
					if ((port.bridge.getAllSynced() && !port.agree) || (port.proposed && port.agree)) state = State.AlternateAgreed_Enter;
					if (port.proposed && !port.agree) state = State.AlternateProposed_Enter;
				}
				break;
		}
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

/*
 * PortInformation.java
 *
 * The Port Information state machine.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class PortInformationSM extends PortStateMachine {
	public enum State {
		None,
		Disabled_Enter,
		Disabled_Steady,
		Aged_Enter,
		Aged_Steady,
		Update_Enter,
		Update_Steady,
		Current_Enter,
		Current_Steady,
		Receive_Enter,
		Receive_Steady,
		SuperiorDesignated_Enter,
		SuperiorDesignated_Steady,
		RepeatedDesignated_Enter,
		RepeatedDesignated_Steady,
		InferiorDesignated_Enter,
		InferiorDesignated_Steady,
		NotDesignated_Enter,
		NotDesignated_Steady,
		Other_Enter,
		Other_Steady;
	}
	
	private State state = State.None;
	
	public PortInformationSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case Disabled_Enter:
				port.rcvdMsg = port.proposing = port.proposed = port.agree = port.agreed = false;
				port.rcvdInfoWhile = 0;
				port.infoIs = RSTPPort.InfoIs.Disabled;
				port.reselect = true;
				port.selected = false;
				state = State.Disabled_Steady;
			case Disabled_Steady:
				if (port.rcvdMsg) state = State.Disabled_Enter;
				if (port.portEnabled) state = State.Aged_Enter;
				break;
				
			case Aged_Enter:
				port.infoIs = RSTPPort.InfoIs.Aged;
				port.reselect = true;
				port.selected = false;
				state = State.Update_Enter;
			case Aged_Steady:
				if (port.selected && port.updtInfo) state = State.Update_Enter;
				break;
				
			case Update_Enter:
				port.proposing = port.proposed = false;
				port.agreed = port.agreed && port.betterOrSameInfo(RSTPPort.InfoIs.Mine);
				port.synced = port.synced && port.agreed;
				
				// portPriority = designatedPriority -- these are the first four components of the
				//   priority vectors
				port.portPriorityVector.rootBridgeID = port.designatedPriorityVector.rootBridgeID;
				port.portPriorityVector.rootPathCost = port.designatedPriorityVector.rootPathCost;
				port.portPriorityVector.designatedBridgeID = port.designatedPriorityVector.designatedBridgeID;
				port.portPriorityVector.designatedPortID = port.designatedPriorityVector.designatedPortID;
				
				port.portTimes.copy(port.designatedTimes);
				port.updtInfo = false;
				port.infoIs = RSTPPort.InfoIs.Mine;
				port.newInfo = true;
				state = State.Update_Steady;
			case Update_Steady:
				if (true) state = State.Current_Enter;
				break;
				
			case Current_Enter:
				state = State.Current_Steady;
			case Current_Steady:
				if (port.rcvdMsg && !port.updtInfo) state = State.Receive_Enter;
				break;
				
			case Receive_Enter:
				port.rcvdInfo = port.rcvInfo();
				state = State.Receive_Steady;
			case Receive_Steady:
				if (port.rcvdInfo == RSTPPort.Info.SuperiorDesignatedInfo) state = State.SuperiorDesignated_Enter;
				if (port.rcvdInfo == RSTPPort.Info.RepeatedDesignatedInfo) state = State.RepeatedDesignated_Enter;
				if (port.rcvdInfo == RSTPPort.Info.InferiorDesignatedInfo) state = State.InferiorDesignated_Enter;
				if (port.rcvdInfo == RSTPPort.Info.InferiorRootAlternateInfo) state = State.NotDesignated_Enter;
				if (port.rcvdInfo == RSTPPort.Info.OtherInfo) state = State.Other_Enter;
				break;
				
			case SuperiorDesignated_Enter:
				port.agreed = port.proposing = false;
				port.recordProposal();
				port.setTcFlags();
				port.agree = port.agree && port.betterOrSameInfo(RSTPPort.InfoIs.Received);
				port.recordPriority();
				port.recordTimes();
				port.updtRcvdInfoWhile();
				port.infoIs = RSTPPort.InfoIs.Received;
				port.reselect = true;
				port.selected = port.rcvdMsg = false;
				state = State.SuperiorDesignated_Steady;
			case SuperiorDesignated_Steady:
				if (true) state = State.Current_Enter;
				break;
				
			case RepeatedDesignated_Enter:
				port.recordProposal();
				port.setTcFlags();
				port.updtRcvdInfoWhile();
				port.rcvdMsg = false;
				state = State.RepeatedDesignated_Steady;
			case RepeatedDesignated_Steady:
				if (true) state = State.Current_Enter;
				break;
				
			case InferiorDesignated_Enter:
				port.recordDispute();
				port.rcvdMsg = false;
				state = State.InferiorDesignated_Steady;
			case InferiorDesignated_Steady:
				if (true) state = State.Current_Enter;
				break;
				
			case NotDesignated_Enter:
				port.recordAgreement();
				port.setTcFlags();
				port.rcvdMsg = false;
				state = State.NotDesignated_Steady;
			case NotDesignated_Steady:
				if (true) state = State.Current_Steady;
				break;
				
			case Other_Enter:
				port.rcvdMsg = false;
				state = State.Other_Steady;
			case Other_Steady:
				if (true) state = State.Current_Enter;
				break;
		}
		
		if ((!port.portEnabled && (port.infoIs != RSTPPort.InfoIs.Disabled)) || port.bridge.begin) state = State.Disabled_Enter;
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

/*
 * PortStateTransition.java
 *
 * The Port State Transition state machine.
 *
 * Jonathan Pearson
 * September 12, 2008
 *
 */

package com.sixnetio.Simulation.Bridge.rstp;

public class PortStateTransitionSM extends PortStateMachine {
	public enum State {
		None,
		Discarding_Enter,
		Discarding_Steady,
		Learning_Enter,
		Learning_Steady,
		Forwarding_Enter,
		Forwarding_Steady;
	}
	
	private State state = State.None;
	
	public PortStateTransitionSM(RSTPPort port) {
		super(port);
	}
	
	public void stateStep() {
		State oldState = state;
		
		switch (state) {
			case None:
				break;
				
			case Discarding_Enter:
				port.disableLearning();
				port.learning = false;
				port.disableForwarding();
				port.forwarding = false;
				state = State.Discarding_Steady;
			case Discarding_Steady:
				if (port.learn) state = State.Learning_Enter;
				break;
				
			case Learning_Enter:
				port.enableLearning();
				port.learning = true;
				state = State.Learning_Steady;
			case Learning_Steady:
				if (!port.learn) state = State.Discarding_Enter;
				break;
				
			case Forwarding_Enter:
				port.enableForwarding();
				port.forwarding = true;
				state = State.Forwarding_Steady;
			case Forwarding_Steady:
				if (!port.forward) state = State.Discarding_Enter;
				break;
		}
		
		if (port.bridge.begin) state = State.Discarding_Enter;
		
		if (oldState != state) port.bridge.stateChanged = true;
	}
	
	public State getCurrentState() {
    	return state;
    }
}

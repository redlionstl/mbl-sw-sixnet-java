/*
 * FilteringDatabase.java
 *
 * Represents a filtering database, which is basically a database of known address/port combos.
 *
 * Jonathan Pearson
 * September 16, 2008
 *
 */

package com.sixnetio.Simulation.Bridge;

import com.sixnetio.net.*;

import java.util.*;

public class FilteringDatabase {
	private static class Entry {
		public long timestamp; // Timestamp of last access
		public EthernetAddress address; // Address corresponding to this entry
		public boolean forward;
		public int port; // Port to send through
		
		public Entry(EthernetAddress address, int port) {
			this.timestamp = System.currentTimeMillis();
			this.address = address;
			this.forward = true;
			this.port = port;
		}
	}
	
	private class DynamicDB extends LinkedHashMap<EthernetAddress, Entry> {
		private final int maxCapacity;
		
		public DynamicDB(int maxCapacity) {
			// By allocating double the necessary space and letting it fill just over halfway,
			//   there should be a low number of collisions and no resizing at all
			super(maxCapacity * 2, 0.51f, false);
			this.maxCapacity = maxCapacity;
		}
		
		public void learn(EthernetAddress address, int port) {
			Entry entry = get(address); // Updates the timestamp and everything
			if (entry == null || entry.port != port) {
				// Doesn't exist or isn't accurate, make a new one
				super.put(address, new Entry(address, port));
			} else {
				// Move this entry to the front of the list
				super.remove(address);
				entry.timestamp = System.currentTimeMillis();
				super.put(address, entry);
			}
		}
		
		public Entry put(EthernetAddress address, Entry entry) {
			throw new IllegalStateException("Use 'learn()' instead of 'put'");
		}
		
		public Entry get(EthernetAddress address) {
			return get(address, false);
		}
		
		public Entry get(EthernetAddress address, boolean ignoreAge) {
			Entry entry = super.get(address);
			
			if (!ignoreAge) {
    			if (entry != null) {
    				if (entry.timestamp + bridge.getAgeingTime() * 1000 < System.currentTimeMillis()) {
    					// It's stale, remove it rather than returning it
    					super.remove(address);
    					entry = null;
    				}
    			}
			}
			
			return entry;
		}
		
		protected boolean removeEldestEntry(Map.Entry<EthernetAddress, Entry> eldest) {
			return (size() > maxCapacity || eldest.getValue().timestamp + bridge.getAgeingTime() * 1000 < System.currentTimeMillis());
		}
	}
	
	private static final int D_MAX_CAPACITY = 1024; // Default (for the dynamic db)
	
	// NOTE: Neither of these is internally synchronized for multi-threaded access
	private DynamicDB dynamicDB;
	private Map<EthernetAddress, Entry> staticDB = new LinkedHashMap<EthernetAddress, Entry>();
	
	private Bridge bridge;
	
	public FilteringDatabase(Bridge bridge) {
		this(bridge, D_MAX_CAPACITY);
	}
	
	public FilteringDatabase(Bridge bridge, int maxCapacity) {
		this.bridge = bridge;
		dynamicDB = new DynamicDB(maxCapacity);
	}
	
	public void setStaticEntry(EthernetAddress address, int port, boolean forward) {
		synchronized (staticDB) {
    		Entry entry = new Entry(address, port);
    		entry.forward = forward;
    		staticDB.put(address, entry);
		}
	}
	
	public boolean hasStaticEntry(EthernetAddress address) {
		synchronized (staticDB) {
			return (staticDB.get(address) != null);
		}
	}
	
	public List<EthernetAddress> getStaticAddresses() {
		synchronized (staticDB) {
			return new LinkedList<EthernetAddress>(staticDB.keySet());
		}
	}
	
	public void removeStaticEntry(EthernetAddress address) {
		synchronized (staticDB) {
			staticDB.remove(address);
		}
	}
	
	public int getStaticPort(EthernetAddress address) {
		synchronized (staticDB) {
			return staticDB.get(address).port;
		}
	}
	
	public boolean getStaticForwarding(EthernetAddress address) {
		synchronized (staticDB) {
			return staticDB.get(address).forward;
		}
	}
	
	// Returns true if the combination was not blocked by an existing static entry, false if it
	//   could not be learned because it was blocked
	protected boolean learn(EthernetAddress address, int port) {
		synchronized (staticDB) {
			Entry staticEntry = staticDB.get(address);
			if (staticEntry != null) return false; // There is already a static entry for that address
		}
		
		synchronized (dynamicDB) {
			dynamicDB.learn(address, port);
		}
		
		return true;
	}
	
	// This is used by the routing code to see if there is an entry
	protected boolean hasDynamicEntry(EthernetAddress address) {
		synchronized (dynamicDB) {
			return (dynamicDB.get(address, false) != null);
		}
	}
	
	// These functions allow examining of the dynamic entries, but no updating
	public List<EthernetAddress> getDynamicAddresses() {
		synchronized (dynamicDB) {
			return new LinkedList<EthernetAddress>(dynamicDB.keySet());
		}
	}
	
	public int getDynamicPort(EthernetAddress address) {
		synchronized (dynamicDB) {
			return dynamicDB.get(address, true).port;
		}
	}
	
	public boolean getDynamicForwarding(EthernetAddress address) {
		synchronized (dynamicDB) {
			return dynamicDB.get(address, true).forward;
		}
	}
	
	public long getDynamicTimestamp(EthernetAddress address) {
		synchronized (dynamicDB) {
			return dynamicDB.get(address, true).timestamp;
		}
	}
}

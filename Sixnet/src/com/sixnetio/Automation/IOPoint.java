/*
 * IOPoint.java
 *
 * Represents an I/O point in a station.
 *
 * Jonathan Pearson
 * April 23, 2009
 *
 */

package com.sixnetio.Automation;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.jonp.xml.Tag;

import com.sixnetio.Station.UDRLib;
import com.sixnetio.UDR.UDRMessage;
import com.sixnetio.util.Conversion;

public class IOPoint {
	public static enum DataType {
		Discrete,
		Analog,
		Float,
		Long,
	}
	
	public static enum Direction {
		Input,
		Output,
	}
	
	public String tagName;
	public IOPoint.DataType dataType;
	public IOPoint.Direction direction;
	public short address;
	
	public String lastKnownValue = null;
	
	public IOPoint(IOPoint.DataType dataType, IOPoint.Direction direction, short address) {
		this(null, dataType, direction, address);
	}
	
	public IOPoint(String tagName, IOPoint.DataType dataType, IOPoint.Direction direction, short address) {
		this.tagName = tagName;
		this.dataType = dataType;
		this.direction = direction;
		this.address = address;
	}
	
	public IOPoint(Tag tag) {
		this.tagName = tag.getStringContents("tag", null);
		this.dataType = DataType.valueOf(tag.getStringContents("type"));
		this.direction = Direction.valueOf(tag.getStringContents("direction"));
		this.address = (short)tag.getIntContents("address");
	}
	
	public Tag toXML() {
		Tag root = new Tag("iopoint");
		Tag temp;
		
		// Tag name
		if (tagName != null) {
    		root.addContent(temp = new Tag("tag"));
    		temp.addContent(tagName);
		}
		
		// Type
		root.addContent(temp = new Tag("type"));
		temp.addContent(dataType.name());
		
		// Direction
		root.addContent(temp = new Tag("direction"));
		temp.addContent(direction.name());
		
		// Address
		root.addContent(temp = new Tag("address"));
		temp.addContent("" + address);
		
		return root;
	}
	
	public void set(boolean val, short station, UDRLib udr) throws IOException, TimeoutException {
		set(val ? 1 : 0, station, udr);
	}
	
	public void set(short val, short station, UDRLib udr) throws IOException, TimeoutException {
		set(val & 0xffff, station, udr);
	}
	
	public void set(float val, short station, UDRLib udr) throws IOException, TimeoutException {
		set(Float.floatToRawIntBits(val), station, udr);
	}
	
	public void set(int val, short station, UDRLib udr) throws IOException, TimeoutException {
		switch (dataType) {
			case Analog: {
				short[] data = new short[] { (short)val };
				switch (direction) {
					case Input:
						udr.putA(station, UDRMessage.T_D_AIN, address, (short)1, data);
						break;
					case Output:
						udr.putA(station, UDRMessage.T_D_AOUT, address, (short)1, data);
						break;
				}
				break;
			}
			case Discrete: {
				boolean[] data = new boolean[] { val != 0 };
				switch (direction) {
					case Input:
						udr.putD(station, UDRMessage.T_D_DIN, address, (short)1, data);
						break;
					case Output:
						udr.putD(station, UDRMessage.T_D_DOUT, address, (short)1, data);
						break;
				}
				break;
			}
			case Float: {
				byte[] data = new byte[4];
				Conversion.intToBytes(data, 0, val);
				switch (direction) {
					case Input:
						udr.putB(station, UDRMessage.T_FLOAT_IN, (short)(address * 4), (short)4, data);
						break;
					case Output:
						udr.putB(station, UDRMessage.T_FLOAT_OUT, (short)(address * 4), (short)4, data);
						break;
				}
				break;
			}
			case Long: {
				byte[] data = new byte[4];
				Conversion.intToBytes(data, 0, val);
				switch (direction) {
					case Input:
						udr.putB(station, UDRMessage.T_LONGINT_IN, (short)(address * 4), (short)4, data);
						break;
					case Output:
						udr.putB(station, UDRMessage.T_LONGINT_OUT, (short)(address * 4), (short)4, data);
						break;
				}
				break;
			}
		}
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		if (tagName != null) {
			builder.append(tagName);
		}
		
		builder.append("[");
		switch (dataType) {
			case Analog:
				builder.append("A");
				break;
			case Discrete:
				builder.append("D");
				break;
			case Float:
				builder.append("F");
				break;
			case Long:
				builder.append("L");
				break;
		}
		
		switch (direction) {
			case Input:
				builder.append("I");
				break;
			case Output:
				builder.append("O");
				break;
		}
		
		builder.append(address);
		builder.append("]");
		
		return builder.toString();
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof IOPoint)) return false;
		
		IOPoint point = (IOPoint)obj;
		
		return toString().equals(point.toString());
	}
	
	public int hashCode() {
		return toString().hashCode();
	}
}
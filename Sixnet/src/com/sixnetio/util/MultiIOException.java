/*
 * MultiIOException.java
 *
 * Holds a list of delayed-throw IOExceptions, to be thrown later.
 *
 * Jonathan Pearson
 * February 29, 2008
 *
 */

package com.sixnetio.util;

import java.io.*;
import java.util.*;

public class MultiIOException extends IOException implements Iterable<IOException> {
	private List<IOException> exceptions = new LinkedList<IOException>();
	
	public MultiIOException() { }
	
	public void add(IOException ioe) {
		if (ioe.getStackTrace() == null) ioe.fillInStackTrace();
		
		synchronized (exceptions) {
	        exceptions.add(ioe);
        }
	}
	
	public void clear() {
		synchronized (exceptions) {
	        exceptions.clear();
        }
	}
	
	public boolean isEmpty() {
		synchronized (exceptions) {
	        return (exceptions.size() == 0);
        }
	}
	
	public int size() {
		synchronized (exceptions) {
	        return exceptions.size();
        }
	}
	
	@Override
    public String getMessage() {
		StringBuilder builder = new StringBuilder();
		
		synchronized (exceptions) {
	        for (int i = 0; i < size(); i++) {
		        builder.append(exceptions.get(i).getMessage());
		        if (i < size() - 1) builder.append("\n");
	        }
        }
		return builder.toString();
	}
	
	@Override
    public void printStackTrace() {
		synchronized (exceptions) {
	        for (IOException ioe : exceptions) {
		        ioe.printStackTrace();
	        }
        }
	}
	
	@Override
    public void printStackTrace(PrintStream s) {
		synchronized (exceptions) {
	        for (IOException ioe : exceptions) {
		        ioe.printStackTrace(s);
	        }
        }
	}
	
	@Override
    public void printStackTrace(PrintWriter s) {
		synchronized (exceptions) {
	        for (IOException ioe : exceptions) {
		        ioe.printStackTrace(s);
	        }
        }
	}
	
	// Returns the first exception's stack trace
	@Override
    public StackTraceElement[] getStackTrace() {
		if (isEmpty()) {
			return null;
		} else {
			synchronized (exceptions) {
	            return exceptions.get(0).getStackTrace();
            }
		}
	}

	public Iterator<IOException> iterator() {
	    synchronized (exceptions) {
	        return Utils.readOnlyIterator(exceptions.iterator());
        }
    }
}

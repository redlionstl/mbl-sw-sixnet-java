/*
 * Version.java
 *
 * Represents a version number, and allows for simple comparisons of version numbers.
 *
 * Jonathan Pearson
 * January 28, 2009
 *
 */

package com.sixnetio.util;

import java.util.*;

/**
 * <p>
 * Represents a version number, and allows for simple comparisons between
 * version numbers.
 * </p>
 * 
 * <p>
 * Note, however, that there are two accepted behaviors for comparisons:
 * </p>
 * 
 * <p>
 * Behavior 1, or the "X-version" behavior: To denote a pre-release firmware,
 * such as "1.3.62x" or "3.2.34t5". In this case, you would want that firmware
 * version to be ordered <i>before</i> the root version of "1.3.62" or
 * "3.2.34", respectively.
 * </p>
 * 
 * <p>
 * Behavior 2, or the "Patch" behavior: To denote a small change such as a bug
 * fix, without releasing an entirely new firmware package, you may add a
 * letter onto the version, such as "2.3.1a" or "5.8.3b". In this case, you
 * would want that firmware version to be ordered <i>after</i> the root version
 * of "2.3.1" or "5.8.3", respectively.
 * </p>
 * 
 * <p>
 * Since there is no way to know which numbering scheme is being used, behavior
 * 2 has been chosen.
 * </p>
 * 
 * @author Jonathan Pearson
 */
public class Version implements Comparable<Version> {
	protected final String version;
	
	/**
	 * Construct a new version object.
	 * 
	 * @param version The string representation of the version.
	 */
	public Version(String version) {
		this.version = version;
	}
	
	/**
	 * Compare this version to another version.
	 * 
	 * @param ver The version to compare to.
	 * @return &lt; 0 If this version is older than <tt>ver</tt>, == 0 if this
	 *         version is equal to <tt>ver</tt>, or &gt; 0 if this version is
	 *         newer than <tt>ver</tt>.
	 */
	public int compareTo(Version ver) {
		// Skip everything if possible
		if (equals(ver)) return 0;
		
		StringTokenizer mtok = new StringTokenizer(version, ".");
		StringTokenizer vtok = new StringTokenizer(ver.version, ".");
		
		while (mtok.hasMoreTokens() && vtok.hasMoreTokens()) {
			String mpiece = mtok.nextToken();
			String vpiece = vtok.nextToken();
			
			// If equal, skip further comparisons here
			if (mpiece.equalsIgnoreCase(vpiece)) continue;
			
			try {
				int m = Integer.parseInt(mpiece);
				int v = Integer.parseInt(vpiece);
				
				if (m != v) {
					return (m - v);
				}
			} catch (NumberFormatException nfe) {
				// This part of the version string is not a number...
				
				// If one starts with the other, that one is better (i.e.
				// "3.2.1" vs. "3.2.1-1"
				if (mpiece.startsWith(vpiece)) {
					// This one is better
					return 1;
				} else if (vpiece.startsWith(mpiece)) {
					// That one is better
					return -1;
				}
				
				// Compare numeric portions
				Integer[] mnumbers = getNumericPortions(mpiece);
				Integer[] vnumbers = getNumericPortions(vpiece);
				
				for (int i = 0; i < mnumbers.length; i++) {
					if (i < vnumbers.length) {
						if (mnumbers[i] != vnumbers[i]) {
							return mnumbers[i] - vnumbers[i];
						}
					} else {
						// mnumbers is longer, so let's say it's a more recent
						// version
						return 1;
					}
				}
				
				// If we got here, either they've got the same length and are
				// equal (not possible, we already checked for that above)
				// OR vnumbers is longer, meaning it is a later version
				return -1;
			}
		}
		
		return 0;
	}
	
	// This returns Integer[] only for convenience
	private static Integer[] getNumericPortions(String s) {
		char[] chars = s.toCharArray();
		
		List<Integer> ints = new Vector<Integer>();
		
		int startIndex = 0;
		for (int i = 0; i < chars.length; i++) {
			if (Character.isDigit(i)) {
				// Continue on, this is normal
			} else {
				// If the section we just finished has any content, pull it out
				// into a number
				if (startIndex < i) {
					// There's some numeric content between startIndex and i -
					// 1, inclusive
					ints.add(Integer.parseInt(new String(chars, startIndex, i - startIndex)));
					
					startIndex = i + 1;
				} else {
					// startIndex referenced something else
					// Use its lower-case ASCII value
					ints.add((int)Character.toLowerCase(chars[i]));
					
					startIndex = i + 1;
				}
			}
		}
		
		return ints.toArray(new Integer[0]);
	}
	
	/**
	 * Compare two versions for equality. This is equivalent to
	 * String.equalsIgnoreCase().
	 * 
	 * @param o The other version to compare to.
	 */
	@Override
    public boolean equals(Object o) {
		if (!(o instanceof Version)) return false;
		
		Version v = (Version)o;
		
		return this.version.equals(v.version);
	}
	
	@Override
    public String toString() {
		return version;
	}
	
	@Override
    public int hashCode() {
		return version.hashCode();
	}
}

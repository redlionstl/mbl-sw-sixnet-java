/*
 * Conversion.java
 *
 * A library of conversion functions for going to/from byte arrays.
 * These used to be split between UDR.UDRMessage and Common.Utils, this seemed
 *   like a more helpful place to put them.
 *
 * Jonathan Pearson
 * May 16, 2008
 *
 */

package com.sixnetio.util;

import java.util.StringTokenizer;


public class Conversion
{
    // Functions ending in LE are little endian, otherwise they are big endian
    public static void longToBytes(byte[] dest, int offset, long src)
    {
        dest[offset++] = (byte)((src >> 56) & 0xFF);
        dest[offset++] = (byte)((src >> 48) & 0xFF);
        dest[offset++] = (byte)((src >> 40) & 0xFF);
        dest[offset++] = (byte)((src >> 32) & 0xFF);
        dest[offset++] = (byte)((src >> 24) & 0xFF);
        dest[offset++] = (byte)((src >> 16) & 0xFF);
        dest[offset++] = (byte)((src >> 8) & 0xFF);
        dest[offset++] = (byte)((src >> 0) & 0xFF);
    }

    public static void longToBytesLE(byte[] dest, int offset, long src)
    {
        dest[offset++] = (byte)((src >> 0) & 0xFF);
        dest[offset++] = (byte)((src >> 8) & 0xFF);
        dest[offset++] = (byte)((src >> 16) & 0xFF);
        dest[offset++] = (byte)((src >> 24) & 0xFF);
        dest[offset++] = (byte)((src >> 32) & 0xFF);
        dest[offset++] = (byte)((src >> 40) & 0xFF);
        dest[offset++] = (byte)((src >> 48) & 0xFF);
        dest[offset++] = (byte)((src >> 56) & 0xFF);
    }

    public static void intToBytes(byte[] dest, int offset, int src)
    {
        dest[offset++] = (byte)((src >> 24) & 0xFF);
        dest[offset++] = (byte)((src >> 16) & 0xFF);
        dest[offset++] = (byte)((src >> 8) & 0xFF);
        dest[offset++] = (byte)((src >> 0) & 0xFF);
    }

    public static void intToBytesLE(byte[] dest, int offset, int src)
    {
        dest[offset++] = (byte)((src >> 0) & 0xFF);
        dest[offset++] = (byte)((src >> 8) & 0xFF);
        dest[offset++] = (byte)((src >> 16) & 0xFF);
        dest[offset++] = (byte)((src >> 24) & 0xFF);
    }

    public static void shortToBytes(byte[] dest, int offset, short src)
    {
        dest[offset++] = (byte)((src >> 8) & 0xFF);
        dest[offset++] = (byte)((src >> 0) & 0xFF);
    }

    public static void shortToBytesLE(byte[] dest, int offset, short src)
    {
        dest[offset++] = (byte)((src >> 0) & 0xFF);
        dest[offset++] = (byte)((src >> 8) & 0xFF);
    }

    public static long bytesToLong(byte[] from, int offset)
    {
        long ans = 0;

        ans |= (from[offset++] & 0xFFl) << 56;
        ans |= (from[offset++] & 0xFFl) << 48;
        ans |= (from[offset++] & 0xFFl) << 40;
        ans |= (from[offset++] & 0xFFl) << 32;
        ans |= (from[offset++] & 0xFFl) << 24;
        ans |= (from[offset++] & 0xFFl) << 16;
        ans |= (from[offset++] & 0xFFl) << 8;
        ans |= (from[offset++] & 0xFFl) << 0;

        return ans;
    }

    public static long bytesToLongLE(byte[] from, int offset)
    {
        long ans = 0;

        ans |= (from[offset++] & 0xFFl) << 0;
        ans |= (from[offset++] & 0xFFl) << 8;
        ans |= (from[offset++] & 0xFFl) << 16;
        ans |= (from[offset++] & 0xFFl) << 24;
        ans |= (from[offset++] & 0xFFl) << 32;
        ans |= (from[offset++] & 0xFFl) << 40;
        ans |= (from[offset++] & 0xFFl) << 48;
        ans |= (from[offset++] & 0xFFl) << 56;

        return ans;
    }

    public static int bytesToInt(byte[] from, int offset)
    {
        int ans = 0;

        ans |= (from[offset++] & 0xFF) << 24;
        ans |= (from[offset++] & 0xFF) << 16;
        ans |= (from[offset++] & 0xFF) << 8;
        ans |= (from[offset++] & 0xFF) << 0;

        return ans;
    }

    public static int bytesToIntLE(byte[] from, int offset)
    {
        int ans = 0;

        ans |= (from[offset++] & 0xFF) << 0;
        ans |= (from[offset++] & 0xFF) << 8;
        ans |= (from[offset++] & 0xFF) << 16;
        ans |= (from[offset++] & 0xFF) << 24;

        return ans;
    }

    public static short bytesToShort(byte[] from, int offset)
    {
        short ans = 0;

        ans |= (from[offset++] & 0xFF) << 8;
        ans |= (from[offset++] & 0xFF) << 0;

        return ans;
    }

    public static short bytesToShortLE(byte[] from, int offset)
    {
        short ans = 0;

        ans |= (from[offset++] & 0xFF) << 0;
        ans |= (from[offset++] & 0xFF) << 8;

        return ans;
    }

    // Make a byte array containing ASCII characters (null terminated) into a
    // string, starting at the specified index in the array
    public static String bytesToString(byte[] data, int index)
    {
        StringBuilder builder = new StringBuilder("");

        while (data[index] != '\0') {
            builder.append((char)data[index]);
            index++;
        }

        return builder.toString();
    }

    // Put a string into a byte array, starting at index in the array and
    // terminating
    // the string with a null
    // Make sure the byte array you pass in is large enough
    public static int stringToBytes(byte[] data, int index, String str)
    {
        byte[] asBytes = str.getBytes();

        System.arraycopy(asBytes, 0, data, index, asBytes.length);
        index += asBytes.length;

        data[index++] = 0;

        return index;
    }

    /**
     * Make a byte array into a string containing a lower-case hex
     * representation, with an optional delimiter.
     * 
     * @param vals The values to convert to a string.
     * @param delim The delimiter. May be any length.
     * @return A string made up of 2-digit hex values separated by the
     *         delimiter, which will not appear before the first or after the
     *         last value.
     */
    public static String bytesToHex(byte[] vals, String delim)
    {
        StringBuilder ans = new StringBuilder();
        for (int i = 0; i < vals.length; i++) {
            ans.append(String.format("%02x", vals[i]));
            if (i < vals.length - 1) {
                ans.append(delim);
            }
        }

        return ans.toString();
    }

    // Make a byte array into a string containing a hex representation
    public static String bytesToHex(byte[] vals)
    {
        return bytesToHex(vals, "");
    }

    /**
     * Make a string containing a series of hex encoded bytes separated by
     * delimiters into a byte array.
     * 
     * @param hex The hex encoded bytes separated by delimiters.
     * @param delim The delimiter. Must have a non-zero length and may not
     *            include only characters in the ranges 0-9, a-z, or A-Z. If it
     *            does not start or end with any of those characters, the hex
     *            encoded bytes may be either one or two digits. No guarantee is
     *            made that parsing will succeed if any of those characters
     *            appear in the delimiter, especially at one end or the other of
     *            it.
     * @return The bytes represented by the hex encoded string.
     */
    public static byte[] hexToBytes(String hex, String delim)
    {
        StringTokenizer tok = new StringTokenizer(hex, delim);
        byte[] ans = new byte[tok.countTokens()];
        int off = 0;
        while (tok.hasMoreTokens()) {
            ans[off++] = (byte)(Integer.parseInt(tok.nextToken(), 16) & 0xff);
        }

        return ans;
    }

    // Make a string containing a hex number into a byte array
    public static byte[] hexToBytes(String hex)
    {
        if (hex.length() % 2 != 0) {
            hex = "0" + hex;
        }

        byte[] ans = new byte[hex.length() / 2];
        for (int i = 0; i < hex.length(); i += 2) {
            ans[i / 2] =
                (byte)(Integer.parseInt(hex.substring(i, i + 2), 16) & 0xff);
        }

        return ans;
    }

    // Reverse the bytes in the given integer
    public static short reverse(short val)
    {
        return (short)((((val >> 0) & 0xff) << 8) | (((val >> 8) & 0xff) << 0));
    }

    public static int reverse(int val)
    {
        return ((((val >> 0) & 0xff) << 24) | (((val >> 8) & 0xff) << 16) |
                (((val >> 16) & 0xff) << 8) | (((val >> 24) & 0xff) << 0));
    }

    public static long reverse(long val)
    {
        return ((((val >> 0) & 0xff) << 56) | (((val >> 8) & 0xff) << 48) |
                (((val >> 16) & 0xff) << 40) | (((val >> 24) & 0xff) << 32) |
                (((val >> 32) & 0xff) << 24) | (((val >> 40) & 0xff) << 16) |
                (((val >> 48) & 0xff) << 8) | (((val >> 56) & 0xff) << 0));
    }

    /**
     * Given a RSSI (Received Signal Strength Indicator) or RSCP (Received
     * Signal Code Power), returns a RSSI.
     * 
     * @param val The RSSI or RSCP.
     * @return An RSSI equivalent to the value passed in.
     */
    public static float toRSSI(float val)
    {
        if (val > 0) {
            // RSCP
            if (val == 99.0f) {
                val = -110.0f;
            }
            else {
                val = -110.0f + (val * 1.09375f);
            }
        }

        return val;
    }

    /**
     * Convert knots to kilometers per hour.
     * 
     * @param knots The speed in knots.
     * @return The speed in kph.
     */
    public static float knotsToKph(float knots)
    {
        return (knots * 1.852f);
    }

    /**
     * Convert miles per hour to kilometers per hour.
     * 
     * @param mph The speed in mph.
     * @return The speed in kph.
     */
    public static float mphToKph(float mph)
    {
        return (mph * 1.609344f);
    }
}

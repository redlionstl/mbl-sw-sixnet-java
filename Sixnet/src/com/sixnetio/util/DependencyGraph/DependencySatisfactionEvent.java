/*
 * DependencySatisfactionEvent.java
 *
 * Used to alert a DependencySatisfactionListener that a DependencyGraph item has been satisfied.
 *
 * Jonathan Pearson
 * June 28, 2007
 *
 */

package com.sixnetio.util.DependencyGraph;

public class DependencySatisfactionEvent<E> {
	private DependencyGraph<E> source;
	private E item;
	
	public DependencySatisfactionEvent(DependencyGraph<E> source, E item) {
		this.source = source;
		this.item = item;
	}
	
	public DependencyGraph<E> getSource() {
		return source;
	}
	
	public E getItem() {
		return item;
	}
}

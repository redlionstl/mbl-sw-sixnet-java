/*
 * DependencyGraph.java
 *
 * Represents the dependencies between objects of the same type.
 *
 * Jonathan Pearson
 * June 28, 2007
 *
 */

package com.sixnetio.util.DependencyGraph;

import java.io.PrintStream;
import java.util.*;

public class DependencyGraph<E> {
	private static class Node<E> {
		public Set<Node<E> > dependsOn;
		public Set<Node<E> > dependents;
		public E data;
		public volatile boolean satisfied; // Set to true when all dependsOn nodes have been satisfied
		
		public Node(E data) {
			this.data = data;
			
			dependsOn = new HashSet<Node<E> >();
			dependents = new HashSet<Node<E> >();
			satisfied = false;
		}
	}
	
	private Hashtable<E, Node<E> > graph;
	private List<DependencySatisfactionListener<E> > listeners;
	
	public DependencyGraph() {
		graph = new Hashtable<E, Node<E> >();
		listeners = new LinkedList<DependencySatisfactionListener<E> >();
	}
	
	public Set<E> getStartingItems() {
		// Returns an enumeration over the nodes that depend on nothing
		HashSet<E> starting = new HashSet<E>();
		
		Enumeration<E> keys = graph.keys();
		while (keys.hasMoreElements()) {
			E key = keys.nextElement();
			Set<Node<E> > dependsOn = graph.get(key).dependsOn;
			synchronized (dependsOn) {
				if (dependsOn.isEmpty()) starting.add(key);
			}
		}
		
		return starting;
	}
	
	public Set<E> getAllItems() {
		return graph.keySet();
	}
	
	public int size() {
		return graph.size();
	}
	
	public void addDependencySatisfactionListener(DependencySatisfactionListener<E> dsl) {
		synchronized (listeners) {
	        listeners.add(dsl);
        }
	}
	
	public void removeDependencySatisfactionListener(DependencySatisfactionListener<E> dsl) {
		synchronized (listeners) {
	        listeners.remove(dsl);
        }
	}
	
	public void addDependency(E guardian, E dependent) {
		Node<E> nGuardian = graph.get(guardian);
		Node<E> nDependent = graph.get(dependent);
		
		if (nGuardian == null) {
			nGuardian = new Node<E>(guardian);
			graph.put(guardian, nGuardian);
		}
		
		if (nDependent == null) {
			nDependent = new Node<E>(dependent);
			graph.put(dependent, nDependent);
		}
		
		synchronized (nGuardian.dependents) {
			nGuardian.dependents.add(nDependent);
		}
		
		synchronized (nDependent.dependsOn) {
			nDependent.dependsOn.add(nGuardian);
		}
	}
	
	public boolean isSatisfied(E item) {
		Node<E> nItem = graph.get(item);
		
		if (nItem == null) throw new NullPointerException("Cannot check satisfaction of a non-existent item");
		
		return nItem.satisfied;
	}
	
	public void satisfy(E item) {
		Node<E> nItem = graph.get(item);
		
		if (nItem == null) throw new NullPointerException("Cannot satisfy a non-existent item");
		
		nItem.satisfied = true;
		
		synchronized (nItem.dependents) {
    		for (Node<E> alert : nItem.dependents) {
    			checkSatisfaction(alert);
    		}
		}
	}
	
	public E getFirstUnsatisfiedDependency(E item) {
		Node<E> nItem = graph.get(item);
		
		if (nItem == null) throw new NullPointerException("Given item does not exist in this graph");
		
		synchronized (nItem.dependsOn) {
    		for (Node<E> dep : nItem.dependsOn) {
    			if (!dep.satisfied) return dep.data;
    		}
		}
		
		return null;
	}
	
	protected void fireDependencySatisfaction(E item) {
		List<DependencySatisfactionListener<E>> listeners;
		synchronized (this.listeners) {
			listeners = new ArrayList<DependencySatisfactionListener<E>>(this.listeners);
		}
		
		for (DependencySatisfactionListener<E> listener : listeners) {
			listener.dependencySatisfied(new DependencySatisfactionEvent<E>(this, item));
		}
	}
	
	protected void checkSatisfaction(Node<E> node) {
		if (!node.satisfied) {
			boolean satisfied = true;
			
			synchronized (node.dependsOn) {
    			for (Node<E> dep : node.dependsOn) {
    				if (!dep.satisfied) {
    					satisfied = false;
    					break;
    				}
    			}
			}
			
			if (satisfied) {
				fireDependencySatisfaction(node.data);
			}
		}
	}
	
	public void print(PrintStream out) {
		Enumeration<E> keys = graph.keys();
		while (keys.hasMoreElements()) {
			E key = keys.nextElement();
			
			out.println(key + " (" + (graph.get(key).satisfied ? "Satisfied" : "Unsatisfied") + ")");
			
			Set<Node<E>> dependents = graph.get(key).dependents;
			synchronized (dependents) {
    			for (Node<E> dep : dependents) {
    				out.println("\t==> " + dep.data);
    			}
			}
		}
	}
}

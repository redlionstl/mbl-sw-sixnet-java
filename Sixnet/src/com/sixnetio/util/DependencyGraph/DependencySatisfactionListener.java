/*
 * DependencySatisfactionListener.java
 *
 * A class may implement this interface to listen for SatisfactionEvents from a DependencyGraph.
 *
 * Jonathan Pearson
 * June 28, 2007
 *
 */

package com.sixnetio.util.DependencyGraph;

public interface DependencySatisfactionListener<E> {
	public void dependencySatisfied(DependencySatisfactionEvent<E> dse);
}

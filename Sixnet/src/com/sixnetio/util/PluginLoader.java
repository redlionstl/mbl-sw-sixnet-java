/*
 * PluginLoader.java
 *
 * Watches a set of directories for new JAR files. If a new file is detected,
 * its manifest is scanned to see if it contains a BVB plugin. If so, the
 * class named in the manifest is loaded.
 *
 * Jonathan Pearson
 * April 7, 2009
 *
 */

package com.sixnetio.util;

import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Hashtable;
import java.util.Map;
import java.util.jar.*;

import org.apache.log4j.Logger;

import com.sixnetio.io.DirectoryWatcher;

/**
 * Watch a given set of directories for .jar files whose manifests contain some
 * specified properties. This expects two properties in each manifest for a
 * plugin; if these properties do not exist, then the jar file is ignored as not
 * being a plugin. The two properties must be in the 'Plugin' section, and are:
 * <ol>
 * <li><b>Plugin-Program</b>: Value should be a string identifying the program
 * that would recognize the plugin.</li>
 * <li><b>Plugin-Class</b>: Value is the name of the main plugin class. Loading
 * this class should register the plugin with the program, allowing it to be
 * used later.</li>
 * </ol>
 * 
 * You may also specify individual JAR files to attempt to load as plugins using
 * {@link #loadPlugin(File)}, rather than watching directories for new files.
 * 
 * Here is an example manifest file that would provide a plugin for the Xample
 * program:
 * <pre>
 * Manifest-Version: 1.0
 * 
 * Name: Plugin
 * Plugin-Program: Xample
 * Plugin-Class: com.xample.plugins.XamplePlugin
 * </pre>
 * 
 * @author Jonathan Pearson
 */
public class PluginLoader {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * A directory change listener that loads JAR files as plugins.
	 *
	 * @author Jonathan Pearson
	 */
	private class MyDirectoryChangeListener implements DirectoryWatcher.DirectoryChangeListener {
		/**
		 * Called when a {@link DirectoryWatcher} detects a change.
		 * 
		 * @param e The event representing the change.
		 */
		@Override
		public void directoryChanged(DirectoryWatcher.DirectoryChangeEvent e) {
			if (e.getChangeType() == DirectoryWatcher.DirectoryChangeType.FileCreated) {
				File f = e.getChangedFile();
				
				try {
					loadPlugin(f);
				} catch (IOException ioe) {
	    			logger.warn(String.format("Unable to read JAR file '%s'",
	    			                          f.getAbsolutePath()), ioe);
	    			
	    		} catch (ClassNotFoundException cnfe) {
	                logger.error(String.format("Unable to load main class from plugin JAR '%s'",
	                                           f.getAbsolutePath()), cnfe);
	                
	    		} catch (Exception ex) {
	    			logger.info(String.format("Unable to load JAR '%s' as a plugin",
	    			                          f.getAbsolutePath()), ex);
	    			
	    		}
			} else {
				// We don't support removing/modifying plugins
			}
		}
		
		/**
		 * Called when a DirectoryWatcher scans its directory.
		 * 
		 * @param watcher The watcher that scanned its directory.
		 */
		@Override
		public void directoryScanned(DirectoryWatcher watcher) {
			synchronized (watchers) {
				watchers.put(watcher, true);
				watchers.notifyAll();
			}
		}
	}
	
	// Watcher --> Has scanned its directory
	final Map<DirectoryWatcher, Boolean> watchers = new Hashtable<DirectoryWatcher, Boolean>();
	private final MyDirectoryChangeListener directoryChangeListener = new MyDirectoryChangeListener();
	
	private String programName;
	
	/**
	 * Construct a new PluginWatcher that will watch for plugins for the given
	 * program.
	 * 
	 * @param programName The name of the program. This is matched against the
	 *            'Plugin-Program' field in the manifest file of JAR files.
	 */
	public PluginLoader(String programName) {
		this.programName = programName;
	}
	
	/**
	 * Watch the given directory for plugin JARs. This will block until the
	 * directory has been scanned for new plugins and those plugins have been
	 * loaded.
	 * 
	 * @param directory The directory to watch.
	 */
	public void watchDirectory(File directory) {
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException("Not a directory: " + directory.getAbsolutePath());
		}
		
		DirectoryWatcher watcher = new DirectoryWatcher(directory, ".*\\.jar$");
		
		watcher.addDirectoryChangeListener(directoryChangeListener);
		
		watcher.setDaemon(true);
		
		synchronized (watchers) {
			// Make sure we put the 'false' in there before giving the watcher
			// a chance to put in a 'true'
			watchers.put(watcher, false);
			
			watcher.start();
			
			while (watchers.get(watcher) == false) {
				try {
	                watchers.wait();
                } catch (InterruptedException ie) {
	                throw new InterruptedOperationException(ie);
                }
			}
		}
	}
	
	/**
	 * Attempt to load the provided file as a JAR containing a plugin.
	 * 
	 * @param f The file to load.
	 * @throws IOException If there was a problem reading the file as a JAR.
	 * @throws ClassNotFoundException If the file had the proper metadata
	 *   to define it as a plugin, but the metadata pointed to a class that
	 *   did not exist in the file.
	 * @throws Exception If the file did not have the necessary metadata to
	 *   define it as a plugin. Will contain a descriptive message.
	 */
	public void loadPlugin(File f) throws IOException, ClassNotFoundException, Exception {
		// A new JAR file was created, examine its manifest
		
		logger.info("Attempting to load '" + f.getAbsolutePath() +
		            "' as a plugin");
		
		FileInputStream fin = new FileInputStream(f);
		Manifest manifest;
		
		try {
			JarInputStream jin = new JarInputStream(fin);
			
			try {
    			// Read the manifest; if missing, it's not a plugin
    			manifest = jin.getManifest();
			} finally {
				jin.close();
			}
		} finally {
			fin.close();
		}
		
		if (manifest == null) {
			// Not a plugin
			throw new Exception("Not a plugin: no manifest");
		}
		
		// Read the 'Plugin' section's attributes; if missing, it's not a plugin
		Attributes attributes = manifest.getAttributes("Plugin");
		if (attributes == null) {
			// Not a plugin
			throw new Exception("Not a plugin: no 'Plugin' section in manifest");
		}
		
		// Look for the Plugin-Program and Plugin-Class attributes
		String pluginProgram = attributes.getValue("Plugin-Program");
		String pluginClass = attributes.getValue("Plugin-Class");
		
		if (pluginProgram == null) {
			throw new Exception("Not a plugin: Missing 'Plugin-Program' property");
		}
		
		if (!pluginProgram.equals(programName)) {
			throw new Exception(String.format("Not a valid plugin: Program '%s' not recognized", pluginProgram));
		}
		
		if (pluginClass == null) {
			throw new Exception("Not a plugin: Missing 'Plugin-Class' property");
		}
		
		// Load the main class
		URLClassLoader loader = URLClassLoader.newInstance(new URL[] {
			new URL("file:" + f.getAbsolutePath())
		}, Thread.currentThread().getContextClassLoader());
		
		if (logger.isDebugEnabled()) {
			ClassLoader currentLoader = loader;
			StringBuilder builder = new StringBuilder();
			while (currentLoader != null) {
				builder.append(currentLoader.getClass().getName());
				
				if (currentLoader instanceof URLClassLoader) {
					builder.append(" [");
					for (URL url : ((URLClassLoader)currentLoader).getURLs()) {
						builder.append(url.toString()).append(", ");
					}
					builder.append("]");
				}
				builder.append(" => ");
				
				currentLoader = currentLoader.getParent();
			}
			
			logger.debug("Class loader hierarchy: " + builder.toString());
		}
		
		/*
		JarClassLoader loader;
		try {
			loader = AccessController.doPrivileged(new PrivilegedExceptionAction<JarClassLoader>() {
				public JarClassLoader run() throws IOException {
					return new JarClassLoader(jin);
				}
			});
		} catch (PrivilegedActionException pae) {
			throw (IOException)pae.getCause();
		}
		*/
		
        Class.forName(pluginClass, true, loader);
        
        // Success!
        logger.info(String.format("Loaded plugin JAR '%s'",
                                  f.getAbsolutePath()));
	}
}

/*
 * Utils.java
 *
 * Common utility functions
 *
 * Jonathan Pearson
 * January 9, 2008
 *
 */

package com.sixnetio.util;

import java.io.*;
import java.net.URL;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.zip.CRC32;

import org.apache.log4j.Logger;
import org.jonp.xml.Tag;
import org.jonp.xml.XML;

/**
 * A class containing static utility functions and data.
 * 
 * @author Jonathan Pearson
 */
public class Utils
{
    private static final Logger logger = Logger.getLogger(thisClassName());

    // Constants
    /**
     * If true, the program is being debugged; if false, the program is
     * executing normally.
     */
    public static final boolean DEBUG;

    /** The stream that debugging messages should be printed on. */
    public static final PrintStream DEBUG_TEXT_STREAM = System.err;

    /** The stream that exceptions should be printed on in debug mode. */
    public static final PrintStream DEBUG_EXCEPTION_STREAM = System.err;

    // This may lose the 'final' modifier in the future, with the goal of
    // tracking the
    // current directory across multiple FileChooser instantiations
    /** The directory that the program was started in. */
    public static final File CUR_DIR;

    /** An XML object to share, to avoid constructing your own every time. */
    public static final XML xmlParser = new XML();

    /** A random number generator for general use. */
    public static final Random rand = new Random();

    /** A secure random number generator for general use. */
    public static final SecureRandom srand = new SecureRandom();

    private static final String BASE64_CHARS =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    private static final byte[] TRANS64;
    private static final byte[] RESTORE64;

    static {
        // If running from a Web server (like Tomcat), these file tests
        // could throw a security exception

        File tempCurDir;
        try {
            tempCurDir = (new File(".")).getAbsoluteFile();
        }
        catch (SecurityException se) {
            tempCurDir = null;
        }
        CUR_DIR = tempCurDir;

        boolean tempDebug;
        try {
            File dbg = new File("DEBUG");
            if (dbg.isFile()) {
                tempDebug = true;
            }
            else {
                tempDebug = false;
            }
        }
        catch (SecurityException se) {
            tempDebug = false;
        }
        DEBUG = tempDebug;

        TRANS64 = BASE64_CHARS.getBytes();

        RESTORE64 = new byte[128];
        for (byte i = 0; i < TRANS64.length; i++) {
            RESTORE64[TRANS64[i]] = i;
        }
    }

    // debug() functions
    /**
     * If in debug mode, prints a message to the debug text stream.
     * 
     * @param msg The message to print.
     */
    public static void debug(String msg)
    {
        if (DEBUG) {
            DEBUG_TEXT_STREAM.println(msg);
        }
    }

    /**
     * If in debug mode, prints a formatted message to the debug text stream.
     * 
     * @param format A format specifier.
     * @param args The arguments to the format specifier.
     */
    public static void debug(String format, Object... args)
    {
        if (DEBUG) {
            DEBUG_TEXT_STREAM.printf(format, args);
        }
    }

    /**
     * If in debug mode, prints a stack trace to the debug exception stream.
     * 
     * @param th The object whose stack trace should be printed.
     */
    public static void debug(Throwable th)
    {
        if (DEBUG) {
            th.printStackTrace(DEBUG_EXCEPTION_STREAM);
        }
    }

    /**
     * If in debug mode, prints a hex representation of the byte array with some
     * indentation.
     * 
     * @param indent The indentation string to use.
     * @param data The array to print; all of it will be printed.
     */
    public static void debug(String indent, byte[] data)
    {
        if (DEBUG) {
            debug(indent, data, 0, data.length);
        }
    }

    /**
     * If in debug mode, prints a hex representation of the byte array with some
     * indentation.
     * 
     * @param indent The indentation string to use.
     * @param data The array to print.
     * @param start The index in the array at which to start printing.
     * @param len The number of bytes to print from the array.
     */
    public static void debug(String indent, byte[] data, int start, int len)
    {
        if (DEBUG) {
            for (int i = start; i < len; i += 8) {
                StringBuilder line = new StringBuilder(indent);
                for (int j = i; j < len && j < i + 8; j++) {
                    line.append(String.format("%02x ", data[j]));
                    if (j == i + 3) {
                        line.append(" ");
                    }
                }

                DEBUG_TEXT_STREAM.println(line.toString());
            }
        }
    }

    // OS functions
    /**
     * Cache the OS detection, it's not going to change while the program runs.
     */
    private static Boolean windows = null;

    /**
     * Check whether the program is running on a Windows platform.
     * 
     * @return True = running on Windows, false = not running on Windows.
     */
    public static boolean osIsWindows()
    {
        if (windows == null) {
            String os = System.getProperty("os.name").toLowerCase();
            windows = (os.indexOf("windows") != -1);
        }

        return windows;
    }

    /**
     * Get the version of the running operating system.
     */
    public static Version osVersion()
    {
        return new Version(System.getProperty("os.version"));
    }

    // File locking
    /**
     * Attempt to acquire a file lock. Note: The Java API specifically says that
     * the method used here should not be used for locking, but it is currently
     * the best method available.
     * 
     * @param lockFile The lock file to try to acquire.
     * @param timeout How long to try for, in seconds.
     * @throws IOException If unable to acquire the lock before the timeout or
     *             an error occurs in File.createNewFile().
     */
    public static void acquireLock(File lockFile, int timeout)
        throws IOException
    {
        // Try to acquire the lock
        // NOTE: The Java API specifically says not to use this for locking, but
        // as it is still better than
        // the current method, and the recommended method would be completely
        // incompatible with existing
        // programs, this is a good compromise
        // Just remember to delete the lock file when you're done...
        int counter = 0;
        boolean lockFileCreated = false;
        while (counter < timeout &&
               !(lockFileCreated = lockFile.createNewFile())) {
            Utils.sleep(1000);
            counter++;
        }

        if (!lockFileCreated) {
            throw new IOException(
                "Timed out while trying to acquire the lock file");
        }
    }

    /**
     * Sleep for a specified period, no busy-waiting.
     * 
     * @param ms The amount of time to sleep for, in milliseconds. If less than
     *            or equal to zero, returns immediately.
     * @throws InterruptedOperationException If the thread is interrupted while
     *             sleeping.
     */
    public static void sleep(long ms)
    {
        if (ms <= 0) {
            return;
        }

        long stopAt = System.currentTimeMillis() + ms;
        while (System.currentTimeMillis() < stopAt) {
            try {
                Thread.sleep(stopAt - System.currentTimeMillis());
            }
            catch (InterruptedException ie) {
                throw new InterruptedOperationException(ie);
            }
        }
    }

    // Formatting functions
    /**
     * Format a time as a date in a short format.
     * 
     * @param ms The time, in milliseconds since the epoch.
     * @return The formatted date, like "1/8/09".
     */
    public static String formatShortDate(long ms)
    {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        return df.format(new Date(ms));
    }

    /**
     * Format a time as a date in a medium format.
     * 
     * @param ms The time, in milliseconds since the epoch.
     * @return The formatted date, like "Jan 8, 2009".
     */
    public static String formatMediumDate(long ms)
    {
        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        return df.format(new Date(ms));
    }

    /**
     * Format a time as a date in a long format.
     * 
     * @param ms The time, in milliseconds since the epoch.
     * @return The formatted date, like "January 8, 2009".
     */
    public static String formatLongDate(long ms)
    {
        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
        return df.format(new Date(ms));
    }

    // Base64 functions
    // Encode
    /**
     * Encode a string as Base64.
     * 
     * @param data The string to encode.
     * @return The encoded Base64 string.
     */
    public static String encode64(String data)
    {
        return encode64(data.getBytes());
    }

    /**
     * Encode a byte array as Base64.
     * 
     * @param data The data to encode.
     * @return The encoded Base64 string.
     */
    public static String encode64(byte[] data)
    {
        StringBuilder ans = new StringBuilder();

        int padCount = data.length % 3;
        if (padCount > 0) {
            padCount = 3 - padCount;
        }

        byte[] work = new byte[data.length + padCount];
        System.arraycopy(data, 0, work, 0, data.length);

        for (int i = 0; i < padCount; i++) {
            work[work.length - i - 1] = (byte)0;
        }

        // Loop through, 3 bytes at a time (3 bytes becomes 4 chars)
        for (int i = 0; i < data.length; i += 3) {
            // Add a newline after every 76 output chars, according to the MIME
            // spec
            if (i > 0 && (i / 3 * 4) % 76 == 0) {
                ans.append('\n');
            }

            // these three 8-bit (ASCII) characters become one 24-bit number
            int n =
                ((work[i] & 0xff) << 16) | ((work[i + 1] & 0xff) << 8) |
                    (work[i + 2] & 0xff);

            // this 24-bit number gets separated into four 6-bit numbers
            int a = (n >>> 18) & 0x3f;
            int b = (n >>> 12) & 0x3f;
            int c = (n >>> 6) & 0x3f;
            int d = n & 0x3f;

            String block =
                "" + (char)TRANS64[a] + (char)TRANS64[b] + (char)TRANS64[c] +
                    (char)TRANS64[d];

            // those four 6-bit numbers are used as indices into the base64
            // character list
            ans.append(block);
        }

        // add the actual padding string, after removing the zero pad
        for (int i = 0; i < padCount; i++) {
            ans.replace(ans.length() - i - 1, ans.length() - i, "=");
        }

        return ans.toString();
    }

    // Decode
    /**
     * Decode Base64 data.
     * 
     * @param s The Base64 string to decode.
     * @return The decoded data.
     */
    public static byte[] decode64(String s)
    {
        // Remove all unexpected chars from the string
        s = s.replaceAll("[^" + BASE64_CHARS + "=]", "");

        // Convert to bytes for easy access
        byte[] work = s.getBytes();

        // Change '=' to 0-padding
        int padCount = 0;
        for (int i = work.length - 1; i >= 0; i--) {
            if (work[i] == (byte)'=') {
                work[i] = 0;
                padCount++;
            }
            else {
                break;
            }
        }

        // Loop through, 4 chars at a time (4 chars becomes 3 bytes)
        byte[] ans = new byte[work.length * 3 / 4];
        int pos = 0;
        for (int i = 0; i < work.length; i += 4) {
            // each of these four characters represents a 6-bit index in the
            // base64 characters list
            // which, when concatenated, will give the 24-bit number for the
            // original 3 characters
            int n =
                ((RESTORE64[work[i]] & 0x3f) << 18) |
                    ((RESTORE64[work[i + 1]] & 0x3f) << 12) |
                    ((RESTORE64[work[i + 2]] & 0x3f) << 6) |
                    (RESTORE64[work[i + 3]] & 0x3f);

            // split the 24-bit number into the original three 8-bit (ASCII)
            // characters
            ans[pos++] = (byte)((n >>> 16) & 0xff);
            ans[pos++] = (byte)((n >>> 8) & 0xff);
            ans[pos++] = (byte)(n & 0xff);
        }

        // Cut out the extra bytes from padding
        byte[] data = new byte[ans.length - padCount];
        System.arraycopy(ans, 0, data, 0, data.length);

        return data;
    }

    /**
     * Converts a subnet mask to a network prefix length.
     * 
     * @param subnet A subnet mask like <tt>255.128.0.0</tt>.
     * @return The network prefix length represented by the given subnet mask,
     *         like <tt>9</tt> for the above example..
     */
    public static short prefixFromSubnet(String subnet)
    {
        // Parse into four bytes, figure out how many contiguous 1s are in it
        StringTokenizer tok = new StringTokenizer(subnet, ".");

        // Only interested in the last four; the rest shouldn't be there
        while (tok.countTokens() > 4) {
            tok.nextToken();
        }

        short counter = 0;
        while (tok.hasMoreTokens()) {
            byte b = 0;

            try {
                int nextByte = Integer.parseInt(tok.nextToken());
                b = (byte)(nextByte & 0xff);
            }
            catch (NumberFormatException nfe) {
                break;
            }

            for (int bit = 7; bit >= 0; bit--) {
                if (((b >> bit) & 0x1) != 0) {
                    counter++;
                }
                else {
                    return counter;
                }
            }
        }

        return counter;
    }

    /**
     * Convert the number after the slash (the network prefix length) to a
     * subnet mask.
     * 
     * @param prefixLength A number, like <tt>24</tt>.
     * @return The subnet mask equivalent to that network prefix length, like
     *         <tt>255.255.255.0</tt> in the above example..
     */
    public static String subnetFromPrefix(int prefixLength)
    {
        try {
            String ans = "";
            int segments = 0;

            while (segments < 4) {
                int nibble;

                if (prefixLength >= 8) {
                    nibble = 8;
                    prefixLength -= 8;
                }
                else {
                    nibble = prefixLength;
                    prefixLength = 0;
                }

                if (ans.length() > 0) {
                    ans += ".";
                }

                switch (nibble) {
                    case 8:
                        ans += "255";
                        break;
                    case 7:
                        ans += "254";
                        break;
                    case 6:
                        ans += "252";
                        break;
                    case 5:
                        ans += "248";
                        break;
                    case 4:
                        ans += "240";
                        break;
                    case 3:
                        ans += "224";
                        break;
                    case 2:
                        ans += "192";
                        break;
                    case 1:
                        ans += "128";
                        break;
                    case 0:
                        ans += "0";
                        break;
                }

                segments++;
            }

            return ans;
        }
        catch (NumberFormatException nfe) {
            return "0.0.0.0";
        }
    }

    /**
     * Parse a string representation of an IPv4 address into an array of
     * integers.
     * 
     * @param s The IP address to parse.
     * @return The integer array representing the IP address, with the first
     *         piece in index 0.
     * @throws NumberFormatException If the IP address is not properly
     *             formatted.
     */
    public static int[] parseIP(String s)
        throws NumberFormatException
    {
        int[] pieces = new int[4];
        StringTokenizer tok = new StringTokenizer(s, ".");

        for (int i = 0; i < pieces.length; i++) {
            if (!tok.hasMoreTokens()) {
                throw new NumberFormatException(
                    "There must be exactly four pieces to an IP address, separated by .s, not enough");
            }

            String token = tok.nextToken();
            pieces[i] = Integer.parseInt(token);

            if (pieces[i] < 0 || pieces[i] > 255) {
                throw new NumberFormatException(
                    "Each piece of an IP address must be in the range [0-255]: " +
                        pieces[i]);
            }
        }

        if (tok.hasMoreTokens()) {
            throw new NumberFormatException(
                "There must be exactly four pieces to an IP address, separated by .s, found extras");
        }

        return pieces;
    }

    /**
     * Given an IPv4 address in an integer array, converts it to a string
     * representation.
     * 
     * @param ip The IP address.
     * @return The string representation, with the first index in the first
     *         position.
     */
    public static String makeIP(int[] ip)
    {
        return String.format("%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
    }

    /**
     * Given an IPv4 address in an integer, converts it to a string
     * representation.
     * 
     * @param ip The IP address, in network byte order (high-order byte has the
     *            first position).
     * @return The string representation.
     */
    public static String makeIP(int ip)
    {
        byte[] pieces = new byte[4];
        Conversion.intToBytes(pieces, 0, ip);

        return String.format("%d.%d.%d.%d", pieces[0] & 0xff, pieces[1] & 0xff,
            pieces[2] & 0xff, pieces[3] & 0xff);
    }

    // Stream utility functions
    /**
     * Read a stream until a specific byte string is encountered. Discards all
     * bytes read, including the byte string being searched for.
     * 
     * @param in The stream to read.
     * @param needle The byte string to search for.
     * @throws IOException If there is a problem with the underlying stream, or
     *             it runs out of data before the byte string being searched for
     *             is read.
     */
    public static void readUntil(InputStream in, byte[] needle)
        throws IOException
    {
        int offset = 0;

        while (offset < needle.length) {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            int b = in.read();
            if (b == -1) {
                throw new IOException("Unexpected end of stream");
            }

            if (((byte)b & 0xff) == needle[offset]) {
                offset++;
            }
            else {
                offset = 0;
            }
        }
    }

    /**
     * Read a stream until a specific string is encountered. Discards all bytes
     * read, including the string being searched for. This will only work
     * properly for ASCII data.
     * 
     * @param in The stream to read.
     * @param needle The string to search for.
     * @throws IOException If there is a problem with the underlying stream, or
     *             it runs out of data before the string being searched for is
     *             read.
     */
    public static void readUntil(InputStream in, String needle)
        throws IOException
    {
        readUntil(in, needle.getBytes());
    }

    /**
     * Read a stream until a specific string is encountered. Returns all bytes
     * read, except for the byte string being searched for. This will only work
     * properly for ASCII data.
     * 
     * @param in The stream to read.
     * @param needle The string to search for.
     * @throws IOException If there is a problem with the underlying stream, or
     *             it runs out of data before the string being searched for is
     *             read.
     */
    public static String returnUntil(InputStream in, String needle)
        throws IOException
    {
        byte[] match = needle.getBytes();
        int offset = 0;
        StringBuilder ans = new StringBuilder();

        while (offset < match.length) {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            int b = in.read();
            if (b == -1) {
                throw new IOException("Unexpected end of stream");
            }

            if (((byte)b & 0xff) == match[offset]) {
                offset++;
            }
            else {
                offset = 0;
            }

            ans.append((char)b);
        }

        // Cut off the needle from the end of the string we built
        ans.setLength(ans.length() - needle.length());
        return ans.toString();
    }

    /**
     * Read a stream until the end is reached. Discards all bytes read.
     * 
     * @param in The stream to read.
     * @throws IOException If there is a problem with the underlying stream.
     */
    public static void readToEOF(InputStream in)
        throws IOException
    {
        // Skip large chunks and then make sure we haven't hit EOF yet
        while (in.skip(1024) > -1 && in.read() != -1) {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }
        }
    }

    /**
     * Read a stream until the end is reached. Returns all bytes read.
     * 
     * @param in The stream to read.
     * @throws IOException If there is a problem with the underlying stream.
     */
    public static byte[] bytesToEOF(InputStream in)
        throws IOException
    {
        List<byte[]> blocks = new LinkedList<byte[]>();
        byte[] block = new byte[1024];
        int readBytes, totalBytes = 0;

        while ((readBytes = in.read(block)) != -1) {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            if (readBytes < block.length) {
                byte[] temp = new byte[readBytes];
                System.arraycopy(block, 0, temp, 0, readBytes);
                blocks.add(temp);
            }
            else {
                blocks.add(block);
                block = new byte[block.length];
            }

            totalBytes += readBytes;
        }

        byte[] ans = new byte[totalBytes];
        int offset = 0;
        for (byte[] thisblock : blocks) {
            System.arraycopy(thisblock, 0, ans, offset, thisblock.length);
            offset += thisblock.length;
        }

        return ans;
    }

    /**
     * Read a stream until until the end is reached. Returns all data read as a
     * string.
     * 
     * @param in The stream to read.
     * @throws IOException If there is a problem with the underlying stream.
     */
    public static String returnToEOF(InputStream in)
        throws IOException
    {
        StringBuilder result = new StringBuilder();
        byte[] data = new byte[1024];
        int bytesRead;
        while ((bytesRead = in.read(data)) > -1) {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            result.append(new String(data, 0, bytesRead));
        }

        return result.toString();
    }

    /**
     * Convert an iterator into an iterator whose 'remove' method fails.
     * 
     * @param it The iterator to convert.
     * @return An iterator over the same data, but whose 'remove' method will
     *         throw an UnsupportedOperationException.
     */
    public static <E> Iterator<E> readOnlyIterator(final Iterator<E> it)
    {
        return new Iterator<E>() {
            public boolean hasNext()
            {
                return it.hasNext();
            }

            public E next()
            {
                return it.next();
            }

            public void remove()
            {
                throw new UnsupportedOperationException(
                    "Cannot remove objects from this iterator");
            }
        };
    }

    /**
     * Perform a rot13 operation on a string, including modifying numeric
     * characters.
     * 
     * @param str The string to rot13.
     * @return A string where every letter has been rotated 13 places around the
     *         alphabet and every numeric character has been rotated 5 places
     *         around the digits (0-9). Case is preserved. The original string
     *         can be obtained by calling rot13 on the rotated string.
     */
    public static String rot13(String str)
    {
        char[] chars = str.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            if ('0' <= chars[i] && chars[i] <= '9') {
                int val = chars[i] - '0';
                val = 9 - val;
                chars[i] = (char)(val + '0');
            }
            else if ('a' <= chars[i] && chars[i] <= 'z') {
                int val = chars[i] - 'a';
                val = 25 - val;
                chars[i] = (char)(val + 'a');
            }
            else if ('A' <= chars[i] && chars[i] <= 'Z') {
                int val = chars[i] - 'A';
                val = 25 - val;
                chars[i] = (char)(val + 'A');
            }
            else {
                chars[i] = chars[i]; // No change
            }
        }

        return new String(chars);
    }

    /**
     * Create a Vector that contains the objects in the provided array.
     * 
     * @param array The array to convert to a vector.
     * @return A vector containing, in the same order, the objects in the array.
     */
    public static <E> Vector<E> makeVector(E... array)
    {
        Vector<E> vec = new Vector<E>(array.length);

        for (E e : array) {
            vec.add(e);
        }

        return vec;
    }

    /**
     * Create a LinkedList that contains the objects in the provided array.
     * 
     * @param array The array to convert to a linked list.
     * @return A linked list containing, in the same order, the objects in the
     *         array.
     */
    public static <E> LinkedList<E> makeLinkedList(E... array)
    {
        LinkedList<E> list = new LinkedList<E>();

        for (E e : array) {
            list.add(e);
        }

        return list;
    }

    /**
     * Create a HashSet that contains the objects in the provided array.
     * 
     * @param array The array to convert to a HashSet.
     * @return A HashSet containing the objects in the array.
     */
    public static <E> HashSet<E> makeHashSet(E... array)
    {
        HashSet<E> set = new HashSet<E>(array.length);

        for (E e : array) {
            set.add(e);
        }

        return set;
    }

    /**
     * Create an ArrayList that contains the objects in the provided array.
     * 
     * @param array The array to convert to an ArrayList.
     * @return An ArrayList containing, in the same order, the objects in the
     *         array.
     */
    public static <E> ArrayList<E> makeArrayList(E... array)
    {
        ArrayList<E> list = new ArrayList<E>(array.length);

        for (E e : array) {
            list.add(e);
        }

        return list;
    }

    /**
     * Split a string into lines.
     * 
     * @param text The string to split.
     * @return An array of lines from that string. It is split on '\n'
     *         characters, with '\r' characters immediately preceding the '\n'
     *         characters removed. Blank lines are preserved.
     */
    public static String[] getLines(String text)
    {
        // By keeping the tokens as well as the lines, we can add empty lines
        // when they appear
        // Otherwise, all empty lines would disappear
        StringTokenizer tok = new StringTokenizer(text, "\n", true);

        // Set to true if the previous line was not just a delimiter
        // If it was a full line, then we can discard the next delimiter
        // If it was not a full line (it was just a delimiter), then if the
        // current char is just a delimiter also then we need to record a blank
        // line
        boolean fullLine = false;

        Vector<String> lines = new Vector<String>();

        while (tok.hasMoreTokens()) {
            String line = tok.nextToken();
            if (line.endsWith("\r")) {
                line = line.substring(0, line.length() - 1);
            }

            // If it was just a '\r', things will still work properly (line is
            // now "", so it goes to the 'else' clause and is added as "", as
            // a full line)

            if (line.equals("\n")) {
                if (!fullLine) {
                    lines.add("");
                }

                fullLine = false;
            }
            else {
                lines.add(line);
                fullLine = true;
            }
        }

        return lines.toArray(new String[0]);
    }

    /**
     * Given an array of lines, combine it into a single string with '\n' as the
     * line separator.
     * 
     * @param lines The array of lines to combine.
     * @return A string with the lines separated with '\n' characters, including
     *         one at the end.
     */
    public static String combineLines(String[] lines)
    {
        return combineStrings(lines, "\n");
    }

    /**
     * Given an array of lines, combine it into a single string with the native
     * line separator between them (System.getProperty("line.separator")).
     * 
     * @param lines The lines to combine.
     * @return A string with the lines separated with the native separator, with
     *         one at the end.
     */
    public static String combineLinesNatively(String[] lines)
    {
        return combineStrings(lines, System.getProperty("line.separator"));
    }

    /**
     * Given an array of strings, combine it into a single string with the
     * specified separator between them.
     * 
     * @param strings The strings to combine.
     * @param lineSep The separator to place between the strings.
     * @return The combined string, with a separator at the end.
     */
    public static String combineStrings(String[] strings, String lineSep)
    {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < strings.length; i++) {
            builder.append(strings[i]);
            builder.append(lineSep);
        }

        return builder.toString();
    }

    /**
     * Get an MD5 digester.
     */
    public static MessageDigest getMD5Digester()
    {
        try {
            return MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException(nsae);
        }
    }

    /**
     * Compute an MD5 sum over a byte array.
     * 
     * @param data The byte array to sum.
     * @return An MD5 sum.
     */
    public static String md5sum(byte[] data)
    {
        byte[] md5 = getMD5Digester().digest(data);
        return Conversion.bytesToHex(md5);
    }

    /**
     * Get a SHA1 digester.
     */
    public static MessageDigest getSHA1Digester()
    {
        try {
            return MessageDigest.getInstance("SHA1");
        }
        catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException(nsae);
        }
    }

    /**
     * Compute a SHA1 sum over a byte array.
     * 
     * @param data The byte array to sum.
     * @return A SHA1 sum.
     */
    public static String sha1sum(byte[] data)
    {
        byte[] sha1 = getSHA1Digester().digest(data);
        return Conversion.bytesToHex(sha1);
    }

    /**
     * Get a SHA-256 digester.
     */
    public static MessageDigest getSHA256Digester()
    {
        try {
            return MessageDigest.getInstance("SHA-256");
        }
        catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException(nsae);
        }
    }

    /**
     * Compute a SHA-256 sum over a byte array.
     * 
     * @param data The byte array to sum.
     * @return A SHA-256 sum.
     */
    public static String sha256sum(byte[] data)
    {
        byte[] sha256 = getSHA256Digester().digest(data);
        return Conversion.bytesToHex(sha256);
    }

    /**
     * Compute a CRC32 checksum over the specified data.
     * 
     * @param data The data to sum.
     * @param off The starting offset in the data array.
     * @param len The number of bytes to sum.
     * @return The CRC32 checksum.
     */
    public static int crc32(byte[] data, int off, int len)
    {
        CRC32 crc = new CRC32();
        crc.update(data, off, len);
        return (int)(crc.getValue() & 0xffffffff);
    }

    /**
     * Parse a string into a long.
     * 
     * @param val A string, which may begin with '0x' to be parsed as hex, or
     *            '0' to be parsed as octal; will be parsed as decimal
     *            otherwise.
     * @return A long containing the number in the string.
     * @throws NumberFormatException If the number is not formed properly.
     */
    public static long parseLong(String val)
    {
        if (val.startsWith("0x") || val.startsWith("0X")) {
            return Long.parseLong(val.substring(2), 16); // Gotta get rid of the
            // 0x
        }
        else if (val.startsWith("0")) {
            return Long.parseLong(val, 8); // The 0 on the front won't affect
            // parsing
        }
        else {
            return Long.parseLong(val, 10);
        }
    }

    /**
     * Parse a string into an int.
     * 
     * @param val A string, which may begin with '0x' to be parsed as hex, or
     *            '0' to be parsed as octal; will be parsed as decimal
     *            otherwise.
     * @return An int containing the number in the string.
     * @throws NumberFormatException If the number is not formed properly.
     */
    public static int parseInt(String val)
    {
        if (val.startsWith("0x") || val.startsWith("0X")) {
            return Integer.parseInt(val.substring(2), 16); // Gotta get rid of
            // the 0x
        }
        else if (val.startsWith("0")) {
            return Integer.parseInt(val, 8); // The 0 on the front won't affect
            // parsing
        }
        else {
            return Integer.parseInt(val, 10);
        }
    }

    /**
     * Convert a path from either Windows or Unix-like to the style of the
     * running OS.
     * 
     * @param path The path to convert. MUST be an absolute path.
     * @return A native path. If on a Windows system, and the path begins with
     *         '/', the initial directory name is treated as a drive letter and
     *         the rest as path components, separated by '\' chars. If not on
     *         Windows, and the path begins with matches '[a-zA-Z]:\\.*', the
     *         drive letter is treated as the first directory off off the root,
     *         and the rest of the path as normal subdirectories, using '/'
     *         chars to separate.
     */
    public static String correctPathToOS(String path)
    {
        if (osIsWindows()) {
            // If the path is Unix-style, convert to Windows-style
            // Assume the first letter in "/<letter>/" is a drive letter
            if (path.startsWith("/")) {
                StringBuilder winPath = new StringBuilder();
                winPath.append(path.charAt(1));
                winPath.append(":\\");

                for (int i = 3; i < path.length(); i++) {
                    char c = path.charAt(i);

                    if (c == '/') {
                        winPath.append("\\");
                    }
                    else {
                        winPath.append(c);
                    }
                }

                path = winPath.toString();
            }
        }
        else {
            // If the path is Windows-style, convert to Unix-style
            // Assume there is a root folder with the same name as the drive
            // letter, lower-case
            if (path.matches("^[a-zA-Z]:\\\\.*")) {
                StringBuilder linPath = new StringBuilder();
                linPath.append("/");
                linPath.append(Character.toLowerCase(path.charAt(0)));
                linPath.append("/");

                for (int i = 3; i < path.length(); i++) {
                    char c = path.charAt(i);

                    if (c == '\\') {
                        linPath.append("/");
                    }
                    else {
                        linPath.append(c);
                    }
                }

                path = linPath.toString();
            }
        }

        return path;
    }

    /**
     * Check whether the given data array is binary.
     * 
     * @param data The data array.
     * @return True if the first 1024 bytes contains a 0-byte, false otherwise.
     */
    public static boolean isBinaryData(byte[] data)
    {
        // Search the first 1K for a 0-byte
        for (int i = 0; i < 1024 && i < data.length; i++) {
            if (data[i] == 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Parse a string as XML.
     * 
     * @param string The string to parse.
     * @param preserveSpaces Whether to preserve leading/trailing spaces in
     *            blocks of text between tags.
     * @return The XML contained in the string.
     * @throws IOException If there was a parse error.
     */
    public static Tag parseXML(String string, boolean preserveSpaces)
        throws IOException
    {
        return xmlParser.parseXML(new ByteArrayInputStream(string.getBytes()),
            false, preserveSpaces);
    }

    /**
     * Convert XML data into a stream.
     * 
     * @param tag The XML data.
     * @param exceptionHandler If not <tt>null</tt>, will be used to handle any
     *            exceptions thrown by the thread used to feed the
     *            PipedOutputStream side.
     * @return A PipedInputStream that can be read for the XML data.
     * @throws IOException If there is a problem with the piped streams.
     */
    public static InputStream createXMLInputStream(
                                                   final Tag tag,
                                                   Thread.UncaughtExceptionHandler exceptionHandler)
        throws IOException
    {
        final PipedOutputStream pos = new PipedOutputStream();

        PipedInputStream pis;

        pis = new PipedInputStream(pos);

        Thread th = new Thread(new Runnable() {
            public void run()
            {
                try {
                    Utils.xmlParser.writeXMLStream(tag, pos);
                }
                catch (IOException ioe) {
                    try {
                        pos.close();
                    }
                    catch (IOException e) {
                        // Ignore this error, it's a piped stream; the other end
                        // must know it's closed now
                    }

                    // Throw the exception back to whatever wants to listen
                    Thread.currentThread().getUncaughtExceptionHandler()
                        .uncaughtException(Thread.currentThread(), ioe);
                }
            }
        });

        if (exceptionHandler != null) {
            th.setUncaughtExceptionHandler(exceptionHandler);
        }

        th.start();

        return pis;
    }

    /**
     * Stick the given collection of strings together into a single string,
     * using the provided separator string between them.
     * 
     * @param collection The collection to mash.
     * @param sep The separator string.
     * @return A concatenation of all of the items in the collection, separated
     *         by the given separator string.
     */
    public static String makeString(Collection<String> collection, String sep)
    {
        StringBuilder builder = new StringBuilder();

        synchronized (collection) {
            for (String s : collection) {
                if (builder.length() > 0) {
                    builder.append(":");
                }
                builder.append(s);
            }
        }

        return builder.toString();
    }

    /**
     * Make a long string out of a character by repeating it a number of times.
     * 
     * @param ch The character to repeat.
     * @param times The number of times to repeat it.
     * @return A string consisting of the provided character, repeated the
     *         specified number of times.
     */
    public static String multiplyChar(char ch, int times)
    {
        char[] chars = new char[times];
        Arrays.fill(chars, ch);
        return new String(chars);
    }

    /**
     * Parse a string on a given separator string, handling all three quote
     * types sensibly.
     * 
     * @param s The string to parse.
     * @param sep The separator string. Behavior is undefined if the separator
     *            contains a single or double quote or a backtick.
     * @param trim True = each token will be trimmed. False = tokens will be
     *            saved as they are found, including spaces between them and the
     *            separator string.
     * @return An array of tokens, or <tt>null</tt> if <tt>s == null</tt>.
     *         Separators and quotes are not included, but blank strings between
     *         separators are. An unfortunate side effect (although it is a
     *         corner case) is that if two quoted strings appear next to each
     *         other, the quotes will be stripped like this: "Hello" "Hi" -->
     *         [0] = Hello Hi
     * 
     * @throws IllegalStateException If quotes are not closed by the end of the
     *             string.
     */
    public static String[] tokenize(String s, String sep, boolean trim)
    {
        if (s == null) {
            return null;
        }
        if (s.length() == 0) {
            return new String[0];
        }

        List<String> result = new LinkedList<String>();
        StringBuilder current = new StringBuilder();
        char quoteChar = 0;
        char[] chars = s.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];

            if (quoteChar == 0) {
                // Not in a quoted portion of the string
                if (c == '\'' || c == '"' || c == '`') {
                    // But now we are...
                    quoteChar = c;
                }
                else {
                    // Not a quote, are we on a separator?
                    if (s.substring(i, i + sep.length()).equals(sep)) {
                        // Yes
                        // Advance to the next token
                        i += sep.length() - 1; // That 1 will be added on the
                        // next iteration

                        if (trim) {
                            result.add(current.toString().trim());
                        }
                        else {
                            result.add(current.toString());
                        }

                        // Clear the current token
                        current.setLength(0);
                    }
                    else {
                        // Nope, just add this character to the current token
                        current.append(c);
                    }
                }
            }
            else {
                // We're in the middle of a quoted section
                if (c == quoteChar) {
                    // This is the end of it, though
                    quoteChar = 0;
                }
                else {
                    current.append(c);
                }
            }
        }

        if (quoteChar != 0) {
            throw new IllegalStateException("Unclosed quote: " + quoteChar);
        }

        // It may be blank, but it still needs to be added
        if (trim) {
            result.add(current.toString().trim());
        }
        else {
            result.add(current.toString());
        }

        return result.toArray(new String[result.size()]);
    }

    /**
     * Sorts a collection of strings alphabetically.
     * 
     * @param strings The collection to sort.
     * @return A list of sorted strings.
     */
    public static List<String> sortStrings(Collection<String> strings)
    {
        String[] array;
        synchronized (strings) {
            array = strings.toArray(new String[strings.size()]);
        }

        Arrays.sort(array);

        return makeLinkedList(array);
    }

    /**
     * Parse a date/time in a format like "2009-02-05 9:56:23" (24-hour time,
     * leading 0s are not required).
     * 
     * @param d The date/time to parse.
     * @return A Date object representing the given date/time.
     * @throws ParseException If the string could not be parsed.
     */
    public static Date parseStandardDateTime(String d)
        throws ParseException
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.parse(d);
    }

    /**
     * Parse a date/time in a format like "2009-02-05 9:56:23" (24-hour time,
     * leading 0s are not required) as a UTC time (rather than local time).
     * 
     * @param d The date/time to parse.
     * @return A Date object representing the given date/time.
     * @throws ParseException If the string could not be parsed.
     */
    public static Date parseTZDateTime(String d)
        throws ParseException
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        return df.parse(d);
    }

    /**
     * Format a date into a string like "2009-02-05 09:56:23" (24-hour time,
     * leading 0s will be applied).
     * 
     * @param d The date to format.
     * @param includeTimezone True = include a timezone indicator for a result
     *            like "2009-02-05 09:56:23 EDT", false = do not include the
     *            timezone indicator.
     * @return A string representation of that date, or "" if <tt>d == null</tt>
     *         .
     */
    public static String formatStandardDateTime(Date d, boolean includeTimezone)
    {
        if (d == null) {
            return "";
        }

        DateFormat df;
        if (includeTimezone) {
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        }
        else {
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }

        return df.format(d);
    }

    /**
     * Parse a date in a format like "2009-02-05" (leading 0s are not required).
     * 
     * @param d The date to parse.
     * @return A Date object representing the given date.
     * @throws ParseException If the string could not be parsed.
     */
    public static Date parseStandardDate(String d)
        throws ParseException
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.parse(d);
    }

    /**
     * Format a date into a string like "2009-02-05".
     * 
     * @param d The date to format.
     * @return A string representation of that date, or "" if <tt>d == null</tt>
     *         .
     */
    public static String formatStandardDate(Date d)
    {
        if (d == null) {
            return "";
        }

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(d);
    }

    /**
     * Parse a time in a format like "9:56:23" (24-hour time, leading 0s are not
     * required).
     * 
     * @param d The time to parse.
     * @return A Date object representing the given time.
     * @throws ParseException If the string could not be parsed.
     */
    public static Date parseStandardTime(String d)
        throws ParseException
    {
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        return df.parse(d);
    }

    /**
     * Format a date into a string like "09:56:23" (24-hour time, leading 0s
     * will be applied).
     * 
     * @param d The date to format.
     * @return A string representation of the time included in that date, or ""
     *         if <tt>d == null</tt>.
     */
    public static String formatStandardTime(Date d)
    {
        if (d == null) {
            return "";
        }

        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        return df.format(d);
    }

    /**
     * Get the first file in a list of paths that exists.
     * 
     * @param files A list of file paths.
     * @return The first path for which File.isFile() returns true. Returns
     *         <tt>null</tt> if none exist.
     */
    public static String getFirstExistingFile(String... files)
    {
        for (String s : files) {
            try {
                File f = new File(s);

                if (f.isFile()) {
                    return s;
                }
            }
            catch (Exception e) {
                // Ignore it
            }
        }

        return null;
    }

    /**
     * Format a date into a string like "5 days 4 hours 1 minute 2 seconds".
     * Pieces may be left off of the left side as they are not needed, but every
     * piece to the right of the first will be included. A minimal string would
     * be "1 second". Another example that could be returned is
     * "1 hour 0 minutes 8 seconds".
     * 
     * @param d The date to format.
     * @return A string representation of that date, or "" if <tt>d == null</tt>
     *         .
     */
    public static String formatTimeDurationLong(Date d)
    {
        if (d == null) {
            return "";
        }

        long ms = d.getTime();

        long seconds = ms / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        seconds -= minutes * 60;
        minutes -= hours * 60;
        hours -= days * 24;

        StringBuilder duration = new StringBuilder();

        if (days > 0) {
            duration.append(String.format("%d day", days));
            if (days != 1) {
                duration.append("s");
            }
            duration.append(" ");
        }

        if (hours > 0 || days > 0) {
            duration.append(String.format("%d hour", hours));
            if (hours != 1) {
                duration.append("s");
            }
            duration.append(" ");
        }

        if (minutes > 0 || hours > 0 || days > 0) {
            duration.append(String.format("%d minute", minutes));
            if (minutes != 1) {
                duration.append("s");
            }
            duration.append(" ");
        }

        if (seconds > 0 || minutes > 0 || hours > 0 || days > 0) {
            duration.append(String.format("%d second", seconds));
            if (seconds != 1) {
                duration.append("s");
            }
        }

        return duration.toString();
    }

    /**
     * Formats a date into a string like "153:22:56". A minimal return value
     * would be "00:00:01".
     * 
     * @param d The date to format.
     * @return A string representation of that date, or "" if <tt>d == null</tt>
     *         .
     */
    public static String formatTimeDurationShort(Date d)
    {
        if (d == null) {
            return "";
        }

        long ms = d.getTime();

        long seconds = ms / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;

        seconds -= minutes * 60;
        minutes -= hours * 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    /**
     * Format a stack trace as a string. Each line will be prefixed by a tab
     * character (as is done for standard Java stack traces).
     * 
     * @param st The stack trace, most recent call first.
     * @return The stack trace, formatted as a string with one entry per line,
     *         prefixed by a tab character.
     */
    public static String formatStackTrace(StackTraceElement[] st)
    {
        StringBuilder buf = new StringBuilder();
        for (StackTraceElement ste : st) {
            buf.append("\tat ").append(ste.toString()).append("\n");
        }

        return buf.toString();
    }

    /**
     * Run through a list of ClassLoader objects, asking each for the given
     * resource until it is found.
     * 
     * @param resourceName The name (path) of the resource.
     * @param defaultClassLoader The first ClassLoader to check, followed by
     *            thread context class loader, the Runtime class loader, and
     *            finally the System class loader.
     * @return The URL of the resource from the first ClassLoader that can
     *         provide it, or <tt>null</tt> if none are able to find it.
     */
    public static URL findResource(String resourceName,
                                   ClassLoader defaultClassLoader)
    {
        ClassLoader[] classLoaders =
            new ClassLoader[] {
                defaultClassLoader,
                Thread.currentThread().getContextClassLoader(),
                Runtime.getRuntime().getClass().getClassLoader(),
                ClassLoader.getSystemClassLoader()
            };

        for (ClassLoader classLoader : classLoaders) {
            if (classLoader == null) {
                continue;
            }

            URL url = classLoader.getResource(resourceName);

            if (url != null) {
                return url;
            }
        }

        return null;
    }

    /**
     * Select a slice from a collection.
     * 
     * @param values The collection to select from. This is not modified.
     * @param start The starting index to select.
     * @param len The number of values to select.
     * @return A list of at most <tt>len</tt> values from <tt>values</tt>,
     *         starting at <tt>start</tt>. If the passed-in data is not large
     *         enough, the result will be smaller than <tt>len</tt>.
     */
    public static <E> List<E> slice(Collection<E> values, int start, int len)
    {
        List<E> valueSlice = new ArrayList<E>(len);

        int index = 0;
        for (Iterator<E> itValues = values.iterator(); itValues.hasNext() &&
                                                       index < start + len; index++) {

            E value = itValues.next();

            if (start <= index) {
                valueSlice.add(value);
            }
        }

        return valueSlice;
    }

    /**
     * Get the name of the enclosing class of the calling function. Useful for
     * constructing Log4j loggers.
     * 
     * @return The full (binary) name of the enclosing class of the calling
     *         function, or Object's class if the class could not be determined.
     */
    public static String thisClassName()
    {
        Throwable th = new Throwable();
        th.fillInStackTrace();

        StackTraceElement[] st = th.getStackTrace();

        try {
            return st[1].getClassName();
        }
        catch (ArrayIndexOutOfBoundsException aioobe) {
            return Object.class.getName();
        }
    }

    /**
     * Read an entire file into a byte array.
     * 
     * @param file The file to read.
     * @return The contents of the file.
     * @throws IOException If the file is too large, or if there is a problem
     *             reading from it.
     */
    public static byte[] readFile(File file)
        throws IOException
    {
        if (file.length() > Integer.MAX_VALUE) {
            throw new IOException("File is too large");
        }

        byte[] data = new byte[(int)file.length()];

        InputStream in = new FileInputStream(file);
        try {
            int off = 0;
            while (off < data.length) {
                int read = in.read(data, off, data.length - off);

                if (read == -1) {
                    throw new IOException("Unexpected end of file");
                }

                off += read;
            }
        }
        finally {
            in.close();
        }

        return data;
    }
}

package com.sixnetio.util;

import org.apache.log4j.Logger;

/**
 * Monitors a given thread for activity. Requires the thread to check-in on a
 * regular basis. If it does not check in on schedule, executes a callback to
 * somehow interrupt/signal/kill the thread.
 */
public class ThreadMonitor
    extends Thread
{
    private static final Logger log = Logger.getLogger(Utils.thisClassName());

    private final Thread thread;
    private final long timeout;
    private final Runnable signaler;

    private boolean signal;
    private final Object l_signal = new Object();

    private boolean running;
    private long nextCheckinBy;

    /**
     * Construct a new ThreadMonitor. Sets up as a daemon thread, but does not
     * start. You should start this thread AFTER starting the target thread.
     * 
     * @param _thread The thread to monitor. Must not be <code>null</code>.
     * @param _timeout The timeout, in milliseconds. The {@link #checkin()}
     *            method must be called within this amount of time (from thread
     *            start) to prevent the signaler to fire and this thread to die.
     * @param _signaler The signaler to call if the {@link #checkin()} method is
     *            not called in a timely fashion.
     */
    public ThreadMonitor(final Thread _thread, final long _timeout,
                         final Runnable _signaler)
    {
        super("Monitor(" + _thread.getName() + ")");

        if (_timeout <= 0) {
            throw new IllegalArgumentException(
                "Timeout must be greater than 0.");
        }

        if (_signaler == null) {
            throw new NullPointerException("_signaler");
        }

        thread = _thread;
        timeout = _timeout;
        signaler = _signaler;

        setDaemon(true);
    }

    @Override
    public synchronized void start()
    {
        if (!Thread.State.NEW.equals(getState())) {
            // Catch this BEFORE making changes
            throw new IllegalThreadStateException();
        }

        running = true;
        signal = false;
        nextCheckinBy = System.currentTimeMillis() + timeout;

        super.start();
    }

    /**
     * Check-in with this thread monitor, preventing it from signaling its
     * thread for another timeout period.
     */
    public void checkin()
    {
        synchronized (l_signal) {
            signal = true;
            l_signal.notify();
        }
    }

    /**
     * Stop this ThreadMonitor.
     */
    public void halt()
    {
        synchronized (l_signal) {
            running = false;
            l_signal.notify();
        }
    }

    @Override
    public void run()
    {
        while (running) {
            long delay = System.currentTimeMillis() - nextCheckinBy;
            if (delay <= 0) {
                delay = 1;
            }

            boolean failure = false;
            synchronized (l_signal) {
                if (!signal) {
                    try {
                        l_signal.wait(delay);
                    }
                    catch (InterruptedException ie) {
                        // Ignore it
                    }
                }

                // This needs to be synchronized to prevent missing a signal
                // before we set it to false
                final long now = System.currentTimeMillis();
                if (signal) {
                    nextCheckinBy = now + timeout;
                    signal = false;
                }
                else if (nextCheckinBy <= now) {
                    failure = true;
                }
            }

            if (failure) {
                String trace = Utils.formatStackTrace(thread.getStackTrace());

                log.warn("ThreadMonitor signaling counterpart; stack trace:\n" +
                         trace);
                signaler.run();
                halt();
            }
        }
    }
}

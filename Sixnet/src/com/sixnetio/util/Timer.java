/*
 * Timer.java
 *
 * Provides a regular timed tick event to interested listeners.
 *
 * Jonathan Pearson
 * September 19, 2008
 *
 */

package com.sixnetio.util;

import java.util.*;

public class Timer extends Thread {
	public interface TickListener {
		public void doTick(long elapsedMS);
	}
	
	private final long interval;
	private boolean running = false;
	private boolean stopped = true;
	
	private List<TickListener> tickListeners = new LinkedList<TickListener>();
	
	public Timer(long intervalMS) {
		this.interval = intervalMS;
		
		setDaemon(true);
	}
	
	public void addTickListener(TickListener listener) {
		synchronized (tickListeners) {
	        tickListeners.add(listener);
        }
	}
	
	public void removeTickListener(TickListener listener) {
		synchronized (tickListeners) {
	        tickListeners.remove(listener);
        }
	}
	
	protected void doTick(long elapsedMS) {
		List<TickListener> tickListeners;
		synchronized (this.tickListeners) {
			tickListeners = new ArrayList<TickListener>(this.tickListeners);
		}
		
		for (TickListener listener : tickListeners) {
			try {
				listener.doTick(elapsedMS);
			} catch (Throwable th) {
				// Just in case, to make sure everybody else is notified
				Utils.debug(th);
			}
		}
	}
	
	@Override
    public void run() {
		stopped = false;
		
		long startTime = System.currentTimeMillis();
		while (running) {
			Utils.sleep(interval);
			long stopTime = System.currentTimeMillis();
			
			// FUTURE: If there are multiple listeners, any of them taking too long here could
			//   cause inaccuracies for the others waiting for the next tick
			doTick(stopTime - startTime);
			
			startTime = stopTime;
		}
		
		stopped = true;
	}
	
	@Override
    public void start() {
		running = true;
		super.start();
	}
	
	public void halt() {
		running = false;
		
		while (!stopped) {
			try {
				join();
			} catch (InterruptedException ie) { }
		}
	}
}

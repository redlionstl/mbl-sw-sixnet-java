/*
 * JarClassLoader.java
 *
 * Loads classes from a JAR file.
 *
 * Jonathan Pearson
 * February 21, 2008
 *
 */

package com.sixnetio.util;

import java.io.IOException;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.apache.log4j.Logger;

/**
 * This class provides class loading capabilities for individual JAR
 * files. To use it, construct it with an input stream that will provide
 * the JAR data. This will read all classes within the JAR into memory,
 * but will only load them into the JVM as they are requested.
 *
 * @author Jonathan Pearson
 */
public class JarClassLoader extends ClassLoader {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	private static class ClassEntry {
		public byte[] data;
		public Class<?> cached;
	}
	
	// Read-only after construction
	private Map<String, ClassEntry> entries;
	
	/**
	 * Construct a new JarClassLoader that reads a JAR file from the given
	 * input stream.
	 * 
	 * @param jin The input stream to read the JAR data from.
	 * @throws IOException If there is a problem reading the input stream.
	 */
	public JarClassLoader(JarInputStream jin) throws IOException {
		this(JarClassLoader.class.getClassLoader(), jin);
	}
	
	/**
	 * Construct a new JarClassLoader with the given parent that reads
	 * JAR data from the given input stream.
	 * 
	 * @param parent The parent class loader.
	 * @param jin The input stream to read the JAR data from.
	 * @throws IOException If there is a problem reading the input stream.
	 */
	public JarClassLoader(ClassLoader parent, JarInputStream jin) throws IOException {
		super(parent);
		
		entries = new Hashtable<String, ClassEntry>();
		
		logger.debug("Scanning JAR for classes");
		
		JarEntry entry;
		while ((entry = jin.getNextJarEntry()) != null) {
			if (entry.getName().toLowerCase().endsWith(".class")) {
				logger.debug(String.format("'%s' is a class", entry.getName()));
				
				// Get the path within the JAR to the class
				String name = entry.getName();
				
				// Replace '/' with '.' to make a binary name
				name = name.replaceAll("\\/", ".");
				
				// Remove the '.class' at the end
				name = name.substring(0, name.length() - ".class".length());
				
				// Read the class data out of the JAR into a ClassEntry
				ClassEntry ce = new ClassEntry();
				ce.data = Utils.bytesToEOF(jin);
				
				logger.debug(String.format("Putting class with name '%s' (originally '%s') into map",
				                           name, entry.getName()));
				
				entries.put(name, ce);
			}
		}
	}
	
	/**
	 * Get the set of binary class names that are provided by this class loader.
	 */
	public Set<String> getClassNames() {
		return new HashSet<String>(entries.keySet());
	}
	
	/**
	 * Given a binary class name, load and return a new class for it loaded from the JAR.
	 * @param className The binary name of the class.
	 * @throws ClassNotFoundException If the class could not be found in the JAR.
	 */
	@Override
	protected Class<?> findClass(String className) throws ClassNotFoundException {
		Class<?> result = null;
		ClassEntry ce = entries.get(className);
		
		logger.debug("Trying to find class '" + className + "'");
		
		if (ce != null) {
			if (ce.cached != null) {
				logger.debug("Result '" + className + "' was cached, returning that");
				
				result = ce.cached;
			} else {
				logger.debug("Found class '" + className + "', trying to define it");
				
    			result = defineClass(className, ce.data, 0, ce.data.length);
    			if (result != null) {
        			// Cache it
    				logger.debug("Class '" + className + "' defined, caching it");
    				
        			ce.cached = result;
        			ce.data = null; // No need for this any more, let it be reclaimed
    			} else {
    				logger.warn("Unable to define class '" + className + "', bad binary data?");
    				
    				throw new ClassNotFoundException("Unable to define class '" + className + "'");
    			}
			}
		} else {
			logger.warn("Unable to locate class '" + className + "' in local store");
			
			throw new ClassNotFoundException("Unable to find class '" + className + "'");
		}
		
		return result;
	}
}

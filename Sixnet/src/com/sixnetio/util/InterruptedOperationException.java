/*
 * InterruptedOperationException.java
 *
 * Thrown when a thread is interrupted during an operation. This is separate from InterruptedException
 * because you may not always want to declare/catch this exception, perhaps because it is related to
 * an operation that you never call Thread.interrupt() on.
 *
 * Jonathan Pearson
 * October 27, 2008
 *
 */

package com.sixnetio.util;

public class InterruptedOperationException extends RuntimeException {
	public InterruptedOperationException() { }
	
	public InterruptedOperationException(String message) {
		super(message);
	}
	
	public InterruptedOperationException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public InterruptedOperationException(Throwable cause) {
		super(cause);
	}
}

/*
 * UserInterface.java
 *
 * An interface that should be implemented to provide feedback from a non-UI class.
 *
 * Jonathan Pearson
 * January 9, 2008
 *
 */

package com.sixnetio.util;

public interface UserInterface {
	// Message importance
	public static final int M_NORMAL = 10,
	                        M_WARNING = 20,
	                        M_ERROR = 30;
	
	public void displayMessage(Object from, int importance, String msg);
	public void updateLastMessage(Object from, int importance, String msg); // Changes the previous message
	
	// In case an exception is thrown
	public void handleException(Object from, Exception e);
	
	// One of these will be called when the operation is done, but not both
	public void operationCompleted(Object from);
	public void operationFailed(Object from);
	
	// Request confirmation
	public boolean confirm(String msg, String title);
	
	// Request input
	public String requestString(String msg, String title, String defaultVal);
	
	// Store/Load persistent values
	// These are optional; you may simply return 'null' for get and do nothing for 'set'
	public String getPersistentValue(String name);
	public void setPersistentValue(String name, String value);
}

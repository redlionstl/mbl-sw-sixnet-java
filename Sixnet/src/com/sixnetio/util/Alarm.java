/*
 * Alarm.java
 *
 * Provides an alarm-clock interrupt signal for any number of interested
 * threads.
 *
 * Jonathan Pearson
 * September 29, 2009
 *
 */

package com.sixnetio.util;

import java.util.*;

import org.apache.log4j.Logger;

/**
 * Allows threads to set alarms for specific times. An alarm "goes off" by
 * calling either calling {@link Thread#interrupt()} on a thread, or by calling
 * {@link Object#notify()} on an object.
 *
 * @author Jonathan Pearson
 */
public class Alarm
	extends Thread
{
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/** Alarm is a singleton, this is the object. */
	private static final Alarm theAlarm;
	
	static
	{
		theAlarm = new Alarm();
		theAlarm.setDaemon(true);
		theAlarm.start();
	}
	
	/**
	 * Specifies which type of alarm each set point represents.
	 *
	 * @author Jonathan Pearson
	 */
	private static enum AlarmType
	{
		/** A set point with this alarm type will interrupt a thread. */
		ThreadInterrupt,
		
		/** A set point with this alarm type will notify an object. */
		ObjectNotify,
		;
	}
	
	/**
	 * Represents an alarm set point.
	 *
	 * @author Jonathan Pearson
	 */
	private static class SetPoint
		implements Comparable<SetPoint>
	{
		/** The time at which this set point occurs. */
		public Date time;
		
		/**
		 * If {@link #type} is {@link AlarmType#ThreadInterrupt}, this is
		 * treated as a thread and is interrupted. If
		 * {@link AlarmType#ObjectNotify}, then the object's lock is acquired
		 * and the object is notified.
		 */
		public Object obj;
		
		/** The type of alarm. */
		public AlarmType type;
		
		/**
		 * Construct a new set point.
		 * 
		 * @param time The time of the set point.
		 * @param obj The thread or object for the set point.
		 * @param type The alarm type.
		 */
		public SetPoint(Date time, Object obj, AlarmType type)
		{
			this.time = time;
			this.obj = obj;
			this.type = type;
		}
		
		/**
		 * Compare this set point to another.
		 * 
		 * @param sp The set point to compare to.
		 * @return A value less than, equal to, or greater than 0 indicating
		 *   whether this set point's time occurs before, at the same time as,
		 *   or after <tt>sp</tt>'s time, respectively.
		 */
		@Override
		public int compareTo(SetPoint sp) {
			return time.compareTo(sp.time);
		}
	}
	
	/**
	 * Set an alarm time for this thread.
	 * 
	 * @param time The time for the alarm to go off.
	 */
	public static void setAlarm(Date time)
	{
		theAlarm.addSetPoint(time, Thread.currentThread(),
		                     AlarmType.ThreadInterrupt);
	}
	
	/**
	 * Set an alarm time for a given object.
	 * 
	 * @param time The time for the alarm to go off.
	 * @param obj The object to notify when the alarm goes off.
	 */
	public static void setAlarm(Date time, Object obj)
	{
		theAlarm.addSetPoint(time, obj, AlarmType.ObjectNotify);
	}
	
	/**
	 * Clear all alarms for the current thread.
	 */
	public static void clearAlarms()
	{
		theAlarm.clearSetPoints(null, Thread.currentThread(), AlarmType.ThreadInterrupt);
	}
	
	/**
	 * Clear all alarms set on the given object.
	 * 
	 * @param obj The object that owns the alarms.
	 */
	public static void clearAlarms(Object obj)
	{
		theAlarm.clearSetPoints(null, obj, AlarmType.ObjectNotify);
	}
	
	/**
	 * Clear any alarms for the current thread set for the specified time.
	 * 
	 * @param time The time to clear.
	 */
	public static void clearAlarm(Date time)
	{
		theAlarm.clearSetPoints(time, Thread.currentThread(), AlarmType.ThreadInterrupt);
	}
	
	/**
	 * Clear any alarms for the given object set for the specified time.
	 * 
	 * @param time The time to clear.
	 * @param obj The object that owns the alarm(s).
	 */
	public static void clearAlarm(Date time, Object obj)
	{
		theAlarm.clearSetPoints(time, obj, AlarmType.ObjectNotify);
	}
	
	/** A priority queue of alarm set points. */
	private PriorityQueue<SetPoint> setPoints = new PriorityQueue<SetPoint>();
	
	/**
	 * Construct a new Alarm.
	 */
	private Alarm()
	{
		super("Alarm Clock Thread");
	}
	
	/**
	 * Public only as an implementation detail.
	 */
	@Override
	public void run()
	{
		// This really is meant to run forever (well... until the JVM dies)
		while (true) {
			synchronized (setPoints) {
				// Grab the top set point and check its time
				SetPoint top = setPoints.peek();
				
				if (top != null) {
					// Are we past time on this set point?
					if (top.time.getTime() <= System.currentTimeMillis()) {
						// Yes
						// Remove the set point from the queue (to avoid
						// multiple alarms, if something weird happens)
						setPoints.remove();
						
						// Which type of alarm is it?
						switch (top.type) {
							case ObjectNotify:
								// NOTE: If the caller is misbehaving, this will
								// block any future alarms from going off
								synchronized (top.obj) {
									top.obj.notify();
								}
								break;
							case ThreadInterrupt:
								if (top.obj instanceof Thread) {
									Thread th = (Thread)top.obj;
									if (th.isAlive()) {
										th.interrupt();
									}
									else {
										// Make sure it doesn't come up again,
										// threads cannot be re-started
										clearSetPoints(null, top.obj, top.type);
									}
								}
								else {
									// Not a thread?
									clearSetPoints(null, top.obj, top.type);
								}
								break;
							default:
								throw new RuntimeException("Unknown alarm type: " +
								                           top.type.name());
						}
					}
					else {
    					// Sleep on the queue until the set point's time is up, or
    					// until something interesting happens
						try {
							long timeout =
								top.time.getTime() - System.currentTimeMillis();
							
							// Do not accidentally wait forever, when we really
							// wanted to wait for 0ms
							if (timeout > 0) {
								setPoints.wait(top.time.getTime() -
								               System.currentTimeMillis());
							}
						}
						catch (InterruptedException ie) {
							logger.warn("Alarm clock thread ignoring an interrupt",
							            ie);
						}
					}
				}
				else {
					// No set points? Sleep until something interesting happens
					try {
						setPoints.wait();
					}
					catch (InterruptedException ie) {
						logger.warn("Alarm clock thread ignoring an interrupt",
						            ie);
					}
				}
			}
		}
	}
	
	/**
	 * Add an alarm set point to the queue.
	 * 
	 * @param time The time for the alarm to go off.
	 * @param obj The object to put in the queue.
	 * @param type The type of set point to create.
	 */
	void addSetPoint(Date time, Object obj, AlarmType type)
	{
		synchronized (setPoints) {
			setPoints.add(new SetPoint(time, obj, type));
			
			setPoints.notify();
		}
	}
	
	/**
	 * Remove all alarm set points related to the given object from the queue.
	 * 
	 * @param time The time of the alarm set point(s) to remove; use
	 *   <tt>null</tt> to remove all alarm set points of the specified type for
	 *   the given object.
	 * @param obj The object whose alarm set points to clear.
	 * @param type The type of alarm set point(s) to remove.
	 */
	void clearSetPoints(Date time, Object obj, AlarmType type)
	{
		synchronized (setPoints) {
			boolean changed = false;
			
			for (Iterator<SetPoint> itSetPoints = setPoints.iterator();
			     itSetPoints.hasNext(); ) {
				
				SetPoint sp = itSetPoints.next();
				
				if ((time == null || sp.time.equals(time)) &&
				    sp.obj.equals(obj) &&
				    sp.type == type) {
					
					itSetPoints.remove();
					changed = true;
				}
			}
			
			if (changed) {
				setPoints.notify();
			}
		}
	}
}

package com.sixnetio.util;

/**
 * A mutable reference to an object.
 * 
 * @param <E> The type of reference to hold.
 */
public class Ref<E>
    implements Cloneable
{
    private E o;

    /** Construct a new <code>null</code> reference. */
    public Ref()
    {
        o = null;
    }

    /**
     * Construct a new reference with the specified value.
     * 
     * @param _o The value.
     */
    public Ref(E _o)
    {
        o = _o;
    }

    /**
     * Get the value of this reference.
     * 
     * @return The value.
     */
    public E get()
    {
        return o;
    }

    /**
     * Set the value of this reference.
     * 
     * @param _o The new value.
     */
    public void set(E _o)
    {
        o = _o;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null) {
            return false;
        }
        else if (o instanceof Ref<?>) {
            Ref<?> r = (Ref<?>)o;
            if (get() == null) {
                throw new NullPointerException("Null reference");
            }
            else {
                return (get().equals(r.get()));
            }
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode()
    {
        if (o == null) {
            return 0;
        }
        else {
            return o.hashCode();
        }
    }

    @Override
    public String toString()
    {
        if (o == null) {
            return "(null)";
        }
        else {
            return o.toString();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Ref<E> clone()
    {
        try {
            return (Ref<E>)super.clone();
        }
        catch (CloneNotSupportedException cnse) {
            // Should not happen
            throw new RuntimeException(cnse);
        }
    }
}

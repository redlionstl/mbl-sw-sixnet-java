package com.sixnetio.util;

import java.math.BigInteger;

/**
 * Implements comparison functions for arrays.
 */
public class Comparer
{
    /**
     * Compare the elements of two arrays, treating each as a signed number.
     * 
     * @param a The first array.
     * @param b The second array.
     * @return -1 if the first unequal byte between the two arrays is lower in
     *         <code>a</code> than in <code>b</code>. If no unequal bytes are
     *         discovered, the longer array is treated as larger. 0 if all
     *         elements are equal.
     */
    public static int compareSigned(final byte[] a, final byte[] b)
    {
        for (int i = 0; i < a.length && i < b.length; i++) {
            if (a[i] < b[i]) {
                return -1;
            }
            else if (a[i] > b[i]) {
                return 1;
            }
        }

        if (a.length < b.length) {
            return -1;
        }
        else if (a.length > b.length) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /**
     * Compare the elements of two arrays, treating each as a signed number.
     * 
     * @param a The first array.
     * @param b The second array.
     * @return -1 if the first unequal byte between the two arrays is lower in
     *         <code>a</code> than in <code>b</code>. If no unequal bytes are
     *         discovered, the longer array is treated as larger. 0 if all
     *         elements are equal.
     */
    public static int compareSigned(final short[] a, final short[] b)
    {
        for (int i = 0; i < a.length && i < b.length; i++) {
            if (a[i] < b[i]) {
                return -1;
            }
            else if (a[i] > b[i]) {
                return 1;
            }
        }

        if (a.length < b.length) {
            return -1;
        }
        else if (a.length > b.length) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /**
     * Compare the elements of two arrays, treating each as a signed number.
     * 
     * @param a The first array.
     * @param b The second array.
     * @return -1 if the first unequal byte between the two arrays is lower in
     *         <code>a</code> than in <code>b</code>. If no unequal bytes are
     *         discovered, the longer array is treated as larger. 0 if all
     *         elements are equal.
     */
    public static int compareSigned(final int[] a, final int[] b)
    {
        for (int i = 0; i < a.length && i < b.length; i++) {
            if (a[i] < b[i]) {
                return -1;
            }
            else if (a[i] > b[i]) {
                return 1;
            }
        }

        if (a.length < b.length) {
            return -1;
        }
        else if (a.length > b.length) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /**
     * Compare the elements of two arrays, treating each as a signed number.
     * 
     * @param a The first array.
     * @param b The second array.
     * @return -1 if the first unequal byte between the two arrays is lower in
     *         <code>a</code> than in <code>b</code>. If no unequal bytes are
     *         discovered, the longer array is treated as larger. 0 if all
     *         elements are equal.
     */
    public static int compareSigned(final long[] a, final long[] b)
    {
        for (int i = 0; i < a.length && i < b.length; i++) {
            if (a[i] < b[i]) {
                return -1;
            }
            else if (a[i] > b[i]) {
                return 1;
            }
        }

        if (a.length < b.length) {
            return -1;
        }
        else if (a.length > b.length) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /**
     * Compare the elements of two arrays, treating each as an unsigned number.
     * 
     * @param a The first array.
     * @param b The second array.
     * @return -1 if the first unequal byte between the two arrays is lower in
     *         <code>a</code> than in <code>b</code>. If no unequal bytes are
     *         discovered, the longer array is treated as larger. 0 if all
     *         elements are equal.
     */
    public static int compareUnsigned(final byte[] a, final byte[] b)
    {
        for (int i = 0; i < a.length && i < b.length; i++) {
            int va = a[i] & 0xff;
            int vb = b[i] & 0xff;
            if (va < vb) {
                return -1;
            }
            else if (va > vb) {
                return 1;
            }
        }

        if (a.length < b.length) {
            return -1;
        }
        else if (a.length > b.length) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /**
     * Compare the elements of two arrays, treating each as an unsigned number.
     * 
     * @param a The first array.
     * @param b The second array.
     * @return -1 if the first unequal byte between the two arrays is lower in
     *         <code>a</code> than in <code>b</code>. If no unequal bytes are
     *         discovered, the longer array is treated as larger. 0 if all
     *         elements are equal.
     */
    public static int compareUnsigned(final short[] a, final short[] b)
    {
        for (int i = 0; i < a.length && i < b.length; i++) {
            int va = a[i] & 0xffff;
            int vb = b[i] & 0xffff;
            if (va < vb) {
                return -1;
            }
            else if (va > vb) {
                return 1;
            }
        }

        if (a.length < b.length) {
            return -1;
        }
        else if (a.length > b.length) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /**
     * Compare the elements of two arrays, treating each as an unsigned number.
     * 
     * @param a The first array.
     * @param b The second array.
     * @return -1 if the first unequal byte between the two arrays is lower in
     *         <code>a</code> than in <code>b</code>. If no unequal bytes are
     *         discovered, the longer array is treated as larger. 0 if all
     *         elements are equal.
     */
    public static int compareUnsigned(final int[] a, final int[] b)
    {
        for (int i = 0; i < a.length && i < b.length; i++) {
            long va = a[i] & 0xffffffff;
            long vb = b[i] & 0xffffffff;
            if (va < vb) {
                return -1;
            }
            else if (va > vb) {
                return 1;
            }
        }

        if (a.length < b.length) {
            return -1;
        }
        else if (a.length > b.length) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /**
     * Compare the elements of two arrays, treating each as an unsigned number.
     * 
     * @param a The first array.
     * @param b The second array.
     * @return -1 if the first unequal byte between the two arrays is lower in
     *         <code>a</code> than in <code>b</code>. If no unequal bytes are
     *         discovered, the longer array is treated as larger. 0 if all
     *         elements are equal.
     */
    public static int compareUnsigned(final long[] a, final long[] b)
    {
        for (int i = 0; i < a.length && i < b.length; i++) {
            byte[] bytes = new byte[8];

            Conversion.longToBytes(bytes, 0, a[i]);
            BigInteger va = new BigInteger(1, bytes);

            Conversion.longToBytes(bytes, 0, b[i]);
            BigInteger vb = new BigInteger(1, bytes);

            int c = va.compareTo(vb);
            if (c != 0) {
                return c;
            }
        }

        if (a.length < b.length) {
            return -1;
        }
        else if (a.length > b.length) {
            return 1;
        }
        else {
            return 0;
        }
    }
}

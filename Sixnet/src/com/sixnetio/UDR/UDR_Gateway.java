/*
 * UDR_GWY.java
 *
 * The base class for the GWY_* subcommands.
 *
 * Jonathan Pearson
 * February 4, 2008
 *
 */

package com.sixnetio.UDR;

import java.io.IOException;

import com.sixnetio.UDR.Gateway.*;

/**
 * The superclass of the Gateway messages.
 *
 * @author Jonathan Pearson
 */
public class UDR_Gateway extends UDRMessage {
	// Constants
	/** The command byte for Read Module List. */
	public static final byte C_READ_MODULE_LIST = 0x00;
	
	/** The command byte for the Miscellaneous command. */
	public static final byte C_MISCELLANEOUS = 0x01;
	
	/** The command byte for Set All Modules Blink Rate. */
	public static final byte C_SET_ALL_MODULES_BLINK_RATE = 0x02;
	
	/** The command byte for Set Module Scan Rate. */
	public static final byte C_SET_MODULE_SCAN_LIST = 0x03;
	
	/** The command byte for Use Port EEROM Comm Parameters. */
	public static final byte C_USE_PORT_EEROM_COMM_PARAMS = 0x06;
	
	/** The command byte for Read Module EEROM. */
	public static final byte C_READ_MODULE_EEROM = 0x07;
	
	/** The command byte for Write Module EEROM. */
	public static final byte C_WRITE_MODULE_EEROM = 0x08;
	
	/** The command byte for Set Module Calibration Constants. */
	public static final byte C_SET_MODULE_CALIBRATION_CONSTANTS = 0x0b;
	
	/** The command byte for Get Module Calibration Constants. */
	public static final byte C_GET_MODULE_CALIBRATION_CONSTANTS = 0x0c;
	
	/** The command byte for Use Channel Calibration Constants. */
	public static final byte C_USE_CHANNEL_CALIBRATION_CONSTANTS = 0x0d;
	
	/** The command byte for Read EEROM. */
	public static final byte C_READ_EEROM = 0x0e;
	
	/** The command byte for Write EEROM. */
	public static final byte C_WRITE_EEROM = 0x0f;
	
	/** The command byte for Reset Module. */
	public static final byte C_RESET_MODULE = 0x10;
	
	/** The command byte for Get Module Scan List. */
	public static final byte C_GET_MODULE_SCAN_LIST = 0x11;
	
	/** The command byte for Reset Gateway. */
	public static final byte C_RESET_GATEWAY = 0x12;
	
	/** The command byte for the Status command. */
	public static final byte C_STATUS = 0x13;
	
	/** The command byte for the Gateway Version command. */
	public static final byte C_GATEWAY_VERSION = 0x14;
	
	/** The command byte for Read Configuration Block. */
	public static final byte C_READ_CONFIGURATION_BLOCK = 0x15;
	
	/** The command byte for Write Configuration Block. */
	public static final byte C_WRITE_CONFIGURATION_BLOCK = 0x16;
	
	/** The command byte for Delete Configuration Block. */
	public static final byte C_DELETE_CONFIGURATION_BLOCK = 0x17;
	
	/** The command byte for VersaTRAK Simulate. */
	public static final byte C_VERSATRAK_SIMULATE = 0x18;
	
	/** The command byte for Use Port EEROM Protocol. */
	public static final byte C_USE_PORT_EEROM_PROTOCOL = 0x19;
	
	/** The command byte for Reset IPm. */
	public static final byte C_RESET_IPM = 0x1a; // Same as ResetRemotetrak, this has the shorter name
	
	/** The command byte for Read Ambient. */
	public static final byte C_READ_AMBIENT = 0x1b;
	
	/** The command byte for IPm Read Settings. */
	public static final byte C_IPM_READ_SETTINGS = 0x1c;
	
	/** The command byte for Execute. */
	public static final byte C_EXECUTE = 0x1e;
	
	/** The command byte for AVR Program. */
	public static final byte C_AVR_PROGRAM = (byte)0xf0;
	
	// Private members
	private byte subcommand;
	
	/**
	 * Construct a new generic Gateway message.
	 * 
	 * @param subcommand The subcommand byte (see constants above). 
	 */
	public UDR_Gateway(byte subcommand) {
		super.setCommand(UDRMessage.C_GWY);
		this.subcommand = subcommand;
	}
	
	/**
	 * Set the data for this message from a byte array (received data).
	 */
	@Override
	public void setData(byte[] data) {
		byte[] realData = new byte[data.length + 1];
		
		realData[0] = subcommand;
		System.arraycopy(data, 0, realData, 1, data.length);
		
		super.setData(realData);
	}
	
	/**
	 * Get the data that {@link #setData(byte[])} would use.
	 */
	@Override
	public byte[] getData() {
		byte[] realData = super.getData();
		byte[] data = new byte[realData.length - 1];
		
		System.arraycopy(realData, 1, data, 0, data.length);
		
		return data;
	}
	
	/**
	 * Set the command byte (will throw an exception).
	 */
	@Override
	public void setCommand(byte val) {
		if (val != super.getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Set the subcommand byte.
	 */
	public void setSubCommand(byte val) {
		subcommand = val;
		setData(getData()); // Make sure that UDRMessage has the correct data
	}
	
	/**
	 * Get the subcommand byte.
	 */
	public final byte getSubCommand() {
		return subcommand;
	}
	
	/**
	 * Create an instance of a specific gateway message (from the known
	 * subclasses).
	 * 
	 * @param data The raw data of the message.
	 * @return A new gateway message.
	 * @throws IOException If there is a problem parsing the data into a
	 *   message.
	 */
	protected static UDR_Gateway createInstance(byte[] data) throws IOException {
		UDR_Gateway ans = null;
		
		if (data.length == 0) throw new IOException("GWY message has no subcommand");
		
		byte[] subdata = new byte[data.length - 1];
		System.arraycopy(data, 1, subdata, 0, subdata.length);
		
		switch (data[0]) {
			case C_READ_MODULE_LIST: {
				ans = new GWY_ReadModuleList(subdata);
				break;
			}
			case C_MISCELLANEOUS: {
				ans = new GWY_Miscellaneous(subdata);
				break;
			}
			case C_SET_ALL_MODULES_BLINK_RATE: {
				ans = new GWY_SetAllModulesBlinkRate(subdata);
				break;
			}
			case C_SET_MODULE_SCAN_LIST: {
				break;
			}
			case C_USE_PORT_EEROM_COMM_PARAMS: {
				ans = new GWY_UsePortEERomCommParams(subdata);
				break;
			}
			case C_READ_MODULE_EEROM: {
				ans = new GWY_ReadModuleEERom(subdata);
				break;
			}
			case C_WRITE_MODULE_EEROM: {
				ans = new GWY_WriteModuleEERom(subdata);
				break;
			}
			case C_SET_MODULE_CALIBRATION_CONSTANTS: {
				break;
			}
			case C_GET_MODULE_CALIBRATION_CONSTANTS: {
				break;
			}
			case C_USE_CHANNEL_CALIBRATION_CONSTANTS: {
				break;
			}
			case C_READ_EEROM: {
				break;
			}
			case C_WRITE_EEROM: {
				break;
			}
			case C_RESET_MODULE: {
				break;
			}
			case C_GET_MODULE_SCAN_LIST: {
				break;
			}
			case C_RESET_GATEWAY: {
				ans = new GWY_ResetGateway(subdata);
				break;
			}
			case C_STATUS: {
				break;
			}
			case C_GATEWAY_VERSION: {
				break;
			}
			case C_READ_CONFIGURATION_BLOCK: {
				break;
			}
			case C_WRITE_CONFIGURATION_BLOCK: {
				break;
			}
			case C_DELETE_CONFIGURATION_BLOCK: {
				break;
			}
			case C_VERSATRAK_SIMULATE: {
				break;
			}
			case C_USE_PORT_EEROM_PROTOCOL: {
				ans = new GWY_UsePortEERomProtocol(subdata);
				break;
			}
			case C_RESET_IPM: {
				ans = new GWY_ResetIPm(subdata);
				break;
			}
			case C_READ_AMBIENT: {
				break;
			}
			case C_IPM_READ_SETTINGS: {
				ans = new GWY_IPmReadSettings(subdata);
				break;
			}
			case C_EXECUTE: {
				ans = new GWY_Execute(subdata);
				break;
			}
			case C_AVR_PROGRAM: {
				break;
			}
			default: {
				throw new IOException("Unrecognized FILESYS subcommand: " + data[0]);
			}
		}
		
		if (ans == null) {
			// Not implemented yet, use UDR_GWY generic type
			ans = new UDR_Gateway(data[0]);
			ans.setData(subdata);
		}
		
		return ans;
	}
	
	/**
	 * Get the descriptive name of this message.
	 */
	@Override
	public String getCommandString() {
		return "Gateway " + getSubCommandString();
	}
	
	/**
	 * Get the descriptive name of the subclass-implemented subcommand.
	 */
	public String getSubCommandString() {
		if (super.getData() == null) {
			return "Unset";
		} else {
			return "Unknown " + subcommand;
		}
	}
}

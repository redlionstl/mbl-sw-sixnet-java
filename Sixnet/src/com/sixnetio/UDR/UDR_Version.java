/*
 * UDR_VERS.java
 *
 * A VERS UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.*;

/**
 * A UDR message used to retrieve the product type and major/minor version number
 * of the firmware running in a device.
 * 
 * @author Jonathan Pearson
 */
public class UDR_Version extends UDRMessage {
	// Acknowledgment translation functions
	/**
	 * Verify the given acknowledgment matches this message.
	 * @return True = the acknowledgment matches; False = the acknowledgment is bad.
	 */
	public boolean verifyAck(UDR_Acknowledge ack) {
		return (ack.getData().length >= 4);
	}
	
	/**
	 * Get the product code of the provided acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The product code in the acknowledgment.
	 */
	public static short getProdCode(UDR_Acknowledge ack) {
		return Conversion.bytesToShort(ack.getData(), 0);
	}
	
	/**
	 * Get the major version of the provided acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The major version in the acknowledgment.
	 */
	public static byte getMajor(UDR_Acknowledge ack) {
		return ack.getData()[2];
	}
	
	/**
	 * Get the minor version of the provided acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The minor version in the acknowledgment.
	 */
	public static byte getMinor(UDR_Acknowledge ack) {
		return ack.getData()[3];
	}
	
	/**
	 * Used by UDRMessage.receive() to construct this message given a data payload.
	 * @param data The data payload.
	 */
	protected UDR_Version(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_VERS);
		setData(data);
	}
	
	/**
	 * Construct a new message of this type.
	 * @param prodcode The product code of the local implementation.
	 * @param major The major version number of the local implementation.
	 * @param minor The minor version number of the local implementation.
	 */
	public UDR_Version(short prodcode, byte major, byte minor) {
		super.setCommand(UDRMessage.C_VERS);
		
		// no type, therefore just use big-endian
		byte[] data = new byte[4];
		Conversion.shortToBytes(data, 0, prodcode);
		data[2] = major;
		data[3] = minor;
		
		setData(data);
	}
	
	/**
	 * Automatically construct an acknowledgment to this message.
	 * @param prodcode The product code of the local implementation.
	 * @param major The major version number of the local implementation.
	 * @param minor The minor version number of the local implementation.
	 * @return An acknowledgment to this message.
	 */
	public UDR_Acknowledge acknowledge(short prodcode, byte major, byte minor) {
		byte[] data = new byte[4];
		Conversion.shortToBytes(data, 0, prodcode);
		data[2] = major;
		data[3] = minor;
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Get the product code contained within this message.
	 */
	public short getProdCode() {
		return Conversion.bytesToShort(getData(), 0);
	}
	
	/**
	 * Get the major version number contained within this message.
	 */
	public byte getMajor() {
		return getData()[2];
	}
	
	/**
	 * Get the minor version number contained within this message.
	 */
	public byte getMinor() {
		return getData()[3];
	}
	
	/**
	 * Throw an IllegalArgumentException if the value does not match what is already set.
	 */
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Get a string description of this message's command byte.
	 */
	public String getCommandString() {
		return "Vers";
	}
	
	/**
	 * Describe the data payload of this message.
	 * @param indent The string to use to indent each line.
	 */
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Product code = %d", getProdCode()));
			} else if (i == 2) {
				ans.append(String.format(" Major = %d", getMajor()));
			} else if (i == 3) {
				ans.append(String.format(" Minor = %d", getMinor()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe the data payload of an acknowledgment to this message.
	 * @param ack The acknowledgment to describe.
	 * @param indent The string to use to indent each line.
	 */
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Product code = %d", getProdCode(ack)));
			} else if (i == 2) {
				ans.append(String.format(" Major = %d", getMajor(ack)));
			} else if (i == 3) {
				ans.append(String.format(" Minor = %d", getMinor()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

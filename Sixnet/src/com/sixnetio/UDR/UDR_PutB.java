/*
 * UDR_PUTB.java
 *
 * A PUTB UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.Conversion;

/**
 * A message to set discretes/floats/longs byte-by-byte in a device.
 *
 * @author Jonathan Pearson
 */
public class UDR_PutB extends UDRMessage {
    // Acknowledgment translation functions
	/**
	 * Verify the given acknowledgment matches this message.
	 * @return True = the acknowledgment matches; False = the acknowledgment is bad.
	 */
	public boolean verifyAck(UDR_Acknowledge ack) {
	    return (ack.getData().length >= 5 &&
	            getStart(ack) == getStart() &&
	            getNum(ack) == getNum());
    }
    
	/**
	 * Get the type byte of the acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The type byte of the acknowledgment.
	 */
	public static byte getType(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	/**
	 * Get the starting offset of the data points being written from an acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The starting offset in the acknowledgment.
	 */
	public static short getStart(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 1);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 1);
		}
	}
	
	/**
	 * Get the number of data points being written from an acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The number of data points in the acknowledgment.
	 */
	public static short getNum(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 3);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 3);
		}
	}
	
	/**
	 * Used by UDRMessage.receive() to construct this message given a data payload.
	 * @param data The data payload.
	 */
	protected UDR_PutB(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_PUTB);
		setData(data);
	}
	
	/**
	 * Construct a new message of this type.
	 * @param type The type byte of the message, see T_* in UDRMessage.
	 * @param start The starting offset to write.
	 * @param num The number of data points to write.
	 * @param vals The values to write to those data points. Bits/bytes increase from
	 *   bit 0 of byte 0 going to the starting offset.
	 */
	public UDR_PutB(byte type, short start, short num, byte[] vals) {
		super.setCommand(UDRMessage.C_PUTB);
		
		byte[] data = new byte[5 + vals.length];
		data[0] = type;
		
		if ((type & T_BIGENDIAN) == T_BIGENDIAN) {
			Conversion.shortToBytes(data, 1, start);
			Conversion.shortToBytes(data, 3, num);
		} else {
			Conversion.shortToBytesLE(data, 1, start);
			Conversion.shortToBytesLE(data, 3, num);
		}
		
		System.arraycopy(vals, 0, data, 5, vals.length);
		
		setData(data);
	}
	
	/**
	 * Automatically construct an acknowledgment to this message. Assumes that all data
	 *   points in the request were written.
	 * @return An acknowledgment to this message.
	 */
	public UDR_Acknowledge acknowledge() {
		byte[] data = new byte[5];
	
		System.arraycopy(getData(), 0, data, 0, 5);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Get the type byte of this message. See T_* in UDRMessage.
	 */
	public byte getType() {
		return getData()[0];
	}
	
	/**
	 * Get the starting offset being written by this message.
	 */
	public short getStart() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 1);
		} else {
			return Conversion.bytesToShortLE(getData(), 1);
		}
	}
	
	/**
	 * Get the number of data points being written by this message.
	 */
	public short getNum() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 3);
		} else {
			return Conversion.bytesToShortLE(getData(), 3);
		}
	}
	
	/**
	 * Get the byte being written at the given index.
	 * @param index The offset from the start of this message,
	 *   irrespective of the starting offset.
	 * @return The byte being written at the given index.
	 */
	public byte getVal(int index) {
		if (index > getNum()) throw new ArrayIndexOutOfBoundsException(String.format("Index %d out of range [0, %d)", index, getNum()));
		
		return getData()[5 + index];
	}
	
	/**
	 * Unless val == getCommand(), throws an IllegalArgumentException.
	 */
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Describe the command byte of this message.
	 */
	public String getCommandString() {
		return "PutB";
	}
	
	/**
	 * Describe the data payload of this message.
	 * @param indent The string to use to indent each line.
	 */
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType())));
			} else if (i == 1) {
				ans.append(String.format(" Start = %d", getStart()));
			} else if (i == 3) {
				ans.append(String.format(" Number = %d", getNum()));
			} else if (i >= 5) {
				ans.append(" Discrete data");
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe the data payload of an acknowledgment to this message.
	 * @param ack The acknowledgment to describe.
	 * @param indent The string to use to indent each line.
	 */
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType(ack))));
			} else if (i == 1) {
				ans.append(String.format(" Start = %d", getStart(ack)));
			} else if (i == 3) {
				ans.append(String.format(" Number = %d", getNum(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

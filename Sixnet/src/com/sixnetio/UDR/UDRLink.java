/*
 * UDRLink.java
 *
 * Implements functions for reading/writing UDR messages over various media,
 * such as serial ports, and over Ethernet via UDP and TCP
 *
 * Jonathan Pearson
 * March 20, 2007
 * 
 * Jonathan Pearson
 * May 10, 2010
 * Overhauled and renamed in preparation for writing a superclass above this and
 * its Modbus equivalent.
 *
 */

package com.sixnetio.UDR;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sixnetio.Station.IOProtocolLink;
import com.sixnetio.Station.TransportMethod;
import com.sixnetio.io.UDPInputStream;
import com.sixnetio.util.Utils;

/**
 * Handles communication of UDR messages over a single TCP, UDP, or serial line.
 * 
 * @author Jonathan Pearson
 */
public class UDRLink
    extends IOProtocolLink
{
    private static final Logger logger =
        Logger.getLogger(Utils.thisClassName());

    /** Standard port for UDR messages over Ethernet. */
    public static final int UDR_PORT = 1594;
    private static final Map<TransportMethod, String> defaultParams;

    static {
        defaultParams = new HashMap<TransportMethod, String>();

        defaultParams.put(TransportMethod.Serial, "9600-8-N-1");
        defaultParams.put(TransportMethod.TCP, "" + UDR_PORT);
        defaultParams.put(TransportMethod.UDP, "" + UDR_PORT);
        defaultParams.put(TransportMethod.UDPBroadcast, "" + UDR_PORT);
        defaultParams.put(TransportMethod.UDPLoop, "" + UDR_PORT);
    }

    /**
     * Construct a new UDRHandler that works over the specified link and
     * communicates with the device at the specified address.
     * 
     * @param transport The link over which to communicate. Supports Serial, TCP
     *            and the various UDP methods.
     * @param address The address with which to communicate. See
     *            {@link IOProtocolLink#IOProtocolLink(TransportMethod, String, Map)}
     *            for details.
     * @throws IOException If there was a problem opening the transport medium.
     */
    public UDRLink(TransportMethod transport, String address)
        throws IOException
    {
        super(transport, address, defaultParams);
    }


    @Override
    public UDRMessage receive(boolean expectAck)
        throws IOException
    {
        // Ignore 'expectAck'

        if (closed) {
            throw new IOException("Cannot receive from a closed link");
        }

        if (overUDP()) {
            UDPInputStream inp = (UDPInputStream)input;
            inp.flush();
        }

        try {
            UDRMessage msg = UDRMessage.receive(input);

            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Received a message via %s:\n%s",
                    description, msg.toString()));
            }

            return msg;
        }
        catch (IOException ioe) {
            // If there's an EOF, this handler is dead; there's no way to get
            // more data from it
            if (ioe.getMessage().equals("Unexpected EOF")) {
                logger.debug("Closing link due to EOF");
                close();
            }

            throw ioe;
        }
    }

    @Override
    public UDRMessage receive(boolean expectAck, InetAddress[] addr, int[] port)
        throws IOException
    {
        // Ignore 'expectAck'

        if (closed) {
            throw new IOException("Cannot receive from a closed link");
        }

        if (addr == null || addr.length < 1) {
            addr = new InetAddress[1];
        }

        if (port == null || port.length < 1) {
            port = new int[1];
        }

        UDPInputStream in = (UDPInputStream)input;

        UDRMessage msg = UDRMessage.receive(in);

        DatagramPacket pkt = in.getLastPacket();
        addr[0] = pkt.getAddress();
        port[0] = pkt.getPort();

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Received a message from %s:%d:\n%s",
                addr[0].getHostAddress(), port[0], msg.toString()));
        }

        return msg;
    }

    @Override
    public UDRMessage receive()
        throws IOException
    {
        return receive(true);
    }

    @Override
    public UDRMessage receive(InetAddress[] out_addr, int[] out_port)
        throws IOException
    {
        return receive(true, out_addr, out_port);
    }
}

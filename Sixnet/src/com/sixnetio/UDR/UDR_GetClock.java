/*
 * UDR_GCLOCK.java
 *
 * A GCLOCK UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.Conversion;

/**
 * A message to retrieve the clock value in a station.
 *
 * @author Jonathan Pearson
 */
public class UDR_GetClock extends UDRMessage {
	/**
	 * Verify that the given ACK matches this message.
	 * 
	 * @param ack The ACK to verify.
	 * @return True = the ACK matches this message, False = it does not match.
	 */
	@Override
	public boolean verifyAck(UDR_Acknowledge ack) {
		return (ack.getData().length >= 4);
	}
	
	// Acknowledgment translation functions
	/**
	 * Get the time (in seconds since the epoch) returned in the ACK.
	 */
	public static int getTime(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 0);
	}
	
	/**
	 * Construct a new GetClock message from a byte array (received data).
	 * 
	 * @param data The byte array to parse into a GetClock message.
	 */
	protected UDR_GetClock(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_GCLOCK);
		setData(data);
	}
	
	/**
	 * Construct a new GetClock message from its constituent parts.
	 */
	public UDR_GetClock() {
		super.setCommand(UDRMessage.C_GCLOCK);
		
		byte[] data = new byte[0];
		
		setData(data);
	}
	
	/**
	 * Construct an acknowledgment to this message.
	 * 
	 * @param time The time to return, in seconds since the epoch.
	 * @return An acknowledgment to this message.
	 */
	public UDR_Acknowledge acknowledge(int time) {
		byte[] data = new byte[4];
		
		// No type, so use big-endian
		Conversion.intToBytes(data, 0, time);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Set the command byte for this message (will throw an exception).
	 */
	@Override
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Get a descriptive name for this message.
	 */
	@Override
	public String getCommandString() {
		return "GClock";
	}
	
	/**
	 * Describe an acknowledgment to this message.
	 * 
	 * @param ack The ACK to describe.
	 * @param indent A string to prepend to each line of the returned string.
	 * @return A description of an ACK to this message.
	 */
	@Override
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Time = %d", getTime(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

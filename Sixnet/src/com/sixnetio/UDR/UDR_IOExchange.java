/*
 * UDR_IOXCHG.java
 *
 * A IOXCHG UDR message
 *
 * Jonathan Pearson
 * April 17, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.*;

/**
 * A message to get/set any number of the four basic I/O types (analog ins/outs,
 *   discrete ins/outs) in a device at the same time.
 *
 * @author Jonathan Pearson
 */
public class UDR_IOExchange extends UDRMessage {
	// Acknowledgment translation functions
	/**
	 * Verify the given acknowledgment matches this message.
	 * @return True = the acknowledgment matches; False = the acknowledgment is bad.
	 */
	public boolean verifyAck(UDR_Acknowledge ack) {
		if (ack.getData().length < 4) return false; // Need to have at least 5 bytes
		
		short numDI = getDInCount(ack);
		short numDO = getDOutCount(ack);
		short numAI = getAInCount(ack);
		short numAO = getAOutCount(ack);
		
		short expectedLength = 4;
		
		if (!writeDIn(ack)) expectedLength += numDI;
		if (!writeDOut(ack)) expectedLength += numDO;
		if (!writeAIn(ack)) expectedLength += numAI;
		if (!writeAOut(ack)) expectedLength += numAO;
		
		return (ack.getData().length >= expectedLength &&
		        writeDIn(ack) == writeDIn() &&
		        writeDOut(ack) == writeDOut() &&
		        writeAIn(ack) == writeAIn() &&
		        writeAOut(ack) == writeAOut() &&
		        getDInCount(ack) == getDInCount() &&
		        getDOutCount(ack) == getDOutCount() &&
		        getAInCount(ack) == getAInCount() &&
		        getAOutCount(ack) == getAOutCount());
	}
	
	/**
	 * Get the number of discrete inputs mentioned by the acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The number of discrete inputs being read/written by the acknowledgment.
	 */
	public static byte getDInCount(UDR_Acknowledge ack) {
		return (byte)(ack.getData()[0] & 0x3F);
	}
	
	/**
	 * Whether discrete inputs were written.
	 * @param ack The acknowledgment to read from.
	 * @return True = discrete inputs were written;
	 *   False = discrete inputs were read and returned by this acknowledgment.
	 */
	public static boolean writeDIn(UDR_Acknowledge ack) {
		return ((ack.getData()[0] & UDR_IOExchange.F_WRITE) != 0);
	}
	
	/**
	 * Get the number of discrete outputs mentioned by the acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The number of discrete outputs being read/written by the acknowledgment.
	 */
	public static byte getDOutCount(UDR_Acknowledge ack) {
		return (byte)(ack.getData()[1] & 0x3F);
	}
	
	/**
	 * Whether discrete outputs were written.
	 * @param ack The acknowledgment to read from.
	 * @return True = discrete outputs were written;
	 *   False = discrete outputs were read and returned by this acknowledgment.
	 */
	public static boolean writeDOut(UDR_Acknowledge ack) {
		return ((ack.getData()[1] & UDR_IOExchange.F_WRITE) != 0);
	}
	
	/**
	 * Get the number of analog inputs mentioned by the acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The number of analog inputs being read/written by the acknowledgment.
	 */
	public static byte getAInCount(UDR_Acknowledge ack) {
		return (byte)(ack.getData()[2] & 0x3F);
	}
	
	/**
	 * Whether analog inputs were written.
	 * @param ack The acknowledgment to read from.
	 * @return True = analog inputs were written;
	 *   False = analog inputs were read and returned by this acknowledgment.
	 */
	public static boolean writeAIn(UDR_Acknowledge ack) {
		return ((ack.getData()[2] & UDR_IOExchange.F_WRITE) != 0);
	}
	
	/**
	 * Get the number of analog outputs mentioned by the acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The number of analog outputs being read/written by the acknowledgment.
	 */
	public static byte getAOutCount(UDR_Acknowledge ack) {
		return (byte)(ack.getData()[3] & 0x3F);
	}
	
	/**
	 * Whether analog outputs were written.
	 * @param ack The acknowledgment to read from.
	 * @return True = analog outputs were written;
	 *   False = analog outputs were read and returned by this acknowledgment.
	 */
	public static boolean writeAOut(UDR_Acknowledge ack) {
		return ((ack.getData()[3] & UDR_IOExchange.F_WRITE) != 0);
	}
	
	/**
	 * If reading, get the discrete inputs in the byte read from at the specified index.
	 * @param ack The acknowledgment to read from.
	 * @param index The index into the discrete inputs in this message to read a value from.
	 * @return The value read from that position.
	 */
	public static byte getDIn(UDR_Acknowledge ack, int index) {
		// If writing, there are none
		if (writeDIn(ack)) throw new ArrayIndexOutOfBoundsException("There are no discrete inputs in this message");
		if (index > getDInCount(ack)) throw new ArrayIndexOutOfBoundsException("Max index for discrete ins is " + getDInCount(ack) + ", cannot read " + index);
		
		return ack.getData()[4 + index];
	}
	
	/**
	 * If reading, get the discrete outputs in the byte read from at the specified index.
	 * @param ack The acknowledgment to read from.
	 * @param index The index into the discrete outputs in this message to read a value from.
	 * @return The value read from that position.
	 */
	public static byte getDOut(UDR_Acknowledge ack, int index) {
		// If writing, there are none
		if (writeDOut(ack)) throw new ArrayIndexOutOfBoundsException("There are no discrete outputs in this message");
		if (index > getDOutCount(ack)) throw new ArrayIndexOutOfBoundsException("Max index for discrete outs is " + getDOutCount(ack) + ", cannot read " + index);
		
		if (!writeDIn(ack)) {
			index += getDInCount(ack);
		}
		
		return ack.getData()[4 + index];
	}
	
	/**
	 * If reading, get the analog inputs in the byte read from at the specified index.
	 * @param ack The acknowledgment to read from.
	 * @param index The index into the analog inputs in this message to read a value from.
	 * @return The value read from that position.
	 */
	public static short getAIn(UDR_Acknowledge ack, int index) {
		// If writing, there are none
		if (writeAIn(ack)) throw new ArrayIndexOutOfBoundsException("There are no analog inputs in this message");
		if (index > getAInCount(ack)) throw new ArrayIndexOutOfBoundsException("Max index for analog ins is " + getAInCount(ack) + ", cannot read " + index);
		
		index *= 2; // These are shorts
		
		if (!writeDIn(ack)) {
			index += getDInCount(ack);
		}
		
		if (!writeDOut(ack)) {
			index += getDOutCount(ack);
		}
		
		if ((ack.getData()[2] & F_BIGENDIAN) == F_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 4 + index);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 4 + index);
		}
	}
	
	/**
	 * If reading, get the analog outputs in the byte read from at the specified index.
	 * @param ack The acknowledgment to read from.
	 * @param index The index into the analog outputs in this message to read a value from.
	 * @return The value read from that position.
	 */
	public static short getAOut(UDR_Acknowledge ack, int index) {
		// If writing, there are none
		if (writeAOut(ack)) throw new ArrayIndexOutOfBoundsException("There are no analog outputs in this message");
		if (index > getAOutCount(ack)) throw new ArrayIndexOutOfBoundsException("Max index for analog outs is " + getAOutCount(ack) + ", cannot read " + index);
		
		index *= 2; // These are shorts
		
		if (!writeDIn(ack)) {
			index += getDInCount(ack);
		}
		
		if (!writeDOut(ack)) {
			index += getDOutCount(ack);
		}
		
		if (!writeAIn(ack)) {
			index += getAInCount(ack) * 2; // These are also shorts
		}
		
		if ((ack.getData()[3] & F_BIGENDIAN) == F_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 4 + index);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 4 + index);
		}
	}
	
	/** Similar to UDRMessage.T_BIGENDIAN, but used for individual types. */ 
	public static final byte F_BIGENDIAN = (byte)(128 & 0xFF);
	
	/** Flag indicating that the data should be written, rather than read. */
	public static final byte F_WRITE = (byte)(64 & 0xFF);
	
	/**
	 * Used by UDRMessage.receive() to construct this message given a data payload.
	 * @param data The data payload.
	 */
	protected UDR_IOExchange(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_IOXCHG);
		setData(data);
	}
	
	/**
	 * Construct a new message of this type.
	 * @param dIn The number of discrete inputs to transfer, with optional flags F_BIGENDIAN and F_WRITE.
	 * @param dOut The number of discrete outputs to transfer, with optional flags F_BIGENDIAN and F_WRITE.
	 * @param aIn The number of analog inputs to transfer, with optional flags F_BIGENDIAN and F_WRITE.
	 * @param aOut The number of analog outputs to transfer, with optional flags F_BIGENDIAN and F_WRITE.
	 * @param dInVals The discrete input values to write, if any.
	 * @param dOutVals The discrete output values to write, if any.
	 * @param aInVals The analog input values to write, if any.
	 * @param aOutVals The analog output values to write, if any.
	 */
	public UDR_IOExchange(byte dIn, byte dOut, byte aIn, byte aOut, 
	                  byte[] dInVals, byte[] dOutVals,
	                  short[] aInVals, short[] aOutVals) {
		
		super.setCommand(UDRMessage.C_IOXCHG);
		
		int len = 4;
		if ((dIn & F_WRITE) == F_WRITE) len += dIn & 0x3F; // Cut off the two high-bits (type and write)
		if ((dOut & F_WRITE) == F_WRITE) len += dOut & 0x3F;
		if ((aIn & F_WRITE) == F_WRITE) len += aIn & 0x3F;
		if ((aOut & F_WRITE) == F_WRITE) len += aOut & 0x3F;
		
		byte[] data = new byte[len];
		data[0] = dIn;
		data[1] = dOut;
		data[2] = aIn;
		data[3] = aOut;
		
		int offset = 4;
		
		if ((dIn & F_WRITE) == F_WRITE) {
			System.arraycopy(dInVals, 0, data, offset, dIn & 0x3F);
			offset += dIn & 0x3F;
		}
		
		if ((dOut & F_WRITE) == F_WRITE) {
			System.arraycopy(dOutVals, 0, data, offset, dOut & 0x3F);
			offset += dOut & 0x3F;
		}
		
		if ((aIn & F_WRITE) == F_WRITE) {
			for (int i = 0; i < (aIn & 0x3F); i++) {
				if ((aIn & F_BIGENDIAN) == F_BIGENDIAN) {
					Conversion.shortToBytes(data, offset, aInVals[i]);
				} else {
					Conversion.shortToBytesLE(data, offset, aInVals[i]);
				}
				
				offset += 2;
			}
		}
		
		if ((aOut & F_WRITE) == F_WRITE) {
			for (int i = 0; i < (aOut & 0x3F); i++) {
				if ((aOut & F_BIGENDIAN) == F_BIGENDIAN) {
					Conversion.shortToBytes(data, offset, aOutVals[i]);
				} else {
					Conversion.shortToBytesLE(data, offset, aOutVals[i]);
				}
				
				offset += 2;
			}
		}
		
		setData(data);
	}
	
	/**
	 * Automatically construct an acknowledgment to this message.
	 * @param dInVals The discrete input values that were read, if any.
	 * @param dOutVals The discrete output values that were read, if any.
	 * @param aInVals The analog input values that were read, if any.
	 * @param aOutVals The analog output values that were read, if any.
	 * @return An acknowledgment to this message.
	 */
	public UDR_Acknowledge acknowledge(byte[] dInVals, byte[] dOutVals,
							   short[] aInVals, short[] aOutVals) {
		
		// ACK is the same format as IOXCHG, but inverted (returning the values that were read)
		
		int len = 4;
		if (!writeDIn()) len += getDInCount();
		if (!writeDOut()) len += getDOutCount();
		if (!writeAIn()) len += getAInCount() * 2; // These are shorts
		if (!writeAOut()) len += getAOutCount() * 2;
		
		byte[] data = new byte[len];
		System.arraycopy(getData(), 0, data, 0, 4);
		
		int offset = 4;
		
		if (!writeDIn()) {
			System.arraycopy(dInVals, 0, data, offset, getDInCount());
			offset += getDInCount();
		}
		
		if (!writeDOut()) {
			System.arraycopy(dOutVals, 0, data, offset, getDOutCount());
			offset += getDOutCount();
		}
		
		if (!writeAIn()) {
			for (int i = 0; i < getAInCount(); i++) {
				if ((getData()[2] & F_BIGENDIAN) == F_BIGENDIAN) {
					Conversion.shortToBytes(data, offset, aInVals[i]);
				} else {
					Conversion.shortToBytesLE(data, offset, aInVals[i]);
				}
				
				offset += 2;
			}
		}
		
		if (!writeAOut()) {
			for (int i = 0; i < getAOutCount(); i++) {
				if ((getData()[3] & F_BIGENDIAN) == F_BIGENDIAN) {
					Conversion.shortToBytes(data, offset, aOutVals[i]);
				} else {
					Conversion.shortToBytesLE(data, offset, aOutVals[i]);
				}
				
				offset += 2;
			}
		}
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Get the number of discrete inputs being read/written by this message.
	 */
	public byte getDInCount() {
		return (byte)(getData()[0] & 0x3F);
	}
	
	/**
	 * Whether or not the discrete inputs in the remote device are being written by
	 *   this message.
	 * @return True = remote data points are being written; False = remote data points
	 *   are being read.
	 */
	public boolean writeDIn() {
		return ((getData()[0] & F_WRITE) == F_WRITE);
	}
	
	/**
	 * Get the number of discrete outputs being read/written by this message.
	 */
	public byte getDOutCount() {
		return (byte)(getData()[1] & 0x3F);
	}
	
	/**
	 * Whether or not the discrete outputs in the remote device are being written by
	 *   this message.
	 * @return True = remote data points are being written; False = remote data points
	 *   are being read.
	 */
	public boolean writeDOut() {
		return ((getData()[1] & F_WRITE) == F_WRITE);
	}
	
	/**
	 * Get the number of analog inputs being read/written by this message.
	 */
	public byte getAInCount() {
		return (byte)(getData()[2] & 0x3F);
	}
	
	/**
	 * Whether or not the analog inputs in the remote device are being written by
	 *   this message.
	 * @return True = remote data points are being written; False = remote data points
	 *   are being read.
	 */
	public boolean writeAIn() {
		return ((getData()[2] & F_WRITE) == F_WRITE);
	}
	
	/**
	 * Get the number of analog outputs being read/written by this message.
	 */
	public byte getAOutCount() {
		return (byte)(getData()[3] & 0x3F);
	}
	
	/**
	 * Whether or not the analog outputs in the remote device are being written by
	 *   this message.
	 * @return True = remote data points are being written; False = remote data points
	 *   are being read.
	 */
	public boolean writeAOut() {
		return ((getData()[3] & F_WRITE) == F_WRITE);
	}
	
	/**
	 * If writing data, get the discrete inputs being written at the specified byte index.
	 * @param index The index to read.
	 * @return The discrete inputs being written by the byte at the specified index.
	 */
	public byte getDIn(int index) {
		// If reading, there are none
		if (!writeDIn()) throw new ArrayIndexOutOfBoundsException("There are no discrete inputs in this message");
		if (index > getDInCount()) throw new ArrayIndexOutOfBoundsException("Max index for discrete ins is " + getDInCount() + ", cannot read " + index);
		
		return getData()[4 + index];
	}
	
	/**
	 * If writing data, get the discrete outputs being written at the specified byte index.
	 * @param index The index to read.
	 * @return The discrete outputs being written by the byte at the specified index.
	 */
	public byte getDOut(int index) {
		// If reading, there are none
		if (!writeDOut()) throw new ArrayIndexOutOfBoundsException("There are no discrete outputs in this message");
		if (index > getDOutCount()) throw new ArrayIndexOutOfBoundsException("Max index for discrete outs is " + getDOutCount() + ", cannot read " + index);
		
		if (writeDIn()) {
			index += getDInCount();
		}
		
		return getData()[4 + index];
	}
	
	/**
	 * If writing data, get the analog inputs being written at the specified byte index.
	 * @param index The index to read.
	 * @return The analog inputs being written by the byte at the specified index.
	 */
	public short getAIn(int index) {
		// If reading, there are none
		if (!writeAIn()) throw new ArrayIndexOutOfBoundsException("There are no analog inputs in this message");
		if (index > getAInCount()) throw new ArrayIndexOutOfBoundsException("Max index for analog ins is " + getAInCount() + ", cannot read " + index);
		
		index *= 2; // These are shorts
		
		if (writeDIn()) {
			index += getDInCount();
		}
		
		if (writeDOut()) {
			index += getDOutCount();
		}
		
		if ((getData()[2] & F_BIGENDIAN) == F_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 4 + index);
		} else {
			return Conversion.bytesToShortLE(getData(), 4 + index);
		}
	}
	
	/**
	 * If writing data, get the analog outputs being written at the specified byte index.
	 * @param index The index to read.
	 * @return The analog outputs being written by the byte at the specified index.
	 */
	public short getAOut(int index) {
		// If reading, there are none
		if (!writeAOut()) throw new ArrayIndexOutOfBoundsException("There are no analog outputs in this message");
		if (index > getAOutCount()) throw new ArrayIndexOutOfBoundsException("Max index for analog outs is " + getAOutCount() + ", cannot read " + index);
		
		index *= 2; // These are shorts
		
		if (writeDIn()) {
			index += getDInCount();
		}
		
		if (writeDOut()) {
			index += getDOutCount();
		}
		
		if (writeAIn()) {
			index += getAInCount() * 2; // These are also shorts
		}
		
		if ((getData()[3] & F_BIGENDIAN) == F_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 4 + index);
		} else {
			return Conversion.bytesToShortLE(getData(), 4 + index);
		}
	}
	
	/**
	 * Unless val == getCommand(), throws an IllegalArgumentException.
	 */
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Describe the command byte of this message.
	 */
	public String getCommandString() {
		return "IOXchg";
	}
	
	/**
	 * Describe the data payload of this message.
	 * @param indent The string to use to indent each line.
	 */
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		int writeDInLimit;
		int writeDOutLimit;
		int writeAInLimit;
		int writeAOutLimit;
		
		if (writeDIn()) {
			writeDInLimit = 4;
		} else {
			writeDInLimit = -1;
		}
		
		if (writeDOut()) {
			if (writeDInLimit > 0) {
				writeDOutLimit = writeDInLimit + getDInCount();
			} else {
				writeDOutLimit = 4;
			}
		} else {
			writeDOutLimit = -1;
		}
		
		if (writeAIn()) {
			if (writeDOutLimit > 0) {
				writeAInLimit = writeDOutLimit + getDOutCount();
			} else if (writeDInLimit > 0) {
				writeAInLimit = writeDInLimit + getDInCount();
			} else {
				writeAInLimit = 4;
			}
		} else {
			writeAInLimit = -1;
		}
		
		if (writeAOut()) {
			if (writeAInLimit > 0) {
				writeAOutLimit = writeAInLimit + getAInCount();
			} else if (writeDOutLimit > 0) {
				writeAOutLimit = writeDOutLimit + getDOutCount();
			} else if (writeDInLimit > 0) {
				writeAOutLimit = writeDInLimit + getDInCount();
			} else {
				writeAOutLimit = 4;
			}
		} else {
			writeAOutLimit = -1;
		}
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Discrete In %s %d %s", (writeDIn() ? "Write" : "Read"), getDInCount(), " bytes"));
			} else if (i == 1) {
				ans.append(String.format(" Discrete Out %s %d %s", (writeDOut() ? "Write" : "Read"), getDOutCount(), " bytes"));
			} else if (i == 2) {
				ans.append(String.format(" Analog In %s %d %s", (writeAIn() ? "Write" : "Read"), getAInCount(), " bytes"));
			} else if (i == 3) {
				ans.append(String.format(" Analog Out %s %d %s", (writeAOut() ? "Write" : "Read"), getAOutCount(), " bytes"));
			} else if (i == writeDInLimit) {
				ans.append(" Writing Discrete Inputs");
			} else if (i == writeDOutLimit) {
				ans.append(" Writing Discrete Outputs");
			} else if (i == writeAInLimit) {
				ans.append(" Writing Analog Inputs");
			} else if (i == writeAOutLimit) {
				ans.append(" Writing Analog Outputs");
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe the data payload of an acknowledgment to this message.
	 * @param ack The acknowledgment to describe.
	 * @param indent The string to use to indent each line.
	 */
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		int writeDInLimit;
		int writeDOutLimit;
		int writeAInLimit;
		int writeAOutLimit;
		
		if (writeDIn(ack)) {
			writeDInLimit = 4;
		} else {
			writeDInLimit = -1;
		}
		
		if (writeDOut(ack)) {
			if (writeDInLimit > 0) {
				writeDOutLimit = writeDInLimit + getDInCount(ack);
			} else {
				writeDOutLimit = 4;
			}
		} else {
			writeDOutLimit = -1;
		}
		
		if (writeAIn(ack)) {
			if (writeDOutLimit > 0) {
				writeAInLimit = writeDOutLimit + getDOutCount(ack);
			} else if (writeDInLimit > 0) {
				writeAInLimit = writeDInLimit + getDInCount(ack);
			} else {
				writeAInLimit = 4;
			}
		} else {
			writeAInLimit = -1;
		}
		
		if (writeAOut(ack)) {
			if (writeAInLimit > 0) {
				writeAOutLimit = writeAInLimit + getAInCount(ack);
			} else if (writeDOutLimit > 0) {
				writeAOutLimit = writeDOutLimit + getDOutCount(ack);
			} else if (writeDInLimit > 0) {
				writeAOutLimit = writeDInLimit + getDInCount(ack);
			} else {
				writeAOutLimit = 4;
			}
		} else {
			writeAOutLimit = -1;
		}
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Discrete In %s %d %s", (writeDIn(ack) ? "Write" : "Read"), getDInCount(ack), " bytes"));
			} else if (i == 1) {
				ans.append(String.format(" Discrete Out %s %d %s", (writeDOut() ? "Write" : "Read"), getDOutCount(ack), " bytes"));
			} else if (i == 2) {
				ans.append(String.format(" Analog In %s %d %s", (writeAIn(ack) ? "Write" : "Read"), getAInCount(ack), " bytes"));
			} else if (i == 3) {
				ans.append(String.format(" Analog Out %s %d %s", (writeAOut(ack) ? "Write" : "Read"), getAOutCount(ack), " bytes"));
			} else if (i == writeDInLimit) {
				ans.append(" Writing Discrete Inputs");
			} else if (i == writeDOutLimit) {
				ans.append(" Writing Discrete Outputs");
			} else if (i == writeAInLimit) {
				ans.append(" Writing Analog Inputs");
			} else if (i == writeAOutLimit) {
				ans.append(" Writing Analog Outputs");
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

/*
 * UDR_USER.java
 *
 * A USER UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

/**
 * Used to implement user-defined messages for communications with user-written
 *   drivers within a device. If you plan on using user-defined messages,
 *   you may wish to subclass this.
 *
 * @author Jonathan Pearson
 */
public class UDR_User extends UDRMessage {
	/**
	 * Construct a new User message.
	 * @param cmd The command of the message (between UDRMessage.C_USER and UDRMessage.C_USERMAX).
	 * @param vals The data payload of this message.
	 */
	public UDR_User(byte cmd, byte[] vals) {
		setCommand(cmd);
		
		byte[] data = new byte[vals.length];
		
		System.arraycopy(vals, 0, data, 0, data.length);
		
		setData(data);
	}
	
	/**
	 * Construct an acknowledgment to this message.
	 * @param vals The data payload for the acknowledgment.
	 * @return An acknowledgment to this message.
	 */
	public UDR_Acknowledge acknowledge(byte[] vals) {
		byte[] data = new byte[vals.length];
	
		System.arraycopy(vals, 0, data, 0, data.length);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Change the command byte of this message.
	 * @param val The new command byte
	 * @throws IllegalArgumentException If the value is not in the
	 *   range [UDRMessage.C_USER, UDRMessage.C_USERMAX].
	 */
	public void setCommand(byte val) {
		if (val < UDRMessage.C_USER || val > UDRMessage.C_USERMAX) {
			throw new IllegalArgumentException("Cannot change the type of a USER message out of USER command value range");
		} else {
			super.setCommand(val);
		}
	}
	
	/**
	 * Describe the command byte of this message.
	 */
	public String getCommandString() {
		return "User " + getCommand();
	}
}

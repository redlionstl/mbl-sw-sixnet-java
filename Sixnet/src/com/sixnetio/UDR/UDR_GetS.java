/*
 * UDR_GETS.java
 *
 * A GETS UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.Conversion;

/**
 * A message to get a series of discrete registers in a device as if they were a
 * null-terminated string.
 *
 * @author Jonathan Pearson
 */
public class UDR_GetS extends UDRMessage {
	// Acknowledgment translation functions
	/**
	 * Verify that the given ACK matches this message.
	 * 
	 * @param ack The ACK to verify.
	 * @return True = the ACK matches this message, False = the ACK does not
	 *   match.
	 */
	@Override
	public boolean verifyAck(UDR_Acknowledge ack) {
		return (ack.getData().length >= 3 &&
		        getStart(ack) == getStart());
	}
	
	/**
	 * Get the type of data in the ACK.
	 * 
	 * @param ack The ACK.
	 * @return The type of data (see {@link UDRMessage#T_C_DIN} and similar).
	 */
	public static byte getType(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	/**
	 * Get the starting register number of the range of registers retrieved.
	 * 
	 * @param ack The ACK.
	 * @return The starting register number.
	 */
	public static short getStart(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 1);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 1);
		}
	}
	
	/**
	 * Get the string returned in the ACK.
	 * 
	 * @param ack The ACK.
	 * @return The string returned by the ACK, minus the terminating NULL.
	 */
	public static String getVal(UDR_Acknowledge ack) {
		return new String(ack.getData(), 3, ack.getData().length - 3);
	}
	
	/**
	 * Construct a new GetS message from a byte array.
	 * 
	 * @param data The byte array containing the message.
	 */
	protected UDR_GetS(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_GETS);
		setData(data);
	}
	
	/**
	 * Construct a new GetS message from its components.
	 * 
	 * @param type The type of data to retrieve (see {@link UDRMessage#T_D_DIN}
	 *   and similar).
	 * @param start The starting register (bit address).
	 */
	public UDR_GetS(byte type, short start) {
		super.setCommand(UDRMessage.C_GETS);
		
		byte[] data = new byte[3];
		data[0] = type;
		
		if ((type & T_BIGENDIAN) == T_BIGENDIAN) {
			Conversion.shortToBytes(data, 1, start);
		} else {
			Conversion.shortToBytesLE(data, 1, start);
		}
		
		setData(data);
	}
	
	/**
	 * Construct an acknowledgment for this GetS message.
	 * 
	 * @param val The string to return.
	 * @return An acknowledgment message matching this GetS message.
	 */
	public UDR_Acknowledge acknowledge(String val) {
		byte[] data = new byte[3 + val.length()];
	
		System.arraycopy(getData(), 0, data, 0, 3);
		System.arraycopy(val.getBytes(), 0, data, 3, val.getBytes().length);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Get the data type requested by this message.
	 */
	public byte getType() {
		return getData()[0];
	}
	
	/**
	 * Get the starting register number requested by this message.
	 */
	public short getStart() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 1);
		} else {
			return Conversion.bytesToShortLE(getData(), 1);
		}
	}
	
	/**
	 * Set the command byte for this message (will throw an exception).
	 */
	@Override
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Get the name of this message as a string.
	 */
	@Override
	public String getCommandString() {
		return "GetS";
	}
	
	/**
	 * Describe the data contained within this message.
	 * 
	 * @param indent A string prepended to each line of the returned string.
	 * @return A description of this message.
	 */
	@Override
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType())));
			} else if (i == 1) {
				ans.append(String.format(" Start = %s", getStart()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe the data contained within an acknowledgment to this message.
	 * 
	 * @param ack The acknowledgment to describe.
	 * @param indent A string prepended to each line of the returned string.
	 * @return A description of the ACK.
	 */
	@Override
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType(ack))));
			} else if (i == 1) {
				ans.append(String.format(" Start = %d", getStart(ack)));
			} else if (i == 3) {
				ans.append(new String(data, 3, data.length - 3));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

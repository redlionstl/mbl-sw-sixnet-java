/*
 * UDR_GETA.java
 *
 * A GETA UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.Conversion;

public class UDR_GetA extends UDRMessage {
	// Acknowledgment translation functions
	/**
	 * Verify that the given ACK matches this message.
	 * 
	 * @param ack The ACK to verify.
	 * @return True = the ACK matches this message, False = does not match.
	 */
	@Override
	public boolean verifyAck(UDR_Acknowledge ack) {
		if (ack.getData().length < 5) return false; // Need to have at least 5 bytes
		
		return (ack.getData().length >= (5 + getNum() * 2) &&
		        getStart(ack) == getStart() &&
		        getNum(ack) == getNum());
	}
	
	/**
	 * Get the data type retrieved by this ACK.
	 * 
	 * @param ack The ACK.
	 * @return The data type (see {@link UDRMessage#T_D_AIN} and similar).
	 */
	public static byte getType(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	/**
	 * Get the starting register number retrieved by this ACK.
	 * 
	 * @param ack The ACK.
	 * @return The starting register number.
	 */
	public static short getStart(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 1);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 1);
		}
	}
	
	/**
	 * Get the number of registers retrieved by this ACK.
	 * 
	 * @param ack The ACK.
	 * @return The number of registers retrieved by the ACK.
	 */
	public static short getNum(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 3);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 3);
		}
	}
	
	/**
	 * Get the specified value from the ACK.
	 * 
	 * @param ack The ACK.
	 * @param index The index of the value to retrieve.
	 * @return The value at that index.
	 */
	public static short getVal(UDR_Acknowledge ack, int index) {
		if (index > getNum(ack)) throw new ArrayIndexOutOfBoundsException(String.format("Index %d out of range [0, %d)", index, getNum(ack)));
		
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 5 + index * 2);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 5 + index * 2);
		}
	}
	
	/**
	 * Get all values retrieved by the ACK.
	 * 
	 * @param ack The ACK.
	 * @return All values retrieved by the ACK.
	 */
	public static short[] getVals(UDR_Acknowledge ack) {
		short[] ans = new short[getNum(ack)];
		
		for (int i = 0; i < ans.length; i++) {
			ans[i] = getVal(ack, i);
		}
		
		return ans;
	}
	
	/**
	 * Construct a new GetA message from a byte array (received data).
	 * 
	 * @param data The data to parse into a GetA message.
	 */
	protected UDR_GetA(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_GETA);
		setData(data);
	}
	
	/**
	 * Construct a new GetA message from its constituent parts.
	 * 
	 * @param type The type of data to retrieve.
	 * @param start The starting register.
	 * @param num The number of registers to retrieve.
	 */
	public UDR_GetA(byte type, short start, short num) {
		super.setCommand(UDRMessage.C_GETA);
		
		byte[] data = new byte[5];
		data[0] = type;
		
		if ((type & T_BIGENDIAN) == T_BIGENDIAN) {
			Conversion.shortToBytes(data, 1, start);
			Conversion.shortToBytes(data, 3, num);
		} else {
			Conversion.shortToBytesLE(data, 1, start);
			Conversion.shortToBytesLE(data, 3, num);
		}
		
		setData(data);
	}
	
	/**
	 * Acknowledge this message.
	 * 
	 * @param vals The values to return in the ACk.
	 * @return An ACK that matches this message.
	 */
	public UDR_Acknowledge acknowledge(short[] vals) {
		byte[] data = new byte[5 + vals.length * 2];
	
		System.arraycopy(getData(), 0, data, 0, 5);
		
		int offset = 5;
		for (int i = 0; i < vals.length; i++) {
			if ((data[0] & T_BIGENDIAN) == T_BIGENDIAN) {
				Conversion.shortToBytes(data, offset, vals[i]);
			} else { // big-endian
				Conversion.shortToBytesLE(data, offset, vals[i]);
			}
			
			offset += 2;
		}
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Get the data type being requested by this message.
	 */
	public byte getType() {
		return getData()[0];
	}
	
	/**
	 * Get the starting register being requested.
	 */
	public short getStart() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 1);
		} else {
			return Conversion.bytesToShortLE(getData(), 1);
		}
	}
	
	/**
	 * Get the number of registers being requested.
	 */
	public short getNum() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 3);
		} else {
			return Conversion.bytesToShortLE(getData(), 3);
		}
	}
	
	/**
	 * Set the command byte (will throw an exception).
	 */
	@Override
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Get the descriptive name of this command.
	 */
	@Override
	public String getCommandString() {
		return "GetA";
	}
	
	/**
	 * Describe the data in this message.
	 * 
	 * @param indent A string to prepend to all lines of the returned string.
	 * @return A description of this message.
	 */
	@Override
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType())));
			} else if (i == 1) {
				ans.append(String.format(" Start = %s", getStart()));
			} else if (i == 3) {
				ans.append(String.format(" Number = %s", getNum()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe an ACK to this message.
	 * 
	 * @param ack The ACK to describe.
	 * @param indent A string to prepend to all lines of the returned string.
	 * @return A description of the ACK.
	 */
	@Override
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType(ack))));
			} else if (i == 1) {
				ans.append(String.format(" Start = %d", getStart(ack)));
			} else if (i == 3) {
				ans.append(String.format(" Number = %d", getNum(ack)));
			} else if (i >= 5 && (i - 5) % 2 == 0) {
				ans.append(String.format(" Analog value %d", getVal(ack, (i - 5) / 2)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

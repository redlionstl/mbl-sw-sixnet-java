/*
 * FILESYS_WRITE.java
 *
 * A WRITE FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

import java.util.*;

public class FS_Write extends UDR_Filesys {
	// Acknowledgment translation functions
	public boolean verifyAck(UDR_Acknowledge ack) {
	    if (ack.getData().length < 4) return false;
	    
	    try {
		    // Grab the last piece of the last block; exception means bad format
		    getNumber(ack, getBlockCount() - 1);
		    
			return true;
	    } catch (ArrayIndexOutOfBoundsException ex) {
		    return false;
	    }
    }
    
	public static int getBlockCount(UDR_Acknowledge ack) {
		int size = ack.getData().length;
		size -= 4; // alias
		size /= 7; // err (1) + pos (4) + num (2)
		return size;
	}
	
	public static int getAlias(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 0);
	}
	
	public static byte getError(UDR_Acknowledge ack, int index) {
		byte[] data = ack.getData();
		int offset = 4; // alias
		offset += index * 7; // size of a block
		
		return data[offset];
	}
	
	public static String translateError(UDR_Acknowledge ack, int index) {
		return translateError(getError(ack, index));
	}
	
	public static String translateError(byte b) {
		switch (b) {
			case E_NO_ERROR:
			    return "No Error";
			case E_INVALID_ALIAS:
			    return "Invalid Alias";
			case E_SEEK_ERROR:
			    return "Seek Error";
			case E_FLASH_BUSY:
				return "Flash Busy";
			default:
			    return "(Unknown Error)";
		}
	}
	
	public static int getPosition(UDR_Acknowledge ack, int index) {
		byte[] data = ack.getData();
		int offset = 4; // alias
		offset += index * 7; // size of a block
		offset++; // skip error code
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static short getNumber(UDR_Acknowledge ack, int index) {
		byte[] data = ack.getData();
		int offset = 4; // alias
		offset += index * 7; // size of a block
		offset++; // skip error code
		offset += 4; // skip the position
		
		return Conversion.bytesToShort(data, offset);
	}
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_INVALID_ALIAS = 0x01,
	                         E_SEEK_ERROR = 0x02,
	                         E_FLASH_BUSY = 0x03;
	
	public FS_Write(byte[] data) {
		super(UDR_Filesys.C_WRITE);
		setData(data);
	}
	
	public FS_Write(int alias, int position, short number, byte[] writeData, Object... writeBlocks) {
		super(UDR_Filesys.C_WRITE);
		
		int size = 10 + number; // Counts up as we add more read blocks
		byte[] startData = new byte[size];
		
		Conversion.intToBytes(startData, 0, alias);
		Conversion.intToBytes(startData, 4, position);
		Conversion.shortToBytes(startData, 8, number);
		System.arraycopy(writeData, 0, startData, 10, number);
		
		Vector<byte[]> moreBlocks = new Vector<byte[]>();
		for (int i = 0; i < writeBlocks.length; i += 3) {
			int pos = Integer.parseInt(writeBlocks[i].toString());
			short num = Short.parseShort(writeBlocks[i + 1].toString());
			byte[] blockData = (byte[])writeBlocks[i + 2];
			
			byte[] moreData = new byte[6 + num];
			Conversion.intToBytes(moreData, 0, pos);
			Conversion.shortToBytes(moreData, 4, num);
			System.arraycopy(blockData, 0, moreData, 6, num);
			
			size += moreData.length;
			moreBlocks.add(moreData);
		}
		
		byte[] realData = new byte[size];
		int offset = 0;
		
		System.arraycopy(startData, 0, realData, offset, startData.length);
		offset += startData.length;
		
		for (byte[] moreData : moreBlocks) {
			System.arraycopy(moreData, 0, realData, offset, moreData.length);
			offset += moreData.length;
		}
		
		setData(realData);
	}
	
	public UDR_Acknowledge acknowledge(int alias, byte error, int position, short number, Object... writeBlocks) {
		int size = 4 + 1 + 4 + 2; // alias + error + position + number
		byte[] startData = new byte[size];
		
		Conversion.intToBytes(startData, 0, alias);
		startData[4] = error;
		Conversion.intToBytes(startData, 5, position);
		Conversion.shortToBytes(startData, 7, number);
		
		Vector<byte[]> moreBlocks = new Vector<byte[]>();
		for (int i = 0; i < writeBlocks.length; i += 4) {
			byte errorCode = Byte.parseByte(writeBlocks[i].toString());
			int pos = Integer.parseInt(writeBlocks[i + 1].toString());
			short num = Short.parseShort(writeBlocks[i + 2].toString());
			
			int tempSize = 1 + 4 + 2;
			size += tempSize;
			byte[] addData = new byte[tempSize];
			
			addData[0] = errorCode;
			Conversion.intToBytes(addData, 1, pos);
			Conversion.shortToBytes(addData, 5, num);
			
			moreBlocks.add(addData);
		}
		
		byte[] realData = new byte[size];
		
		int offset = 0;
		System.arraycopy(startData, 0, realData, 0, startData.length);
		offset += startData.length;
		
		for (byte[] moreData : moreBlocks) {
			System.arraycopy(moreData, 0, realData, offset, moreData.length);
			offset += moreData.length;
		}
		
		UDR_Acknowledge ack = new UDR_Acknowledge(realData);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
        
        return ack;
    }
	
	public int getAlias() {
		return Conversion.bytesToInt(getData(), 0);
	}
	
	public int getBlockCount() {
		byte[] data = getData();
		int offset = 4; // skip alias
		int counter = 0;
		
		while (offset < data.length) {
			offset += 4; // skip position
			offset += 2 + Conversion.bytesToShort(data, offset); // number of bytes (2) + each byte read
			
			counter++;
		}
		
		return counter;
	}
	
	public int getPosition(int index) {
		byte[] data = getData();
		int offset = 4; // skip alias
		int counter = 0;
		
		while (counter < index) {
			offset += 4; // skip position
			offset += 2 + Conversion.bytesToShort(data, offset); // number of bytes + each data byte
			
			counter++;
		}
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public short getNumber(int index) {
		byte[] data = getData();
		int offset = 4; // skip alias
		int counter = 0;
		
		while (counter < index) {
			offset += 4; // skip position
			offset += 2 + Conversion.bytesToShort(data, offset); // number of bytes + each data byte
			
			counter++;
		}
		
		offset += 4; // skip position
		
		return Conversion.bytesToShort(data, offset);
	}
	
	public byte[] getData(int index) {
		byte[] data = getData();
		int offset = 4; // skip alias
		int counter = 0;
		
		while (counter < index) {
			offset += 4; // skip position
			offset += 2 + Conversion.bytesToShort(data, offset); // number of bytes + each data byte
			
			counter++;
		}
		
		offset += 4; // skip position
		
		byte[] result = new byte[Conversion.bytesToShort(data, offset)];
		System.arraycopy(result, 0, data, offset, result.length);
		
		return result;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	public String getSubCommandString() {
		return "Write";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		int counter = 0;
		int nextBlock = 4;
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Alias = %d", getAlias()));
			} else if (i == nextBlock) {
				ans.append(String.format(" Block %d position = %d", counter + 1, getPosition(counter)));
			} else if (i == nextBlock + 4) {
				ans.append(String.format(" Block %d size = %d", counter + 1, getNumber(counter)));
			} else if (i == nextBlock + 6) {
				ans.append(String.format(" Block %d file data", counter + 1));
				nextBlock = i + getNumber(counter);
				counter++;
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		int counter = 0;
		int nextBlock = 4;
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Alias = %d", getAlias(ack)));
			} else if (i == nextBlock) {
				ans.append(String.format(" Block %d error '%s'", counter + 1, translateError(ack, counter)));
			} else if (i == nextBlock + 1) {
				ans.append(String.format(" Block %d position = %d", counter + 1, getPosition(ack, counter)));
			} else if (i == nextBlock + 5) {
				ans.append(String.format(" Block %d size = %d", counter + 1, getNumber(ack, counter)));
				nextBlock = i + 2;
				counter++;
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

/*
 * FILESYS_FREESPACE.java
 *
 * A FREESPACE FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

public class FS_FreeSpace extends UDR_Filesys {
	// Acknowledgment translation functions
	public boolean verifyAck(UDR_Acknowledge ack) {
	    return (ack.getData().length == 1 || ack.getData().length >= 6);
    }
    
	public static byte getError(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static byte getCompression(UDR_Acknowledge ack) {
		if (getError(ack) != FS_FreeSpace.E_NO_ERROR) return 0; // doesn't exist
		
		return ack.getData()[1];
	}
	
	public static int getMem(UDR_Acknowledge ack) {
		if (getError(ack) != FS_FreeSpace.E_NO_ERROR) return 0; // doesn't exist
		
		return Conversion.bytesToInt(ack.getData(), 2);
	}
	
	public static String translateOptions(byte options) {
		switch (options) {
			case O_NONE:
				return "No Options";
			default:
				return "(Unknown Options)";
		}
	}
	
	public static String translateError(UDR_Acknowledge ack) {
		return translateError(getError(ack));
	}
	
	public static String translateError(byte error) {
		switch (error) {
			case E_NO_ERROR: return "No Error";
			case E_INVALID_OPTION: return "Invalid Option";
			case E_ACCESS_ERROR: return "Access Error";
			default: return "(Unknown Error)";
		}
	}
	
	public static String translateCompression(UDR_Acknowledge ack) {
		return translateCompression(getCompression(ack));
	}
	
	public static String translateCompression(byte compression) {
		switch (compression) {
			case COMPRESS_ENABLED: return "Enabled";
			case COMPRESS_NONE: return "Disabled";
			default: return "(Unknown Compression)";
		}
	}
	
	
	// Options
	public static final byte O_NONE = 0x00;
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_INVALID_OPTION = 0x01,
	                         E_ACCESS_ERROR = 0x02;
	
	// Return values
	public static final byte COMPRESS_NONE = 0x00, // No compression
	                         COMPRESS_ENABLED = 0x01; // Compressed
	
	public FS_FreeSpace(byte[] data) {
		super(UDR_Filesys.C_FREESPACE);
		setData(data);
	}
	
	public FS_FreeSpace(byte options, String path) {
		super(UDR_Filesys.C_FREESPACE);
		
		byte[] data = new byte[1 + 1 + path.length() + 1];
		
		if (options != O_NONE) throw new IllegalArgumentException("FILESYS FREESPACE must use O_NONE");
		
		data[0] = options;
		data[1] = (byte)((path.length() + 1) & 0xFF); // add the null terminator
		
		byte[] str = path.getBytes();
		if (str.length != path.length()) throw new RuntimeException("String length differs from byte length!");
		
		System.arraycopy(str, 0, data, 2, str.length);
		data[data.length - 1] = 0; // null terminator
		
		setData(data);
	}
	
	public UDR_Acknowledge acknowledge(byte error) {
		byte[] data = new byte[1];
		
		data[0] = error;
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
        
		return ack;
	}
	
	
	public UDR_Acknowledge acknowledge(byte error, byte compression, int mem) {
		byte[] data = new byte[6];
		
		data[0] = error;
		data[1] = compression;
		
		Conversion.intToBytes(data, 2, mem);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
        
		return ack;
	}
	
	public byte getOptions() {
		return getData()[0];
	}
	
	public byte getPathLength() {
		return getData()[1];
	}
	
	public String getPath() {
		byte[] data = getData();
		int offset = 2; // skip options & length
		
		String ans = "";
		while (data[offset] != 0) {
			ans += (char)data[offset++];
		}
		
		return ans;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	public String getSubCommandString() {
		return "FreeSpace";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Options '%s'", translateOptions(getOptions())));
			} else if (i == 1) {
				ans.append(String.format(" Path length = %d", getPathLength()));
			} else if (i == 2) {
				ans.append(String.format(" Path '%s'", getPath()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Error '%s'", translateError(ack)));
			} else if (i == 1) {
				ans.append(String.format(" Compression '%s'", translateCompression(ack)));
			} else if (i == 2) {
				ans.append(String.format(" Free space = %d", getMem(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

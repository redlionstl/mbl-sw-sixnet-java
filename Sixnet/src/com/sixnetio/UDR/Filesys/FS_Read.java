/*
 * FILESYS_READ.java
 *
 * A READ FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

import java.util.*;

public class FS_Read extends UDR_Filesys {
	// Acknowledgment translation functions
	public boolean verifyAck(UDR_Acknowledge ack) {
	    try {
		    for (int i = 0; i < getBlockCount(); i++) {
				byte[] block = getData(ack, i);
				if (block.length != getNumber(ack, i)) { // It is okay if the station returns fewer bytes than expected, as long is it returns as many as it says it did
					Utils.debug("Incorrect number of bytes (" + block.length + ") in block " + i + ", expected " + getNumber(ack, i));
					return false;
				}
		    }
		    
		    return true;
	    } catch (ArrayIndexOutOfBoundsException ex) {
		    return false;
	    }
    }
    
	public static int getAlias(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 0);
	}
	
	public static int getBlockCount(UDR_Acknowledge ack) {
		byte[] data = ack.getData();
		int offset = 4; // skip alias
		int counter = 0;
		
		while (offset < data.length) {
			offset++; // skip error code
			offset += 4; // skip position
			offset += 2 + Conversion.bytesToShort(data, offset); // number of bytes (2) + each byte read
			
			counter++;
		}
		
		return counter;
	}
	
	public static byte getError(UDR_Acknowledge ack, int index) {
		byte[] data = ack.getData();
		int offset = 4; // skip alias
		int counter = 0;
		
		while (counter < index) {
			offset++; // skip error code
			offset += 4; // skip position
			offset += 2 + Conversion.bytesToShort(data, offset); // number of bytes + each byte read
			
			counter++;
		}
		
		return data[offset];
	}
	
	public static int getPosition(UDR_Acknowledge ack, int index) {
		byte[] data = ack.getData();
		int offset = 4; // skip alias
		int counter = 0;
		
		while (counter < index) {
			offset++; // skip error code
			offset += 4; // skip position
			offset += 2 + Conversion.bytesToShort(data, offset); // number of bytes + each byte read
			
			counter++;
		}
		
		offset++; // skip error code
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static short getNumber(UDR_Acknowledge ack, int index) {
		byte[] data = ack.getData();
		int offset = 4; // skip alias
		int counter = 0;
		
		while (counter < index) {
			offset++; // skip error code
			offset += 4; // skip position
			offset += 2 + Conversion.bytesToShort(data, offset); // number of bytes + each byte read
			
			counter++;
		}
		
		offset++; // skip error code
		offset += 4; // skip position
		
		return Conversion.bytesToShort(data, offset);
	}
	
	public static byte[] getData(UDR_Acknowledge ack, int index) {
		byte[] data = ack.getData();
		int offset = 4; // skip alias
		int counter = 0;
		
		while (counter < index) {
			offset++; // skip error code
			offset += 4; // skip position
			offset += 2 + Conversion.bytesToShort(data, offset); // number of bytes + each byte read
			
			counter++;
		}
		
		offset++; // skip error code
		offset += 4; // skip position
		
		byte[] result = new byte[Conversion.bytesToShort(data, offset)];
		offset += 2; // skip number, now that we've read it
		
		System.arraycopy(data, offset, result, 0, result.length);
		
		return result;
	}
	
	public static String translateError(UDR_Acknowledge ack, int index) {
		return translateError(getError(ack, index));
	}
	
	public static String translateError(byte b) {
		switch (b) {
			case E_NO_ERROR:
			    return "No Error";
			case E_INVALID_ALIAS:
			    return "Invalid Alias";
			case E_SEEK_ERROR:
			    return "Seek Error";
			default:
			    return "(Unknown Error)";
		}
	}
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_INVALID_ALIAS = 0x01,
	                         E_SEEK_ERROR = 0x02;
	
	public FS_Read(byte[] data) {
		super(UDR_Filesys.C_READ);
		setData(data);
	}
	
	public FS_Read(int alias, int position, short number, Object... readBlocks) {
		super(UDR_Filesys.C_READ);
		
		int size = 10; // Counts up as we add more read blocks
		byte[] startData = new byte[size];
		
		Conversion.intToBytes(startData, 0, alias);
		Conversion.intToBytes(startData, 4, position);
		Conversion.shortToBytes(startData, 8, number);
		
		Vector<byte[]> moreBlocks = new Vector<byte[]>();
		for (int i = 0; i < readBlocks.length; i += 2) {
			int pos = Integer.parseInt(readBlocks[i].toString());
			short num = Short.parseShort(readBlocks[i + 1].toString());
			
			byte[] moreData = new byte[6];
			Conversion.intToBytes(moreData, 0, pos);
			Conversion.shortToBytes(moreData, 4, num);
			
			size += moreData.length;
			moreBlocks.add(moreData);
		}
		
		byte[] realData = new byte[size];
		int offset = 0;
		
		System.arraycopy(startData, 0, realData, offset, startData.length);
		offset += startData.length;
		
		for (byte[] moreData : moreBlocks) {
			System.arraycopy(moreData, 0, realData, offset, moreData.length);
			offset += moreData.length;
		}
		
		setData(realData);
	}
	
	public UDR_Acknowledge acknowledge(int alias, byte error, int position, short number, byte[] readData, Object... readBlocks) {
		int size = 4 + 1 + 4 + 2 + number; // alias + error + position + number + readData
		byte[] startData = new byte[size];
		
		Conversion.intToBytes(startData, 0, alias);
		startData[4] = error;
		Conversion.intToBytes(startData, 5, position);
		Conversion.shortToBytes(startData, 7, number);
		System.arraycopy(readData, 0, startData, 9, number);
		
		Vector<byte[]> moreBlocks = new Vector<byte[]>();
		for (int i = 0; i < readBlocks.length; i += 4) {
			byte errorCode = Byte.parseByte(readBlocks[i].toString());
			int pos = Integer.parseInt(readBlocks[i + 1].toString());
			short num = Short.parseShort(readBlocks[i + 2].toString());
			byte[] moreData = (byte[])readBlocks[i + 3];
			
			int tempSize = 1 + 4 + 2 + num;
			size += tempSize;
			byte[] addData = new byte[tempSize];
			
			addData[0] = errorCode;
			Conversion.intToBytes(addData, 1, pos);
			Conversion.shortToBytes(addData, 5, num);
			System.arraycopy(moreData, 0, addData, 7, num);
			
			moreBlocks.add(addData);
		}
		
		byte[] realData = new byte[size];
		
		int offset = 0;
		System.arraycopy(startData, 0, realData, 0, startData.length);
		offset += startData.length;
		
		for (byte[] moreData : moreBlocks) {
			System.arraycopy(moreData, 0, realData, offset, moreData.length);
			offset += moreData.length;
		}
		
		UDR_Acknowledge ack = new UDR_Acknowledge(realData);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
        
        return ack;
    }
	
	public int getBlockCount() {
		int size = getData().length;
		size -= 4; // alias
		size /= 6; // pos (4) + num (2)
		return size;
	}
	
	public int getAlias() {
		return Conversion.bytesToInt(getData(), 0);
	}
	
	public int getPosition(int index) {
		byte[] data = getData();
		int offset = 4; // alias
		offset += index * 6; // size of a block
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public short getNumber(int index) {
		byte[] data = getData();
		int offset = 4; // alias
		offset += index * 6; // size of a block
		offset += 4; // skip the position
		
		return Conversion.bytesToShort(data, offset);
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	public String getSubCommandString() {
		return "Read";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		int counter = 0;
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Alias = %d", getAlias()));
			} else if (i - 4 - counter * 6 == 0) {
				ans.append(String.format(" Block %d position = %d", counter + 1, getPosition(counter)));
			} else if (i - 4 - counter * 6 == 4) {
				ans.append(String.format(" Block %d size = %d", counter + 1, getNumber(counter)));
				counter++;
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		int counter = 0;
		int nextBlock = 4;
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Alias = %d", getAlias(ack)));
			} else if (i == nextBlock) {
				ans.append(String.format(" Block %d error '%s'", counter + 1, translateError(ack, counter)));
			} else if (i == nextBlock + 1) {
				ans.append(String.format(" Block %d position = %d", counter + 1, getPosition(ack, counter)));
			} else if (i == nextBlock + 5) {
				ans.append(String.format(" Block %d size = %d", counter + 1, getNumber(ack, counter)));
			} else if (i == nextBlock + 7) {
				ans.append(String.format(" Block %d file data", counter + 1));
				nextBlock = i + getNumber(ack, counter);
				counter++;
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

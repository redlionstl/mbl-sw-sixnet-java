/*
 * FILESYS_GETALIAS.java
 *
 * A GETALIAS FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

public class FS_GetAlias extends UDR_Filesys {
	// Acknowledgment translation functions
	public boolean verifyAck(UDR_Acknowledge ack) {
	    return (ack.getData().length == 1 || ack.getData().length == 5);
    }
    
	public static byte getError(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static String translateError(UDR_Acknowledge ack) {
		return translateError(getError(ack));
	}
	
	public static String translateError(byte error) {
		switch (error) {
			case E_NO_ERROR:
			    return "No Error";
			case E_INVALID_OPTION:
			    return "Invalid Option";
			case E_FAILED_TO_OPEN:
			    return "Failed to Open";
			default:
			    return "(Unknown Error)";
		}
	}
	
	public static int getAlias(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 1); // Skip error
	}
	
	public static String translateOptions(byte options) {
		switch (options) {
			case O_RDWR:
				return "Read-Write";
			case O_RDONLY:
				return "Read Only";
			case O_WRONLY:
				return "Write Only";
			case O_APPEND:
				return "Append";
			case O_TRUNCATE:
				return "Truncate";
			case O_RESERVED1:
				return "Reserved Options(1)";
			case O_RESERVED2:
				return "Reserved Options(2)";
			case O_RESERVED3:
				return "Reserved Options(3)";
			case O_RESERVED4:
				return "Reserved Options(4)";
			default:
				return "(Unknown Options)";
		}
	}
	
	public static final byte O_RDWR = 0x00,
	                         O_RDONLY = (byte)0xD0,
	                         O_WRONLY = (byte)0xD1,
	                         O_APPEND = (byte)0xD2,
	                         O_TRUNCATE = (byte)0xD3,
	                         O_RESERVED1 = (byte)0xD4,
	                         O_RESERVED2 = (byte)0xD5,
	                         O_RESERVED3 = (byte)0xD6,
	                         O_RESERVED4 = (byte)0xD7;
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_INVALID_OPTION = 0x01,
	                         E_FAILED_TO_OPEN = 0x02;
	
	public FS_GetAlias(byte[] data) {
		super(UDR_Filesys.C_GETALIAS);
		setData(data);
	}
	
	public FS_GetAlias(byte mode, String fname) {
		super(UDR_Filesys.C_GETALIAS);
		
		byte[] data;
		
		if (fname.length() > 51) throw new IllegalArgumentException("File name too long");
		
		data = new byte[fname.length() + 2]; // 0 is mode, last is \0
		data[0] = mode;
		
		byte[] str = fname.getBytes();
		if (str.length != fname.length()) throw new RuntimeException("String bytes different length than string! Are you using special characters?");
		
		System.arraycopy(str, 0, data, 1, str.length);
		data[data.length - 1] = 0; // null terminator
		
		setData(data);
	}
	
	public UDR_Acknowledge acknowledge(byte error, int alias) {
		byte[] data = new byte[5];
		
		data[0] = error;
		
		Conversion.intToBytes(data, 1, alias);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
        
		return ack;
	}
	
	public byte getMode() {
		return getData()[0];
	}
	
	public String getFileName() {
		byte[] temp = getData();
		byte[] str = new byte[temp.length - 2]; // subtract mode and null terminator
		
		System.arraycopy(temp, 1, str, 0, str.length);
		
		return new String(str);
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	public String getSubCommandString() {
		return "GetAlias";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Options '%s'", translateOptions(getMode())));
			} else if (i == 1) {
				ans.append(String.format(" File name '%s'", getFileName()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Error '%s'", translateError(ack)));
			} else if (i == 1) {
				ans.append(String.format(" Alias = %d", getAlias(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

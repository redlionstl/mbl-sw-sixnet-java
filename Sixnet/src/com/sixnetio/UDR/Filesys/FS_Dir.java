/*
 * FILESYS_DIR.java
 *
 * A DIR FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

public class FS_Dir extends UDR_Filesys {
	public static class DirEntry {
		public final String name;
		public final int size;
		public final int timestamp;
		
		public final boolean extended; // If true, the following data will exist; if false, all will be zeros
		public final int accessTime;
		public final int modifyTime;
		public final int mode;
		public final int uid;
		public final int gid;
		
		private DirEntry(String name, int size, int timestamp) {
			this.name = name;
			this.size = size;
			this.timestamp = timestamp;
			
			this.extended = false;
			this.accessTime = 0;
			this.modifyTime = 0;
			this.mode = 0;
			this.uid = 0;
			this.gid = 0;
		}
		
		private DirEntry(String name, int size, int timestamp, int accessTime, int modifyTime, int mode, int uid, int gid) {
			this.name = name;
			this.size = size;
			this.timestamp = timestamp;
			
			this.extended = true;
			this.accessTime = accessTime;
			this.modifyTime = modifyTime;
			this.mode = mode;
			this.uid = uid;
			this.gid = gid;
		}
	}
	
	// Acknowledgment translation functions
	public boolean verifyAck(UDR_Acknowledge ack) {
	    if (ack.getData().length < 6) return false;
	    
	    try {
	    	if (getCount(ack) == 0) return true;
	    	
	    	// Just try to grab the last piece of the last entry; if an exception is throw we will catch it
			if ((getOptions() & O_EXTENDED) == O_EXTENDED) {
				// Last position is GID
				getGID(ack, getOptions(), getCount(ack) - 1);
			} else {
				// Last position is timestamp
				getTimestamp(ack, getOptions(), getCount(ack) - 1);
			}
			
			return true;
		} catch (ArrayIndexOutOfBoundsException ex) {
			return false;
		}
    }
    
	// Helper function to get the offset of a given directory entry
	private static int findOffset(UDR_Acknowledge ack, byte options, int index) {
		ack.getData();
		int offset = 6; // skip error, position, and count
		int counter = 0;
		
		while (counter < index) {
			// search for the end of the file name
			offset = skipName(ack, offset);
			
			// skip the data
			offset += 8; // size & timestamp
			
			if (options == FS_Dir.O_EXTENDED) {
				offset += 20; // access, modified, mode, uid, gid
			}
			
			counter++;
		}
		
		return offset;
	}
	
	// Helper function, since we need to skip the name (a variable-length field) often
	private static int skipName(UDR_Acknowledge ack, int offset) {
		byte[] data = ack.getData();
		
		while (data[offset++] != 0);
		
		return offset;
	}
	
	public static byte getError(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static int getPosition(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 1);
	}
	
	public static byte getCount(UDR_Acknowledge ack) {
		return ack.getData()[5];
	}
	
	public static String getName(UDR_Acknowledge ack, byte options, int index) {
		byte[] data = ack.getData();
		int offset = findOffset(ack, options, index);
		
		String fname = "";
		while (data[offset] != 0) {
			fname += (char)(data[offset++]);
		}
		
		return fname;
	}
	
	public static int getSize(UDR_Acknowledge ack, byte options, int index) {
		byte[] data = ack.getData();
		int offset = findOffset(ack, options, index);
		offset = skipName(ack, offset);
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static int getTimestamp(UDR_Acknowledge ack, byte options, int index) {
		byte[] data = ack.getData();
		int offset = findOffset(ack, options, index);
		offset = skipName(ack, offset);
		
		offset += 4; // skip size
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static int getAccessTime(UDR_Acknowledge ack, byte options, int index) {
		if (options != FS_Dir.O_EXTENDED) return 0; // It doesn't exist without that option
		
		byte[] data = ack.getData();
		int offset = findOffset(ack, options, index);
		offset = skipName(ack, offset);
		
		offset += 8; // skip size & timestamp
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static int getModifiedTime(UDR_Acknowledge ack, byte options, int index) {
		if (options != FS_Dir.O_EXTENDED) return 0; // It doesn't exist without that option
		
		byte[] data = ack.getData();
		int offset = findOffset(ack, options, index);
		offset = skipName(ack, offset);
		
		offset += 12; // skip size, timestamp, and access
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static int getMode(UDR_Acknowledge ack, byte options, int index) {
		if (options != FS_Dir.O_EXTENDED) return 0; // It doesn't exist without that option
		
		byte[] data = ack.getData();
		int offset = findOffset(ack, options, index);
		offset = skipName(ack, offset);
		
		offset += 16; // skip size, timestamp, access, modified
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static int getUID(UDR_Acknowledge ack, byte options, int index) {
		if (options != FS_Dir.O_EXTENDED) return 0; // It doesn't exist without that option
		
		byte[] data = ack.getData();
		int offset = findOffset(ack, options, index);
		offset = skipName(ack, offset);
		
		offset += 20; // skip size, timestamp, access, modified, mode
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static int getGID(UDR_Acknowledge ack, byte options, int index) {
		if (options != FS_Dir.O_EXTENDED) return 0; // It doesn't exist without that option
		
		byte[] data = ack.getData();
		int offset = findOffset(ack, options, index);
		offset = skipName(ack, offset);
		
		offset += 24; // skip size, timestamp, access, modified, mode, uid
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static DirEntry[] getEntries(UDR_Acknowledge ack, byte options) {
		int count = getCount(ack);
		DirEntry[] entries = new DirEntry[count];
		
		for (int i = 0; i < count; i++) {
			if (options == O_STANDARD) {
				entries[i] = new DirEntry(getName(ack, options, i),
				                          getSize(ack, options, i),
				                          getTimestamp(ack, options, i));
			} else {
    			entries[i] = new DirEntry(getName(ack, options, i),
    			                          getSize(ack, options, i),
    			                          getTimestamp(ack, options, i),
    			                          getAccessTime(ack, options, i),
    			                          getModifiedTime(ack, options, i),
    			                          getMode(ack, options, i),
    			                          getUID(ack, options, i),
    			                          getGID(ack, options, i));
			}
		}
		
		return entries;
	}
	
	public static String translateOptions(byte options) {
		switch (options) {
			case O_STANDARD:
				return "Standard Listing";
			case O_EXTENDED:
				return "Extended Listing";
			default:
				return "(Unknown Options)";
		}
	}
	
	public static String translateError(UDR_Acknowledge ack) {
		return translateError(getError(ack));
	}
	
	public static String translateError(byte error) {
		switch (error) {
			case E_NO_ERROR: return "No Error";
			case E_INVALID_OPTION: return "Invalid Option";
			case E_INVALID_ALIAS: return "Invalid Alias";
			default: return "(Unknown Error)";
		}
	}
	
	// Options
	public static final byte O_STANDARD = 0x00,
	                         O_EXTENDED = 0x01;
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_INVALID_OPTION = 0x01,
	                         E_INVALID_ALIAS = 0x02; // Not in the spec, assuming it must exist
	
	// Special file names
	public static final String F_BASE = "__RAMDSK1__"; // Get the alias for this to list the main file system
	
	public FS_Dir(byte[] data) {
		super(UDR_Filesys.C_DIR);
		setData(data);
	}
	
	public FS_Dir(byte options, int alias, int position) {
		super(UDR_Filesys.C_DIR);
		
		byte[] data = new byte[9];
		
		data[0] = options;
		Conversion.intToBytes(data, 1, alias);
		Conversion.intToBytes(data, 5, position);
		
		setData(data);
	}
	
	public UDR_Acknowledge acknowledge(byte error, int position, byte count, byte[] dirData) {
		byte[] data = new byte[1 + 4 + 1 + dirData.length];
		
		data[0] = error;
		Conversion.intToBytes(data, 1, position);
		data[5] = count;
		System.arraycopy(dirData, 0, data, 6, dirData.length);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	public byte getOptions() {
		return getData()[0];
	}
	
	public int getAlias() {
		return Conversion.bytesToInt(getData(), 1);
	}
	
	public int getPosition() {
		return Conversion.bytesToInt(getData(), 5);
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	public String getSubCommandString() {
		return "Dir";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Options '%s'", translateOptions(getOptions())));
			} else if (i == 1) {
				ans.append(String.format(" Alias = %d", getAlias()));
			} else if (i == 5) {
				ans.append(String.format(" Position = %d", getPosition()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		int nextBlock = 6;
		int endOfName = 999; // Keep from accidentally detecting a wrong location
		int counter = 0;
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Error '%s'", translateError(ack)));
			} else if (i == 1) {
				ans.append(String.format(" Position = %d", getPosition(ack)));
			} else if (i == 5) {
				ans.append(String.format(" Count = %d", getCount(ack)));
			} else if (i == nextBlock) {
				ans.append(String.format(" Name '%s'", getName(ack, getOptions(), counter)));
				endOfName = skipName(ack, i);
			} else if (i == endOfName) {
				ans.append(String.format(" Size = %d", getSize(ack, getOptions(), counter)));
			} else if (i == endOfName + 4) {
				ans.append(String.format(" Timestamp = %d", getTimestamp(ack, getOptions(), counter)));
				
				// By setting nextBlock here, the next time around will fall out before reaching access time
				if (getOptions() == O_STANDARD) {
					nextBlock = i + 4;
					counter++;
				}
			} else if (i == endOfName + 8) {
				ans.append(String.format(" Access Time = %d", getAccessTime(ack, getOptions(), counter)));
			} else if (i == endOfName + 12) {
				ans.append(String.format(" Modification Time = %d", getModifiedTime(ack, getOptions(), counter)));
			} else if (i == endOfName + 16) {
				ans.append(String.format(" Mode = 0%o", getMode(ack, getOptions(), counter)));
			} else if (i == endOfName + 20) {
				ans.append(String.format(" UID = %d", getUID(ack, getOptions(), counter)));
			} else if (i == endOfName + 24) {
				ans.append(String.format(" GID = %d", getGID(ack, getOptions(), counter)));
				
				nextBlock = i + 4;
				counter++;
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

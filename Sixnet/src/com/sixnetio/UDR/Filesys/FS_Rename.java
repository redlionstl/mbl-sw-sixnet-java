/*
 * FILESYS_RENAME.java
 *
 * A RENAME FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.*;

public class FS_Rename extends UDR_Filesys {
	// Acknowledgment translation functions
	public boolean verifyAck(UDR_Acknowledge ack) {
	    return (ack.getData().length >= 1);
    }
    
	public static byte getError(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static String translateError(UDR_Acknowledge ack) {
		return translateError(getError(ack));
	}
	
	public static String translateError(byte error) {
		switch (error) {
			case E_NO_ERROR: return "No Error";
			case E_INVALID_OPTION: return "Invalid Option";
			case E_FAILED: return "Failed";
			default: return "(Unknown Error)";
		}
	}
	
	public static String translateOptions(byte options) {
		switch (options) {
			case O_NONE:
				return "No Options";
			default:
				return "(Unknown Options)";
		}
	}
	
	// Options
	public static final byte O_NONE = 0x00;
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_INVALID_OPTION = 0x01,
	                         E_FAILED = 0x02; // bad file name, source doesn't exists, or destination does exist
	
	
	public FS_Rename(byte[] data) {
		super(UDR_Filesys.C_RENAME);
		setData(data);
	}
	
	public FS_Rename(byte options, String src, String dest) {
		super(UDR_Filesys.C_RENAME);
		
		byte[] data = new byte[1 + src.length() + 1 + dest.length() + 1];
		int offset = 0;
		
		if (options != O_NONE) throw new IllegalArgumentException("FILESYS RENAME must use O_NONE");
		
		data[offset++] = options;
		
		byte[] str = src.getBytes();
		if (str.length != src.length()) throw new RuntimeException("String length differs from its length in bytes!");
		
		System.arraycopy(str, 0, data, offset, str.length);
		offset += str.length;
		data[offset++] = 0; // null terminator
		
		str = dest.getBytes();
		if (str.length != dest.length()) throw new RuntimeException("String length differs from its length in bytes!");
		
		System.arraycopy(str, 0, data, offset, str.length);
		offset += str.length;
		data[offset++] = 0; // null terminator
		
		setData(data);
	}
	
	public UDR_Acknowledge acknowledge(byte error) {
		byte[] data = new byte[1];
		
		data[0] = error;
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
        
		return ack;
	}
	
	public byte getOptions() {
		return getData()[0];
	}
	
	public String getSourcePath() {
		byte[] data = getData();
		int offset = 1; // skip options
		
		String ans = "";
		while (data[offset] != 0) {
			ans += (char)data[offset++];
		}
		
		return ans;
	}
	
	public String getDestinationPath() {
		byte[] data = getData();
		int offset = 1; // skip options
		
		while (data[offset++] != 0); // skip source
		
		String ans = "";
		while (data[offset] != 0) {
			ans += (char)data[offset++];
		}
		
		return ans;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	public String getSubCommandString() {
		return "Rename";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Options '%s'", translateOptions(getOptions())));
			} else if (i == 1) {
				ans.append(String.format(" From '%s'", getSourcePath()));
			} else if (i == 1 + getSourcePath().length() + 1) { // 1 = source, 1 + source.length = byte after (\0), go one further
				ans.append(String.format(" To '%s'", getDestinationPath()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Error '%s'", translateError(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

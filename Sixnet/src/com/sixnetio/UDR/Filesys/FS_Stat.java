/*
 * FILESYS_STAT.java
 *
 * A STAT FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.UDR_Acknowledge;
import com.sixnetio.UDR.UDR_Filesys;
import com.sixnetio.util.Conversion;

public class FS_Stat extends UDR_Filesys {
	public static class Stat {
		public final String name;
		public final int size;
		public final int timestamp;
		public final byte access;
		
		Stat(String name, int size, int timestamp, byte access)
		{
			this.name = name;
			this.size = size;
			this.timestamp = timestamp;
			this.access = access;
		}
	}
	
	// Acknowledgment translation functions
	@Override
	public boolean verifyAck(UDR_Acknowledge ack) {
		try {
			getAccess(ack); // Last byte that needs to be here
			return true;
		} catch (ArrayIndexOutOfBoundsException ex) {
			return false;
		}
	}
	
	public static byte getError(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static String translateError(UDR_Acknowledge ack) {
		return translateError(getError(ack));
	}
	
	public static String translateError(byte error) {
		switch (error) {
			case E_NO_ERROR: return "No Error";
			case E_INVALID_OPTION: return "Invalid Option";
			case E_INVALID_ALIAS: return "Invalid Alias";
			default: return "(Unknown Error)";
		}
	}
	
	public static String getName(UDR_Acknowledge ack) {
		byte[] data = ack.getData();
		int offset = 1; // skip error code
		
		String fname = "";
		while (data[offset] != 0) {
			fname += (char)data[offset++];
		}
		
		return fname;
	}
	
	public static int getSize(UDR_Acknowledge ack) {
		byte[] data = ack.getData();
		int offset = 1; // skip error code
		
		while (data[offset++] != 0); // skip file name
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static int getTimestamp(UDR_Acknowledge ack) {
		byte[] data = ack.getData();
		int offset = 1; // skip error code
		
		while (data[offset++] != 0); // skip file name
		
		offset += 4; // skip size
		
		return Conversion.bytesToInt(data, offset);
	}
	
	public static byte getAccess(UDR_Acknowledge ack) {
		byte[] data = ack.getData();
		int offset = 1; // skip error code
		
		while (data[offset++] != 0); // skip file name
		
		offset += 8; // skip size & timestamp
		
		return data[offset];
	}
	
	public static Stat getStat(UDR_Acknowledge ack)
	{
		return new Stat(getName(ack), getSize(ack), getTimestamp(ack),
		                getAccess(ack));
	}
	
	public static String translateOptions(byte options) {
		switch (options) {
			case O_NONE:
				return "No Options";
			default:
				return "(Unknown Options)";
		}
	}
	
	// Options
	public static final byte O_NONE = 0x00;
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_INVALID_OPTION = 0x01,
	                         E_INVALID_ALIAS = 0x02;
	
	public FS_Stat(byte[] data) {
		super(UDR_Filesys.C_STAT);
		setData(data);
	}
	
	public FS_Stat(byte options, int alias) {
		super(UDR_Filesys.C_STAT);
		
		byte[] data = new byte[5];
		
		if (options != O_NONE) throw new IllegalArgumentException("FILESYS STAT only supports O_NONE");
		
		data[0] = options;
		Conversion.intToBytes(data, 1, alias);
		
		setData(data);
	}

	public UDR_Acknowledge acknowledge(byte error, String filename, int size, int timestamp, byte access) {
		byte[] data = new byte[1 + filename.length() + 4 + 4 + 1];
		int offset = 0;
		
		data[offset++] = error;
		
		byte[] fnameBytes = filename.getBytes();
		if (fnameBytes.length != filename.length()) throw new RuntimeException("String and byte lengths differ!");
		System.arraycopy(data, 1, fnameBytes, 0, fnameBytes.length);
		offset += fnameBytes.length;
		
		Conversion.intToBytes(data, offset, size);
		offset += 4;
		
		Conversion.intToBytes(data, offset, timestamp);
		offset += 4;
		
		data[offset] = access;
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
		
	public byte getOptions() {
		return getData()[0];
	}
	
	public int getAlias() {
		return Conversion.bytesToInt(getData(), 1);
	}
	
	@Override
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	@Override
	public String getSubCommandString() {
		return "Stat";
	}
	
	@Override
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Options '%s'", translateOptions(getOptions())));
			} else if (i == 1) {
				ans.append(String.format(" Alias = %d", getAlias()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	@Override
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		int sizeOffset = 2;
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Error '%s'", translateError(ack)));
			} else if (i == 1) {
				String name = getName(ack);
				sizeOffset = name.length() + 2; // Skip the error code and the \0 at the end
				
				ans.append(String.format(" Name '%s'", name));
			} else if (i == sizeOffset) {
				ans.append(String.format(" Size = %d", getSize(ack)));
			} else if (i == sizeOffset + 4) {
				ans.append(String.format(" Timestamp = %d", getTimestamp(ack)));
			} else if (i == sizeOffset + 8) {
				ans.append(String.format(" Access = 0%o", getAccess(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

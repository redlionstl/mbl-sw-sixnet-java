/*
 * FILESYS_CLOSEALIAS.java
 *
 * A CLOSEALIAS FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

public class FS_CloseAlias extends UDR_Filesys {
	// There is no data in the acknowledgment
	public boolean verifyAck(UDR_Acknowledge ack) {
	    return (ack.getData().length >= 0);
    }
    
	
	public FS_CloseAlias(byte[] data) {
		super(UDR_Filesys.C_CLOSEALIAS);
		setData(data);
	}
	
	public FS_CloseAlias(int alias) {
		super(UDR_Filesys.C_CLOSEALIAS);
		
		byte[] data = new byte[4];
		
		Conversion.intToBytes(data, 0, alias);
		
		setData(data);
	}
	
	public UDR_Acknowledge acknowledge() {
		byte[] data = new byte[0];

		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
        
		return ack;
	}
	
	public int getAlias() {
		return Conversion.bytesToInt(getData(), 0);
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	public String getSubCommandString() {
		return "CloseAlias";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Alias = %d", getAlias()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

/*
 * FILESYS_MEMAVAIL.java
 *
 * A MEMAVAIL FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

public class FS_MemAvail extends UDR_Filesys {
	// Acknowledgment translation functions
	public boolean verifyAck(UDR_Acknowledge ack) {
	    return (ack.getData().length >= 5);
    }
    
	public static byte getError(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static String translateError(UDR_Acknowledge ack) {
		return translateError(getError(ack));
	}
	
	public static String translateError(byte error) {
		switch (error) {
			case E_NO_ERROR: return "No Error";
			case E_INVALID_OPTION: return "Invalid Option";
			case E_ACCESS_ERROR: return "Access Error";
			default: return "(Unknown Error)";
		}
	}
	
	public static int getMem(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 1);
	}
	
	public static String translateOptions(byte options) {
		switch (options) {
			case O_NONE:
				return "No Options";
			default:
				return "(Unknown Options)";
		}
	}
	
	// Options
	public static final byte O_NONE = 0x00;
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_INVALID_OPTION = 0x01,
	                         E_ACCESS_ERROR = 0x02;
	
	public FS_MemAvail(byte[] data) {
		super(UDR_Filesys.C_MEMAVAIL);
		setData(data);
	}
	
	public FS_MemAvail(byte options) {
		super(UDR_Filesys.C_MEMAVAIL);
		
		byte[] data = new byte[1];
		
		if (options != O_NONE) throw new IllegalArgumentException("FILESYS MEMAVAIL must use O_NONE");
		
		data[0] = options;
		setData(data);
	}
	
	public UDR_Acknowledge acknowledge(byte error, int mem) {
		byte[] data = new byte[5];
		
		data[0] = error;
		
		Conversion.intToBytes(data, 1, mem);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
        
		return ack;
	}
	
	public byte getOptions() {
		return getData()[0];
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	public String getSubCommandString() {
		return "MemAvail";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Options '%s'", translateOptions(getOptions())));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Error '%s'", translateError(ack)));
			} else if (i == 1) {
				ans.append(String.format(" Available memory = %d", getMem(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

/*
 * FILESYS_DELETE.java
 *
 * A DELETE FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.*;

public class FS_Delete extends UDR_Filesys {
	// Acknowledgment translation functions
	public boolean verifyAck(UDR_Acknowledge ack) {
	    return (ack.getData().length >= 1);
    }
    
	public static byte getError(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static String translateError(UDR_Acknowledge ack) {
		return translateError(getError(ack));
	}
	
	public static String translateError(byte error) {
		switch (error) {
			case E_NO_ERROR:
			    return "No Error";
			case E_INVALID_OPTION:
			    return "Invalid Option";
			case E_FAILED_TO_DELETE:
			    return "Failed to Delete";
			default:
			    return "(Unknown Error)";
		}
	}
	
	public static String translateOptions(byte options) {
		switch (options) {
			case O_NONE:
				return "No Options";
			default:
				return "(Unknown Options)";
		}
	}
	
	// Options
	public static final byte O_NONE = 0x00;
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_INVALID_OPTION = 0x01,
	                         E_FAILED_TO_DELETE = 0x02;
	
	// Special file names
	public static final String F_FORMAT_FILESYS = "__RAMDSK1__";
	
	public FS_Delete(byte[] data) {
		super(UDR_Filesys.C_DELETE);
		setData(data);
	}
	
	public FS_Delete(byte options, String fname) {
		super(UDR_Filesys.C_DELETE);
		
		byte[] data;
		
		if (options != O_NONE) throw new IllegalArgumentException("The only legal option for DELETE is O_NONE");
		if (fname.length() > 51) throw new IllegalArgumentException("File name too long");
		
		data = new byte[fname.length() + 2];
		data[0] = options;
		
		byte[] str = fname.getBytes();
		if (str.length != fname.length()) throw new RuntimeException("String bytes different length than string!");
		
		System.arraycopy(str, 0, data, 1, str.length);
		data[data.length - 1] = 0; // null terminator
		
		setData(data);
	}
	
	public UDR_Acknowledge acknowledge(byte error) {
		byte[] data = new byte[1];
		
		data[0] = error;
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
        
		return ack;
	}
	
	public byte getOptions() {
		return getData()[0];
	}
	
	public String getFileName() {
		byte[] temp = getData();
		byte[] str = new byte[temp.length - 2]; // subtract mode and null terminator
		
		System.arraycopy(temp, 1, str, 0, str.length);
		
		return new String(str);
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	public String getSubCommandString() {
		return "Delete";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Options '%s'", translateOptions(getOptions())));
			} else if (i == 1) {
				ans.append(String.format(" File '%s'", getFileName()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Error '%s'", translateError(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

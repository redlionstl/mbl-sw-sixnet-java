/*
 * FILESYS_CREATE.java
 *
 * A CREATE FILESYS UDR message.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR.Filesys;

import com.sixnetio.UDR.UDR_Acknowledge;
import com.sixnetio.UDR.UDR_Filesys;
import com.sixnetio.util.Conversion;

public class FS_Create extends UDR_Filesys {
	// Acknowledgment translation functions
	@Override
    public boolean verifyAck(UDR_Acknowledge ack) {
	    return (ack.getData().length == 1 || ack.getData().length >= 5);
    }
    
	public static byte getError(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static String translateError(UDR_Acknowledge ack) {
		return translateError(getError(ack));
	}
	
	public static String translateError(byte error) {
		switch (error) {
			case E_NO_ERROR:
			    return "No Error";
			case E_INVALID_OPTION:
			    return "Invalid Option";
			case E_FAILED_TO_CREATE:
			    return "Failed to Create";
			case E_UNABLE_TO_ALLOCATE:
			    return "Unable to Allocate";
			default:
			    return "(Unknown Error)";
		}
	}
	
	public static int getAlias(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 1);
	}
	
	public static String translateOptions(byte options) {
		switch (options) {
			case O_RDWR:
				return "Read-Write";
			case O_RDONLY:
				return "Read Only";
			case O_WRONLY:
				return "Write Only";
			case O_APPEND:
				return "Append";
			case O_TRUNCATE:
				return "Truncate";
/*			case O_RESERVED1:
				return "Reserved Options(1)";
			case O_RESERVED2:
				return "Reserved Options(2)";
			case O_RESERVED3:
				return "Reserved Options(3)";
			case O_RESERVED4:
				return "Reserved Options(4)";*/
			default:
				return "(Unknown Options)";
		}
	}
	
	// Options
	public static final byte O_RDWR = 0x00,
	                         O_RDONLY = (byte)0x01, //D0
	                         O_WRONLY = (byte)0x02, //D1
	                         O_APPEND = (byte)0x04, //D2
	                         O_TRUNCATE = (byte)0x08; //D3
	                         //O_RESERVED1 = (byte)0xD4,
	                         //O_RESERVED2 = (byte)0xD5,
	                         //O_RESERVED3 = (byte)0xD6,
	                         //O_RESERVED4 = (byte)0xD7;
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_INVALID_OPTION = 0x01,
	                         E_FAILED_TO_CREATE = 0x02,
	                         E_UNABLE_TO_ALLOCATE = 0x03;
	
	public static final int S_DYNAMIC = 0; // Dynamic size
	
	public FS_Create(byte[] data) {
		super(UDR_Filesys.C_CREATE);
		setData(data);
	}
	
	public FS_Create(byte mode, int size, String fname) {
		super(UDR_Filesys.C_CREATE);
		
		byte[] data;
		
		if (fname.length() > 51) throw new IllegalArgumentException("File name too long");
		
		data = new byte[fname.length() + 6];
		data[0] = mode;
		
		Conversion.intToBytes(data, 1, size);
		
		byte[] str = fname.getBytes();
		if (str.length != fname.length()) throw new RuntimeException("String bytes different length than string!");
		
		System.arraycopy(str, 0, data, 5, str.length);
		data[data.length - 1] = 0; // null terminator
		
		setData(data);
	}
	
	public UDR_Acknowledge acknowledge(byte error, int alias) {
		byte[] data = new byte[5];
		
		data[0] = error;
		
		Conversion.intToBytes(data, 1, alias);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
        
		return ack;
	}
	
	public byte getMode() {
		return getData()[0];
	}
	
	public int getSize() {
		return Conversion.bytesToInt(getData(), 1);
	}
	
	public String getFileName() {
		byte[] temp = getData();
		byte[] str = new byte[temp.length - 6]; // subtract mode and null terminator
		
		System.arraycopy(temp, 5, str, 0, str.length);
		
		return new String(str);
	}
	
	@Override
    public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific FILESYS message");
		}
	}
	
	@Override
    public String getSubCommandString() {
		return "Create";
	}
	
	@Override
    public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Mode '%s'", translateOptions(getMode())));
			} else if (i == 1) {
				ans.append(String.format(" Size = %d", getSize()));
			} else if (i == 5) {
				ans.append(String.format(" Name '%s'", getFileName()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	@Override
    public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Error '%s'", translateError(getError(ack))));
			} else if (i == 5) {
				ans.append(String.format(" Alias = %d", getAlias(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

/*
 * UDR_NAK.java
 *
 * A NAK UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

/**
 * A message to negatively acknowledge received messages. Used when a
 *   received message is not supported. Errors in executing messages
 *   are sent in positive acknowledgments.
 *
 * @author Jonathan Pearson
 */
public class UDR_NAcknowledge extends UDRMessage {
	/**
	 * Used by UDRMessage.receive() to construct this message given a data payload.
	 * @param data The data payload.
	 */
	protected UDR_NAcknowledge(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_NAK);
		setData(data);
	}
	
	/**
	 * Construct a new message of this type.
	 */
	public UDR_NAcknowledge() {
		super.setCommand(UDRMessage.C_NAK);
		setData(new byte[0]);
	}
	
	/**
	 * Throws an IllegalStateException, since you cannot ACK a NAK.
	 */
	public UDR_Acknowledge acknowledge() {
		throw new IllegalStateException("Cannot ACK a NAK");
	}
	
	/**
	 * Unless val == getCommand(), throws an IllegalArgumentException.
	 */
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Describe the command byte of this message.
	 */
	public String getCommandString() {
		return "NAK";
	}
}

/*
 * UDRMessage.java
 *
 * Implements a general utility for building a new UDR (SIXNET Universal DRiver
 * protocol) message.
 * If you are looking into how to send/receive these messages, take a look at
 * MessageDispatcher and UDRLib in com.sixenio.Station.
 *
 * Jonathan Pearson
 * March 20, 2007
 *
 */

package com.sixnetio.UDR;

import java.io.*;
import java.util.Arrays;

import com.sixnetio.Station.IOMessage;
import com.sixnetio.util.Utils;

/**
 * Parent class for the various UDR messages. Provides a number of useful default
 * implementations, and will automatically construct the necessary subclasses for
 * received messages.
 * 
 * @author Jonathan Pearson
 */
public abstract class UDRMessage
	implements IOMessage
{
	// Constants
	// Command bytes
	/** Command byte for NOP message. */
	public static final byte C_NOP = 0;
	
	/** Command byte for acknowledgment message. */
	public static final byte C_ACK = 1;
	
	/** Command byte for non-acknowledgment message. */
	public static final byte C_NAK = 2;
	
	/** Command byte for version request message. */
	public static final byte C_VERS = 3;
	
	/** Command byte for number of I/O points request message. */
	public static final byte C_NIO = 4;
	
	/** Command byte for get discretes message. */
	public static final byte C_GETD = 10;
	
	/** Command byte for get bytes message. */
	public static final byte C_GETB = 11;
	
	/** Command byte for get analogs message. */
	public static final byte C_GETA = 12;
	
	/** Command byte for get string message. */
	public static final byte C_GETS = 13;
	
	/** Command byte for put discretes message. */
	public static final byte C_PUTD = 14;
	
	/** Command byte for put bytes message. */
	public static final byte C_PUTB = 15;
	
	/** Command byte for put analogs message. */
	public static final byte C_PUTA = 16;
	
	/** Command byte for set discretes (turn on set bits, ignore zero bits) message. */
	public static final byte C_SETD = 17;
	
	/** Command byte for clear discretes (turn off set bits, ignore zero bits) message. */
	public static final byte C_CLRD = 18;
	
	/** Command byte for set clock message. */
	public static final byte C_SCLOCK = 24;
	
	/** Command byte for get clock message. */
	public static final byte C_GCLOCK = 25;
	
	/** Command byte for filesys subcommands. */
	public static final byte C_FILESYS = 26;
	
	/** Command byte for I/O exchange message. */
	public static final byte C_IOXCHG = 32;
	
	/** Minimum command byte for user messages. See C_USERMAX for the maximum command byte. */
	public static final byte C_USER = 96; // Add user message indices to this, no greater than C_USERMAX
	
	/** Maximum command byte for user messages. See C_USER for the minimum command byte. */
	public static final byte C_USERMAX = 127;
	
	/** Command byte for gateway subcommands. */
	public static final byte C_GWY = (byte)208;
	
	// Format bytes
	/** Format byte for binary messages. */
	public static final byte F_BINARY = (byte)')';
	
	/** Format byte for hexadecimal messages. */
	public static final byte F_HEX = (byte)']';
	
	/** Format byte for fixed CRC binary messages. */
	public static final byte F_FIXEDCRC = (byte)'}';
	
	// Data type bytes
	/** Type byte for default analog inputs. */
	public static final byte T_D_AIN = 0;
	
	/** Type byte for default analog outputs. */
	public static final byte T_D_AOUT = 1;
	
	/** Type byte for communications analog inputs. */
	public static final byte T_C_AIN = 2;
	
	/** Type byte for communications analog outputs. */
	public static final byte T_C_AOUT = 3;
	
	/** Type byte for physical analog inputs. */
	public static final byte T_P_AIN = 4;
	
	/** Type byte for physical analog outputs. */
	public static final byte T_P_AOUT = 5;
	
	/** Type byte for default discrete inputs. */
	public static final byte T_D_DIN = 10;
	
	/** Type byte for default discrete outputs. */
	public static final byte T_D_DOUT = 11;
	
	/** Type byte for communications discrete inputs. */
	public static final byte T_C_DIN = 12;
	
	/** Type byte for communications discrete outputs. */
	public static final byte T_C_DOUT = 13;
	
	/** Type byte for physical discrete inputs. */
	public static final byte T_P_DIN = 14;
	
	/** Type byte for physical discrete outputs. */
	public static final byte T_P_DOUT = 15;
	
	/** Type byte for long inputs. */
	public static final byte T_LONGINT_IN = 20;
	
	/** Type byte for long outputs. */
	public static final byte T_LONGINT_OUT = 21;
	
	/** Type byte for floating-point inputs. */
	public static final byte T_FLOAT_IN = 22;
	
	/** Type byte for floating-point outputs. */
	public static final byte T_FLOAT_OUT = 23;
	
	/** Minimum type byte for user-defined types. */
	public static final byte T_USER = 24; // Add user type to this, no greater than T_USERMAX
	
	/** Maximum type byte for user-defined types. */
	public static final byte T_USERMAX = 127;
	
	/** Flag for big-endian transmissions. */
	public static final byte T_BIGENDIAN = (byte)0x80; // OR this into a type to set big-endian instead of little-endian	
	
	// Some VERS product values
	/** Product code for old, non-Sixnet products. */
	public static final short V_P_OLDNONSIXNET = 0;
	
	/** Product code for IOMux/Versamux products. */
	public static final short V_P_MUX = 110;
	
	/** Product code for 60-IBM/N products. */
	public static final short V_P_60IBMN = 140;
	
	/** Product code for SixTrak Gateway products. */
	public static final short V_P_SIXTRAKGATEWAY = 200;
	
	/** Product code for Sixnet Control Room software. */
	public static final short V_P_SIXNETCONTROLROOM = 250;
	
	/** Product code for new non-Sixnet products (supporting non-byte boundaries). */
	public static final short V_P_NEWNONSIXNET = 254;
	
	/** Station number for any station. */
	public static final short STA_ANY = 255; // Any station
	
	/** Initial value for CRC used as input to the CRC function. */
	private static final short CRC_INIT = (short)0xFFFF;
	
	/** CRC value used for fixed-CRC messages, and as a match for CRC comparisons. */
	private static final short CRC_MAGIC = (short)0x1D0F;
	
	// Private data members
	// These are initialized with illegal values so we can tell what needs to be set
	private byte command = -1; // Bytes are signed in Java
	private byte format = -1;
	private short destination = -1;
	private short source = -1;
	private byte session = 0; // No illegal values here
	private byte sequence = 0;
	private byte[] data = null;
	private String term = ""; // This is a legal default
	
	/**
	 * Set up a message before sending it.
	 * @param format The format byte for the message.
	 * @param source The source station number for the message.
	 * @param destination The destination station number for the message.
	 * @param session The session byte for the message.
	 * @param sequence The sequence byte for the message.
	 */
	public final void setup(byte format, short source, short destination, byte session, byte sequence) {
		setFormat(format);
		setSource(source);
		setDestination(destination);
		setSession(session);
		setSequence(sequence);
	}
	
	// Accessors
	/**
	 * Get the command byte for this message.
	 */
	public final byte getCommand() {
		return command;
	}
	
	/**
	 * Set the command byte for this message.
	 * @param val The value to set the command byte to.
	 */
	public void setCommand(byte val) {
		command = val;
	}
	
	/**
	 * Get the format byte for this message.
	 */
	public final byte getFormat() {
		return format;
	}
	
	/**
	 * Set the format byte for this message.
	 * @param val The value to set the format byte to.
	 */
	public final void setFormat(byte val) {
		format = val;
	}
	
	/**
	 * Get the destination station number for this message.
	 */
	public final short getDestination() {
		return destination;
	}
	
	/**
	 * Set the destination station number for this message.
	 * @param val The value to set the destination station number to.
	 */
	public final void setDestination(short val) {
		destination = val;
	}
	
	/**
	 * Get the source station number for this message.
	 */
	public final short getSource() {
		return source;
	}
	
	/**
	 * Set the source station number for this message.
	 * @param val The value to set the source station number to.
	 */
	public final void setSource(short val) {
		source = val;
	}
	
	/**
	 * Get the session number for this message.
	 */
	public final byte getSession() {
		return session;
	}
	
	/**
	 * Set the session number for this message.
	 * @param val The value to set the session number to.
	 */
	public final void setSession(byte val) {
		session = val;
	}
	
	/**
	 * Get the sequence number for this message.
	 */
	public final byte getSequence() {
		return sequence;
	}
	
	/**
	 * Set the sequence number for this message.
	 * @param val The value to set the sequence number to.
	 */
	public final void setSequence(byte val) {
		sequence = val;
	}
	
	/**
	 * Get the data payload of this message. Subclasses may override this to
	 * implement subcommands, such as the Gateway and Filesys messages.
	 */
	public byte[] getData() {
		byte[] val = new byte[data.length];
		System.arraycopy(data, 0, val, 0, val.length);
		return val;
	}
	
	/**
	 * Set the data payload of this message. Subclasses may override this to
	 * implement subcommands, such as the Gateway and Filesys messages.
	 * @param val The data payload to set in this message.
	 */
	public void setData(byte[] val) {
		data = new byte[val.length];
		System.arraycopy(val, 0, data, 0, data.length);
	}
	
	/**
	 * Get the termination string for this message ('', '\r', '\n', or '\r\n').
	 */
	public final String getTerm() {
		return term;
	}
	
	/**
	 * Set the termination string for this message ('', '\r', '\n', or '\r\n').
	 * @param val The termination string for this message.
	 */
	public final void setTerm(String val) {
		term = val;
	}
	
	/**
	 * Auto-generate a non-acknowledgment reply to this message. Includes all necessary
	 * addressing and session/sequence information to be a reply to this message.
	 */
	public UDR_NAcknowledge nAcknowledge() {
		UDR_NAcknowledge nak = new UDR_NAcknowledge();
		
		nak.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return nak;
	}
	
	// TODO: This should throw IllegalStateException or something declared
	/**
	 * Check that this message is valid.
	 * @throws IllegalStateException If any part of this message is not valid.
	 */
	private void check() {
		boolean pass;
		
		// Legal values for command are the C_ values above
		pass = false;
		switch(command) {
			case C_NOP:
			case C_ACK:
			case C_NAK:
			case C_VERS:
			case C_NIO:
			case C_GETD:
			case C_GETB:
			case C_GETA:
			case C_GETS:
			case C_PUTD:
			case C_PUTB:
			case C_PUTA:
			case C_SETD:
			case C_CLRD:
			case C_SCLOCK:
			case C_GCLOCK:
			case C_IOXCHG:
			case C_FILESYS:
			case C_GWY:
				pass = true;
				break;
				
			default:
				if (C_USER <= command && command <= C_USERMAX) {
					pass = true;
				}
		}
		
		if (!pass) throw new IllegalStateException("Command not set to a legal value");
		
		
		pass = false;
		switch(format) {
			case F_BINARY:
			case F_HEX:
			case F_FIXEDCRC:
				pass = true;
				break;
		}
		
		if (!pass) throw new IllegalStateException("Format not set to a legal value");
		
		
		pass = false;
		if (0 <= destination && destination <= 15999) {
			pass = true;
		}
		
		if (!pass) throw new IllegalStateException("Destination not set to a legal value");
		
		
		pass = false;
		if (0 <= source && source <= 15999) {
			pass = true;
		}
		
		if (!pass) throw new IllegalStateException("Source not set to a legal value");
		
		
		if (data == null) throw new IllegalStateException("Data not set");
		
		
		pass = false;
		if (format == F_HEX) {
			if (term.equals("") || term.equals("\n") || term.equals("\r") || term.equals("\r\n")) {
				pass = true;
			}
		} else {
			pass = true;
		}
		
		if (!pass) throw new IllegalStateException("Terminator not set to a legal value");
		
		// Everything must have passed at this point
	}
	
	// Private static helper functions
	/**
	 * Put a variable-length short value into a byte array at the given offset.
	 * @param v The value to put into the array.
	 * @param msg The array to add it to.
	 * @param offset The offset where the first byte will go.
	 */
	private static int putVar(short v, byte[] msg, int offset) {
		if (v < 128 || v == STA_ANY) {
			msg[offset++] = (byte)(v & 0xFF);
			return offset;
		} else {
			byte b1, b2;
			
			b1 = (byte)((0x80 | ((v >> 8) & 0x3F)) & 0xFF);
			b2 = (byte)(v & 0xFF);
			
			msg[offset++] = b1;
			msg[offset++] = b2;
			return offset;
		}
	}
	
	/**
	 * Get a variable-length short value out of a byte array from the given offset.
	 * @param msg The array to pull data out of.
	 * @param offset The starting offset to pull data from.
	 * @return The value found at that offset.
	 */
	private static short getVar(byte[] msg, int offset) {
		byte b1 = msg[offset++];
		
		if ((b1 & 0x80) != 0 && b1 != (byte)STA_ANY) {
			short ans = (short)((b1 << 8) & 0xFFFF);
			ans |= (short)(msg[offset++] & 0xFFFF);
			
			return ans;
		} else {
			return b1;
		}
	}
	
	/**
	 * Read a variable-length short value from an input stream using the given format.
	 * @param in The input stream to read from.
	 * @param fmt The format to use when reading data.
	 * @return The value read.
	 * @throws IOException If the underlying stream throws an IOException.
	 */
	private static short readVar(InputStream in, int fmt) throws IOException {
		byte b1 = read(in, fmt);
		
		if ((b1 & 0x80) != 0 && b1 != (byte)STA_ANY) {
			short ans = (short)((b1 << 8) & 0xFFFF);
			ans |= (short)(read(in, fmt) & 0xFFFF);
			
			return ans;
		} else {
			return (short)(b1 & 0xFF);
		}
	}
	
	/**
	 * Read an entire UDR message from the given stream.
	 * @param in The input stream to read from.
	 * @return The message received from the stream.
	 * @throws IOException If the underlying stream throws an IOException or there
	 *   is an error in the format of the message.
	 */
	public static UDRMessage receive(InputStream in) throws IOException {
		// FIXME: This drops CR/LF instead of telling the user to pass it back later
		// Notes: if serial, reading won't hurt, since it'll just return immediately with
		//   no data if there is no CR/LF, and with the CR/LF if it is there; with UDP,
		//   would block until a packet received if no CR/LF, but we could also check
		//   available() to see if they were there; serial available() will return 0
		byte format = (byte)0;
		
		// Reads until we see a valid format byte
		do {
			int temp = in.read();
			if (temp == -1) throw new IOException("Unexpected EOF");
			
			format = (byte)temp;
		} while (format != F_HEX && format != F_BINARY && format != F_FIXEDCRC);
		
		//Utils.debug("Format is \"" + (char)format + "\"");
		
		int len = read(in, format) & 0xff;
		
		//Utils.debug("Length is " + len);
		
		short destination = readVar(in, format);
		len--;
		
		//Utils.debug("Destination is " + destination);
		
		short source = readVar(in, format);
		len--;
		
		//Utils.debug("Source is " + source);
		
		byte session = read(in, format);
		len--;
		
		//Utils.debug("Session is " + session);
		
		byte sequence = read(in, format);
		len--;
		
		//Utils.debug("Sequence is " + sequence);
		
		byte command = read(in, format);
		len--;
		
		//Utils.debug("Command is " + command);
		
		len -= 2; // CRC
		
		//Utils.debug("Reading " + len + " bytes of data:");
		
		// rest is data
		byte[] data = new byte[len];
		for (int i = 0; i < len; i++) {
			data[i] = read(in, format);
			
			//Utils.debug(String.format("  Read 0x%02x", data[i] & 0xFF));
		}
		
		UDRMessage answer = null;
		switch (command) {
			case C_NOP: {
				answer = new UDR_NoOp(data);
				break;
			}
			case C_ACK: {
				answer = new UDR_Acknowledge(data);
				break;
			}
			case C_NAK: {
				answer = new UDR_NAcknowledge(data);
				break;
			}
			case C_VERS: {
				answer = new UDR_Version(data);
				break;
			}
			case C_NIO: {
				answer = new UDR_NumberIO(data);
				break;
			}
			case C_GETD: {
				answer = new UDR_GetD(data);
				break;
			}
			case C_GETB: {
				answer = new UDR_GetB(data);
				break;
			}
			case C_GETA: {
				answer = new UDR_GetA(data);
				break;
			}
			case C_GETS: {
				answer = new UDR_GetS(data);
				break;
			}
			case C_PUTD: {
				answer = new UDR_PutD(data);
				break;
			}
			case C_PUTB: {
				answer = new UDR_PutB(data);
				break;
			}
			case C_PUTA: {
				answer = new UDR_PutA(data);
				break;
			}
			case C_SETD: {
				answer = new UDR_SetD(data);
				break;
			}
			case C_CLRD: {
				answer = new UDR_ClearD(data);
				break;
			}
			case C_SCLOCK: {
				answer = new UDR_SetClock(data);
				break;
			}
			case C_GCLOCK: {
				answer = new UDR_GetClock(data);
				break;
			}
			case C_IOXCHG: {
				answer = new UDR_IOExchange(data);
				break;
			}
			case C_FILESYS: {
				answer = UDR_Filesys.createInstance(data);
				break;
			}
			case C_GWY: {
				answer = UDR_Gateway.createInstance(data);
				break;
			}
			default: {
				if (C_USER <= command && command <= C_USERMAX) {
					answer = new UDR_User(command, data);
					break;
				} else {
					throw new IOException("Unknown command: " + command);
				}
			}
		}
		
		answer.setFormat(format);
		answer.setDestination(destination);
		answer.setSource(source);
		answer.setSession(session);
		answer.setSequence(sequence);
		
		
		// now grab the CRC and check it (if necessary)
		byte b1 = read(in, format);
		byte b2 = read(in, format);
		
		if (format == F_FIXEDCRC) {
			short crc = (short)((b1 << 8) | b2);
			if (crc != CRC_MAGIC) {
				throw new IOException("Fixed CRC does not match");
			}
		} else {
			byte[] temp = answer.buildNoCheck(); // Put the message back together
			
			// Replace the CRC we just calculated with the one we read
			temp[temp.length - 2] = b1;
			temp[temp.length - 1] = b2;
			
			// Check against their CRC
			if (!checkCRC(temp)) {
				Utils.debug("Received data has bad CRC:");
				Utils.debug("  ", temp);
				
				throw new IOException("CRC does not match");
			}
		}
		
		// TODO: optional CR/LF? if it's there, it gets left; if it's not, we would 
		//   block and lose data trying to read it
		
		return answer;
	}
	
	/**
	 * Convert the input byte array into ASCII hex values, with some optional extra bytes.
	 * @param data The data to convert to hex.
	 * @param extra Extra bytes to leave at the end of the array.
	 * @return ASCII hex chars corresponding to the byte array, with <tt>extra</tt> unused
	 *   bytes at the end.
	 */
	private static byte[] makeHex(byte[] data, int extra) {
		// Each byte takes 2 hex, but the first is not converted
		byte[] hex = new byte[data.length * 2 - 1 + extra];
		
		hex[0] = data[0];
		
		int offset = 1;
		for (int i = 1; i < data.length; i++) {
			hex[offset] = (byte)((data[i] >> 4) & 0x0F);
			
			if (hex[offset] < 10) {
				hex[offset] += '0';
			} else {
				hex[offset] = (byte)(hex[offset] - 10 + 'A');
			}
			
			offset++;
			
			hex[offset] = (byte)(data[i] & 0x0F);
			if (hex[offset] < 10) {
				hex[offset] += '0';
			} else {
				hex[offset] = (byte)(hex[offset] - 10 + 'A');
			}
			
			offset++;
		}
		
		return hex;
	}
	
	/**
	 * Convert ASCII hex into byte data.
	 * @param data ASCII hex data, possibly ending with '\r' and/or '\n'.
	 * @return Byte data converted from ASCII hex. Line endings are left off.
	 */
	protected static byte[] parseHex(byte[] data) {
		byte end1, end2;
		end1 = data[data.length - 1];
		end2 = data[data.length - 2];
		
		int len = data.length;
		if (end1 == '\r' || end1 == '\n') len--;
		if (end2 == '\r') len--;
		
		byte[] bin = new byte[len / 2 + 1]; // Add one for the format char at the beginning
		
		bin[0] = data[0];
		
		int offset = 1;
		for (int i = 1; i < bin.length; i++) {
			byte c = data[offset++];
			
			if ('0' <= c && c <= '9') {
				c -= '0';
			} else if ('a' <= c && c <= 'f') {
				c -= 'a';
			} else if ('A' <= c && c <= 'F') {
				c -= 'A';
			} else {
				return null;
			}
			
			bin[i] = (byte)(c << 4);
			
			c = data[offset++];
			
			if ('0' <= c && c <= '9') {
				c -= '0';
			} else if ('a' <= c && c <= 'f') {
				c -= 'a';
			} else if ('A' <= c && c <= 'F') {
				c -= 'A';
			} else {
				return null;
			}
			
			bin[i] |= c;
		}
		
		return bin;
	}
	
	/**
	 * Compute the CRC for the provided bytes (skipping the first and where the CRC goes).
	 * @param msg The message bytes.
	 * @param offset Offset of where the CRC will be written.
	 * @return The offset after incrementing past the CRC.
	 */
	private static int makeCRC(byte[] msg, int offset) {
		short crc = CRC_INIT;
		
		for (int i = 1; i < offset; i++) { // Do not include the lead char
			crc = crccitt(crc, msg[i]);
		}
		
		crc = (short)~crc;
		
		msg[offset++] = (byte)((crc >> 8) & 0xFF);
		msg[offset++] = (byte)(crc & 0xFF);
		
		return offset;
	}
	
	/**
	 * Check whether the provided message bytes have the proper CRC.
	 * @param msg The message bytes (not ASCII hex).
	 * @return True = matching CRC, False = bad CRC.
	 */
	private static boolean checkCRC(byte[] msg) {
		short crc = CRC_INIT;
		
		for (int i = 1; i < msg.length; i++) {
			crc = crccitt(crc, msg[i]);
		}
		
		return (crc == CRC_MAGIC);
	}
	
	/**
	 * Computes a partial CRC.
	 * @param crc The current CRC value.
	 * @param data The data byte to add to the CRC value.
	 * @return The new CRC value.
	 */
	public static short crccitt(short crc, byte data) {
		// I've tried reducing this, but it never comes out much clearer
		crc = (short)(((((crc & 0x00ff) << 8) | ((crc & 0xff00) >> 8)) ^ (data & 0xff)) & 0xffff); 
		crc ^= (short)(((crc & 0xf0) >> 4) & 0xffff);
		crc ^= (short)((((crc & 0x0f) << 12) ^ ((crc & 0xff) << 5)) & 0xffff);
		
		return crc;
	}
	
	/**
	 * Read a byte from an input stream using the provided format.
	 * @param in The input stream to read from.
	 * @param fmt The format (so we know whether to read one or two chars).
	 * @return The byte read from the stream (not ASCII hex).
	 * @throws IOException If the underlying stream throws an IOException, ends unexpectedly,
	 *   or should be ASCII hex and has an illegal char.
	 */
	private static byte read(InputStream in, int fmt) throws IOException {
		if (fmt == F_HEX) {
			int b1 = in.read();
			int b2 = in.read();
			byte ans = 0;
			
			if (b2 == -1) throw new IOException("Unexpected EOF"); // if b1 == -1, b2 definitely == -1
			
			if ('0' <= b1 && b1 <= '9') {
				b1 -= '0';
			} else if ('a' <= b1 && b1 <= 'f') {
				b1 -= 'a';
			} else if ('A' <= b1 && b1 <= 'F') {
				b1 -= 'A';
			} else {
				throw new IOException(String.format("Unexpected char in stream: 0x%02x" + (byte)(b1 & 0xff)));
			}
			
			ans = (byte)((b1 << 4) & 0xF0);
			
			if ('0' <= b2 && b2 <= '9') {
				b2 -= '0';
			} else if ('a' <= b2 && b2 <= 'f') {
				b2 -= 'a';
			} else if ('A' <= b2 && b2 <= 'F') {
				b2 -= 'A';
			} else {
				throw new IOException("Unexpected char in stream: " + (char)b1);
			}
			
			ans |= b2;
			
			return ans;
		} else {
			int b = in.read();
			
			if (b == -1) throw new IOException("Unexpected EOF");
			
			return (byte)b;
		}
	}
	
	/**
	 * Verify that this message contains all necessary pieces and build it
	 * into a byte array.
	 * @return The byte array representing this message.
	 * @throws IllegalArgumentException If the message is not legal.
	 */
	public byte[] build() {
		check();
		return buildNoCheck();
	}
	
	@Override
	public byte[] getBytes()
	{
		return build();
	}
	
	@Override
	public void toStream(OutputStream out)
		throws IOException, IllegalStateException
	{
		byte[] bytes = getBytes();
		out.write(bytes);
	}
	
	/**
	 * Build this message into a byte array without checking for validity.
	 * @return The byte array representing this message.
	 * @throws IllegalArgumentException If the message is invalid beyond
	 *   saving (too long, for example).
	 */
	protected byte[] buildNoCheck() {
		int realLen;
		int len;
		
		// dest + source + session + sequence + command + data + crc*2
		realLen = len = 5 + data.length + 2;
		
		// format + len
		realLen += 2;
		
		if (source > 127 && source != STA_ANY) realLen++;
		if (destination > 127 && destination != STA_ANY) realLen++;
		
		if (len > 255) throw new IllegalArgumentException("Message is too long (length is " + len + ")");
		
		byte[] msg = new byte[realLen];
		int offset = 0;
		
		msg[offset++] = format; // The format is a legal ASCII char and does not get converted to something else
		msg[offset++] = (byte)(len & 0xFF);
		
		offset = putVar(destination, msg, offset);
		offset = putVar(source, msg, offset);
		
		msg[offset++] = session;
		msg[offset++] = sequence;
		
		msg[offset++] = command;
		
		for (int i = 0; i < data.length; i++) {
			msg[offset++] = data[i];
		}
		
		if (format == F_FIXEDCRC) {
			msg[offset++] = (byte)((CRC_MAGIC >> 8) & 0xFF);
			msg[offset++] = (byte)(CRC_MAGIC & 0xFF);
		} else {
			offset = makeCRC(msg, offset);
		}
		
		if (format == F_HEX) {
			byte[] termBytes = term.getBytes();
			msg = makeHex(msg, termBytes.length);
			System.arraycopy(termBytes, 0, msg, msg.length - termBytes.length, termBytes.length);
		}
		
		/*
		// For debugging
		for (int i = 0; i < msg.length; i++) {
			Utils.debug("msg[" + i + "] = " + (msg[i] & 0xFF));
		}
		*/
		
		return msg;
	}
	
	/**
	 * Produce a string representation of this message with no indentation.
	 */
	@Override
    public String toString() {
		return toString(0);
	}
	
	/**
	 * Produce a string representation of this message with the given number of
	 *   indentation characters.
	 * @param indent The number of spaces to prepend to every line of the output.
	 * @return An indented string representation of this message.
	 */
	public String toString(int indent) {
		char[] indentChars = new char[indent];
		Arrays.fill(indentChars, ' ');
		
		return toString(new String(indentChars));
	}
	
	/**
	 * Produce a string representation of this message with the given string
	 *   indentation.
	 * @param indent A string to prepend to every line of the output.
	 * @return An indented string representation of this message.
	 */
	public String toString(String indent) {
		byte[] fullMsg = buildNoCheck();
		if (format == F_HEX) {
			fullMsg = parseHex(fullMsg);
		}
		
		StringBuilder ans = new StringBuilder();
		
		ans.append(String.format("%s%s\n", indent, getCommandString()));
		
		ans.append(String.format("%1$sFormat:      0x%2$02x   (%2$5d) %3$s\n", indent, format & 0xFF, getFormatString()));
		ans.append(String.format("%1$sLength:      0x%2$02x   (%2$5d)\n", indent, fullMsg[1] & 0xFF));
		
		if (destination == -1) {
			ans.append(String.format("%1$sDestination: 0xffff (Unset)\n", indent));
		} else {
			ans.append(String.format("%1$sDestination: 0x%2$04x (%2$5d)\n", indent, destination & 0xFFFF));
		}
		
		if (source == -1) {
			ans.append(String.format("%1$sSource:      0xffff (Unset)\n", indent));
		} else {
			ans.append(String.format("%1$sSource:      0x%2$04x (%2$5d)\n", indent, source & 0xFFFF));
		}
		
		ans.append(String.format("%1$sSession:     0x%2$02x   (%2$5d)\n", indent, session & 0xFF));
		ans.append(String.format("%1$sSequence:    0x%2$02x   (%2$5d)\n", indent, sequence & 0xFF));
		ans.append(String.format("%1$sCommand:     0x%2$02x   (%2$5d) %3$s\n", indent, command & 0xFF, getCommandString()));
		
		if (data == null) {
			ans.append(String.format("%1$sData:               (Unset)\n", indent));
		} else if (data.length == 0) {
			ans.append(String.format("%1$sData:               ( None)\n", indent));
		} else {
			ans.append(describeData(indent));
			ans.append("\n");
		}
		
		byte[] crc = null;
		
		// CRC is the last two bytes
		crc = new byte[2];
		System.arraycopy(fullMsg, fullMsg.length - 2, crc, 0, 2);
		
		ans.append(String.format("%1$sCRC(1):      0x%2$02x   (%2$5d)\n", indent, crc[0] & 0xFF));
		ans.append(String.format("%1$sCRC(2):      0x%2$02x   (%2$5d)", indent, crc[1] & 0xFF));
		
		return ans.toString();
	}
	
	/**
	 * Describe an acknowledgment to this message.
	 * @param ack The acknowledgment to describe.
	 * @return The description of the acknowledgment with no indentation.
	 */
	public String toString(UDR_Acknowledge ack) {
		return toString(ack, 0);
	}
	
	/**
	 * Describe an acknowledgment to this message with the given amount of indentation.
	 * @param ack The acknowledgment to describe.
	 * @param indent The number of spaces to prepend to each line of the output.
	 * @return The description of the acknowledgment.
	 */
	public String toString(UDR_Acknowledge ack, int indent) {
		char[] indentChars = new char[indent];
		Arrays.fill(indentChars, ' ');
		
		return toString(ack, new String(indentChars));
	}
	
	/**
	 * Describe an acknowledgment to this message with a given indentation string.
	 * @param ack The acknowledgment to describe.
	 * @param indent The indentation string to prepend to each line of the output.
	 * @return The description of the acknowledgment.
	 */
	public String toString(UDR_Acknowledge ack, String indent) {
		byte[] fullMsg = ack.buildNoCheck();
		if (format == F_HEX) {
			fullMsg = parseHex(fullMsg);
		}
		
		StringBuilder ans = new StringBuilder();
		
		ans.append(String.format("%sAcknowledgment to %s\n", indent, getCommandString()));
		
		ans.append(String.format("%1$sFormat:      0x%2$02x   (%2$5d) %3$s\n", indent, ack.getFormat() & 0xFF, ack.getFormatString()));
		ans.append(String.format("%1$sLength:      0x%2$02x   (%2$5d)\n", indent, fullMsg[1] & 0xFF));
		
		if (ack.getDestination() == -1) {
			ans.append(String.format("%1$sDestination: 0xffff (Unset)\n", indent));
		} else {
			ans.append(String.format("%1$sDestination: 0x%2$04x (%2$5d)\n", indent, ack.getDestination() & 0xFFFF));
		}
		
		if (ack.getSource() == -1) {
			ans.append(String.format("%1$sSource:      0xffff (Unset)\n", indent));
		} else {
			ans.append(String.format("%1$sSource:      0x%2$04x (%2$5d)\n", indent, ack.getSource() & 0xFFFF));
		}
		
		ans.append(String.format("%1$sSession:     0x%2$02x   (%2$5d)\n", indent, ack.getSession() & 0xFF));
		ans.append(String.format("%1$sSequence:    0x%2$02x   (%2$5d)\n", indent, ack.getSequence() & 0xFF));
		ans.append(String.format("%1$sCommand:     0x%2$02x   (%2$5d) %3$s\n", indent, ack.getCommand() & 0xFF, ack.getCommandString()));
		
		if (ack.getData() == null) {
			ans.append(String.format("%1$sData:               (Unset)\n", indent));
		} else if (ack.getData().length == 0) {
			ans.append(String.format("%1$sData:               ( None)\n", indent));
		} else {
			ans.append(describeData(ack, indent));
			ans.append("\n");
		}
		
		byte[] crc = null;
		
		// CRC is the last two bytes
		crc = new byte[2];
		System.arraycopy(fullMsg, fullMsg.length - 2, crc, 0, 2);
		
		ans.append(String.format("%1$sCRC(1):      0x%2$02x   (%2$5d)\n", indent, crc[0] & 0xFF));
		ans.append(String.format("%1$sCRC(2):      0x%2$02x   (%2$5d)", indent, crc[1] & 0xFF));
		
		return ans.toString();
	}
	
	/**
	 * Describe the command byte of this message. This is meant to be
	 *   overridden by subclasses.
	 */
	public String getCommandString() {
		if (command == -1) {
			return "Unset";
		} else {
			return "Unknown " + command;
		}
	}
	
	/**
	 * Describe the format byte of this message. This is <b>not</b> meant
	 *   to be overridden by subclasses.
	 */
	public String getFormatString() {
		switch (format) {
			case -1:
				return "Unset";
			case F_BINARY:
				return "Binary";
			case F_HEX:
				return "Hex";
			case F_FIXEDCRC:
				return "Fixed CRC";
			default:
				return "Unknown";
		}
	}
	
	
	/**
	 * Verify that the provided acknowledgment is acceptable for this message.
	 *   Subclasses should override this, as currently it always returns True.
	 * @param ack The acknowledgment to test.
	 * @return True = the ack is acceptable; False = the acknowledgment is bad.
	 */
	public boolean verifyAck(UDR_Acknowledge ack) {
		return true;
	}
	
	/**
	 * Describe the data payload of this message with no indentation.
	 */
	public String describeData() {
		return describeData(0);
	}
	
	/**
	 * Describe the data payload of this message with some spaces for indentation.
	 * Calls {@link #describeData(String)}, passing 'indent' spaces.
	 * @param indent The number of spaces to use to indent.
	 */
	public String describeData(int indent) {
		char[] indentChars = new char[indent];
		Arrays.fill(indentChars, ' ');
		
		return describeData(new String(indentChars));
	}
	
	/**
	 * Describe the data payload of the given acknowledgment with no indentation.
	 * @param ack The acknowledgment to describe.
	 * @return A description of the data payload of the acknowledgment.
	 */
	public String describeData(UDR_Acknowledge ack) {
		return describeData(ack, 0);
	}
	
	/**
	 * Describe the data payload of the given acknowledgment with some indentation spaces.
	 * @param ack The acknowledgment to describe.
	 * @param indent The number of spaces to use for indentation.
	 * @return A description of the payload of the acknowledgment with indentation.
	 */
	public String describeData(UDR_Acknowledge ack, int indent) {
		char[] indentChars = new char[indent];
		Arrays.fill(indentChars, ' ');
		
		return describeData(ack, new String(indentChars));
	}
	
	/**
	 * Describe the data payload of this message with the given indentation string.
	 * @param indent The indentation string.
	 * @return A description of the payload of this message.
	 */
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe the data payload of the given acknowledgment using the given indentation string.
	 * @param ack The acknowledgment to describe.
	 * @param indent The indentation string.
	 * @return An indented description of the payload of the acknowledgment.
	 */
	public String describeData(UDR_Acknowledge ack, String indent) {
		return ack.toString(indent);
	}
	
	// Public static helper functions
	// These are some helper functions, mainly for subclasses
	/**
	 * Describe a given byte.
	 * @param b The byte to describe.
	 * @return A description of the byte in the format '0xhh   (  ddd) [bbbb bbbb]'
	 *   where 'h' is a hex digit, 'd' is a decimal digit, and 'b' is a 0 or 1.
	 */
	public static String describeByte(byte b) {
		StringBuilder ans = new StringBuilder();
		
		ans.append(String.format("0x%1$02x   (%1$5d) [", b & 0xFF));
		
		for (int bit = 7; bit >= 0; bit--) {
			if (bit == 3) ans.append(' ');
			
			if ((b & (1 << bit)) == 0) {
				ans.append('0');
			} else {
				ans.append('1');
			}
		}
		
		ans.append(']');
		
		return ans.toString();
	}
	
	/**
	 * Describe the given byte as a type.
	 * @param b The byte to describe.
	 * @return A description such as 'Default Analog In' or 'Physical Discrete Out'.
	 */
	public static String describeType(byte b) {
		String ans = "";
		
		if ((b & T_BIGENDIAN) == T_BIGENDIAN) {
			ans += "BigEndian ";
			
			b &= (byte)((~T_BIGENDIAN) & 0xff);
		}
		
		switch (b) {
			case T_D_AIN:
				ans += "Default Analog In";
				break;
			case T_D_AOUT:
				ans += "Default Analog Out";
				break;
			case T_C_AIN:
				ans += "Communication Analog In";
				break;
			case T_C_AOUT:
				ans += "Communication Analog Out";
				break;
			case T_P_AIN:
				ans += "Physical Analog In";
				break;
			case T_P_AOUT:
				ans += "Physical Analog Out";
				break;
			case T_D_DIN:
				ans += "Default Discrete In";
				break;
			case T_D_DOUT:
				ans += "Default Discrete Out";
				break;
			case T_C_DIN:
				ans += "Communication Discrete In";
				break;
			case T_C_DOUT:
				ans += "Communication Discrete Out";
				break;
			case T_P_DIN:
				ans += "Physical Discrete In";
				break;
			case T_P_DOUT:
				ans += "Physical Discrete Out";
				break;
			case T_LONGINT_IN:
				ans += "Long In";
				break;
			case T_LONGINT_OUT:
				ans += "Long Out";
				break;
			case T_FLOAT_IN:
				ans += "Float In";
				break;
			case T_FLOAT_OUT:
				ans += "Float Out";
				break;
			default:
				if (T_USER <= b && b <= T_USERMAX) {
					ans += "User " + b;
				} else {
					ans += "Unknown " + b;
				}
		}
		
		return ans;
	}
}

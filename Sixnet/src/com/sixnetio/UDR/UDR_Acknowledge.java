/*
 * UDR_ACK.java
 *
 * A ACK UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

public class UDR_Acknowledge extends UDRMessage {
	protected UDR_Acknowledge(byte cmd, byte[] data) {
		//Used when receiving a new message
		super.setCommand(cmd);
		setData(data);
	}
	
	public UDR_Acknowledge(byte[] vals) {
		super.setCommand(UDRMessage.C_ACK);
		
		byte[] data = new byte[vals.length];
		
		System.arraycopy(vals, 0, data, 0, data.length);
		
		setData(data);
	}
	
	public UDR_Acknowledge acknowledge() {
		throw new IllegalStateException("Cannot ACK an ACK");
	}
	
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	public String getCommandString() {
		return "ACK";
	}
}

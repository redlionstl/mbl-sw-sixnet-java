/*
 * UDR_SCLOCK.java
 *
 * A SCLOCK UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.*;

/**
 * A message to set the clock in the device.
 *
 * @author Jonathan Pearson
 */
public class UDR_SetClock extends UDRMessage {
	// Acknowledgment translation functions
	/**
	 * Verify the given acknowledgment matches this message.
	 * @return True = the acknowledgment matches; False = the acknowledgment is bad.
	 */
	public boolean verifyAck(UDR_Acknowledge ack) {
	    return (ack.getData().length >= 1);
    }
    
	/**
	 * Get the status byte of the message. See S_* in this class.
	 * @param ack The acknowledgment to read from.
	 * @return The status byte.
	 */
	public static byte getStatus(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	/**
	 * Describe a status byte.
	 * @param status The status byte to describe.
	 * @return A description of the status byte.
	 */
	public static String translateStatus(byte status) {
		switch (status) {
			case S_NOCLOCK: return "No Clock";
			case S_MUX_EXP1SET: return "ST Clock or MUX Expansion Port 1 Clock Set";
			case S_MUX_EXP2SET: return "MUX Expansion Port 2 Clock Set";
			case S_MUX_MAINSET: return "MUX Main Clock Set";
			case S_MUX_SLAVESET: return "MUX Slave Clock Set";
			default: return "(Unknown Status)";
		}
	}
	
	// Common values
	/** Device reports that it has no clock. */
	public static final byte S_NOCLOCK = 0;
	
	/** Mux device reports that the clock on expansion port 1 has been set. */
	public static final byte S_MUX_EXP1SET = 1;
	
	/** Mux device reports that the clock on expansion port 2 has been set. */
	public static final byte S_MUX_EXP2SET = 2;
	
	/** Mux device reports that the clock on the main board has been set. */
	public static final byte S_MUX_MAINSET = 3;
	
	/** Mux device reports that the clock on the slave has been set. */
	public static final byte S_MUX_SLAVESET = 4;
	
	/** SixTrak device reports that its clock has been set. */
	public static final byte S_ST_CLOCKSET = 1;
	
	/**
	 * Used by UDRMessage.receive() to construct this message given a data payload.
	 * @param data The data payload.
	 */
	protected UDR_SetClock(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_SCLOCK);
		setData(data);
	}
	
	/**
	 * Construct a new message of this type.
	 * @param time The time to set the clock to in seconds since the epoch (January 1, 1970).
	 */
	public UDR_SetClock(int time) {
		super.setCommand(UDRMessage.C_SCLOCK);
		
		byte[] data = new byte[4];
		
		// No type, so use big-endian
		Conversion.intToBytes(data, 0, time);
		
		setData(data);
	}
	
	/**
	 * Automatically construct an acknowledgment to this message. Assumes that all data
	 *   points in the request were written.
	 * @param status The status byte to report.
	 * @return An acknowledgment to this message.
	 */
	public UDR_Acknowledge acknowledge(byte status) {
		// Choose from S_MUX_* or S_ST_* for the status byte
		
		byte[] data = new byte[1];
		data[0] = status;
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Get the time being set by this message, in seconds since the epoch (January 1, 1970).
	 */
	public int getTime() {
		return Conversion.bytesToInt(getData(), 0);
	}
	
	/**
	 * Unless val == getCommand(), throws an IllegalArgumentException.
	 */
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Describe the command byte of this message.
	 */
	public String getCommandString() {
		return "SClock";
	}
	
	/**
	 * Describe the data payload of this message.
	 * @param indent The string to use to indent each line.
	 */
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) ans.append(String.format(" Time = %d", getTime()));
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe the data payload of an acknowledgment to this message.
	 * @param ack The acknowledgment to describe.
	 * @param indent The string to use to indent each line.
	 */
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) ans.append(String.format(" Status = %s", getStatus(ack)));
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

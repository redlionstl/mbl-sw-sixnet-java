/*
 * GWY_SetAllModulesBlinkRate.java
 *
 * A Set All Modules Blink Rate gateway message.
 *
 * Jonathan Pearson
 * February 15, 2008
 *
 */

package com.sixnetio.UDR.Gateway;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

public class GWY_SetAllModulesBlinkRate extends UDR_Gateway {
	// Acknowledgment translation functions
	// No data in the ACK
	
	// Interval Specifiers
	public static final short INT_LED_FULL_ON = 0,
	                          INT_LED_FULL_OFF = 1,
	                          INT_LED_FAST_BLINK = 2,
	                          INT_LED_SLOW_BLINK = 3;
	
	public GWY_SetAllModulesBlinkRate(byte[] data) {
		super(UDR_Gateway.C_SET_ALL_MODULES_BLINK_RATE);
		setData(data);
	}
	
	public GWY_SetAllModulesBlinkRate(short intervalSpecifier) {
		super(UDR_Gateway.C_SET_ALL_MODULES_BLINK_RATE);
		
		byte[] data = new byte[2];
		Conversion.shortToBytes(data, 0, intervalSpecifier);
		
		setData(data);
	}
	
	public short getIntervalSpecifier() {
		return Conversion.bytesToShort(getData(), 0);
	}
	
	public UDR_Acknowledge acknowledge() {
		UDR_Acknowledge ack = new UDR_Acknowledge(new byte[0]);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific GWY message");
		}
	}
	
	public String getSubCommandString() {
		return "Set All Modules Blink Rate";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(String.format(" Interval Specifier = %d", getIntervalSpecifier()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

/*
 * GWY_WriteModuleEERom.java
 *
 * A Write Module EERom gateway message.
 *
 * Jonathan Pearson
 * February 15, 2008
 *
 */

package com.sixnetio.UDR.Gateway;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

import java.util.*;

public class GWY_WriteModuleEERom extends UDR_Gateway {
	// Acknowledgment translation functions
	// No acknowledgment data
	
	public static String translateBankSelect(byte bankSelect) {
		String where;
		if ((bankSelect & BankSelect_Module) == BankSelect_Module) {
			where = "Module";
		} else {
			where = "Base";
		}
		
		return String.format("%s %d", where, ((bankSelect & 0xff) >> 1));
	}
	
	// Start with one of these, put the bank number in the upper 7 bits
	public static final byte BankSelect_Base = 0x00,
	                         BankSelect_Module = 0x01;
	
	public GWY_WriteModuleEERom(byte[] data) {
		super(UDR_Gateway.C_WRITE_MODULE_EEROM);
		setData(data);
	}
	
	public GWY_WriteModuleEERom(int serial, byte bankSelect, byte offset, byte[] blockData, Object... moreBlocks) {
		super(UDR_Gateway.C_WRITE_MODULE_EEROM);
		
		int count = 0;
		
		Vector<byte[]> blocks = new Vector<byte[]>();
		
		byte[] block = new byte[3 + blockData.length];
		block[0] = bankSelect;
		block[1] = (byte)blockData.length;
		block[2] = offset;
		System.arraycopy(blockData, 0, block, 3, blockData.length);
		
		count += block.length;
		blocks.add(block);
		
		int i = 0;
		while (i < moreBlocks.length) {
			bankSelect = (Byte)moreBlocks[i++];
			offset = (Byte)moreBlocks[i++];
			blockData = (byte[])moreBlocks[i++];
			
			block = new byte[3 + blockData.length];
			
			block[0] = bankSelect;
			block[1] = (byte)blockData.length;
			block[2] = offset;
			System.arraycopy(blockData, 0, block, 3, blockData.length);
			
			count += block.length;
			blocks.add(block);
		}
		
		byte[] data = new byte[4 + count];
		Conversion.intToBytes(data, 0, serial);
		
		int pos = 4;
		for (byte[] blk : blocks) {
			System.arraycopy(blk, 0, data, pos, blk.length);
			pos += blk.length;
		}
		
		setData(data);
	}
	
	public int getSerial() {
		return Conversion.bytesToInt(getData(), 0);
	}
	
	public int getBlockCount() {
		byte[] data = getData();
		int offset = 4; // skip serial
		int counter = 0;
		
		while (offset < data.length) {
			offset++; // bank select
			offset += 2 + ((int)data[offset] & 0xff); // skip byte count, offset, and data
			
			counter++;
		}
		
		return counter;
	}
	
	public byte getBankSelect(int index) {
		byte[] data = getData();
		int offset = 4; // skip serial
		int counter = 0;
		
		while (counter < index) {
			offset++; // bank select
			offset += 2 + ((int)data[offset] & 0xff); // skip byte count, offset, and data
			
			counter++;
		}
		
		return data[offset];
	}
	
	public byte getCount(int index) {
		byte[] data = getData();
		int offset = 4; // skip serial
		int counter = 0;
		
		while (counter < index) {
			offset++; // bank select
			offset += 2 + ((int)data[offset] & 0xff); // skip byte count, offset, and data
			
			counter++;
		}
		
		return data[offset + 1];
	}
	
	public byte getOffset(int index) {
		byte[] data = getData();
		int offset = 4; // skip serial
		int counter = 0;
		
		while (counter < index) {
			offset++; // bank select
			offset += 2 + (int)(data[offset] & 0xff); // skip byte count, offset, and data
			
			counter++;
		}
		
		return data[offset + 2];
	}
	
	public byte[] getBlockData(int index) {
		byte[] data = getData();
		int offset = 4; // skip serial
		int counter = 0;
		
		while (counter < index) {
			offset++; // bank select
			offset += 2 + ((int)data[offset] & 0xff); // skip byte count, offset, and data
			
			counter++;
		}
		
		byte[] blockData = new byte[(int)(data[offset + 1] & 0xff)];
		System.arraycopy(data, offset + 3, blockData, 0, blockData.length);
		return blockData;
	}
	
	public UDR_Acknowledge acknowledge() {
		UDR_Acknowledge ack = new UDR_Acknowledge(new byte[0]);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific GWY message");
		}
	}
	
	public String getSubCommandString() {
		return "Read Module EERom";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		int nextBlock = 4;
		int thisBlock = 4;
		int counter = 0;
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(String.format(" Base Serial Number = %d", getSerial()));
			} else if (i == thisBlock) {
				ans.append(String.format(" Bank select %s", translateBankSelect(getBankSelect(counter))));
			} else if (i - 1 == thisBlock) {
				ans.append(" Number of bytes");
				nextBlock = thisBlock + 3 + (int)(data[i] & 0xff);
			} else if (i - 2 == thisBlock) {
				ans.append(" Starting byte offset");
			} else if (i - 3 == thisBlock) {
				ans.append(" Block data");
				thisBlock = nextBlock;
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

/*
 * GWY_UsePortEERomProtocol.java
 *
 * A Use Port EERom Protocol gateway message.
 *
 * Jonathan Pearson
 * February 15, 2008
 *
 */

package com.sixnetio.UDR.Gateway;

import com.sixnetio.UDR.*;

public class GWY_UsePortEERomProtocol extends UDR_Gateway {
	// Acknowledgment translation functions
	// No data in the ACK
	
	// Port Specifiers (see sgwymsgs.any.doc, there are a lot)
	
	public GWY_UsePortEERomProtocol(byte[] data) {
		super(UDR_Gateway.C_USE_PORT_EEROM_PROTOCOL);
		setData(data);
	}
	
	public GWY_UsePortEERomProtocol(byte portSpecifier, Byte... moreSpecifiers) {
		super(UDR_Gateway.C_USE_PORT_EEROM_PROTOCOL);
		
		byte[] data = new byte[1 + moreSpecifiers.length];
		
		data[0] = portSpecifier;
		
		for (int i = 0; i < moreSpecifiers.length; i++) {
			data[i + 1] = moreSpecifiers[i];
		}
		
		setData(data);
	}
	
	public int getPortSpecifierCount() {
		return getData().length;
	}
	
	public byte getPortSpecifier(int index) {
		return getData()[index];
	}
	
	public UDR_Acknowledge acknowledge() {
		UDR_Acknowledge ack = new UDR_Acknowledge(new byte[0]);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific GWY message");
		}
	}
	
	public String getSubCommandString() {
		return "Use Port EERom Protocol";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			// Every byte is a port specifier
			ans.append(" Port Specifier");
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

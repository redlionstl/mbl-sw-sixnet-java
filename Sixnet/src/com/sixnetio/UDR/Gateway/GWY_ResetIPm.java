/*
 * GWY_ResetIPm.java
 *
 * A Reset IPm gateway message.
 *
 * Jonathan Pearson
 * October 1, 2008
 *
 */

package com.sixnetio.UDR.Gateway;

import com.sixnetio.UDR.*;
import com.sixnetio.util.Conversion;

public class GWY_ResetIPm extends UDR_Gateway {
	// There is no data in the ACK
	
	public static String translateOptions(byte options) {
		switch (options) {
			case O_SOFT_RESET: return "Soft Reset";
			case O_HARD_RESET: return "Hard Reset";
			default: return "(Unknown Options)";
		}
	}
	
	public static final byte O_SOFT_RESET = 0x00,
	                         O_HARD_RESET = 0x01;
	
	public GWY_ResetIPm(byte[] data) {
		super(UDR_Gateway.C_RESET_IPM);
		setData(data);
	}
	
	public GWY_ResetIPm(int serial, byte options) {
		super(UDR_Gateway.C_RESET_IPM);
		
		byte[] data = new byte[5];
		
		Conversion.intToBytes(data, 0, serial);
		data[4] = options;
		
		setData(data);
	}
	
	public int getSerial() {
		return Conversion.bytesToInt(getData(), 0);
	}
	
	public byte getOptions() {
		return getData()[4];
	}
	
	public UDR_Acknowledge acknowledge() {
		UDR_Acknowledge ack = new UDR_Acknowledge(new byte[0]);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		return ack;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific GWY message");
		}
	}
	
	public String getSubCommandString() {
		return "Reset IPm";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Base serial #%d", getSerial()));
			} else if (i == 4) {
				ans.append(String.format(" Options '%s'", translateOptions(getOptions())));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

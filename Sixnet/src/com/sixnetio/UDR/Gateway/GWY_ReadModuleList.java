/*
 * GWY_ReadModuleList.java
 *
 * A Read Module List gateway message.
 *
 * Jonathan Pearson
 * February 15, 2008
 *
 */

package com.sixnetio.UDR.Gateway;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

import java.util.*;

public class GWY_ReadModuleList extends UDR_Gateway {
	// Acknowledgment translation functions
	public static byte getBlockCount(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static int getSerial(UDR_Acknowledge ack, int index) {
		int offset = 1 + index * 11;
		return Conversion.bytesToInt(ack.getData(), offset);
	}
	
	public static short getBaseType(UDR_Acknowledge ack, int index) {
		int offset = 1 + index * 11 + 4;
		return Conversion.bytesToShort(ack.getData(), offset);
	}
	
	public static short getModuleType(UDR_Acknowledge ack, int index) {
		int offset = 1 + index * 11 + 6;
		return Conversion.bytesToShort(ack.getData(), offset);
	}
	
	public static byte getModuleAttributes(UDR_Acknowledge ack, int index) {
		int offset = 1 + index * 11 + 8;
		return ack.getData()[offset];
	}
	
	public static short getPlacement(UDR_Acknowledge ack, int index) {
		int offset = 1 + index * 11 + 9;
		return Conversion.bytesToShort(ack.getData(), offset);
	}
	
	public static String translateModuleAttributes(UDR_Acknowledge ack, int index) {
		return translateModuleAttributes(getModuleAttributes(ack, index));
	}
	
	public static String translateModuleAttributes(byte attr) {
		switch (attr) {
			case M_ONLINE: return "Online";
			case M_CONFIGURED: return "Configured";
			case M_LED_SET: return "LED Set";
			case M_WATCHDOG: return "Watchdog";
			case M_HOLD_IO: return "Hold I/O";
			case M_CONFIGURATION_READ: return "Configuration Read";
			default: return "(Unknown Module Attributes)";
		}
	}
	
	// Module attributes
	public static final byte M_ONLINE = 0x01,
	                         M_CONFIGURED = 0x02,
	                         M_LED_SET = 0x04,
	                         M_WATCHDOG = 0x08,
	                         M_HOLD_IO = 0x10,
	                         M_CONFIGURATION_READ = 0x20;
	
	public GWY_ReadModuleList(byte[] data) {
		super(UDR_Gateway.C_READ_MODULE_LIST);
		setData(data);
	}
	
	public GWY_ReadModuleList(int serial) {
		super(UDR_Gateway.C_READ_MODULE_LIST);
		
		byte[] data = new byte[4];
		Conversion.intToBytes(data, 0, serial);
		
		setData(data);
	}
	
	public int getSerial() {
		return Conversion.bytesToInt(getData(), 0);
	}
	
	public UDR_Acknowledge acknowledge(int serial, short baseType, short moduleType, byte moduleAttributes, short placement, Object... moreBlocks) {
		Vector<byte[]> blocks = new Vector<byte[]>();
		
		byte[] block = new byte[11];
		Conversion.intToBytes(block, 0, serial);
		Conversion.shortToBytes(block, 4, baseType);
		Conversion.shortToBytes(block, 6, moduleType);
		block[8] = moduleAttributes;
		Conversion.shortToBytes(block, 9, placement);
		
		blocks.add(block);
		
		int i = 0;
		while (i < moreBlocks.length) {
			block = new byte[11];
			
			serial = (Integer)moreBlocks[i++];
			baseType = (Short)moreBlocks[i++];
			moduleType = (Short)moreBlocks[i++];
			moduleAttributes = (Byte)moreBlocks[i++];
			placement = (Short)moreBlocks[i++];
			
			Conversion.intToBytes(block, 0, serial);
			Conversion.shortToBytes(block, 4, baseType);
			Conversion.shortToBytes(block, 6, moduleType);
			block[8] = moduleAttributes;
			Conversion.shortToBytes(block, 9, placement);
			
			blocks.add(block);
		}
		
		byte[] data = new byte[1 + blocks.size() * 11];
		data[0] = (byte)blocks.size();
		
		int offset = 1;
		for (byte[] blk : blocks) {
			System.arraycopy(blk, 0, data, offset, blk.length);
			offset += blk.length;
		}
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific GWY message");
		}
	}
	
	public String getSubCommandString() {
		return "Read Module List";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(String.format(" Starting Module Base Serial = %d", getSerial()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		int nextBlock = 1;
		int counter = 0;
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(String.format(" Number of blocks"));
			} else if (i == nextBlock) {
				ans.append(String.format(" Module serial = %d", getSerial(ack, counter)));
			} else if (i == nextBlock + 4) {
				ans.append(String.format(" Base type word = %d", getBaseType(ack, counter)));
			} else if (i == nextBlock + 6) {
				ans.append(String.format(" Module type word = %d", getModuleType(ack, counter)));
			} else if (i == nextBlock + 8) {
				ans.append(String.format(" Module attributes '%s'", getModuleAttributes(ack, counter)));
			} else if (i == nextBlock + 9) {
				ans.append(String.format(" Coordinates & orientation = %d", getPlacement(ack, counter)));
				
				nextBlock = i + 2;
				counter++;
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

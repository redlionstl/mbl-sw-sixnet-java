/*
 * GWY_ReadModuleEERom.java
 *
 * A Read Module EERom gateway message.
 *
 * Jonathan Pearson
 * February 15, 2008
 *
 */

package com.sixnetio.UDR.Gateway;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

import java.util.*;

public class GWY_ReadModuleEERom extends UDR_Gateway {
	// Acknowledgment translation functions
	public static byte[] getData(UDR_Acknowledge ack) {
		return Arrays.copyOf(ack.getData(), ack.getData().length);
	}
	
	public static String translateBankSelect(byte bankSelect) {
		String where;
		if ((bankSelect & BANK_SELECT_MODULE) == BANK_SELECT_MODULE) {
			where = "Module";
		} else {
			where = "Base";
		}
		
		return String.format("%s %d", where, ((bankSelect & 0xff) >> 1));
	}
	
	// Start with one of these, put the bank number in the upper 7 bits
	public static final byte BANK_SELECT_BASE = 0x00,
	                         BANK_SELECT_MODULE = 0x01;
	
	public GWY_ReadModuleEERom(byte[] data) {
		super(UDR_Gateway.C_READ_MODULE_EEROM);
		setData(data);
	}
	
	public GWY_ReadModuleEERom(int serial, byte bankSelect, byte count, byte offset, Byte... moreBlocks) {
		super(UDR_Gateway.C_READ_MODULE_EEROM);
		
		byte[] data = new byte[4 + 3 + 3 * moreBlocks.length];
		Conversion.intToBytes(data, 0, serial);
		data[4] = bankSelect;
		data[5] = count;
		data[6] = offset;
		
		for (int i = 0; i < moreBlocks.length; i++) {
			data[i + 7] = moreBlocks[i];
		}
		
		setData(data);
	}
	
	public int getSerial() {
		return Conversion.bytesToInt(getData(), 0);
	}
	
	public int getBlockCount() {
		return ((getData().length - 4) / 3);
	}
	
	public byte getBankSelect(int index) {
		return getData()[4 + index * 3];
	}
	
	public byte getCount(int index) {
		return getData()[4 + index * 3 + 1];
	}
	
	public byte getOffset(int index) {
		return getData()[4 + index * 3 + 2];
	}
	
	public UDR_Acknowledge acknowledge(byte[] data) {
		UDR_Acknowledge ack = new UDR_Acknowledge(Arrays.copyOf(data, data.length));
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific GWY message");
		}
	}
	
	public String getSubCommandString() {
		return "Read Module EERom";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(" Base Serial Number");
			} else if ((i - 4) % 3 == 0) {
				ans.append(" Bank select");
			} else if ((i - 5) % 11 == 0) {
				ans.append(" Number of bytes");
			} else if ((i - 6) % 11 == 0) {
				ans.append(" Starting byte offset");
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(" EERom data");
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

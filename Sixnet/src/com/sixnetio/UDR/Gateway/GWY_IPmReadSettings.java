/*
 * GWY_IPmReadSettings.java
 *
 * An IPm Read Settings gateway message.
 *
 * Jonathan Pearson
 * December 4, 2008
 *
 */

package com.sixnetio.UDR.Gateway;

import com.sixnetio.UDR.*;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.Utils;

public class GWY_IPmReadSettings extends UDR_Gateway {
	// Acknowledgment translation functions
	public static int getSerial(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 0);
	}
	
	public static byte getParameterSelect(UDR_Acknowledge ack) {
		return ack.getData()[4];
	}
	
	public static short getStationNumber(UDR_Acknowledge ack) {
		return Conversion.bytesToShort(ack.getData(), 5);
	}
	
	public static int getIPAddr(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 7);
	}
	
	public static int getNetMask(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 11);
	}
	
	public static int getSecondIPAddr(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 15);
	}
	
	public static int getSecondNetMask(UDR_Acknowledge ack) {
		return Conversion.bytesToInt(ack.getData(), 19);
	}
	
	public static String translateParameterSelect(byte paramSelect) {
		switch (paramSelect) {
			case PS_SERIAL_AND_IP: return "Serial Number and IP data";
			default: return "(Unknown Parameter Select)";
		}
	}
	
	public static final byte PS_SERIAL_AND_IP = (byte)0;
	
	public static final int ANY_SERIAL = 0xffffffff;
	
	public GWY_IPmReadSettings(byte[] data) {
		super(UDR_Gateway.C_IPM_READ_SETTINGS);
		setData(data);
	}
	
	// Technically, you can request more than one parameter, but there *is* only one parameter
	//   so it would have been a waste of time to implement all of the scanning
	public GWY_IPmReadSettings(int serial, byte paramSelect) {
		super(UDR_Gateway.C_IPM_READ_SETTINGS);
		
		byte[] data = new byte[5];
		
		Conversion.intToBytes(data, 0, serial);
		data[4] = paramSelect;
		
		setData(data);
	}
	
	public int getSerial() {
		return Conversion.bytesToInt(getData(), 0);
	}
	
	public byte getParameterSelect() {
		return getData()[4];
	}
	
	public UDR_Acknowledge acknowledge(int serial, byte paramSelect, short stationNumber, int ipAddr, int netMask) {
		byte[] data = new byte[15];
		
		Conversion.intToBytes(data, 0, serial);
		data[4] = paramSelect;
		Conversion.shortToBytes(data, 5, stationNumber);
		Conversion.intToBytes(data, 7, ipAddr);
		Conversion.intToBytes(data, 11, netMask);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		return ack;
	}
	
	public UDR_Acknowledge acknowledge(int serial, byte paramSelect, short stationNumber, int ipAddr, int netMask, int ipAddr2, int netMask2) {
		byte[] data = new byte[23];
		
		Conversion.intToBytes(data, 0, serial);
		data[4] = paramSelect;
		Conversion.shortToBytes(data, 5, stationNumber);
		Conversion.intToBytes(data, 7, ipAddr);
		Conversion.intToBytes(data, 11, netMask);
		Conversion.intToBytes(data, 15, ipAddr2);
		Conversion.intToBytes(data, 19, netMask2);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		return ack;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific GWY message");
		}
	}
	
	public String getSubCommandString() {
		return "IPm Read Settings";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Base serial #%d", getSerial()));
			} else if (i == 4) {
				ans.append(String.format(" Parameter Select '%s'", translateParameterSelect(getParameterSelect())));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(String.format(" Base Serial Number = %d", getSerial(ack)));
			} else if (i == 4) {
				ans.append(String.format(" Parameter Select '%s'", translateParameterSelect(getParameterSelect(ack))));
			} else if (i == 5) {
				ans.append(String.format(" Station Number = %d", getStationNumber(ack)));
			} else if (i == 7) {
				ans.append(String.format(" IP Address '%s'", Utils.makeIP(getIPAddr(ack))));
			} else if (i == 11) {
				ans.append(String.format(" Netmask '%s'", Utils.makeIP(getNetMask(ack))));
			} else if (i == 15) {
				ans.append(String.format(" IP Address 2 '%s'", Utils.makeIP(getSecondIPAddr(ack))));
			} else if (i == 19) {
				ans.append(String.format(" Netmask 2 '%s'", Utils.makeIP(getSecondNetMask(ack))));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

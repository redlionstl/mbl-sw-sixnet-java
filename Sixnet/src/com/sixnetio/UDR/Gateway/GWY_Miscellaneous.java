/*
 * GWY_Miscellaneous.java
 *
 * A Miscellaneous gateway message.
 *
 * Jonathan Pearson
 * February 15, 2008
 *
 */

package com.sixnetio.UDR.Gateway;

import com.sixnetio.UDR.*;
import com.sixnetio.util.*;

import java.util.*;

public class GWY_Miscellaneous extends UDR_Gateway {
	// Acknowledgment translation functions
	public static byte[] getParameters(UDR_Acknowledge ack) {
		return Arrays.copyOf(ack.getData(), ack.getData().length);
	}
	
	// Command specifiers
	// SIXTRAK
	public static final short ST_LED_FULL_ON = 0,
	                          ST_LED_FULL_OFF = 1,
	                          ST_LED_FAST_BLINK = 2,
	                          ST_LED_SLOW_BLINK = 3;
	
	// RemoteTrak/EtherTrak
	// Unless otherwise specified, the user program is in control of the LED in these states and the module will not override
	public static final short RE_LED_FULL_ON = 0, // Communications timeout has occurred, module in control of the LED
	                          RE_LED_FULL_OFF = 1,
	                          RE_LED_FAST_BLINK = 2,
	                          RE_LED_FAIL_SELFTEST = 3,
	                          RE_LED_UNCONFIGURED = 4,
	                          RE_LED_OPERATIONAL = 5; // Communications are valid, module in control of the LED
	
	// DHCP
	public static final short DHCP_READ_STATUS = 0x20,
	                          DHCP_RELEASE = 0x21,
	                          DHCP_RENEW = 0x22;
	
	// DHCP Parameter IDs
	public static byte DHCP_PARAM_CLIENT_IP = 0, // 4 bytes
	                   DHCP_PARAM_SERVER_IP = 1, // 4 bytes
	                   DHCP_PARAM_SUBNET_MASK = 2, // 4 bytes
	                   DHCP_PARAM_DEFAULT_GATEWAY = 3, // 4 bytes
	                   DHCP_PARAM_OBTAINED_TIME = 4, // 4 bytes
	                   DHCP_PARAM_EXPIRE_TIME = 5, // 4 bytes
	                   DHCP_PARAM_RENEW_TIME = 6, // 4 bytes
	                   DHCP_PARAM_REBIND_TIME = 7, // 4 bytes
	                   DHCP_PARAM_ERROR_CODE = 8, // 1 byte
	                   DHCP_PARAM_CURRENT_TIME = 9; // 4 bytes
	
	// DHCP status byte values
	public static final byte DHCP_STATUS_SHUTDOWN = 0x00,
	                         DHCP_STATUS_RELEASE = 0x10,
	                         DHCP_STATUS_INIT_REBOOT = 0x20,
	                         DHCP_STATUS_INIT = 0x30,
	                         DHCP_STATUS_SELECTING = 0x40,
	                         DHCP_STATUS_REQUESTING = 0x50,
	                         DHCP_STATUS_BOUND = 0x60,
	                         DHCP_STATUS_RENEWING = 0x70,
	                         DHCP_STATUS_REBINDING = (byte)0x80;
	
	// DHCP error codes
	public static final byte DHCP_ERROR_NO_ERROR = 0x00,
	                         DHCP_ERROR_DISCOVER_TIMEOUT = 0x01,
	                         DHCP_ERROR_REQUEST_TIMEOUT = 0x02,
	                         DHCP_ERROR_RENEW_NAK = 0x03,
	                         DHCP_ERROR_REBIND_NAK = 0x04,
	                         DHCP_ERROR_EXPIRED = 0x05;
	
	// For DHCP messages, the parameter list is:
	// <Ethernet Port Number><DHCP Status>
	// <First Parameter ID><First Parameter Length><First Parameter Value>...
	// ...
	// <Last Parameter ID><Last Parameter Length><Last Parameter Value>...
	
	public GWY_Miscellaneous(byte[] data) {
		super(UDR_Gateway.C_MISCELLANEOUS);
		setData(data);
	}
	
	public GWY_Miscellaneous(int serial, short commandSpecifier, byte[] parameters) {
		super(UDR_Gateway.C_MISCELLANEOUS);
		
		byte[] data = new byte[6 + parameters.length];
		Conversion.intToBytes(data, 0, serial);
		Conversion.shortToBytes(data, 4, commandSpecifier);
		System.arraycopy(parameters, 0, data, 6, parameters.length);
		
		setData(data);
	}
	
	public int getSerial() {
		return Conversion.bytesToInt(getData(), 0);
	}
	
	public short getCommandSpecifier() {
		return Conversion.bytesToShort(getData(), 4);
	}
	
	public byte[] getParameters() {
		byte[] data = getData();
		
		return Arrays.copyOfRange(data, 6, data.length);
	}
	
	public UDR_Acknowledge acknowledge(byte[] parameters) {
		byte[] data = Arrays.copyOf(parameters, parameters.length);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific GWY message");
		}
	}
	
	public String getSubCommandString() {
		return "Miscellaneous";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(String.format(" Module serial = %d", getSerial()));
			} else if (i == 4) {
				ans.append(String.format(" Command Specifier = %d", getCommandSpecifier()));
			} else if (i == 6) {
				ans.append(" Parameters");
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(" Parameters");
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

/*
 * GWY_Execute.java
 *
 * An Execute gateway message.
 *
 * Jonathan Pearson
 * September 17, 2008
 *
 */

package com.sixnetio.UDR.Gateway;

import com.sixnetio.UDR.*;

public class GWY_Execute extends UDR_Gateway {
	// Acknowledgment translation functions
	public static byte getError(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static String translateError(UDR_Acknowledge ack) {
		return translateError(getError(ack));
	}
	
	public static byte getResponseLength(UDR_Acknowledge ack) {
		return ack.getData()[1];
	}
	
	public static String getResponse(UDR_Acknowledge ack) {
		int size = (int)(getResponseLength(ack) & 0xff);
		
		if (size == 0) return "";
		
		if (ack.getData()[2 + size - 1] == 0) {
			// Skip the terminating \0 if it's there
			return new String(ack.getData(), 2, size - 1);
		} else {
			return new String(ack.getData(), 2, size);
		}
	}
	
	// Translations
	public static String translateOptions(byte options) {
		switch (options) {
			case O_NONE:
				return "No Options";
			default:
				return "(Unknown Options)";
		}
	}
	
	public static String translateError(byte error) {
		switch (error) {
			case E_NO_ERROR:
				return "No Error";
			case E_UNABLE_TO_EXECUTE:
				return "Unable to execute";
			default:
				return "(Unknown Error)";
		}
	}
	
	// Versions
	public static final byte V_1 = 1;
	
	// Options
	public static final byte O_NONE = 0;
	
	// Error codes
	public static final byte E_NO_ERROR = 0x00,
	                         E_UNABLE_TO_EXECUTE = 0x01;
	
	public GWY_Execute(byte[] data) {
	    super(C_EXECUTE);
	    setData(data);
    }
	
	public GWY_Execute(byte version, byte options, String command) {
		super(C_EXECUTE);
		
		byte[] data = new byte[2 + command.length() + 1];
		
		data[0] = version;
		data[1] = options;
		
		System.arraycopy(command.getBytes(), 0, data, 2, command.length());
		
		data[data.length - 1] = 0;
		
		setData(data);
	}
	
	public byte getVersion() {
		return getData()[0];
	}
	
	public byte getOptions() {
		return getData()[1];
	}
	
	public String getShellCommand() {
		return new String(getData(), 2, getData().length - 3);
	}
	
	public UDR_Acknowledge acknowledge(byte error, String response) {
		byte[] data;
		
		if (response.length() < 244) {
			data = new byte[2 + response.length() + 1];
		} else {
			data = new byte[2 + response.length()];
		}
		
		data[0] = error;
		data[1] = (byte)(response.length() & 0xff);
		
		System.arraycopy(response.getBytes(), 0, data, 2, response.length());
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific GWY message");
		}
	}
	
	public String getSubCommandString() {
		return "Execute";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(" Version");
			} else if (i == 1) {
				ans.append(String.format(" Options '%s'", translateOptions(getOptions())));
			} else if (i == 2) {
				ans.append(String.format(" Command: '%s'", getShellCommand()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}

	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(indent);
			ans.append(String.format("Data(%3d):   ", i) + describeByte(data[i]));
			
			if (i == 0) {
				ans.append(String.format(" Error '%s'", translateError(ack)));
			} else if (i == 1) {
				ans.append(String.format(" Response length"));
			} else if (i == 2) {
				ans.append(String.format(" Response: '%s'", getResponse(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

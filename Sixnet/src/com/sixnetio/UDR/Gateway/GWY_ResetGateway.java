/*
 * GWY_ResetGateway.java
 *
 * A Reset Gateway gateway message.
 *
 * Jonathan Pearson
 * February 4, 2008
 *
 */

package com.sixnetio.UDR.Gateway;

import com.sixnetio.UDR.*;

public class GWY_ResetGateway extends UDR_Gateway {
	// There is no acknowledgment to this message
	
	public GWY_ResetGateway(byte[] data) {
		super(UDR_Gateway.C_RESET_GATEWAY);
		setData(data);
	}
	
	public GWY_ResetGateway() {
		super(UDR_Gateway.C_RESET_GATEWAY);
		setData(new byte[0]);
	}
	
	// There is no acknowledgment to this message
	
	public void setSubCommand(byte subcommand) {
		if (subcommand != getSubCommand()) {
			throw new IllegalArgumentException("Cannot change the subcommand of a specific GWY message");
		}
	}
	
	public String getSubCommandString() {
		return "Reset Gateway";
	}
	
	// No point in overriding describeData, since there is none...
}

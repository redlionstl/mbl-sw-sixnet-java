/*
 * UDR_FILESYS.java
 *
 * The base class for the FILESYS_* subcommands.
 *
 * Jonathan Pearson
 * April 25, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.UDR.Filesys.*;

import java.io.*;

public class UDR_Filesys extends UDRMessage {
	// Constants
	public static final byte C_GETALIAS = 0,
	                         C_READ = 1,
	                         C_WRITE = 2,
	                         C_CREATE = 3,
	                         C_DELETE = 4,
	                         C_DIR = 5,
	                         C_STAT = 6,
	                         C_COMPRESS = 7,
	                         C_CHKDSK = 8,
	                         C_RENAME = 9,
	                         C_MEMAVAIL = 10,
	                         C_FREESPACE = 11,
	                         C_CLOSEALIAS = 12;
	
	// Private members
	private byte subcommand;
	
	// Constructor
	public UDR_Filesys(byte subcommand) {
		super.setCommand(UDRMessage.C_FILESYS);
		this.subcommand = subcommand;
	}
	
	// setData()
	public void setData(byte[] data) {
		byte[] realData = new byte[data.length + 1];
		
		realData[0] = subcommand;
		System.arraycopy(data, 0, realData, 1, data.length);
		
		super.setData(realData);
	}
	
	// getData()
	public byte[] getData() {
		byte[] realData = super.getData();
		byte[] data = new byte[realData.length - 1];
		
		System.arraycopy(realData, 1, data, 0, data.length);
		
		return data;
	}
	
	// setCommand()
	public void setCommand(byte val) {
		if (val != super.getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	// setSubCommand()
	public void setSubCommand(byte val) {
		subcommand = val;
		setData(getData()); // Make sure that UDRMessage has the correct data
	}
	
	// getSubCommand()
	public final byte getSubCommand() {
		return subcommand;
	}
	
	// createInstance()
	protected static UDR_Filesys createInstance(byte[] data) throws IOException {
		UDR_Filesys ans = null;
		
		if (data.length == 0) throw new IOException("FILESYS message has no subcommand");
		
		byte[] subdata = new byte[data.length - 1];
		System.arraycopy(data, 1, subdata, 0, subdata.length);
		
		switch (data[0]) {
			case C_GETALIAS: {
				ans = new FS_GetAlias(subdata);
				break;
			}
			case C_READ: {
				ans = new FS_Read(subdata);
				break;
			}
			case C_WRITE: {
				ans = new FS_Write(subdata);
				break;
			}
			case C_CREATE: {
				ans = new FS_Create(subdata);
				break;
			}
			case C_DELETE: {
				ans = new FS_Delete(subdata);
				break;
			}
			case C_DIR: {
				ans = new FS_Dir(subdata);
				break;
			}
			case C_STAT: {
				ans = new FS_Stat(subdata);
				break;
			}
			case C_COMPRESS: {
				ans = new FS_Compress(subdata);
				break;
			}
			case C_CHKDSK: {
				ans = new FS_CheckDisk(subdata);
				break;
			}
			case C_RENAME: {
				ans = new FS_Rename(subdata);
				break;
			}
			case C_MEMAVAIL: {
				ans = new FS_MemAvail(subdata);
				break;
			}
			case C_FREESPACE: {
				ans = new FS_FreeSpace(subdata);
				break;
			}
			case C_CLOSEALIAS: {
				ans = new FS_CloseAlias(subdata);
				break;
			}
			default: {
				throw new IOException("Unrecognized FILESYS subcommand: " + data[0]);
			}
		}
		
		return ans;
	}
	
	public String getCommandString() {
		return "Filesys " + getSubCommandString();
	}
	
	// This is expected to be overridden
	public String getSubCommandString() {
		if (super.getData() == null) {
			return "Unset";
		} else {
			return "Unknown " + subcommand;
		}
	}
}

/*
 * UDR_NIO.java
 *
 * A NIO UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.*;

/**
 * A message to request the number of available I/O points in a device.
 *
 * @author Jonathan Pearson
 */
public class UDR_NumberIO extends UDRMessage {
	// Acknowledgment translation functions
	/**
	 * Verify the given acknowledgment matches this message.
	 * @return True = the acknowledgment matches; False = the acknowledgment is bad.
	 */
	public boolean verifyAck(UDR_Acknowledge ack) {
		return (ack.getData().length >= 4);
	}
	
	/**
	 * Get the type byte of the acknowledgment.
	 * @param ack The acknowledgment to read from.
	 * @return The type byte of the acknowledgment.
	 */
	public static byte getType(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	/**
	 * Usually gets the number of discretes of the proper type in the device (see details).
	 * @param ack The acknowledgment to read from.
	 * @return If the type byte is one of the analog or discrete types, returns the number
	 *   of discretes of that type (input/output, default/communications/physical) in the
	 *   device. If the type byte is something else, returns the number of inputs/outputs
	 *   of that type in the device. In the 'other' case, this will return the same value
	 *   as getAnalogs().
	 */
	public static short getDiscretes(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 1);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 1);
		}
	}
	
	/**
	 * Usually gets the number of analogs of the proper type in the device (see details).
	 * @param ack The acknowledgment to read from.
	 * @return If the type byte is one of the analog types, returns the number of analogs
	 *   of that type (input/output, default/communications/physical) in the device. If
	 *   the type byte is something else, returns the number of inputs/outputs of that
	 *   type in the device (including the discrete types). In the 'other' case, this
	 *   will return the same value as getDiscretes().
	 */
	public static short getAnalogs(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 3);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 3);
		}
	}
	
	/**
	 * Used by UDRMessage.receive() to construct this message given a data payload.
	 * @param data The data payload.
	 */
	protected UDR_NumberIO(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_NIO);
		setData(data);
	}
	
	/**
	 * Construct a new message of this type.
	 * @param type The type of data array to get a count of.
	 */
	public UDR_NumberIO(byte type) {
		super.setCommand(UDRMessage.C_NIO);
		
		byte[] data = new byte[1];
		data[0] = type;
		
		setData(data);
	}
	
	/**
	 * Automatically construct an acknowledgment to this message.
	 * @param discrete The number of discretes or the number of the proper type
	 *   of value for this implementation (see getDiscretes()).
	 * @param analog The number of analogs or the number of the proper type of
	 *   value for this implementation (see getAnalogs()).
	 * @return An acknowledgment to this message.
	 */
	public UDR_Acknowledge acknowledge(short discrete, short analog) {
		byte[] data = new byte[5];
	
		data[0] = getData()[0]; // type
		
		if ((data[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			Conversion.shortToBytes(data, 1, discrete);
			Conversion.shortToBytes(data, 3, analog);
		} else {
			Conversion.shortToBytesLE(data, 1, discrete);
			Conversion.shortToBytesLE(data, 3, analog);
		}
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Get the type byte of this message. See T_* in UDRMessage.
	 */
	public byte getType() {
		return getData()[0];
	}
	
	/**
	 * Unless val == getCommand(), throws an IllegalArgumentException.
	 */
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Describe the command byte of this message.
	 */
	public String getCommandString() {
		return "NIO";
	}
	
	/**
	 * Describe the data payload of this message.
	 * @param indent The string to use to indent each line.
	 */
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType())));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe the data payload of an acknowledgment to this message.
	 * @param ack The acknowledgment to describe.
	 * @param indent The string to use to indent each line.
	 */
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType(ack))));
			} else if (i == 1) {
				ans.append(String.format(" Discrete count = %d", getDiscretes(ack)));
			} else if (i == 3) {
				ans.append(String.format(" Analog count = %d", getAnalogs(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

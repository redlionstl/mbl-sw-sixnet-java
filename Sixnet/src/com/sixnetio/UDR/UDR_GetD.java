/*
 * UDR_GETD.java
 *
 * A GETD UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.Conversion;

/**
 * A message used to retrieve discrete registers.
 *
 * @author Jonathan Pearson
 */
public class UDR_GetD extends UDRMessage {
	// Acknowledgment translation functions
	/**
	 * Verify that the given ACK matches this message.
	 * 
	 * @param ack The ACK to verify.
	 * @return True = the ACK matches this message, False = the ACK does not
	 *   match.
	 */
	@Override
	public boolean verifyAck(UDR_Acknowledge ack) {
		if (ack.getData().length < 5) return false; // Need to have at least 5 bytes
		
		short num = getNum(ack);
		return (ack.getData().length >= (5 + num / 8 + (num % 8 == 0 ? 0 : 1)) &&
		        getStart(ack) == getStart() &&
		        num == getNum());
	}
	
	/**
	 * Get the type of data retrieved in the ACK (see {@link UDRMessage#T_D_DIN}
	 * and similar).
	 * 
	 * @param ack The ACK.
	 * @return The type of data in the ACK.
	 */
	public static byte getType(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	/**
	 * Get the starting register number retrieved in the ACK.
	 * 
	 * @param ack The ACK.
	 * @return The starting register number.
	 */
	public static short getStart(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 1);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 1);
		}
	}
	
	/**
	 * Get the number of registers retrieved in the ACK.
	 * 
	 * @param ack The ACK.
	 * @return The number of registers retrieved.
	 */
	public static short getNum(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 3);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 3);
		}
	}
	
	/**
	 * Get an individual value retrieved in the ACK.
	 * 
	 * @param ack The ACK.
	 * @param index The index of the value to retrieve
	 *   (0..{@link #getNum(UDR_Acknowledge)}).
	 * @return The discrete value stored at that index.
	 */
	public static boolean getVal(UDR_Acknowledge ack, int index) {
		if (index > getNum(ack)) throw new ArrayIndexOutOfBoundsException(String.format("Index %d out of range [0, %d)", index, getNum(ack)));
		
		return ((ack.getData()[5 + index / 8] & (1 << (index % 8))) != 0);
	}
	
	/**
	 * Get all of the values in the acknowledgment.
	 * 
	 * @param ack The ACK.
	 * @return The values.
	 */
	public static boolean[] getVals(UDR_Acknowledge ack) {
		byte[] bytes= new byte[(getNum(ack) + 7) >> 3];
		
		System.arraycopy(ack.getData(), 5, bytes, 0, bytes.length);
		
		boolean[] ans = new boolean[getNum(ack)];
		for (int i = 0; i < ans.length; i++) {
			ans[i] = ((bytes[i >> 3] & (1 << (i & 7))) != 0);
		}
		
		return ans;
	}
	
	/**
	 * Construct a new GetD message from a byte array (received data).
	 * 
	 * @param data The byte array to parse into a GetD message.
	 */
	protected UDR_GetD(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_GETD);
		setData(data);
	}
	
	/**
	 * Construct a GetD message from its constituent parts.
	 * 
	 * @param type The data type to retrieve (see {@link UDRMessage#T_D_DIN} and
	 *   similar).
	 * @param start The starting register number.
	 * @param num The number of registers to retrieve.
	 */
	public UDR_GetD(byte type, short start, short num) {
		super.setCommand(UDRMessage.C_GETD);
		
		byte[] data = new byte[5];
		data[0] = type;
		
		if ((type & T_BIGENDIAN) == T_BIGENDIAN) {
			Conversion.shortToBytes(data, 1, start);
			Conversion.shortToBytes(data, 3, num);
		} else {
			Conversion.shortToBytesLE(data, 1, start);
			Conversion.shortToBytesLE(data, 3, num);
		}
		
		setData(data);
	}
	
	/**
	 * Construct an acknowledgment message to this GetD message.
	 * 
	 * @param vals The values to return. The low-order register should be in bit
	 *   0 of byte 0.
	 * @return An acknowledgment message matching this message.
	 */
	public UDR_Acknowledge acknowledge(byte[] vals) {
		byte[] data = new byte[5 + vals.length];
	
		System.arraycopy(getData(), 0, data, 0, 5);
		System.arraycopy(vals, 0, data, 5, vals.length);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Get the data type being retrieved by this message.
	 */
	public byte getType() {
		return getData()[0];
	}
	
	/**
	 * Get the starting register number being retrieved by this message.
	 */
	public short getStart() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 1);
		} else {
			return Conversion.bytesToShortLE(getData(), 1);
		}
	}
	
	/**
	 * Get the number of registers being retrieved by this message.
	 */
	public short getNum() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 3);
		} else {
			return Conversion.bytesToShortLE(getData(), 3);
		}
	}
	
	/**
	 * Set the command byte for this message (will throw an exception).
	 */
	@Override
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Get the descriptive name of this message.
	 */
	@Override
	public String getCommandString() {
		return "GetD";
	}
	
	/**
	 * Describe this message.
	 * 
	 * @param indent A string to prepend to all lines of the returned string.
	 * @return A description of this message.
	 */
	@Override
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType())));
			} else if (i == 1) {
				ans.append(String.format(" Start = %s", getStart()));
			} else if (i == 3) {
				ans.append(String.format(" Number = %s", getNum()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe an acknowledgment to this message.
	 * 
	 * @param ack The acknowledgment to describe.
	 * @param indent A string to prepend to all lines of the returned string.
	 * @return A description of the ACK.
	 */
	@Override
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType(ack))));
			} else if (i == 1) {
				ans.append(String.format(" Start = %d", getStart(ack)));
			} else if (i == 3) {
				ans.append(String.format(" Number = %d", getNum(ack)));
			} else if (i >= 5) {
				ans.append(" Discrete data");
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

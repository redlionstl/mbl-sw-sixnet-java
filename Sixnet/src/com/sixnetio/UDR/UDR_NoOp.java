/*
 * UDR_NOP.java
 *
 * A NOP UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

/**
 * A message to do nothing. Useful for verifying connectivity with a device.
 *
 * @author Jonathan Pearson
 */
public class UDR_NoOp extends UDRMessage {
	/**
	 * Verify the given acknowledgment matches this message.
	 * @return True = the acknowledgment matches; False = the acknowledgment is bad.
	 */
	public boolean verifyAck(UDR_Acknowledge ack) {
		return (ack.getData().length >= 0);
	}
	
	/**
	 * Used by UDRMessage.receive() to construct this message given a data payload.
	 * @param data The data payload.
	 */
	protected UDR_NoOp(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_NOP);
		setData(data);
	}
	
	/**
	 * Construct a new message of this type.
	 */
	public UDR_NoOp() {
		super.setCommand(UDRMessage.C_NOP);
		setData(new byte[0]);
	}
	
	/**
	 * Automatically construct an acknowledgment to this message.
	 * @return An acknowledgment to this message.
	 */
	public UDR_Acknowledge acknowledge() {
		UDR_Acknowledge ack = new UDR_Acknowledge(new byte[0]);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Unless val == getCommand(), throws an IllegalArgumentException.
	 */
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Describe the command byte of this message.
	 */
	public String getCommandString() {
		return "No-Op";
	}
}

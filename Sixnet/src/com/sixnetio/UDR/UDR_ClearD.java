/*
 * UDR_CLRD.java
 *
 * A CLRD UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.Conversion;

public class UDR_ClearD extends UDRMessage {
    // Acknowledgment translation functions
    public boolean verifyAck(UDR_Acknowledge ack) {
	    return (ack.getData().length >= 5 &&
	            getStart(ack) == getStart() &&
	            getNum(ack) == getNum());
    }
    
	public static byte getType(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	public static short getStart(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 1);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 1);
		}
	}
	
	public static short getNum(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 3);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 3);
		}
	}
	
	protected UDR_ClearD(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_CLRD);
		setData(data);
	}
	
	public UDR_ClearD(byte type, short start, short num, byte[] vals) {
		super.setCommand(UDRMessage.C_CLRD);
		
		byte[] data = new byte[5 + vals.length];
		data[0] = type;
		
		if ((type & T_BIGENDIAN) == T_BIGENDIAN) {
			Conversion.shortToBytes(data, 1, start);
			Conversion.shortToBytes(data, 3, num);
		} else {
			Conversion.shortToBytesLE(data, 1, start);
			Conversion.shortToBytesLE(data, 3, num);
		}
		
		System.arraycopy(vals, 0, data, 5, vals.length);
		
		setData(data);
	}
	
	public UDR_ClearD(byte type, short start, short num, boolean[] vals) {
		super.setCommand(UDRMessage.C_CLRD);
		
		byte[] data = new byte[5 + vals.length / 8 + (vals.length % 8 == 0 ? 0 : 1)];
		data[0] = type;
		
		if ((type & T_BIGENDIAN) == T_BIGENDIAN) {
			Conversion.shortToBytes(data, 1, start);
			Conversion.shortToBytes(data, 3, num);
		} else {
			Conversion.shortToBytesLE(data, 1, start);
			Conversion.shortToBytesLE(data, 3, num);
		}
		
		for (int i = 0; i < vals.length; i++) {
			if (vals[i]) {
				data[5 + i / 8] |= 1 << (i % 8);
			}
		}
		
		setData(data);
	}
	
	public UDR_Acknowledge acknowledge() {
		byte[] data = new byte[5];
	
		System.arraycopy(getData(), 0, data, 0, 5);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	public byte getType() {
		return getData()[0];
	}
	
	public short getStart() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 1);
		} else {
			return Conversion.bytesToShortLE(getData(), 1);
		}
	}
	
	public short getNum() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 3);
		} else {
			return Conversion.bytesToShortLE(getData(), 3);
		}
	}
	
	public boolean getVal(int index) {
		if (index > getNum()) throw new ArrayIndexOutOfBoundsException(String.format("Index %d out of range [0, %d)", index, getNum()));
		
		return ((getData()[5 + index / 8] & (1 << (index % 8))) != 0);
	}
	
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	public String getCommandString() {
		return "ClrD";
	}
	
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType())));
			} else if (i == 1) {
				ans.append(String.format(" Start = %s", getStart()));
			} else if (i == 3) {
				ans.append(String.format(" Number = %s", getNum()));
			} else if (i >= 5) {
				ans.append(" Discrete data");
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType(ack))));
			} else if (i == 1) {
				ans.append(String.format(" Start = %d", getStart(ack)));
			} else if (i == 3) {
				ans.append(String.format(" Number = %d", getNum(ack)));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

/*
 * UDR_GETB.java
 *
 * A GETB UDR message
 *
 * Jonathan Pearson
 * March 21, 2007
 *
 */

package com.sixnetio.UDR;

import com.sixnetio.util.Conversion;

/**
 * A message used to retrieve blocks of 8 discretes from a station.
 *
 * @author Jonathan Pearson
 */
public class UDR_GetB extends UDRMessage {
	// Acknowledgment translation functions
	/**
	 * Verify that the specified ACK matches this message.
	 * 
	 * @param ack The ACK to verify.
	 * @return True = the ACK matches this message, False = does not match.
	 */
	@Override
	public boolean verifyAck(UDR_Acknowledge ack) {
		if (ack.getData().length < 5) return false; // Need to have at least 5 bytes
		
		return (ack.getData().length >= (5 + getNum() * 1) &&
		        getStart(ack) == getStart() &&
		        getNum(ack) == getNum());
	}
	
	/**
	 * Get the type of data retrieved by the ACK (see {@link UDRMessage#T_D_DIN}
	 * and similar).
	 * 
	 * @param ack The ACK.
	 * @return The type of data retrieved by the ACK.
	 */
	public static byte getType(UDR_Acknowledge ack) {
		return ack.getData()[0];
	}
	
	/**
	 * Get the starting block number (a block is 8 discretes).
	 * 
	 * @param ack The ACK.
	 * @return The starting block number.
	 */
	public static short getStart(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 1);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 1);
		}
	}
	
	/**
	 * Get the number of blocks retrieved (a block is 8 discretes).
	 * 
	 * @param ack The ACK.
	 * @return The number of blocks retrieved.
	 */
	public static short getNum(UDR_Acknowledge ack) {
		if ((ack.getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(ack.getData(), 3);
		} else {
			return Conversion.bytesToShortLE(ack.getData(), 3);
		}
	}
	
	/**
	 * Get an individual block of discretes.
	 * 
	 * @param ack The ACK.
	 * @param index The index of the block.
	 * @return The block.
	 */
	public static byte getVal(UDR_Acknowledge ack, int index) {
		if (index > getNum(ack)) throw new ArrayIndexOutOfBoundsException(String.format("Index %d out of range [0, %d)", index, getNum(ack)));
		
		return ack.getData()[5 + index];
	}
	
	/**
	 * Get an array containing all of the retrieved blocks.
	 * 
	 * @param ack The ACK.
	 * @return All retrieved discrete blocks.
	 */
	public static byte[] getVals(UDR_Acknowledge ack) {
		byte[] ans = new byte[getNum(ack)];
		
		System.arraycopy(ack.getData(), 5, ans, 0, ans.length);
		
		return ans;
	}
	
	/**
	 * Construct a new GetB message from a byte array (received data).
	 * 
	 * @param data The byte array to parse into a GetB message.
	 */
	protected UDR_GetB(byte[] data) {
		//Used when receiving a new message
		super.setCommand(UDRMessage.C_GETB);
		setData(data);
	}
	
	/**
	 * Construct a new GetB message from its constituent parts.
	 * 
	 * @param type The data type to retrieve.
	 * @param start The starting block number.
	 * @param num The number of blocks to retrieve.
	 */
	public UDR_GetB(byte type, short start, short num) {
		super.setCommand(UDRMessage.C_GETB);
		
		byte[] data = new byte[5];
		data[0] = type;
		
		if ((type & T_BIGENDIAN) == T_BIGENDIAN) {
			Conversion.shortToBytes(data, 1, start);
			Conversion.shortToBytes(data, 3, num);
		} else {
			Conversion.shortToBytesLE(data, 1, start);
			Conversion.shortToBytesLE(data, 3, num);
		}
		
		setData(data);
	}
	
	/**
	 * Acknowledge this GetB message.
	 * 
	 * @param vals The values to return in the ACK.
	 * @return An ACK matching this message.
	 */
	public UDR_Acknowledge acknowledge(byte[] vals) {
		byte[] data = new byte[5 + vals.length];
	
		System.arraycopy(getData(), 0, data, 0, 5);
		System.arraycopy(vals, 0, data, 5, vals.length);
		
		UDR_Acknowledge ack = new UDR_Acknowledge(data);
		ack.setup(getFormat(), getDestination(), getSource(), getSession(), getSequence());
		
		return ack;
	}
	
	/**
	 * Get the data type being requested by this message.
	 */
	public byte getType() {
		return getData()[0];
	}
	
	/**
	 * Get the starting block number requested by this message.
	 */
	public short getStart() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 1);
		} else {
			return Conversion.bytesToShortLE(getData(), 1);
		}
	}
	
	/**
	 * Get the number of blocks being retrieved by this message.
	 */
	public short getNum() {
		if ((getData()[0] & T_BIGENDIAN) == T_BIGENDIAN) {
			return Conversion.bytesToShort(getData(), 3);
		} else {
			return Conversion.bytesToShortLE(getData(), 3);
		}
	}
	
	/**
	 * Set the command byte (will throw an exception).
	 */
	@Override
	public void setCommand(byte val) {
		if (val != getCommand()) {
			throw new IllegalArgumentException("Cannot change the type of a specific message");
		}
	}
	
	/**
	 * Get the descriptive name of this message.
	 */
	@Override
	public String getCommandString() {
		return "GetB";
	}
	
	/**
	 * Describe this message.
	 * 
	 * @param indent A string to prepend to all lines of the returned string.
	 * @return A description of this message.
	 */
	@Override
	public String describeData(String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType())));
			} else if (i == 1) {
				ans.append(String.format(" Start = %s", getStart()));
			} else if (i == 3) {
				ans.append(String.format(" Number = %s", getNum()));
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
	
	/**
	 * Describe an ACK to this message.
	 * 
	 * @param ack The ACK to describe.
	 * @param indent A string to prepend to all lines of the returned string.
	 * @return A description of the ACK.
	 */
	@Override
	public String describeData(UDR_Acknowledge ack, String indent) {
		StringBuilder ans = new StringBuilder();
		byte[] data = ack.getData();
		
		for (int i = 0; i < data.length; i++) {
			ans.append(String.format("%sData(%3d):   %s", indent, i, describeByte(data[i])));
			
			if (i == 0) {
				ans.append(String.format(" Type = %s", describeType(getType(ack))));
			} else if (i == 1) {
				ans.append(String.format(" Start = %d", getStart(ack)));
			} else if (i == 3) {
				ans.append(String.format(" Number = %d", getNum(ack)));
			} else if (i >= 5) {
				ans.append(" Discrete data");
			}
			
			if (i < data.length - 1) ans.append("\n");
		}
		
		return ans.toString();
	}
}

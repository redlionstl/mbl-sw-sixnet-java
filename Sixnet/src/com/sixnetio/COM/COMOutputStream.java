/*
 * COMOutputStream.java
 *
 * Allows for communication with a COM port via the normal OutputStream
 * semantics.
 *
 * Jonathan Pearson
 * March 19, 2007
 *
 */

package com.sixnetio.COM;

import java.io.IOException;
import java.io.OutputStream;

public class COMOutputStream extends OutputStream {
    private String path;
    private boolean closed = false;
    private ComPort conxn = new ComPort();

    /**
     * Construct a new COMOutputStream, specifying the communications parameters
     * explicitly.
     * 
     * @param path
     *            The path to the serial port.
     * @param baud
     *            The baud.
     * @param bitsPerByte
     *            The byte size.
     * @param parity
     *            The parity.
     * @param stopBits
     *            The number of stop bits.
     * @throws IOException
     *             If there was a problem opening the serial port.
     */
    public COMOutputStream(String path, int baud, int bitsPerByte, int parity, int stopBits) throws IOException {
        this.path = path;

        conxn.openComPort(path, baud, bitsPerByte, parity, stopBits);
    }

    /**
     * Construct a new COMOutputStream, specifying the communications parameters
     * explicitly.
     * 
     * @param path
     *            The path to the serial port.
     * @param params
     *            The communications parameters (see
     *            {@link ComPort#openComPort(String, String)} for the format of this
     *            parameter).
     * @throws IOException
     *             If there was a problem opening the serial port.
     */
    public COMOutputStream(String path, String params) throws IOException {
        this.path = path;

        conxn.openComPort(path, params);
    }

    /**
     * Construct a new COMOutputStream, connecting to an already-open serial
     * device..
     * 
     * @param path
     *            The path of the serial device.
     * @throws IOException
     *             If the device was not already open.
     */
    public COMOutputStream(String path) throws IOException {
        this.path = path;

        ComPort.openComPort(path);
    }

    @Override
    public void write(int b) throws IOException {
        conxn.writeComPort(path, b);
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        conxn.writeComPort(path, bytes);
    }

    @Override
    public void write(byte[] bytes, int offset, int len) throws IOException {
        byte[] data = new byte[len];
        System.arraycopy(bytes, offset, data, 0, len);
        write(data);
    }

    @Override
    public void close() throws IOException {
        closed = true;
        conxn.closeComPort(path);
    }

    @Override
    public void flush() throws IOException {
        // ComPort.flushComPort(path);
    }

    @Override
    protected void finalize() throws Throwable {
        if (!closed) {
            close();
        }
    }
}

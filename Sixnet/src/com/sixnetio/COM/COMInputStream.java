/*
 * COMInputStream.java
 *
 * Allows for communication with a COM port via the normal InputStream
 * semantics.
 *
 * Jonathan Pearson
 * March 19, 2007
 *
 */

package com.sixnetio.COM;

import java.io.IOException;
import java.io.InputStream;

import com.sixnetio.util.Utils;

public class COMInputStream extends InputStream {
    private String path;
    private boolean closed = false;

    private ComPort conxn = new ComPort();

    /**
     * Construct a new COMInputStream, explicitly specifying the communication
     * parameters.
     * 
     * @param path
     *            The path to the serial device.
     * @param baud
     *            The baud rate.
     * @param bitsPerByte
     *            The byte size.
     * @param parity
     *            The parity.
     * @param stopBits
     *            The number of stop bits.
     * @throws IOException
     *             If there was a problem opening the serial device.
     */
    public COMInputStream(String path, int baud, int bitsPerByte, int parity, int stopBits) throws IOException {
        this.path = path;

        conxn.openComPort(path, baud, bitsPerByte, parity, stopBits);
    }

    /**
     * Construct a new COMInputStream, explicitly specifying the communication
     * parameters.
     * 
     * @param path
     *            The path to the serial device.
     * @param params
     *            The communication parameters (see
     *            {@link ComPort#openComPort(String, String)} for format details).
     * @throws IOException
     *             If there was a problem opening the serial device.
     */
    public COMInputStream(String path, String params) throws IOException {
        this.path = path;

        conxn.openComPort(path, params);
    }

    /**
     * Construct a new COMInputStream, attaching to an already open serial
     * device.
     * 
     * @param path
     *            The path to the serial device.
     * @throws IOException
     *             If the serial device was not already open.
     */
    public COMInputStream(String path) throws IOException {
        this.path = path;

        ComPort.openComPort(path);
    }

    @Override
    public int read() throws IOException {
        int val;

        while ((val = conxn.readComPort(path)) == -1) {
            Utils.sleep(50);
            Thread.yield();
        }

        return val;
    }

    @Override
    public int read(byte[] bytes) throws IOException {
        return conxn.readComPort(path, bytes);
    }

    @Override
    public int read(byte[] bytes, int offset, int len) throws IOException {
        byte[] data = new byte[len];

        int bytesRead = conxn.readComPort(path, data);
        System.arraycopy(data, 0, bytes, offset, bytesRead);

        return bytesRead;
    }

    /**
     * Read a line from the serial device.
     * 
     * @return The line read, minus the trailing newline character. Completely
     *         ignores carriage returns, including them in the returned value.
     * @throws IOException
     *             If there was a problem reading from the serial
     *             device.
     */
    public String readLine() throws IOException {
        StringBuilder line = new StringBuilder();
        int ch;
        boolean eol = false;

        while (!eol && (ch = read()) != -1) {
            if (ch == '\n') {
                eol = true;
            } else {
                line.append((char) ch);
            }
        }

        return line.toString();
    }

    @Override
    public void close() throws IOException {
        closed = true;
        conxn.closeComPort(path);
    }

    @Override
    protected void finalize() throws Throwable {
        if (!closed)
            close();
    }
}

/*
 * UBootCommunicator.java
 *
 * Provides a simple interface for communicating with UBoot.
 *
 * Jonathan Pearson
 * September 11, 2008
 *
 */

package com.sixnetio.COM;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import com.sixnetio.GUI.ProgressListener;
import com.sixnetio.server.tftp.TFTP;
import com.sixnetio.util.Conversion;
import com.sixnetio.util.InterruptedOperationException;
import com.sixnetio.util.Utils;

public class UBootCommunicator {
    static final Logger logger = Logger.getLogger(Utils.thisClassName());

    private ComPort conxn = new ComPort();

    // Milliseconds between 'delayabit'
    private static final long UBOOT_DELAY = 100;

    private class UBootCatcher extends Thread {
        public boolean caught = false;
        public boolean running = false;

        // If we fail to catch, there may be a reason
        public IOException reason = null;

        public UBootCatcher() {
            super("UBootCatcher");

            setDaemon(true);
            start();

            while (!running) {
                Thread.yield();
            }
        }

        @Override
        public void run() {
            // Shadow the real 'caught' until we're exiting this thread
            boolean caught = false;

            // The serial port should be closed at this point, no reason to
            // flush input

            running = true;

            File comFile = new File(comPath);

            if (!Utils.osIsWindows()) {
                logger.debug(String.format("Serial file '%s' %s", comFile.getAbsolutePath(),
                        comFile.exists() ? "exists" : "does not exist"));
            }

            while (running) {
                // Wait until the serial device shows up
                // In Windows, you cannot test for existence of COM ports except
                // by enumerating them, and that's slow
                if (!Utils.osIsWindows() && !comFile.exists()) {
                    Utils.sleep(25);
                    continue;
                }

                try {
                    if (!open) {
                        try {
                            conxn.openComPort(comPath, baud, bitsPerByte, parity, stopBits);
                        } catch (IOException ioe) {
                            if (Utils.osIsWindows()) {
                                // On Windows, the COM port will fail to open if
                                // the USB device has not powered up yet; we
                                // cannot detect the existence of the device
                                // (well... we could enumerate and check if the
                                // device exists, but that's slow), so just keep
                                // trying until we are told to stop
                                continue;
                            } else {
                                // Unable to open the serial device, but it
                                // exists... Something else has likely locked
                                // it, we will not succeed until it is unlocked,
                                // and without failing here the user won't know
                                // why we're hanging
                                logger.error("UBootCatcher giving up, serial" + " device is likely locked by"
                                        + " another process", ioe);
                                running = false;
                                reason = ioe;

                                continue;
                            }
                        }

                        logger.debug("  UBootCatcher opened the COM port");
                    }
                    open = true;
                    // Every UBootDelay ms, send "delayabit"
                    // The \n will cause a prompt to print even if we sent
                    // 'delayabit' to a prompt
                    write("delayabit\n");
                    // Read data for UBootDelay ms to see if it printed a prompt
                    // at the end
                    long startTime = System.currentTimeMillis();
                    StringBuilder buffer = new StringBuilder();
                    while (startTime + UBOOT_DELAY > System.currentTimeMillis()) {
                        int b;
                        while ((b = read()) != -1) {
                            buffer.append((char) b);
                        }
                    }

                    if (buffer.length() > 0) {
                        String s = buffer.toString();
                        logger.debug(String.format("Read '%s' after" + " 'delayabit'", s));

                        if (s.contains("=>")) {
                            logger.debug("UBootCatcher caught UBoot");

                            caught = true;
                            running = false;

                            // Need to sleep for about 1/2 second, otherwise
                            // UBoot may drop the
                            // first command given to it
                            Utils.sleep(500);

                            flushInput(250);
                        }
                    }
                } catch (IOException ioe) {
                    // Ignore it, sleep for a moment
                    Utils.sleep(25);
                }
            }

            this.caught = caught;

            logger.debug("UBootCatcher thread exiting");
        }
    }

    private class UBootKeepAlive extends Thread {
        public boolean running = true;

        public UBootKeepAlive() {
            super("UBootKeepAlive");

            setDaemon(true);
            start();
        }

        @Override
        public void run() {
            while (running) {
                long timeLeftMS = lastCommandTime + 5000 - System.currentTimeMillis();

                if (timeLeftMS <= 0) {
                    try {
                        execCommand("echo");
                    } catch (IOException ioe) {
                        logger.warn("Error sending 'echo' keepalive", ioe);
                    }
                } else {
                    Utils.sleep(timeLeftMS);
                }
            }

            logger.debug("UBootKeepAlive thread exiting");
        }
    }

    String comPath;
    int baud;
    int bitsPerByte;
    int parity;
    int stopBits;

    private UBootCatcher catcher = null;
    private UBootKeepAlive keepAlive = null;
    long lastCommandTime;

    private long ramAddr = 0x20100000; // For ARM9
    boolean open = false; // Only allow closing the port when it is open

    // To generate template U-boot scripts that do the same
    // thing as SwitchUtility, set this public member variable
    // to true.
    public boolean scriptPrint = false;

    /**
     * Construct a new UBootCommunicator to communicate over the given serial
     * port with the given settings.
     *
     * @param comPath
     *            The path to the serial port.
     * @param baud
     *            The baud rate.
     * @param bitsPerByte
     *            The byte size.
     * @param parity
     *            The parity setting.
     * @param stopBits
     *            The number of stop bits.
     */
    public UBootCommunicator(String comPath, int baud, int bitsPerByte, int parity, int stopBits) {
        this.comPath = comPath;
        this.baud = baud;
        this.bitsPerByte = bitsPerByte;
        this.parity = parity;
        this.stopBits = stopBits;
    }

    /**
     * Finalize (by calling {@link #close()}).
     */
    @Override
    protected void finalize() {
        close();
    }

    /**
     * Close the communicator. Stops any catcher/keepalive threads and closes
     * the serial port.
     */
    public void close() {
        stopCatch();
        stopKeepAlive();

        if (open) {
            try {
                conxn.closeComPort(comPath);
            } catch (IOException ioe) {
                logger.debug("IOException while closing the serial port", ioe);
            } finally {
                // Better to assume it's closed now and not accidentally close
                // it later when the garbage collector calls finalize(), than to
                // close it again when something else may have it open
                open = false;
            }
        }
    }

    /**
     * Check if this communicator is closed (no open handle to the serial port).
     */
    public boolean isClosed() {
        return (!open);
    }

    /**
     * Start a keepalive thread. This thread will send an 'echo' message every 5
     * seconds if no other commands have been executed.
     */
    public void startKeepAlive() {
        stopKeepAlive();
        keepAlive = new UBootKeepAlive();
    }

    /**
     * If there is a running keepalive thread, this will stop it and return once
     * it is gone.
     */
    public void stopKeepAlive() {
        if (keepAlive != null) {
            keepAlive.running = false;

            while (true) {
                try {
                    keepAlive.join();
                    break;
                } catch (InterruptedException ie) {
                    throw new InterruptedOperationException(ie);
                }
            }

            keepAlive = null;
        }
    }

    /**
     * Start a catch. Call this, then turn on the device, then call
     * {@link #waitForCatch(long)}.
     */
    public void startCatch() {
        stopCatch();
        catcher = new UBootCatcher();
    }

    /**
     * If there is a catcher thread running, stop it and return when is is gone.
     */
    public void stopCatch() {
        if (catcher != null) {
            catcher.running = false;

            while (true) {
                try {
                    catcher.join();
                    break;
                } catch (InterruptedException ie) {
                    throw new InterruptedOperationException(ie);
                }
            }

            catcher = null;
        }
    }

    /**
     * Check whether we have successfully caught UBoot with the catcher thread.
     */
    public boolean isCaught() {
        return (catcher != null && catcher.caught);
    }

    /**
     * Wait for the catcher thread to catch UBoot.
     * 
     * @param timeout
     *            How long to wait, in milliseconds. 0 waits forever.
     * @throws TimeoutException
     *             If UBoot was not caught within the timeout
     *             period.
     * @throws IOException
     *             If the catch failed for a different reason (locked
     *             COM port?).
     */
    public void waitForCatch(long timeout) throws TimeoutException, IOException {
        if (catcher != null) {
            try {
                catcher.join(timeout);
            } catch (InterruptedException ie) {
                throw new InterruptedOperationException(ie);
            }

            // Make sure it shuts down, even if we timed out
            catcher.running = false;

            if (!catcher.caught) {
                if (catcher.reason == null) {
                    throw new TimeoutException("The UBoot catcher timed out" + " before catching UBoot. Make"
                            + " sure only one form of serial" + " communication is connected.");
                } else {
                    throw catcher.reason;
                }
            }
        } else {
            throw new IllegalStateException("Cannot wait for UBoot to catch" + " when not trying to catch UBoot");
        }
    }

    /**
     * Read a single byte from the serial port (nonblocking).
     * 
     * @return A single byte, or -1 if no data was available.
     * @throws IOException
     *             If there was a problem reading from the port.
     */
    public int read() throws IOException {
        if (isClosed()) {
            throw new IOException("Cannot read from a closed communicator");
        }

        return conxn.readComPort(comPath);
    }

    /**
     * Read a bunch of bytes from the serial port (nonblocking).
     * 
     * @param buffer
     *            The buffer to fill.
     * @return The number of bytes actually read (may be 0).
     * @throws IOException
     *             If there was a problem reading from the port.
     */
    public int read(byte[] buffer) throws IOException {
        if (isClosed()) {
            throw new IOException("Cannot read from a closed communicator");
        }

        return conxn.readComPort(comPath, buffer);
    }

    /**
     * Read a bunch of bytes from the serial port (nonblocking).
     * 
     * @param buffer
     *            The buffer to fill.
     * @param off
     *            The offset into the buffer.
     * @param len
     *            The maximum number of bytes to read.
     * @return The actual number of bytes read (may be 0).
     * @throws IOException
     *             If there was a problem reading from the port.
     */
    public int read(byte[] buffer, int off, int len) throws IOException {
        if (isClosed()) {
            throw new IOException("Cannot read from a closed communicator");
        }

        byte[] realBuffer = new byte[len];
        int val = read(realBuffer);
        System.arraycopy(realBuffer, 0, buffer, off, len);
        return val;
    }

    /**
     * Read until a newline (\n) is encountered (blocking). Will drop carriage
     * returns (\r).
     * 
     * @return The line read.
     * @throws IOException
     *             If there was a problem reading from the port.
     */
    public String readLine() throws IOException {
        if (isClosed()) {
            throw new IOException("Cannot read from a closed communicator");
        }

        StringBuilder builder = new StringBuilder();
        int readByte;

        while ((readByte = read()) != '\n') {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            if (readByte == -1) {
                continue;
            }
            if (readByte == '\r') {
                continue;
            }

            builder.append((char) readByte);
        }

        return builder.toString();
    }

    /**
     * Drop bytes from the port until there has been no data for the specified
     * amount of time.
     * 
     * @param ms
     *            The number of milliseconds of silence required to stop flushing
     *            the port.
     * @throws IOException
     *             If there was a problem reading from the port.
     */
    public void flushInput(long ms) throws IOException {
        // Read until there is 'ms' ms of silence, discarding everything
        logger.debug("Flushing all input data");

        long startTime = System.currentTimeMillis();
        byte[] block = new byte[1024];
        do {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            while (read(block) > 0) {
                if (Thread.interrupted()) {
                    throw new InterruptedOperationException();
                }

                // Read something, reset the timer
                startTime = System.currentTimeMillis();
            }
        } while (startTime + ms > System.currentTimeMillis());
    }

    /**
     * Write a single byte to the serial port.
     * 
     * @param b
     *            The byte to write. Only the least significant byte of this value
     *            will be written.
     * @throws IOException
     *             If there is a problem writing to the port.
     */
    public void write(int b) throws IOException {
        if (isClosed()) {
            throw new IOException("Cannot write to a closed communicator");
        }

        conxn.writeComPort(comPath, b);
    }

    /**
     * Write a bunch of bytes to the port.
     * 
     * @param b
     *            The buffer to write.
     * @throws IOException
     *             If there was a problem writing to the port.
     */
    public synchronized void write(byte[] b) throws IOException {
        if (isClosed()) {
            throw new IOException("Cannot write to a closed communicator");
        }

        conxn.writeComPort(comPath, b);
    }

    /**
     * Write a bunch of bytes to the port.
     * 
     * @param b
     *            The buffer to write.
     * @param off
     *            The offset in the buffer to start at.
     * @param len
     *            The number of bytes to write.
     * @throws IOException
     *             If there was a problem writing to the port.
     */
    public synchronized void write(byte[] b, int off, int len) throws IOException {
        if (isClosed()) {
            throw new IOException("Cannot write to a closed communicator");
        }

        byte[] buf = new byte[len];
        System.arraycopy(b, off, buf, 0, len);
        conxn.writeComPort(comPath, buf);
    }

    /**
     * Write a string to the serial port.
     * 
     * @param s
     *            The string to write (should be standard ASCII, unless you are
     *            sure that the device on the other end of the serial connection
     *            understands UTF-8).
     * @throws IOException
     *             If there is a problem writing to the port.
     */
    public synchronized void write(String s) throws IOException {
        if (isClosed()) {
            throw new IOException("Cannot write to a closed communicator");
        }

        write(s.getBytes());
    }

    /**
     * Write a string followed by a newline (\n).
     * 
     * @param s
     *            The string to write.
     * @throws IOException
     *             If there is a problem writing to the port.
     */
    public synchronized void writeln(String s) throws IOException {
        if (isClosed()) {
            throw new IOException("Cannot write to a closed communicator");
        }

        logger.debug(String.format("Executing UBoot command '%s'", s));

        if (this.scriptPrint) {
            System.out.println("+ " + s);
        }
        write(s.getBytes());
        write("\n".getBytes());
    }

    /**
     * Set a UBoot variable in the device. You may call this a number of times
     * to set up variables, but make sure to call {@link #flushVariables()} when
     * you are done, to write those variables into storage.
     *
     * @param name
     *            The name of the variable to set.
     * @param value
     *            The value for the variable.
     * @throws IOException
     *             If there is a problem writing to the port or there is
     *             an error executing the command.
     */
    public void setVariable(String name, String value) throws IOException {
        execCommand(String.format("setenv %s %s", name, value));
    }

    /**
     * Delete a UBoot variable in the device. You may call this a number of
     * times to set up variables, but make sure to call
     * {@link #flushVariables()} when you are done, to write those variables
     * into storage.
     *
     * @param name
     *            The name of the variable to delete.
     * @throws IOException
     *             If there is a problem writing to the port or there is
     *             an error executing the command.
     */
    public void delVariable(String name) throws IOException {
        execCommand(String.format("setenv %s", name));
    }

    /**
     * Save all variable settings to permanent storage.
     *
     * @throws IOException
     *             If there is a problem writing to the port.
     */
    public void flushVariables() throws IOException {
        /*
         * Why save 2x? Because it's 1 higher, of course.
         * Some platforms require it to save the "backup" copy of
         * the environment. Others don't care. Note that you
         * can't use "&&" here because some older u-boots
         * don't support it.
         */
        execCommand("saveenv ; saveenv");
    }

    /**
     * Get the value of a single UBoot variable.
     *
     * @param name
     *            The name of the variable.
     * @return The value of the variable.
     * @throws IOException
     *             If there is a problem writing to the port, reading
     *             from U-Boot, or extracting the variable value (including if the
     *             variable is not found).
     */
    public String getVariable(String name) throws IOException {
        String result = execCommand(String.format("printenv %s", name));
        if (!result.startsWith(name)) {

            // Try again, sometimes the text gets out of sync
            result = execCommand(String.format("printenv %s", name));
        }

        if (!result.contains(name)) {
            // Unable to retrieve the variable
            throw new IOException("Unable to retrieve variable '" + name + "'");
        }

        // Cut off any whitespace, the variable name, and the '=' after it
        return result.trim().substring(name.length() + 1);
    }

    /**
     * Read the port until the specified string is seen (blocking).
     *
     * @param needle
     *            The string to search for.
     * @return Data up to, but not including the search string.
     * @throws IOException
     *             If there is a problem reading from the port.
     */
    public String readUntil(String needle) throws IOException {
        StringBuilder searchBuffer = new StringBuilder();
        StringBuilder dataBuffer = new StringBuilder();

        while (searchBuffer.length() < needle.length()) {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            int readByte = read();
            if (readByte == -1) {
                Utils.sleep(50);
                continue; // This happens sometimes, it's non-blocking
            }

            searchBuffer.append((char) readByte);
        }

        while (!searchBuffer.toString().equals(needle)) {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            int readByte = read();
            if (readByte == -1) {
                Utils.sleep(50);
                continue;
            }

            dataBuffer.append(searchBuffer.charAt(0));
            searchBuffer.deleteCharAt(0); // Cut off a char from the beginning
            searchBuffer.append((char) readByte); // Add a char to the end
        }

        return dataBuffer.toString();
    }

    /**
     * Execute a UBoot command by writing the command and a newline, and then
     * waiting for a prompt to be printed.
     *
     * @param command
     *            The command to execute.
     * @return Everything UBoot printed, except for the prompt.
     * @throws IOException
     *             If there is a problem reading/writing the port, or
     *             the output contains "error:" (ignoring case).
     */
    public synchronized String execCommand(String command) throws IOException {
        writeln(command);
        String result = readUntil("=>");

        logger.debug(String.format("Response: '%s'", result));

        // UBoot echoes commands, so make sure it did that this time
        if (!result.contains(command)) {
            throw new IOException("Incorrect response to '" + command + "': '" + result + "'");
        }

        // Strip off the echoed command
        result = result.substring(command.length()).trim();

        lastCommandTime = System.currentTimeMillis();

        if (result.toLowerCase().indexOf("error:") != -1) {
            // Cut off up to 'Error' and add 'UBoot' so it reads 'UBoot Error'
            result = "UBoot " + result.substring(result.toLowerCase().indexOf("error"));

            throw new IOException(result);
        }

        return result;
    }

    /**
     * Erase all NAND.
     *
     * @throws IOException
     *             If {@link #execCommand(String)} throws one.
     */
    public void erase() throws IOException {
        execCommand(String.format("nand erase clean"));
    }

    /**
     * Erase a range of NAND.
     *
     * @param start
     *            The start of the range.
     * @param length
     *            The length of the range.
     * @throws IOException
     *             If {@link #execCommand(String)} throws one.
     */
    public void eraseRange(long start, long length) throws IOException {
        execCommand(String.format("nand erase clean %x %x", start, length));
    }

    /**
     * Get a file over TFTP via UBoot
     *
     * @param file
     *            The name of the file to load. This will be passed to UBoot so
     *            it can request it via TFTP.
     * @param handleTFTP
     *            If true, 'file' must be a full path to a file. The file
     *            will be added to the TFTP server, and its name will be passed to
     *            UBoot
     *            for the TFTP transfer. If false, 'file' should just be a file name
     *            that
     *            the TFTP server is already sharing.
     * @param fileSize
     *            If you pass -1, the file size will be requested from the
     *            internal TFTP server. If you pass a value greater than -1, this
     *            will be
     *            used as the file size and the TFTP server will not be queried. You
     *            may
     *            want to do this if you are using an external TFTP server, as
     *            requesting
     *            anything from the internal TFTP server will attempt to start it
     *            up.
     * @param progressListener
     *            If provided, will be notified of TFTP progress.
     * @throws IOException
     *             If there is an error communicating with UBoot.
     * @throws InterruptedOperationException
     *             If the thread is interrupted during
     *             the load process.
     */
    public synchronized void loadTFTP(String file, boolean handleTFTP, int fileSize, ProgressListener progressListener)
            throws IOException {
        String fileName;

        if (handleTFTP) {
            File f = new File(file);
            if (!f.isFile()) {
                throw new IOException("'" + file + "' does not exist or is not a file");
            }

            // Share the file over TFTP
            TFTP tftpServer = TFTP.getTFTP();
            tftpServer.addFile(f.getName(), f);

            fileName = f.getName();
        } else {
            fileName = file;
        }

        try { // Finally block cleans up the TFTP server
              // Number of TFTP blocks to transfer this file
            if (fileSize == -1) {
                fileSize = TFTP.getTFTP().getFileSize(fileName);
            }

            int blockCount = fileSize / 512 + (fileSize % 512 == 0 ? 0 : 1);

            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            // Tell the device to download the file
            // Would use 'execCommand' here, but we want to see the progress
            writeln(String.format("tftp %x %s", getRamAddress(), fileName));

            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            String result = readUntil("Loading:");

            logger.debug("Response: '" + result + "'");

            // Read data for progress output until it says 'done', then discard
            // until '=>'
            StringBuilder searchBuffer = new StringBuilder();
            String needle = "done";
            int blocknum = 0;

            while (searchBuffer.length() < needle.length()) {
                if (Thread.interrupted()) {
                    throw new InterruptedOperationException();
                }

                int readByte = read();
                if (readByte == -1) {
                    Utils.sleep(50);
                    continue; // This happens sometimes, it's non-blocking
                }

                if (readByte == '#') {
                    blocknum += 10; // Each '#' symbol is 10 blocks
                    if (blocknum > blockCount) {
                        blocknum = blockCount; // Don't pass 100%
                    }
                    if (progressListener != null) {
                        progressListener.updateProgress((float) blocknum / blockCount);
                    }
                }

                searchBuffer.append((char) readByte);
            }

            while (!searchBuffer.toString().equals(needle)) {
                if (Thread.interrupted()) {
                    throw new InterruptedOperationException();
                }

                int readByte = read();
                if (readByte == -1) {
                    continue;
                }

                if (readByte == '#') {
                    blocknum += 10; // Each '#' symbol is 10 blocks
                    if (blocknum > blockCount) {
                        blocknum = blockCount; // Don't pass 100%
                    }
                    if (progressListener != null) {
                        progressListener.updateProgress((float) blocknum / blockCount);
                    }
                }

                // Cut off a char from the beginning
                searchBuffer.deleteCharAt(0);
                searchBuffer.append((char) readByte); // Add a char to the end
            }

            // Since UBoot is busy TFTPing that file, this will block until it
            // is finished
            result = readUntil("=>");
            logger.debug("Result: " + result);

            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }
        } finally {
            // Remove the file from the TFTP server
            if (handleTFTP) {
                TFTP.getTFTP().removeFile(fileName);
            }
        }
    }

    /**
     * Load a file through UBoot to the device, using TFTP. This will leave the
     * file data at the temporary RAM location, in case any more operations must
     * be performed on it.
     *
     * @param file
     *            The name of the file to load. This will be passed to UBoot so
     *            it can request it via TFTP.
     * @param handleTFTP
     *            If true, 'file' must be a full path to a file. The file
     *            will be added to the TFTP server, and its name will be passed to
     *            UBoot
     *            for the TFTP transfer. If false, 'file' should just be a file name
     *            that
     *            the TFTP server is already sharing.
     * @param fileSize
     *            If you pass -1, the file size will be requested from the
     *            internal TFTP server. If you pass a value greater than -1, this
     *            will be
     *            used as the file size and the TFTP server will not be queried. You
     *            may
     *            want to do this if you are using an external TFTP server, as
     *            requesting
     *            anything from the internal TFTP server will attempt to start it
     *            up.
     * @param address
     *            The address in NAND that the file data should go
     *            eventually.
     * @param eraseLength
     *            The size of the block of NAND to erase before loading
     *            the data into it. If <= 0, no erasing is performed.
     * @param progressListener
     *            If provided, will be notified of TFTP progress.
     * @throws IOException
     *             If there is an error communicating with UBoot.
     * @throws InterruptedOperationException
     *             If the thread is interrupted during
     *             the load process.
     */
    public synchronized void loadFile(String file, boolean handleTFTP, int fileSize, long address, int eraseLength,
            ProgressListener progressListener) throws IOException {
        loadTFTP(file, handleTFTP, fileSize, progressListener);

        // Erase the target range (if eraseLength > 0)
        if (eraseLength > 0) {
            eraseRange(address, eraseLength);
        }

        // Copy the file from RAM to the target range
        execCommand(String.format("nand write.e %x %x $(filesize)", getRamAddress(), address));
    }

    /**
     * Load a file through UBoot to the device, using TFTP. This will leave the
     * file data at the temporary RAM location, in case any more operations must
     * be performed on it.
     *
     * @param file
     *            The name of the file to load. This will be passed to UBoot so
     *            it can request it via TFTP.
     * @param handleTFTP
     *            If true, 'file' must be a full path to a file. The file
     *            will be added to the TFTP server, and its name will be passed to
     *            UBoot
     *            for the TFTP transfer. If false, 'file' should just be a file name
     *            that
     *            the TFTP server is already sharing.
     * @param fileSize
     *            If you pass -1, the file size will be requested from the
     *            internal TFTP server. If you pass a value greater than -1, this
     *            will be
     *            used as the file size and the TFTP server will not be queried. You
     *            may
     *            want to do this if you are using an external TFTP server, as
     *            requesting
     *            anything from the internal TFTP server will attempt to start it
     *            up.
     * @param part
     *            The UBI part to which the file should be loaded (ie nand0,3).
     * @param volumeName
     *            The name of the volume to write (ie rootfs)
     * @param progressListener
     *            If provided, will be notified of TFTP progress.
     * @throws IOException
     *             If there is an error communicating with UBoot.
     * @throws InterruptedOperationException
     *             If the thread is interrupted during
     *             the load process.
     */
    public synchronized void loadFile(String file, boolean handleTFTP, int fileSize, String part, String volumeName,
            ProgressListener progressListener) throws IOException {
        loadTFTP(file, handleTFTP, fileSize, progressListener);

        if (execCommand(String.format("ubi part %s", part)).contains("UBI: number of user volumes:	 0")) {
            execCommand(String.format("ubi create %s", volumeName));
        }
        execCommand(String.format("ubi write %x %s $filesize", getRamAddress(), volumeName));
    }

    /**
     * Write a block of data to a specific area of NAND using <tt>cp.b</tt>.
     *
     * @param data
     *            The data to write.
     * @param address
     *            The address so start writing.
     * @throws IOException
     *             If {@link #execCommand(String)} throws one.
     */
    public void writeBlock(byte[] data, long address) throws IOException {
        int offset = 0;
        while (offset < data.length) {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            execCommand(String.format("mw.b %x %x 1", ramAddr + offset, data[offset]));

            offset++;
        }

        execCommand(String.format("cp.b %x %x %x", ramAddr, address, offset));
    }

    /**
     * Write a block of data to a specific area of NAND using
     * <tt>nand write.e</tt>.
     *
     * @param data
     *            The data to write.
     * @param address
     *            The address so start writing.
     * @throws IOException
     *             If {@link #execCommand(String)} throws one.
     */
    public void writeNand(byte[] data, long address) throws IOException {
        int offset = 0;
        while (offset < data.length) {
            if (Thread.interrupted()) {
                throw new InterruptedOperationException();
            }

            execCommand(String.format("mw.b %x %x 1", ramAddr + offset, data[offset]));

            offset++;
        }

        execCommand(String.format("nand write.e %x %x %x", ramAddr, address, offset));
    }

    /**
     * Read a block of data from a specific area of NAND.
     *
     * @param address
     *            The address to start reading from.
     * @param length
     *            The length of the range to read.
     * @return The data that was read.
     * @throws IOException
     *             If {@link #execCommand(String)} throws one.
     */
    public byte[] readBlock(long address, int length) throws IOException {
        String result = execCommand(String.format("md.b %x %x", address, length));

        // Result looks like "<address>: ## ## ## ## ## ## ## ## ."
        // Pull out the part we want
        // Cut off <address>: and any spaces
        result = result.substring(result.indexOf(':') + 1).trim();

        // Cut off everything past the part we're interested in
        // Each byte is 2 chars + 1 space, remove the last space
        result = result.substring(0, 3 * length - 1);

        // Get rid of the spaces
        result = result.replaceAll(" ", "");

        // Convert to bytes
        return Conversion.hexToBytes(result);
    }

    /**
     * Get the memory address where RAM can be used for temporary storage.
     */
    public long getRamAddress() {
        return ramAddr;
    }

    /**
     * Set the memory address where RAM can be used for temporary storage.
     *
     * @param ramAddr
     *            The address for temporary RAM.
     */
    public void setRamAddress(long ramAddr) {
        this.ramAddr = ramAddr;
    }

    /**
     * Get the path to the serial port being used.
     */
    public String getComPort() {
        return comPath;
    }

    /**
     * Get the size of the NAND, in megabytes.
     *
     * @return The size of the NAND, in megabytes.
     */
    public int getNandSize() throws IOException {
        String result = execCommand("nand info");
        // Output is of the form:
        //
        // Device 0: NAND 128MiB 3,3V 8-bit, sector size 128 KiB
        //
        // The following ignores all leading chars, matches on
        // whitespace leading any number of digits followed by
        // MiB, then more whitespace than anything else. The
        // digits are a match group, which we convert to an int
        // and return. The regex guarantees that the input to our
        // converter is good.

        String output = result.replaceAll(".*\\s+(\\d+)MiB\\s+.*", "$1");
        int size = 0;
        try {
            size = Integer.parseInt(output);
        } catch (NumberFormatException e) {
            // device either doesn't have nand, or it's not
            // reporting in an expected format.
            size = 0;
        }
        return size;
    }
}

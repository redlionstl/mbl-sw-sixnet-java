/*
 * ComPort.java
 *
 * A static class that keeps track of handles to open COM ports and does I/O
 * with them.
 *
 * Jonathan Pearson
 * March 19, 2007
 *
 */

package com.sixnetio.COM;

import java.io.IOException;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.fazecast.jSerialComm.SerialPort;
import com.sixnetio.util.Utils;

public class ComPort {
    private static final Logger logger = Logger.getLogger(Utils.thisClassName());

    public static final int PARITY_NONE = 0;
    public static final int PARITY_ODD = 1;
    public static final int PARITY_EVEN = 2;
    public static final int PARITY_MARK = 3;
    public static final int PARITY_SPACE = 4;

    public static final int STOPBITS_1 = 0;
    public static final int STOPBITS_1POINT5 = 1;
    public static final int STOPBITS_2 = 2;

    private SerialPort conxn = null;

    static {
        openPorts = new Hashtable<String, int[]>();
        debugLine = new Hashtable<String, StringBuilder>();
    }

    // [0] = OS handle/file descriptor for the device; cannot be 0
    // [1] = reference count (how many opens without closes); when 0, the OS is
    // told to close the device
    private static Hashtable<String, int[]> openPorts;

    /**
     * Used to debug serial communications. Map of port -&gt; current line. Port
     * is prefixed with "r" or "w" for the read and write ends.
     */
    private static Hashtable<String, StringBuilder> debugLine;

    /**
     * Open a COM port. If it is already open, this will increment the reference
     * counter, but the connection details will not change.
     * 
     * @param path
     *            The path to the COM device.
     * @param baud
     *            The speed for the connection.
     * @param bitsPerByte
     *            The byte size to use.
     * @param parity
     *            The parity to use (see PARITY_* above).
     * @param stopBits
     *            The number of stop bits (see STOPBITS_* above).
     * @throws IOException
     *             If there is a problem opening the COM port.
     */
    // public static synchronized void openComPort(String path, int baud,
    // int bitsPerByte, int parity,
    // int stopBits)
    public synchronized void openComPort(String path, int baud, int bitsPerByte, int parity, int stopBits)
            throws IOException {
        int[] comPort = openPorts.get(path);

        if (comPort == null) {
            logger.debug("Opening fresh COM port " + path);

            comPort = new int[2];
            comPort[0] = 0;
            comPort[1] = 0; // reference count

            openPorts.put(path, comPort);
        }

        if (comPort[0] == 0) {
            try {
                conxn = SerialPort.getCommPort(path);
            } catch (Exception excpn) {
                throw new IOException("Unable to open COM port \"" + path + "\"");
            }
        }

        conxn.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 50, 1000);
        conxn.setBaudRate(baud); // Set Baud Rate
        conxn.setNumDataBits(bitsPerByte); // Set Byte size

        switch (parity) { // Set Connection Parity
        case PARITY_NONE:
            conxn.setParity(SerialPort.NO_PARITY);
            break;
        case PARITY_ODD:
            conxn.setParity(SerialPort.ODD_PARITY);
            break;
        case PARITY_EVEN:
            conxn.setParity(SerialPort.EVEN_PARITY);
            break;
        case PARITY_MARK:
            conxn.setParity(SerialPort.MARK_PARITY);
            break;
        case PARITY_SPACE:
            conxn.setParity(SerialPort.SPACE_PARITY);
            break;
        default:
            throw new IOException("Unrecognized parity: " + parity);
        }

        switch (stopBits) { // Set Connection Stop Bits
        case STOPBITS_1:
            conxn.setNumStopBits(SerialPort.ONE_STOP_BIT);
            break;
        case STOPBITS_1POINT5:
            conxn.setNumStopBits(SerialPort.ONE_POINT_FIVE_STOP_BITS);
            break;
        case STOPBITS_2:
            conxn.setNumStopBits(SerialPort.TWO_STOP_BITS);
            break;
        default:
            throw new IOException("Unrecognized Stop Bits: " + stopBits);
        }

        if (!conxn.openPort()) {
            throw new IOException("COM port \"" + path + "\" cannot be opened with specified parameters");
        }

        comPort[0] = 1; // Indicate port is open
        comPort[1]++; // Increment reference counter
    }

    /**
     * Open a COM port. If it is already open, this will increment the reference
     * counter, but the connection details will not change.
     * 
     * @param path
     *            The path to the serial device.
     * @param params
     *            The communications parameters, in a format like
     *            "9600-8-N-1", which means 9600 baud, 8 bits per byte, no parity, 1
     *            stop bit. Valid parity values are:
     *            <ul>
     *            <li><b>N</b> None</li>
     *            <li><b>O</b> Odd</li>
     *            <li><b>E</b> Even</li>
     *            <li><b>M</b> Mark</li>
     *            <li><b>S</b> Space</li>
     *            </ul>
     *            Valid stop bits values are "1", "1.5", and "2".
     * @throws IOException
     *             If there is a problem parsing the communications
     *             parameters or opening the serial device.
     */
    public synchronized void openComPort(String path, String params) throws IOException {
        // Parse the parameters
        int baud, bytesize, parity, stopbits;
        StringTokenizer tok = new StringTokenizer(params, "-");

        try {
            baud = Integer.parseInt(tok.nextToken());
            bytesize = Integer.parseInt(tok.nextToken());

            char par = tok.nextToken().charAt(0);
            switch (par) {
            case 'N':
                parity = PARITY_NONE;
                break;
            case 'O':
                parity = PARITY_ODD;
                break;
            case 'E':
                parity = PARITY_EVEN;
                break;
            case 'M':
                parity = PARITY_MARK;
                break;
            case 'S':
                parity = PARITY_SPACE;
                break;
            default:
                throw new IOException("Unrecognized parity: " + par);
            }

            String sb = tok.nextToken();
            if (sb.equals("1")) {
                stopbits = STOPBITS_1;
            } else if (sb.equals("1.5")) {
                stopbits = STOPBITS_1POINT5;
            } else if (sb.equals("2")) {
                stopbits = STOPBITS_2;
            } else {
                throw new IOException("Unrecognized stop bits: " + sb);
            }
        } catch (NumberFormatException nfe) {
            logger.error("Comm params include a non-number: " + params, nfe);
            throw new IOException("Communications parameters contain an invalid value: " + params, nfe);
        } catch (NoSuchElementException nsee) {
            logger.error("Comm params missing a value: " + params, nsee);
            throw new IOException("Communications parameters missing a value: " + params, nsee);
        }

        openComPort(path, baud, bytesize, parity, stopbits);
    }

    /**
     * Allocate another reference to the given COM port.
     * 
     * @param path
     *            The path to the COM device.
     * @throws IOException
     *             If the port is not already open.
     */
    public static synchronized void openComPort(String path) throws IOException {
        int[] comPort = openPorts.get(path);

        if (comPort == null || comPort[0] == 0) {
            throw new IOException("Cannot attach to COM port \"" + path + "\" as it is not open.");
        }

        logger.debug("Re-opening COM port " + path);

        comPort[1]++; // increment reference count
    }

    /**
     * Deallocate a reference to the specified COM port. Will close
     * the device if the reference count reaches 0.
     * 
     * @param path
     *            The path to the COM device.
     * @throws IOException
     *             If the port was not open.
     */
    public synchronized void closeComPort(String path) throws IOException {
        int[] comPort = openPorts.get(path);

        if (comPort == null || comPort[0] == 0) {
            throw new IOException("Cannot close COM port \"" + path + "\" as it is not open");
        }

        logger.debug("Closing COM port " + path + "");

        comPort[1]--;

        if (comPort[1] <= 0) {
            if (!conxn.closePort()) {
                throw new IOException("Failed to close COM port \"" + path + "\"");
            }

            comPort[0] = comPort[1] = 0;
        }
    }

    /**
     * Read a single byte from the COM port.
     * 
     * @param path
     *            The path to the COM device.
     * @return The byte read, or -1 if no data was available.
     * @throws IOException
     *             If there was a problem reading the device.
     */
    public synchronized int readComPort(String path) throws IOException {
        int[] comPort = openPorts.get(path);

        byte[] buffer = new byte[1];

        int readCnt = 0;

        if (comPort == null || comPort[0] == 0) {
            throw new IOException("Unable to read from COM port \"" + path + "\" as it is not open");
        }

        readCnt = conxn.readBytes(buffer, 1);

        if (readCnt < 0) {
            throw new IOException("COM port \"" + path + "\" read failure");
        } else if (readCnt == 0) {
            buffer[0] = -1;
        }

        if (logger.isDebugEnabled()) {
            appendDebugData(path, buffer, 0, 1, true);
        }

        return buffer[0];
    }

    /**
     * Read a byte array from the COM port.
     * 
     * @param path
     *            The path to the COM device.
     * @param buffer
     *            The byte buffer to fill.
     * @return The number of bytes actually read.
     * @throws IOException
     *             If there was a problem reading the COM device.
     */
    public synchronized int readComPort(String path, byte[] buffer) throws IOException {
        int[] comPort = openPorts.get(path);

        int readCnt = 0;

        if (comPort == null || comPort[0] == 0) {
            throw new IOException("Unable to read from COM port \"" + path + "\" as it is not open");
        }

        readCnt = conxn.readBytes(buffer, buffer.length);

        if (readCnt < 0) {
            throw new IOException("COM port \"" + path + "\" read failure");
        }

        if (logger.isDebugEnabled()) {
            appendDebugData(path, buffer, 0, readCnt, true);
        }

        return readCnt;
    }

    /**
     * Write a single byte to the COM port.
     * 
     * @param path
     *            The path to the COM device.
     * @param b
     *            The byte to write (only the least significant byte will be
     *            written).
     * @throws IOException
     *             If there was a problem writing to the COM device.
     */
    public synchronized void writeComPort(String path, int b) throws IOException {
        int[] comPort = openPorts.get(path);

        byte[] buffer = new byte[1];

        buffer[0] = (byte) (b & 0xff); // Take last 8 bits only

        if (comPort == null || comPort[0] == 0) {
            throw new IOException("Unable to write to COM port \"" + path + "\" as it is not open");
        }

        if (conxn.writeBytes(buffer, 1) < 0) {
            throw new IOException("Failed to write to COM port \"" + path + "\"");
        }

        if (logger.isDebugEnabled()) {
            appendDebugData(path, buffer, 0, 1, false);
        }
    }

    /**
     * Write a byte array to the COM port.
     * 
     * @param path
     *            The path to the COM device.
     * @param buffer
     *            The bytes to write.
     * @throws IOException
     *             If there was a problem writing to the COM device.
     */
    public synchronized void writeComPort(String path, byte[] buffer) throws IOException {
        int[] comPort = openPorts.get(path);

        int byteCnt = 0;

        if (comPort == null || comPort[0] == 0) {
            throw new IOException("Unable to write to COM port \"" + path + "\" as it is not open");
        }

        byteCnt = conxn.writeBytes(buffer, buffer.length);

        if (byteCnt < 0) {
            throw new IOException("Failed to write to COM port \"" + path + "\"");
        }

        if (logger.isDebugEnabled()) {
            appendDebugData(path, buffer, 0, buffer.length, false);
        }
    }

    /**
     * Flush any buffered data out the port.
     * 
     * @param path
     *            The path to the COM device.
     * @throws IOException
     *             If there was a problem flushing the COM device.
     */
    public synchronized void flushComPort(String path) throws IOException {
        int[] comPort = openPorts.get(path);

        int byteCnt;
        int readLen;
        int bufferLen = 100;

        byte[] buffer = new byte[bufferLen];

        if (comPort == null || comPort[0] == 0) {
            throw new IOException("Unable to flush COM port \"" + path + "\" as it is not open");
        }

        for (byteCnt = conxn.bytesAvailable(); byteCnt > 0; byteCnt = conxn.bytesAvailable()) {
            if (byteCnt < bufferLen) {
                readLen = conxn.readBytes(buffer, byteCnt);
            } else {
                readLen = conxn.readBytes(buffer, bufferLen);
            }

            if (readLen < 0) {
                throw new IOException("Failed to flush COM port \"" + path + "\"");
            }
        }
    }

    /**
     * Get a listing of all serial ports currently available on the system.
     * 
     * @return A list of serial ports.
     * @throws IOException
     *             If there is a problem enumerating the serial ports.
     */
    public synchronized String[] getSerialPorts() throws IOException {
        SerialPort[] commPorts = SerialPort.getCommPorts(); // COM ports available on this machine
        String[] portNames = new String[commPorts.length];

        int ndx = 0;

        for (SerialPort port : commPorts) {
            portNames[ndx] = port.getSystemPortName();
            ndx++;
        }

        if (commPorts.length == 0) {
            throw new IOException("Unable to enumerate serial ports");
        }

        return portNames;
    }

    /**
     * Keep track of read/written data to the serial port, dumping to the
     * logger's debug stream at each newline.
     * 
     * @param path
     *            The path of the serial port.
     * @param data
     *            An array containing the data that was just read/written.
     * @param off
     *            The offset within the array where the reading/writing began.
     * @param len
     *            The length of the section that was read/written.
     * @param read
     *            Whether the operation was a read (<tt>true</tt>) or a write
     *            (<tt>false</tt>).
     */
    private static void appendDebugData(String path, byte[] data, int off, int len, boolean read) {
        String name;
        if (read) {
            name = "r" + path;
        } else {
            name = "w" + path;
        }

        StringBuilder line = debugLine.get(name);
        if (line == null) {
            line = new StringBuilder();
            debugLine.put(name, line);
        }

        for (int i = 0; i < len; i++) {
            byte b = data[off + i];

            switch (b) {
            case '\0':
                line.append("\\0");
                break;
            case '\t':
                line.append("\\t");
                break;
            case '\r':
                line.append("\\r");
                break;
            default:
                if (b >= 32) { // 32 is a space, the first printable char
                    line.append((char) b);
                } else if (b != '\n') {
                    line.append('.');
                }

                if (b == '\n' || line.length() >= 80) {
                    logger.debug(String.format("%s %s: [%s]", path, read ? "read" : "write", line.toString()));
                    line.setLength(0);
                }
            }
        }
    }
}

/*
 * Matrix.java
 *
 * Acts like a matrix which is indexed by a pair of arbitrary objects.
 * Internally represented as a Hashtable of Hashtables.
 *
 * This file is property of Jonathan Pearson. SIXNET is granted a non-exclusive
 * license to use this without limitation.
 *
 * Jonathan Pearson
 * April 19, 2007
 *
 */

package org.jonp.util;

import java.util.*;

public class Matrix<I1, I2, E> {
    private Hashtable<I1, Hashtable<I2, E> > matrix;
    
    public Matrix() {
        matrix = new Hashtable<I1, Hashtable<I2, E> >();
    }
    
    public E put(I1 index1, I2 index2, E data) {
        Hashtable<I2, E> subtable = matrix.get(index1);
        
        if (subtable == null) {
            subtable = new Hashtable<I2, E>();
            matrix.put(index1, subtable);
        }
        
        return subtable.put(index2, data);
    }
    
    public E get(I1 index1, I2 index2) {
        Hashtable<I2, E> subtable = matrix.get(index1);
        
        if (subtable == null) {
            return null;
        } else {
            return subtable.get(index2);
        }
    }
    
    public E remove(I1 index1, I2 index2) {
        Hashtable<I2, E> subtable = matrix.get(index1);
        
        if (subtable == null) {
            return null;
        } else {
            E old = subtable.remove(index2);
            
            if (subtable.size() == 0) matrix.remove(index1); // Save space if we don't need it any more
            
            return old;
        }
    }
    
    public Enumeration<I1> keys1() {
        return matrix.keys();
    }
    
    public Enumeration<I1> keys1(I2 index2) {
        // There is no way to detect a ConcurrentModificationException
        Vector<I1> matching = new Vector<I1>();
        
        Enumeration<I1> level1 = matrix.keys();
        while (level1.hasMoreElements()) {
            I1 key = level1.nextElement();
            
            Hashtable<I2, E> subtable = matrix.get(key);
            
            if (subtable.containsKey(index2)) matching.add(key);
        }
        
        return matching.elements();
    }
    
    public Enumeration<I2> keys2() {
        // Also no way to detect a ConcurrentModificationException
        Vector<I2> matching = new Vector<I2>();
        
        Enumeration<I1> level1 = matrix.keys();
        while (level1.hasMoreElements()) {
            I1 key = level1.nextElement();
            
            Hashtable<I2, E> subtable = matrix.get(key);
            
            matching.addAll(subtable.keySet());
        }
        
        return matching.elements();
    }
    
    public Enumeration<I2> keys2(I1 index1) {
        Hashtable<I2, E> subtable = matrix.get(index1);
        
        if (subtable == null) {
            subtable = new Hashtable<I2, E>();
            matrix.put(index1, subtable); // In case of a ConcurrentModificationException
        }
        
        return subtable.keys();
    }
    
    public int size() {
        int ans = 0;
        
        Enumeration<I1> level1 = keys1();
        while (level1.hasMoreElements()) {
            ans += matrix.get(level1.nextElement()).size();
        }
        
        return ans;
    }
    
    public Collection<E> values() {
        Vector<E> collect = new Vector<E>();
        
        Enumeration<I1> level1 = keys1();
        while (level1.hasMoreElements()) {
            Hashtable<I2, E> subtable = matrix.get(level1);
            Enumeration<I2> level2 = subtable.keys();
            while (level2.hasMoreElements()) {
                collect.add(subtable.get(level2.nextElement()));
            }
        }
        
        return collect;
    }
}

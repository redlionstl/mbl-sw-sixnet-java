/*
 * Content.java
 *
 * Represents a piece of the content of an XML tag
 *
 * This file is property of Jonathan Pearson. SIXNET is granted a non-exclusive
 * license to use this without limitation.
 *
 * Jonathan Pearson
 * March 14, 2007
 *
 */

package org.jonp.xml;


/**
 * The content of a XML.Tag is made up of strings and more tags. This class is used
 * to hold onto both of them in a common place. Strings are broken into pieces by
 * tags between them, and tags are separate.
 */
public class Content {
    // One of these two; in C this would be a union
    private String stringVal;
    private Tag tagVal;
    
    /**
     * Construct a new String-type Content object
     * @param str The value of the object
     * 
     * @throws IllegalArgumentException If the string contains an illegal character.
     */
    public Content(String str) {
    	stringVal = str;
        tagVal = null;
    }
    
    /**
     * Construct a new Tag-type Content object
     * @param tag The value of the object
     */
    public Content(Tag tag) {
        tagVal = tag;
        stringVal = null;
    }
    
    /**
     * Is this a String-type Content object?
     * @return True if String-type, False otherwise
     */
    public boolean isString() {
        return (stringVal != null);
    }
    
    /**
     * Is this a Tag-type Content object?
     * @return True if Tag-type, False otherwise
     */
    public boolean isTag() {
        return (tagVal != null);
    }
    
    /**
     * Get the String value of this object
     * @return The String value of this object, or null if not String-type
     */
    public String getString() {
        return stringVal;
    }
    
    /**
     * Get the Tag value of this object
     * @return The Tag value of this object, or null if not Tag-type
     */
    public Tag getTag() {
        return tagVal;
    }
}

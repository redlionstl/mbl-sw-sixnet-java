/*
 * XML.java
 *
 * Parses XML input and builds an XML.Tag structure from it.
 *
 * This file is property of Jonathan Pearson. SIXNET is granted a non-exclusive
 * license to use this without limitation.
 *
 * Jonathan Pearson
 * March 13, 2007
 *
 * Update: September 4, 2007 - Modified to be more friendly to subclassing
 *
 */

package org.jonp.xml;

import java.io.*;
import java.util.List;

import javax.xml.parsers.*;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.xml.sax.*;

import com.sixnetio.util.Utils;

/**
 * An XML parser. If you need to, you can subclass by overriding
 * the parseXML(InputStream, boolean) and
 * writeXMLStream(Tag, OutputStream, boolean) functions.
 */
public class XML {
	private static final Logger logger = Logger.getLogger(Utils.thisClassName());
	
	/**
	 * Create the default XML parser. It will use the static methods for parsing data.
	 */
	public XML() {
		// Nothing to do
	}
	
	/**
	 * Parse the specified file
	 * @param fileName The name of the file to parse
	 * @param validate If true, the parser will validate the XML file using its DTD or schema
	 * @param preserveSpaces Whether to preserve leading/trailing spaces in blocks of text
	 *   between tags.
	 * @throws IOException If there is an error reading from the file
	 */
	public Tag parseXML(String fileName, boolean validate, boolean preserveSpaces) throws IOException {
		InputStream in = new FileInputStream(fileName);
		
		try {
			return parseXML(in, validate, preserveSpaces);
		} finally {
			in.close();
		}
	}
	
	/**
	 * Parse the specified file
	 * @param file The file to parse
	 * @param validate If true, the parser will validate the XML file using its DTD or schema
	 * @param preserveSpaces Whether to preserve leading/trailing spaces in blocks of text
	 *   between tags.
	 * @throws IOException If there is an error reading from the file
	 */
	public Tag parseXML(File file, boolean validate, boolean preserveSpaces) throws IOException {
		InputStream in = new FileInputStream(file);
		
		try {
			return parseXML(in, validate, preserveSpaces);
		} finally {
			in.close();
		}
	}
	
	
	/**
	 * Parse the specified XML stream until EOF is read. I suggest using MUXOutputStream for
	 * sending and MUXInputStream for receiving over a network when using this, as you can
	 * keep the socket open but still simulate an EOF when necessary
	 * @param from The stream to parse
	 * @param validate If true, the parser will validate the XML data using its DTD or schema
	 * @param preserveSpaces Whether to preserve leading/trailing spaces in blocks of text
	 *   between tags.
	 * @return The parsed root-level tag.
	 * @throws IOException If there is an error reading from the stream
	 */
	public Tag parseXML(InputStream from, boolean validate, boolean preserveSpaces) throws IOException {
		return parseXML(new InputSource(from), validate, preserveSpaces, null);
	}
	
	/**
	 * Parse the specified XML stream until EOF is read. This is useful for determining
	 * approximately where an error occurs in a stream (use a LineNumberReader).
	 * 
	 * @param from The reader to parse.
	 * @param validate If true, the parser will validate the XML data using its DTD or schema.
	 * @param preserveSpaces Whether to preserve leading/trailing spaces in blocks of text
	 *   between tags.
	 * @return The parsed root-level tag.
	 * @throws IOException If there is an error reading from the stream.
	 */
	public Tag parseXML(Reader from, boolean validate, boolean preserveSpaces) throws IOException {
		return parseXML(new InputSource(from), validate, preserveSpaces, null);
	}
	
	/**
	 * Parse the specified XML stream until EOF is read. I suggest using MUXOutputStream for
	 * sending and MUXInputStream for receiving over a network when using this, as you can
	 * keep the socket open but still simulate an EOF when necessary
	 * @param from The stream to parse
	 * @param validate If true, the parser will validate the XML data using its DTD or schema
	 * @param preserveSpaces Whether to preserve leading/trailing spaces in blocks of text
	 *   between tags.
	 * @param messages If not <tt>null</tt>, will receive messages from the parser prefixed by
	 *   'Warning', 'Error', or 'Fatal'. Synchronization is the responsibility of the caller.
	 * @return The parsed root-level tag.
	 * @throws IOException If there is an error reading from the stream
	 */
	public Tag parseXML(InputStream from, boolean validate, boolean preserveSpaces, List<String> messages) throws IOException {
		return parseXML(new InputSource(from), validate, preserveSpaces, messages);
	}
	
	/**
	 * Parse the specified XML stream until EOF is read. This is useful for determining
	 * approximately where an error occurs in a stream (use a LineNumberReader).
	 * 
	 * @param from The reader to parse.
	 * @param validate If true, the parser will validate the XML data using its DTD or schema.
	 * @param preserveSpaces Whether to preserve leading/trailing spaces in blocks of text
	 *   between tags.
	 * @param messages If not <tt>null</tt>, will receive messages from the parser prefixed by
	 *   'Warning', 'Error', or 'Fatal'. Synchronization is the responsibility of the caller.
	 * @return The parsed root-level tag.
	 * @throws IOException If there is an error reading from the stream.
	 */
	public Tag parseXML(Reader from, boolean validate, boolean preserveSpaces, List<String> messages) throws IOException {
		return parseXML(new InputSource(from), validate, preserveSpaces, messages);
	}
	
	private Tag parseXML(final InputSource from, boolean validate, boolean preserveSpaces, final List<String> messages) throws IOException {
		ErrorHandler errorHandler = new ErrorHandler() {
			@Override
            public void error(SAXParseException exception) throws SAXException {
				System.err.println("error");
				
	            if (messages != null) {
	            	if (from.getCharacterStream() != null && from.getCharacterStream() instanceof LineNumberReader) {
	            		LineNumberReader reader = (LineNumberReader)from.getCharacterStream();
	            		
	            		messages.add(String.format("Error (line %d): %s", reader.getLineNumber(), exception.getMessage()));
	            	} else {
	            		messages.add(String.format("Error: %s", exception.getMessage()));
	            	}
	            }
            }

			@Override
            public void fatalError(SAXParseException exception) throws SAXException {
				System.err.println("fatal");
				
	            if (messages != null) {
	            	if (from.getCharacterStream() != null && from.getCharacterStream() instanceof LineNumberReader) {
	            		LineNumberReader reader = (LineNumberReader)from.getCharacterStream();
	            		
	            		messages.add(String.format("Fatal (line %d): %s", reader.getLineNumber(), exception.getMessage()));
	            	} else {
	            		messages.add(String.format("Fatal: %s", exception.getMessage()));
	            	}
	            }
            }

			@Override
            public void warning(SAXParseException exception) throws SAXException {
				System.err.println("warning");
				
	            if (messages != null) {
	            	if (from.getCharacterStream() != null && from.getCharacterStream() instanceof LineNumberReader) {
	            		LineNumberReader reader = (LineNumberReader)from.getCharacterStream();
	            		
	            		messages.add(String.format("Warning (line %d): %s", reader.getLineNumber(), exception.getMessage()));
	            	} else {
	            		messages.add(String.format("Warning: %s", exception.getMessage()));
	            	}
	            }
            }
		};
		
		TagHandler tagHandler = new TagHandler(preserveSpaces);
		
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setValidating(validate);
			
			SAXParser parser = factory.newSAXParser();
			parser.getXMLReader().setErrorHandler(errorHandler);
			parser.parse(from, tagHandler);
			
			return tagHandler.getMainTag();
		} catch (SAXException saxe) {
			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.debug("Element stack:");
				
				for (Tag tag : tagHandler.getElementStack()) {
					StringBuilder summary = new StringBuilder();
					
					summary.append("<").append(tag.getName());
					for (String attName : tag.getAttNames()) {
						summary.append(" ").append(attName).append("=\"").append(tag.getStringAttribute(attName)).append("\"");
					}
					summary.append(">");
					
					logger.debug(summary.toString());
				}
			}
			
			throw new IOException(saxe);
		} catch (ParserConfigurationException pce) {
			throw new RuntimeException(pce); // This is a program error, not an execution error
		} // could also catch an IOException, but we would've just thrown it again anyway
	}
	
	/**
	 * Dump a tag to a file in a nice format, along with a proper XML header
	 * @param tag The tag to dump to the file
	 * @param fileName The name of the output file
	 * @throws IOException If there is an error writing to the file
	 */
	public void writeXMLFile(Tag tag, String fileName) throws IOException {
		OutputStream out = new FileOutputStream(fileName);
		
		try {
			writeXMLStream(tag, out, true);
		} finally {
			out.close();
		}
	}
	
	/**
	 * Dump a tag to a file in a nice format, along with a proper XML header
	 * @param tag The tag to dump to the file
	 * @param file The output file
	 * @throws IOException If there is an error writing to the file
	 */
	public void writeXMLFile(Tag tag, File file) throws IOException {
		OutputStream out = new FileOutputStream(file);
		
		try {
			writeXMLStream(tag, out, true);
		} finally {
			out.close();
		}
	}
	
	/**
	 * Dump a tag to a stream on a single line, along with a proper XML header. I suggest using MUXOutputStream
	 * to write and MUXInputStream to read over a network, to avoid having to close the socket
	 * after each transfer
	 * @param tag The tag to dump to the stream
	 * @param to The stream to dump to
	 * @throws IOException If there is an error in the underlying OutputStream
	 */
	public void writeXMLStream(Tag tag, OutputStream to) throws IOException {
		writeXMLStream(tag, to, false);
	}
	
	/**
	 * Dump a tag to a stream in either a nice format or a single line, along with a proper XML header. *
	 * suggest using MUXOutputStream to write and MUXInputStream to read over a network, to avoid having to
	 * close the socket after each transfer
	 * @param tag The tag to dump to the stream
	 * @param to The stream to dump to
	 * @param format If true, dumps with nice formatting, otherwise one line for the header and one for the
	 *   body
	 * @throws IOException If there is an error in the underlying OutputStream
	 */
	public void writeXMLStream(Tag tag, OutputStream to, boolean format) throws IOException {
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(to));
		
		pw.println("<?xml version=\"1.0\"?>");
		tag.toStream(pw, 0, format);
		pw.println();
		pw.flush();
	}
}

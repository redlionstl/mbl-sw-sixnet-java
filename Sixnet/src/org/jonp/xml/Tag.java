/*
 * Tag.java
 *
 * Represents an XML tag with attributes and contents
 *
 * This file is property of Jonathan Pearson. SIXNET is granted a non-exclusive
 * license to use this without limitation.
 *
 * Jonathan Pearson
 * March 14, 2007
 *
 */

package org.jonp.xml;

import java.io.*;
import java.util.*;

import org.xml.sax.Attributes;


/**
 * Represents an XML tag with attributes and contents. This is not thread-safe,
 * so if you are using this in a multi-threaded environment, you should perform
 * the necessary synchronization yourself.
 * 
 * @author Jonathan Pearson
 */
public class Tag implements Iterable<Content> {
	/** The separator between members in a path through the tags */
	private static final String pathSep = "/";
	
	/** The separator between a path of tags and an attribute name */
	private static final String attSep = " ";
	
	private String name;
	private Map<String, String> attributes;
	private List<Content> contents;
	
	/**
	 * Construct a new Tag with the given attributes. This is only used
	 * when parsing XML content
	 * @param name The name of the tag
	 * @param att The attributes of the tag
	 */
	public Tag(String name, Attributes att) {
		this(name);
		
		int count = att.getLength();
		for (int i = 0; i < count; i++) {
			attributes.put(att.getLocalName(i).trim(), att.getValue(i).trim());
		}
	}
	
	/**
	 * Construct a new Tag with no attributes
	 * @param name The name of the tag
	 */
	public Tag(String name) {
		attributes = new LinkedHashMap<String, String>();
		contents = new LinkedList<Content>();
		
		this.name = name.trim();
	}
	
	/**
	 * Construct a new Tag with no attributes.
	 * 
	 * @param name The name of the tag.
	 * @param content The initial content of the tag.
	 */
	public Tag(String name, String content) {
		this(name);
		
		addContent(content);
	}
	
	/**
	 * Get the name of this tag
	 * @return The name of this tag
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set the name of this tag. Note that this does not check the name for legality.
	 * @param val The new name for the tag
	 */
	public void setName(String val) {
		name = val;
	}
	
	/**
	 * Get a string attribute of this tag or a descendant. Use the
	 * format "path/to/descendant/tag attribute" to get a descendant's attribute
	 * @param attName The name of the attribute, possibly with a path to a descendant
	 * @return The specified attribute, or an empty string if not found
	 */
	public String getStringAttribute(String attName) {
		return getStringAttribute(attName, "");
	}
	
	/**
	 * Get a string attribute of this tag, or a descendant. Use the format
	 * "path/to/descendant/tag attribute" to get a dencendant's attribute
	 * 
	 * @param attName The name of the attribute, possibly with a path to a descendant
	 * @param def The default value to return if that attribute does not exist
	 * @return The specified attribute, or <tt>def</tt> if not found
	 */
	public String getStringAttribute(String attName, String def) {
		// Allows referencing of attributes of deeper tags
		// Tags are separated by pathSep, use attSep to indicate attribute name comes next
		// Ex: Tag tag is a "config" tag
		//   getStringAttribute("network/server/setup name") to get the 'name' attribute of 'setup',
		//   which is a child of server, which is a child of network, which is a child of the current
		//   tag "config"
		if (attName.indexOf(attSep) != -1) {
			// We need to delve for it
			Tag tag = getTag(attName.substring(0, attName.indexOf(attSep)));
			
			if (tag == null) {
				return "";
			} else {
				return tag.getStringAttribute(attName.substring(attName.indexOf(attSep) + attSep.length()), def);
			}
		} else {
			String str = attributes.get(attName);
			
			if (str == null) {
				return def;
			} else {
				return str;
			}
		}
	}
	
	/**
	 * Get an integer attribute. See getStringAttribute(String attName) for special usage
	 * @param attName The name of the attribute
	 * @return The attribute parsed as an integer, or 0 if not found/parse error
	 */
	public int getIntAttribute(String attName) {
		return getIntAttribute(attName, 0);
	}
	
	/**
	 * Get an integer attribute. See getStringAttribute(String attName) for special usage
	 * @param attName The name of the attribute
	 * @param def The default value, if the attribute does not exist or there is a parse error
	 * @return The attribute parsed as an integer, or def if not found/parse error
	 */
	public int getIntAttribute(String attName, int def) {
		String val = getStringAttribute(attName, null);
		
		if (val == null) {
			return def;
		} else {
			try {
				return Integer.parseInt(val);
			} catch (NumberFormatException nfe) {
				return def;
			}
		}
	}
	
	/**
	 * Get a long attribute. See getStringAttribute(String attName) for special usage
	 * @param attName The name of the attribute
	 * @return The attribute parsed as a long, or 0 if not found/parse error
	 */
	public long getLongAttribute(String attName) {
		return getLongAttribute(attName, 0);
	}
	
	/**
	 * Get a long attribute. See getStringAttribute(String attName) for special usage
	 * @param attName The name of the attribute
	 * @param def The default value, if the attribute does not exist or there is a parse error
	 * @return The attribute parsed as a long, or def if not found/parse error
	 */
	public long getLongAttribute(String attName, long def) {
		String val = getStringAttribute(attName, null);
		
		if (val == null) {
			return def;
		} else {
			try {
				return Long.parseLong(val);
			} catch (NumberFormatException nfe) {
				return def;
			}
		}
	}
	
	/**
	 * Get a float attribute. See getStringAttribute(String attName) for special usage
	 * @param attName The name of the attribute
	 * @return The attribute parsed as a float, or 0 if not found/parse error
	 */
	public float getFloatAttribute(String attName) {
		return getFloatAttribute(attName, 0);
	}
	
	/**
	 * Get a float attribute. See getStringAttribute(String attName) for special usage
	 * @param attName The name of the attribute
	 * @param def The default value, if the attribute does not exist or there is a parse error
	 * @return The attribute parsed as a float, or def if not found/parse error
	 */
	public float getFloatAttribute(String attName, float def) {
		String val = getStringAttribute(attName, null);
		
		if (val == null) {
			return def;
		} else {
			try {
				return Float.parseFloat(val);
			} catch (NumberFormatException nfe) {
				return def;
			}
		}
	}
	
	/**
	 * Get a double attribute. See getStringAttribute(String attName) for special usage
	 * @param attName The name of the attribute
	 * @return The attribute parsed as a double, or 0 if not found/parse error
	 */
	public double getDoubleAttribute(String attName) {
		return getDoubleAttribute(attName, 0);
	}
	
	/**
	 * Get a double attribute. See getStringAttribute(String attName) for special usage
	 * @param attName The name of the attribute
	 * @param def The default value, if the attribute does not exist or there is a parse error
	 * @return The attribute parsed as a double, or def if not found/parse error
	 */
	public double getDoubleAttribute(String attName, double def) {
		String val = getStringAttribute(attName, null);
		
		if (val == null) {
			return def;
		} else {
			try {
				return Double.parseDouble(val);
			} catch (NumberFormatException nfe) {
				return def;
			}
		}
	}
	
	/**
	 * Get a boolean attribute. See getStringAttribute(String attName) for special usage
	 * @param attName The name of the attribute
	 * @return The attribute parsed as a boolean (case-insensitive "true" is the only true value)
	 */
	public boolean getBooleanAttribute(String attName) {
		return getBooleanAttribute(attName, false);
	}
	
	/**
	 * Get a boolean attribute. See getStringAttribute(String attName) for special usage
	 * @param attName The name of the attribute
	 * @param def The default value, if the attribute does not exist or there is a parse error
	 * @return The attribute parsed as a boolean (case-insensitive "true" is the only true value), or def
	 *   if not found
	 */
	public boolean getBooleanAttribute(String attName, boolean def) {
		String val = getStringAttribute(attName, null);
		
		if (val == null) {
			return def;
		} else {
			return Boolean.parseBoolean(val);
		}
	}
	
	
	/**
	 * Append a string to the content of this tag
	 * @param str The string to append
	 */
	public void addContent(String str) {
		addContent(new Content(str));
	}
	
	/**
	 * Append a tag to the content of this tag
	 * @param tag The tag to append
	 */
	public void addContent(Tag tag) {
		addContent(new Content(tag));
	}
	
	/**
	 * Append a Content object to the content of this tag
	 * @param content The content to append
	 */
	public void addContent(Content content) {
		contents.add(content);
	}
	
	/**
	 * Determine the position of a Content object in the contents
	 * @param content The Content object to search for
	 * @return The index of that specific Content object, or -1 if not found
	 */
	public int getPosition(Content content) {
		return contents.indexOf(content);
	}
	
	/**
	 * Grab all strings from the contents of this tag and return them, concatenated,
	 * with spaces in between
	 * @return The string contents of this tag
	 */
	public String getContents() {
		return getContents(true);
	}
	
	/**
	 * Grab all string srom the contents of this tag and return them, concatenated
	 * @param withSpaces If true, put a space between each string
	 * @return The string contents of this tag
	 */
	public String getContents(boolean withSpaces) {
		String ans = "";
		
		for (Content c : contents) {
			if (c.isString()) {
				if (ans.length() > 0 && withSpaces) ans += " ";
				ans += c.getString();
			}
		}
		
		return ans;
	}
	
	/**
	 * Grab all strings from the contents of this tag and all descendants, concatenated,
	 * with spaces in between
	 * @return The string contents of this tag and all descendants
	 */
	public String getAllContents() {
		return getAllContents(true);
	}
	
	/**
	 * Grab all strings from the contents of this tag and all descendants, concatenated
	 * @param withSpaces If true, put spaces between each string
	 * @return The string contents of this tag and all descendants
	 */
	public String getAllContents(boolean withSpaces) {
		String ans = "";
		
		for (Content c : contents) {
			if (ans.length() > 0 && withSpaces) ans += " ";
			if (c.isString()) {
				ans += c.getString();
			} else {
				ans += c.getTag().getAllContents(withSpaces);
			}
		}
		
		return ans;
	}
	
	/**
	 * Another name for getContents
	 * @return The string contents of this tag, or null if none is found
	 */
	public String getStringContents() {
		return getContents();
	}
	
	/**
	 * Get the contents of a descendant of this tag, or null if not found
	 * @param id The path to the tag
	 * @return The contents of the tag, or null if it is not found (if the
	 *   tag exists but has no string content, "" will be returned)
	 */
	public String getStringContents(String id) {
		return getStringContents(id, null);
	}
	
	/**
	 * Get the contents of a descendant of this tag, or def if not found
	 * @param id The path to the tag
	 * @param def The default value, if the tag is not found
	 * @return The contents of the tag, def if the tag is not found, or "" if
	 *   the tag has no string content
	 */
	public String getStringContents(String id, String def) {
		Tag tag = getTag(id);
		
		if (tag == null) {
			return def;
		} else {
			return tag.getStringContents();
		}
	}
	
	/**
	 * Another name for getAllContents()
	 * @return The string contents of this tag and all descendants
	 */
	public String getAllStringContents() {
		return getAllContents();
	}
	
	/**
	 * Get the contents of this tag, parsed as an integer, defaulting to 0
	 * @return The content of this tag, parsed as an integer
	 */
	public int getIntContents() {
		return getIntContents(0);
	}
	
	/**
	 * Get the contents of this tag, parsed as an integer, defaulting to def
	 * @param def The value returned if there is a parse error
	 * @return The content of this tag, parsed as an integer
	 */
	public int getIntContents(int def) {
		String c = getContents();
		
		try {
			return Integer.parseInt(c);
		} catch (NumberFormatException nfe) {
			return def;
		}
	}
	
	/**
	 * Get the contents of a descendant tag, parsed as an integer defaulting to 0
	 * @param id The path to the descendant tag
	 * @return The content of the specified tag, parsed as an integer
	 */
	public int getIntContents(String id) {
		return getIntContents(id, 0);
	}
	
	/**
	 * Get the contents of a descendant tag, parsed as an integer defaulting to def
	 * @param id The path to the descendant tag
	 * @param def The value returned if there is a parse error or the tag is not found
	 * @return The content of the specified tag, parsed as an integer
	 */
	public int getIntContents(String id, int def) {
		Tag tag = getTag(id);
		
		if (tag == null) {
			return def;
		} else {
			return tag.getIntContents(def);
		}
	}
	
	/**
	 * Get the contents of this tag, parsed as a long defaulting to 0
	 * @return The content of this tag, parsed as a long
	 */
	public long getLongContents() {
		return getLongContents(0);
	}
	
	/**
	 * Get the contents of this tag, parsed as a long, defaulting to def
	 * @param def The value returned if there is a parse error
	 * @return The content of this tag, parsed as a long
	 */
	public long getLongContents(long def) {
		String c = getContents();
		
		try {
			return Long.parseLong(c);
		} catch (NumberFormatException nfe) {
			return def;
		}
	}
	
	/**
	 * Get the contents of a descendant tag, parsed as a long defaulting to 0
	 * @param id The path to the descendant tag
	 * @return The content of the specified tag, parsed as a long
	 */
	public long getLongContents(String id) {
		return getLongContents(id, 0);
	}
	
	/**
	 * Get the contents of a descendant tag, parsed as a long defaulting to def
	 * @param id The path to the descendant tag
	 * @param def The value returned if there is a parse error or the tag is not found
	 * @return The content of the specified tag, parsed as a long
	 */
	public long getLongContents(String id, long def) {
		Tag tag = getTag(id);
		
		if (tag == null) {
			return def;
		} else {
			return tag.getLongContents(def);
		}
	}

	/**
	 * Get the contents of this tag, parsed as a float defaulting to 0
	 * @return The content of this tag, parsed as a float
	 */
	public float getFloatContents() {
		return getFloatContents(0);
	}
	
	/**
	 * Get the contents of this tag, parsed as a float, defaulting to def
	 * @param def The value returned if there is a parse error
	 * @return The content of this tag, parsed as a float
	 */
	public float getFloatContents(float def) {
		String c = getContents();
		
		try {
			return Float.parseFloat(c);
		} catch (NumberFormatException nfe) {
			return def;
		}
	}
	
	/**
	 * Get the contents of a descendant tag, parsed as a float defaulting to 0
	 * @param id The path to the descendant tag
	 * @return The content of the specified tag, parsed as a float
	 */
	public float getFloatContents(String id) {
		return getFloatContents(id, 0);
	}
	
	/**
	 * Get the contents of a descendant tag, parsed as a float defaulting to def
	 * @param id The path to the descendant tag
	 * @param def The value returned if there is a parse error or the tag is not found
	 * @return The content of the specified tag, parsed as a float
	 */
	public float getFloatContents(String id, float def) {
		Tag tag = getTag(id);
		
		if (tag == null) {
			return def;
		} else {
			return tag.getFloatContents(def);
		}
	}
	
	/**
	 * Get the contents of this tag, parsed as a double
	 * @return The content of this tag, parsed as a double
	 */
	public double getDoubleContents() {
		return getDoubleContents(0);
	}
	
	/**
	 * Get the contents of this tag, parsed as a double, defaulting to def
	 * @param def The value returned if there is a parse error
	 * @return The content of this tag, parsed as a double
	 */
	public double getDoubleContents(double def) {
		String c = getContents();
		
		try {
			return Double.parseDouble(c);
		} catch (NumberFormatException nfe) {
			return def;
		}
	}
	
	/**
	 * Get the contents of a descendant tag, parsed as a double defaulting to 0
	 * @param id The path to the descendant tag
	 * @return The content of the specified tag, parsed as a double
	 */
	public double getDoubleContents(String id) {
		return getDoubleContents(id, 0);
	}
	
	/**
	 * Get the contents of a descendant tag, parsed as a double defaulting to def
	 * @param id The path to the descendant tag
	 * @param def The value returned if there is a parse error or the tag is not found
	 * @return The content of the specified tag, parsed as a double
	 */
	public double getDoubleContents(String id, double def) {
		Tag tag = getTag(id);
		
		if (tag == null) {
			return def;
		} else {
			return tag.getDoubleContents(def);
		}
	}
	
	/**
	 * Get the contents of this tag, parsed as a boolean
	 * @return The content of this tag, parsed as a boolean
	 */
	public boolean getBooleanContents() {
		return getBooleanContents(false);
	}
	
	/**
	 * Get the contents of this tag, parsed as a boolean
	 * @param def The value returned if there is a parse error (impossible with booleans)
	 * @return The content of this tag, parsed as a boolean
	 */
	public boolean getBooleanContents(boolean def) {
		String c = getContents();
		
		return Boolean.parseBoolean(c);
	}
	
	/**
	 * Get the contents of a descendant tag, parsed as a boolean defaulting to false
	 * @param id The path to the descendant tag
	 * @return The content of the specified tag, parsed as a boolean
	 */
	public boolean getBooleanContents(String id) {
		return getBooleanContents(id, false);
	}
	
	/**
	 * Get the contents of a descendant tag, parsed as a boolean defaulting to def
	 * @param id The path to the descendant tag
	 * @param def The value returned if there is a parse error or the tag is not found
	 * @return The content of the specified tag, parsed as a boolean
	 */
	public boolean getBooleanContents(String id, boolean def) {
		Tag tag = getTag(id);
		
		if (tag == null) {
			return def;
		} else {
			return tag.getBooleanContents(def);
		}
	}
	
	/**
	 * Get the string contents of every tag in the given path with the given name.
	 * @param path The path to search
	 * @param tagName The tag name to search for
	 * @return The string contents of each matching tag
	 */
	public String[] getEach(String path, String tagName) {
		Tag[] tags = getEachTag(path, tagName);
		String[] ans = new String[tags.length];
		
		for (int i = 0; i < ans.length; i++) {
			ans[i] = tags[i].getStringContents();
		}
		
		return ans;
	}
	
	/**
	 * Get every tag with the given name that is a child of the tag with the specified path.
	 * @param path The path to search
	 * @param tagName The tag name to search for, or <tt>null</tt> to return all tags
	 * @return All of the tags that match
	 */
	public Tag[] getEachTag(String path, String tagName) {
		Vector<Tag> ans = new Vector<Tag>();
		Tag tag = getTag(path);
		
		if (tag != null) {
			Iterator<Content> it = tag.iterator();
			while (it.hasNext()) {
				Content c = it.next();
				if (!c.isTag()) continue;
				
				if (tagName == null || c.getTag().getName().equals(tagName)) {
					ans.add(c.getTag());
				}
			}
		}
		
		return ans.toArray(new Tag[0]);
	}
	
	/**
	 * Remove all contents from this tag
	 */
	public void removeContents() {
		contents.clear();
	}
	
	/**
	 * Remove all attributes from this tag
	 */
	public void removeAttributes() {
		attributes.clear();
	}
	
	/**
	 * Remove the specified attribute from this tag or a descendant
	 * (see getStringAttribute(String attName) for the format of attName).
	 * Does nothing if the attribute does not exist.
	 * @param attName The attribute to remove
	 */
	public void removeAttribute(String attName) {
		if (attName.indexOf(attSep) != -1) {
			Tag tag = getTag(attName.substring(0, attName.indexOf(attSep)));
			
			if (tag != null) tag.removeAttribute(attName.substring(attName.indexOf(attSep) + attSep.length()));
		} else {
			attributes.remove(attName);
		}
	}
	
	/**
	 * Add or replace the specified attribute of this tag or a descendant
	 * (see getStringAttribute(String attName) for the format of attName) with
	 * the given value
	 * @param attName The attribute to set
	 * @param val The value to give it
	 * 
	 * @throws IllegalArgumentException If the value would not be representable in textual output.
	 */
	public void setAttribute(String attName, String val) {
		if (val.indexOf('"') != -1 && val.indexOf('\'') != -1) {
			throw new IllegalArgumentException("Attribute value contains both types of quote characters");
		}
		
		if (attName.indexOf(attSep) != -1) {
			Tag tag = getTag(attName.substring(0, attName.indexOf(attSep)));
			
			if (tag == null) {
				throw new NullPointerException("The specified tag does not exist");
			} else {
				tag.setAttribute(attName.substring(attName.indexOf(attSep) + attSep.length()), val);
			}
		} else {
			attributes.put(attName, val);
		}
	}
	
	/**
	 * Add or replace the specified attribute of this tag or a descendant
	 * (see getStringAttribute(String attName) for the format of attName) with
	 * the given value
	 * @param attName The attribute to set
	 * @param val The value to give it
	 */
	public void setAttribute(String attName, int val) {
		setAttribute(attName, "" + val);
	}
	
	/**
	 * Add or replace the specified attribute of this tag or a descendant
	 * (see getStringAttribute(String attName) for the format of attName) with
	 * the given value
	 * @param attName The attribute to set
	 * @param val The value to give it
	 */
	public void setAttribute(String attName, long val) {
		setAttribute(attName, "" + val);
	}
	
	/**
	 * Add or replace the specified attribute of this tag or a descendant
	 * (see getStringAttribute(String attName) for the format of attName) with
	 * the given value
	 * @param attName The attribute to set
	 * @param val The value to give it
	 */
	public void setAttribute(String attName, float val) {
		setAttribute(attName, "" + val);
	}
	
	/**
	 * Add or replace the specified attribute of this tag or a descendant
	 * (see getStringAttribute(String attName) for the format of attName) with
	 * the given value
	 * @param attName The attribute to set
	 * @param val The value to give it
	 */
	public void setAttribute(String attName, double val) {
		setAttribute(attName, "" + val);
	}
	
	/**
	 * Add or replace the specified attribute of this tag or a descendant
	 * (see getStringAttribute(String attName) for the format of attName) with
	 * the given value
	 * @param attName The attribute to set
	 * @param val The value to give it
	 */
	public void setAttribute(String attName, boolean val) {
		setAttribute(attName, "" + val);
	}
	
	
	/**
	 * Get an iterator over the contents of this tag. Use this to remove contents if necessary
	 * @return An iterator which will loop over the contents of this tag
	 */
	public Iterator<Content> iterator() {
		return contents.iterator();
	}
	
	/**
	 * Get the set of attribute names in this tag
	 * @return A set containing all of the names of the attributes of this tag
	 */
	public Set<String> getAttNames() {
		return attributes.keySet();
	}
	
	
	/**
	 * Get a descendant of this tag. Separate each level of the path with a '/'
	 * @param id The path to the descendant tag
	 * @return The descendant tag, or null if none is found
	 */
	public Tag getTag(String id) {
		// id is a /-separated list of tag names; each one is found, and its children searched for the next
		StringTokenizer tok = new StringTokenizer(id, pathSep);
		Tag cur = this;
		while (tok.hasMoreTokens()) {
			String tagName = tok.nextToken();
			cur = cur.getChild(tagName);
			
			if (cur == null) return null;
		}
		
		return cur;
	}
	
	/**
	 * Remove a descendant of this tag. Separate each level of the path with
	 *   a '/'.
	 * 
	 * @param id The path to the descendant tag.
	 */
	public void removeTag(String id) {
	    StringTokenizer tok = new StringTokenizer(id, pathSep);
	    
	    Tag parent = null;
	    Tag cur = this;
	    while (tok.hasMoreTokens()) {
	    	String tagName = tok.nextToken();
	    	
	    	parent = cur;
	    	cur = cur.getChild(tagName);
	    	
	    	// Doesn't exist? Nothing to do
	    	if (cur == null) return;
	    }
	    
	    parent.removeTag(cur);
    }
	
	/**
	 * Remove the given child tag of this tag. Does not drop through descendants.
	 * 
	 * @param tag The tag to remove.
	 */
	public void removeTag(Tag tag) {
		for (Iterator<Content> itContents = contents.iterator(); itContents.hasNext(); ) {
			Content content = itContents.next();
			if (content.getTag() == tag) {
				itContents.remove();
				return;
			}
		}
	}
	
	/**
	 * Get a child of this tag. This does not locate descendant tags with a path, only
	 * immediate children of this tag by name
	 * @param id The name of the child tag
	 * @return The child tag, or null if none is found
	 */
	public Tag getChild(String id) {
		// id is the name of a child tag
		for (Content c : contents) {
			if (c.isTag() && c.getTag().getName().equals(id)) {
				return c.getTag();
			}
		}
		
		return null;
	}
	
	/**
	 * Test for existance of a tag path
	 * @param id The path to the descendant tag
	 * @return If the tag exists, returns true; else returns false
	 */
	public boolean tagExists(String id) {
		return (getTag(id) != null);
	}
	
	/**
	 * Test for existance of an attribute
	 * @param id The path to the attribute
	 * @return If the tag exists, returns true; else returns false
	 */
	public boolean attExists(String id) {
		if (id.indexOf(attSep) != -1) {
			Tag tag = getTag(id.substring(0, id.indexOf(attSep)));
			
			if (tag != null) {
				return tag.attExists(id.substring(id.indexOf(attSep) + attSep.length()));
			} else {
				return false;
			}
		} else {
			return attributes.containsKey(id);
		}
	}
	
	
	/**
	 * How many Content objects does this tag hold?
	 * @return The number of Content objects in this tag
	 */
	public int countContents() {
		return contents.size();
	}
	
	/**
	 * How many attributes does this tag have?
	 * @return The number of attributes in this tag
	 */
	public int countAttributes() {
		return attributes.size();
	}
	
	/**
	 * Get a well-formatted string containing the contents of this tag and all descendants
	 * @return A well-formatted XML tag
	 */
	@Override
    public String toString() {
		return toString(0, true);
	}
	
	/**
	 * Get an optionally-formatted string containing the contents of this tag and all descendants
	 * @param withSpaces If true, will format with tabs and newlines, otherwise everything will
	 *   be on a single line
	 * @return An optionally-formatted XML tag
	 */
	public String toString(boolean withSpaces) {
		return toString(0, withSpaces);
	}
	
	/**
	 * Get an optionally-formatted string containing the contents of this tag and all descendants,
	 * starting at the specified indentation level
	 * @param indent How many tabs to place in front of the top-level tag
	 * @param withSpaces If true, will use tabs and newlines, otherwise everything will be on one line
	 *   with no tabs in between
	 */
	public String toString(int indent, boolean withSpaces) {
		// This is the same as toStream, make sure to change both if you change one
		
		StringBuilder str = new StringBuilder();
		
		if (withSpaces) str.append(tabs(indent));
		
		str.append("<");
		str.append(getName());
		
		for (String key : getAttNames()) {
			String value = getStringAttribute(key);
			
			str.append(" ");
			str.append(key);
			
			if (value.indexOf('"') == -1) {
				str.append("=\"");
				str.append(value);
				str.append("\"");
			} else {
				// Assume that it's a legal value and setAttribute() did its job
				str.append("='");
				str.append(value);
				str.append("'");
			}
		}
		
		if (contents.size() > 0) {
			str.append(">");
			
			Iterator<Content> it = iterator();
			while (it.hasNext()) {
				Content c = it.next();
				
				if (withSpaces) str.append("\n");
				
				if (c.isString()) {
					if (withSpaces) str.append(tabs(indent + 1));
					
					String value = c.getString();
					value = sanitize(value);
					
					str.append(value);
				} else {
					str.append(c.getTag().toString(indent + 1, withSpaces));
				}
			}
			
			if (withSpaces) {
				str.append("\n");
				str.append(tabs(indent));
			}
			
			str.append("</");
			str.append(getName());
			str.append(">");
		} else {
			str.append(" />");
		}
		
		return str.toString();
	}
	
	/**
	 * Similar to toString(), but instead of writing to a String, this writes to a stream. Also,
	 * this DOES NOT format with tabs and newlines by default
	 * @param out The stream to receive the tag
	 * @throws IOException When there is an error in the underlying OutputStream
	 */
	public void toStream(OutputStream out) throws IOException {
		toStream(new PrintWriter(new OutputStreamWriter(out)), 0, false);
	}
	
	/**
	 * Same as toString(boolean withSpaces), but instead of writing to a String, this writes to
	 * a stream
	 * @param out The stream to receive the tag
	 * @param withSpaces If true, will include tabs and newlines in the output
	 * @throws IOException When there is an error in the underlying OutputStream
	 */
	public void toStream(OutputStream out, boolean withSpaces) throws IOException {
		toStream(new PrintWriter(new OutputStreamWriter(out)), 0, withSpaces);
	}
	
	/**
	 * Same as toString(int indent, boolean withSpaces), but instead of writing to a String, this
	 * writes to a PrintWriter
	 * @param out The writer to receive the tag. This is a PrintWriter for easy writing and to
	 *   avoid excessive memory consumption in a large tag structure by holding on to many new
	 *   copies of PrintWriter objects as we descend
	 * @param indent The number of tabs to place in front of the root tag
	 * @param withSpaces If true, will include tabs and newlines in the output
	 * @throws IOException When there is an error in the underlying PrintWriter
	 */
	public void toStream(PrintWriter out, int indent, boolean withSpaces) throws IOException {
		// This is the same as toString, make sure to change both if you change one
		
		if (withSpaces) out.print(tabs(indent));
		
		out.print("<");
		out.print(getName());
		
		for (String key : getAttNames()) {
			String value = getStringAttribute(key);
			
			out.print(" ");
			out.print(key);
			
			if (value.indexOf('"') == -1) {
				out.print("=\"");
				out.print(value);
				out.print("\"");
			} else {
				// Assume that it's a legal value and setAttribute() did its job
				out.print("='");
				out.print(value);
				out.print("'");
			}
		}
		
		if (contents.size() > 0) {
			out.print(">");
			
			Iterator<Content> it = iterator();
			while (it.hasNext()) {
				Content c = it.next();
				
				if (withSpaces) out.println();
				
				if (c.isString()) {
					if (withSpaces) out.print(tabs(indent + 1));
					
					String value = c.getString();
					value = sanitize(value);
					
					out.print(value);
				} else {
					c.getTag().toStream(out, indent + 1, withSpaces);
				}
			}
			
			if (withSpaces) {
				out.println();
				out.print(tabs(indent));
			}
			
			out.print("</");
			out.print(getName());
			out.print(">");
		} else {
			out.print(" />");
		}
		
		out.flush();
	}
	
	/**
	 * Create a string of the specified number of tab characters
	 * @param count The number of tabs to put in the string
	 * @return A string of tabs, or the empty string if dontSkip is false
	 */
	protected static String tabs(int count) {
		char[] chrs = new char[count];
		
		for (int i = 0; i < count; i++) {
			chrs[i] = '\t';
		}
		
		return new String(chrs);
	}
	
	/**
	 * Sanitize a string so that it may be properly saved/restored in XML.
	 * @param s The string to sanitize.
	 * @return The sanitized string.
	 */
	private String sanitize(String s) {
		// NEED to do this first, otherwise something like '&lt;' will
		//   become '&amp;lt;'
		s = s.replaceAll("&", "&amp;");
		
		s = s.replaceAll("<", "&lt;");
		s = s.replaceAll(">", "&gt;");
		
		return s;
	}
}

/*
 * TagHandler.java
 *
 * An implementation of DefaultHandler for building the tag structure
 * used in this package.
 *
 * This file is property of Jonathan Pearson. SIXNET is granted a non-exclusive
 * license to use this without limitation.
 *
 * Jonathan Pearson
 * September 4, 2007
 *
 */

package org.jonp.xml;

import java.util.*;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * An implementation of DefaultHandler for building this specific Tag structure. This
 * is public only to facilitate subclassing by other packages; there is probably no
 * other reason to use this class.
 */
public class TagHandler extends DefaultHandler {
	protected Tag myTag;
	protected Stack<Tag> elementStack;
	
	protected boolean preserveSpaces;
	
	/**
	 * Construct a new TagHandler
	 */
	public TagHandler(boolean preserveSpaces) {
		this.preserveSpaces = preserveSpaces;
		
		elementStack = new Stack<Tag>();
	}
	
	/**
	 * Get the element stack of unclosed tags. Useful for debugging.
	 */
	public List<Tag> getElementStack() {
		List<Tag> stack = new LinkedList<Tag>(elementStack);
		
		if (myTag != null) stack.add(myTag);
		
		return stack;
	}
	
	/**
	 * Start a new tag, see DefaultHandler for arguments
	 */
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (myTag != null) {
			elementStack.push(myTag);
		}
		
		myTag = new Tag(qName, attributes);
	}
	
	/**
	 * End a tag, see DefaultHandler for arguments
	 */
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (!elementStack.empty()) {
			Tag tempTag = elementStack.pop();
			tempTag.addContent(myTag);
			myTag = tempTag;
		}
	}
	
	/**
	 * Process characters, see DefaultHandler for arguments
	 */
	public void characters(char[] ch, int start, int length) throws SAXException {
		String str = new String(ch, start, length);
		
		myTag.addContent(str);
	}
	
	/**
	 * Return the current tag in this object. If parsing has completed, this will
	 * be the root tag of the document
	 * @return The root tag of the document, assuming parsing is complete
	 */
	public Tag getMainTag() {
		// Perform final parsing on the tag tree
		cleanup(myTag);
		
		return myTag;
	}
	
	private void cleanup(Tag tag) {
		// Combine sections of text into a single blob using this
		StringBuilder text = new StringBuilder();
		
		// Tags don't have much support for editing contents, so we
		//   need to build new content, clear the old, and set the new
		List<Content> newContents = new LinkedList<Content>();
		
		for (Iterator<Content> itContents = tag.iterator(); itContents.hasNext(); ) {
			Content c = itContents.next();
			
			if (c.isTag()) {
				// If it's a tag, finalize any text we may have built up
				if (text.length() > 0) {
					String s = text.toString();
					
					// Trim as necessary
					if (!preserveSpaces) {
						s = s.trim();
					}
					
					newContents.add(new Content(s));
					
					// Prepare for more text
					text = new StringBuilder();
				}
				
				// Cleanup the child tag
				cleanup(c.getTag());
				newContents.add(c);
			} else {
				// Text? Append to our blob
				text.append(c.getString());
			}
			
			// To save a little bit of memory (throw out the strings
			//   after we've appended them)
			itContents.remove();
		}
		
		// Don't forget any trailing text!
		if (text.length() > 0) {
			String s = text.toString();
			
			// Trim as necessary
			if (!preserveSpaces) {
				s = s.trim();
			}
			
			// Add to the new contents
			newContents.add(new Content(s));
		}
		
		// Rebuild the tag contents
		for (Content c : newContents) {
			tag.addContent(c);
		}
	}
}
/*
 * MUXOutputStream.java
 *
 * A Multiplexed output stream, gives the impression that multiple files are being sent.
 * This allows a MUXInputStream to be read until it seems to end (EOF occurs), however
 * the stream stays open and can be reset to continue reading from a this stream.
 * NOTE: Only use this stream for TEXT DATA, as it uses byte 0 to mark the end of a stream.
 *
 * This file is property of Jonathan Pearson. SIXNET is granted a non-exclusive
 * license to use this without limitation.
 *
 * Jonathan Pearson
 * March 14, 2007
 *
 */

package org.jonp.io;

import java.io.*;

public class MUXOutputStream extends OutputStream {
    private OutputStream tgt;
    
    public MUXOutputStream(OutputStream target) {
        tgt = target;
        
        if (tgt == null) {
            throw new IllegalArgumentException("target stream cannot be null");
        }
    }
    
    public void write(int b) throws IOException {
        tgt.write(b);
    }
    
    public void startNew() throws IOException {
        tgt.write(MUXInputStream.SEP_CHAR);
    }
}

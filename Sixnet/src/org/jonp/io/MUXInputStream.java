/*
 * MUXInputStream.java
 *
 * A Multiplexed input stream, gives the impression that multiple files are being sent.
 * This allows a single stream to be read until it seems to end (EOF occurs), however
 * the stream stays open and can be reset to continue reading from a real input stream.
 * NOTE: Only use this stream for TEXT DATA, as it uses byte 0 to mark the end of a stream.
 *
 * This file is property of Jonathan Pearson. SIXNET is granted a non-exclusive
 * license to use this without limitation.
 *
 * Jonathan Pearson
 * March 14, 2007
 *
 */

package org.jonp.io;

import java.io.*;

public class MUXInputStream extends InputStream {
    protected static final int SEP_CHAR = 0;
    
    private boolean eof;
    private InputStream src;
    
    public MUXInputStream(InputStream source) {
        eof = false;
        src = source;
        
        if (src == null) {
            throw new IllegalArgumentException("source stream cannot be null");
        }
    }
    
    public int read() throws IOException {
        if (eof) return -1;
        
        int val = src.read();
        if (val == SEP_CHAR) {
            eof = true;
            return -1;
        } else {
            return val;
        }
    }
    
    public void startNew() {
        eof = false;
    }
}
